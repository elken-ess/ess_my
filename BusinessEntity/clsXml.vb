Imports System.Xml
Imports BusinessEntity


Public Class clsXml

#Region "Declaration"

    Private fstrXmlFile As String
    Private fstrXmlLabelID As String
    Private fstrXmlLabelName As String
    Private xmlDataDoc As XmlDataDocument
    Private xmlLoad As Boolean

#End Region

#Region "Properties"

    Public Property XmlFile() As String
        Get

            Return fstrXmlFile
        End Get
        Set(ByVal Value As String)
            xmlLoad = False
            fstrXmlFile = Value
        End Set
    End Property

    Public Property XmlLabelID() As String
        Get
            Return fstrXmlLabelID
        End Get
        Set(ByVal Value As String)
            fstrXmlLabelID = Value
        End Set
    End Property

    Public Property XmlLabelName() As String
        Get
            Return fstrXmlLabelName
        End Get
        Set(ByVal Value As String)
            fstrXmlLabelName = Value
        End Set
    End Property
#End Region

#Region "Methods"

    Public Function GetLabelName(ByVal inpXmlName As String, ByVal inpLabelID As String) As String
        Dim node As XmlNode
        Dim result As String = ""
        Dim objLanguage As New clsCommonClass

        

        result = objLanguage.LanguageSelect(inpLabelID, inpXmlName)

        If result.Trim = "" Then
            'If xmlLoad = False Then
            xmlDataDoc = New XmlDataDocument
            xmlDataDoc.Load(fstrXmlFile)
            xmlLoad = True
            'End If

            fstrXmlLabelID = inpLabelID


            node = xmlDataDoc.SelectSingleNode("/" & inpXmlName & "/BB_FIL[@ID='" & fstrXmlLabelID & "']/" & "FLDNM")
            If node Is Nothing Then result = ""
            If Not node Is Nothing Then
                Dim value As String
                value = node.InnerXml.ToString()
                result = value
            End If

            If result.Trim <> "" Then

                objLanguage.InsertLanguage(fstrXmlLabelID, result, inpXmlName)
            End If
        End If
        objLanguage = Nothing

        GetLabelName = result

    End Function

    

    Public Function GetLabelID(ByVal inpXmlName As String, ByVal inpLabelNM As String) As String
        Dim node As XmlNodeList
        Dim result As String

        If xmlLoad = False Then
            xmlDataDoc = New XmlDataDocument
            xmlDataDoc.Load(fstrXmlFile)
        End If

        fstrXmlLabelName = inpLabelNM

        'node = xmlDataDoc.SelectSingleNode("/" & inpXmlName & "/BB_FIL/FLDNM")
        node = xmlDataDoc.SelectNodes("/" & inpXmlName & "/BB_FIL/FLDNM")
        If node.Count > 0 Then
            Dim value As String
            Dim count As Integer
            For count = 0 To node.Count - 1
                If node(count).InnerXml.Equals(fstrXmlLabelName) Then
                    value = node(count).ParentNode.Attributes("ID").Value.ToString()
                    result = value
                End If
            Next
        End If
        GetLabelID = result
    End Function

#End Region

   

End Class
