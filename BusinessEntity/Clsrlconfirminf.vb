Imports System.Xml
Imports SQLDataAccess
Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Web
Imports System.Data




Public Class clsrlconfirminf

#Region "Declaration"

    Private fstrXmlFile As String
  
    Private xmlDoc As XmlDocument
    Private xmlDataDoc As XmlDataDocument
    Private status As String
    Private fstrSpName As String ' store procedure name

#End Region

#Region "Methods"
    Public Function searchconfirminf(ByVal statFlag As String) As ArrayList

        'Dim statConnection As SqlConnection = Nothing
        Dim statConn As SqlConnection = Nothing
        'statConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        statConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        'Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_SELECTSTATUS"
        Try
            'trans = statConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(1) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(statConn, fstrSpName)
            Param(0).Value = Trim(statFlag)

            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(statConn, CommandType.StoredProcedure, fstrSpName, Param)
            Dim statXmlTr As New clsXml
            Dim strPaid As String
            Dim strPanm As String
            Dim infon As ArrayList = New ArrayList()
            While dr.Read()
                statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                strPaid = dr.GetValue(0).ToString()
                strPanm = statXmlTr.GetLabelName("StatusMessage", strPaid)
                infon.Add(strPaid).ToString()
                infon.Add(strPanm).ToString()
            End While
            dr.Close()
            statConn.Dispose()
            Return infon

        Catch ex As Exception
            'trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("wang", ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clslconfirminf.vb")

        Catch ex As SqlException
            'trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("wang", ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsrlconfirminf.vb")
        End Try

    End Function


   
#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)

        connection.Open()

        Return connection
    End Function
#End Region



#End Region

End Class
