﻿Imports Microsoft.VisualBasic
Imports SQLDataAccess
Imports BusinessEntity
Imports System.Configuration
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web

#Region " Module Amment Hisotry "
'=========================================================================================='
'Description     :  the Calculate Incentive for Technician Customer Relation Officer entity
'History         :  
'Modified Date   :  2006-06-13
'Version         :  v1.0
'Author          :  Fan
'=========================================================================================='

#End Region

Public Class clsCalIncenforTech

#Region "Declaration"

    Private strUserID As String                         'User Login ID
    Private strFrDate As String                         'From Date 
    Private strToDate As String                         'To Date 
    Private strStaffTy As String                        'Staff Type
    Private strFrStaffCode As String                    'From Staff Code
    Private strToStaffCode As String                    'To Staff Code
    Private strServiceTy As String                      'Service Type
    Private strFrSerCent As String                      'From Service Center
    Private strToSerCent As String                      'To Service Center
    Private strSerStats As String                       'Service Status 
    Private strSpName As String                         'Store procedure name

#End Region

#Region "Properties"

    Public Property UserID() As String
        Get
            Return strUserID
        End Get
        Set(ByVal value As String)
            strUserID = value
        End Set
    End Property
    Public Property FrDate() As String
        Get
            Return strFrDate
        End Get
        Set(ByVal value As String)
            strFrDate = value
        End Set
    End Property
    Public Property ToDate() As String
        Get
            Return strToDate
        End Get
        Set(ByVal value As String)
            strToDate = value
        End Set
    End Property
    Public Property StaffType() As String
        Get
            Return strStaffTy
        End Get
        Set(ByVal value As String)
            strStaffTy = value
        End Set
    End Property
    Public Property FrStaffCode() As String
        Get
            Return strFrStaffCode
        End Get
        Set(ByVal value As String)
            strFrStaffCode = value
        End Set
    End Property
    Public Property ToStaffCode() As String
        Get
            Return strToStaffCode
        End Get
        Set(ByVal value As String)
            strToStaffCode = value
        End Set
    End Property
    Public Property SerType() As String
        Get
            Return strServiceTy
        End Get
        Set(ByVal Value As String)
            strServiceTy = Value
        End Set
    End Property
    Public Property FrSerCent() As String
        Get
            Return strFrSerCent
        End Get
        Set(ByVal Value As String)
            strFrSerCent = Value
        End Set
    End Property
    Public Property ToSerCent() As String
        Get
            Return strToSerCent
        End Get
        Set(ByVal Value As String)
            strToSerCent = Value
        End Set
    End Property
    Public Property SerStatus() As String
        Get
            Return strSerStats
        End Get
        Set(ByVal Value As String)
            strSerStats = Value
        End Set
    End Property

#End Region

#Region "Methods"

#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)

        connection.Open()

        Return connection
    End Function
#End Region

#Region "Seach and DisPlay the Information"
    Public Function GetIncentiveInfo() As DataSet

        Dim selConnection As SqlConnection = Nothing
        Dim selConn As SqlConnection = Nothing
        selConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        strSpName = "BB_FNCCINC_View"
        Try
            trans = selConnection.BeginTransaction()

            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(9) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, strSpName)
            Param(0).Value = Trim(strUserID)
            Param(1).Value = Trim(strFrDate)
            Param(2).Value = Trim(strToDate)
            Param(3).Value = Trim(strFrStaffCode)
            Param(4).Value = Trim(strToStaffCode)
            Param(5).Value = Trim(strFrSerCent)
            Param(6).Value = Trim(strToSerCent)
            Param(7).Value = Trim(strStaffTy)
            Param(8).Value = Trim(strSerStats)

            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, strSpName, Param)

            Return ds

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("Fan", ErrorLog.Item(1).ToString, strSpName, WriteErrLog.SELE, _
                                    "clsCalIncenforTech.vb")
            Return Nothing
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("Fan", ErrorLog.Item(1).ToString, strSpName, WriteErrLog.SELE, _
                                    "clsCalIncenforTech.vb")
            Return Nothing
        Finally
            selConnection.Close()
            selConn.Close()
        End Try

    End Function
#End Region

#Region "Get Export Job Detail Information"
    Public Function GetExportInfo() As DataSet

        Dim selConnection As SqlConnection = Nothing
        Dim selConn As SqlConnection = Nothing
        selConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        strSpName = "BB_FNCCINC_Export"
        Try
            trans = selConnection.BeginTransaction()

            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(9) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, strSpName)
            Param(0).Value = Trim(strUserID)
            Param(1).Value = Trim(strFrDate)
            Param(2).Value = Trim(strToDate)
            Param(3).Value = Trim(strFrStaffCode)
            Param(4).Value = Trim(strToStaffCode)
            Param(5).Value = Trim(strFrSerCent)
            Param(6).Value = Trim(strToSerCent)
            Param(7).Value = Trim(strStaffTy)
            Param(8).Value = Trim(strSerStats)

            Dim ds As DataSet = New DataSet()

            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, strSpName, Param)

            Return ds

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(strUserID, ErrorLog.Item(1).ToString, strSpName, WriteErrLog.SELE, _
                                    "clsCalIncenforTech.vb")
            Return Nothing
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("Fan", ErrorLog.Item(1).ToString, strSpName, WriteErrLog.SELE, _
                                    "clsCalIncenforTech.vb")
            Return Nothing
        Finally
            selConnection.Close()
            selConn.Close()
        End Try

    End Function
#End Region

#End Region
End Class
