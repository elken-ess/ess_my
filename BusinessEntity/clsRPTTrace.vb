
Imports Microsoft.VisualBasic
Imports SQLDataAccess
Imports BusinessEntity
Imports System.Configuration
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web


Public Class clsRPTTrace

#Region "Declaration"

    Public strUserID As String                         'User Login ID
    Public strFrPartCode As String                     'From PartCode 
    Public strToPartCode As String                     'To PartCode 
    Public strMinModelID As String                     'Min ModelID
    Public strMaxModelID As String                     'Max ModelID
    Public strMinSerialNumber As String                'Min SerialNo
    Public strMaxSerialNumber As String                'Max SerialNo
    Public strMindate As String                        'From Date 
    Public strMaxdate As String                        'To Date 
    Public strContractTy As String                     'contract
    Public strSpName As String                         'Store procedure name
    Public strFrPriceType As String                     'From PriceType 
    Public strToPriceType As String                     'To PriceType 


#End Region

#Region "Properties"

    Public Property UserID() As String
        Get
            Return strUserID
        End Get
        Set(ByVal value As String)
            strUserID = value
        End Set
    End Property

    Public Property FrPartCode() As String
        Get
            Return strFrPartCode
        End Get
        Set(ByVal value As String)
            strFrPartCode = value
        End Set
    End Property


    Public Property ToPartCode() As String
        Get
            Return strToPartCode
        End Get
        Set(ByVal value As String)
            strToPartCode = value
        End Set
    End Property


    Public Property MinModelID() As String
        Get
            Return strMinModelID
        End Get
        Set(ByVal value As String)
            strMinModelID = value
        End Set
    End Property

    Public Property MaxModelID() As String
        Get
            Return strMaxModelID
        End Get
        Set(ByVal value As String)
            strMaxModelID = value
        End Set
    End Property


    Public Property MinSerialNumber() As String
        Get
            Return strMinSerialNumber
        End Get
        Set(ByVal value As String)
            strMinSerialNumber = value
        End Set
    End Property

    Public Property MaxSerialNumber() As String
        Get
            Return strMaxSerialNumber
        End Get
        Set(ByVal value As String)
            strMaxSerialNumber = value
        End Set
    End Property

    Public Property Mindate() As String
        Get
            Return strMindate
        End Get
        Set(ByVal value As String)
            strMindate = value
        End Set
    End Property

    Public Property Maxdate() As String
        Get
            Return strMaxdate
        End Get
        Set(ByVal value As String)
            strMaxdate = value
        End Set
    End Property


    Public Property ContractTy() As String
        Get
            Return strContractTy
        End Get
        Set(ByVal value As String)
            strContractTy = value
        End Set
    End Property

    Public Property FrPriceType() As String
        Get
            Return strFrPriceType
        End Get
        Set(ByVal value As String)
            strFrPriceType = value
        End Set
    End Property


    Public Property ToPriceType() As String
        Get
            Return strToPriceType
        End Get
        Set(ByVal value As String)
            strToPriceType = value
        End Set
    End Property
#End Region

#Region "Methods"

#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)

        connection.Open()

        Return connection
    End Function
#End Region

#Region "Seach and DisPlay the Information"
    Public Function GetTraceReport() As DataSet

        Dim selConnection As SqlConnection = Nothing
        Dim selConn As SqlConnection = Nothing
        selConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        strSpName = "BB_RPTTrace_View"
        Try
            trans = selConnection.BeginTransaction()

            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(12) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, strSpName)
            Param(0).Value = Trim(strUserID)
            Param(1).Value = Trim(strFrPartCode)
            Param(2).Value = Trim(strToPartCode)
            Param(3).Value = Trim(strMinModelID)
            Param(4).Value = Trim(strMaxModelID)
            Param(5).Value = Trim(strMinSerialNumber)
            Param(6).Value = Trim(strMaxSerialNumber)
            Param(7).Value = Trim(strMindate)
            Param(8).Value = Trim(strMaxdate)
            Param(9).Value = Trim(strContractTy)
            Param(10).Value = Trim(strFrPriceType)
            Param(11).Value = Trim(strToPriceType)

            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, strSpName, Param)

            Return ds

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("Fan", ErrorLog.Item(1).ToString, strSpName, WriteErrLog.SELE, _
                                    "clsCalIncenforTech.vb")
            Return Nothing
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("Fan", ErrorLog.Item(1).ToString, strSpName, WriteErrLog.SELE, _
                                    "clsCalIncenforTech.vb")
            Return Nothing
        Finally
            selConnection.Close()
            selConn.Close()
        End Try

    End Function
#End Region

#Region "Get Export Job Detail Information"
    'Public Function GetExportInfo() As DataSet

    '    Dim selConnection As SqlConnection = Nothing
    '    Dim selConn As SqlConnection = Nothing
    '    selConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
    '    selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
    '    Dim trans As SqlTransaction = Nothing

    '    strSpName = "BB_FNCCINC_Export"
    '    Try
    '        trans = selConnection.BeginTransaction()

    '        ' define search fields 
    '        Dim Param() As SqlParameter = New SqlParameter(9) {}
    '        Param = SqlHelperParameterCache.GetSpParameterSet(selConn, strSpName)
    '        Param(0).Value = Trim(strUserID)
    '        Param(1).Value = Trim(strFrDate)
    '        Param(2).Value = Trim(strToDate)
    '        Param(3).Value = Trim(strFrStaffCode)
    '        Param(4).Value = Trim(strToStaffCode)
    '        Param(5).Value = Trim(strFrSerCent)
    '        Param(6).Value = Trim(strToSerCent)
    '        Param(7).Value = Trim(strStaffTy)
    '        Param(8).Value = Trim(strSerStats)

    '        Dim ds As DataSet = New DataSet()

    '        ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, strSpName, Param)

    '        Return ds

    '    Catch ex As SqlException
    '        trans.Rollback()
    '        Dim ErrorLog As ArrayList = New ArrayList
    '        ErrorLog.Add("Error").ToString()
    '        ErrorLog.Add(ex.Number).ToString()
    '        Dim WriteErrLog As New clsLogFile()
    '        WriteErrLog.ErrorLog(strUserID, ErrorLog.Item(1).ToString, strSpName, WriteErrLog.SELE, _
    '                                "clsCalIncenforTech.vb")
    '        Return Nothing
    '    Catch ex As Exception
    '        trans.Rollback()
    '        Dim ErrorLog As ArrayList = New ArrayList
    '        ErrorLog.Add("Error").ToString()
    '        ErrorLog.Add(ex.Message).ToString()
    '        Dim WriteErrLog As New clsLogFile()
    '        WriteErrLog.ErrorLog("Fan", ErrorLog.Item(1).ToString, strSpName, WriteErrLog.SELE, _
    '                                "clsCalIncenforTech.vb")
    '        Return Nothing
    '    Finally
    '        selConnection.Close()
    '        selConn.Close()
    '    End Try

    'End Function
#End Region

#End Region

End Class
