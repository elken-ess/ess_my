Imports SQLDataAccess
Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Web
Imports System.Data

Public Class clsModelType
#Region "version info"
    'Description     :  the model type
    'History         :  
    'Modified Date   :  2006-04-15
    'Version         :  v1.0
    'Author          :  xxl
#End Region

    Private strConn As String

#Region "Declaration"

    Private fstrCountryID As String ' country id
    Private fstrModelTypeID As String  '
    Private fstModelTypeName As String ' country name
    Private fstrModelTypeAlternateName As String ' alternate country  name
    Private fstrModelTypeStatus As String ' country status
    Private fstrProductClass As String ' country status
    Private fstrEffectivedate As Date ' country currency
    Private fstrObsoleteDate As Date ' country currency
    Private fstrWarrantyperiod As Integer
    Private fstrcreatedby As String ' createdby
    Private fdtcreateddate As Date ' country currency
    Private fstrmodifyby As String ' modify by
    Private fdtmodifydate As Date  'modify date   


    Private fstrSpName As String ' store procedure name
    Private fstrLoginSvc As String
    Private fstrLoginIP As String
#End Region
#Region "property"
    Public Property LoginSvc() As String
        Get
            Return fstrLoginSvc
        End Get
        Set(ByVal Value As String)
            fstrLoginSvc = Value
        End Set
    End Property
    Public Property LoginIP() As String
        Get
            Return fstrLoginIP
        End Get
        Set(ByVal Value As String)
            fstrLoginIP = Value
        End Set
    End Property
    Public Property Warrantyperiod() As Integer
        Get
            Return fstrWarrantyperiod
        End Get
        Set(ByVal Value As Integer)
            fstrWarrantyperiod = Value
        End Set
    End Property
    Public Property ModelTypeStatus() As String
        Get
            Return fstrModelTypeStatus
        End Get
        Set(ByVal Value As String)
            fstrModelTypeStatus = Value
        End Set
    End Property

    Public Property ProductClass() As String
        Get
            Return fstrProductClass
        End Get
        Set(ByVal Value As String)
            fstrProductClass = Value
        End Set
    End Property


    Public Property Effectivedate() As Date
        Get
            Return fstrEffectivedate
        End Get
        Set(ByVal Value As Date)
            fstrEffectivedate = Value
        End Set
    End Property

    Public Property ObsoleteDate() As Date
        Get
            Return fstrObsoleteDate
        End Get
        Set(ByVal Value As Date)
            fstrObsoleteDate = Value
        End Set
    End Property

    Public Property CountryID() As String
        Get
            Return fstrCountryID
        End Get
        Set(ByVal Value As String)
            fstrCountryID = Value
        End Set
    End Property

    Public Property ModelTypeID() As String
        Get
            Return fstrModelTypeID
        End Get
        Set(ByVal Value As String)
            fstrModelTypeID = Value
        End Set
    End Property


    Public Property ModelTypeName() As String
        Get
            Return fstModelTypeName
        End Get
        Set(ByVal Value As String)
            fstModelTypeName = Value
        End Set
    End Property

    Public Property ModelTypeAlternateName() As String
        Get
            Return fstrModelTypeAlternateName
        End Get
        Set(ByVal Value As String)
            fstrModelTypeAlternateName = Value
        End Set
    End Property

    Public Property Createdby() As String
        Get
            Return fstrcreatedby
        End Get
        Set(ByVal Value As String)
            fstrcreatedby = Value
        End Set
    End Property

    Public Property CreatedDate() As Date
        Get
            Return fdtcreateddate
        End Get
        Set(ByVal Value As Date)
            fdtcreateddate = Value
        End Set
    End Property
    Public Property Modifyby() As String
        Get
            Return fstrmodifyby
        End Get
        Set(ByVal Value As String)
            fstrmodifyby = Value
        End Set
    End Property

    Public Property ModdifyDate() As Date
        Get
            Return fdtmodifydate
        End Get
        Set(ByVal Value As Date)
            fdtmodifydate = Value
        End Set
    End Property
#End Region
#Region "Methods"

#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)
        connection.Open()
        Return connection
    End Function
#End Region

#Region "Insert"

    Public Function Insert() As Integer

        Dim insConnection As SqlConnection = Nothing
        Dim insConn As SqlConnection = Nothing
        insConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        insConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASMOTY_Add"
        Try
            trans = insConnection.BeginTransaction()
            Dim Param() As SqlParameter = New SqlParameter(13) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(insConn, fstrSpName)
            Param(0).Value = Trim(fstrModelTypeID)
            Param(1).Value = Trim(fstModelTypeName)
            Param(2).Value = Trim(fstrModelTypeAlternateName)
            Param(3).Value = Trim(fstrCountryID)
            Param(4).Value = Trim(fstrProductClass)
            Param(5).Value = Trim(fstrEffectivedate)
            Param(6).Value = Trim(fstrObsoleteDate)
            Param(7).Value = Trim(fstrWarrantyperiod)
            Param(8).Value = Trim(fstrModelTypeStatus)
            Param(9).Value = Trim(fstrcreatedby)
            Param(10).Value = Trim(fdtcreateddate)
            Param(11).Value = Trim(fstrmodifyby)
            Param(12).Value = Trim(fdtmodifydate)
            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(insConn, CommandType.StoredProcedure, fstrSpName, Param)
            trans.Commit()
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrmodifyby, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsModelType.vb")
            Return -1
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrmodifyby, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsModelType.vb")
            Return -1
        End Try
    End Function

#End Region

#Region "Update"

    Public Function Update() As Integer

        Dim UpdConnection As SqlConnection = Nothing
        Dim UpdConn As SqlConnection = Nothing
        UpdConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        UpdConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASMOTY_Upd"
        Try
            trans = UpdConnection.BeginTransaction()
            Dim Param() As SqlParameter = New SqlParameter(14) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(UpdConn, fstrSpName)
            Param(0).Value = Trim(fstrModelTypeID)
            Param(1).Value = Trim(fstModelTypeName)
            Param(2).Value = Trim(fstrModelTypeAlternateName)
            Param(3).Value = Trim(fstrCountryID)
            Param(4).Value = Trim(fstrProductClass)
            Param(5).Value = Trim(fstrEffectivedate)
            Param(6).Value = Trim(fstrObsoleteDate)
            Param(7).Value = Trim(fstrWarrantyperiod)
            Param(8).Value = Trim(fstrModelTypeStatus)
            Param(9).Value = Trim(fdtcreateddate)
            Param(10).Value = Trim(fstrmodifyby)
            Param(11).Value = Trim(fdtmodifydate)
            Param(12).Value = Trim(fstrLoginIP)
            Param(13).Value = Trim(fstrLoginSvc) '
            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(UpdConn, CommandType.StoredProcedure, fstrSpName, Param)

            trans.Commit()
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrmodifyby, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "clsModelType.vb")
            Return -1
        End Try

    End Function

#End Region
#Region "View Model Type "

    Public Function GetAllModelType() As DataSet
        Dim viewConnection As SqlConnection = Nothing
        Dim viewConn As SqlConnection = Nothing
        viewConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        viewConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASMOTY_View"
        Try
            trans = viewConnection.BeginTransaction()
            Dim ds As DataSet = SqlHelper.ExecuteDataset(viewConn, CommandType.StoredProcedure, fstrSpName)
            ' switch status id to status name for display
            Dim count As Integer
            Dim statXmlTr As New clsXml
            Dim strPanm As String
            Dim prdctclname As String
            'Dim clscom As New clsCommonClass

            statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            For count = 0 To ds.Tables(0).Rows.Count - 1
                'Dim statusid As String = ds.Tables(0).Rows(count).Item(8).ToString 'item(8) is status
                Dim prdctclsid As String = ds.Tables(0).Rows(count).Item(4).ToString 'item(4) is product class 
                Dim statusid As String = ds.Tables(0).Rows(count).Item(8).ToString 'item(8) is status
                strPanm = statXmlTr.GetLabelName("StatusMessage", statusid)
                ds.Tables(0).Rows(count).Item(8) = strPanm
                prdctclname = statXmlTr.GetLabelName("StatusMessage", prdctclsid)

                ds.Tables(0).Rows(count).Item(4) = prdctclname
                'crt datetiame change
                Dim datetimestr As String = ds.Tables(0).Rows(count).Item(9).ToString()
                ds.Tables(0).Rows(count).Item(9) = datetimestr
            Next


            Return ds

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrmodifyby, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsModelType.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrmodifyby, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsModelType.vb")

        End Try
    End Function
#End Region

#End Region
#Region "Select model type "

    Public Function GetModelTypeSel() As DataSet

        Dim selConnection As SqlConnection = Nothing
        Dim selConn As SqlConnection = Nothing
        selConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASMOTY_View"
        Try
            trans = selConnection.BeginTransaction()
            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(5) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrModelTypeID)
            Param(1).Value = Trim(fstModelTypeName)
            Param(2).Value = Trim(fstrCountryID)
            Param(3).Value = Trim(fstrProductClass)
            Param(4).Value = Trim(fstrModelTypeStatus)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            ' switch status id to status name for display
            Dim count As Integer
            Dim statXmlTr As New clsXml
            Dim strPanm As String
            Dim prdctclname As String
            statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            For count = 0 To ds.Tables(0).Rows.Count - 1
                Dim prdctclsid As String = ds.Tables(0).Rows(count).Item(4).ToString 'item(8) is product class 
                prdctclname = statXmlTr.GetLabelName("StatusMessage", prdctclsid)
                ds.Tables(0).Rows(count).Item(4) = prdctclname
            Next
            For count = 0 To ds.Tables(0).Rows.Count - 1
                Dim statusid As String = ds.Tables(0).Rows(count).Item(8).ToString 'item(3) is status
                strPanm = statXmlTr.GetLabelName("StatusMessage", statusid)
                ds.Tables(0).Rows(count).Item(8) = strPanm
            Next
            Return ds
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrmodifyby, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.VIEWS, "clsModelType.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrmodifyby, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.VIEWS, "clsModelType.vb")

        End Try

    End Function

#End Region

#Region "Select By model type ID  and status and ctry id"

    Public Function GetMdtypeDetailsByID() As SqlDataReader
        Dim retnArray As ArrayList = New ArrayList()
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASMOTY_SelByID"
        Try
            trans = sidConnection.BeginTransaction()
            Dim Param() As SqlParameter = New SqlParameter(3) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstrModelTypeID)
            Param(1).Value = Trim(fstrCountryID)
            Param(2).Value = Trim(fstrModelTypeStatus)
            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            Return dr
          
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrmodifyby, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsModelType.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrmodifyby, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsModelType.vb")

        End Try
    End Function
#End Region
#Region "Select Jobs Per Day By model type name and  status and ctry id"

    Public Function GetMdtypeDetailsByName() As SqlDataReader
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASMOTY_SelByName"
        Try
            trans = sidConnection.BeginTransaction()
            Dim Param() As SqlParameter = New SqlParameter(4) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstModelTypeName)
            Param(1).Value = Trim(fstrCountryID)
            Param(2).Value = Trim(fstrModelTypeStatus)
            Param(3).Value = Trim(fstrModelTypeID)
            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            Return dr
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrmodifyby, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsModelType.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrmodifyby, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsModelType.vb")

        End Try
    End Function
#End Region

#Region "Select Jobs Per Day By model type ID and  status and ctry id"

    Public Function GetMdtypeDetailsByIDSTAT() As SqlDataReader
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASMOTY_SelByIDSTAT"
        Try
            trans = sidConnection.BeginTransaction()
            Dim Param() As SqlParameter = New SqlParameter(3) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstrModelTypeID)
            Param(1).Value = Trim(fstrCountryID)
            Param(2).Value = Trim(fstrModelTypeStatus)
            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            Return dr
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrmodifyby, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsModelType.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrmodifyby, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsModelType.vb")

        End Try
    End Function
#End Region

#Region "modify  By model type name and  dup"

    Public Function GetMDTypeDupNM() As SqlDataReader
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASMOTY_SelMDNMDUP"
        Try
            trans = sidConnection.BeginTransaction()
            Dim Param() As SqlParameter = New SqlParameter(4) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstModelTypeName)
            Param(1).Value = Trim(fstrCountryID)
            Param(2).Value = Trim(fstrModelTypeStatus)
            Param(3).Value = Trim(fstrModelTypeID)
            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            Return dr
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrmodifyby, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsModelType.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrmodifyby, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsModelType.vb")

        End Try
    End Function
#End Region

#Region "DEL CHECK"
    Public Function GetMdtypedel() As DataSet
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASMOD_DELID"
        Try
            trans = sidConnection.BeginTransaction()
            Dim Param() As SqlParameter = New SqlParameter(1) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstrModelTypeID)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            Return ds
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrmodifyby, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsModelType.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrmodifyby, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsModelType.vb")

        End Try
    End Function
#End Region

End Class
