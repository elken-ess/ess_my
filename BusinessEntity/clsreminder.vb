﻿Imports System.Data.SqlClient
Imports SQLDataAccess
Imports System.Configuration
Imports BusinessEntity
Imports System.Data
Public Class clsreminder
#Region "Declaration"
    Private fstrSpName As String ' store procedure name
    Private fstrMinModeType As String
    Private fstrMaxModeType As String
    Private fstrMinStatID As String
    Private fstrMaxStatID As String
    Private fstrMinAreaID As String
    Private fstrMaxAreaID As String
    Private fstrMinSVCID As String
    Private fstrMaxSVCID As String
    Private fstrMinDueDate As String
    Private fstrMaxDueDate As String
    Private fstrCustType As String 'LW Added
    Private fstrRace As String 'LW Added
    Private fstrPrefLang As String 'LLY added 201702

    Dim selConnection As SqlConnection = Nothing
    Dim selConn As SqlConnection = Nothing
    Dim trans As SqlTransaction = Nothing
#End Region
#Region "Properties"
    Public WriteOnly Property FromModeType() As String
        'Get
        '    Return fstrMinModeType
        'End Get
        Set(ByVal Value As String)
            fstrMinModeType = Value
        End Set
    End Property
    Public WriteOnly Property ToModeType() As String
        'Get
        '    Return fstrMaxModeType
        'End Get
        Set(ByVal Value As String)
            fstrMaxModeType = Value
        End Set
    End Property
    Public WriteOnly Property FromStatID() As String
        'Get
        '    Return fstrMinStatID
        'End Get
        Set(ByVal Value As String)
            fstrMinStatID = Value
        End Set
    End Property
    Public WriteOnly Property ToStatID() As String
        'Get
        '    Return fstrMaxStatID
        'End Get
        Set(ByVal Value As String)
            fstrMaxStatID = Value
        End Set
    End Property
    Public WriteOnly Property StartingAreaID() As String
        'Get
        '    Return fstrMinAreaID
        'End Get
        Set(ByVal Value As String)
            fstrMinAreaID = Value
        End Set
    End Property
    Public WriteOnly Property EndingAreaID() As String
        'Get
        '    Return fstrMaxAreaID
        'End Get
        Set(ByVal Value As String)
            fstrMaxAreaID = Value
        End Set
    End Property
    Public WriteOnly Property FromServerCenterID() As String
        'Get
        '    Return fstrMinSVCID
        'End Get
        Set(ByVal Value As String)
            fstrMinSVCID = Value
        End Set
    End Property
    Public WriteOnly Property ToServerCenterID() As String
        'Get
        '    Return fstrMaxSVCID
        'End Get
        Set(ByVal Value As String)
            fstrMaxSVCID = Value
        End Set
    End Property
    Public WriteOnly Property StartingDueDate() As String
        'Get
        '    Return fstrMinDueDate
        'End Get
        Set(ByVal Value As String)
            fstrMinDueDate = Value
        End Set
    End Property
    Public WriteOnly Property EndingDueDate() As String
        'Get
        '    Return fstrMaxDueDate
        'End Get
        Set(ByVal Value As String)
            fstrMaxDueDate = Value
        End Set
    End Property
    'LW Added
    Public WriteOnly Property CustomerType() As String
        Set(ByVal value As String)
            fstrCustType = value
        End Set
    End Property
    'LW Added
    Public WriteOnly Property CustomerRace() As String
        Set(ByVal value As String)
            fstrRace = value
        End Set
    End Property

    'LLY Added
    Public WriteOnly Property PrefLang() As String
        Set(ByVal value As String)
            fstrPrefLang = value
        End Set
    End Property

#End Region
#Region "Methods"
#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)
        connection.Open()
        Return connection
    End Function
#End Region
#Region "Methods help"
    Private Sub BeginMethod()
        selConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        trans = selConnection.BeginTransaction()
    End Sub
    Private Sub DealException(ByVal ex As Exception)
        trans.Rollback()
        Dim ErrorLog As ArrayList = New ArrayList
        ErrorLog.Add("Error").ToString()
        ErrorLog.Add(ex.Message).ToString()
        Dim WriteErrLog As New clsLogFile()
        WriteErrLog.ErrorLog("zqw", ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsAppointment.vb")
    End Sub
    Private Sub EndMethod()
        selConnection.Close()
        selConn.Close()
    End Sub
#End Region
    '根据不同的查询条件返回不同的Required Reminder Customer Info
#Region "GetReminder"
    Public Function GetReminder(ByVal strUserID As String, ByVal strInclude As String) As DataSet

        fstrSpName = "BB_FNCRMSG_View"
        Try
            BeginMethod()
            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(15) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(strUserID)
            Param(1).Value = Trim(fstrMinModeType)
            Param(2).Value = Trim(fstrMaxModeType)
            Param(3).Value = Trim(fstrMinStatID)
            Param(4).Value = Trim(fstrMaxStatID)
            Param(5).Value = Trim(fstrMinAreaID)
            Param(6).Value = Trim(fstrMaxAreaID)
            Param(7).Value = Trim(fstrMinSVCID)
            Param(8).Value = Trim(fstrMaxSVCID)
            Param(9).Value = Trim(fstrMinDueDate)
            Param(10).Value = Trim(fstrMaxDueDate)
            Param(11).Value = Trim(fstrCustType)    'LW Added
            Param(12).Value = Trim(fstrRace)    'LW Added
            Param(13).Value = Trim(strInclude)
            Param(14).Value = Trim(fstrPrefLang) 'LLY Added


            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            ds.Tables(0).TableName = "Reminder"
            ds.Tables(1).TableName = "Mobileno" '客户的移动电话
            Dim CoverEntity As New clsCommonClass
            Dim i As Integer
            For i = 0 To ds.Tables("Mobileno").Rows.Count - 1
                Dim str As String = ds.Tables("Mobileno").Rows(i).Item("MTEL_TELNO").ToString().Trim()
                ds.Tables("Mobileno").Rows(i).Item("MTEL_TELNO") = CoverEntity.passconverttel(str)
            Next

            Return ds
        Catch ex As Exception
            DealException(ex)
            Return Nothing
        Finally
            EndMethod()
        End Try
    End Function
#End Region
#End Region

End Class
