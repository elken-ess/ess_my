Imports SQLDataAccess
Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Web
Imports System.Data

#Region " Module Amment Hisotry "
'Description     :  the country entity
'History         :  
'Modified Date   :  2006-04-4
'Version         :  v1.0
'Author          :  Woods Wong


#End Region
Public Class clsarea
    Private strConn As String

#Region "Declaration"

    Private fstrCountryID As String ' country id
    Private fstrStateID As String ' state id
    Private fstrAreaID As String ' area name
    Private fstrAreaName As String ' area name
    Private fstrAlternateName As String ' alternate state  name
    Private fstrAreaStatus As String ' area status
    Private fstrServiceID As String ' service id
    Private fstrPoID As String
    Private fstrPoStatus As String
    Private fstrCreatBy As String
    Private fstrCreatDate As System.DateTime
    Private fstrMifyBy As String
    Private fstrMifyDate As System.DateTime
    Private fstrSpName As String ' store procedure name
    Private fstrserid As String
    Private fstrIPaddr As String

#End Region

#Region "Properties"

    Public Property CountryID() As String
        Get
            Return fstrCountryID
        End Get
        Set(ByVal Value As String)
            fstrCountryID = Value
        End Set
    End Property
    Public Property StateID() As String
        Get
            Return fstrStateID
        End Get
        Set(ByVal Value As String)
            fstrStateID = Value
        End Set
    End Property
    Public Property AreaID() As String
        Get
            Return fstrAreaID
        End Get
        Set(ByVal Value As String)
            fstrAreaID = Value
        End Set
    End Property
    Public Property AreaName() As String
        Get
            Return fstrAreaName
        End Get
        Set(ByVal Value As String)
            fstrAreaName = Value
        End Set
    End Property

    Public Property AreaAlternateName() As String
        Get
            Return fstrAlternateName
        End Get
        Set(ByVal Value As String)
            fstrAlternateName = Value
        End Set
    End Property

    Public Property AreaStatus() As String
        Get
            Return fstrAreaStatus
        End Get
        Set(ByVal Value As String)
            fstrAreaStatus = Value
        End Set
    End Property
    Public Property POStatus() As String
        Get
            Return fstrPoStatus
        End Get
        Set(ByVal Value As String)
            fstrPoStatus = Value
        End Set
    End Property

    Public Property ServiceID() As String
        Get
            Return fstrServiceID
        End Get
        Set(ByVal Value As String)
            fstrServiceID = Value
        End Set
    End Property

    Public Property POID() As String
        Get
            Return fstrPoID
        End Get
        Set(ByVal Value As String)
            fstrPoID = Value
        End Set
    End Property

    Public Property CreateBy() As String
        Get
            Return fstrCreatBy
        End Get
        Set(ByVal Value As String)
            fstrCreatBy = Value
        End Set
    End Property

    Public Property CreateDate() As String
        Get
            Return fstrCreatDate
        End Get
        Set(ByVal Value As String)
            fstrCreatDate = Value
        End Set
    End Property

    Public Property ModifyBy() As String
        Get
            Return fstrMifyBy
        End Get
        Set(ByVal Value As String)
            fstrMifyBy = Value
        End Set
    End Property
    Public Property ModifyDate() As String
        Get
            Return fstrMifyDate
        End Get
        Set(ByVal Value As String)
            fstrMifyDate = Value
        End Set
    End Property
    Public Property servid() As String
        Get
            Return fstrserid
        End Get
        Set(ByVal Value As String)
            fstrserid = Value
        End Set
    End Property
    Public Property IPaddr() As String
        Get
            Return fstrIPaddr
        End Get
        Set(ByVal Value As String)
            fstrIPaddr = Value
        End Set
    End Property

#End Region

#Region "Methods"
#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)

        connection.Open()

        Return connection
    End Function
#End Region

#Region "Insert"

    Public Function Insert() As Integer

        Dim insConnection As SqlConnection = Nothing
        Dim insConn As SqlConnection = Nothing
        insConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        insConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASAREA_Addnew"
        Try
            trans = insConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(9) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(insConn, fstrSpName)


            Param(0).Value = Trim(fstrAreaID)
            Param(1).Value = Trim(fstrAreaName)
            Param(2).Value = Trim(fstrAlternateName)
            Param(3).Value = Trim(fstrAreaStatus)
            Param(4).Value = Trim(fstrCountryID)
            Param(5).Value = Trim(fstrStateID)
            Param(6).Value = Trim(fstrServiceID)
            Param(7).Value = Trim(fstrCreatBy) '创建者
            'Param(9).Value = Trim(fstrCreatDate) '创建时间
            Param(8).Value = Trim(fstrMifyBy) '最后修改者
            'Param(11).Value = Trim(fstrMifyDate) '最后修改者
            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(insConn, CommandType.StoredProcedure, fstrSpName, Param)

            ' insert audit log
            'Dim WriteAuditLog As New clsLogFile()
            'Dim insData As String = Param(0).Value + "," + Param(1).Value + "," + Param(2).Value + "," + Param(3).Value + "," + Param(4).Value + "," + Param(5).Value + "," + Param(6).Value + "," + Param(7).Value + "," + Param(8).Value
            'WriteAuditLog.AddLog(fstrMifyBy, "table name:mare_fil", "Data:" + insData, WriteAuditLog.ADD)
            trans.Commit()
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsArea.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsArea.vb")
            Return -1
        End Try

    End Function

#End Region
#Region "Insert po"

    Public Function Insert_po() As Integer

        Dim insConnection As SqlConnection = Nothing
        Dim insConn As SqlConnection = Nothing
        insConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        insConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASARPO_Add"
        Try
            trans = insConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(7) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(insConn, fstrSpName)

            Param(0).Value = Trim(fstrCountryID)
            Param(1).Value = Trim(fstrStateID)
            Param(2).Value = Trim(fstrAreaID)
            Param(3).Value = Trim(fstrPoStatus)
            Param(4).Value = Trim(fstrPoID)
            Param(5).Value = Trim(fstrCreatBy) '创建者
            ' Param(4).Value = Trim(fstrCreatDate) '创建时间
            Param(6).Value = Trim(fstrMifyBy) '最后修改者
            '  Param(6).Value = Trim(fstrMifyDate) '最后修改者
            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(insConn, CommandType.StoredProcedure, fstrSpName, Param)

            ' insert audit log
            Dim WriteAuditLog As New clsLogFile()
            'Dim insData As String = Param(0).Value + "," + Param(1).Value + "," + Param(2).Value + "," + Param(3).Value + "," + Param(4).Value + "," + Param(5).Value + "," + Param(6).Value
            'WriteAuditLog.AddLog(fstrMifyBy, "table name:marp_fil", "Data:" + insData, WriteAuditLog.ADD)

            trans.Commit()
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsArea.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsArea.vb")
            Return -1
        End Try

    End Function

#End Region
#Region "bonds id-name "
    Public Function Getidname(ByVal fstrSpName) As DataSet
        Dim viewConnection As SqlConnection = Nothing
        Dim viewConn As SqlConnection = Nothing
        viewConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        viewConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        'fstrSpName = "BB_MASJOBS_VIEW"

        Try
            trans = viewConnection.BeginTransaction()
            Dim ds As DataSet = SqlHelper.ExecuteDataset(viewConn, CommandType.StoredProcedure, fstrSpName)
            Return ds

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsState.vb")
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsState.vb")

        End Try

    End Function
#End Region
 
#Region "Select Area"

    Public Function GetArea() As DataSet

        Dim selConnection As SqlConnection = Nothing
        Dim selConn As SqlConnection = Nothing
        selConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASAREA_VIEW"
        Try
            trans = selConnection.BeginTransaction()

            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(4) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrAreaID)
            Param(1).Value = Trim(fstrAreaName)
            Param(2).Value = Trim(fstrAreaStatus)
            Param(3).Value = Trim(fstrCountryID)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            ' switch status id to status name for display
            Dim count As Integer
            Dim statXmlTr As New clsXml
            Dim strPanm As String
            statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            For count = 0 To ds.Tables(0).Rows.Count - 1
                Dim statusid As String = ds.Tables(0).Rows(count).Item(3).ToString 'item(3) is status
                strPanm = statXmlTr.GetLabelName("StatusMessage", statusid)
                ds.Tables(0).Rows(count).Item(3) = strPanm
            Next
            Return ds

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsArea.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsArea.vb")
        End Try

    End Function
#End Region
#Region "Select Area  By ID"

    Public Function GetAreaDetailByID() As SqlDataReader
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASAREA_SELBYID"
        Try
            trans = sidConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(4) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstrCountryID)
            Param(1).Value = Trim(fstrStateID)
            Param(2).Value = Trim(fstrAreaID)
            Param(3).Value = Trim(fstrAreaStatus)

            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            Return dr


        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, fstrSpName, ErrorLog.Item(1).ToString, WriteErrLog.SELE, "clsArea.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, fstrSpName, ErrorLog.Item(1).ToString, WriteErrLog.SELE, "clsArea.vb")
        End Try
    End Function
#End Region
#Region "Select Area By NAME"
    Public Function GetAreaDetailsByAreaNM() As Integer
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASAREA_SELBYNM"
        Try
            trans = sidConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(4) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstrCountryID)
            Param(1).Value = Trim(fstrStateID)
            Param(2).Value = Trim(fstrAreaID)
            Param(3).Value = Trim(fstrAreaName)

            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            If dr.Read() Then
                dr.Close()
                Return -1
            End If
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsArea.vb")
            Return -1
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsArea.vb")
            Return -1
        Finally
            sidConnection.Close()
            sidConn.Close()
        End Try
    End Function
#End Region
#Region "Select PO"

    Public Function GetPO() As DataSet

        Dim selConnection As SqlConnection = Nothing
        Dim selConn As SqlConnection = Nothing
        selConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASARPO_VIEW"
        Try
            trans = selConnection.BeginTransaction()

            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(3) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrCountryID)
            Param(1).Value = Trim(fstrStateID)
            Param(2).Value = Trim(fstrAreaID)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            ' switch status id to status name for display
            Dim count As Integer
            Dim statXmlTr As New clsXml
            Dim strPanm As String
            statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            For count = 0 To ds.Tables(0).Rows.Count - 1
                Dim statusid As String = ds.Tables(0).Rows(count).Item(2).ToString 'item(3) is status
                strPanm = statXmlTr.GetLabelName("StatusMessage", statusid)
                ds.Tables(0).Rows(count).Item(2) = strPanm
            Next
            Return ds

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsArea.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsArea.vb")
        End Try

    End Function
#End Region
#Region "Select PO by ID"

    Public Function GetPODetail_byAreaID() As SqlDataReader

        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASARPO_SELBYID"
        Try
            trans = sidConnection.BeginTransaction()

            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(5) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstrCountryID)
            Param(1).Value = Trim(fstrStateID)
            Param(2).Value = Trim(fstrAreaID)
            Param(3).Value = Trim(fstrPoID)
            Param(4).Value = Trim(fstrPoStatus)

            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            Return dr


        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, fstrSpName, ErrorLog.Item(1).ToString, WriteErrLog.SELE, "clsArea.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, fstrSpName, ErrorLog.Item(1).ToString, WriteErrLog.SELE, "clsArea.vb")
        End Try

    End Function
#End Region
#Region "Update"

    Public Function Update() As Integer

        Dim UpdConnection As SqlConnection = Nothing
        Dim UpdConn As SqlConnection = Nothing
        UpdConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        UpdConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASAREA_UpdNEW"
        Try
            trans = UpdConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(10) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(UpdConn, fstrSpName)
            Param(0).Value = Trim(fstrAreaID)
            Param(1).Value = Trim(fstrAreaName)
            Param(2).Value = Trim(fstrAlternateName)
            Param(3).Value = Trim(fstrCountryID)
            Param(4).Value = Trim(fstrStateID)
            Param(5).Value = Trim(fstrAreaStatus)
            Param(6).Value = Trim(fstrServiceID)
            Param(7).Value = Trim(fstrMifyBy) '最后修改者
            Param(8).Value = Trim(fstrserid)
            Param(9).Value = Trim(fstrIPaddr)
            'Param(7).Value = Trim(fstrMifyDate) '最后修改时间
            'Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(UpdConn, CommandType.StoredProcedure, fstrSpName, Param)
            SqlHelper.ExecuteDataset(UpdConn, CommandType.StoredProcedure, fstrSpName, Param)
            ' insert audit log
            'Dim WriteAuditLog As New clsLogFile()
            'Dim insData As String = Param(0).Value + "," + Param(1).Value + "," + Param(2).Value + "," + Param(3).Value + "," + Param(4).Value + "," + Param(5).Value + "," + Param(6).Value + "," + Param(7).Value + "," + Param(8).Value + "," + Param(9).Value + "," + Param(10).Value
            'WriteAuditLog.AddLog("wang", "table name:mctr_fil", "Data:" + insData, WriteAuditLog.ADD)

            trans.Commit()
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "clsArea.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "clsArea.vb")
            Return -1

        Finally
            UpdConnection.Close()
            UpdConn.Close()
        End Try

    End Function

#End Region
#Region "Update po"

    Public Function Update_po() As Integer

        Dim UpdConnection As SqlConnection = Nothing
        Dim UpdConn As SqlConnection = Nothing
        UpdConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        UpdConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASARPO_Upd"
        Try
            trans = UpdConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(8) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(UpdConn, fstrSpName)
            Param(0).Value = Trim(fstrCountryID)
            Param(1).Value = Trim(fstrStateID)
            Param(2).Value = Trim(fstrAreaID)
            Param(3).Value = Trim(fstrPoID)
            Param(4).Value = Trim(fstrPoStatus)
            Param(5).Value = Trim(fstrMifyBy) '最后修改者
            Param(6).Value = Trim(fstrserid)
            Param(7).Value = Trim(fstrIPaddr)

            'Param(7).Value = Trim(fstrMifyDate) '最后修改时间
            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(UpdConn, CommandType.StoredProcedure, fstrSpName, Param)
            ' insert audit log
            'Dim WriteAuditLog As New clsLogFile()
            'Dim insData As String = Param(0).Value + "," + Param(1).Value + "," + Param(2).Value + "," + Param(3).Value + "," + Param(4).Value + "," + Param(5).Value + "," + Param(6).Value + "," + Param(7).Value + "," + Param(8).Value + "," + Param(9).Value + "," + Param(10).Value
            'WriteAuditLog.AddLog("wang", "table name:mctr_fil", "Data:" + insData, WriteAuditLog.ADD)

            trans.Commit()
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "clsArea.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "clsArea.vb")
            Return -1

        Finally
            UpdConnection.Close()
            UpdConn.Close()
        End Try

    End Function

#End Region

#Region "confirm no duplicated id and name"
    Public Function GetDuplicatedArea() As Integer
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASAREA_IFDUP"
        Try
            trans = sidConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(4) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstrCountryID)
            Param(1).Value = Trim(fstrStateID)
            Param(2).Value = Trim(fstrAreaID)
            Param(3).Value = Trim(fstrAreaName)

            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            If dr.Read() Then
                dr.Close()
                Return -1
            End If
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("wang", ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsArea.vb")
            Return -1
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("wang", ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsArea.vb")
            Return -1
        Finally
            sidConnection.Close()
            sidConn.Close()
        End Try
    End Function
#End Region

#Region "confirm no duplicated postcode"
    Public Function GetDuplicatedPO() As Integer
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASARPO_IFDUP"
        Try
            trans = sidConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(4) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)

            Param(0).Value = Trim(fstrCountryID)
            Param(1).Value = Trim(fstrStateID)
            Param(2).Value = Trim(fstrAreaID)
            Param(3).Value = Trim(fstrPoID)
         

            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            If dr.Read() Then
                dr.Close()
                Return -1
            End If
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("wang", ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsArea.vb")
            Return -1
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("wang", ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsArea.vb")
            Return -1
        Finally
            sidConnection.Close()
            sidConn.Close()
        End Try
    End Function
#End Region
#Region "Select COM By ID"

    Public Function GetcompByAreaID(ByVal fstrSpName) As Integer
        Dim retnArray As ArrayList = New ArrayList()
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        'fstrSpName = "BB_MASAREA_DELID"
        Try
            trans = sidConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(3) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstrCountryID)
            Param(1).Value = Trim(fstrStateID)
            Param(2).Value = Trim(fstrAreaID)


            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            If dr.Read() Then
                dr.Close()
                Return -1
            End If
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            'Dim WriteErrLog As New clsLogFile()
            'WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsCountry.vb")
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            'Dim WriteErrLog As New clsLogFile()
            'WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsCountry.vb")
        Finally
            sidConnection.Close()
            sidConn.Close()
        End Try
    End Function
#End Region
#End Region


End Class
