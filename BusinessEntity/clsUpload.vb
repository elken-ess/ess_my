﻿Imports System.Data.SqlClient
Imports SQLDataAccess
Imports System.Configuration
Imports BusinessEntity
Imports System.data
Imports System.Collections.Generic

Public Class clsUpload
    Private strServiceBill As String
    Private decAmount As Decimal
    Private dtCollectionDate As Date
    Private strType As String
    Private strFileName As String
    Private strUserId As String
    Public ProductsList As New List(Of clsUpload)

#Region "Properties"
    Public Property ServiceBillNo() As String
        Get
            Return strServiceBill
        End Get
        Set(ByVal value As String)
            strServiceBill = value
        End Set
    End Property

    Public Property Amount() As Decimal
        Get
            Return decAmount
        End Get
        Set(ByVal value As Decimal)
            decAmount = value
        End Set
    End Property

    Public Property CollectionDate() As Date
        Get
            Return dtCollectionDate
        End Get
        Set(ByVal value As Date)
            dtCollectionDate = value
        End Set
    End Property

    Public Property PaymentType() As String
        Get
            Return strType
        End Get
        Set(ByVal value As String)
            strType = value
        End Set
    End Property

    Public Property FileName() As String
        Get
            Return strFileName
        End Get
        Set(ByVal value As String)
            strFileName = value
        End Set
    End Property

    Public Property UserID() As String
        Get
            Return strUserId
        End Get
        Set(ByVal value As String)
            strUserId = value
        End Set
    End Property

    Public Sub New()

    End Sub

    Public Sub New(ByVal strServiceBillNo As String, ByVal decAmount As Decimal, ByVal dtCollectionDate As Date, ByVal strPaymentType As String, ByVal strFileName As String, ByVal strUserID As String)
        Me.ServiceBillNo = strServiceBillNo
        Me.Amount = decAmount
        Me.CollectionDate = dtCollectionDate
        Me.PaymentType = strPaymentType
        Me.FileName = strFileName
        Me.UserID = strUserID
    End Sub
#End Region

#Region "Checking"
    Public Function GetExistFIV1_CR(ByVal ServiceBillNo As String, ByVal ModBy As String) As DataSet
        Dim checkConnection As SqlConnection = Nothing
        Dim checkConn As SqlConnection = Nothing
        checkConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        checkConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        Dim trans As SqlTransaction = Nothing
        Dim fstrSpName As String = "BB_FIV1_CR_SelByFIV1_CR_SVBIL"

        Try
            trans = checkConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(1) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(checkConn, fstrSpName)
            Param(0).Value = ServiceBillNo

            Dim ds As DataSet = SqlHelper.ExecuteDataset(checkConn, CommandType.StoredProcedure, fstrSpName, Param)
            GetExistFIV1_CR = ds

            checkConnection.Dispose()
            checkConn.Dispose()
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(ModBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsUpload.vb")
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(ModBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsUpload.vb")
        End Try
    End Function

    Public Function GetExistFIV1_CR_UPLOAD(ByVal ServiceBillNo As String, ByVal ModBy As String) As DataSet
        Dim checkConnection As SqlConnection = Nothing
        Dim checkConn As SqlConnection = Nothing
        checkConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        checkConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        Dim trans As SqlTransaction = Nothing
        Dim fstrSpName As String = "BB_FIV1_CR_UPLOAD_SelByFIV1_CR_SVBIL"

        Try
            trans = checkConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(1) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(checkConn, fstrSpName)
            Param(0).Value = ServiceBillNo

            Dim ds As DataSet = SqlHelper.ExecuteDataset(checkConn, CommandType.StoredProcedure, fstrSpName, Param)
            GetExistFIV1_CR_UPLOAD = ds

            checkConnection.Dispose()
            checkConn.Dispose()
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(ModBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsUpload.vb")
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(ModBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsUpload.vb")
        End Try
    End Function
#End Region

#Region "Insert"
    Public Function Insert(ByVal ExcelList As clsUpload, ByVal ModBy As String) As Integer
        Dim insConnection As SqlConnection = Nothing
        Dim insConn As SqlConnection = Nothing
        insConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        insConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        Dim trans As SqlTransaction = Nothing
        Dim fstrSpName As String = "BB_FIV1_CR_UPLOAD_ADD"
        Dim iInsert As Integer = 0

        Try
            trans = insConnection.BeginTransaction()

            For i As Integer = 0 To ExcelList.ProductsList.Count - 1
                Dim Param() As SqlParameter = New SqlParameter(4) {}
                Param = SqlHelperParameterCache.GetSpParameterSet(insConn, fstrSpName)
                Param(0).Value = ExcelList.ProductsList.Item(i).ServiceBillNo
                Param(1).Value = ExcelList.ProductsList.Item(i).Amount
                Param(2).Value = ExcelList.ProductsList.Item(i).CollectionDate
                Param(3).Value = ExcelList.ProductsList.Item(i).PaymentType
                Param(4).Value = ExcelList.ProductsList.Item(i).FileName
                Param(5).Value = ExcelList.ProductsList.Item(i).UserID

                iInsert = SqlHelper.ExecuteNonQuery(insConn, CommandType.StoredProcedure, fstrSpName, Param)
            Next

            trans.Commit()
            insConnection.Dispose()
            insConn.Dispose()
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(ModBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsUpload.vb")
            Return -1
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(ModBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsUpload.vb")
            Return -1
        End Try
    End Function
#End Region

#Region "Methods"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)
        connection.Open()
        Return connection
    End Function
#End Region
End Class