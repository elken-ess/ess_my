Imports SQLDataAccess
Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Web
Imports System.Data


#Region " Module Amment Hisotry "
'Description     :  the country entity
'History         :  
'Modified Date   :  2006-04-4
'Version         :  v1.0
'Author          :  Woods Wong


#End Region

Public Class clsCountry
    Private strConn As String

#Region "Declaration"

    Private fstrCountryID As String ' country id
    Private fstrCountryName As String ' country name
    Private fstrAlternateName As String ' alternate country  name
    Private fstrCountryStatus As String ' country status
    Private fstrTelPref As String ' country status
    Private fstrCurrencyid As String ' country currency
    Private fstrCurrencyname As String ' country currency
    Private fstrCreatBy As String
    Private fstrCreatDate As System.DateTime
    Private fstrMifyBy As String
    Private fstrMifyDate As System.DateTime
    Private fstrSpName As String ' store procedure name
    Private fstrserid As String
    Private fstrIPaddr As String



#End Region

#Region "Properties"

    Public Property CountryID() As String
        Get
            Return fstrCountryID
        End Get
        Set(ByVal Value As String)
            fstrCountryID = Value
        End Set
    End Property

    Public Property CountryName() As String
        Get
            Return fstrCountryName
        End Get
        Set(ByVal Value As String)
            fstrCountryName = Value
        End Set
    End Property


    Public Property CountryAlternateName() As String
        Get
            Return fstrAlternateName
        End Get
        Set(ByVal Value As String)
            fstrAlternateName = Value
        End Set
    End Property

    Public Property CountryStatus() As String
        Get
            Return fstrCountryStatus
        End Get
        Set(ByVal Value As String)
            fstrCountryStatus = Value
        End Set
    End Property
    Public Property TelPref() As String
        Get
            Return fstrTelPref
        End Get
        Set(ByVal Value As String)
            fstrTelPref = Value
        End Set
    End Property

    Public Property CountryCurrencyid() As String
        Get
            Return fstrCurrencyid
        End Get
        Set(ByVal Value As String)
            fstrCurrencyid = Value
        End Set
    End Property
    Public Property CountryCurrencyname() As String
        Get
            Return fstrCurrencyname
        End Get
        Set(ByVal Value As String)
            fstrCurrencyname = Value
        End Set
    End Property

    Public Property CreateBy() As String
        Get
            Return fstrCreatBy
        End Get
        Set(ByVal Value As String)
            fstrCreatBy = Value
        End Set
    End Property

    Public Property CreateDate() As String
        Get
            Return fstrCreatDate
        End Get
        Set(ByVal Value As String)
            fstrCreatDate = Value
        End Set
    End Property

    Public Property ModifyBy() As String
        Get
            Return fstrMifyBy
        End Get
        Set(ByVal Value As String)
            fstrMifyBy = Value
        End Set
    End Property
    Public Property ModifyDate() As String
        Get
            Return fstrMifyDate
        End Get
        Set(ByVal Value As String)
            fstrMifyDate = Value
        End Set
    End Property

    Public Property servid() As String
        Get
            Return fstrserid
        End Get
        Set(ByVal Value As String)
            fstrserid = Value
        End Set
    End Property
    Public Property IPaddr() As String
        Get
            Return fstrIPaddr
        End Get
        Set(ByVal Value As String)
            fstrIPaddr = Value
        End Set
    End Property

#End Region

#Region "Methods"

#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)

        connection.Open()

        Return connection
    End Function
#End Region

#Region "Insert"

    Public Function Insert() As Integer

        Dim insConnection As SqlConnection = Nothing
        Dim insConn As SqlConnection = Nothing
        insConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        insConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASCTRY_Add"
        Try
            trans = insConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(9) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(insConn, fstrSpName)
            Param(0).Value = Trim(fstrCountryID)
            Param(1).Value = Trim(fstrCountryName)
            Param(2).Value = Trim(fstrAlternateName)
            Param(3).Value = Trim(fstrCountryStatus)
            Param(4).Value = Trim(fstrCurrencyid)
            Param(5).Value = Trim(fstrCurrencyname)
            Param(6).Value = Trim(fstrTelPref)
            Param(7).Value = Trim(fstrCreatBy) '创建者
            Param(8).Value = Trim(fstrMifyBy) '最后修改者
            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(insConn, CommandType.StoredProcedure, fstrSpName, Param)

            trans.Commit()
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsCountry.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsCountry.vb")
            Return -1
        Finally
            insConnection.Close()
            insConn.Close()
        End Try

    End Function

#End Region
#Region "Insert LPFX"

    Public Function InsertLFPX() As Integer

        Dim insConnection As SqlConnection = Nothing
        Dim insConn As SqlConnection = Nothing
        insConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        insConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASLFPX_ADD"
        Try
            trans = insConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(1) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(insConn, fstrSpName)
            Param(0).Value = Trim(fstrCountryID)
            
            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(insConn, CommandType.StoredProcedure, fstrSpName, Param)

            trans.Commit()
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsCountry.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsCountry.vb")
            Return -1
        Finally
            insConnection.Close()
            insConn.Close()
        End Try

    End Function

#End Region

#Region "Update"

    Public Function Update() As Integer

        Dim UpdConnection As SqlConnection = Nothing
        Dim UpdConn As SqlConnection = Nothing
        UpdConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        UpdConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASCTRY_UPD"
        Try
            trans = UpdConnection.BeginTransaction()
            Dim updParam() As SqlParameter = New SqlParameter(10) {}
            updParam = SqlHelperParameterCache.GetSpParameterSet(UpdConn, fstrSpName)
            updParam(0).Value = Trim(fstrCountryID)
            updParam(1).Value = Trim(fstrCountryName)
            updParam(2).Value = Trim(fstrAlternateName)
            updParam(3).Value = Trim(fstrCountryStatus)
            updParam(4).Value = Trim(fstrCurrencyid)
            updParam(5).Value = Trim(fstrCurrencyname)
            updParam(6).Value = Trim(fstrTelPref)
            updParam(7).Value = Trim(fstrMifyBy)
            updParam(8).Value = Trim(fstrserid)
            updParam(9).Value = Trim(fstrIPaddr)




            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(UpdConn, CommandType.StoredProcedure, fstrSpName, updParam)
            
            trans.Commit()
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "clsCountry.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "clsCountry.vb")
            Return -1
        Finally
            UpdConnection.Close()
            UpdConn.Close()
        End Try

    End Function

#End Region

#Region "Select Country"

    Public Function GetCountry() As DataSet

        Dim selConnection As SqlConnection = Nothing
        Dim selConn As SqlConnection = Nothing
        selConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASCTRY_VIEW"
        Try
            trans = selConnection.BeginTransaction()

            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(3) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrCountryID)
            Param(1).Value = Trim(fstrCountryName)
            Param(2).Value = Trim(fstrCountryStatus)

            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            ' switch status id to status name for display
            Dim count As Integer
            Dim statXmlTr As New clsXml
            Dim strPanm As String
            statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            For count = 0 To ds.Tables(0).Rows.Count - 1
                Dim statusid As String = ds.Tables(0).Rows(count).Item(3).ToString 'item(3) is status
                strPanm = statXmlTr.GetLabelName("StatusMessage", statusid)
                ds.Tables(0).Rows(count).Item(3) = strPanm
            Next
            Return ds

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsCountry.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsCountry.vb")
        Finally
            selConnection.Close()
            selConn.Close()
        End Try

    End Function
#End Region


#Region "Select Country By ID"

    Public Function GetCountryDetailsByCountryID() As ArrayList
        Dim retnArray As ArrayList = New ArrayList()
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASCTRY_SELBYID"
        Try
            trans = sidConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstrCountryID)
            Param(1).Value = Trim(fstrCountryStatus)

            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            'Return dr
            If dr.Read() Then
                Dim count As Integer
                For count = 0 To dr.FieldCount - 1
                    retnArray.Add(dr.GetValue(count).ToString())
                Next
            End If
            dr.Close()
            Return retnArray

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsCountry.vb")
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsCountry.vb")
        Finally
            sidConnection.Close()
            sidConn.Close()
        End Try
    End Function
#End Region

#Region "Select Country By NAME"
    Public Function GetCountryDetailsByCountryNM() As Integer
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASTRY_SELBYNM"
        Try
            trans = sidConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstrCountryID)
            Param(1).Value = Trim(fstrCountryName)

            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            If dr.Read() Then
                dr.Close()
                Return -1
            End If
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsCountry.vb")
            Return -1
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsCountry.vb")
            Return -1
        Finally
            sidConnection.Close()
            sidConn.Close()
        End Try
    End Function
#End Region

#Region "confirm no duplicated id and name"
    Public Function GetDuplicatedCountry() As Integer
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASCTRY_IFDUP"
        Try
            trans = sidConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstrCountryID)
            Param(1).Value = Trim(fstrCountryName)

            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            If dr.Read() Then
                dr.Close()
                Return -1
            End If
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsCountry.vb")
            Return -1
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsCountry.vb")
            Return -1
        Finally
            sidConnection.Close()
            sidConn.Close()
        End Try
    End Function
#End Region
#Region "confirm no duplicated LPFX"
    Public Function GetDuplicatedLPFX() As Integer
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASLFPX_IFDUP"
        Try
            trans = sidConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(1) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstrCountryID)

            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            If dr.Read() Then
                dr.Close()
                Return -1
            End If
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsCountry.vb")
            Return -1
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsCountry.vb")
            Return -1
        Finally
            sidConnection.Close()
            sidConn.Close()
        End Try
    End Function
#End Region

#Region "Select STA By ID"

    Public Function GetstateByCountryID(ByVal fstrSpName) As Integer
        Dim retnArray As ArrayList = New ArrayList()
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        'fstrSpName = "BB_MASCTRY_DELID"
        Try
            trans = sidConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstrCountryID)


            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            If dr.Read() Then
                dr.Close()
                Return -1
            End If
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            'Dim WriteErrLog As New clsLogFile()
            'WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsCountry.vb")
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            'Dim WriteErrLog As New clsLogFile()
            'WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsCountry.vb")
        Finally
            sidConnection.Close()
            sidConn.Close()
        End Try
    End Function
#End Region

#End Region

End Class
