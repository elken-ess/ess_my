Imports SQLDataAccess
Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Web
Imports System.Data


#Region " Module Amment Hisotry "
'Description     :  the country entity
'History         :  
'Modified Date   :  2006-04-11
'Version         :  v1.0
'Author          :  xxl


#End Region

Public Class clsJobsPerDay
    Private strConn As String

#Region "Declaration"
    Private fstrservicecenterid As String ' serverice center id 
    Private fdtdate1 As Date ' date
    Private fdtdate2 As Date ' date
    Private fintnotech As Integer ' alternate country  name
    Private finttotaljob As Integer  ' total of job
    Private fstrcreatedby As String ' createdby
    Private fdtcreateddate As Date ' country currency
    Private fstrmodifyby As String ' modify by
    Private fdtmodifydate As Date  'modify date    
    Private fstrSpName As String ' store procedure name
    Private fstrUserName As String
    Private fstrRank As Integer
    Private fstrCtryID As String
    Private fstrCmpID As String
    Private fstrLoginSvc As String
    Private fstrLoginIP As String
#End Region
#Region "Properties"
    Public Property LoginSvc() As String
        Get
            Return fstrLoginSvc
        End Get
        Set(ByVal Value As String)
            fstrLoginSvc = Value
        End Set
    End Property
    Public Property LoginIP() As String
        Get
            Return fstrLoginIP
        End Get
        Set(ByVal Value As String)
            fstrLoginIP = Value
        End Set
    End Property
    '''
    Public Property CmpId() As String
        Get
            Return fstrCmpID
        End Get
        Set(ByVal Value As String)
            fstrCmpID = Value
        End Set
    End Property
    Public Property CtryID() As String
        Get
            Return fstrCtryID
        End Get
        Set(ByVal Value As String)
            fstrCtryID = Value
        End Set
    End Property

    Public Property Rank() As Integer
        Get
            Return fstrRank
        End Get
        Set(ByVal Value As Integer)
            fstrRank = Value
        End Set
    End Property
    Public Property UserName() As String
        Get
            Return fstrUserName
        End Get
        Set(ByVal Value As String)
            fstrUserName = Value
        End Set
    End Property


    Public Property ServiceCenterID() As String
        Get
            Return fstrservicecenterid
        End Get
        Set(ByVal Value As String)
            fstrservicecenterid = Value
        End Set
    End Property

    Public Property dtdate1() As Date
        Get
            Return fdtdate1
        End Get
        Set(ByVal Value As Date)
            fdtdate1 = Value
        End Set
    End Property
    Public Property dtdate() As Date
        Get
            Return fdtdate2
        End Get
        Set(ByVal Value As Date)
            fdtdate2 = Value
        End Set

    End Property


    Public Property NoofTechnician() As Integer
        Get
            Return fintnotech
        End Get
        Set(ByVal Value As Integer)
            fintnotech = Value
        End Set
    End Property

    Public Property TotalofJob() As Integer
        Get
            Return finttotaljob
        End Get
        Set(ByVal Value As Integer)
            finttotaljob = Value
        End Set
    End Property
    Public Property Createdby() As String
        Get
            Return fstrcreatedby
        End Get
        Set(ByVal Value As String)
            fstrcreatedby = Value
        End Set
    End Property

    Public Property CreatedDate() As Date
        Get
            Return fdtcreateddate
        End Get
        Set(ByVal Value As Date)
            fdtcreateddate = Value
        End Set
    End Property
    Public Property Modifyby() As String
        Get
            Return fstrmodifyby
        End Get
        Set(ByVal Value As String)
            fstrmodifyby = Value
        End Set
    End Property

    Public Property ModdifyDate() As Date
        Get
            Return fdtmodifydate
        End Get
        Set(ByVal Value As Date)
            fdtmodifydate = Value
        End Set
    End Property
#End Region

#Region "Methods"

#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)

        connection.Open()

        Return connection
    End Function
#End Region

#Region "Insert"

    Public Function Insert() As Integer

        Dim insConnection As SqlConnection = Nothing
        Dim insConn As SqlConnection = Nothing
        insConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        insConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASJOBS_Add"
        Try
            trans = insConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(8) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(insConn, fstrSpName)
            Param(0).Value = Trim(fstrservicecenterid)
            Param(1).Value = Trim(fdtdate2)
            Param(2).Value = Trim(fintnotech)
            Param(3).Value = Trim(finttotaljob)

            Param(4).Value = Trim(fstrcreatedby)
            Param(5).Value = Trim(fdtcreateddate)
            Param(6).Value = Trim(fstrmodifyby)
            Param(7).Value = Trim(fdtmodifydate)

            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(insConn, CommandType.StoredProcedure, fstrSpName, Param)
            trans.Commit()
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrUserName, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsJobsPerDay.vb")
            Return -1
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrUserName, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsJobsPerDay.vb")
            Return -1
        End Try
    End Function

#End Region

#Region "Update"

    Public Function Update() As Integer

        Dim UpdConnection As SqlConnection = Nothing
        Dim UpdConn As SqlConnection = Nothing
        UpdConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        UpdConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASJOBS_Upd"
        Try
            trans = UpdConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(7) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(UpdConn, fstrSpName)
            Param(0).Value = Trim(fstrservicecenterid)
            Param(1).Value = fdtdate2
            Param(2).Value = Trim(fintnotech)
            Param(3).Value = Trim(finttotaljob)
            Param(4).Value = Trim(fstrmodifyby)
            Param(5).Value = Trim(fstrLoginIP)
            Param(6).Value = Trim(fstrLoginSvc)

            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(UpdConn, CommandType.StoredProcedure, fstrSpName, Param)
          
            trans.Commit()
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrmodifyby, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "clsJobsPerDay.vb")
            Return -1
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrmodifyby, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "clsJobsPerDay.vb")
            Return -1
        End Try
    End Function
#End Region

#Region "Select Jobs per day "

    Public Function GetJobs() As DataSet

        Dim selConnection As SqlConnection = Nothing
        Dim selConn As SqlConnection = Nothing
        selConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASJOBS_VIEW"
        Try
            trans = selConnection.BeginTransaction()

            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(7) {}

            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrservicecenterid)
            Param(1).Value = fdtdate1
            Param(2).Value = fdtdate2
            Param(3).Value = fstrCtryID
            Param(4).Value = fstrCmpID
            Param(5).Value = fstrRank
            Param(6).Value = LoginSvc
          
            Dim ds As DataSet = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            Return ds

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrUserName, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.VIEWS, "clsJobsPerDay.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrUserName, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.VIEWS, "clsJobsPerDay.vb")

        End Try
    End Function
#End Region


#Region "Select Jobs Per Day By ID and date"

    Public Function GetJobsDetailsBySvcID() As SqlDataReader
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASJOBS_SELBYIDN"
        Try
            trans = sidConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstrservicecenterid)
            Param(1).Value = fdtdate2

            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            Return dr

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrUserName, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.VIEWS, "clsJobsPerDay.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrUserName, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.VIEWS, "clsJobsPerDay.vb")

          
        End Try
    End Function
#End Region

#Region "Select no of date job"
    Public Function GetNoTdJobs() As Integer
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASMOD_SELNOJOB"
        Try
            trans = sidConnection.BeginTransaction()
            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fdtdate2)
            Param(1).Value = Trim(fstrservicecenterid)
            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            If dr.Read() Then
                Return Convert.ToInt32(dr.Item(0))
            End If
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrUserName, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsJobsPerDay.vb")
            Return 0
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrUserName, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsJobsPerDay.vb")
            Return 0
        Finally
            sidConnection.Close()
            sidConn.Close()
        End Try
    End Function
#End Region

#End Region
End Class
