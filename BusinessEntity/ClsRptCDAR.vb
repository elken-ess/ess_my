Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web
Imports BusinessEntity
Imports SQLDataAccess


Public Class ClsRptCDAR
#Region "Declaration"

    Private fstrSpName As String ' store procedure name
    Private fstrusername As String ' username

    Private fstrminsvcid As String
    Private fstrmaxsvcid As String

    Private fstrminusrid As String
    Private fstrmaxusrid As String
    Private fstrmindate As System.DateTime ' store procedure name
    Private fstrmaxdate As System.DateTime

    Private fstrminlogtime As System.DateTime
    Private fstrmaxlogtime As System.DateTime ' store procedure name
    Private fstrmindept As String

    Private fstrmaxdept As String

    Private fstrminscreenid As String

    Private fstrmaxscreenid As String
    Private fstrsrtel As String ' store procedure name
    Private fstrsortby As String

    

#End Region

#Region "Properties"
    Public Property username() As String
        Get
            Return fstrusername
        End Get
        Set(ByVal Value As String)
            fstrusername = Value
        End Set
    End Property

    Public Property Minsvcid() As String
        Get
            Return fstrminsvcid
        End Get
        Set(ByVal Value As String)
            fstrminsvcid = Value
        End Set
    End Property

    Public Property Maxsvcid() As String
        Get
            Return fstrmaxsvcid
        End Get
        Set(ByVal Value As String)
            fstrmaxsvcid = Value
        End Set
    End Property

    Public Property Minusrid() As String
        Get
            Return fstrminusrid
        End Get
        Set(ByVal Value As String)
            fstrminusrid = Value
        End Set
    End Property

    Public Property Maxusrid() As String
        Get
            Return fstrmaxusrid
        End Get
        Set(ByVal Value As String)
            fstrmaxusrid = Value
        End Set
    End Property

    Public Property Mindate() As String
        Get
            Return fstrmindate
        End Get
        Set(ByVal Value As String)
            fstrmindate = Value
        End Set
    End Property

    Public Property Maxdate() As String
        Get
            Return fstrmaxdate
        End Get
        Set(ByVal Value As String)
            fstrmaxdate = Value
        End Set
    End Property

    Public Property Minlogtime() As String
        Get
            Return fstrminlogtime
        End Get
        Set(ByVal Value As String)
            fstrminlogtime = Value
        End Set
    End Property

    Public Property Maxlogtime() As String
        Get
            Return fstrmaxlogtime
        End Get
        Set(ByVal Value As String)
            fstrmaxlogtime = Value
        End Set
    End Property
    Public Property Mindept() As String
        Get
            Return fstrmindept
        End Get
        Set(ByVal Value As String)
            fstrmindept = Value
        End Set
    End Property
    Public Property Maxdept() As String
        Get
            Return fstrmaxdept
        End Get
        Set(ByVal Value As String)
            fstrmaxdept = Value
        End Set
    End Property
    Public Property Minscreenid() As String
        Get
            Return fstrminscreenid
        End Get
        Set(ByVal Value As String)
            fstrminscreenid = Value
        End Set
    End Property

    Public Property Maxscreenid() As String
        Get
            Return fstrmaxscreenid
        End Get
        Set(ByVal Value As String)
            fstrmaxscreenid = Value
        End Set
    End Property
    Public Property Srtel() As String
        Get
            Return fstrsrtel
        End Get
        Set(ByVal Value As String)
            fstrsrtel = Value
        End Set
    End Property
    Public Property Sortby() As String
        Get
            Return fstrsortby
        End Get
        Set(ByVal Value As String)
            fstrsortby = Value
        End Set
    End Property

#End Region

#Region "Methods"
#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)

        connection.Open()

        Return connection
    End Function
#End Region
#Region "Select contact"

    Public Function GetRptsysa() As DataSet

        Dim selConnection As SqlConnection = Nothing
        Dim selConn As SqlConnection = Nothing
        selConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_RPTSYSA_VIEW"
        Try
            trans = selConnection.BeginTransaction()


            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(14) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrminsvcid)
            Param(1).Value = Trim(fstrmaxsvcid)
            Param(2).Value = Trim(fstrminusrid)
            Param(3).Value = Trim(fstrmaxusrid)
            Param(4).Value = Trim(fstrmindate)
            Param(5).Value = Trim(fstrmaxdate)
            Param(6).Value = Trim(fstrminlogtime)
            Param(7).Value = Trim(fstrmaxlogtime)
            Param(8).Value = Trim(fstrmindept)
            Param(9).Value = Trim(fstrmaxdept)
            Param(10).Value = Trim(fstrminscreenid)
            Param(11).Value = Trim(fstrmaxscreenid)
            Param(12).Value = Trim(fstrsrtel)
            Param(13).Value = Trim(fstrsortby)



            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)


            Return ds

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "ClsRptCDAR.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "ClsRptCDAR.vb")
        Finally
            selConnection.Close()
            selConn.Close()
        End Try

    End Function
#End Region
#End Region


End Class
