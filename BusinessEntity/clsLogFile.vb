Imports System
Imports System.IO
Imports System.Configuration
Imports System.Data.SqlClient
Imports SQLDataAccess.SqlHelper
Imports System.Data


Public Class clsLogFile
    Private strConn As String = ConfigurationSettings.AppSettings("ConnectionString")

#Region "Declaration"
    Private Const SAVE As String = "INSERT"
    Private Const DELE As String = "DELETE"
    Private Const UPD As String = "UPDATE"
    Private Const SEL As String = "SELECT"
    Private Const View As String = "VIEW"
    Private Const Er As String = "Error"
#End Region

    Public Sub New()
    End Sub 'New

#Region "Properties"
    Public ReadOnly Property ADD() As String
        Get
            Return SAVE
        End Get
    End Property
    Public ReadOnly Property EDIT() As String
        Get
            Return UPD
        End Get
    End Property
    Public ReadOnly Property DEL() As String
        Get
            Return DELE
        End Get
    End Property
    Public ReadOnly Property SELE() As String
        Get
            Return SEL
        End Get
    End Property
    Public ReadOnly Property VIEWS() As String
        Get
            Return VIEW
        End Get
    End Property
#End Region

#Region "Method AddLog"
    Public Sub AddLog(ByVal UserName As String, ByVal Prog As String, ByVal msgData As String, ByVal TransactionId As String)

        SyncLock Me
            Try
                ' If ChkIsAuditable(ProgramID) Then
                Dim _logDate As String = "Transactions Log on _" & DateTime.Now.ToString("dd-MM-yyyy")
                Dim fs As FileStream = Nothing
                Dim file_name As String = ""

                Dim AuditLogPath As String = ConfigurationSettings.AppSettings("AuditLogPath")
                file_name = AuditLogPath + _logDate + ".log"

                'file_name = "C:\Trans\" & _logDate & ".log"

                If Not File.Exists(file_name) Then
                    fs = New FileStream(file_name, FileMode.Create, FileAccess.Write)
                Else
                    fs = New FileStream(file_name, FileMode.Append, FileAccess.Write)
                End If


                Dim str_writer As New StreamWriter(fs)
                str_writer.WriteLine((UserName & "," & Prog & "," & msgData & "," & TransactionId & "," & Now))
                str_writer.Close()
                'End If
            Catch ex As Exception

            End Try
        End SyncLock

    End Sub 'AddLog


#End Region

#Region "Method ErrorLog"
      Public Sub ErrorLog(ByVal UserName As String, ByVal Mes As String, ByVal FuncName As String, ByVal TransactionId As String, ByVal UrlHref As String)

        SyncLock Me
            Try
                ' If ChkIsAuditable(ProgramID) Then
                Dim _logDate As String = "Errors Log on _" & DateTime.Now.ToString("dd-MM-yyyy")
                Dim fs As FileStream = Nothing
                Dim file_name As String = ""
                Dim ErrLogPath As String = ConfigurationSettings.AppSettings("ErrLogPath")
                file_name = ErrLogPath + _logDate + ".log"
                'file_name = "C:\Error\" & _logDate & ".log"

                If Not File.Exists(file_name) Then
                    fs = New FileStream(file_name, FileMode.Create, FileAccess.Write)
                Else
                    fs = New FileStream(file_name, FileMode.Append, FileAccess.Write)
                End If


                Dim str_writer As New StreamWriter(fs)
                str_writer.WriteLine((UserName & "," & Mes & "," & FuncName & "," & TransactionId & "," & UrlHref & "," & Now))
                str_writer.Close()
            Catch ex As Exception

            End Try
        End SyncLock
    End Sub 'ErrorLog
#End Region

End Class 'LogFile

