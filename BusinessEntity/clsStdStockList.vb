﻿Imports Microsoft.VisualBasic
Imports SQLDataAccess
Imports System.Data
Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient

Structure PartTabIndex
    Public partID As Integer
    Public partName As Integer
    Public ctrID As Integer
    Public sign As Integer
    Public quantity As Integer
    Public unitprice As Integer
    Public lineamt As Integer
    Public linetax As Integer
    Public status As Integer
End Structure

Public Class clsStdStockList

#Region "Declaration"
    Private UserID As String
    Private userCtr As String
    Private userCmp As String
    Private userSVC As String
    Private pklType As String
    Private pklNo As String
    Private _transDate As String
    Private _transSVC As String
    Private _techID As String
    Private _IPaddr As String
    Private _SessionID As String

    Private screends As DataSet = Nothing
    Private stocklistds As DataSet = Nothing
    Private parttable As DataTable = Nothing
    Private parttbindex As PartTabIndex
#End Region

#Region "Properities"
    Public Property UserCtrID() As String
        Get
            Return userCtr
        End Get
        Set(ByVal Value As String)
            userCtr = Value
        End Set
    End Property
    Public Property UserCmpID() As String
        Get
            Return userCmp
        End Get
        Set(ByVal Value As String)
            userCmp = Value
        End Set
    End Property
    Public Property UserSVCID() As String
        Get
            Return userSVC
        End Get
        Set(ByVal Value As String)
            userSVC = Value
        End Set
    End Property
    Public Property PackListType() As String
        Get
            Return pklType
        End Get
        Set(ByVal Value As String)
            pklType = Value
        End Set
    End Property
    Public Property PackListNo() As String
        Get
            Return pklNo
        End Get
        Set(ByVal Value As String)
            pklNo = Value
        End Set
    End Property
    Public Property IPaddr() As String
        Get
            Return _IPaddr
        End Get
        Set(ByVal Value As String)
            _IPaddr = Value
        End Set
    End Property
    Public Property SessionID() As String
        Get
            Return _SessionID
        End Get
        Set(ByVal Value As String)
            _SessionID = Value
        End Set
    End Property
    Public Property TransDate() As String
        Get
            Return _transDate
        End Get
        Set(ByVal Value As String)
            _transDate = Value
        End Set
    End Property
    Public Property TransSVC() As String
        Get
            Return _transSVC
        End Get
        Set(ByVal Value As String)
            _transSVC = Value
        End Set
    End Property
    Public Property TechID() As String
        Get
            Return _techID
        End Get
        Set(ByVal Value As String)
            _techID = Value
        End Set
    End Property
#End Region

#Region "Methods"

    Public Sub New(ByVal userid As String)
        Me.UserID = userid
        Dim reader As SqlDataReader = Nothing
        Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim fstrSpName As String = "BB_MASUSER_SelByID"

        Try
            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(conn, fstrSpName)
            Param(0).Value = userid
            Param(1).Value = "ACTIVE"
            reader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, fstrSpName, Param)
            If reader.Read() Then
                Me.UserCtrID = reader.Item("MUSR_CTRCD")
                Me.UserCmpID = reader.Item("MUSR_COMCD")
                Me.UserSVCID = reader.Item("MUSR_SVCID")
            End If
        Catch ex As SqlException
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(userid, ErrorLog.Item(1).ToString, fstrSpName, _
                               WriteErrLog.ADD, "clsStdStockList.vb")
        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(userid, ErrorLog.Item(1).ToString, fstrSpName, _
                               WriteErrLog.ADD, "clsStdStockList.vb")
        Finally
            reader.Close()
            conn.Close()
        End Try
    End Sub

    '获得服务类型
    Public Function getStockType() As ArrayList
        Dim stocktype As New clsrlconfirminf()
        Dim statParam As ArrayList = New ArrayList
        statParam = stocktype.searchconfirminf("STKTY")
       
        Return statParam
    End Function

    '根据三级控制获得服务中心和技术员
    Public Function getSVC_Tech() As Integer
        Dim result As Integer = 0
        Me.screends = New DataSet()
        Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim fstrSpName As String = "BB_FNCSTDS_View"

        Try
            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(conn, fstrSpName)
            Param(0).Value = UserID
            Param(1).Direction = ParameterDirection.Output
            screends = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, fstrSpName, Param)
            result = Param(1).Value
        Catch ex As SqlException
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(UserID, ErrorLog.Item(1).ToString, fstrSpName, _
                               WriteErrLog.ADD, "clsStdStockList.vb")
            result = -1
        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(UserID, ErrorLog.Item(1).ToString, fstrSpName, _
                               WriteErrLog.ADD, "clsStdStockList.vb")
            result = -1
        Finally
            conn.Close()
        End Try

        Return result
    End Function

    '获得服务中心
    Public Function getSVC() As DataView
        If screends IsNot Nothing Then
            If screends.Tables.Count > 0 Then
                Return screends.Tables(0).DefaultView
            End If
        End If

        Return Nothing
    End Function

    '获得技术员
    Public Function getTech(ByVal svcid As String) As DataTable
        Dim table As DataTable = Nothing
        Dim rows As DataRow() = Nothing
        If screends IsNot Nothing Then
            If screends.Tables.Count > 1 Then
                table = screends.Tables(1).Clone()
                rows = screends.Tables(1).Select("MTCH_SVCID='" + svcid + "'")
                Dim newrow As DataRow = Nothing
                For i As Integer = 0 To rows.Length - 1
                    newrow = table.NewRow()
                    newrow.Item(0) = rows(i).ItemArray(0)
                    newrow.Item(1) = rows(i).ItemArray(1)
                    newrow.Item(2) = rows(i).ItemArray(2)
                    table.Rows.Add(newrow)
                Next
                Return table
            End If
        End If

        Return Nothing
    End Function

    '增加新的标准库存
    Public Function addStdStockList(ByVal techid As String, _
                                    ByVal adddate As String, _
                                    ByVal svcid As String, _
                                    ByVal type As String, _
                                    ByRef transno As String _
                                    ) As Integer
        Dim result As Integer = 0
        Dim xTrnNo As String = ""
        Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim fstrSpName As String = "BB_FNCSTDS_Add"

        Try
            Dim Param() As SqlParameter = New SqlParameter() {}
            Param = SqlHelperParameterCache.GetSpParameterSet(conn, fstrSpName)
            Param(0).Value = type '"STD"
            Param(1).Value = adddate
            Param(2).Value = svcid
            Param(3).Value = techid
            Param(4).Value = Me.UserID
            Param(5).Value = Me.SessionID
            Param(6).Value = Me.IPaddr
            Param(7).Direction = ParameterDirection.Output
            Param(8).Direction = ParameterDirection.Output
            SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, fstrSpName, Param)
            xTrnNo = Param(7).Value
            result = Param(8).Value
        Catch ex As SqlException
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(UserID, ErrorLog.Item(1).ToString, fstrSpName, _
                               WriteErrLog.ADD, "clsStdStockList.vb")
            result = -1
        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(UserID, ErrorLog.Item(1).ToString, fstrSpName, _
                               WriteErrLog.ADD, "clsStdStockList.vb")
            result = -1
        Finally
            conn.Close()
        End Try

        transno = xTrnNo
        Return result
    End Function

    '获得某一个技术员的标准库存
    Public Function getStdStockListByTechID(ByVal techid As String, _
                                            ByVal svcid As String, _
                                            ByVal xType As String, _
                                            ByRef result As Integer) As DataSet
        stocklistds = New DataSet()
        Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim fstrSpName As String = "BB_FNCSDTS_SelByTechID"

        Try
            Dim Param() As SqlParameter = New SqlParameter(5) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(conn, fstrSpName)
            Param(0).Value = xType '"STD"
            Param(1).Value = svcid
            Param(2).Value = techid
            Param(3).Value = UserID
            Param(4).Direction = ParameterDirection.Output
            stocklistds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, fstrSpName, Param)
            result = Param(4).Value
        Catch ex As SqlException
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(UserID, ErrorLog.Item(1).ToString, fstrSpName, _
                               WriteErrLog.ADD, "clsStdStockList.vb")
            result = -1
        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(UserID, ErrorLog.Item(1).ToString, fstrSpName, _
                               WriteErrLog.ADD, "clsStdStockList.vb")
            result = -1
        Finally
            conn.Close()
        End Try

        Return stocklistds
    End Function

    '生成一个库存表
    Private Sub CreatePartTable()
        Me.parttable = New DataTable()
        Me.parttable.Columns.Add("PartID", System.Type.GetType("System.String"))
        Me.parttable.Columns.Add("PartName", System.Type.GetType("System.String"))
        Me.parttable.Columns.Add("CtrID", System.Type.GetType("System.String"))
        Me.parttable.Columns.Add("Sign", System.Type.GetType("System.String"))
        Me.parttable.Columns.Add("Quantity", System.Type.GetType("System.Int32"))
        Me.parttable.Columns.Add("UnitPrice", System.Type.GetType("System.Single"))
        Me.parttable.Columns.Add("LineAmt", System.Type.GetType("System.Single"))
        Me.parttable.Columns.Add("LineTax", System.Type.GetType("System.Single"))
        Me.parttable.Columns.Add("Stat", System.Type.GetType("System.Int32"))
        'Me.parttable.PrimaryKey = New DataColumn() { _
        '                    Me.parttable.Columns(0), _
        '                    Me.parttable.Columns(2)}
        Me.parttbindex = New PartTabIndex()
        Me.parttbindex.partID = 0
        Me.parttbindex.partName = 1
        Me.parttbindex.ctrID = 2
        Me.parttbindex.sign = 3
        Me.parttbindex.quantity = 4
        Me.parttbindex.unitprice = 5
        Me.parttbindex.lineamt = 6
        Me.parttbindex.linetax = 7
        Me.parttbindex.status = 8
    End Sub

    '添加零件入库存表
    Public Function addPart(ByVal partid As String, _
                            ByVal partname As String, _
                            ByVal ctrid As String, _
                            ByVal unitprice As Single, _
                            ByVal quantity As Integer, _
                            ByVal linetax As Single, _
                            ByVal lineamt As Single, _
                            ByVal TranType As String) As Integer
        If Me.parttable Is Nothing Then
            Me.CreatePartTable()
        End If
        Try
            Dim existrow As DataRow() = Me.parttable.Select("PartID='" + partid + _
                                                 "'" + " and CtrID='" + ctrid + "'" + _
                                                 " and Stat=1")
            If existrow.Length > 0 Then
                If existrow(0).ItemArray(Me.parttbindex.quantity) + quantity < 0 Then
                    existrow(0).Item(Me.parttbindex.sign) = "-"
                Else
                    existrow(0).Item(Me.parttbindex.sign) = "+"
                End If
                existrow(0).Item(Me.parttbindex.quantity) = _
                            Math.Abs(existrow(0).Item(Me.parttbindex.quantity) + quantity)
                existrow(0).Item(Me.parttbindex.lineamt) = _
                            Format((existrow(0).Item(Me.parttbindex.lineamt) + lineamt), "####0.00")
                existrow(0).Item(Me.parttbindex.linetax) = _
                            (existrow(0).Item(Me.parttbindex.linetax) + linetax)
            Else
                Dim newrow As DataRow = Me.parttable.NewRow()
                newrow.Item(Me.parttbindex.partID) = partid
                newrow.Item(Me.parttbindex.partName) = partname
                newrow.Item(Me.parttbindex.ctrID) = ctrid
                If TranType = "STK-ADJ" Or TranType = "ADJ" Then 'quantity < 0 Then
                    newrow.Item(Me.parttbindex.sign) = "-"
                Else
                    newrow.Item(Me.parttbindex.sign) = "+"
                End If
                newrow.Item(Me.parttbindex.quantity) = quantity
                newrow.Item(Me.parttbindex.unitprice) = Format(unitprice, "####0.00")
                newrow.Item(Me.parttbindex.lineamt) = Format(lineamt, "####0.00")
                newrow.Item(Me.parttbindex.linetax) = linetax
                newrow.Item(Me.parttbindex.status) = 1
                Me.parttable.Rows.Add(newrow)
            End If
            Return 1
        Catch ex As Exception
            Return -1
        End Try
    End Function

    '删除库存表中的某一个零件
    Public Function delPart(ByVal partid As String, ByVal ctrid As String) As Integer
        If Me.parttable Is Nothing Then
            Me.CreatePartTable()
        End If
        Try
            Dim existrow As DataRow() = Me.parttable.Select("PartID='" + partid + _
                                                 "'" + " and CtrID='" + ctrid + "'")
            If existrow.Length > 0 Then
                existrow(0).Item(Me.parttbindex.status) = -1
                existrow(0).Delete()
            End If
            Return 1
        Catch ex As Exception
            Return -1
        End Try
    End Function

    '返回更新的库存信息
    Public Function getPartTable() As DataTable
        Return Me.parttable
    End Function

    '根据库存表更新数据库
    Public Function updateStdStock(ByVal pklty As String, _
                                   ByVal pklno As String, _
                                   ByRef errorPartArray As ArrayList) As Integer
        Dim result As Integer = 0
        Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim fstrSpName As String = "BB_FNCSTDS_Upd"

        Try
            Dim Param() As SqlParameter = New SqlParameter() {}
            Param = SqlHelperParameterCache.GetSpParameterSet(conn, fstrSpName)
            If Me.parttable IsNot Nothing Then
                errorPartArray.Clear()
                For i As Integer = 0 To Me.parttable.Rows.Count - 1
                    If Me.parttable.Rows(i).ItemArray(Me.parttbindex.status) = 1 Then
                        Param(0).Value = pklty
                        Param(1).Value = pklno
                        Param(2).Value = Me.parttable.Rows(i).ItemArray(Me.parttbindex.partID)
                        Param(3).Value = Me.parttable.Rows(i).ItemArray(Me.parttbindex.ctrID)
                        Param(4).Value = Me.parttable.Rows(i).ItemArray(Me.parttbindex.unitprice)
                        Param(5).Value = Me.parttable.Rows(i).ItemArray(Me.parttbindex.quantity)
                        Param(6).Value = Me.parttable.Rows(i).ItemArray(Me.parttbindex.lineamt)
                        Param(7).Value = UserID
                        Param(8).Direction = ParameterDirection.Output
                        SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, fstrSpName, Param)
                        result = Param(8).Value
                        If result <> 1 Then
                            errorPartArray.Add(Me.parttable.Rows(i).ItemArray(Me.parttbindex.partID))
                        End If
                    End If
                Next
                If errorPartArray.Count > 0 Then
                    result = 0
                Else
                    result = 1
                End If
            End If
        Catch ex As SqlException
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(UserID, ErrorLog.Item(1).ToString, fstrSpName, _
                               WriteErrLog.ADD, "clsStdStockList.vb")
            result = -1
        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(UserID, ErrorLog.Item(1).ToString, fstrSpName, _
                               WriteErrLog.ADD, "clsStdStockList.vb")
            result = -1
        Finally
            conn.Close()
        End Try
        Return result
    End Function

    '预览即将更新成的标准库存（此时还未真正更新数据库）
    Public Function ViewCurrentStdList(ByVal techid As String, _
                                       ByVal svcid As String, _
                                       ByVal xType As String, _
                                       ByRef result As Integer) As DataTable()
        Try
            Dim curstklst As DataTable() = { _
                     New DataTable(), New DataTable() _
                    }

            'If Me.stocklistds Is Nothing Then
            '    Me.stocklistds = Me.getStdStockListByTechID(techid, svcid, xType, result)
            'End If
            curstklst(0) = Me.stocklistds.Tables(0).Copy()
            curstklst(1) = Me.stocklistds.Tables(1).Copy()

            If Me.parttable IsNot Nothing Then
                Dim rows As DataRow() = Nothing
                Dim addtax As Single = 0, addamt As Single = 0
                For i As Integer = 0 To Me.parttable.Rows.Count - 1
                    Dim qty As Integer = 0
                    If Me.parttable.Rows(i).ItemArray(Me.parttbindex.status) = 1 Then
                        Select Case Me.parttable.Rows(i).ItemArray(Me.parttbindex.sign)
                            Case "-"
                                qty = -Me.parttable.Rows(i).ItemArray(Me.parttbindex.quantity)
                            Case "+"
                                qty = Me.parttable.Rows(i).ItemArray(Me.parttbindex.quantity)
                        End Select

                        rows = curstklst(1).Select("FTS2_PARID='" + _
                                Me.parttable.Rows(i).ItemArray(Me.parttbindex.partID) + "'" + _
                                " and FTS2_CTRID='" + Me.parttable.Rows(i).ItemArray(Me.parttbindex.ctrID) + "'")
                        If rows.Length > 0 Then
                            rows(0).Item("FTS2_TRNQT") += qty
                            rows(0).Item("FTS2_TOTAM") += Me.parttable.Rows(i).ItemArray(Me.parttbindex.lineamt)
                        Else
                            Dim newrow As DataRow = curstklst(1).NewRow()
                            newrow.Item("FTS2_PARID") = _
                               Me.parttable.Rows(i).ItemArray(Me.parttbindex.partID)
                            newrow.Item("MPAR_ENAME") = _
                               Me.parttable.Rows(i).ItemArray(Me.parttbindex.partName)
                            newrow.Item("FTS2_CTRID") = _
                               Me.parttable.Rows(i).ItemArray(Me.parttbindex.ctrID)
                            newrow.Item("FTS2_TRNQT") = _
                               Me.parttable.Rows(i).ItemArray(Me.parttbindex.quantity)
                            newrow.Item("FTS2_TOTAM") = _
                               Me.parttable.Rows(i).ItemArray(Me.parttbindex.lineamt)
                            curstklst(1).Rows.Add(newrow)
                        End If
                    End If
                Next
            End If

            result = 1
            Return curstklst
        Catch ex As Exception
            result = -1
            Return Nothing
        End Try
    End Function

    '获得某一个零件的单价和对应的税率
    Public Function getPartInfo(ByVal partid As String, _
                                ByVal ctrid As String, _
                                ByVal svcid As String, _
                                ByVal dt As String) As ArrayList
        Dim partinfo As ArrayList = Nothing
        Dim reader As SqlDataReader = Nothing
        Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim fstrSpName As String = "BB_FNCSDTS_ParNmPrcTax"

        Try
            Dim Param() As SqlParameter = New SqlParameter(4) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(conn, fstrSpName)
            Param(0).Value = partid
            Param(1).Value = ctrid
            Param(2).Value = svcid
            Param(3).Value = dt
            reader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, fstrSpName, Param)
            If reader.Read() Then
                partinfo = New ArrayList()
                partinfo.Add(reader.Item("partname"))
                partinfo.Add(reader.Item("unitprice"))
                partinfo.Add(reader.Item("taxrate"))
            End If
        Catch ex As SqlException
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(UserID, ErrorLog.Item(1).ToString, fstrSpName, _
                               WriteErrLog.ADD, "clsStdStockList.vb")
            partinfo = Nothing
        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(UserID, ErrorLog.Item(1).ToString, fstrSpName, _
                               WriteErrLog.ADD, "clsStdStockList.vb")
            partinfo = Nothing
        Finally
            If reader IsNot Nothing Then
                reader.Close()
            End If
            conn.Close()
        End Try

        Return partinfo
    End Function

    '获得某一个技术员的标准库存的Standard Stock TransactionNO
    Public Function getStkListNo(ByVal svcid As String, _
                                 ByVal techid As String, ByVal TranType As String) As ArrayList
        Dim transTYNO As ArrayList = Nothing
        Dim reader As SqlDataReader = Nothing
        Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim fstrSpName As String = "BB_FNCSDTS_STKTYNO"

        Try
            Dim Param() As SqlParameter = New SqlParameter() {}
            Param = SqlHelperParameterCache.GetSpParameterSet(conn, fstrSpName)
            Param(0).Value = TranType '"STD"
            Param(1).Value = svcid
            Param(2).Value = techid
            reader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, fstrSpName, Param)
            If reader.Read() Then
                transTYNO = New ArrayList()
                transTYNO.Add(reader.Item("FTS1_TRNTY"))
                transTYNO.Add(reader.Item("FTS1_TRNNO"))
            End If
        Catch ex As SqlException
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(UserID, ErrorLog.Item(1).ToString, fstrSpName, _
                               WriteErrLog.ADD, "clsStdStockList.vb")
            transTYNO = Nothing
        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(UserID, ErrorLog.Item(1).ToString, fstrSpName, _
                               WriteErrLog.ADD, "clsStdStockList.vb")
            transTYNO = Nothing
        Finally
            If reader IsNot Nothing Then
                reader.Close()
            End If
            conn.Close()
        End Try

        Return transTYNO
    End Function

    '清空零件表
    Public Sub ClearPartTab()
        Try
            Me.parttable.Rows.Clear()
        Catch ex As Exception
            'error
        End Try
    End Sub

    '生成提货单
    Public Function GeneratePackList(ByVal techid As String, ByRef errlist As ArrayList) As Integer
        If Me.parttable Is Nothing Then
            Return 0
        ElseIf Me.parttable.Rows.Count < 1 Then
            Return 0
        End If

        Dim totaltax As Single = 0, totalamt As Single = 0
        For i As Integer = 0 To Me.parttable.Rows.Count - 1
            If Me.parttable.Rows(i).ItemArray(Me.parttbindex.status) = 1 Then
                totaltax += Me.parttable.Rows(i).ItemArray(Me.parttbindex.linetax)
                totalamt += Me.parttable.Rows(i).ItemArray(Me.parttbindex.lineamt)
            End If
        Next

        Dim result As Integer = 0
        Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim fstrSpName As String = "BB_FNCSDTS_GenPack"

        Try
            Dim Param() As SqlParameter = New SqlParameter() {}
            Param = SqlHelperParameterCache.GetSpParameterSet(conn, fstrSpName)
            Param(0).Value = "PKL"
            Param(1).Value = Me.TransDate
            Param(2).Value = Me.TechID
            Param(3).Value = Me.TransSVC
            Param(4).Value = totaltax
            Param(5).Value = totalamt
            Param(6).Value = Me.UserID
            Param(7).Value = Me.SessionID
            Param(8).Value = Me.IPaddr
            Param(9).Direction = ParameterDirection.Output
            Param(10).Direction = ParameterDirection.Output
            SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, fstrSpName, Param)
            result = Param(10).Value
            If result = 1 Then
                GeneratePackList2(Param(9).Value, errlist)
                If errlist.Count > 0 Then
                    result = 0
                End If
            End If
        Catch ex As SqlException
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(UserID, ErrorLog.Item(1).ToString, fstrSpName, _
                               WriteErrLog.ADD, "clsStdStockList.vb")
            result = -1
        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(UserID, ErrorLog.Item(1).ToString, fstrSpName, _
                               WriteErrLog.ADD, "clsStdStockList.vb")
            result = -1
        Finally
            conn.Close()
        End Try

        Return result
    End Function

    '生成提货单续
    Public Function GeneratePackList2(ByVal packlistno As String, ByRef errorPartArray As ArrayList) As Integer
        Dim result As Integer = 0
        Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim fstrSpName As String = "BB_FNCSDTS_GenPack2"

        Try
            Dim Param() As SqlParameter = New SqlParameter() {}
            Param = SqlHelperParameterCache.GetSpParameterSet(conn, fstrSpName)
            If Me.parttable IsNot Nothing Then
                errorPartArray.Clear()
                For i As Integer = 0 To Me.parttable.Rows.Count - 1
                    If Me.parttable.Rows(i).ItemArray(Me.parttbindex.status) = 1 Then
                        Param(0).Value = "PKL"
                        Param(1).Value = packlistno
                        Param(2).Value = Me.parttable.Rows(i).ItemArray(Me.parttbindex.partID)
                        Param(3).Value = Me.parttable.Rows(i).ItemArray(Me.parttbindex.ctrID)
                        Param(4).Value = Integer.Parse(Trim(Me.parttable.Rows(i).ItemArray(Me.parttbindex.sign)) + _
                                                       Trim(Me.parttable.Rows(i).ItemArray(Me.parttbindex.quantity)) _
                                                      )
                        result = SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, fstrSpName, Param)
                        If result <> 1 Then
                            errorPartArray.Add(Me.parttable.Rows(i).ItemArray(Me.parttbindex.partID))
                        End If
                    End If
                Next
                If errorPartArray.Count > 0 Then
                    result = 0
                Else
                    result = 1
                End If
            End If
        Catch ex As SqlException
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(UserID, ErrorLog.Item(1).ToString, fstrSpName, _
                               WriteErrLog.ADD, "clsStdStockList.vb")
            result = -1
        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(UserID, ErrorLog.Item(1).ToString, fstrSpName, _
                               WriteErrLog.ADD, "clsStdStockList.vb")
            result = -1
        Finally
            conn.Close()
        End Try
        Return result
    End Function

    '导入第一份标准库存
    Public Function Load1stStkList(ByVal trantype As String, _
                                   ByVal transno As String, _
                                   ByVal txtfilepath As String) As Integer


    End Function

    Public Function InsertIntoDB(ByVal params As ArrayList, ByVal TranNo As String) As Integer
        Dim result As Integer = 0
        Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim fstrSpName As String = "BB_FNCSDTS_LoadStdStk"

        Try
            Dim partinfo As ArrayList = Me.getPartInfo(params.Item(0), Me.UserCtrID, _
                                                       Me.TransSVC, Me.TransDate)
            Dim Param() As SqlParameter = New SqlParameter() {}
            Param = SqlHelperParameterCache.GetSpParameterSet(conn, fstrSpName)
            Param(0).Value = TranNo 'Me.PackListNo
            Param(1).Value = Me.UserCtrID
            Param(2).Value = params.Item(0)
            Param(3).Value = params.Item(1)
            Param(4).Value = CInt(params.Item(2)) 'Integer.Parse(Trim(params.Item(1)) + Trim(params.Item(2))) MODIFIED BY DEYB 06292006
            Param(5).Value = params.Item(3) 'Param(3).Value * Param(4).Value
            result = SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, fstrSpName, Param)
        Catch ex As SqlException
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(UserID, ErrorLog.Item(1).ToString, fstrSpName, _
                               WriteErrLog.ADD, "clsStdStockList.vb")
            result = -1
        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(UserID, ErrorLog.Item(1).ToString, fstrSpName, _
                               WriteErrLog.ADD, "clsStdStockList.vb")
            result = -1
        Finally
            conn.Close()
        End Try

        Return result
    End Function

    Public Function VerifyPart(ByVal PartID As String, ByVal CtrID As String) As Boolean
        Dim myCn As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim myCmd As SqlCommand = New SqlCommand("BB_FNCSTDS_VerifyPart", myCn)

        myCmd.CommandType = CommandType.StoredProcedure

        Dim prmUID As SqlParameter = New SqlParameter("@PartID", SqlDbType.VarChar, 20)
        prmUID.Value = PartID
        myCmd.Parameters.Add(prmUID)

        Dim prmCID As SqlParameter = New SqlParameter("@CtrID", SqlDbType.VarChar, 5)
        prmCID.Value = CtrID
        myCmd.Parameters.Add(prmCID)

        Dim prmRes As SqlParameter = New SqlParameter("@Result", SqlDbType.Bit)
        prmRes.Direction = ParameterDirection.Output
        myCmd.Parameters.Add(prmRes)

        myCn.Open()
        myCmd.ExecuteNonQuery()
        myCn.Dispose()

        Return CBool(myCmd.Parameters("@Result").Value)

    End Function

    Public Function GetPriceID(ByVal CtrID As String) As DataSet
        Dim myCn As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim myCmd As SqlDataAdapter = New SqlDataAdapter("BB_FNC_GETPRICEID", myCn)

        myCmd.SelectCommand.CommandType = CommandType.StoredProcedure

        Dim prmCID As SqlParameter = New SqlParameter("@CTRID", SqlDbType.VarChar, 5)
        prmCID.Value = CtrID
        myCmd.SelectCommand.Parameters.Add(prmCID)

        Dim myDataset As New DataSet
        myCmd.Fill(myDataset)
        Return myDataset

    End Function

    Public Function GeneratePackingList(ByVal TechID As String, ByVal SvcID As String, ByVal ComID As String, ByVal CtrID As String, ByVal UserID As String, ByRef MSG As String) As String
        Dim myCn As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim myCmd As SqlCommand = New SqlCommand("BB_FNCGENPACKLIST", myCn)

        myCmd.CommandType = CommandType.StoredProcedure

        Dim prmtID As SqlParameter = New SqlParameter("@TCHID", SqlDbType.VarChar, 20)
        prmtID.Value = TechID
        myCmd.Parameters.Add(prmtID)

        Dim prmSID As SqlParameter = New SqlParameter("@SVCID", SqlDbType.VarChar, 20)
        prmSID.Value = SvcID
        myCmd.Parameters.Add(prmSID)

        Dim prmCID As SqlParameter = New SqlParameter("@COMID", SqlDbType.VarChar, 20)
        prmCID.Value = ComID
        myCmd.Parameters.Add(prmCID)

        Dim prmCrID As SqlParameter = New SqlParameter("@CtrID", SqlDbType.VarChar, 20)
        prmCrID.Value = CtrID
        myCmd.Parameters.Add(prmCrID)

        Dim prmUID As SqlParameter = New SqlParameter("@USRID", SqlDbType.VarChar, 20)
        prmUID.Value = UserID
        myCmd.Parameters.Add(prmUID)

        Dim prmRes As SqlParameter = New SqlParameter("@MSG", SqlDbType.VarChar, 50)
        prmRes.Direction = ParameterDirection.Output
        myCmd.Parameters.Add(prmRes)

        Dim prmRes2 As SqlParameter = New SqlParameter("@TRNNO", SqlDbType.VarChar, 50)
        prmRes2.Direction = ParameterDirection.Output
        myCmd.Parameters.Add(prmRes2)

        myCn.Open()
        myCmd.ExecuteNonQuery()
        myCn.Dispose()

        MSG = CStr(myCmd.Parameters("@MSG").Value)
        Return CStr(myCmd.Parameters("@TRNNO").Value)

    End Function

    Public Function CheckAdjustmentQty(ByVal TechID As String, ByVal SvcID As String, ByVal PartID As String, ByVal CtrID As String, ByVal TranQty As Integer) As Boolean
        Dim myCn As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim myCmd As SqlCommand = New SqlCommand("BB_FNCSTDS_CHKADJ", myCn)

        myCmd.CommandType = CommandType.StoredProcedure

        Dim prmtID As SqlParameter = New SqlParameter("@TCHID", SqlDbType.VarChar, 20)
        prmtID.Value = TechID
        myCmd.Parameters.Add(prmtID)

        Dim prmSID As SqlParameter = New SqlParameter("@SVCID", SqlDbType.VarChar, 20)
        prmSID.Value = SvcID
        myCmd.Parameters.Add(prmSID)

        Dim prmPID As SqlParameter = New SqlParameter("@PARTID", SqlDbType.VarChar, 20)
        prmPID.Value = PartID
        myCmd.Parameters.Add(prmPID)

        Dim prmCrID As SqlParameter = New SqlParameter("@CtrID", SqlDbType.VarChar, 5)
        prmCrID.Value = CtrID
        myCmd.Parameters.Add(prmCrID)

        Dim prmQty As SqlParameter = New SqlParameter("@TRQTY", SqlDbType.Int)
        prmQty.Value = TranQty
        myCmd.Parameters.Add(prmQty)

        Dim prmRes As SqlParameter = New SqlParameter("@RESULT", SqlDbType.Bit)
        prmRes.Direction = ParameterDirection.Output
        myCmd.Parameters.Add(prmRes)

        myCn.Open()
        myCmd.ExecuteNonQuery()
        myCn.Dispose()

        Return CBool(myCmd.Parameters("@RESULT").Value)

    End Function

    Public Function VerifyPartInfo(ByVal PartID As String, ByVal CtrID As String, ByVal PriceID As String, ByVal TranDate As String) As String
        Dim myCn As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim myCmd As SqlCommand = New SqlCommand("BB_FNCSDTS_GETPARTINFO", myCn)

        myCmd.CommandType = CommandType.StoredProcedure

        Dim prmPID As SqlParameter = New SqlParameter("@PartID", SqlDbType.VarChar, 10)
        prmPID.Value = PartID
        myCmd.Parameters.Add(prmPID)

        Dim prmCID As SqlParameter = New SqlParameter("@CtrID", SqlDbType.VarChar, 20)
        prmCID.Value = CtrID
        myCmd.Parameters.Add(prmCID)

        Dim prmPrID As SqlParameter = New SqlParameter("@PriceID", SqlDbType.VarChar, 10)
        prmPrID.Value = PriceID
        myCmd.Parameters.Add(prmPrID)

        Dim prmDID As SqlParameter = New SqlParameter("@Date", SqlDbType.VarChar, 10)
        prmDID.Value = TranDate
        myCmd.Parameters.Add(prmDID)

        Dim prmxRes As SqlParameter = New SqlParameter("@Result", SqlDbType.VarChar, 100)
        prmxRes.Direction = ParameterDirection.Output
        myCmd.Parameters.Add(prmxRes)

        myCn.Open()
        myCmd.ExecuteNonQuery()
        myCn.Dispose()

        Return CStr(myCmd.Parameters("@Result").Value)

    End Function

#End Region
End Class
