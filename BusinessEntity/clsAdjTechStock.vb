Imports Microsoft.VisualBasic
Imports SQLDataAccess
Imports System.Data
Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient

Structure PartTabIndex2
    Public partID As Integer
    Public partName As Integer
    Public ctrID As Integer
    Public sign As Integer
    Public quantity As Integer
    Public unitprice As Integer
    Public lineamt As Integer
    Public linetax As Integer
    Public status As Integer
End Structure

Public Class clsAdjTechStock

#Region "Declaration"
    Private UserID As String
    Private fstrCountryID As String
    Private fstrCompanyID As String
    Private fstrServiceCenterID As String
    Private fstrTechnicianID As String
    Private fstrTransactionNo As String
    Private fstrTransactionType As String
    Private fstrPartID As String
    Private fstrPartQuantity As String

    Private fstrLine As String
    Private fstrTransactionDate As String

    Private fstrStatus As String
    Private fstrIOFlag As String
    Private fstrCreateByUserID As String


    Private _transDate As String
    Private _transSVC As String
    Private _techID As String
    Private _IPaddr As String
    Private _SessionID As String

    Private screends As DataSet = Nothing
    Private stocklistds As DataSet = Nothing
    Private parttable As DataTable = Nothing
    Private parttbindex As PartTabIndex2
#End Region

#Region "Properities"
    Public Property Status() As String
        Get
            Return fstrStatus
        End Get
        Set(ByVal Value As String)
            fstrStatus = Value
        End Set
    End Property

    Public Property IOFlag() As String
        Get
            Return fstrIOFlag
        End Get
        Set(ByVal Value As String)
            fstrIOFlag = Value
        End Set
    End Property

    Public Property CreateByUserID() As String
        Get
            Return fstrCreateByUserID
        End Get
        Set(ByVal Value As String)
            fstrCreateByUserID = Value
        End Set
    End Property


    Public Property LineNo() As String
        Get
            Return fstrLine
        End Get
        Set(ByVal Value As String)
            fstrLine = Value
        End Set
    End Property

    Public Property TransactionDate() As String
        Get
            Return fstrTransactionDate
        End Get
        Set(ByVal Value As String)
            fstrTransactionDate = Value
        End Set
    End Property


    Public Property CountryID() As String
        Get
            Return fstrCountryID
        End Get
        Set(ByVal Value As String)
            fstrCountryID = Value
        End Set
    End Property
    Public Property CompanyID() As String
        Get
            Return fstrCompanyID
        End Get
        Set(ByVal Value As String)
            fstrCompanyID = Value
        End Set
    End Property
    Public Property ServiceCenterID() As String
        Get
            Return fstrServiceCenterID
        End Get
        Set(ByVal Value As String)
            fstrServiceCenterID = Value
        End Set
    End Property

    Public Property TransactionType() As String
        Get
            Return fstrTransactionType
        End Get
        Set(ByVal Value As String)
            fstrTransactionType = Value
        End Set
    End Property
    Public Property TransactionNo() As String
        Get
            Return fstrTransactionNo
        End Get
        Set(ByVal Value As String)
            fstrTransactionNo = Value
        End Set
    End Property

    Public Property PartID() As String
        Get
            Return fstrPartID
        End Get
        Set(ByVal Value As String)
            fstrPartID = Value
        End Set
    End Property

    Public Property PartQuantity() As String
        Get
            Return fstrPartQuantity
        End Get
        Set(ByVal Value As String)
            fstrPartQuantity = Value
        End Set
    End Property

    Public Property IPaddr() As String
        Get
            Return _IPaddr
        End Get
        Set(ByVal Value As String)
            _IPaddr = Value
        End Set
    End Property
    Public Property SessionID() As String
        Get
            Return _SessionID
        End Get
        Set(ByVal Value As String)
            _SessionID = Value
        End Set
    End Property
    Public Property TransDate() As String
        Get
            Return _transDate
        End Get
        Set(ByVal Value As String)
            _transDate = Value
        End Set
    End Property
    Public Property TransSVC() As String
        Get
            Return _transSVC
        End Get
        Set(ByVal Value As String)
            _transSVC = Value
        End Set
    End Property
    Public Property TechnicianID() As String
        Get
            Return fstrTechnicianID
        End Get
        Set(ByVal Value As String)
            fstrTechnicianID = Value
        End Set
    End Property
#End Region



#Region "Methods"

    Function GenerateRunningNo() As String
        Dim result As Integer = 0
        Me.screends = New DataSet()
        Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim fstrSpName As String = "BB_FNCTSAJ_GenerateRunningNo"

        Try
            Dim Param() As SqlParameter = New SqlParameter(3) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(conn, fstrSpName)
            Param(0).Value = fstrServiceCenterID
            Param(1).Direction = ParameterDirection.Output

            screends = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, fstrSpName, Param)
            GenerateRunningNo = Param(1).Value


            conn.Dispose()


        Catch ex As SqlException
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(UserID, ErrorLog.Item(1).ToString, fstrSpName, _
                               WriteErrLog.SELE, "clsAdjTechStock.vb")
            result = -1
        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(UserID, ErrorLog.Item(1).ToString, fstrSpName, _
                               WriteErrLog.SELE, "clsAdjTechStock.vb")
            result = -1
        Finally
            conn.Close()
        End Try


    End Function

    Function GetTechnicianPartOnHand() As String
        Dim result As Integer = 0
        Me.screends = New DataSet()
        Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim fstrSpName As String = "BB_FNCTSAJ_GetTechPartOnHand"

        Try
            Dim Param() As SqlParameter = New SqlParameter(5) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(conn, fstrSpName)
            Param(0).Value = fstrServiceCenterID
            Param(1).Value = fstrTechnicianID
            Param(2).Value = fstrPartID
            Param(3).Value = fstrCountryID
            Param(4).Direction = ParameterDirection.Output

            screends = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, fstrSpName, Param)
            result = Val(Param(4).Value)

            conn.Dispose()
            Return result


        Catch ex As SqlException
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(UserID, ErrorLog.Item(1).ToString, fstrSpName, _
                               WriteErrLog.SELE, "clsAdjTechStock.vb")
            result = -1
        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(UserID, ErrorLog.Item(1).ToString, fstrSpName, _
                               WriteErrLog.SELE, "clsAdjTechStock.vb")
            result = -1
        Finally
            conn.Close()
        End Try


    End Function

    Function SaveTechnicianAdjustmentRecord() As Integer
        Dim result As Integer = 0

        Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim fstrSpName As String = "BB_FNCTSAJ_ITPMADD"

        Try
            Dim Param() As SqlParameter = New SqlParameter() {}
            Param = SqlHelperParameterCache.GetSpParameterSet(conn, fstrSpName)

            If Me.parttable IsNot Nothing Then

                For i As Integer = 0 To Me.parttable.Rows.Count - 1
                    If Me.parttable.Rows(i).ItemArray(Me.parttbindex.status) = 1 Then

                        fstrPartID = Me.parttable.Rows(i).ItemArray(Me.parttbindex.partID)
                        fstrCountryID = Me.parttable.Rows(i).ItemArray(Me.parttbindex.ctrID)
                        fstrPartQuantity = Me.parttable.Rows(i).ItemArray(Me.parttbindex.quantity)
                        fstrLine = i + 1

                        If fstrPartQuantity > 0 Then
                            fstrIOFlag = "I"
                        Else
                            fstrIOFlag = "O"
                            fstrPartQuantity = fstrPartQuantity * -1
                        End If

                        fstrStatus = "ACTIVE"

                        If fstrPartQuantity <> 0 Then
                            Param(0).Value = fstrTransactionType
                            Param(1).Value = fstrTransactionNo
                            Param(2).Value = fstrLine
                            Param(3).Value = fstrTransactionDate
                            Param(4).Value = fstrTechnicianID
                            Param(5).Value = fstrCompanyID
                            Param(6).Value = fstrServiceCenterID
                            Param(7).Value = fstrCountryID
                            Param(8).Value = fstrPartID
                            Param(9).Value = fstrPartQuantity
                            Param(10).Value = fstrStatus
                            Param(11).Value = fstrIOFlag
                            Param(12).Value = fstrCreateByUserID
                            SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, fstrSpName, Param)
                        End If
                    End If
                Next
            End If
            result = 1

        Catch ex As SqlException
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(UserID, ErrorLog.Item(1).ToString, fstrSpName, _
                               WriteErrLog.ADD, "clsAdjTechStock")
            result = 0
        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(UserID, ErrorLog.Item(1).ToString, fstrSpName, _
                               WriteErrLog.ADD, "clsAdjTechStock")
            result = 0
        Finally
            conn.Close()
            conn.Dispose()
        End Try

        Return result

    End Function

    Private Sub CreatePartTable()
        Me.parttable = New DataTable()
        Me.parttable.Columns.Add("PartID", System.Type.GetType("System.String"))
        Me.parttable.Columns.Add("PartName", System.Type.GetType("System.String"))
        Me.parttable.Columns.Add("CtrID", System.Type.GetType("System.String"))
        Me.parttable.Columns.Add("Sign", System.Type.GetType("System.String"))
        Me.parttable.Columns.Add("Quantity", System.Type.GetType("System.Int32"))
        Me.parttable.Columns.Add("UnitPrice", System.Type.GetType("System.Single"))
        Me.parttable.Columns.Add("LineAmt", System.Type.GetType("System.Single"))
        Me.parttable.Columns.Add("LineTax", System.Type.GetType("System.Single"))
        Me.parttable.Columns.Add("Stat", System.Type.GetType("System.Int32"))
      
        Me.parttbindex = New PartTabIndex2()
        Me.parttbindex.partID = 0
        Me.parttbindex.partName = 1
        Me.parttbindex.ctrID = 2
        Me.parttbindex.sign = 3
        Me.parttbindex.quantity = 4
        Me.parttbindex.unitprice = 5
        Me.parttbindex.lineamt = 6
        Me.parttbindex.linetax = 7
        Me.parttbindex.status = 8
    End Sub

    Public Function addPart(ByVal partid As String, _
                                ByVal partname As String, _
                                ByVal ctrid As String, _
                                ByVal unitprice As Single, _
                                ByVal quantity As Integer, _
                                ByVal linetax As Single, _
                                ByVal lineamt As Single, _
                                ByVal TranType As String) As Integer
        If Me.parttable Is Nothing Then
            Me.CreatePartTable()
        End If
        Try
            Dim existrow As DataRow() = Me.parttable.Select("PartID='" + partid + _
                                                 "'" + " and CtrID='" + ctrid + "'" + _
                                                 " and Stat=1")
            If existrow.Length > 0 Then
                If existrow(0).ItemArray(Me.parttbindex.quantity) + quantity < 0 Then
                    existrow(0).Item(Me.parttbindex.sign) = "-"
                Else
                    existrow(0).Item(Me.parttbindex.sign) = "+"
                End If
                existrow(0).Item(Me.parttbindex.quantity) = _
                            Math.Abs(existrow(0).Item(Me.parttbindex.quantity) + quantity)
                existrow(0).Item(Me.parttbindex.lineamt) = _
                            Format((existrow(0).Item(Me.parttbindex.lineamt) + lineamt), "####0.00")
                existrow(0).Item(Me.parttbindex.linetax) = _
                            (existrow(0).Item(Me.parttbindex.linetax) + linetax)
            Else
                Dim newrow As DataRow = Me.parttable.NewRow()
                newrow.Item(Me.parttbindex.partID) = partid
                newrow.Item(Me.parttbindex.partName) = partname
                newrow.Item(Me.parttbindex.ctrID) = ctrid

                If Val(Me.parttbindex.quantity) + quantity < 0 Then 'quantity < 0 Then
                    newrow.Item(Me.parttbindex.sign) = "-"
                Else
                    newrow.Item(Me.parttbindex.sign) = "+"
                End If
                newrow.Item(Me.parttbindex.quantity) = quantity
                newrow.Item(Me.parttbindex.unitprice) = Format(unitprice, "####0.00")
                newrow.Item(Me.parttbindex.lineamt) = Format(lineamt, "####0.00")
                newrow.Item(Me.parttbindex.linetax) = linetax
                newrow.Item(Me.parttbindex.status) = 1
                Me.parttable.Rows.Add(newrow)
            End If
            Return 1
        Catch ex As Exception
            Return -1
        End Try
    End Function


    Public Function delPart(ByVal partid As String, ByVal ctrid As String) As Integer
        If Me.parttable Is Nothing Then
            Me.CreatePartTable()
        End If
        Try
            Dim existrow As DataRow() = Me.parttable.Select("PartID='" + partid + _
                                                 "'" + " and CtrID='" + ctrid + "'")
            If existrow.Length > 0 Then
                existrow(0).Item(Me.parttbindex.status) = -1
                existrow(0).Delete()
            End If
            Return 1
        Catch ex As Exception
            Return -1
        End Try
    End Function


    Public Function getPartTable() As DataTable
        Return Me.parttable
    End Function

    Public Sub New(ByVal userid As String)
        Me.UserID = userid
        Dim reader As SqlDataReader = Nothing
        Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim fstrSpName As String = "BB_MASUSER_SelByID"

        Try
            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(conn, fstrSpName)
            Param(0).Value = userid
            Param(1).Value = "ACTIVE"
            reader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, fstrSpName, Param)
            If reader.Read() Then
                fstrCountryID = reader.Item("MUSR_CTRCD")
                fstrCompanyID = reader.Item("MUSR_COMCD")
                fstrServiceCenterID = reader.Item("MUSR_SVCID")
                fstrCreateByUserID = userid
            End If
        Catch ex As SqlException
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(userid, ErrorLog.Item(1).ToString, fstrSpName, _
                               WriteErrLog.ADD, "clsAdjTechStock")
        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(userid, ErrorLog.Item(1).ToString, fstrSpName, _
                               WriteErrLog.ADD, "clsAdjTechStock")
        Finally
            reader.Close()
            conn.Close()
        End Try
    End Sub

    Public Function getSVC() As DataView
        If screends IsNot Nothing Then
            If screends.Tables.Count > 0 Then
                Return screends.Tables(0).DefaultView
            End If
        End If

        Return Nothing
    End Function

    Public Function getSVC_Tech() As Integer
        Dim result As Integer = 0
        Me.screends = New DataSet()
        Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim fstrSpName As String = "BB_FNCSTDS_View"

        Try
            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(conn, fstrSpName)
            Param(0).Value = UserID
            Param(1).Direction = ParameterDirection.Output
            screends = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, fstrSpName, Param)
            result = Param(1).Value
        Catch ex As SqlException
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(UserID, ErrorLog.Item(1).ToString, fstrSpName, _
                               WriteErrLog.ADD, "clsAdjTechStock")
            result = -1
        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(UserID, ErrorLog.Item(1).ToString, fstrSpName, _
                               WriteErrLog.ADD, "clsAdjTechStock")
            result = -1
        Finally
            conn.Close()
        End Try

        Return result
    End Function

    Public Function GetPriceID(ByVal CtrID As String) As DataSet
        Dim myCn As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim myCmd As SqlDataAdapter = New SqlDataAdapter("BB_FNC_GETPRICEID", myCn)

        myCmd.SelectCommand.CommandType = CommandType.StoredProcedure

        Dim prmCID As SqlParameter = New SqlParameter("@CTRID", SqlDbType.VarChar, 5)
        prmCID.Value = CtrID
        myCmd.SelectCommand.Parameters.Add(prmCID)

        Dim myDataset As New DataSet
        myCmd.Fill(myDataset)
        Return myDataset

    End Function

    Public Sub ClearPartTab()
        Try
            Me.parttable.Rows.Clear()
        Catch ex As Exception
            'error
        End Try
    End Sub

    Public Function getTech(ByVal svcid As String) As DataTable
        Dim table As DataTable = Nothing
        Dim rows As DataRow() = Nothing
        If screends IsNot Nothing Then
            If screends.Tables.Count > 1 Then
                table = screends.Tables(1).Clone()
                rows = screends.Tables(1).Select("MTCH_SVCID='" + svcid + "'")
                Dim newrow As DataRow = Nothing
                For i As Integer = 0 To rows.Length - 1
                    newrow = table.NewRow()
                    newrow.Item(0) = rows(i).ItemArray(0)
                    newrow.Item(1) = rows(i).ItemArray(1)
                    newrow.Item(2) = rows(i).ItemArray(2)
                    table.Rows.Add(newrow)
                Next
                Return table
            End If
        End If

        Return Nothing
    End Function

    Public Function getPartInfo(ByVal partid As String, _
                               ByVal ctrid As String, _
                               ByVal svcid As String, _
                               ByVal dt As String) As ArrayList
        Dim partinfo As ArrayList = Nothing
        Dim reader As SqlDataReader = Nothing
        Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim fstrSpName As String = "BB_FNCSDTS_ParNmPrcTax"

        Try
            Dim Param() As SqlParameter = New SqlParameter(4) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(conn, fstrSpName)
            Param(0).Value = partid
            Param(1).Value = ctrid
            Param(2).Value = svcid
            Param(3).Value = dt
            reader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, fstrSpName, Param)
            If reader.Read() Then
                partinfo = New ArrayList()
                partinfo.Add(reader.Item("partname"))
                partinfo.Add(reader.Item("unitprice"))
                partinfo.Add(reader.Item("taxrate"))
            End If
        Catch ex As SqlException
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(UserID, ErrorLog.Item(1).ToString, fstrSpName, _
                               WriteErrLog.ADD, "clsAdjTechStock")
            partinfo = Nothing
        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(UserID, ErrorLog.Item(1).ToString, fstrSpName, _
                               WriteErrLog.ADD, "clsAdjTechStock")
            partinfo = Nothing
        Finally
            If reader IsNot Nothing Then
                reader.Close()
            End If
            conn.Close()
        End Try

        Return partinfo
    End Function

    Public Function VerifyPartInfo(ByVal PartID As String, ByVal CtrID As String, ByVal PriceID As String, ByVal TranDate As String) As String
        Dim myCn As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim myCmd As SqlCommand = New SqlCommand("BB_FNCSDTS_GETPARTINFO", myCn)

        myCmd.CommandType = CommandType.StoredProcedure

        Dim prmPID As SqlParameter = New SqlParameter("@PartID", SqlDbType.VarChar, 10)
        prmPID.Value = PartID
        myCmd.Parameters.Add(prmPID)

        Dim prmCID As SqlParameter = New SqlParameter("@CtrID", SqlDbType.VarChar, 20)
        prmCID.Value = CtrID
        myCmd.Parameters.Add(prmCID)

        Dim prmPrID As SqlParameter = New SqlParameter("@PriceID", SqlDbType.VarChar, 10)
        prmPrID.Value = PriceID
        myCmd.Parameters.Add(prmPrID)

        Dim prmDID As SqlParameter = New SqlParameter("@Date", SqlDbType.VarChar, 10)
        prmDID.Value = TranDate
        myCmd.Parameters.Add(prmDID)

        Dim prmxRes As SqlParameter = New SqlParameter("@Result", SqlDbType.VarChar, 100)
        prmxRes.Direction = ParameterDirection.Output
        myCmd.Parameters.Add(prmxRes)

        myCn.Open()
        myCmd.ExecuteNonQuery()
        myCn.Dispose()

        Return CStr(myCmd.Parameters("@Result").Value)

    End Function
#End Region
End Class
