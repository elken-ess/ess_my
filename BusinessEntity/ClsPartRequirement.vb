
Imports SQLDataAccess
Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web
#Region " Module Amment Hisotry "
'Description     :   part requirement information
'History         :  
'Modified Date   :  2006-04-13
'Version         :  v1.0
'Author          :  ding
#End Region
Public Class ClsPartRequirement
#Region "Declaration"
    Private fstrSpName As String ' store procedure name
    Private fstrpartid As String 'part code
    Private fstrpartname As String ' part name
    Private fstraltname As String 'Alternate Name
    Private fstrCountryid As String 'Country ID
    Private fstrcatecode As String ' Category Code
    Private fstrmodelid As String ' Parent/ Model ID
    Private fstrfinipro As String ' Finished Product
    Private fstruom As String ' UOM
    Private fstreffectivedate As System.DateTime  ' Effective Date
    Private fstrobsoleteDate As System.DateTime 'Obsolete Date
    Private fstrsuppliername As String 'Supplier Name
    Private fstrtraditem As String ' Trading Item
    Private fstrcommonpart As String 'Common Part
    Private fstrserviceitem As String  'Service Item
    Private fstrkititem As String 'Kit Item 
    Private fstrStatus As String 'Status
    Private fstrRemarks As String 'Remarks
    Private fstrCreatedBy As String 'Created By
    Private fstrCreatedDate As System.DateTime 'Created Date
    Private fstrModifiedBy As String 'Modified By
    Private fstrModifiedDate As System.DateTime 'Modified Date
    Private fstrlogcountryid As String
    Private fstrlogrank As String
    Private fstrlogsercenter As String
    Private fstripadress As String
    Private fstrExistKit As String = ""
    Private fstrPartUse As String = ""
 


#End Region
#Region "Properties"

    Public Property partid() As String
        Get
            Return fstrpartid
        End Get
        Set(ByVal Value As String)
            fstrpartid = Value
        End Set
    End Property
    Public Property partname() As String
        Get
            Return fstrpartname
        End Get
        Set(ByVal Value As String)
            fstrpartname = Value
        End Set
    End Property

    Public Property altname() As String
        Get
            Return fstraltname
        End Get
        Set(ByVal Value As String)
            fstraltname = Value
        End Set
    End Property


    Public Property Countryid() As String
        Get
            Return fstrCountryid
        End Get
        Set(ByVal Value As String)
            fstrCountryid = Value
        End Set
    End Property

    Public Property catecode() As String
        Get
            Return fstrcatecode
        End Get
        Set(ByVal Value As String)
            fstrcatecode = Value
        End Set
    End Property
    Public Property modelid() As String
        Get
            Return fstrmodelid
        End Get
        Set(ByVal Value As String)
            fstrmodelid = Value
        End Set
    End Property

    Public Property finipro() As String
        Get
            Return fstrfinipro
        End Get
        Set(ByVal Value As String)
            fstrfinipro = Value
        End Set
    End Property
    Public Property uom() As String
        Get
            Return fstruom
        End Get
        Set(ByVal Value As String)
            fstruom = Value
        End Set
    End Property

    Public Property effectivedate() As String
        Get
            Return fstreffectivedate
        End Get
        Set(ByVal Value As String)
            fstreffectivedate = Value
        End Set
    End Property

    Public Property obsoleteDate() As String
        Get
            Return fstrobsoleteDate
        End Get
        Set(ByVal Value As String)
            fstrobsoleteDate = Value
        End Set
    End Property
    Public Property suppliername() As String
        Get
            Return fstrsuppliername
        End Get
        Set(ByVal Value As String)
            fstrsuppliername = Value
        End Set
    End Property
    Public Property traditem() As String
        Get
            Return fstrtraditem
        End Get
        Set(ByVal Value As String)
            fstrtraditem = Value
        End Set
    End Property
    Public Property commonpart() As String
        Get
            Return fstrcommonpart
        End Get
        Set(ByVal Value As String)
            fstrcommonpart = Value
        End Set
    End Property
    Public Property serviceitem() As String
        Get
            Return fstrserviceitem
        End Get
        Set(ByVal Value As String)
            fstrserviceitem = Value
        End Set
    End Property
    Public Property kititem() As String
        Get
            Return fstrkititem
        End Get
        Set(ByVal Value As String)
            fstrkititem = Value
        End Set
    End Property
    Public Property Status() As String
        Get
            Return fstrStatus
        End Get
        Set(ByVal Value As String)
            fstrStatus = Value
        End Set
    End Property
    Public Property Remarks() As String
        Get
            Return fstrRemarks
        End Get
        Set(ByVal Value As String)
            fstrRemarks = Value
        End Set
    End Property
    Public Property CreatedBy() As String
        Get
            Return fstrCreatedBy
        End Get
        Set(ByVal Value As String)
            fstrCreatedBy = Value
        End Set
    End Property
    Public Property CreatedDate() As String
        Get
            Return fstrCreatedDate
        End Get
        Set(ByVal Value As String)
            fstrCreatedDate = Value
        End Set
    End Property
    Public Property ModifiedBy() As String
        Get
            Return fstrModifiedBy
        End Get
        Set(ByVal Value As String)
            fstrModifiedBy = Value
        End Set
    End Property
    Public Property ModifiedDate() As String
        Get
            Return fstrModifiedDate
        End Get
        Set(ByVal Value As String)
            fstrModifiedDate = Value
        End Set
    End Property
    Public Property logcountryid() As String
        Get
            Return fstrlogcountryid
        End Get
        Set(ByVal Value As String)
            fstrlogcountryid = Value
        End Set
    End Property
    Public Property logrank() As String
        Get
            Return fstrlogrank
        End Get
        Set(ByVal Value As String)
            fstrlogrank = Value
        End Set
    End Property
    Public Property logsercenter() As String
        Get
            Return fstrlogsercenter
        End Get
        Set(ByVal Value As String)
            fstrlogsercenter = Value
        End Set
    End Property
    Public Property ipadress() As String
        Get
            Return fstripadress
        End Get
        Set(ByVal Value As String)
            fstripadress = Value
        End Set
    End Property

    Public Property existKit() As String
        Get
            Return fstrExistKit
        End Get
        Set(ByVal Value As String)
            fstrExistKit = Value
        End Set
    End Property

    Public Property partUse() As String
        Get
            Return fstrPartUse
        End Get
        Set(ByVal Value As String)
            fstrPartUse = Value
        End Set
    End Property
#End Region

#Region "Methods"

#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)

        connection.Open()

        Return connection
    End Function
#End Region

#Region "Insert partrequirement"

    Public Function Insert() As Integer

        Dim insConnection As SqlConnection = Nothing
        Dim insConn As SqlConnection = Nothing
        insConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        insConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASPREQ_Add"
        Try
            trans = insConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(19) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(insConn, fstrSpName)
            Param(0).Value = Trim(fstrpartid)
            Param(1).Value = Trim(fstrpartname)
            Param(2).Value = Trim(fstraltname)
            Param(3).Value = Trim(fstrCountryid)
            Param(4).Value = Trim(fstrcatecode)
            Param(5).Value = Trim(fstrmodelid)
            Param(6).Value = Trim(fstrfinipro)
            Param(7).Value = Trim(fstruom)
            Param(8).Value = Trim(fstreffectivedate)
            Param(9).Value = Trim(fstrobsoleteDate)
            Param(10).Value = Trim(fstrsuppliername)
            Param(11).Value = Trim(fstrtraditem)
            Param(12).Value = Trim(fstrcommonpart)
            Param(13).Value = Trim(fstrserviceitem)
            Param(14).Value = Trim(fstrkititem)
            Param(15).Value = Trim(fstrStatus)
            Param(16).Value = Trim(fstrRemarks)
            Param(17).Value = Trim(fstrCreatedBy) '创建者
            Param(18).Value = Trim(fstrModifiedBy) '最后修改者

            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(insConn, CommandType.StoredProcedure, fstrSpName, Param)

            ' insert audit log
            Dim WriteAuditLog As New clsLogFile()
            Dim insData As String = Param(0).Value + "," + Param(1).Value + "," + Param(2).Value + "," + Param(3).Value + "," + Param(4).Value + "," + Param(5).Value + "," + Param(6).Value + "," + Param(7).Value + "," + Param(8).Value + "," + Param(9).Value + "," + Param(10).Value
            WriteAuditLog.AddLog(fstrModifiedBy, "table name:MROU_FIL", "Data:" + insData, WriteAuditLog.ADD)

            trans.Commit()
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "ClsPartRequirement.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "ClsPartRequirement.vb")
            Return -1
        Finally
            insConnection.Close()
            insConn.Close()
        End Try

    End Function

#End Region


#Region "Update Partrequirement"

    Public Function Update() As Integer

        Dim UpdConnection As SqlConnection = Nothing
        Dim UpdConn As SqlConnection = Nothing
        UpdConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        UpdConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASPREQ_Upd"
        Try
            trans = UpdConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(20) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(UpdConn, fstrSpName)
            Param(0).Value = Trim(fstrpartid)
            Param(1).Value = Trim(fstrpartname)
            Param(2).Value = Trim(fstraltname)
            Param(3).Value = Trim(fstrCountryid)
            Param(4).Value = Trim(fstrcatecode)
            Param(5).Value = Trim(fstrmodelid)
            Param(6).Value = Trim(fstrfinipro)
            Param(7).Value = Trim(fstruom)
            Param(8).Value = Trim(fstreffectivedate)
            Param(9).Value = Trim(fstrobsoleteDate)
            Param(10).Value = Trim(fstrsuppliername)
            Param(11).Value = Trim(fstrtraditem)
            Param(12).Value = Trim(fstrcommonpart)
            Param(13).Value = Trim(fstrserviceitem)
            Param(14).Value = Trim(fstrkititem)
            Param(15).Value = Trim(fstrStatus)
            Param(16).Value = Trim(fstrRemarks)
            Param(17).Value = Trim(fstrModifiedBy) '最后修改者
            Param(18).Value = Trim(fstrlogsercenter)
            Param(19).Value = Trim(fstripadress)

            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(UpdConn, CommandType.StoredProcedure, fstrSpName, Param)
          
            trans.Commit()
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "ClsInstalledBase.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "ClsInstalledBase.vb")
            Return -1
        Finally
            UpdConnection.Close()
            UpdConn.Close()
        End Try
    End Function
#End Region

#Region "search part requirement"

    Public Function Getpartrequire() As DataSet

        Dim selConnection As SqlConnection = Nothing
        Dim selConn As SqlConnection = Nothing
        selConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASPREQ_View"
        Try
            trans = selConnection.BeginTransaction()

            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(5) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrpartid)
            Param(1).Value = Trim(fstrpartname)
            Param(2).Value = Trim(fstrStatus)
            Param(3).Value = Trim(fstrlogcountryid)
            Param(4).Value = Trim(fstrlogrank)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)

            ' switch status id to status name for display   
            Dim count As Integer
            Dim statXmlTr As New clsXml
            statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            count = 0
            For count = 0 To ds.Tables(0).Rows.Count - 1
                Dim statusid As String = ds.Tables(0).Rows(count).Item(5).ToString 'item(5) is status
                Dim finipro As String = ds.Tables(0).Rows(count).Item(3).ToString 'item(3) is  Finished Product
                Dim kititem As String = ds.Tables(0).Rows(count).Item(4).ToString 'item(4) is  kiting items

                ds.Tables(0).Rows(count).Item(5) = statXmlTr.GetLabelName("StatusMessage", statusid)
                ds.Tables(0).Rows(count).Item(3) = statXmlTr.GetLabelName("StatusMessage", finipro)
                ds.Tables(0).Rows(count).Item(4) = statXmlTr.GetLabelName("StatusMessage", kititem)
            Next
            Return ds

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "ClsPartRequirement.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "ClsPartRequirement.vb")
        End Try

    End Function

    Public Function GetStandardPart() As DataSet


        Dim selConn As SqlConnection = Nothing

        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))


        fstrSpName = "BB_MASPREQ_GetStandardPart"
        Try


            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(6) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrpartid)
            Param(1).Value = Trim(fstrpartname)
            Param(2).Value = Trim(fstrStatus)
            Param(3).Value = Trim(fstrlogcountryid)
            Param(4).Value = Trim(fstrlogrank)
            Param(5).Value = Trim(fstrobsoleteDate)

            Dim ds As DataSet = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)

            ' switch status id to status name for display   
            Dim count As Integer
            Dim statXmlTr As New clsXml
            statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            count = 0
            For count = 0 To ds.Tables(0).Rows.Count - 1
                Dim statusid As String = ds.Tables(0).Rows(count).Item(5).ToString 'item(5) is status
                Dim finipro As String = ds.Tables(0).Rows(count).Item(3).ToString 'item(3) is  Finished Product
                Dim kititem As String = ds.Tables(0).Rows(count).Item(4).ToString 'item(4) is  kiting items

                ds.Tables(0).Rows(count).Item(5) = statXmlTr.GetLabelName("StatusMessage", statusid)
                ds.Tables(0).Rows(count).Item(3) = statXmlTr.GetLabelName("StatusMessage", finipro)
                ds.Tables(0).Rows(count).Item(4) = statXmlTr.GetLabelName("StatusMessage", kititem)
            Next
            Return ds

        Catch ex As Exception

            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "ClsPartRequirement.vb")

        Catch ex As SqlException

            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "ClsPartRequirement.vb")
        End Try

    End Function


#End Region

#Region "Select PartRequirement By ID"

    Public Function GetpartDetailsByID() As SqlDataReader
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASPREQ_SelByID"
        Try
            trans = sidConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstrpartid)

            Param(1).Value = Trim(fstrCountryid)
            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            Return dr

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            'Return ErrorLog

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            'Return ErrorLog
        End Try
    End Function
#End Region
#Region "confirm no duplicated id and name"
    Public Function GetDuplicatedpart() As Integer
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASPREQ_Check"
        Try
            trans = sidConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstrpartid)
            Param(1).Value = Trim(fstrCountryid)

            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            If dr.Read() Then
                dr.Close()
                Return -1
            End If
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("ding", ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsPartRequirement.vb")
            Return -1
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("ding", ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsPartRequirement.vb")
            Return -1
        Finally
            sidConnection.Close()
            sidConn.Close()
        End Try
    End Function
#End Region

#Region "confirm no duplicated name"
    Public Function GetDuplicatedpartName() As Integer
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASPREQ_CheckName"
        Try
            trans = sidConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstrpartname)
            Param(1).Value = Trim(fstrCountryid)

            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            If dr.Read() Then
                dr.Close()
                Return -1
            End If
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("ding", ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsPartRequirement.vb")
            Return -1
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("ding", ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsPartRequirement.vb")
            Return -1
        Finally
            sidConnection.Close()
            sidConn.Close()
        End Try
    End Function
#End Region

#Region "confirm no duplicated name for modify page"
    Public Function GetDuplicatedModifypartName() As Integer
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASPREQ_ModifyCheckName"
        Try
            trans = sidConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(3) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstrpartname)
            Param(1).Value = Trim(fstrpartid)
            Param(2).Value = Trim(fstrCountryid)

            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            If dr.Read() Then
                dr.Close()
                Return -1
            End If
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("ding", ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsPartRequirement.vb")
            Return -1
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("ding", ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsPartRequirement.vb")
            Return -1
        Finally
            sidConnection.Close()
            sidConn.Close()
        End Try
    End Function
#End Region

#Region "search Kit item messages"
    Public Function GetKititem() As DataSet
        Dim selConnection As SqlConnection = Nothing
        Dim selConn As SqlConnection = Nothing
        selConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASKIT_Search"
        Try
            trans = selConnection.BeginTransaction()

            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrpartid)
            Param(1).Value = Trim(fstrCountryid)

            Dim ds As DataSet = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)

            ' switch status id to status name for display   
            Dim count As Integer
            Dim statXmlTr As New clsXml
            statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            count = 0
            For count = 0 To ds.Tables(0).Rows.Count - 1
                Dim statusid As String = ds.Tables(0).Rows(count).Item(2).ToString 'item(2) is status
                ds.Tables(0).Rows(count).Item(2) = statXmlTr.GetLabelName("StatusMessage", statusid)
            Next
            Return ds

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "ClsPartRequirement.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "ClsPartRequirement.vb")
        End Try

    End Function
#End Region

#Region "Check Exist Kit Item"
    Public Function GetExistKit() As Integer
        Dim UpdConnection As SqlConnection = Nothing
        Dim UpdConn As SqlConnection = Nothing
        UpdConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        UpdConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASPREQ_CHKKIT"
        Try
            trans = UpdConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(20) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(UpdConn, fstrSpName)

            Param(0).Value = Trim(fstrpartid)
            Param(1).Value = ""

            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(UpdConn, CommandType.StoredProcedure, fstrSpName, Param)
            fstrExistKit = Param(1).Value

            If fstrExistKit <> "" Then
                Return -1
            Else : Return 0
            End If
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsPartcategory.vb")
            Return -1
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsPartcategory.vb")
            Return -1
        Finally
            UpdConnection.Close()
            UpdConn.Close()
        End Try

    End Function
#End Region

#Region "Check Part ID is being in used"
    Public Function GetPartUse() As Integer
        Dim UpdConnection As SqlConnection = Nothing
        Dim UpdConn As SqlConnection = Nothing
        UpdConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        UpdConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASKIT_CHECKKIT"
        Try
            trans = UpdConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(3) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(UpdConn, fstrSpName)

            Param(0).Value = Trim(fstrpartid)
            Param(1).Value = Trim(fstrCountryid)
            Param(2).Value = ""

            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(UpdConn, CommandType.StoredProcedure, fstrSpName, Param)
            fstrPartUse = Param(1).Value

            If fstrPartUse <> "" Then
                Return -1  'Part Being Use
            Else : Return 0
            End If
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsPartcategory.vb")
            Return -1
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsPartcategory.vb")
            Return -1
        Finally
            UpdConnection.Close()
            UpdConn.Close()
        End Try

    End Function
#End Region

#Region "Update Part and Kit Status"
    Public Function UpdatePartKit() As Integer
        Dim UpdConnection As SqlConnection = Nothing
        Dim UpdConn As SqlConnection = Nothing
        UpdConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        UpdConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASPREQ_UpdPartKit"
        Try
            trans = UpdConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(20) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(UpdConn, fstrSpName)
            Param(0).Value = Trim(fstrpartid)
            Param(1).Value = Trim(fstrpartname)
            Param(2).Value = Trim(fstraltname)
            Param(3).Value = Trim(fstrCountryid)
            Param(4).Value = Trim(fstrcatecode)
            Param(5).Value = Trim(fstrmodelid)
            Param(6).Value = Trim(fstrfinipro)
            Param(7).Value = Trim(fstruom)
            Param(8).Value = Trim(fstreffectivedate)
            Param(9).Value = Trim(fstrobsoleteDate)
            Param(10).Value = Trim(fstrsuppliername)
            Param(11).Value = Trim(fstrtraditem)
            Param(12).Value = Trim(fstrcommonpart)
            Param(13).Value = Trim(fstrserviceitem)
            Param(14).Value = Trim(fstrkititem)
            Param(15).Value = Trim(fstrStatus)
            Param(16).Value = Trim(fstrRemarks)
            Param(17).Value = Trim(fstrModifiedBy) '最后修改者
            Param(18).Value = Trim(fstrlogsercenter)
            Param(19).Value = Trim(fstripadress)

            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(UpdConn, CommandType.StoredProcedure, fstrSpName, Param)

            trans.Commit()
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "ClsInstalledBase.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "ClsInstalledBase.vb")
            Return -1
        Finally
            UpdConnection.Close()
            UpdConn.Close()
        End Try
    End Function
#End Region


#Region "DEL CHECK"
    Public Function GetPartdel() As SqlDataReader
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASPREQ_DELID"
        Try
            trans = sidConnection.BeginTransaction()
            Dim Param() As SqlParameter = New SqlParameter(1) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstrpartid)
            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            Return dr
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsPartcategory.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsPartcategory.vb")

        End Try
    End Function
#End Region
#End Region
End Class
