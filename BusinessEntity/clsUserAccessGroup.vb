﻿'Imports Microsoft.VisualBasic
Imports SQLDataAccess
Imports BusinessEntity
Imports System.Data.SqlClient
Imports System.Data
Imports System.Configuration
Imports System.Web.SessionState.HttpSessionState

#Region "Module Description"
'Description        :   User Access Control
'Last Modified Date :   2006-04-28
'Version            :   1.0
'Author             :   BAIYUNPENG
#End Region

Public Class clsUserAccessGroup

#Region "Declaration"
    Private strGroupID As String
    Private strGroupName As String
    Private strAlterGroupName As String
    Private strGroupStatus As String
    Private strCreatedBy As String
    Private strCreatedDate As String
    Private strModifiedBy As String
    Private strModifiedDate As String
    Private strStoreProcedureName As String

    Private strUAGFunID As String
    Private strIPAddress As String
    Private strSessionID As String
    Private strServiceID As String

    Private cNewFG As Char
    Private cEditFG As Char
    Private cViewFG As Char
    Private cPrintFG As Char
    Private cDelFG As Char

#End Region

#Region "Properties"
    Public Property GroupID() As String
        Get
            Return strGroupID
        End Get
        Set(ByVal value As String)
            strGroupID = value
        End Set
    End Property

    Public Property GroupName() As String
        Get
            Return strGroupName
        End Get
        Set(ByVal value As String)
            strGroupName = value
        End Set
    End Property

    Public Property AlterGroupName() As String
        Get
            Return strAlterGroupName
        End Get
        Set(ByVal value As String)
            strAlterGroupName = value
        End Set
    End Property

    Public Property GroupStatus() As String
        Get
            Return strGroupStatus
        End Get
        Set(ByVal value As String)
            strGroupStatus = value
        End Set
    End Property

    Public Property CreatedBy() As String
        Get
            Return strCreatedBy
        End Get
        Set(ByVal value As String)
            strCreatedBy = value
        End Set
    End Property

    Public Property CreatedDate() As String
        Get
            Return strCreatedDate
        End Get
        Set(ByVal value As String)
            strCreatedDate = value
        End Set
    End Property

    Public Property ModifiedBy() As String
        Get
            Return strModifiedBy
        End Get
        Set(ByVal value As String)
            strModifiedBy = value
        End Set
    End Property

    Public Property ModifiedDate() As String
        Get
            Return strModifiedDate
        End Get
        Set(ByVal value As String)
            strModifiedDate = value
        End Set
    End Property

    Public Property FunctionId() As String
        Get
            Return strUAGFunID
        End Get
        Set(ByVal value As String)
            strUAGFunID = value
        End Set
    End Property

    Public Property NewFlag() As Char
        Get
            Return cNewFG
        End Get
        Set(ByVal value As Char)
            cNewFG = value
        End Set
    End Property

    Public Property EditFlag() As Char
        Get
            Return cEditFG
        End Get
        Set(ByVal value As Char)
            cEditFG = value
        End Set
    End Property

    Public Property ViewFlag() As Char
        Get
            Return cViewFG
        End Get
        Set(ByVal value As Char)
            cViewFG = value
        End Set
    End Property

    Public Property PrintFlag() As Char
        Get
            Return cPrintFG
        End Get
        Set(ByVal value As Char)
            cPrintFG = value
        End Set
    End Property

    Public Property DeleteFlag() As Char
        Get
            Return cDelFG
        End Get
        Set(ByVal value As Char)
            cDelFG = value
        End Set
    End Property

    Public Property IPAddress() As String
        Get
            Return strIPAddress
        End Get
        Set(ByVal value As String)
            strIPAddress = value
        End Set
    End Property

    Public Property SessionID() As String
        Get
            Return strSessionID
        End Get
        Set(ByVal value As String)
            strSessionID = value
        End Set
    End Property

    Public Property ServiceID() As String
        Get
            Return strServiceID
        End Get
        Set(ByVal value As String)
            strServiceID = value
        End Set
    End Property

#End Region

#Region "Methods"

#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        connection.Open()

        Return connection
    End Function
#End Region

#Region "Query User Access Group"

    Public Function GetUserGroup() As DataSet
        Dim sqlConn As SqlConnection = Nothing
        sqlConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        strStoreProcedureName = "BB_MASMUAG_View"
        Dim dsUAG As DataSet = New DataSet
        Try
            Dim paramStorc() As SqlParameter = New SqlParameter(3) {}
            paramStorc = SqlHelperParameterCache.GetSpParameterSet(sqlConn, strStoreProcedureName)
            paramStorc(0).Value = strGroupID
            paramStorc(1).Value = strGroupName
            paramStorc(2).Value = strGroupStatus


            dsUAG = SqlHelper.ExecuteDataset(sqlConn, CommandType.StoredProcedure, strStoreProcedureName, paramStorc)
            Dim countDS As Integer
            Dim stateXML As New clsXml
            Dim stateMessage As String
            stateXML.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            For countDS = 0 To dsUAG.Tables(0).Rows.Count - 1
                stateMessage = stateXML.GetLabelName("StatusMessage", dsUAG.Tables(0).Rows(countDS).Item(3).ToString)
                dsUAG.Tables(0).Rows(countDS).Item(3) = stateMessage
            Next
        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("UAG", ErrorLog.Item(1).ToString, strStoreProcedureName, WriteErrLog.SELE, "clsUserAccessGroup.vb")
            Return Nothing
        Finally
            sqlConn.Close()
        End Try
        GetUserGroup = dsUAG
        sqlConn.Dispose()
    End Function

#End Region

#Region "SelectedByID"
    Public Function SelectedByID() As DataSet
        Dim sqlConn As SqlConnection = Nothing
        sqlConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        strStoreProcedureName = "BB_MASMUAG_SelByID"
        Dim dsUAG As DataSet = New DataSet
        Try
            Dim paramStorc() As SqlParameter = New SqlParameter(2) {}
            paramStorc = SqlHelperParameterCache.GetSpParameterSet(sqlConn, strStoreProcedureName)
            paramStorc(0).Value = strGroupID
            paramStorc(1).Value = strGroupStatus

            dsUAG = SqlHelper.ExecuteDataset(sqlConn, CommandType.StoredProcedure, strStoreProcedureName, paramStorc)

        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("UAG", ErrorLog.Item(1).ToString, strStoreProcedureName, WriteErrLog.SELE, "clsUserAccessGroup.vb")
            Return Nothing
        Finally
            sqlConn.Close()
        End Try
        SelectedByID = dsUAG
        sqlConn.Dispose()

    End Function
#End Region

#Region "SelectedByName"
    Public Function SelectedByName() As Integer
        Dim sqlConn As SqlConnection = Nothing
        sqlConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        strStoreProcedureName = "BB_MASMUAG_SelByName"
        Dim iAffectedCount As Integer = 0
        Dim reader As SqlDataReader

        Try
            Dim paramStorc() As SqlParameter = New SqlParameter(2) {}
            paramStorc = SqlHelperParameterCache.GetSpParameterSet(sqlConn, strStoreProcedureName)
            paramStorc(0).Value = strGroupID
            paramStorc(1).Value = strGroupName

            reader = SqlHelper.ExecuteReader(sqlConn, CommandType.StoredProcedure, strStoreProcedureName, paramStorc)
            If reader.Read() Then
                iAffectedCount = reader.Item(0)
            End If
        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("UAG", ErrorLog.Item(1).ToString, strStoreProcedureName, WriteErrLog.SELE, "clsUserAccessGroup.vb")
        Finally
            sqlConn.Close()
        End Try

        sqlConn.Dispose()
        Return iAffectedCount
    End Function
#End Region

#Region "Update User Access Group"
    Public Function UpdatedUserGroup() As Integer
        Dim sqlConn As SqlConnection = Nothing
        sqlConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        strStoreProcedureName = "BB_MASMUAG_Upd"
        Dim iAffectedCount As Integer = 0
        Try
            Dim paramStorc() As SqlParameter = New SqlParameter(9) {}
            paramStorc = SqlHelperParameterCache.GetSpParameterSet(sqlConn, strStoreProcedureName)
            paramStorc(0).Value = Trim(strGroupID)
            paramStorc(1).Value = Trim(strGroupName)
            paramStorc(2).Value = Trim(strAlterGroupName)
            paramStorc(3).Value = Trim(strGroupStatus)
            paramStorc(4).Value = Trim(strModifiedBy)
            paramStorc(5).Value = Trim(strModifiedDate)
            paramStorc(6).Value = Trim(strIPAddress)
            paramStorc(7).Value = Trim(strServiceID)
            paramStorc(8).Value = Trim(strSessionID)

            iAffectedCount = SqlHelper.ExecuteNonQuery(sqlConn, CommandType.StoredProcedure, strStoreProcedureName, paramStorc)
        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("UAG", ErrorLog.Item(1).ToString, strStoreProcedureName, WriteErrLog.SELE, "clsUserAccessGroup.vb")
        Finally
            sqlConn.Close()
        End Try

        sqlConn.Dispose()
        Return iAffectedCount
    End Function
#End Region

#Region "SelectByDuplicate"
    Public Function SelectedByDup() As Integer
        Dim sqlConn As SqlConnection = Nothing
        sqlConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        strStoreProcedureName = "BB_MASMUAG_SelByDup"
        Dim iAffectedCount As Integer = 0
        Dim reader As SqlDataReader
        Try
            Dim paramStorc() As SqlParameter = New SqlParameter(2) {}
            paramStorc = SqlHelperParameterCache.GetSpParameterSet(sqlConn, strStoreProcedureName)
            paramStorc(0).Value = strGroupID
            paramStorc(1).Value = strGroupName

            reader = SqlHelper.ExecuteReader(sqlConn, CommandType.StoredProcedure, strStoreProcedureName, paramStorc)
            If reader.Read() Then
                iAffectedCount = reader.Item(0)
            End If
        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("UAG", ErrorLog.Item(1).ToString, strStoreProcedureName, WriteErrLog.SELE, "clsUserAccessGroup.vb")
        Finally
            sqlConn.Close()
        End Try

        sqlConn.Dispose()
        Return iAffectedCount
    End Function
#End Region

#Region "Add User Access Group"
    Public Function AddUserGroup() As Integer
        Dim sqlConn As SqlConnection = Nothing
        sqlConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        strStoreProcedureName = "BB_MASMUAG_Add"
        Dim iAffectedCount As Integer = 0
        Try
            Dim paramStorc() As SqlParameter = New SqlParameter(8) {}
            paramStorc = SqlHelperParameterCache.GetSpParameterSet(sqlConn, strStoreProcedureName)
            paramStorc(0).Value = strGroupID
            paramStorc(1).Value = strGroupName
            paramStorc(2).Value = strAlterGroupName
            paramStorc(3).Value = strGroupStatus
            paramStorc(4).Value = strModifiedBy
            paramStorc(5).Value = Trim(strModifiedDate)
            paramStorc(6).Value = strCreatedBy
            paramStorc(7).Value = Trim(strCreatedDate)

            iAffectedCount = SqlHelper.ExecuteNonQuery(sqlConn, CommandType.StoredProcedure, strStoreProcedureName, paramStorc)
        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("UAG", ErrorLog.Item(1).ToString, strStoreProcedureName, WriteErrLog.SELE, "clsUserAccessGroup.vb")
        Finally
            sqlConn.Close()
        End Try
        sqlConn.Dispose()
        Return iAffectedCount
    End Function
#End Region

#Region "SelectUserGroup"
    'The procedure In User Access Group Function Set Interface
    'Query Table(1) -- All Module ID and Function ID
    '      Table(2) -- User specified  Module ID and Function ID
    Public Function SelectUserGroup() As DataSet
        Dim sqlConn As SqlConnection = Nothing
        sqlConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        strStoreProcedureName = "BB_MASMUAG_SelUserGroup"
        Dim dsUAG As DataSet = New DataSet
        Try
            Dim paramStorc() As SqlParameter = New SqlParameter(1) {}
            paramStorc = SqlHelperParameterCache.GetSpParameterSet(sqlConn, strStoreProcedureName)
            paramStorc(0).Value = strGroupID

            dsUAG = SqlHelper.ExecuteDataset(sqlConn, CommandType.StoredProcedure, strStoreProcedureName, paramStorc)
        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("UAG", ErrorLog.Item(1).ToString, strStoreProcedureName, WriteErrLog.SELE, "clsUserAccessGroup.vb")
            Return Nothing
        Finally
            sqlConn.Close()
        End Try
        SelectUserGroup = dsUAG
        sqlConn.Dispose()

    End Function
#End Region


#Region "Update User Access Group Function"
    Public Function UpdateUAGFunctions() As Integer
        Dim sqlConn As SqlConnection = Nothing
        sqlConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        strStoreProcedureName = "BB_MASMUAG_UpdFun"
        Dim iAffectedCount As Integer = 0
        Try
            Dim paramStorc() As SqlParameter = New SqlParameter(11) {}
            paramStorc = SqlHelperParameterCache.GetSpParameterSet(sqlConn, strStoreProcedureName)
            paramStorc(0).Value = Trim(strGroupID)
            paramStorc(1).Value = Trim(strUAGFunID)
            paramStorc(2).Value = cNewFG
            paramStorc(3).Value = cEditFG
            paramStorc(4).Value = cViewFG
            paramStorc(5).Value = cPrintFG
            paramStorc(6).Value = cDelFG
            paramStorc(7).Value = Trim(ModifiedBy)
            paramStorc(8).Value = Trim(strIPAddress)
            paramStorc(9).Value = Trim(strServiceID)
            paramStorc(10).Value = Trim(strSessionID)


            iAffectedCount = SqlHelper.ExecuteNonQuery(sqlConn, CommandType.StoredProcedure, strStoreProcedureName, paramStorc)
        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("UAG", ErrorLog.Item(1).ToString, strStoreProcedureName, WriteErrLog.SELE, "clsUserAccessGroup.vb")
        Finally
            sqlConn.Close()
        End Try

        sqlConn.Dispose()
        Return iAffectedCount
    End Function
#End Region

#Region "Prepare Update User Access Group Function"
    Public Function PreUpdateUAGFunctions() As Integer
        Dim sqlConn As SqlConnection = Nothing
        sqlConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        strStoreProcedureName = "BB_MASMUAG_PreUpdFun"
        Dim iAffectedCount As Integer = 0
        Try
            Dim paramStorc() As SqlParameter = New SqlParameter(1) {}
            paramStorc = SqlHelperParameterCache.GetSpParameterSet(sqlConn, strStoreProcedureName)
            paramStorc(0).Value = strGroupID

            iAffectedCount = SqlHelper.ExecuteNonQuery(sqlConn, CommandType.StoredProcedure, strStoreProcedureName, paramStorc)
        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("UAG", ErrorLog.Item(1).ToString, strStoreProcedureName, WriteErrLog.SELE, "clsUserAccessGroup.vb")
        Finally
            sqlConn.Close()
        End Try

        sqlConn.Dispose()
        Return iAffectedCount
    End Function
#End Region

#Region "GetUserPurview"
    Public Overloads Shared Function GetUserPurview(ByVal userAccessGroup As String, ByVal functionID As String) As Array
        Dim sqlConn As SqlConnection = Nothing
        sqlConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        Dim purviewArray As Array = New Boolean() {False, False, False, False, False}
        'Dim purviewArray As Array = Array.CreateInstance(GetType(Boolean), 4)

        Dim ds As DataSet = New DataSet        

        Try
            Dim paramStorc() As SqlParameter = New SqlParameter(2) {}
            paramStorc = SqlHelperParameterCache.GetSpParameterSet(sqlConn, "BB_MASMUAG_GetPurview")
            paramStorc(0).Value = functionID
            paramStorc(1).Value = userAccessGroup

            ds = SqlHelper.ExecuteDataset(sqlConn, CommandType.StoredProcedure, "BB_MASMUAG_GetPurview", paramStorc)

            If ds Is Nothing Then
                Return purviewArray
            End If

            purviewArray(0) = (ds.Tables(0).Rows(0).Item(0) = "1"c)  'New
            purviewArray(1) = (ds.Tables(0).Rows(0).Item(1) = "1"c)  'Edit
            purviewArray(2) = (ds.Tables(0).Rows(0).Item(2) = "1"c)  'View
            purviewArray(3) = (ds.Tables(0).Rows(0).Item(3) = "1"c)  'Print
            purviewArray(4) = (ds.Tables(0).Rows(0).Item(4) = "1"c)  'Delete


        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("UAG", ErrorLog.Item(1).ToString, "BB_MASMUAG_GetPurview", WriteErrLog.SELE, "clsUserAccessGroup.vb")
            Return purviewArray
        Finally

        End Try
        sqlConn.Dispose()
        Return purviewArray
    End Function

    Public Overloads Shared Function GetUserPurview(ByVal userAccessGroup As String, ByVal functionID As String, ByVal conFlag As String) As Boolean
        Dim sqlConn As SqlConnection = Nothing
        sqlConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        'Dim purviewArray As Array = New Boolean() {False, False, False, False}
        'Dim purviewArray As Array = Array.CreateInstance(GetType(Boolean), 4)

        Dim ds As DataSet = New DataSet
        Dim bFlag As Boolean = False
        Dim strFlag As String
        strFlag = Trim(conFlag).Substring(0, 1).ToUpper()

        Try
            Dim paramStorc() As SqlParameter = New SqlParameter(2) {}
            paramStorc = SqlHelperParameterCache.GetSpParameterSet(sqlConn, "BB_MASMUAG_GetFLAGPurview")
            paramStorc(0).Value = userAccessGroup
            paramStorc(1).Value = functionID
            paramStorc(2).Value = strFlag

            ds = SqlHelper.ExecuteDataset(sqlConn, CommandType.StoredProcedure, "BB_MASMUAG_GetFLAGPurview", paramStorc)

            If ds Is Nothing Then
                Return False
            End If

            bFlag = (ds.Tables(0).Rows(0).Item(0) = "1"c)
            
        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("UAG", ErrorLog.Item(1).ToString, "BB_MASMUAG_GetFLAGPurview", WriteErrLog.SELE, "clsUserAccessGroup.vb")
            Return False
        Finally
            sqlConn.Close()
        End Try

        sqlConn.Dispose()
        Return bFlag
    End Function

#End Region


#Region "IsDeleteUserGroup"
    'The procedure In User Access Group Function Judge that Function can or not be Deleted

    Public Function IsDeleteUserGroup(ByVal strUserGroupID As String) As Boolean
        Dim sqlConn As SqlConnection = Nothing
        sqlConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        strStoreProcedureName = "BB_MASMUAG_IsDeleteUserGroup"
        Dim bDelete As Boolean = False

        Try
            Dim paramStorc() As SqlParameter = New SqlParameter(1) {}
            paramStorc = SqlHelperParameterCache.GetSpParameterSet(sqlConn, strStoreProcedureName)
            paramStorc(0).Value = Trim(strUserGroupID)

            Dim reader As SqlDataReader = SqlHelper.ExecuteReader(sqlConn, CommandType.StoredProcedure, strStoreProcedureName, paramStorc)
            Dim iCount As Integer = 0
            If reader.Read Then
                iCount = reader.Item(0)
            End If

            bDelete = (iCount = 0)
        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("UAG", ErrorLog.Item(1).ToString, strStoreProcedureName, WriteErrLog.SELE, "clsUserAccessGroup.vb")
            Return False
        Finally
            sqlConn.Close()
        End Try

        sqlConn.Dispose()
        Return bDelete

    End Function
#End Region

#End Region

End Class

