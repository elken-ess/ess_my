Imports SQLDataAccess
Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data

#Region " Module Amment Hisotry "
'Description     :  the country entity
'History         :  
'Modified Date   :  2016-06-13
'Version         :  v1.0
'Author          :  LLY


#End Region

Public Class clsupdatepaymentcollection
    Private strConn As String
#Region "Declaration"

    Private fCRDetail As DataSet
    Private fstrROUnitID As String
    Private fstrUserID As String
    Private fstrSOID As String
    Private fstrPhoneNo As String
    Private fstrServiceBillType As String
    Private fstrServiceType As String
    Private fstrtechnician As String
    Private fstrModlty As String ' model type
    Private fstrSerialNo As String ' serial no
    Private fstrCustomerID As String
    Private fstrCustomerPrefix As String
    Private fstrCustomerName As String
    Private fstrSpName As String ' store procedure name
    Private fstrSTATUS As String
    Private fstrMifyBy As String
    Private fstrStartDate As String
    Private fstrEndDate As String
    Private fstrAppointmentNo As String
    Private fstrAppointmentPrefix As String
    Private fstrServiceBillNo As String
    Private fstrAppointmentFix As String
    Private fTransactionPf As String
    Private fTransactionNo As String
    Private fRefNo As String
#End Region

#Region "Property"
    Public Property CRDetails() As DataSet
        Get
            Return fCRDetail

        End Get
        Set(ByVal value As DataSet)
            fCRDetail = value
        End Set
    End Property

    Public Property TransactionPrefix() As String
        Get
            Return fTransactionPf
        End Get
        Set(ByVal value As String)
            fTransactionPf = value
        End Set
    End Property

    Public Property TransactionNo() As String
        Get
            Return fTransactionNo
        End Get
        Set(ByVal value As String)
            fTransactionNo = value
        End Set
    End Property

    Public Property RefNo() As String
        Get
            Return fRefNo
        End Get
        Set(ByVal value As String)
            fRefNo = value
        End Set
    End Property

    Public Property AppointmentPrefix()
        Get
            Return fstrAppointmentPrefix
        End Get
        Set(ByVal value)
            fstrAppointmentPrefix = value
        End Set
    End Property
    Public Property AppointmentNo()
        Get
            Return fstrAppointmentNo
        End Get
        Set(ByVal value)
            fstrAppointmentNo = value
        End Set
    End Property


    Public Property ServiceBillNo()
        Get
            Return fstrServiceBillNo
        End Get
        Set(ByVal value)
            fstrServiceBillNo = value
        End Set
    End Property

    Public Property StartBillDate()
        Get
            Return fstrStartDate
        End Get
        Set(ByVal value)
            fstrStartDate = value
        End Set
    End Property

    Public Property EndBillDate()
        Get
            Return fstrEndDate
        End Get
        Set(ByVal value)
            fstrEndDate = value
        End Set
    End Property

    Public Property ROUnitID()
        Get
            Return fstrROUnitID
        End Get
        Set(ByVal value)
            fstrROUnitID = value
        End Set
    End Property
    Public Property UserID()
        Get
            Return fstrUserID
        End Get
        Set(ByVal value)
            fstrUserID = value
        End Set
    End Property

    Public Property SOID()
        Get
            Return fstrSOID
        End Get
        Set(ByVal value)
            fstrSOID = value
        End Set
    End Property
    Public Property ServiceType()
        Get
            Return fstrServiceType
        End Get
        Set(ByVal value)
            fstrServiceType = value
        End Set
    End Property
    Public Property ServiceBillType()
        Get
            Return fstrServiceBillType
        End Get
        Set(ByVal value)
            fstrServiceBillType = value
        End Set
    End Property

    Public Property PhoneNo() As String
        Get
            Return fstrPhoneNo
        End Get
        Set(ByVal value As String)
            fstrPhoneNo = value
        End Set
    End Property
    Public Property CustomerName() As String
        Get
            Return fstrCustomerName
        End Get
        Set(ByVal value As String)
            fstrCustomerName = value
        End Set
    End Property

    Public Property Modlty() As String
        Get
            Return fstrModlty
        End Get
        Set(ByVal value As String)
            fstrModlty = value
        End Set
    End Property
    Public Property SerialNo() As String
        Get
            Return fstrSerialNo
        End Get
        Set(ByVal value As String)
            fstrSerialNo = value
        End Set
    End Property

    Public Property CustomerID() As String
        Get
            Return fstrCustomerID
        End Get
        Set(ByVal value As String)
            fstrCustomerID = value
        End Set
    End Property
    Public Property CustomerPrefix() As String
        Get
            Return fstrCustomerPrefix
        End Get
        Set(ByVal value As String)
            fstrCustomerPrefix = value
        End Set
    End Property

    Public Property SpName() As String
        Get
            Return fstrSpName
        End Get
        Set(ByVal value As String)
            fstrSpName = value
        End Set
    End Property
    Public Property STATUS() As String
        Get
            Return fstrSTATUS
        End Get
        Set(ByVal value As String)
            fstrSTATUS = value
        End Set
    End Property
    Public Property ModifyBy() As String
        Get
            Return fstrMifyBy
        End Get
        Set(ByVal Value As String)
            fstrMifyBy = Value
        End Set
    End Property

    Public Property Technician() As String
        Get
            Return fstrtechnician
        End Get
        Set(ByVal value As String)
            fstrtechnician = value
        End Set
    End Property

    Public Property AppointmentFix() As String
        Get
            Return fstrAppointmentFix
        End Get
        Set(ByVal value As String)
            fstrAppointmentFix = value
        End Set
    End Property

#End Region

#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)

        connection.Open()

        Return connection
    End Function
#End Region

#Region "search"
    Public Function SelectAllPaymentCollection() As DataSet
        Dim selConn As SqlConnection = Nothing
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        fstrSpName = "BB_FNCUSCR_View"
        Dim ds As DataSet = New DataSet()
        Try
            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(16) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrSOID)
            Param(1).Value = Trim(fstrCustomerName)
            Param(2).Value = Trim(fstrPhoneNo)
            Param(3).Value = Trim(fstrSerialNo)
            Param(4).Value = Trim(fstrUserID)
            Param(5).Value = Trim(fstrSTATUS)
            Param(6).Value = Trim(fstrStartDate)
            Param(7).Value = Trim(fstrEndDate)
            Param(8).Value = Trim(fstrCustomerPrefix)
            Param(9).Value = Trim(fstrCustomerID)
            Param(10).Value = Trim(fstrAppointmentPrefix)
            Param(11).Value = Trim(fstrAppointmentNo)
            Param(12).Value = Trim(fstrServiceBillNo)
            Param(13).Value = ""
            Param(14).Value = Trim(fstrtechnician)
            Param(15).Value = Trim(fstrAppointmentFix) 
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            SelectAllPaymentCollection = ds

            selConn.Dispose()
        Catch ex As SqlException
        Finally
            selConn.Close()
        End Try

    End Function
#End Region

#Region "seldropdown"

    Public Function selds(ByVal transNo As String, ByVal transType As String) As DataSet
        Dim selConn As SqlConnection = Nothing
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        fstrSpName = "BB_FNCUSINV"
        Dim ds As DataSet = New DataSet()
        Try
            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(transNo)
            Param(1).Value = Trim(transType)
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("wang", ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsupdatepaymentcollection.vb")

        Finally
            selConn.Close()
        End Try
        Return ds
    End Function

#End Region

#Region "get status message name"
    Public Function searchmoney(ByVal transType As String, ByVal transno As String) As DataTable

        Dim statConnection As SqlConnection = Nothing
        Dim statConn As SqlConnection = Nothing
        statConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        statConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_FNCUSB2_STATUS"

        Dim ds As DataSet = New DataSet()
        Try
            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(statConn, fstrSpName)
            Param(0).Value = Trim(transType)
            Param(1).Value = Trim(transno)
            ds = SqlHelper.ExecuteDataset(statConn, CommandType.StoredProcedure, fstrSpName, Param)
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("wang", ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsupdateservice2.vb")
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("wang", ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsupdateservice2.vb")
        Finally
            statConn.Close()
            statConnection.Close()
        End Try

        Dim statXmlTr As New clsXml
        statXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        Dim strPaid As String
        Dim strpayam As String
        Dim infon As New DataTable
        Dim newrow As DataRow
        infon.Columns.Add("PayID", System.Type.GetType("System.String"))
        infon.Columns.Add("PayAM", System.Type.GetType("System.String"))

        For i As Integer = 0 To ds.Tables(1).Rows.Count - 1
            strPaid = ds.Tables(1).Rows(i).Item(0).ToString()
            strpayam = ds.Tables(1).Rows(i).Item(1).ToString()
            newrow = infon.Rows.Add()
            newrow.Item(0) = strPaid
            newrow.Item(1) = strpayam
        Next

        Return infon

    End Function

    Public Function CheckDuplicateServiceBillNo() As Integer
        Dim fstrSPName As String
        Dim scnTran As SqlConnection
        Dim scnProc As SqlConnection
        scnTran = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        scnProc = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        scnTran.Open()
        scnProc.Open()

        Dim trans As SqlTransaction = Nothing
        fstrSPName = "BB_FNCSVCBILL_CHECKDUP"
        Try
            trans = scnTran.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(5) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(scnProc, fstrSPName)
            Param(0).Value = Me.fTransactionPf
            Param(1).Value = Me.fTransactionNo
            Param(2).Value = Me.fstrServiceBillNo
            Param(3).Value = 0

            SqlHelper.ExecuteNonQuery(scnProc, CommandType.StoredProcedure, fstrSPName, Param)
            Dim spCounter = Param(3).Value
            scnTran.Close()
            scnProc.Close()

            Return spCounter

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSPName, WriteErrLog.ADD, "clsServiceBillUpdate.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSPName, WriteErrLog.ADD, "clsServiceBillUpdate.vb")
            Return -1
        End Try
    End Function
#End Region

    Public Function SaveColPaymentDetails() As String

        Dim fstrSPName As String
        Dim scnTran As SqlConnection
        Dim scnProc As SqlConnection
        scnTran = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        scnProc = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        scnTran.Open()
        scnProc.Open()

        SaveColPaymentDetails = fTransactionPf + fTransactionNo

        Dim trans As SqlTransaction = Nothing
        Dim Param() As SqlParameter = New SqlParameter(20) {}

        fstrSPName = "BB_FNCSVCBILL_ADD_CR"
        Dim i As Integer
        trans = scnTran.BeginTransaction()
        If fCRDetail.Tables.Count = 0 Then Exit Function

        While i <= fCRDetail.Tables(0).Rows.Count - 1
            Try
                Param = SqlHelperParameterCache.GetSpParameterSet(scnProc, fstrSPName)
                Param(0).Value = fTransactionNo
                Param(1).Value = fstrServiceBillNo
                Param(2).Value = fCRDetail.Tables(0).Rows(i).Item(3) 'receipt no
                Param(3).Value = fCRDetail.Tables(0).Rows(i).Item(0) 'rcv datetime
                Param(4).Value = fCRDetail.Tables(0).Rows(i).Item(2) 'rcv amt
                Param(5).Value = fCRDetail.Tables(0).Rows(i).Item(4)
                ' CC = credit card ; CQ= cheq ; CR=Credit ; CS=Cash ; OP = other payment mode


                SqlHelper.ExecuteNonQuery(scnProc, CommandType.StoredProcedure, fstrSPName, Param)


            Catch ex As Exception
                trans.Rollback()
                scnTran.Close()
                scnProc.Close()

                Dim ErrorLog As ArrayList = New ArrayList
                ErrorLog.Add("Error").ToString()
                If ex.Message.Trim <> "" Then
                    'fstrErrorMessage = "[2]" & ex.Message
                End If
                ErrorLog.Add(ex.Message).ToString()

                Dim WriteErrLog As New clsLogFile()
                WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSPName, WriteErrLog.ADD, "clsupdatepaymentcollection.vb")
                Return -1

            Catch ex As SqlException
                trans.Rollback()
                scnTran.Close()
                scnProc.Close()

                Dim ErrorLog As ArrayList = New ArrayList
                ErrorLog.Add("Error").ToString()
                ErrorLog.Add(ex.Number).ToString()

                Dim WriteErrLog As New clsLogFile()
                WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSPName, WriteErrLog.ADD, "clsupdatepaymentcollection.vb")
                Return -1
            End Try

            i = i + 1
        End While

        scnTran.Close()
        scnProc.Close()

    End Function

    Public Function DeleteColPaymentDetails() As String

        Dim fstrSPName As String
        Dim scnTran As SqlConnection
        Dim scnProc As SqlConnection
        scnTran = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        scnProc = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        scnTran.Open()
        scnProc.Open()

        DeleteColPaymentDetails = fTransactionPf + fTransactionNo

        Dim trans As SqlTransaction = Nothing
        Dim Param() As SqlParameter = New SqlParameter(20) {}

        fstrSPName = "BB_FNCSVCBILL_DEL_CR"
        Dim i As Integer
        trans = scnTran.BeginTransaction()

        Try
            Param = SqlHelperParameterCache.GetSpParameterSet(scnProc, fstrSPName)
            Param(0).Value = fTransactionNo
            Param(1).Value = fstrServiceBillNo
            Param(2).Value = fRefNo 'receipt no  - cannot be empty/null
            SqlHelper.ExecuteNonQuery(scnProc, CommandType.StoredProcedure, fstrSPName, Param)


        Catch ex As Exception
            trans.Rollback()
            scnTran.Close()
            scnProc.Close()

            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            If ex.Message.Trim <> "" Then
                'fstrErrorMessage = "[2]" & ex.Message
            End If
            ErrorLog.Add(ex.Message).ToString()

            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSPName, WriteErrLog.ADD, "clsupdatepaymentcollection.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            scnTran.Close()
            scnProc.Close()

            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()

            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSPName, WriteErrLog.ADD, "clsupdatepaymentcollection.vb")
            Return -1
        End Try


            scnTran.Close()
            scnProc.Close()

    End Function



End Class
