Imports SQLDataAccess
Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data

#Region " Module Amment Hisotry "
'Description     :  tstock replenishment update
'History         :  
'Modified Date   :  2006-06-08
'Version         :  v1.0
'Author          :  xjxted

#End Region
Public Class clsStockupdate

#Region "Declaration"
    Private fstrCountryID As String
    Private fstrCompanyID As String
    Private fstrServiceCenterID As String
    Private frsuserid As String

    Private fstrPKLTY As String
    Private fstrPKLNO As String
    Private fstrPKLDT As String
    Private fstrPRFTY As String
    Private fstrPRFNO As String
    Private fstrPRFDT As String
    Private fstrPRFQT As String
    Private fstrPKLDcNO As String
    Private fstrPRFDcNO As String
    Private fstrLine As String
    Private fstrcancelPL As String
    Private fstrOnlyToday As String


    Private dsdetails As New DataSet

    Private fstrSTATUS As String

    Private fstrTaxID As String
    Private fstrTaxPerc As Single
    Private fstrUnitPrice As String 'Unit Price
    Private fstrQuantity As Integer  'Quantity
    Private fstrtax As String 'tax
    Private fstrLineAmt As String 'line Amt

    Private fstrtechnician As String '��������
    Private fstrdiscount As String 'discount
    Private fstrtotal As String 'total
    Private fstrInvoiceTax As String 'InvoiceTax
    Private fstrInvoiceDate As System.DateTime
    Private fstrRemark As String 'remark

    Private fstrSpName As String ' store procedure name
    Private fstrCreatBy As String
    Private fstrCreatDate As System.DateTime
    Private fstrMifyBy As String
    Private fstrMifyDate As System.DateTime
    Private fstrIPAddress As String
    Private fstrUserSvc As String
    Private fstrUserID As String
    'added by deyb 26092006
    Private fstrCollDate As String
    '------------------------------

    Private screends As DataSet = Nothing

#End Region

#Region "Property"
    Public Property CountryID() As String
        Get
            Return fstrCountryID
        End Get
        Set(ByVal Value As String)
            fstrCountryID = Value
        End Set
    End Property
    Public Property CompanyID() As String
        Get
            Return fstrCompanyID
        End Get
        Set(ByVal Value As String)
            fstrCompanyID = Value
        End Set
    End Property
    Public Property ServiceCenterID() As String
        Get
            Return fstrServiceCenterID
        End Get
        Set(ByVal Value As String)
            fstrServiceCenterID = Value
        End Set
    End Property
    Public Property userid() As String
        Get
            Return frsuserid
        End Get
        Set(ByVal Value As String)
            frsuserid = Value
        End Set
    End Property

    Public Property PKLType() As String
        Get
            Return fstrPKLTY
        End Get
        Set(ByVal Value As String)
            fstrPKLTY = Value
        End Set
    End Property
    Public Property PKLNO() As String
        Get
            Return fstrPKLNO
        End Get
        Set(ByVal Value As String)
            fstrPKLNO = Value
        End Set
    End Property
    Public Property PKLDate() As String
        Get
            Return fstrPKLDT
        End Get
        Set(ByVal Value As String)
            fstrPKLDT = Value
        End Set
    End Property
    Public Property PRFType() As String
        Get
            Return fstrPRFTY
        End Get
        Set(ByVal Value As String)
            fstrPRFTY = Value
        End Set
    End Property
    Public Property PRFNO() As String
        Get
            Return fstrPRFNO
        End Get
        Set(ByVal Value As String)
            fstrPRFNO = Value
        End Set
    End Property
    Public Property PRFDate() As String
        Get
            Return fstrPRFDT
        End Get
        Set(ByVal Value As String)
            fstrPRFDT = Value
        End Set
    End Property
    Public Property PRFQT() As String
        Get
            Return fstrPRFQT
        End Get
        Set(ByVal Value As String)
            fstrPRFQT = Value
        End Set
    End Property
    Public Property PKLDocNO() As String
        Get
            Return fstrPKLDcNO
        End Get
        Set(ByVal Value As String)
            fstrPKLDcNO = Value
        End Set
    End Property
    Public Property Line() As String
        Get
            Return fstrLine
        End Get
        Set(ByVal Value As String)
            fstrLine = Value
        End Set
    End Property
    Public Property PRFDocNO() As String
        Get
            Return fstrPRFDcNO
        End Get
        Set(ByVal Value As String)
            fstrPRFDcNO = Value
        End Set
    End Property

    Public Property cancelPl() As String
        Get
            Return fstrcancelPL
        End Get
        Set(ByVal Value As String)
            fstrcancelPL = Value
        End Set
    End Property
    Public Property OnlyToday() As String
        Get
            Return fstrOnlyToday
        End Get
        Set(ByVal Value As String)
            fstrOnlyToday = Value
        End Set
    End Property

    Public Property TaxID()
        Get
            Return fstrTaxID
        End Get
        Set(ByVal value)
            fstrTaxID = value
        End Set
    End Property
    Public Property TaxPerc()
        Get
            Return fstrTaxPerc
        End Get
        Set(ByVal value)
            fstrTaxPerc = value
        End Set
    End Property
    Public Property technician()
        Get
            Return fstrtechnician
        End Get
        Set(ByVal value)
            fstrtechnician = value
        End Set
    End Property
    Public Property Remark() As String
        Get
            Return fstrRemark
        End Get
        Set(ByVal value As String)
            fstrRemark = value
        End Set
    End Property

    Public Property SpName() As String
        Get
            Return fstrSpName
        End Get
        Set(ByVal value As String)
            fstrSpName = value
        End Set
    End Property
    Public Property STATUS() As String
        Get
            Return fstrSTATUS
        End Get
        Set(ByVal value As String)
            fstrSTATUS = value
        End Set
    End Property

    Public Property UnitPrice() As String
        Get
            Return fstrUnitPrice
        End Get
        Set(ByVal Value As String)
            fstrUnitPrice = Value
        End Set
    End Property
    Public Property Quantity() As String
        Get
            Return fstrQuantity
        End Get
        Set(ByVal Value As String)
            fstrQuantity = Value
        End Set
    End Property
    Public Property tax() As String
        Get
            Return fstrtax
        End Get
        Set(ByVal Value As String)
            fstrtax = Value
        End Set
    End Property
    Public Property LineAmt() As String
        Get
            Return fstrLineAmt
        End Get
        Set(ByVal Value As String)
            fstrLineAmt = Value
        End Set
    End Property
    Public Property discount() As String
        Get
            Return fstrdiscount
        End Get
        Set(ByVal Value As String)
            fstrdiscount = Value
        End Set
    End Property
    Public Property total() As String
        Get
            Return fstrtotal
        End Get
        Set(ByVal Value As String)
            fstrtotal = Value
        End Set
    End Property
    Public Property InvoiceTax() As String
        Get
            Return fstrInvoiceTax
        End Get
        Set(ByVal Value As String)
            fstrInvoiceTax = Value
        End Set
    End Property

    Public Property CreatBy() As String
        Get
            Return fstrCreatBy
        End Get
        Set(ByVal Value As String)
            fstrCreatBy = Value
        End Set
    End Property
    Public Property CreateDate() As String
        Get
            Return fstrCreatDate
        End Get
        Set(ByVal Value As String)
            fstrCreatDate = Value
        End Set
    End Property
    Public Property ModifyBy() As String
        Get
            Return fstrMifyBy
        End Get
        Set(ByVal Value As String)
            fstrMifyBy = Value
        End Set
    End Property
    Public Property ModifyDate() As String
        Get
            Return fstrMifyDate
        End Get
        Set(ByVal Value As String)
            fstrMifyDate = Value
        End Set
    End Property
    Public Property IPAddress() As String
        Get
            Return fstrIPAddress
        End Get
        Set(ByVal Value As String)
            fstrIPAddress = Value
        End Set
    End Property

    Public Property UserSvc() As String
        Get
            Return fstrUserSvc
        End Get
        Set(ByVal Value As String)
            fstrUserSvc = Value
        End Set
    End Property

    Public Property _fstrUserID() As String
        Get
            Return fstrUserID
        End Get
        Set(ByVal Value As String)
            fstrUserID = Value
        End Set
    End Property

    Public Property PRFDetails() As DataSet
        Get
            Return Me.dsdetails
        End Get
        Set(ByVal value As DataSet)
            dsdetails = value
        End Set
    End Property

    Public Property CollectDate() As String
        Get
            Return fstrCollDate
        End Get
        Set(ByVal Value As String)
            fstrCollDate = Value
        End Set
    End Property

#End Region

#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)
        connection.Open()
        Return connection
    End Function
#End Region

#Region "VIEW"
    Public Function view() As DataSet

        Dim selConnection As SqlConnection = Nothing
        Dim selConn As SqlConnection = Nothing
        selConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_FNCSTKR_View"
        Try
            trans = selConnection.BeginTransaction()

            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(7) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrServiceCenterID)
            Param(1).Value = Trim(fstrPKLDcNO)
            Param(2).Value = Trim(fstrPKLDT)
            Param(3).Value = Trim(fstrcancelPL)
            Param(4).Value = Trim(fstrOnlyToday)
            Param(5).Value = Trim(fstrSTATUS)
            Param(6).Value = Trim(fstrtechnician)
            Param(7).Value = Trim(fstrUserID)

            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            ' switch status id to status name for display
            'Dim count As Integer
            'Dim statXmlTr As New clsXml
            'Dim strPanm As String
            'statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            'For count = 0 To ds.Tables(0).Rows.Count - 1
            '    Dim statusid As String = ds.Tables(0).Rows(count).Item(5).ToString 'item(5) is status
            '    strPanm = statXmlTr.GetLabelName("StatusMessage", statusid)
            '    ds.Tables(0).Rows(count).Item(5) = strPanm
            'Next
            Return ds

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsStockupdate.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsStockupdate.vb")
        Finally
            selConnection.Close()
            selConn.Close()
        End Try

    End Function

#End Region

#Region "UPDATA"
    Public Function UPDATE() As Integer
        Dim UpdConnection As SqlConnection = Nothing
        Dim UpdConn As SqlConnection = Nothing
        UpdConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        UpdConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        Dim detailtran As Integer
        fstrSpName = "BB_FNCSTKR_Upd"
        Try

            trans = UpdConnection.BeginTransaction()
            Dim updParam() As SqlParameter = New SqlParameter(9) {}
            updParam = SqlHelperParameterCache.GetSpParameterSet(UpdConn, fstrSpName)
            updParam(0).Value = Trim(fstrPKLTY)
            updParam(1).Value = Trim(fstrPKLDcNO)
            updParam(2).Value = Trim(fstrPRFTY)
            updParam(3).Value = Trim(fstrPRFDcNO)
            updParam(4).Value = Trim(fstrPRFDT)
            updParam(5).Value = Trim(fstrMifyBy)
            updParam(6).Value = Trim(fstrIPAddress)
            updParam(7).Value = Trim(fstrSTATUS)
            updParam(8).Value = Trim(fstrCollDate)

            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(UpdConn, CommandType.StoredProcedure, fstrSpName, updParam)
            fstrPRFDcNO = updParam(3).Value
            fstrPRFTY = updParam(2).Value

            If UCase(fstrSTATUS) <> "DELETE" Then
                detailtran = GenPrf()
                If detailtran = -1 Then
                    Return -1
                End If
            End If

            trans.Commit()
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "clsCountry.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "clsCountry.vb")
            Return -1
        Finally
            UpdConnection.Close()
            UpdConn.Close()

        End Try




    End Function
#End Region

#Region "technician"
    Public Function technician1(ByVal svcid As String, ByVal userid As String) As DataTable

        Dim result As Integer = 0
        Me.screends = New DataSet()
        Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim fstrSpName As String = "BB_FNCSTDS_View"

        Try
            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(conn, fstrSpName)
            Param(0).Value = userid
            Param(1).Direction = ParameterDirection.Output
            screends = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, fstrSpName, Param)
            result = Param(1).Value

            Dim table As DataTable = Nothing
            Dim rows As DataRow() = Nothing
            If screends IsNot Nothing Then
                If screends.Tables.Count > 1 Then
                    table = screends.Tables(1).Clone()
                    rows = screends.Tables(1).Select("MTCH_SVCID='" + svcid + "'")
                    Dim newrow As DataRow = Nothing
                    For i As Integer = 0 To rows.Length - 1
                        newrow = table.NewRow()
                        newrow.Item(0) = rows(i).ItemArray(0)
                        newrow.Item(1) = rows(i).ItemArray(1)
                        newrow.Item(2) = rows(i).ItemArray(2)
                        table.Rows.Add(newrow)
                    Next
                    Return table
                End If
            End If
        Catch ex As SqlException
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(userid, ErrorLog.Item(1).ToString, fstrSpName, _
                               WriteErrLog.ADD, "clsStdStockList.vb")
            result = -1
        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(userid, ErrorLog.Item(1).ToString, fstrSpName, _
                               WriteErrLog.ADD, "clsStdStockList.vb")
            result = -1
        Finally
            conn.Close()
        End Try

    End Function
#End Region
#Region "selectByID"
    Public Function SelByID() As DataSet
        Dim selConnection As SqlConnection = Nothing
        Dim selConn As SqlConnection = Nothing
        selConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_FNCSTKR_SelByID "
        Try
            trans = selConnection.BeginTransaction()

            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(3) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(Me.fstrPKLTY)
            Param(1).Value = Trim(Me.fstrPKLNO)
            Param(2).Value = Trim(frsuserid)
            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)

            Return ds

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsStockupdate.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsStockupdate.vb")
        Finally
            selConnection.Close()
            selConn.Close()
        End Try

    End Function


    Public Function SelByIDForReport() As DataSet
        Dim selConnection As SqlConnection = Nothing
        Dim selConn As SqlConnection = Nothing
        selConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_FNCSTKR_SelByIDForReport "
        Try
            trans = selConnection.BeginTransaction()

            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(3) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(Me.fstrPKLTY)
            Param(1).Value = Trim(Me.fstrPKLNO)
            Param(2).Value = Trim(frsuserid)
            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)

            Return ds

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsStockupdate.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsStockupdate.vb")
        Finally
            selConnection.Close()
            selConn.Close()
        End Try

    End Function
#End Region


#Region "GeneratePrf"

    Public Function GenPrf() As Integer
        Dim UpdConnection As SqlConnection = Nothing
        Dim UpdConn As SqlConnection = Nothing
        UpdConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        UpdConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_FNCSTKR_GenPRF"
        Dim intAddr As Integer
        Dim i As Integer = 0
        Try


            For i = 0 To dsdetails.Tables(0).Rows.Count - 1
                trans = UpdConnection.BeginTransaction()
                Dim updParam() As SqlParameter = New SqlParameter(6) {}
                updParam = SqlHelperParameterCache.GetSpParameterSet(UpdConn, fstrSpName)
                updParam(0).Value = Trim(dsdetails.Tables(0).Rows(i).Item(0).ToString)
                updParam(1).Value = Trim(dsdetails.Tables(0).Rows(i).Item(1).ToString)
                updParam(2).Value = Trim(dsdetails.Tables(0).Rows(i).Item(4).ToString)
                updParam(3).Value = Trim(dsdetails.Tables(0).Rows(i).Item(2).ToString)
                updParam(4).Value = Trim(frsuserid)
                updParam(5).Value = fstrIPAddress
                intAddr = SqlHelper.ExecuteNonQuery(UpdConn, CommandType.StoredProcedure, fstrSpName, updParam)
                trans.Commit()
            Next


        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "clsCountry.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "clsCountry.vb")
            Return -1
        Finally
            UpdConnection.Close()
            UpdConn.Close()

        End Try
        Return intAddr
    End Function
#End Region

End Class
