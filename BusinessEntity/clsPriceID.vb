Imports SQLDataAccess
Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Web
Imports System.Data


#Region " Module Amment Hisotry "
'Description     :  the PriceID entity
'History         :  
'Modified Date   :  2006-04-10
'Version         :  v1.0
'Author          :  CM


#End Region

Public Class clsPriceID
    Private strConn As String

#Region "Declaration"

    Private fstrPriceID As String ' PriceID
    Private fstrPriceName As String ' Price name
    Private fstrPriceAlternateName As String ' alternate Price  name
    Private fstrPriceStatus As String 'Price status
    Private fstrCountryID As String ' country currency
    Private fstrCurrencyName As String ' country currency
    Private fdateEffectiveDate As System.DateTime 'EffectiveDate 
    Private fdateObsoleteDate As System.DateTime 'ObsoleteDate
    Private fstrCreatedBy As String ' CreatedBy
    Private fdateCreatedDate As System.DateTime ' CreatedDate 
    Private fstrModifiedBy As String 'ModifiedBy
    Private fdateModifiedDate As System.DateTime 'ModifiedDate
    Private fstrSpName As String ' store  procedure name
    Private fstrrank As String
    Private fstrsvcid As String
    Private fstrip As String
    ' Private fstrrandom As String

#End Region

#Region "Properties"

    Public Property PriceID() As String
        Get
            Return fstrPriceID
        End Get
        Set(ByVal Value As String)
            fstrPriceID = Value
        End Set
    End Property

    Public Property PriceName() As String
        Get
            Return fstrPriceName
        End Get
        Set(ByVal Value As String)
            fstrPriceName = Value
        End Set
    End Property


    Public Property PriceAlternateName() As String
        Get
            Return fstrPriceAlternateName
        End Get
        Set(ByVal Value As String)
            fstrPriceAlternateName = Value
        End Set
    End Property

    Public Property PriceStatus() As String
        Get
            Return fstrPriceStatus
        End Get
        Set(ByVal Value As String)
            fstrPriceStatus = Value
        End Set
    End Property
    Public Property CountryID() As String
        Get
            Return fstrCountryID
        End Get
        Set(ByVal Value As String)
            fstrCountryID = Value
        End Set
    End Property

    Public Property CurrencyName() As String
        Get
            Return fstrCurrencyName
        End Get
        Set(ByVal Value As String)
            fstrCurrencyName = Value
        End Set
    End Property
    Public Property EffectiveDate() As String
        Get
            Return fdateEffectiveDate
        End Get
        Set(ByVal Value As String)
            fdateEffectiveDate = Value
        End Set
    End Property

    Public Property ObsoleteDate() As String
        Get
            Return fdateObsoleteDate
        End Get
        Set(ByVal Value As String)
            fdateObsoleteDate = Value
        End Set
    End Property

    Public Property CreatedBy() As String
        Get
            Return fstrCreatedBy
        End Get
        Set(ByVal Value As String)
            fstrCreatedBy = Value
        End Set
    End Property
    Public Property CreatedDate() As String
        Get
            Return fdateCreatedDate
        End Get
        Set(ByVal Value As String)
            fdateCreatedDate = Value
        End Set
    End Property
    Public Property ModifiedBy() As String
        Get
            Return fstrModifiedBy
        End Get
        Set(ByVal Value As String)
            fstrModifiedBy = Value
        End Set
    End Property
    Public Property ModifiedDate() As String
        Get
            Return fdateModifiedDate
        End Get
        Set(ByVal Value As String)
            fdateModifiedDate = Value
        End Set
    End Property

    Public Property rank() As String
        Get
            Return fstrrank
        End Get
        Set(ByVal Value As String)
            fstrrank = Value
        End Set
    End Property
    Public Property svcid() As String
        Get
            Return fstrsvcid
        End Get
        Set(ByVal Value As String)
            fstrsvcid = Value
        End Set
    End Property
    Public Property ip() As String
        Get
            Return fstrip
        End Get
        Set(ByVal Value As String)
            fstrip = Value
        End Set
    End Property


#End Region

#Region "Methods"

#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)

        connection.Open()

        Return connection
    End Function
#End Region

#Region "Insert"

    Public Function Insert() As Integer

        Dim insConnection As SqlConnection = Nothing
        Dim insConn As SqlConnection = Nothing
        insConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        insConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASPRID_Add"
        Try
            trans = insConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(9) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(insConn, fstrSpName)
            Param(0).Value = Trim(fstrPriceID)
            Param(1).Value = Trim(fstrPriceName)
            Param(2).Value = Trim(fstrPriceAlternateName)
            Param(3).Value = Trim(fstrPriceStatus)
            Param(4).Value = Trim(fstrCountryID)
            'Param(5).Value = Trim(fstrCurrencyName)
            Param(5).Value = Trim(fdateEffectiveDate)
            Param(6).Value = Trim(fdateObsoleteDate)
            Param(7).Value = Trim(fstrCreatedBy)
            Param(8).Value = Trim(fstrModifiedBy)

            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(insConn, CommandType.StoredProcedure, fstrSpName, Param)

            trans.Commit()
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsPriceID.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsPriceID.vb")
            Return -1
        End Try

    End Function

#End Region

#Region "Update"

    Public Function Update() As Integer

        Dim UpdConnection As SqlConnection = Nothing
        Dim UpdConn As SqlConnection = Nothing
        UpdConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        UpdConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASPRID_Upd"
        Try
            trans = UpdConnection.BeginTransaction()
            Dim CounID As New clsCommonClass()
            Dim connm As String = CounID.getsubstring(fstrCountryID)
            Dim Param() As SqlParameter = New SqlParameter(10) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(UpdConn, fstrSpName)
            Param(0).Value = Trim(fstrPriceID)
            Param(1).Value = Trim(fstrPriceName)
            Param(2).Value = Trim(fstrPriceAlternateName)
            Param(3).Value = Trim(fstrPriceStatus)
            Param(4).Value = Trim(connm)
            Param(5).Value = Trim(fdateEffectiveDate)
            Param(6).Value = Trim(fdateObsoleteDate)
            Param(7).Value = Trim(fstrModifiedBy)
            Param(8).Value = Trim(fstrsvcid)
            Param(9).Value = Trim(fstrip)
           
            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(UpdConn, CommandType.StoredProcedure, fstrSpName, Param)
           

            trans.Commit()
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "clsPriceID.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "clsPriceID.vb")
            Return -1
        End Try

    End Function

#End Region



   
#Region "Select PriceID"

    Public Function GetPriceID() As DataSet

        Dim selConnection As SqlConnection = Nothing
        Dim selConn As SqlConnection = Nothing
        selConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASPRID_View"
        Try
            trans = selConnection.BeginTransaction()

            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(5) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrPriceID)
            Param(1).Value = Trim(fstrCountryID)
            Param(2).Value = Trim(fstrPriceStatus)
            Param(3).Value = Trim(fstrPriceName)
            Param(4).Value = Trim(fstrrank)
          

            Dim ds As DataSet = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            ' switch status id to status name for display
            Dim count As Integer
            Dim statXmlTr As New clsXml
            Dim strPanm As String
            statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            ' count = 0
            For count = 0 To ds.Tables(0).Rows.Count - 1
                Dim statusid As String = ds.Tables(0).Rows(count).Item(2).ToString 'item(2) is status
                strPanm = statXmlTr.GetLabelName("StatusMessage", statusid)
                ds.Tables(0).Rows(count).Item(2) = strPanm
                'ds.Tables(0).Rows(count).Item(2) = statXmlTr.GetLabelName("StatusMessage", statusid)
            Next
            Return ds

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsPriceID.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsPriceID.vb")
        End Try

    End Function
#End Region


#Region "Select PriceID By ID"

    Public Function GetPriceIDDetailsByID() As ArrayList
        Dim retnArray As ArrayList = New ArrayList()
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASPRID_SELBYID"
        Try
            trans = sidConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstrPriceID)
            Param(1).Value = Trim(fstrCountryID)
            'Param(2).Value = Trim(fstrPriceStatus)


            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            'Return dr
            If dr.Read() Then
                Dim count As Integer
                For count = 0 To dr.FieldCount - 1
                    retnArray.Add(dr.GetValue(count).ToString())
                Next
            End If
            dr.Close()
            Return retnArray

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsPriceID.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsPriceID.vb")
        End Try

    End Function
#End Region
#Region "Select BY PriceName"

    Public Function GetPriceIDDetailsByPriceNM() As Integer

        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASPRID_SelByName"
        Try
            trans = sidConnection.BeginTransaction()

            Dim CounID As New clsCommonClass()
            Dim connm As String = CounID.getsubstring(fstrCountryID)

            Dim Param() As SqlParameter = New SqlParameter(3) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstrPriceID)
            Param(1).Value = Trim(fstrPriceName)
            Param(2).Value = Trim(connm)

            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            If dr.Read() Then
                dr.Close()
                Return -1
            End If
            Return 0

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsPriceID.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsPriceID.vb")
            Return -1
        End Try

    End Function
#End Region
#Region "confirm no duplicated id and  name "
    Public Function GetDuplicatedPriceID() As Integer
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASPRID_IFDUP"
        Try
            trans = sidConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(3) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstrPriceID)
            Param(1).Value = Trim(fstrPriceName)
            Param(2).Value = Trim(fstrCountryID)




            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            If dr.Read() Then
                dr.Close()
                Return -1
            End If
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsPriceID.vb")
            Return -1
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsPriceID.vb")
            Return -1
        End Try
    End Function
#End Region

#Region "Select Country By ID"

    Public Function GetCountryID() As ArrayList
        Dim retnArray As ArrayList = New ArrayList()
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASPRID_COUNTRYID"
        Try
            trans = sidConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(1) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstrCountryID)


            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            'Return dr
            If dr.Read() Then
                Dim count As Integer
                For count = 0 To dr.FieldCount - 1
                    retnArray.Add(dr.GetValue(count).ToString())
                Next
            End If
            dr.Close()
            Return retnArray

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsPriceID.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsPriceID.vb")
        End Try
    End Function
#End Region
#Region "Select priceid  in pricesetup"

    Public Function GetPriceSetUp() As SqlDataReader
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASPRICESET_DELID"
        Try
            trans = sidConnection.BeginTransaction()
            Dim Param() As SqlParameter = New SqlParameter(1) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstrPriceID)
            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            Return dr
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsSalTaxID.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsSalTaxID.vb")

        End Try
    End Function


#End Region
    Public Function GetProductPriceListing() As DataSet
        Dim selConnection As SqlConnection = Nothing
        Dim selConn As SqlConnection = Nothing
        selConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASPRID_VIEWALLTYPE"
        Try
            trans = selConnection.BeginTransaction()

            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(3) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrPriceID)
            Param(1).Value = Trim(fstrCountryID)
            'Param(2).Value = Trim(fstrPriceFromID)
            'Param(3).Value = Trim(fstrPriceToID)
            'Param(4).Value = Trim(fstrSortby)
            'Param(5).Value = Trim(fstrThenby)
            'Param(6).Value = Trim(fstrCommPart)
            'Param(7).Value = Trim(fstrStatus)
            'Param(8).Value = Trim(fstrUserName)
            'Param(9).Value = Trim(fstrctrid)
            'Param(10).Value = Trim(fstrcomid)
            'Param(11).Value = Trim(fstrsvcid)
            Param(2).Value = Trim(fstrrank)
            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            ' switch status id to status name for display
            'Dim count As Integer
            'Dim statXmlTr As New clsXml
            'Dim strPanm As String
            'statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            'For count = 0 To ds.Tables(0).Rows.Count - 1
            '    Dim statusid As String = ds.Tables(0).Rows(count).Item(3).ToString 'item(3) is status
            '    strPanm = statXmlTr.GetLabelName("StatusMessage", statusid)
            '    ds.Tables(0).Rows(count).Item(3) = strPanm
            'Next
            Return ds

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsPriceID.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsPriceID.vb")
        Finally
            selConnection.Close()
            selConn.Close()
        End Try
    End Function

#End Region


End Class
