Imports SQLDataAccess
Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Web
Imports System.Data
#Region " Module Amment Hisotry "
'Description     :  the ServiceCente
'History         :  
'Modified Date   :  2006-04-11
'Version         :  v1.0
'Author          :  朱文姿


#End Region
Public Class clsServiceCente
    Private strConn As String
#Region "Declaration"

    Private fstrServiceCenterID As String ' Service Center ID
    Private fstrServiceCenterName As String 'Service Center name
    Private fstrAlternateName As String ' alternate country  name
    Private fstrPersonInCharge As String ' Person In Charge
    Private fstrBranchType As String ' Branch Type
    Private fstrAddress1 As String ' Address 1
    Private fstrAddress2 As String ' Address 2
    Private fstrPOCode As String  'PO Code
    Private fstrCountryID As String 'Country ID
    Private fstrStateID As String ' State ID

    Private fstrAreaID As String ' Area ID
    Private fstrTelephone1 As String 'Telephone 1
    Private fstrTelephone2 As String ' Telephone 2
    Private fstrPICMobile As String ' PIC Mobile 
    Private fstrFax As String ' Fax
    Private fstrStatus As String ' Status
    Private fstrEffectiveDate As System.DateTime ' Effective Date
    Private fintJobsPerDay As Integer  'Jobs Per Day
    Private fintOTPremiumJobs As Integer 'OT Premium Jobs
    Private fstrCompanyID As String 'Company ID

    Private fstrCreatedBy As String ' Created By
    'Private fstrCreatedDate As System.DateTime  'Created Date
    Private fstrModifiedBy As String 'Modified By
    'Private fstrModifiedDate As System.DateTime 'Modified Date

    Private fstrSpName As String ' store procedure name
    Private fstrusername As String ' username
    Private fstrrank As String ' rank

    Private fstrctry As String ' user country
    Private fstrcomp As String ' user company
    Private fstrsvc As String '  user svcid
    Private fstripadress As String ' ipadress
#End Region

#Region "Properties"
    Public Property Ipadress() As String
        Get
            Return fstripadress
        End Get
        Set(ByVal Value As String)
            fstripadress = Value
        End Set
    End Property
    Public Property username() As String
        Get
            Return fstrusername
        End Get
        Set(ByVal Value As String)
            fstrusername = Value
        End Set
    End Property
    Public Property ctryid() As String
        Get
            Return fstrctry
        End Get
        Set(ByVal Value As String)
            fstrctry = Value
        End Set
    End Property
    Public Property compid() As String
        Get
            Return fstrcomp
        End Get
        Set(ByVal Value As String)
            fstrcomp = Value
        End Set
    End Property
    Public Property svcid() As String
        Get
            Return fstrsvc
        End Get
        Set(ByVal Value As String)
            fstrsvc = Value
        End Set
    End Property
    Public Property rank() As String
        Get
            Return fstrrank
        End Get
        Set(ByVal Value As String)
            fstrrank = Value
        End Set
    End Property

    Public Property ServiceCenterID() As String
        Get
            Return fstrServiceCenterID
        End Get
        Set(ByVal Value As String)
            fstrServiceCenterID = Value
        End Set
    End Property

    Public Property ServiceCenterName() As String
        Get
            Return fstrServiceCenterName
        End Get
        Set(ByVal Value As String)
            fstrServiceCenterName = Value
        End Set
    End Property


    Public Property AlternateName() As String
        Get
            Return fstrAlternateName
        End Get
        Set(ByVal Value As String)
            fstrAlternateName = Value
        End Set
    End Property

    Public Property PersonInCharge() As String
        Get
            Return fstrPersonInCharge
        End Get
        Set(ByVal Value As String)
            fstrPersonInCharge = Value
        End Set
    End Property
    Public Property BranchType() As String
        Get
            Return fstrBranchType
        End Get
        Set(ByVal Value As String)
            fstrBranchType = Value
        End Set
    End Property

    Public Property Address1() As String
        Get
            Return fstrAddress1
        End Get
        Set(ByVal Value As String)
            fstrAddress1 = Value
        End Set
    End Property
    Public Property Address2() As String
        Get
            Return fstrAddress2
        End Get
        Set(ByVal Value As String)
            fstrAddress2 = Value
        End Set
    End Property

    Public Property POCode() As String
        Get
            Return fstrPOCode
        End Get
        Set(ByVal Value As String)
            fstrPOCode = Value
        End Set
    End Property

    Public Property CountryID() As String
        Get
            Return fstrCountryID
        End Get
        Set(ByVal Value As String)
            fstrCountryID = Value
        End Set
    End Property
    Public Property StateID() As String
        Get
            Return fstrStateID
        End Get
        Set(ByVal Value As String)
            fstrStateID = Value
        End Set
    End Property

    Public Property AreaID() As String
        Get
            Return fstrAreaID
        End Get
        Set(ByVal Value As String)
            fstrAreaID = Value
        End Set
    End Property
    Public Property Telephone1() As String
        Get
            Return fstrTelephone1
        End Get
        Set(ByVal Value As String)
            fstrTelephone1 = Value
        End Set
    End Property
    Public Property Telephone2() As String
        Get
            Return fstrTelephone2
        End Get
        Set(ByVal Value As String)
            fstrTelephone2 = Value
        End Set
    End Property
    Public Property PICMobile() As String
        Get
            Return fstrPICMobile
        End Get
        Set(ByVal Value As String)
            fstrPICMobile = Value
        End Set
    End Property
    Public Property Fax() As String
        Get
            Return fstrFax
        End Get
        Set(ByVal Value As String)
            fstrFax = Value
        End Set
    End Property
    Public Property Status() As String
        Get
            Return fstrStatus
        End Get
        Set(ByVal Value As String)
            fstrStatus = Value
        End Set
    End Property
    Public Property EffectiveDate() As String
        Get
            Return fstrEffectiveDate
        End Get
        Set(ByVal Value As String)
            fstrEffectiveDate = Value
        End Set
    End Property
    Public Property JobsPerDay() As Integer
        Get
            Return fintJobsPerDay
        End Get
        Set(ByVal Value As Integer)
            fintJobsPerDay = Value
        End Set
    End Property
    Public Property OTPremiumJobs() As Integer
        Get
            Return fintOTPremiumJobs
        End Get
        Set(ByVal Value As Integer)
            fintOTPremiumJobs = Value
        End Set
    End Property
    Public Property CompanyID() As String
        Get
            Return fstrCompanyID
        End Get
        Set(ByVal Value As String)
            fstrCompanyID = Value
        End Set
    End Property
    Public Property CreateBy() As String
        Get
            Return fstrCreatedBy
        End Get
        Set(ByVal Value As String)
            fstrCreatedBy = Value
        End Set
    End Property

    'Public Property CreateDate() As String
    '    Get
    '        Return fstrCreatedDate
    '    End Get
    '    Set(ByVal Value As String)
    '        fstrCreatedDate = Convert.ToDateTime(Value)
    '    End Set
    'End Property

    Public Property ModifyBy() As String
        Get
            Return fstrModifiedBy
        End Get
        Set(ByVal Value As String)
            fstrModifiedBy = Value
        End Set
    End Property
    'Public Property ModifyDate() As String
    '    Get
    '        Return fstrModifiedDate
    '    End Get
    '    Set(ByVal Value As String)
    '        fstrModifiedDate = Convert.ToDateTime(Value)
    '    End Set
    'End Property

#End Region

#Region "Methods"

#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)

        connection.Open()

        Return connection
    End Function
#End Region

#Region "Insert"

    Public Function Insert() As Integer

        Dim insConnection As SqlConnection = Nothing
        Dim insConn As SqlConnection = Nothing
        insConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        insConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASSVRC_Add"
        Try
            trans = insConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(22) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(insConn, fstrSpName)
            Param(0).Value = Trim(fstrServiceCenterID)
            Param(1).Value = Trim(fstrServiceCenterName)
            Param(2).Value = Trim(fstrAlternateName)
            Param(3).Value = Trim(fstrPersonInCharge)
            Param(4).Value = Trim(fstrBranchType)
            Param(5).Value = Trim(fstrAddress1)
            Param(6).Value = Trim(fstrAddress2)


            Param(7).Value = Trim(fstrPOCode)
            Param(8).Value = Trim(fstrCountryID)
            Param(9).Value = Trim(fstrStateID)
            Param(10).Value = Trim(fstrAreaID)
            Param(11).Value = Trim(fstrTelephone1)
            Param(12).Value = Trim(fstrTelephone2)
            Param(13).Value = Trim(fstrPICMobile)

            Param(14).Value = Trim(fstrFax)
            Param(15).Value = Trim(fstrStatus)
            Param(16).Value = Trim(fstrEffectiveDate)
            Param(17).Value = Trim(fintJobsPerDay)
            Param(18).Value = Trim(fintOTPremiumJobs)
            Param(19).Value = Trim(fstrCompanyID)
            Param(20).Value = Trim(fstrCreatedBy) '创建者



            'Param(21).Value = Convert.ToDateTime(Trim(fstrCreatedDate)) '创建时间
            
            Param(21).Value = Trim(fstrModifiedBy)  '最后修改者
            'Param(23).Value = Convert.ToDateTime(Trim(fstrModifiedDate)) '最后修改者


            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(insConn, CommandType.StoredProcedure, fstrSpName, Param)

            ' insert audit log
            ' Dim WriteAuditLog As New clsLogFile()
            'Dim insData As String = Param(0).Value + "," + Param(1).Value + "," + Param(2).Value + "," + Param(3).Value + "," + Param(4).Value + "," + Param(5).Value + "," + Param(6).Value + "," + Param(7).Value + "," + Param(8).Value + "," + Param(9).Value + "," + Param(10).Value
            ' WriteAuditLog.AddLog("wang", "table name:mctr_fil", "Data:" + insData, WriteAuditLog.ADD)

            trans.Commit()
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.VIEWS, "clsServiceCente.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.VIEWS, "clsServiceCente.vb")
            Return -1
        End Try

    End Function

#End Region

#Region "Update"

    Public Function Update() As Integer

        Dim UpdConnection As SqlConnection = Nothing
        Dim UpdConn As SqlConnection = Nothing
        UpdConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        UpdConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASSVRC_Upd"
        Try
            trans = UpdConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(24) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(UpdConn, fstrSpName)
            Param(0).Value = Trim(fstrServiceCenterID)
            Param(1).Value = Trim(fstrServiceCenterName)
            Param(2).Value = Trim(fstrAlternateName)
            Param(3).Value = Trim(fstrPersonInCharge)
            Param(4).Value = Trim(fstrBranchType)
            Param(5).Value = Trim(fstrAddress1)
            Param(6).Value = Trim(fstrAddress2)


            Param(7).Value = Trim(fstrPOCode)
            Param(8).Value = Trim(fstrCountryID)
            Param(9).Value = Trim(fstrStateID)
            Param(10).Value = Trim(fstrAreaID)
            Param(11).Value = Trim(fstrTelephone1)
            Param(12).Value = Trim(fstrTelephone2)
            Param(13).Value = Trim(fstrPICMobile)

            Param(14).Value = Trim(fstrFax)
            Param(15).Value = Trim(fstrStatus)
            Param(16).Value = Trim(fstrEffectiveDate)
            Param(17).Value = Trim(fintJobsPerDay)
            Param(18).Value = Trim(fintOTPremiumJobs)
            Param(19).Value = Trim(fstrCompanyID)
            Param(20).Value = Trim(fstrCreatedBy)
            Param(21).Value = Trim(fstrModifiedBy)  '最后修改者

            Param(22).Value = Trim(fstripadress)
            Param(23).Value = Trim(fstrsvc)
            'Param(21).Value = Convert.ToDateTime(Trim(fstrModifiedDate)) '最后修改者
            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(UpdConn, CommandType.StoredProcedure, fstrSpName, Param)
            ' insert audit log
            'Dim WriteAuditLog As New clsLogFile()
            'Dim insData As String = Param(0).Value + "," + Param(1).Value + "," + Param(2).Value + "," + Param(3).Value + "," + Param(4).Value + "," + Param(5).Value + "," + Param(6).Value + "," + Param(7).Value + "," + Param(8).Value + "," + Param(9).Value + "," + Param(10).Value
            'WriteAuditLog.AddLog("wang", "table name:mctr_fil", "Data:" + insData, WriteAuditLog.ADD)

            trans.Commit()
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "clsServiceCente.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "clsServiceCente.vb")
            Return -1
        End Try

    End Function

#End Region

#Region "Select Service  Center By ID"

    Public Function GetServiceCenterDetailsByID() As SqlDataReader
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASSVRC_SelByID"
        Try
            trans = sidConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(1) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstrServiceCenterID)

            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            Return dr

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            'Return ErrorLog

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            'Return ErrorLog
        End Try
    End Function
#End Region

#Region "Get All Service center For View"
    Public Function GetAllService() As DataSet
        Dim viewConnection As SqlConnection = Nothing
        Dim viewConn As SqlConnection = Nothing
        viewConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        viewConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASSVRC_View"
        Try
            trans = viewConnection.BeginTransaction()
            Dim ds As DataSet = SqlHelper.ExecuteDataset(viewConn, CommandType.StoredProcedure, fstrSpName)
            ' switch status id to status name for display
            Dim count As Integer
            Dim statXmlTr As New clsXml
            Dim strPanm As String
            statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            For count = 0 To ds.Tables(0).Rows.Count - 1
                Dim statusid As String = ds.Tables(0).Rows(count).Item(5).ToString 'item(3) is status
                strPanm = statXmlTr.GetLabelName("StatusMessage", statusid)
                ds.Tables(0).Rows(count).Item(5) = strPanm
            Next
            Return ds

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.VIEWS, "clsServiceCente.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.VIEWS, "clsServiceCente.vb")
        End Try
    End Function
#End Region
#Region "Select ServiceCenter"

    Public Function GetServiceCenter() As DataSet

        '    Dim ds As DataSet = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
        Dim selConnection As SqlConnection = Nothing
        Dim selConn As SqlConnection = Nothing
        selConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASSVRC_View"
        Try
            trans = selConnection.BeginTransaction()

            'define search fields 
            Dim Param() As SqlParameter = New SqlParameter(8) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrServiceCenterID)

            Param(1).Value = Trim(fstrServiceCenterName)
            Param(2).Value = Trim(fstrStatus)
            Param(3).Value = Trim(fstrctry)

            Param(4).Value = Trim(fstrcomp)
            Param(5).Value = Trim(fstrsvc)
            Param(6).Value = Trim(fstrrank)

            'Dim dr As SqlDataReader = SqlHelper.ExecuteReader(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            'Return dr

            Dim dst As DataSet = New DataSet()
            'Dim dst1 As DataSet = New DataSet()
            ''dst = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            'Dim sqda As New SqlDataAdapter("exec " + fstrSpName + " " + Param(0).Value + "," + Param(1).Value + "," + Param(2).Value, selConn)
            'sqda.Fill(dst1, "test")
            'Dim j As Integer
            'j = dst1.Tables("test").Rows.Count
            dst = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)

            ''''
       
            ' switch status id to status name for display
            Dim i As Integer
            i = dst.Tables(0).Rows.Count


            Dim count As Integer
            Dim statXmlTr As New clsXml
            Dim strPanm As String
            statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            For count = 0 To dst.Tables(0).Rows.Count - 1
                Dim statusid As String = dst.Tables(0).Rows(count).Item(3).ToString 'item(3) is status
                strPanm = statXmlTr.GetLabelName("StatusMessage", statusid)
                dst.Tables(0).Rows(count).Item(3) = strPanm
            Next
            Return dst

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsServiceCente.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsServiceCente.vb")
        End Try

    End Function
#End Region


   
#Region "Select Country By NAME"
    Public Function GetDetailsByServiceCenterNM() As Integer
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASSVRC_SelByname"
        Try
            trans = sidConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstrServiceCenterName)
            Param(1).Value = Trim(fstrServiceCenterID)


            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            If dr.Read() Then
                dr.Close()
                Return -1
            End If
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsServiceCente.vb")
            Return -1
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsServiceCente.vb")
            Return -1
        Finally
            sidConnection.Close()
            sidConn.Close()
        End Try
    End Function
#End Region
#Region "confirm no duplicated id and name"
    Public Function GetDuplicatedServiceCenter() As Integer
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASSVRC_IFDUP"
        Try
            trans = sidConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstrServiceCenterID)
            Param(1).Value = Trim(fstrServiceCenterName)

            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            If dr.Read() Then
                dr.Close()
                Return -1
            End If
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsServiceCente.vb")
            Return -1
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsServiceCente.vb")
            Return -1
        Finally
            sidConnection.Close()
            sidConn.Close()
        End Try
    End Function
#End Region
#Region "confirm no exist svc in tech"
    Public Function GetdSvcfromtech() As Integer
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASSVRC_TEXIST"
        Try
            trans = sidConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstrServiceCenterID)


            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            If dr.Read() Then
                dr.Close()
                Return -1
            End If
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsServiceCente.vb")
            Return -1
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsServiceCente.vb")
            Return -1
        Finally
            sidConnection.Close()
            sidConn.Close()
        End Try
    End Function
#End Region
#Region "confirm no exist svc in cust"
    Public Function GetdSvcfromcust() As Integer
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASSVRC_CEXIST"
        Try
            trans = sidConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstrServiceCenterID)

           
            Dim dr As DataSet = SqlHelper.ExecuteDataset(sidConn, CommandType.StoredProcedure, fstrSpName, Param)

            Dim tcount As Integer
            Dim sysbom As Integer = 0
            For tcount = 0 To dr.Tables.Count - 1
                If dr.Tables(tcount).Rows.Count <> 0 Then
                    sysbom = 1

                End If

            Next
            If sysbom = 1 Then
                Return -1
            ElseIf sysbom = 0 Then
                Return 0
            End If
            
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsServiceCente.vb")
            Return -1
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsServiceCente.vb")
            Return -1
        Finally
            sidConnection.Close()
            sidConn.Close()
        End Try
    End Function
#End Region

#Region "confirm no exist svc in cust , rou,user,tech"
    Public Function GetsvrcDel() As Integer
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASSVRC_DEL"
        Try
            trans = sidConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(1) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstrServiceCenterID)



            Dim dr As DataSet = SqlHelper.ExecuteDataset(sidConn, CommandType.StoredProcedure, fstrSpName, Param)

            Dim tcount As Integer
            Dim sysbom As Integer = 0
            For tcount = 0 To dr.Tables.Count - 1
                If dr.Tables(tcount).Rows.Count <> 0 Then
                    sysbom = 1

                End If

            Next
            If sysbom = 1 Then
                Return -1
            ElseIf sysbom = 0 Then
                Return 0
            End If
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsServiceCente.vb")
            Return -1
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsServiceCente.vb")
            Return -1
        Finally
            sidConnection.Close()
            sidConn.Close()
        End Try
    End Function
#End Region
#End Region

End Class
