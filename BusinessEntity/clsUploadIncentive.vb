﻿Imports System.Data.SqlClient
Imports SQLDataAccess
Imports System.Configuration
Imports BusinessEntity
Imports System.data
Imports System.Collections.Generic

Public Class clsUploadIncentive
    
    Private strFileName As String
    Private strUserId As String
    Private strTchId As String
    Private strAdjJob As String
    Private strRemark As String
    Private strDemerit As String
    Private strDemeritCP As String
    Private strPremiumML As String
    Private strPremiumPL As String
    Private bInsertUpdate As String
    Public IncentiveList As New List(Of clsUploadIncentive)

#Region "Properties"

    Public Property FileName() As String
        Get
            Return strFileName
        End Get
        Set(ByVal value As String)
            strFileName = value
        End Set
    End Property

    Public Property UserID() As String
        Get
            Return strUserId
        End Get
        Set(ByVal value As String)
            strUserId = value
        End Set
    End Property

    Public Property TchID() As String
        Get
            Return strTchId
        End Get
        Set(ByVal value As String)
            strTchId = value
        End Set
    End Property

    Public Property AdjJob() As String
        Get
            Return strAdjJob
        End Get
        Set(ByVal value As String)
            strAdjJob = value
        End Set
    End Property

    Public Property Remark() As String
        Get
            Return strRemark
        End Get
        Set(ByVal value As String)
            strRemark = value
        End Set
    End Property

    Public Property Demerit() As String
        Get
            Return strDemerit
        End Get
        Set(ByVal value As String)
            strDemerit = value
        End Set
    End Property

    Public Property DemeritCP() As String
        Get
            Return strDemeritCP
        End Get
        Set(ByVal value As String)
            strDemeritCP = value
        End Set
    End Property

    Public Property PremiumML() As String
        Get
            Return strPremiumML
        End Get
        Set(ByVal value As String)
            strPremiumML = value
        End Set
    End Property

    Public Property PremiumPL() As String
        Get
            Return strPremiumPL
        End Get
        Set(ByVal value As String)
            strPremiumPL = value
        End Set
    End Property

    Public Property InsertUpdate() As String
        Get
            Return bInsertUpdate
        End Get
        Set(ByVal value As String)
            bInsertUpdate = value
        End Set
    End Property

    Public Sub New()

    End Sub

    Public Sub New(ByVal strFileName As String, ByVal strUserID As String, ByVal strTchId As String, _
                   ByVal strAdjJob As String, ByVal strRemark As String, ByVal strDemerit As String, _
                   ByVal strDemeritCP As String, ByVal strPremiumML As String, ByVal strPremiumPL As String, ByVal bInsertUpdate As String)

        Me.TchID = strTchId
        Me.AdjJob = strAdjJob
        Me.Remark = strRemark
        Me.Demerit = strDemerit
        Me.DemeritCP = strDemeritCP
        Me.PremiumML = strPremiumML
        Me.PremiumPL = strPremiumPL
        Me.FileName = strFileName
        Me.UserID = strUserID
        Me.bInsertUpdate = bInsertUpdate
    End Sub
#End Region

#Region "Checking"
   
    Public Function GetExistFINCUPLOAD_FIL(ByVal FileName As String, ByVal TechnicianID As String, ByVal UserID As String) As DataSet
        Dim checkConnection As SqlConnection = Nothing
        Dim checkConn As SqlConnection = Nothing
        checkConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        checkConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        Dim trans As SqlTransaction = Nothing
        Dim fstrSpName As String = "BB_FINCUPLOAD_FIL_Sel"

        Try
            trans = checkConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(3) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(checkConn, fstrSpName)
            Param(0).Value = FileName
            Param(1).Value = TechnicianID
            Param(2).Value = UserID

            Dim ds As DataSet = SqlHelper.ExecuteDataset(checkConn, CommandType.StoredProcedure, fstrSpName, Param)
            GetExistFINCUPLOAD_FIL = ds

            checkConnection.Dispose()
            checkConn.Dispose()
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(UserID, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsUpload.vb")
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(UserID, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsUpload.vb")
        End Try
    End Function

    Public Function GetExistFIV1_CR_UPLOAD(ByVal ServiceBillNo As String, ByVal ModBy As String) As DataSet
        Dim checkConnection As SqlConnection = Nothing
        Dim checkConn As SqlConnection = Nothing
        checkConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        checkConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        Dim trans As SqlTransaction = Nothing
        Dim fstrSpName As String = "BB_FIV1_CR_UPLOAD_SelByFIV1_CR_SVBIL"

        Try
            trans = checkConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(1) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(checkConn, fstrSpName)
            Param(0).Value = ServiceBillNo

            Dim ds As DataSet = SqlHelper.ExecuteDataset(checkConn, CommandType.StoredProcedure, fstrSpName, Param)
            GetExistFIV1_CR_UPLOAD = ds

            checkConnection.Dispose()
            checkConn.Dispose()
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(ModBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsUpload.vb")
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(ModBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsUpload.vb")
        End Try
    End Function
#End Region

#Region "Insert"
    Public Function Insert(ByVal ExcelList As clsUploadIncentive, ByVal ModBy As String) As Integer
        
        'Dim insConnection As SqlConnection = Nothing
        ''Dim insConn As SqlConnection = Nothing
        'insConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        ''insConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        'Dim myCommand As SqlCommand = insConnection.CreateCommand()
        'Dim trans As SqlTransaction = Nothing
        'Dim fstrSpName As String = "BB_FINCUPLOAD_ADD"
        'Dim iInsert As Integer = 0
        'myCommand.CommandText = fstrSpName
        'myCommand.CommandType = CommandType.StoredProcedure

        'myCommand.Connection = insConnection
        'myCommand.Transaction = trans
        'Try
        '    trans = insConnection.BeginTransaction(IsolationLevel.ReadCommitted, "Transaction")

        '    For i As Integer = 0 To ExcelList.IncentiveList.Count - 1
        '        myCommand.Parameters.Clear()
        '        myCommand.Parameters.AddWithValue("@FINCUPLOAD_FILENAME", ExcelList.IncentiveList.Item(i).strFileName)
        '        myCommand.Parameters.AddWithValue("@FINCUPLOAD_USERID", ExcelList.IncentiveList.Item(i).strUserId)
        '        myCommand.Parameters.AddWithValue("@FINCUPLOAD_TCHID", ExcelList.IncentiveList.Item(i).TchID)
        '        myCommand.Parameters.AddWithValue("@FINCUPLOAD_ADJJOB", ExcelList.IncentiveList.Item(i).strAdjJob)
        '        myCommand.Parameters.AddWithValue("@FINCUPLOAD_REMARK", ExcelList.IncentiveList.Item(i).strRemark)
        '        myCommand.Parameters.AddWithValue("@FINCUPLOAD_DEMERIT", ExcelList.IncentiveList.Item(i).strDemerit)
        '        myCommand.Parameters.AddWithValue("@FINCUPLOAD_DEMERIT_CP", ExcelList.IncentiveList.Item(i).strDemeritCP)
        '        myCommand.Parameters.AddWithValue("@FINCUPLOAD_PREMIUM_ML", ExcelList.IncentiveList.Item(i).strPremiumML)
        '        myCommand.Parameters.AddWithValue("@FINCUPLOAD_PREMIUM_PL", ExcelList.IncentiveList.Item(i).strPremiumPL)

        '        iInsert = myCommand.ExecuteNonQuery()
        '        'myCommand.ExecuteNonQuery(insConnection, CommandType.StoredProcedure, fstrSpName, Param)
        '    Next


        'Catch ex As Exception
        '    trans.Rollback("Transaction")
        '    Dim ErrorLog As ArrayList = New ArrayList
        '    ErrorLog.Add("Error").ToString()
        '    ErrorLog.Add(ex.Message).ToString()
        '    Dim WriteErrLog As New clsLogFile()
        '    WriteErrLog.ErrorLog(ModBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsUpload.vb")
        '    Return -1
        'Catch ex As SqlException
        '    trans.Rollback()
        '    Dim ErrorLog As ArrayList = New ArrayList
        '    ErrorLog.Add("Error").ToString()
        '    ErrorLog.Add(ex.Number).ToString()
        '    Dim WriteErrLog As New clsLogFile()
        '    WriteErrLog.ErrorLog(ModBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsUpload.vb")
        '    Return -1
        'End Try

        'trans.Commit()
        'insConnection.Dispose()
        'Return 0


        Dim insConnection As SqlConnection = Nothing
        insConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        Dim myCommand As SqlCommand = insConnection.CreateCommand()
        Dim transaction As SqlTransaction

        transaction = insConnection.BeginTransaction(IsolationLevel.ReadCommitted)
        myCommand.Connection = insConnection
        myCommand.Transaction = transaction
        myCommand.CommandText = "BB_FINCUPLOAD_ADD"
        myCommand.CommandType = CommandType.StoredProcedure
        Dim iInsert As Integer = 0
        Try

            For i As Integer = 0 To ExcelList.IncentiveList.Count - 1
                myCommand.Parameters.Clear()
                myCommand.Parameters.AddWithValue("@FINCUPLOAD_FILENAME", ExcelList.IncentiveList.Item(i).strFileName)
                myCommand.Parameters.AddWithValue("@FINCUPLOAD_USERID", ExcelList.IncentiveList.Item(i).strUserId)
                myCommand.Parameters.AddWithValue("@FINCUPLOAD_TCHID", ExcelList.IncentiveList.Item(i).TchID)
                myCommand.Parameters.AddWithValue("@FINCUPLOAD_ADJJOB", ExcelList.IncentiveList.Item(i).strAdjJob)
                myCommand.Parameters.AddWithValue("@FINCUPLOAD_REMARK", ExcelList.IncentiveList.Item(i).strRemark)
                myCommand.Parameters.AddWithValue("@FINCUPLOAD_DEMERIT", ExcelList.IncentiveList.Item(i).strDemerit)
                myCommand.Parameters.AddWithValue("@FINCUPLOAD_DEMERIT_CP", ExcelList.IncentiveList.Item(i).strDemeritCP)
                myCommand.Parameters.AddWithValue("@FINCUPLOAD_PREMIUM_ML", ExcelList.IncentiveList.Item(i).strPremiumML)
                myCommand.Parameters.AddWithValue("@FINCUPLOAD_PREMIUM_PL", ExcelList.IncentiveList.Item(i).strPremiumPL)

                iInsert = myCommand.ExecuteNonQuery()
                'myCommand.ExecuteNonQuery(insConnection, CommandType.StoredProcedure, fstrSpName, Param)
            Next

            transaction.Commit()

        Catch e As Exception
            Try
                transaction.Rollback()
            Catch ex As SqlException
                Dim ErrorLog As ArrayList = New ArrayList
                ErrorLog.Add("Error").ToString()
                ErrorLog.Add(ex.Message).ToString()
                Dim WriteErrLog As New clsLogFile()
                WriteErrLog.ErrorLog(ModBy, ErrorLog.Item(1).ToString, "", WriteErrLog.ADD, "clsUpload.vb")
                Return -1
            End Try
            Throw New Exception(e.Message)
        Finally
            insConnection.Close()

        End Try
        Return 0


    End Function
    Public Function Insert_New(ByVal ExcelList As clsUploadIncentive, ByVal ModBy As String) As Integer

        Dim insConn As SqlConnection = Nothing
        insConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        Dim fstrSpName As String = "BB_FINCUPLOAD_ADD"
        Dim iInsert As Integer = 0

        Dim conn As SqlConnection
        Dim cmd As SqlCommand
        Dim tran As SqlTransaction
        ' Create a New Connection
        conn = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        ' Open the Connection
        conn.Open()
        tran = conn.BeginTransaction
        ' Set the Transaction within which the Commands execute
        Try
            For i As Integer = 0 To ExcelList.IncentiveList.Count - 1
                Dim Param() As SqlParameter = New SqlParameter(4) {}
                Param = SqlHelperParameterCache.GetSpParameterSet(insConn, fstrSpName)
                Param(0).Value = ExcelList.IncentiveList.Item(i).FileName
                Param(1).Value = ExcelList.IncentiveList.Item(i).UserID
                Param(2).Value = ExcelList.IncentiveList.Item(i).TchID
                Param(3).Value = ExcelList.IncentiveList.Item(i).AdjJob
                Param(4).Value = ExcelList.IncentiveList.Item(i).Remark
                Param(5).Value = ExcelList.IncentiveList.Item(i).Demerit
                Param(6).Value = ExcelList.IncentiveList.Item(i).DemeritCP
                Param(7).Value = ExcelList.IncentiveList.Item(i).PremiumML
                Param(8).Value = ExcelList.IncentiveList.Item(i).PremiumPL

                iInsert = SqlHelper.ExecuteNonQuery(insConn, CommandType.StoredProcedure, fstrSpName, Param)
            Next

            ' Commit the Transaction
            tran.Commit()
        Catch
            ' Rollback the Transaction
            tran.Rollback()
        Finally
            ' Cleanup Code
            ' Close the Connection
            conn.Close()
        End Try

    End Function

      
#End Region

#Region "Methods"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)
        connection.Open()
        Return connection
    End Function
#End Region
End Class