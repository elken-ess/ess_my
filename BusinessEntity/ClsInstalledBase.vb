
Imports SQLDataAccess
Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Web
Imports System.Data

#Region " Module Amment Hisotry "
'Description     :   installed base information
'History         :  
'Modified Date   :  2006-04-10
'Version         :  v1.0
'Author          :  ding
#End Region
Public Class ClsInstalledBase

#Region "Declaration"

    Private fstrmodelid As String 'model id
    Private fstrserialid As String ' serial number
    Private fnumroid As String 'RO id
    Private fstrCustid As String ' Customer ID
    Private fstrStatus As String ' installbase status
    Private fstCusPre As String ' Customer Prefix
    Private fstrContracted As String ' Contracted
    Private fstrFrseren As String ' Free Service Entitled
    Private fstrInstalledDate As System.DateTime  ' Installed Date
    Private fstrProducDate As System.DateTime 'Production Date
    Private fstrProducClass As String 'Product Class
    Private fstrSpName As String ' store procedure name
    Private fstrWarrExdate As System.DateTime  ' Warranty Expiry Date
    Private fstrTechID As String  'Technician ID
    Private fstrTechType As String 'Technician Type
    Private fstrSerCenterID As String 'Service Center ID
    Private fstrSerTypeID As String 'Service Type ID
    Private fstrFrSerRemiDate As System.DateTime  'Free Service Reminder Date
    Private fstrLastSerDate As System.DateTime  'Last Service Date
    Private fstrLastRemiDate As System.DateTime   'Last Reminder Date
    Private fstrRemarks As String 'Remarks
    Private fstrCreatedBy As String 'Created By
    Private fstrCreatedDate As System.DateTime 'Created Date
    Private fstrModifiedBy As String 'Modified By
    Private fstrModifiedDate As System.DateTime 'Modified Date
   
    Private fstrlogctrid As String 'country id
    Private fstrlogcmpid As String ' Company ID
    Private fstrlogsvcid As String ' service center id
    Private fstrlogrank As String 'user rank
    Private fstripadress As String 'login ip

    'set default value 
    Private fstrrelfg As String 'mrou_reflg
    Private fstrcarem As String 'mrou_carem
    Private fstrncadate As System.DateTime  'mrou_ncadt
    Private fstrnsvdate As System.DateTime  'mrou_nsvdt

    Private fstrcustname As String 'customer name
    Private fstrcusttelpho As String 'customer telpho
    Private fstrareaid As String 'area id

#End Region

#Region "Properties"

    Public Property RoID() As String
        Get
            Return fnumroid
        End Get
        Set(ByVal Value As String)
            fnumroid = Value
        End Set
    End Property
    Public Property ModelID() As String
        Get
            Return fstrmodelid
        End Get
        Set(ByVal Value As String)
            fstrmodelid = Value
        End Set
    End Property

    Public Property SerialNo() As String
        Get
            Return fstrserialid
        End Get
        Set(ByVal Value As String)
            fstrserialid = Value
        End Set
    End Property


    Public Property CustomerID() As String
        Get
            Return fstrCustid
        End Get
        Set(ByVal Value As String)
            fstrCustid = Value
        End Set
    End Property

    Public Property Status() As String
        Get
            Return fstrStatus
        End Get
        Set(ByVal Value As String)
            fstrStatus = Value
        End Set
    End Property
    Public Property CustomerPre() As String
        Get
            Return fstCusPre
        End Get
        Set(ByVal Value As String)
            fstCusPre = Value
        End Set
    End Property
    Public Property Contracted() As String
        Get
            Return fstrContracted
        End Get
        Set(ByVal Value As String)
            fstrContracted = Value
        End Set
    End Property
    Public Property Freeserviceen() As String
        Get
            Return fstrFrseren
        End Get
        Set(ByVal Value As String)
            fstrFrseren = Value
        End Set
    End Property

    Public Property Installdate() As String
        Get
            Return fstrInstalledDate
        End Get
        Set(ByVal Value As String)
            fstrInstalledDate = Value
        End Set
    End Property

    Public Property ProductionDate() As String
        Get
            Return fstrProducDate
        End Get
        Set(ByVal Value As String)
            fstrProducDate = Value
        End Set
    End Property
    Public Property ProductClass() As String
        Get
            Return fstrProducClass
        End Get
        Set(ByVal Value As String)
            fstrProducClass = Value
        End Set
    End Property
    Public Property WarrantyExDate() As String
        Get
            Return fstrWarrExdate
        End Get
        Set(ByVal Value As String)
            fstrWarrExdate = Value
        End Set
    End Property
    Public Property TechID() As String
        Get
            Return fstrTechID
        End Get
        Set(ByVal Value As String)
            fstrTechID = Value
        End Set
    End Property
    Public Property TechType() As String
        Get
            Return fstrTechType
        End Get
        Set(ByVal Value As String)
            fstrTechType = Value
        End Set
    End Property
    Public Property SerCenterID() As String
        Get
            Return fstrSerCenterID
        End Get
        Set(ByVal Value As String)
            fstrSerCenterID = Value
        End Set
    End Property
    Public Property SerTypeID() As String
        Get
            Return fstrSerTypeID
        End Get
        Set(ByVal Value As String)
            fstrSerTypeID = Value
        End Set
    End Property

    Public Property FrSerRemiDate() As String
        Get
            Return fstrFrSerRemiDate
        End Get
        Set(ByVal Value As String)
            fstrFrSerRemiDate = Value
        End Set
    End Property

    Public Property LastSerDate() As String
        Get
            Return fstrLastSerDate
        End Get
        Set(ByVal Value As String)
            fstrLastSerDate = Value
        End Set
    End Property
    Public Property LastRemiDate() As String
        Get
            Return fstrLastRemiDate
        End Get
        Set(ByVal Value As String)
            fstrLastRemiDate = Value
        End Set
    End Property
    Public Property Remarks() As String
        Get
            Return fstrRemarks
        End Get
        Set(ByVal Value As String)
            fstrRemarks = Value
        End Set
    End Property
    Public Property CreatedBy() As String
        Get
            Return fstrCreatedBy
        End Get
        Set(ByVal Value As String)
            fstrCreatedBy = Value
        End Set
    End Property
    Public Property CreatedDate() As String
        Get
            Return fstrCreatedDate
        End Get
        Set(ByVal Value As String)
            fstrCreatedDate = Value
        End Set
    End Property
    Public Property ModifiedBy() As String
        Get
            Return fstrModifiedBy
        End Get
        Set(ByVal Value As String)
            fstrModifiedBy = Value
        End Set
    End Property
    Public Property ModifiedDate() As String
        Get
            Return fstrModifiedDate
        End Get
        Set(ByVal Value As String)
            fstrModifiedDate = Value
        End Set
    End Property

    Public Property logctrid() As String
        Get
            Return fstrlogctrid
        End Get
        Set(ByVal Value As String)
            fstrlogctrid = Value
        End Set
    End Property
    Public Property logcmpid() As String
        Get
            Return fstrlogcmpid
        End Get
        Set(ByVal Value As String)
            fstrlogcmpid = Value
        End Set
    End Property
    Public Property logsvcid() As String
        Get
            Return fstrlogsvcid
        End Get
        Set(ByVal Value As String)
            fstrlogsvcid = Value
        End Set
    End Property

    Public Property logrank() As String
        Get
            Return fstrlogrank
        End Get
        Set(ByVal Value As String)
            fstrlogrank = Value
        End Set
    End Property
    Public Property ipadress() As String
        Get
            Return fstripadress
        End Get
        Set(ByVal Value As String)
            fstripadress = Value
        End Set
    End Property
    Public Property relfg() As String
        Get
            Return fstrrelfg
        End Get
        Set(ByVal Value As String)
            fstrrelfg = Value
        End Set
    End Property
    Public Property carem() As String
        Get
            Return fstrcarem
        End Get
        Set(ByVal Value As String)
            fstrcarem = Value
        End Set
    End Property
    Public Property ncadate() As String
        Get
            Return fstrncadate
        End Get
        Set(ByVal Value As String)
            fstrncadate = Value
        End Set
    End Property
    Public Property nsvdate() As String
        Get
            Return fstrnsvdate
        End Get
        Set(ByVal Value As String)
            fstrnsvdate = Value
        End Set
    End Property
    Public Property custname() As String
        Get
            Return fstrcustname
        End Get
        Set(ByVal Value As String)
            fstrcustname = Value
        End Set
    End Property
    Public Property custtelpho() As String
        Get
            Return fstrcusttelpho
        End Get
        Set(ByVal Value As String)
            fstrcusttelpho = Value
        End Set
    End Property
    Public Property areaid() As String
        Get
            Return fstrareaid
        End Get
        Set(ByVal Value As String)
            fstrareaid = Value
        End Set
    End Property

#End Region


#Region "Methods"

#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)

        connection.Open()

        Return connection
    End Function
#End Region

#Region "Insert installbase"

    Public Function Insert() As Integer

        Dim viewConnection As SqlConnection = Nothing
        Dim viewConn As SqlConnection = Nothing
        viewConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        viewConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASROUM_Add"
        Try
            trans = viewConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(24) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(viewConn, fstrSpName)
            Param(0).Value = Trim(fnumroid)
            Param(1).Value = Trim(fstrmodelid)
            Param(2).Value = Trim(fstrserialid)
            Param(3).Value = Trim(fstrCustid)
            Param(4).Value = Trim(fstCusPre)
            Param(5).Value = Trim(fstrContracted)
            Param(6).Value = Trim(fstrFrseren)
            Param(7).Value = Trim(fstrInstalledDate)
            Param(8).Value = Trim(fstrProducDate)
            Param(9).Value = Trim(fstrProducClass)
            Param(10).Value = Trim(fstrWarrExdate)
            Param(11).Value = Trim(fstrTechID)
            Param(12).Value = Trim(fstrTechType)
            Param(13).Value = Trim(fstrSerCenterID)
            Param(14).Value = Trim(fstrSerTypeID)
            Param(15).Value = Trim(fstrStatus)
            Param(16).Value = Trim(fstrRemarks)
            Param(17).Value = Trim(fstrCreatedBy) '创建者
            Param(18).Value = Trim(fstrModifiedBy) '最后修改者
            Param(19).Value = Trim(fstrFrSerRemiDate)

            Param(20).Value = Trim(fstrrelfg)
            Param(21).Value = Trim(fstrcarem)
            Param(22).Value = Trim(fstrncadate)
            Param(23).Value = Trim(fstrnsvdate)

            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(viewConn, CommandType.StoredProcedure, fstrSpName, Param)

            trans.Commit()

            viewConnection.Dispose()
            viewConn.Dispose()

            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "ClsInstalledBase.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "ClsInstalledBase.vb")
            Return -1
        Finally
           
        End Try

    End Function

#End Region


#Region "Update"

    Public Function Update() As Integer

        Dim viewConnection As SqlConnection = Nothing
        Dim viewConn As SqlConnection = Nothing
        viewConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        viewConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASROUM_Upd"
        Try
            trans = viewConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(21) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(viewConn, fstrSpName)
            Param(0).Value = Trim(fnumroid)
            Param(1).Value = Trim(fstrmodelid)
            Param(2).Value = Trim(fstrserialid)
            Param(3).Value = Trim(fstrCustid)
            Param(4).Value = Trim(fstCusPre)
            Param(5).Value = Trim(fstrContracted)
            Param(6).Value = Trim(fstrFrseren)
            Param(7).Value = Trim(fstrInstalledDate)
            Param(8).Value = Trim(fstrProducDate)
            Param(9).Value = Trim(fstrProducClass)
            Param(10).Value = Trim(fstrWarrExdate)
            Param(11).Value = Trim(fstrTechID)
            Param(12).Value = Trim(fstrTechType)
            Param(13).Value = Trim(fstrSerCenterID)
            Param(14).Value = Trim(fstrSerTypeID)
            Param(15).Value = Trim(fstrStatus)
            Param(16).Value = Trim(fstrRemarks)
            Param(17).Value = Trim(fstrModifiedBy) '最后修改者
            Param(18).Value = Trim(fstrFrSerRemiDate)
            Param(19).Value = Trim(fstrlogsvcid)
            Param(20).Value = Trim(fstripadress)

            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(viewConn, CommandType.StoredProcedure, fstrSpName, Param)
            trans.Commit()

            viewConnection.Dispose()
            viewConn.Dispose()
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "ClsInstalledBase.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "ClsInstalledBase.vb")
            Return -1
        Finally
            viewConnection.Close()
            viewConn.Close()
        End Try
    End Function


#End Region

#Region "search installbase"

    Public Function Getinstallbase() As DataSet

        Dim viewConnection As SqlConnection = Nothing
        Dim viewConn As SqlConnection = Nothing
        viewConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        viewConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASROUM_View"
        Try
            trans = viewConnection.BeginTransaction()

            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(10) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(viewConn, fstrSpName)
            Param(0).Value = Trim(fstrmodelid)
            Param(1).Value = Trim(fstrserialid)
            Param(2).Value = Trim(fstrStatus)

            Param(3).Value = Trim(fstrlogctrid)
            Param(4).Value = Trim(fstrlogcmpid)
            Param(5).Value = Trim(fstrlogsvcid)
            Param(6).Value = Trim(fstrlogrank)

            Param(7).Value = Trim(fstrcusttelpho)
            Param(8).Value = Trim(fstrcustname)
            Param(9).Value = Trim(fstrCustid)
            Param(10).Value = Trim(fstrareaid)

            Dim ds As DataSet = SqlHelper.ExecuteDataset(viewConn, CommandType.StoredProcedure, fstrSpName, Param)

            ' switch status id to status name for display
            'modify by ding 2006-4-12
            Dim count As Integer
            Dim statXmlTr As New clsXml
            Dim strPanm As String
            Dim strcontpar As String
            statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            count = 0
            For count = 0 To ds.Tables(0).Rows.Count - 1
                Dim statusid As String = ds.Tables(0).Rows(count).Item(5).ToString 'item(5) is status
                strPanm = statXmlTr.GetLabelName("StatusMessage", statusid)
                ds.Tables(0).Rows(count).Item(5) = strPanm
            Next
            Getinstallbase = ds
            viewConnection.Dispose()
            viewConn.Dispose()

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "ClsInstalledBase.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "ClsInstalledBase.vb")
        Finally
          
        End Try

    End Function
#End Region
#Region "Select installbase By ID"

    Public Function GetinstallbaseDetailsByRoID() As SqlDataReader
        Dim viewConnection As SqlConnection = Nothing
        Dim viewConn As SqlConnection = Nothing
        viewConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        viewConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASROUM_SelByID"
        Try
            trans = viewConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(1) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(viewConn, fstrSpName)
            Param(0).Value = Trim(fnumroid)

            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(viewConn, CommandType.StoredProcedure, fstrSpName, Param)
            viewConnection.Dispose()
            GetinstallbaseDetailsByRoID = dr

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            'Return ErrorLog

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            'Return ErrorLog
        End Try
    End Function
#End Region


#Region "select maxid "
    Public Function selmaxid() As Long

        Dim viewConnection As SqlConnection = Nothing
        Dim viewConn As SqlConnection = Nothing
        viewConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        viewConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASROUM_MAXid"
        Try
            trans = viewConnection.BeginTransaction()

            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(viewConn, CommandType.StoredProcedure, fstrSpName)
            Dim maxroid As Long

            While dr.Read()
                maxroid = dr.GetValue(0)
            End While
            dr.Close()
            viewConnection.Dispose()
            viewConn.Dispose()

            Return maxroid

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "ClsInstalledBase.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "ClsInstalledBase.vb")
        End Try

    End Function
#End Region
#Region "SELECT CUSTOMOR NAME "
    Public Function selcusname(ByVal custid As String, ByVal custpf As String) As SqlDataReader

        Dim viewConnection As SqlConnection = Nothing
        Dim viewConn As SqlConnection = Nothing
        viewConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        viewConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASCUST_SelByIDNAME"
        Try
            trans = viewConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(viewConn, fstrSpName)
            Param(0).Value = custid
            Param(1).Value = custpf

            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(viewConn, CommandType.StoredProcedure, fstrSpName, Param)
            viewConnection.Dispose()
            selcusname = dr
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "ClsInstalledBase.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "ClsInstalledBase.vb")
        End Try

    End Function
#End Region
#Region "confirm no duplicated record"
    Public Function GetDuplicatedinstall() As Integer
        Dim viewConnection As SqlConnection = Nothing
        Dim viewConn As SqlConnection = Nothing
        viewConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        viewConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASROUM_Check"
        Try
            trans = viewConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(3) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(viewConn, fstrSpName)
            Param(0).Value = Trim(fstrmodelid)
            Param(1).Value = Trim(fstrserialid)
            Param(2).Value = Trim(Me.fnumroid)
            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(viewConn, CommandType.StoredProcedure, fstrSpName, Param)
            If dr.Read() Then
                dr.Close()
                Return -1
            End If
            viewConnection.Dispose()
            viewConn.Dispose()

            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "ClsInstalledBase.vb")
            Return -1
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsInstalledBase.vb")
            Return -1
        Finally
            viewConnection.Close()
            viewConn.Close()
        End Try
    End Function
#End Region

#Region "Update LPFX_FIL ,SET RO's LPFX_RUNNO,修改前缀表中ROID的最大值"

    Public Function UpdatePfx() As Integer

        Dim viewConnection As SqlConnection = Nothing
        Dim viewConn As SqlConnection = Nothing
        viewConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        viewConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASROUM_PFXUpd"
        Try
            trans = viewConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(1) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(viewConn, fstrSpName)
            Param(0).Value = Trim(fnumroid)
            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(viewConn, CommandType.StoredProcedure, fstrSpName, Param)
            trans.Commit()

            viewConnection.Dispose()
            viewConn.Dispose()
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "ClsInstalledBase.vb")
            Return -1
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "ClsInstalledBase.vb")
            Return -1
        Finally
            viewConnection.Close()
            viewConn.Close()
        End Try
    End Function


#End Region


#Region "select model table's warrary days"

    Public Function GetWardays() As Integer

        Dim viewConnection As SqlConnection = Nothing
        Dim viewConn As SqlConnection = Nothing
        viewConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        viewConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASMOTY_SELWRDAYS"
        Try
            trans = viewConnection.BeginTransaction()

            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(viewConn, fstrSpName)
            Param(0).Value = Trim(fstrmodelid)
            Param(1).Value = Trim(fstrlogctrid)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(viewConn, CommandType.StoredProcedure, fstrSpName, Param)
            'Dim ds As DataSet = SqlHelper.ExecuteDataset(viewConn, CommandType.StoredProcedure, fstrSpName)

            Dim wdays As String = ds.Tables(0).Rows(0).Item(0).ToString
            Dim intday As Integer = Convert.ToInt16(wdays)

            trans.Commit()
            viewConnection.Dispose()
            viewConn.Dispose()
            Return intday

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsInstalledBase.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsInstalledBase.vb")
        Finally
            
        End Try

    End Function
#End Region
#Region "select FREE SERVICE REMINDER days"

    Public Function GetReminderdays() As Integer

        Dim viewConnection As SqlConnection = Nothing
        Dim viewConn As SqlConnection = Nothing
        viewConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        viewConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_FREESERVREMDAY_VIEW"
        Try
            trans = viewConnection.BeginTransaction()

            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(viewConn, fstrSpName)
            Param(0).Value = Trim(fstrSerTypeID)
            Param(1).Value = fstrlogctrid
            Dim ds As DataSet = SqlHelper.ExecuteDataset(viewConn, CommandType.StoredProcedure, fstrSpName, Param)


            Dim remdays As String = ds.Tables(0).Rows(0).Item(0).ToString
            Dim intday As Integer = Convert.ToInt16(remdays)


            viewConnection.Dispose()
            viewConn.Dispose()

            Return intday

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsInstalledBase.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsInstalledBase.vb")
        Finally
            viewConnection.Close()
            viewConn.Close()
        End Try

    End Function
#End Region
#Region "confirm no duplicated Roid"
    Public Function GetDuplicatedRo() As Integer
        Dim viewConnection As SqlConnection = Nothing
        Dim viewConn As SqlConnection = Nothing
        viewConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        viewConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASROUM_CheckRo"
        Try
            trans = viewConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(1) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(viewConn, fstrSpName)
            Param(0).Value = Trim(fnumroid)

            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(viewConn, CommandType.StoredProcedure, fstrSpName, Param)
            If dr.Read() Then
                dr.Close()
                viewConnection.Dispose()
                viewConn.Dispose()

                Return -1
            End If

            viewConnection.Dispose()
            viewConn.Dispose()

            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "ClsInstalledBase.vb")
            Return -1
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsInstalledBase.vb")
            Return -1
        Finally
            viewConnection.Close()
            viewConn.Close()
        End Try
    End Function
#End Region

#Region "DEL CHECK"
    Public Function Getroudel() As SqlDataReader
        Dim viewConnection As SqlConnection = Nothing
        Dim viewConn As SqlConnection = Nothing
        viewConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        viewConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASROUM_DELID"
        Try
            trans = viewConnection.BeginTransaction()
            Dim Param() As SqlParameter = New SqlParameter(1) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(viewConn, fstrSpName)
            Param(0).Value = Trim(fnumroid)
            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(viewConn, CommandType.StoredProcedure, fstrSpName, Param)

            viewConnection.Dispose()
            Return dr
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "ClsInstalledBase.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "ClsInstalledBase.vb")

        End Try
    End Function
#End Region

#Region "Get Area By Country"

    Public Function GetAreaByCountry(ByVal CtryID As String) As DataSet
        Dim myCn As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim myCmd As SqlDataAdapter = New SqlDataAdapter("BB_MASAREA_SELBYCTRY", myCn)

        myCmd.SelectCommand.CommandType = CommandType.StoredProcedure

        Dim prmCID As SqlParameter = New SqlParameter("@CtryID", SqlDbType.Char, 2)
        prmCID.Value = CtryID
        myCmd.SelectCommand.Parameters.Add(prmCID)

        Dim myDataset As New DataSet
        myCmd.Fill(myDataset)
        Return myDataset

    End Function

#End Region

#End Region


End Class
