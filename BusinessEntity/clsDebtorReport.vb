Imports SQLDataAccess
Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Imports System

Public Class clsDebtorReport
    Private strConn As String
#Region "Declaration"

    Private fCRDetail As DataSet
    Private fstrROUnitID As String
    Private fstrUserID As String
    Private fstrSOID As String
    Private fstrtechnician As String
    Private fstrModlty As String ' model type
    Private fstrSerialNo As String ' serial no
    Private fstrCustomerID As String
    Private fstrCustomerPrefix As String
    Private fstrCustomerName As String
    Private fstrSpName As String ' store procedure name
    Private fstrServiceBillNo As String
    Private fTransactionPf As String
    Private fTransactionNo As String
    Private fRefNo As String

    Private _reportType As String
    Private _fromSvcID As String
    Private _toSvcID As String
    Private _custType As String
    Private _startInvDate As String
    Private _endInvDate As String
    Private _ctryID As String
    Private _comID As String
    Private _userName As String
    Private _ageing As String
    Private _printlist As String
    Private _invlist As String

    Private _errorLog As ArrayList = New ArrayList
    Private _writeErrLog As New clsLogFile()
#End Region

#Region "Property"
    Public Property CRDetails() As DataSet
        Get
            Return fCRDetail

        End Get
        Set(ByVal value As DataSet)
            fCRDetail = value
        End Set
    End Property

    Public Property TransactionPrefix() As String
        Get
            Return fTransactionPf
        End Get
        Set(ByVal value As String)
            fTransactionPf = value
        End Set
    End Property

    Public Property TransactionNo() As String
        Get
            Return fTransactionNo
        End Get
        Set(ByVal value As String)
            fTransactionNo = value
        End Set
    End Property

    Public Property RefNo() As String
        Get
            Return fRefNo
        End Get
        Set(ByVal value As String)
            fRefNo = value
        End Set
    End Property

    Public Property ServiceBillNo()
        Get
            Return fstrServiceBillNo
        End Get
        Set(ByVal value)
            fstrServiceBillNo = value
        End Set
    End Property

    Public Property ROUnitID()
        Get
            Return fstrROUnitID
        End Get
        Set(ByVal value)
            fstrROUnitID = value
        End Set
    End Property
    Public Property UserID()
        Get
            Return fstrUserID
        End Get
        Set(ByVal value)
            fstrUserID = value
        End Set
    End Property

    Public Property SOID()
        Get
            Return fstrSOID
        End Get
        Set(ByVal value)
            fstrSOID = value
        End Set
    End Property

    Public Property CustomerName() As String
        Get
            Return fstrCustomerName
        End Get
        Set(ByVal value As String)
            fstrCustomerName = value
        End Set
    End Property

    Public Property Modlty() As String
        Get
            Return fstrModlty
        End Get
        Set(ByVal value As String)
            fstrModlty = value
        End Set
    End Property
    Public Property SerialNo() As String
        Get
            Return fstrSerialNo
        End Get
        Set(ByVal value As String)
            fstrSerialNo = value
        End Set
    End Property

    Public Property CustomerID() As String
        Get
            Return fstrCustomerID
        End Get
        Set(ByVal value As String)
            fstrCustomerID = value
        End Set
    End Property
    Public Property CustomerPrefix() As String
        Get
            Return fstrCustomerPrefix
        End Get
        Set(ByVal value As String)
            fstrCustomerPrefix = value
        End Set
    End Property

    Public Property SpName() As String
        Get
            Return fstrSpName
        End Get
        Set(ByVal value As String)
            fstrSpName = value
        End Set
    End Property

    Public Property Technician() As String
        Get
            Return fstrtechnician
        End Get
        Set(ByVal value As String)
            fstrtechnician = value
        End Set
    End Property

    Public Property FromSvcID() As String
        Get
            Return _fromSvcID
        End Get
        Set(ByVal value As String)
            _fromSvcID = value
        End Set
    End Property

    Public Property ToSvcID() As String
        Get
            Return _toSvcID
        End Get
        Set(ByVal value As String)
            _toSvcID = value
        End Set
    End Property


    Public Property CustType() As String
        Get
            Return _custType
        End Get
        Set(ByVal value As String)
            _custType = value
        End Set
    End Property


    Public Property StartInvDate() As String
        Get
            Return _startInvDate
        End Get
        Set(ByVal value As String)
            _startInvDate = value
        End Set
    End Property

    Public Property EndInvDate() As String
        Get
            Return _endInvDate
        End Get
        Set(ByVal value As String)
            _endInvDate = value
        End Set
    End Property


    Public Property CtryID() As String
        Get
            Return _ctryID
        End Get
        Set(ByVal value As String)
            _ctryID = value
        End Set
    End Property

    Public Property ComID() As String
        Get
            Return _comID
        End Get
        Set(ByVal value As String)
            _comID = value
        End Set
    End Property

    Public Property UserName() As String
        Get
            Return _userName
        End Get
        Set(ByVal value As String)
            _userName = value
        End Set
    End Property

    Public Property ReportType() As String
        Get
            Return _reportType
        End Get
        Set(ByVal value As String)
            _reportType = value
        End Set
    End Property

    Public Property Ageing() As String
        Get
            Return _ageing
        End Get
        Set(ByVal value As String)
            _ageing = value
        End Set
    End Property

    Public Property PrintList() As String
        Get
            Return _printlist
        End Get
        Set(ByVal value As String)
            _printlist = value
        End Set
    End Property
    Public Property InvList() As String
        Get
            Return _invlist
        End Get
        Set(ByVal value As String)
            _invlist = value
        End Set
    End Property

#End Region

#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)

        connection.Open()

        Return connection
    End Function
#End Region

#Region "search"
    Public Function SelectAllPaymentCollection() As DataSet
        Dim selConn As SqlConnection = Nothing
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        fstrSpName = "BB_FNCUSCR_View"
        Dim ds As DataSet = New DataSet()
        Try
            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(16) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrSOID)
            Param(1).Value = Trim(fstrCustomerName)
            Param(3).Value = Trim(fstrSerialNo)
            Param(4).Value = Trim(fstrUserID)
            Param(6).Value = Trim(_startInvDate)
            Param(7).Value = Trim(_endInvDate)
            Param(8).Value = Trim(fstrCustomerPrefix)
            Param(9).Value = Trim(fstrCustomerID)
            Param(12).Value = Trim(fstrServiceBillNo)
            Param(13).Value = ""
            Param(14).Value = Trim(fstrtechnician)
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            SelectAllPaymentCollection = ds

            selConn.Dispose()
        Catch ex As SqlException
        Finally
            selConn.Close()
        End Try
        Return ds
    End Function
#End Region

    Public Function GetDebtorReport() As DataSet
        Dim selConn As SqlConnection = Nothing
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim ds As New DataSet
        Dim storedProc As String

        Select Case _reportType
            Case "outstanding"
                storedProc = "BB_RPTDEBTOR_VIEWOUTSTANDING"
            Case "ageing"
                storedProc = "BB_RPTDEBTOR_VIEWAGEING"
            Case "debtor"
                storedProc = "BB_RPTDEBTOR_VIEWAGEING_PRINT"
            Case Else
                Throw New ArgumentException("Report Type is not valid")
        End Select

        Try
            Dim param() As SqlParameter
            param = SqlHelperParameterCache.GetSpParameterSet(selConn, storedProc)
            param(0).Value = _startInvDate
            param(1).Value = _endInvDate
            param(2).Value = _custType
            param(3).Value = _toSvcID
            param(4).Value = _fromSvcID

            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, storedProc, param)
        Catch ex As Exception
            _errorLog.Add("Error").ToString()
            _errorLog.Add(ex.Message).ToString()
            _writeErrLog.ErrorLog(UserName, _errorLog.Item(1).ToString, storedProc, _writeErrLog.SELE, "ClsDebtorReport.vb")
            Return New DataSet()
        End Try

        Return ds

    End Function

    Public Function getDebtorListbyAgeing() As DataSet
        Dim selConn As SqlConnection = Nothing
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        fstrSpName = "BB_RPTDEBTOR_VIEWAGEING_BYAGEING"
        Dim ds As DataSet = New DataSet()
        Try
            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(_custType)
            Param(1).Value = Trim(_ageing)
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            getDebtorListbyAgeing = ds

            selConn.Dispose()
        Catch ex As SqlException
        Finally
            selConn.Close()
        End Try
        Return ds

    End Function

    Public Function getDebtorReminderLetterPrint() As DataSet
        Dim selConn As SqlConnection = Nothing
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        fstrSpName = "BB_RPTDEBTOR_VIEWAGEING_PRINT"
        Dim ds As DataSet = New DataSet()
        Try
            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(_printlist)
            Param(1).Value = Trim(_ageing)
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            getDebtorReminderLetterPrint = ds

            selConn.Dispose()
        Catch ex As SqlException
        Finally
            selConn.Close()
        End Try
        Return ds

    End Function
    Public Function getDebtorFinalReminderLetterPrint() As DataSet
        Dim selConn As SqlConnection = Nothing
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        fstrSpName = "BB_RPTDEBTOR_VIEWAGEING_PRINT_FINAL"
        Dim ds As DataSet = New DataSet()
        Try
            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(_printlist)
            Param(1).Value = Trim(_ageing)
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            getDebtorFinalReminderLetterPrint = ds

            selConn.Dispose()
        Catch ex As SqlException
        Finally
            selConn.Close()
        End Try
        Return ds

    End Function

    Public Function getDebtorReminderLetterPrintAccSum() As DataSet
        Dim selConn As SqlConnection = Nothing
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        fstrSpName = "BB_RPTDEBTOR_VIEWAGEING_PRINT_ACCSUM"
        Dim ds As DataSet = New DataSet()
        Try
            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(_invlist)
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            getDebtorReminderLetterPrintAccSum = ds

            selConn.Dispose()
        Catch ex As SqlException
        Finally
            selConn.Close()
        End Try
        Return ds

    End Function

End Class
