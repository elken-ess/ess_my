Imports SQLDataAccess
Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Web
Imports System
Imports System.Data



#Region " Module Amment Hisotry "
'Description     :  the SalTaxID entity
'History         :  
'Modified Date   :  2006-04-10
'Version         :  v1.0
'Author          :  CM


#End Region

Public Class clsSalTaxID
    Private strConn As String

#Region "Declaration"
    Private fstrSalTaxsSetUpID As Integer
    Private fstrSalTaxID As String ' SalTaxID
    Private fstrSalTaxName As String ' SalTax name
    Private fstrSalTaxAlternateName As String ' alternate Price  name
    Private fstrSalTaxStatus As String 'SalTax status
    Private fdobTaxPercentage As Decimal  ' TaxPercentagey
    Private fstrCountryID As String ' country id
    Private fdateEffectiveDate As System.DateTime  'EffectiveDate 
    Private fdateObsoleteDate As System.DateTime  'ObsoleteDate
    Private fstrCreatedBy As String ' CreatedBy
    Private fdateCreatedDate As System.DateTime  ' CreatedDate 
    Private fstrModifiedBy As String 'ModifiedBy
    Private fdateModifiedDate As System.DateTime  'ModifiedDate
    Private fstrSpName As String ' store  procedure name
    Private fdcrdate As System.DateTime  ' store  procedure name
    Private fstrrank As String ' 
    Private fstrsvcid As String
    Private fstrip As String

#End Region

#Region "Properties"
   
    Public Property SalTaxSetUpID() As Integer
        Get
            Return fstrSalTaxsSetUpID
        End Get
        Set(ByVal Value As Integer)
            fstrSalTaxsSetUpID = Value
        End Set
    End Property

    Public Property SalTaxID() As String
        Get
            Return fstrSalTaxID
        End Get
        Set(ByVal Value As String)
            fstrSalTaxID = Value
        End Set
    End Property

    Public Property SalTaxName() As String
        Get
            Return fstrSalTaxName
        End Get
        Set(ByVal Value As String)
            fstrSalTaxName = Value
        End Set
    End Property


    Public Property SalTaxAlternateName() As String
        Get
            Return fstrSalTaxAlternateName
        End Get
        Set(ByVal Value As String)
            fstrSalTaxAlternateName = Value
        End Set
    End Property

    Public Property SalTaxStatus() As String
        Get
            Return fstrSalTaxStatus
        End Get
        Set(ByVal Value As String)
            fstrSalTaxStatus = Value
        End Set
    End Property
    Public Property CountryID() As String
        Get
            Return fstrCountryID
        End Get
        Set(ByVal Value As String)
            fstrCountryID = Value
        End Set
    End Property

    Public Property TaxPercentage() As Decimal

        Get
            Return fdobTaxPercentage
        End Get
        Set(ByVal Value As Decimal)
            fdobTaxPercentage = Value
        End Set
    End Property
    Public Property EffectiveDate() As String
        Get
            Return fdateEffectiveDate
        End Get
        Set(ByVal Value As String)
            fdateEffectiveDate = Value
        End Set
    End Property

    Public Property ObsoleteDate() As String
        Get
            Return fdateObsoleteDate
        End Get
        Set(ByVal Value As String)
            fdateObsoleteDate = Value
        End Set
    End Property

    Public Property CreatedBy() As String
        Get
            Return fstrCreatedBy
        End Get
        Set(ByVal Value As String)
            fstrCreatedBy = Value
        End Set
    End Property
    Public Property crdate() As System.DateTime
        Get
            Return fdcrdate
        End Get
        Set(ByVal Value As System.DateTime)
            fdcrdate = Value
        End Set
    End Property
    Public Property CreatedDate() As String
        Get
            Return fdateCreatedDate
        End Get
        Set(ByVal Value As String)
            fdateCreatedDate = Value
        End Set
    End Property
    Public Property ModifiedBy() As String
        Get
            Return fstrModifiedBy
        End Get
        Set(ByVal Value As String)
            fstrModifiedBy = Value
        End Set
    End Property
    Public Property ModifiedDate() As String
        Get
            Return fdateModifiedDate
        End Get
        Set(ByVal Value As String)
            fdateModifiedDate = Value
        End Set
    End Property

    Public Property rank() As String
        Get
            Return fstrrank
        End Get
        Set(ByVal Value As String)
            fstrrank = Value
        End Set
    End Property
    Public Property svcid() As String
        Get
            Return fstrsvcid
        End Get
        Set(ByVal Value As String)
            fstrsvcid = Value
        End Set
    End Property
    Public Property ip() As String
        Get
            Return fstrip
        End Get
        Set(ByVal Value As String)
            fstrip = Value
        End Set
    End Property




#End Region

#Region "Methods"

#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)

        connection.Open()

        Return connection
    End Function
#End Region

#Region "Insert"

    Public Function Insert() As Integer

        Dim insConnection As SqlConnection = Nothing
        Dim insConn As SqlConnection = Nothing
        insConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        insConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASSTAX_Add"
        Try
            trans = insConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(8) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(insConn, fstrSpName)
            Param(0).Value = Trim(fstrSalTaxsSetUpID)
            Param(1).Value = Trim(fstrSalTaxID)
            Param(2).Value = Trim(fstrSalTaxName)
            Param(3).Value = Trim(fstrSalTaxAlternateName)
            Param(4).Value = Trim(fstrSalTaxStatus)
            Param(5).Value = Trim(fstrCountryID)
            Param(6).Value = Trim(fstrCreatedBy)
            Param(7).Value = Trim(fstrModifiedBy)


            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(insConn, CommandType.StoredProcedure, fstrSpName, Param)
            trans.Commit()
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsSalTax.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsSalTax.vb")
            Return -1
        End Try

    End Function

#End Region
#Region "Insertdetail"

    Public Function Insertdetail() As Integer

        Dim insConnection As SqlConnection = Nothing
        Dim insConn As SqlConnection = Nothing
        insConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        insConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASSTAX_Adddetail"
        Try
            trans = insConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(6) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(insConn, fstrSpName)
            Param(0).Value = Trim(fstrSalTaxsSetUpID)
            Param(1).Value = Trim(fdateEffectiveDate)
            Param(2).Value = Trim(fdobTaxPercentage)
            Param(3).Value = Trim(fstrSalTaxStatus)
            Param(4).Value = Trim(fstrCreatedBy)
            Param(5).Value = Trim(fstrModifiedBy)


            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(insConn, CommandType.StoredProcedure, fstrSpName, Param)
            trans.Commit()
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsSalTax.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsSalTax.vb")
            Return -1
        End Try

    End Function

#End Region

#Region "Update"

    Public Function Update() As Integer

        Dim UpdConnection As SqlConnection = Nothing
        Dim UpdConn As SqlConnection = Nothing
        UpdConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        UpdConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASSTAX_Upd"
        Try
            trans = UpdConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(9) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(UpdConn, fstrSpName)
            Param(0).Value = Trim(fstrSalTaxsSetUpID)
            Param(1).Value = Trim(fstrSalTaxID)
            Param(2).Value = Trim(fstrSalTaxName)
            Param(3).Value = Trim(fstrSalTaxAlternateName)
            Param(4).Value = Trim(fstrSalTaxStatus)
            Param(5).Value = Trim(fstrCountryID)
            Param(6).Value = Trim(fstrModifiedBy)
            Param(7).Value = Trim(fstrsvcid)
            Param(8).Value = Trim(fstrip)


           
            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(UpdConn, CommandType.StoredProcedure, fstrSpName, Param)
            
            trans.Commit()
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "clsSalTax.vb")
            ' Return ErrorLog

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "clsSalTax.vb")
            'Return ErrorLog
        End Try

    End Function

#End Region

#Region "Updatedetail"

    Public Function Updatedetail() As Integer

        Dim UpdConnection As SqlConnection = Nothing
        Dim UpdConn As SqlConnection = Nothing
        UpdConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        UpdConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASSTAX_Upddetail"
        Try
            trans = UpdConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(8) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(UpdConn, fstrSpName)
            Param(0).Value = Trim(fstrSalTaxsSetUpID)
            Param(1).Value = Trim(fdobTaxPercentage)
            Param(2).Value = Trim(fdateEffectiveDate)
            Param(3).Value = Trim(fstrSalTaxStatus)
            Param(4).Value = Trim(fstrModifiedBy)
            Param(5).Value = Trim(fdcrdate)
            Param(6).Value = Trim(fstrsvcid)
            Param(7).Value = Trim(fstrip)
            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(UpdConn, CommandType.StoredProcedure, fstrSpName, Param)
            trans.Commit()
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "clsSalTax.vb")
            ' Return ErrorLog

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "clsSalTax.vb")
            'Return ErrorLog
        End Try

    End Function

#End Region

 
#Region "Select SalTaxID"

    Public Function GetSalTaxID() As DataSet

        Dim selConnection As SqlConnection = Nothing
        Dim selConn As SqlConnection = Nothing
        selConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASSTAX_View"
        Try
            trans = selConnection.BeginTransaction()

            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(5) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            'Param(0).Value = Trim(fstrSalTaxsSetUpID)
            Param(0).Value = Trim(fstrSalTaxID)
            Param(1).Value = Trim(fstrSalTaxName)
            Param(2).Value = Trim(fstrCountryID)
            Param(3).Value = Trim(fstrSalTaxStatus)
            Param(4).Value = Trim(fstrrank)

            Dim ds As DataSet = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            ' switch status id to status name for display
            Dim count As Integer
            Dim statXmlTr As New clsXml
            Dim strPanm As String
            statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            For count = 0 To ds.Tables(0).Rows.Count - 1
                Dim statusid As String = ds.Tables(0).Rows(count).Item(3).ToString 'item(3) is status
                strPanm = statXmlTr.GetLabelName("StatusMessage", statusid)
                ds.Tables(0).Rows(count).Item(3) = strPanm
            Next
            Return ds

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("wang", ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsSalTaxID.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsSalTaxID.vb")
        End Try

    End Function
#End Region
#Region "Select SalTaxID"

    Public Function GetSalTaxIDdetail() As DataSet

        Dim selConnection As SqlConnection = Nothing
        Dim selConn As SqlConnection = Nothing
        selConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASSTAX_Viewdetail"
        Try
            trans = selConnection.BeginTransaction()

            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrSalTaxsSetUpID)
            Param(1).Value = Trim(fstrSalTaxStatus)


            Dim ds As DataSet = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            ' switch status id to status name for display
            Dim count As Integer
            Dim statXmlTr As New clsXml
            Dim strPanm As String
            statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            For count = 0 To ds.Tables(0).Rows.Count - 1
                Dim statusid As String = ds.Tables(0).Rows(count).Item(3).ToString 'item(3) is status
                strPanm = statXmlTr.GetLabelName("StatusMessage", statusid)
                ds.Tables(0).Rows(count).Item(3) = strPanm
            Next
            Return ds

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsSalTaxID.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsSalTaxID.vb")
        End Try

    End Function
#End Region

#Region "Select SalTaxID By ID"

    Public Function GetSalTaxIDDetailsByID() As ArrayList
        Dim retnArray As ArrayList = New ArrayList()
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASSTAX_SELBYID"
        Try
            trans = sidConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(1) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstrSalTaxsSetUpID)
            'Param(1).Value = Trim(fstrSalTaxStatus)


            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            If dr.Read() Then
                Dim count As Integer
                For count = 0 To dr.FieldCount - 1
                    retnArray.Add(dr.GetValue(count).ToString())
                Next
            End If
            dr.Close()
            Return retnArray

        Catch ex As Exception
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsSalTaxID.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsSalTaxID.vb")
        End Try

    End Function
#End Region
#Region "Select SalTaxID By ID 1"

    Public Function GetSalTaxIDDetailsByID1() As ArrayList
        Dim retnArray As ArrayList = New ArrayList()
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASSTAX_SELBYID1"
        Try
            trans = sidConnection.BeginTransaction()
           

            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstrSalTaxsSetUpID)
            Param(1).Value = Trim(fdcrdate)

            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            If dr.Read() Then
                Dim count As Integer
                For count = 0 To dr.FieldCount - 1
                    retnArray.Add(dr.GetValue(count).ToString())
                Next
            End If
            dr.Close()
            Return retnArray


        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsSalTaxID.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsSalTaxID.vb")
        End Try

    End Function
#End Region

#Region "Select BY SalTaxName"

    Public Function GetSalTaxIDDetailsBySalTaxNM() As Integer

        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASSTAX_SelByName"
        Try
            trans = sidConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(4) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstrSalTaxsSetUpID)
            Param(1).Value = Trim(fstrSalTaxID)
            Param(2).Value = Trim(fstrSalTaxName)
            Param(3).Value = Trim(fstrCountryID)

            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            If dr.Read() Then
                dr.Close()
                Return -1
            End If
            Return 0

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsPriceID.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsPriceID.vb")
        End Try

    End Function
#End Region
#Region "confirm no duplicated id and name"
    Public Function GetDuplicatedSalTaxID() As Integer
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASSTAX_IFDUP"
        Try
            trans = sidConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(4) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstrSalTaxsSetUpID)
            Param(1).Value = Trim(fstrSalTaxName)
            Param(2).Value = Trim(fstrSalTaxID)
            Param(3).Value = Trim(fstrCountryID)

            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            If dr.Read() Then
                dr.Close()
                Return -1
            End If
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsSalTax.vb")
            Return -1
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsSalTax.vb")
            Return -1
        Finally
            sidConnection.Close()
            sidConn.Close()
        End Try
    End Function
#End Region
    Public Function GetReturnSALTAXIDDetailsByID() As ArrayList
        Dim retnArray As ArrayList = New ArrayList()
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASSTAX_SELRETURN"
        Try
            trans = sidConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(1) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstrSalTaxsSetUpID)

            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            'Return dr
            If dr.Read() Then
                Dim count As Integer
                For count = 0 To dr.FieldCount - 1
                    retnArray.Add(dr.GetValue(count).ToString())
                Next
            End If
            dr.Close()
            Return retnArray

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsSalTaxID.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsSalTaxID.vb")
        End Try
    End Function
#End Region
#Region "Select maxid"

    Public Function Getmaxid() As Integer

        Dim selConnection As SqlConnection = Nothing
        Dim selConn As SqlConnection = Nothing
        selConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASSTAX_maxid"
        Try
            trans = selConnection.BeginTransaction()

            ' define search fields 


            Dim ds As DataSet = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName)
            Dim maxid As Integer = ds.Tables(0).Rows(0).Item(0)
            Return maxid

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsPrceSetUp.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsPrceSetUp.vb")
        End Try

    End Function
#End Region
#Region "confirm no duplicated Percentage"
    Public Function GetDuplicatedPercentage() As Integer
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASSTAX_BYPercentage"
        Try
            trans = sidConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstrSalTaxsSetUpID)
            'Param(1).Value = Trim(fdobTaxPercentage)
            Param(1).Value = Trim(fdateEffectiveDate)

            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            If dr.Read() Then
                dr.Close()
                Return -1
            End If
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsPriceSetUp.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsPriceSetUp.vb")
            Return -1
        End Try

    End Function
#End Region
    Public Function GetDuplicatedIDPercentage() As Integer
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASSTAX_DUPPercentage"
        Try
            trans = sidConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(3) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstrSalTaxsSetUpID)
            Param(1).Value = Trim(fdobTaxPercentage)
            Param(2).Value = Trim(fdateEffectiveDate)

            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            If dr.Read() Then
                dr.Close()
                Return -1
            End If
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsSalTaxID.vb")
            Return -1
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsSalTaxID.vb")
            Return -1
        Finally
            sidConnection.Close()
            sidConn.Close()
        End Try
    End Function
#Region "Select returnsalSetUp By ID"

    Public Function GetReturnsalSetUpIDDetailsByID() As ArrayList
        Dim retnArray As ArrayList = New ArrayList()
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASSTAX_SELRETURN"
        Try
            trans = sidConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(1) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstrSalTaxsSetUpID)

            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            'Return dr
            If dr.Read() Then
                Dim count As Integer
                For count = 0 To dr.FieldCount - 1
                    retnArray.Add(dr.GetValue(count).ToString())
                Next
            End If
            dr.Close()
            Return retnArray

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsPriceID.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsPriceID.vb")
        End Try
    End Function
#End Region
#Region "Select taxid  in state"
  
    Public Function Getsate() As SqlDataReader
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASTAXSTATE_DELID"
        Try
            trans = sidConnection.BeginTransaction()
            Dim Param() As SqlParameter = New SqlParameter(1) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstrSalTaxID)
            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            Return dr
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsSalTaxID.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsSalTaxID.vb")

        End Try
    End Function
#End Region

End Class
