Imports SQLDataAccess
Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Web
Imports System.Data


#Region " Module Amment Hisotry "
'Description     :  the customer record
'History         :  
'Modified Date   :  2006-04-13
'Version         :  v1.0
'Author          :  ������


#End Region
Public Class clsCommonClass
    Private strConn As String
#Region "Declaration"
    Private fstrSpName As String ' store procedure name
    Private fstrspctr As String ' store procedure name
    Private fstrspstat As String ' store procedure name
    Private fstrsparea As String ' store procedure name
    Private fstruserid As String ' store procedure name
    Private fstrrank As String ' store procedure name
    Private fstrminsvcid As String
    Private fstrmaxsvcid As String
    Private fstrminstat As String
    Private fstrmaxstat As String
    Private fstrminarea As String
    Private fstrmaxarea As String
    Private fstrctry As String
    Private fstrClsID As String

#End Region
#Region "Properties"
    Public Property userid() As String
        Get
            Return fstruserid
        End Get
        Set(ByVal Value As String)
            fstruserid = Value
        End Set
    End Property
    Public Property rank() As String
        Get
            Return fstrrank
        End Get
        Set(ByVal Value As String)
            fstrrank = Value
        End Set
    End Property
    Public Property spctr() As String
        Get
            Return fstrspctr
        End Get
        Set(ByVal Value As String)
            fstrspctr = Value
        End Set
    End Property

    Public Property spstat() As String
        Get
            Return fstrspstat
        End Get
        Set(ByVal Value As String)
            fstrspstat = Value
        End Set
    End Property
    Public Property sparea() As String
        Get
            Return fstrsparea
        End Get
        Set(ByVal Value As String)
            fstrsparea = Value
        End Set
    End Property

    Public Property Minsvcid() As String
        Get
            Return fstrminsvcid
        End Get
        Set(ByVal Value As String)
            fstrminsvcid = Value
        End Set
    End Property

    Public Property Maxsvcid() As String
        Get
            Return fstrmaxsvcid
        End Get
        Set(ByVal Value As String)
            fstrmaxsvcid = Value
        End Set
    End Property

    Public Property Minstat() As String
        Get
            Return fstrminstat
        End Get
        Set(ByVal Value As String)
            fstrminstat = Value
        End Set
    End Property

    Public Property Maxstat() As String
        Get
            Return fstrmaxstat
        End Get
        Set(ByVal Value As String)
            fstrmaxstat = Value
        End Set
    End Property

    Public Property MinArea() As String
        Get
            Return fstrminarea
        End Get
        Set(ByVal Value As String)
            fstrminarea = Value
        End Set
    End Property
    Public Property MaxArea() As String
        Get
            Return fstrmaxarea
        End Get
        Set(ByVal Value As String)
            fstrmaxarea = Value
        End Set
    End Property
    Public Property ctryid() As String
        Get
            Return fstrctry
        End Get
        Set(ByVal Value As String)
            fstrctry = Value
        End Set
    End Property
    Public Property ClsID() As String
        Get
            Return fstrClsID
        End Get
        Set(ByVal Value As String)
            fstrClsID = Value
        End Set
    End Property


#End Region
#Region "Methods"
#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)

        connection.Open()

        Return connection
    End Function
#End Region
#Region "bonds  id-name "
    Public Function Getidname(ByVal fstrSpName) As DataSet
        Dim viewConnection As SqlConnection = Nothing
        Dim viewConn As SqlConnection = Nothing
        viewConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        viewConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        'fstrSpName = "BB_MASJOBS_VIEW"
        fstrSpName = "EXEC " & fstrSpName
        Try
            trans = viewConnection.BeginTransaction()
            Dim ds As DataSet = SqlHelper.ExecuteDataset(viewConn, CommandType.Text, fstrSpName)
            Return ds

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            'WriteErrLog.ErrorLog("zwz", ErrorLog.Item(1).ToString, WriteErrLog.SELE, "clsJobsPerDay.vb")
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            'WriteErrLog.ErrorLog("zwz", ErrorLog.Item(1).ToString, WriteErrLog.SELE, "clsJobsPerDay.vb")

        End Try

    End Function
#End Region
#Region "bonds tax id-name "
    Public Function Gettaxidname(ByVal fstrSpName) As DataSet
        Dim viewConnection As SqlConnection = Nothing
        Dim viewConn As SqlConnection = Nothing
        viewConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        viewConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        'fstrSpName = "BB_MASJOBS_VIEW"

        Try
            trans = viewConnection.BeginTransaction()
            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(viewConn, fstrSpName)
            Param(0).Value = Trim(spctr)

            Dim ds As DataSet = SqlHelper.ExecuteDataset(viewConn, CommandType.StoredProcedure, fstrSpName, Param)

            viewConn = Nothing
            viewConnection = Nothing
            Return ds

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            'WriteErrLog.ErrorLog("zwz", ErrorLog.Item(1).ToString, WriteErrLog.SELE, "clsJobsPerDay.vb")
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            'WriteErrLog.ErrorLog("zwz", ErrorLog.Item(1).ToString, WriteErrLog.SELE, "clsJobsPerDay.vb")

        End Try

    End Function
#End Region
#Region "bonds  stat  area id-name "
    Public Function Getcomidname(ByVal fstrSpName) As DataSet
        'Dim viewConnection As SqlConnection = Nothing
        Dim viewConn As SqlConnection = Nothing
        'viewConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        viewConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        'Dim trans As SqlTransaction = Nothing
        'fstrSpName = "BB_MASJOBS_VIEW"

        Try
            'trans = viewConnection.BeginTransaction()
            Dim Param() As SqlParameter = New SqlParameter(5) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(viewConn, fstrSpName)

            Param(0).Value = Trim(spctr)
            Param(1).Value = Trim(spstat)
            Param(2).Value = Trim(fstrsparea)
            Param(3).Value = Trim(fstrrank)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(viewConn, CommandType.StoredProcedure, fstrSpName, Param)
            Getcomidname = ds
            'viewConnection.Dispose()
            viewConn.Dispose()

        Catch ex As Exception
            'trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("zwz", ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsCommonClass.vb")
        Catch ex As SqlException
            'trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("zwz", ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsCommonClass.vb")

        End Try

    End Function
#End Region
#Region "bonds  State ID and name "
    Public Function GetStateIdName(ByVal fstrSpName) As DataSet
        'Dim viewConnection As SqlConnection = Nothing
        Dim viewConn As SqlConnection = Nothing
        'viewConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        viewConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        'Dim trans As SqlTransaction = Nothing
        'fstrSpName = "BB_MASJOBS_VIEW"

        Try
            'trans = viewConnection.BeginTransaction()
            Dim Param() As SqlParameter = New SqlParameter(5) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(viewConn, fstrSpName)

            Param(0).Value = Trim(spctr)
            Param(1).Value = Trim(spstat)
            Param(2).Value = Trim(fstrsparea)
            Param(3).Value = Trim(fstrrank)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(viewConn, CommandType.StoredProcedure, fstrSpName, Param)
            GetStateIdName = ds
            'viewConnection.Dispose()
            viewConn.Dispose()

        Catch ex As Exception
            'trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("zwz", ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsCommonClass.vb")
        Catch ex As SqlException
            'trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("zwz", ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsCommonClass.vb")

        End Try

    End Function
#End Region
#Region "switch date style to display"
    Public Function DatetoDisplay(ByVal datestyle As Date) As String
        Dim datearray As Array = datestyle.ToString().Split(" ")
        Dim datearr = datearray(0).ToString().Split("-")
        Dim rtdate As String = datearr(2) + "/" + datearr(1) + "/" + datearr(0)
        rtdate = rtdate + " " + datearray(1)
        Return rtdate
    End Function
#End Region
#Region "switch date style to insert database"
    Public Function DatetoDatabase(ByVal datestyle As String) As String
        Dim datearray As Array = datestyle.ToString().Split(" ")
        Dim datearr As Array = datearray(0).ToString().Split("/")
        Dim rtdate As String = datearr(2) + "-" + datearr(1) + "-" + datearr(0)
        rtdate = rtdate + " " + datearray(1)
        Return rtdate
    End Function
#End Region
#Region " GET SUBSTRING"
    Public Function getsubstring(ByVal str As String) As String
        Dim str1 As String
        Dim n As Integer
        n = str.IndexOf("-")
        str1 = str.Substring(0, n)
        Return str1
    End Function
#End Region

#Region " telephone convert to password"
    Public Function telconvertpass(ByVal tel As String) As String
        Dim i As Integer

        For i = 1 To tel.Length
            Dim aa As Char
            aa = GetChar(tel, i)
            Select Case aa
                Case "0"

                    tel = tel.Replace(aa, "A")

                Case "1"

                    tel = tel.Replace(aa, "B")
                Case "2"

                    tel = tel.Replace(aa, "C")
                Case "3"

                    tel = tel.Replace(aa, "D")
                Case "4"

                    tel = tel.Replace(aa, "#")
                Case "5"

                    tel = tel.Replace(aa, "V")
                Case "6"

                    tel = tel.Replace(aa, "*")
                Case "7"

                    tel = tel.Replace(aa, "X")
                Case "8"

                    tel = tel.Replace(aa, ".")
                Case "9"

                    tel = tel.Replace(aa, "Z")

            End Select

        Next
        Return tel
    End Function

#End Region

#Region " password convert to telephone"
    Public Function passconverttel(ByVal tel As String) As String
        Dim i As Integer

        For i = 1 To tel.Length
            Dim aa As Char
            aa = GetChar(tel, i)
            Select Case aa
                Case "A"

                    tel = tel.Replace(aa, "0")

                Case "B"

                    tel = tel.Replace(aa, "1")
                Case "C"

                    tel = tel.Replace(aa, "2")
                Case "D"

                    tel = tel.Replace(aa, "3")
                Case "#"

                    tel = tel.Replace(aa, "4")
                Case "V"

                    tel = tel.Replace(aa, "5")
                Case "*"

                    tel = tel.Replace(aa, "6")
                Case "X"

                    tel = tel.Replace(aa, "7")
                Case "."

                    tel = tel.Replace(aa, "8")
                Case "Z"

                    tel = tel.Replace(aa, "9")

            End Select

        Next
        Return tel
    End Function

#End Region
#Region "bonds  user id-name "
    Public Function Getuseridname(ByVal fstrSpName) As DataSet
        'Dim viewConnection As SqlConnection = Nothing
        Dim viewConn As SqlConnection = Nothing
        'viewConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        viewConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        'Dim trans As SqlTransaction = Nothing
        'fstrSpName = "BB_MASJOBS_VIEW"

        Try
            'trans = viewConnection.BeginTransaction()
            Dim Param() As SqlParameter = New SqlParameter(1) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(viewConn, fstrSpName)

            Param(0).Value = Trim(fstruserid)

            Dim ds As DataSet = SqlHelper.ExecuteDataset(viewConn, CommandType.StoredProcedure, fstrSpName, Param)
            Getuseridname = ds
            'viewConnection.Dispose()
            viewConn.Dispose()
        Catch ex As Exception
            'trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstruserid, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsCommonClass.vb")
        Catch ex As SqlException
            'trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstruserid, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsCommonClass.vb")

        End Try

    End Function
#End Region

#Region "Get Area ID and Name depends on country code "
    Public Function GetAreaidname() As DataSet
        'Dim viewConnection As SqlConnection = Nothing
        Dim viewConn As SqlConnection = Nothing
        'viewConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        viewConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        'Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASAREA_AREAIDNAME"

        Try
            'trans = viewConnection.BeginTransaction()
            Dim Param() As SqlParameter = New SqlParameter(1) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(viewConn, fstrSpName)

            Param(0).Value = Trim(spctr)

            Dim ds As DataSet = SqlHelper.ExecuteDataset(viewConn, CommandType.StoredProcedure, fstrSpName, Param)
            GetAreaidname = ds
            'viewConnection.Dispose()
            viewConn.Dispose()

        Catch ex As Exception
            'trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("zwz", ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsCommonClass.vb")
        Catch ex As SqlException
            'trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("zwz", ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsCommonClass.vb")

        End Try

    End Function
#End Region

#Region "Get State ID and Name depends on country code "
    Public Function GetStateidname() As DataSet
        'Dim viewConnection As SqlConnection = Nothing
        Dim viewConn As SqlConnection = Nothing
        'viewConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        viewConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        'Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MSTA_STATEIDNAME"

        Try
            'trans = viewConnection.BeginTransaction()
            Dim Param() As SqlParameter = New SqlParameter(1) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(viewConn, fstrSpName)

            Param(0).Value = Trim(spctr)

            Dim ds As DataSet = SqlHelper.ExecuteDataset(viewConn, CommandType.StoredProcedure, fstrSpName, Param)
            GetStateidname = ds
            'viewConnection.Dispose()
            viewConn.Dispose()

        Catch ex As Exception
            'trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("zwz", ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsCommonClass.vb")
        Catch ex As SqlException
            'trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("zwz", ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsCommonClass.vb")

        End Try

    End Function
#End Region

    Public Function GetReminderDay() As Integer
        GetReminderDay = 240
    End Function


    Public Function InsertLanguage(ByVal strID As String, ByVal strName As String, ByVal strType As String) As Int16
        Dim viewConnection As SqlConnection = Nothing
        Dim viewConn As SqlConnection = Nothing
        viewConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        viewConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "language_insert"

        Try
            trans = viewConnection.BeginTransaction()
            Dim Param() As SqlParameter = New SqlParameter(3) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(viewConnection, fstrSpName)
            Param(0).Value = Trim(strID)
            Param(1).Value = Trim(strName)
            Param(2).Value = Trim(strType)

            Dim ds As DataSet = SqlHelper.ExecuteDataset(viewConn, CommandType.StoredProcedure, fstrSpName, Param)
            viewConnection.Dispose()
            viewConn.Dispose()
            Return 1


        Catch ex As Exception
            trans.Rollback()
            Return 0


        Catch ex As SqlException
            trans.Rollback()
            Return 0


        End Try
    End Function

    Public Function LanguageSelect(ByVal strID As String, ByVal strType As String) As String
        Dim viewConnection As SqlConnection = Nothing
        Dim viewConn As SqlConnection = Nothing
        Dim Result As String
        viewConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        viewConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        'Dim trans As SqlTransaction = Nothing

        fstrSpName = "language_select"

        Try
            'trans = viewConnection.BeginTransaction()
            Dim Param() As SqlParameter = New SqlParameter(3) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(viewConnection, fstrSpName)
            Param(0).Value = Trim(strID)
            Param(1).Value = Trim(strType)
            Param(2).Value = Trim(Result)

            'Dim ds As DataSet = SqlHelper.ExecuteDataset(viewConn, CommandType.StoredProcedure, fstrSpName, Param)
            'If Not ds Is Nothing Then
            SqlHelper.ExecuteNonQuery(viewConn, CommandType.StoredProcedure, fstrSpName, Param)
            Result = Param(2).Value

            'Result = ds.Tables(0).Rows(0).Item(0).ToString

            viewConnection.Dispose()
            viewConn.Dispose()
            Return Result
            'Else
            'viewConnection.Dispose()
            'viewConn.Dispose()
            'Return ""
            'End If

        Catch ex As Exception
            'trans.Rollback()
            Return ""


        Catch ex As SqlException
            'trans.Rollback()
            Return ""


        End Try
    End Function


#Region "getServiceCenter and States"
    Public Function SVCnStatebyUserID() As DataSet
        Dim sqlConn As SqlConnection = Nothing
        sqlConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        fstrSpName = "BB_RPTCUSR_GetSVCSTATE"
        Dim ds As DataSet = New DataSet
        Try
            Dim paramStorc() As SqlParameter = New SqlParameter(1) {}
            paramStorc = SqlHelperParameterCache.GetSpParameterSet(sqlConn, fstrSpName)
            paramStorc(0).Value = Trim(fstruserid)

            ds = SqlHelper.ExecuteDataset(sqlConn, CommandType.StoredProcedure, fstrSpName, paramStorc)

        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("Customer reference Report", ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "ClsRptCUSR.vb")
            Return Nothing
        Finally
            sqlConn.Close()
        End Try
        Return ds
    End Function

#End Region


#Region "Get State and area from service center"

    Public Function getStateAndArea() As DataSet
        Dim sqlConn As SqlConnection = Nothing
        sqlConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        fstrSpName = "BB_FNCCLAS_SVC_STA_ARE"
        Dim dsCLA As DataSet = New DataSet
        Try
            Dim paramStorc() As SqlParameter = New SqlParameter(1) {}
            paramStorc = SqlHelperParameterCache.GetSpParameterSet(sqlConn, fstrSpName)
            paramStorc(0).Value = Trim(fstrminsvcid)
            paramStorc(1).Value = Trim(fstrctry)

            dsCLA = SqlHelper.ExecuteDataset(sqlConn, CommandType.StoredProcedure, fstrSpName, paramStorc)

        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("Customer reference Report", ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "ClsRptCUSR.vb")
            Return Nothing
        Finally
            sqlConn.Close()
        End Try
        Return dsCLA
    End Function

#End Region

#Region "Get area from state"

    Public Function getAreafromState() As DataSet
        Dim sqlConn As SqlConnection = Nothing
        sqlConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        fstrSpName = "BB_FNCCLAS_STA_ARE"
        Dim dsCLA As DataSet = New DataSet
        Try
            Dim paramStorc() As SqlParameter = New SqlParameter(1) {}
            paramStorc = SqlHelperParameterCache.GetSpParameterSet(sqlConn, fstrSpName)
            paramStorc(0).Value = Trim(fstrminstat)
            paramStorc(1).Value = Trim(fstrminsvcid)

            dsCLA = SqlHelper.ExecuteDataset(sqlConn, CommandType.StoredProcedure, fstrSpName, paramStorc)

        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("Customer reference Report", ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "ClsRptCUSR.vb")
            Return Nothing
        Finally
            sqlConn.Close()
        End Try
        Return dsCLA
    End Function
#End Region


#Region "Get MODEL from Class ID"

    Public Function getModelfrmClass() As DataSet
        Dim sqlConn As SqlConnection = Nothing
        sqlConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        fstrSpName = "BB_RPTCUSR_GETMODELBYCLASSID"
        Dim dsCLA As DataSet = New DataSet
        Try
            Dim paramStorc() As SqlParameter = New SqlParameter(2) {}
            paramStorc = SqlHelperParameterCache.GetSpParameterSet(sqlConn, fstrSpName)
            paramStorc(0).Value = Trim(fstrctry)
            paramStorc(1).Value = Trim(fstrClsID)

            dsCLA = SqlHelper.ExecuteDataset(sqlConn, CommandType.StoredProcedure, fstrSpName, paramStorc)

        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("Customer reference Report", ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "ClsRptCUSR.vb")
            Return Nothing
        Finally
            sqlConn.Close()
        End Try
        Return dsCLA
    End Function
#End Region


#End Region

End Class
