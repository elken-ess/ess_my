Imports SQLDataAccess
Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Web
Imports System.Data
#Region " Module Amment Hisotry "
'Description     :  the customer record
'History         :  
'Modified Date   :  2006-04-10
'Version         :  v1.0
'Author          :  朱文姿

'201702 - Mod by LLY - SMR 1701/1995/Add "Preferred Language" in Customer Record Maintenance.

#End Region
Public Class clsCustomerRecord

#Region "Declaration"

    Private fstrSpName As String ' store procedure name
    Private fstrCustomerID As String ' customer id
    Private fstrCustpf As String ' customer Prefix
    Private fstrCustomerName As String ' customer name
    Private fstrCustomerJDE As String ' customer name
    Private fstrAreaID As String ' area id
    Private fstrContact As String ' contact
    Private fstrRoSerialNo As String ' Ro serial No
    Private fstrromodelid As String

    Private fstrJDECustID As String ' JDE Customer ID
    Private fstrAlterName As String ' Alternate Name
    Private fstrRaces As String ' Races
    Private fstrCustType As String ' Customer Type
    Private fstrStatus As String ' Status
    Private fstrNotes As String ' Notes
    Private fstrCreatBy As String ' Created By

    Private fstrCreatDate As String ' Created Date
    Private fstrModBy As String ' Modified By
    Private fstrModDate As String ' Modified Date
    Private fstrusername As String ' username
    Private fstrConType As String ' contact
    Private fintTORS As Integer  ' contactTORS
    Private fstrsvcid As String ' svcid
    Private fstrrank As String ' rank

    Private fstrctry As String ' user country
    Private fstrcomp As String ' user company
    Private fstrsvc As String '  user svcid
    Private fstripadress As String ' ipadress

    Private fstrEmail As String 'Email
    Private fstrTelty As String ' telephone type

    ' Added by Ryan Estandarte 17 July 2012
    Private fstrModelID As String
    Private fstrAddress1 As String
    Private fstrAddress2 As String
    Private fstrStateID As String
    Private fstrPostCode As String

    'Added by LLY 201702
    Private fstrPrefLang As String


#End Region

#Region "Properties"
    Public Property Teltype() As String
        Get
            Return fstrTelty
        End Get
        Set(ByVal Value As String)
            fstrTelty = Value
        End Set
    End Property

    Public Property Ipadress() As String
        Get
            Return fstripadress
        End Get
        Set(ByVal Value As String)
            fstripadress = Value
        End Set
    End Property
    Public Property modelid() As String
        Get
            Return fstrromodelid
        End Get
        Set(ByVal Value As String)
            fstrromodelid = Value
        End Set
    End Property
    Public Property email() As String
        Get
            Return fstrEmail
        End Get
        Set(ByVal Value As String)
            fstrEmail = Value
        End Set
    End Property
    Public Property username() As String
        Get
            Return fstrusername
        End Get
        Set(ByVal Value As String)
            fstrusername = Value
        End Set
    End Property
    Public Property ctryid() As String
        Get
            Return fstrctry
        End Get
        Set(ByVal Value As String)
            fstrctry = Value
        End Set
    End Property
    Public Property compid() As String
        Get
            Return fstrcomp
        End Get
        Set(ByVal Value As String)
            fstrcomp = Value
        End Set
    End Property
    Public Property usvcid() As String
        Get
            Return fstrsvc
        End Get
        Set(ByVal Value As String)
            fstrsvc = Value
        End Set
    End Property
    Public Property rank() As String
        Get
            Return fstrrank
        End Get
        Set(ByVal Value As String)
            fstrrank = Value
        End Set
    End Property

    Public Property Svcid() As String
        Get
            Return fstrsvcid
        End Get
        Set(ByVal Value As String)
            fstrsvcid = Value
        End Set
    End Property
    Public Property TORS() As Integer
        Get
            Return fintTORS
        End Get
        Set(ByVal Value As Integer)
            fintTORS = Value
        End Set
    End Property

    Public Property JDECustID() As String
        Get
            Return fstrCustomerJDE
        End Get
        Set(ByVal Value As String)
            fstrCustomerJDE = Value
        End Set
    End Property
    Public Property AlterName() As String
        Get
            Return fstrAlterName
        End Get
        Set(ByVal Value As String)
            fstrAlterName = Value
        End Set
    End Property
    Public Property Races() As String
        Get
            Return fstrRaces
        End Get
        Set(ByVal Value As String)
            fstrRaces = Value
        End Set
    End Property
    Public Property CustType() As String
        Get
            Return fstrCustType
        End Get
        Set(ByVal Value As String)
            fstrCustType = Value
        End Set
    End Property
    Public Property Status() As String
        Get
            Return fstrStatus
        End Get
        Set(ByVal Value As String)
            fstrStatus = Value
        End Set
    End Property
    Public Property Notes() As String
        Get
            Return fstrNotes
        End Get
        Set(ByVal Value As String)
            fstrNotes = Value
        End Set
    End Property
    Public Property ModBy() As String
        Get
            Return fstrModBy
        End Get
        Set(ByVal Value As String)
            fstrModBy = Value
        End Set
    End Property
    Public Property CreatBy() As String
        Get
            Return fstrCreatBy
        End Get
        Set(ByVal Value As String)
            fstrCreatBy = Value
        End Set
    End Property
    Public Property CustomerID() As String
        Get
            Return fstrCustomerID
        End Get
        Set(ByVal Value As String)
            fstrCustomerID = Value
        End Set
    End Property
    Public Property Customerpf() As String
        Get
            Return fstrCustpf
        End Get
        Set(ByVal Value As String)
            fstrCustpf = Value
        End Set
    End Property

    Public Property CustomerName() As String
        Get
            Return fstrCustomerName
        End Get
        Set(ByVal Value As String)
            fstrCustomerName = Value
        End Set
    End Property

    Public Property AreaID() As String
        Get
            Return fstrAreaID
        End Get
        Set(ByVal Value As String)
            fstrAreaID = Value
        End Set
    End Property

    Public Property Contact() As String
        Get
            Return fstrContact
        End Get
        Set(ByVal Value As String)
            fstrContact = Value
        End Set
    End Property

    Public Property RoSerialNo() As String
        Get
            Return fstrRoSerialNo
        End Get
        Set(ByVal Value As String)
            fstrRoSerialNo = Value
        End Set
    End Property
    Public Property Contacttype() As String
        Get
            Return fstrConType
        End Get
        Set(ByVal Value As String)
            fstrConType = Value
        End Set
    End Property

    ''' <summary>
    ''' Model ID which the customer has purchased
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property CustModelID() As String
        Get
            Return fstrModelID
        End Get
        Set(ByVal value As String)
            fstrModelID = value
        End Set
    End Property

    Public Property Address1() As String
        Get
            Return fstrAddress1
        End Get
        Set(ByVal value As String)
            fstrAddress1 = value
        End Set
    End Property

    Public Property Address2() As String
        Get
            Return fstrAddress2
        End Get
        Set(ByVal value As String)
            fstrAddress2 = value
        End Set
    End Property

    Public Property StateID() As String
        Get
            Return fstrStateID
        End Get
        Set(ByVal value As String)
            fstrStateID = value
        End Set
    End Property

    Public Property PostCode() As String
        Get
            Return fstrPostCode
        End Get
        Set(ByVal value As String)
            fstrPostCode = value
        End Set
    End Property

    'added by LLY 201702
    Public Property PrefLang() As String
        Get
            Return fstrPrefLang
        End Get
        Set(ByVal value As String)
            fstrPrefLang = value
        End Set
    End Property

#End Region
#Region "Methods"

#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)

        connection.Open()

        Return connection
    End Function
#End Region
#Region "SEARCH Customer For View IN INSTALLSEARCH"

    Public Function GetSearchCustomer() As DataSet

        '    Dim ds As DataSet = SqlHelper.ExecuteDataset(viewConn, CommandType.StoredProcedure, fstrSpName, Param)
        Dim viewConnection As SqlConnection = Nothing
        Dim viewConn As SqlConnection = Nothing
        viewConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        viewConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASCUST_installsearch"
        Try
            trans = viewConnection.BeginTransaction()
            'define search fields 
            Dim Param() As SqlParameter = New SqlParameter(11) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(viewConn, fstrSpName)
            Param(0).Value = Trim(fstrCustomerID)
            Param(1).Value = Trim(fstrCustomerName)
            Param(2).Value = Trim(fstrAreaID)
            Param(3).Value = Trim(fstrContact)
            Param(4).Value = Trim(fstrRoSerialNo)
            Param(5).Value = Trim(fstrConType)
            Param(6).Value = Trim(fstrModBy)
            Param(7).Value = Trim(fstrctry)
            Param(8).Value = Trim(fstrcomp)
            Param(9).Value = Trim(fstrsvc)
            Param(10).Value = Trim(fstrrank)
            Dim dst As DataSet = New DataSet()
            dst = SqlHelper.ExecuteDataset(viewConn, CommandType.StoredProcedure, fstrSpName, Param)
            GetSearchCustomer = dst
            viewConnection.Dispose()
            viewConn.Dispose()


        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsCustomerRecord.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsCustomerRecord.vb")
        End Try
    End Function
#End Region



#Region "Select All Customer For View"

    Public Function GetAllCustomer() As DataSet

        '    Dim ds As DataSet = SqlHelper.ExecuteDataset(viewConn, CommandType.StoredProcedure, fstrSpName, Param)
        Dim viewConnection As SqlConnection = Nothing
        Dim viewConn As SqlConnection = Nothing
        viewConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        viewConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASCUST_View"
        Try
            trans = viewConnection.BeginTransaction()

            'define search fields 
            ' Modified by Ryan Estandarte 17 July 2012
            'Dim Param() As SqlParameter = New SqlParameter(14) {}
            Dim Param() As SqlParameter = New SqlParameter(19) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(viewConn, fstrSpName)
            Param(0).Value = Trim(fstrCustomerID)
            'Dim str As String = Param(0).Value
            Param(1).Value = Trim(fstrCustomerName)
            Param(2).Value = Trim(fstrAreaID)
            Param(3).Value = Trim(fstrContact)
            Param(4).Value = Trim(fstrRoSerialNo)
            Param(5).Value = Trim(fstrConType)
            Param(6).Value = Trim(fstrModBy)
            Param(7).Value = Trim(fstrctry)

            Param(8).Value = Trim(fstrcomp)
            Param(9).Value = Trim(fstrsvc)
            Param(10).Value = Trim(fstrrank)
            Param(11).Value = Trim(fstrStatus)

            'LW Added
            Param(12).Value = Trim(fstrCustType)
            Param(13).Value = Trim(fstrRaces)

            ' Added by Ryan Estandarte 17 July 2012
            Param(14).Value = Trim(fstrModelID)
            Param(15).Value = Trim(fstrAddress1)
            Param(16).Value = Trim(fstrAddress2)
            Param(17).Value = Trim(fstrStateID)
            Param(18).Value = Trim(fstrPostCode)

            Dim dst As DataSet = New DataSet()

            dst = SqlHelper.ExecuteDataset(viewConn, CommandType.StoredProcedure, fstrSpName, Param)

            Dim count As Integer
            'Dim statXmlTr As New clsXml

            'statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            'For count = 0 To dst.Tables(0).Rows.Count - 1

            '    Dim teleph As String = dst.Tables(0).Rows(count).Item(10).ToString


            '    Dim telepass As New clsCommonClass()
            '    dst.Tables(0).Rows(count).Item(10) = telepass.passconverttel(teleph)
            'Next
            GetAllCustomer = dst
            viewConnection.Dispose()
            viewConn.Dispose()



        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsCustomerRecord.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsCustomerRecord.vb")
        End Try

    End Function
#End Region

#Region "Get ROUID for Customer"
    Public Function GetInstallROUID() As String
        Dim viewConn As SqlConnection = Nothing
        Dim viewConnection As SqlConnection = Nothing
        Dim oReturnVal As DataSet
        Dim oConvertedVal As String
        viewConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        viewConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASCUST_GETROUID"
        Dim Param() As SqlParameter = New SqlParameter(4) {}
        Param = SqlHelperParameterCache.GetSpParameterSet(viewConn, fstrSpName)
        Param(0).Value = Trim(fstrCustomerID)
        Param(1).Value = Trim(fstrCustpf)
        Param(2).Value = Trim(fstrromodelid)
        Param(3).Value = Trim(fstrRoSerialNo)


        Try
            trans = viewConnection.BeginTransaction()
            Dim ds As DataSet = SqlHelper.ExecuteDataset(viewConn, CommandType.StoredProcedure, fstrSpName, Param)
            'oReturnVal = SqlHelper.ExecuteDataset(viewConn, CommandType.Text, fstrSpName)
            'oConvertedVal = oReturnVal.Tables(0).Rows(0).Item(0).ToString
            oConvertedVal = ds.Tables(0).Rows(0).Item(0).ToString
            GetInstallROUID = oConvertedVal

            viewConnection.Dispose()
            viewConn.Dispose()

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.VIEWS, "clsCustomerRecord.vb")
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.VIEWS, "clsCustomerRecord.vb")
        End Try
    End Function
#End Region

#Region "Get Customer ID from LPFX_FIL"
    Public Function GetCustomerID() As DataSet
        Dim viewConnection As SqlConnection = Nothing
        Dim viewConn As SqlConnection = Nothing
        viewConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        viewConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASLPFX_RUNNO"
        Try
            trans = viewConnection.BeginTransaction()
            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(viewConn, fstrSpName)
            Param(0).Value = Trim(fstrCustpf)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(viewConn, CommandType.StoredProcedure, fstrSpName, Param)
            GetCustomerID = ds
            viewConnection.Dispose()
            viewConn.Dispose()



        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.VIEWS, "clsCustomerRecord.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.VIEWS, "clsCustomerRecord.vb")
        End Try
    End Function
#End Region

#Region "Update Customer ID in LPFX_FIL"

    Public Function UpdateCustomerID() As Integer

        Dim viewConnection As SqlConnection = Nothing
        Dim viewConn As SqlConnection = Nothing
        viewConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        viewConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASLPFX_RUNNOUpd"
        Try
            trans = viewConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(viewConn, fstrSpName)
            Param(0).Value = Trim(fstrCustpf)

            'Param(21).Value = Convert.ToDateTime(Trim(fstrModifiedDate)) '最后修改者
            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(viewConn, CommandType.StoredProcedure, fstrSpName, Param)
            ' insert audit log
            'Dim WriteAuditLog As New clsLogFile()
            'Dim insData As String = Param(0).Value + "," + Param(1).Value + "," + Param(2).Value + "," + Param(3).Value + "," + Param(4).Value + "," + Param(5).Value + "," + Param(6).Value + "," + Param(7).Value + "," + Param(8).Value + "," + Param(9).Value + "," + Param(10).Value
            'WriteAuditLog.AddLog("wang", "table name:mctr_fil", "Data:" + insData, WriteAuditLog.ADD)

            trans.Commit()
            viewConnection.Dispose()
            viewConn.Dispose()
            UpdateCustomerID = 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "clsCustomerRecord.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "clsCustomerRecord.vb")
            Return -1
        End Try

    End Function

#End Region


#Region "Select Customer By ID"

    Public Function GetCustomerDetailsByID() As SqlDataReader
        Dim viewConnection As SqlConnection = Nothing
        Dim viewConn As SqlConnection = Nothing
        viewConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        viewConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASCUST_SelByID"
        Try
            trans = viewConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(3) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(viewConn, fstrSpName)
            Param(0).Value = Trim(fstrCustomerID)
            Param(1).Value = Trim(fstrCustpf)

            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(viewConn, CommandType.StoredProcedure, fstrSpName, Param)
            GetCustomerDetailsByID = dr
            viewConnection.Dispose()
            'viewConn.Dispose()

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            'Return ErrorLog

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            'Return ErrorLog
        End Try
    End Function
#End Region

#Region "Select Customer Add"

    Public Function GetCustomerAddress() As SqlDataReader
        Dim viewConnection As SqlConnection = Nothing
        Dim viewConn As SqlConnection = Nothing
        viewConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        viewConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASCUST_SelAddress"
        Try
            trans = viewConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(3) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(viewConn, fstrSpName)
            Param(0).Value = Trim(fstrCustomerID)
            Param(1).Value = Trim(fstrCustpf)

            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(viewConn, CommandType.StoredProcedure, fstrSpName, Param)
            GetCustomerAddress = dr
            viewConnection.Dispose()
            'viewConn.Dispose()



        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            'Return ErrorLog

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            'Return ErrorLog
        End Try
    End Function
#End Region

#Region "Select Customer Tele"
    Public Function GetCustomerTele() As DataSet
        Dim viewConnection As SqlConnection = Nothing
        Dim viewConn As SqlConnection = Nothing
        viewConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        viewConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASCUST_SelTEL"
        Try
            trans = viewConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(3) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(viewConn, fstrSpName)
            Param(0).Value = Trim(fstrCustomerID)
            Param(1).Value = Trim(fstrCustpf)

            Dim ds As DataSet = SqlHelper.ExecuteDataset(viewConn, CommandType.StoredProcedure, fstrSpName, Param)
            GetCustomerTele = ds
            viewConnection.Dispose()
            viewConn.Dispose()
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            'Return ErrorLog

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            'Return ErrorLog
        End Try
    End Function
#End Region

#Region "Insert"

    Public Function Insert() As Integer

        Dim viewConnection As SqlConnection = Nothing
        Dim viewConn As SqlConnection = Nothing
        viewConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        viewConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASCUST_Add"
        Try
            trans = viewConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(14) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(viewConn, fstrSpName)
            Param(0).Value = Trim(fstrCustpf)
            Param(1).Value = Trim(fstrCustomerID)
            Param(2).Value = Trim(fstrJDECustID)
            Param(3).Value = Trim(fstrCustomerName)
            Param(4).Value = Trim(fstrAlterName)
            Param(5).Value = Trim(fstrRaces)
            Param(6).Value = Trim(fstrCustType)
            Param(7).Value = Trim(fstrStatus)
            Param(8).Value = Trim(fstrNotes)
            Param(9).Value = Trim(fstrsvcid)
            Param(10).Value = Trim(fstrCreatBy)
            Param(11).Value = Trim(fstrModBy)
            Param(12).Value = Trim(fstrEmail)
            Param(13).Value = Trim(fstrPrefLang)   'added by LLY 201702

            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(viewConn, CommandType.StoredProcedure, fstrSpName, Param)

            ' insert audit log
            ' Dim WriteAuditLog As New clsLogFile()
            'Dim insData As String = Param(0).Value + "," + Param(1).Value + "," + Param(2).Value + "," + Param(3).Value + "," + Param(4).Value + "," + Param(5).Value + "," + Param(6).Value + "," + Param(7).Value + "," + Param(8).Value + "," + Param(9).Value + "," + Param(10).Value
            ' WriteAuditLog.AddLog("wang", "table name:mctr_fil", "Data:" + insData, WriteAuditLog.ADD)

            trans.Commit()
            viewConnection.Dispose()
            viewConn.Dispose()
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.VIEWS, "clsCustomerRecord.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.VIEWS, "clsCustomerRecord.vb")
            Return -1
        End Try


    End Function

#End Region
#Region "Select ro"

    Public Function GetRO() As DataSet

        Dim viewConnection As SqlConnection = Nothing
        Dim viewConn As SqlConnection = Nothing
        viewConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        viewConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASCUST_ROVIEW"
        Try
            trans = viewConnection.BeginTransaction()

            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(viewConn, fstrSpName)
            Param(0).Value = Trim(fstrCustomerID)
            Param(1).Value = Trim(fstrCustpf)




            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(viewConn, CommandType.StoredProcedure, fstrSpName, Param)
            ' switch status id to status name for display
            Dim count As Integer
            Dim statXmlTr As New clsXml
            Dim strPanm As String

            statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            For count = 0 To ds.Tables(0).Rows.Count - 1

                Dim statusid As String = ds.Tables(0).Rows(count).Item(2).ToString 'item(3) is status
                strPanm = statXmlTr.GetLabelName("StatusMessage", statusid)

                ds.Tables(0).Rows(count).Item(2) = strPanm
            Next
            GetRO = ds
            viewConnection.Dispose()
            viewConn.Dispose()

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsCustomerRecord.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsCustomerRecord.vb")
        Finally
            viewConnection.Close()
            viewConn.Close()
        End Try

    End Function
#End Region
#Region "Update"

    Public Function Update() As Integer

        Dim viewConnection As SqlConnection = Nothing
        Dim viewConn As SqlConnection = Nothing
        viewConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        viewConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASCUST_UPD"
        Try
            trans = viewConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(15) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(viewConn, fstrSpName)

            Param(0).Value = Trim(fstrCustomerID)
            Param(1).Value = Trim(fstrJDECustID)
            Param(2).Value = Trim(fstrCustomerName)
            Param(3).Value = Trim(fstrAlterName)
            Param(4).Value = Trim(fstrRaces)
            Param(5).Value = Trim(fstrCustType)
            Param(6).Value = Trim(fstrStatus)
            Param(7).Value = Trim(fstrNotes)

            Param(8).Value = Trim(fstrsvcid)
            Param(9).Value = Trim(fstrModBy)
            Param(10).Value = Trim(fstrCustpf)
            Param(11).Value = Trim(fstripadress)
            Param(12).Value = Trim(fstrsvc)
            Param(13).Value = Trim(fstrEmail)
            Param(14).Value = Trim(fstrPrefLang)  'lly 201702

            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(viewConn, CommandType.StoredProcedure, fstrSpName, Param)

            trans.Commit()
            viewConnection.Dispose()
            viewConn.Dispose()
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "clsCustomerRecord.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "clsCustomerRecord.vb")
            Return -1
        Finally
           
        End Try

    End Function

#End Region
#Region "Insert SADC"

    Public Function Insertsadf() As Integer

        Dim viewConnection As SqlConnection = Nothing
        Dim viewConn As SqlConnection = Nothing
        viewConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        viewConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASCUST_AddSADC"
        Try
            trans = viewConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(8) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(viewConn, fstrSpName)
            Param(0).Value = Trim(fstrCustomerID)
            'Dim str As String = Param(0).Value
            Param(1).Value = Trim(fstrCustomerName)
            Param(2).Value = Trim(fstrAreaID)
            Param(3).Value = Trim(fstrContact)
            Param(4).Value = Trim(fstrRoSerialNo)
            Param(5).Value = Trim(fstrConType)
            Param(6).Value = Trim(fintTORS)

            Param(7).Value = Trim(fstrModBy)

            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(viewConn, CommandType.StoredProcedure, fstrSpName, Param)

            ' insert audit log
            ' Dim WriteAuditLog As New clsLogFile()
            'Dim insData As String = Param(0).Value + "," + Param(1).Value + "," + Param(2).Value + "," + Param(3).Value + "," + Param(4).Value + "," + Param(5).Value + "," + Param(6).Value + "," + Param(7).Value + "," + Param(8).Value + "," + Param(9).Value + "," + Param(10).Value
            ' WriteAuditLog.AddLog("wang", "table name:mctr_fil", "Data:" + insData, WriteAuditLog.ADD)

            trans.Commit()
            viewConnection.Dispose()
            viewConn.Dispose()
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.VIEWS, "clsCustomerRecord.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.VIEWS, "clsCustomerRecord.vb")
            Return -1
        End Try


    End Function

#End Region
#Region "bonds RO id-name "
    Public Function GetROidname() As DataSet
        Dim viewConnection As SqlConnection = Nothing
        Dim viewConn As SqlConnection = Nothing
        viewConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        viewConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASCUST_GETROIDNAME"
        Try
            trans = viewConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(viewConn, fstrSpName)

            Param(0).Value = Trim(fstrCustomerID)
            Param(1).Value = Trim(fstrCustpf)

            Dim ds As DataSet = SqlHelper.ExecuteDataset(viewConn, CommandType.StoredProcedure, fstrSpName, Param)
            GetROidname = ds
            viewConnection.Dispose()
            viewConn.Dispose()


        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsCustomerRecord.vb")
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsCustomerRecord.vb")

        End Try

    End Function
#End Region
#Region "bonds RO id-name by ro id"
    Public Function GetROidnamebyid(ByVal fstrSpName) As DataSet
        Dim viewConnection As SqlConnection = Nothing
        Dim viewConn As SqlConnection = Nothing
        viewConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        viewConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        'fstrSpName = "BB_MASJOBS_VIEW"

        Try
            trans = viewConnection.BeginTransaction()


            Dim Param() As SqlParameter = New SqlParameter(1) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(viewConn, fstrSpName)

            Param(0).Value = Trim(fstrRoSerialNo)

            Dim ds As DataSet = SqlHelper.ExecuteDataset(viewConn, CommandType.StoredProcedure, fstrSpName, Param)
            GetROidnamebyid = ds
            viewConnection.Dispose()
            viewConn.Dispose()

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsCustomerRecord.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsCustomerRecord.vb")


        End Try

    End Function
#End Region
 
#Region "confirm no duplicated id "
    Public Function GetDuplicatedcustomerid() As Integer
        Dim viewConnection As SqlConnection = Nothing
        Dim viewConn As SqlConnection = Nothing
        viewConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        viewConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASCUST_SelByID"
        Try
            trans = viewConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(viewConn, fstrSpName)
            Param(0).Value = Trim(fstrCustomerID)
            Param(1).Value = Trim(fstrCustpf)



            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(viewConn, CommandType.StoredProcedure, fstrSpName, Param)
            If dr.Read() Then
                dr.Close()
                viewConnection.Dispose()
                'viewConn.Dispose()
                Return -1
            End If

            viewConnection.Dispose()
            'viewConn.Dispose()

            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsCustomerRecord.vb")
            Return -1
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsCustomerRecord.vb")
            Return -1
        Finally
           
        End Try
    End Function
#End Region
#Region "Select customer in rou"
    Public Function GetcustDel() As Integer
        Dim viewConnection As SqlConnection = Nothing
        Dim viewConn As SqlConnection = Nothing
        viewConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        viewConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASCUST_DEL"
        Try
            trans = viewConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(3) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(viewConn, fstrSpName)

            Param(0).Value = Trim(fstrCustomerID)
            Param(1).Value = Trim(fstrCustpf)




            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(viewConn, CommandType.StoredProcedure, fstrSpName, Param)
            If dr.Read() Then
                dr.Close()
                viewConnection.Dispose()
                'viewConn.Dispose()
                Return -1
            End If

            viewConnection.Dispose()
            'viewConn.Dispose()
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsServiceCente.vb")
            Return -1
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsServiceCente.vb")
            Return -1
        Finally
            viewConnection.Close()
            viewConn.Close()
        End Try
    End Function
#End Region
#End Region


End Class
