﻿Imports System.Data
Imports System.IO
Imports System.Xml
Imports System.Text
Imports System.Configuration
Imports System.Data.SqlClient

Public Class OutlookMenu
    Public Function BuildOutlook(ByVal GroupId As String) As String
        Dim selConn As New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim strModID, strFunID, strModIDTemp, strModName, strFunName, strCol, strTemp As String
        Dim i, j As Integer
        Dim ds As New DataSet
        Dim outlookmenu As New StringBuilder
        Dim xmlSet As New XmlWriterSettings
        xmlSet.Encoding = System.Text.Encoding.UTF8
        xmlSet.Indent = True
        xmlSet.IndentChars = (ControlChars.Tab)
        xmlSet.OmitXmlDeclaration = True
        Dim xtw As XmlWriter = XmlWriter.Create(outlookmenu, xmlSet)
        Try
            ds.ReadXml(ConfigurationSettings.AppSettings("ResouceXmlPath"))
            selConn.Open()
            Dim SqlDa As New SqlDataAdapter("exec BB_USERGROUP_Control '" + GroupId + "'", selConn)
            SqlDa.Fill(ds, "ModFun")
            If ds.Tables("ModFun").Rows.Count = 0 Then
                Return ""
            End If
            strModID = ""
            strModIDTemp = ""
            xtw.WriteStartDocument(True)
            xtw.WriteStartElement("modules")
            For i = 0 To ds.Tables("ModFun").Rows.Count - 1

                strModID = ds.Tables("ModFun").Rows(i).Item("LFNC_MODCD")
                If strModID <> strModIDTemp Then
                    If i <> 0 Then
                        xtw.WriteEndElement()
                    End If
                    xtw.WriteStartElement("module")
                    xtw.WriteAttributeString("id", strModID)
                    strModName = ds.Tables("Module").Rows(Convert.ToInt32(strModID) - 1).Item("NAME")
                    xtw.WriteAttributeString("name", strModName)
                    strModIDTemp = strModID
                End If

                strFunID = ds.Tables("ModFun").Rows(i).Item("LFNC_FNCID")
                xtw.WriteStartElement("function")
                xtw.WriteAttributeString("id", strFunID)
                strFunName = ds.Tables("Function").Rows(Convert.ToInt32(strFunID) - 1).Item("NAME")
                xtw.WriteAttributeString("name", strFunName)

                For j = 2 To ds.Tables("Function").Columns.Count - 1
                    strCol = ds.Tables("Function").Columns(j).Caption.ToString()
                    strTemp = ds.Tables("Function").Rows(Convert.ToInt32(strFunID) - 1).Item(j).ToString()
                    xtw.WriteElementString(strCol, strTemp)
                Next
                xtw.WriteEndElement()
            Next
            xtw.WriteEndElement()
            xtw.WriteEndDocument()
            xtw.Close()

            SqlDa.Dispose()
            selConn.Dispose()


            Return outlookmenu.ToString()
        Catch ex As Exception
            Return ""
        Finally

        End Try
    End Function
End Class
