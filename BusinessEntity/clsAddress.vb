Imports SQLDataAccess
Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Web
Imports System.Data
#Region " Module Amment Hisotry "
'Description     :  the address record
'History         :  
'Modified Date   :  2006-04-21
'Version         :  v1.0
'Author          :  朱文姿


#End Region
Public Class clsAddress
#Region "Declaration"

    Private fstrSpName As String ' store procedure name
    Private fstrAddressType As String 'Address Type
    Private fstrCustPrefix As String ' Customer Prefix
    Private fstrCustomerID As String ' Customer ID

    'Private fstrRoSerialNo As String ' Ro serial No
    Private fstrPICName As String ' Person In Charge Name
    Private fstrAddress1 As String ' Address 1
    Private fstrAddress2 As String ' Address 2
    Private fstrCountryID As String ' Country ID
    Private fstrStateID As String ' State ID
    Private fstrAreaID As String ' Area ID
    Private fstrPOCode As String ' PO Code
    Private fstrCreatedBy As String ' Created By
    Private fstrModifiedBy As String ' contact
    Private fstrStatus As String ' Status
    Private fintrouid As Integer ' ROUID
    Private fstrroumod As String ' ROModel Type
    Private fstrrouser As String ' ROSerial No
    Private fstrusername As String ' username
    Private fstripadress As String ' ipadress

    Private fstrsvrcid As String ' ipadress

    Private fstraddtype As String ' old Address Type
    Private fstrExistCustID As String = "" ' Existing Customer ID

#End Region
#Region "Properties"
    Public Property Ipadress() As String
        Get
            Return fstripadress
        End Get
        Set(ByVal Value As String)
            fstripadress = Value
        End Set
    End Property
    'Public Property Random() As String
    '    Get
    '        Return fstrrandom
    '    End Get
    '    Set(ByVal Value As String)
    '        fstrrandom = Value
    '    End Set
    'End Property
    Public Property Svcid() As String
        Get
            Return fstrsvrcid
        End Get
        Set(ByVal Value As String)
            fstrsvrcid = Value
        End Set
    End Property
    Public Property username() As String
        Get
            Return fstrusername
        End Get
        Set(ByVal Value As String)
            fstrusername = Value
        End Set
    End Property

    Public Property CustomerID() As String
        Get
            Return fstrCustomerID
        End Get
        Set(ByVal Value As String)
            fstrCustomerID = Value
        End Set
    End Property

    Public Property CustPrefix() As String
        Get
            Return fstrCustPrefix
        End Get
        Set(ByVal Value As String)
            fstrCustPrefix = Value
        End Set
    End Property
    Public Property Status() As String
        Get
            Return fstrStatus
        End Get
        Set(ByVal Value As String)
            fstrStatus = Value
        End Set
    End Property
    Public Property AreaID() As String
        Get
            Return fstrAreaID
        End Get
        Set(ByVal Value As String)
            fstrAreaID = Value
        End Set
    End Property
    Public Property RouID() As Integer
        Get
            Return fintrouid
        End Get
        Set(ByVal Value As Integer)
            fintrouid = Value
        End Set
    End Property

    Public Property PICName() As String
        Get
            Return fstrPICName
        End Get
        Set(ByVal Value As String)
            fstrPICName = Value
        End Set
    End Property

    Public Property AddressType() As String
        Get
            Return fstrAddressType
        End Get
        Set(ByVal Value As String)
            fstrAddressType = Value
        End Set
    End Property
    Public Property Address1() As String
        Get
            Return fstrAddress1
        End Get
        Set(ByVal Value As String)
            fstrAddress1 = Value
        End Set
    End Property
    Public Property Address2() As String
        Get
            Return fstrAddress2
        End Get
        Set(ByVal Value As String)
            fstrAddress2 = Value
        End Set
    End Property
    Public Property CountryID() As String
        Get
            Return fstrCountryID
        End Get
        Set(ByVal Value As String)
            fstrCountryID = Value
        End Set
    End Property
    Public Property StateID() As String
        Get
            Return fstrStateID
        End Get
        Set(ByVal Value As String)
            fstrStateID = Value
        End Set
    End Property
    Public Property POCode() As String
        Get
            Return fstrPOCode
        End Get
        Set(ByVal Value As String)
            fstrPOCode = Value
        End Set
    End Property
    Public Property CreatedBy() As String
        Get
            Return fstrCreatedBy
        End Get
        Set(ByVal Value As String)
            fstrCreatedBy = Value
        End Set
    End Property
    Public Property ModifiedBy() As String
        Get
            Return fstrModifiedBy
        End Get
        Set(ByVal Value As String)
            fstrModifiedBy = Value
        End Set
    End Property
    Public Property ModelType() As String
        Get
            Return fstrroumod
        End Get
        Set(ByVal Value As String)
            fstrroumod = Value
        End Set
    End Property
    Public Property SerialNo() As String
        Get
            Return fstrrouser
        End Get
        Set(ByVal Value As String)
            fstrrouser = Value
        End Set
    End Property

    Public Property Addtype() As String
        Get
            Return fstraddtype
        End Get
        Set(ByVal Value As String)
            fstraddtype = Value
        End Set
    End Property

    Public Property ExistCustID() As String
        Get
            Return fstrExistCustID
        End Get
        Set(ByVal Value As String)
            fstrExistCustID = Value
        End Set
    End Property
#End Region

#Region "Meth"
#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)

        connection.Open()

        Return connection
    End Function
#End Region
#Region "Insert"

    Public Function Insert() As Integer

        Dim viewConnection As SqlConnection = Nothing
        Dim viewConn As SqlConnection = Nothing
        viewConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        viewConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASADDRESS_Add"
        Try
            trans = viewConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(14) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(viewConn, fstrSpName)
            Param(0).Value = Trim(fstrAddressType)
            Param(1).Value = Trim(fstrCustPrefix)
            Param(2).Value = Trim(fstrCustomerID)
            Param(3).Value = Trim(fintrouid)
            Param(4).Value = Trim(fstrPICName)
            Param(5).Value = Trim(fstrAddress1)
            Param(6).Value = Trim(fstrAddress2)
            Param(7).Value = Trim(fstrCountryID)
            Param(8).Value = Trim(fstrStateID)

            Param(9).Value = Trim(fstrAreaID)
            Param(10).Value = Trim(fstrPOCode)
            Param(11).Value = Trim(fstrStatus)
            Param(12).Value = Trim(fstrCreatedBy) '创建者
            Param(13).Value = Trim(fstrModifiedBy) '最后修改者
            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(viewConn, CommandType.StoredProcedure, fstrSpName, Param)

            trans.Commit()

            viewConnection.Dispose()
            viewConn.Dispose()
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsAddress.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsAddress.vb")
            Return -1
        Finally
            viewConnection.Close()
            viewConn.Close()
        End Try

    End Function

#End Region
#Region "Update"

    Public Function Update() As Integer

        Dim viewConnection As SqlConnection = Nothing
        Dim viewConn As SqlConnection = Nothing
        viewConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        viewConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASADDRESS_UPD"
        Try
            trans = viewConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(20) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(viewConn, fstrSpName)

            Param(0).Value = Trim(fstrAddressType)
            Param(1).Value = Trim(fstrCustomerID)
            'Param(2).Value = Trim(fstrCustPrefix)
            Param(2).Value = Trim(fintrouid)
            Param(3).Value = Trim(fstrPICName)
            Param(4).Value = Trim(fstrAddress1)
            Param(5).Value = Trim(fstrAddress2)
            Param(6).Value = Trim(fstrCountryID)
            Param(7).Value = Trim(fstrStateID)
            Param(8).Value = Trim(fstrAreaID)

            Param(9).Value = Trim(fstrPOCode)

            Param(10).Value = Trim(fstrStatus)
            'Param(11).Value = Trim(fstrCreatedBy) '创建者
            Param(11).Value = Trim(fstrModifiedBy) '最后修改者
            Param(12).Value = Trim(fstrCustPrefix)
            Param(13).Value = Trim(fstripadress)
            Param(14).Value = Trim(fstrsvrcid)
            Param(15).Value = Trim(fstraddtype)
            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(viewConn, CommandType.StoredProcedure, fstrSpName, Param)

            trans.Commit()

            viewConnection.Dispose()
            viewConn.Dispose()
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "clsAddress.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "clsAddress.vb")
            Return -1
        Finally
           
        End Try

    End Function

    Public Function Update2() As Integer

        Dim viewConnection As SqlConnection = Nothing
        Dim viewConn As SqlConnection = Nothing
        viewConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        viewConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASADDRESS_UPD2"
        Try
            trans = viewConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(11) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(viewConn, fstrSpName)

            Param(0).Value = Trim(fstrCustomerID)
            Param(1).Value = Trim(fstrAddress1)
            Param(2).Value = Trim(fstrAddress2)
            Param(3).Value = Trim(fstrCountryID)
            Param(4).Value = Trim(fstrStateID)
            Param(5).Value = Trim(fstrAreaID)
            Param(6).Value = Trim(fstrPOCode)
            Param(7).Value = Trim(fstrModifiedBy)
            Param(8).Value = Trim(fstrCustPrefix)
            Param(9).Value = Trim(fstripadress)
            Param(10).Value = Trim(fstrsvrcid)
            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(viewConn, CommandType.StoredProcedure, fstrSpName, Param)

            trans.Commit()
            viewConnection.Dispose()
            viewConn.Dispose()

            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "clsAddress.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "clsAddress.vb")
            Return -1
        Finally
           
        End Try

    End Function

#End Region
#Region "Select roid"

    Public Function GetROID() As DataSet

        Dim viewConnection As SqlConnection = Nothing
        Dim viewConn As SqlConnection = Nothing
        viewConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        viewConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASROUM_SelROID"
        Try
            trans = viewConnection.BeginTransaction()

            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(viewConn, fstrSpName)
            Param(0).Value = Trim(fstrroumod)
            Param(1).Value = Trim(fstrrouser)


            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(viewConn, CommandType.StoredProcedure, fstrSpName, Param)

            GetROID = ds
            viewConnection.Dispose()
            viewConn.Dispose()

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsAddress.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsAddress.vb")
        Finally
            viewConnection.Close()
            viewConn.Close()
        End Try

    End Function
#End Region
#Region "Select address"

    Public Function GetAddress() As DataSet

        Dim viewConnection As SqlConnection = Nothing
        Dim viewConn As SqlConnection = Nothing
        viewConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        viewConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASADDRESS_VIEW"
        Try
            trans = viewConnection.BeginTransaction()

            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(viewConn, fstrSpName)
            Param(0).Value = Trim(fstrCustomerID)
            Param(1).Value = Trim(fstrCustPrefix)


            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(viewConn, CommandType.StoredProcedure, fstrSpName, Param)
            ' switch status id to status name for display
            'remove the first two first
            ds.Tables(0).Columns.RemoveAt(1)
            ds.Tables(0).Columns.RemoveAt(0)
            Dim count As Integer
            Dim statXmlTr As New clsXml
            Dim strPanm As String
            Dim strPaty As String
            statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            For count = 0 To ds.Tables(0).Rows.Count - 1
                Dim addressty As String = ds.Tables(0).Rows(count).Item(0).ToString
                Dim statusid As String = ds.Tables(0).Rows(count).Item(1).ToString 'item(3) is status
                strPanm = statXmlTr.GetLabelName("StatusMessage", statusid)
                strPaty = statXmlTr.GetLabelName("StatusMessage", addressty)
                ds.Tables(0).Rows(count).Item(0) = strPaty
                ds.Tables(0).Rows(count).Item(1) = strPanm
            Next
            GetAddress = ds
            viewConnection.Dispose()
            viewConn.Dispose()


        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsAddress.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsAddress.vb")
        Finally
          
        End Try

    End Function
#End Region
#Region "confirm no duplicated addresstype"
    Public Function GetDupaddresstype() As Integer
        Dim viewConnection As SqlConnection = Nothing
        Dim viewConn As SqlConnection = Nothing
        viewConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        viewConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASADDRESS_IFDUP"
        Try
            trans = viewConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(3) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(viewConn, fstrSpName)
            Param(0).Value = Trim(fstrAddressType)
            Param(1).Value = Trim(fstrCustomerID)

            Param(2).Value = Trim(fstrCustPrefix)

            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(viewConn, CommandType.StoredProcedure, fstrSpName, Param)
            If dr.Read() Then
                dr.Close()
                viewConnection.Dispose()
                'viewConn.Dispose()
                Return -1
            End If
            viewConnection.Dispose()
            'viewConn.Dispose()
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsAddress.vb")
            Return -1
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsAddress.vb")
            Return -1
        Finally

        End Try
    End Function
#End Region

#Region "confirm no duplicated all address and address type for different customer ID"
    Public Function GetDuplicateAddress() As Integer
        Dim viewConnection As SqlConnection = Nothing
        Dim viewConn As SqlConnection = Nothing
        viewConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        viewConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASADDRESS_DUP"
        Try
            trans = viewConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(10) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(viewConn, fstrSpName)
            Param(0).Value = Trim(fstrAddressType)
            Param(1).Value = Trim(fstrAddress1)
            Param(2).Value = Trim(fstrAddress2)
            Param(3).Value = Trim(fstrCountryID)
            Param(4).Value = Trim(fstrStateID)
            Param(5).Value = Trim(fstrAreaID)
            Param(6).Value = Trim(fstrPOCode)
            Param(7).Value = Trim(fstrCustomerID)
            Param(8).Value = ""

            Dim dr As Integer = SqlHelper.ExecuteNonQuery(viewConn, CommandType.StoredProcedure, fstrSpName, Param)

            fstrExistCustID = Param(8).Value
            If fstrExistCustID <> "" Then
                GetDuplicateAddress = -1
            Else : GetDuplicateAddress = 0
            End If

            viewConnection.Dispose()
            viewConn.Dispose()

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsAddress.vb")
            Return -1
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsAddress.vb")
            Return -1
        Finally
            
        End Try
    End Function
#End Region

#Region "Select address By ID"

    Public Function GetaddressDetailsByID() As SqlDataReader
        Dim viewConnection As SqlConnection = Nothing
        Dim viewConn As SqlConnection = Nothing
        viewConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        viewConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASADDRESS_SELBYID"
        Try
            trans = viewConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(3) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(viewConn, fstrSpName)
            Param(0).Value = Trim(fstrCustomerID)
            Param(1).Value = Trim(fstrAddressType)
            Param(2).Value = Trim(fstrCustPrefix)
            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(viewConn, CommandType.StoredProcedure, fstrSpName, Param)
            GetaddressDetailsByID = dr
            viewConnection.Dispose()
            'viewConn.Dispose()

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            'Return ErrorLog

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            'Return ErrorLog
        End Try
    End Function
#End Region
#Region "confirm no duplicated address"
    Public Function GetDupaddress() As Integer
        Dim viewConnection As SqlConnection = Nothing
        Dim viewConn As SqlConnection = Nothing
        viewConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        viewConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASADDRESS_AIFDUP"
        Try
            trans = viewConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(8) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(viewConn, fstrSpName)

            Param(0).Value = Trim(fstrCustomerID)

            Param(1).Value = Trim(fstrCustPrefix)
            Param(2).Value = Trim(fstrAddress1)
            Param(3).Value = Trim(fstrAddress2)
            Param(4).Value = Trim(fstrCountryID)
            Param(5).Value = Trim(fstrStateID)
            Param(6).Value = Trim(fstrAreaID)
            Param(7).Value = Trim(fstrAddressType)

            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(viewConn, CommandType.StoredProcedure, fstrSpName, Param)
            If dr.Read() Then
                dr.Close()
                viewConnection.Dispose()
                'viewConn.Dispose()
                Return -1
            End If

            viewConnection.Dispose()
            'viewConn.Dispose()
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsAddress.vb")
            Return -1
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsAddress.vb")
            Return -1
        Finally
            
        End Try
    End Function
#End Region
#End Region
End Class
