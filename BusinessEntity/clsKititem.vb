Imports SQLDataAccess
Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Web
Imports System.Data


#Region " Module Amment Hisotry "
'Description     :  the country entity
'History         :  
'Modified Date   :  2006-04-4
'Version         :  v1.0
'Author          :  Woods Wong


#End Region

Public Class clsKititem
    Private strConn As String

#Region "Declaration"

    Private fstrKitiD As String ' kit id
    Private fstrPartID As String ' part id
    Private fstrKitQuantity As Integer ' KIT Quantity
    Private fstrStatus As String '  status
    Private fstrCreatBy As String
    Private fstrCreatDate As System.DateTime
    Private fstrMifyBy As String
    Private fstrMifyDate As System.DateTime
    Private fstrSpName As String ' store procedure name
    Private fstrlogsercenter As String
    Private fstripadress As String
    Private fstrCountryID As String ' country id
#End Region

#Region "Properties"

    Public Property KitiD() As String
        Get
            Return fstrKitiD
        End Get
        Set(ByVal Value As String)
            fstrKitiD = Value
        End Set
    End Property

    Public Property PartID() As String
        Get
            Return fstrPartID
        End Get
        Set(ByVal Value As String)
            fstrPartID = Value
        End Set
    End Property


    Public Property KitQuantity() As String
        Get
            Return fstrKitQuantity
        End Get
        Set(ByVal Value As String)
            fstrKitQuantity = Value
        End Set
    End Property

    Public Property Status() As String
        Get
            Return fstrStatus
        End Get
        Set(ByVal Value As String)
            fstrStatus = Value
        End Set
    End Property
   
    Public Property CreateBy() As String
        Get
            Return fstrCreatBy
        End Get
        Set(ByVal Value As String)
            fstrCreatBy = Value
        End Set
    End Property

    Public Property CreateDate() As String
        Get
            Return fstrCreatDate
        End Get
        Set(ByVal Value As String)
            fstrCreatDate = Value
        End Set
    End Property

    Public Property ModifyBy() As String
        Get
            Return fstrMifyBy
        End Get
        Set(ByVal Value As String)
            fstrMifyBy = Value
        End Set
    End Property
    Public Property ModifyDate() As String
        Get
            Return fstrMifyDate
        End Get
        Set(ByVal Value As String)
            fstrMifyDate = Value
        End Set
    End Property
    Public Property logsercenter() As String
        Get
            Return fstrlogsercenter
        End Get
        Set(ByVal Value As String)
            fstrlogsercenter = Value
        End Set
    End Property
    Public Property ipadress() As String
        Get
            Return fstripadress
        End Get
        Set(ByVal Value As String)
            fstripadress = Value
        End Set
    End Property

    Public Property CountryID() As String
        Get
            Return fstrCountryID
        End Get
        Set(ByVal Value As String)
            fstrCountryID = Value
        End Set
    End Property
#End Region

#Region "Methods"

#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)

        connection.Open()

        Return connection
    End Function
#End Region

#Region "Insert"

    Public Function Insert() As Integer

        Dim insConnection As SqlConnection = Nothing
        Dim insConn As SqlConnection = Nothing
        insConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        insConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASKIT_Add"
        Try
            trans = insConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(7) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(insConn, fstrSpName)
            Param(0).Value = Trim(fstrKitiD)
            Param(1).Value = Trim(fstrPartID)
            Param(2).Value = Trim(fstrKitQuantity)
            Param(3).Value = Trim(fstrStatus)
            Param(4).Value = Trim(fstrCreatBy) '创建者
            Param(5).Value = Trim(fstrMifyBy) '最后修改者
            Param(6).Value = Trim(fstrCountryID) 'country id
            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(insConn, CommandType.StoredProcedure, fstrSpName, Param)

            trans.Commit()
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsKititem.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsKititem.vb")
            Return -1
        Finally
            insConnection.Close()
            insConn.Close()
        End Try

    End Function

#End Region

#Region "Update"

    Public Function Update() As Integer

        Dim UpdConnection As SqlConnection = Nothing
        Dim UpdConn As SqlConnection = Nothing
        UpdConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        UpdConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASKIT_UPD"
        Try
            trans = UpdConnection.BeginTransaction()
            Dim updParam() As SqlParameter = New SqlParameter(8) {}
            updParam = SqlHelperParameterCache.GetSpParameterSet(UpdConn, fstrSpName)
            updParam(0).Value = Trim(fstrKitiD)
            updParam(1).Value = Trim(fstrPartID)
            updParam(2).Value = Trim(fstrKitQuantity)
            updParam(3).Value = Trim(fstrStatus)
            updParam(4).Value = Trim(fstrMifyBy)
            updParam(5).Value = Trim(fstrlogsercenter)
            updParam(6).Value = Trim(fstripadress)
            updParam(7).Value = Trim(fstrCountryID)
          
            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(UpdConn, CommandType.StoredProcedure, fstrSpName, updParam)

            trans.Commit()
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "clsKititem.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "clsKititem.vb")
            Return -1
        Finally
            UpdConnection.Close()
            UpdConn.Close()
        End Try

    End Function

#End Region



#Region "Select KITITEM By ID"

    Public Function GetKitByID() As ArrayList
        Dim retnArray As ArrayList = New ArrayList()
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASKIT_SelByID"
        Try
            trans = sidConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(3) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstrPartID)
            Param(1).Value = Trim(fstrKitiD)
            Param(2).Value = Trim(fstrCountryID)


            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            If dr.Read() Then
                Dim count As Integer
                For count = 0 To dr.FieldCount - 1
                    retnArray.Add(dr.GetValue(count).ToString())
                Next
            End If
            dr.Close()
            Return retnArray

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsKititem.vb")
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsKititem.vb")
        Finally
            sidConnection.Close()
            sidConn.Close()
        End Try
    End Function
#End Region

#Region "confirm no duplicated Record"
    Public Function GetDuplicatedkititem() As Integer
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASKIT_Check"
        Try
            trans = sidConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(3) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstrKitiD)
            Param(1).Value = Trim(fstrPartID)
            Param(2).Value = Trim(fstrCountryID)

            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            If dr.Read() Then
                dr.Close()
                Return -1
            End If
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsKititem.vb")
            Return -1
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsKititem.vb")
            Return -1
        Finally
            sidConnection.Close()
            sidConn.Close()
        End Try
    End Function
#End Region

#Region "confirm whether update qty and deleted status or not"
    Public Function Getupdateqty() As Integer
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASKIT_QTYupdate"
        Try
            trans = sidConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstrKitiD)
            Param(1).Value = Trim(fstrCountryID)

            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            If dr.Read() Then
                dr.Close()
                Return -1
            End If
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsKititem.vb")
            Return -1
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsKititem.vb")
            Return -1
        Finally
            sidConnection.Close()
            sidConn.Close()
        End Try
    End Function
#End Region

#End Region

End Class
