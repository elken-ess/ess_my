Imports SQLDataAccess
Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Web
Imports System.Data
#Region " Module Amment Hisotry "
'Description     :  the address record
'History         :  
'Modified Date   :  2006-04-21
'Version         :  v1.0
'Author          :  朱文姿


#End Region
Public Class clsContact
#Region "Declaration"

    Private fstrSpName As String ' store procedure name
    Private fstrTelephType As String 'Telephone Type
    Private fstrCustPrefix As String ' Customer Prefix
    Private fstrCustomerID As String ' Customer ID

    'Private fstrRoSerialNo As String ' Ro serial No
    Private fstrPICName As String ' Person In Charge Name
    Private fstrTelephone As String ' Telephone
    Private fstrEmail As String ' Email
    
    Private fstrCreatedBy As String ' Created By
    Private fstrModifiedBy As String ' contact
    Private fstrStatus As String ' Status
    Private fintrouid As Integer ' ROUID
    Private fstrroumod As String ' ROModel Type
    Private fstrrouser As String ' ROSerial No
    Private fstrusername As String ' username
    Private fstripadress As String ' ipadress

    Private fstrsvrcid As String ' svrcid
#End Region
#Region "Properties"
    Public Property Ipadress() As String
        Get
            Return fstripadress
        End Get
        Set(ByVal Value As String)
            fstripadress = Value
        End Set
    End Property
    'Public Property Random() As String
    '    Get
    '        Return fstrrandom
    '    End Get
    '    Set(ByVal Value As String)
    '        fstrrandom = Value
    '    End Set
    'End Property
    Public Property Svrcid() As String
        Get
            Return fstrsvrcid
        End Get
        Set(ByVal Value As String)
            fstrsvrcid = Value
        End Set
    End Property

    Public Property username() As String
        Get
            Return fstrusername
        End Get
        Set(ByVal Value As String)
            fstrusername = Value
        End Set
    End Property
    Public Property CustomerID() As String
        Get
            Return fstrCustomerID
        End Get
        Set(ByVal Value As String)
            fstrCustomerID = Value
        End Set
    End Property

    Public Property CustPrefix() As String
        Get
            Return fstrCustPrefix
        End Get
        Set(ByVal Value As String)
            fstrCustPrefix = Value
        End Set
    End Property
    Public Property Status() As String
        Get
            Return fstrStatus
        End Get
        Set(ByVal Value As String)
            fstrStatus = Value
        End Set
    End Property
   
    Public Property RouID() As Integer
        Get
            Return fintrouid
        End Get
        Set(ByVal Value As Integer)
            fintrouid = Value
        End Set
    End Property

    Public Property PICName() As String
        Get
            Return fstrPICName
        End Get
        Set(ByVal Value As String)
            fstrPICName = Value
        End Set
    End Property

    Public Property Telephone() As String
        Get
            Return fstrTelephone
        End Get
        Set(ByVal Value As String)
            fstrTelephone = Value
        End Set
    End Property
    Public Property TelephType() As String
        Get
            Return fstrTelephType
        End Get
        Set(ByVal Value As String)
            fstrTelephType = Value
        End Set
    End Property
    Public Property Email() As String
        Get
            Return fstrEmail
        End Get
        Set(ByVal Value As String)
            fstrEmail = Value
        End Set
    End Property
   
    Public Property CreatedBy() As String
        Get
            Return fstrCreatedBy
        End Get
        Set(ByVal Value As String)
            fstrCreatedBy = Value
        End Set
    End Property
    Public Property ModifiedBy() As String
        Get
            Return fstrModifiedBy
        End Get
        Set(ByVal Value As String)
            fstrModifiedBy = Value
        End Set
    End Property
    Public Property ModelType() As String
        Get
            Return fstrroumod
        End Get
        Set(ByVal Value As String)
            fstrroumod = Value
        End Set
    End Property
    Public Property SerialNo() As String
        Get
            Return fstrrouser
        End Get
        Set(ByVal Value As String)
            fstrrouser = Value
        End Set
    End Property

#End Region
#Region "Meth"
#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)

        connection.Open()

        Return connection
    End Function
#End Region

#Region "select by telephone type"
    Function GetExistingTelephoneType() As DataSet
        Dim viewConn As SqlConnection = Nothing
        viewConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        fstrSpName = "BB_MASCUST_SelTelType"

        Try
            'define search fields 
            Dim Param() As SqlParameter = New SqlParameter(3) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(viewConn, fstrSpName)
            Param(0).Value = Trim(fstrCustomerID)
            Param(1).Value = Trim(fstrCustPrefix)
            Param(2).Value = Trim(fstrTelephType)

            Dim dst As DataSet = New DataSet()

            dst = SqlHelper.ExecuteDataset(viewConn, CommandType.StoredProcedure, fstrSpName, Param)
            GetExistingTelephoneType = dst
            viewConn.Dispose()

        Catch ex As Exception

            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsCustomerRecord.vb")

        Catch ex As SqlException

            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsCustomerRecord.vb")
        End Try
    End Function
#End Region
#Region "Select contact"

    Public Function Getcontact() As DataSet

        Dim viewConnection As SqlConnection = Nothing
        Dim viewConn As SqlConnection = Nothing
        viewConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        viewConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASCONTACT_VIEW"
        Try
            trans = viewConnection.BeginTransaction()
            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(viewConn, fstrSpName)
            Param(0).Value = Trim(fstrCustomerID)
            Param(1).Value = Trim(fstrCustPrefix)
            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(viewConn, CommandType.StoredProcedure, fstrSpName, Param)
            ' switch status id to status name for display
            Dim count As Integer
            Dim statXmlTr As New clsXml
            Dim strPanm As String
            Dim strPaty As String
            ds.Tables(0).Columns.RemoveAt(1)
            ds.Tables(0).Columns.RemoveAt(0)
            statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            For count = 0 To ds.Tables(0).Rows.Count - 1
                Dim addressty As String = ds.Tables(0).Rows(count).Item(0).ToString
                Dim statusid As String = ds.Tables(0).Rows(count).Item(1).ToString 'item(3) is status
                Dim teleph As String = ds.Tables(0).Rows(count).Item(3).ToString
                strPanm = statXmlTr.GetLabelName("StatusMessage", statusid)
                strPaty = statXmlTr.GetLabelName("StatusMessage", addressty)
                ds.Tables(0).Rows(count).Item(0) = strPaty
                ds.Tables(0).Rows(count).Item(1) = strPanm
                Dim telepass As New clsCommonClass()
                ds.Tables(0).Rows(count).Item(3) = telepass.passconverttel(teleph)
            Next

            Getcontact = ds
            viewConnection.Dispose()
            viewConn.Dispose()


        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsContact.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsContact.vb")
        Finally
            
        End Try

    End Function

    Public Function GetCustContacts(ByVal CustID As String, ByVal CustPF As String) As DataSet
        Dim viewConn As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim myCmd As SqlDataAdapter = New SqlDataAdapter("BB_MASCONTACT_VIEW", viewConn)

        myCmd.SelectCommand.CommandType = CommandType.StoredProcedure

        Dim prmCID As SqlParameter = New SqlParameter("@CUSTID", SqlDbType.VarChar, 10)
        prmCID.Value = CustID
        myCmd.SelectCommand.Parameters.Add(prmCID)

        Dim prmPIF As SqlParameter = New SqlParameter("@CUSTPF", SqlDbType.VarChar, 10)
        prmPIF.Value = CustPF
        myCmd.SelectCommand.Parameters.Add(prmPIF)

        Dim myDataset As New DataSet
        myCmd.Fill(myDataset)
        GetCustContacts = myDataset

        viewConn.Dispose()


    End Function

#End Region
#Region "confirm no duplicated contacttype"
    Public Function GetDupcontacttype() As Integer
        Dim viewConnection As SqlConnection = Nothing
        Dim viewConn As SqlConnection = Nothing
        viewConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        viewConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASCONTACT_IFDUP"
        Try
            trans = viewConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(3) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(viewConn, fstrSpName)
            Param(0).Value = Trim(fstrTelephType)
            Param(1).Value = Trim(fstrCustomerID)

            Param(2).Value = Trim(fstrCustPrefix)

            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(viewConn, CommandType.StoredProcedure, fstrSpName, Param)
            If dr.Read() Then
                dr.Close()
                viewConnection.Dispose()
                'viewConn.Dispose()
                Return -1
            End If
            viewConnection.Dispose()
            'viewConn.Dispose()
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsContact.vb")
            Return -1
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsContact.vb")
            Return -1
        Finally
           
        End Try
    End Function
#End Region
#Region "Insert"

    Public Function Insert() As Integer

        Dim viewConnection As SqlConnection = Nothing
        Dim viewConn As SqlConnection = Nothing
        viewConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        viewConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASCONTACT_ADD"
        Try
            trans = viewConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(10) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(viewConn, fstrSpName)
            Param(0).Value = Trim(fstrTelephType)
            Param(1).Value = Trim(fstrCustPrefix)
            Param(2).Value = Trim(fstrCustomerID)
            Param(3).Value = Trim(fintrouid)
            Param(4).Value = Trim(fstrPICName)
            Param(5).Value = Trim(fstrTelephone)
            Param(6).Value = Trim(fstrEmail)

            Param(7).Value = Trim(fstrStatus)
            Param(8).Value = Trim(fstrCreatedBy) '创建者
            Param(9).Value = Trim(fstrModifiedBy) '最后修改者
            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(viewConn, CommandType.StoredProcedure, fstrSpName, Param)

            trans.Commit()
            viewConnection.Dispose()
            viewConn.Dispose()
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsContact.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsContact.vb")
            Return -1
        Finally

        End Try

    End Function

#End Region
#Region "Select contact By ID"

    Public Function GetcontactDetailsByID() As SqlDataReader
        Dim viewConnection As SqlConnection = Nothing
        Dim viewConn As SqlConnection = Nothing
        viewConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        viewConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASCONTACT_SELBYID"
        Try
            trans = viewConnection.BeginTransaction()
            Dim Param() As SqlParameter = New SqlParameter(4) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(viewConn, fstrSpName)
            Param(0).Value = Trim(fstrCustomerID)
            Param(1).Value = Trim(fstrTelephType)
            Param(2).Value = Trim(fstrCustPrefix)
            'Param(3).Value = Trim(fstrCustPrefix)

            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(viewConn, CommandType.StoredProcedure, fstrSpName, Param)
            GetcontactDetailsByID = dr
            viewConnection.Dispose()
            'viewConn.Dispose()

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            'Return ErrorLog

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            'Return ErrorLog
        End Try
    End Function
#End Region
#Region "Update"

    Public Function Update() As Integer

        Dim viewConnection As SqlConnection = Nothing
        Dim viewConn As SqlConnection = Nothing
        viewConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        viewConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASCONTACT_UPD"
        Try
            trans = viewConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(11) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(viewConn, fstrSpName)

            Param(0).Value = Trim(fstrTelephType)
            Param(1).Value = Trim(fstrCustomerID)
            'Param(2).Value = Trim(fstrCustPrefix)
            Param(2).Value = Trim(fintrouid)
            Param(3).Value = Trim(fstrPICName)
            Param(4).Value = Trim(fstrTelephone)
            Param(5).Value = Trim(fstrEmail)

            Param(6).Value = Trim(fstrStatus)
            'Param(11).Value = Trim(fstrCreatedBy) '创建者
            Param(7).Value = Trim(fstrModifiedBy) '最后修改者
            Param(8).Value = Trim(fstrCustPrefix)
            Param(9).Value = Trim(fstripadress)
            Param(10).Value = Trim(fstrsvrcid)

            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(viewConn, CommandType.StoredProcedure, fstrSpName, Param)

            trans.Commit()
            viewConnection.Dispose()
            viewConn.Dispose()
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "clsContact.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "clsContact.vb")
            Return -1
        Finally
            
        End Try

    End Function


    Public Function Delete() As Integer

        Dim viewConnection As SqlConnection = Nothing
        Dim viewConn As SqlConnection = Nothing
        viewConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        viewConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASCONTACT_UPD"
        Try
            trans = viewConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(11) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(viewConn, fstrSpName)

            Param(0).Value = Trim(fstrTelephType)
            Param(1).Value = Trim(fstrCustomerID)
            'Param(2).Value = Trim(fstrCustPrefix)
            Param(2).Value = Trim(fintrouid)
            Param(3).Value = Trim(fstrPICName)
            Param(4).Value = Trim(fstrTelephone)
            Param(5).Value = Trim(fstrEmail)

            Param(6).Value = "DELETE"
            'Param(11).Value = Trim(fstrCreatedBy) '创建者
            Param(7).Value = Trim(fstrModifiedBy) '最后修改者
            Param(8).Value = Trim(fstrCustPrefix)
            Param(9).Value = Trim(fstripadress)
            Param(10).Value = Trim(fstrsvrcid)

            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(viewConn, CommandType.StoredProcedure, fstrSpName, Param)

            trans.Commit()
            viewConnection.Dispose()
            viewConn.Dispose()
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "clsContact.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "clsContact.vb")
            Return -1
        Finally

        End Try

    End Function

#End Region
#End Region
End Class
