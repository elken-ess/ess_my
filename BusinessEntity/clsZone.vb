﻿Imports Microsoft.VisualBasic
Imports SQLDataAccess
Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Web
Imports System.Data


#Region " Module Amment Hisotry "
'============================================'
'Description     :  the Zone entity
'History         :  
'Modified Date   :  2006-04-29
'Version         :  v1.0
'Author          :  Fan
'============================================'

#End Region

Public Class clsZone

#Region "Declaration"

    Private strUserID As String                         'User Login ID
    Private strSerCenter As String                      'Service Center ID
    Private strApptDate As String              'Appt Date
    Private TotalTech As Integer                        'Total no of Technicians
    Private strMasZoneCode As String                    'Master Zone Code
    Private strNorZoneCode As String
    Private strZoneCode As String
    Private strAreaCode As String                       'Area Code
    Private strCreatBy As String                        'Created By
    Private CreatDate As String                 'Created Date
    Private strModifyBy As String                       'Modified By
    Private ModifyDate As String               'Modified Date 
    Private strSpName As String                         'Store procedure name
    Private strZoneType As String                       'Zone Type

    Private strIPAddress As String
    Private strUserSvcID As String

    Dim selConnection As SqlConnection = Nothing
    Dim selConn As SqlConnection = Nothing
    Dim trans As SqlTransaction = Nothing
#End Region

#Region "Properties"

    Public Property UserID() As String
        Get
            Return strUserID
        End Get
        Set(ByVal value As String)
            strUserID = value
        End Set
    End Property

    Public Property ServiceCenterID() As String
        Get
            Return strSerCenter
        End Get
        Set(ByVal Value As String)
            strSerCenter = Value
        End Set
    End Property
    Public Property ApptDate() As String
        Get
            Return strApptDate
        End Get
        Set(ByVal value As String)
            strApptDate = value
        End Set
    End Property
    Public Property TotalNoofTech() As String
        Get
            Return TotalTech
        End Get
        Set(ByVal value As String)
            TotalTech = value
        End Set
    End Property
    Public Property MasterZoneCode() As String
        Get
            Return strMasZoneCode
        End Get
        Set(ByVal value As String)
            strMasZoneCode = value
        End Set
    End Property
    Public Property NormalZoneCode() As String
        Get
            Return strNorZoneCode
        End Get
        Set(ByVal value As String)
            strNorZoneCode = value
        End Set
    End Property
    Public Property ZoneCode() As String
        Get
            Return strZoneCode
        End Get
        Set(ByVal value As String)
            strZoneCode = value
        End Set
    End Property
    Public Property AreaCode() As String
        Get
            Return strAreaCode
        End Get
        Set(ByVal value As String)
            strAreaCode = value
        End Set
    End Property
    Public Property CreatedBy() As String
        Get
            Return strCreatBy
        End Get
        Set(ByVal Value As String)
            strCreatBy = Value
        End Set
    End Property

    Public Property CreatedDate() As String
        Get
            Return CreatDate
        End Get
        Set(ByVal Value As String)
            CreatDate = Value
        End Set
    End Property

    Public Property ModifiedBy() As String
        Get
            Return strModifyBy
        End Get
        Set(ByVal Value As String)
            strModifyBy = Value
        End Set
    End Property
    Public Property ModifiedDate() As String
        Get
            Return ModifyDate
        End Get
        Set(ByVal Value As String)
            ModifyDate = Value
        End Set
    End Property
    Public Property ZoneType() As String
        Get
            Return strZoneType
        End Get
        Set(ByVal value As String)
            strZoneType = value
        End Set
    End Property

    Public Property IpAddress() As String
        Get
            Return strIPAddress
        End Get
        Set(ByVal Value As String)
            strIPAddress = Value
        End Set
    End Property
    Public Property UserServCenterID() As String
        Get
            Return strUserSvcID
        End Get
        Set(ByVal Value As String)
            strUserSvcID = Value
        End Set
    End Property
#End Region

#Region "Methods"

#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)

        connection.Open()

        Return connection
    End Function
#End Region

#Region "Methods help" '类中public方法的辅助方法
    Private Sub BeginMethod()
        selConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        trans = selConnection.BeginTransaction()
    End Sub
    Private Sub DealException(ByVal ex As Exception)
        trans.Rollback()
        Dim ErrorLog As ArrayList = New ArrayList
        ErrorLog.Add("Error").ToString()
        ErrorLog.Add(ex.Message).ToString()
        Dim WriteErrLog As New clsLogFile()
        WriteErrLog.ErrorLog("zqw", ErrorLog.Item(1).ToString, strSpName, WriteErrLog.ADD, "clsAppointment.vb")
    End Sub
    Private Sub EndMethod()
        selConnection.Close()
        selConn.Close()
    End Sub
#End Region


#Region "Select Total Service Center ID By UserID"

    Public Function GetSerCent() As DataSet

        strSpName = "BB_FNCZONE_SelSerCent"
        Try
            BeginMethod()
            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(1) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, strSpName)
            Param(0).Value = Trim(strUserID).ToUpper()
            Dim ds As DataSet = New DataSet
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, strSpName, Param)
            Return ds
        Catch ex As Exception
            DealException(ex)
            Return Nothing
        Finally
            EndMethod()
        End Try
    End Function
#End Region

#Region "Display Zone Info by Service Center ID , Appt Date and ZoneType"

    Public Function GetZoneInfo() As DataSet

        strSpName = "BB_FNCZONE_View"
        Try
            BeginMethod()

            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(3) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, strSpName)
            Param(0).Value = Trim(strSerCenter).ToUpper()
            Param(1).Value = Trim(strApptDate)
            Param(2).Value = Trim(strZoneType)

            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, strSpName, Param)
            ds.Tables(0).TableName = "TOTCH"
            ds.Tables(1).TableName = "ZoneInfo"
            ds.Tables(1).Columns.Add("AreaIDs", System.Type.GetType("System.String"))

            Dim i As Integer = 0
            Dim j As Integer = 0
            Dim k As Integer = 0
            Dim zoneid As String
            For i = 0 To ds.Tables("ZoneInfo").Rows.Count - 1
                zoneid = ds.Tables("ZoneInfo").Rows(i).Item("FZN1_ZONID").ToString().Trim()
                Dim areas As String = ""
                For j = k To ds.Tables(2).Rows.Count - 1
                    If zoneid <> ds.Tables(2).Rows(j).Item("FZN2_ZONID").ToString().Trim() Then
                        k = j
                        Exit For
                    End If
                    areas = areas + ds.Tables(2).Rows(j).Item("FZN2_AREID").ToString().Trim() + ","
                Next
                If areas <> "" Then
                    areas = areas.Remove(areas.Length - 1)
                End If
                ds.Tables("ZoneInfo").Rows(i).Item("AreaIDs") = areas
            Next
            Return ds
        Catch ex As Exception
            DealException(ex)
            Return Nothing
        Finally
            EndMethod()
        End Try

    End Function

    Public Function CopyMasZoneToNormal() As Integer           'Copy Master Info for Daily Zoning
        strSpName = "BB_FNCZONE_Duplicate"
        Try
            BeginMethod()

            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(6) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, strSpName)
            Param(0).Value = Trim(strUserID).ToUpper()
            Param(1).Value = Trim(strSerCenter).ToUpper()
            Param(2).Value = Trim(strApptDate)
            Param(3).Value = Trim(strMasZoneCode).ToUpper()
            Param(4).Value = Trim(strNorZoneCode).ToUpper()
            SqlHelper.ExecuteNonQuery(selConn, CommandType.StoredProcedure, strSpName, Param)
            Return Param(5).Value 'return value
        Catch ex As Exception
            DealException(ex)
            Return -1
        Finally
            EndMethod()
        End Try
    End Function

    Public Function SaveAsNewMas() As Integer           'Copy Master Info for Daily Zoning
        strSpName = "BB_FNCZONE_SaveNewMas"
        Try
            BeginMethod()

            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(6) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, strSpName)
            Param(0).Value = Trim(strUserID).ToUpper()
            Param(1).Value = Trim(strSerCenter).ToUpper()
            Param(2).Value = Trim(strApptDate)
            Param(3).Value = Trim(strNorZoneCode).ToUpper()
            SqlHelper.ExecuteNonQuery(selConn, CommandType.StoredProcedure, strSpName, Param)
            Return Param(4).Value 'return value
        Catch ex As Exception
            DealException(ex)
            Return -1
        Finally
            EndMethod()
        End Try
    End Function

    Public Function GetDailyZoneInfo() As DataSet

        strSpName = "BB_FNCZONE_SelDailyZonesInfo"
        Try
            BeginMethod()

            Dim Param() As SqlParameter = New SqlParameter(3) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, strSpName)
            Param(0).Value = Trim(strSerCenter).ToUpper()
            Param(1).Value = Trim(strApptDate)
            Param(2).Value = Trim(strZoneType)

            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, strSpName, Param)
            ds.Tables(0).TableName = "AreaInfo"
            ds.Tables(1).TableName = "ZoneInfo" 'edit june 27 
            ds.Tables(2).TableName = "DailyZoneInfo"
            Return ds
        Catch ex As Exception
            DealException(ex)
            Return Nothing
        Finally
            EndMethod()
        End Try

    End Function
    Public Function SaveZoneInfo(ByVal EmptyZones As DataTable, ByVal ZoneInfo As DataTable, ByVal IsSaveAsMaster As String) As Boolean

        Try
            BeginMethod()
            Dim Param() As SqlParameter = New SqlParameter(10) {}
            Dim i As Integer
            Dim ZoneCodeTemp As String
            Dim NowTime As DateTime = DateTime.Now
            ZoneCodeTemp = ""
            strZoneCode = ""
            For i = 0 To ZoneInfo.Rows.Count - 1
                strZoneCode = ZoneInfo.Rows(i).Item("ZoneID")
                If strZoneCode <> ZoneCodeTemp Then
                    SaveZone("BB_FNCZONE_UpdZoneInfo", Param, IsSaveAsMaster, NowTime)
                    ZoneCodeTemp = strZoneCode
                End If
                strAreaCode = ZoneInfo.Rows(i).Item("AreaID")
                SaveArea("BB_FNCZONE_UpdAreaInfo", Param, IsSaveAsMaster, NowTime)
            Next
            'edit june 26
            For i = 0 To EmptyZones.Rows.Count - 1
                strZoneCode = EmptyZones.Rows(i).Item("ZoneID")
                SaveZone("BB_FNCZONE_UpdZoneInfo", Param, IsSaveAsMaster, NowTime)
            Next

            DelZoneInfo("BB_FNCZONE_Del", Param, IsSaveAsMaster, NowTime)
            Return True
        Catch ex As Exception
            DealException(ex)
            Return False
        Finally
            EndMethod()
        End Try

    End Function
    Private Sub SaveZone(ByVal strSpName As String, ByVal Param As SqlParameter(), _
                     ByVal IsSaveAsMaster As String, ByVal NowTime As DateTime)
        Param = SqlHelperParameterCache.GetSpParameterSet(selConn, strSpName)
        Param(0).Value = Trim(strUserID).ToUpper()
        Param(1).Value = Trim(strZoneCode).ToUpper()
        Param(2).Value = Trim(strZoneType).ToUpper()
        Param(3).Value = Trim(strSerCenter).ToUpper()
        Param(4).Value = Trim(strApptDate)
        Param(5).Value = NowTime
        Param(6).Value = Trim(IsSaveAsMaster)
        Param(7).Value = Trim(strIPAddress)
        Param(8).Value = Trim(strUserSvcID).ToUpper()
        SqlHelper.ExecuteNonQuery(selConn, CommandType.StoredProcedure, strSpName, Param)
    End Sub
    Private Sub SaveArea(ByVal strSpName As String, ByVal Param As SqlParameter(), _
                     ByVal IsSaveAsMaster As String, ByVal NowTime As DateTime)
        Param = SqlHelperParameterCache.GetSpParameterSet(selConn, strSpName)
        Param(0).Value = Trim(strUserID).ToUpper()
        Param(1).Value = Trim(strZoneCode).ToUpper()
        Param(2).Value = Trim(strZoneType).ToUpper()
        Param(3).Value = Trim(strSerCenter).ToUpper()
        Param(4).Value = Trim(strApptDate)
        Param(5).Value = NowTime
        Param(6).Value = Trim(IsSaveAsMaster)
        Param(7).Value = Trim(strIPAddress)
        Param(8).Value = Trim(strUserSvcID).ToUpper()
        Param(9).Value = Trim(strAreaCode).ToUpper()
        SqlHelper.ExecuteNonQuery(selConn, CommandType.StoredProcedure, strSpName, Param)
    End Sub
    Private Sub DelZoneInfo(ByVal strSpName As String, ByVal Param As SqlParameter(), _
                     ByVal IsSaveAsMaster As String, ByVal NowTime As DateTime)
        Param = SqlHelperParameterCache.GetSpParameterSet(selConn, strSpName)
        Param(0).Value = Trim(strUserID).ToUpper()
        Param(1).Value = Trim(strZoneType).ToUpper()
        Param(2).Value = Trim(strSerCenter).ToUpper()
        Param(3).Value = Trim(strApptDate)
        Param(4).Value = NowTime
        Param(5).Value = Trim(IsSaveAsMaster)
        Param(6).Value = Trim(strIPAddress)
        Param(7).Value = Trim(strUserSvcID).ToUpper()
        SqlHelper.ExecuteNonQuery(selConn, CommandType.StoredProcedure, strSpName, Param)
    End Sub
#End Region

#End Region

End Class
