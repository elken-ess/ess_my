﻿Imports Microsoft.VisualBasic
Imports SQLDataAccess
Imports BusinessEntity
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Data


#Region "Module Description"
'Description        :   Call List Assignment
'Last Modified Date :   2006-05-16
'Version            :   1.0
'Author             :   BAIYUNPENG
#End Region

Public Class clsCallListAssignment

#Region "Declaration"
    Private strStoreProcedureName As String
    Private strFState As String
    Private strTState As String
    Private strFSC As String
    Private strTSC As String
    Private strAreaID As String
    Private strRemYear As String
    Private strRemMonth As String
    Private strServiceType As String
    Private strSortBy1 As String
    Private strSortBy2 As String
    Private strReleased As String
    Private strAssignTo As String
    Private strMRouID As String
    Private strModiBy As String
    Private strRemOperand As String
    Private strAreaIDTo As String
    Private strfrmDueDate As String
    Private strtoDueDate As String
    Private strClassID As String
    Private strCtryID As String

#End Region

#Region "Properties"
    Public Property AreaIDTo() As String
        Get
            Return strAreaIDTo
        End Get
        Set(ByVal value As String)
            strAreaIDTo = value
        End Set
    End Property


    Public Property ClassID() As String
        Get
            Return strClassID
        End Get
        Set(ByVal value As String)
            strClassID = value
        End Set
    End Property

    Public Property frmDueDate() As String
        Get
            Return strfrmDueDate
        End Get
        Set(ByVal value As String)
            strfrmDueDate = value
        End Set
    End Property

    Public Property toDueDate() As String
        Get
            Return strtoDueDate
        End Get
        Set(ByVal value As String)
            strtoDueDate = value
        End Set
    End Property

    Public Property StateFrom() As String
        Get
            Return strFState
        End Get
        Set(ByVal value As String)
            strFState = value
        End Set
    End Property

    Public Property StateTo() As String
        Get
            Return strTState
        End Get
        Set(ByVal value As String)
            strTState = value
        End Set
    End Property

    Public Property ServiceCenterFrom() As String
        Get
            Return strFSC
        End Get
        Set(ByVal value As String)
            strFSC = value
        End Set
    End Property

    Public Property ServiceCenterTo() As String
        Get
            Return strTSC
        End Get
        Set(ByVal value As String)
            strTSC = value
        End Set
    End Property

    Public Property AreaID() As String
        Get
            Return strAreaID
        End Get
        Set(ByVal value As String)
            strAreaID = value
        End Set
    End Property

    Public Property ReminderYear() As String
        Get
            Return strRemYear
        End Get
        Set(ByVal value As String)
            strRemYear = value
        End Set
    End Property

    Public Property ReminderMonth() As String
        Get
            Return strRemMonth
        End Get
        Set(ByVal value As String)
            strRemMonth = value
        End Set
    End Property

    Public Property ServiceType() As String
        Get
            Return strServiceType
        End Get
        Set(ByVal value As String)
            strServiceType = value
        End Set
    End Property

    Public Property SortByFirst() As String
        Get
            Return strSortBy1
        End Get
        Set(ByVal value As String)
            strSortBy1 = value
        End Set
    End Property

    Public Property SortBySecond() As String
        Get
            Return strSortBy2
        End Get
        Set(ByVal value As String)
            strSortBy2 = value
        End Set
    End Property

    Public Property Released() As String
        Get
            Return strReleased
        End Get
        Set(ByVal value As String)
            strReleased = value
        End Set
    End Property

    Public Property AssignTo() As String
        Get
            Return strAssignTo
        End Get
        Set(ByVal value As String)
            strAssignTo = value
        End Set
    End Property

    Public Property MRouID() As String
        Get
            Return strMRouID
        End Get
        Set(ByVal value As String)
            strMRouID = value
        End Set
    End Property

    Public Property ModiBy() As String
        Get
            Return strModiBy
        End Get
        Set(ByVal value As String)
            strModiBy = value
        End Set
    End Property


    Public Property Operand() As String
        Get
            Return strRemOperand
        End Get
        Set(ByVal value As String)
            strRemOperand = value
        End Set
    End Property

    Public Property CtryID() As String
        Get
            Return strCtryID
        End Get
        Set(ByVal value As String)
            strCtryID = value
        End Set
    End Property

#End Region

#Region "Method"

#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)

        connection.Open()

        Return connection
    End Function
#End Region

#Region "Query Screen Info"

    Public Function PreQuery() As DataSet
        Dim sqlConn As SqlConnection = Nothing
        sqlConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        strStoreProcedureName = "BB_FNCCLAS_PreQuery"
        Dim dsCLA As DataSet = New DataSet
        Try
            Dim paramStorc() As SqlParameter = New SqlParameter(1) {}
            paramStorc = SqlHelperParameterCache.GetSpParameterSet(sqlConn, strStoreProcedureName)
            paramStorc(0).Value = Trim(strModiBy)

            dsCLA = SqlHelper.ExecuteDataset(sqlConn, CommandType.StoredProcedure, strStoreProcedureName, paramStorc)

        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("Call List Assignment", ErrorLog.Item(1).ToString, strStoreProcedureName, WriteErrLog.SELE, "clsCallListAssignment.vb")
            Return Nothing
        Finally
            sqlConn.Close()
        End Try
        Return dsCLA
    End Function

#End Region

#Region "Retrive Info"

    Public Function RetriveAppointmentInfo() As DataSet
        Dim sqlConn As SqlConnection = Nothing
        sqlConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        strStoreProcedureName = "BB_FNCCLAS_View"
        Dim dsCLA As DataSet = New DataSet
        Try
            Dim paramStorc() As SqlParameter = New SqlParameter(15) {}
            paramStorc = SqlHelperParameterCache.GetSpParameterSet(sqlConn, strStoreProcedureName)
            paramStorc(0).Value = Trim(strFState) '
            paramStorc(1).Value = Trim(strTState) '
            paramStorc(2).Value = Trim(strFSC) '
            paramStorc(3).Value = Trim(strTSC) '
            paramStorc(4).Value = Trim(strAreaID) '
            paramStorc(5).Value = Trim(strfrmDueDate)
            paramStorc(6).Value = Trim(strtoDueDate)
            paramStorc(7).Value = Trim(strServiceType) '
            paramStorc(8).Value = Trim(strSortBy1) '
            paramStorc(9).Value = Trim(strSortBy2) '
            paramStorc(10).Value = Trim(strReleased) '
            paramStorc(11).Value = Trim(strModiBy) '
            paramStorc(12).Value = Trim(strAssignTo) '
            'paramStorc(13).Value = Trim(strRemOperand)
            paramStorc(13).Value = Trim(strAreaIDTo) '
            paramStorc(14).Value = Trim(strClassID)

            dsCLA = SqlHelper.ExecuteDataset(sqlConn, CommandType.StoredProcedure, strStoreProcedureName, paramStorc)

        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("Call List Assignment", ErrorLog.Item(1).ToString, strStoreProcedureName, WriteErrLog.SELE, "clsCallListAssignment.vb")
            Return Nothing
        Finally
            sqlConn.Close()
        End Try
        Return dsCLA
    End Function

#End Region

#Region "Save Info"

    Public Function SaveAppointmentInfo() As Integer
        Dim sqlConn As SqlConnection = Nothing
        sqlConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        strStoreProcedureName = "BB_FNCCLAS_Upd"
        Dim iCount As Integer
        Try
            Dim paramStorc() As SqlParameter = New SqlParameter(4) {}
            paramStorc = SqlHelperParameterCache.GetSpParameterSet(sqlConn, strStoreProcedureName)
            paramStorc(0).Value = Trim(strMRouID)
            paramStorc(1).Value = Trim(strReleased)
            paramStorc(2).Value = Trim(strAssignTo)
            paramStorc(3).Value = Trim(strModiBy)

            iCount = SqlHelper.ExecuteNonQuery(sqlConn, CommandType.StoredProcedure, strStoreProcedureName, paramStorc)

        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("Call List Assignment", ErrorLog.Item(1).ToString, strStoreProcedureName, WriteErrLog.SELE, "clsCallListAssignment.vb")
        Finally
            sqlConn.Close()
        End Try
        Return iCount
    End Function

#End Region

#Region "Get State and area from service center"

    Public Function getStateAndArea() As DataSet
        Dim sqlConn As SqlConnection = Nothing
        sqlConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        strStoreProcedureName = "BB_FNCCLAS_SVC_STA_ARE"
        Dim dsCLA As DataSet = New DataSet
        Try
            Dim paramStorc() As SqlParameter = New SqlParameter(1) {}
            paramStorc = SqlHelperParameterCache.GetSpParameterSet(sqlConn, strStoreProcedureName)
            paramStorc(0).Value = Trim(strFSC)
            paramStorc(1).Value = Trim(strCtryID)

            dsCLA = SqlHelper.ExecuteDataset(sqlConn, CommandType.StoredProcedure, strStoreProcedureName, paramStorc)

        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("Call List Assignment", ErrorLog.Item(1).ToString, strStoreProcedureName, WriteErrLog.SELE, "clsCallListAssignment.vb")
            Return Nothing
        Finally
            sqlConn.Close()
        End Try
        Return dsCLA
    End Function

#End Region

#Region "Get area from state"

    Public Function getAreafromState() As DataSet
        Dim sqlConn As SqlConnection = Nothing
        sqlConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        strStoreProcedureName = "BB_FNCCLAS_STA_ARE"
        Dim dsCLA As DataSet = New DataSet
        Try
            Dim paramStorc() As SqlParameter = New SqlParameter(1) {}
            paramStorc = SqlHelperParameterCache.GetSpParameterSet(sqlConn, strStoreProcedureName)
            paramStorc(0).Value = Trim(strFState)
            paramStorc(1).Value = Trim(strFSC)

            dsCLA = SqlHelper.ExecuteDataset(sqlConn, CommandType.StoredProcedure, strStoreProcedureName, paramStorc)

        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("Call List Assignment", ErrorLog.Item(1).ToString, strStoreProcedureName, WriteErrLog.SELE, "clsCallListAssignment.vb")
            Return Nothing
        Finally
            sqlConn.Close()
        End Try
        Return dsCLA
    End Function

#End Region
#End Region


End Class
