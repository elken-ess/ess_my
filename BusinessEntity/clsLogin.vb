﻿Imports SQLDataAccess
Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Web
Imports System.Data


#Region " Module Amment Hisotry "
'Description     :  the country entity
'History         :  
'Modified Date   :  2006-04-11
'Version         :  v1.0
'Author          :  xxl

#End Region
Public Class clsLogin
#Region "Declaration"

    Private fstruserID As String ' serverice center id 
    Private fstrpsw As String ' date
    Private finttime As Integer
    Private fstrchangepsw As String
    Private fstrstafstat As String
    Private fstrname As String
    Private fdtlogintime As Date
    Private fdtlvtime As Date
    Private fstrpcname As String
    Private fstripadd As String
    Private fstrSpName As String ' store procedure name
    ' Added by Ryan Estandarte 3 October 2012
    Private fstrPWStatus As String
#End Region

#Region "Properties"
    Public Property IPAdress() As String
        Get
            Return fstripadd
        End Get
        Set(ByVal Value As String)
            fstripadd = Value
        End Set
    End Property
    Public Property PCName() As String
        Get
            Return fstrpcname
        End Get
        Set(ByVal Value As String)
            fstrpcname = Value
        End Set
    End Property
    Public Property LVTime() As Date
        Get
            Return fdtlvtime
        End Get
        Set(ByVal Value As Date)
            fdtlvtime = Value
        End Set
    End Property
    Public Property LogTime() As Date
        Get
            Return fdtlogintime
        End Get
        Set(ByVal Value As Date)
            fdtlogintime = Value
        End Set
    End Property
    Public Property USNM() As String
        Get
            Return fstrname
        End Get
        Set(ByVal Value As String)
            fstrname = Value
        End Set
    End Property

    Public Property UseID() As String
        Get
            Return fstruserID
        End Get
        Set(ByVal Value As String)
            fstruserID = Value
        End Set
    End Property

    Public Property StafStat() As String
        Get
            Return fstrstafstat
        End Get
        Set(ByVal Value As String)
            fstrstafstat = Value
        End Set
    End Property

    Public Property ChangePSW() As String
        Get
            Return fstrchangepsw
        End Get
        Set(ByVal Value As String)
            fstrchangepsw = Value
        End Set
    End Property
    Public Property PassWord() As String
        Get
            Return fstrpsw
        End Get
        Set(ByVal Value As String)
            fstrpsw = Value
        End Set
    End Property
    Public Property UseFLT() As Integer
        Get
            Return finttime
        End Get
        Set(ByVal Value As Integer)
            finttime = Value
        End Set
    End Property

    ''' <remarks>Added by Ryan Estandarte 3 October 2012</remarks>
    Public Property PwStatus() As String
        Get
            Return fstrPWStatus
        End Get
        Set(ByVal value As String)
            fstrPWStatus = value
        End Set
    End Property

#End Region
#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)
        Try
            connection.Open()
        Catch ex As Exception
            GetConnection = Nothing
        End Try


        Return connection
    End Function


    Public Function CheckConnection(ByVal connectionString As String) As String

        Try
            If GetConnection(connectionString) IsNot Nothing Then
                CheckConnection = ""
            End If
        Catch ex As Exception
            CheckConnection = ex.Message

        End Try
    End Function
#End Region




#Region "Update"
    Public Function Update() As Integer

        Dim UpdConnection As SqlConnection = Nothing
        Dim UpdConn As SqlConnection = Nothing
        UpdConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        UpdConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASLOG_UPD"
        Try
            trans = UpdConnection.BeginTransaction()
            'Modified by Ryan Estandarte 3 Oct 2012
            'Dim Param() As SqlParameter = New SqlParameter(4) {}
            Dim Param() As SqlParameter = New SqlParameter(5) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(UpdConn, fstrSpName)
            Param(0).Value = Trim(fstruserID)
            Param(1).Value = Trim(finttime)
            Param(2).Value = Trim(fstrchangepsw)
            Param(3).Value = Trim(fstrpsw)
            Param(4).Value = Trim(fstrPWStatus)
            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(UpdConn, CommandType.StoredProcedure, fstrSpName, Param)
            trans.Commit()

            UpdConnection.Dispose()
            UpdConn.Dispose()

            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrname, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "clsLogin.vb")
            Return -1
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrname, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "clsLogin.vb")
            Return -1
        End Try
    End Function

#End Region

#Region "Update HAVE NO CHANGE "
    Public Function UpdateStat() As Integer

        Dim UpdConnection As SqlConnection = Nothing
        Dim UpdConn As SqlConnection = Nothing
        UpdConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        UpdConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASLOG_UPD2"
        Try
            trans = UpdConnection.BeginTransaction()
            Dim Param() As SqlParameter = New SqlParameter(3) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(UpdConn, fstrSpName)
            Param(0).Value = Trim(fstruserID)
            Param(1).Value = Trim(finttime)
            Param(2).Value = Trim(fstrstafstat)

            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(UpdConn, CommandType.StoredProcedure, fstrSpName, Param)
            trans.Commit()


            UpdConnection.Dispose()
            UpdConn.Dispose()

            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrname, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "clsLogin.vb")
            Return -1
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrname, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "clsLogin.vb")
            Return -1
        End Try
    End Function

#End Region
#Region "Update HAVE NO CHANGE "
    Public Function updateflog() As Integer

        Dim UpdConnection As SqlConnection = Nothing
        Dim UpdConn As SqlConnection = Nothing
        UpdConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        UpdConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASLOG_UPDnew"
        Try
            trans = UpdConnection.BeginTransaction()
            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(UpdConn, fstrSpName)
            Param(0).Value = Trim(fstruserID)
            Param(1).Value = Trim(finttime)

            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(UpdConn, CommandType.StoredProcedure, fstrSpName, Param)
            trans.Commit()


            UpdConnection.Dispose()
            UpdConn.Dispose()

            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrname, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "clsLogin.vb")
            Return -1
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrname, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "clsLogin.vb")
            Return -1
        End Try
    End Function

#End Region
#Region "add ssya table "
    Public Function UpdateSSYA_Add() As Integer

        Dim UpdConnection As SqlConnection = Nothing
        Dim UpdConn As SqlConnection = Nothing
        UpdConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        UpdConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_LOGSSYA_ADD"
        Try
            trans = UpdConnection.BeginTransaction()
            Dim Param() As SqlParameter = New SqlParameter(5) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(UpdConn, fstrSpName)
            Param(0).Value = Trim(fstruserID)
            Param(1).Value = Trim(fdtlogintime)
            Param(2).Value = Trim(fdtlvtime)
            Param(3).Value = Trim(fstrpcname)
            Param(4).Value = Trim(fstripadd)
            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(UpdConn, CommandType.StoredProcedure, fstrSpName, Param)
            trans.Commit()


            UpdConnection.Dispose()
            UpdConn.Dispose()

            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrname, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "clsLogin.vb")
            Return -1
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrname, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "clsLogin.vb")
            Return -1
        End Try
    End Function

#End Region

#Region "update  ssya table "
    Public Function UpdateSSYA_Update() As Integer

        Dim UpdConnection As SqlConnection = Nothing
        Dim UpdConn As SqlConnection = Nothing
        UpdConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        UpdConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_LOGSSYA_LogUPD"
        Try
            trans = UpdConnection.BeginTransaction()
            Dim Param() As SqlParameter = New SqlParameter(5) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(UpdConn, fstrSpName)
            Param(0).Value = Trim(fstruserID)
            Param(1).Value = Trim(fdtlogintime)
            Param(2).Value = Trim(fdtlvtime)
            Param(3).Value = Trim(fstrpcname)
            Param(4).Value = Trim(fstripadd)
            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(UpdConn, CommandType.StoredProcedure, fstrSpName, Param)
            trans.Commit()

            UpdConnection.Dispose()
            UpdConn.Dispose()

            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrname, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "clsLogin.vb")
            Return -1
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrname, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "clsLogin.vb")
            Return -1
        End Try
    End Function

#End Region
#Region "login check company and service center  status "
    Public Function checkNameStat() As SqlDataReader
        'Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        'sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        'Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASLOG_STAT"
        Try
            'trans = sidConnection.BeginTransaction()
            Dim Param() As SqlParameter = New SqlParameter(1) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstruserID)
            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)

            'sidConnection.Dispose()
            Return dr
        Catch ex As SqlException
            'trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrname, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsLogin.vb")
        Catch ex As Exception
            'trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrname, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsLogin.vb")


        End Try



    End Function
#End Region

#Region "login check  psw "
    Public Function checkNamePSW() As SqlDataReader
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        'Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASLOG_CHECKNew"
        Try
            'trans = sidConnection.BeginTransaction()
            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstruserID)
            Param(1).Value = Trim(fstrpsw)
            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)

            sidConnection.Dispose()
            Return dr
        Catch ex As SqlException
            'trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstruserID, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsLogin.vb")
        Catch ex As Exception
            'trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstruserID, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsLogin.vb")
        End Try
    End Function

    ' generate random number
    Function GenerateSessionRandomNumber() As String
        Dim lstrRandomNo As String = ""
        Dim intUpperBound, intLowerBound, intRangeSize As Integer
        ' Initialize the random number generator.
        ' Randomize can actually take parameters telling it how to initialize
        ' things, but for the most you'll just want to call it without passing
        ' it anything.
        Randomize()

        ' Generate our random number.
        ' The Rnd function does most of the work.  It returns a value in the
        ' range 0 <= value < 1 so to generate a random integer in the specified
        ' range we need to do some calculation.  Specifically we take the size
        ' of the range in which we want to generate the number (add 1 so the
        ' upper bound can be generated!) and then multiply it by our random
        ' element.  Then to place the value into the correct range of numbers
        ' we add the lower bound.  Finally we truncate the number leaving us
        ' with the integer portion which is always somewhere between the
        ' lower bound and upper bound (inclusively).

        ' Find range size

        intRangeSize = 100000
        intUpperBound = intRangeSize
        intLowerBound = 1

        intRangeSize = intUpperBound - intLowerBound + 1

        ' Get a random number from 0 to the size of the range
        lstrRandomNo = intRangeSize * Rnd()

        ' Center the range of possible random numbers over the desired result set
        lstrRandomNo = lstrRandomNo + Val(intLowerBound)


        GenerateSessionRandomNumber = Int(lstrRandomNo)
    End Function

#End Region

    '#Region "加密函数"
    '    Function EncryptPassword(ByVal password As String, ByVal passwordformate As String)
    '        If passwordformate = "sha1" Then
    '            EncryptPassword = FormsAuthentication.HashPasswordForStoringInConfigFile(password, "sha1")
    '        ElseIf passwordformate = "md5" Then
    '            EncryptPassword = FormsAuthentication.HashPasswordForStoringInConfigFile(password, "md5")
    '        End If
    '    End Function
    '#End Region
End Class
