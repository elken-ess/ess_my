Imports SQLDataAccess
Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Web
Imports System.Data


#Region " Module Amment Hisotry "
'Description     :  the SerType1 entity
'History         :  
'Modified Date   :  2006-04-10
'Version         :  v1.0
'Author          :  CM


#End Region

Public Class clsSerType
    Private strConn As String

#Region "Declaration"

    Private fstrSerTypeID As String ' SerTypeID
    Private fstrSerTypeName As String ' SerType name
    Private fstrSerTypeAlternateName As String ' alternate SerType  name
    Private fstrSerTypeStatus As String 'SerType status
    Private fstrCountryID As String ' country currency
    Private fstrSerType As String ' country currency
    Private fstrModelID As String 'ModelID
    Private fstrFrequency As Integer  'Frequency
    Private fstrNoOfYears As Integer 'No of Years
    Private fstrReReSlip As String 'Required Reminder slip
    Private fstrPointCR As Decimal  'Point (CR)
    Private fstrPointTe As Decimal 'Point (Technician)
    Private fstrReStDate As String 'Reference Start Date
    Private fstrUpLaSeDate As String 'Update Last Service Date
    Private fdateEffectiveDate As System.DateTime 'EffectiveDate 
    Private fdateObsoleteDate As System.DateTime 'ObsoleteDate
    Private fstrCreatedBy As String ' CreatedBy
    Private fdateCreatedDate As System.DateTime ' CreatedDate 
    Private fstrModifiedBy As String 'ModifiedBy
    Private fdateModifiedDate As System.DateTime 'ModifiedDate
    Private fintDuring As Integer   ' During
    Private fintRMDAT As Integer
    Private fstrSpName As String ' store  procedure name
    Private fstrrank As String ' 
    Private fstrsvcid As String
    Private fstrip As String
#End Region

#Region "Properties"
    Public Property RMDAT() As Integer
        Get
            Return fintRMDAT
        End Get
        Set(ByVal Value As Integer)
            fintRMDAT = Value
        End Set
    End Property

    Public Property During() As String
        Get
            Return fintDuring
        End Get
        Set(ByVal Value As String)
            fintDuring = Value
        End Set
    End Property
    Public Property SerTypeID() As String
        Get
            Return fstrSerTypeID
        End Get
        Set(ByVal Value As String)
            fstrSerTypeID = Value
        End Set
    End Property

    Public Property SerTypeName() As String
        Get
            Return fstrSerTypeName
        End Get
        Set(ByVal Value As String)
            fstrSerTypeName = Value
        End Set
    End Property


    Public Property AlternateName() As String
        Get
            Return fstrSerTypeAlternateName
        End Get
        Set(ByVal Value As String)
            fstrSerTypeAlternateName = Value
        End Set
    End Property

    Public Property SerTypeStatus() As String
        Get
            Return fstrSerTypeStatus
        End Get
        Set(ByVal Value As String)
            fstrSerTypeStatus = Value
        End Set
    End Property
    Public Property CountryID() As String
        Get
            Return fstrCountryID
        End Get
        Set(ByVal Value As String)
            fstrCountryID = Value
        End Set
    End Property

    Public Property SerType() As String
        Get
            Return fstrSerType
        End Get
        Set(ByVal Value As String)
            fstrSerType = Value
        End Set
    End Property
    Public Property ModelID() As String
        Get
            Return fstrModelID
        End Get
        Set(ByVal Value As String)
            fstrModelID = Value
        End Set
    End Property
    Public Property Frequency() As String
        Get
            Return fstrFrequency
        End Get
        Set(ByVal Value As String)
            fstrFrequency = Value
        End Set
    End Property
    Public Property NoOfYears() As String
        Get
            Return fstrNoOfYears
        End Get
        Set(ByVal Value As String)
            fstrNoOfYears = Value
        End Set
    End Property
    Public Property ReReSlip() As String
        Get
            Return fstrReReSlip
        End Get
        Set(ByVal Value As String)
            fstrReReSlip = Value
        End Set
    End Property
    Public Property PointCR() As Decimal
        Get
            Return fstrPointCR
        End Get
        Set(ByVal Value As Decimal)
            fstrPointCR = Value
        End Set
    End Property
    Public Property PointTe() As Decimal
        Get
            Return fstrPointTe
        End Get
        Set(ByVal Value As Decimal)
            fstrPointTe = Value
        End Set
    End Property
    Public Property ReStDate() As String
        Get
            Return fstrReStDate
        End Get
        Set(ByVal Value As String)
            fstrReStDate = Value
        End Set
    End Property
    Public Property UpLaSeDate() As String
        Get
            Return fstrUpLaSeDate
        End Get
        Set(ByVal Value As String)
            fstrUpLaSeDate = Value
        End Set
    End Property
   
    Public Property EffectiveDate() As String
        Get
            Return fdateEffectiveDate
        End Get
        Set(ByVal Value As String)
            fdateEffectiveDate = Value
        End Set
    End Property

    Public Property ObsoleteDate() As String
        Get
            Return fdateObsoleteDate
        End Get
        Set(ByVal Value As String)
            fdateObsoleteDate = Value
        End Set
    End Property

    Public Property CreatedBy() As String
        Get
            Return fstrCreatedBy
        End Get
        Set(ByVal Value As String)
            fstrCreatedBy = Value
        End Set
    End Property
    Public Property CreatedDate() As String
        Get
            Return fdateCreatedDate
        End Get
        Set(ByVal Value As String)
            fdateCreatedDate = Value
        End Set
    End Property
    Public Property ModifiedBy() As String
        Get
            Return fstrModifiedBy
        End Get
        Set(ByVal Value As String)
            fstrModifiedBy = Value
        End Set
    End Property
    Public Property ModifiedDate() As String
        Get
            Return fdateModifiedDate
        End Get
        Set(ByVal Value As String)
            fdateModifiedDate = Value
        End Set
    End Property
    Public Property rank() As String
        Get
            Return fstrrank
        End Get
        Set(ByVal Value As String)
            fstrrank = Value
        End Set
    End Property
    Public Property svcid() As String
        Get
            Return fstrsvcid
        End Get
        Set(ByVal Value As String)
            fstrsvcid = Value
        End Set
    End Property
    Public Property ip() As String
        Get
            Return fstrip
        End Get
        Set(ByVal Value As String)
            fstrip = Value
        End Set
    End Property



#End Region

#Region "Methods"

#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)

        connection.Open()

        Return connection
    End Function
#End Region

#Region "Insert"

    Public Function Insert() As Integer

        Dim insConnection As SqlConnection = Nothing
        Dim insConn As SqlConnection = Nothing
        insConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        insConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASSRCT_Add"
        Try
            trans = insConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(19) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(insConn, fstrSpName)
            Param(0).Value = Trim(fstrSerTypeID)
            Param(1).Value = Trim(fstrSerTypeName)
            Param(2).Value = Trim(fstrSerTypeAlternateName)
            Param(3).Value = Trim(fstrSerTypeStatus)
            Param(4).Value = Trim(fstrCountryID)
            Param(5).Value = Trim(fstrSerType)
            Param(6).Value = Trim(fstrModelID)
            Param(7).Value = Trim(fstrFrequency)
            Param(8).Value = Trim(fstrNoOfYears)
            Param(9).Value = Trim(fstrReReSlip)
            Param(10).Value = Trim(fstrPointCR)
            Param(11).Value = Trim(fstrPointTe)
            Param(12).Value = Trim(fstrReStDate)
            Param(13).Value = Trim(fstrUpLaSeDate)
            Param(14).Value = Trim(fdateEffectiveDate)
            Param(15).Value = Trim(fdateObsoleteDate)
            Param(16).Value = Trim(fstrCreatedBy)
            Param(17).Value = Trim(fstrModifiedBy)
            Param(18).Value = Trim(fintRMDAT)


           
            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(insConn, CommandType.StoredProcedure, fstrSpName, Param)

           
            trans.Commit()
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsSerType.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsSerType.vb")
            Return -1
        End Try

    End Function

#End Region
#Region "InsertDetail"

    Public Function InsertDetail(ByVal frequency As Integer, ByVal NoYear As Integer) As Integer

        Dim insConnection As SqlConnection = Nothing
        Dim insConn As SqlConnection = Nothing
        insConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        insConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASSRCT_AddDeTail"
        Try
            trans = insConnection.BeginTransaction()

            fintDuring = NoYear * 12 / frequency
            Dim firstDuring As Integer = fintDuring
            Dim count As Integer
            For count = 1 To frequency
                Dim Param() As SqlParameter = New SqlParameter(5) {}
                Param = SqlHelperParameterCache.GetSpParameterSet(insConn, fstrSpName)
                Param(0).Value = Trim(fstrSerTypeID)
                Param(1).Value = Trim(fintDuring)
                Param(2).Value = Trim(fstrCreatedBy)
                Param(3).Value = Trim(fstrModifiedBy)
                Param(4).Value = Trim(fstrCountryID)


                Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(insConn, CommandType.StoredProcedure, fstrSpName, Param)
                fintDuring = firstDuring * (count + 1)
            Next
            trans.Commit()
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsSerType.vb")
            Return -1


        End Try

    End Function

#End Region
#Region "Update"

    Public Function Update() As Integer

        Dim UpdConnection As SqlConnection = Nothing
        Dim UpdConn As SqlConnection = Nothing
        UpdConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        UpdConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASSRCT_Upd"
        Try
            trans = UpdConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(20) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(UpdConn, fstrSpName)
            Param(0).Value = Trim(fstrSerTypeID)
            Param(1).Value = Trim(fstrSerTypeName)
            Param(2).Value = Trim(fstrSerTypeAlternateName)
            Param(3).Value = Trim(fstrSerTypeStatus)
            Param(4).Value = Trim(fstrCountryID)
            Param(5).Value = Trim(fstrSerType)
            Param(6).Value = Trim(fstrModelID)
            Param(7).Value = Trim(fstrFrequency)
            Param(8).Value = Trim(fstrNoOfYears)
            Param(9).Value = Trim(fstrReReSlip)
            Param(10).Value = Trim(fstrPointCR)
            Param(11).Value = Trim(fstrPointTe)
            Param(12).Value = Trim(fstrReStDate)
            Param(13).Value = Trim(fstrUpLaSeDate)
            Param(14).Value = Trim(fdateEffectiveDate)
            Param(15).Value = Trim(fdateObsoleteDate)
            Param(16).Value = Trim(fstrModifiedBy)
            Param(17).Value = Trim(fintRMDAT)
            Param(18).Value = Trim(fstrsvcid)
            Param(19).Value = Trim(fstrip)



           
            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(UpdConn, CommandType.StoredProcedure, fstrSpName, Param)
          
            trans.Commit()
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "clsSerType.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "clsSerType.vb")
            Return -1
        End Try

    End Function

#End Region

#Region "Update"

    Public Function updtDetail(ByVal frequency As Integer, ByVal NoYear As Integer) As Integer

        Dim insConnection As SqlConnection = Nothing
        Dim insConn As SqlConnection = Nothing
        insConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        insConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        'fstrSpName = "BB_MASSRCT_UpDateduring"
        Try
            trans = insConnection.BeginTransaction()

            fintDuring = NoYear * 12 / frequency
            Dim firstDuring As Integer = fintDuring
            Dim count As Integer
            For count = 1 To frequency
                Dim Param() As SqlParameter = New SqlParameter(7) {}
                Param = SqlHelperParameterCache.GetSpParameterSet(insConn, fstrSpName)
                Param(0).Value = Trim(fstrSerTypeID)
                Param(1).Value = Trim(fintDuring)
                Param(2).Value = Trim(fstrCreatedBy)
                Param(3).Value = Trim(fstrModifiedBy)
                Param(4).Value = Trim(fstrsvcid)
                Param(5).Value = Trim(fstrip)
                Param(6).Value = Trim(fstrCountryID)


                Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(insConn, CommandType.StoredProcedure, fstrSpName, Param)
                fintDuring = firstDuring * (count + 1)
            Next
            trans.Commit()
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsSerType.vb")
            Return -1


        End Try
    End Function

#End Region
  
#Region "Select SerTypeID"

    Public Function GetSerTypeID1() As DataSet

        Dim selConnection As SqlConnection = Nothing
        Dim selConn As SqlConnection = Nothing
        selConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASSRCT_View"
        Try
            trans = selConnection.BeginTransaction()

            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(5) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrSerTypeID)
            Param(1).Value = Trim(fstrSerTypeName)
            Param(2).Value = Trim(fstrSerTypeStatus)
            Param(3).Value = Trim(fstrCountryID)
            Param(4).Value = Trim(fstrrank)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            ' switch status id to status name for display
            Dim count As Integer
            Dim statXmlTr As New clsXml
            Dim strPanm As String
            statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            For count = 0 To ds.Tables(0).Rows.Count - 1
                Dim statusid As String = ds.Tables(0).Rows(count).Item(2).ToString 'item(3) is status
                strPanm = statXmlTr.GetLabelName("StatusMessage", statusid)
                ds.Tables(0).Rows(count).Item(2) = strPanm
            Next
            Return ds

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsSerType.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsSerType.vb")
        End Try

    End Function
#End Region

    Public Function GetContractTypeID() As DataSet


        Dim selConn As SqlConnection = Nothing
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASSRCT_CONTRACT_IDNAME"
        Try
            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(1) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrCountryID)

            Dim ds As DataSet = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)

            Return ds

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsSerType.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsSerType.vb")
        End Try

    End Function


#Region "Select SerTypeID"

    Public Function GetSerTypeID2() As DataSet

        Dim selConnection As SqlConnection = Nothing
        Dim selConn As SqlConnection = Nothing
        selConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASSRCT_View1"
        Try
            trans = selConnection.BeginTransaction()

            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrSerTypeID)
            Param(1).Value = Trim(fstrCountryID)

            Dim ds As DataSet = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
           
            Return ds

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsSerType.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsSerType.vb")
        End Try

    End Function
   
#End Region

#Region "Select SerType By ID"

    Public Function GetSerTypeIDDetailsByID() As ArrayList
        Dim retnArray As ArrayList = New ArrayList()
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASSRCT_SELBYID"
        Try
            trans = sidConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstrSerTypeID)
            Param(1).Value = Trim(fstrCountryID)
            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            If dr.Read() Then
                Dim count As Integer
                For count = 0 To dr.FieldCount - 1
                    retnArray.Add(dr.GetValue(count).ToString())
                Next
            End If
            dr.Close()
            Return retnArray
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsSerType.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsSerType.vb")
        End Try

    End Function
#End Region
   
#End Region
#Region "Select BY SerTypeName"

    Public Function SelectSerTypeName() As Integer

        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASSRCT_SelByName"
        Try
            trans = sidConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(3) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstrSerTypeID)
            Param(1).Value = Trim(fstrSerTypeName)
            Param(2).Value = Trim(fstrCountryID)


            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            If dr.Read() Then
                dr.Close()
                Return -1
            End If
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsSerType.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsSerType.vb")
        End Try


    End Function
#End Region
#Region "confirm no duplicated id and name"
    Public Function GetDuplicatedSerTypeID() As Integer
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASSRCT_IFDUP"
        Try
            trans = sidConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(3) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstrSerTypeID)
            Param(1).Value = Trim(fstrSerTypeName)
            Param(2).Value = Trim(fstrCountryID)

            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            If dr.Read() Then
                dr.Close()
                Return -1
            End If
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsSerType.vb")
            Return -1
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsSerType.vb")
            Return -1
        Finally
            sidConnection.Close()
            sidConn.Close()
        End Try
    End Function
#End Region
#Region "confirm no duplicated MONTH"
    Public Function GetDuplicatedmonth() As Integer
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASSRCT_dupmoth "
        Try
            trans = sidConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(3) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fintDuring)
            Param(1).Value = Trim(fstrSerTypeID)
            Param(2).Value = Trim(fstrCountryID)

            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            If dr.Read() Then
                dr.Close()
                Return -1
            End If
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsSerType.vb")
            Return -1
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsSerType.vb")
            Return -1
        Finally
            sidConnection.Close()
            sidConn.Close()
        End Try
    End Function
#End Region
#Region "Select sertypeid  in contract unit"

    Public Function GetContractUnit() As SqlDataReader
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASSERTYPEID_DELID"
        Try
            trans = sidConnection.BeginTransaction()
            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstrSerTypeID)
            Param(1).Value = Trim(fstrCountryID)

            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            Return dr
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsSerType.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsSerType.vb")

        End Try
    End Function
#End Region
End Class
