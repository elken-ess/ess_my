Imports SQLDataAccess
Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Web
Imports System.Data
Public Class clsUser
#Region "version info"
    'Description     :  the USER
    'History         :  
    'Modified Date   :  2006-04-22
    'Version         :  v1.0
    'Author          :  xxl
#End Region

    Private strConn As String

#Region "Declaration"


    Private fstrUserID As String  '
    Private fstUserName As String ' country name
    Private fstrUserAlternateName As String ' alternate country  name
    Private fstrStaffStatus As String ' country status
    Private fstrAccessProfileGrop As String ' country status
    Private fstrRank As String  ' country currency
    Private fstrChangeWd As String  ' country currency

    Private fstrCountryID As String ' country id
    Private fstrSVCID As String
    Private fstrCompID As String
    Private fstrDepmentID As String
    Private fstrPenentStuffID As String

    Private fstrPassWord As String
    Private fstrEmailAddrss As String
    Private fstrFailLogon As Integer

    Private fstrcreatedby As String ' createdby
    Private fdtcreateddate As Date ' country currency
    Private fstrmodifyby As String ' modify by
    Private fdtmodifydate As Date  'modify date   

    Private fstrSpName As String ' store procedure name
    Private fstrLoginSvc As String
    Private fstrLoginIP As String

    'Private fstrRank As Integer
    
#End Region
   
#Region "Properties"
    Public Property LoginSvc() As String
        Get
            Return fstrLoginSvc
        End Get
        Set(ByVal Value As String)
            fstrLoginSvc = Value
        End Set
    End Property
    Public Property LoginIP() As String
        Get
            Return fstrLoginIP
        End Get
        Set(ByVal Value As String)
            fstrLoginIP = Value
        End Set
    End Property
    Public Property UserID() As String
        Get
            Return fstrUserID
        End Get
        Set(ByVal Value As String)
            fstrUserID = Value
        End Set
    End Property

    Public Property UserName() As String
        Get
            Return fstUserName
        End Get
        Set(ByVal Value As String)
            fstUserName = Value
        End Set
    End Property


    Public Property UserAlternateName() As String
        Get
            Return fstrUserAlternateName
        End Get
        Set(ByVal Value As String)
            fstrUserAlternateName = Value
        End Set
    End Property

    Public Property StaffStatus() As String
        Get
            Return fstrStaffStatus
        End Get
        Set(ByVal Value As String)
            fstrStaffStatus = Value
        End Set
    End Property

    Public Property AccessProfileGrop() As String
        Get
            Return fstrAccessProfileGrop
        End Get
        Set(ByVal Value As String)
            fstrAccessProfileGrop = Value
        End Set
    End Property

    Public Property Rank() As String
        Get
            Return fstrRank
        End Get
        Set(ByVal Value As String)
            fstrRank = Value
        End Set
    End Property

    Public Property ChangeWd() As String
        Get
            Return fstrChangeWd
        End Get
        Set(ByVal Value As String)
            fstrChangeWd = Value
        End Set
    End Property
    'property


    Public Property DepmentID() As String
        Get
            Return fstrDepmentID
        End Get
        Set(ByVal Value As String)
            fstrDepmentID = Value
        End Set
    End Property

    Public Property PenentStuffID() As String
        Get
            Return fstrPenentStuffID
        End Get
        Set(ByVal Value As String)
            fstrPenentStuffID = Value
        End Set
    End Property
    Public Property CountryID() As String
        Get
            Return fstrCountryID
        End Get
        Set(ByVal Value As String)
            fstrCountryID = Value
        End Set
    End Property
    'property
    Public Property CompID() As String
        Get
            Return fstrCompID
        End Get
        Set(ByVal Value As String)
            fstrCompID = Value
        End Set
    End Property
    Public Property SVCID() As String
        Get
            Return fstrSVCID
        End Get
        Set(ByVal Value As String)
            fstrSVCID = Value
        End Set
    End Property



    Public Property PassWord() As String
        Get
            Return fstrPassWord
        End Get
        Set(ByVal Value As String)
            fstrPassWord = Value
        End Set
    End Property
    Public Property EmailAddrss() As String
        Get
            Return fstrEmailAddrss
        End Get
        Set(ByVal Value As String)
            fstrEmailAddrss = Value
        End Set
    End Property
    Public Property FailLogon() As String
        Get
            Return fstrFailLogon
        End Get
        Set(ByVal Value As String)
            fstrFailLogon = Value
        End Set
    End Property 'fail haong 

    Public Property Createdby() As String
        Get
            Return fstrcreatedby
        End Get
        Set(ByVal Value As String)
            fstrcreatedby = Value
        End Set
    End Property

    Public Property CreatedDate() As Date
        Get
            Return fdtcreateddate
        End Get
        Set(ByVal Value As Date)
            fdtcreateddate = Value
        End Set
    End Property
    Public Property Modifyby() As String
        Get
            Return fstrmodifyby
        End Get
        Set(ByVal Value As String)
            fstrmodifyby = Value
        End Set
    End Property

    Public Property ModdifyDate() As Date
        Get
            Return fdtmodifydate
        End Get
        Set(ByVal Value As Date)
            fdtmodifydate = Value
        End Set
    End Property
#End Region
#Region "Methods"
#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)
        connection.Open()
        Return connection
    End Function
#End Region
#Region "Insert"
    Public Function Insert() As Integer

        Dim insConnection As SqlConnection = Nothing
        Dim insConn As SqlConnection = Nothing
        insConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        insConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASUSER_Add"
        Try
            trans = insConnection.BeginTransaction()
            Dim Param() As SqlParameter = New SqlParameter(19) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(insConn, fstrSpName)
            Param(0).Value = Trim(fstrUserID)
            Param(1).Value = Trim(fstUserName)
            Param(2).Value = Trim(fstrUserAlternateName)
            Param(3).Value = Trim(fstrAccessProfileGrop)
            Param(4).Value = Trim(fstrStaffStatus)
            Param(5).Value = Trim(fstrDepmentID)
            Param(6).Value = Trim(fstrRank)
            Param(7).Value = Trim(fstrFailLogon)
            Param(8).Value = Trim(fstrChangeWd)
            Param(9).Value = Trim(fstrCompID)
            Param(10).Value = Trim(fstrCountryID)
            Param(11).Value = Trim(fstrSVCID)
            Param(12).Value = Trim(fstrPassWord)
            Param(13).Value = Trim(fstrEmailAddrss)
            Param(14).Value = Trim(fstrPenentStuffID)

            Param(15).Value = Trim(fstrcreatedby)
            Param(16).Value = Trim(fdtcreateddate)
            Param(17).Value = Trim(fstrmodifyby)
            Param(18).Value = Trim(fdtmodifydate)
            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(insConn, CommandType.StoredProcedure, fstrSpName, Param)
            trans.Commit()

            insConnection.Dispose()
            insConn.Dispose()

            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrmodifyby, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsModelType.vb")
            Return -1
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrmodifyby, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsModelType.vb")
            Return -1
        End Try
    End Function

#End Region
#Region "Update"

    Public Function Update() As Integer

        Dim UpdConnection As SqlConnection = Nothing
        Dim UpdConn As SqlConnection = Nothing
        UpdConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        UpdConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASUSER_Upd"
        Try
            trans = UpdConnection.BeginTransaction()
            Dim Param() As SqlParameter = New SqlParameter(19) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(UpdConn, fstrSpName)
            Param(0).Value = Trim(fstrUserID)
            Param(1).Value = Trim(fstUserName)
            Param(2).Value = Trim(fstrUserAlternateName)
            Param(3).Value = Trim(fstrAccessProfileGrop)
            Param(4).Value = Trim(fstrStaffStatus)
            Param(5).Value = Trim(fstrDepmentID)
            Param(6).Value = Trim(fstrRank)
            Param(7).Value = Trim(fstrFailLogon)
            Param(8).Value = Trim(fstrChangeWd)
            Param(9).Value = Trim(fstrCompID)
            Param(10).Value = Trim(fstrCountryID)
            Param(11).Value = Trim(fstrSVCID)
            Param(12).Value = Trim(fstrPassWord)
            Param(13).Value = Trim(fstrEmailAddrss)
            Param(14).Value = Trim(fstrPenentStuffID)
            Param(15).Value = Trim(fstrmodifyby)
            Param(16).Value = Trim(fstrLoginIP)
            Param(17).Value = Trim(fstrLoginSvc)
            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(UpdConn, CommandType.StoredProcedure, fstrSpName, Param)
            trans.Commit()

            UpdConnection.Dispose()
            UpdConn.Dispose()
            Return 0
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrmodifyby, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "clsModelType.vb")
            Return -1
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrmodifyby, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "clsModelType.vb")
            Return -1
       
        End Try

    End Function

#End Region
#End Region
#Region "Select USER "

    Public Function GetUserSel() As DataSet

        Dim selConnection As SqlConnection = Nothing
        Dim selConn As SqlConnection = Nothing
        selConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASUSER_View"
        Try
            trans = selConnection.BeginTransaction()
            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(7) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrUserID)
            Param(1).Value = Trim(fstrCountryID)
            Param(2).Value = Trim(fstrStaffStatus)
            Param(3).Value = Trim(fstrDepmentID)
            Param(4).Value = Trim(fstrRank)
            Param(5).Value = Trim(fstrCompID)
            Param(6).Value = Trim(fstrSVCID)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            ' switch status id to status name for display
            Dim count As Integer
            Dim statXmlTr As New clsXml
            Dim Statname As String
            Dim Deptname As String
            Dim Rankname As String
            Dim pstffname As String
            statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            For count = 0 To ds.Tables(0).Rows.Count - 1
                Dim statid As String = ds.Tables(0).Rows(count).Item(2).ToString 'item(2) is status
                Dim deptid As String = ds.Tables(0).Rows(count).Item(3).ToString ' item(3) id dept
                Dim rankid As String = ds.Tables(0).Rows(count).Item(4).ToString 'iten(4)is rank id
                Dim pstffid As String = ds.Tables(0).Rows(count).Item(5).ToString 'item(5)is pstff
                Statname = statXmlTr.GetLabelName("StatusMessage", statid)
                ds.Tables(0).Rows(count).Item(2) = Statname
                Deptname = statXmlTr.GetLabelName("StatusMessage", deptid)
                ds.Tables(0).Rows(count).Item(3) = Deptname
                Rankname = statXmlTr.GetLabelName("StatusMessage", rankid)
                ds.Tables(0).Rows(count).Item(4) = Rankname
                pstffname = statXmlTr.GetLabelName("StatusMessage", pstffid)
                ds.Tables(0).Rows(count).Item(5) = pstffname
            Next

            GetUserSel = ds
            selConnection.Dispose()
            selConn.Dispose()

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrmodifyby, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsModelType.vb")

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrmodifyby, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsModelType.vb")

        End Try
    End Function
#End Region

#Region "select user info and initial sheet when mdy user  "

    Public Function GetUserDetailsByID() As SqlDataReader
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASUSER_SelByID"
        Try
            trans = sidConnection.BeginTransaction()
            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstrUserID)
            Param(1).Value = Trim(fstrStaffStatus)
            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)

            sidConnection.Dispose()
            Return dr
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrmodifyby, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsModelType.vb")

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrmodifyby, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsModelType.vb")

        End Try
    End Function
#End Region

#Region "check name if dup "

    Public Function UserCheckName() As SqlDataReader
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASUSER_DupByNM"
        Try
            trans = sidConnection.BeginTransaction()
            Dim Param() As SqlParameter = New SqlParameter(1) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstUserName)
            'Param(1).Value = Trim(fstrStaffStatus)
            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)

            sidConnection.Dispose()
            Return dr
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrmodifyby, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsModelType.vb")

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrmodifyby, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsModelType.vb")

        End Try
    End Function

#End Region
#Region " check  userID  if dup"

    Public Function UserCheckID() As SqlDataReader
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASUSER_DupByID"
        Try
            trans = sidConnection.BeginTransaction()
            Dim Param() As SqlParameter = New SqlParameter(1) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstrUserID)
            'Param(1).Value = Trim(fstrStaffStatus)
            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)

            sidConnection.Dispose()
            Return dr
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrmodifyby, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsModelType.vb")

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrmodifyby, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsModelType.vb")

        End Try
    End Function
#End Region
#Region "check name if dup "

    Public Function MdyUserCheckName() As SqlDataReader
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASUSER_MDYNMDup"
        Try
            trans = sidConnection.BeginTransaction()
            Dim Param() As SqlParameter = New SqlParameter(3) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstUserName)
            Param(1).Value = Trim(fstrStaffStatus)
            Param(2).Value = Trim(fstrUserID)
            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)

            sidConnection.Dispose()
            Return dr
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrmodifyby, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsModelType.vb")

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrmodifyby, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsModelType.vb")

        End Try
    End Function
#End Region
End Class


