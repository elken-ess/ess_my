﻿Imports Microsoft.VisualBasic
Imports SQLDataAccess
Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Web
Imports System.data

#Region " Module Amment Hisotry "
'============================================'
'Description     :  the Zone entity
'History         :  
'Modified Date   :  2006-05-17
'Version         :  v1.0
'Author          :  Fan
'============================================'

#End Region

Public Class clsApptCalList

#Region "Declaration"

    Private strUserID As String                         'User Login ID
    Private strCusName As String                        'Customer Name
    Private strCustID As String                         'Customer ID 
    Private strStatus As String                         'Appointment Call Status
    Private strRemarks As String                        'Remarks
    Private strTelType As String                        'Telephon Type
    Private strCreatBy As String                        'Created By
    Private CreatDate As System.DateTime                'Created Date
    Private strModifyBy As String                       'Modified By
    Private ModifyDate As System.DateTime               'Modified Date 
    Private strSpName As String                         'Store procedure name
    Private strNextCDT As String                        'Next Call Date
    Private strIPAddress As String                      'IPaddress
    Private strSessionID As String                      'SessionID
    Private strStateID As String                        'State ID
    Private strAreaID As String
    Private strContractEntitled As String                 'Contract Entitle
    Private strRoID As String
    Private strCustPf As String
    Private strCurrentDate As String

#End Region

#Region "Properties"

    Public Property UserID() As String
        Get
            Return strUserID
        End Get
        Set(ByVal value As String)
            strUserID = value
        End Set
    End Property

    Public Property CurrentDate() As String
        Get
            Return strCurrentDate
        End Get
        Set(ByVal value As String)
            strCurrentDate = value
        End Set
    End Property

    Public Property CusName() As String
        Get
            Return strCusName
        End Get
        Set(ByVal value As String)
            strCusName = value
        End Set
    End Property
    Public Property CustomerID() As String
        Get
            Return strCustID
        End Get
        Set(ByVal value As String)
            strCustID = value
        End Set
    End Property
    Public Property CustomerPrefix() As String
        Get
            Return strCustPf
        End Get
        Set(ByVal value As String)
            strCustPf = value
        End Set
    End Property

    Public Property Status() As String
        Get
            Return strStatus
        End Get
        Set(ByVal value As String)
            strStatus = value
        End Set
    End Property
    Public Property Remarks() As String
        Get
            Return strRemarks
        End Get
        Set(ByVal value As String)
            strRemarks = value
        End Set
    End Property
    Public Property TelType() As String
        Get
            Return strTelType
        End Get
        Set(ByVal value As String)
            strTelType = value
        End Set
    End Property

    Public Property CreatedBy() As String
        Get
            Return strCreatBy
        End Get
        Set(ByVal Value As String)
            strCreatBy = Value
        End Set
    End Property

    Public Property CreatedDate() As String
        Get
            Return CreatDate
        End Get
        Set(ByVal Value As String)
            CreatDate = Value
        End Set
    End Property

    Public Property ModifiedBy() As String
        Get
            Return strModifyBy
        End Get
        Set(ByVal Value As String)
            strModifyBy = Value
        End Set
    End Property
    Public Property ModifiedDate() As String
        Get
            Return ModifyDate
        End Get
        Set(ByVal Value As String)
            ModifyDate = Value
        End Set
    End Property
    Public Property IPaddress() As String
        Get
            Return strIPAddress
        End Get
        Set(ByVal Value As String)
            strIPAddress = Value
        End Set
    End Property
    Public Property SessionID() As String
        Get
            Return strSessionID
        End Get
        Set(ByVal Value As String)
            strSessionID = Value
        End Set
    End Property

    Public Property NextCallDate() As String
        Get
            Return strNextCDT
        End Get
        Set(ByVal Value As String)
            strNextCDT = Value
        End Set
    End Property

    Public Property GetStateID() As String
        Get
            Return strStateID
        End Get
        Set(ByVal Value As String)
            strStateID = Value
        End Set
    End Property

    Public Property GetAreaID() As String
        Get
            Return strAreaID
        End Get
        Set(ByVal Value As String)
            strAreaID = Value
        End Set
    End Property

    Public Property ContractEntitled() As String
        Get
            Return strContractEntitled
        End Get
        Set(ByVal Value As String)
            strContractEntitled = Value
        End Set
    End Property

    Public Property GetRoId() As String
        Get
            Return strRoID
        End Get
        Set(ByVal Value As String)
            strRoID = Value
        End Set
    End Property
#End Region



#Region "Methods"

#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)

        connection.Open()

        Return connection
    End Function
#End Region

    Public Function GetApptList() As DataSet
        Dim selConnection As SqlConnection = Nothing
        Dim selConn As SqlConnection = Nothing
        selConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        strSpName = "BB_FNCAPPC_View"
        Try
            trans = selConnection.BeginTransaction()

            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(7) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, strSpName)
            Param(0).Value = Trim(strUserID)
            Param(1).Value = Trim(strCustID)
            Param(2).Value = Trim(strCusName)
            Param(3).Value = Trim(strStatus)
            Param(4).Value = Trim(strStateID)
            Param(5).Value = Trim(strAreaID)
            Param(6).Value = Trim(strContractEntitled)

            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, strSpName, Param)

            Return ds

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(strUserID, ErrorLog.Item(1).ToString, strSpName, WriteErrLog.SELE, "clsApptCalList.vb")
            Return Nothing
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(strUserID, ErrorLog.Item(1).ToString, strSpName, WriteErrLog.SELE, "clsApptCalList.vb")
            Return Nothing
        Finally
            selConnection.Close()
            selConn.Close()
        End Try

    End Function

    Public Function GetApptListPop() As DataSet
        Dim selConnection As SqlConnection = Nothing
        Dim selConn As SqlConnection = Nothing
        selConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        strSpName = "BB_FNCAPPC_Pop_View"
        Try
            trans = selConnection.BeginTransaction()

            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, strSpName)
            Param(0).Value = Trim(strUserID)
            Param(1).Value = Trim(strCurrentDate)


            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, strSpName, Param)

            Return ds

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(strUserID, ErrorLog.Item(1).ToString, strSpName, WriteErrLog.SELE, "clsApptCalList.vb")
            Return Nothing
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(strUserID, ErrorLog.Item(1).ToString, strSpName, WriteErrLog.SELE, "clsApptCalList.vb")
            Return Nothing
        Finally
            selConnection.Close()
            selConn.Close()
        End Try

    End Function

    Public Function UpdApptInfo() As Integer
        Dim selConnection As SqlConnection = Nothing
        Dim selConn As SqlConnection = Nothing
        selConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        strSpName = "BB_FNCAPPC_Upd"
        Try
            trans = selConnection.BeginTransaction()

            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(8) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, strSpName)
            Param(0).Value = Trim(strUserID)
            Param(1).Value = Trim(strCustID)
            Param(2).Value = Trim(strCustPf)
            Param(3).Value = Trim(strStatus)
            Param(4).Value = Trim(strRemarks)
            Param(5).Value = Trim(strNextCDT)
            Param(6).Value = Trim(strIPAddress)
            Param(7).Value = Trim(strRoID)

            UpdApptInfo = SqlHelper.ExecuteNonQuery(selConn, CommandType.StoredProcedure, strSpName, Param)

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("Fan", ErrorLog.Item(1).ToString, strSpName, WriteErrLog.SELE, "clsApptCalList.vb")

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("Fan", ErrorLog.Item(1).ToString, strSpName, WriteErrLog.SELE, "clsApptCalList.vb")

        Finally
            selConnection.Close()
            selConn.Close()
        End Try
    End Function

#End Region

#Region "Check 13 month contract for appointment"
    Public Function CheckContractOn13Month() As DataSet
        Dim selConn As SqlConnection = Nothing
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        strSpName = "BB_FNCAPPC_CheckContract"
        Try

            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(4) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, strSpName)
            Param(0).Value = Trim(strRoID)
            Param(1).Value = Trim(strCustID)
            Param(2).Value = Trim(strCustPf)
            Param(3).Value = Trim(strNextCDT)

            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, strSpName, Param)
            Return ds

        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("Fan", ErrorLog.Item(1).ToString, strSpName, WriteErrLog.SELE, "clsApptCalList.vb")

            Return Nothing
        Finally
            selConn.Close()
        End Try
    End Function
#End Region


Public Function GetStatus(ByVal StatusFlag As String) As DataSet
        Dim myCn As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim myCmd As SqlDataAdapter = New SqlDataAdapter("BB_SELECTSTATUS", myCn)

        myCmd.SelectCommand.CommandType = CommandType.StoredProcedure

        Dim prmStat As SqlParameter = New SqlParameter("@StatFlag", SqlDbType.VarChar, 10)
        prmStat.Value = StatusFlag
        myCmd.SelectCommand.Parameters.Add(prmStat)

        Dim myDataset As New DataSet
        myCmd.Fill(myDataset)
        myCmd.Dispose()
        myCn.Dispose()
        Return myDataset

    End Function
End Class
