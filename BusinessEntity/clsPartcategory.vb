Imports SQLDataAccess
Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Web
Imports System.Data


#Region " Module Amment Hisotry "
'Description     :  the part category entity
'History         :  
'Modified Date   :  2006-04-4
'Version         :  v1.0
'Author          :  ding


#End Region

Public Class clsPartcategory
    Private strConn As String

#Region "Declaration"

    Private fstrCategoryID As String 'Category ID
    Private fstrCategoryName As String ' Category Name
    Private fstrAlternateName As String ' Alternate Category Name
    Private fstrCountryID As String ' Country ID
    Private fstrStatus As String ' Status
    Private fstrEffectiveDate As System.DateTime  'Effective Date 
    Private fstrObsoleteDate As System.DateTime  ' Obsolete Date
    Private fstrCreatBy As String
    Private fstrCreatDate As System.DateTime
    Private fstrMifyBy As String
    Private fstrMifyDate As System.DateTime
    Private fstrSpName As String ' store procedure name

    Private fstrlogcountryid As String ' login country id
    Private fstrlogrank As String ' login rank
    Private fstrlogservcenter As String 'login servicecenter
    Private fstripadress As String 'login ip

#End Region

#Region "Properties"

    Public Property CategoryID() As String
        Get
            Return fstrCategoryID
        End Get
        Set(ByVal Value As String)
            fstrCategoryID = Value
        End Set
    End Property

    Public Property CategoryName() As String
        Get
            Return fstrCategoryName
        End Get
        Set(ByVal Value As String)
            fstrCategoryName = Value
        End Set
    End Property


    Public Property CategoryAlternateName() As String
        Get
            Return fstrAlternateName
        End Get
        Set(ByVal Value As String)
            fstrAlternateName = Value
        End Set
    End Property

    Public Property CountryID() As String
        Get
            Return fstrCountryID
        End Get
        Set(ByVal Value As String)
            fstrCountryID = Value
        End Set
    End Property
    Public Property Status() As String
        Get
            Return fstrStatus
        End Get
        Set(ByVal Value As String)
            fstrStatus = Value
        End Set
    End Property

    Public Property EffectiveDate() As String
        Get
            Return fstrEffectiveDate
        End Get
        Set(ByVal Value As String)
            fstrEffectiveDate = Value
        End Set
    End Property
    Public Property ObsoleteDate() As String
        Get
            Return fstrObsoleteDate
        End Get
        Set(ByVal Value As String)
            fstrObsoleteDate = Value
        End Set
    End Property

    Public Property CreateBy() As String
        Get
            Return fstrCreatBy
        End Get
        Set(ByVal Value As String)
            fstrCreatBy = Value
        End Set
    End Property

    Public Property CreateDate() As String
        Get
            Return fstrCreatDate
        End Get
        Set(ByVal Value As String)
            fstrCreatDate = Value
        End Set
    End Property

    Public Property ModifyBy() As String
        Get
            Return fstrMifyBy
        End Get
        Set(ByVal Value As String)
            fstrMifyBy = Value
        End Set
    End Property
    Public Property ModifyDate() As String
        Get
            Return fstrMifyDate
        End Get
        Set(ByVal Value As String)
            fstrMifyDate = Value
        End Set
    End Property

    Public Property logcountryid() As String
        Get
            Return fstrlogcountryid
        End Get
        Set(ByVal Value As String)
            fstrlogcountryid = Value
        End Set
    End Property

    Public Property logrank() As String
        Get
            Return fstrlogrank
        End Get
        Set(ByVal Value As String)
            fstrlogrank = Value
        End Set
    End Property

    Public Property logservcenter() As String
        Get
            Return fstrlogservcenter
        End Get
        Set(ByVal Value As String)
            fstrlogservcenter = Value
        End Set
    End Property
    Public Property ipadress() As String
        Get
            Return fstripadress
        End Get
        Set(ByVal Value As String)
            fstripadress = Value
        End Set
    End Property

#End Region

#Region "Methods"

#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)

        connection.Open()

        Return connection
    End Function
#End Region

#Region "Insert"

    Public Function Insert() As Integer
        Dim insConnection As SqlConnection = Nothing
        Dim insConn As SqlConnection = Nothing
        insConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        insConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASPCAT_Add"
        Try
            trans = insConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(9) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(insConn, fstrSpName)
            Param(0).Value = Trim(fstrCategoryID)
            Param(1).Value = Trim(fstrCategoryName)
            Param(2).Value = Trim(fstrAlternateName)
            Param(3).Value = Trim(fstrCountryID)
            Param(4).Value = Trim(fstrStatus)
            Param(5).Value = Trim(fstrEffectiveDate)
            Param(6).Value = Trim(fstrObsoleteDate)
            Param(7).Value = Trim(fstrCreatBy) '创建者
            'Param(8).Value = Trim(fstrCreatDate)
            Param(8).Value = Trim(fstrMifyBy) '最后修改者
            'Param(10).Value = Trim(fstrMifyDate)
            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(insConn, CommandType.StoredProcedure, fstrSpName, Param)

            trans.Commit()
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsPartcategory.vb")
            Return -1
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsPartcategory.vb")
            Return -1
        Finally
            insConnection.Close()
            insConn.Close()
        End Try

    End Function

#End Region

#Region "Update"

    Public Function Update() As Integer

        Dim UpdConnection As SqlConnection = Nothing
        Dim UpdConn As SqlConnection = Nothing
        UpdConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        UpdConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASPCAT_Upd"
        Try
            trans = UpdConnection.BeginTransaction()
            Dim updParam() As SqlParameter = New SqlParameter(10) {}
            updParam = SqlHelperParameterCache.GetSpParameterSet(UpdConn, fstrSpName)
            updParam(0).Value = Trim(fstrCategoryID)
            updParam(1).Value = Trim(fstrCategoryName)
            updParam(2).Value = Trim(fstrAlternateName)
            updParam(3).Value = Trim(fstrCountryID)
            updParam(4).Value = Trim(fstrStatus)
            updParam(5).Value = Trim(fstrEffectiveDate)
            updParam(6).Value = Trim(fstrObsoleteDate)
            updParam(7).Value = Trim(fstrMifyBy)
            updParam(8).Value = Trim(fstrlogservcenter)
            updParam(9).Value = Trim(fstripadress)
           
            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(UpdConn, CommandType.StoredProcedure, fstrSpName, updParam)

            trans.Commit()
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "clsPartcategory.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "clsPartcategory.vb")
            Return -1
        Finally
            UpdConnection.Close()
            UpdConn.Close()
        End Try

    End Function

#End Region

#Region "Search Part Category"

    Public Function Getpartcategory() As DataSet

        Dim selConnection As SqlConnection = Nothing
        Dim selConn As SqlConnection = Nothing
        selConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASPCAT_View"
        Try
            trans = selConnection.BeginTransaction()

            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(5) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrCategoryID)
            Param(1).Value = Trim(fstrCategoryName)
            Param(2).Value = Trim(fstrStatus)
            Param(3).Value = Trim(fstrlogcountryid)
            Param(4).Value = Trim(fstrlogrank)
            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            ' switch status id to status name for display
            Dim count As Integer
            Dim statXmlTr As New clsXml
            Dim strPanm As String
            statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            For count = 0 To ds.Tables(0).Rows.Count - 1
                Dim statusid As String = ds.Tables(0).Rows(count).Item(3).ToString 'item(3) is status
                strPanm = statXmlTr.GetLabelName("StatusMessage", statusid)
                ds.Tables(0).Rows(count).Item(3) = strPanm
            Next
            Return ds

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsPartcategory.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsPartcategory.vb")
        Finally
            selConnection.Close()
            selConn.Close()
        End Try

    End Function
#End Region

#Region "Select Category By ID"

    Public Function GetCategoryByID() As ArrayList
        Dim retnArray As ArrayList = New ArrayList()
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASPCAT_SelByID"
        Try
            trans = sidConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(3) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstrCategoryID)
            Param(1).Value = Trim(fstrCountryID)
            Param(2).Value = Trim(fstrStatus)
            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            'Return dr
            If dr.Read() Then
                Dim count As Integer
                For count = 0 To dr.FieldCount - 1
                    retnArray.Add(dr.GetValue(count).ToString())
                Next
            End If
            dr.Close()
            Return retnArray

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsPartcategory.vb")
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsPartcategory.vb")
        Finally
            sidConnection.Close()
            sidConn.Close()
        End Try
    End Function
#End Region

#Region "Select Category By NAME"
    Public Function GetcategoryByNM() As Integer
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASPCAT_SELBYNM"
        Try
            trans = sidConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(3) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstrCategoryID)
            Param(1).Value = Trim(fstrCategoryName)
            Param(2).Value = Trim(fstrCountryID)
            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            If dr.Read() Then
                dr.Close()
                Return -1
            End If
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsCountry.vb")
            Return -1
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsCountry.vb")
            Return -1
        Finally
            sidConnection.Close()
            sidConn.Close()
        End Try
    End Function
#End Region
#Region "confirm no duplicated id and name"
    Public Function GetDuplicatedCategory() As Integer
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASPCAT_Check"
        Try
            trans = sidConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(3) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstrCategoryID)
            Param(1).Value = Trim(fstrCountryID)

            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            If dr.Read() Then
                dr.Close()
                Return -1
            End If
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsPartcategory.vb")
            Return -1
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsPartcategory.vb")
            Return -1
        Finally
            sidConnection.Close()
            sidConn.Close()
        End Try
    End Function
#End Region
#Region "DEL CHECK"
    Public Function GetPartceategorydel() As SqlDataReader
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASPCAT_DELID"
        Try
            trans = sidConnection.BeginTransaction()
            Dim Param() As SqlParameter = New SqlParameter(1) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstrCategoryID)
            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            Return dr
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsPartcategory.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrMifyBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsPartcategory.vb")

        End Try
    End Function
#End Region
#End Region

End Class
