Imports BusinessEntity
Imports System.Configuration
Imports System.Data

Partial Class PresentationLayer_masterrecord_PartCategory
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Page.IsPostBack) Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    'Response.Redirect("~/PresentationLayer/logon.aspx")
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                'Response.Redirect("~/PresentationLayer/logon.aspx")
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try
            Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
            'display label message
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            CategoryIDlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCGR-0001")
            Categorynamelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCGR-0002")
            ctryStat.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCGR-0005")
            addButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Add")
            searchButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Search")
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-partcategorytl")
            Label1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Maslbleinfo")
            Label1.Visible = False
            Me.addinfo.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-AR")
            Me.addinfo.Visible = False
            'create the status dropdownlist
            Dim Stat As New clsrlconfirminf()
            Dim statParam As ArrayList = New ArrayList
            statParam = Stat.searchconfirminf("STATUS")
            Dim count As Integer
            Dim statid As String
            Dim statnm As String
            For count = 0 To statParam.Count - 1
                statid = statParam.Item(count)
                statnm = statParam.Item(count + 1)

                If statid.Equals("ACTIVE") Or statid.Equals("OBSOLETE") Then
                    ctryStatDrop.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                End If
                count = count + 1

            Next
            'ctryStatDrop.Items(0).Selected = True

            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "20")
            Me.addButton.Enabled = purviewArray(0)
            'Me.searchButton.Visible = purviewArray(2)
            ''editcol.Visible = purviewArray(1)
            'Me.partcategoryView.Visible = purviewArray(2)

            Dim categoryEntity As New clsPartcategory()
            categoryEntity.ModifyBy = Session("username")
            categoryEntity.logcountryid = Session("login_ctryID")
            categoryEntity.logrank = Session("login_rank")

            categoryEntity.Status = "ACTIVE"
            Dim ctryall As DataSet = categoryEntity.Getpartcategory()

            If ctryall.Tables(0).Rows.Count = 0 Then
                Label1.Visible = True
            Else
                Label1.Visible = False
            End If

            If ctryall.Tables(0).Rows.Count <> 0 Then
                partcategoryView.DataSource = ctryall
                Session("categoryView") = ctryall
                partcategoryView.AllowPaging = True
                partcategoryView.AllowSorting = True
                Dim editcol As New CommandField
                editcol.EditText = objXmlTr.GetLabelName("EngLabelMsg", "BB-Edit") '"edit"
                editcol.HeaderImageUrl = "~/PresentationLayer/graph/edit.gif"
                editcol.ShowEditButton = True
                partcategoryView.Columns.Add(editcol)
                partcategoryView.DataBind()
                'set right
                editcol.Visible = purviewArray(2)

                partcategoryView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PARTCATHD0")
                partcategoryView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PARTCATHD1")
                partcategoryView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PARTCATHD2")
                partcategoryView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PARTCATHD3")
                partcategoryView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PARTCATHD4")
                partcategoryView.HeaderRow.Cells(1).Font.Size = 8
                partcategoryView.HeaderRow.Cells(2).Font.Size = 8
                partcategoryView.HeaderRow.Cells(3).Font.Size = 8
                partcategoryView.HeaderRow.Cells(4).Font.Size = 8
                partcategoryView.HeaderRow.Cells(5).Font.Size = 8

            End If
        End If
    End Sub
   
    Protected Sub searchButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles searchButton.Click
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        partcategoryView.PageIndex = 0
        Dim partcategoryEntity As New clsPartcategory()
        partcategoryEntity.ModifyBy = Session("username")
        partcategoryEntity.logcountryid = Session("login_ctryID")
        partcategoryEntity.logrank = Session("login_rank")
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        If (Trim(CategoryIDBox.Text) <> "") Then
            partcategoryEntity.CategoryID = CategoryIDBox.Text
        End If
        If (Trim(CategorynameBox.Text) <> "") Then
            partcategoryEntity.CategoryName = CategorynameBox.Text
        End If
        If (ctryStatDrop.SelectedValue().ToString() <> "") Then
            partcategoryEntity.Status = ctryStatDrop.SelectedItem.Value.ToString()
        End If
        If Trim(CategorynameBox.Text) = "" And Trim(CategorynameBox.Text) = "" Then
            addinfo.Visible = True
        Else
            addinfo.Visible = False
        End If
      
        Dim selDS As DataSet = partcategoryEntity.Getpartcategory()
        partcategoryView.DataSource = selDS
        partcategoryView.DataBind()
        Session("categoryView") = selDS
        If selDS.Tables(0).Rows.Count = 0 Then
            Label1.Visible = True
        Else
            Label1.Visible = False
        
        End If

        If selDS.Tables(0).Rows.Count <> 0 Then
            partcategoryView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PARTCATHD0")
            partcategoryView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PARTCATHD1")
            partcategoryView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PARTCATHD2")
            partcategoryView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PARTCATHD3")
            partcategoryView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PARTCATHD4")

            partcategoryView.HeaderRow.Cells(1).Font.Size = 8
            partcategoryView.HeaderRow.Cells(2).Font.Size = 8
            partcategoryView.HeaderRow.Cells(3).Font.Size = 8
            partcategoryView.HeaderRow.Cells(4).Font.Size = 8
            partcategoryView.HeaderRow.Cells(5).Font.Size = 8

        End If
    End Sub

    Protected Sub ctryView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles partcategoryView.PageIndexChanging
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        partcategoryView.PageIndex = e.NewPageIndex
        'Dim ctryall As DataSet = Session("categoryView")
        Dim partcategoryEntity As New clsPartcategory()
        partcategoryEntity.ModifyBy = Session("username")
        partcategoryEntity.logcountryid = Session("login_ctryID")
        partcategoryEntity.logrank = Session("login_rank")
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        If (Trim(CategoryIDBox.Text) <> "") Then
            partcategoryEntity.CategoryID = CategoryIDBox.Text
        End If
        If (Trim(CategorynameBox.Text) <> "") Then
            partcategoryEntity.CategoryName = CategorynameBox.Text
        End If
        If (ctryStatDrop.SelectedValue().ToString() <> "") Then
            partcategoryEntity.Status = ctryStatDrop.SelectedItem.Value.ToString()
        End If
        If Trim(CategorynameBox.Text) = "" And Trim(CategorynameBox.Text) = "" Then
            addinfo.Visible = True
        Else
            addinfo.Visible = False
        End If

        Dim ctryall As DataSet = partcategoryEntity.Getpartcategory()
        partcategoryView.DataSource = ctryall
        partcategoryView.DataBind()
        'Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        If ctryall.Tables(0).Rows.Count <> 0 Then
            partcategoryView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PARTCATHD0")
            partcategoryView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PARTCATHD1")
            partcategoryView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PARTCATHD2")
            partcategoryView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PARTCATHD3")
            partcategoryView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PARTCATHD4")

            partcategoryView.HeaderRow.Cells(1).Font.Size = 8
            partcategoryView.HeaderRow.Cells(2).Font.Size = 8
            partcategoryView.HeaderRow.Cells(3).Font.Size = 8
            partcategoryView.HeaderRow.Cells(4).Font.Size = 8
            partcategoryView.HeaderRow.Cells(5).Font.Size = 8

        End If
    End Sub

    Protected Sub partcategoryView_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles partcategoryView.RowEditing
        Dim page As Integer = partcategoryView.PageIndex * partcategoryView.PageSize
        'Dim categoryid As String = ds.Tables(0).Rows(page + e.NewEditIndex).Item(0).ToString()
        Dim categoryid As String = Me.partcategoryView.Rows(e.NewEditIndex).Cells(1).Text
        categoryid = Server.UrlEncode(categoryid)
        'Dim ctrid As String = ds.Tables(0).Rows(page + e.NewEditIndex).Item(2).ToString()
        Dim ctrid As String = Me.partcategoryView.Rows(e.NewEditIndex).Cells(3).Text
        ctrid = Server.UrlEncode(ctrid)
        'Dim status As String = ds.Tables(0).Rows(page + e.NewEditIndex).Item(3).ToString
        Dim status As String = Me.partcategoryView.Rows(e.NewEditIndex).Cells(4).Text
        Dim i As Integer
        For i = 0 To Me.ctryStatDrop.Items.Count - 1
            If status = Me.ctryStatDrop.Items(i).Text Then
                status = Me.ctryStatDrop.Items(i).Value
            End If
        Next
        status = Server.UrlEncode(status)
        Dim strTempURL As String = "categoryid=" + categoryid + "&ctrid=" + ctrid + "&status=" + status
        strTempURL = "~/PresentationLayer/masterrecord/ModifyPartCategory.aspx?" + strTempURL
        Response.Redirect(strTempURL)
    End Sub
End Class
