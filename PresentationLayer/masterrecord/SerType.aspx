<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SerType.aspx.vb" Inherits="PresentationLayer_masterrecord_SerType" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Service Type - Search</title>
    <link href="../css/style.css" rel="stylesheet" type="text/css" />
        <STYLE type="text/css">A:link { COLOR: #336666; TEXT-DECORATION: none }
	BODY { FONT-SIZE: 9pt; FONT-FAMILY: "Arial", "Verdana", "Tahoma"; BACKGROUND-COLOR: #ffffff }
	TD { FONT-SIZE: 9pt; FONT-FAMILY: Arial,Verdana,Tahoma }
	</STYLE>
</head>
<body id="PriceIDView">
    <form id="form1" runat="server">
    <div>
    <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>
							<TD width="1%" background="../graph/title_bg.gif" style="height: 24px"><IMG height="24" src="../graph/title1.gif" width="5"></TD>
							<TD class="style2" width="98%" background="../graph/title_bg.gif" >
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></TD>
							<TD align="right" width="1%" background="../graph/title_bg.gif" style="height: 24px"><IMG height="24" src="../graph/title_2.gif" width="5"></TD>
						</TR>
	  </TABLE>  
	   
	   <tr>
           <td>
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<tr>
                <td background="../graph/title_bg.gif" style="width: 1%">
                </td>
                <td background="../graph/title_bg.gif" class="style2" width="98%">
                    <asp:Label ID="addinfo" runat="server" ForeColor="Red" Text="Label" Visible="False"></asp:Label></td>
                <td align="right" background="../graph/title_bg.gif" width="1%">
                </td>
            </tr>
			 </TABLE>
           </tr>
           <TR>
           <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
           <tr >
        <td  width="15%"><asp:Label ID="SerTypeIDlab" runat="server" Text="label" Width="100%"></asp:Label></td>
        <td width="15%"><asp:TextBox ID="SerTypeIDBox" runat="server" Width="70%" CssClass="textborder" MaxLength="10"></asp:TextBox></td>
        <td width="15%"><asp:Label ID="SerTypeNamelab" runat="server" Text="label" Width="100%"></asp:Label></Td>
        <Td width="20%"><asp:TextBox ID="SerTypeNameBox" runat="server" Width="90%" CssClass="textborder" MaxLength="50"></asp:TextBox></td>
        <td width="10%"><asp:Label ID="Statuslab" runat="server" Text="label" Width="100%"></asp:Label></td>
        <td width="13%"><asp:DropDownList ID="StatusDrop" runat="server" Width="100%" CssClass="textborder"></asp:DropDownList></td>
        <td width="20%"><asp:LinkButton ID="LinkButton2" runat="server">Seach</asp:LinkButton>&nbsp;&nbsp;
        <asp:LinkButton ID="LinkButton1" runat="server" PostBackUrl="~/PresentationLayer/masterrecord/addSerType.aspx">add</asp:LinkButton></td>
               </tr>
        </TABLE>                
        <hr />
        </div>       
         <asp:GridView ID="SerTypeView" runat="server" Width="100%" AllowPaging="True">
        </asp:GridView>
        <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="Label"></asp:Label>
        <br />
    </form>
</body>
</html>
