<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Instsearchcustomer.aspx.vb" Inherits="PresentationLayer_masterrecord_Instsearchcustomer" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Install Based Search Customer</title>
      <LINK href="../css/style.css" type="text/css" rel="stylesheet">
       <STYLE type="text/css">A:link { COLOR: #336666; TEXT-DECORATION: none }
	BODY { FONT-SIZE: 9pt; FONT-FAMILY: "Arial", "Verdana", "Tahoma"; BACKGROUND-COLOR: #ffffff }
	TD { FONT-SIZE: 9pt; FONT-FAMILY: Arial,Verdana,Tahoma }
	</STYLE>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table id="TABLE2" border="0" width ="100%">
            <tr>
                <td style="width: 100%; height: 13px">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title1.gif" width="5" /></td>
                            <td background="../graph/title_bg.gif" class="style2" width="98%">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="right" background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title_2.gif" width="5" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100%; height: 12px">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" class="style2" style="height: 1px" width="98%">
                                <font color="red">
                                    <asp:Label ID="addinfo" runat="server" Text="Label" Visible="false"></asp:Label></font></td>
                        </tr>
                    </table>
                    <asp:Label ID="LblErr" runat="server" ForeColor="Red" Text="Label" Visible="False"></asp:Label></td>
            </tr>
        </table>
        <table id="countrytab" border="0" width ="100%">
            <tr>
                <td  width= " 100%" style="height: 85%">
                    <table id="country" bgcolor="#b7e6e6" border="0" cellpadding="2" cellspacing="1" width ="100%">
                        <tr bgcolor="#ffffff">
                            <td align="right" width="15%" style="height: 28px">
        <asp:Label ID="customerIDlab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 27%; height: 28px;">
                                <asp:TextBox ID="customerIDbox" runat="server" Font-Size="Small" CssClass="textborder" MaxLength="10"></asp:TextBox></td>
                            <td align="right" width="15%" style="height: 28px">
        <asp:Label ID="areaIDlab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="height: 28px; width: 35%;">
                                <asp:DropDownList ID="areaID" runat="server" Width="152px" CssClass="textborder">
                                </asp:DropDownList></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" width="15%">
        <asp:Label ID="customernamelab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 27%">
                                <asp:TextBox ID="customernamebox" runat="server" Font-Size="Small" CssClass="textborder" MaxLength="50"></asp:TextBox></td>
                            <td align="right" width="15%">
                                <asp:Label ID="contactlab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%">
                                <asp:TextBox ID="contactbox" runat="server" Font-Size="Small" CssClass="textborder" MaxLength="30"></asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="left" colspan="4">
        <asp:LinkButton ID="searchButton" runat="server">search</asp:LinkButton>
                            </td>
                        </tr>
                    </table>
        <hr />
                    <asp:GridView ID="ctryView" runat="server" AllowPaging="True" AllowSorting="True"
                        Font-Size="Smaller" Width="100%">
                        <Columns>
                            <asp:TemplateField HeaderImageUrl="~/PresentationLayer/graph/edit.gif">
                                <ItemTemplate>
                                    <asp:LinkButton ID="LnkSelect" runat="server" CommandArgument ='<%#Container.DataItemIndex %>' CommandName="Select">select</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderImageUrl="~/PresentationLayer/graph/edit.gif">
                                <ItemTemplate>
                                    <asp:LinkButton ID="LnkView" runat="server" CommandArgument ='<%#Container.DataItemIndex %>' CommandName="View">view</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
        &nbsp;<asp:Label ID="Label1" runat="server" ForeColor="Red" Text="Label" Visible="False"
            Width="441px"></asp:Label><br />
        &nbsp;&nbsp;
        <br />
    </div>
    </form>
</body>
</html>
