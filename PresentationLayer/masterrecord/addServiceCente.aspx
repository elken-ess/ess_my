<%@ Page Language="VB" AutoEventWireup="false" EnableEventValidation="false"CodeFile="addServiceCente.aspx.vb" Inherits="PresentationLayer_masterrecord_addServiceCente" %>

<%@ Register Assembly="JCalendar" Namespace="JCalendar" TagPrefix="cc2" %>

<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">

    <title>Service Center - Add</title>
    <LINK href="../css/style.css" type="text/css" rel="stylesheet" />
    <STYLE type="text/css">A:link { COLOR: #336666; TEXT-DECORATION: none }
	BODY { FONT-SIZE: 9pt; FONT-FAMILY: "Arial", "Verdana", "Tahoma"; BACKGROUND-COLOR: #ffffff }
	TD { FONT-SIZE: 9pt; FONT-FAMILY: Arial,Verdana,Tahoma }
	</STYLE>
	<script language="JavaScript" src="../js/common.js"></script> 
 </head>
<body id="ServiceCentebody">
    <form id="ServiceCenter" method="post" runat="server">
        <TABLE border="0" id=statetab style="width: 100%">
    <tr>
           <td>
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>
							<TD width="1%" background="../graph/title_bg.gif"><IMG height="24" src="../graph/title1.gif" width="5"></TD>
							<TD class="style2" width="98%" background="../graph/title_bg.gif">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></TD>
							<TD align="right" width="1%" background="../graph/title_bg.gif"><IMG height="24" src="../graph/title_2.gif" width="5"></TD>
						</TR>
			 </TABLE>
           </tr>
            <tr>
           <td >
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>					
							<TD class="style2" width="98%" background="../graph/title_bg.gif">
                                <font color=red><asp:Label ID="errlab" runat="server" Text="Label" Visible=false></asp:Label></font>
                               
						</TR>
			 </TABLE>
           </tr>
			<TR>
				<TD >
                    <table id="country" bgcolor="#b7e6e6" border="0" cellpadding="2" cellspacing="1"
                        style="width: 100%">
                         <tr bgcolor="#ffffff">
<td colspan = 4>
                            <asp:Label ID="lblCompTop" runat="server" ForeColor="Red" Text="Label" Visible="True"></asp:Label>
</td>
             </tr>
             <tr bgcolor="#ffffff">
<td colspan = 4>
                        	&nbsp;
                     	</td>
               </tr>   
                        
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 14%; height: 28%">
                                <asp:Label ID="ServiceCenterNamelab" runat="server" bgColor="#b7e6e6" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%;">
                                <asp:TextBox ID="ServiceCenterName" runat="server" CssClass="textborder" Font-Overline="False"
                                    Width="89%" MaxLength="50"></asp:TextBox><span style="color: #ff0000"> 
                                        <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="Red" Text="*"></asp:Label></span><span style="width: 115%;
                                        color: #000000"><asp:RequiredFieldValidator ID="ServiceCenterNamemsg" runat="server"
                                            ControlToValidate="ServiceCenterName" ErrorMessage="*" ForeColor="White"> <strong>*</strong> </asp:RequiredFieldValidator></span></td>
                            <td align="right" style="width: 76px; color: #000000; height: 28%">
                                <asp:Label ID="ServiceCenterIDlab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%; color: red;">
                                <asp:TextBox ID="ServiceCenterID" runat="server" CssClass="textborder" Width="88%" MaxLength="10"></asp:TextBox>
                                <asp:Label ID="Label2" runat="server" Font-Bold="True" Text="*" ForeColor="Red"></asp:Label><span
                                    style="width: 100%; color: #ff0000"><span style="color: #000000"></span><asp:RequiredFieldValidator
                                        ID="ServiceCenterIDMsg" runat="server" ControlToValidate="ServiceCenterID" ErrorMessage="*" ForeColor="White"> <strong>*</strong> </asp:RequiredFieldValidator></span>
                            </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 14%; height: 28%">
                                <asp:Label ID="AlternateNamelab" runat="server" Text="Label" Width="48px"></asp:Label></td>
                            <td align="left" style="width: 35%">
                                <asp:TextBox ID="AlternateName" runat="server" CssClass="textborder" Width="89%" MaxLength="50"></asp:TextBox><span
                                    style="color: #ff0000">
                                    <asp:Label ID="Label3" runat="server" Font-Bold="True" ForeColor="Red" Text="*"></asp:Label>
                                </span><span style="width: 115%;
                                        color: #000000"></span>
                                <asp:RequiredFieldValidator ID="AlternateNamemsg" runat="server" ControlToValidate="AlternateName"
                                    ErrorMessage="*" ForeColor="White"> <strong>*</strong> </asp:RequiredFieldValidator></td>
                            <td align="right" style="width: 76px; height: 28%">
                                <asp:Label ID="BranchTypelab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%">
                                <asp:DropDownList ID="BranchType" runat="server" CssClass="textborder" Width="88%">
                                </asp:DropDownList>
                                <asp:Label ID="Label4" runat="server" Font-Bold="True" Text="*" ForeColor="Red"></asp:Label>
                                <asp:RequiredFieldValidator ID="BranchTypemsg" runat="server" ControlToValidate="BranchType"
                                    ForeColor="WhiteSmoke"> <strong>*</strong> </asp:RequiredFieldValidator></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 14%; height: 28%">
                                &nbsp;<asp:Label ID="PersonInChargelab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%">
                                <asp:TextBox ID="PersonInCharge" runat="server" CssClass="textborder" Width="89%" MaxLength="50"></asp:TextBox><span
                                    style="color: #ff0000"></span><span style="width: 115%;
                                        color: #000000"></span></td>
                            <td align="right" style="width: 76px; height: 28%">
                                <asp:Label ID="status" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%">
                                <span style="color: #ff0000"></span><asp:DropDownList ID="ctrystat" runat="server" CssClass="textborder" Width="89%">
                                </asp:DropDownList></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 14%; height: 30px">
                                <asp:Label ID="Address1lab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" colspan="3" style="height: 30px">
                                <asp:TextBox ID="Address1" runat="server" CssClass="textborder" Width="95%" MaxLength="100"></asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 14%; height: 30px">
                                <asp:Label ID="Address2lab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" colspan="3" style="height: 30px">
                                <asp:TextBox ID="Address2" runat="server" CssClass="textborder" Width="95%" MaxLength="100"></asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 14%; height: 28%">
                                <asp:Label ID="POCodelab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%">
                                <asp:TextBox ID="POCode" runat="server" CssClass="textborder" Width="89%" MaxLength="10"></asp:TextBox><span
                                    style="color: #ff0000; width: 35%;">
                                    <asp:Label ID="Label5" runat="server" Font-Bold="True" Text="*" ForeColor="Red"></asp:Label>
                                    <asp:RequiredFieldValidator ID="POCodeMsg" runat="server"
                                        ControlToValidate="POCode" ForeColor="White"> <strong>*</strong> </asp:RequiredFieldValidator></span></td>
                            <td align="right" style="width: 76px; height: 28%">
                                <asp:Label ID="CountryIDlab" runat="server" Text="Label"></asp:Label>
                            </td>
                            <td align="left" style="width: 35%" id="#ctry">
                                <asp:DropDownList ID="CountryID" runat="server" CssClass="textborder" Width="89%" AutoPostBack="True">
                                </asp:DropDownList>
                                <asp:Label ID="Label13" runat="server" Font-Bold="True" Text="*" ForeColor="Red"></asp:Label><asp:RequiredFieldValidator ID="countrymsg" runat="server" ControlToValidate="CountryID" ForeColor="White"> <strong>*</strong> </asp:RequiredFieldValidator></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 14%; height: 28%">
                                <asp:Label ID="StateIDlab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%">
                                &nbsp;<asp:DropDownList ID="StateID" runat="server" CssClass="textborder" Width="89%" AutoPostBack="True">
                                </asp:DropDownList>
                                <asp:Label ID="Label6" runat="server" Font-Bold="True" Text="*" ForeColor="Red"></asp:Label>
                                <asp:RequiredFieldValidator ID="statmsg" runat="server" ControlToValidate="StateID" ForeColor="White"> <strong>*</strong> </asp:RequiredFieldValidator></td>
                            <td align="right" style="width: 76px; height: 28%">
                                <asp:Label ID="AreaIDlab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%">
                                <asp:DropDownList ID="AreaID" runat="server" CssClass="textborder" Width="89%">
                                </asp:DropDownList>
                                <asp:Label ID="Label12" runat="server" Font-Bold="True" Text="*" ForeColor="Red"></asp:Label><asp:RequiredFieldValidator ID="areamsg" runat="server" ControlToValidate="AreaID" ForeColor="White"> <strong>*</strong> </asp:RequiredFieldValidator></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 14%; height: 28%">
                                <asp:Label ID="Telephone1lab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%">
                                <asp:TextBox ID="Telephone1" runat="server" CssClass="textborder" Width="89%" MaxLength="20"></asp:TextBox>
                                <asp:Label ID="Label7" runat="server" Font-Bold="True" Text="*" ForeColor="Red"></asp:Label><span
                                    style="color: #ff0000"></span><asp:RegularExpressionValidator ID="Telephone1Msg"
                                        runat="server" ControlToValidate="Telephone1" ErrorMessage="*" ValidationExpression="\d{0,20}" ForeColor="White"> <strong>*</strong> </asp:RegularExpressionValidator></td>
                            <td align="right" style="width: 76px; height: 28%">
                                <asp:Label ID="picmobilelab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%">
                                <asp:TextBox ID="picmobile" runat="server" CssClass="textborder" Width="88%" MaxLength="20"></asp:TextBox><span
                                    style="color: #ff0000"></span><asp:RegularExpressionValidator ID="picmobileMsg"
                                        runat="server" ControlToValidate="picmobile" ErrorMessage="*" ValidationExpression="\d{0,20}" ForeColor="White"> <strong>*</strong> </asp:RegularExpressionValidator></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 14%; height: 28%">
                                <asp:Label ID="Telephone2lab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%;">
                                <asp:TextBox ID="Telephone2" runat="server" CssClass="textborder" Width="89%" MaxLength="20"></asp:TextBox><span
                                    style="color: #ff0000"></span><asp:RegularExpressionValidator ID="Telephone2Msg"
                                        runat="server" ControlToValidate="Telephone2" ErrorMessage="*" ValidationExpression="\d{0,20}" ForeColor="White"> <strong>*</strong> </asp:RegularExpressionValidator></td>
                            <td align="right" style="width: 76px; height: 28%">
                                <asp:Label ID="Faxlab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%;">
                                <asp:TextBox ID="Fax" runat="server" CssClass="textborder" Width="88%" MaxLength="20"></asp:TextBox><span
                                    style="color: #ff0000"></span><asp:RegularExpressionValidator ID="FaxMsg" runat="server"
                                        ControlToValidate="Fax" ErrorMessage="*" ValidationExpression="\d{0,20}" ForeColor="White"> <strong>*</strong> </asp:RegularExpressionValidator></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 14%; height: 28%">
                                <asp:Label ID="EffectiveDatelab" runat="server" Text="Label"></asp:Label>
                            </td>
                            <td align="left" style="width: 35%;">
                                <asp:TextBox ID="EffectiveDate" runat="server" CssClass="textborder"
                                    Width="85%" ReadOnly="True"></asp:TextBox><font color="red"></font><cc2:JCalendar ID="JCalendar1"
                                        runat="server" ControlToAssign="EffectiveDate" ImgURL="~/PresentationLayer/graph/calendar.gif" Visible="False" />
                                <asp:HyperLink ID="HypCal" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                                    ToolTip="Choose a Date">Choose a Date</asp:HyperLink><asp:Label ID="Label8" runat="server" Font-Bold="True"  ForeColor="Red" Visible=true  >*</asp:Label>
                                &nbsp;
                            </td>
                            <td align="right" style="width: 76px; height: 28%">
                                <asp:Label ID="CompanyIDlab" runat="server" Text="Label"></asp:Label>
                            </td>
                            <td align="left" style="width: 35%;">
                                <asp:DropDownList ID="CompanyID" runat="server" CssClass="textborder" Width="88%">
                                </asp:DropDownList><span style="color: #ff0000">
                                    <asp:Label ID="Label11" runat="server" Font-Bold="True" ForeColor="Red" >*</asp:Label></span><asp:RequiredFieldValidator ID="CompanyIDmsg" runat="server" ControlToValidate="CompanyID"
                                    ErrorMessage="*" ForeColor="White"><strong>*</strong> </asp:RequiredFieldValidator></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 14%; height: 28%">
                                <asp:Label ID="JobsPeDaylab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%">
                                <asp:TextBox ID="JobsPerDay" runat="server" CssClass="textborder" Width="89%" MaxLength="9"></asp:TextBox><span
                                    style="color: #ff0000">
                                    <asp:Label ID="Label9" runat="server" Font-Bold="True" Text="*" ForeColor="Red"></asp:Label>
                                </span>
                            </td>
                            <td align="right" style="width: 76px; height: 28%">
                                <asp:Label ID="OTPremiumJobslab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%">
                                <asp:TextBox ID="OTPremiumJobs" runat="server" CssClass="textborder" Width="88%" MaxLength="9"></asp:TextBox><span
                                    style="color: #ff0000">
                                    <asp:Label ID="Label10" runat="server" Font-Bold="True" Text="*" ForeColor="Red"></asp:Label>
                                </span>
                            </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 14%; height: 25%">
                                <asp:Label ID="CreatedBylab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%;">
                                <asp:TextBox ID="CreatedBy" runat="server" CssClass="textborder" Width="89%"></asp:TextBox></td>
                            <td align="right" style="width: 76px; height: 28%">
                                <asp:Label ID="CreatedDatelab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%;">
                                <asp:TextBox ID="CreatedDate" runat="server" CssClass="textborder" Width="88%"></asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 14%; height: 28%">
                                <asp:Label ID="ModiBylab" runat="server" Text="Label"></asp:Label>
                            </td>
                            <td align="left" style="width: 35%">
                                <asp:TextBox ID="ModiBy" runat="server" CssClass="textborder" Width="89%"></asp:TextBox></td>
                            <td align="right" style="width: 76px; height: 28%">
                                <asp:Label ID="ModifiedDatelab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%">
                                <asp:TextBox ID="ModifiedDate" runat="server" CssClass="textborder" Width="88%"></asp:TextBox></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 844px; height: 37%">
                    <font color=red><asp:Label ID="lblCompBottom" runat="server" Text="Label" Visible="true"></asp:Label></font>
                    <table id="Table2" border="0" cellpadding="1" cellspacing="1">
                        <tr>
                            <td align="center" style="width: 20%">
                                &nbsp;<asp:LinkButton ID="LinkButton1" runat="server">save</asp:LinkButton></td>
                            <td align="center">
                                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/PresentationLayer/masterrecord/ServiceCenter.aspx">cancel</asp:HyperLink></td>
                                <td align=center style="height: 16px">
                                            <asp:HyperLink ID="AddLink" runat="server" >[Add]</asp:HyperLink>
                                            </td>
                                            
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <asp:RangeValidator ID="JobsPerDayMsg" runat="server"
                                        ControlToValidate="JobsPerDay" ErrorMessage="*" MaximumValue="2147483647" MinimumValue="0"
                                        Type="Integer" ForeColor="White"> <strong>*</strong> </asp:RangeValidator>
                                <asp:RequiredFieldValidator ID="JPDaymsg" runat="server" ControlToValidate="JobsPerDay"
                                    ErrorMessage="*" ForeColor="White"> <strong>*</strong> </asp:RequiredFieldValidator>
        <asp:RangeValidator ID="OTPremiumJobsMsg" runat="server"
                                        ControlToValidate="OTPremiumJobs" ErrorMessage="*" MaximumValue="2147483647"
                                        MinimumValue="0" Type="Integer" ForeColor="White"> <strong>*</strong> </asp:RangeValidator>
                                <asp:RequiredFieldValidator ID="OTPmsg" runat="server" ControlToValidate="OTPremiumJobs"
                                    ErrorMessage="*" ForeColor="White"> <strong>*</strong> </asp:RequiredFieldValidator>&nbsp;
        <asp:RequiredFieldValidator ID="EffectiveDatemsg" runat="server" ControlToValidate="EffectiveDate"
                                    ErrorMessage="*" ForeColor="White"> <strong>*</strong> </asp:RequiredFieldValidator>
        <cc1:messagebox id="MessageBox1" runat="server"></cc1:messagebox>
        <br />
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
            ShowSummary="False" />
    </form>
</body>
</html>
