
Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data

Partial Class PresentationLayer_masterrecord_modifymodeltype
    Inherits System.Web.UI.Page
    Private fdtcrtdt As String ' country id

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "16")
            Me.LinkButton1.Enabled = purviewArray(1)
            If purviewArray(2) = False Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return

            End If
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
            'System.Threading.Thread.CurrentThread.CurrentCulture = datestyle  'datetime format
            Dim cntryStat As New clsrlconfirminf()
            Dim statXmlTr As New clsXml
            statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            '''''''''''''''''''''''''''''''''''''''''''''''''''''
            Try
                Dim str As String = Request.Params("ctryid").ToString
                str = Request.Params("mdtyid").ToString()
                str = Request.Params("mdfystat").ToString()
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim mdfyctrid As String = Request.Params("ctryid").ToString()
            Dim mdmdtyid As String = Request.Params("mdtyid").ToString() 'tryid={0}&mdtyid={1}"
            Dim tempstat As String = Request.Params("mdfystat")
            Dim modeltypeEntity As New clsModelType()
            If mdfyctrid.Trim() <> "" Then
                modeltypeEntity.CountryID = mdfyctrid
            End If
            If mdmdtyid.Trim() <> "" Then
                modeltypeEntity.ModelTypeID = mdmdtyid
            End If
            If mdmdtyid.Trim() <> "" Then
                ' modeltypeEntity.ModelTypeStatus = statXmlTr.GetLabelID("StatusMessage", tempstat)
                modeltypeEntity.ModelTypeStatus = tempstat
            End If

            Dim edtReader As SqlDataReader = modeltypeEntity.GetMdtypeDetailsByID()
            If edtReader.Read() Then
                Me.ModelIDBox.Text = edtReader.GetValue(0).ToString()
                Me.ModelIDBox.ReadOnly = True
                Me.CountryIDDDL.Text = edtReader.GetValue(1).ToString()
                Me.CountryIDDDL.Enabled = False
                Me.ModelNameBox.Text = edtReader.GetValue(2).ToString()
                Me.AlModelNamBox.Text = edtReader.GetValue(3).ToString()
                Dim prdctclsid As String = edtReader.Item(4).ToString    'item(4) is product class                
                mdfydtbox.Text = edtReader.GetValue(11).ToString()
                Dim statParam As ArrayList = New ArrayList
                Dim pclsParam As ArrayList = New ArrayList
                Dim count As Integer
                Dim statid As String
                Dim statnm As String
                Dim pclsid As String
                Dim pclsname As String
                ' product cls  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
                pclsParam = cntryStat.searchconfirminf("PRODUCTCL")
                For count = 0 To pclsParam.Count - 1
                    pclsid = pclsParam.Item(count)
                    pclsname = pclsParam.Item(count + 1)
                    Me.ProductClassDDL.Items.Add(New ListItem(pclsname.ToString(), pclsid.ToString()))
                    If pclsid.Equals(edtReader.Item(4).ToString) Then
                        ProductClassDDL.Items(count / 2).Selected = True
                    End If
                    count = count + 1
                Next
                'status \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ 
                statParam = cntryStat.searchconfirminf("STATUS")
                For count = 0 To statParam.Count - 1
                    statid = statParam.Item(count)
                    statnm = statParam.Item(count + 1)
                    If (statid.Equals("ACTIVE") Or statid.Equals("DELETE") Or statid.Equals("OBSOLETE")) Then
                        Me.StatusDDL.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                    End If
                    If statid.Equals(edtReader.Item(8).ToString) Then
                        StatusDDL.Items(count / 2).Selected = True
                    End If

                    count = count + 1
                Next
                ''''''''''''''modify by xxl 0614''''''''''''''''''''''''''''''''''''''''''''''''
                If purviewArray(4) = False Then
                    Me.StatusDDL.Items.Remove(Me.StatusDDL.Items.FindByValue("DELETE"))
                End If                '  
                ''''''''''''''modify by xxl 0614''''''''''''''''''''''''''''''''''''''''''''''''
            Me.EffectiveDateBox.Text = edtReader.GetValue(5).ToString()
            Me.ObsoleteDateLabBox.Text = edtReader.GetValue(6).ToString()
            Me.WarrantyDayBox.Text = edtReader.GetValue(7).ToString()
            Me.creatdtbox.Text = edtReader.GetValue(9).ToString()
            Me.creatdtbox.ReadOnly = True
                '  Me.creatbybox.Text = edtReader.GetValue(10).ToString().ToUpper()
                Dim Rcreatby As String = edtReader.Item(10).ToString().ToUpper()
                If Rcreatby <> "ADMIN" Then
                    Dim Rcreat As New clsCommonClass
                    Rcreat.userid() = Rcreatby
                    Dim Rcreatds As New DataSet()
                    Rcreatds = Rcreat.Getuseridname("BB_MASUSER_SelByIDName")

                    Me.creatbybox.Text = Rcreatds.Tables(0).Rows(0).Item(0)
                Else
                    Me.creatbybox.Text = "ADMIN-ADMIN"
                End If


            End If


        'display label message
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        'label text
        Me.CountryIDLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-0001")
        Me.ModelIDLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-0002")
        Me.AlModelNamLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-0003")
        Me.EffectiveDateLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-0004")
        Me.WarrantyDayLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-0005")
        Me.ProductClassLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-0006")
        Me.ModelNameLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-0007")
        Me.ObsoleteDateLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-0008")
        Me.StatusLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-0009")
        creatby.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATBY")
        creatdate.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
        modfyby.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFBY")
            mdfydate.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFDATE")
            Label9.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            Label6.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            Me.titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-0012")

            LinkButton1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Save")
            HyperLink1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Cancel")

            Dim userIDNamestr As String = Session("userID").ToString().ToUpper + "-" + Session("username").ToString.ToUpper
            modfybybox.Text = userIDNamestr

            Me.ModelNameBox.Focus()
            '\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
            Me.RangeValidator1.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "MDWRD")
            Me.RequiredFieldValidator1.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "MDNAM")
            Me.RequiredFieldValidator2.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "MDWD")
            Me.RequiredFieldValidator3.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "MDEFF")
            Me.RequiredFieldValidator4.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "MDOBS")
            Me.rfvctry.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "MDCTR")
            Me.rfvaname.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "MDANAME")
            '\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
        End If
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle  'datetime format
        HypCal.NavigateUrl = "javascript:DoCal(document.mdfymodeltypeform.EffectiveDateBox);"
        HypCal2.NavigateUrl = "javascript:DoCal(document.mdfymodeltypeform.ObsoleteDateLabBox);"

    End Sub
#Region " command bond function "
    Public Function databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        Dim row As DataRow
        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            NewItem.Text = Trim(row(0).ToString()) & "-" & Trim(row(1).ToString())
            NewItem.Value = row(0)
            dropdown.Items.Add(NewItem)
        Next
    End Function


#End Region

#Region "update record"
    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton1.Click
        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim msginfo As String = objXm.GetLabelName("StatusMessage", "SAVEORNOT")
        Dim msgtitle As String = objXm.GetLabelName("StatusMessage", "SAVETITLE")
        Dim effdate As Date
        Dim obsdate As Date
        If (Trim(Request.Form("EffectiveDateBox")) <> "") Then
            Dim datearr As Array = Request.Form("EffectiveDateBox").Split("/") 'EffectiveDateBox.Text.ToString().Split("/")
            Dim rtdate As String = datearr(2) + "-" + datearr(1) + "-" + datearr(0)
            effdate = Convert.ToDateTime(rtdate)
        End If

        If (Trim(Request.Form("ObsoleteDateLabBox")) <> "") Then
            Dim datearr As Array = Request.Form("ObsoleteDateLabBox").Split("/") 'ObsoleteDateLabBox.Text.ToString().Split("/")
            Dim rtdate As String = datearr(2) + "-" + datearr(1) + "-" + datearr(0)
            obsdate = Convert.ToDateTime(rtdate)
        End If
        If obsdate < effdate Then
            mdyinfo.Text = objXm.GetLabelName("StatusMessage", "BB-HintMsg-COMPAREEFFOBDATE")
            mdyinfo.Visible = True
            Return
        End If

        If Me.StatusDDL.SelectedValue = "DELETE" Then
            If check() = 1 Then
                mdyinfo.Text = objXm.GetLabelName("StatusMessage", "CHECKDELSTAT")
                mdyinfo.Visible = True
                Return
            End If

        End If

        EffectiveDateBox.Text = Request.Form("EffectiveDateBox")
        ObsoleteDateLabBox.Text = Request.Form("ObsoleteDateLabBox")
        MessageBox1.Confirm(msginfo, msgtitle)
    End Sub
#End Region
#Region "modify modeltype"
    Public Sub mdfymodel()

        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim modeltypeEntity As New clsModelType()
        If (Trim(Me.CountryIDDDL.Text) <> "") Then
            modeltypeEntity.CountryID = Me.CountryIDDDL.Text.ToString().Substring(0, Me.CountryIDDDL.Text.ToString().IndexOf("-"))
        End If
        If (Trim(Me.ProductClassDDL.Text) <> "") Then
            modeltypeEntity.ProductClass = ProductClassDDL.SelectedValue
        End If
        If (Trim(Me.ModelIDBox.Text) <> "") Then
            modeltypeEntity.ModelTypeID = ModelIDBox.Text.ToUpper()
        End If
        If (Trim(Me.ModelNameBox.Text) <> "") Then
            modeltypeEntity.ModelTypeName = ModelNameBox.Text.ToUpper()
        End If
        If (Trim(Me.AlModelNamBox.Text) <> "") Then
            modeltypeEntity.ModelTypeAlternateName = AlModelNamBox.Text.ToUpper()
        End If
        If (Trim(Request.Form("EffectiveDateBox")) <> "") Then
            Dim effarr As Array
            effarr = Request.Form("EffectiveDateBox").Split("/") 'Me.EffectiveDateBox.Text.ToString().Split("/")
            'Dim datearr As Array = JDATEBox.Text.ToString().Split("/")
            Dim rtdate As String = effarr(2) + "-" + effarr(1) + "-" + effarr(0)
            modeltypeEntity.Effectivedate = rtdate
        End If
        If (Trim(Request.Form("ObsoleteDateLabBox")) <> "") Then
            Dim obs As Array
            obs = Request.Form("ObsoleteDateLabBox").Split("/") 'Me.ObsoleteDateLabBox.Text.ToString().Split("/")
            Dim obsStr As String = obs(2) + "-" + obs(1) + "-" + obs(0)
            modeltypeEntity.ObsoleteDate = obsStr
        End If
        Me.ObsoleteDateLabBox.ReadOnly = True
        If (Trim(Me.WarrantyDayBox.Text) <> "") Then
            modeltypeEntity.Warrantyperiod = WarrantyDayBox.Text
        End If
        If (Trim(Me.StatusDDL.Text) <> "") Then
            modeltypeEntity.ModelTypeStatus = StatusDDL.SelectedValue
        End If
        If (Trim(Me.modfybybox.Text) <> "") Then
            modeltypeEntity.Modifyby = Session("userID").ToString().ToUpper
        End If
        modeltypeEntity.LoginIP = Request.UserHostAddress.ToString
        modeltypeEntity.LoginSvc = Session("login_svcID").ToString

        'check if there are same model type name in same country
        Dim checkint As Integer = checkname()
        If checkint <> 1 Then

            Dim insJobCnt As Integer = modeltypeEntity.Update()
            If insJobCnt = 0 Then
                Me.mdyinfo.Text = objXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Suc")
                mdyinfo.Visible = True
            Else
                Response.Redirect("~/PresentationLayer/Error.aspx")
            End If
        ElseIf checkint = 1 Then
            Me.mdyinfo.Text = objXmlTr.GetLabelName("StatusMessage", "CHECKNAMEID")
            mdyinfo.Visible = True
        End If
    End Sub
#End Region
#Region "check  if there are same model type name in same courty"
    Public Function checkname() As Integer
        Dim modeltypeEntity As New clsModelType()
        If (Trim(Me.ModelNameBox.Text) <> "") Then
            modeltypeEntity.ModelTypeName = Me.ModelNameBox.Text
        End If
        If (Trim(Me.CountryIDDDL.Text) <> "") Then
            modeltypeEntity.CountryID = Me.CountryIDDDL.Text.ToString().Trim().Split("-")(0)
        End If
        If (Trim(Me.StatusDDL.Text) <> "") Then
            modeltypeEntity.ModelTypeStatus = Me.StatusDDL.SelectedValue
        End If
        If (Trim(Me.ModelIDBox.Text) <> "") Then
            modeltypeEntity.ModelTypeID = Me.ModelIDBox.Text
        End If
        Dim sqldr As SqlDataReader = modeltypeEntity.GetMDTypeDupNM()
        'if there are identical record then tishi error
        Dim i As Integer = 0
        While (sqldr.Read())
            i = 1
        End While
        Return i
    End Function
#End Region

    '#Region "calender eff and obs"
    '    Protected Sub effImageBt_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles effImageBt.Click
    '        Me.effCalendar.Visible = True
    '        '  effCalendar.Attributes.Add("style", " POSITION: absolute")
    '    End Sub

    '    Protected Sub obsImageBt_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles obsImageBt.Click
    '        Me.obsCalendar.Visible = True
    '        '  obsCalendar.Attributes.Add("style", " POSITION: left 50%")
    '    End Sub
    '#End Region

    '    Protected Sub effCalendar_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles effCalendar.SelectionChanged
    '        Me.EffectiveDateBox.Text = effCalendar.SelectedDate.Day.ToString() + "/" + effCalendar.SelectedDate.Month.ToString() + "/" + effCalendar.SelectedDate.Year.ToString()
    '        effCalendar.Visible = False
    '    End Sub

    '    Protected Sub obsCalendar_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles obsCalendar.SelectionChanged
    '        '  Me.ObsoleteDateLabBox.Text = obsCalendar.SelectedDate.Year.ToString() + "/" + obsCalendar.SelectedDate.Month.ToString() + "/" + obsCalendar.SelectedDate.Day.ToString()
    '        Me.ObsoleteDateLabBox.Text = obsCalendar.SelectedDate.Day.ToString() + "/" + obsCalendar.SelectedDate.Month.ToString() + "/" + obsCalendar.SelectedDate.Year.ToString()
    '        obsCalendar.Visible = False
    '    End Sub

    Protected Sub MessageBox1_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox1.GetMessageBoxResponse
        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
            mdfymodel()
        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
        End If
        EffectiveDateBox.Text = Request.Form("EffectiveDateBox")
        ObsoleteDateLabBox.Text = Request.Form("ObsoleteDateLabBox")
    End Sub

#Region "check delete"
    Function check() As Integer
        Dim i As Integer = 0
        Dim modeltypeEntity As New clsModelType()
        If (Trim(Me.ModelIDBox.Text) <> "") Then
            modeltypeEntity.ModelTypeID = Me.ModelIDBox.Text
        End If
        Dim sqlds As DataSet = modeltypeEntity.GetMdtypedel()
        'if there are identical record then tishi error
        For tablecount As Integer = 0 To sqlds.Tables.Count - 1
            If sqlds.Tables(tablecount).Rows.Count > 0 Then
                i = 1
            End If
        Next
        Return i
    End Function
#End Region
End Class
