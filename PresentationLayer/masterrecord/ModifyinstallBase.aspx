<%@ Page Language="VB" AutoEventWireup="false"  EnableEventValidation ="false" CodeFile="ModifyinstallBase.aspx.vb" Inherits="PresentationLayer_masterrecord_ModifyinstallBase_aspx" %>

<%@ Register Assembly="JCalendar" Namespace="JCalendar" TagPrefix="cc2" %>

<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Modify install Base</title>
     <STYLE type="text/css">A:link { COLOR: #336666; TEXT-DECORATION: none }
     
     </STYLE>
     
	<link href="../css/style.css" type="text/css" rel="stylesheet">
      <script language="JavaScript" src="../js/common.js"></script>
      <script language =javascript >
      
      
 function LoadTechnician_CallBack(response){
 
     //if the server-side code threw an exception
     if (response.error != null)
     {
      //we should probably do better than this
      alert(response.error); 
      
      return;
     }

     var ResponseValue = response.value;
     
   
     //if the response wasn't what we expected  
     if (ResponseValue == null || typeof(ResponseValue) != "object")
     {       
      return;
     }
          
     //Get the states drop down
     var ResultList = document.getElementById("<%=Technicianiddrp.ClientID%>");
     ResultList.options.length = 0; //reset the states dropdown
 
    
     //Remember, its length not Length in JavaScript
     for (var i = 0; i < ResponseValue.length; ++i)
     {
     
      //the columns of our rows are exposed like named properties
      var al = ResponseValue[i].split(":");
      var alid = al[0];
      var alname = al[1];
      ResultList.options[ResultList.options.length] =  new Option(alname, alid);
     }
     
     
}
 
 
 function LoadTechnician(objectClient)
{
 
 if (objectClient.selectedIndex > 0){
    var countryid =  '<%=Session("login_ctryID")%>';
    var companyid =  '<%=Session("login_cmpID")%>';
    var servicecenterid = document.getElementById("<%=Sercenteriddrp.ClientID%>").options[document.getElementById("<%=Sercenteriddrp.ClientID%>").selectedIndex].value;
    var rank = '<%=Session("login_rank")%>';
   
   
  
    PresentationLayer_masterrecord_ModifyinstallBase_aspx.GetTechnicianList( countryid, companyid,servicecenterid,rank, LoadTechnician_CallBack);
    
    
    
 }
 else
 {

    document.getElementById("<%=Technicianiddrp.ClientID%>").options.length = 0
 }
}

function LoadReminderDate_CallBack(response){
 
     //if the server-side code threw an exception
     if (response.error != null)
     {
      //we should probably do better than this
      alert(response.error); 
      
      return;
     }

     var ResponseValue = response.value;
     
     document.getElementById("<%=FreSerRemDatetbox.ClientID%>").value=ResponseValue;
        
     
}


 function LoadReminderDate(objectClient){
 
    var countryid =  '<%=Session("login_ctryID")%>';
    var installdate =  document.getElementById("<%=Installeddatetbox.ClientID%>").value;
    var freeserviceid = document.getElementById("<%=SerTypeiddrpd.ClientID%>").options[document.getElementById("<%=SerTypeiddrpd.ClientID%>").selectedIndex].value;
    var freeserviceentitle = document.getElementById("<%=freeserentitledrpd.ClientID%>").options[document.getElementById("<%=freeserentitledrpd.ClientID%>").selectedIndex].value;
    
    PresentationLayer_masterrecord_ModifyinstallBase_aspx.GetReminderDate( countryid, installdate,freeserviceid,freeserviceentitle, LoadReminderDate_CallBack);
    
 }
 
 function LoadFreeServiceEntitled(objectClient){
 
    var countryid =  '<%=Session("login_ctryID")%>';
    var installdate =  document.getElementById("<%=Installeddatetbox.ClientID%>").value;
    var freeserviceid = document.getElementById("<%=SerTypeiddrpd.ClientID%>").options[document.getElementById("<%=SerTypeiddrpd.ClientID%>").selectedIndex].value;
    var freeserviceentitle = document.getElementById("<%=freeserentitledrpd.ClientID%>").options[document.getElementById("<%=freeserentitledrpd.ClientID%>").selectedIndex].value;
    
    
    PresentationLayer_masterrecord_ModifyinstallBase_aspx.GetReminderDate( countryid, installdate,freeserviceid,freeserviceentitle, LoadReminderDate_CallBack);
    
    if (freeserviceentitle == "Y"){
        document.getElementById("<%=SerTypeiddrpd.ClientID%>").disabled=false;
    }
    else {
        document.getElementById("<%=SerTypeiddrpd.ClientID%>").selectedIndex=0;
        document.getElementById("<%=SerTypeiddrpd.ClientID%>").disabled=true;
    }
 }

function bodyOnload(){
    var freeserviceentitle = document.getElementById("<%=freeserentitledrpd.ClientID%>").options[document.getElementById("<%=freeserentitledrpd.ClientID%>").selectedIndex].value;
     
    if (freeserviceentitle == "Y"){
        document.getElementById("<%=SerTypeiddrpd.ClientID%>").disabled=false;
    }
    else {
        document.getElementById("<%=SerTypeiddrpd.ClientID%>").selectedIndex=0;
        document.getElementById("<%=SerTypeiddrpd.ClientID%>").disabled=true;
    }
    }
      </script>
      
</head>
<body onload ="bodyOnload()">
    <form id="Modifyinstallform" runat="server">
    <div>
        <table id="countrytab" border="0" width="100%">
            <tr>
                <td style="width: 100%">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title1.gif" width="5" /></td>
                            <td background="../graph/title_bg.gif" class="style2" width="98%">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="right" background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title_2.gif" width="5" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100%">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" class="style2" width="98%">
                                <font color="red">
                                    <asp:Label ID="errlab" runat="server" Text="Label" Visible="false"></asp:Label></font></td>
                        </tr>
                    </table>
                    <asp:Label ID="Label8" runat="server" ForeColor="Red"></asp:Label></td>
            </tr>
        </table>
        <table id="Installtab" border="0" style="width: 100%">
            <tr>
                <td style="width: 100%;">
                    <table id="Install" bgcolor="#b7e6e6" border="0" cellpadding="2" cellspacing="1" style="width: 100%">
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%; height: 30px">
                                <asp:Label ID="Customernamelab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" colspan="3" style="height: 30px; width: 85%;">
                                <asp:TextBox ID="Customernametbox" runat="server" CssClass="textborder" ReadOnly="True" Width="504px" style="width: 91%"></asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 20%;">
                                <asp:Label ID="customeridlab" runat="server" Text="Label"></asp:Label>
                            </td>
                            <td align="left" style="width: 30%;" colspan="3">
                                <asp:TextBox ID="cusid" runat="server" CssClass="textborder" MaxLength="2" ReadOnly="True"
                                    Width="60%"></asp:TextBox>
                                <asp:LinkButton ID="LinkButton2" runat="server" BackColor="MediumPurple" BorderStyle="Outset"
                                    CausesValidation="False">...</asp:LinkButton>
                                <font color="red"><strong>*</strong></font>
                                <asp:RequiredFieldValidator ID="cusiderr" runat="server" ControlToValidate="cusid" ForeColor="White">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%; height: 30px">
                                <asp:Label ID="CustomerjdeLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%; height: 30px">
                                <asp:TextBox ID="Customerjdetbox" runat="server" CssClass="textborder" MaxLength="2"
                                    ReadOnly="True" Style="width: 90%"></asp:TextBox>
                                </td>
                            <td align="right" style="width: 15%; height: 30px">
                                <asp:Label ID="jderoidlab" runat="server" Text="Label"></asp:Label>
                            </td>
                            <td align="left" style="width: 35%; height: 30px">
                                <asp:TextBox ID="jderoidtbox" runat="server" CssClass="textborder" MaxLength="2" style="width: 90%" ReadOnly="True"></asp:TextBox>
                                </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%; height: 30px">
                                <asp:Label ID="serialnoLab" runat="server" Text="Serial Number"></asp:Label>
                            </td>
                            <td align="left" style="width: 35%; height: 30px">
                                <asp:TextBox ID="serialnotbox" runat="server" CssClass="textborder" Width="152px" MaxLength="15" style="width: 90%"></asp:TextBox>
                                <asp:Label ID="Label2" runat="server" ForeColor="Red" Text="*" Font-Bold="True"></asp:Label>
                                <asp:RequiredFieldValidator ID="serialidRFValid" runat="server" ControlToValidate="serialnotbox" ForeColor="White">*</asp:RequiredFieldValidator></td>
                            <td align="right" style="width: 15%; height: 30px">
                                <asp:Label ID="modelidlab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%; height: 30px">
                                <asp:DropDownList ID="modelidtdrp" runat="server" CssClass="textborder" Enabled="False" style="width: 90%">
                                </asp:DropDownList>
                                <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="*" Font-Bold="True"></asp:Label>
                                &nbsp;<asp:RequiredFieldValidator ID="modelidRFValid"
                                    runat="server" ControlToValidate="modelidtdrp" ForeColor="White">*</asp:RequiredFieldValidator></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%; height: 30px">
                                <asp:Label ID="Contractedlab" runat="server" Text="Label"></asp:Label>
                            </td>
                            <td align="left" style="width: 35%; height: 2px">
                                <asp:DropDownList ID="Contractdrpd" runat="server" CssClass="textborder" style="width: 90%">
                                </asp:DropDownList></td>
                            <td align="right" style="width: 15%; height: 2px">
                                <asp:Label ID="freeserentitlelab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%; height: 2px">
                                <asp:DropDownList ID="freeserentitledrpd" runat="server" CssClass="textborder" AutoPostBack="false"  style="width: 90%" onchange="LoadFreeServiceEntitled(this)">
                                </asp:DropDownList></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%; height: 30px">
                                <asp:Label ID="Installeddatelab" runat="server" Text="Label"></asp:Label><br />
                                <asp:Label ID="Label11" runat="server" Text="DD/MM/YYYY"></asp:Label></td>
                            <td align="left" style="width: 35%; height: 30px">
                                <asp:TextBox ID="Installeddatetbox" runat="server" CssClass="textborder" Width="160px" MaxLength="10" onblur ="LoadFreeServiceEntitled(this)"></asp:TextBox>
                                <asp:HyperLink ID="HypCal" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                                    ToolTip="Choose a Date">Choose a Date</asp:HyperLink><%-- <asp:ImageButton ID="ImageButton1" runat="server" Height="24px" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                                    Width="24px" CausesValidation="False" />--%><%-- <asp:Calendar ID="Calendarinstall" runat="server" Visible="False" BackColor="White" BorderColor="#3366CC" BorderWidth="1px" CellPadding="1" DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="8pt" ForeColor="#003399" Height="80px" Width="120px">
                                    <SelectedDayStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                                    <TodayDayStyle BackColor="#99CCCC" ForeColor="White" />
                                    <SelectorStyle BackColor="#99CCCC" ForeColor="#336666" />
                                    <WeekendDayStyle BackColor="#CCCCFF" />
                                    <OtherMonthDayStyle ForeColor="#999999" />
                                    <NextPrevStyle Font-Size="8pt" ForeColor="#CCCCFF" />
                                    <DayHeaderStyle BackColor="#99CCCC" ForeColor="#336666" Height="1px" />
                                    <TitleStyle BackColor="#003399" BorderColor="#3366CC" BorderWidth="1px" Font-Bold="True"
                                        Font-Size="10pt" ForeColor="#CCCCFF" Height="25px" />
                                </asp:Calendar>--%><cc2:JCalendar ID="InstalleddateCalendar" runat="server"  ControlToAssign="Installeddatetbox" imgurl="~/PresentationLayer/graph/calendar.gif" Visible="False"/>
                                <asp:Label ID="Label5" runat="server" ForeColor="Red" Text="*" Font-Bold="True"></asp:Label>
                                <asp:RequiredFieldValidator ID="inserr" runat="server" ControlToValidate="Installeddatetbox" ForeColor="White">*</asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="CompDateVal" runat="server" ControlToValidate="Installeddatetbox"
                                    Display="Dynamic" Operator="DataTypeCheck" SetFocusOnError="True" Type="Date"></asp:CompareValidator></td>
                            <td align="right" style="width: 15%; height: 30px">
                                <asp:Label ID="producedatelab" runat="server" Text="Label"></asp:Label><br />
                                <asp:Label ID="Label3" runat="server" Text="DD/MM/YYYY"></asp:Label></td>
                            <td align="left" style="width: 35%; height: 30px">
                                <asp:TextBox ID="producedatetbox" runat="server" CssClass="textborder" Width="160px" MaxLength="10"></asp:TextBox>
                                <asp:HyperLink ID="HypCal2" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                                    ToolTip="Choose a Date">Choose a Date</asp:HyperLink><%-- <asp:ImageButton ID="ImageButton2" runat="server" Height="24px" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                                    Width="24px" CausesValidation="False" />--%><%--<asp:Calendar ID="Calendarprodu" runat="server" Visible="False" BackColor="White" BorderColor="#3366CC" BorderWidth="1px" CellPadding="1" DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="8pt" ForeColor="#003399" Height="80px" Width="128px">
                                    <SelectedDayStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                                    <TodayDayStyle BackColor="#99CCCC" ForeColor="White" />
                                    <SelectorStyle BackColor="#99CCCC" ForeColor="#336666" />
                                    <WeekendDayStyle BackColor="#CCCCFF" />
                                    <OtherMonthDayStyle ForeColor="#999999" />
                                    <NextPrevStyle Font-Size="8pt" ForeColor="#CCCCFF" />
                                    <DayHeaderStyle BackColor="#99CCCC" ForeColor="#336666" Height="1px" />
                                    <TitleStyle BackColor="#003399" BorderColor="#3366CC" BorderWidth="1px" Font-Bold="True"
                                        Font-Size="10pt" ForeColor="#CCCCFF" Height="25px" />
                                </asp:Calendar>--%><cc2:JCalendar ID="producedateCalendar" runat="server"  ControlToAssign="producedatetbox" imgurl="~/PresentationLayer/graph/calendar.gif" Visible="False"/>
                                <asp:Label ID="Label4" runat="server" ForeColor="Red" Text="*" Font-Bold="True"></asp:Label>
                                <asp:RequiredFieldValidator ID="proerr" runat="server" ControlToValidate="producedatetbox" ForeColor="White">*</asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="CompDateVal2" runat="server" ControlToValidate="producedatetbox"
                                    Display="Dynamic" Operator="DataTypeCheck" SetFocusOnError="True" Type="Date"></asp:CompareValidator></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%; height: 30px">
                                <asp:Label ID="ProductClasslab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%; height: 30px">
                                <asp:DropDownList ID="productclassdrpd" runat="server" CssClass="textborder" style="width: 90%">
                            </asp:DropDownList></td>
                            <td align="right" style="width: 15%; height: 30px">
                                <asp:Label ID="TechnicianIDlab" runat="server" Text="Label"></asp:Label>
                            </td>
                            <td align="left" style="width: 35%; height: 30px">
                                <asp:DropDownList ID="Technicianiddrp" runat="server" CssClass="textborder" style="width: 90%">
                                </asp:DropDownList></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%; height: 36px" id="#svcctr">
                                <asp:Label ID="Sercenteridlab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%; height: 36px">
                                <asp:DropDownList ID="Sercenteriddrp" runat="server" CssClass="textborder" style="width: 90%" AutoPostBack="false" onchange="LoadTechnician(this)">
                                </asp:DropDownList>
                                <asp:Label ID="Label7" runat="server" Font-Bold="True" ForeColor="Red" Text="*"></asp:Label>
                                <asp:RequiredFieldValidator ID="sercenteriderr" runat="server" ControlToValidate="Sercenteriddrp"
                                    Height="1px" Width="1px" ForeColor="White">*</asp:RequiredFieldValidator>&nbsp;
                            </td>
                            <td align="right" style="width: 15%; height: 36px">
                                <asp:Label ID="TechnicianTypelab" runat="server" Text="label"></asp:Label></td>
                            <td align="left" style="width: 35%; height: 36px">
                                <asp:DropDownList ID="TechnicianTypedrpd" runat="server" CssClass="textborder" style="width: 90%">
                                </asp:DropDownList></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%; height: 35px">
                                <asp:Label ID="WarrExpdatelab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%; height: 35px">
                                <asp:TextBox ID="WarrExpdatetbox" runat="server" CssClass="textborder" ReadOnly="True" style="width: 90%"></asp:TextBox></td>
                            <td align="right" style="width: 15%; height: 35px">
                                <asp:Label ID="SerTypeidlab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%; height: 35px">
                                <asp:DropDownList ID="SerTypeiddrpd" runat="server" CssClass="textborder" Style="width: 90%" AutoPostBack="false" disabled="false" onchange="LoadReminderDate(this)"  Width="168px" >
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%; height: 30px">
                                <asp:Label ID="lastserdatelab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%; height: 30px">
                                <asp:TextBox ID="lastserdatetbox" runat="server" CssClass="textborder" ReadOnly="True" style="width: 90%"></asp:TextBox></td>
                            <td align="right" style="width: 15%; height: 30px">
                                <asp:Label ID="lastremidatelab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%; height: 30px">
                                <asp:TextBox ID="lastremidatetbox" runat="server" CssClass="textborder" ReadOnly="True" style="width: 90%"></asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%; height: 30px">
                                <asp:Label ID="FreSerRemDatelab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%; height: 30px">
                                <asp:TextBox ID="FreSerRemDatetbox" runat="server" CssClass="textborder" ReadOnly =true  style="width: 90%"></asp:TextBox>
                                &nbsp;
                            </td>
                            <td align="right" style="width: 15%; height: 30px">
                                <asp:Label ID="Statuslab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%; height: 30px">
                                <asp:DropDownList ID="statusdrpd" runat="server" CssClass="textborder" style="width: 90%">
                                </asp:DropDownList></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%; height: 30px">
                                <asp:Label ID="Createdbylab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%; height: 30px">
                                <asp:TextBox ID="CreatedBytbox" runat="server" CssClass="textborder" ReadOnly="True" style="width: 90%"></asp:TextBox></td>
                            <td align="right" style="width: 15%; height: 30px">
                                <asp:Label ID="ModifiedBylab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%; height: 30px">
                                <asp:TextBox ID="ModifiedBytbox" runat="server" CssClass="textborder" ReadOnly="True" style="width: 90%"></asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%; height: 30px">
                                <asp:Label ID="createbydatelab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%; height: 30px">
                                <asp:TextBox ID="createbydatetbox" runat="server" CssClass="textborder" ReadOnly="True" style="width: 90%"></asp:TextBox>&nbsp;
                                </td>
                            <td align="right" style="width: 15%; height: 30px">
                                <asp:Label ID="modifybydatelab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%; height: 30px">
                                <asp:TextBox ID="modifybydatetbox" runat="server" CssClass="textborder" ReadOnly="True" style="width: 90%"></asp:TextBox>&nbsp;
                                </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%; height: 30px">
                                <asp:Label ID="Remarkslab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" colspan="3" style="height: 30px; width: 85%;">
                                <asp:TextBox ID="Remarkstbox" runat="server" CssClass="textborder" Height="48px" Width="496px" MaxLength="400"></asp:TextBox></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label9" runat="server" ForeColor="Red"></asp:Label></td>
            </tr>
            <tr>
                <td style="width: 707px; height: 37px">
                    <table id="Table1" border="0" cellpadding="1" cellspacing="1">
                        <tr>
                            <td align="center" style="width: 20%; height: 17px">
                                &nbsp;<asp:LinkButton ID="LinkButton1" runat="server">save</asp:LinkButton></td>
                            <td align="center" style="width: 128px; height: 17px">
                                <asp:HyperLink ID="HyperLink1" runat="server">cancel</asp:HyperLink></td>
                        </tr>
                    </table>
                    &nbsp;
                    <asp:ValidationSummary ID="ValidationSummary" runat="server" ShowMessageBox="True" ShowSummary="False" />
                    <asp:TextBox ID="rouidtbox" runat="server" Visible="False"></asp:TextBox>&nbsp;
                </td>
            </tr>
        </table>
        <cc1:messagebox id="MessageBox1" runat="server"></cc1:messagebox>
    
    </div>
    </form>
</body>
</html>