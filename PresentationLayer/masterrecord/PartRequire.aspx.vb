Imports BusinessEntity
Imports System.Configuration
Imports System.Xml
Imports System.Data.SqlClient
Imports System.Data
Partial Class PresentationLayer_masterrecord_PartRequire
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try
            Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            'linkbutton
            LBadd.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Add")
            LBsearch.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Search")
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-parttl")

            PartIDlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCAT-0001")
            Partname.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCAT-0002")
            statusLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCAT-0016")
            Label1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Maslbleinfo")
            Label1.Visible = False
            Me.addinfo.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-AR")
            Me.addinfo.Visible = False

            'create the dropdownlist
            Dim stat As New clsrlconfirminf()
            Dim param As ArrayList = New ArrayList
            param = stat.searchconfirminf("STATUS")
            Dim count As Integer
            Dim statid As String
            Dim statnm As String
            count = 0
            For count = 0 To param.Count - 1
                statid = param.Item(count)
                statnm = param.Item(count + 1)
                count = count + 1
                If statid.Equals("ACTIVE") Or statid.Equals("OBSOLETE") Then
                    ctryStatDrop.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                End If
            Next
            ctryStatDrop.Items(0).Selected = True

            Dim partEntity As New ClsPartRequirement()
            partEntity.ModifiedBy = Session("username")
            partEntity.logcountryid = Session("login_ctryID")
            partEntity.logrank = Session("login_rank")

            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "06")
            Me.LBadd.Enabled = purviewArray(0)
            'Me.LBsearch.Visible = purviewArray(2)
            ''editcol.Visible = purviewArray(1)
            'Me.partView.Visible = purviewArray(2)
            partEntity.Status = "ACTIVE"

            Dim partall As DataSet = partEntity.Getpartrequire()
            If partall.Tables(0).Rows.Count = 0 Then
                Label1.Visible = True
            Else
                Label1.Visible = False
            End If
            If partall.Tables(0).Rows.Count <> 0 Then
                partView.DataSource = partall
                Session("partView") = partall
                partView.AllowPaging = True
                partView.AllowSorting = True

                Dim editcol As New CommandField
                editcol.EditText = objXmlTr.GetLabelName("EngLabelMsg", "BB-Edit") '"edit"
                editcol.HeaderImageUrl = "~/PresentationLayer/graph/edit.gif"
                editcol.ShowEditButton = True
                partView.Columns.Add(editcol)
                partView.DataBind()
                'set right
                editcol.Visible = purviewArray(2)

                partView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PARTHD0")
                partView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PARTHD1")
                partView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PARTHD2")
                partView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PARTHD3")
                partView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PARTHD4")
                partView.HeaderRow.Cells(6).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PARTHD5")
                partView.HeaderRow.Cells(7).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PARTHD6")
                partView.HeaderRow.Cells(1).Font.Size = 8
                partView.HeaderRow.Cells(2).Font.Size = 8
                partView.HeaderRow.Cells(3).Font.Size = 8
                partView.HeaderRow.Cells(4).Font.Size = 8
                partView.HeaderRow.Cells(5).Font.Size = 8
                partView.HeaderRow.Cells(6).Font.Size = 8
                partView.HeaderRow.Cells(7).Font.Size = 8

            End If
        End If
    End Sub

    Protected Sub LBsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LBsearch.Click
        Dim partEntity As New ClsPartRequirement()
        Dim objXmlTr As New clsXml
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        partView.PageIndex = 0
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        If (Trim(PartIDBox.Text) <> "") Then
            partEntity.partid = PartIDBox.Text
        End If
        If (Trim(PartnmBox.Text) <> "") Then
            partEntity.partname = PartnmBox.Text
        End If
        If (ctryStatDrop.SelectedItem.Value.ToString() <> "") Then
            partEntity.Status = ctryStatDrop.SelectedItem.Value.ToString()
        End If
        If Trim(PartIDBox.Text) = "" And Trim(PartnmBox.Text) = "" Then
            addinfo.Visible = True
        Else
            addinfo.Visible = False
        End If
        partEntity.ModifiedBy = Session("username")
        partEntity.logcountryid = Session("login_ctryID")
        partEntity.logrank = Session("login_rank")
        Dim selDS As DataSet = partEntity.Getpartrequire()
        partView.DataSource = selDS
        partView.DataBind()
        Session("partView") = selDS
        If selDS.Tables(0).Rows.Count = 0 Then
            Label1.Visible = True
        Else
            Label1.Visible = False
          
        End If

        If selDS.Tables(0).Rows.Count <> 0 Then
            partView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PARTHD0")
            partView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PARTHD1")
            partView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PARTHD2")
            partView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PARTHD3")
            partView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PARTHD4")
            partView.HeaderRow.Cells(6).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PARTHD5")
            partView.HeaderRow.Cells(7).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PARTHD6")

            partView.HeaderRow.Cells(1).Font.Size = 8
            partView.HeaderRow.Cells(2).Font.Size = 8
            partView.HeaderRow.Cells(3).Font.Size = 8
            partView.HeaderRow.Cells(4).Font.Size = 8
            partView.HeaderRow.Cells(5).Font.Size = 8
            partView.HeaderRow.Cells(6).Font.Size = 8
            partView.HeaderRow.Cells(7).Font.Size = 8

        End If
    End Sub

    Protected Sub partView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles partView.PageIndexChanging
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        partView.PageIndex = e.NewPageIndex
        Dim partEntity As New ClsPartRequirement()
        If (Trim(PartIDBox.Text) <> "") Then
            partEntity.partid = PartIDBox.Text
        End If
        If (Trim(PartnmBox.Text) <> "") Then
            partEntity.partname = PartnmBox.Text
        End If
        If (ctryStatDrop.SelectedItem.Value.ToString() <> "") Then
            partEntity.Status = ctryStatDrop.SelectedItem.Value.ToString()
        End If
        partEntity.ModifiedBy = Session("username")
        partEntity.logcountryid = Session("login_ctryID")
        partEntity.logrank = Session("login_rank")
        Dim selDS As DataSet = partEntity.Getpartrequire()
        partView.DataSource = selDS
        partView.DataBind()
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        If selDS.Tables(0).Rows.Count <> 0 Then
            partView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PARTHD0")
            partView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PARTHD1")
            partView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PARTHD2")
            partView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PARTHD3")
            partView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PARTHD4")
            partView.HeaderRow.Cells(6).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PARTHD5")
            partView.HeaderRow.Cells(7).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PARTHD6")
            partView.HeaderRow.Cells(1).Font.Size = 8
            partView.HeaderRow.Cells(2).Font.Size = 8
            partView.HeaderRow.Cells(3).Font.Size = 8
            partView.HeaderRow.Cells(4).Font.Size = 8
            partView.HeaderRow.Cells(5).Font.Size = 8
            partView.HeaderRow.Cells(6).Font.Size = 8
            partView.HeaderRow.Cells(7).Font.Size = 8
        End If
    End Sub

    
    Protected Sub partView_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles partView.RowEditing
        Dim ds As DataSet = Session("partView")
        Dim page As Integer = partView.PageIndex * partView.PageSize
        'Dim partid As String = ds.Tables(0).Rows(page + e.NewEditIndex).Item(0).ToString()
        Dim partid As String = Me.partView.Rows(e.NewEditIndex).Cells(1).Text
        partid = Server.UrlEncode(partid)
        'Dim ctrid As String = ds.Tables(0).Rows(page + e.NewEditIndex).Item(2).ToString()
        Dim ctrid As String = Me.partView.Rows(e.NewEditIndex).Cells(3).Text
        ctrid = Server.UrlEncode(ctrid)
        'Dim status As String = ds.Tables(0).Rows(page + e.NewEditIndex).Item(5).ToString
        Dim status As String = Me.partView.Rows(e.NewEditIndex).Cells(6).Text
        Dim i As Integer
        For i = 0 To Me.ctryStatDrop.Items.Count - 1
            If status = Me.ctryStatDrop.Items(i).Text Then
                status = Me.ctryStatDrop.Items(i).Value
            End If
        Next
        status = Server.UrlEncode(status)
        Dim strTempURL As String = "partid=" + partid + "&ctrid=" + ctrid + "&status=" + status
        strTempURL = "~/PresentationLayer/masterrecord/ModifyPartRequire.aspx?" + strTempURL
        Response.Redirect(strTempURL)
    End Sub
End Class
