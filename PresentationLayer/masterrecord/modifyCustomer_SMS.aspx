<%@ Page Language="VB" AutoEventWireup="false" CodeFile="modifyCustomer_SMS.aspx.vb" Inherits="PresentationLayer_masterrecord_modifyCustomer_SMS" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Send SMS</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <strong>
        Text Message<br />
        </strong>
        <br />
        <asp:RadioButton ID="Normal" runat="server" Text="Normal (160 Chars)" AutoPostBack="True" Checked="True" />
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
        <asp:RadioButton ID="Chinese" runat="server" Text="Chinese (70 Chars)" AutoPostBack="True" /><br />
        <asp:TextBox ID="MessageTxt" runat="server" Height="101px" Width="335px" AutoPostBack="True" TextMode="MultiLine">Elken Service appointment: xx/xx/xxxx. Confirmation number:35XXXXXXX  Any doubt pls call XX-XXXXXXXX. From Elken Service (computer generated, do not reply)</asp:TextBox><br />
        <asp:TextBox ID="CountTxt" runat="server" Width="69px"></asp:TextBox>
        Characters &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<asp:Button ID="SubmitBtn"
            runat="server" Text="Send Message" />&nbsp;
        <asp:Label ID="Label1" runat="server" ForeColor="Red"
            Visible="False"></asp:Label><br />
        <span style="color: red">*</span> Press "Tab" or click anywhere outside text input
        to refresh Character Count.</div>
    </form>
</body>
</html>
