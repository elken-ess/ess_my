Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Partial Class PresentationLayer_masterrecord_ModifyKitItem
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    'Response.Redirect("~/PresentationLayer/logon.aspx")
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try
            quantitytbox.Focus()
          
            'get params
            Dim partid As String = Request.Params("partid").ToString()
            Dim kitid As String = Request.Params("kitid").ToString()
            Dim status As String = Request.Params("status").ToString()
            TextBox1.Text = Request.Params("countryid").ToString()
            'display label message
            Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            kitidlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASKITITEM-0001")
            partidLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASKITITEM-0002")
            quantitylab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASKITITEM-0003")
            statusLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASKITITEM-0004")
            creatby.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATBY")
            creatdate.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
            modfyby.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFBY")
            mdfydate.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFDATE")
            saveButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Save")
            cancelLink.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Cancel")
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-ModifyKititemtl")
            Label9.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            Label1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            Dim XmlTr As New clsXml
            XmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            kitiderr.ErrorMessage = XmlTr.GetLabelName("StatusMessage", "BB-HintMsg-KITIDERR")
            partiderr.ErrorMessage = XmlTr.GetLabelName("StatusMessage", "BB-HintMsg-part")
            quantityerr.ErrorMessage = XmlTr.GetLabelName("StatusMessage", "BB-HintMsg-KITQUANTITY")
            Dim strPanm As String = objXmlTr.GetLabelID("StatusMessage", status)
            'bond  PART ID and name
            Dim bond As New clsCommonClass
            Dim rank As String = Session("login_rank")
            bond.rank = rank
            If rank <> "0" Then
                bond.spctr = Session("login_ctryID")
                bond.spstat = Session("login_cmpID")
                bond.sparea = Session("login_svcID")
            End If
            Dim sertype As New DataSet
            sertype = bond.Getcomidname("BB_MASKIT_SPartid")
            If sertype.Tables.Count <> 0 Then
                databonds(sertype, DpDpartid)
            End If
          
            Dim kitEntity As New clsKititem()
            If partid.Trim() <> "" Then
                kitEntity.PartID = partid
                kitEntity.KitiD = kitid
                kitEntity.Status = strPanm
                kitEntity.CountryID = Session("login_ctryID")

                Dim retnArray As ArrayList = kitEntity.GetKitByID()
                kitidbox.Text = retnArray(0).ToString()
                kitidbox.ReadOnly = True
                DpDpartid.Text = retnArray(1).ToString()
                DpDpartid.Enabled = False
                quantitytbox.Text = retnArray(2).ToString()


               
                'create the dropdownlist
                Dim Stat As New clsrlconfirminf()
                Dim statParam As ArrayList = New ArrayList
                statParam = Stat.searchconfirminf("STATUS")
                Dim count As Integer
                Dim statid As String
                Dim statnm As String
                For count = 0 To statParam.Count - 1
                    statid = statParam.Item(count)
                    statnm = statParam.Item(count + 1)
                    If statid.Equals("ACTIVE") Or statid.Equals("OBSOLETE") Or statid.Equals("DELETE") Then
                        statdrp.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                    End If
                    If statid.Equals(retnArray(3).ToString()) Then
                        statdrp.Items(count / 2).Selected = True
                    End If
                    count = count + 1
                Next
                'set qty and status

                Dim i As Integer = kitEntity.Getupdateqty()

                If i = -1 Then
                    Me.statdrp.Items.Remove(Me.statdrp.Items.FindByValue("DELETE"))
                    quantitytbox.ReadOnly = True
                Else
                    quantitytbox.ReadOnly = False
                End If
                Dim Rcreatby As String = retnArray(4).ToString().ToUpper()
                If Rcreatby <> "ADMIN" Then
                    Dim Rcreat As New clsCommonClass
                    Rcreat.userid() = Rcreatby
                    Dim Rcreatds As New DataSet()
                    Rcreatds = Rcreat.Getuseridname("BB_MASUSER_SelByIDName")

                    Me.creatbybox.Text = Rcreatds.Tables(0).Rows(0).Item(0)
                Else
                    Me.creatbybox.Text = "ADMIN-ADMIN"
                End If
                'creatbybox.Text = retnArray(4).ToString()
                creatdtbox.Text = retnArray(5).ToString()
                Dim userIDNamestr As String = Session("userID").ToString().ToUpper + "-" + Session("username").ToString.ToUpper
                modfybybox.Text = userIDNamestr
                mdfydtbox.Text = retnArray(7).ToString()

            End If
        End If
    End Sub
#Region " bonds"
    Public Function databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList) As ArrayList
        dropdown.Items.Clear()
        dropdown.Items.Add(" ")
        Dim row As DataRow
        Dim infon As ArrayList = New ArrayList()
        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            NewItem.Text = row("id") & "-" & row("name")
            NewItem.Value = row("id")
            dropdown.Items.Add(NewItem)
            infon.Add(NewItem.Value)
        Next
        Return infon
    End Function
#End Region
    Protected Sub saveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveButton.Click
        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim msginfo As String = objXm.GetLabelName("StatusMessage", "SAVEORNOT")
        Dim msgtitle As String = objXm.GetLabelName("StatusMessage", "SAVETITLE")
        MessageBox1.Confirm(msginfo)

    End Sub

    Protected Sub cancelLink_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cancelLink.Click
        'return countryid and partid to AddPartRequire.aspx
        'Dim url As String = "~/PresentationLayer/masterrecord/ModifyPartRequire.aspx?"
        'url &= "partid=" & Me.partidbox.Text.ToUpper().ToString & "&"
        'url &= "countryid=" & Me.TextBox1.Text.ToString & "&"
        'url &= "editkit=" & "1"
        'Response.Redirect(url)
        Dim partid As String = kitidbox.Text.ToUpper.ToString
        partid = Server.UrlEncode(partid)
        Dim countryid As String = TextBox1.Text.ToString
        countryid = Server.UrlEncode(countryid)
        Dim editkit As String = "1"
        editkit = Server.UrlEncode(editkit)
        Dim strTempURL As String = "partid=" + partid + "&countryid=" + countryid + "&editkit=" + editkit
        strTempURL = "~/PresentationLayer/masterrecord/ModifyPartRequire.aspx?" + strTempURL
        Response.Redirect(strTempURL)

    End Sub

    Protected Sub MessageBox1_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox1.GetMessageBoxResponse
        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
            Dim KitItemEntity As New clsKititem
            If (Trim(kitidbox.Text) <> "") Then
                KitItemEntity.KitiD = kitidbox.Text.ToUpper()
            End If
            If (Trim(DpDpartid.SelectedItem.Value.ToString()) <> "") Then
                KitItemEntity.PartID = DpDpartid.SelectedItem.Value.ToString()
            End If
            If (Trim(quantitytbox.Text) <> "") Then
                KitItemEntity.KitQuantity = quantitytbox.Text.ToString()
            End If
            If (statdrp.SelectedItem.Value.ToString() <> "") Then
                KitItemEntity.Status = statdrp.SelectedItem.Value.ToString()
            End If
            If (Trim(creatbybox.Text) <> "") Then
                KitItemEntity.CreateBy = Session("userID").ToString().ToUpper
            End If
            If (Trim(modfybybox.Text) <> "") Then
                KitItemEntity.ModifyBy = Session("userID").ToString().ToUpper
            End If

            KitItemEntity.CountryID = Session("login_ctryID")
            KitItemEntity.logsercenter = Session("login_svcID")
            KitItemEntity.ipadress = Request.UserHostAddress.ToString()
            Dim updCtryCnt As Integer = KitItemEntity.Update()
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            If updCtryCnt = 0 Then
                Dim objXm As New clsXml
                objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                errlab.Text = objXm.GetLabelName("StatusMessage", "BB-HintMsg-Suc")
                errlab.Visible = True
            Else
                Response.Redirect("~/PresentationLayer/Error.aspx")
            End If
        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
        End If
    End Sub

End Class
