Imports SQLDataAccess
Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Partial Class PresentationLayer_masterrecord_addSalTaxID
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try

        If Not Page.IsPostBack Then
            Dim returnpsid As Integer
            returnpsid = Request.QueryString("saltaxsetupid")
            Session("saxsybom") = False

            Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            saltaxIDLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTAX-0001")
            salname.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTAX-0002")
            ALterNamelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTAX-0003")
            CountryIDlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0003")
            addperButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0010")
            'EffectiveDatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRID-0006")
            CreatedBylab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATBY")
            CreatedDatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
            ModifiedBylab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFBY")
            ModifiedDatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFDATE")
            StatusLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Status")
            saveButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Save")
            cancelLink.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Cancel")
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTAX-0012")
            perlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTAX-0005")
            addperButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTAX-0011")
            Me.lblCompTop.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            Me.lblCompBottom.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")

            AddLink.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Add")
            AddLink.NavigateUrl = Page.Request.RawUrl

            'Session("symbl") = False

            Dim objXmlTr1 As New clsXml
            objXmlTr1.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            saltaxIDva.ErrorMessage = objXmlTr1.GetLabelName("StatusMessage", "SALTAXID")
            salnameva.ErrorMessage = objXmlTr1.GetLabelName("StatusMessage", "SALTAXNAME")
            COUNTRYIDVA.ErrorMessage = objXmlTr1.GetLabelName("StatusMessage", "CountryID")
            'salnameVA.ErrorMessage = objXmlTr1.GetLabelName("StatusMessage", "SALTAXNAME")

            '''access control'''''''''''''''''''''''''''''''''''''''''''''''
            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "18")
            Me.savebutton.Enabled = purviewArray(0)
            Me.addperButton.Enabled = purviewArray(0)
            If purviewArray(0) = False Then
                Response.Redirect("~/PresentationLayer/logon.aspx")

            End If


            Me.saltaxIDbox.Focus()
            Dim userIDNamestr As String = Session("userID").ToString().ToUpper + "-" + Session("username").ToString.ToUpper


            CreatedBy.Text = userIDNamestr
            ModifiedBy.Text = userIDNamestr
            CreatedDate.Text = Date.Now()
            ModifiedDate.Text = Date.Now()
            'CreatedBy.Text = Session("username").ToString().ToUpper()
            'ModifiedBy.Text = Session("username").ToString().ToUpper()
            Dim rank As String = Session("login_rank").ToString().ToUpper()
            Dim country As New clsCommonClass
            If rank <> 0 Then
                country.spctr = Session("login_ctryID").ToString().ToUpper()
            End If
            country.rank = rank
            Dim countryds As New DataSet
            countryds = country.Getcomidname("BB_MASCTRY_IDNAME")
            If countryds.Tables.Count <> 0 Then
                databonds(countryds, Me.countryiddrop)
                Me.countryiddrop.Items.Insert(0, "")
            End If
            countryiddrop.SelectedValue = Session("login_ctryID").ToString().ToUpper()
            'Dim country As New clsCommonClass
            'Dim countryds As New DataSet
            'countryds = country.Getidname("BB_MASCTRY_IDNAME")
            'databonds(countryds, Me.countryiddrop)
            'countryiddrop.Items(0).Selected = True


            Dim cntryStat As New clsrlconfirminf()
            Dim statParam As ArrayList = New ArrayList
            statParam = cntryStat.searchconfirminf("STATUS")
            Dim count As Integer
            Dim statid As String
            Dim statnm As String
            For count = 0 To statParam.Count - 1
                statid = statParam.Item(count)
                statnm = statParam.Item(count + 1)
                count = count + 1
                If (statid.Equals("ACTIVE") Or statid.Equals("OBSOLETE")) Then
                    Me.StatusDrop.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                End If
            Next
            StatusDrop.Items(0).Selected = True


            Dim salsetupid As New clsSalTaxID
            Dim n As Integer
            n = salsetupid.Getmaxid()
            SetUpID.Text = n + 1

            'return  master table
            If returnpsid <> 0 Then
                Session("saxsybom") = True

                Dim salSetUpEntity1 As New clsSalTaxID()
                salSetUpEntity1.SalTaxSetUpID = returnpsid

                Dim retnArray As ArrayList = salSetUpEntity1.GetReturnSALTAXIDDetailsByID()

                'create the dropdownlist
                Dim userIDNamestr1 As String = Session("userID").ToString().ToUpper + "-" + Session("username").ToString.ToUpper
                ModifiedBy.Text = userIDNamestr1

                Dim Rcreatby As String = retnArray.Item(6).ToString().ToUpper()
                If Rcreatby <> "ADMIN" Then
                    Dim Rcreat As New clsCommonClass
                    Rcreat.userid() = Rcreatby
                    Dim Rcreatds As New DataSet()
                    Rcreatds = Rcreat.Getuseridname("BB_MASUSER_SelByIDName")

                    Me.CreatedBy.Text = Rcreatds.Tables(0).Rows(0).Item(0)
                Else
                    Me.CreatedBy.Text = "ADMIN-ADMIN"
                End If


                SetUpID.Text = retnArray(0)
                saltaxIDBox.Text = retnArray(1).ToString()

                StatusDrop.Text = retnArray(5).ToString()
                'CreatedBy.Text = retnArray(6).ToString()
                CreatedBy.ReadOnly = True
                CreatedDate.ReadOnly = True
                'ModifiedBy.Text = retnArray(8).ToString()

                CreatedDate.Text = retnArray(7).ToString()
                CreatedDate.ReadOnly = True
                ModifiedDate.Text = retnArray(9).ToString()
                countryiddrop.Text = retnArray(4).ToString()
                salnameBox.Text = retnArray(2).ToString()
                ALterNamebox.Text = retnArray(3).ToString()

            End If

            'display detail table

            Dim objXmlTr2 As New clsXml
            objXmlTr2.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            Dim salSetUpEntity2 As New clsSalTaxID()
            salSetUpEntity2.SalTaxSetUpID = returnpsid
            Dim SalSetUpall As DataSet = salSetUpEntity2.GetSalTaxIDdetail()
            saltaxsetupView.DataSource = SalSetUpall
            Session("saltaxsetupView") = SalSetUpall
            saltaxsetupView.AllowPaging = True
            saltaxsetupView.AllowSorting = True
            saltaxsetupView.DataBind()
            If SalSetUpall.Tables(0).Rows.Count <> 0 Then
                saltaxsetupView.HeaderRow.Cells(0).Text = objXmlTr2.GetLabelName("EngLabelMsg", "BB-MASSTAX-0013")
                saltaxsetupView.HeaderRow.Cells(0).Font.Size = 8
                saltaxsetupView.HeaderRow.Cells(1).Text = objXmlTr2.GetLabelName("EngLabelMsg", "BB-MASSTAX-0005")
                saltaxsetupView.HeaderRow.Cells(1).Font.Size = 8
                saltaxsetupView.HeaderRow.Cells(2).Text = objXmlTr2.GetLabelName("EngLabelMsg", "BB-MASSTAX-0006")
                saltaxsetupView.HeaderRow.Cells(2).Font.Size = 8
                saltaxsetupView.HeaderRow.Cells(3).Text = objXmlTr2.GetLabelName("EngLabelMsg", "BB-Status")
                saltaxsetupView.HeaderRow.Cells(3).Font.Size = 8
                saltaxsetupView.HeaderRow.Cells(4).Text = objXmlTr2.GetLabelName("EngLabelMsg", "BB-CREATDATE")
                saltaxsetupView.HeaderRow.Cells(4).Font.Size = 8

            End If

            If Session("saxsybom") = True Then
                savebutton.Enabled = False
            End If
        End If


    End Sub

#Region " id bond"
    Public Function databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        Dim row As DataRow
        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            NewItem.Text = row("id") & "-" & row("name")
            NewItem.Value = row("id")
            dropdown.Items.Add(NewItem)
        Next

    End Function
#End Region


    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveButton.Click
        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim msginfo As String = objXm.GetLabelName("StatusMessage", "SAVEORNOT")
        Dim msgtitle As String = objXm.GetLabelName("StatusMessage", "SAVETITLE")
        MessageBox1.Confirm(msginfo)
        Dim dsPriceIDall As DataSet = Session("saltaxsetupView")
        saltaxsetupView.DataSource = dsPriceIDall
        saltaxsetupView.DataBind()
        saltaxsetupView.AllowPaging = True
        saltaxsetupView.AllowSorting = True
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        If dsPriceIDall.Tables(0).Rows.Count <> 0 Then
            saltaxsetupView.HeaderRow.Cells(0).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTAX-0013")
            saltaxsetupView.HeaderRow.Cells(0).Font.Size = 8
            saltaxsetupView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTAX-0005")
            saltaxsetupView.HeaderRow.Cells(1).Font.Size = 8
            saltaxsetupView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTAX-0006")
            saltaxsetupView.HeaderRow.Cells(2).Font.Size = 8
            saltaxsetupView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Status")
            saltaxsetupView.HeaderRow.Cells(3).Font.Size = 8
            saltaxsetupView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
            saltaxsetupView.HeaderRow.Cells(4).Font.Size = 8

        End If
       

    End Sub




    Protected Sub LinkButton2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles addperButton.Click
        Dim objXm1 As New clsXml
        objXm1.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        If Me.StatusDrop.SelectedValue <> "ACTIVE" Then
            errlab.Text = objXm1.GetLabelName("StatusMessage", "PRICESETUPADDERROR")
            errlab.Visible = True

            Return
        End If
        'If Session("symbl") = True Then

        If savebutton.Enabled = False Then

            Dim url As String = "~/PresentationLayer/masterrecord/addSalTaxID1.aspx?"
            url &= "saltaxsetupid=" & Me.SetUpID.Text & "&"
            url &= "symbl=" & 0
            Response.Redirect(url)
        Else
            Dim objXm As New clsXml
            objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            errlab.Text = objXm.GetLabelName("StatusMessage", "IFSAVE")
            errlab.Visible = True


        End If


    End Sub

    Protected Sub MessageBox1_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox1.GetMessageBoxResponse
        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
            Dim pricesidStat As New clsCommonClass()

            Dim SALSetUpEntity As New clsSalTaxID()
            If (Trim(SetUpID.Text) <> "") Then
                SALSetUpEntity.SalTaxSetUpID = SetUpID.Text
            End If
            If (Trim(saltaxIDBox.Text) <> "") Then
                SALSetUpEntity.SalTaxID = saltaxIDBox.Text.ToString().ToUpper()
            End If
            If (countryiddrop.SelectedValue().ToString() <> "") Then
                SALSetUpEntity.CountryID = countryiddrop.SelectedItem.Value.ToString()
            End If
            If (Trim(salnameBox.Text) <> "") Then
                SALSetUpEntity.SalTaxName = salnameBox.Text.ToString().ToUpper()
            End If
            If (Trim(ALterNamebox.Text) <> "") Then
                SALSetUpEntity.SalTaxAlternateName = ALterNamebox.Text.ToString().ToUpper()
            End If
            If (StatusDrop.SelectedItem.Value.ToString() <> "") Then
                SALSetUpEntity.SalTaxStatus = StatusDrop.SelectedItem.Value.ToString()
            End If
            'If (Trim(CreatedBy.Text) <> "") Then
            '    SALSetUpEntity.CreatedBy = CreatedBy.Text.ToUpper()
            'End If
            If (Trim(CreatedDate.Text) <> "") Then
                SALSetUpEntity.CreatedDate = pricesidStat.DatetoDatabase(CreatedDate.Text)
            End If
            'If (Trim(ModifiedBy.Text) <> "") Then
            '    SALSetUpEntity.ModifiedBy = ModifiedBy.Text.ToUpper()
            'End If
            If (Trim(ModifiedDate.Text) <> "") Then
                SALSetUpEntity.ModifiedDate = pricesidStat.DatetoDatabase(ModifiedDate.Text)
            End If

            If (Trim(Me.CreatedBy.Text) <> "") Then
                SALSetUpEntity.CreatedBy = Session("userID").ToString.ToUpper()
            End If
            If (Trim(Me.ModifiedBy.Text) <> "") Then
                SALSetUpEntity.ModifiedBy = Session("userID").ToString.ToUpper()
            End If
            Dim dupCount As Integer = SALSetUpEntity.GetDuplicatedSalTaxID()
            If dupCount.Equals(-1) Then
                Dim objXm As New clsXml
                objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                errlab.Text = objXm.GetLabelName("StatusMessage", "DUPSALIDNAME")
                errlab.Visible = True
            ElseIf dupCount.Equals(0) Then
                Dim insPRIDCnt As Integer = SALSetUpEntity.Insert()
                If insPRIDCnt = 0 Then
                    Dim objXm As New clsXml
                    objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    errlab.Text = objXm.GetLabelName("StatusMessage", "SuccessfullySaved")
                    errlab.Visible = True
                    saveButton.Enabled = False
                Else

                    Response.Redirect("~/PresentationLayer/Error.aspx")

                End If
            End If
            Dim dsPriceIDall As DataSet = Session("saltaxsetupView")
            saltaxsetupView.DataSource = dsPriceIDall
            saltaxsetupView.DataBind()
            saltaxsetupView.AllowPaging = True
            saltaxsetupView.AllowSorting = True
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            If dsPriceIDall.Tables(0).Rows.Count <> 0 Then
                saltaxsetupView.HeaderRow.Cells(0).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTAX-0013")
                saltaxsetupView.HeaderRow.Cells(0).Font.Size = 8
                saltaxsetupView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTAX-0005")
                saltaxsetupView.HeaderRow.Cells(1).Font.Size = 8
                saltaxsetupView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTAX-0006")
                saltaxsetupView.HeaderRow.Cells(2).Font.Size = 8
                saltaxsetupView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Status")
                saltaxsetupView.HeaderRow.Cells(3).Font.Size = 8
                saltaxsetupView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
                saltaxsetupView.HeaderRow.Cells(4).Font.Size = 8

            End If

        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
        End If

      


    End Sub

    Protected Sub saltaxsetupView_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles saltaxsetupView.PageIndexChanging
        saltaxsetupView.PageIndex = e.NewPageIndex
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Dim dsPriceIDall As DataSet = Session("saltaxsetupView")
        saltaxsetupView.DataSource = dsPriceIDall
        saltaxsetupView.DataBind()
        saltaxsetupView.AllowPaging = True
        saltaxsetupView.AllowSorting = True
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        If dsPriceIDall.Tables(0).Rows.Count <> 0 Then
            saltaxsetupView.HeaderRow.Cells(0).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTAX-0013")
            saltaxsetupView.HeaderRow.Cells(0).Font.Size = 8
            saltaxsetupView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTAX-0005")
            saltaxsetupView.HeaderRow.Cells(1).Font.Size = 8
            saltaxsetupView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTAX-0006")
            saltaxsetupView.HeaderRow.Cells(2).Font.Size = 8
            saltaxsetupView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Status")
            saltaxsetupView.HeaderRow.Cells(3).Font.Size = 8
            saltaxsetupView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
            saltaxsetupView.HeaderRow.Cells(4).Font.Size = 8

        End If
      
    End Sub
   
End Class
