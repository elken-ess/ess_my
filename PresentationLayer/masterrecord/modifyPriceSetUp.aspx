<%@ Page Language="VB" AutoEventWireup="false" EnableEventValidation="false"  CodeFile="modifyPriceSetUp.aspx.vb" Inherits="PresentationLayer_masterrecord_modifyPriceSetUp" %>

<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Modify Price Setup</title>
    <meta http-equiv="Content-Type" content="text/html; charset=gb2312">
	<LINK href="../css/style.css" type="text/css" rel="stylesheet">
</head>
<body>
    <form id="form1" runat="server">
    
      <TABLE border="0" id=countrytab style="width: 100%">
           <tr>
           <td style="width: 100%; height: 40px;"  >
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>
							<TD width="1%" background="../graph/title_bg.gif"><IMG height="24" src="../graph/title1.gif" width="5"></TD>
							<TD class="style2" width="98%" background="../graph/title_bg.gif" style="width: 100%">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></TD>
							<TD align="right" width="1%" background="../graph/title_bg.gif"><IMG height="24" src="../graph/title_2.gif" width="5"></TD>
						</TR>
			 </TABLE>
           </tr>
           <tr>
           <td style="width: 100%"  >
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>					
							<TD class="style2" width="98%" background="../graph/title_bg.gif">
                                <font color=red><asp:Label ID="errlab" runat="server" Text="Label" Visible=false></asp:Label></font></TD>
						</TR>
			 </TABLE>
           </tr>
			<TR>
				<TD style="width: 100%" >
     <TABLE border="0" id=PriceIDtab style="width: 100%">
			<TR>
				<TD style="width: 100%"  >
					<TABLE id="PriceSetUp" cellSpacing="1" cellPadding="2" bgColor="#b7e6e6" border="0" style="width: 100%">
					
<tr bgcolor="#ffffff">
<td colspan = 4>
                            <asp:Label ID="lblNoteUP" runat="server" ForeColor="Red" Text="Label" Visible="True"></asp:Label>
</td>
             </tr>
             <tr bgcolor="#ffffff">
<td colspan = 4>
                        	&nbsp;
                     	</td>
               </tr>

									  <TR bgColor="#ffffff">
											<TD  align=right width="15%" style="width: 20%" >
                                                <asp:Label ID="PriceIDLab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 30%" >
                                                <asp:DropDownList ID="PriceIDDrop" runat="server" Width="80%" CssClass="textborder" Enabled="False">
                                                </asp:DropDownList>
                                                <asp:Label ID="Label2" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                <asp:RequiredFieldValidator ID="priceidva" runat="server" ForeColor="White" Visible="False" ControlToValidate="PriceIDDrop">*</asp:RequiredFieldValidator></TD>
                                                <TD  align=right width="15%" style="width: 20%" >
                                                <asp:Label ID="PartIDLab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 30%"  >
                                                <asp:TextBox ID="partidbox" runat="server" Width="97%" ReadOnly="True" CssClass="textborder"></asp:TextBox>
                                            </TD>
                                                
										</TR>
										
										<TR bgColor="#ffffff">
											<TD  align=right width="15%" style="width: 20%; height: 30px">
                                                <asp:Label ID="CountryIDlab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 30%; height: 30px" >
                                                <asp:TextBox ID="countryidBox" runat="server" Width="90%" ReadOnly="True" CssClass="textborder"></asp:TextBox>
                                            </TD>
                                                <TD  align=right width="15%" style="width: 20%; height: 30px">
                                                <asp:Label ID="StatusLab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left width="85%" colspan=3 style="width: 30%; height: 30px">
                                                <asp:DropDownList ID="StatusDrop" runat="server" Width="90%" CssClass="textborder">
                                                </asp:DropDownList></TD>
											
										</TR>
										
										
										<TR bgColor="#ffffff">
											<TD  align=right width="15%" style="width: 20%; height: 30px" >
                                                <asp:Label ID="CreatedBylab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 30%; height: 30px" >
                                                <asp:TextBox ID="CreatedBy" runat="server" CssClass="textborder" ReadOnly="True" Width="90%"></asp:TextBox></TD>
											<TD  align=right width="15%" style="width: 20%; height: 30px" >
                                                <asp:Label ID="CreatedDatelab" runat="server" Text="Label"></asp:Label></TD>
											<TD  align=left style="width: 30%; height: 30px" >
                                                <asp:TextBox ID="CreatedDate" runat="server" Width="97%" CssClass="textborder" ReadOnly="True"></asp:TextBox>
                                               
                                            </TD>
										</TR>
										<TR bgColor="#ffffff">
											<TD  align=right width="15%" style="width: 20%" >
                                                <asp:Label ID="ModifiedBylab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 30%" >
                                                <asp:TextBox ID="ModifiedBy" runat="server" CssClass="textborder" ReadOnly="True" Width="90%"></asp:TextBox></TD>
											<TD  align=right width="15%" style="width: 20%" >
                                                <asp:Label ID="ModifiedDatelab" runat="server" Text="Label"></asp:Label></TD>
											<TD  align=left style="width: 30%"  >
                                                <asp:TextBox ID="ModifiedDate" runat="server" Width="97%" CssClass="textborder" ReadOnly="True"></asp:TextBox>
                                               
                                            </TD>
										</TR>
										
										
								</TABLE>
								<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>
							<TD width="1%" background="../graph/title_bg.gif" style="height: 22px"><IMG height="24" src="../graph/title1.gif" width="5"></TD>
							<TD class="style2" width="98%" background="../graph/title_bg.gif" style="height: 22px">
                                <asp:Label ID="amuntLabel" runat="server" Text="Label"></asp:Label>
                                <asp:LinkButton ID="AddButton" runat="server">LinkButton</asp:LinkButton></TD>
							<TD align="right" width="1%" background="../graph/title_bg.gif" style="height: 22px"><IMG height="24" src="../graph/title_2.gif" width="5"></TD>
						</TR>
			 </TABLE>
                    <asp:GridView ID="PriceSetUp1View" runat="server" Width="100%" AllowPaging="True">
                    </asp:GridView>
                    </TD>
					</TR>
					<TR>
						<TD  >
							<TABLE id="Table1" cellSpacing="1" cellPadding="1"  border="0">
							    <tr bgcolor="#ffffff">
<td colspan = 2>
                        	&nbsp;
                     	</td>
               </tr>
							
<tr bgcolor="#ffffff">
<td colspan = 2>
                            <asp:Label ID="lblNoteDown" runat="server" ForeColor="Red" Text="Label" Visible="True"></asp:Label>
</td>
             </tr>
         

									<TR>
										<TD style="width: 89px" >
                                            &nbsp;<asp:LinkButton ID="saveButton" runat="server"></asp:LinkButton></TD>
										<td align=center style="height: 16px">
                                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<asp:HyperLink ID="cancelLink" runat="server" NavigateUrl="~/PresentationLayer/masterrecord/PriceSetUp.aspx">[cancelLink]</asp:HyperLink></td>
									</TR>
							</TABLE>
						</TD>
					</TR>
			</TABLE>
			</TABLE>
       <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
           ShowSummary="False" />
                    <asp:TextBox ID="SetUpID" runat="server" Visible="False"></asp:TextBox>
                    <cc1:messagebox id="MessageBox1" runat="server"></cc1:messagebox>

    </form>
</body>
</html>
