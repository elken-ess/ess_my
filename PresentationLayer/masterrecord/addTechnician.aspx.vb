Imports BusinessEntity
Imports System.Configuration
Imports System.Xml
Imports System.Data
Partial Class PresentationLayer_masterrecord_addTechnician
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.MaintainScrollPositionOnPostBack = True
        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try

        BindGrid()
    End Sub
    Protected Sub BindGrid()
        If Not Page.IsPostBack Then
            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "11")
            If purviewArray(0) = False Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return

            End If
            Me.LinkButton1.Enabled = purviewArray(0)
            Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "ADDTECHNI")
            Statuslab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCTRY-0004")
            Me.Address1lab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0006")
            Me.Address2lab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0007")
            AlternateNameLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0005")

            POCodelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0008")
            AreaIDlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0009")
            CountryIDlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCTRY-0001")
            StateIDlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0011")
            Telephone1lab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0012")
            Telephone2lab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0013")

            Me.lblCompTop.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            Me.lblCompBottom.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")

            Me.AreaID2lab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0009")
            Faxlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0015")
            Me.CountryID2lab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCTRY-0001")
            Me.DrivingLicenselab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMTCH-0008")

            Me.MailAddress1lab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMTCH-0006")
            Me.MailAddress2lab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMTCH-0007")
            Me.MaxJobdaylab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMTCH-0009")
            Me.Mobilelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMTCH-0010")
            Me.NRIClab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMTCH-0004")
            Me.POCode2lab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0008")
            Me.ServiceCenterlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0001")
            Me.StateID2lab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0011")
            Me.TechnicianIDlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMTCH-0001")
            Me.TechnicianNamelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMTCH-0002")
            Me.TechnicianTypelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMTCH-0005")


            CreatedBylab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATBY")
            CreatedDatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
            ModiBylab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFBY")
            ModifiedDatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFDATE")
            LinkButton1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Save")
            HyperLink1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Cancel")
            AddLink.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Add")
            AddLink.NavigateUrl = Page.Request.RawUrl
            Dim statXmlTr As New clsXml
            statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            Me.POCodeMsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-POCodein")
            'Me.POCodeMsg2.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-POCode")
            Me.FaxMsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Fax")
            Me.Telephone1Msg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Tele")
            Me.Telephone2Msg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Tele")
            Me.mobileMsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Mobile")
            Me.MaxJobdayMsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-MaxJobday")
            Me.TechnicianIDMsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Techid")
            Me.NRICMsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Technric")
            Me.AlternateNamemsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-AlterName")

            Me.TechnicianNamemsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Techname")
            Me.countrymsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "MAS-COUNTRY")
            Me.statmsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "MAS-STAT")
            Me.areamsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "MAS-AREA")
            Me.svcmsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "MAS-SVC")
            Me.TechnicianTypemsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "TECHTMSG")
            Me.DrivingLicensemsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "DRIVEMSG")
            Dim rank As String = Session("login_rank")
            Dim userIDNamestr As String = Session("userID").ToString().ToUpper + "-" + Session("username").ToString.ToUpper

            Me.CreatedBy.Text = userIDNamestr
            Me.CreatedDate.Text = Date.Now()
            Me.ModiBy.Text = userIDNamestr
            Me.ModifiedDate.Text = Date.Now()
            Dim technician As New clsTechnician()
            technician.username = userIDNamestr
            'create  the dropdownlist
            'create the dropdownlist

            Dim cntryStat As New clsrlconfirminf()

            Dim statParam As ArrayList = New ArrayList
            statParam = cntryStat.searchconfirminf("TECHSTATUS")
            Dim count As Integer
            Dim statid As String
            Dim statnm As String
            For count = 0 To statParam.Count - 1
                statid = statParam.Item(count)
                statnm = statParam.Item(count + 1)
                count = count + 1
                If statid.Equals("ACTIVE") Then
                    Me.Status.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                End If
            Next
            Status.Items(0).Selected = True

            'create  BRANCHTYPE the dropdownlist
            Dim Techniciantype As New clsrlconfirminf()
            Dim TechnicianParam As ArrayList = New ArrayList
            TechnicianParam = Techniciantype.searchconfirminf("TECHNICIAN")
            Dim Techniciancount As Integer
            Dim Technicianid As String
            Dim Techniciannm As String
            For Techniciancount = 0 To TechnicianParam.Count - 1
                Technicianid = TechnicianParam.Item(Techniciancount)
                Techniciannm = TechnicianParam.Item(Techniciancount + 1)
                Techniciancount = Techniciancount + 1
                Me.TechnicianType.Items.Add(New ListItem(Techniciannm.ToString(), Technicianid.ToString()))
            Next


            Dim dltype As New clsrlconfirminf()
            Dim dlParam As ArrayList = New ArrayList
            dlParam = dltype.searchconfirminf("YESNO")
            Dim dlcount As Integer
            Dim dlid As String
            Dim dlnm As String
            For dlcount = 0 To dlParam.Count - 1
                dlid = dlParam.Item(dlcount)
                dlnm = dlParam.Item(dlcount + 1)
                dlcount = dlcount + 1
                Me.DrivingLicense.Items.Add(New ListItem(dlnm.ToString(), dlid.ToString()))
            Next


            'create  country the dropdownlist
            Dim country As New clsCommonClass

            If rank <> 0 Then
                country.spctr = Session("login_ctryID").ToString().ToUpper

            End If
            country.rank = rank

            Dim countryds As New DataSet


            countryds = country.Getcomidname("BB_MASCTRY_IDNAME")
            If countryds.Tables.Count <> 0 Then
                databonds(countryds, Me.CountryID)
                databonds(countryds, Me.CountryID2)
                Me.CountryID2.Items.Insert(0, "")
                Me.CountryID.Items.Insert(0, "")
            End If
            Me.CountryID.SelectedValue = Session("login_ctryID").ToString().ToUpper

            'create  state the dropdownlist
            Dim stat As New clsCommonClass
            If (CountryID.SelectedValue().ToString() <> "") Then
                stat.spctr = Me.CountryID.SelectedValue().ToString().ToUpper()
            End If

            stat.rank = rank
            Dim stateds As New DataSet

            stateds = stat.Getcomidname("BB_MASSTAT_IDNAME")
            If stateds.Tables.Count <> 0 Then
                databonds(stateds, Me.StateID)
            End If
            Me.StateID.Items.Insert(0, "")
            Dim area As New clsCommonClass


            If (CountryID.SelectedValue().ToString() <> "") Then
                area.spctr = Me.CountryID.SelectedValue().ToString().ToUpper()
            End If

            If (StateID.SelectedValue().ToString() <> "") Then
                area.spstat = Me.StateID.SelectedValue().ToString().ToUpper()
            End If
            area.rank = rank

            'create  area the dropdownlist
            'Dim area As New clsCommonClass
            Dim areads As New DataSet

            areads = area.Getcomidname("BB_MASAREA_IDNAME")
            If areads.Tables.Count <> 0 Then
                databonds(areads, Me.AreaID)
            End If
            Me.AreaID.Items.Insert(0, "")
            'Dim state2 As New clsCommonClass

            'If (CountryID2.SelectedValue().ToString() <> "") Then
            '    state2.spctr = Me.CountryID2.SelectedValue().ToString().ToUpper()
            'End If

            'state2.rank = rank
            ''Dim state As New clsCommonClass
            'Dim stateds2 As New DataSet

            'stateds2 = state2.Getcomidname("BB_MASSTAT_IDNAME")
            'If stateds2.Tables.Count <> 0 Then
            '    databonds(stateds2, Me.StateID2)
            '    Me.StateID2.Items.Insert(0, "")
            'End If
            'Dim area2 As New clsCommonClass
            ''Dim ctridname As String
            'If (CountryID2.SelectedValue().ToString() <> "") Then
            '    area2.spctr = Me.CountryID2.SelectedValue().ToString().ToUpper()
            'End If
            ''Dim staid As String
            'If (StateID2.SelectedValue().ToString() <> "") Then
            '    area2.spstat = Me.StateID2.SelectedValue().ToString().ToUpper()
            'End If
            ''create  area the dropdownlist
            ''Dim area As New clsCommonClass
            'area2.rank = rank
            'Dim areads2 As New DataSet

            'areads2 = area2.Getcomidname("BB_MASAREA_IDNAME")
            'If areads2.Tables.Count <> 0 Then
            '    databonds(areads2, Me.AreaID2)
            '    Me.AreaID2.Items.Insert(0, "")
            'End If

            Dim service As New clsCommonClass
            If rank = 7 Then
                service.spctr = Me.CountryID.SelectedValue().ToString().ToUpper()

            End If
            If rank = 8 Then
                service.spctr = Me.CountryID.SelectedValue().ToString().ToUpper()
                service.spstat = Session("login_cmpID")
            End If
            If rank = 9 Then
                service.spctr = Me.CountryID.SelectedValue().ToString().ToUpper()
                service.spstat = Session("login_cmpID")
                service.sparea = Session("login_svcID")
            End If
            If rank = 0 Then
                service.spctr = Me.CountryID.SelectedValue().ToString().ToUpper()
            End If
            service.rank = rank
            Dim serviceds As New DataSet

            serviceds = service.Getcomidname("BB_MASSVRC_IDNAME")
            If serviceds.Tables.Count <> 0 Then
                databonds(serviceds, Me.ServiceCenter)
            End If
            Me.ServiceCenter.Items.Insert(0, "")

            ServiceCenter.SelectedValue = Session("login_svcID")
            'If (Trim(Me.MailAddress1.Text) <> "") Then

            '    Me.POCode2.Enabled = True
            '    Me.CountryID2.Enabled = True
            '    Me.AreaID2.Enabled = True
            '    Me.StateID2.Enabled = True

            'End If
            Me.TechnicianName.Focus()

        End If
    End Sub
#Region " id bond"
    Public Sub databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        Dim row As DataRow
        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            NewItem.Text = row("id") & "-" & row("name")
            NewItem.Value = row("id")
            dropdown.Items.Add(NewItem)
        Next

    End Sub
#End Region

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton1.Click
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim msginfo As String = objXm.GetLabelName("StatusMessage", "SAVEORNOT")
        Dim msgtitle As String = objXm.GetLabelName("StatusMessage", "SAVETITLE")
        MessageBox1.Confirm(msginfo)
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    End Sub

    Protected Sub CountryID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CountryID.SelectedIndexChanged
        Me.StateID.Items.Clear()
        Me.AreaID.Items.Clear()
        Me.ServiceCenter.Items.Clear()
        Dim rank As String = Session("login_rank")
        Dim state As New clsCommonClass

        If (CountryID.SelectedValue().ToString() <> "") Then
            state.spctr = Me.CountryID.SelectedValue().ToString().ToUpper()
        End If
        state.rank = rank

        'Dim state As New clsCommonClass
        Dim stateds As New DataSet

        stateds = state.Getcomidname("BB_MASSTAT_IDNAME")
        If stateds.Tables.Count <> 0 Then
            databonds(stateds, Me.StateID)
        End If
        Me.StateID.Items.Insert(0, "")
        Dim area As New clsCommonClass
        'Dim ctridname As String
        If (CountryID.SelectedValue().ToString() <> "") Then
            area.spctr = Me.CountryID.SelectedValue().ToString().ToUpper()
        End If
        'Dim staid As String
        If (StateID.SelectedValue().ToString() <> "") Then
            area.spstat = Me.StateID.SelectedValue().ToString().ToUpper()
        End If
        area.rank = rank

        'create  area the dropdownlist
        'Dim area As New clsCommonClass
        Dim areads As New DataSet

        areads = area.Getcomidname("BB_MASAREA_IDNAME")
        If areads.Tables.Count <> 0 Then
            databonds(areads, Me.AreaID)
        End If
        Me.AreaID.Items.Insert(0, "")
        Dim service As New clsCommonClass
        If rank = 7 Then
            service.spctr = Me.CountryID.SelectedValue().ToString().ToUpper()

        End If
        If rank = 8 Then
            service.spctr = Me.CountryID.SelectedValue().ToString().ToUpper()
            service.spstat = Session("login_cmpID")
        End If
        If rank = 9 Then
            service.spctr = Me.CountryID.SelectedValue().ToString().ToUpper()
            service.spstat = Session("login_cmpID")
            service.sparea = Session("login_svcID")
        End If
        If rank = 0 Then
            service.spctr = Me.CountryID.SelectedValue().ToString().ToUpper()
        End If
        service.rank = rank
        Dim serviceds As New DataSet

        serviceds = service.Getcomidname("BB_MASSVRC_IDNAME")
        If serviceds.Tables.Count <> 0 Then
            databonds(serviceds, Me.ServiceCenter)
        End If
        Me.ServiceCenter.Items.Insert(0, "")
    End Sub

    Protected Sub StateID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles StateID.SelectedIndexChanged
        Me.AreaID.Items.Clear()
        Dim rank As String = Session("login_rank")
        Dim area As New clsCommonClass
        'Dim ctridname As String
        If (CountryID.SelectedValue().ToString() <> "") Then
            area.spctr = Me.CountryID.SelectedValue().ToString().ToUpper()
        End If
        'Dim staid As String
        If (StateID.SelectedValue().ToString() <> "") Then
            area.spstat = Me.StateID.SelectedValue().ToString().ToUpper()
        End If
        area.rank = rank

        'create  area the dropdownlist
        'Dim area As New clsCommonClass
        Dim areads As New DataSet

        areads = area.Getcomidname("BB_MASAREA_IDNAME")
        If areads.Tables.Count <> 0 Then
            databonds(areads, Me.AreaID)
        End If
        Me.AreaID.Items.Insert(0, "")
    End Sub

    Protected Sub CountryID2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CountryID2.SelectedIndexChanged
        Me.AreaID2.Items.Clear()
        Me.StateID2.Items.Clear()
        Dim rank As String = Session("login_rank")
        Dim state As New clsCommonClass

        If (CountryID2.SelectedValue().ToString() <> "") Then
            state.spctr = Me.CountryID2.SelectedValue().ToString().ToUpper()
        End If
        state.rank = rank

        'Dim state As New clsCommonClass
        Dim stateds As New DataSet

        stateds = state.Getcomidname("BB_MASSTAT_IDNAME")
        If stateds.Tables.Count <> 0 Then
            databonds(stateds, Me.StateID2)
        End If
        Me.StateID2.Items.Insert(0, "")
        Dim area As New clsCommonClass
        'Dim ctridname As String
        If (CountryID2.SelectedValue().ToString() <> "") Then
            area.spctr = Me.CountryID2.SelectedValue().ToString().ToUpper()
        End If
        'Dim staid As String
        If (StateID2.SelectedValue().ToString() <> "") Then
            area.spstat = Me.StateID2.SelectedValue().ToString().ToUpper()
        End If
        area.rank = rank

        'create  area the dropdownlist
        'Dim area As New clsCommonClass
        Dim areads As New DataSet

        areads = area.Getcomidname("BB_MASAREA_IDNAME")
        If areads.Tables.Count <> 0 Then
            databonds(areads, Me.AreaID2)
        End If
        Me.AreaID2.Items.Insert(0, "")
        'Dim script As String = "self.location='#POCode';"
        'Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "POCode", script, True)
    End Sub

    Protected Sub StateID2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles StateID2.SelectedIndexChanged
        Me.AreaID2.Items.Clear()
        Dim rank As String = Session("login_rank")
        Dim area As New clsCommonClass
        'Dim ctridname As String
        If (CountryID2.SelectedValue().ToString() <> "") Then
            area.spctr = Me.CountryID2.SelectedValue().ToString().ToUpper()
        End If
        'Dim staid As String
        If (StateID2.SelectedValue().ToString() <> "") Then
            area.spstat = Me.StateID2.SelectedValue().ToString().ToUpper()
        End If
        area.rank = rank

        'create  area the dropdownlist
        'Dim area As New clsCommonClass
        Dim areads As New DataSet

        areads = area.Getcomidname("BB_MASAREA_IDNAME")
        If areads.Tables.Count <> 0 Then
            databonds(areads, Me.AreaID2)
        End If
        Me.AreaID2.Items.Insert(0, "")
        'Dim script As String = "self.location='#POCode';"
        'Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "POCode", script, True)
    End Sub

    Protected Sub MessageBox1_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox1.GetMessageBoxResponse
        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
            Dim TechnicianEntity As New clsTechnician()
            Dim telepass As New clsCommonClass()

            'If ( Trim(Me.Address1.Text)) <> "")  Then
            '    TechnicianEntity.Address1 = Me.Address1.Text.ToString()
            'End If
            If (Trim(Me.Address1.Text) <> "") Then
                TechnicianEntity.Address1 = Me.Address1.Text.ToString().ToUpper()

            End If
            If (Trim(Me.Address2.Text) <> "") Then
                TechnicianEntity.Address2 = Me.Address2.Text.ToString().ToUpper()
            End If
            If (Trim(Me.AlternateName.Text) <> "") Then
                TechnicianEntity.AlternateName = Me.AlternateName.Text.ToString().ToUpper()
            End If
            If (Status.SelectedValue().ToString() <> "") Then

                TechnicianEntity.Status = Me.Status.SelectedValue().ToString()
            End If

            'If (Trim(Request.Params.Item("AlternateName").ToString()) <> "") Then
            '    TechnicianEntity. = Me.AlternateName.Text.ToString()
            'End If

            If (AreaID.SelectedValue().ToString() <> "") Then
                TechnicianEntity.AreaID = Me.AreaID.SelectedValue().ToString()
            End If
            If (AreaID2.SelectedValue().ToString() <> "") Then
                TechnicianEntity.AreaID2 = Me.AreaID2.SelectedValue().ToString()
            End If
            If (DrivingLicense.SelectedValue().ToString() <> "") Then
                TechnicianEntity.DrivingLicense = Me.DrivingLicense.SelectedValue().ToString()
            End If
            If (Trim(Me.MailAddress1.Text) <> "") Then
                TechnicianEntity.MailingAddress1 = Me.MailAddress1.Text.ToString().ToUpper()
                

            End If
            If (Trim(Me.MailAddress2.Text) <> "") Then
                TechnicianEntity.MailingAddress2 = Me.MailAddress2.Text.ToString().ToUpper()
            End If
            If (CountryID.SelectedValue().ToString() <> "") Then
                TechnicianEntity.CountryID = Me.CountryID.SelectedValue().ToString()
            End If
            If (CountryID2.SelectedValue().ToString() <> "") Then
                TechnicianEntity.CountryID2 = Me.CountryID2.SelectedValue().ToString()
            End If
            If (StateID.SelectedValue().ToString() <> "") Then
                TechnicianEntity.StateID = Me.StateID.SelectedValue().ToString()
            End If
            If (StateID2.SelectedValue().ToString() <> "") Then
                TechnicianEntity.StateID2 = Me.StateID2.SelectedValue().ToString()
            End If

            If (Trim(Me.Fax.Text) <> "") Then
                TechnicianEntity.Fax = telepass.telconvertpass(Me.Fax.Text.ToString())
            End If
            If (Trim(Me.Mobile.Text) <> "") Then
                TechnicianEntity.Mobile = telepass.telconvertpass(Me.Mobile.Text.ToString())
            End If
            If (Trim(Me.MaxJobday.Text.ToString()) <> "") Then
                TechnicianEntity.MaxJobday = Convert.ToInt32(Me.MaxJobday.Text.ToString())
            End If

            If (Trim(Me.NRIC.Text) <> "") Then
                TechnicianEntity.NRIC = Me.NRIC.Text.ToString().ToUpper()
            End If
            If (Trim(Me.TechnicianID.Text) <> "") Then
                TechnicianEntity.TechnicianID = Me.TechnicianID.Text.ToString().ToUpper()
            End If
            If (Trim(Me.POCode.Text) <> "") Then
                TechnicianEntity.POCode = Me.POCode.Text.ToString()
            End If
            If (Trim(Me.POCode2.Text) <> "") Then
                TechnicianEntity.POCode2 = Me.POCode2.Text.ToString()
            End If
            If (Trim(Me.Telephone1.Text) <> "") Then
                TechnicianEntity.Telephone1 = telepass.telconvertpass(Me.Telephone1.Text.ToString())
            End If
            If (Trim(Me.Telephone2.Text) <> "") Then
                TechnicianEntity.Telephone2 = telepass.telconvertpass(Me.Telephone2.Text.ToString())
            End If
            If (TechnicianType.SelectedValue().ToString() <> "") Then
                TechnicianEntity.TechnicianType = Me.TechnicianType.SelectedValue().ToString()
            End If
            If (ServiceCenter.SelectedValue().ToString() <> "") Then
                TechnicianEntity.ServiceCenter = Me.ServiceCenter.SelectedValue().ToString()
            End If
            If (Trim(Me.TechnicianName.Text) <> "") Then
                TechnicianEntity.TechnicianName = Me.TechnicianName.Text.ToString().ToUpper()
            End If

            If (Trim(CreatedBy.Text) <> "") Then
                TechnicianEntity.CreateBy = Session("userID").ToString().ToUpper
            End If
            'If (Trim(CreatedDate.Text) <> "") Then
            '    TechnicianEntity.CreateDate = Me.CreatedDate.Text()
            'End If
            If (Trim(ModiBy.Text) <> "") Then
                TechnicianEntity.ModifyBy = Session("userID").ToString().ToUpper
            End If
            TechnicianEntity.username = Session("userID")
            If Me.MailAddress1.Text <> "" Then
                If Me.POCode2.Text = "" Then
                    Dim objXm As New clsXml
                    objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    errlab.Text = objXm.GetLabelName("StatusMessage", "TECHMAILPO")
                    errlab.Visible = True
                    Return
                End If
                If CountryID2.SelectedValue().ToString() = "" Then
                    Dim objXm As New clsXml
                    objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    errlab.Text = objXm.GetLabelName("StatusMessage", "TECHMAILCOUNTRY")
                    errlab.Visible = True
                    Return
                End If
                If StateID2.SelectedValue().ToString() = "" Then
                    Dim objXm As New clsXml
                    objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    errlab.Text = objXm.GetLabelName("StatusMessage", "TECHMAILSTATE")
                    errlab.Visible = True
                    Return
                End If
                If AreaID2.SelectedValue().ToString() = "" Then
                    Dim objXm As New clsXml
                    objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    errlab.Text = objXm.GetLabelName("StatusMessage", "TECHMAILAREA")
                    errlab.Visible = True
                    Return
                End If
            End If
            If Me.MailAddress1.Text = "" And Me.MailAddress2.Text = "" Then
                If Me.POCode2.Text <> "" Then
                    Dim objXm As New clsXml
                    objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    errlab.Text = objXm.GetLabelName("StatusMessage", "TECHMAILADRES1")
                    errlab.Visible = True
                    Return
                End If
                If CountryID2.SelectedValue().ToString() <> "" Then
                    Dim objXm As New clsXml
                    objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    errlab.Text = objXm.GetLabelName("StatusMessage", "TECHMAILADRES1")
                    errlab.Visible = True
                    Return
                End If
                If StateID2.SelectedValue().ToString() <> "" Then
                    Dim objXm As New clsXml
                    objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    errlab.Text = objXm.GetLabelName("StatusMessage", "TECHMAILADRES1")
                    errlab.Visible = True
                    Return
                End If
                If AreaID2.SelectedValue().ToString() <> "" Then
                    Dim objXm As New clsXml
                    objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    errlab.Text = objXm.GetLabelName("StatusMessage", "TECHMAILADRES1")
                    errlab.Visible = True
                    Return
                End If
            End If
            Dim dupCount As Integer = TechnicianEntity.GetDuplicatedTechnician()
            If dupCount.Equals(-1) Then
                Dim objXm As New clsXml
                objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                errlab.Text = objXm.GetLabelName("StatusMessage", "DUPTCHIDORNM")
                errlab.Visible = True
            ElseIf dupCount.Equals(0) Then
                Dim NRICCount As New clsTechnician()
                Dim count As New Integer
                count = NRICCount.GetNRICCount(Me.NRIC.Text.ToString(), Me.TechnicianID.Text.ToString())

                If (count <> 0) Then
                    Dim objXm As New clsXml
                    objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    errlab.Text = objXm.GetLabelName("StatusMessage", "DUPTCHNM")
                    errlab.Visible = True
                    'End If
                    'Dim selvalue As Integer = TechnicianEntity.GetDetailsByNRIC()
                    'If selvalue <> 0 Then
                    '    'Response.Redirect("~/PresentationLayer/Error.aspx")
                    '    Dim objXm As New clsXml
                    '    objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    '    errlab.Text = objXm.GetLabelName("StatusMessage", "DUPTCHNM")
                    '    errlab.Visible = True
                ElseIf (count = 0) Then


                    Dim insCtryCnt As Integer = TechnicianEntity.Insert()

                    Dim objXmlTr As New clsXml
                    objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    If insCtryCnt = 0 Then

                        Dim objXm As New clsXml
                        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                        errlab.Text = objXm.GetLabelName("StatusMessage", "BB-HintMsg-Suc")
                        errlab.Visible = True
                    Else

                        Response.Redirect("~/PresentationLayer/Error.aspx")
                    End If
                End If
            End If
        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
        End If
    End Sub

    'Protected Sub MailAddress1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles MailAddress1.TextChanged
    '    If (Trim(Me.MailAddress1.Text) <> "") Then

    '        Me.POCode2.Enabled = True
    '        Me.MailAddress2.Enabled = True

    '        Dim country As New clsCommonClass
    '        Dim rank As String = Session("login_rank")
    '        If rank <> 0 Then
    '            country.spctr = Session("login_ctryID").ToString().ToUpper

    '        End If
    '        country.rank = rank

    '        Dim countryds As New DataSet


    '        countryds = country.Getcomidname("BB_MASCTRY_IDNAME")
    '        If countryds.Tables.Count <> 0 Then

    '            databonds(countryds, Me.CountryID2)
    '            Me.CountryID2.Items.Insert(0, "")

    '        End If
    '    Else
    '        Me.POCode2.Text = "".ToString()
    '        Me.POCode2.Enabled = False
    '        Me.MailAddress2.Text = "".ToString()
    '        Me.MailAddress2.Enabled = False

    '        Me.AreaID2.Items.Clear()
    '        Me.StateID2.Items.Clear()
    '        Me.CountryID2.Items.Clear()
    '    End If
    'End Sub

    Protected Sub Mobile_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Mobile.TextChanged

    End Sub

End Class
