<%@ Page Language="VB" AutoEventWireup="false"  EnableEventValidation ="false"  CodeFile="addTechnician.aspx.vb" Inherits="PresentationLayer_masterrecord_addTechnician" %>

<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Add Technician</title>
    <LINK href="../css/style.css" type="text/css" rel="stylesheet">
    <STYLE type="text/css">A:link { COLOR: #336666; TEXT-DECORATION: none }
	BODY { FONT-SIZE: 9pt; FONT-FAMILY: "Arial", "Verdana", "Tahoma"; BACKGROUND-COLOR: #ffffff }
	TD { FONT-SIZE: 9pt; FONT-FAMILY: Arial,Verdana,Tahoma }
	</STYLE>
</head>
<body style="width: 100%">
    <form id="form1" runat="server">
    <TABLE border="0" id=TABLE2 style="width: 100%">
           <tr>
           <td style="width: 100%">
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>
							<TD width="1%" background="../graph/title_bg.gif"><IMG height="24" src="../graph/title1.gif" width="5"></TD>
							<TD class="style2" width="98%" background="../graph/title_bg.gif">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></TD>
							<TD align="right" width="1%" background="../graph/title_bg.gif"><IMG height="24" src="../graph/title_2.gif" width="5"></TD>
						</TR>
			 </TABLE>
           </tr>
           <tr>
           <td style="width:100%">
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>					
							<TD class="style2" width="98%" background="../graph/title_bg.gif">
                                <font color=red><asp:Label ID="errlab" runat="server" Text="Label" Visible=false></asp:Label></font>
                               </TD>
						</TR>
			 </TABLE>
           </tr>
			<TR>
				<TD style="width:100%">
        <table id="countrytab" border="0" width="100%">
            <tr>
                <td style="width: 368%">
                    <table id="country" bgcolor="#b7e6e6" border="0" cellpadding="2" cellspacing="1"
                        style="width: 100%;">
                        <tr bgcolor="#ffffff">
<td colspan = 4 style="height: 19px">
                            <asp:Label ID="lblCompTop" runat="server" ForeColor="Red" Text="Label" Visible="True"></asp:Label>
</td>
             </tr>
             <tr bgcolor="#ffffff">
<td colspan = 4>
                        	&nbsp;
                     	</td>
               </tr>    
                        <tr bgcolor="#ffffff">
                        <td align="right" style="width: 15%;">
                                <asp:Label ID="TechnicianNamelab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%; color: red;">
                                <asp:TextBox ID="TechnicianName" runat="server" CssClass="textborder" Width="87%" MaxLength="50"></asp:TextBox>
                                <asp:Label ID="Label2" runat="server" Font-Bold="True" ForeColor="Red" Text="*" Width="14px"></asp:Label><asp:RequiredFieldValidator
                                    ID="TechnicianNamemsg" runat="server" ControlToValidate="TechnicianName" ErrorMessage="*" ForeColor="White">*</asp:RequiredFieldValidator></td>
                                  
                            <td align="right" style="width: 15%;">
                                &nbsp;<asp:Label ID="TechnicianIDlab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 39%;">
                                <asp:TextBox ID="TechnicianID" runat="server" CssClass="textborder" MaxLength="10"
                                    Width="87%"></asp:TextBox>
                                <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="Red" Text="*" Width="14px"></asp:Label><asp:RequiredFieldValidator
                                        ID="TechnicianIDMsg" runat="server" ControlToValidate="TechnicianID" ForeColor="White">*</asp:RequiredFieldValidator></td>
                              
                                    
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%;">
                                &nbsp;<asp:Label ID="Statuslab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 39%;">
                                <span style="width: 35%; color: #ff0000;">
                                    <asp:DropDownList ID="Status" runat="server" CssClass="textborder" Width="87%">
                                    </asp:DropDownList>&nbsp;</span></td>
                            <td align="right" style="width: 15%;">
                                <asp:Label ID="AlternateNameLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%; color: red;">
                                <asp:TextBox ID="AlternateName" runat="server" CssClass="textborder" Width="87%" MaxLength="50"></asp:TextBox>
                                <asp:Label ID="Label3" runat="server" Font-Bold="True" ForeColor="Red" Text="*" Width="14px"></asp:Label><asp:RequiredFieldValidator
                                    ID="AlternateNamemsg" runat="server" ControlToValidate="AlternateName" ErrorMessage="*" ForeColor="White">*</asp:RequiredFieldValidator></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%">
                                <asp:Label ID="NRIClab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 39%;">
                                <asp:TextBox ID="NRIC" runat="server" CssClass="textborder" Width="87%" MaxLength="50"></asp:TextBox>
                                <span
                                    style="width: 58%; color: #ff0000; height: 28%"><asp:RegularExpressionValidator
                                        ID="NRICMsg" runat="server" ControlToValidate="NRIC" ValidationExpression="\d{0,50}" ForeColor="White">*</asp:RegularExpressionValidator></span></td>
                            <td align="right" style="width: 15%">
                                <asp:Label ID="TechnicianTypelab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%">
                                <asp:DropDownList ID="TechnicianType" runat="server" CssClass="textborder" Width="87%">
                                </asp:DropDownList>
                                <asp:Label ID="Label5" runat="server" Font-Bold="True" ForeColor="Red" Text="*" Width="14px"></asp:Label><asp:RequiredFieldValidator ID="TechnicianTypemsg" runat="server" ControlToValidate="TechnicianType"
                                    ForeColor="White">*</asp:RequiredFieldValidator></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%; height: 30px;">
                                <asp:Label ID="Address1lab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" colspan="3" style="height: 30px">
                                <asp:TextBox ID="Address1" runat="server" CssClass="textborder" Width="87%" MaxLength="100"></asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%">
                                <asp:Label ID="Address2lab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" colspan="3" style="height: 30px">
                                <asp:TextBox ID="Address2" runat="server" CssClass="textborder" Width="87%" MaxLength="100"></asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%" >
                                <asp:Label ID="POCodelab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 39%; color: #ff0066;">
                                <asp:TextBox ID="POCode" runat="server" CssClass="textborder" Width="87%" MaxLength="10"></asp:TextBox>
                                <asp:Label ID="Label6" runat="server" Font-Bold="True" ForeColor="Red" Text="*" Width="14px"></asp:Label>
                                <asp:RequiredFieldValidator ID="POCodeMsg" runat="server" ControlToValidate="POCode"
                                    ForeColor="White">*</asp:RequiredFieldValidator>
                                    <span style="width: 45%; color: #ff0000; height: 28%"></span></td>
                            <td align="right" style="width: 15%">
                                <asp:Label ID="CountryIDlab" runat="server" Text="Label"></asp:Label>
                            </td>
                            <td align="left" style="width: 35%">
                                <asp:DropDownList ID="CountryID" runat="server" CssClass="textborder" Width="87%" AutoPostBack="True">
                                </asp:DropDownList>
                                <asp:Label ID="Label8" runat="server" Font-Bold="True" ForeColor="Red" Text="*" Width="14px"></asp:Label><asp:RequiredFieldValidator ID="countrymsg" runat="server" ControlToValidate="CountryID" ForeColor="White">*</asp:RequiredFieldValidator></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%">
                                <asp:Label ID="StateIDlab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 39%;">
                                <asp:DropDownList ID="StateID" runat="server" CssClass="textborder" Width="87%" AutoPostBack="True">
                                </asp:DropDownList>
                                <asp:Label ID="Label7" runat="server" Font-Bold="True" ForeColor="Red" Text="*" Width="14px"></asp:Label><asp:RequiredFieldValidator ID="statmsg" runat="server" ControlToValidate="StateID" ForeColor="White">*</asp:RequiredFieldValidator></td>
                            <td align="right" style="width: 15%">
                                <asp:Label ID="AreaIDlab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%;">
                                <asp:DropDownList ID="AreaID" runat="server" CssClass="textborder" Width="87%">
                                </asp:DropDownList>
                                <asp:Label ID="Label9" runat="server" Font-Bold="True" ForeColor="Red" Text="*" Width="14px"></asp:Label><asp:RequiredFieldValidator ID="areamsg" runat="server" ControlToValidate="AreaID" ForeColor="White">*</asp:RequiredFieldValidator></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%">
                                <asp:Label ID="Telephone1lab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 39%;">
                                <asp:TextBox ID="Telephone1" runat="server" CssClass="textborder" Width="87%" MaxLength="20"></asp:TextBox>
                                <span
                                    style="width: 50%; color: #ff0000; height: 28%"></span><asp:RegularExpressionValidator
                                        ID="Telephone1Msg" runat="server" ControlToValidate="Telephone1" ValidationExpression="\d{0,20}" ForeColor="White">*</asp:RegularExpressionValidator></td>
                            <td align="right" style="width: 15%">
                                <asp:Label ID="Mobilelab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%;">
                                <asp:TextBox ID="Mobile" runat="server" CssClass="textborder" Width="87%" MaxLength="20"></asp:TextBox>&nbsp;
                                <asp:RegularExpressionValidator ID="mobileMsg" runat="server" ControlToValidate="Mobile"
                                    ValidationExpression="\d{0,20}" ForeColor="White">*</asp:RegularExpressionValidator></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%;">
                                <asp:Label ID="Telephone2lab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 39%;">
                                <asp:TextBox ID="Telephone2" runat="server" CssClass="textborder" Width="87%" MaxLength="20"></asp:TextBox>
                                <span
                                    style="width: 50%; color: #ff0000; height: 28%"></span><asp:RegularExpressionValidator
                                        ID="Telephone2Msg" runat="server" ControlToValidate="Telephone2" ValidationExpression="\d{0,20}" ForeColor="White">*</asp:RegularExpressionValidator></td>
                            <td align="right" style="width: 15%">
                                <asp:Label ID="Faxlab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%;">
                                <asp:TextBox ID="Fax" runat="server" CssClass="textborder" Width="87%" MaxLength="20"></asp:TextBox>&nbsp;
                                <asp:RegularExpressionValidator ID="FaxMsg" runat="server" ControlToValidate="Fax"
                                    ValidationExpression="\d{0,20}" ForeColor="White">*</asp:RegularExpressionValidator></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%">
                                <asp:Label ID="MailAddress1lab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" colspan="3" style="height: 28%">
                                <asp:TextBox ID="MailAddress1" runat="server" CssClass="textborder" Width="87%" MaxLength="100"></asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%;">
                                <asp:Label ID="MailAddress2lab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" colspan="3" style="height: 28%">
                                <asp:TextBox ID="MailAddress2" runat="server" CssClass="textborder" Width="87%" MaxLength="100"></asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%;">
                                <asp:Label ID="POCode2lab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 39%;">
                                <asp:TextBox ID="POCode2" runat="server" CssClass="textborder" Width="87%" MaxLength="10"></asp:TextBox><span
                                    style="width: 50%; color: #ff0000; height: 28%"></span></td>
                            <td align="right" style="width: 15%">
                                <asp:Label ID="CountryID2lab" runat="server" Text="Label"></asp:Label>
                            </td>
                            <td align="left" style="width: 35%;">
                                <asp:DropDownList ID="CountryID2" runat="server" CssClass="textborder" Width="87%" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%">
                                <asp:Label ID="StateID2lab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 39%;">
                                <asp:DropDownList ID="StateID2" runat="server" CssClass="textborder" Width="87%" AutoPostBack="True">
                                </asp:DropDownList></td>
                            <td align="right" style="width: 15%">
                                <asp:Label ID="AreaID2lab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%;">
                                <asp:DropDownList ID="AreaID2" runat="server" CssClass="textborder" Width="87%">
                                </asp:DropDownList></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%;">
                                <asp:Label ID="DrivingLicenselab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 39%;">
                                <asp:DropDownList ID="DrivingLicense" runat="server" CssClass="textborder" Width="87%">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="DrivingLicensemsg" runat="server" ControlToValidate="DrivingLicense"
                                    ForeColor="White">*</asp:RequiredFieldValidator></td>
                            <td align="right" style="width: 15%;">
                                <asp:Label ID="ServiceCenterlab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%;">
                                <asp:DropDownList ID="ServiceCenter" runat="server" CssClass="textborder" Width="87%">
                                </asp:DropDownList>
                                <asp:Label ID="Label4" runat="server" Font-Bold="True" ForeColor="Red" Text="*"
                                    Width="14px"></asp:Label><asp:RequiredFieldValidator ID="svcmsg" runat="server" ControlToValidate="ServiceCenter" ForeColor="White">*</asp:RequiredFieldValidator></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%;">
                                <asp:Label ID="MaxJobdaylab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" colspan="3">
                                <asp:TextBox ID="MaxJobday" runat="server" CssClass="textborder" Width="39%" MaxLength="2"></asp:TextBox>
                                <span
                                    style="width: 50%; color: #ff0000; height: 28%"></span><asp:RangeValidator ID="MaxJobdayMsg"
                                        runat="server" ControlToValidate="MaxJobday" MaximumValue="2147483647" MinimumValue="0"
                                        Type="Integer" ForeColor="White">*</asp:RangeValidator>&nbsp;</td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%">
                                <asp:Label ID="CreatedBylab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 39%;">
                                <asp:TextBox ID="CreatedBy" runat="server" CssClass="textborder" ReadOnly="true"
                                    Width="87%"></asp:TextBox></td>
                            <td align="right" style="width: 15%">
                                <asp:Label ID="CreatedDatelab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%;">
                                <asp:TextBox ID="CreatedDate" runat="server" CssClass="textborder" ReadOnly="true"
                                    Width="87%"></asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%;">
                                &nbsp;<asp:Label ID="ModiBylab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 39%;">
                                <asp:TextBox ID="ModiBy" runat="server" CssClass="textborder" ReadOnly="true" Width="87%"></asp:TextBox></td>
                            <td align="right" style="width: 15%;">
                                <asp:Label ID="ModifiedDatelab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%;">
                                <asp:TextBox ID="ModifiedDate" runat="server" CssClass="textborder" ReadOnly="true"
                                    Width="87%"></asp:TextBox></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 368%; height: 37%">
                <font color=red><asp:Label ID="lblCompBottom" runat="server" Text="Label" Visible="true"></asp:Label></font>
                    <table id="Table1" border="0" cellpadding="1" cellspacing="1">
                        <tr>
                            <td align="center" style="width: 20%">
                                &nbsp;<asp:LinkButton ID="LinkButton1" runat="server">save</asp:LinkButton></td>
                            <td align="center">
                                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/PresentationLayer/masterrecord/Technican.aspx">cancel</asp:HyperLink></td>
                        <td align=center>
<asp:HyperLink ID="AddLink" runat="server" >[Add]</asp:HyperLink>
</td></tr>
                    </table>
                </td>
            </tr>
        </table>
        </table>
        &nbsp;
         <cc1:MessageBox ID="MessageBox1" runat="server" />
         <br />
        &nbsp;
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True" ShowSummary="False" />
    
                    
    </form>
</body>
</html>
