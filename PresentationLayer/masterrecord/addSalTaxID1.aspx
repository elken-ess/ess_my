<%@ Page Language="VB" AutoEventWireup="false" EnableEventValidation="false"  CodeFile="addSalTaxID1.aspx.vb" Inherits="PresentationLayer_masterrecord_addSalTaxID1" %>

<%@ Register Assembly="JCalendar" Namespace="JCalendar" TagPrefix="cc2" %>

<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>add country message</title>
    <meta http-equiv="Content-Type" content="text/html; charset=gb2312">
	<LINK href="../css/style.css" type="text/css" rel="stylesheet">
	<script language="JavaScript" src="../js/common.js"></script>
</head>
<body  id="SALTAXSetUp1">
   <form id="SALTAXSetUp1form"  method="post" action=""  runat=server>
     <TABLE border="0" id=SALTAXSetUp1tab style="width: 100%">
           <tr>
           <td  >
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0" style="width: 100%">
						<TR>
							<TD width="1%" background="../graph/title_bg.gif" style="height: 24px"><IMG height="24" src="../graph/title1.gif" width="5"></TD>
							<TD class="style2" background="../graph/title_bg.gif" style="width: 100%; height: 24px">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></TD>
							<TD align="right" background="../graph/title_bg.gif" style="width: 1%; height: 24px">
                                <img height="24" src="../graph/title_2.gif" width="5" /></TD>
						</TR>
			 </TABLE>
           </tr>
           <tr>
           <td >
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0" style="width: 100%">
						<TR>					
							<TD class="style2" width="98%" background="../graph/title_bg.gif"  >
                                <font color=red><asp:Label ID="errlab" runat="server" Text="Label" Visible="false"></asp:Label></font>
                                </TD>
						</TR>
			 </TABLE>
           </tr>
			<TR>
				<TD  >
					<TABLE id="country" cellSpacing="1" cellPadding="2" bgColor="#b7e6e6" border="0" style="width: 100%">
					
  <tr bgcolor="#ffffff">
<td colspan = 4>
                            <asp:Label ID="lblCompTop" runat="server" ForeColor="Red" Text="Label" Visible="True"></asp:Label>
</td>
             </tr>
             <tr bgcolor="#ffffff">
<td colspan = 4>
                        	&nbsp;
                     	</td>
               </tr> 					
									  <TR bgColor="#ffffff">
											<TD  align=right width="15%" style="width: 20%; height: 30px" >
                                               <asp:Label ID="perlab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 30%; height: 30px;">
                                                <asp:TextBox ID="perbox" runat="server" CssClass="textborder" Width="90%" MaxLength="6"></asp:TextBox>
                                                <font color=red><strong>*</strong></font>&nbsp;
                                            </TD>
											<TD align=right width="15%" style="width: 20%; height: 30px">
                                                <asp:Label ID="statusLab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 30%; height: 30px">
                                                <asp:DropDownList ID="statusDrop" runat="server" CssClass="textborder" Width="90%">
                                                </asp:DropDownList>
										</TR>
										<TR bgColor="#ffffff">
											
                                            <TD  align=right width="15%" style="width: 20%">
                                                <asp:Label ID="effectivelab" runat="server"></asp:Label></TD>
											<TD align=left width="85%" colspan=3 style="width: 80%">
                                                <asp:TextBox ID="effectivebox" runat="server" Width="62%" CssClass="textborder" ReadOnly="True"></asp:TextBox>
                                                <font color=red><strong>*</strong> </font>&nbsp;<asp:HyperLink ID="HypCal" runat="server"
                                                    ImageUrl="~/PresentationLayer/graph/calendar.gif" ToolTip="Choose a Date">Choose a Date</asp:HyperLink>
                                                <cc2:JCalendar ID="Calendar" runat="server" ControlToAssign="effectivebox"
                                                    ImgURL="~/PresentationLayer/graph/calendar.gif" Visible="False" />
                                                <asp:RequiredFieldValidator ID="EFFDATVA" runat="server" ControlToValidate="effectivebox"
                                                    ForeColor="White">*</asp:RequiredFieldValidator>
                                                &nbsp;&nbsp;
                                            </TD>
										</TR>
										
										
										<TR bgColor="#ffffff">
											<TD align=right width="15%" style="width: 20%; height: 30px">
                                                <asp:Label ID="createby" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 30%; height: 30px;">
                                                <asp:TextBox ID="creatbybox" runat="server" CssClass="textborder" ReadOnly=true Width="90%"></asp:TextBox></TD>
											<TD align=right width="15%" style="width: 20%; height: 30px">
                                                <asp:Label ID="creatdate" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 30%; height: 30px">
                                                <asp:TextBox ID="creatdtbox" runat="server" CssClass="textborder" ReadOnly=true Width="90%"></asp:TextBox></TD>
										</TR>
										<TR bgColor="#ffffff">
											<TD align=right width="15%" style="width: 20%">
                                                <asp:Label ID="modifyby" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 30%">
                                                <asp:TextBox ID="modfybybox" runat="server" CssClass="textborder" ReadOnly=true Width="90%"></asp:TextBox></TD>
											<TD align=right width="15%" style="width: 20%">
                                                <asp:Label ID="mdifydate" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 30%">
                                                <asp:TextBox ID="mdfydtbox" runat="server" CssClass="textborder" ReadOnly=true Width="90%"></asp:TextBox></TD>
										</TR>
								</TABLE>
                    <asp:GridView ID="SALSETUPSetUpView" runat="server" Width="100%" AllowPaging="True" style="width: 100%">
                    </asp:GridView>
                    <asp:TextBox ID="salsetupidbox" runat="server" Visible="False"></asp:TextBox></TD>
					</TR>
					<TR>
						<TD style="width: 771px; height: 18px;" >
						<font color=red><asp:Label ID="lblCompBottom" runat="server" Text="Label" Visible="true"></asp:Label></font>
							<TABLE id="Table1" cellSpacing="1" cellPadding="1"  border="0">
									<TR>
										<TD style="WIDTH: 20%" align=center>
                                            &nbsp;<asp:LinkButton ID="saveButton" runat="server"></asp:LinkButton></TD>
										<td align=center>
                                            &nbsp;<asp:LinkButton ID="cancel" runat="server" CausesValidation="False"></asp:LinkButton></td>
									</TR>
							</TABLE>
						</TD>
					</TR>
			</TABLE>
       <cc1:messagebox id="MessageBox1" runat="server"></cc1:messagebox>
       <asp:ValidationSummary ID="errormessage" runat="server" ShowMessageBox="True" ShowSummary="False" />
                                                <asp:RangeValidator ID="perVa" runat="server" ControlToValidate="perbox" ForeColor="White" Type="Double" MaximumValue="999.99" MinimumValue="000.00">*</asp:RangeValidator>
                                                <asp:RequiredFieldValidator ID="PERVAVA" runat="server" ControlToValidate="perbox"
                                                    ForeColor="White">*</asp:RequiredFieldValidator>
    </form>
</body>
</html>