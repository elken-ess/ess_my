﻿<%@ Page Language="VB" AutoEventWireup="false" EnableEventValidation ="false"  CodeFile="modifyarea.aspx.vb" Inherits="PresentationLayer_masterrecord_modifyarea" %>

<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Modify Area</title>
     <LINK href="../css/style.css" type="text/css" rel="stylesheet">
</head>
<body>
    <form id="form1" runat="server">
    <TABLE border="0" id=countrytab>
			<tr>
           <td style="width: 416px">
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>
							<TD width="1%" background="../graph/title_bg.gif"><IMG height="24" src="../graph/title1.gif" width="5"></TD>
							<TD class="style2" width="98%" background="../graph/title_bg.gif">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></TD>
							<TD align="right" width="1%" background="../graph/title_bg.gif"><IMG height="24" src="../graph/title_2.gif" width="5"></TD>
						</TR>
			 </TABLE>
           </tr>
             <tr>
           <td style="width: 416px">
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>					
							<TD class="style2" width="100%" background="../graph/title_bg.gif">
                                <font color=red><asp:Label ID="errlab" runat="server" Text="Label" Visible=false></asp:Label></font></TD>
						</TR>
			 </TABLE>
           </tr>
			<TR>
				<TD style="width: 416px">
					<TABLE id="area" cellSpacing="1" cellPadding="2" bgColor="#b7e6e6" border="0">
									  <TR bgColor="#ffffff">
											<TD  align=right width="15%">
                                                &nbsp;<asp:Label ID="ctryidlab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left width="35%"><asp:DropDownList ID="ctryid" runat="server" CssClass="textborder">
                                            </asp:DropDownList></TD>
											<TD  align=right width="15%">
                                                <asp:Label ID="staidLab" runat="server" Text="Label"></asp:Label></TD>
											<TD  align=left width="35%"><asp:DropDownList ID="staid" runat="server" CssClass="textborder">
                                            </asp:DropDownList></TD>
										</TR>
										<TR bgColor="#ffffff">
											<TD  align=right width="15%">
                                                <asp:Label ID="areaidLab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left width="35%" >
                                                <asp:TextBox ID="areaidbox" runat="server" CssClass="textborder"></asp:TextBox></TD>
                                                <TD  align=right width="15%">
                                                <asp:Label ID="areanmlab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left width="35%" >
                                                <asp:TextBox ID="areanm" runat="server" CssClass="textborder"></asp:TextBox></TD>
										</TR>
										<TR bgColor="#ffffff">
											<TD align=right width="15%">
                                                <asp:Label ID="alterLab" runat="server" Text="Label"></asp:Label></TD>
											<TD align="left" width="85%" colspan="3">
                                                <asp:TextBox ID="alter" runat="server" CssClass="textborder"></asp:TextBox></TD>
											 
										</TR>
										<TR bgColor="#ffffff">
											<TD  align=right width="15%" style="height: 22px">
                                                <asp:Label ID="serviceidlab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left width="35%" style="height: 22px">
											<asp:DropDownList ID="serviceid" runat="server" CssClass="textborder" width="90%">
											</asp:DropDownList></TD>
											<TD align=right width="15%" style="height: 22px">
                                                <asp:Label ID="statusLab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left width="35%" style="height: 22px">
                                                <asp:DropDownList ID="ctrystat" runat="server" CssClass="textborder">
                                                </asp:DropDownList>
											</TD>
										</TR>
										
										<TR bgColor="#ffffff">
											<TD align=right width="15%">
                                                <asp:Label ID="creatby" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left width="35%">
                                                <asp:TextBox ID="creatbybox" runat="server" CssClass="textborder" ReadOnly=true></asp:TextBox></TD>
											<TD align=right width="15%">
                                                <asp:Label ID="creatdate" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left width="35%">
                                                <asp:TextBox ID="creatdtbox" runat="server" CssClass="textborder" ReadOnly=true></asp:TextBox></TD>
										</TR>
										<TR bgColor="#ffffff">
											<TD align=right width="15%">
                                                <asp:Label ID="modfyby" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left width="35%">
                                                <asp:TextBox ID="modfybybox" runat="server" CssClass="textborder" ReadOnly=true></asp:TextBox></TD>
											<TD align=right width="15%">
                                                <asp:Label ID="mdfydate" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left width="35%">
                                                <asp:TextBox ID="mdfydtbox" runat="server" CssClass="textborder" ReadOnly=true></asp:TextBox></TD>
										</TR>
										<TR bgColor="#ffffff">
											<TD align=right width="15%">
                                                <asp:Label ID="POlab" runat="server" Text="Label"></asp:Label></TD>
											<TD align="left" width="35%" colspan="3">
                                                <asp:TextBox ID="PObox" runat="server" CssClass="textborder"></asp:TextBox>
                                                <asp:DropDownList ID="POstat" runat="server" CssClass="textborder" Width="104px">
                                                </asp:DropDownList>
                                                <asp:LinkButton ID="addButton" runat="server"></asp:LinkButton></TD>
											
										</TR>
										<TR bgColor="#ffffff">
											<TD align=right width="15%">
                                                <asp:Label ID="polabm" runat="server" Text="Label"></asp:Label></TD>
											<TD align="left" width="35%" colspan="3">
                                                <asp:TextBox ID="poBOXm" runat="server" CssClass="textborder"></asp:TextBox>&nbsp;<asp:DropDownList ID="postatm" runat="server" CssClass="textborder" Width="104px">
                                                </asp:DropDownList>
                                                <asp:LinkButton ID="modiButton" runat="server"></asp:LinkButton></TD>
											
										</TR>
						 	</TABLE>&nbsp;&nbsp;
					<div>	
                    <asp:GridView ID="POView" runat="server" AutoGenerateSelectButton="True">
                        <SelectedRowStyle BackColor="White" />
                    </asp:GridView> 
                 
                    </div>	
						</TD>
					</TR>
					<TR>
						<TD style="height: 37px; width: 416px;">
							<TABLE id="Table1" cellSpacing="1" cellPadding="1"  border="0">
									<TR>
										<TD style="WIDTH: 20%; height: 14px;" align=center>
                                            &nbsp;<asp:LinkButton ID="saveButton" runat="server"></asp:LinkButton></TD>
										<td align=center style="width: 205; height: 14px">
                                            &nbsp;<asp:HyperLink ID="cancelLink" runat="server" NavigateUrl="~/PresentationLayer/masterrecord/locale.aspx">[cancelLink]</asp:HyperLink></td>
									</TR>
							</TABLE>
						</TD>
					</TR>
			</TABLE>
        <cc1:messagebox id="MessageBox1" runat="server"></cc1:messagebox>

    </form>
</body>
</html>
