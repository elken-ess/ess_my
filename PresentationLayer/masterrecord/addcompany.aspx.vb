Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Partial Class PresentationLayer_masterrecord_addcompany
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try

            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "19")
            Me.saveButton.Enabled = purviewArray(0)
            If purviewArray(0) = False Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If


            Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
            'display label message
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            Me.ctridlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCOMP-0001")
            Me.staidlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCOMP-0002")
            Me.areaidlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCOMP-0003")
            statusLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCOMP-0004")
            Me.alterLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCOMP-0005")
            Me.pocodelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCOMP-0006")
            Me.compidlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCOMP-0007")
            Me.compnameLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCOMP-0008")
            Me.faxlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCOMP-0009")
            Me.emailLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCOMP-0010")
            Me.ad1lab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCOMP-0011")
            Me.ad2lab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCOMP-0012")
            Me.tel1lab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCOMP-0013")
            Me.tel2lab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCOMP-0014")

            Me.lblCompTop.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            Me.lblCompBottom.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")



            saveButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Save")
            cancelLink.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Cancel")
            AddLink.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Add")
            AddLink.NavigateUrl = Page.Request.RawUrl
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "ADDCOMPTL")


            creatby.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATBY")
            creatdate.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
            modfyby.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFBY")
            mdfydate.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFDATE")

            Dim userIDNamestr As String = Session("userID").ToString().ToUpper + "-" + Session("username").ToString.ToUpper
            creatbybox.Text = userIDNamestr
            creatdtbox.Text = Date.Now()
            modfybybox.Text = userIDNamestr
            mdfydtbox.Text = Date.Now()

            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            Me.ctryiderr.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "MAS-COUNTRY")
            Me.staiderr.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "MAS-STAT")
            Me.areiderr.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "MAS-AREA")
            Me.comiderr.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "COMIDER")
            Me.comnmerr.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "COMNMER")
            Me.alterr.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "COMALTER")
            Me.ad1err.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "COMADDR1")
            Me.ad2err.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "COMADDR2")
            Me.pocoderr.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "COMPCODE")
            Me.telerr1.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "COMTEL11")
            'Me.telerr2.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "COMTEL22")
            Me.tel1err.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "COMTEL1")
            Me.tel2err.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "COMTEL2")
            Me.faxerr.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "COMFAX")
            'Me.eemail_err.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "COMEMAIL11")
            Me.emailerr.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "COMEMAIL")
            Me.faxerr_1.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "COMFAX11")
            Me.poerr.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "COMPCODE11")

            'create the dropdownlist
            Dim cntryStat As New clsrlconfirminf()
            Dim statParam As ArrayList = New ArrayList
            statParam = cntryStat.searchconfirminf("STATUS")
            Dim count As Integer
            Dim statid As String
            Dim statnm As String
            For count = 0 To statParam.Count - 1
                statid = statParam.Item(count)
                statnm = statParam.Item(count + 1)
                count = count + 1
                If (statid.Equals("ACTIVE") Or statid.Equals("OBSOLETE")) Then
                    ctrystat.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                End If
            Next
            ctrystat.Items(0).Selected = True

            Dim rank As String = Session("login_rank")
            'bind ctryid
            'create  ctryid the dropdownlist
            'Dim ctry As New clsCommonClass
            'Dim ctryds As New DataSet

            'ctryds = ctry.Getidname("BB_MASCTRY_IDNAME")

            'If ctryds.Tables.Count <> 0 Then

            '    databonds(ctryds, Me.ctrid)
            'End If

            'Dim state As New clsCommonClass

            'If (Me.ctrid.SelectedValue().ToString() <> "") Then
            '    state.spctr = Me.ctrid.SelectedValue().ToString().ToUpper()
            'End If

            'Dim stateds As New DataSet

            'stateds = state.Getcomidname("BB_MASSTAT_IDNAME")
            'If stateds.Tables.Count <> 0 Then
            '    databonds(stateds, Me.staid)
            'End If


            'If (ctrid.SelectedValue().ToString() <> "") Then
            '    state.spctr = Me.ctrid.SelectedValue().ToString().ToUpper()
            'End If

            'If (Me.staid.SelectedValue().ToString() <> "") Then
            '    state.spstat = Me.staid.SelectedValue().ToString().ToUpper()
            'End If



            'Dim areads As New DataSet
            'areads = state.Getcomidname("BB_MASAREA_IDNAME")
            'If areads.Tables.Count <> 0 Then
            '    databonds(areads, Me.areaid)
            'End If
            Dim country As New clsCommonClass

            If rank <> 0 Then
                country.spctr = Session("login_ctryID").ToString().ToUpper

            End If
            country.rank = rank

            Dim countryds As New DataSet


            countryds = country.Getcomidname("BB_MASCTRY_IDNAME")
            If countryds.Tables.Count <> 0 Then
                databonds(countryds, Me.ctrid)
            End If
            Me.ctrid.Items.Insert(0, "")
            ctrid.SelectedValue = Session("login_ctryID")
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'create  state the dropdownlist
            Dim stat As New clsCommonClass
            If (ctrid.SelectedValue().ToString() <> "") Then
                stat.spctr = Me.ctrid.SelectedValue().ToString().ToUpper()
            End If

            stat.rank = rank
            Dim stateds As New DataSet

            stateds = stat.Getcomidname("BB_MASSTAT_IDNAME")
            If stateds.Tables.Count <> 0 Then
                databonds(stateds, Me.staid)
            End If

            Me.staid.Items.Insert(0, "")

            'Dim area As New clsCommonClass


            'If (ctrid.SelectedValue().ToString() <> "") Then
            '    area.spctr = Me.ctrid.SelectedValue().ToString().ToUpper()
            'End If

            'If (staid.SelectedValue().ToString() <> "") Then
            '    area.spstat = Me.staid.SelectedValue().ToString().ToUpper()
            'End If
            'area.rank = rank

            ''create  area the dropdownlist
            ''Dim area As New clsCommonClass
            'Dim areads As New DataSet

            'areads = area.Getcomidname("BB_MASAREA_IDNAME")
            'If areads.Tables.Count <> 0 Then
            '    databonds(areads, Me.areaid)
            'End If
            'Me.areaid.Items.Insert(0, "")
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Me.compidbox.Focus()

            Me.areaid.Items.Add(New ListItem("", ""))
        End If

     
    End Sub
#Region " id bond"
    Public Function databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        Dim row As DataRow
        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            NewItem.Text = row("id") & "-" & row("name")
            NewItem.Value = row("id")
            dropdown.Items.Add(NewItem)
        Next
        'dropdown.Items(0).Selected = True
    End Function
#End Region

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveButton.Click

        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim msginfo As String = objXm.GetLabelName("StatusMessage", "SAVEORNOT")
        Dim msgtitle As String = objXm.GetLabelName("StatusMessage", "SAVETITLE")
        MessageBox1.Confirm(msginfo)
    End Sub

#Region "save record"
    Public Sub save()
        Dim telEntity As New clsCommonClass
        Dim compEntity As New clsCompany()
        If (Trim(Me.compidbox.Text) <> "") Then
            compEntity.CompID = compidbox.Text.ToUpper()
        End If
        If (Trim(Me.compnmbox.Text) <> "") Then
            compEntity.CompName = compnmbox.Text.ToUpper()
        End If
        If (Trim(Me.altbox.Text) <> "") Then
            compEntity.CompAlternateName = altbox.Text.ToUpper()
        End If

        If (ctrystat.SelectedItem.Value.ToString() <> "") Then
            compEntity.CompStatus = ctrystat.SelectedItem.Value.ToString()
        End If

        If (Trim(Me.tel1box.Text) <> "") Then
            compEntity.Tel1 = telEntity.telconvertpass(tel1box.Text)
        End If

        If (Trim(Me.tel2box.Text) <> "") Then
            compEntity.Tel2 = telEntity.telconvertpass(tel2box.Text)
        End If
        If (Trim(Me.faxbox.Text) <> "") Then
            compEntity.Fax = telEntity.telconvertpass(faxbox.Text)
        End If
        If (Trim(Me.emailbox.Text) <> "") Then
            compEntity.email = emailbox.Text
        End If

        If (Trim(Me.emailbox.Text) <> "") Then
            compEntity.email = emailbox.Text
        End If
        If (Trim(Me.ad1box.Text) <> "") Then
            compEntity.Address1 = ad1box.Text
        End If
        If (Trim(Me.ad2box.Text) <> "") Then
            compEntity.Address2 = ad2box.Text
        End If
        If (Trim(Me.pocodebox.Text) <> "") Then
            compEntity.POcode = pocodebox.Text
        End If


        If (Me.ctrid.SelectedItem.Value.ToString() <> "") Then

            compEntity.CountryID = ctrid.SelectedValue.ToString()
        End If


        If (Me.staid.SelectedValue.ToString() <> "") Then
            compEntity.StateID = Me.staid.SelectedValue.ToString()
        End If
        If (Me.areaid.SelectedValue.ToString() <> "") Then
            compEntity.AreaID = Me.areaid.SelectedValue.ToString()
        End If

        If (Trim(creatbybox.Text) <> "") Then
            compEntity.CreateBy = Session("userID").ToString.ToUpper()
        End If
        'If (Trim(creatdtbox.Text) <> "") Then
        '    compEntity.CreateDate = creatdtbox.Text
        'End If
        If (Trim(modfybybox.Text) <> "") Then
            compEntity.ModifyBy = Session("userID").ToString.ToUpper()
        End If
        'If (Trim(mdfydtbox.Text) <> "") Then
        '    compEntity.ModifyDate = mdfydtbox.Text
        'End If

        Dim dupCount As Integer = compEntity.GetDuplicatedComp()
        'If selvalue.Read() <> 0 Then
        '    Response.Redirect("~/PresentationLayer/Error.aspx")
        'End If
        If dupCount.Equals(-1) Then
            Dim objXm As New clsXml
            objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            errlab.Text = objXm.GetLabelName("StatusMessage", "DUPCOMPIDORNM")
            errlab.Visible = True
        ElseIf dupCount.Equals(0) Then
            Dim insStatCnt As Integer = compEntity.Insert()
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            If insStatCnt = 0 Then
                Dim objXm As New clsXml
                objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                errlab.Text = objXm.GetLabelName("StatusMessage", "BB-HintMsg-Suc")
                errlab.Visible = True
                'Response.Redirect("company.aspx")
            Else

                Response.Redirect("~/PresentationLayer/Error.aspx")
            End If
        End If
    End Sub
#End Region
    Protected Sub ctrid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ctrid.SelectedIndexChanged
        
        Me.staid.Items.Clear()
        Me.areaid.Items.Clear()

        'If Me.ctrid.Text <> "" Then
        Dim rank As String = Session("login_rank")
        Dim state As New clsCommonClass

        If (Me.ctrid.SelectedValue().ToString() <> "") Then
            state.spctr = Me.ctrid.SelectedValue().ToString().ToUpper()
        End If
        state.rank = rank

        'Dim state As New clsCommonClass
        Dim stateds As New DataSet

        stateds = state.Getcomidname("BB_MASSTAT_IDNAME")
        If stateds.Tables.Count <> 0 Then
            databonds(stateds, Me.staid)
        End If
        Me.staid.Items.Insert(0, "")

        'Dim area As New clsCommonClass
        ''Dim ctridname As String
        'If (ctrid.SelectedValue().ToString() <> "") Then
        '    area.spctr = Me.ctrid.SelectedValue().ToString().ToUpper()
        'End If
        ''Dim staid As String
        'If (staid.SelectedValue().ToString() <> "") Then
        '    area.spstat = Me.staid.SelectedValue().ToString().ToUpper()
        'End If
        'area.rank = rank

        ''create  area the dropdownlist
        ''Dim area As New clsCommonClass
        'Dim areads As New DataSet

        'areads = area.Getcomidname("BB_MASAREA_IDNAME")
        'If areads.Tables.Count <> 0 Then
        '    databonds(areads, Me.areaid)
        'End If
        'Me.areaid.Items.Insert(0, "")
        'Else

        '    Me.staid.Items.Clear()
        '    Me.areaid.Items.Clear()
        '    Me.staid.Items.Add(New ListItem("", ""))
        '    Me.areaid.Items.Add(New ListItem("", ""))
        'End If

    End Sub

    Protected Sub staid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles staid.SelectedIndexChanged
 
        Me.areaid.Items.Clear()
        'If Me.staid.Text <> "" Then
        Dim rank As String = Session("login_rank")
        Dim area As New clsCommonClass
        'Dim ctridname As String
        If (ctrid.SelectedValue().ToString() <> "") Then
            area.spctr = Me.ctrid.SelectedValue().ToString().ToUpper()
        End If
        'Dim staid As String
        If (staid.SelectedValue().ToString() <> "") Then
            area.spstat = Me.staid.SelectedValue().ToString().ToUpper()
        End If
        area.rank = rank

        'create  area the dropdownlist
        'Dim area As New clsCommonClass
        Dim areads As New DataSet

        areads = area.Getcomidname("BB_MASAREA_IDNAME")
        If areads.Tables.Count <> 0 Then
            databonds(areads, Me.areaid)
        End If
        Me.areaid.Items.Insert(0, "")
        'Else

        'Me.areaid.Items.Clear()
        'Me.staid.Items.Add(New ListItem("", ""))
        'Me.areaid.Items.Add(New ListItem("", ""))
        'End If
    End Sub

    Protected Sub MessageBox1_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox1.GetMessageBoxResponse
        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
            save()
        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
        End If
    End Sub
End Class
