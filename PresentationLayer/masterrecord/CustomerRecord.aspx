<%@ Page Language="VB" AutoEventWireup="false"  EnableEventValidation ="false" CodeFile="CustomerRecord.aspx.vb" Inherits="PresentationLayer_masterrecord_CustomerRecord" %>

<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Customer Search</title>
     <meta http-equiv="Content-Type" content="text/html; charset=gb2312">
    <LINK href="../css/style.css" type="text/css" rel="stylesheet">
     <STYLE type="text/css">A:link { COLOR: #336666; TEXT-DECORATION: none }
	BODY { FONT-SIZE: 9pt; FONT-FAMILY: "Arial", "Verdana", "Tahoma"; BACKGROUND-COLOR: #ffffff }
	TD { FONT-SIZE: 9pt; FONT-FAMILY: Arial,Verdana,Tahoma }
	</STYLE>
	
	<!--LW Added--> 
     <script language="javascript">  
    
        function LoadRaces_CallBack(response){
 

         //if the server-side code threw an exception
         if (response.error != null)
         {
          //we should probably do better than this
          alert(response.error); 
          
          return;
         }

         var ResponseValue = response.value;
         
         //if the response wasn't what we expected  
         if (ResponseValue == null || typeof(ResponseValue) != "object")
         {       
          return;
         }
              
         //Get the states drop down
         var ResultList = document.getElementById("<%=RacesDLL.ClientID%>");
         ResultList.options.length = 0; //reset the states dropdown

        
         //Remember, its length not Length in JavaScript
         for (var i = 0; i < ResponseValue.length; ++i)
         {
         
          //the columns of our rows are exposed like named properties
          var al = ResponseValue[i].split(":");
          var alid = al[0];
          var alname = al[1];
          ResultList.options[ResultList.options.length] =  new Option(alname, alid);
         }
     
        }
     
        function LoadRaces(ObjectClient)
        {
             var ObjectId = ObjectClient.options[ObjectClient.selectedIndex].value;
              
             PresentationLayer_masterrecord_CustomerRecord.GetRaces(ObjectId, LoadRaces_CallBack); 
        }
    </script>
</head>
<body style="width: 100%">
    <form id="form1" runat="server">
    <div>
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
            <tr>
                <td background="../graph/title_bg.gif" width="1%">
                    <img height="24" src="../graph/title1.gif" width="5" /></td>
                <td background="../graph/title_bg.gif" class="style2" style="width: 100%">
                    <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
                <td align="left" background="../graph/title_bg.gif" style="width: 1%">
                    <img height="24" src="../graph/title_2.gif" width="5" /></td>
            </tr>
        </table>
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
            <tr>
                <td background="../graph/title_bg.gif" style="width: 1%">
                </td>
                <td background="../graph/title_bg.gif" class="style2" width="98%">
                    <asp:Label ID="addinfo" runat="server" ForeColor="Red" Text="Label" Visible="False"></asp:Label></td>
            </tr>
        </table>
        <table id="countrytab" border="0" style="width: 100%">
            <tr>
                <td  width= " 90%" style="width: 100%; text-align: left;">
                    <table id="country" bgcolor="#b7e6e6" border="0" cellpadding="2" cellspacing="1" style="width: 100%">
                        <tr bgcolor="#ffffff">
                            <td align="left" width="15%" style="width: 15%;">
        <asp:Label ID="customerIDlab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%;">
                                <asp:TextBox ID="customerIDbox" runat="server" Font-Size="Small" CssClass="textborder" MaxLength="10"></asp:TextBox></td>
                            <td align="left" width="15%" style="width: 15%;">
                                <%--Modified by Ryan Estandarte 9 August 2012--%>
                                <asp:Label ID="contactlab" runat="server" Text="Label"></asp:Label>
                                <%--<asp:Label ID="areaIDlab" runat="server" Text="Label"></asp:Label>--%>
                            </td>
                            <td align="left" style="width: 35%;">
                                <%--Modified by Ryan Estandarte 9 August 2012--%>
                                <asp:TextBox ID="contactbox" runat="server" Font-Size="Small" CssClass="textborder" MaxLength="30"></asp:TextBox>
                                <%--<asp:DropDownList ID="areaID" runat="server" Width="100%" CssClass="textborder">
                                </asp:DropDownList>--%>
                            </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="left" width="15%" style="width: 15%; height: 30px;">
                                <asp:Label ID="customernamelab" runat="server" Text="Label"></asp:Label>
                            </td>
                            <td align="left" style="width: 35%; height: 30px;">
                                <asp:TextBox ID="customernamebox" runat="server" Font-Size="Small" CssClass="textborder" MaxLength="50"></asp:TextBox>
                            </td>
                            <td align="left" width="15%" style="width: 15%; height: 30px;">
                                <%--Modified by Ryan Estandarte 9 August 2012--%>
                                <%--<asp:Label ID="contactlab" runat="server" Text="Label"></asp:Label>--%>
                                <asp:Label ID="lblState" runat="server" Text="State"/>
                            </td>
                            <td align="left" style="width: 35%; height: 30px;">
                                <%--Modified by Ryan Estandarte 9 August 2012--%>
                                <%--<asp:TextBox ID="contactbox" runat="server" Font-Size="Small" CssClass="textborder" MaxLength="30"></asp:TextBox>--%>
                                <asp:DropDownList runat="server" ID="ddState" style="width: 150px;" CssClass="textborder"/>
                                
                            </td>
                        </tr>
                        <%--Added by Ryan Estandarte 17 July 2012
                        Description: additional search criteria in customer search--%>
                        <tr bgcolor="#ffffff">
                            <td align="left" width="15%" style="width: 15%; height: 30px;">
                                <asp:Label ID="lblAddress1" runat="server" Text="Address 1"/>
                            </td>
                            <td align="left" style="width: 35%; height: 30px;">
                                <asp:TextBox ID="txtAddress1" runat="server" Font-Size="Small" CssClass="textborder"/>
                            </td>
                            <td align="left" width="15%" style="width: 15%; height: 30px;">
                                <%--Modified by Ryan Estandarte 9 August 2012--%>
                                <%--<asp:Label ID="lblAddress2" runat="server" Text="Address 2"/>--%>
                                <asp:Label ID="areaIDlab" runat="server" Text="Label"/>
                            </td>
                            <td align="left" style="width: 35%; height: 30px;">
                                <%--Modified by Ryan Estandarte 9 August 2012--%>
                                <%--<asp:TextBox ID="txtAddress2" runat="server" Font-Size="Small" CssClass="textborder" MaxLength="30"/>--%>
                                <asp:DropDownList ID="areaID" runat="server" Width="100%" CssClass="textborder"/>
                            </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="left" width="15%" style="width: 15%; height: 30px;">
                                <%--Modified by Ryan Estandarte 9 August 2012--%>
                                <%--<asp:Label ID="lblState" runat="server" Text="State"/>--%>
                                <asp:Label ID="lblAddress2" runat="server" Text="Address 2"/>
                            </td>
                            <td align="left" style="width: 35%; height: 30px;">
                                <%--Modified by Ryan Estandarte 9 August 2012--%>
                                <%--<asp:DropDownList runat="server" ID="ddState" style="width: 150px;" CssClass="textborder"/>--%>
                                <asp:TextBox ID="txtAddress2" runat="server" Font-Size="Small" CssClass="textborder" MaxLength="30"/>
                                
                            </td>
                            <td align="left" width="15%" style="width: 15%; height: 30px;">
                                <asp:Label ID="lblPostCode" runat="server" Text="Postal Code"/>
                                
                            </td>
                            <td align="left" style="width: 35%; height: 30px;">
                                <asp:TextBox ID="txtPostCode" runat="server" Font-Size="Small" CssClass="textborder" MaxLength="30"/>
                            </td>
                        </tr>
                        
                        <tr bgcolor="#ffffff">
                            <td align="left" width="15%" style="width: 15%; height: 30px;">
                                <asp:Label ID="lblModelID" runat="server" Text="Model ID"/>
                            </td>
                            <td align="left" style="width: 35%; height: 30px;">
                                <%--Modified by Ryan Estandarte 18 July 2012--%>
                                <%--<asp:TextBox ID="txtModelID" runat="server" Font-Size="Small" CssClass="textborder" MaxLength="50"/>--%>
                                <asp:DropDownList runat="server" ID="ddModelID" style="width: 150px;" CssClass="textborder"/>
                            </td>
                            <td align="left" width="15%" style="width: 15%; height: 30px;">
                                <%--Modified by Ryan Estandarte 9 August 2012--%>
                                <asp:Label ID="lblStatus" runat="server" Text="Status"/>
                                &nbsp;
                            </td>
                            <td align="left" style="width: 35%; height: 30px;">
                                <asp:DropDownList ID="cboStatus" runat="server" CssClass="textborder" Width="114px"/>
                                &nbsp;
                            </td>
                        </tr>
                        <%--end change.--%>
                        <tr bgcolor="#ffffff">
                            <td align="left" width="15%" style="width: 15%">
                                <asp:Label ID="roserialnolab" runat="server" Text="Label"></asp:Label>
                            </td>
                            <td align="left" style="width: 35%">
                                <asp:TextBox ID="roserialnobox" runat="server" Font-Size="Small" CssClass="textborder" MaxLength="10"></asp:TextBox>
                            </td>
                            <td align="left" style="width: 15%">
                                <%--Modified by Ryan Estandarte 9 August 2012--%>
                                <%--<asp:Label ID="lblStatus" runat="server" Text="Status"></asp:Label>--%>
                                <asp:Label ID="RacesLab" runat="server" Text="Label"></asp:Label>
                            </td>
                            <td align="left" style="width: 35%">
                                <%--Modified by Ryan Estandarte 9 August 2012--%>
                                 <%--<asp:DropDownList ID="cboStatus" runat="server" CssClass="textborder" Width="114px">
                                    </asp:DropDownList>--%>
                                <asp:DropDownList ID="RacesDLL" runat="server" CssClass="textborder" Width="50%"/>
                            </td>
                            
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="left" style="width: 15%;">
                                <asp:Label ID="CustomerTypeLab" runat="server" Text="Label"></asp:Label>
                            </td>
                            <td style="width: 35%;" align="left">
                                <asp:DropDownList ID="CustomerTypeDLL" runat="server" CssClass="textborder" Width="50%" AutoPostBack="false" onchange="LoadRaces(this)">
                                </asp:DropDownList>
                            </td>
                            <td align="left" style="width: 15%;">
                                <%--Modified by Ryan Estandarte 9 August 2012--%>
                                <%--<asp:Label ID="RacesLab" runat="server" Text="Label"></asp:Label>--%>
                            </td>
                            <td style="width: 35%;" align="left">
                                <%--Modified by Ryan Estandarte 9 August 2012--%>
                                <%--<asp:DropDownList ID="RacesDLL" runat="server" CssClass="textborder" Width="50%">
                                </asp:DropDownList>--%>
                            </td>
                        </tr>
                        <tr  bgcolor="#ffffff">
                        <td align="left" width="15%" style="width: 15%">
                            
                            </td>
                            <td align="left" style="width: 35%">
                            
                            </td>
                            <td align="left" style="width: 15%">
                                
                            </td>
                            <td align="left" width="15%" style="width: 15%">
                                <asp:LinkButton ID="searchButton" runat="server">search</asp:LinkButton>
                                &nbsp; &nbsp;&nbsp; &nbsp;
                                <asp:LinkButton ID="addButton" runat="server" PostBackUrl="~/PresentationLayer/masterrecord/addcustomer.aspx">add</asp:LinkButton>
                            </td>
                        </tr>
                      
                        
                    </table>
        <hr />
                    <asp:GridView ID="ctryView" runat="server" AllowPaging="True" AllowSorting="True"
                        Font-Size="Smaller" Width="100%">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:Label ID="lblRadio" runat="server" Text=""></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td align=left>
                    <asp:Label ID="Label2" runat="server" Visible="False"></asp:Label>
                    &nbsp;<asp:Label ID="LblTotRecNo" runat="server" Visible="False"></asp:Label>
                    &nbsp; &nbsp;
                </td>
            </tr>
            <tr>
                <td style="width: 100%; text-align: right; height: 18px;" width="90%">
                    <asp:LinkButton ID="lblEdit" runat="server" Font-Bold="True" Font-Underline="True"
                        ForeColor="#000099" Enabled="False">Edit</asp:LinkButton>                    &nbsp; &nbsp;
                    <asp:LinkButton ID="lblCreateApp" runat="server" Font-Bold="True" Font-Underline="True"
                        ForeColor="#000099" Enabled="False">Create Appointment </asp:LinkButton>
                    &nbsp; &nbsp; 
                    <asp:LinkButton ID="LnkViewAppt" runat="server" Enabled="False" Font-Bold="True"
                        Font-Underline="True" ForeColor="#000099">View Appointment </asp:LinkButton>
                    &nbsp; &nbsp;
                    <asp:LinkButton ID="lblViewInst" runat="server" Font-Bold="True" Font-Underline="True"
                        ForeColor="#000099" Enabled="False">View Install Base</asp:LinkButton>
                    &nbsp; &nbsp;
                    <asp:LinkButton ID="lblCreateInstall" runat="server" Font-Bold="True" Font-Underline="True"
                        ForeColor="#000099" Enabled="False">Create Install Base</asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td style="width: 100%; text-align: right" width="90%">
                </td>
            </tr>
        </table>
        <asp:Label ID="Label1" runat="server" ForeColor="Red" Height="62px" Text="Label"
            Width="638px"></asp:Label>&nbsp;<cc1:messagebox id="MessageBox1" runat="server"></cc1:messagebox><br />
        &nbsp;&nbsp;
        <br />
    
    </div>
        &nbsp;&nbsp; &nbsp;
    </form>
</body>
</html>
