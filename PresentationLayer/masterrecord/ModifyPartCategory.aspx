<%@ Page Language="VB" AutoEventWireup="false" EnableEventValidation ="false"  CodeFile="ModifyPartCategory.aspx.vb" Inherits="PresentationLayer_masterrecord_ModifyPartCategory" %>

<%@ Register Assembly="JCalendar" Namespace="JCalendar" TagPrefix="cc2" %>

<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>modify part category</title>
    <LINK href="../css/style.css" type="text/css" rel="stylesheet">
<script language="JavaScript" src="../js/common.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div>
            <table id="countrytab" border="0" width="100%">
                <tr>
                    <td style="width: 100%">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td background="../graph/title_bg.gif" style="height: 24px" width="1%">
                                    <img height="24" src="../graph/title1.gif" width="5" /></td>
                                <td background="../graph/title_bg.gif" class="style2" style="height: 24px" width="98%">
                                    <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="right" background="../graph/title_bg.gif" style="width: 1%; height: 24px">
                                    <img height="24" src="../graph/title_2.gif" width="5" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="width: 100%; height: 30px;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td background="../graph/title_bg.gif" class="style2" style="width: 98%; height: 14px;">
                                    <font color="red">
                                        <asp:Label ID="errlab" runat="server" Text="Label" Visible="false"></asp:Label></font></td>
                            </tr>
                        </table>
                        <asp:Label ID="Label7" runat="server" ForeColor="Red"></asp:Label></td>
                </tr>
                <tr>
                    <td style="width: 100%">
                        <table id="country" bgcolor="#b7e6e6" border="0" cellpadding="2" cellspacing="1"
                            style="width: 100%">
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width: 15%; height: 30px" width="15%">
                                    &nbsp;<asp:Label ID="CategoryIDlab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 35%; height: 30px">
                                    <asp:TextBox ID="CategoryIDbox" runat="server" CssClass="textborder" MaxLength="2"
                                        Width="81%" ReadOnly="True"></asp:TextBox>
                                    <asp:Label ID="Label3" runat="server" ForeColor="Red" Height="16px" Text="*" Width="8px" Font-Bold="True"></asp:Label>
                                    <asp:RequiredFieldValidator ID="categoryiderr" runat="server" ControlToValidate="CategoryIDbox" ForeColor="White">*</asp:RequiredFieldValidator>
                                    </td>
                                <td align="right" style="width: 15%; height: 30px" width="15%">
                                    <asp:Label ID="CategoryNamelab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 35%; height: 30px">
                                    <asp:TextBox ID="CategoryNamebox" runat="server" CssClass="textborder" Width="81%"></asp:TextBox>
                                    <asp:Label ID="Label2" runat="server" ForeColor="Red" Height="16px" Text="*" Width="8px"></asp:Label>
                                    <asp:RequiredFieldValidator ID="categorynmerr" runat="server" ControlToValidate="CategoryNamebox" ForeColor="White">*</asp:RequiredFieldValidator>
                                    </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width: 15%" width="15%">
                                    <asp:Label ID="categoryalterLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" colspan="3" style="width: 85%" width="85%">
                                    <asp:TextBox ID="categoryalterbox" runat="server" CssClass="textborder" Width="512px" style="width: 93%"></asp:TextBox>
                                    <asp:Label ID="Label5" runat="server" ForeColor="Red" Height="16px" Text="*" Width="8px"></asp:Label>
                                    <asp:RequiredFieldValidator ID="altererr" runat="server" ControlToValidate="categoryalterbox" ForeColor="White">*</asp:RequiredFieldValidator>&nbsp;
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width: 15%; height: 30px" width="15%">
                                    <asp:Label ID="countryidlab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 35%; height: 30px">
                                    <asp:DropDownList ID="countryiddrpd" runat="server" CssClass="textborder" Style="width: 80%"
                                        Width="176px" Enabled="False">
                                    </asp:DropDownList>
                                    <asp:Label ID="Label1" runat="server" ForeColor="Red" Height="16px" Text="*" Width="1px" Font-Bold="True"></asp:Label>
                                    <asp:RequiredFieldValidator ID="ctryiderr" runat="server" ControlToValidate="countryiddrpd"
                                        Width="8px" ForeColor="White">*</asp:RequiredFieldValidator>&nbsp;
                                </td>
                                <td align="right" style="width: 15%; height: 30px" width="15%">
                                    <asp:Label ID="statuslab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 35%; height: 30px">
                                    <asp:DropDownList ID="statusdrpd" runat="server" CssClass="textborder" Width="168px">
                                    </asp:DropDownList></td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width: 15%" width="15%">
                                    <asp:Label ID="effectivedatelab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 35%">
                                    <asp:TextBox ID="effectibedatebox" runat="server" CssClass="textborder" ReadOnly="True"
                                        Width="168px" style="width: 72%"></asp:TextBox>
                                    <asp:Label ID="Label4" runat="server" Font-Bold="True" ForeColor="Red" Height="16px"
                                        Text="*" Width="1px"></asp:Label>
                                    <asp:HyperLink ID="HypCal" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                                        ToolTip="Choose a Date">Choose a Date</asp:HyperLink>
                                    <cc2:JCalendar ID="JCalendar1" runat="server" ControlToAssign="effectibedatebox"
                                        ImgURL="~/PresentationLayer/graph/calendar.gif" Visible="False" />
                                    <asp:RequiredFieldValidator ID="efferr" runat="server" ControlToValidate="effectibedatebox" ForeColor="White">*</asp:RequiredFieldValidator>&nbsp;
                                </td>
                                <td align="right" style="width: 15%" width="15%">
                                    <asp:Label ID="obsoletedatelab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 35%">
                                    <asp:TextBox ID="obsoletedatebox" runat="server" CssClass="textborder" ReadOnly="True"
                                        Width="168px" style="width: 72%"></asp:TextBox>&nbsp;<asp:Label ID="Label6" runat="server"
                                            ForeColor="Red" Height="16px" Text="*" Width="8px"></asp:Label>
                                    <asp:HyperLink ID="HypCal2" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                                        ToolTip="Choose a Date">Choose a Date</asp:HyperLink>
                                    <cc2:JCalendar ID="JCalendar2" runat="server" ControlToAssign="obsoletedatebox" ImgURL="~/PresentationLayer/graph/calendar.gif" Visible="False" />
                                    <asp:RequiredFieldValidator ID="obserr" runat="server" ControlToValidate="obsoletedatebox" ForeColor="White">*</asp:RequiredFieldValidator>&nbsp;&nbsp;
                                    &nbsp; &nbsp; &nbsp; &nbsp;
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width: 15%" width="15%">
                                    <asp:Label ID="creatby" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 178px">
                                    <asp:TextBox ID="creatbybox" runat="server" CssClass="textborder" ReadOnly="true"
                                        Width="168px"></asp:TextBox></td>
                                <td align="right" style="width: 15%" width="15%">
                                    <asp:Label ID="creatdate" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 35%">
                                    <asp:TextBox ID="creatdtbox" runat="server" CssClass="textborder" ReadOnly="true"
                                        Width="168px" style="width: 82%"></asp:TextBox></td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width: 15%" width="15%">
                                    <asp:Label ID="modfyby" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 178px">
                                    <asp:TextBox ID="modfybybox" runat="server" CssClass="textborder" ReadOnly="true"
                                        Width="168px"></asp:TextBox></td>
                                <td align="right" style="width: 15%" width="15%">
                                    <asp:Label ID="mdfydate" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 35%">
                                    <asp:TextBox ID="mdfydtbox" runat="server" CssClass="textborder" ReadOnly="true"
                                        Width="168px" style="width: 82%"></asp:TextBox></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label8" runat="server" ForeColor="Red"></asp:Label></td>
                </tr>
                <tr>
                    <td style="width: 657px; height: 37px">
                        <table id="Table1" border="0" cellpadding="1" cellspacing="1">
                            <tr>
                                <td align="center" style="width: 20%; height: 16px">
                                    &nbsp;<asp:LinkButton ID="saveButton" runat="server"></asp:LinkButton></td>
                                <td align="center" style="height: 16px">
                                    <asp:HyperLink ID="cancelLink" runat="server" NavigateUrl="~/PresentationLayer/masterrecord/PartCategory.aspx">[cancelLink]</asp:HyperLink></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <cc1:messagebox id="MessageBox1" runat="server"></cc1:messagebox>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                ShowSummary="False" />
        </div>
    
    </div>
    </form>
</body>
</html>
