Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data

Partial Class PresentationLayer_masterrecord_PriceID
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Page.IsPostBack) Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try
            BindGrid()
        End If
    End Sub
    Protected Sub BindGrid()
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        Me.PriceIDlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRID-0001")
        Me.PriceNamelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRID-0002")
        Me.Statuslab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Status")
        Me.LinkButton1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Add")
        Me.LinkButton2.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Search")
        Me.titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRID-0010")
        Me.Label1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-NR")
        Me.Label1.Visible = False
        Me.addinfo.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-AR")
        Me.addinfo.Visible = False

        Me.PriceIDBox.Focus()
       
        'create the dropdownlist
        Dim piceidStat As New clsrlconfirminf()
        Dim statParam As ArrayList = New ArrayList
        statParam = piceidStat.searchconfirminf("STATUS")
        Dim count As Integer
        Dim statid As String
        Dim statnm As String
        For count = 0 To statParam.Count - 1
            statid = statParam.Item(count)
            statnm = statParam.Item(count + 1)
            count = count + 1
            If (statid.Equals("ACTIVE") Or statid.Equals("OBSOLETE")) Then
                Me.StatusDrop.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
            End If
            
        Next
        If StatusDrop.Items.Count <> 0 Then
            StatusDrop.Items(0).Selected = True
        End If

        Dim PriceIDEntity As New clsPriceID()
        Dim rank As String = Session("login_rank").ToString().ToUpper()
        If rank <> 0 Then
            PriceIDEntity.CountryID = Session("login_ctryID").ToString().ToUpper()
        End If
        PriceIDEntity.rank = rank
        If (StatusDrop.SelectedValue().ToString() <> "") Then
            PriceIDEntity.PriceStatus = StatusDrop.SelectedItem.Value.ToString()
        End If
        Dim dsPriceIDall As DataSet = PriceIDEntity.GetPriceID()
        PriceIDView.DataSource = dsPriceIDall
        Session("PriceIDView") = dsPriceIDall
        checknorecord(dsPriceIDall)

        PriceIDView.AllowPaging = True
        PriceIDView.AllowSorting = True

        Dim editcol As New CommandField
        editcol.EditText = objXmlTr.GetLabelName("EngLabelMsg", "BB-Edit") '"edit"
        editcol.HeaderImageUrl = "~/PresentationLayer/graph/edit.gif"
        editcol.ShowEditButton = True
        PriceIDView.Columns.Add(editcol)

        '''access control'''''''''''''''''''''''''''''''''''''''''''''''
        Dim accessgroup As String = Session("accessgroup").ToString
        Dim purviewArray As Array = New Boolean() {False, False, False, False, False}
        purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "17")
        Me.LinkButton1.Enabled = purviewArray(0) 'NEW
        editcol.Visible = purviewArray(2) 'View


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


        PriceIDView.DataBind()
        If dsPriceIDall.Tables(0).Rows.Count <> 0 Then
            PriceIDView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRID-0001")
            PriceIDView.HeaderRow.Cells(1).Font.Size = 8
            PriceIDView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRID-0002")
            PriceIDView.HeaderRow.Cells(2).Font.Size = 8
            PriceIDView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Status")
            PriceIDView.HeaderRow.Cells(3).Font.Size = 8
            PriceIDView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRID-0004")
            PriceIDView.HeaderRow.Cells(4).Font.Size = 8
            PriceIDView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRID-0006")
            PriceIDView.HeaderRow.Cells(5).Font.Size = 8
            PriceIDView.HeaderRow.Cells(6).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRID-0007")
            PriceIDView.HeaderRow.Cells(6).Font.Size = 8
        End If

    End Sub
#Region " id bond"
    Public Function databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        Dim row As DataRow
        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            NewItem.Text = row("id") & "-" & row("name")
            NewItem.Value = row("id")
            dropdown.Items.Add(NewItem)
        Next

    End Function
#End Region
    Protected Sub LinkButton2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton2.Click
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        PriceIDView.PageIndex = 0
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        Dim PriceIDEntity As New clsPriceID()
        Dim rank As String = Session("login_rank").ToString().ToUpper()
        If rank <> 0 Then
            PriceIDEntity.CountryID = Session("login_ctryID").ToString().ToUpper()
        End If
        PriceIDEntity.rank = rank
        If (Trim(PriceIDBox.Text) <> "") Then
            PriceIDEntity.PriceID = PriceIDBox.Text
        End If
        If (Trim(PriceNameBox.Text) <> "") Then
            PriceIDEntity.PriceName = PriceNameBox.Text
        End If
        If (StatusDrop.SelectedValue().ToString() <> "") Then
            PriceIDEntity.PriceStatus = StatusDrop.SelectedItem.Value.ToString()
        End If
        checkinfo()
        PriceIDEntity.ModifiedBy = Session("username")
        Dim selDS As DataSet = PriceIDEntity.GetPriceID()
        PriceIDView.DataSource = selDS
        PriceIDView.DataBind()
        Session("PriceIDView") = selDS
        checknorecord(selDS)
        If selDS.Tables(0).Rows.Count <> 0 Then
            PriceIDView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRID-0001")
            PriceIDView.HeaderRow.Cells(1).Font.Size = 8
            PriceIDView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRID-0002")
            PriceIDView.HeaderRow.Cells(2).Font.Size = 8
            PriceIDView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Status")
            PriceIDView.HeaderRow.Cells(3).Font.Size = 8
            PriceIDView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRID-0004")
            PriceIDView.HeaderRow.Cells(4).Font.Size = 8
            PriceIDView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRID-0006")
            PriceIDView.HeaderRow.Cells(5).Font.Size = 8
            PriceIDView.HeaderRow.Cells(6).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRID-0007")
            PriceIDView.HeaderRow.Cells(6).Font.Size = 8

        End If

    End Sub

  

    Protected Sub PriceIDView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles PriceIDView.PageIndexChanging
        PriceIDView.PageIndex = e.NewPageIndex
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Dim PriceIDEntity As New clsPriceID()
        Dim rank As String = Session("login_rank").ToString().ToUpper()
        If rank <> 0 Then
            PriceIDEntity.CountryID = Session("login_ctryID").ToString().ToUpper()
        End If
        PriceIDEntity.rank = rank
        If (Trim(PriceIDBox.Text) <> "") Then
            PriceIDEntity.PriceID = PriceIDBox.Text
        End If
        If (Trim(PriceNameBox.Text) <> "") Then
            PriceIDEntity.PriceName = PriceNameBox.Text
        End If
        If (StatusDrop.SelectedValue().ToString() <> "") Then
            PriceIDEntity.PriceStatus = StatusDrop.SelectedItem.Value.ToString()
        End If
        Dim selDS As DataSet = PriceIDEntity.GetPriceID()
        PriceIDView.DataSource = selDS
        PriceIDView.DataBind()
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        If selDS.Tables(0).Rows.Count <> 0 Then
            PriceIDView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRID-0001")
            PriceIDView.HeaderRow.Cells(1).Font.Size = 8
            PriceIDView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRID-0002")
            PriceIDView.HeaderRow.Cells(2).Font.Size = 8
            PriceIDView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Status")
            PriceIDView.HeaderRow.Cells(3).Font.Size = 8
            PriceIDView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRID-0004")
            PriceIDView.HeaderRow.Cells(4).Font.Size = 8
            PriceIDView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRID-0006")
            PriceIDView.HeaderRow.Cells(5).Font.Size = 8
            PriceIDView.HeaderRow.Cells(6).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRID-0007")
            PriceIDView.HeaderRow.Cells(6).Font.Size = 8
        End If
    End Sub

    Protected Sub PriceIDView_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles PriceIDView.RowEditing
        Dim ds As DataSet = Session("PriceIDView")
        'ctryid As String = Me.userView.Rows(e.NewEditIndex).Cells(1).Text
        Dim page As Integer = PriceIDView.PageIndex * PriceIDView.PageSize
        Dim prcid As String = Me.PriceIDView.Rows(e.NewEditIndex).Cells(1).Text
        'Me.ds.Tables(0).Rows(page + e.NewEditIndex).Item(0).ToString()
        prcid = Server.UrlEncode(prcid)
        Dim conid As String = Me.PriceIDView.Rows(e.NewEditIndex).Cells(4).Text
        conid = Server.UrlEncode(conid)
        'Dim statusid As String = ds.Tables(0).Rows(page + e.NewEditIndex).Item(2).ToString
        'statusid = Server.UrlEncode(statusid)
        Dim strTempURL As String = "PriceID=" + prcid + "&CountryID=" + conid
        strTempURL = "~/PresentationLayer/masterrecord/modifyPriceID.aspx?" + strTempURL
        Response.Redirect(strTempURL)
    End Sub
#Region "if have no record "
    Public Function checknorecord(ByVal ds As DataSet) As Integer
        If ds.Tables(0).Rows.Count = 0 Then
            Me.Label1.Visible = True
        Else
            Me.Label1.Visible = False
        End If
    End Function
#End Region
#Region "��ʾ��Ϣ"
    Public Function checkinfo() As Integer
        If Me.PriceIDBox.Text = "" And Me.PriceNameBox.Text = "" Then
            Me.addinfo.Visible = True
        Else
            Me.addinfo.Visible = False
        End If
    End Function
#End Region

End Class
