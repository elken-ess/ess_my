<%@ Page Language="VB" AutoEventWireup="false" EnableEventValidation="false" CodeFile="addSerType.aspx.vb" Inherits="PresentationLayer_masterrecord_addSerType" %>

<%@ Register Assembly="JCalendar" Namespace="JCalendar" TagPrefix="cc2" %>

<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Add Service Type</title>
     <style type="text/css">A:link { COLOR: #336666; TEXT-DECORATION: none }
	BODY { FONT-SIZE: 9pt; FONT-FAMILY: "Arial", "Verdana", "Tahoma"; BACKGROUND-COLOR: #ffffff }
	TD { FONT-SIZE: 9pt; FONT-FAMILY: Arial,Verdana,Tahoma }
	</style>
	<link href="../css/style.css" type="text/css" rel="stylesheet">
    <script language="JavaScript" src="../js/common.js"></script>
</head>
<body>
    <form id="form1" runat="server">
   <TABLE border="0" id=countrytab style="width: 100%">
           <tr>
           <td style="width: 100%" >
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>
							<TD width="1%" background="../graph/title_bg.gif" style="height: 24px"><IMG height="24" src="../graph/title1.gif" width="5"></TD>
							<TD class="style2" width="98%" background="../graph/title_bg.gif" style="height: 24px">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></TD>
							<TD align="right" width="1%" background="../graph/title_bg.gif" style="height: 24px"><IMG height="24" src="../graph/title_2.gif" width="5"></TD>
						</TR>
				 </TABLE>
           </tr>
           <tr>
           <td style="width:100%">
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>					
							<TD class="style2" width="98%" background="../graph/title_bg.gif">
                                <font color=red><asp:Label ID="errlab" runat="server" Text="Label" Visible=false></asp:Label></font>
                                </TD>
						</TR>
			 </TABLE>
           </tr>
           <TR>
				<TD style="width: 100%" >
            <table id="SerTypeID" bgcolor="#b7e6e6" border="0" cellpadding="2" cellspacing="1" style="width: 100%">
              <tr bgcolor="#ffffff">
<td colspan = 4>
                            <asp:Label ID="lblCompTop" runat="server" ForeColor="Red" Text="Label" Visible="True"></asp:Label>
</td>
             </tr>
             <tr bgcolor="#ffffff">
<td colspan = 4>
                        	&nbsp;
                     	</td>
               </tr> 
                <tr bgcolor="#ffffff">
                    <td align="right" width="15%" style="width: 20%">
                        <asp:label id="SerTypeIDLab" runat="server" text="Label"></asp:label>
                    </td>
                    <td align="left" width="35%" style="width: 30%">
                        <asp:textbox id="SerTypeIDbox" runat="server" cssclass="textborder" MaxLength="10" Width="80%" ></asp:textbox>
                        <font color=red><strong>*</strong></font>
                        <asp:RequiredFieldValidator ID="SerTypeIDva" runat="server" ForeColor="White" ControlToValidate="SerTypeIDbox">*</asp:RequiredFieldValidator>&nbsp;
                    </td>
                    <td align="right" width="15%" style="width: 20%">
                        <asp:label id="SerTypeNameLab" runat="server" text="Label"></asp:label>
                    </td>
                    <td align="left" style="width: 30%">
                        <asp:textbox id="SerTypeNamebox" runat="server" cssclass="textborder" MaxLength="50" Width="80%"></asp:textbox>
                        <font color=red><strong>*</strong></font>
                        <asp:RequiredFieldValidator ID="SerNameVa" runat="server" ControlToValidate="SerTypeNamebox"
                            ErrorMessage="RequiredFieldValidator" ForeColor="White">*</asp:RequiredFieldValidator></td>
                </tr>
                <tr bgcolor="#ffffff">
                    <td align="right" width="15%" style="width: 20%">
                        <asp:label id="ALterNamelab" runat="server" text="Label"></asp:label>
                    </td>
                    <td align="left" colspan="3" width="85%" style="width: 80%">
                        <asp:textbox id="ALterNamebox" runat="server" cssclass="textborder" width="62%" MaxLength="50"></asp:textbox>
                    </td>
                </tr>
                <tr bgcolor="#ffffff">
                    <td align="right" width="15%" style="width: 20%">
                        <asp:label id="CountryIDlab" runat="server" text="Label"></asp:label>
                    </td>
                    <td align="left" style="width: 30%" >
                       <asp:dropdownlist id="CountryIDDrop" runat="server" cssclass="textborder"
                            width="80%" AutoPostBack="True">
                                                    <asp:ListItem></asp:ListItem>
                                                </asp:dropdownlist>
                        <font color=red><strong>*</strong></font><asp:RequiredFieldValidator ID="CounTryIDva" runat="server"
                            ForeColor="White" ControlToValidate="CountryIDDrop">*</asp:RequiredFieldValidator>
                    </td>
                    <td align="right" width="15%" style="width: 20%">
                        <asp:label id="SerTypelab" runat="server" text="Label"></asp:label>
                    </td>
                    <td align="left" style="width: 30%">
                       <asp:DropDownList ID="SerTypeDrop" runat="server" Width="62%" AutoPostBack="True">
                        </asp:DropDownList></td>
                </tr>
                 <tr bgcolor="#ffffff">
                    <td align="right" width="15%" style="width: 20%">
                        <asp:label id="ModelIDlab" runat="server" text="LAB"></asp:label>
                    </td>
                    <td align="left" width="35%" style="width: 30%" >
                        <asp:DropDownList ID="ModelIDDrop" runat="server" Width="80%">
                        </asp:DropDownList>
                        <font color=red><strong></strong></font>
                        <asp:RequiredFieldValidator ID="modva"  Enabled =false  runat="server" ControlToValidate="ModelIDDrop"
                            ForeColor="White"></asp:RequiredFieldValidator></td>
                    <td align="right"  width="15%" style="width: 20%">
                        <asp:label id="ReReSliplab" runat="server" text="Label"></asp:label>
                    </td>
                    <td align="left" style="width: 30%;">
                       <asp:DropDownList ID="ReReSlipDrop" runat="server" Width="62%">
                        </asp:DropDownList></td>
                </tr>
                <tr bgcolor="#ffffff">
                    <td align="right" width="15%" style="width: 20%; height: 26px" >
                        <asp:label id="FrequencyLab" runat="server" text="LAB"></asp:label>
                    </td>
                    <td align="left" style="width: 30%; height: 26px" >
                        <asp:DropDownList ID="FrequencyDrop" runat="server" Width ="80%">
                        </asp:DropDownList></td>
                    <td align="right" width="15%" style="width: 20%; height: 26px">
                        <asp:label id="NoOfYearLab" runat="server" text="Label"></asp:label>
                    </td>
                    <td align="left" style="width: 30%; height: 26px">
                       <asp:DropDownList ID="NoOfYearDrop" runat="server" Width="62%">
                        </asp:DropDownList></td>
                       
                    
                </tr>
                     
                <tr bgcolor="#ffffff">
                    <td align="right" style="height: 30px; width: 20%;" >
                        <asp:label id="EffectiveDatelab" runat="server" text="Label"></asp:label>
                    </td>
                    <td align="left" style="height: 30px; width: 30%;" >
                       <asp:textbox id="Efftext" runat="server" cssclass="textborder" width="75%" ReadOnly="True"></asp:textbox>
                        <asp:HyperLink ID="HypCal" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                            ToolTip="Choose a Date">Choose a Date</asp:HyperLink>
                        <cc2:JCalendar
                           ID="Calendareffe" runat="server" ControlToAssign="Efftext" ImgURL="~/PresentationLayer/graph/calendar.gif" Visible="False" />
                        <font color=red><strong>*</strong></font>&nbsp;
                        <asp:RequiredFieldValidator ID="EFFDATVA" runat="server" ControlToValidate="Efftext"
                            ForeColor="White">*</asp:RequiredFieldValidator>&nbsp;
                      
                    </td>
                    <td align="right" style="height: 30px; width: 20%;" >
                        <asp:label id="ObsoleteDatelab" runat="server" text="Label"></asp:label>
                    </td>
                    <td align="left" style="height: 30px; width: 30%;" >
                       <asp:textbox id="Obsotext" runat="server" cssclass="textborder" width="60%" ReadOnly="True"></asp:textbox>
                        <font color=red><strong>*</strong></font>&nbsp;<asp:HyperLink ID="HypCal2" runat="server"
                            ImageUrl="~/PresentationLayer/graph/calendar.gif" ToolTip="Choose a Date">Choose a Date</asp:HyperLink>
                        <cc2:JCalendar
                           ID="Calendarobs" runat="server" ControlToAssign="Obsotext" ImgURL="~/PresentationLayer/graph/calendar.gif" Visible="False" />
                        <asp:RequiredFieldValidator ID="OBSLVA" runat="server" ControlToValidate="Obsotext" ForeColor="White">*</asp:RequiredFieldValidator>&nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
                <tr bgcolor="#ffffff">
                    <td align="right" width="15%" style="width: 20%">
                        <asp:label id="Statuslab" runat="server" text="Label"></asp:label>
                    </td>
                    <td align="left" colspan="3" width="85%" style="width: 80%">
                        <asp:dropdownlist id="StatusDrop" runat="server" cssclass="textborder" Width="35%">
                                                </asp:dropdownlist>
                    </td>
                    
                </tr>
                 <tr bgcolor="#ffffff">
                    <td align="right" width="15%" style="width: 20%">
                        <asp:label id="RMDATLab" runat="server" text="Label"></asp:label>
                    </td>
                    <td align="left" colspan="3" width="85%" style="width: 80%">
                        <asp:TextBox ID="RMDATBOX" runat="server" CssClass="textborder" MaxLength="10" Width="62%"></asp:TextBox>
                        <asp:RangeValidator ID="rmdtVA" runat="server" ControlToValidate="RMDATBOX" ForeColor="White"
                            Type="Double" MaximumValue="9999999999" MinimumValue="0000000000">*</asp:RangeValidator></td>
                    
                </tr>
                <tr bgcolor="#ffffff">
                    <td align="right" s style="width: 20%">
                        <asp:label id="PointCRLab" runat="server" text="Label"></asp:label>
                    </td>
                    <td align="left" style="width: 30%" >
                        <asp:textbox id="PointCRbox" runat="server" cssclass="textborder" MaxLength="6" Width="80%"></asp:textbox>&nbsp;<asp:RangeValidator
                            ID="PointCRVA" runat="server" ControlToValidate="PointCRbox" ForeColor="White"
                            Type="Double" MaximumValue="999.99" MinimumValue="000.00">*</asp:RangeValidator>
                    </td>
                    <td align="right" style="color: #000000; width: 20%;" width="15%">
                        <asp:label id="PointTeLab" runat="server" text="Label"></asp:label>
                    </td>
                    <td align="left" style="width: 30%;">
                        <asp:textbox id="PointTebox" runat="server" cssclass="textborder" MaxLength="6" Width="80%"></asp:textbox>
                        <asp:RangeValidator ID="PointTeVA" runat="server" ControlToValidate="PointTebox"
                            ForeColor="White" Type="Double" MaximumValue="999.99" MinimumValue="000.00">*</asp:RangeValidator></td>
                    
                </tr>
                <tr bgcolor="#ffffff">
                    <td align="right" width="15%" style="width: 20%; height: 24px;">
                        <asp:label id="RsdLab" runat="server" text="Label"></asp:label>
                    </td>
                    <td align="left" colspan="3" width="85%" style="width: 80%; height: 24px;">
                        <asp:dropdownlist id="RsdDrop" runat="server" cssclass="textborder" width="35%">
                                                </asp:dropdownlist>
                        </td>
                </tr>
                <tr bgcolor="#ffffff">
                    <td align="right" width="15%" style="height: 24px; width: 20%;">
                        <asp:label id="ulsdLab" runat="server" text="Label"></asp:label>
                    </td>
                    <td align="left" colspan="3" width="85%" style="height: 24px; width: 80%;">
                        <asp:dropdownlist id="ulsdDrop" runat="server" cssclass="textborder" width="35%">
                                                </asp:dropdownlist>
                    </td>
                </tr>
                <tr bgcolor="#ffffff">
                    <td align="right" style="width: 20%" >
                        <asp:label id="CreatedBylab" runat="server" text="Label"></asp:label>
                    </td>
                    <td align="left" style="width: 30%" >
                        <asp:textbox id="CreatedBy" runat="server" cssclass="textborder" Width="80%"></asp:textbox>&nbsp;
                    </td>
                    <td align="right" style="width: 20%" >
                        <asp:label id="CreatedDatelab" runat="server" text="Label"></asp:label>
                    </td>
                    <td align="left" style="width: 30%" >
                        <asp:textbox id="CreatedDate" runat="server" cssclass="textborder" Width="90%" ></asp:textbox>
                       
                    </td>
                </tr>
                <tr bgcolor="#ffffff">
                    <td align="right" style="width: 20%" >
                        <asp:label id="ModifiedBylab" runat="server" text="Label"></asp:label>
                    </td>
                    <td align="left" style="width: 30%" >
                        <asp:textbox id="ModifiedBy" runat="server" cssclass="textborder" Width="80%"></asp:textbox>
                    </td>
                    <td align="right" style="width: 20%" >
                        <asp:label id="ModifiedDatelab" runat="server" text="Label"></asp:label>
                    </td>
                    <td align="left" style="width: 30%" >
                        <asp:textbox id="ModifiedDate" runat="server" cssclass="textborder" Width="90%" ></asp:textbox>
                       
                    </td>
                </tr>
            </table>
                    <asp:GridView ID="duringView" runat="server" style="width: 100%">
                    </asp:GridView>
                    <font color=red><asp:Label ID="lblCompBottom" runat="server" Text="Label" Visible="true"></asp:Label></font>
                    <br />
                                           <asp:LinkButton ID="savebutton" runat="server">save</asp:LinkButton>
                    &nbsp; &nbsp;&nbsp;
                   
                  
                    <asp:HyperLink ID="cancellink" runat="server" NavigateUrl="~/PresentationLayer/masterrecord/SerType.aspx">cancel</asp:HyperLink>
                    &nbsp; &nbsp;&nbsp;
                    <asp:HyperLink ID="AddLink" runat="server" >[Add]</asp:HyperLink>
                    
                    </TD>
					</TR>
					<TR>
						<TD style="width: 763px" >
							<TABLE id="Table1" cellSpacing="1" cellPadding="1"  border="0">
									<TR>
										<TD  align=center>
                                           </TD>
										<TD align=center >
                                            </TD>
									    </TR>
							</TABLE>
							</TD>
									    </TR>
							</TABLE>
							
                            <asp:ValidationSummary ID="ErrorMessage" runat="server" ForeColor="DarkRed" ShowMessageBox="True"
                                ShowSummary="False" />
                            <cc1:messagebox id="MessageBox1" runat="server"></cc1:messagebox>
                       
					</form>
</body>
</html>
