<%@ Page Language="VB" AutoEventWireup="false"  enableEventValidation="false" CodeFile="mdfyjobsperdayRS.aspx.vb" Inherits="PresentationLayer_masterrecord_mdfyjobsperdayRS" %>

<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
  <title>Modify Jobs Per Day (Rental)</title>
    <STYLE type="text/css">A:link { COLOR: #336666; TEXT-DECORATION: none }
	BODY { FONT-SIZE: 9pt; FONT-FAMILY: "Arial", "Verdana", "Tahoma"; BACKGROUND-COLOR: #ffffff }
	TD { FONT-SIZE: 9pt; FONT-FAMILY: Arial,Verdana,Tahoma }
	</STYLE>
		<LINK href="../css/style.css" type="text/css" rel="stylesheet">
	<SCRIPT language="JavaScript">		
		</SCRIPT>
</head>
<body  id="jobsperdaybody">
   <form id="jobsperdayform"  method="post" action=""  runat=server>   
    <TABLE border="0" id=TABLE2 style="width: 100%">
           <tr>
           <td>
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>
							<TD width="1%" background="../graph/title_bg.gif"><IMG height="24" src="../graph/title1.gif" width="5"></TD>
							<TD class="style2" width="98%" background="../graph/title_bg.gif">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></TD>
							<TD align="right" width="1%" background="../graph/title_bg.gif"><IMG height="24" src="../graph/title_2.gif" width="5"></TD>
						</TR>
			 </TABLE>
           </tr>
           <tr>
           <td>
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>					
							<TD class="style2" width="98%" background="../graph/title_bg.gif">
                                <font color=red><asp:Label ID="mdyinfo" runat="server" Text="Label" Visible=false></asp:Label></font></TD>
						</TR>
			 </TABLE>
           </tr>
           <tr>
            <td>
                <asp:Label ID="Label5" runat="server" ForeColor="Red"></asp:Label></td>
           </tr>
			<TR>
				<TD>
    <TABLE id="jobsperday" cellSpacing="1" cellPadding="2" bgColor="#b7e6e6" border="0">
									  <TR bgColor="#ffffff">
											<TD  align=right style="width: 14%; height: 19px">
                                                &nbsp;<asp:Label ID="svcidLab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%; height: 19px;">
                                                <asp:TextBox ID="SVCID" runat="server" CssClass="textborder" ReadOnly="True" width="90%"></asp:TextBox>
                                                <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red" Font-Bold="True"></asp:Label></TD>
											<TD  align=right style="width: 175px; height: 19px">
                                                <asp:Label ID="totchLab" runat="server" Text="Label"></asp:Label></TD>
											<TD  align=left style="width: 35%; height: 19px">
                                                <asp:TextBox ID="TOTCHBox" runat="server" CssClass="textborder" MaxLength="4"></asp:TextBox>
                                                <asp:Label ID="Label2" runat="server" ForeColor="Red" Text="*" Font-Bold="True"></asp:Label>
                                                <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="TOTCHBox"
                                                    ErrorMessage="RangeValidator" MaximumValue="999999" MinimumValue="0" Type="Integer" ForeColor="White">*</asp:RangeValidator>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TOTCHBox"
                                                    ErrorMessage="RequiredFieldValidator" ForeColor="White">*</asp:RequiredFieldValidator></TD>
										</TR>
										<TR bgColor="#ffffff">
											<TD  align=right style="height: 1px; width: 14%;">
                                                &nbsp;<asp:Label ID="jdateLab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%; height: 1px">
                                                <asp:TextBox ID="JDATEBox" runat="server" CssClass="textborder" ReadOnly="True"></asp:TextBox>
                                                <asp:Label ID="Label4" runat="server" ForeColor="Red" Text="*" Font-Bold="True"></asp:Label></TD>
											<TD  align=right style="height: 1px; width: 175px;">
                                                <asp:Label ID="totaljobLab" runat="server" Text="Label"></asp:Label></TD>
											<TD  align=left style="height: 1px; width: 35%;">
                                                <asp:TextBox ID="TOJOBBox" runat="server" CssClass="textborder" MaxLength="4"></asp:TextBox>
                                                <asp:Label
                                                        ID="Label3" runat="server" ForeColor="Red" Text="*" Font-Bold="True"></asp:Label>
                                                <asp:RangeValidator ID="RangeValidator2" runat="server" ErrorMessage="RangeValidator"
                                                    MaximumValue="999999" MinimumValue="0" Type="Integer" ControlToValidate="TOJOBBox" ForeColor="White">*</asp:RangeValidator>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TOJOBBox"
                                                    ErrorMessage="RequiredFieldValidator" ForeColor="White">*</asp:RequiredFieldValidator></TD>
										</TR>
										<TR bgColor="#ffffff">
										<TD  align=right style="height: 26px; width: 14%;">
                                                &nbsp;<asp:Label ID="totaljobtdyLab" runat="server" Text="Label"></asp:Label></TD>
                                            <td align="left" colspan="3" style="height: 26px">
                                                <asp:TextBox ID="totaltdy" runat="server" CssClass="textborder" ReadOnly="True"></asp:TextBox></td>
										</TR>
										<TR bgColor="#ffffff">
											<TD  align=right style="height: 26px; width: 14%;">
                                                &nbsp;<asp:Label ID="crtdLab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%; height: 26px">
                                                <asp:TextBox ID="CUSERBox" runat="server" CssClass="textborder"></asp:TextBox></TD>
											<TD  align=right style="height: 26px; width: 175px;">
                                                <asp:Label ID="crtddtLab" runat="server" Text="Label"></asp:Label></TD>
											<TD  align=left style="height: 26px; width: 35%;">
                                                <asp:TextBox ID="CREDTBox" runat="server" CssClass="textborder"></asp:TextBox></TD>
										</TR>
										<TR bgColor="#ffffff">
											<TD  align=right style="width: 14%; height: 28px">
                                                &nbsp;<asp:Label ID="mdfyLab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%; height: 28px;">
                                                <asp:TextBox ID="LUSERtBox" runat="server" CssClass="textborder"></asp:TextBox></TD>
											<TD  align=right style="width: 175px; height: 28px">
                                                <asp:Label ID="mdfydtLab" runat="server" Text="Label"></asp:Label></TD>
											<TD  align=left style="width: 35%; height: 28px">
                                                <asp:TextBox ID="LSTDTBox" runat="server" CssClass="textborder"></asp:TextBox></TD>
										</TR>
										
								</TABLE>
								<table>
								<tr>
					    <td style="height: 16px">
                            <asp:Label ID="Label6" runat="server" ForeColor="Red"></asp:Label></td>
					</tr></table>
		<TABLE id="Table1" cellSpacing="1" cellPadding="1"  border="0">
									<TR>
										<TD style="WIDTH: 20%; height: 17px;" align=center>
                                            &nbsp;<asp:LinkButton ID="LinkButton1" runat="server">save</asp:LinkButton></TD>
										<td align=center style="width: 173px; height: 17px">
                                            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/PresentationLayer/masterrecord/jobsperdayRS.aspx">cancel</asp:HyperLink></td>
				                    <tr>
			                   </TABLE>
			                                  			</TD>
					</TR>
			</TABLE>
       <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
           ShowSummary="False" />
                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="TOJOBBox"
                        ControlToValidate="totaltdy" ErrorMessage="CompareValidator" ForeColor="White"
                        Operator="LessThanEqual" Type="Integer">*</asp:CompareValidator>
                    <cc1:messagebox id="MessageBox1" runat="server"></cc1:messagebox>
       <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="SVCID"
           ErrorMessage="RequiredFieldValidator" ForeColor="White">*</asp:RequiredFieldValidator>

    </form>
</body>
</html>
