Imports SQLDataAccess
Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data





Partial Class PresentationLayer_masterrecord_addPriceID

    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        If Not Page.IsPostBack Then
           
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            PriceIDLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRID-0001")
            PriceNameLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRID-0002")
            ALterNamelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRID-0003")
            CountryIDlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRID-0004")
            CurrencyNamelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRID-0005")
            EffectiveDatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRID-0006")
            ObsoleteDatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRID-0007")
            CreatedBylab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATBY")
            CreatedDatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
            ModifiedBylab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFBY")
            ModifiedDatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFDATE")
            Statuslab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Status")
            saveButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Save")
            cancelLink.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Cancel")
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRID-0008")
            Me.lblCompTop.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            Me.lblCompBottom.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")

            AddLink.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Add")
            AddLink.NavigateUrl = Page.Request.RawUrl

            Dim objXmlTr1 As New clsXml
            objXmlTr1.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            PriceIDVA.ErrorMessage = objXmlTr1.GetLabelName("StatusMessage", "ID")
            CountryIDVa.ErrorMessage = objXmlTr1.GetLabelName("StatusMessage", "CountryID")
            Pricenameva.ErrorMessage = objXmlTr1.GetLabelName("StatusMessage", "PRICENAME")
            EFFDATVA.ErrorMessage = objXmlTr1.GetLabelName("StatusMessage", "BB-HintMsg-Effecdate")
            OBSLVA.ErrorMessage = objXmlTr1.GetLabelName("StatusMessage", "BB-HintMsg-Obsedate")

            '''access control'''''''''''''''''''''''''''''''''''''''''''''''
            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "17")
            Me.saveButton.Enabled = purviewArray(0)
            If purviewArray(0) = False Then
                Response.Redirect("~/PresentationLayer/logon.aspx")

            End If


            Me.PriceIDbox.Focus()
            Dim userIDNamestr As String = Session("userID").ToString().ToUpper + "-" + Session("username").ToString.ToUpper


            Me.CreatedBy.Text = userIDNamestr
            Me.ModifiedBy.Text = userIDNamestr
            CreatedDate.Text = Date.Now()
            ModifiedDate.Text = Date.Now()
            'Me.CreatedBy.Text = Session("username").ToString().ToUpper()
            'Me.ModifiedBy.Text = Session("username").ToString().ToUpper()
            Dim rank As String = Session("login_rank").ToString().ToUpper()
            Dim country As New clsCommonClass
            If rank <> 0 Then
                country.spctr = Session("login_ctryID").ToString().ToUpper()
            End If
            country.rank = rank
            Dim countryds As New DataSet
            countryds = country.Getcomidname("BB_MASCTRY_IDNAME")
            If countryds.Tables.Count <> 0 Then
                databonds(countryds, Me.CountryIDDrop)
                Me.CountryIDDrop.Items.Insert(0, "")
            End If
            CountryIDDrop.SelectedValue = Session("login_ctryID").ToString().ToUpper()

            Dim PriceIDEntity As New clsPriceID()
            PriceIDEntity.CountryID = CountryIDDrop.SelectedValue
            Dim edtReader As ArrayList = PriceIDEntity.GetCountryID()
            If (edtReader.Count <> 0) Then
                CurrencyNamebox.Text = edtReader(5).ToString()
            End If


            Dim priceidStat As New clsrlconfirminf()
            Dim statParam As ArrayList = New ArrayList
            statParam = priceidStat.searchconfirminf("STATUS")
            Dim count As Integer
            Dim statid As String
            Dim statnm As String
            For count = 0 To statParam.Count - 1
                statid = statParam.Item(count)
                statnm = statParam.Item(count + 1)
                count = count + 1
                If (statid.Equals("ACTIVE") Or statid.Equals("OBSOLETE")) Then
                    Me.StatusDrop.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                End If
            Next
            If StatusDrop.Items.Count <> 0 Then
                StatusDrop.Items(0).Selected = True
            End If

        End If
        HypCal.NavigateUrl = "javascript:DoCal(document.PriceIDform.Efftext);"
        HypCal2.NavigateUrl = "javascript:DoCal(document.PriceIDform.Obsotext);"
    End Sub
   
#Region " id bond"
    Public Function databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        Dim row As DataRow
        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            NewItem.Text = row("id") & "-" & row("name")
            NewItem.Value = row("id")
            dropdown.Items.Add(NewItem)
        Next

    End Function
#End Region

  Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveButton.Click
        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim temparr As Array = Request.Form("Efftext").Split("/") 'Efftext.Text.Split("/")
        Dim streff As String
        Dim strobs As String
        streff = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
        temparr = Request.Form("Obsotext").Split("/") 'Obsotext.Text.Split("/")
        strobs = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
        If Convert.ToDateTime(strobs) < Convert.ToDateTime(streff) Then
            errlab.Text = objXm.GetLabelName("StatusMessage", "BB-HintMsg-COMPAREEFFOBDATE")
            errlab.Visible = True
            Return
        End If
        Efftext.Text = Request.Form("Efftext")
        Obsotext.Text = Request.Form("Obsotext")
        Dim msginfo As String = objXm.GetLabelName("StatusMessage", "SAVEORNOT")
        Dim msgtitle As String = objXm.GetLabelName("StatusMessage", "SAVETITLE")
        MessageBox1.Confirm(msginfo)

    End Sub

    Protected Sub CountryIDDrop_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CountryIDDrop.TextChanged
        Dim PriceIDEntity As New clsPriceID()
        PriceIDEntity.CountryID = CountryIDDrop.SelectedItem.Value.ToString()
        Dim edtReader As ArrayList = PriceIDEntity.GetCountryID()
        If (edtReader.Count <> 0) Then
            CurrencyNamebox.Text = edtReader(5).ToString()
        Else
            CurrencyNamebox.Text = ""
        End If
        ' user infor
        PriceIDEntity.ModifiedBy = Session("username").ToString().ToUpper()
        Efftext.Text = Request.Form("Efftext")
        Obsotext.Text = Request.Form("Obsotext")
    End Sub

  

   
  
    Protected Sub MessageBox1_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox1.GetMessageBoxResponse
        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then


            Dim priceidStat As New clsCommonClass()
            Dim PriceIDEntity As New clsPriceID()

            If (Trim(PriceIDbox.Text) <> "") Then
                PriceIDEntity.PriceID = PriceIDbox.Text.ToUpper()
            End If
            If (Trim(PriceNamebox.Text) <> "") Then
                PriceIDEntity.PriceName = PriceNamebox.Text.ToUpper()
            End If
            If (Trim(ALterNamebox.Text) <> "") Then
                PriceIDEntity.PriceAlternateName = ALterNamebox.Text.ToUpper()
            End If
            If (StatusDrop.SelectedItem.Value.ToString() <> "") Then
                PriceIDEntity.PriceStatus = StatusDrop.SelectedItem.Value.ToString()
            End If
            If (CountryIDDrop.SelectedItem.Value.ToString() <> "") Then
                PriceIDEntity.CountryID = CountryIDDrop.SelectedItem.Value.ToString()
            End If
            If (Trim(Request.Form("Obsotext")) <> "") Then
                Dim temparr As Array = Request.Form("Obsotext").Split("/") 'Obsotext.Text.Split("/")
                PriceIDEntity.ObsoleteDate = temparr(2) + "-" + temparr(1) + "-" + temparr(0)

            End If
            If (Trim(Request.Form("Efftext")) <> "") Then
                Dim temparr As Array = Request.Form("Efftext").Split("/") 'Efftext.Text.Split("/")
                PriceIDEntity.EffectiveDate = temparr(2) + "-" + temparr(1) + "-" + temparr(0)

            End If
          
            'If (Trim(CreatedBy.Text) <> "") Then
            '    PriceIDEntity.CreatedBy = CreatedBy.Text.ToUpper()
            'End If
            If (Trim(CreatedDate.Text) <> "") Then
                PriceIDEntity.CreatedDate = priceidStat.DatetoDatabase(CreatedDate.Text)
            End If
            'If (Trim(ModifiedBy.Text) <> "") Then
            '    PriceIDEntity.ModifiedBy = ModifiedBy.Text.ToUpper()
            'End If
            If (Trim(ModifiedDate.Text) <> "") Then
                PriceIDEntity.ModifiedDate = priceidStat.DatetoDatabase(ModifiedDate.Text)
            End If

            If (Trim(Me.CreatedBy.Text) <> "") Then
                PriceIDEntity.CreatedBy = Session("userID").ToString.ToUpper()
            End If
            If (Trim(Me.ModifiedBy.Text) <> "") Then
                PriceIDEntity.ModifiedBy = Session("userID").ToString.ToUpper()
            End If
            Dim dupCount As Integer = PriceIDEntity.GetDuplicatedPriceID()
            If dupCount.Equals(-1) Then
                Dim objXm As New clsXml
                objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                errlab.Text = objXm.GetLabelName("StatusMessage", "DUPPRICE")
                errlab.Visible = True
            ElseIf dupCount.Equals(0) Then
                Dim insPRIDCnt As Integer = PriceIDEntity.Insert()
                If insPRIDCnt = 0 Then
                    Dim objXm As New clsXml
                    objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    errlab.Text = objXm.GetLabelName("StatusMessage", "SuccessfullySaved")
                    errlab.Visible = True
                Else
                    Response.Redirect("~/PresentationLayer/Error.aspx")
                End If
            End If
            Efftext.Text = Request.Form("Efftext")
            Obsotext.Text = Request.Form("Obsotext")
        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
            Efftext.Text = Request.Form("Efftext")
            Obsotext.Text = Request.Form("Obsotext")
        End If
    End Sub

    Protected Sub Calendareffe_SelectedDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendareffe.SelectedDateChanged
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Dim prcidDate As New clsCommonClass()
        Efftext.Text = Calendareffe.SelectedDate.Date.ToString().Substring(0, 10)
        'Calendareffe.Visible = False
    End Sub

    Protected Sub CalendarObs_SelectedDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CalendarObs.SelectedDateChanged
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Dim prcidDate As New clsCommonClass()
        Obsotext.Text = CalendarObs.SelectedDate.Date.ToString().Substring(0, 10)
        'CalendarObs.Visible = False
    End Sub

    Protected Sub Calendareffe_CalendarVisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendareffe.CalendarVisibleChanged
        StatusDrop.Visible = Not Calendareffe.CalendarVisible
    End Sub
End Class
