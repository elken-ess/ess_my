Imports SQLDataAccess
Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data




Partial Class PresentationLayer_masterrecord_addSerType

    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.MaintainScrollPositionOnPostBack = True
        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        If Not Page.IsPostBack Then

            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            SerTypeIDLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSRCT-0001")
            SerTypeNameLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSRCT-0002")
            ALterNamelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSRCT-0003")
            CountryIDlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTAX-0004")
            SerTypelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSRCT-0005")
            ModelIDlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSRCT-0006")
            ReReSliplab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSRCT-0009")
            FrequencyLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSRCT-0007")
            NoOfYearLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSRCT-0008")
            PointCRLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSRCT-0010")
            PointTeLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSRCT-0011")
            RsdLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSRCT-0012")
            ulsdLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSRCT-0013")
            EffectiveDatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSRCT-0014")
            ObsoleteDatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSRCT-0015")
            CreatedBylab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATBY")
            CreatedDatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
            ModifiedBylab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFBY")
            ModifiedDatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFDATE")
            Statuslab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Status")
            savebutton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Save")
            cancellink.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Cancel")
            Me.lblCompTop.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            Me.lblCompBottom.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            'LinkButton2.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSRCT-0016")
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSRCT-0017")
            RMDATLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSRCT-0022")

            AddLink.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Add")
            AddLink.NavigateUrl = Page.Request.RawUrl

            Dim objXmlTr1 As New clsXml
            objXmlTr1.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            SerTypeIDva.ErrorMessage = objXmlTr1.GetLabelName("StatusMessage", "ID")
            CounTryIDva.ErrorMessage = objXmlTr1.GetLabelName("StatusMessage", "CountryID")
            SerNameVa.ErrorMessage = objXmlTr1.GetLabelName("StatusMessage", "SERNAMENULL")
            rmdtVA.ErrorMessage = objXmlTr1.GetLabelName("StatusMessage", "RMDTTERROR")
            PointCRVA.ErrorMessage = objXmlTr1.GetLabelName("StatusMessage", "PointCRERROR")
            PointTeVA.ErrorMessage = objXmlTr1.GetLabelName("StatusMessage", "PointTeERROR")
            EFFDATVA.ErrorMessage = objXmlTr1.GetLabelName("StatusMessage", "BB-HintMsg-Effecdate")
            OBSLVA.ErrorMessage = objXmlTr1.GetLabelName("StatusMessage", "BB-HintMsg-Obsedate")
            modva.ErrorMessage = objXmlTr1.GetLabelName("StatusMessage", "SERTYPEMODLE")


            'RSDVA.ErrorMessage = objXmlTr1.GetLabelName("StatusMessage", "RSDNULLCM")

            'COMOBSL.ErrorMessage = objXmlTr1.GetLabelName("StatusMessage", "BB-HintMsg-COMPAREEFFOBDATE")

            '''access control'''''''''''''''''''''''''''''''''''''''''''''''
            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "21")
            Me.savebutton.Enabled = purviewArray(0)
            If purviewArray(0) = False Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return

            End If


            Me.SerTypeIDbox.Focus()
            'display countryid
            Dim userIDNamestr As String = Session("userID").ToString().ToUpper + "-" + Session("username").ToString.ToUpper


            CreatedBy.Text = userIDNamestr
            ModifiedBy.Text = userIDNamestr
            CreatedDate.Text = Date.Now()
            ModifiedDate.Text = Date.Now()
            'Me.CreatedBy.Text = Session("username").ToString().ToUpper()
            'Me.ModifiedBy.Text = Session("username").ToString().ToUpper()
            Me.CreatedBy.ReadOnly = True
            Me.CreatedDate.ReadOnly = True
            Me.ModifiedBy.ReadOnly = True
            Me.ModifiedDate.ReadOnly = True

            Dim rank As String = Session("login_rank").ToString().ToUpper()
            Dim country As New clsCommonClass
            If rank <> 0 Then
                country.spctr = Session("login_ctryID").ToString().ToUpper()

            End If
            country.rank = rank
            Dim countryds As New DataSet
            countryds = country.Getcomidname("BB_MASCTRY_IDNAME")
            If countryds.Tables.Count <> 0 Then
                databonds(countryds, Me.CountryIDDrop)
                Me.CountryIDDrop.Items.Insert(0, "")
            End If
            CountryIDDrop.SelectedValue = Session("login_ctryID").ToString().ToUpper()
            'Dim country As New clsCommonClass
            'Dim countryds As New DataSet
            'countryds = country.Getidname("BB_MASCTRY_IDNAME")
            'databonds(countryds, Me.CountryIDDrop)

            'display modelid
            Dim modelid As New clsCommonClass
            If rank <> 0 Then
                modelid.spctr = CountryIDDrop.SelectedItem.Value.ToString()

            End If
            modelid.rank = rank
            Dim modelidds As New DataSet
            modelidds = modelid.Getcomidname("BB_MASMOTY_IDNAME ")
            If modelidds.Tables.Count <> 0 Then
                databonds(modelidds, Me.ModelIDDrop)
                Me.ModelIDDrop.Items.Insert(0, "")
            End If

            'display Status
            Dim cntryStat As New clsrlconfirminf()
            Dim statParam As ArrayList = New ArrayList
            statParam = cntryStat.searchconfirminf("STATUS")
            Dim count As Integer
            Dim statid As String
            Dim statnm As String
            For count = 0 To statParam.Count - 1
                statid = statParam.Item(count)
                statnm = statParam.Item(count + 1)
                count = count + 1
                If (statid.Equals("ACTIVE") Or statid.Equals("OBSOLETE")) Then
                    Me.StatusDrop.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                End If

            Next
            StatusDrop.Items(0).Selected = True

            'display ServiceType
            Dim sertypeStat As New clsrlconfirminf()
            Dim sertypeParam As ArrayList = New ArrayList
            sertypeParam = sertypeStat.searchconfirminf("SERTYPE")
            Dim count1 As Integer
            Dim sertypeid As String
            Dim sertypenm As String
            For count1 = 0 To sertypeParam.Count - 1
                sertypeid = sertypeParam.Item(count1)
                sertypenm = sertypeParam.Item(count1 + 1)
                count1 = count1 + 1
                SerTypeDrop.Items.Add(New ListItem(sertypenm.ToString(), sertypeid.ToString()))

            Next
            SerTypeDrop.Items(1).Selected = True
            'display rsd
            Dim rsdStat As New clsrlconfirminf()
            Dim rsdParam As ArrayList = New ArrayList
            rsdParam = rsdStat.searchconfirminf("RSD")
            Dim count2 As Integer
            Dim rsdid As String
            Dim rsdnm As String
            For count2 = 0 To rsdParam.Count - 1
                rsdid = rsdParam.Item(count2)
                rsdnm = rsdParam.Item(count2 + 1)
                count2 = count2 + 1
                RsdDrop.Items.Add(New ListItem(rsdnm.ToString(), rsdid.ToString()))

            Next
            Me.RsdDrop.Items.Insert(0, "")
            RsdDrop.Items(0).Selected = True
            'display ulsd
            Dim ulsdStat As New clsrlconfirminf()
            Dim ulsdParam As ArrayList = New ArrayList
            ulsdParam = ulsdStat.searchconfirminf("YESNO")
            Dim count3 As Integer
            Dim ulsdid As String
            Dim ulsdnm As String
            For count3 = 0 To ulsdParam.Count - 1
                ulsdid = ulsdParam.Item(count3)
                ulsdnm = ulsdParam.Item(count3 + 1)
                count3 = count3 + 1
                ulsdDrop.Items.Add(New ListItem(ulsdnm.ToString(), ulsdid.ToString()))

            Next
            ulsdDrop.Items(0).Selected = True
            'display rrs
            Dim rrsStat As New clsrlconfirminf()
            Dim rrsParam As ArrayList = New ArrayList
            rrsParam = rrsStat.searchconfirminf("YESNO")
            Dim count4 As Integer
            Dim rrsid As String
            Dim rrsnm As String
            For count4 = 0 To rrsParam.Count - 1
                rrsid = rrsParam.Item(count4)
                rrsnm = rrsParam.Item(count4 + 1)
                count4 = count4 + 1
                ReReSlipDrop.Items.Add(New ListItem(rrsnm.ToString(), rrsid.ToString()))

            Next
            ReReSlipDrop.Items(0).Selected = True
            'display fre
            Dim freStat As New clsrlconfirminf()
            Dim freParam As ArrayList = New ArrayList
            freParam = freStat.searchconfirminf("FREQUENCY")
            Dim count5 As Integer
            Dim freid As String
            Dim frenm As String
            For count5 = 0 To freParam.Count - 1
                freid = freParam.Item(count5)
                frenm = freParam.Item(count5 + 1)
                count5 = count5 + 1
                FrequencyDrop.Items.Add(New ListItem(frenm.ToString(), freid.ToString()))

            Next
            FrequencyDrop.Items(0).Selected = True
            'display NOY
            Dim noyStat As New clsrlconfirminf()
            Dim noyParam As ArrayList = New ArrayList
            noyParam = noyStat.searchconfirminf("NOY")
            Dim count6 As Integer
            Dim noyid As String
            Dim noynm As String
            For count6 = 0 To noyParam.Count - 1
                noyid = noyParam.Item(count6)
                noynm = noyParam.Item(count6 + 1)
                count6 = count6 + 1
                NoOfYearDrop.Items.Add(New ListItem(noynm.ToString(), noyid.ToString()))

            Next
            NoOfYearDrop.Items(0).Selected = True
        End If
        HypCal.NavigateUrl = "javascript:DoCal(document.form1.Efftext);"
        HypCal2.NavigateUrl = "javascript:DoCal(document.form1.Obsotext);"
    End Sub
#Region " id bond"
    Public Function databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        Dim row As DataRow
        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            NewItem.Text = row("id") & "-" & row("name")
            NewItem.Value = row("id")
            dropdown.Items.Add(NewItem)
        Next

    End Function
#End Region
    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles savebutton.Click
        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim temparr As Array = Request.Form("Efftext").Split("/") 'Efftext.Text.Split("/")
        Dim streff As String
        Dim strobs As String
        streff = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
        temparr = Request.Form("Obsotext").Split("/") 'Obsotext.Text.Split("/")
        strobs = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
        If Convert.ToDateTime(strobs) < Convert.ToDateTime(streff) Then
            errlab.Text = objXm.GetLabelName("StatusMessage", "BB-HintMsg-COMPAREEFFOBDATE")
            errlab.Visible = True
            Return
        End If

        If ulsdDrop.SelectedIndex = 0 And Val(RMDATBOX.Text.Trim) <= 0 Then
            MessageBox1.Alert(objXm.GetLabelName("StatusMessage", "REMINDER_DAY_IS_ZERO"))
            Return
        End If

        Efftext.Text = Request.Form("Efftext")
        Obsotext.Text = Request.Form("Obsotext")
        Dim msginfo As String = objXm.GetLabelName("StatusMessage", "SAVEORNOT")
        Dim msgtitle As String = objXm.GetLabelName("StatusMessage", "SAVETITLE")
        MessageBox1.Confirm(msginfo)
        


    End Sub


   

   

    
   

 
  Protected Sub SerTypeDrop_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles SerTypeDrop.SelectedIndexChanged
        Dim SerTypeEntity As New clsSerType()
        SerTypeEntity.ModifiedBy = Session("username").ToString().ToUpper()
        If SerTypeDrop.SelectedItem.Value.ToString() = "FREE" Then
            FrequencyDrop.Items.Clear()

            Dim frequency As New clsrlconfirminf()
            Dim freParam As ArrayList = New ArrayList
            freParam = frequency.searchconfirminf("FREQUENCY")
            Dim count As Integer
            Dim freid As String
            Dim frenm As String
            For count = 0 To freParam.Count - 1
                freid = freParam.Item(count)
                frenm = freParam.Item(count + 1)
                count = count + 1
                If freid.Equals("1") Then
                    FrequencyDrop.Items.Add(New ListItem(frenm.ToString(), freid.ToString()))
                End If
            Next
            FrequencyDrop.Items(0).Selected = True


            NoOfYearDrop.Items.Clear()

            Dim noy As New clsrlconfirminf()
            Dim noyParam As ArrayList = New ArrayList
            noyParam = noy.searchconfirminf("NOY")
            Dim count1 As Integer
            Dim noyid As String
            Dim noynm As String
            For count1 = 0 To noyParam.Count - 1
                noyid = noyParam.Item(count1)
                noynm = noyParam.Item(count1 + 1)
                count1 = count1 + 1
                If noyid.Equals("a") Then
                    NoOfYearDrop.Items.Add(New ListItem(noynm.ToString(), noyid.ToString()))
                End If
            Next
            NoOfYearDrop.Items(0).Selected = True


        Else
            'display fre
            FrequencyDrop.Items.Clear()
            Dim freStat As New clsrlconfirminf()
            Dim freParam As ArrayList = New ArrayList
            freParam = freStat.searchconfirminf("FREQUENCY")
            Dim count5 As Integer
            Dim freid As String
            Dim frenm As String
            For count5 = 0 To freParam.Count - 1
                freid = freParam.Item(count5)
                frenm = freParam.Item(count5 + 1)
                count5 = count5 + 1
                FrequencyDrop.Items.Add(New ListItem(frenm.ToString(), freid.ToString()))

            Next
            FrequencyDrop.Items(0).Selected = True

            'display NOY
            NoOfYearDrop.Items.Clear()

            Dim noyStat As New clsrlconfirminf()
            Dim noyParam As ArrayList = New ArrayList
            noyParam = noyStat.searchconfirminf("NOY")
            Dim count6 As Integer
            Dim noyid As String
            Dim noynm As String
            For count6 = 0 To noyParam.Count - 1
                noyid = noyParam.Item(count6)
                noynm = noyParam.Item(count6 + 1)
                count6 = count6 + 1
                NoOfYearDrop.Items.Add(New ListItem(noynm.ToString(), noyid.ToString()))

            Next
            NoOfYearDrop.Items(0).Selected = True
        End If
        FrequencyDrop.AutoPostBack = False
        NoOfYearDrop.AutoPostBack = False

        Efftext.Text = Request.Form("Efftext")
        Obsotext.Text = Request.Form("Obsotext")
    End Sub

   

   
    Protected Sub MessageBox1_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox1.GetMessageBoxResponse
        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
            Dim SerTypeEntity As New clsSerType()
            Dim SerTypeStat As New clsCommonClass()

            'SAVE
            If (Trim(SerTypeIDbox.Text) <> "") Then
                SerTypeEntity.SerTypeID = SerTypeIDbox.Text.ToUpper()
            End If
            If (Trim(SerTypeNamebox.Text) <> "") Then
                SerTypeEntity.SerTypeName = SerTypeNamebox.Text.ToUpper()
            End If
            If (Trim(ALterNamebox.Text) <> "") Then
                SerTypeEntity.AlternateName = ALterNamebox.Text.ToUpper()
            End If
            If (StatusDrop.SelectedItem.Value.ToString() <> "") Then
                SerTypeEntity.SerTypeStatus = StatusDrop.SelectedItem.Value.ToString()
            End If
            If (CountryIDDrop.SelectedItem.Value.ToString() <> "") Then
                SerTypeEntity.CountryID = CountryIDDrop.SelectedItem.Value.ToString()
            End If
            If (SerTypeDrop.SelectedItem.Value.ToString() <> "") Then
                SerTypeEntity.SerType = SerTypeDrop.SelectedItem.Value.ToString().ToUpper()
            End If
            If (ModelIDDrop.SelectedItem.Value.ToString() <> "") Then
                SerTypeEntity.ModelID = ModelIDDrop.SelectedItem.Value.ToString().ToUpper()
            End If
            If (ReReSlipDrop.SelectedItem.Value.ToString() <> "") Then
                SerTypeEntity.ReReSlip = ReReSlipDrop.SelectedItem.Value.ToString().ToUpper()
            End If

            If (FrequencyDrop.SelectedItem.Value.ToString() <> "") Then
                SerTypeEntity.Frequency = FrequencyDrop.SelectedItem.Value
            End If
            If (NoOfYearDrop.SelectedItem.Text <> "") Then
                SerTypeEntity.NoOfYears = Convert.ToInt32(NoOfYearDrop.SelectedItem.Text)
            End If
            If (Trim(PointCRbox.Text) <> "") Then
                SerTypeEntity.PointCR = PointCRbox.Text
            End If

            If (Trim(PointTebox.Text) <> "") Then
                SerTypeEntity.PointTe = PointTebox.Text
            End If
            If (RsdDrop.SelectedItem.Value.ToString() <> "") Then
                SerTypeEntity.ReStDate = RsdDrop.SelectedItem.Value.ToString().ToUpper()
            End If
            If (ulsdDrop.SelectedItem.Value.ToString() <> "") Then
                SerTypeEntity.UpLaSeDate = ulsdDrop.SelectedItem.Value.ToString().ToUpper()
            End If
            If (Trim(Request.Form("Obsotext")) <> "") Then
                Dim temparr As Array = Request.Form("Obsotext").Split("/") 'Obsotext.Text.Split("/")
                SerTypeEntity.ObsoleteDate = temparr(2) + "-" + temparr(1) + "-" + temparr(0)

            End If
            If (Trim(Request.Form("Efftext")) <> "") Then
                Dim temparr As Array = Request.Form("Efftext").Split("/") 'Efftext.Text.Split("/")
                SerTypeEntity.EffectiveDate = temparr(2) + "-" + temparr(1) + "-" + temparr(0)

            End If
            'If (Trim(CreatedBy.Text) <> "") Then
            '    SerTypeEntity.CreatedBy = CreatedBy.Text.ToUpper()
            'End If
            If (Trim(CreatedDate.Text) <> "") Then
                SerTypeEntity.CreatedDate = SerTypeStat.DatetoDatabase(CreatedDate.Text)
            End If
            'If (Trim(ModifiedBy.Text) <> "") Then
            '    SerTypeEntity.ModifiedBy = ModifiedBy.Text.ToUpper()
            'End If
            If (Trim(ModifiedDate.Text) <> "") Then
                SerTypeEntity.ModifiedDate = SerTypeStat.DatetoDatabase(ModifiedDate.Text)
            End If
            If (Trim(RMDATBOX.Text) <> "") Then
                SerTypeEntity.RMDAT = RMDATBOX.Text
            End If
            If (Trim(Me.CreatedBy.Text) <> "") Then
                SerTypeEntity.CreatedBy = Session("userID").ToString.ToUpper()
            End If
            If (Trim(Me.ModifiedBy.Text) <> "") Then
                SerTypeEntity.ModifiedBy = Session("userID").ToString.ToUpper()
            End If

            Dim dupCount As Integer = SerTypeEntity.GetDuplicatedSerTypeID()
            If dupCount.Equals(-1) Then
                Dim objXm As New clsXml
                objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                errlab.Text = objXm.GetLabelName("StatusMessage", "SEVTPDUP")
                errlab.Visible = True
            ElseIf dupCount.Equals(0) Then
                Dim insPRIDCnt As Integer = SerTypeEntity.Insert()
                If insPRIDCnt = 0 Then
                    Dim objXm As New clsXml
                    objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    errlab.Text = objXm.GetLabelName("StatusMessage", "SuccessfullySaved")
                    errlab.Visible = True
                Else

                    Response.Redirect("~/PresentationLayer/Error.aspx")

                End If

                If SerTypeDrop.SelectedValue = "CONTRACT" Then

                    Dim noYear As Integer = Nothing
                    Dim frequency As Integer = Nothing
                    If (NoOfYearDrop.SelectedItem.Text.ToString() <> "") And (FrequencyDrop.SelectedItem.Text.ToString() <> "") Then
                        noYear = Convert.ToInt16(NoOfYearDrop.SelectedItem.Text.ToString())
                        frequency = FrequencyDrop.SelectedItem.Text.ToString()
                    End If


                    Dim firstduring As Integer = noYear * 12 / frequency
                    SerTypeEntity.During = firstduring
                    SerTypeEntity.SerTypeID = SerTypeIDbox.Text
                    Dim dupCountdetail As Integer = SerTypeEntity.GetDuplicatedmonth()
                    If dupCount.Equals(-1) Then
                        Dim objXm2 As New clsXml
                        objXm2.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                        errlab.Text = objXm2.GetLabelName("StatusMessage", "DETAILERROR")
                        errlab.Visible = True

                    Else

                        Dim insDetailCnt As Integer = SerTypeEntity.InsertDetail(frequency, noYear)
                        If insDetailCnt = 0 Then
                            Dim objXm1 As New clsXml
                            objXm1.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                            errlab.Text = objXm1.GetLabelName("StatusMessage", "SuccessfullySaved")
                            errlab.Visible = True
                            savebutton.Enabled = False
                        Else
                            Response.Redirect("~/PresentationLayer/Error.aspx")
                        End If
                        Dim objXmlTr As New clsXml
                        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
                        SerTypeEntity.SerTypeID = SerTypeIDbox.Text

                        Dim SerTypeall As DataSet = SerTypeEntity.GetSerTypeID2()
                        duringView.DataSource = SerTypeall
                        duringView.DataBind()
                        If SerTypeall.Tables(0).Rows.Count <> 0 Then
                            duringView.HeaderRow.Cells(0).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSRCT-0020")
                            duringView.HeaderRow.Cells(0).Font.Size = 8
                        End If
                    End If
                End If
            Else

                Response.Redirect("~/PresentationLayer/Error.aspx")

            End If

        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
            Efftext.Text = Request.Form("Efftext")
            Obsotext.Text = Request.Form("Obsotext")
        End If

    End Sub

    Protected Sub CountryIDDrop_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CountryIDDrop.SelectedIndexChanged
        'display modelid
        Me.ModelIDDrop.Items.Clear()
        Dim rank As String = Session("login_rank").ToString().ToUpper()
        Dim modelid As New clsCommonClass
        If rank <> 0 Then
            modelid.spctr = CountryIDDrop.SelectedItem.Value.ToString()

        End If
        modelid.rank = rank
        Dim modelidds As New DataSet
        modelidds = modelid.Getcomidname("BB_MASMOTY_IDNAME ")
        If modelidds.Tables.Count <> 0 Then
            databonds(modelidds, Me.ModelIDDrop)
            Me.ModelIDDrop.Items.Insert(0, "")
        End If
        Efftext.Text = Request.Form("Efftext")
        Obsotext.Text = Request.Form("Obsotext")
    End Sub

   


  

   

    Protected Sub Calendareffe_SelectedDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendareffe.SelectedDateChanged
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Dim prcidDate As New clsCommonClass()
        Efftext.Text = Calendareffe.SelectedDate.Date.ToString().Substring(0, 10)
    End Sub

    Protected Sub Calendarobs_SelectedDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendarobs.SelectedDateChanged
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Dim prcidDate As New clsCommonClass()
        Obsotext.Text = Calendarobs.SelectedDate.Date.ToString().Substring(0, 10)
    End Sub

    Protected Sub Calendareffe_CalendarVisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendareffe.CalendarVisibleChanged
        StatusDrop.Visible = Not Calendareffe.CalendarVisible
        RsdDrop.Visible = Not Calendareffe.CalendarVisible
    End Sub
End Class