<%@ Page Language="VB" AutoEventWireup="false" CodeFile="modeltype.aspx.vb" Inherits="PresentationLayer_masterrecord_modeltype" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
 <title>Model Type - Search</title>
     <STYLE type="text/css">A:link { COLOR: #336666; TEXT-DECORATION: none }
	BODY { FONT-SIZE: 9pt; FONT-FAMILY: "Arial", "Verdana", "Tahoma"; BACKGROUND-COLOR: #ffffff }
	TD { FONT-SIZE: 9pt; FONT-FAMILY: Arial,Verdana,Tahoma }
	</STYLE>
	<LINK href="../css/style.css" type="text/css" rel="stylesheet">
</head>
<body>
    <form id="form1" runat="server">
    <TABLE border="0" id=TABLE2 width ="100%">
           <tr>
           <td style="height: 13px; width: 100%;">
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>
							<TD width="1%" background="../graph/title_bg.gif"><IMG height="24" src="../graph/title1.gif" width="5"></TD>
							<TD class="style2" width="98%" background="../graph/title_bg.gif">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></TD>
							<TD align="right" width="1%" background="../graph/title_bg.gif"><IMG height="24" src="../graph/title_2.gif" width="5"></TD>
						</TR>
			 </TABLE>
           </tr>
           <tr>
           <td style="width: 90%; height: 2px">
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>					
							<TD class="style2" width="98%" background="../graph/title_bg.gif" style="height: 1px">
                                <font color=red><asp:Label ID="addinfo" runat="server" Text="Label" Visible=false></asp:Label></font></TD>
						</TR>
			 </TABLE>
           </tr>			
				</table> 
				<div>
                    &nbsp;
        <asp:Label ID="ModelNameLab" runat="server" Text="Label"></asp:Label>
        &nbsp; &nbsp;<asp:TextBox ID="ModelNameBox" runat="server" CssClass="textborder" Width="15%" MaxLength="50"></asp:TextBox>&nbsp; <asp:Label ID="ModelIDLab" runat="server" Text="Label"></asp:Label>&nbsp;
        <asp:TextBox ID="ModelIDBox" runat="server" Width="15%" CssClass="textborder" MaxLength="10"></asp:TextBox>&nbsp; <asp:Label ID="ctryIDLab" runat="server" Text="Label"></asp:Label>&nbsp;<asp:DropDownList
            ID="ctryIDDD" runat="server" CssClass="textborder" Width="15%">
        </asp:DropDownList>&nbsp;<br />
        &nbsp; <asp:Label ID="ProductClassLab" runat="server" Text="Label"></asp:Label>&nbsp;
        <asp:DropDownList ID="ProductClassDDL" runat="server"  CssClass="textborder" Width="15%">
        </asp:DropDownList>&nbsp;&nbsp; &nbsp; &nbsp;
        <asp:Label ID="StatusLab" runat="server" Text="Label"></asp:Label>&nbsp;&nbsp;<asp:DropDownList ID="StatusLabDDL" runat="server" Font-Size="Small" CssClass="textborder" Width="15%">
        </asp:DropDownList>&nbsp;&nbsp; &nbsp;
        <asp:LinkButton ID="searchLink" runat="server">Search</asp:LinkButton>
        &nbsp; &nbsp; &nbsp; &nbsp; 
        <asp:LinkButton ID="LinkButton1" runat="server">LinkButton</asp:LinkButton>
        &nbsp; &nbsp;&nbsp;
        <asp:LinkButton ID="addLink" runat="server" PostBackUrl="~/PresentationLayer/masterrecord/addmodeltype.aspx">Label</asp:LinkButton><br />
        <hr />
    
    </div>
        <asp:GridView ID="modeltypeView" runat="server" Width="100%" AllowPaging="True" Font-Size="Smaller">
        </asp:GridView>
        <asp:Label ID="Label1" runat="server" ForeColor="Red" Height="62px" Text="Label"
            Width="638px"></asp:Label>
    </form>
</body>
</html>
