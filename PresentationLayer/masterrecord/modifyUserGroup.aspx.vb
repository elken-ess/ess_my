﻿Imports System.Configuration
Imports System.Globalization
Imports BusinessEntity
Imports System.Data

Partial Class PresentationLayer_masterrecord_Default
    Inherits System.Web.UI.Page

    Private bAdd As Boolean

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                If (Session("username").ToString() = Nothing Or Session("username").ToString() = "") Then
                    Response.Redirect("logon.aspx")
                End If
            Catch ex As Exception
                Response.Redirect("logon.aspx")
            End Try

            BindGrid()
        End If
    End Sub

    Protected Sub BindGrid()
        Dim dataStyle As CultureInfo = New CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = dataStyle
        Dim uagID As String = ""
        Dim uagStat As String = ""
        Dim flag As String = ""
        'Accept parameters        
        If (Request.Params.HasKeys()) Then
            uagID = Request.Params("uagID").ToString()
            uagStat = Request.Params("uagStat").ToString()
            flag = Request.Params("flag").ToString()
        End If
        'if bAdd is True then Add User Access Group else Modify User Access Group
        bAdd = Not (uagID.Length > 0)
        Session.Contents("bAdd") = bAdd
        'Display the Label or Button Text=================================================
        Dim objXML As New clsXml
        objXML.XmlFile = ConfigurationManager.AppSettings("XmlFilePath")
        If (bAdd) Then
            uagTitleLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_MASMUAG_TITLE_ADD")
        Else
            uagTitleLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_MASMUAG_TITLE_EDIT")
        End If

        uagIDLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_MASMUAG_UGI")
        uagNameLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_MASMUAG_GN")
        uagAlterNameLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_MASMUAG_AGN")
        uagStatusLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_MASMUAG_STATUS")
        uagGreatedByLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_MASMUAG_CREBY")
        uagCreatedDateLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_MASMUAG_CREDT")
        uagModifiedByLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_MASMUAG_LUSER")
        uagModifiedDateLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_MASMUAG_LSTDT")
        saveButton.Text = objXML.GetLabelName("EngLabelMsg", "BB_MASMUAG_SAVE")
        cancelLink.Text = objXML.GetLabelName("EngLabelMsg", "BB_MASMUAG_CANCEL")
        lblNoteUP.Text = objXML.GetLabelName("EngLabelMsg", "BB-RFV")
        lblNoteDown.Text = objXML.GetLabelName("EngLabelMsg", "BB-RFV")

        'Get Error Message
        objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
        uagIDError.ErrorMessage = objXML.GetLabelName("StatusMessage", "BB_MASMUAG_IDEMPTY")
        uagNameError.ErrorMessage = objXML.GetLabelName("StatusMessage", "BB_MASMUAG_NAMEEMPTY")

        'Create the DropDownList Items========================
        Dim userGroupStat As New clsUtil()
        Dim statParam As ArrayList = New ArrayList
        statParam = userGroupStat.searchconfirminf("STATUS")
        Dim count As Integer
        Dim statid As String
        Dim statnm As String
        For count = 0 To statParam.Count - 1
            statid = statParam.Item(count)
            statnm = statParam.Item(count + 1)
            count = count + 1
            If Not statid.Equals("DELETE") Then
                uagStatusDrop.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
            End If
        Next

        If (bAdd) Then
            'Display as Add User Access Group 
            uagCreatedByBox.Text = Session("username")
            uagCreatedDateBox.Text = Format(Date.Today, "dd/MM/yyyy")
            uagModifiedByBox.Text = Session("username")
            uagModifiedDateBox.Text = uagCreatedDateBox.Text
            uagStatusDrop.Items(0).Selected = True
        Else
            'Display as Edit User Access Group
            Dim userGroupEntity As New clsUserAccessGroup()
            userGroupEntity.GroupID = Trim(uagID)
            Dim uagItem As DataSet = userGroupEntity.SelectedByID()
            uagIDBox.Text = uagID
            uagNameBox.Text = uagItem.Tables(0).Rows(0).Item(1).ToString
            uagAlterNameBox.Text = uagItem.Tables(0).Rows(0).Item(2).ToString
            For count = 0 To statParam.Count - 1
                If statParam.Item(count).Equals(uagItem.Tables(0).Rows(0).Item(3).ToString) Then
                    uagStatusDrop.Items(count / 2).Selected = True
                End If
                count = count + 1
            Next
            uagCreatedByBox.Text = uagItem.Tables(0).Rows(0).Item(4).ToString
            uagCreatedDateBox.Text = Format(uagItem.Tables(0).Rows(0).Item(5), "dd/MM/yyyy")
            uagModifiedByBox.Text = Session("username")
            uagModifiedDateBox.Text = Format(Date.Today, "dd/MM/yyyy")
            uagIDBox.ReadOnly = True
        End If
    End Sub

    Protected Sub saveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveButton.Click
        Dim userGroupEntity As New clsUserAccessGroup()
        Dim objXML As New clsXml
        objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
        userGroupEntity.GroupID = uagIDBox.Text
        userGroupEntity.GroupName = uagNameBox.Text
        userGroupEntity.AlterGroupName = uagAlterNameBox.Text
        userGroupEntity.GroupStatus = uagStatusDrop.SelectedItem.Value.ToString
        userGroupEntity.ModifiedBy = uagModifiedByBox.Text
        userGroupEntity.ModifiedDate = uagModifiedDateBox.Text

        Dim iCount As Integer = 0
        If (Session.Contents("bAdd")) Then
            'Add User Access Group
            userGroupEntity.CreatedBy = uagCreatedByBox.Text
            userGroupEntity.CreatedDate = uagCreatedDateBox.Text

            iCount = userGroupEntity.SelectedByDup()
            If (iCount > 0) Then
                uagErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_MASMUAG_DUP")
                uagErrLab.Visible = True
                Return
            Else
                iCount = userGroupEntity.AddUserGroup()
            End If
        Else
            'Update User Access Group
            iCount = userGroupEntity.SelectedByName()
            If (iCount > 0) Then
                uagErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_MASMUAG_DUP")
                uagErrLab.Visible = True
                Return
            Else
                iCount = userGroupEntity.UpdatedUserGroup()
            End If
        End If

        If (iCount > 0) Then
            uagErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_MASMUAG_SUCCESS")
        Else
            uagErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_MASMUAG_FAILED")
        End If
        uagErrLab.Visible = True
    End Sub
End Class
