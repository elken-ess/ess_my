<%@ Page Language="VB" AutoEventWireup="false" EnableEventValidation="false"CodeFile="addcustomer.aspx.vb" Inherits="PresentationLayer_masterrecord_Addcustomer" %>

<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Add Customer</title>
    <LINK href="../css/style.css" type="text/css" rel="stylesheet">
    <script src="..\js\AjaxVariables.js"></script>
	<script src="..\js\AjaxScript.js"></script>
     <STYLE type="text/css">A:link { COLOR: #336666; TEXT-DECORATION: none }
	BODY { FONT-SIZE: 9pt; FONT-FAMILY: "Arial", "Verdana", "Tahoma"; BACKGROUND-COLOR: #ffffff }
	TD { FONT-SIZE: 9pt; FONT-FAMILY: Arial,Verdana,Tahoma }
	</STYLE>
	<script language="javascript">
	   
 
 function LoadAreas_CallBack(response){
 
     //if the server-side code threw an exception
     if (response.error != null)
     {
      //we should probably do better than this
      alert(response.error); 
      
      return;
     }

     var ResponseValue = response.value;
     
   
     //if the response wasn't what we expected  
     if (ResponseValue == null || typeof(ResponseValue) != "object")
     {       
      return;
     }
          
     //Get the states drop down
     var ResultList = document.getElementById("<%=areaid.ClientID%>");
     ResultList.options.length = 0; //reset the states dropdown
 
    
     //Remember, its length not Length in JavaScript
     for (var i = 0; i < ResponseValue.length; ++i)
     {
     
      //the columns of our rows are exposed like named properties
      var al = ResponseValue[i].split(":");
      var alid = al[0];
      var alname = al[1];
      ResultList.options[ResultList.options.length] =  new Option(alname, alid);
     }
     
     
}
 
 
 function LoadAreas(objectClient)
{
 
 var stateid = objectClient.options[objectClient.selectedIndex].value;
 
 if (objectClient.selectedIndex > 0){
   var countryid = document.getElementById("<%=ctrid.ClientID%>").options[document.getElementById("<%=ctrid.ClientID%>").selectedIndex].value;
 PresentationLayer_masterrecord_Addcustomer.GetAreas( countryid, stateid, LoadAreas_CallBack);
 }
 else
 {
    document.getElementById("<%=areaid.ClientID%>").options.length = 0
 }
}


function LoadRaces_CallBack(response){
 

     //if the server-side code threw an exception
     if (response.error != null)
     {
      //we should probably do better than this
      alert(response.error); 
      
      return;
     }

     var ResponseValue = response.value;
     
   
     //if the response wasn't what we expected  
     if (ResponseValue == null || typeof(ResponseValue) != "object")
     {       
      return;
     }
          
     //Get the states drop down
     var ResultList = document.getElementById("<%=Races.ClientID%>");
     ResultList.options.length = 0; //reset the states dropdown
 
    
     //Remember, its length not Length in JavaScript
     for (var i = 0; i < ResponseValue.length; ++i)
     {
     
      //the columns of our rows are exposed like named properties
      var al = ResponseValue[i].split(":");
      var alid = al[0];
      var alname = al[1];
      ResultList.options[ResultList.options.length] =  new Option(alname, alid);
     }
     
}
 
 
 function LoadRaces(ObjectClient)
{
 
 var ObjectId = ObjectClient.options[ObjectClient.selectedIndex].value;
 
// if (ObjectClient.selectedIndex > 0){   
 PresentationLayer_masterrecord_Addcustomer.GetRaces(ObjectId, LoadRaces_CallBack);
// }
// else
// {
//    document.getElementById("<%=Races.ClientID%>").options.length = 0
// }
}

</script>



</head>
<body>
    <form id="form1" runat="server">
    <div style="width: 100%">
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>
							<TD width="1%" background="../graph/title_bg.gif"><IMG height="24" src="../graph/title1.gif" width="5"></TD>
							<TD class="style2" background="../graph/title_bg.gif" style="width: 100%">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></TD>
							<TD align="right" background="../graph/title_bg.gif" style="width: 1%"><IMG height="24" src="../graph/title_2.gif" width="5"></TD>
						</TR>
			 </TABLE>
         <tr>
           <td>
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>					
							<TD class="style2" width="98%" background="../graph/title_bg.gif">
                                <font color=red><asp:Label ID="errlab" runat="server" Text="Label" Visible=false></asp:Label>
                                </font>
                                </TD>
						</TR>
			 </TABLE>
           </tr>
			<TR>
				<TD>
        <table id="Table3" border="0">
            <tr>
                    <td></td>
                
                <td style="width: 100%">
                    <table id="Table2" bgcolor="#b7e6e6" border="0" cellpadding="2" cellspacing="1" style="width: 100%">
                         <tr bgcolor="#ffffff">
<td colspan = 4>
                            <asp:Label ID="lblCompTop" runat="server" ForeColor="Red" Text="Label" Visible="True"></asp:Label>
</td>
             </tr>
             <tr bgcolor="#ffffff">
<td colspan = 4>
                        	&nbsp;
                     	</td>
               </tr>    
                        <tr bgcolor="#ffffff">
                       
                            <td align="right" style="width: 15%; height: 11px;" width="15%">
                                <asp:Label ID="CustomerNamelab" runat="server" Text="Label"></asp:Label>
                            </td>
                            <td align="right" colspan="3" style="text-align: left; height: 11px; color: red; ">
                                <asp:TextBox ID="CustomerName" runat="server" CssClass="textborder" Width="97.4%" MaxLength="50"></asp:TextBox><strong>*</strong></td>
                         </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%; height: 19px;" width="15%">
                                <asp:Label ID="CustomerCtrLbl" runat="server" Text="Country ID"></asp:Label></td>
                            <td align="left" style="width: 35%; color: red; height: 19px;">
                                            <asp:TextBox ID="CustomerCtryID" runat="server" CssClass="textborder" MaxLength="50" Width="94%" ReadOnly="True"></asp:TextBox>
                                <strong>*</strong></td>
                            <td align="right" style="height: 19px; width: 15%;">
                                <asp:Label ID="AlternateNameLab" runat="server" Text="Label"></asp:Label>
                            </td>
                            <td align="left" style="width: 35%; height: 19px">
                                <font color="red" style="width: 35%">
                                <asp:TextBox ID="AlternateName" runat="server" CssClass="textborder" Width="94%" MaxLength="50"></asp:TextBox></font></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" width="15%" style="width: 15%">
                                <asp:Label ID="custidlab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%">
                                <asp:TextBox ID="custid" runat="server" CssClass="textborder" MaxLength="2" Width="94%" Enabled="False"></asp:TextBox>
                                <strong><!-- commented by Raymond <span style="color: #ff0000">*</span>--></strong></td>
                            <td align="right" style="width: 15%">
                                <asp:Label ID="JDECIDLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%">
                                <asp:TextBox ID="JDECID" runat="server" CssClass="textborder" Width="94%" Enabled="False"></asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" width="15%" style="width: 15%; height: 24px;">
                                <asp:Label ID="RacesLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%; height: 24px;">
                                <asp:DropDownList ID="Races" runat="server" CssClass="textborder" Width="94%">
                                </asp:DropDownList></td>
                            <td align="right" style="height: 24px; width: 15%;">
                                &nbsp;<asp:Label ID="CustomerTypeLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%; height: 24px;">
                                <asp:DropDownList ID="CustomerType" runat="server" CssClass="textborder" Width="96%" AutoPostBack="false" onchange="LoadRaces(this)">
                                </asp:DropDownList></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" width="15%" style="width: 15%; height: 1px;">
                                <asp:Label ID="Label12" runat="server" Text="Preferred Language"></asp:Label></td>
                            <td align="left" style="width: 25%; height: 1px;">
                               <asp:DropDownList ID="prefLang" runat="server" AutoPostBack="False" CssClass="textborder" Width="94%">
                                        </asp:DropDownList></td>
                            <td align="right" style="height: 1px; width: 15%;">
                                <asp:Label ID="CustomerEmailLbl" runat="server" Text="Email Address 1"></asp:Label></td>
                            <td align="left" style="width: 35%; height: 1px;">
                                <asp:TextBox ID="CustomerEmail" runat="server" CssClass="textborder" MaxLength="50"
                                    Width="94%"></asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" width="15%" style="width: 15%; height: 1px;">
                                <asp:Label ID="modelLbl" runat="server" Text="Model / Serial No"></asp:Label></td>
                            <td align="left" style="width: 25%; height: 1px;">
                                <asp:TextBox ID="TextBox12" runat="server" CssClass="textborder" MaxLength="50"
                                    Width="94%" ReadOnly="True"></asp:TextBox></td>
                            <td align="right" style="height: 1px; width: 15%;"></td>
                            <td align="left" style="width: 35%; height: 1px;"></td>
                        </tr>
                        
                        <tr bgcolor="#ffffff">
                            <td align="left" colspan="4">
                                <hr />
                                <table bgcolor="#b7e6e6" border="0" cellpadding="2" cellspacing="1" width="100%">
                                    <tr width="100%">
                                        <td background="../graph/title_bg.gif" style="height: 14px" colspan="6">
                                            <font color="red">
                                            <asp:Label ID="lblDistributor" runat="server" Text="Distributor Details" ForeColor="Black"></asp:Label>
                                            </font>
                                        </td>
                                    </tr>
                                    <tr width="100%" bgcolor="#ffffff">
                                        <td style="width: 6%; text-align: right">
                                            <asp:Label ID="DistNameLbl" runat="server" Text="Distributor Name"></asp:Label></td>
                                        <td style="width: 16%">
                                            <asp:TextBox ID="DistName"  runat="server" CssClass="textborder" MaxLength="50" Width="94%"></asp:TextBox></td>
                                        <td style="width: 7%; text-align: right">
                                            <asp:Label ID="DistContactLbl" runat="server" Text="Distributor Contact"></asp:Label></td>
                                        <td width="15%">
                                            <asp:TextBox ID="tele1" runat="server" CssClass="textborder" MaxLength="50" Width="94%"></asp:TextBox></td>
                                    </tr>
                                    <tr width="100%" bgcolor="#ffffff">
                                        <td style="width: 6%; text-align: right">
                                            </td>
                                        <td style="width: 16%">
                                            </td>
                                        <td style="width: 7%; text-align: right">
                                            <asp:Label ID="DistEmailLbl" runat="server" Text="Email Address 2"></asp:Label></td>
                                        <td width="15%">
                                            <asp:TextBox ID="DistEmail" runat="server" CssClass="textborder" MaxLength="50" Width="94%"></asp:TextBox></td>
                                    </tr>
                                </table>
                                <hr />
                                <table bgcolor="#b7e6e6" border="0" cellpadding="2" cellspacing="1" width="100%">
                                    <tr>
                                        <td background="../graph/title_bg.gif" style="height: 14px" colspan="6"  id="#ctry">
                                            <font color="red">
                                            <asp:Label ID="adresstitlLabel" runat="server" Text="Label" ForeColor="Black"></asp:Label>
                                            </font></td>
                                    </tr>
                                    
                                    <tr width="100%" bgcolor="#ffffff">
                                        <td style="text-align: right; width: 6%;">
                                            <asp:Label ID="Label1" runat="server" Text="Primary Address 1"></asp:Label></td>
                                        <td width="15%" colspan="5">
                                            <asp:TextBox ID="Address1" runat="server" CssClass="textborder" MaxLength="50" Width="97%"></asp:TextBox>
                                            <strong><span style="color: #ff0000">*</span></strong></td>
                                    </tr>
                                    <tr width="100%" bgcolor="#ffffff">
                                        <td style="text-align: right; width: 6%; height: 18px;">
                                            <asp:Label ID="Label2" runat="server" Text="Address 2"></asp:Label></td>
                                        <td width="15%" colspan="5" style="height: 18px">
                                            <asp:TextBox ID="Address2" runat="server" CssClass="textborder" MaxLength="50" Width="97%"></asp:TextBox>
                                            <strong><span style="color: #ff0000">*</span></strong></td>
                                    </tr>
                                    <tr width="100%" bgcolor="#ffffff">
                                        <td style="text-align: right; width: 6%;">
                                            <asp:Label ID="Label3" runat="server" Text="PO Code"></asp:Label></td>
                                        <td style="width: 15%">
                                            <asp:TextBox ID="POCode" runat="server" CausesValidation =true  CssClass="textborder" MaxLength="50" Width="90%">
                                            </asp:TextBox>
                                            <strong><span style="color: #ff0000">*</span></strong></td>
                                        <td style="text-align: right; width: 7%;">
                                <asp:Label ID="SvcidLab" runat="server" Text="Label"></asp:Label></td>
                                        <td style="width: 13%"><asp:DropDownList ID="SVCID" runat="server" CssClass="textborder" Width="90%">
                                </asp:DropDownList>
                                            <strong><span style="color: #ff0000">*</span></strong></td>
                                    </tr>
                                    <tr width="100%" bgcolor="#ffffff">
                                        <td style="text-align: right; width: 6%;">
                                            <asp:Label ID="Label5" runat="server" Text="Country"></asp:Label>
                                        </td>
                                        <td style="width: 16%"><strong><span style="color: #ff0000">
                                        <asp:DropDownList ID="ctrid" runat="server" AutoPostBack="false"  CssClass="textborder" 
                                            Width="90%">
                                        </asp:DropDownList></span></strong>&nbsp;<strong><span style="color: #ff0000">*</span></strong></td>
                                        
                                       
                                             <td style="text-align: right; width: 6%;">
                                            <asp:Label ID="Label4" runat="server" Text="State"></asp:Label></td>
                                        <td style="width: 16%">
                                            <asp:DropDownList ID="staid" runat="server" AutoPostBack="False" CssClass="textborder" onchange="LoadAreas(this)"
                                                Width="90%">
                                            </asp:DropDownList>
                                            <strong><span style="color: #ff0000">*</span></strong></td>
                                        
                                    </tr>
                                    <tr width="100%" bgcolor="#ffffff">
                                    
                                     <td style="width: 7%; text-align: right">
                                            <asp:Label ID="Label6" runat="server" Text="Area"></asp:Label></td>
                                        <td style="width: 13%">
                                            <asp:DropDownList ID="areaid" runat="server" CssClass="textborder" Width="90%">
                                            </asp:DropDownList>
                                            <strong><span style="color: #ff0000">*</span></strong></td>
                                            
                                            
                                       
                                        <td style="width: 7%"></td>
                                        <td style="width: 13%">
                                            <asp:LinkButton ID="linkAddAddress" runat="server" Enabled="False" Visible =false >Add Other Address</asp:LinkButton></td>
                                    </tr>
                                </table>
                                <hr />
                                <asp:GridView ID="addressView" runat="server" AllowPaging="True" AllowSorting="True"
                                    Font-Size="Smaller" PageSize="5" Width="100%" Visible = false >
                                </asp:GridView>
                                <table bgcolor="#b7e6e6" border="0" cellpadding="2" cellspacing="1" width="100%">
                                    <tr>
                                        <td background="../graph/title_bg.gif" style="height: 20px;" colspan="6">
                                            <asp:Label ID="teltitllab" runat="server" Text="Label"></asp:Label>
                                            </td>                              
                                    </tr>
                                    <tr width="100%" bgcolor="#ffffff">
                                        <td style="width: 7%; text-align: right;">
                                            <asp:Label ID="Label8" runat="server" Text="Telephone (H)"></asp:Label></td>
                                        <td style="width: 18%">
                                            <asp:TextBox ID="tele2" runat="server" CssClass="textborder" MaxLength="30" Width="44%"></asp:TextBox>
                                            <asp:TextBox ID="txtRemark2" runat="server" CssClass="textborder" MaxLength="50" Width="46%"></asp:TextBox>
                                            
                                            </td>
                                        <td style="width: 8%; text-align: right;">
                                            <asp:Label ID="Label7" runat="server" Text="Mobile 1"></asp:Label></td>
                                        <td style="width: 17%">
                                            <asp:TextBox ID="tele5" runat="server" CssClass="textborder" MaxLength="30" Width="44%"></asp:TextBox>
                                            <asp:TextBox ID="txtRemark5" runat="server" CssClass="textborder" MaxLength="50" Width="46%"></asp:TextBox>
                                            </td>
                                    </tr>
                                    <tr width="100%" bgcolor="#ffffff">
                                        <td style="width: 7%; text-align: right;">
                                            <asp:Label ID="Label9" runat="server" Text="Telephone (O)"></asp:Label></td>
                                        <td style="width: 18%">
                                            <asp:TextBox ID="tele3" runat="server" CssClass="textborder" MaxLength="30" Width="44%"></asp:TextBox>
                                            <asp:TextBox ID="txtRemark3" runat="server" CssClass="textborder" MaxLength="50" Width="46%"></asp:TextBox>
                                            
                                            </td>
                                        <td style="width: 8%; text-align: right;">
                                            <asp:Label ID="Label10" runat="server" Text="Mobile 2"></asp:Label></td>
                                        <td style="width: 17%">
                                            <asp:TextBox ID="tele6" runat="server" CssClass="textborder" MaxLength="30" Width="44%"></asp:TextBox>
                                            <asp:TextBox ID="txtRemark6" runat="server" CssClass="textborder" MaxLength="50" Width="46%"></asp:TextBox>
                                            
                                            </td>
                                    </tr>
                                    <tr width="100%" bgcolor="#ffffff">
                                        <td style="width: 7%; text-align: right;">
                                            <asp:Label ID="Label11" runat="server" Text="Fax"></asp:Label></td>
                                        <td style="width: 18%">
                                            <asp:TextBox ID="tele4" runat="server" CssClass="textborder" MaxLength="30" Width="44%"></asp:TextBox>
                                            <asp:TextBox ID="txtRemark4" runat="server" CssClass="textborder" MaxLength="50" Width="46%"></asp:TextBox>
                                            
                                            </td>
                                        <td style="width: 8%; text-align: right;">
                                            </td>
                                        <td style="width: 17%"><asp:LinkButton ID="LinkButton2" runat="server" Enabled="False" CausesValidation=false Visible =false >Add Other Contacts</asp:LinkButton></td>
                                    </tr>
                                    
                                </table><hr />
                                <asp:GridView ID="contactView" runat="server" AllowPaging="True" AllowSorting="True"
                                    Font-Size="Smaller" PageSize="5" Width="100%" Visible = false >
                                </asp:GridView><table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td background="../graph/title_bg.gif" style="height: 20px; width: 1%;">
                                        </td>
                                        <td background="../graph/title_bg.gif" class="style2" style="height: 20px; width: 98%;">
                                            <asp:Label ID="roLab" runat="server" Text="Label"></asp:Label>
                                        </td>
                                        <td align="right" background="../graph/title_bg.gif" style="height: 20px; width: 1%;">
                                        </td>
                                    </tr>
                                </table>
                                <hr />
                                <asp:GridView ID="roView" runat="server" AllowPaging="false" AllowSorting="True"
                                    Font-Size="Smaller" PageSize="50" Width="100%">
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" width="15%" style="width: 15%">
                                <asp:Label ID="creatby" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%">
                                <asp:TextBox ID="creatbybox" runat="server" CssClass="textborder" ReadOnly="true"></asp:TextBox></td>
                            <td align="right" style="width: 15%">
                                <asp:Label ID="creatdate" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" colspan="1" rowspan="1" style="width: 35%">
                                <asp:TextBox ID="creatdtbox" runat="server" CssClass="textborder" ReadOnly="true" Width="160px"></asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%;" width="15%">
                                <asp:Label ID="modfyby" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%;">
                                <asp:TextBox ID="modfybybox" runat="server" CssClass="textborder" ReadOnly="true"></asp:TextBox></td>
                            <td align="right" style="width: 15%;">
                                <asp:Label ID="mdfydate" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" colspan="1" rowspan="1" style="width: 35%;">
                                <asp:TextBox ID="mdfydtbox" runat="server" CssClass="textborder" ReadOnly="true" Width="160px"></asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%;" width="15%">
                            </td>
                            <td align="right" style="width: 15%;" width="15%">
                            </td>
                            <td align="right" style="width: 15%;">
                                <asp:Label ID="statuslab" runat="server" Text="Label"></asp:Label></td>
                            <td align="right" style="width: 15%; text-align: left;" width="15%">
                                <asp:DropDownList ID="status" runat="server" CssClass="textborder" Width="103px">
                            </asp:DropDownList></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%; height: 91px;" width="15%">
                                <asp:Label ID="noteslab" runat="server" Text="Label"></asp:Label></td>
                            <td align="right" colspan="4" style="height: 91px; text-align: left">
                                <asp:TextBox ID="notes" runat="server" CssClass="textborder" Width="96%" MaxLength="400" Height="77px" TextMode="MultiLine"></asp:TextBox></td>
                        </tr>
                       <tr bgcolor="#ffffff">
<td colspan = 4>
                        	&nbsp;
                     	</td>
               </tr>      
                         <tr bgcolor="#ffffff">
<td colspan = 4>
                            <asp:Label ID="lblCompBottom" runat="server" ForeColor="Red" Text="Label" Visible="True"></asp:Label>
</td>
             </tr>
                        <tr bgcolor="#ffffff">
<td colspan = 4>
                        	&nbsp;
                     	</td>
               </tr>        
                    </table>
                                        &nbsp;&nbsp;<br />
                    <asp:LinkButton ID="lblCreateApp" runat="server" Font-Bold="True" Font-Underline="True"
                        ForeColor="#000099" Enabled="False">Create Appointment </asp:LinkButton>
                    &nbsp; &nbsp;
                    <asp:LinkButton ID="lblViewInst" runat="server" Font-Bold="True" Font-Underline="True"
                        ForeColor="#000099" Enabled="False" Visible="False">View Install Base</asp:LinkButton>
                    &nbsp; &nbsp; &nbsp;<asp:LinkButton ID="HyperLink2" runat="server" ForeColor="DarkBlue" Enabled="False" Font-Bold="True" Font-Underline="True">LinkButton</asp:LinkButton>
                    &nbsp; &nbsp;&nbsp;
                    <asp:LinkButton ID="saveButton" runat="server" Font-Bold="True" Font-Underline="True" ForeColor="#000099"></asp:LinkButton>
                    &nbsp; &nbsp; &nbsp; &nbsp; <asp:HyperLink ID="cancelLink" runat="server" NavigateUrl="~/PresentationLayer/masterrecord/CustomerRecord.aspx" Font-Bold="True" Font-Underline="True" ForeColor="#000099">[cancelLink]</asp:HyperLink>
                    &nbsp; &nbsp; &nbsp;
                    <asp:HyperLink ID="AddLink" runat="server" >[Add]</asp:HyperLink>
                     &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                    &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                    &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                </td>
            </tr>
        </table>
                    <asp:RequiredFieldValidator ID="custnamemsg" runat="server" ControlToValidate="CustomerName"
                        ForeColor="White">*</asp:RequiredFieldValidator>
        &nbsp;&nbsp;
                    <cc1:messagebox id="MessageBox1" runat="server"></cc1:messagebox>
        <asp:TextBox ID="CustomerPrefix" runat="server" CssClass="textborder" Enabled="False"
                                    MaxLength="2" Visible="False" Width="2%"></asp:TextBox>
                    <asp:Label ID="CustomerPrefixLab" runat="server" Text="Label" Visible="False"></asp:Label>&nbsp;
                    <asp:RequiredFieldValidator ID="svcidmsg" runat="server" ControlToValidate="SVCID"
                        ForeColor="WhiteSmoke">*</asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator ID="Racesmsg" runat="server" ControlToValidate="Races"
                        ForeColor="WhiteSmoke">*</asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator ID="CustomerTypemsg" runat="server" ControlToValidate="CustomerType"
                        ForeColor="WhiteSmoke">*</asp:RequiredFieldValidator>
                    <cc1:MessageBox ID="MessageboxSaveAdd" runat="server" />
                    <asp:RequiredFieldValidator ID="AdresRequired2" runat="server" ControlToValidate="Address2"
                        ForeColor="WhiteSmoke">*</asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator ID="AdresRequired1" runat="server" ControlToValidate="Address1"
                        ForeColor="WhiteSmoke">*</asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator ID="StaIDValid" runat="server" ControlToValidate="staid"
                        ForeColor="WhiteSmoke">*</asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator ID="rfvAREAID" runat="server" ControlToValidate="areaid"
                        ForeColor="WhiteSmoke">*</asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator ID="rfvCTRID" runat="server" ControlToValidate="ctrid"
                        ForeColor="WhiteSmoke">*</asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator ID="frvPostCode" runat="server" ControlToValidate="POCode"
                        ForeColor="WhiteSmoke">*</asp:RequiredFieldValidator>
                    <cc1:MessageBox ID="MessageboxSaveCon" runat="server" />
                    <br />
        <br />
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                        ShowSummary="False" />
        <br />
        <br />
        <br />
    
    </div>
    </form>
</body>
</html>
