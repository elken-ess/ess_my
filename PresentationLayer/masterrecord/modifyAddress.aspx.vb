Imports BusinessEntity
Imports System.Configuration
Imports System.Xml
Imports System.Data
Imports System.Data.SqlClient
Partial Class PresentationLayer_masterrecord_modifyAddress
    Inherits System.Web.UI.Page
    Shared addtypesybom As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try

            Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "04")
            If purviewArray(2) = False Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return

            End If
            Me.LinkButton1.Enabled = purviewArray(1)
            Dim mdtype As String
            'Dim mdfystat As String = Request.Params("addressstat").ToString()
            Dim mdfyid As String
            Dim mdfypref As String
            '''''''''''''''''''''''''''''''''''''''''''''''''''''
            Try
                mdtype = Request.Params("addresstype").ToString()
                mdfyid = Request.QueryString("customid")
                mdfypref = Request.QueryString("Prefix")
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Me.custid.Text = Request.QueryString("customid")
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            custidlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0001")
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "MODADDRESS")
            Me.AddressTypelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0016")
            Me.Address1lab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0006")
            Me.Address2lab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0007")
            'Me.ROUserIDlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0017")
            Me.PIClab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0003")
            POCodelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0008")
            AreaIDlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0009")
            CountryIDlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCTRY-0001")
            StateIDlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0011")
            Me.rouidLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0007")
            Me.rotypelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0006")
            Me.statusLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCTRY-0004")
            Label1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            Label2.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")

            CreatedBylab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATBY")
            CreatedDatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
            ModiBylab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFBY")
            ModifiedDatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFDATE")
            LinkButton1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Save")
            HyperLink1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Cancel")
            AddressType.Enabled = False

            Dim statXmlTr As New clsXml
            statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            'Me.POCodeMsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-POCode")
            Me.POCodeMsg1.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-POCodein")
            Me.address1msg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Address1")
            Me.address2msg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Address2")
            Me.countrymsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "MAS-COUNTRY")
            Me.statmsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "MAS-STAT")
            Me.areamsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "MAS-AREA")
            Me.countrymsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "MAS-COUNTRY")
            Me.statmsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "MAS-STAT")
            Me.areamsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "MAS-AREA")
            Me.AddressTypemsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "ADDTYPE")
            'Dim strPanm As String = statXmlTr.GetLabelID("StatusMessage", mdfystat)
            Dim styPanm As String = statXmlTr.GetLabelID("StatusMessage", mdtype)
            Dim rank As String = Session("login_rank")
            Dim userIDNamestr As String = Session("userID").ToString().ToUpper + "-" + Session("username").ToString.ToUpper
            If mdfyid.Trim() <> "" Then
                mdfypref = Request.QueryString("Prefix")
                Dim rotype As New clsCustomerRecord()
                'If (Trim(Me.custid.Text) <> "") Then
                rotype.CustomerID = mdfyid
                rotype.Customerpf = mdfypref
                'End If
                Dim rotypeds As New DataSet

                'rotypeds = rotype.GetROidname("BB_MASROUM_MODSER")
                'If rotypeds.Tables(0).Rows.Count <> 0 Then
                '    databondros(rotypeds, Me.rotype)
                'End If
                Me.rotype.Items.Insert(0, "")
                ''databondros(rotypeds, Me.rotype)
                ''Me.rotype.Items.Insert(0, address)
                'Dim rotypestr As String
                'rotypestr = Me.rotype.Text
                'Dim ro As Array
                'ro = rotypestr.Split("-")
                'If ro.Length > 1 Then
                '    Dim roid As New clsAddress()
                '    roid.ModelType = ro(0)
                '    roid.SerialNo = ro(1)
                '    Dim rods As New DataSet
                '    rods = roid.GetROID()
                '    'Me.rouid.Text = rods.Tables(0).Rows(0).Item(0).ToString()
                'End If


                Dim addressEntity As New clsAddress()
                addressEntity.CustomerID = Trim(mdfyid)
                addressEntity.CustPrefix = Trim(mdfypref)
                addressEntity.AddressType = Trim(mdtype)
                addressEntity.Status = Trim(mdfypref)

                Dim edtReader As SqlDataReader = addressEntity.GetaddressDetailsByID()
                If edtReader.Read() = True Then

                    Dim addtype As New clsrlconfirminf()
                    Dim addParam As ArrayList = New ArrayList
                    addParam = addtype.searchconfirminf("ADDRESSTY")
                    Dim addcount As Integer
                    Dim addid As String
                    Dim addnm As String
                    For addcount = 0 To addParam.Count - 1
                        addid = addParam.Item(addcount)
                        addnm = addParam.Item(addcount + 1)
                        Me.AddressType.Items.Add(New ListItem(addnm.ToString(), addid.ToString()))
                        If Me.AddressType.Equals(edtReader.GetValue(0).ToString()) Then
                            Me.AddressType.Items(addcount / 2).Selected = True
                        End If
                        Me.AddressType.Text = edtReader.GetValue(0).ToString()
                        addcount = addcount + 1

                    Next

                    'added by deyb 19-07-2006
                    Dim xAddType As String = CStr(Request.Params("addresstype"))
                    AddressType.SelectedItem.Text = xAddType
                    '------------------------------

                    addressEntity.CustPrefix = edtReader.GetValue(1).ToString()
                    Me.custid.Text = edtReader.GetValue(2).ToString()
                    Me.custid.ReadOnly = True
                    Me.rouid.Text = edtReader.GetValue(3).ToString()
                    Dim rocontact As New clsCustomerRecord()
                    If (Trim(Me.rouid.Text) <> "") Then
                        rocontact.RoSerialNo = rouid.Text
                    End If
                    'If (Trim(Me.rouid.Text) <> "") Then
                    '    rocontact.RoSerialNo = rouid.Text
                    'End If
                    'Dim rocon As DataSet
                    'rocon = rocontact.GetROidnamebyid("BB_MASROUM_MODSERBYROID")
                    'Dim row As DataRow
                    'Dim NewItem As New ListItem()
                    'For Each row In rocon.Tables(0).Rows

                    '    'NewItem.Text = row("id") & "-" & row("name")

                    'Next
                    'If row.Table.Rows.Count <> 0 Then
                    'Me.rotype.Text = NewItem.Text
                    'Else

                    'End If

                    Me.PIC.Text = edtReader.GetValue(4).ToString()
                    Me.Address1.Text = edtReader.GetValue(5).ToString()

                    Me.Address2.Text = edtReader.GetValue(6).ToString()
                    'create the dropdownlist
                    'create  country the dropdownlist
                    'create  country the dropdownlist
                    Dim country As New clsCommonClass

                    If rank <> 0 Then
                        country.spctr = Session("login_ctryID").ToString().ToUpper

                    End If
                    country.rank = rank

                    Dim countryds As New DataSet


                    countryds = country.Getcomidname("BB_MASCTRY_IDNAME")
                    If countryds.Tables.Count <> 0 Then
                        databonds(countryds, Me.CountryID)
                    End If
                    Me.CountryID.Items.Insert(0, "")
                    Me.CountryID.Text = edtReader.GetValue(7).ToString()

                    'create  state the dropdownlist
                    Dim stat As New clsCommonClass
                    If (CountryID.SelectedValue().ToString() <> "") Then
                        stat.spctr = Me.CountryID.SelectedValue().ToString().ToUpper()
                    End If

                    stat.rank = rank
                    Dim stateds As New DataSet

                    stateds = stat.Getcomidname("BB_MASSTAT_IDNAME")
                    If stateds.Tables.Count <> 0 Then
                        databonds(stateds, Me.StateID)
                    End If
                    Me.StateID.Items.Insert(0, "")
                    Me.StateID.Text = edtReader.GetValue(8).ToString()

                    Dim area As New clsCommonClass


                    If (CountryID.SelectedValue().ToString() <> "") Then
                        area.spctr = Me.CountryID.SelectedValue().ToString().ToUpper()
                    End If

                    If (StateID.SelectedValue().ToString() <> "") Then
                        area.spstat = Me.StateID.SelectedValue().ToString().ToUpper()
                    End If
                    area.rank = rank

                    'create  area the dropdownlist
                    'Dim area As New clsCommonClass
                    Dim areads As New DataSet

                    areads = area.Getcomidname("BB_MASAREA_IDNAME")
                    If areads.Tables.Count <> 0 Then
                        databonds(areads, Me.AreaID)
                    End If
                    Me.AreaID.Items.Insert(0, "")
                    Me.AreaID.Text = edtReader.GetValue(9).ToString()

                    Me.POCode.Text = edtReader.GetValue(10).ToString()

                    Dim cntryStat As New clsrlconfirminf()
                    Dim statParam As New ArrayList
                    statParam = cntryStat.searchconfirminf("STATUS")
                    Dim count As Integer
                    Dim statid As String
                    Dim statnm As String
                    For count = 0 To statParam.Count - 1
                        statid = statParam.Item(count)
                        statnm = statParam.Item(count + 1)
                        If (statid.Equals("ACTIVE") Or statid.Equals("DELETE") Or statid.Equals("OBSOLETE")) Then
                            Me.status.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                        End If
                        'Me.status.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                        If statid.Equals(edtReader.GetValue(11).ToString()) Then
                            status.Items(count / 2).Selected = True
                        End If
                        count = count + 1
                    Next
                    If purviewArray(4) = False Then
                        Me.status.Items.Remove(Me.status.Items.FindByValue("DELETE"))
                    End If

                    'Me.CreatedBy.Text = edtReader.GetValue(12).ToString()
                    Dim Rcreatby As String = edtReader.Item(12).ToString().ToUpper()
                    If Rcreatby <> "ADMIN" Then
                        Dim Rcreat As New clsCommonClass
                        Rcreat.userid() = Rcreatby
                        Dim Rcreatds As New DataSet()
                        Rcreatds = Rcreat.Getuseridname("BB_MASUSER_SelByIDName")

                        Me.CreatedBy.Text = Rcreatds.Tables(0).Rows(0).Item(0)
                    Else
                        Me.CreatedBy.Text = "ADMIN-ADMIN"
                    End If
                    CreatedBy.ReadOnly = True
                    Me.CreatedDate.Text = edtReader.GetValue(13).ToString()
                    CreatedDate.ReadOnly = True
                    Me.ModiBy.Text = userIDNamestr
                    ModiBy.ReadOnly = True
                    Dim address As New clsAddress()
                    address.username = userIDNamestr
                    Me.ModifiedDate.Text = edtReader.GetValue(15).ToString()

                    ModifiedDate.ReadOnly = True
                    CreateGrid()
                End If

            End If

            addtypesybom = Me.AddressType.SelectedValue().ToString()

        End If
    End Sub
#Region " id bond"
    Public Function databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        Dim row As DataRow
        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            NewItem.Text = row("id") & "-" & row("name")
            NewItem.Value = row("id")
            dropdown.Items.Add(NewItem)
        Next

    End Function
#End Region
#Region " ro bond"
    Public Function databondros(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        Dim row As DataRow
        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            NewItem.Text = row("id") & "-" & row("name")
            NewItem.Value = row("id") & "-" & row("name")
            dropdown.Items.Add(NewItem)
        Next

    End Function
#End Region

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton1.Click

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim msginfo As String = objXm.GetLabelName("StatusMessage", "SAVEORNOT")
        Dim msgtitle As String = objXm.GetLabelName("StatusMessage", "POCTITLE")
        MessageBox1.Confirm(msginfo)
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        CreateGrid()
    End Sub

    Protected Sub rotype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rotype.SelectedIndexChanged
        Dim rotypestr As String
        rotypestr = Me.rotype.Text
        If Me.rotype.Text <> "" Then
            'Dim ro As Array
            'ro = rotypestr.Split("-")
            'Dim roid As New clsAddress()
            'roid.ModelType = ro(0)
            'roid.SerialNo = ro(1)
            'Dim rods As New DataSet
            'rods = roid.GetROID()
            'Me.rouid.Text = rods.Tables(0).Rows(0).Item(0).ToString()
        Else
            Me.rouid.Text = 0
        End If
        CreateGrid()
    End Sub

    Protected Sub addressView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles addressView.PageIndexChanging
        Me.addressView.PageIndex = e.NewPageIndex
        GetGrid()
    End Sub

    Protected Sub HyperLink1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HyperLink1.Click
        Dim mdsymbol As String = Request.QueryString("symbol")
        Dim mdfypref As String = Request.QueryString("Prefix")
        If mdsymbol = 0 Then
            Dim url As String = "~/PresentationLayer/masterrecord/addcustomer.aspx?"
            url &= "custid=" & Me.custid.Text.ToUpper().ToString & "&"
            url &= "Prefix=" & mdfypref
            Response.Redirect(url)
        End If
        If mdsymbol = 1 Then
            Dim url As String = "~/PresentationLayer/masterrecord/modifycustomer.aspx?"
            url &= "custid=" & Me.custid.Text.ToUpper().ToString & "&"
            url &= "Prefix=" & mdfypref & "&"
            url &= "modisy=" & 1
            Response.Redirect(url)
        End If
    End Sub

    Protected Sub CountryID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CountryID.SelectedIndexChanged
        Me.StateID.Items.Clear()
        Me.AreaID.Items.Clear()
        Dim rank As String = Session("login_rank")
        Dim state As New clsCommonClass

        If (CountryID.SelectedValue().ToString() <> "") Then
            state.spctr = Me.CountryID.SelectedValue().ToString().ToUpper()
        End If
        state.rank = rank

        'Dim state As New clsCommonClas
        Dim stateds As New DataSet

        stateds = state.Getcomidname("BB_MASSTAT_IDNAME")
        If stateds.Tables.Count <> 0 Then
            databonds(stateds, Me.StateID)
        End If
        Me.StateID.Items.Insert(0, "")
        Dim area As New clsCommonClass
        'Dim ctridname As String
        If (CountryID.SelectedValue().ToString() <> "") Then
            area.spctr = Me.CountryID.SelectedValue().ToString().ToUpper()
        End If
        'Dim staid As String
        If (StateID.SelectedValue().ToString() <> "") Then
            area.spstat = Me.StateID.SelectedValue().ToString().ToUpper()
        End If
        area.rank = rank

        'create  area the dropdownlist
        'Dim area As New clsCommonClass
        Dim areads As New DataSet

        areads = area.Getcomidname("BB_MASAREA_IDNAME")
        If areads.Tables.Count <> 0 Then
            databonds(areads, Me.AreaID)
        End If
        Me.AreaID.Items.Insert(0, "")
        GetGrid()
    End Sub

    Protected Sub StateID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles StateID.SelectedIndexChanged
        Me.AreaID.Items.Clear()
        Dim rank As String = Session("login_rank")
        Dim area As New clsCommonClass
        'Dim ctridname As String
        If (CountryID.SelectedValue().ToString() <> "") Then
            area.spctr = Me.CountryID.SelectedValue().ToString().ToUpper()
        End If
        'Dim staid As String
        If (StateID.SelectedValue().ToString() <> "") Then
            area.spstat = Me.StateID.SelectedValue().ToString().ToUpper()
        End If
        area.rank = rank

        'create  area the dropdownlist
        'Dim area As New clsCommonClass
        Dim areads As New DataSet
        areads = area.Getcomidname("BB_MASAREA_IDNAME")
        If areads.Tables.Count <> 0 Then
            databonds(areads, Me.AreaID)
        End If
        Me.AreaID.Items.Insert(0, "")

        GetGrid()
    End Sub

    Protected Sub MessageBox1_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox1.GetMessageBoxResponse
        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
            Dim addressEntity As New clsAddress()
            'If (Trim(Me.CustomerPrefix.Text) <> "") Then
            '    addressEntity.CountryID = CustomerPrefix.Text.ToUpper()
            'End If
            Dim mdfypref As String = Request.QueryString("Prefix")
            addressEntity.CustPrefix = mdfypref
            If (Trim(Me.custid.Text) <> "") Then
                addressEntity.CustomerID = custid.Text.ToUpper()
            End If
            If (Trim(Me.rouid.Text) <> "") Then
                addressEntity.RouID = rouid.Text
            End If
            If (Trim(Me.Address1.Text) <> "") Then
                addressEntity.Address1 = Address1.Text.ToUpper()
            End If

            If (Trim(Me.Address2.Text) <> "") Then
                addressEntity.Address2 = Me.Address2.Text.ToString().ToUpper()
            End If


            If (AreaID.SelectedValue().ToString() <> "") Then
                addressEntity.AreaID = Me.AreaID.SelectedValue().ToString().ToUpper()
            End If

            If (CountryID.SelectedValue().ToString() <> "") Then
                addressEntity.CountryID = Me.CountryID.SelectedValue().ToString().ToUpper()
            End If

            If (StateID.SelectedValue().ToString() <> "") Then
                addressEntity.StateID = Me.StateID.SelectedValue().ToString().ToUpper()
            End If


            If (Trim(Me.AddressType.SelectedValue().ToString()) <> "") Then
                addressEntity.AddressType = Me.AddressType.SelectedValue().ToString()
            End If
            'Dim oldaddtype As String = addressEntity.AddressType
            If (Trim(Me.PIC.Text) <> "") Then
                addressEntity.PICName = Me.PIC.Text.ToString().ToUpper()
            End If
            If (Trim(Me.POCode.Text.ToString()) <> "") Then
                addressEntity.POCode = Me.POCode.Text.ToString().ToUpper()
            End If

            If (Me.status.SelectedItem.Value.ToString() <> "") Then
                addressEntity.Status = status.SelectedItem.Value.ToString()
            End If

            If (Trim(CreatedBy.Text) <> "") Then
                addressEntity.CreatedBy = Me.CreatedBy.Text().ToUpper()
            End If
            'If (Trim(CreatedDate.Text) <> "") Then
            '    TechnicianEntity.CreateDate = Me.CreatedDate.Text()
            'End If
            If (Trim(ModiBy.Text) <> "") Then
                addressEntity.ModifiedBy = Session("userID").ToString().ToUpper
            End If
            addressEntity.Ipadress = Request.UserHostAddress.ToString()
            addressEntity.Svcid = Session("login_svcID")
            addressEntity.username = Session("userID")
            addressEntity.Addtype = addtypesybom
            If addtypesybom = addressEntity.AddressType Then
                Dim addressdupCount As Integer = addressEntity.GetDupaddress()
                If addressdupCount.Equals(-1) Then
                    Dim objXm As New clsXml
                    objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    errlab.Text = objXm.GetLabelName("StatusMessage", "ADDRESSFAIL")
                    errlab.Visible = True
                    CreateGrid()
                ElseIf addressdupCount.Equals(0) Then
                    Dim updCtryCnt As Integer = addressEntity.Update()
                    Dim objXmlTr As New clsXml
                    objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    If updCtryCnt = 0 Then
                        addtypesybom = Me.AddressType.SelectedValue().ToString()
                        Dim objXm As New clsXml
                        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                        errlab.Text = objXm.GetLabelName("StatusMessage", "BB-HintMsg-Suc")
                        errlab.Visible = True
                        CreateGrid()
                    Else
                        'Dim msg As String = objXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Fail")
                        'Dim msgBox As New MessageBox(Me)
                        'msgBox.Show(msg)
                        Response.Redirect("~/PresentationLayer/Error.aspx")
                    End If
                End If
            End If
            If addtypesybom <> addressEntity.AddressType Then
                Dim dupCount As Integer = addressEntity.GetDupaddresstype()
                If dupCount.Equals(-1) Then
                    Dim objXm As New clsXml
                    objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    errlab.Text = objXm.GetLabelName("StatusMessage", "ADDRESSTYPEFAIL")
                    errlab.Visible = True
                    CreateGrid()
                ElseIf dupCount.Equals(0) Then
                    addressEntity.AddressType = addtypesybom
                    Dim addressdupCount As Integer = addressEntity.GetDupaddress()
                    If addressdupCount.Equals(-1) Then
                        Dim objXm As New clsXml
                        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                        errlab.Text = objXm.GetLabelName("StatusMessage", "ADDRESSFAIL")
                        errlab.Visible = True
                        CreateGrid()
                    ElseIf addressdupCount.Equals(0) Then
                        addressEntity.AddressType = Me.AddressType.SelectedValue().ToString()
                        Dim insCtryCnt As Integer = addressEntity.Update()
                        Dim objXmlTr As New clsXml
                        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                        If insCtryCnt = 0 Then
                            addtypesybom = Me.AddressType.SelectedValue().ToString()
                            Dim objXm As New clsXml
                            objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                            errlab.Text = objXm.GetLabelName("StatusMessage", "BB-HintMsg-Suc")
                            errlab.Visible = True
                            'Dim addressEntity As New clsAddress()
                            CreateGrid()
                        End If
                    Else
                        'Dim msg As String = objXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Fail")
                        'Dim msgBox As New MessageBox(Me)
                        'msgBox.Show(msg)
                        Response.Redirect("~/PresentationLayer/Error.aspx")
                    End If
                End If
            End If

        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
        End If
    End Sub

    Protected Sub MessageBox2_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox2.GetMessageBoxResponse
        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim objXm As New clsXml
            objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            Dim msginfo As String = objXm.GetLabelName("StatusMessage", "SAVEORNOT")
            Dim msgtitle As String = objXm.GetLabelName("StatusMessage", "SAVETITLE")
            MessageBox1.Confirm(msginfo)
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            CreateGrid()
        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
        End If
    End Sub

    Private Function CreateGrid()
        Dim addressEntity As New clsAddress()
        Dim addressall As DataSet = addressEntity.GetAddress()
        'If addressall.Tables(0).Rows.Count <> 0 Then
        Me.addressView.DataSource = addressall
        Session("addressView") = addressall
        addressView.AllowPaging = True
        addressView.AllowSorting = True
        addressView.DataBind()
        Dim obiXmlTr As New clsXml
        obiXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        If addressall.Tables(0).Rows.Count >= 1 Then
            addressView.HeaderRow.Cells(0).Text = obiXmlTr.GetLabelName("EngLabelMsg", "ADDRESSTYPE")
            addressView.HeaderRow.Cells(0).Font.Size = 8
            addressView.HeaderRow.Cells(1).Text = obiXmlTr.GetLabelName("EngLabelMsg", "STATUS")
            addressView.HeaderRow.Cells(1).Font.Size = 8
            addressView.HeaderRow.Cells(2).Text = obiXmlTr.GetLabelName("EngLabelMsg", "ADDRESS1")
            addressView.HeaderRow.Cells(2).Font.Size = 8
            addressView.HeaderRow.Cells(3).Text = obiXmlTr.GetLabelName("EngLabelMsg", "ADDRESS2")
            addressView.HeaderRow.Cells(3).Font.Size = 8
            addressView.HeaderRow.Cells(4).Text = obiXmlTr.GetLabelName("EngLabelMsg", "POCODE")
            addressView.HeaderRow.Cells(4).Font.Size = 8
        End If
    End Function

    Private Function GetGrid()
        Dim addressall As DataSet = Session("addressView")
        addressView.DataSource = addressall
        addressView.DataBind()
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        If addressall.Tables(0).Rows.Count >= 1 Then
            addressView.HeaderRow.Cells(0).Text = objXmlTr.GetLabelName("EngLabelMsg", "ADDRESSTYPE")
            addressView.HeaderRow.Cells(0).Font.Size = 8
            addressView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "STATUS")
            addressView.HeaderRow.Cells(1).Font.Size = 8
            addressView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "ADDRESS1")
            addressView.HeaderRow.Cells(2).Font.Size = 8
            addressView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "ADDRESS2")
            addressView.HeaderRow.Cells(3).Font.Size = 8
            addressView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "POCODE")
            addressView.HeaderRow.Cells(4).Font.Size = 8
        End If
    End Function
End Class
