Imports BusinessEntity
Imports System.Configuration
Imports System.Xml
Imports System.Data
Partial Class PresentationLayer_masterrecord_addContact
    Inherits System.Web.UI.Page
    Dim contactEntity As New clsContact()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try

        BindGrid()
    End Sub
    Protected Sub BindGrid()
        If Not Page.IsPostBack Then
            Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "04")
            If purviewArray(0) = False Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
            Me.custid.Text = Request.QueryString("custID").ToString()
            Dim mdfypref As String = Request.QueryString("custPf").ToString()
            If mdfypref Is Nothing Then mdfypref = Session("login_ctryid").ToString
            Me.PIC.Text = Request.QueryString("custname").ToString()
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            custidlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0001")
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "ADDCONTACT")
            Me.statusLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCTRY-0004")
            Me.TelepTypelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASTELETYPE")

            Me.PIClab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0003")
            Me.Telephonelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASTELE")
            Me.Emaillab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASEmail")

            Me.rouidLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0007")
            Me.rotypelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0006")
            CreatedBylab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATBY")
            CreatedDatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
            ModiBylab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFBY")
            ModifiedDatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFDATE")
            LinkButton1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Save")
            HyperLink1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Cancel")
            'Dim address As String
            'address = objXmlTr.GetLabelName("EngLabelMsg", "INSTERPL")

            Me.lblCompTop.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            Me.lblCompBottom.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")

            Dim statXmlTr As New clsXml
            statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            Me.TelephoneMsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Tele")
            Me.Emailmsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Email")
            Me.telnoerr.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "BB-NOTEL")

            Me.TelepTypemsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "TELTYPE")
            'Me.mailnoerr.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "BB-NOEMAIL")
            Dim userIDNamestr As String = Session("userID").ToString().ToUpper + "-" + Session("username").ToString.ToUpper
            Me.CreatedBy.Text = userIDNamestr
            Me.CreatedDate.Text = Date.Now()
            Me.ModiBy.Text = userIDNamestr
            Me.ModifiedDate.Text = Date.Now()
            Dim contact As New clsContact()
            contact.username = userIDNamestr

            'create  the dropdownlist
            'create the dropdownlist
            Dim techStat As New clsrlconfirminf()
            Dim statParam As ArrayList = New ArrayList
            statParam = techStat.searchconfirminf("STATUS")
            Dim count As Integer
            Dim statid As String
            Dim statnm As String
            For count = 0 To statParam.Count - 1
                statid = statParam.Item(count)
                statnm = statParam.Item(count + 1)
                count = count + 1
                If statid.Equals("ACTIVE") Or statid.Equals("OBSOLETE") Then
                    Me.status.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                End If
            Next
            'status.Items(0).Selected = True


            'create  BRANCHTYPE the dropdownlist
            Dim Tetype As New clsrlconfirminf()
            Dim TeParam As ArrayList = New ArrayList
            TeParam = Tetype.searchconfirminf("TELETYPE")
            Dim Tecount As Integer
            Dim Teid As String
            Dim Tenm As String
            For Tecount = 0 To TeParam.Count - 1
                Teid = TeParam.Item(Tecount)
                Tenm = TeParam.Item(Tecount + 1)
                Tecount = Tecount + 1
                Me.TelepType.Items.Add(New ListItem(Tenm.ToString(), Teid.ToString()))
            Next
            'create  country the dropdownlist
            Dim rotype As New clsCustomerRecord()
            If (Trim(Me.custid.Text) <> "") Then
                rotype.CustomerID = custid.Text.ToUpper()
            End If
            rotype.Customerpf = mdfypref
            'Dim rotypeds As New DataSet
            'rotypeds = rotype.GetROidname("BB_MASROUM_MODSER")
            'databondros(rotypeds, Me.rotype)
            Me.rotype.Items.Insert(0, "")
            Dim rotypestr As String
            rotypestr = Me.rotype.Text
            Dim ro As Array
            ro = rotypestr.Split("-")
            If ro.Length > 1 Then
                Dim roid As New clsAddress()
                roid.ModelType = ro(0)
                roid.SerialNo = ro(1)
                Dim rods As New DataSet
                rods = roid.GetROID()
                Me.rouid.Text = rods.Tables(0).Rows(0).Item(0).ToString()
            End If
            If (Trim(Me.custid.Text) <> "") Then
                contactEntity.CustomerID = custid.Text.ToUpper()
            End If
            'Dim mdfypref As String = Request.Params("Prefix").ToString()
            contactEntity.CustPrefix = mdfypref
            CreateGrid()
            Me.PIC.Focus()

        End If
    End Sub
#Region " ro bond"
    Public Sub databondros(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        Dim row As DataRow
        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            NewItem.Text = row("id") & "-" & row("name")
            NewItem.Value = row("id") & "-" & row("name")
            dropdown.Items.Add(NewItem)
        Next

    End Sub
#End Region

    Protected Sub rotype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rotype.SelectedIndexChanged
        Dim rotypestr As String
        rotypestr = Me.rotype.Text
        If Me.rotype.Text <> "" Then
            Dim ro As Array
            ro = rotypestr.Split("-")
            Dim roid As New clsAddress()
            roid.ModelType = ro(0)
            roid.SerialNo = ro(1)
            Dim rods As New DataSet
            rods = roid.GetROID()
            Me.rouid.Text = rods.Tables(0).Rows(0).Item(0).ToString()
        Else
            Me.rouid.Text = 0
        End If
        CreateGrid()
    End Sub

    Protected Sub contactView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles contactView.PageIndexChanging
        Me.contactView.PageIndex = e.NewPageIndex
        GetGrid()
    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton1.Click
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim msginfo As String = objXm.GetLabelName("StatusMessage", "SAVEORNOT")
        Dim msgtitle As String = objXm.GetLabelName("StatusMessage", "SAVETITLE")
        MessageBox1.Confirm(msginfo)

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        CreateGrid()
    End Sub

    Protected Sub HyperLink1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HyperLink1.Click
        Dim mdsymbol As String = Request.Params("consymbol")
        Dim mdfypref As String = Session("login_ctryid").ToString
        If mdsymbol = 0 Then
            Dim url As String = "~/PresentationLayer/masterrecord/addcustomer.aspx?"
            url &= "custid=" & Me.custid.Text.ToUpper().ToString & "&"
            url &= "Prefix=" & mdfypref
            Response.Redirect(url)
        End If
        If mdsymbol = 1 Then
            Dim url As String = "~/PresentationLayer/masterrecord/modifycustomer.aspx?"
            url &= "custid=" & Me.custid.Text.ToUpper().ToString & "&"
            url &= "Prefix=" & mdfypref & "&"
            url &= "modisy=" & 1
            Response.Redirect(url)
        End If
    End Sub

    Protected Sub MessageBox1_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox1.GetMessageBoxResponse
        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
            Dim mdfypref As String = Session("login_ctryid").ToString 'Request.Params("Prefix").ToString()
            If mdfypref Is Nothing Then mdfypref = Session("login_ctryid").ToString

            Dim contactEntity As New clsContact()
            Dim telepass As New clsCommonClass()
            'If (Trim(Me.CustomerPrefix.Text) <> "") Then

            '    'CustomerPrefix.Text.ToUpper()
            'End If
            If (Trim(Me.custid.Text) <> "") Then
                contactEntity.CustomerID = custid.Text.ToUpper()
                contactEntity.CustPrefix = mdfypref.ToUpper()
            End If
            If (Trim(Me.Telephone.Text) <> "") Then
                contactEntity.Telephone = telepass.telconvertpass(Telephone.Text)
            End If

            If (Trim(Me.Email.Text) <> "") Then
                contactEntity.Email = Me.Email.Text.ToString()
            End If



            If (Me.status.SelectedItem.Value.ToString() <> "") Then
                contactEntity.Status = status.SelectedItem.Value
            End If

            If (Trim(Me.TelepType.Text) <> "") Then
                contactEntity.TelephType = Me.TelepType.Text.ToString().ToUpper()
            End If
            If (Trim(Me.PIC.Text) <> "") Then
                contactEntity.PICName = Me.PIC.Text.ToString()
            End If


            If (Trim(CreatedBy.Text) <> "") Then
                contactEntity.CreatedBy = Session("userID").ToString().ToUpper
            End If
            'If (Trim(CreatedDate.Text) <> "") Then
            '    TechnicianEntity.CreateDate = Me.CreatedDate.Text()
            'End If
            If (Trim(ModiBy.Text) <> "") Then
                contactEntity.ModifiedBy = Session("userID").ToString().ToUpper
            End If
            If (Trim(rouid.Text) <> "") Then
                contactEntity.RouID = Convert.ToInt32(Me.rouid.Text)
                'Else
                '    addressEntity.RouID = Convert.ToInt32("")
            End If
            contactEntity.username = Session("userID")
            Dim dupCount As Integer = contactEntity.GetDupcontacttype()
            If dupCount.Equals(-1) Then
                Dim objXm As New clsXml
                objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                errlab.Text = objXm.GetLabelName("StatusMessage", "TELEPHTYPEFAIL")
                errlab.Visible = True
                CreateGrid()
            ElseIf dupCount.Equals(0) Then
                Dim insCtryCnt As Integer = contactEntity.Insert()
                Dim objXmlTr As New clsXml
                objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                If insCtryCnt = 0 Then
                    Dim objXm As New clsXml
                    objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    errlab.Text = objXm.GetLabelName("StatusMessage", "BB-HintMsg-Suc")
                    errlab.Visible = True
                    CreateGrid()
                Else
                    Response.Redirect("~/PresentationLayer/Error.aspx")
                End If
            End If
        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
        End If
    End Sub

    Private Function CreateGrid()
        Dim contactall As DataSet = contactEntity.Getcontact()
        ''If addressall.Tables(0).Rows.Count <> 0 Then
        Me.contactView.DataSource = contactall
        Session("contactView") = contactall
        contactView.AllowPaging = True
        contactView.AllowSorting = True
        contactView.DataBind()
        Dim obiXmlTr As New clsXml
        obiXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        If contactall.Tables(0).Rows.Count >= 1 Then
            'contactView.HeaderRow.Cells(0).Text = obiXmlTr.GetLabelName("EngLabelMsg", "BB-INSTALLHD1")
            'contactView.HeaderRow.Cells(0).Font.Size = 8
            'contactView.HeaderRow.Cells(1).Text = obiXmlTr.GetLabelName("EngLabelMsg", "BB-INSTALLHD2")
            'contactView.HeaderRow.Cells(1).Font.Size = 8
            contactView.HeaderRow.Cells(0).Text = obiXmlTr.GetLabelName("EngLabelMsg", "TELEPHONETYPE")
            contactView.HeaderRow.Cells(0).Font.Size = 8
            contactView.HeaderRow.Cells(1).Text = obiXmlTr.GetLabelName("EngLabelMsg", "STATUS")
            contactView.HeaderRow.Cells(1).Font.Size = 8
            contactView.HeaderRow.Cells(2).Text = obiXmlTr.GetLabelName("EngLabelMsg", "PICNAME")
            contactView.HeaderRow.Cells(2).Font.Size = 8
            contactView.HeaderRow.Cells(3).Text = obiXmlTr.GetLabelName("EngLabelMsg", "TELEPHONE")
            contactView.HeaderRow.Cells(3).Font.Size = 8
        End If
    End Function

    Private Function GetGrid()
        Dim contactall As DataSet = Session("contactView")
        contactView.DataSource = contactall
        contactView.DataBind()
        Dim obiXmlTr As New clsXml
        obiXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        If contactall.Tables(0).Rows.Count >= 1 Then
            'contactView.HeaderRow.Cells(0).Text = obiXmlTr.GetLabelName("EngLabelMsg", "BB-INSTALLHD1")
            'contactView.HeaderRow.Cells(0).Font.Size = 8
            'contactView.HeaderRow.Cells(1).Text = obiXmlTr.GetLabelName("EngLabelMsg", "BB-INSTALLHD2")
            'contactView.HeaderRow.Cells(1).Font.Size = 8
            contactView.HeaderRow.Cells(0).Text = obiXmlTr.GetLabelName("EngLabelMsg", "TELEPHONETYPE")
            contactView.HeaderRow.Cells(0).Font.Size = 8
            contactView.HeaderRow.Cells(1).Text = obiXmlTr.GetLabelName("EngLabelMsg", "STATUS")
            contactView.HeaderRow.Cells(1).Font.Size = 8
            contactView.HeaderRow.Cells(2).Text = obiXmlTr.GetLabelName("EngLabelMsg", "PICNAME")
            contactView.HeaderRow.Cells(2).Font.Size = 8
            contactView.HeaderRow.Cells(3).Text = obiXmlTr.GetLabelName("EngLabelMsg", "TELEPHONE")
            contactView.HeaderRow.Cells(3).Font.Size = 8
        End If
    End Function
End Class
