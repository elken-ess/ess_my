<%@ Page Language="VB" AutoEventWireup="false" EnableEventValidation ="false"  CodeFile="AddKitItem.aspx.vb" Inherits="PresentationLayer_masterrecord_AddKitItem" %>

<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Kit item Page</title>
     <meta http-equiv="Content-Type" content="text/html; charset=gb2312">
	<LINK href="../css/style.css" type="text/css" rel="stylesheet">
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table id="countrytab" border="0" width="100%">
            <tr>
                <td style="width: 500px">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title1.gif" width="5" /></td>
                            <td background="../graph/title_bg.gif" class="style2" width="98%" style="width: 100%">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="right" background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title_2.gif" width="5" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 500px; height: 30px">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" class="style2" style="height: 14px" width="98%">
                                <font color="red" style="width: 100%">
                                    <asp:Label ID="errlab" runat="server" Text="Label" Visible="False" Width="496px"></asp:Label></font>
                                    </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 500px">
                    <table id="country" bgcolor="#b7e6e6" border="0" cellpadding="2" cellspacing="1" style="width: 100%">
                      <tr bgcolor="#ffffff">
<td colspan = 4>
                            <asp:Label ID="lblCompTop" runat="server" ForeColor="Red" Text="Label" Visible="True"></asp:Label>
</td>
             </tr>
             <tr bgcolor="#ffffff">
<td colspan = 4>
                        	&nbsp;
                     	</td>
               </tr> 
                        <tr bgcolor="#ffffff">
                            <td align="right" style="height: 30px" width="15%">
                                &nbsp;<asp:Label ID="kitidlab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%; height: 30px">
                                <asp:TextBox ID="kitidbox" runat="server" CssClass="textborder" MaxLength="10" Width="82%" ReadOnly="True"></asp:TextBox><font
                                    color="red"><strong>*</strong></font>
                                <asp:RequiredFieldValidator ID="kitiderr" runat="server" ControlToValidate="kitidbox"
                                    Width="2px" ForeColor="White">*</asp:RequiredFieldValidator></td>
                            <td align="right" style="height: 30px" width="15%">
                                <asp:Label ID="partidLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="height: 30px; width: 35%;">
                                <asp:DropDownList ID="Drppartid" runat="server" CssClass="textborder" Style="width: 82%"
                                    Width="136px">
                                </asp:DropDownList>
                                <font
                                    color="red"><strong>*</strong></font>
                                <asp:RequiredFieldValidator ID="partiderr" runat="server" ControlToValidate="Drppartid" ForeColor="White">*</asp:RequiredFieldValidator></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" width="15%">
                                <asp:Label ID="quantitylab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%">
                                <asp:TextBox ID="quantitytbox" runat="server" CssClass="textborder" Width="136px" style="width: 82%"></asp:TextBox>
                                <asp:RangeValidator ID="quantityerr" runat="server" ErrorMessage="RangeValidator"
                                    MaximumValue="24767" MinimumValue="0" Type="Integer" ControlToValidate="quantitytbox" ForeColor="White">*</asp:RangeValidator></td>
                            <td align="right" width="15%">
                                <asp:Label ID="statusLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%">
                                <asp:DropDownList ID="stat" runat="server" CssClass="textborder" style="width: 82%" Width="144px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" width="15%">
                                <asp:Label ID="creatby" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%">
                                <asp:TextBox ID="creatbybox" runat="server" CssClass="textborder" ReadOnly="true" style="width: 82%" Width="136px"></asp:TextBox></td>
                            <td align="right" width="15%">
                                <asp:Label ID="creatdate" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%">
                                <asp:TextBox ID="creatdtbox" runat="server" CssClass="textborder" ReadOnly="true" style="width: 82%" Width="136px"></asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" width="15%">
                                <asp:Label ID="modfyby" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%">
                                <asp:TextBox ID="modfybybox" runat="server" CssClass="textborder" ReadOnly="true" style="width: 82%" Width="136px"></asp:TextBox></td>
                            <td align="right" width="15%">
                                <asp:Label ID="mdfydate" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%">
                                <asp:TextBox ID="mdfydtbox" runat="server" CssClass="textborder" ReadOnly="true" style="width: 82%"></asp:TextBox></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 500px; height: 37px">
                    <asp:GridView ID="kititemView" runat="server" AllowPaging="True" Font-Size="Smaller"
                        Height="56px" Style="width: 100%" Width="100%">
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td style="width: 500px; height: 37px">
                <font color=red><asp:Label ID="lblCompBottom" runat="server" Text="Label" Visible="true"></asp:Label></font>
                    <br />
                                <asp:LinkButton ID="saveButton" runat="server"></asp:LinkButton>
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<asp:LinkButton ID="cancelLink" runat="server" CausesValidation="False"></asp:LinkButton>&nbsp;
                </td>
            </tr>
        </table>
        &nbsp;
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
            ShowSummary="False" />
        <asp:TextBox ID="TextBox1" runat="server" Visible="False"></asp:TextBox>
        <cc1:MessageBox ID="MessageBox1" runat="server" />
    </div>
    </form>
</body>
</html>
