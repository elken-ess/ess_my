Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Partial Class PresentationLayer_masterrecord_addmodeltype
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then

            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "16")
            Me.LinkButton1.Enabled = purviewArray(0)
            If purviewArray(0) = False Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return

            End If
           
            HypCal.NavigateUrl = "javascript:DoCal(document.mdfymodeltypeform.EffectiveDateBox);"
            HypCal2.NavigateUrl = "javascript:DoCal(document.mdfymodeltypeform.ObsoleteDateLabBox);"

            'Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
            'System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
            'display label message
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")

            Me.CountryIDLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-0001")
            Me.ModelIDLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-0002")
            Me.AlModelNamLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-0003")
            Me.EffectiveDateLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-0004")
            Me.WarrantyDayLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-0005")
            Me.ProductClassLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-0006")
            Me.ModelNameLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-0007")
            Me.ObsoleteDateLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-0008")
            Me.StatusLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-0009")
            Me.lblCompTop.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            Me.lblCompBottom.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            creatby.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATBY")
            creatdate.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
            modfyby.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFBY")
            mdfydate.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFDATE")
            Me.titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-0011")
            LinkButton1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Save")
            HyperLink1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Cancel")
            AddLink.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Add")
            AddLink.NavigateUrl = Page.Request.RawUrl
            'create by create date 
            Dim userIDNamestr As String = Session("userID").ToString().ToUpper + "-" + Session("username").ToString.ToUpper


            creatbybox.Text = userIDNamestr
            creatdtbox.Text = Date.Now
            Me.ModelIDBox.Focus()
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''          
            'Me.Form.InnerText = "fsadlfjpawefjldsajfpsdajflkds;jfklsad;fdsa"
            'Me.Form.InnerHtml = "innerhtml"
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' 

            modfybybox.Text = userIDNamestr
            mdfydtbox.Text = Date.Now
            ' \\\\\\\\\\\\\\\\\\\\\\\\
            Dim statXmlTr As New clsXml
            statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            Me.RangeValidator1.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "MDWRD")
            Me.RequiredFieldValidator1.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "MDNAM")
            Me.RequiredFieldValidator2.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "MDWD")
            Me.RequiredFieldValidator3.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "MDEFF")
            Me.RequiredFieldValidator4.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "MDOBS")
            Me.RequiredFieldValidator5.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "MDID")
            Me.rfvctry.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "MDCTR")
            Me.rfvaname.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "MDANAME")
            '\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
            'create the dropdownlist
            Dim cntryStat As New clsrlconfirminf()
            Dim statParam As ArrayList = New ArrayList
            statParam = cntryStat.searchconfirminf("STATUS")
            Dim count As Integer
            Dim statid As String
            Dim statnm As String
            For count = 0 To statParam.Count - 1
                statid = statParam.Item(count)
                statnm = statParam.Item(count + 1)
                count = count + 1

                If statid.Equals("ACTIVE") Or statid.Equals("OBSOLETE") Then
                    Me.StatusDDL.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                End If

            Next
            'product class dropdrown list bond 
            Dim pcdtclass As ArrayList = New ArrayList
            pcdtclass = cntryStat.searchconfirminf("PRODUCTCL")
            Dim pcdtclsID As String
            Dim pcdtclsName As String
            Me.ProductClassDDL.Items.Clear()
            For count = 0 To pcdtclass.Count - 1
                pcdtclsID = pcdtclass.Item(count)
                pcdtclsName = pcdtclass.Item(count + 1)
                count = count + 1
                Me.ProductClassDDL.Items.Add(New ListItem(pcdtclsName.ToString(), pcdtclsID.ToString()))
            Next
            ' Me.ProductClassDDL.Items(0).Selected = True
            'bonds country  id  name
            Dim clscom As New clsCommonClass()
            Dim idnameds As New DataSet()
            Dim rank As Integer = Session("login_rank")
            clscom.rank = rank
            If rank <> 0 Then
                clscom.spctr = Session("login_ctryID")
                clscom.spstat = Session("login_cmpID")
                clscom.sparea = Session("login_svcID")
            End If
            idnameds = clscom.Getcomidname("BB_MASCTRY_IDNAME")
            databonds(idnameds, Me.CountryIDDDL)
            Me.CountryIDDDL.Items.Insert(0, "")

            CountryIDDDL.SelectedValue = Session("login_ctryID")
            '///////////////////////////////////
        End If
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
    End Sub

#Region " command bond function "
    Public Function databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        Dim row As DataRow
        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            NewItem.Text = Trim(row(0).ToString()) & "-" & Trim(row(1).ToString())
            NewItem.Value = row(0)
            dropdown.Items.Add(NewItem)
        Next
    End Function


#End Region

#Region "insert record"
    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton1.Click

        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim msginfo As String = objXm.GetLabelName("StatusMessage", "SAVEORNOT")
        Dim msgtitle As String = objXm.GetLabelName("StatusMessage", "SAVETITLE")
        MessageBox1.Font.Name = "Verdana"
        Dim effdate As Date
        Dim obsdate As Date

        If (Trim(Request.Form("EffectiveDateBox")) <> "") Then
            Dim datearr As Array = Request.Form("EffectiveDateBox").Split("/") 'EffectiveDateBox.Text.ToString().Split("/")
            Dim rtdate As String = datearr(2) + "-" + datearr(1) + "-" + datearr(0)
            effdate = Convert.ToDateTime(rtdate)
        End If

        If (Trim(Request.Form("ObsoleteDateLabBox")) <> "") Then
            Dim datearr As Array = Request.Form("ObsoleteDateLabBox").Split("/") 'ObsoleteDateLabBox.Text.ToString().Split("/")
            Dim rtdate As String = datearr(2) + "-" + datearr(1) + "-" + datearr(0)
            obsdate = Convert.ToDateTime(rtdate)
        End If
        If obsdate < effdate Then
            addinfo.Text = objXm.GetLabelName("StatusMessage", "BB-HintMsg-COMPAREEFFOBDATE")
            addinfo.Visible = True
            Return
        End If
        EffectiveDateBox.Text = Request.Form("EffectiveDateBox")
        ObsoleteDateLabBox.Text = Request.Form("ObsoleteDateLabBox")
        MessageBox1.Confirm(msginfo, msgtitle)

    End Sub
#End Region

#Region "add record "
    Public Sub addrecord()

        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim modeltypeEntity As New clsModelType()
        If (Trim(Me.CountryIDDDL.Text) <> "") Then
            modeltypeEntity.CountryID = Me.CountryIDDDL.SelectedValue
        End If
        If (Trim(Me.ProductClassDDL.Text) <> "") Then
            modeltypeEntity.ProductClass = ProductClassDDL.SelectedValue
        End If
        If (Trim(Me.ModelIDBox.Text) <> "") Then
            modeltypeEntity.ModelTypeID = ModelIDBox.Text.ToUpper()
        End If
        If (Trim(Me.ModelNameBox.Text) <> "") Then
            modeltypeEntity.ModelTypeName = ModelNameBox.Text.ToUpper()
        End If
        If (Trim(Me.AlModelNamBox.Text) <> "") Then
            modeltypeEntity.ModelTypeAlternateName = AlModelNamBox.Text.ToUpper()
        End If
        If (Trim(Request.Form("EffectiveDateBox")) <> "") Then
            Dim datearr As Array = Request.Form("EffectiveDateBox").Split("/") 'EffectiveDateBox.Text.ToString().Split("/")
            Dim rtdate As String = datearr(2) + "-" + datearr(1) + "-" + datearr(0)
            modeltypeEntity.Effectivedate = rtdate
            '   modeltypeEntity.Effectivedate = EffectiveDateBox.Text
        End If
        Me.ObsoleteDateLabBox.ReadOnly = True
        Me.EffectiveDateBox.ReadOnly = True
        If (Trim(Request.Form("ObsoleteDateLabBox")) <> "") Then
            Dim datearr As Array = Request.Form("ObsoleteDateLabBox").Split("/") 'ObsoleteDateLabBox.Text.ToString().Split("/")
            Dim rtdate As String = datearr(2) + "-" + datearr(1) + "-" + datearr(0)
            modeltypeEntity.ObsoleteDate = rtdate
            ' modeltypeEntity.ObsoleteDate = ObsoleteDateLabBox.Text
        End If
        If (Trim(Me.WarrantyDayBox.Text) <> "") Then
            modeltypeEntity.Warrantyperiod = WarrantyDayBox.Text.ToUpper()
        End If
        If (Trim(Me.StatusDDL.Text) <> "") Then
            modeltypeEntity.ModelTypeStatus = StatusDDL.SelectedValue
        End If

        If (Trim(Me.creatbybox.Text) <> "") Then
            modeltypeEntity.Createdby = Session("userID").ToString().ToUpper
        End If
        If (Trim(Me.modfybybox.Text) <> "") Then
            modeltypeEntity.Modifyby = Session("userID").ToString().ToUpper
        End If
        'check no of date and total of today
        Dim namecheckint As Integer = checkname()
        Dim idcheckint As Integer = checkid()
        If (namecheckint = 1) Or (idcheckint = 1) Then
            addinfo.Text = objXmlTr.GetLabelName("StatusMessage", "CHECKNAMEID")
            addinfo.Visible = True
        Else
            Dim insJobCnt As Integer = modeltypeEntity.Insert()
            If insJobCnt = 0 Then
                addinfo.Text = objXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Suc")
                addinfo.Visible = True
            Else
                Response.Redirect("~/PresentationLayer/Error.aspx")
            End If
        End If
    End Sub
#End Region

#Region "check  if there are same model type name in same courty"
    Public Function checkname() As Integer
        Dim namchint As Integer = 0
        Dim modeltypeEntity As New clsModelType()
        If (Trim(Me.ModelNameBox.Text) <> "") Then
            modeltypeEntity.ModelTypeName = Me.ModelNameBox.Text
        End If
        If (Trim(Me.CountryIDDDL.Text) <> "") Then
            modeltypeEntity.CountryID = Me.CountryIDDDL.SelectedValue
        End If
        If (Trim(Me.StatusDDL.Text) <> "") Then
            modeltypeEntity.ModelTypeStatus = Me.StatusDDL.SelectedValue
        End If
        Dim sqldr As SqlDataReader = modeltypeEntity.GetMdtypeDetailsByName()
        'if there are identical record then tishi error
        If (sqldr.Read()) Then
            namchint = 1
        End If
        Return namchint
    End Function
#End Region

#Region "check  if there are same model type id in same courty"
    Public Function checkid() As Integer
        Dim idcheckint As Integer = 0
        Dim modeltypeEntity As New clsModelType()
        If (Trim(Me.ModelIDBox.Text) <> "") Then
            modeltypeEntity.ModelTypeID = Me.ModelIDBox.Text
        End If
        If (Trim(Me.CountryIDDDL.Text) <> "") Then
            modeltypeEntity.CountryID = Me.CountryIDDDL.SelectedValue
        End If
        If (Trim(Me.StatusDDL.Text) <> "") Then
            modeltypeEntity.ModelTypeStatus = Me.StatusDDL.SelectedValue
        End If
        Dim sqldr As SqlDataReader = modeltypeEntity.GetMdtypeDetailsByIDSTAT()
        'if there are identical record then tishi error
        If (sqldr.Read()) Then
            idcheckint = 1
        End If
        Return idcheckint
    End Function
#End Region

    '#Region "calender eff and obs"
    '    Protected Sub effImageBt_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles effImageBt.Click
    '        Me.effCalendar.Visible = True
    '        ' effCalendar.Attributes.Add("style", " POSITION: absolute")
    '    End Sub

    '    Protected Sub obsImageBt_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles obsImageBt.Click
    '        Me.obsCalendar.Visible = True
    '        ' obsCalendar.Attributes.Add("style", " POSITION: absolute")
    '    End Sub
    '#End Region

    '    Protected Sub effCalendar_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles effCalendar.SelectionChanged
    '        Me.EffectiveDateBox.Text = effCalendar.SelectedDate.Day.ToString() + "/" + effCalendar.SelectedDate.Month.ToString() + "/" + effCalendar.SelectedDate.Year.ToString()
    '        effCalendar.Visible = False
    '    End Sub

    '    Protected Sub obsCalendar_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles obsCalendar.SelectionChanged
    '        '  Me.ObsoleteDateLabBox.Text = obsCalendar.SelectedDate.Year.ToString() + "/" + obsCalendar.SelectedDate.Month.ToString() + "/" + obsCalendar.SelectedDate.Day.ToString()
    '        Me.ObsoleteDateLabBox.Text = obsCalendar.SelectedDate.Day.ToString() + "/" + obsCalendar.SelectedDate.Month.ToString() + "/" + obsCalendar.SelectedDate.Year.ToString()
    '        obsCalendar.Visible = False
    '    End Sub

    Protected Sub MessageBox1_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox1.GetMessageBoxResponse
        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
            addrecord()
        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
        End If
        EffectiveDateBox.Text = Request.Form("EffectiveDateBox")
        ObsoleteDateLabBox.Text = Request.Form("ObsoleteDateLabBox")
    End Sub

    Protected Sub EffectiveDateBoxJCalendar_SelectedDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles EffectiveDateBoxJCalendar.SelectedDateChanged

    End Sub
End Class
