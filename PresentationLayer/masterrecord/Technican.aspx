<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Technican.aspx.vb" Inherits="PresentationLayer_masterrecord_Technican" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Technician - Search</title>
    <link href="../css/style.css" rel="stylesheet" type="text/css" />
     <STYLE type="text/css">A:link { COLOR: #336666; TEXT-DECORATION: none }
	BODY { FONT-SIZE: 9pt; FONT-FAMILY: "Arial", "Verdana", "Tahoma"; BACKGROUND-COLOR: #ffffff }
	TD { FONT-SIZE: 9pt; FONT-FAMILY: Arial,Verdana,Tahoma }
	</STYLE>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td background="../graph/title_bg.gif" style="width: 1%">
                    <img height="24" src="../graph/title1.gif" width="5" /></td>
                <td background="../graph/title_bg.gif" class="style2" width="98%">
                    <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
                <td align="right" background="../graph/title_bg.gif" width="1%">
                    <img height="24" src="../graph/title_2.gif" width="5" /></td>
            </tr>
            <tr>
                <td background="../graph/title_bg.gif" style="width: 1%">
                </td>
                <td background="../graph/title_bg.gif" class="style2" width="98%">
                    <asp:Label ID="addinfo" runat="server" ForeColor="Red" Text="Label" Visible="False"></asp:Label></td>
                <td align="right" background="../graph/title_bg.gif" width="1%">
                </td>
            </tr>
        </table>
        <asp:Label ID="TechnicianIDlab" runat="server" Text="Label"></asp:Label><asp:TextBox ID="TechnicianID" runat="server" CssClass="textborder" Width="15%" MaxLength="10"></asp:TextBox>
        <asp:Label ID="TechnicianNamelab" runat="server" Text="Label"></asp:Label><asp:TextBox ID="TechnicianName" runat="server" CssClass="textborder" Width="15%" MaxLength="50"></asp:TextBox>
        <asp:Label ID="ctryStat" runat="server" Text="Label"></asp:Label><asp:DropDownList ID="ctryStatDrop" runat="server">
        </asp:DropDownList>&nbsp; 
        <asp:LinkButton ID="search" runat="server" Font-Size="Small">search</asp:LinkButton>
        <asp:LinkButton ID="LinkButton1" runat="server" Font-Size="Small" PostBackUrl="~/PresentationLayer/masterrecord/addTechnician.aspx" Width="16px">add</asp:LinkButton>&nbsp; 
        <hr style="font-size: 12pt" />
        <asp:GridView ID="ctryView" runat="server" AllowPaging="True" AllowSorting="True"
            Font-Size="Smaller" Width="100%">
        </asp:GridView>
        <asp:Label ID="Label1" runat="server" ForeColor="Red" Height="62px" Text="Label"
            Width="638px"></asp:Label></div>
    </form>
</body>
</html>
