<%@ Page Language="VB" AutoEventWireup="false" EnableEventValidation="false"  CodeFile="addAddress.aspx.vb" Inherits="PresentationLayer_masterrecord_addAddress" %>

<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <LINK href="../css/style.css" type="text/css" rel="stylesheet">
     <STYLE type="text/css">A:link { COLOR: #336666; TEXT-DECORATION: none }
	BODY { FONT-SIZE: 9pt; FONT-FAMILY: "Arial", "Verdana", "Tahoma"; BACKGROUND-COLOR: #ffffff }
	TD { FONT-SIZE: 9pt; FONT-FAMILY: Arial,Verdana,Tahoma }
	</STYLE>
</head>
<body>
    <form id="form1" runat="server">
        <div style="width: 100%">
            <table id="Table4" border="0" style="width: 100%">
                <tr>
                    <td style="width: 100%">
                        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%" width="100%">
                            <tr>
                                <td background="../graph/title_bg.gif" style="width: 1%">
                                    <img height="24" src="../graph/title1.gif" width="5" /></td>
                                <td background="../graph/title_bg.gif" class="style2" width="98%">
                                    <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="right" background="../graph/title_bg.gif" style="width: 1%">
                                    <img height="24" src="../graph/title_2.gif" width="5" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="width: 100%">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td background="../graph/title_bg.gif" class="style2" width="98%">
                                    <font color="red">
                                        <asp:Label ID="errlab" runat="server" Text="Label" Visible="false"></asp:Label></font>
                                        </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="width: 100%">
                        <table id="Table2" bgcolor="#b7e6e6" border="0" cellpadding="2" cellspacing="1" style="width: 100%">
                        <tr bgcolor="#ffffff">
<td colspan = 4>
                            <asp:Label ID="lblCompTop" runat="server" ForeColor="Red" Text="Label" Visible="True"></asp:Label>
</td>
             </tr>
             <tr bgcolor="#ffffff">
<td colspan = 4>
                        	&nbsp;
                     	</td>
               </tr>  
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width: 15%">
                                    <asp:Label ID="custidlab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 35%; color: red">
                                    <asp:TextBox ID="custid" runat="server" CssClass="textborder" Enabled="False" MaxLength="2"
                                        Width="88%"></asp:TextBox></td>
                                <td align="right" style="width: 15%">
                                    <asp:Label ID="statusLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 35%">
                                    <asp:DropDownList ID="status" runat="server" CssClass="textborder" Width="92%">
                                    </asp:DropDownList></td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width: 15%">
                                    <asp:Label ID="PIClab" runat="server" Text="Label"></asp:Label>&nbsp;</td>
                                <td align="left" style="width: 35%; color: red">
                                    <asp:TextBox ID="PIC" runat="server" CssClass="textborder" MaxLength="50" Width="88%"></asp:TextBox></td>
                                <td align="right" style="width: 15%">
                                    <asp:Label ID="AddressTypelab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 35%">
                                    <asp:DropDownList ID="AddressType" runat="server" CssClass="textborder"
                                        Width="92%">
                                    </asp:DropDownList></td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width: 15%">
                                    <asp:Label ID="rotypelab" runat="server" Text="Label"  visible ="false" ></asp:Label></td>
                                <td align="left" colspan="3" style="height: 28%">
                                    <asp:DropDownList ID="rotype" runat="server" AutoPostBack="True" CssClass="textborder"  visible ="false"
                                        Width="38%">
                                    </asp:DropDownList>&nbsp;
                                    <asp:Label ID="rouidLab" runat="server" Text="Label" Visible="False"></asp:Label><asp:TextBox
                                        ID="rouid" runat="server" CssClass="textborder" Enabled="False" Visible="False"
                                        Width="1%"></asp:TextBox></td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width: 15%">
                                    <asp:Label ID="Address1lab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" colspan="3" style="color: red">
                                    <asp:TextBox ID="Address1" runat="server" CssClass="textborder" MaxLength="100" Width="95%"></asp:TextBox> <strong>*</strong> </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width: 15%">
                                    <asp:Label ID="Address2lab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" colspan="3" style="color: red">
                                    <asp:TextBox ID="Address2" runat="server" CssClass="textborder" MaxLength="100" Width="95%"></asp:TextBox> <strong>*</strong> </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width: 15%">
                                    <asp:Label ID="CountryIDlab" runat="server" Text="Label"></asp:Label>
                                </td>
                                <td align="left" style="width: 35%; color: red">
                                    <asp:DropDownList ID="CountryID" runat="server" AutoPostBack="True" CssClass="textborder"
                                        Width="92%">
                                    </asp:DropDownList>
                                </td>
                                <td align="right" style="width: 15%">
                                    <asp:Label ID="POCodelab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 35%; color: red">
                                    <asp:TextBox ID="POCode" runat="server" CssClass="textborder" MaxLength="10" Width="88%"></asp:TextBox> <strong>*</strong> </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width: 15%">
                                    <asp:Label ID="StateIDlab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 35%; color: red">
                                    <asp:DropDownList ID="StateID" runat="server" AutoPostBack="True" CssClass="textborder"
                                        Width="92%">
                                    </asp:DropDownList></td>
                                <td align="right" style="width: 15%">
                                    <asp:Label ID="AreaIDlab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 35%">
                                    <asp:DropDownList ID="AreaID" runat="server" CssClass="textborder" Width="92%">
                                    </asp:DropDownList><asp:RequiredFieldValidator ID="areamsg" runat="server" ControlToValidate="AreaID" ForeColor="White"> <strong>*</strong> </asp:RequiredFieldValidator></td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width: 15%">
                                    <asp:Label ID="CreatedBylab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 35%; color: red">
                                    <asp:TextBox ID="CreatedBy" runat="server" CssClass="textborder" ReadOnly="true"
                                        Width="88%"></asp:TextBox></td>
                                <td align="right" style="width: 15%">
                                    &nbsp;<asp:Label ID="CreatedDatelab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 35%">
                                    <asp:TextBox ID="CreatedDate" runat="server" CssClass="textborder" Enabled="False"
                                        ReadOnly="true" Width="92%"></asp:TextBox>
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width: 15%">
                                    <asp:Label ID="ModiBylab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 35%; color: red">
                                    <asp:TextBox ID="ModiBy" runat="server" CssClass="textborder" ReadOnly="true" Width="88%"></asp:TextBox></td>
                                <td align="right" style="width: 15%">
                                    <asp:Label ID="ModifiedDatelab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 35%">
                                    <asp:TextBox ID="ModifiedDate" runat="server" CssClass="textborder" Enabled="False"
                                        ReadOnly="true" Width="92%"></asp:TextBox></td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="left" colspan="4">
                                    <asp:GridView ID="addressView" runat="server" AllowPaging="True" AllowSorting="True"
                                        Font-Size="Smaller" PageSize="2" Width="100%">
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                        <asp:RequiredFieldValidator ID="POCodeMsg1" runat="server" ControlToValidate="POCode"
                            ForeColor="White"> <strong>*</strong> </asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="address2msg" runat="server" ControlToValidate="Address2"
                            ForeColor="White"> <strong>*</strong> </asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="address1msg" runat="server" ControlToValidate="Address1"
                            ForeColor="White"> <strong>*</strong> </asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="AddressTypemsg" runat="server" ControlToValidate="AddressType"
                            ForeColor="White"> <strong>*</strong> </asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="countrymsg" runat="server" ControlToValidate="CountryID" ForeColor="White">*</asp:RequiredFieldValidator></td>
                </tr>
                <tr>
                    <td style="width: 100%; height: 52px;">
                    <font color=red><asp:Label ID="lblCompBottom" runat="server" Text="Label" Visible="true"></asp:Label></font>
                        <table id="Table3" border="0" cellpadding="1" cellspacing="1">
                            <tr>
                                <td align="center" style="width: 20%; height: 16px">
                                    &nbsp;<asp:LinkButton ID="LinkButton1" runat="server">save</asp:LinkButton></td>
                                <td align="center" style="height: 16px">
                                    &nbsp;<asp:LinkButton ID="HyperLink1" runat="server" CausesValidation="False">LinkButton</asp:LinkButton></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            &nbsp;<cc1:MessageBox ID="MessageBox1" runat="server" />
            <cc1:MessageBox ID="MessageBox2" runat="server" />
            <asp:RequiredFieldValidator ID="statmsg" runat="server" ControlToValidate="StateID" ForeColor="White"> <strong>*</strong> </asp:RequiredFieldValidator><br />
            &nbsp;
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                ShowSummary="False" />
            &nbsp;&nbsp;
        </div>
    </form>
</body>
</html>
