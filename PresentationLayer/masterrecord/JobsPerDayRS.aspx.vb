Imports System.Xml
Imports System.Web
Imports SQLDataAccess
Imports BusinessEntity
Imports System.Configuration
Imports System.Web.UI
Imports System.Data
Imports System.Data.SqlClient
Partial Class PresentationLayer_masterrecord_jobsperdayRS
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try

            Dim objXmlTr As New clsXml
            Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            Me.LinkButton1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Add") 'add new
            Me.LinkButton2.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Search") 'search
            Me.SVClab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASJOBS-0001")
            Dim strdate As String = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASJOBS-0002")
            strdate = strdate & "  " & objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Me.DateLab.Text = strdate
            Me.Label2.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Me.titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASJOBS-0007")
            Me.Label1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-NR")
            Me.Label1.Visible = False
            Me.addinfo.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-AR")
            Me.addinfo.Visible = False


            Me.dateBox.Text = Date.Today
            Me.TextBox1.Text = Date.Today
            '''''''''''''''''''''''added by xiao''''''''''''''''
            Dim jobsall As DataSet = createds()
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            jobView.DataSource = jobsall
            jobView.DataBind()
            checknorecord(jobsall)
            'page index changed

            jobView.AllowPaging = True
            jobView.AllowSorting = True

            Dim editcol As New CommandField
            editcol.EditText = objXmlTr.GetLabelName("EngLabelMsg", "BB-Edit") '"edit"
            editcol.HeaderImageUrl = "~/PresentationLayer/graph/edit.gif"
            editcol.ShowEditButton = True
            jobView.Columns.Add(editcol)
            jobView.DataBind()
            '''access control'''''''''''''''''''''''''''''''''''''''''''''''
            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "13")
            Me.LinkButton1.Enabled = purviewArray(0)
            editcol.Visible = purviewArray(2)


            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'add table head text
            If jobsall.Tables(0).Rows.Count <> 0 Then
                jobView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASJOBS-0001")
                jobView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASJOBS-0002")
                jobView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASJOBS-0006")
                jobView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASJOBS-0003")
                jobView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASJOBS-0004")

                Dim i As Integer
                For i = 1 To 5
                    jobView.HeaderRow.Cells(i).Font.Size = 8
                Next
            End If
        End If
        Dim datestyle1 As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle1

        HypCalStart.NavigateUrl = "javascript:DoCal(document.form1.dateBox);"
        HypCalEnd.NavigateUrl = "javascript:DoCal(document.form1.TextBox1);"
    End Sub

    Sub RequestFromValue()
        dateBox.Text = Request.Form("dateBox")
        TextBox1.Text = Request.Form("TextBox1")
    End Sub

#Region "create dataset "
    Public Function createds() As DataSet
        Dim JobsPerDayEntity As New clsJobsPerDayRS()
        'user name 
        If Session("userID") <> "" Then
            JobsPerDayEntity.UserName = Session("userID")
        End If

        '/////////////////////////////////////////////////////////
        Dim ctryIDPara As String = ""
        Dim cmpIDPara As String = ""
        Dim svcIDPara As String = ""
        Dim login_rank As Integer = Session("login_rank")
        If login_rank <> 0 Then
            ctryIDPara = Session("login_ctryID").ToString.ToUpper
            cmpIDPara = Session("login_cmpID").ToString.ToUpper
            svcIDPara = Session("login_svcID").ToString.ToUpper
        End If
        ''''''''''''''''''''''''''''''''''''''''''''''''''

        JobsPerDayEntity.Rank = login_rank
        JobsPerDayEntity.CmpId = cmpIDPara
        JobsPerDayEntity.CtryID = ctryIDPara
        If login_rank = 9 Then
            JobsPerDayEntity.LoginSvc = svcIDPara
        End If
        ' get serviceCenterid
        If (Trim(Me.SvcBox.Text) <> "") Then
            JobsPerDayEntity.ServiceCenterID = SvcBox.Text
        End If
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        If (Trim(dateBox.Text) <> "") Then
            Dim dateArr As Array = Trim(dateBox.Text.ToString()).Split("/")
            Dim datestr As String = dateArr(2) + "-" + dateArr(1) + "-" + dateArr(0)
            'jobsEntity.dtdate = datestr
            JobsPerDayEntity.dtdate1 = datestr
        Else
            JobsPerDayEntity.dtdate = Date.Today
            JobsPerDayEntity.dtdate1 = Date.Today

        End If

        If (Trim(Me.TextBox1.Text) <> "") Then
            Dim dateArr As Array = Trim(Me.TextBox1.Text.ToString()).Split("/")
            Dim datestr As String = dateArr(2) + "-" + dateArr(1) + "-" + dateArr(0)

            JobsPerDayEntity.dtdate = datestr
        Else
            JobsPerDayEntity.dtdate = Date.Today
            JobsPerDayEntity.dtdate1 = Date.Today
        End If
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Dim jobsall As DataSet = JobsPerDayEntity.GetJobs()
        Session("jobsViewDS") = jobsall
        Return jobsall
    End Function
#End Region
#Region "search  button  search  service center id"
    Protected Sub LinkButton2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton2.Click
        Me.jobView.PageIndex = 0
        Dim objXmlTr As New clsXml

        RequestFromValue()

        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        Dim jobsEntity As New clsJobsPerDayRS()
        checkinfo()

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Dim selDS As DataSet = createds()
        jobView.DataSource = selDS
        jobView.DataBind()

        checknorecord(selDS)
        jobView.AllowPaging = True
        jobView.AllowSorting = True
        'add table head text  
        If (selDS.Tables.Count > 0) Then
            If (selDS.Tables(0).Rows.Count <> 0) Then

                jobView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASJOBS-0001")
                jobView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASJOBS-0002")
                jobView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASJOBS-0006")
                jobView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASJOBS-0003")
                jobView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASJOBS-0004")
                'jobView.HeaderRow.Cells(6).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASJOBS-0005")
                'jobView.HeaderRow.Cells(7).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFBY")
                'jobView.HeaderRow.Cells(8).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFDATE")
                Dim i As Integer
                For i = 1 To 5
                    jobView.HeaderRow.Cells(i).Font.Size = 8
                Next
            End If
        End If
    End Sub
#End Region
#Region "if have no record "
    Public Function checknorecord(ByVal ds As DataSet) As Integer
        If ds.HasErrors Then
            If ds.Tables(0).Rows.Count = 0 Then
                Me.Label1.Visible = True
            Else
                Me.Label1.Visible = False
            End If

        End If
        If ds.Tables(0).Rows.Count = 0 Then
            Me.Label1.Visible = True
        Else
            Me.Label1.Visible = False
        End If

    End Function
#End Region
#Region "��ʾ��Ϣ"
    Public Function checkinfo() As Integer
        If Me.SvcBox.Text = "" Then
            Me.addinfo.Visible = True
        Else
            Me.addinfo.Visible = False
        End If
    End Function
#End Region


#Region " bond"

    Public Function databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        Dim row As DataRow
        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            NewItem.Text = row(0) & "-" & row(1)
            NewItem.Value = row(0)
            dropdown.Items.Add(NewItem)
        Next
    End Function

#End Region
    'page change
    Protected Sub jobView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles jobView.PageIndexChanging
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        jobView.PageIndex = e.NewPageIndex
        Dim jobsall As DataSet = Session("jobsViewDS")
        jobView.DataSource = jobsall
        jobView.DataBind()
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        If (jobsall.Tables.Count > 0) Then
            If (jobsall.Tables(0).Rows.Count <> 0) Then
                jobView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASJOBS-0001")
                jobView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASJOBS-0002")
                jobView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASJOBS-0006")
                jobView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASJOBS-0003")
                jobView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASJOBS-0004")

                Dim i As Integer
                For i = 1 To 5
                    jobView.HeaderRow.Cells(i).Font.Size = 8
                Next
            End If
        End If
    End Sub

    Protected Sub jobView_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles jobView.RowEditing
        'Dim ds As DataSet = Session("jobsViewDS")

        'Dim page As Integer = jobView.PageIndex * jobView.PageSize

        'Dim ctryid As String = ds.Tables(0).Rows(page + e.NewEditIndex).Item(0).ToString()
        Dim ctryid As String = Me.jobView.Rows(e.NewEditIndex).Cells(1).Text
        ctryid = Server.UrlEncode(ctryid)

        'Dim jdate As String = ds.Tables(0).Rows(page + e.NewEditIndex).Item(1).ToString
        Dim jdate As String = Me.jobView.Rows(e.NewEditIndex).Cells(2).Text
        jdate = Server.UrlEncode(jdate)
        Dim strTempURL As String = "ctryid=" + ctryid + "&jdate=" + jdate

        strTempURL = "~/PresentationLayer/masterrecord/mdfyjobsperdayRS.aspx?" + strTempURL
        Response.Redirect(strTempURL)

    End Sub

    Protected Sub JCalendar1_SelectedDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles JCalendar1.SelectedDateChanged
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        Dim jobsall As DataSet = Session("jobsViewDS")
        jobView.DataSource = jobsall
        jobView.DataBind()
        If jobsall.Tables(0).Rows.Count <> 0 Then
            jobView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASJOBS-0001")
            jobView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASJOBS-0002")
            jobView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASJOBS-0006")
            jobView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASJOBS-0003")
            jobView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASJOBS-0004")
            Dim i As Integer
            For i = 1 To 5
                jobView.HeaderRow.Cells(i).Font.Size = 8
            Next
        End If
    End Sub

    Protected Sub JCalendar2_SelectedDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles JCalendar2.SelectedDateChanged
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        Dim jobsall As DataSet = Session("jobsViewDS")
        jobView.DataSource = jobsall
        jobView.DataBind()
        If jobsall.Tables(0).Rows.Count <> 0 Then
            jobView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASJOBS-0001")
            jobView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASJOBS-0002")
            jobView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASJOBS-0006")
            jobView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASJOBS-0003")
            jobView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASJOBS-0004")
            Dim i As Integer
            For i = 1 To 5
                jobView.HeaderRow.Cells(i).Font.Size = 8
            Next
        End If
    End Sub

    Protected Sub JCalendar2_CalendarVisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles JCalendar2.CalendarVisibleChanged
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        Dim jobsall As DataSet = Session("jobsViewDS")
        jobView.DataSource = jobsall
        jobView.DataBind()
        If jobsall.Tables(0).Rows.Count <> 0 Then
            jobView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASJOBS-0001")
            jobView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASJOBS-0002")
            jobView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASJOBS-0006")
            jobView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASJOBS-0003")
            jobView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASJOBS-0004")
            Dim i As Integer
            For i = 1 To 5
                jobView.HeaderRow.Cells(i).Font.Size = 8
            Next
        End If
    End Sub

    Protected Sub JCalendar1_CalendarVisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles JCalendar1.CalendarVisibleChanged
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        Dim jobsall As DataSet = Session("jobsViewDS")
        jobView.DataSource = jobsall
        jobView.DataBind()
        If jobsall.Tables(0).Rows.Count <> 0 Then
            jobView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASJOBS-0001")
            jobView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASJOBS-0002")
            jobView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASJOBS-0006")
            jobView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASJOBS-0003")
            jobView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASJOBS-0004")
            Dim i As Integer
            For i = 1 To 5
                jobView.HeaderRow.Cells(i).Font.Size = 8
            Next
        End If
    End Sub
End Class
