<%@ Page Language="VB" AutoEventWireup="false" EnableEventValidation="false" CodeFile="addarea.aspx.vb"
    Inherits="PresentationLayer_masterrecord_addarea" %>

<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Area Add</title>
    <link href="../css/style.css" type="text/css" rel="stylesheet">
</head>
<body>
    <form id="form1" runat="server">
        <table border="0" id="countrytab" style="width: 100%">
            <tr>
                <td style="width: 95%">
                    <table cellspacing="0" cellpadding="0" width="100%" border="0" style="width: 100%">
                        <tr>
                            <td width="1%" background="../graph/title_bg.gif">
                                <img height="24" src="../graph/title1.gif" width="5"></td>
                            <td class="style2" background="../graph/title_bg.gif" style="width: 100%">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="right" background="../graph/title_bg.gif" style="width: 4%">
                                <img height="24" src="../graph/title_2.gif" width="5"></td>
                        </tr>
                    </table>
            </tr>
             <tr>
           <td style="width: 100%">
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>					
							<TD class="style2" background="../graph/title_bg.gif" style="width: 100%">
                                <font color=red style="width: 100%"><asp:Label ID="errlab" runat="server" Text="Label" Visible=false></asp:Label></font>
                               </TD>
						</TR>
			 </TABLE>
           </tr>
            <tr>
                <td style="width: 100%">
                    <table id="area" cellspacing="1" cellpadding="2" bgcolor="#b7e6e6" border="0" style="width: 100%">
                     <tr bgcolor="#ffffff">
<td colspan = 4>
                            <asp:Label ID="lblCompTop" runat="server" ForeColor="Red" Text="Label" Visible="True"></asp:Label>
</td>
             </tr>
             <tr bgcolor="#ffffff">
<td colspan = 4>
                        	&nbsp;
                     	</td>
               </tr> 
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 20%">
                                &nbsp;<asp:Label ID="ctryidlab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%">
                                <asp:DropDownList ID="ctryid" runat="server" CssClass="textborder" AutoPostBack="True" width ="90%">
                                </asp:DropDownList>
                                <span style="color: #ff0000"><strong>*</strong></span></td>
                            <td align="right" width="15%">
                                <asp:Label ID="staidLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 39%">
                                <asp:DropDownList ID="staid" runat="server" CssClass="textborder" width ="90%">
                                </asp:DropDownList>
                                <span style="color: #ff0000"><strong>*</strong></span></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 20%">
                                <asp:Label ID="areaidLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%">
                                <asp:TextBox ID="areaidbox" runat="server" CssClass="textborder" MaxLength="10"></asp:TextBox>
                                <font
                                    color="red"><strong>*</strong></font>
                            </td>
                            <td align="right" width="15%">
                                <asp:Label ID="areanmlab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 39%">
                                <asp:TextBox ID="areanm" runat="server" CssClass="textborder" MaxLength="50"></asp:TextBox>
                                <font
                                    color="red"><strong>*</strong></font>
                            </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 20%">
                                <asp:Label ID="alterLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" width="85%" colspan="3">
                                <asp:TextBox ID="alter" runat="server" CssClass="textborder" MaxLength="50"></asp:TextBox>
                            </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 20%;">
                                <asp:Label ID="serviceidlab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="height: 22px; width: 35%;">
                                <asp:DropDownList ID="serviceid" runat="server" CssClass="textborder" width ="90%">
                                </asp:DropDownList></td>
                            <td align="right" width="15%" style="height: 22px">
                                <asp:Label ID="statusLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="height: 22px; width: 39%;">
                                <asp:DropDownList ID="ctrystat" runat="server" CssClass="textborder">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 20%">
                                <asp:Label ID="creatby" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%">
                                <asp:TextBox ID="creatbybox" runat="server" CssClass="textborder" ReadOnly="true" MaxLength="20"></asp:TextBox></td>
                            <td align="right" width="15%">
                                <asp:Label ID="creatdate" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 39%">
                                <asp:TextBox ID="creatdtbox" runat="server" CssClass="textborder" ReadOnly="true"></asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 20%;">
                                <asp:Label ID="modfyby" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="height: 11px; width: 35%;">
                                <asp:TextBox ID="modfybybox" runat="server" CssClass="textborder" ReadOnly="true" MaxLength="20"></asp:TextBox></td>
                            <td align="right" width="15%" style="height: 11px">
                                <asp:Label ID="mdfydate" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 39%; height: 11px;">
                                <asp:TextBox ID="mdfydtbox" runat="server" CssClass="textborder" ReadOnly="true"></asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            
                            <td align="left" colspan="4" width="35%" style="height: 34px">
                            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                <tr>
                                    <td class="style2" background="../graph/title_bg.gif" style="width: 110%; height: 14px;">
                                        <font   style="width: 100%">
                                            <asp:Label ID="potitle" runat="server" Text="Label"></asp:Label></font></td>
                                </tr>
                            </table>
                            
                            </td>
                        </tr>
                    
                        
            <tr bgcolor="#ffffff">
                <td align="right" style="width: 20%; height: 30px;">
                    <asp:Label ID="POlab" runat="server" Text="Label"></asp:Label></td>
                <td align="left" width="35%" colspan="3" style="height: 30px">
                    <asp:TextBox ID="PObox" runat="server" CssClass="textborder" MaxLength="10"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="poerr" runat="server" ControlToValidate="PObox"
                        ForeColor="White" ValidationExpression="\d{0,20}">*</asp:RegularExpressionValidator>
                    <asp:DropDownList ID="POstat" runat="server" CssClass="textborder" Width="104px">
                    </asp:DropDownList>
                    <asp:LinkButton ID="addButton" runat="server"></asp:LinkButton></td>
            </tr>
        </table>
        &nbsp;&nbsp;
        <div>
            <asp:GridView ID="POView" runat="server">
            </asp:GridView>
        </div>
        <table>
                  <tr>
                <td style="height: 37px; width: 469px;">
                <font color=red><asp:Label ID="lblCompBottom" runat="server" Text="Label" Visible="true"></asp:Label></font>
                    <table id="Table1" cellspacing="1" cellpadding="1" border="0">
                        <tr>
                            <td style="width: 20%; height: 14px;" align="center">
                                &nbsp;<asp:LinkButton ID="saveButton" runat="server"></asp:LinkButton></td>
                            <td align="center" style="width: 205; height: 14px">
                                &nbsp;<asp:HyperLink ID="cancelLink" runat="server" NavigateUrl="~/PresentationLayer/masterrecord/locale.aspx">[cancelLink]</asp:HyperLink></td>
                        <td align=center><asp:HyperLink ID="AddLink" runat="server" >[Add]</asp:HyperLink></td>

</tr>
                    </table>
                </td>
            </tr>
        </table>
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
            ShowSummary="False" />
        <asp:RequiredFieldValidator ID="ctryiderr" runat="server" ControlToValidate="ctryid"
            ForeColor="White"></asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator ID="areiderr" runat="server" ControlToValidate="areaidbox"
            ForeColor="White"></asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator ID="arenmerr" runat="server" ControlToValidate="areanm"
            ErrorMessage=" " ForeColor="White">*</asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator ID="staiderr" runat="server" ControlToValidate="staid"
            ForeColor="White"></asp:RequiredFieldValidator>
            
            </table>
        <cc1:MessageBox ID="MessageBox1" runat="server"></cc1:MessageBox>
        <cc1:MessageBox ID="MessageBox2" runat="server"></cc1:MessageBox>
    </form>
</body>
</html>
