﻿Imports BusinessEntity
Imports System.Configuration
Imports System.Data


Partial Class PresentationLayer_masterrecord_megasvcgroupsetup
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Page.IsPostBack) Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Response.Redirect("~/PresentationLayer/logon.aspx")
                End If
            Catch ex As Exception
                Response.Redirect("~/PresentationLayer/logon.aspx")
            End Try
            BindGrid()
        End If
    End Sub

    Protected Sub BindGrid()
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        lblGroup.Text = "Service Group"
        lblModel.Text = "Model Type"
        lblMonth.Text = "No. of Months"
        lblstat.Text = "Status"
        addButton.Text = "Add"
        searchButton.Text = "Search"
        titleLab.Text = "Service Internal Setup"

        Me.addinfo.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-AR") 'error message label on top
        Me.addinfo.Visible = False
        Me.Label1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-NR") 'error message label at bottom
        Me.Label1.Visible = False

        Dim _class As New ClsMegaSVCGroupSetup()

        _class.ModifiedBy = Session("userID")
        _class.Stat = ddlstat.SelectedValue

        Dim getdata As DataSet = _class.GetData()
        ctryView.DataSource = getdata

        checknorecord(getdata)

        ctryView.AllowPaging = True
        ctryView.AllowSorting = True
        ctryView.DataBind()

    End Sub

#Region "if have no record "
    Public Function checknorecord(ByVal ds As DataSet) As Integer

        If ds.HasErrors Then
            If ds.Tables(0).Rows.Count = 0 Then
                Me.Label1.Visible = True
            Else
                Me.Label1.Visible = False

            End If

        End If
        If ds.Tables(0).Rows.Count = 0 Then
            Me.Label1.Visible = True
        Else
            Me.Label1.Visible = False

        End If

    End Function
#End Region
#Region "if have all record"
    Public Function checkinfo() As Integer
        If Me.txtGroup.Text = "" And Me.txtModel.Text = "" And Me.txtMonths.Text = "" Then
            Me.addinfo.Visible = True
        Else
            Me.addinfo.Visible = False
        End If
    End Function
#End Region
    Protected Sub LinkButton2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles searchButton.Click
        checkinfo()
        ctryView.PageIndex = 0
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Dim _class As New ClsMegaSVCGroupSetup()
        If (Trim(txtGroup.Text) <> "") Then
            _class.Group = txtGroup.Text
        End If
        If (Trim(txtModel.Text) <> "") Then
            _class.Model = txtModel.Text
        End If
        If (Trim(txtMonths.Text) <> "") Then
            _class.Months = txtMonths.Text
        End If
        If (Trim(ddlstat.SelectedValue) <> "") Then
            _class.Stat = ddlstat.SelectedValue
        End If

        _class.ModifiedBy = Session("userID")

        Dim selDS As DataSet = _class.GetData()
        ctryView.DataSource = selDS
        ctryView.DataBind()

        checknorecord(selDS)

    End Sub

    Protected Sub ctryView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles ctryView.PageIndexChanging
        ctryView.PageIndex = e.NewPageIndex
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Dim _class As New ClsMegaSVCGroupSetup()
        If (Trim(txtGroup.Text) <> "") Then
            _class.Group = txtGroup.Text
        End If
        If (Trim(txtModel.Text) <> "") Then
            _class.Model = txtModel.Text
        End If
        If (Trim(txtMonths.Text) <> "") Then
            _class.Months = txtMonths.Text
        End If
        If (Trim(ddlstat.SelectedValue) <> "") Then
            _class.Stat = ddlstat.SelectedValue
        End If

        _class.ModifiedBy = Session("userID")

        Dim ctryall As DataSet = _class.GetData()
        ctryView.DataSource = ctryall
        ctryView.DataBind()

    End Sub

    Protected Sub ctryView_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles ctryView.RowEditing

        Dim lblid As String = ctryView.DataKeys(e.NewEditIndex).Value.ToString()
        lblid = Server.UrlEncode(lblid)

        Dim strTempURL As String = "id=" + lblid
        strTempURL = "~/PresentationLayer/masterrecord/megasvcgroupsetup_modify.aspx?" + strTempURL
        Response.Redirect(strTempURL)
    End Sub


    Protected Sub ctryView_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub
    Protected Sub ctryView_SelectedIndexChanged1(sender As Object, e As EventArgs) Handles ctryView.SelectedIndexChanged

    End Sub
    Protected Sub addButton_Click(sender As Object, e As EventArgs) Handles addButton.Click

    End Sub
End Class
