<%@ Page Language="VB" AutoEventWireup="false" EnableEventValidation ="false"  CodeFile="ModifyPartRequire.aspx.vb" Inherits="PresentationLayer_masterrecord_ModifyPartRequire" %>

<%@ Register Assembly="JCalendar" Namespace="JCalendar" TagPrefix="cc2" %>

<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Modify Part</title>
    <LINK href="../css/style.css" type="text/css" rel="stylesheet">
    <script language="JavaScript" src="../js/common.js"></script> 
</head>
<body>
    <form id="AddPartRequire" runat="server">
      <TABLE border="0" id=TABLE1 width="100%">
           <tr>
           <td style="height: 39px; width: 100%;">
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>
							<TD width="1%" background="../graph/title_bg.gif"><IMG height="24" src="../graph/title1.gif" width="5"></TD>
							<TD class="style2" width="98%" background="../graph/title_bg.gif" style="width: 100%">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></TD>
							<TD align="right" width="1%" background="../graph/title_bg.gif"><IMG height="24" src="../graph/title_2.gif" width="5"></TD>
						</TR>
			 </TABLE>
           </tr>
           <tr>
           <td style="width: 100%">
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>					
							<TD class="style2" width="98%" background="../graph/title_bg.gif">
                                <font color=red style="width: 100%"><asp:Label ID="errlab" runat="server" Text="Label" Visible=false></asp:Label></font></TD>
						</TR>
			 </TABLE>
               <asp:Label ID="Label8" runat="server" ForeColor="Red"></asp:Label>
           </tr>
			<TR>
				<TD style="width: 100%">
          <table id="AddPartRequiretab" border="0" style="width: 100%">
                <tr>
                <td style="width: 100%" >
                        <table id="PartRequire" bgcolor="#b7e6e6" border="0" cellpadding="2" cellspacing="1" width="100%">
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width: 10%; height: 34px">
                                    <asp:Label ID="countryidlab" runat="server" Text="Label"></asp:Label>&nbsp;</td>
                                <td align="left" style="width: 40%; height: 34px">
                                    <asp:DropDownList ID="countryiddrpd" runat="server" CssClass="textborder" Height="24px"
                                        Style="width: 78%" Width="104px">
                                    </asp:DropDownList>
                                    <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="*" Font-Bold="True"></asp:Label>
                                    <asp:RequiredFieldValidator ID="countryerr" runat="server" ControlToValidate="countryiddrpd" Width="8px" ForeColor="White">*</asp:RequiredFieldValidator>
                                    &nbsp;&nbsp;
                                </td>
                                <td align="right" style="width: 10%; height: 34px">
                                    <asp:Label ID="CategoryIDLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 40%; height: 30px">
                                    <asp:DropDownList ID="CategoryIDdrpd" runat="server" CssClass="textborder" Style="width: 78%"
                                        Width="248px">
                                    </asp:DropDownList>
                                    <asp:Label ID="Label6" runat="server" ForeColor="Red" Text="*" Width="8px" Font-Bold="True"></asp:Label><asp:RequiredFieldValidator
                                        ID="partcateerr" runat="server" ControlToValidate="CategoryIDdrpd" Width="16px" ForeColor="White">*</asp:RequiredFieldValidator>
                                    &nbsp;&nbsp;
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width: 10%; height: 30px">
                                    &nbsp;<asp:Label ID="partidlab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" colspan="3" style="width: 90%; height: 30px" width="85%">
                                    <asp:TextBox ID="partidtbox" runat="server" CssClass="textborder" Style="width: 32%"
                                        Width="112px" MaxLength="10"></asp:TextBox>
                                    <asp:Label ID="Label2" runat="server" ForeColor="Red" Text="*" Width="8px" Font-Bold="True"></asp:Label>
                                    &nbsp;<asp:RequiredFieldValidator ID="partiderr" runat="server" ControlToValidate="partidtbox"
                                        Width="16px" ForeColor="White">*</asp:RequiredFieldValidator>&nbsp;
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width: 10%; height: 30px">
                                    &nbsp;<asp:Label ID="partnameLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" colspan="3" style="width: 80%; height: 30px">
                                    <asp:TextBox ID="partnametbox" runat="server" CssClass="textborder" Style="width: 89%"
                                        Width="136px" MaxLength="50"></asp:TextBox>
                                    <asp:Label ID="Label3" runat="server"
                                            ForeColor="Red" Text="*" Width="8px" Font-Bold="True"></asp:Label><asp:RequiredFieldValidator ID="partnameerr"
                                                runat="server" ControlToValidate="partnametbox" Width="16px" ForeColor="White">*</asp:RequiredFieldValidator>&nbsp;</td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width: 10%; height: 30px">
                                    <asp:Label ID="partanamelab" runat="server" Text="Label"></asp:Label>&nbsp;</td>
                                <td align="left" colspan="3" style="width: 90%; height: 30px">
                                    <asp:TextBox ID="partanametbox" runat="server" CssClass="textborder" Style="width: 89%"
                                        Width="136px" MaxLength="50"></asp:TextBox>&nbsp;&nbsp; &nbsp;
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width: 10%; height: 30px" id="#cal">
                                    <asp:Label ID="FiniProdulab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 40%; height: 30px">
                                    <asp:DropDownList ID="FiniProdudrpd" runat="server" CssClass="textborder" Style="width: 78%"
                                        Width="128px">
                                    </asp:DropDownList></td>
                                <td align="right" style="width: 10%; height: 30px">
                                    <asp:Label ID="ModelIDlab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 40%; height: 30px">
                                    <asp:DropDownList ID="ModelIDdrpd" runat="server" CssClass="textborder" Style="width: 78%"
                                        Width="248px">
                                    </asp:DropDownList>
                                    <asp:Label ID="Label7" runat="server" ForeColor="Red" Text=""
                                        Width="8px" Font-Bold="True"></asp:Label><asp:RequiredFieldValidator ID="modelerr"
                                            runat="server" ControlToValidate="ModelIDdrpd" Enabled =false  Width="16px" ForeColor="White">*</asp:RequiredFieldValidator></td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width: 10%; height: 22px">
                                    <asp:Label ID="uomlab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 40%; height: 22px">
                                    <asp:DropDownList ID="uomdrpd" runat="server" CssClass="textborder" Style="width: 78%"
                                        Width="128px">
                                    </asp:DropDownList></td>
                                <td align="right" style="width: 10%; height: 22px">
                                    <asp:Label ID="TradingItemlab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 40%; height: 22px">
                                    <asp:DropDownList ID="TradingItemdrpd" runat="server" CssClass="textborder" Style="width: 78%"
                                        Width="128px">
                                    </asp:DropDownList></td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width: 10%; height: 30px">
                                    <asp:Label ID="EffectDatelab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 40%; height: 30px">
                                    <asp:TextBox ID="EffectDatetbox" runat="server" CssClass="textborder" Style="position: static; width: 70%;" Width="130px" ReadOnly="True"></asp:TextBox>&nbsp;<asp:HyperLink
                                        ID="HypCal" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                                        ToolTip="Choose a Date">Choose a Date</asp:HyperLink>
                                    <cc2:JCalendar ID="JCalendar1" runat="server" ControlToAssign="EffectDatetbox" ImgURL="~/PresentationLayer/graph/calendar.gif" Visible="False" />
                                    <asp:Label ID="Label4" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                <td align="right" style="width: 10%; height: 30px">
                                    <asp:Label ID="ObsolDatelab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 40%; height: 30px">
                                    <asp:TextBox ID="ObsolDatetbox" runat="server" CssClass="textborder" MaxLength="2" Width="72%" ReadOnly="True" style="width: 70%"></asp:TextBox>&nbsp;<asp:HyperLink
                                        ID="HypCal2" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                                        ToolTip="Choose a Date">Choose a Date</asp:HyperLink>
                                    <cc2:JCalendar ID="JCalendar2" runat="server" ControlToAssign="ObsolDatetbox" ImgURL="~/PresentationLayer/graph/calendar.gif" Visible="False" />
                                    <asp:Label ID="Label5" runat="server" ForeColor="Red" Height="8px" Text="*" Width="1px" Font-Bold="True"></asp:Label></td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width: 10%; height: 30px">
                                    <asp:Label ID="suppnamelab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" colspan="3" style="width: 90%; height: 30px">
                                    <asp:TextBox ID="suppnametbox" runat="server" CssClass="textborder" Style="width: 89%"
                                        Width="352px" MaxLength="50"></asp:TextBox></td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width: 10%; height: 30px">
                                    <asp:Label ID="commonpartlab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 40%; height: 31px">
                                    <asp:DropDownList ID="commonpartdrpd" runat="server" CssClass="textborder" Style="width: 78%"
                                        Width="128px">
                                    </asp:DropDownList></td>
                                <td align="right" style="width: 10%; height: 30px">
                                    <asp:Label ID="ServiceItemlab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 40%; height: 30px">
                                    <asp:DropDownList ID="ServiceItemdrpd" runat="server" CssClass="textborder" Style="width: 78%"
                                        Width="176px">
                                    </asp:DropDownList></td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width: 10%; height: 30px">
                                    <asp:Label ID="KitItemlab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 40%; height: 30px">
                                    <asp:DropDownList ID="KitItemdrpd" runat="server" CssClass="textborder" Style="width: 78%"
                                        Width="128px" AutoPostBack="True">
                                    </asp:DropDownList></td>
                                <td align="right" style="width: 10%; height: 30px">
                                    <asp:Label ID="Statuslab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 40%; height: 30px">
                                    <asp:DropDownList ID="Statusdrpd" runat="server" CssClass="textborder" Style="width: 78%"
                                        Width="176px">
                                    </asp:DropDownList></td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width: 10%; height: 30px">
                                    <asp:Label ID="remarkslab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" colspan="3" style="width: 90%; height: 30px">
                                    <asp:TextBox ID="remarkstbox" runat="server" CssClass="textborder"
                                        Width="92%" Height="40px" MaxLength="400" style="width: 89%"></asp:TextBox>&nbsp;
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width: 10%; height: 30px">
                                    <asp:Label ID="createbylab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 40%; height: 30px">
                                    <asp:TextBox ID="creatbytbox" runat="server" CssClass="textborder" Style="width: 78%"
                                        Width="144px" ReadOnly="True"></asp:TextBox></td>
                                <td align="right" style="width: 10%; height: 30px">
                                    <asp:Label ID="creatdatelab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 40%; height: 30px">
                                    <asp:TextBox ID="creatdatetbox" runat="server" CssClass="textborder" MaxLength="2"
                                        Style="width: 78%" Width="73%" ReadOnly="True"></asp:TextBox>
                                    </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width: 10%; height: 30px">
                                    <asp:Label ID="modifybylab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 40%; height: 30px">
                                    <asp:TextBox ID="modifybytbox" runat="server" CssClass="textborder" Style="width: 78%"
                                        Width="144px" ReadOnly="True"></asp:TextBox>
                                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                </td>
                                <td align="right" style="width: 10%; height: 30px">
                                    <asp:Label ID="modifydatelab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 40%; height: 30px">
                                    <asp:TextBox ID="modifydatetbox" runat="server" CssClass="textborder" MaxLength="2"
                                        Style="width: 78%" Width="72%" ReadOnly="True"></asp:TextBox></td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="left" colspan="4" style="height: 39px">
                                    <asp:GridView ID="kititemview" runat="server" AllowPaging="True"
                                        Font-Size="Smaller" Style="width: 95%" Width="100%">
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label9" runat="server" ForeColor="Red"></asp:Label></td>
                </tr>
              <tr>
                  <td style="width: 99%">
                                    <asp:LinkButton ID="saveButton" runat="server">saveb</asp:LinkButton>
                      &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/PresentationLayer/masterrecord/PartRequire.aspx">HyperLink</asp:HyperLink>
                      &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                      <asp:LinkButton ID="Addkit" runat="server" CausesValidation="False" Enabled="False"
                          Width="56px">LinkB</asp:LinkButton>&nbsp;
                  </td>
              </tr>
            </table>
                   </td>
                </tr>
            </table>
                    <cc1:messagebox id="MessageBox1" runat="server"></cc1:messagebox>
                    <cc1:messagebox id="MessageBox2" runat="server"></cc1:messagebox>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                ShowSummary="False" />
       </form>
</body>
</html>

