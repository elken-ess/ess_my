Imports BusinessEntity
Imports System.Configuration
Imports System.Xml
Imports System.Data
Partial Class PresentationLayer_masterrecord_ServiceCenter
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Page.IsPostBack) Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try

            BindGrid()
        End If
    End Sub
    Protected Sub BindGrid()
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        If Not Page.IsPostBack Then
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BROSERVICE")
            ServiceCenterIDlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0001")
            ServiceCenterNamelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0002")
            ctryStat.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Status")

            LinkButton1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Add")
            search.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Search")
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.addinfo.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-AR")
            Me.addinfo.Visible = False
            Me.Label1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-NR")
            Me.Label1.Visible = False
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim service As New clsServiceCente()
            service.username = Session("userID").ToString.ToUpper() + Session("username").ToString.ToUpper()
            'create the dropdownlist
            Dim cntryStat As New clsrlconfirminf()

            Dim statParam As ArrayList = New ArrayList
            statParam = cntryStat.searchconfirminf("STATUS")
            Dim count As Integer
            Dim statid As String
            Dim statnm As String
            For count = 0 To statParam.Count - 1
                statid = statParam.Item(count)
                statnm = statParam.Item(count + 1)
                count = count + 1
                If statid.Equals("ACTIVE") Or statid.Equals("OBSOLETE") Then
                    ctryStatDrop.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                End If
            Next




            Dim ServiceCenterEntity As New clsServiceCente()
            Dim rank As String = Session("login_rank")
            If rank <> 0 Then
                ServiceCenterEntity.ctryid = Session("login_ctryID")
                ServiceCenterEntity.compid = Session("login_cmpID")
                ServiceCenterEntity.svcid = Session("login_svcID")

            End If
            ServiceCenterEntity.rank = rank
            ServiceCenterEntity.Status = "ACTIVE"
            Dim centryall As DataSet = ServiceCenterEntity.GetServiceCenter()
            ctryView.DataSource = centryall

            ctryView.AllowPaging = True
            ctryView.AllowSorting = True
            checknorecord(centryall)
            'Dim editcol As System.Web.UI.WebControls.HyperLinkField = New HyperLinkField()
            'editcol.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Edit")
            'editcol.DataNavigateUrlFormatString = "~/PresentationLayer/masterrecord/modifyServiceCenter.aspx?centryid={0}"
            'editcol.HeaderImageUrl = "~/PresentationLayer/graph/edit.gif"
            'If centryall.Tables(0).Rows.Count >= 1 Then
            '    Dim centrid As String = centryall.Tables.Item(0).Columns(0).ColumnName.ToString()
            '    Dim NavigateUrls As Array = Array.CreateInstance(GetType(String), 1)
            '    NavigateUrls(0) = centrid
            '    editcol.DataNavigateUrlFields = NavigateUrls
            'End If
            'ctryView.Columns.Add(editcol)

            Dim editcol As New CommandField
            editcol.EditText = objXmlTr.GetLabelName("EngLabelMsg", "BB-Edit") '"edit"
            editcol.HeaderImageUrl = "~/PresentationLayer/graph/edit.gif"
            editcol.ShowEditButton = True
            ctryView.Columns.Add(editcol)
            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "07")
            Me.LinkButton1.Enabled = purviewArray(0)
            'Me.search.Visible = purviewArray(2)
            editcol.Visible = purviewArray(2)
            'Me.ctryView.Visible = purviewArray(2)
            ctryView.DataBind()

            If centryall.Tables(0).Rows.Count >= 1 Then
                ctryView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "SVCRID")
                ctryView.HeaderRow.Cells(1).Font.Size = 8
                ctryView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "SVCRNAME")
                ctryView.HeaderRow.Cells(2).Font.Size = 8
                ctryView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "ALTERNAME")
                ctryView.HeaderRow.Cells(3).Font.Size = 8
                ctryView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "STATUS")
                ctryView.HeaderRow.Cells(4).Font.Size = 8
                


            End If
        End If
    End Sub



    Protected Sub search_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles search.Click
        checkinfo()
        ctryView.PageIndex = 0
        Dim ServiceCenterEntity As New clsServiceCente()
        If (Trim(ServiceCenterID.Text) <> "") Then
            ServiceCenterEntity.ServiceCenterID = ServiceCenterID.Text
        End If
        If (Trim(ServiceCenterName.Text) <> "") Then
            ServiceCenterEntity.ServiceCenterName = ServiceCenterName.Text
        End If
        If (ctryStatDrop.SelectedValue().ToString() <> "") Then
            ServiceCenterEntity.Status = ctryStatDrop.SelectedItem.Value.ToString()
        End If
        Dim rank As String = Session("login_rank")
        If rank <> 0 Then
            ServiceCenterEntity.ctryid = Session("login_ctryID")
            ServiceCenterEntity.compid = Session("login_cmpID")
            ServiceCenterEntity.svcid = Session("login_svcID")

        End If
        ServiceCenterEntity.rank = rank
        Dim selDS As DataSet = ServiceCenterEntity.GetServiceCenter()
        ctryView.DataSource = selDS
        ctryView.DataBind()

        checknorecord(selDS)
        Dim count As Integer = selDS.Tables(0).Rows.Count

        If selDS.Tables(0).Rows.Count >= 1 Then
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            ctryView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "SVCRID")
            ctryView.HeaderRow.Cells(1).Font.Size = 8
            ctryView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "SVCRNAME")
            ctryView.HeaderRow.Cells(2).Font.Size = 8
            ctryView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "ALTERNAME")
            ctryView.HeaderRow.Cells(3).Font.Size = 8
            'ctryView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "PICNAME")
            'ctryView.HeaderRow.Cells(4).Font.Size = 8
            'ctryView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "PICMOB")
            'ctryView.HeaderRow.Cells(5).Font.Size = 8
            ctryView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "STATUS")
            ctryView.HeaderRow.Cells(4).Font.Size = 8

        End If
    End Sub

    Protected Sub ctryView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles ctryView.PageIndexChanging

        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
       


        ctryView.PageIndex = e.NewPageIndex
        'Dim priceidall As DataSet = Session("ServiceCenterView")
        Dim ServiceCenterEntity As New clsServiceCente()
        If (Trim(ServiceCenterID.Text) <> "") Then
            ServiceCenterEntity.ServiceCenterID = ServiceCenterID.Text
        End If
        If (Trim(ServiceCenterName.Text) <> "") Then
            ServiceCenterEntity.ServiceCenterName = ServiceCenterName.Text
        End If
        If (ctryStatDrop.SelectedValue().ToString() <> "") Then
            ServiceCenterEntity.Status = ctryStatDrop.SelectedItem.Value.ToString()
        End If
        Dim rank As String = Session("login_rank")
        If rank <> 0 Then
            ServiceCenterEntity.ctryid = Session("login_ctryID")
            ServiceCenterEntity.compid = Session("login_cmpID")
            ServiceCenterEntity.svcid = Session("login_svcID")

        End If
        ServiceCenterEntity.rank = rank
        Dim selDS As DataSet = ServiceCenterEntity.GetServiceCenter()
        ctryView.DataSource = selDS
        ctryView.DataBind()
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        ctryView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "SVCRID")
        ctryView.HeaderRow.Cells(1).Font.Size = 8
        ctryView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "SVCRNAME")
        ctryView.HeaderRow.Cells(2).Font.Size = 8
        ctryView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "ALTERNAME")
        ctryView.HeaderRow.Cells(3).Font.Size = 8
        'ctryView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "PICNAME")
        'ctryView.HeaderRow.Cells(4).Font.Size = 8
        'ctryView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "PICMOB")
        'ctryView.HeaderRow.Cells(5).Font.Size = 8
        ctryView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "STATUS")
        ctryView.HeaderRow.Cells(4).Font.Size = 8
        
    End Sub

 
    Protected Sub ctryView_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles ctryView.RowEditing

        Dim centrid As String = Me.ctryView.Rows(e.NewEditIndex).Cells(1).Text
        centrid = Server.UrlEncode(centrid)
        'Dim ctrystat As String = ds.Tables(0).Rows(cntView.PageIndex * cntView.PageSize + e.NewEditIndex).Item(3).ToString
        'ctrystat = Server.UrlEncode(ctrystat)
        Dim strTempURL As String = "centrid=" + centrid
        strTempURL = "~/PresentationLayer/masterrecord/modifyServiceCenter.aspx?" + strTempURL
        Response.Redirect(strTempURL)

        'If centryall.Tables(0).Rows.Count >= 1 Then
        '    Dim centrid As String = centryall.Tables.Item(0).Columns(0).ColumnName.ToString()
        '    Dim NavigateUrls As Array = Array.CreateInstance(GetType(String), 1)
        '    NavigateUrls(0) = centrid
        '    editcol.DataNavigateUrlFields = NavigateUrls
        'End If
    End Sub
#Region "��ʾ��Ϣ"
    Public Function checkinfo() As Integer
        If Me.ServiceCenterID.Text = "" And Me.ServiceCenterName.Text = "" Then
            Me.addinfo.Visible = True
        Else
            Me.addinfo.Visible = False
        End If
    End Function
#End Region
#Region "if have no record "
    Public Function checknorecord(ByVal ds As DataSet) As Integer
        If ds.HasErrors Then
            If ds.Tables(0).Rows.Count = 0 Then
                Me.Label1.Visible = True
                'Me.addinfo.Visible = False
            Else
                Me.Label1.Visible = False

            End If

        End If
        If ds.Tables(0).Rows.Count = 0 Then
            Me.Label1.Visible = True
            'Me.addinfo.Visible = False
        Else
            Me.Label1.Visible = False

        End If

    End Function
#End Region
End Class
