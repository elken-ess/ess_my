<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PriceSetUp.aspx.vb" Inherits="PresentationLayer_masterrecord_PriceSetUp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Price Setup - Search</title>
    
     <style type="text/css">A:link { COLOR: #336666; TEXT-DECORATION: none }
	BODY { FONT-SIZE: 9pt; FONT-FAMILY: "Arial", "Verdana", "Tahoma"; BACKGROUND-COLOR: #ffffff }
	TD { FONT-SIZE: 9pt; FONT-FAMILY: Arial,Verdana,Tahoma }
	</style>
	<link href="../css/style.css" type="text/css" rel="stylesheet">
</head>
<body id="PriceIDView">
    <form id="form1" runat="server">
   
    <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>
							<TD width="1%" background="../graph/title_bg.gif"><IMG height="24" src="../graph/title1.gif" width="5"></TD>
							<TD class="style2" width="98%" background="../graph/title_bg.gif">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></TD>
							<TD align="right" width="1%" background="../graph/title_bg.gif"><IMG height="24" src="../graph/title_2.gif" width="5"></TD>
						</TR>
	  </TABLE>
	   <tr>
           <td>
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<tr>
                <td background="../graph/title_bg.gif" style="width: 1%">
                </td>
                <td background="../graph/title_bg.gif" class="style2" width="98%">
                    <asp:Label ID="addinfo" runat="server" ForeColor="Red" Text="Label" Visible="False"></asp:Label></td>
                <td align="right" background="../graph/title_bg.gif" width="1%">
                </td>
            </tr>
			 </TABLE>
           </tr>
           <TR>
      
        
        <asp:Label ID="PartIDlab" runat="server" Text="Label" Width="11%"></asp:Label>&nbsp;<asp:TextBox
            ID="priceIDBox" runat="server" CssClass="textborder" MaxLength="10" Width="20%"></asp:TextBox>
        <asp:Label ID="CountryIDlab" runat="server" Text="Label" Width="9%" Visible =False ></asp:Label>&nbsp;&nbsp;<asp:TextBox
                   ID="ctryBox" runat="server" CssClass="textborder" MaxLength="2" Width="20%" Visible =False ></asp:TextBox>
        <asp:Label ID="statusLAB" runat="server" Text="Label" Width="9%"></asp:Label><asp:DropDownList ID="statusDrop" runat="server" Width="17%" CssClass="textborder">
        </asp:DropDownList>
        <asp:LinkButton ID="LinkButton2" runat="server">Seach</asp:LinkButton>
        <asp:LinkButton ID="LinkButton1" runat="server" PostBackUrl="~/PresentationLayer/masterrecord/addPriceSetUp.aspx">add</asp:LinkButton><br />
        <hr />

       
        <asp:GridView ID="PriceSetUpView" runat="server" Width="100%" AllowPaging="True">
        </asp:GridView>
        <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="Label"></asp:Label>
    </form>
</body>
</html>
