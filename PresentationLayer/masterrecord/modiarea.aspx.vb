﻿Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Partial Class PresentationLayer_masterrecord_modiarea
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try

            '''''''''''''''''''''''''''''''''''''''''''''''''''''
            Try
                Dim str As String = Request.Params("areid").ToString()
                str = Request.Params("staid").ToString()
                str = Request.Params("ctrid").ToString()
                str = Request.Params("arestat").ToString()
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' 


            Dim mdfyareid As String = Request.Params("areid").ToString()
            Dim mdfystaid As String = Request.Params("staid").ToString()
            Dim mdfyctrid As String = Request.Params("ctrid").ToString()
            Dim mdfystat As String = Request.Params("arestat").ToString()


            'display label message
            Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            ctryidlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCTRY-0001")
            Me.staidLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTATE-0001")
            Me.areaidLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASAREA-0001")
            Me.areanmlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASAREA-0002")
            Me.alterLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCTRY-0003")
            Me.serviceidlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASAREA-0005")
            Me.POlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASAREA-0004")
            statusLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCTRY-0004")
            'Me.polabm.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASAREA-0006")
            creatby.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATBY")
            creatdate.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
            modfyby.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFBY")
            mdfydate.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFDATE")
            Label1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            Label5.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")

            saveButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Save")
            cancelLink.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Cancel")
            addButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Add")
            'Me.modiButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Modify")
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "MODAREATL")

            Me.potitle.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASAREA-0004")

            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            Me.arenmerr.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "ARENMER")
            Me.poerr.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "AREACODE")
            'Me.altererr.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "AREALTER")

            Dim strPanm As String = mdfystat
            If mdfystaid.Trim() <> "" Then
                Dim areaEntity As New clsarea()
                areaEntity.AreaID = mdfyareid
                areaEntity.StateID = mdfystaid
                areaEntity.CountryID = mdfyctrid
                areaEntity.AreaStatus = strPanm


                Dim edtReader As SqlDataReader = areaEntity.GetAreaDetailByID()
                If edtReader.Read() Then
                    Me.areaidbox.Text = edtReader.GetValue(0).ToString()
                    Me.areaidbox.ReadOnly = True
                    Me.areanm.Text = edtReader.GetValue(1).ToString()
                    Me.alter.Text = edtReader.GetValue(2).ToString()

                    Me.ctrid.Text = edtReader.GetValue(3).ToString()
                    Me.ctrid.Enabled = False
                    ''''BANDING''''''''''''''''''''''''

                    Me.serviceid.Items.Clear()
                    Dim rank As Integer = Session("login_rank")
                    Dim service As New clsCommonClass
                    If rank = 7 Then
                        service.spctr = Me.ctrid.Text.ToString().ToUpper()

                    End If
                    If rank = 8 Then
                        service.spctr = Me.ctrid.Text.ToString().ToUpper()
                        service.spstat = Session("login_cmpID")
                    End If
                    If rank = 9 Then
                        service.spctr = Me.ctrid.Text.ToString().ToUpper()
                        service.spstat = Session("login_cmpID")
                        service.sparea = Session("login_svcID")
                    End If
                    If rank = 0 Then
                        service.spctr = Me.ctrid.Text.ToString().ToUpper()
                    End If
                    service.rank = rank
                    Dim serviceds As New DataSet

                    serviceds = service.Getcomidname("BB_MASSVRC_IDNAME")
                    If serviceds.Tables.Count <> 0 Then
                        databonds(serviceds, Me.serviceid)
                    End If

                    ''''BANDING''''''''''''''''''''''''

                    ' Dim ss As String = edtReader.GetValue(4).ToString()
                    Me.staid.Text = edtReader.GetValue(4).ToString()
                    Me.staid.Enabled = False
                    'create the dropdownlist
                    Dim cntryStat As New clsrlconfirminf()
                    Dim statParam As ArrayList = New ArrayList
                    statParam = cntryStat.searchconfirminf("STATUS")
                    Dim count As Integer
                    Dim statid As String
                    Dim statnm As String
                    For count = 0 To statParam.Count - 1
                        statid = statParam.Item(count)
                        statnm = statParam.Item(count + 1)
                        If (statid.Equals("ACTIVE") Or statid.Equals("DELETE") Or statid.Equals("OBSOLETE")) Then
                            ctrystat.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                        End If
                        If statid.Equals("ACTIVE") Then
                            Me.POstat.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                        End If

                        If statid.Equals(edtReader.GetValue(5).ToString()) Then
                            ctrystat.Items(count / 2).Selected = True
                        End If
                        count = count + 1
                    Next





                    Me.serviceid.Text = edtReader.GetValue(6).ToString()

                    'creatbybox.Text = edtReader.GetValue(7).ToString().ToUpper()
                    creatbybox.ReadOnly = True
                    Dim Rcreatby As String = edtReader.GetValue(7).ToString().ToUpper()
                    If Rcreatby <> "ADMIN" Then
                        Dim Rcreat As New clsCommonClass
                        Rcreat.userid() = Rcreatby
                        Dim Rcreatds As New DataSet()
                        Rcreatds = Rcreat.Getuseridname("BB_MASUSER_SelByIDName")

                        Me.creatbybox.Text = Rcreatds.Tables(0).Rows(0).Item(0)
                    Else
                        Me.creatbybox.Text = "ADMIN-ADMIN"
                    End If
                    creatdtbox.Text = edtReader.GetValue(8).ToString()
                    creatdtbox.ReadOnly = True
                    Dim userIDNamestr As String = Session("userID").ToString().ToUpper + "-" + Session("username").ToString.ToUpper
                    modfybybox.Text = userIDNamestr
                    modfybybox.ReadOnly = True
                    mdfydtbox.Text = edtReader.GetValue(10).ToString()
                    mdfydtbox.ReadOnly = True
                    'pocode table               





                    Dim poall As DataSet = areaEntity.GetPO()
                    Session("ds") = poall
                    POView.DataSource = poall



                    Dim objXmlTr1 As New clsXml
                    objXmlTr1.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
                    Dim editcol As New CommandField
                    editcol.EditText = objXmlTr1.GetLabelName("EngLabelMsg", "BB-Edit") '"edit"
                    editcol.HeaderImageUrl = "~/PresentationLayer/graph/edit.gif"
                    editcol.ShowEditButton = True
                    POView.Columns.Add(editcol)

                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    Dim accessgroup As String = Session("accessgroup").ToString
                    Dim purviewArray As Array = New Boolean() {False, False, False, False, False}
                    purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "10")

                    If purviewArray(2) = False Then
                        Response.Redirect("~/PresentationLayer/logon.aspx")
                    End If
                    Me.saveButton.Enabled = purviewArray(1)
                    Me.addButton.Enabled = purviewArray(1)
                    editcol.Visible = purviewArray(1)

                    If Convert.ToString(purviewArray(4)).ToUpper = "FALSE" Then
                        Me.ctrystat.Items.Remove(Me.ctrystat.Items.FindByValue("DELETE"))
                    End If

                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                    POView.DataBind()
                    objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
                    If poall.Tables(0).Rows.Count >= 1 Then
                        POView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "POHD1")
                        POView.HeaderRow.Cells(1).Font.Size = 8
                        POView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "POHD2")
                        POView.HeaderRow.Cells(2).Font.Size = 8
                        POView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "POHD3")
                        POView.HeaderRow.Cells(3).Font.Size = 8
                    End If


                End If

            End If
        End If
    End Sub
#Region " id bond"
    Public Function databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        dropdown.Items.Add("")
        Dim row As DataRow
        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            NewItem.Text = row("id") & "-" & row("name")
            NewItem.Value = row("id")
            dropdown.Items.Add(NewItem)
        Next
        'dropdown.Items(0).Selected = True
    End Function
#End Region

    Protected Sub addButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles addButton.Click
 

        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        'Dim msginfo As String = objXm.GetLabelName("StatusMessage", "SAVEORNOT")
        Dim msginfo As String = objXm.GetLabelName("StatusMessage", "ADDPOST")
        Dim msgtitle As String = objXm.GetLabelName("StatusMessage", "SAVETITLE")
        Dim msgAddPoCode As String = objXm.GetLabelName("StatusMessage", "AREACODE")

        If Me.ctrystat.SelectedValue <> "ACTIVE" Then
            errlab.Text = objXm.GetLabelName("StatusMessage", "DELPO")
            errlab.Visible = True
            Return
        End If


        If Me.PObox.Text.Trim <> "" Then
            MessageBox1.Confirm(msginfo)
        Else
            MessageBox1.Alert(msgAddPoCode)
        End If

    End Sub
    Public Sub saverecord()


        Dim areaEntity As New clsarea()

        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        areaEntity.CountryID = Me.ctrid.Text
        areaEntity.StateID = Me.staid.Text
        areaEntity.AreaID = Me.areaidbox.Text
        If Me.PObox.Text <> "" Then
            areaEntity.POID = Me.PObox.Text
        End If
        areaEntity.POStatus = Me.POstat.SelectedItem.Value
        areaEntity.CreateBy = Session("userID").ToString().ToUpper
        areaEntity.ModifyBy = Session("userID").ToString().ToUpper

        Dim dupCount As Integer = areaEntity.GetDuplicatedPO()

        If dupCount.Equals(-1) Then
            Dim objXm As New clsXml
            objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            errlab.Text = objXm.GetLabelName("StatusMessage", "DUPPOID")
            errlab.Visible = True
        ElseIf dupCount.Equals(0) Then
            If Me.PObox.Text <> "" Then
                Dim insStatCnt As Integer = areaEntity.Insert_po()
                If insStatCnt = 0 Then

                    Me.PObox.Text = ""
                    errlab.Text = objXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Suc")
                    errlab.Visible = True

                    Dim poall As DataSet = areaEntity.GetPO()
                    POView.DataSource = poall
                    POView.DataBind()

                    Session("POView") = poall

                    Dim obiXmlTr As New clsXml
                    obiXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
                    If poall.Tables(0).Rows.Count >= 1 Then
                        POView.HeaderRow.Cells(1).Text = obiXmlTr.GetLabelName("EngLabelMsg", "POHD1")
                        POView.HeaderRow.Cells(1).Font.Size = 8
                        POView.HeaderRow.Cells(2).Text = obiXmlTr.GetLabelName("EngLabelMsg", "POHD2")
                        POView.HeaderRow.Cells(2).Font.Size = 8
                        POView.HeaderRow.Cells(3).Text = obiXmlTr.GetLabelName("EngLabelMsg", "POHD3")
                        POView.HeaderRow.Cells(3).Font.Size = 8
                    End If
                    'Response.Redirect("state.aspx")
                Else

                    Response.Redirect("~/PresentationLayer/Error.aspx")
                End If
            End If
        End If
    End Sub

    Protected Sub saveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveButton.Click


        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim msginfo As String = objXm.GetLabelName("StatusMessage", "SAVEORNOT")
        Dim msgtitle As String = objXm.GetLabelName("StatusMessage", "SAVE_TITLE")

        If Me.PObox.Text.Trim <> "" Then
            MessageBox1.Confirm(msgtitle)
        End If
        MessageBox2.Confirm(msginfo)



    End Sub
    Public Sub saverecord1()

        Dim areaEntity As New clsarea()
        'If (Me.ctry_id.SelectedItem.Value.ToString() <> "") Then
        '    stateEntity.CountryID = ctry_id.SelectedItem.Value.ToString().ToUpper()
        'End If


        areaEntity.CountryID = Me.ctrid.Text.ToUpper()
        areaEntity.StateID = Me.staid.Text.ToUpper()
        areaEntity.AreaID = Me.areaidbox.Text.ToUpper()

        If (Trim(Me.areanm.Text) <> "") Then
            areaEntity.AreaName = areanm.Text.ToUpper()
        End If
        If (Trim(Me.alter.Text) <> "") Then
            areaEntity.AreaAlternateName = alter.Text.ToUpper()
        End If
        If (ctrystat.SelectedItem.Value.ToString() <> "") Then
            areaEntity.AreaStatus = ctrystat.SelectedItem.Value.ToString()
        End If


        If (Me.serviceid.Text <> "") Then
            'If (Me.taxid.SelectedItem.Value.ToString() <> "") Then
            areaEntity.ServiceID = serviceid.SelectedItem.Value.ToString()
        End If

        'If (Trim(creatbybox.Text) <> "") Then
        '    areaEntity.CreateBy = creatbybox.Text.ToUpper()
        'End If
        Dim cntryDate As New clsCommonClass()
        If (Trim(creatdtbox.Text) <> "") Then
            areaEntity.CreateDate = cntryDate.DatetoDatabase(creatdtbox.Text)
        End If
        If (Trim(modfybybox.Text) <> "") Then
            areaEntity.ModifyBy = Session("userID").ToString().ToUpper
        End If
        If (Trim(mdfydtbox.Text) <> "") Then
            areaEntity.ModifyDate = cntryDate.DatetoDatabase(mdfydtbox.Text)
        End If

        areaEntity.IPaddr = Request.UserHostAddress.ToString()
        areaEntity.servid = Session("login_svcID")

        '''''''''delete'''''''''''''''''''''''''

        If areaEntity.AreaStatus = "DELETE" Then
            Dim delcompid As Integer = areaEntity.GetcompByAreaID("BB_MASAREA_DELID")
            Dim delmardid As Integer = areaEntity.GetcompByAreaID("BB_MASMARD_DELID")
            If delcompid <> 0 Or delmardid <> 0 Then
                Dim objXm As New clsXml
                objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                errlab.Text = objXm.GetLabelName("StatusMessage", "DELAREA")
                errlab.Visible = True
                Return
            End If
        End If
        '''''''''delete'''''''''''''''''''''''''

        Dim selvalue As Integer = areaEntity.GetAreaDetailsByAreaNM()
        If selvalue <> 0 Then
            Dim objXm As New clsXml
            objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            errlab.Text = objXm.GetLabelName("StatusMessage", "DUPSTATNM")
            errlab.Visible = True
        ElseIf selvalue.Equals(0) Then
            Dim updCtryCnt As Integer = areaEntity.Update()
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            If updCtryCnt = 0 Then
                Dim objXm As New clsXml
                objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                errlab.Text = objXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Suc")
                errlab.Visible = True

                'saveButton.Enabled = False

                'Response.Redirect("state.aspx")
            Else
                'errlab.Text = objXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Fail")
                'errlab.Visible = True
                Response.Redirect("~/PresentationLayer/Error.aspx")
            End If
        End If
    End Sub
    Protected Sub POView_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles POView.RowEditing
        'Dim ds As DataSet = Session("ds")

        'If saveButton.Enabled = True Then

        '    Dim objXm As New clsXml
        '    objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        '    errlab.Text = objXm.GetLabelName("StatusMessage", "IFSAVE")
        '    errlab.Visible = True
        '    Return

        'End If
 
        Dim areaEntity As New clsarea()
        areaEntity.CountryID = Me.ctrid.Text
        areaEntity.StateID = Me.staid.Text
        areaEntity.AreaID = Me.areaidbox.Text
        Dim poall As DataSet = areaEntity.GetPO()

        Dim areid As String = poall.Tables(0).Rows(POView.PageIndex * POView.PageSize + e.NewEditIndex).Item(0).ToString()
        areid = Server.UrlEncode(areid)
        Dim pocode As String = poall.Tables(0).Rows(POView.PageIndex * POView.PageSize + e.NewEditIndex).Item(1).ToString()
        pocode = Server.UrlEncode(pocode)
        Dim postat As String = poall.Tables(0).Rows(POView.PageIndex * POView.PageSize + e.NewEditIndex).Item(2).ToString
        postat = Server.UrlEncode(postat)

        Dim i As Integer
        For i = 0 To Me.POstat.Items.Count - 1
            If postat = Me.POstat.Items(i).Text Then
                postat = Me.POstat.Items(i).Value
            End If
        Next


        Dim mdfyareid As String = Request.Params("areid").ToString()
        mdfyareid = Server.UrlEncode(mdfyareid)
        Dim mdfystaid As String = Request.Params("staid").ToString()
        mdfystaid = Server.UrlEncode(mdfystaid)
        Dim mdfystat As String = Request.Params("arestat").ToString()
        mdfystat = Server.UrlEncode(mdfystat)
        Dim mdfyctrid As String = Request.Params("ctrid").ToString()
        mdfyctrid = Server.UrlEncode(mdfyctrid)


        Dim strTempURL As String = "areid=" + areid + "&pocode=" + pocode + "&postat=" + postat + "&mdfyareid=" + mdfyareid + "&mdfystaid=" + mdfystaid + "&mdfystat=" + mdfystat + "&mdfyctrid=" + mdfyctrid
        strTempURL = "~/PresentationLayer/masterrecord/modifyPO.aspx?" + strTempURL
        Response.Redirect(strTempURL)
    End Sub
    Protected Sub MessageBox1_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox1.GetMessageBoxResponse
        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
            saverecord()
        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
        End If
    End Sub

    Protected Sub MessageBox2_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox2.GetMessageBoxResponse
        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
            saverecord1()
        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
        End If
    End Sub
End Class
