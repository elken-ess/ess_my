Imports BusinessEntity
Imports System.Configuration
Imports System.Data
Partial Class PresentationLayer_masterrecord_AddPartCategory
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        If Not Page.IsPostBack Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Response.Redirect("~/PresentationLayer/logon.aspx")
                End If
            Catch ex As Exception
                Response.Redirect("~/PresentationLayer/logon.aspx")
            End Try
            'display label message
            CategoryIDbox.Focus()

            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "20")
            Me.saveButton.Enabled = purviewArray(0)
            If purviewArray(0) = False Then
                Response.Redirect("~/PresentationLayer/logon.aspx")
            End If

            'Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
            'System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            CategoryIDlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCGR-0001")
            CategoryNamelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCGR-0002")
            categoryalterLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCGR-0003")
            countryidlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCGR-0004")
            statuslab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCGR-0005")
            effectivedatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCGR-0006")
            obsoletedatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCGR-0007")
            creatby.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATBY")
            creatdate.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
            modfyby.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFBY")
            mdfydate.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFDATE")
            saveButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Save")
            cancelLink.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Cancel")
            AddLink.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Add")
            AddLink.NavigateUrl = Page.Request.RawUrl

            Me.lblCompTop.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            Me.lblCompBottom.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Addpartcategorytl")
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            'Comparedateerr.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-COMPAREEFFOBDATE")
            categoryiderr.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-CategoryID")
            categorynmerr.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Categoryname")
            ctryiderr.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-COUNTRYID")
            altererr.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Categaltname")
            efferr.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Effecdate")
            obserr.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Obsedate")
            Dim userIDNamestr As String = Session("userID").ToString().ToUpper + "-" + Session("username").ToString.ToUpper
            creatbybox.Text = userIDNamestr
            Dim Stat As New clsrlconfirminf()
            creatdtbox.Text = Date.Now()
            modfybybox.Text = userIDNamestr
            mdfydtbox.Text = Date.Now()

            'create the dropdownlist
            Dim statParam As ArrayList = New ArrayList
            statParam = Stat.searchconfirminf("STATUS")
            Dim count As Integer
            Dim statid As String
            Dim statnm As String
            For count = 0 To statParam.Count - 1
                statid = statParam.Item(count)
                statnm = statParam.Item(count + 1)
                count = count + 1
                If statid.Equals("ACTIVE") Or statid.Equals("OBSOLETE") Then
                    statusdrpd.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                End If
            Next
            'statusdrpd.Items(0).Selected = True
            ''bonds country id and name
            Dim countrybond As New clsCommonClass
            'Dim modelds As New DataSet
            'modelds = countrybond.Getidname("BB_MASCTRY_IDNAME")
            'countryiddrpd.Items.Clear()
            'databonds(modelds, countryiddrpd)

            Dim rank As String = Session("login_rank")
            countrybond.rank = rank
            If rank <> 0 Then
                countrybond.spctr = Session("login_ctryID")
                countrybond.spstat = Session("login_cmpID")
                countrybond.sparea = Session("login_svcID")
            End If
            Dim serviceds As New DataSet
            serviceds = countrybond.Getcomidname("BB_MASCTRY_IDNAME")
            If serviceds.Tables.Count <> 0 Then
                databonds(serviceds, Me.countryiddrpd)
            End If
            countryiddrpd.SelectedValue = Session("login_ctryID")

        End If
        HypCal.NavigateUrl = "javascript:DoCal(document.form1.effectibedatebox);"
        HypCal2.NavigateUrl = "javascript:DoCal(document.form1.obsoletedatebox);"

    End Sub

    Protected Sub saveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveButton.Click
        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
      
        Dim temparr As Array = Request.Form("effectibedatebox").Split("/") 'effectibedatebox.Text.Split("/")
        Dim streff As String
        Dim strobs As String
        streff = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
        temparr = Request.Form("obsoletedatebox").Split("/") 'obsoletedatebox.Text.Split("/")
        strobs = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
        If Convert.ToDateTime(strobs) < Convert.ToDateTime(streff) Then
            errlab.Text = objXm.GetLabelName("StatusMessage", "BB-HintMsg-COMPAREEFFOBDATE")
            errlab.Visible = True
            Return
        End If

        Dim msginfo As String = objXm.GetLabelName("StatusMessage", "SAVEORNOT")
        Dim msgtitle As String = objXm.GetLabelName("StatusMessage", "SAVETITLE")
        effectibedatebox.Text = Request.Form("effectibedatebox")
        obsoletedatebox.Text = Request.Form("obsoletedatebox")
        MessageBox1.Confirm(msginfo)
      
    End Sub

   
#Region " bonds"
    Public Function databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList) As ArrayList
        dropdown.Items.Clear()
        dropdown.Items.Add(" ")
        Dim row As DataRow
        Dim infon As ArrayList = New ArrayList()
        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            NewItem.Text = row("id") & "-" & row("name")
            NewItem.Value = row("id")
            dropdown.Items.Add(NewItem)
            infon.Add(NewItem.Value)
        Next
        Return infon
    End Function
#End Region

    Protected Sub MessageBox1_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox1.GetMessageBoxResponse
        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
            Dim categoryEntity As New clsPartcategory()
            Dim cntryStat As New clsCommonClass()
            If (Trim(CategoryIDbox.Text) <> "") Then
                categoryEntity.CategoryID = CategoryIDbox.Text.ToUpper()
            End If
            If (Trim(CategoryNamebox.Text) <> "") Then
                categoryEntity.CategoryName = CategoryNamebox.Text.ToUpper()
            End If
            If (Trim(categoryalterbox.Text) <> "") Then
                categoryEntity.CategoryAlternateName = categoryalterbox.Text.ToUpper()
            End If
            If (countryiddrpd.SelectedItem.Value.ToString() <> "") Then
                categoryEntity.CountryID = countryiddrpd.SelectedItem.Value.ToString()
            End If
            If (Trim(statusdrpd.Text) <> "") Then
                categoryEntity.Status = statusdrpd.Text.ToString()
            End If
            If (Trim(Request.Form("effectibedatebox")) <> "") Then
                'categoryEntity.EffectiveDate = cntryStat.DatetoDatabase(effectibedatebox.Text)
                Dim temparr As Array = Request.Form("effectibedatebox").Split("/") 'effectibedatebox.Text.Split("/")
                categoryEntity.EffectiveDate = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
            End If
            If (Trim(Request.Form("obsoletedatebox")) <> "") Then
                'categoryEntity.ObsoleteDate = cntryStat.DatetoDatabase(obsoletedatebox.Text)
                Dim temparr As Array = Request.Form("obsoletedatebox").Split("/") 'obsoletedatebox.Text.Split("/")
                categoryEntity.ObsoleteDate = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
            End If
            If (Trim(creatbybox.Text) <> "") Then
                categoryEntity.CreateBy = Session("userID").ToString.ToUpper()
            End If
            If (Trim(creatdtbox.Text) <> "") Then
                categoryEntity.CreateDate = cntryStat.DatetoDatabase(creatdtbox.Text)
            End If
            If (Trim(modfybybox.Text) <> "") Then
                categoryEntity.ModifyBy = Session("userID").ToString.ToUpper()
            End If
            If (Trim(mdfydtbox.Text) <> "") Then
                categoryEntity.ModifyDate = cntryStat.DatetoDatabase(mdfydtbox.Text)
            End If

            Dim objXm As New clsXml
            objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            Dim dupCount As Integer = categoryEntity.GetDuplicatedCategory()
            If dupCount.Equals(-1) Then

                errlab.Text = objXm.GetLabelName("StatusMessage", "DUPCATEIDORNM")
                errlab.Visible = True
            ElseIf dupCount.Equals(0) Then
                Dim selvalue As Integer = categoryEntity.GetcategoryByNM()
                If selvalue <> 0 Then
                    errlab.Text = objXm.GetLabelName("StatusMessage", "DUPCATENM")
                    errlab.Visible = True
                Else
                    Dim insCtryCnt As Integer = categoryEntity.Insert()
                    Dim objXmlTr As New clsXml
                    objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    If insCtryCnt = 0 Then

                        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                        errlab.Text = objXm.GetLabelName("StatusMessage", "BB-HintMsg-Suc")
                        errlab.Visible = True
                    Else
                        Response.Redirect("~/PresentationLayer/Error.aspx")
                    End If
                End If
            End If
            effectibedatebox.Text = Request.Form("effectibedatebox")
            obsoletedatebox.Text = Request.Form("obsoletedatebox")
        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
            effectibedatebox.Text = Request.Form("effectibedatebox")
            obsoletedatebox.Text = Request.Form("obsoletedatebox")
        End If
    End Sub
End Class
