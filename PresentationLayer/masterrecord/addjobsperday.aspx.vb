Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data

Partial Class PresentationLayer_masterrecord_addjobsperday
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try
        
            ' LinkButton1.Attributes.Add("onclick", "Javascript:var flag=confirm('�Ƿ���ʾ');return flag;")
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "13")
            Me.LinkButton1.Enabled = purviewArray(0)

            If purviewArray(0) = False Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
            Dim objXmlTr As New clsXml

            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            svcidLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-JOBS-0001")
            jdateLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-JOBS-0002")
            totchLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-JOBS-0003")
            totaljobLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-JOBS-0004")
            Me.totaljobtdyLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-JOBS-0005")
            Me.titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASJOBS-0008")
            crtdLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATBY")
            crtddtLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
            mdfyLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFBY")
            mdfydtLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFDATE")
            Me.LinkButton1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Save")
            Me.HyperLink1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Cancel")

            Me.lblCompTop.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            Me.lblCompBottom.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            AddLink.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Add")
            AddLink.NavigateUrl = Page.Request.RawUrl
            Me.CREDTBox.Text = Date.Now   'created date
            Me.LSTDTBox.Text = Date.Now   'modify date
            Me.CREDTBox.Enabled = False
            Me.LSTDTBox.Enabled = False
            Me.LUSERtBox.Enabled = False
            Me.CUSERBox.Enabled = False
            Dim userIDNamestr As String = Session("userID").ToString().ToUpper() + "-" + Session("username").ToString.ToUpper

            Me.CUSERBox.Text = userIDNamestr
            Me.LUSERtBox.Text = userIDNamestr
            'ininital
            JDATEBox.Text = Date.Today
            JDATEBox.ReadOnly = True
            'totaltdy.Text = 0
            Me.totaltdy.Enabled = False
            ' \\\\\\\\\\\\\\\\\\\\\\\\
            Dim statXmlTr As New clsXml
            statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            Me.RequiredFieldValidator3.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "TJBBSVC")
            Me.RangeValidator1.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "JOBCH")
            Me.RangeValidator2.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "JOBTOTAL")
            Me.RequiredFieldValidator1.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "TCHBLK")
            Me.RequiredFieldValidator2.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "TJBBLK")
            Me.CompareValidator1.ErrorMessage = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASJOBS-0010")
            'check 
            Dim objXmlTrcheck As New clsXml
            objXmlTrcheck.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            '/////////////////////////////////////////////////////////////////////
           
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim clscom As New clsCommonClass()
            Dim idnameds As New DataSet()
            Dim ctryIDPara As String = ""
            Dim cmpIDPara As String = ""
            Dim svcIDPara As String = ""

            If Session("userID").ToString.ToUpper <> "ADMIN" Then
                Dim login_rank As Integer = Session("login_rank")
                Select Case (login_rank)
                    Case 9
                        ctryIDPara = Session("login_ctryID").ToString.ToUpper
                        cmpIDPara = Session("login_cmpID").ToString.ToUpper
                        svcIDPara = Session("login_svcID").ToString.ToUpper
                    Case 8
                        ctryIDPara = Session("login_ctryID").ToString.ToUpper
                        cmpIDPara = Session("login_cmpID").ToString.ToUpper
                    Case 7
                        ctryIDPara = Session("login_ctryID").ToString.ToUpper
                End Select
            Else

            End If

            Dim rank As String = Session("login_rank")
            clscom.spctr = ctryIDPara
            clscom.spstat = cmpIDPara
            clscom.sparea = svcIDPara
            clscom.rank = rank
            idnameds = clscom.Getcomidname("BB_MASSVRC_IDNAME")
            databonds(idnameds, Me.SVCID)
            SVCID.SelectedValue = Session("login_svcID").ToString.ToUpper

            'total no of taday job
            totaltdy.Text = 0
            Dim jobsEntity As New clsJobsPerDay()
            If Me.SVCID.Text <> "" Then
                jobsEntity.ServiceCenterID = Me.SVCID.SelectedValue
                totaltdy.Text = jobsEntity.GetNoTdJobs()
            Else
                totaltdy.Text = 0
            End If
            If Me.JDATEBox.Text <> "" Then
                Dim temparr As Array = Me.JDATEBox.Text.Split("/")
                jobsEntity.dtdate = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
                totaltdy.Text = jobsEntity.GetNoTdJobs()
            Else
                totaltdy.Text = 0
            End If
        End If
        Dim datestyle1 As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle1
    End Sub
#Region "service center id bond"
    Public Function databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        Dim row As DataRow
        dropdown.Items.Add("")
        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            NewItem.Text = row(0) & "-" & row(1)
            NewItem.Value = row(0)
            dropdown.Items.Add(NewItem)
        Next
    End Function

#End Region
#Region "check  when save record "
    Public Function check() As Integer
        ' Dim jobsEntity As New clsJobsPerDay()
        Dim checkint As Integer = 0
        Dim jobsEntity As New clsJobsPerDay()
        If (Trim(SVCID.Text) <> "") Then
            jobsEntity.ServiceCenterID = Me.SVCID.SelectedValue
        End If
        jobsEntity.UserName = Session("userID").ToString.ToUpper

        If (Trim(JDATEBox.Text) <> "") Then
            ' jobsEntity.dtdate = JDATEBox.Text
            Dim datearr As Array = JDATEBox.Text.ToString().Split("/")
            Dim rtdate As String = datearr(2) + "-" + datearr(1) + "-" + datearr(0)
            jobsEntity.dtdate = rtdate
        End If
        Dim sqldr As SqlDataReader = jobsEntity.GetJobsDetailsBySvcID()
        'if there are identical record then tishi error
        If (sqldr.Read()) Then
            checkint = 1
        End If
        Return checkint
    End Function
#End Region
#Region "check no of date job >job no of afd when save record "
    Public Function checkno() As Integer
        If TOJOBBox.Text <> "" Then
            If (Convert.ToInt32(TOJOBBox.Text) < Convert.ToInt32(totaltdy.Text)) Then
                Return 1
            Else
                Return 0
            End If
        End If
    End Function
#End Region

#Region "insert record"
    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton1.Click
        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim msginfo As String = objXm.GetLabelName("StatusMessage", "SAVEORNOT")
        Dim msgtitle As String = objXm.GetLabelName("StatusMessage", "SAVETITLE")
        Dim msg As String = objXm.GetLabelName("StatusMessage", "BB-MASJOBS-0010")

        Dim jdate As Date


        If (Trim(Me.JDATEBox.Text) <> "") Then
            Dim datearr As Array = JDATEBox.Text.ToString().Split("/")
            Dim rtdate As String = datearr(2) + "-" + datearr(1) + "-" + datearr(0)
            jdate = Convert.ToDateTime(rtdate)
        End If
        If jdate < Date.Today Then
            addinfo.Text = objXm.GetLabelName("StatusMessage", "BB-HintMsg-JOBDATE")
            addinfo.Visible = True
            Return
        End If
        If checkno() = 1 Then
            Me.addinfo.Text = msg
            Me.addinfo.Visible = True
        Else
            MessageBox1.Confirm(msginfo)
        End If

    End Sub
#End Region

#Region "insert function "
    Public Sub insertjobsperday()
        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim jobsEntity As New clsJobsPerDay()
        If (Trim(SVCID.Text) <> "") Then
            jobsEntity.ServiceCenterID = Me.SVCID.SelectedValue
        End If
        If (Trim(JDATEBox.Text) <> "") Then
            Dim datearr As Array = JDATEBox.Text.ToString().Split("/")
            Dim rtdate As String = datearr(2) + "-" + datearr(1) + "-" + datearr(0)
            jobsEntity.dtdate = rtdate
        End If
        jobsEntity.UserName = Session("userID").ToString.ToUpper
        If (Trim(TOJOBBox.Text) <> "") Then
            jobsEntity.TotalofJob = TOJOBBox.Text
        End If

        If (Trim(TOTCHBox.Text) <> "") Then
            jobsEntity.NoofTechnician = TOTCHBox.Text
        End If
        If (Trim(CUSERBox.Text) <> "") Then
            jobsEntity.Createdby = Session("userID").ToString.ToUpper
        End If
        If (Trim(Me.LUSERtBox.Text) <> "") Then
            jobsEntity.Modifyby = Session("userID").ToString.ToUpper
        End If
        Dim checkint As Integer = check()
        If checkint <> 0 Then
            addinfo.Text = objXm.GetLabelName("StatusMessage", "JBDATE")
            addinfo.Visible = True
        ElseIf checkint = 0 Then
            Dim insJobCnt As Integer = jobsEntity.Insert()
            If insJobCnt = 0 Then
                addinfo.Text = objXm.GetLabelName("StatusMessage", "BB-HintMsg-Suc")
                addinfo.Visible = True
            Else
                Response.Redirect("~/PresentationLayer/Error.aspx")
            End If
        End If
    End Sub
#End Region
    Protected Sub SVCID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles SVCID.SelectedIndexChanged
        Dim jobsEntity As New clsJobsPerDay()
        If Me.SVCID.Text <> "" Then
            jobsEntity.ServiceCenterID = Me.SVCID.SelectedValue
        Else
            totaltdy.Text = 0
        End If
        If Me.JDATEBox.Text <> "" Then
            Dim temparr As Array = Me.JDATEBox.Text.Split("/")
            jobsEntity.dtdate = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
            totaltdy.Text = jobsEntity.GetNoTdJobs()
        Else
            totaltdy.Text = 0
        End If

    End Sub

    Protected Sub MessageBox1_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox1.GetMessageBoxResponse

        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
            insertjobsperday()
        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
        End If
    End Sub

    Protected Sub JCalendar1_SelectedDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles JCalendar1.SelectedDateChanged
        Dim jobsEntity As New clsJobsPerDay()
        If Me.SVCID.Text <> "" Then
            jobsEntity.ServiceCenterID = Me.SVCID.SelectedValue
        Else
            totaltdy.Text = 0
        End If
        If Me.JDATEBox.Text <> "" Then
            Dim temparr As Array = Me.JDATEBox.Text.Split("/")

            jobsEntity.dtdate = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
            totaltdy.Text = jobsEntity.GetNoTdJobs()
        Else
            totaltdy.Text = 0
        End If
    End Sub
End Class
