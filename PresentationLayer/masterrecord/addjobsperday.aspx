<%@ Page Language="VB" AutoEventWireup="false"  enableEventValidation="false" CodeFile="addjobsperday.aspx.vb" Inherits="PresentationLayer_masterrecord_addjobsperday" %>

<%@ Register Assembly="JCalendar" Namespace="JCalendar" TagPrefix="cc2" %>

<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>add job per day</title>
    <meta http-equiv="Content-Type" content="text/html; charset=gb2312">
	<LINK href="../css/style.css" type="text/css" rel="stylesheet">
</head>
<body  id="countrybody">
   <form id="Countryform"  method="post" action=""  runat=server>
     <TABLE border="0" id=TABLE2 style="width: 100%">
           <tr>
           <td>
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>
							<TD width="1%" background="../graph/title_bg.gif"><IMG height="24" src="../graph/title1.gif" width="5"></TD>
							<TD class="style2" width="98%" background="../graph/title_bg.gif">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></TD>
							<TD align="right" width="1%" background="../graph/title_bg.gif"><IMG height="24" src="../graph/title_2.gif" width="5"></TD>
						</TR>
			 </TABLE>
           </tr>
           <tr>
           <td style="width: 100%; height: 1px;">
              <TABLE cellSpacing="0" cellPadding="0" border="0" style="width: 100%; height: 1px;">
						<TR>					
							<TD class="style2" background="../graph/title_bg.gif" style="width: 100%; height: 1px;">
                                <font color=red style="width: 100%"><asp:Label ID="addinfo" runat="server" Text="Label" Visible=false></asp:Label></font>
                                </TD>
						</TR>
			 </TABLE>
           </tr>
			<TR>
				<TD style="width: 100%">
   <TABLE id="jobsperday" cellSpacing="1" cellPadding="2" bgColor="#b7e6e6" border="0" style="width: 100%; height: 158px;">
    <tr bgcolor="#ffffff">
<td colspan = 4>
                            <asp:Label ID="lblCompTop" runat="server" ForeColor="Red" Text="Label" Visible="True"></asp:Label>
</td>
             </tr>
             <tr bgcolor="#ffffff">
<td colspan = 4>
                        	&nbsp;
                     	</td>
               </tr>    
									  <TR bgColor="#ffffff">
											<TD  align=right style="width: 16%; height: 19px">
                                                &nbsp;<asp:Label ID="svcidLab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%; height: 19px;">
                                                <asp:DropDownList ID="SVCID" runat="server" CssClass="textborder" Width="90%">
                                                </asp:DropDownList>
                                                &nbsp;
                                                <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label></TD>
											<TD  align=right style="width: 15%; height: 19px">
                                                <asp:Label ID="totchLab" runat="server" Text="Label"></asp:Label></TD>
											<TD  align=left style="width: 35%; height: 19px">
                                                <asp:TextBox ID="TOTCHBox" runat="server" CssClass="textborder" MaxLength="4" Width="90%"></asp:TextBox>
                                                <asp:Label ID="Label2" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="TOTCHBox"
                                                    ErrorMessage="RangeValidator" MaximumValue="999999" MinimumValue="0" Type="Integer" ForeColor="White">*</asp:RangeValidator>&nbsp;
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TOTCHBox"
                                                    ErrorMessage="RequiredFieldValidator" ForeColor="White">.</asp:RequiredFieldValidator></TD>
										</TR>
										<TR bgColor="#ffffff">
											<TD  align=right style="height: 1px; width: 16%;">
                                                &nbsp;<asp:Label ID="jdateLab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%; height: 1px">
                                                <asp:TextBox ID="JDATEBox" runat="server" CssClass="textborder" ReadOnly="True" Width="90%"></asp:TextBox>&nbsp;<cc2:JCalendar
                                                    ID="JCalendar1" runat="server" ControlToAssign="JDATEBox" ImgURL="~/PresentationLayer/graph/calendar.gif" />
                                            </TD>
											<TD  align=right style="height: 1px; width: 15%;">
                                                <asp:Label ID="totaljobLab" runat="server" Text="Label"></asp:Label></TD>
											<TD  align=left style="height: 1px; width: 35%;">
                                                <asp:TextBox ID="TOJOBBox" runat="server" CssClass="textborder" MaxLength="4" Width="90%"></asp:TextBox>
                                                <asp:Label
                                                        ID="Label3" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                <asp:RangeValidator ID="RangeValidator2" runat="server" ErrorMessage="RangeValidator"
                                                    MaximumValue="999999" MinimumValue="0" Type="Integer" ControlToValidate="TOJOBBox" ForeColor="White">*</asp:RangeValidator>&nbsp;
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TOJOBBox"
                                                    ErrorMessage="RequiredFieldValidator" ForeColor="White">.</asp:RequiredFieldValidator></TD>
										</TR>
										<TR bgColor="#ffffff">
										<TD  align=right style="height: 26px; width: 16%;">
                                                &nbsp;<asp:Label ID="totaljobtdyLab" runat="server" Text="Label"></asp:Label></TD>
                                            <td align="left" colspan="3" style="height: 26px">
                                                <asp:TextBox ID="totaltdy" runat="server" CssClass="textborder" ReadOnly="True" Width="26%"></asp:TextBox></td>
										</TR>
										<TR bgColor="#ffffff">
											<TD  align=right style="height: 26px; width: 16%;">
                                                &nbsp;<asp:Label ID="crtdLab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%; height: 26px">
                                                <asp:TextBox ID="CUSERBox" runat="server" CssClass="textborder" Width="90%"></asp:TextBox></TD>
											<TD  align=right style="height: 26px; width: 15%;">
                                                <asp:Label ID="crtddtLab" runat="server" Text="Label"></asp:Label></TD>
											<TD  align=left style="height: 26px; width: 35%;">
                                                <asp:TextBox ID="CREDTBox" runat="server" CssClass="textborder" Width="90%"></asp:TextBox></TD>
										</TR>
										<TR bgColor="#ffffff">
											<TD  align=right style="width: 16%; height: 28px">
                                                &nbsp;<asp:Label ID="mdfyLab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%; height: 28px;">
                                                <asp:TextBox ID="LUSERtBox" runat="server" CssClass="textborder" Width="63%"></asp:TextBox></TD>
											<TD  align=right style="width: 15%; height: 28px">
                                                <asp:Label ID="mdfydtLab" runat="server" Text="Label"></asp:Label></TD>
											<TD  align=left style="width: 35%; height: 28px">
                                                <asp:TextBox ID="LSTDTBox" runat="server" CssClass="textborder" Width="90%"></asp:TextBox></TD>
										</TR>
										
								</TABLE>
								<font color=red><asp:Label ID="lblCompBottom" runat="server" Text="Label" Visible="true"></asp:Label></font>
							<TABLE id="Table1" cellSpacing="1" cellPadding="1"  border="0">
									<TR>
										<TD style="WIDTH: 20%; height: 17px;" align=center>
                                            &nbsp;<asp:LinkButton ID="LinkButton1" runat="server">save</asp:LinkButton></TD>
										<td align=center>
                                            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/PresentationLayer/masterrecord/jobsperday.aspx">cancel</asp:HyperLink></td>
                                            <td align=center><asp:HyperLink ID="AddLink" runat="server" >[Add]</asp:HyperLink></td>

				                    <tr>

			                   </TABLE>
			                   			</TD>
					</TR>
			</TABLE>
       <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
           ShowSummary="False" />
                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="TOJOBBox"
                        ControlToValidate="totaltdy" ErrorMessage="CompareValidator" ForeColor="White"
                        Operator="LessThanEqual" Type="Integer">*</asp:CompareValidator>&nbsp;&nbsp;
                    <cc1:MessageBox ID="MessageBox1" runat="server" />
       <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="SVCID"
           ForeColor="White">.</asp:RequiredFieldValidator>

    </form>
</body>
</html>
