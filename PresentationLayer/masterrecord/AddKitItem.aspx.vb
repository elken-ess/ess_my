Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Partial Class PresentationLayer_masterrecord_AddKitItem
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    'Response.Redirect("~/PresentationLayer/logon.aspx")
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                'Response.Redirect("~/PresentationLayer/logon.aspx")
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try
            'display label message
            kitidbox.Focus()
            Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            kitidlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASKITITEM-0001")
            partidLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASKITITEM-0002")
            quantitylab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASKITITEM-0003")
            statusLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASKITITEM-0004")
            creatby.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATBY")
            creatdate.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
            modfyby.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFBY")
            mdfydate.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFDATE")
            saveButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Save")
            cancelLink.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Cancel")
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-AddKititemtl")
            Me.lblCompTop.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            Me.lblCompBottom.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")

            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            Dim userIDNamestr As String = Session("userID").ToString().ToUpper + "-" + Session("username").ToString.ToUpper
            creatbybox.Text = userIDNamestr
            Dim cntryStat As New clsrlconfirminf()
            creatdtbox.Text = Date.Now()
            modfybybox.Text = userIDNamestr
            mdfydtbox.Text = Date.Now()

            'partidbox.Text = Request.Params("partid").ToString()
            kitidbox.Text = Request.Params("partid").ToString()
            TextBox1.Text = Request.Params("countryid").ToString()

            Dim XmlTr As New clsXml
            XmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            kitiderr.ErrorMessage = XmlTr.GetLabelName("StatusMessage", "BB-HintMsg-KITIDERR")
            quantityerr.ErrorMessage = XmlTr.GetLabelName("StatusMessage", "BB-HintMsg-KITQUANTITY")
            partiderr.ErrorMessage = XmlTr.GetLabelName("StatusMessage", "BB-HintMsg-part")
            'create the dropdownlist
            Dim statParam As ArrayList = New ArrayList
            statParam = cntryStat.searchconfirminf("STATUS")
            Dim count As Integer
            Dim statid As String
            Dim statnm As String
            For count = 0 To statParam.Count - 1
                statid = statParam.Item(count)
                statnm = statParam.Item(count + 1)
                count = count + 1
                If statid.Equals("ACTIVE") Or statid.Equals("OBSOLETE") Then
                    stat.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                End If
            Next
            stat.Items(0).Selected = True

            'retrieve kititem view 
            Dim kitEntity As New ClsPartRequirement()
            kitEntity.partid = kitidbox.Text.ToUpper()
            kitEntity.Countryid = Session("login_ctryID")

            Dim ctryall As DataSet = kitEntity.GetKititem()
            kititemView.DataSource = ctryall
            kititemView.DataBind()

            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            If ctryall.Tables(0).Rows.Count <> 0 Then
                kititemView.HeaderRow.Cells(0).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-KITITEMHD0")
                kititemView.HeaderRow.Cells(0).Font.Size = 8
                kititemView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-KITITEMHD1")
                kititemView.HeaderRow.Cells(1).Font.Size = 8
                kititemView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-KITITEMHD2")
                kititemView.HeaderRow.Cells(2).Font.Size = 8
                kititemView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCAT-0001")
                kititemView.HeaderRow.Cells(3).Font.Size = 8
            End If
            'bond  PART ID and name
            Dim bond As New clsCommonClass
            Dim rank As String = Session("login_rank")
            bond.rank = rank
            If rank <> "0" Then
                bond.spctr = Session("login_ctryID")
                bond.spstat = Session("login_cmpID")
                bond.sparea = Session("login_svcID")
            End If
            Dim sertype As New DataSet
            sertype = bond.Getcomidname("BB_MASKIT_SPartid")
            If sertype.Tables.Count <> 0 Then
                databonds(sertype, Drppartid)
            End If
        End If
    End Sub

    Protected Sub saveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveButton.Click
        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim msginfo As String = objXm.GetLabelName("StatusMessage", "SAVEORNOT")
        Dim msgtitle As String = objXm.GetLabelName("StatusMessage", "SAVETITLE")
        MessageBox1.Confirm(msginfo)


    End Sub

    Protected Sub cancelLink_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cancelLink.Click

        Dim partid As String = kitidbox.Text.ToUpper().ToString
        partid = Server.UrlEncode(partid)
        Dim countryid As String = TextBox1.Text.ToString
        countryid = Server.UrlEncode(countryid)
        Dim editkit As String = "1"
        editkit = Server.UrlEncode(editkit)

        Dim type As String = Request.Params("type").ToString()
        If type = "add" Then
            Dim strTempURL As String = "partid=" + partid + "&countryid=" + countryid + "&editkit=" + editkit
            strTempURL = "~/PresentationLayer/masterrecord/AddPartRequire.aspx?" + strTempURL
            Response.Redirect(strTempURL)
        Else
            Dim strTempURL As String = "partid=" + partid + "&countryid=" + countryid + "&editkit=" + editkit
            strTempURL = "~/PresentationLayer/masterrecord/ModifyPartRequire.aspx?" + strTempURL
            Response.Redirect(strTempURL)
        End If
    End Sub

    Protected Sub kititemView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles kititemView.PageIndexChanging
        kititemView.PageIndex = e.NewPageIndex
        Dim kitEntity As New ClsPartRequirement()
        kitEntity.partid = kitidbox.Text.ToUpper()
        kitEntity.Countryid = Session("login_ctryID")

        Dim ctryall As DataSet = kitEntity.GetKititem()
        kititemView.DataSource = ctryall
        kititemView.DataBind()
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        If ctryall.Tables(0).Rows.Count <> 0 Then
            kititemView.HeaderRow.Cells(0).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-KITITEMHD0")
            kititemView.HeaderRow.Cells(0).Font.Size = 8
            kititemView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-KITITEMHD1")
            kititemView.HeaderRow.Cells(1).Font.Size = 8
            kititemView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-KITITEMHD2")
            kititemView.HeaderRow.Cells(2).Font.Size = 8
            kititemView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCAT-0001")
            kititemView.HeaderRow.Cells(3).Font.Size = 8
        End If
    End Sub

    Protected Sub MessageBox1_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox1.GetMessageBoxResponse
        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
            Dim KitItemEntity As New clsKititem
            If (Trim(kitidbox.Text) <> "") Then
                KitItemEntity.KitiD = kitidbox.Text.ToUpper()
            End If
            If (Trim(Drppartid.SelectedItem.Value.ToString) <> "") Then
                KitItemEntity.PartID = Drppartid.SelectedItem.Value.ToString()
            End If
            If (Trim(quantitytbox.Text) <> "") Then
                KitItemEntity.KitQuantity = quantitytbox.Text.ToString()
            End If
            If (stat.SelectedItem.Value.ToString() <> "") Then
                KitItemEntity.Status = stat.SelectedItem.Value.ToString()
            End If
            If (Trim(creatbybox.Text) <> "") Then
                'KitItemEntity.CreateBy = creatbybox.Text.ToUpper()
                KitItemEntity.CreateBy = Session("userID").ToString.ToUpper()

            End If
            If (Trim(modfybybox.Text) <> "") Then
                KitItemEntity.ModifyBy = Session("userID").ToString.ToUpper()
            End If

            KitItemEntity.CountryID = Session("login_ctryID")

            Dim dupCount As Integer = KitItemEntity.GetDuplicatedkititem()
            If dupCount.Equals(-1) Then
                Dim objXm As New clsXml
                objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                errlab.Text = objXm.GetLabelName("StatusMessage", "DUPKITIDPARTID")
                errlab.Visible = True
            ElseIf dupCount.Equals(0) Then
                Dim insCtryCnt As Integer = KitItemEntity.Insert()
                Dim objXmlTr As New clsXml
                objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                If insCtryCnt = 0 Then
                    Dim objXm As New clsXml
                    objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    errlab.Text = objXm.GetLabelName("StatusMessage", "BB-HintMsg-Suc")
                    errlab.Visible = True
                
                    'retrieve kititem view 
                    Dim kitEntity As New ClsPartRequirement()
                    kitEntity.partid = kitidbox.Text.ToUpper()
                    kitEntity.Countryid = Session("login_ctryID")
                    Dim ctryall As DataSet = kitEntity.GetKititem()
                    kititemView.DataSource = ctryall
                    kititemView.DataBind()

                    objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
                    If ctryall.Tables(0).Rows.Count <> 0 Then
                        kititemView.HeaderRow.Cells(0).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-KITITEMHD0")
                        kititemView.HeaderRow.Cells(0).Font.Size = 8
                        kititemView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-KITITEMHD1")
                        kititemView.HeaderRow.Cells(1).Font.Size = 8
                        kititemView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-KITITEMHD2")
                        kititemView.HeaderRow.Cells(2).Font.Size = 8
                        kititemView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCAT-0001")
                        kititemView.HeaderRow.Cells(3).Font.Size = 8
                    End If
                Else
                    Response.Redirect("~/PresentationLayer/Error.aspx")
                End If
            End If
        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
        End If
    End Sub
#Region " bonds"
    Public Function databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList) As ArrayList
        dropdown.Items.Clear()
        dropdown.Items.Add(" ")
        Dim row As DataRow
        Dim infon As ArrayList = New ArrayList()
        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            NewItem.Text = row("id") & "-" & row("name")
            NewItem.Value = row("id")
            dropdown.Items.Add(NewItem)
            infon.Add(NewItem.Value)
        Next
        Return infon
    End Function
#End Region
End Class
