Imports BusinessEntity
Imports System.Configuration
Imports System.Xml
Imports SQLDataAccess
Imports System.Data.SqlClient
Imports System.Web
Imports System.Data

Partial Class PresentationLayer_masterrecord_Addcustomer
    Inherits System.Web.UI.Page
    Private customerid As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Page.MaintainScrollPositionOnPostBack = True
        AjaxPro.Utility.RegisterTypeForAjax(GetType(PresentationLayer_masterrecord_Addcustomer))

        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try

        BindGrid()
        'ctrid.Attributes("OnSelectedIndexChanged") = "self.location.href=""#ctry"""
    End Sub

    Protected Sub BindGrid()
        If Not Page.IsPostBack Then
            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "04")
            If purviewArray(0) = False Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
            Me.saveButton.Enabled = purviewArray(0)
            'display label message
            Dim coid As String
            Dim paid As String
            paid = Request.QueryString("custid")
            coid = Request.QueryString("Prefix")
            CustomerCtryID.Text = Session("login_ctryID")
            Session("sybom") = False
            Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")

            custidlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0001")
            JDECIDLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0009")
            CustomerNamelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0010")
            statuslab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCTRY-0004")
            AlternateNameLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0005")
            RacesLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0011")
            CustomerTypeLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0012")
            noteslab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0013")
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "ADDCUSTOM")
            adresstitlLabel.Text = objXmlTr.GetLabelName("EngLabelMsg", "BROADDRESS")
            teltitllab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BROCONTACT")
            Me.roLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0017")
            Me.SvcidLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "SVCRID")
            saveButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Save")
            cancelLink.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Cancel")
            HyperLink2.Text = objXmlTr.GetLabelName("EngLabelMsg", "CREATINBASE")

            AddLink.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Add")
            AddLink.NavigateUrl = Page.Request.RawUrl
            creatby.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATBY")
            creatdate.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
            modfyby.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFBY")
            mdfydate.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFDATE")

            Me.lblCompTop.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            Me.lblCompBottom.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")

            Dim statXmlTr As New clsXml
            statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            'Me.custidmsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "Custiderr")
            Me.svcidmsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "MAS-SVC")
            Me.custnamemsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "Custnameerr")
            Me.Racesmsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "RACEMSG")
            Me.CustomerTypemsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "CUSTTYPEMSG")

            '--ADDED BY RAYMOND 2006-07-17
            Me.AdresRequired1.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "COMADDR1")
            Me.AdresRequired2.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "COMADDR2")

            Me.rfvCTRID.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "MAS-COUNTRY")
            Me.rfvAREAID.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "MAS-AREA")
            Me.StaIDValid.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "MAS-STAT")
            Me.frvPostCode.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "COMPCODE")


            Dim userIDNamestr As String = Session("userID").ToString().ToUpper + "-" + Session("username").ToString.ToUpper
            creatbybox.Text = userIDNamestr
            creatdtbox.Text = Date.Now()
            modfybybox.Text = userIDNamestr
            mdfydtbox.Text = Date.Now()
            Dim customer As New clsCustomerRecord()
            customer.username = userIDNamestr
            Dim Prefix As String = Session("login_ctryID")
            Me.CustomerPrefix.Text = Prefix
            'create the dropdownlist
            Dim cntryStat As New clsrlconfirminf()
            Dim statParam As ArrayList = New ArrayList
            statParam = cntryStat.searchconfirminf("STATUS")
            Dim count As Integer
            Dim statid As String
            Dim statnm As String
            For count = 0 To statParam.Count - 1
                statid = statParam.Item(count)
                statnm = statParam.Item(count + 1)
                count = count + 1
                If statid.Equals("ACTIVE") Or statid.Equals("OBSOLETE") Then
                    status.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                End If
            Next
            'status.Items(0).Selected = True

            Dim race As New clsrlconfirminf()
            Dim raceParam As ArrayList = New ArrayList
            raceParam = race.searchconfirminf("PERSONRACE")
            Dim racecount As Integer
            Dim raceid As String
            Dim racenm As String
            For racecount = 0 To raceParam.Count - 1
                raceid = raceParam.Item(racecount)
                racenm = raceParam.Item(racecount + 1)
                racecount = racecount + 1
                If Not raceid.Equals("ZNONAPPL") Then
                    Me.Races.Items.Add(New ListItem(racenm.ToString(), raceid.ToString()))
                End If
            Next

            'load country
            Dim country As New clsCommonClass
            Dim rankID As String = Session("login_rank")
            If rankID <> 0 Then
                country.spctr = Session("login_ctryID").ToString().ToUpper
            End If
            country.rank = rankID
            Dim countryds As New DataSet
            countryds = country.Getcomidname("BB_MASCTRY_IDNAME")
            If countryds.Tables.Count <> 0 Then
                databonds(countryds, Me.ctrid)
            End If

            'Me.ctrid.Items.Insert(0, "")

            Me.areaid.Items.Clear()
            'If Me.ctrid.Text <> "" Then

            Dim state As New clsCommonClass
            If (Me.ctrid.SelectedValue().ToString() <> "") Then
                state.spctr = Me.ctrid.SelectedValue().ToString()
            End If
            state.rank = Session("login_rank")
            Dim stateds As New DataSet
            stateds = state.Getcomidname("BB_MASSTAT_IDNAME")
            If stateds.Tables.Count <> 0 Then
                databonds(stateds, Me.staid)
            End If

            Me.staid.Items.Insert(0, "")

            Dim custtype As New clsrlconfirminf()
            Dim custParam As ArrayList = New ArrayList
            custParam = custtype.searchconfirminf("CUSTTYPE")
            Dim custcount As Integer
            Dim custid As String
            Dim custnm As String
            For custcount = 0 To custParam.Count - 1
                custid = custParam.Item(custcount)
                custnm = custParam.Item(custcount + 1)
                custcount = custcount + 1
                Me.CustomerType.Items.Add(New ListItem(custnm.ToString(), custid.ToString()))
            Next


            'load preferred language - Added by LLY 201702
            Me.prefLang.Items.Clear()
            Dim prefLang As New clsrlconfirminf()
            Dim langParam As ArrayList = New ArrayList
            langParam = prefLang.searchconfirminf("PREFLANG")
            Dim preflangcount As Integer
            Dim langid As String
            Dim langnm As String
            For preflangcount = 0 To langParam.Count - 1
                langid = langParam.Item(preflangcount)
                langnm = langParam.Item(preflangcount + 1)
                preflangcount = preflangcount + 1
                Me.prefLang.Items.Add(New ListItem(langnm.ToString(), langid.ToString()))
            Next
            Me.prefLang.SelectedValue = "PREFLANG-ENGLISH"


            Dim rank As String = Session("login_rank")
            Dim service As New clsCommonClass
            If rank = 7 Then
                service.spctr = Session("login_ctryID")

            End If
            If rank = 8 Then
                service.spctr = Session("login_ctryID")
                service.spstat = Session("login_cmpID")
            End If
            If rank = 9 Then
                service.spctr = Session("login_ctryID")
                service.spstat = Session("login_cmpID")
                service.sparea = Session("login_svcID")
            End If
            If rank = 0 Then
                'service.spctr = Me.CountryID.SelectedValue().ToString().ToUpper()
            End If
            'service.rank = rank
            service.rank = rank
            Dim serviceds As New DataSet

            serviceds = service.Getcomidname("BB_MASSVRC_IDNAME")
            If serviceds.Tables.Count <> 0 Then
                databonds(serviceds, Me.SVCID)
            End If
            Me.SVCID.Items.Insert(0, "")
            'return from addaddress/modifaddress,set value
            If Trim(paid).Length > 0 Then
                Session("sybom") = True
                Me.CustomerPrefix.Text = coid

                CustomerPrefix.ReadOnly = True
                Me.custid.Text = paid
                Dim CustomerEntity As New clsCustomerRecord()

                CustomerEntity.CustomerID = paid
                CustomerEntity.Customerpf = coid
                Dim edtReader As SqlDataReader = CustomerEntity.GetCustomerDetailsByID()
                If edtReader.Read() Then

                    'edtReader.GetValue(1).ToString() 'part name
                    Me.JDECID.Text = edtReader.GetValue(2).ToString() 'altername
                    Me.CustomerName.Text = edtReader.GetValue(3).ToString() 'country id
                    'countryiddrpd.Enabled = False
                    Me.AlternateName.Text = edtReader.GetValue(4).ToString() 'category id

                    Dim mtchdl As New clsrlconfirminf()
                    Dim tchdlParam As New ArrayList
                    tchdlParam = mtchdl.searchconfirminf("PERSONRACE")
                    Dim tchdlcount As Integer
                    Dim tchdlid As String

                    Dim strRace As String
                    strRace = edtReader.GetValue(5).ToString()
                    For tchdlcount = 0 To tchdlParam.Count - 1
                        tchdlid = tchdlParam.Item(tchdlcount)

                        If tchdlid = strRace Then
                            Me.Races.Items(tchdlcount / 2).Selected = True
                            Me.Races.Text = edtReader.GetValue(5).ToString() 'parent/model id
                        End If
                        tchdlcount = tchdlcount + 1
                    Next

                    Me.CustomerType.Text = edtReader.GetValue(6).ToString()
                    Me.status.Text = edtReader.GetValue(7).ToString()
                    Me.notes.Text = edtReader.GetValue(8).ToString()
                    Me.SVCID.Text = edtReader.GetValue(9).ToString()
                    'Me.creatbybox.Text = edtReader.GetValue(10).ToString()
                    Dim Rcreatby As String = edtReader.Item(10).ToString().ToUpper()
                    If Rcreatby <> "ADMIN" Then
                        Dim Rcreat As New clsCommonClass
                        Rcreat.userid() = Rcreatby
                        Dim Rcreatds As New DataSet()
                        Rcreatds = Rcreat.Getuseridname("BB_MASUSER_SelByIDName")

                        Me.creatbybox.Text = Rcreatds.Tables(0).Rows(0).Item(0)
                    Else
                        Me.creatbybox.Text = "ADMIN-ADMIN"
                    End If
                    Me.creatdtbox.Text = edtReader.GetValue(11).ToString()
                    Me.modfybybox.Text = userIDNamestr
                    Me.mdfydtbox.Text = edtReader.GetValue(13).ToString()

                End If

            Else



            End If
            'addressview bond
            loadaddressviewbond()
            loadcontactviewbond()
            loadroviewbond()
            If Session("sybom") = True Then
                saveButton.Enabled = False
            End If
            Me.CustomerName.Focus()
        End If
    End Sub
#Region " id bond"
    Public Sub databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        Dim row As DataRow
        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            NewItem.Text = row("id") & "-" & row("name")
            NewItem.Value = row("id")
            dropdown.Items.Add(NewItem)
        Next

    End Sub
#End Region
   

    Protected Sub addressView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles addressView.PageIndexChanging
        Me.addressView.PageIndex = e.NewPageIndex
        viewaddressviewbond()

        viewcontactviewbond()
        viewroviewbond()
    End Sub
#Region " load addressview bond"
    Public Sub loadaddressviewbond()
        Dim addressEntity As New clsAddress()

        If (Trim(Me.custid.Text) <> "") Then
            addressEntity.CustomerID = Me.custid.Text.ToUpper()

        End If
        addressEntity.CustPrefix = Me.CustomerPrefix.Text.ToUpper()
        Dim addressall As DataSet = addressEntity.GetAddress()
        'If addressall.Tables(0).Rows.Count <> 0 Then
        Me.addressView.DataSource = addressall
        Session("addressView") = addressall
        addressView.AllowPaging = True
        addressView.AllowSorting = True
        Dim editcol As System.Web.UI.WebControls.HyperLinkField = New HyperLinkField()
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        editcol.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Edit") '"edit"
        editcol.DataNavigateUrlFormatString = "~/PresentationLayer/masterrecord/modifyAddress.aspx?addresstype={0}&customid=" & Me.custid.Text.ToUpper() & "&Prefix=" & Me.CustomerPrefix.Text.ToUpper() & "&symbol=0"
        editcol.HeaderImageUrl = "~/PresentationLayer/graph/edit.gif"
        If addressall.Tables(0).Rows.Count >= 1 Then
            Dim addresstype As String = addressall.Tables.Item(0).Columns(0).ColumnName.ToString()
            Dim NavigateUrls As Array = Array.CreateInstance(GetType(String), 1) '时间和ID做主键
            NavigateUrls(0) = addresstype
            'NavigateUrls(1) = addressstat
            editcol.DataNavigateUrlFields = NavigateUrls
        End If
        addressView.Columns.Add(editcol)
        addressView.DataBind()
        If addressall.Tables(0).Rows.Count >= 1 Then
            addressView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "ADDRESSTYPE")
            addressView.HeaderRow.Cells(1).Font.Size = 8
            addressView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "STATUS")
            addressView.HeaderRow.Cells(2).Font.Size = 8
            addressView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "ADDRESS1")
            addressView.HeaderRow.Cells(3).Font.Size = 8
            addressView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "ADDRESS2")
            addressView.HeaderRow.Cells(4).Font.Size = 8
            addressView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "POCODE")
            addressView.HeaderRow.Cells(5).Font.Size = 8
        End If


    End Sub
#End Region

#Region " load contactview bond"
    Public Sub loadcontactviewbond()
        Dim contactEntity As New clsContact()
        If (Trim(Me.custid.Text) <> "") Then
            contactEntity.CustomerID = Me.custid.Text.ToUpper()

        End If
        contactEntity.CustPrefix = Me.CustomerPrefix.Text.ToUpper()
        Dim contactall As DataSet = contactEntity.Getcontact()
        'If addressall.Tables(0).Rows.Count <> 0 Then
        Me.contactView.DataSource = contactall
        Session("contactView") = contactall
        contactView.AllowPaging = True
        contactView.AllowSorting = True
        Dim editcol As System.Web.UI.WebControls.HyperLinkField = New HyperLinkField()
        Dim obiXmlTr As New clsXml
        obiXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        editcol.Text = obiXmlTr.GetLabelName("EngLabelMsg", "BB-Edit") '"edit"
        editcol.DataNavigateUrlFormatString = "~/PresentationLayer/masterrecord/modifyContact.aspx?contacttype={0}&customid=" & Me.custid.Text.ToUpper() & "&Prefix=" & Me.CustomerPrefix.Text.ToUpper() & "&consymbol=0"
        editcol.HeaderImageUrl = "~/PresentationLayer/graph/edit.gif"
        If contactall.Tables(0).Rows.Count >= 1 Then
            Dim contacttype As String = contactall.Tables.Item(0).Columns(2).ColumnName.ToString()
            'Dim contactstat As String = contactall.Tables.Item(0).Columns(3).ColumnName.ToString()
            Dim NavigateUrls As Array = Array.CreateInstance(GetType(String), 1) '时间和ID做主键

            NavigateUrls(0) = contacttype
            'NavigateUrls(1) = contactstat
            editcol.DataNavigateUrlFields = NavigateUrls
        End If
        contactView.Columns.Add(editcol)
        contactView.DataBind()

        If contactall.Tables(0).Rows.Count >= 1 Then
            contactView.HeaderRow.Cells(1).Text = obiXmlTr.GetLabelName("EngLabelMsg", "BB-INSTALLHD1")
            contactView.HeaderRow.Cells(1).Font.Size = 8
            contactView.HeaderRow.Cells(2).Text = obiXmlTr.GetLabelName("EngLabelMsg", "BB-INSTALLHD2")
            contactView.HeaderRow.Cells(2).Font.Size = 8
            contactView.HeaderRow.Cells(3).Text = obiXmlTr.GetLabelName("EngLabelMsg", "TELEPHONETYPE")
            contactView.HeaderRow.Cells(3).Font.Size = 8
            contactView.HeaderRow.Cells(4).Text = obiXmlTr.GetLabelName("EngLabelMsg", "STATUS")
            contactView.HeaderRow.Cells(4).Font.Size = 8
            contactView.HeaderRow.Cells(5).Text = obiXmlTr.GetLabelName("EngLabelMsg", "PICNAME")
            contactView.HeaderRow.Cells(5).Font.Size = 8
            contactView.HeaderRow.Cells(6).Text = obiXmlTr.GetLabelName("EngLabelMsg", "TELEPHONE")
            contactView.HeaderRow.Cells(6).Font.Size = 8
        End If


    End Sub
#End Region

    Protected Sub contactView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles contactView.PageIndexChanging
        Me.contactView.PageIndex = e.NewPageIndex
        viewcontactviewbond()
        viewaddressviewbond()
        viewroviewbond()
    End Sub
#Region " load roview bond"
    Public Sub loadroviewbond()
        Dim roEntity As New clsCustomerRecord()
        If (Trim(Me.custid.Text) <> "") Then
            roEntity.CustomerID = Me.custid.Text.ToUpper()

        End If
        roEntity.Customerpf = Me.CustomerPrefix.Text.ToUpper()
        Dim roall As DataSet = roEntity.GetRO()
        'If addressall.Tables(0).Rows.Count <> 0 Then
        Me.roView.DataSource = roall
        Session("roView") = roall
        Me.roView.AllowPaging = True
        roView.AllowSorting = True

        roView.DataBind()
        Dim obiXmlTr As New clsXml
        obiXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        If roall.Tables(0).Rows.Count >= 1 Then
            roView.HeaderRow.Cells(0).Text = obiXmlTr.GetLabelName("EngLabelMsg", "MODEL")
            roView.HeaderRow.Cells(0).Font.Size = 8
            roView.HeaderRow.Cells(1).Text = obiXmlTr.GetLabelName("EngLabelMsg", "SERIALNO")
            roView.HeaderRow.Cells(1).Font.Size = 8
            roView.HeaderRow.Cells(2).Text = obiXmlTr.GetLabelName("EngLabelMsg", "STATUS")
            roView.HeaderRow.Cells(2).Font.Size = 8
            roView.HeaderRow.Cells(3).Text = obiXmlTr.GetLabelName("EngLabelMsg", "LASTSERVICEDATE")
            roView.HeaderRow.Cells(3).Font.Size = 8
            roView.HeaderRow.Cells(4).Text = obiXmlTr.GetLabelName("EngLabelMsg", "INSTALLDATE")
            roView.HeaderRow.Cells(4).Font.Size = 8

        End If


    End Sub


#End Region
    Protected Sub HyperLink2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HyperLink2.Click
        Dim url As String = "~/PresentationLayer/masterrecord/addinstalleBase.aspx?"
        url &= "custid=" & Me.custid.Text & "&"
        url &= "custpre=" & Me.CustomerPrefix.Text.ToString() & "&"
        url &= "custname=" & Me.CustomerName.Text.ToString()
        Response.Redirect(url)
    End Sub

    Protected Sub roView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles roView.PageIndexChanging
        Me.addressView.PageIndex = e.NewPageIndex
        viewaddressviewbond()
        viewcontactviewbond()
        viewroviewbond()
    End Sub
#Region " view ro bond"
    Public Sub viewroviewbond()
        Dim roEntity As New clsCustomerRecord()
        If (Trim(Me.custid.Text) <> "") Then
            roEntity.CustomerID = Me.custid.Text.ToUpper()

        End If
        roEntity.Customerpf = Me.CustomerPrefix.Text.ToUpper()
        Dim roall As DataSet = roEntity.GetRO()
        'If addressall.Tables(0).Rows.Count <> 0 Then
        Me.roView.DataSource = roall
        roView.DataBind()
        Dim obiXmlTr As New clsXml
        obiXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        If roall.Tables(0).Rows.Count >= 1 Then
            roView.HeaderRow.Cells(0).Text = obiXmlTr.GetLabelName("EngLabelMsg", "MODEL")
            roView.HeaderRow.Cells(0).Font.Size = 8
            roView.HeaderRow.Cells(1).Text = obiXmlTr.GetLabelName("EngLabelMsg", "SERIALNO")
            roView.HeaderRow.Cells(1).Font.Size = 8
            roView.HeaderRow.Cells(2).Text = obiXmlTr.GetLabelName("EngLabelMsg", "STATUS")
            roView.HeaderRow.Cells(2).Font.Size = 8
            roView.HeaderRow.Cells(3).Text = obiXmlTr.GetLabelName("EngLabelMsg", "LASTSERVICEDATE")
            roView.HeaderRow.Cells(3).Font.Size = 8
            roView.HeaderRow.Cells(4).Text = obiXmlTr.GetLabelName("EngLabelMsg", "INSTALLDATE")
            roView.HeaderRow.Cells(4).Font.Size = 8

        End If

    End Sub
#End Region
#Region " view addressview bond"
    Public Sub viewaddressviewbond()
        Dim addressEntity As New clsAddress()

        If (Trim(Me.custid.Text) <> "") Then
            addressEntity.CustomerID = Me.custid.Text.ToUpper()
        End If
        addressEntity.CustPrefix = Me.CustomerPrefix.Text.ToUpper()
        Dim addressall As DataSet = addressEntity.GetAddress()

        addressView.DataSource = addressall
        addressView.DataBind()
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")

        If addressall.Tables(0).Rows.Count >= 1 Then
            addressView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "ADDRESSTYPE")
            addressView.HeaderRow.Cells(1).Font.Size = 8
            addressView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "STATUS")
            addressView.HeaderRow.Cells(2).Font.Size = 8
            addressView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "ADDRESS1")
            addressView.HeaderRow.Cells(3).Font.Size = 8
            addressView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "ADDRESS2")
            addressView.HeaderRow.Cells(4).Font.Size = 8
            addressView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "POCODE")
            addressView.HeaderRow.Cells(5).Font.Size = 8
        End If
    End Sub
#End Region
#Region " view contactview bond"
    Public Sub viewcontactviewbond()
        Dim contactEntity As New clsContact()
        If (Trim(Me.custid.Text) <> "") Then
            contactEntity.CustomerID = Me.custid.Text.ToUpper()
        End If
        contactEntity.CustPrefix = Me.CustomerPrefix.Text.ToUpper()
        Dim contactall As DataSet = contactEntity.Getcontact()
        'If addressall.Tables(0).Rows.Count <> 0 Then
        Me.contactView.DataSource = contactall
        contactView.DataBind()
        Dim obiXmlTr As New clsXml
        obiXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        If contactall.Tables(0).Rows.Count >= 1 Then
            contactView.HeaderRow.Cells(1).Text = obiXmlTr.GetLabelName("EngLabelMsg", "TELEPHONETYPE")
            contactView.HeaderRow.Cells(1).Font.Size = 8
            contactView.HeaderRow.Cells(2).Text = obiXmlTr.GetLabelName("EngLabelMsg", "STATUS")
            contactView.HeaderRow.Cells(2).Font.Size = 8
            contactView.HeaderRow.Cells(3).Text = obiXmlTr.GetLabelName("EngLabelMsg", "PICNAME")
            contactView.HeaderRow.Cells(3).Font.Size = 8
            contactView.HeaderRow.Cells(4).Text = obiXmlTr.GetLabelName("EngLabelMsg", "TELEPHONE")
            contactView.HeaderRow.Cells(4).Font.Size = 8
        End If
    End Sub
#End Region

    Sub saveRecord()
        Dim CustomerEntity As New clsCustomerRecord()

        If (Trim(Me.CustomerPrefix.Text) <> "") Then
            CustomerEntity.Customerpf = Me.CustomerPrefix.Text.ToString().ToUpper()
        End If
        Dim ctryall As DataSet = CustomerEntity.GetCustomerID()

        If ctryall.Tables(0).Rows.Count <> 0 Then
            Dim maxid As Integer
            maxid = Convert.ToInt64(ctryall.Tables(0).Rows(0).Item(0)) + 1
            customerid = maxid.ToString()
            Me.CustomerPrefix.Text = ctryall.Tables(0).Rows(0).Item(1).ToString()
        End If


        CustomerEntity.CustomerID = customerid.ToString().ToUpper()


        If (Trim(Me.JDECID.Text) <> "") Then
            CustomerEntity.JDECustID = Me.JDECID.Text.ToString().ToUpper()
        End If
        If (Trim(Me.CustomerName.Text) <> "") Then
            CustomerEntity.CustomerName = Me.CustomerName.Text.ToString().ToUpper()
        End If
        If (Trim(Me.AlternateName.Text) <> "") Then
            CustomerEntity.AlterName = Me.AlternateName.Text.ToString().ToUpper()
        End If
        If (Me.Races.SelectedValue().ToString() <> "") Then
            CustomerEntity.Races = Me.Races.SelectedValue().ToString().ToUpper()
        End If
        If (Me.CustomerType.SelectedValue().ToString() <> "") Then
            CustomerEntity.CustType = Me.CustomerType.SelectedValue().ToString()
        End If
        If (Me.status.SelectedValue().ToString() <> "") Then
            CustomerEntity.Status = Me.status.SelectedValue().ToString()
        End If
        If (Trim(Me.notes.Text) <> "") Then
            CustomerEntity.Notes = Me.notes.Text.ToString().ToUpper()
        End If
        If (Me.SVCID.SelectedValue().ToString() <> "") Then
            CustomerEntity.Svcid = Me.SVCID.SelectedValue().ToString()
        End If
        If (Trim(Me.creatby.Text) <> "") Then
            CustomerEntity.CreatBy = Session("userID").ToString().ToUpper
        End If
        If (Trim(CustomerEmail.Text) <> "") Then
            CustomerEntity.email = CustomerEmail.Text.ToString().ToUpper()
        End If
        If (Trim(Me.modfyby.Text) <> "") Then
            CustomerEntity.ModBy = Session("userID").ToString().ToUpper
        End If

        'get selected preferred language - Added by LLY 201702
        If (Me.prefLang.SelectedValue().ToString() <> "") Then
            CustomerEntity.PrefLang = Me.prefLang.SelectedValue.Substring(9, Me.prefLang.SelectedValue.ToString.Length - 9) 'PREFLANG-MALAY
        End If

        CustomerEntity.username = Session("userID")
        Dim dupCount As Integer = CustomerEntity.GetDuplicatedcustomerid()
        If dupCount.Equals(-1) Then
            Dim objXm As New clsXml
            objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            errlab.Text = objXm.GetLabelName("StatusMessage", "DUPCUSTID")
            errlab.Visible = True
            MessageBox1.Alert(objXm.GetLabelName("StatusMessage", "DUPCUSTID"))
        ElseIf dupCount.Equals(0) Then

            Dim insCtryCnt As Integer = CustomerEntity.Insert()
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            If insCtryCnt = 0 Then
                Dim objXm As New clsXml
                objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                errlab.Text = objXm.GetLabelName("StatusMessage", "BB-HintMsg-Suc")
                errlab.Visible = True
                MessageBox1.Alert(objXm.GetLabelName("StatusMessage", "BB-HintMsg-Suc"))
                Dim updCtryCnt As Integer = CustomerEntity.UpdateCustomerID()
                If updCtryCnt = 0 Then
                    Session("sybom") = True
                    Me.custid.Text = customerid.ToString.ToUpper()
                Else
                    Response.Redirect("~/PresentationLayer/Error.aspx")
                End If
            End If
            SaveAddress()
            SaveContact()
            linkAddAddress.Enabled = True
            LinkButton2.Enabled = True
            HyperLink2.Enabled = True
            lblViewInst.Enabled = True
            lblCreateApp.Enabled = True
        Else
            Response.Redirect("~/PresentationLayer/Error.aspx")
        End If
        saveButton.Enabled = False
        viewaddressviewbond()
        viewcontactviewbond()
        viewroviewbond()
    End Sub
    Protected Sub MessageBox1_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox1.GetMessageBoxResponse
        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then

            saveRecord()
        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
        End If
    End Sub

    Protected Sub ctrid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ctrid.SelectedIndexChanged
        Me.areaid.Items.Clear()
        'If Me.ctrid.Text <> "" Then
        Dim rank As String = Session("login_rank")
        Dim state As New clsCommonClass
        If (Me.ctrid.SelectedValue().ToString() <> "") Then
            state.spctr = Me.ctrid.SelectedValue().ToString().ToUpper()
        End If
        state.rank = rank
        Dim stateds As New DataSet
        stateds = state.Getcomidname("BB_MASSTAT_IDNAME")
        If stateds.Tables.Count <> 0 Then
            databonds(stateds, Me.staid)
        End If
        Me.staid.Items.Insert(0, "")
        'Dim script As String = "self.location='#ctry';"
        'Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "test", script, True)
    End Sub

    Protected Sub staid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles staid.SelectedIndexChanged
        Me.areaid.Items.Clear()
        Dim rank As String = Session("login_rank")
        Dim area As New clsCommonClass
        If (ctrid.SelectedValue().ToString() <> "") Then
            area.spctr = Me.ctrid.SelectedValue().ToString().ToUpper()
        End If
        If (staid.SelectedValue().ToString() <> "") Then
            area.spstat = Me.staid.SelectedValue().ToString().ToUpper()
        End If
        area.rank = rank
        Dim areads As New DataSet
        areads = area.Getcomidname("BB_MASAREA_IDNAME")
        If areads.Tables.Count <> 0 Then
            databonds(areads, Me.areaid)
        End If
        Me.areaid.Items.Insert(0, "")
        'Dim script As String = "self.location='#ctry';"
        'Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "test", script, True)
    End Sub

    Protected Sub linkAddAddress_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles linkAddAddress.Click
        If custid.Text <> "" Then
            Dim ctryid As String = ctrid.SelectedValue.ToString.ToUpper()
            ctryid = Server.UrlEncode(ctryid)
            Dim Prefix As String = Me.CustomerPrefix.Text.Trim()
            Prefix = Server.UrlEncode(Prefix)
            Dim custname As String = Me.CustomerName.Text.Trim()
            custname = Server.UrlEncode(custname)
            Dim symbol As String = 0
            symbol = Server.UrlEncode(symbol)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim objXm As New clsXml
            objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            Dim msginfo As String = objXm.GetLabelName("StatusMessage", "SAVEORNOT")
            Dim msgtitle As String = objXm.GetLabelName("StatusMessage", "POCTITLE")
            MessageboxSaveAdd.Confirm(msginfo)
            viewaddressviewbond()
            viewcontactviewbond()
            viewroviewbond()
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If Trim(Me.TextBox12.Text) = "" Then
                lblViewInst.Enabled = False
            Else
                lblViewInst.Enabled = True
            End If


        Else
            Dim objXm As New clsXml
            objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            errlab.Text = objXm.GetLabelName("StatusMessage", "IFSAVE")
            errlab.Visible = True
            MessageBox1.Alert(objXm.GetLabelName("StatusMessage", "IFSAVE"))
            viewaddressviewbond()
            viewcontactviewbond()
            viewroviewbond()
        End If
    End Sub

    Protected Sub MessageboxSaveAdd_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageboxSaveAdd.GetMessageBoxResponse
        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
            SaveAddress()
        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
        End If

    End Sub

    Function IsDuplicatedAddress() As Boolean
        Dim addressEntity As New clsAddress()
        If (Trim(Me.custid.Text) <> "") Then
            addressEntity.CustomerID = custid.Text.ToUpper()
            addressEntity.CustPrefix = Me.CustomerPrefix.Text.Trim()
        End If
        If (Trim(Me.Address1.Text) <> "") Then
            addressEntity.Address1 = Address1.Text.ToUpper()
        End If

        If (Trim(Me.Address2.Text) <> "") Then
            addressEntity.Address2 = Me.Address2.Text.ToString().ToUpper()
        End If

        If (areaid.SelectedValue().ToString() <> "") Then
            addressEntity.AreaID = Me.areaid.SelectedValue().ToString().ToUpper()
        End If

        If (ctrid.SelectedValue().ToString() <> "") Then
            addressEntity.CountryID = Me.ctrid.SelectedValue().ToString().ToUpper()
        End If

        If (staid.SelectedValue().ToString() <> "") Then
            addressEntity.StateID = Me.staid.SelectedValue().ToString().ToUpper()
        End If

        If (Me.status.SelectedItem.Value.ToString() <> "") Then
            addressEntity.Status = status.SelectedItem.Value.ToString()
        End If

        'If (Trim(Me.AddressType.Text) <> "") Then
        addressEntity.AddressType = "SITE" 'Me.AddressType.Text.ToString().ToUpper()
        'End If

        If (Trim(Me.CustomerName.Text) <> "") Then
            addressEntity.PICName = Me.CustomerName.Text.ToString().ToUpper()
        End If
        If (Trim(Me.POCode.Text.ToString()) <> "") Then
            addressEntity.POCode = Me.POCode.Text.ToString().ToUpper()
        End If

        If (Trim(creatbybox.Text) <> "") Then
            addressEntity.CreatedBy = Session("userID").ToString().ToUpper
        End If
        'If (Trim(CreatedDate.Text) <> "") Then
        '    TechnicianEntity.CreateDate = Me.CreatedDate.Text()
        'End If
        If (Trim(modfybybox.Text) <> "") Then
            addressEntity.ModifiedBy = Session("userID").ToString().ToUpper
        End If
        addressEntity.username = Session("userID")

        Dim dupCount As Integer = addressEntity.GetDuplicateAddress()

        If dupCount.Equals(-1) Then
            Dim objXm As New clsXml
            Dim existCust As String
            Dim errorMsg As String
            objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            existCust = addressEntity.ExistCustID
            errorMsg = Trim(objXm.GetLabelName("StatusMessage", "DUPLICATE_ADDR_WITH") & " " & existCust.Trim & ". ")
            'errorMsg = errorMsg & Trim(objXm.GetLabelName("StatusMessage", "BB-HintMsg-NOTCREATE"))
            errlab.Text = errorMsg
            errlab.Visible = True
            MessageBox1.Alert(errorMsg)
            Return True
        End If
        Return False
    End Function

    Private Function SaveAddress()
        Dim addressEntity As New clsAddress()
        If (Trim(Me.custid.Text) <> "") Then
            addressEntity.CustomerID = custid.Text.ToUpper()
            addressEntity.CustPrefix = Me.CustomerPrefix.Text.Trim()
        End If
        If (Trim(Me.Address1.Text) <> "") Then
            addressEntity.Address1 = Address1.Text.ToUpper()
        End If

        If (Trim(Me.Address2.Text) <> "") Then
            addressEntity.Address2 = Me.Address2.Text.ToString().ToUpper()
        End If

        If (areaid.SelectedValue().ToString() <> "") Then
            addressEntity.AreaID = Me.areaid.SelectedValue().ToString().ToUpper()
        End If

        If (ctrid.SelectedValue().ToString() <> "") Then
            addressEntity.CountryID = Me.ctrid.SelectedValue().ToString().ToUpper()
        End If

        If (staid.SelectedValue().ToString() <> "") Then
            addressEntity.StateID = Me.staid.SelectedValue().ToString().ToUpper()
        End If

        If (Me.status.SelectedItem.Value.ToString() <> "") Then
            addressEntity.Status = status.SelectedItem.Value.ToString()
        End If

        'If (Trim(Me.AddressType.Text) <> "") Then
        addressEntity.AddressType = "SITE" 'Me.AddressType.Text.ToString().ToUpper()
        'End If

        If (Trim(Me.CustomerName.Text) <> "") Then
            addressEntity.PICName = Me.CustomerName.Text.ToString().ToUpper()
        End If
        If (Trim(Me.POCode.Text.ToString()) <> "") Then
            addressEntity.POCode = Me.POCode.Text.ToString().ToUpper()
        End If

        If (Trim(creatbybox.Text) <> "") Then
            addressEntity.CreatedBy = Session("userID").ToString().ToUpper
        End If
        'If (Trim(CreatedDate.Text) <> "") Then
        '    TechnicianEntity.CreateDate = Me.CreatedDate.Text()
        'End If
        If (Trim(modfybybox.Text) <> "") Then
            addressEntity.ModifiedBy = Session("userID").ToString().ToUpper
        End If
        addressEntity.username = Session("userID")


        Dim insCtryCnt As Integer = addressEntity.Insert()
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        If insCtryCnt = 0 Then

            Dim objXm As New clsXml
            objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            errlab.Text = objXm.GetLabelName("StatusMessage", "BB-HintMsg-Suc")
            errlab.Visible = True
            MessageBox1.Alert(objXm.GetLabelName("StatusMessage", "BB-HintMsg-Suc"))
            'Dim addressEntity As New clsAddress()

            Dim addressall As DataSet = addressEntity.GetAddress()
            'If addressall.Tables(0).Rows.Count <> 0 Then
            Me.addressView.DataSource = addressall
            Session("addressView") = addressall
            addressView.AllowPaging = True
            addressView.AllowSorting = True
            addressView.DataBind()
            Dim obiXmlTr As New clsXml
            obiXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            viewaddressviewbond()
            viewcontactviewbond()
            viewroviewbond()
        End If

        'Dim dupCount As Integer = addressEntity.GetDuplicateAddress()

        'If dupCount.Equals(-1) Then
        '    Dim objXm As New clsXml
        '    Dim existCust As String
        '    Dim errorMsg As String
        '    objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        '    existCust = addressEntity.ExistCustID
        '    errorMsg = objXm.GetLabelName("StatusMessage", "BB-HintMsg-DUPADDR") & " " & existCust & ". "
        '    errorMsg = errorMsg & objXm.GetLabelName("StatusMessage", "BB-HintMsg-NOTCREATE")
        '    errlab.Text = errorMsg
        '    errlab.Visible = True
        '    MessageBox1.Alert(errorMsg)
        'ElseIf dupCount.Equals(0) Then 'if no error then add
        '    Dim addressdupCount As Integer = addressEntity.GetDupaddress()
        '    If addressdupCount.Equals(-1) Then
        '        Dim objXm As New clsXml
        '        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        '        errlab.Text = objXm.GetLabelName("StatusMessage", "ADDRESSFAIL")
        '        errlab.Visible = True
        '        Dim addressall As DataSet = addressEntity.GetAddress()
        '        'If addressall.Tables(0).Rows.Count <> 0 Then
        '        Me.addressView.DataSource = addressall
        '        Session("addressView") = addressall
        '        addressView.AllowPaging = True
        '        addressView.AllowSorting = True
        '        addressView.DataBind()
        '        Dim obiXmlTr As New clsXml
        '        obiXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        '        viewaddressviewbond()
        '        viewcontactviewbond()
        '        viewroviewbond()
        '    ElseIf addressdupCount.Equals(0) Then
        '        Dim insCtryCnt As Integer = addressEntity.Insert()
        '        Dim objXmlTr As New clsXml
        '        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        '        If insCtryCnt = 0 Then

        '            Dim objXm As New clsXml
        '            objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        '            errlab.Text = objXm.GetLabelName("StatusMessage", "BB-HintMsg-Suc")
        '            errlab.Visible = True
        '            MessageBox1.Alert(objXm.GetLabelName("StatusMessage", "BB-HintMsg-Suc"))
        '            'Dim addressEntity As New clsAddress()

        '            Dim addressall As DataSet = addressEntity.GetAddress()
        '            'If addressall.Tables(0).Rows.Count <> 0 Then
        '            Me.addressView.DataSource = addressall
        '            Session("addressView") = addressall
        '            addressView.AllowPaging = True
        '            addressView.AllowSorting = True
        '            addressView.DataBind()
        '            Dim obiXmlTr As New clsXml
        '            obiXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        '            viewaddressviewbond()
        '            viewcontactviewbond()
        '            viewroviewbond()
        '        End If
        '    Else
        '        'Dim msg As String = objXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Fail")
        '        'Dim msgBox As New MessageBox(Me)
        '        'msgBox.Show(msg)
        '        Response.Redirect("~/PresentationLayer/Error.aspx")
        '    End If
        'End If
    End Function

    Private Function SaveContact()

        'tele1 - dist contact
        'tele2 - customer tele(H)
        'tele3 - customer tele(o)
        'tele4 - customer fax
        'tele5 - customer mob1
        'tele6 - Customer mob2
        'DistEmail - Distributor Email
        Dim i As Integer
        'add the records
        For i = 1 To 6 'loop through all the contacts provided
            Dim contactEntity As New clsContact()
            Dim telepass As New clsCommonClass()
            'get the tele text
            Select Case i
                Case 1
                    If (Trim(tele1.Text) <> "") Then
                        contactEntity.Telephone = telepass.telconvertpass(tele1.Text)
                        contactEntity.TelephType = "DIST"
                        contactEntity.PICName = Me.DistName.Text.ToString()
                    End If
                Case 2
                    If (Trim(tele2.Text) <> "") Then
                        contactEntity.Telephone = telepass.telconvertpass(tele2.Text)
                        contactEntity.TelephType = "TELH"
                        contactEntity.PICName = Me.txtRemark2.Text.ToString()
                    End If
                Case 3
                    If (Trim(tele3.Text) <> "") Then
                        contactEntity.Telephone = telepass.telconvertpass(tele3.Text)
                        contactEntity.TelephType = "TELO"
                        contactEntity.PICName = Me.txtRemark3.Text.ToString()
                    End If
                Case 4
                    If (Trim(tele4.Text) <> "") Then
                        contactEntity.Telephone = telepass.telconvertpass(tele4.Text)
                        contactEntity.TelephType = "FAX"
                        contactEntity.PICName = Me.txtRemark4.Text.ToString()
                    End If
                Case 5
                    If (Trim(tele5.Text) <> "") Then
                        contactEntity.Telephone = telepass.telconvertpass(tele5.Text)
                        contactEntity.TelephType = "MOB1"
                        contactEntity.PICName = Me.txtRemark5.Text.ToString()
                    End If
                Case 6
                    If (Trim(tele6.Text) <> "") Then
                        contactEntity.Telephone = telepass.telconvertpass(tele6.Text)
                        contactEntity.TelephType = "MOB2"
                        contactEntity.PICName = Me.txtRemark6.Text.ToString()
                    End If
            End Select

            If Not contactEntity.Telephone = "" Then
                If (Trim(Me.custid.Text) <> "") Then
                    contactEntity.CustomerID = custid.Text.ToUpper()
                    contactEntity.CustPrefix = Me.CustomerPrefix.Text.Trim()
                End If
                If (Trim(Me.DistEmail.Text) <> "") Then
                    contactEntity.Email = Me.DistEmail.Text.ToString()
                End If
                If (Me.status.SelectedItem.Value.ToString() <> "") Then
                    contactEntity.Status = status.SelectedItem.Value
                End If
                'If (Trim(Me.DistName.Text) <> "") Then
                '    contactEntity.PICName = Me.DistName.Text.ToString()
                'End If
                If (Trim(creatbybox.Text) <> "") Then
                    contactEntity.CreatedBy = Session("userID").ToString().ToUpper
                End If
                If (Trim(modfybybox.Text) <> "") Then
                    contactEntity.ModifiedBy = Session("userID").ToString().ToUpper
                End If
                contactEntity.username = Session("userID")
                Dim dupCount As Integer = contactEntity.GetDupcontacttype()
                If dupCount.Equals(-1) Then
                    Dim objXm As New clsXml
                    objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    errlab.Text = objXm.GetLabelName("StatusMessage", "TELEPHTYPEFAIL")
                    errlab.Visible = True
                    MessageBox1.Alert(objXm.GetLabelName("StatusMessage", "TELEPHTYPEFAIL"))
                ElseIf dupCount.Equals(0) Then
                    Dim insCtryCnt As Integer = contactEntity.Insert()
                    Dim objXmlTr As New clsXml
                    objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    If insCtryCnt = 0 Then
                        Dim objXm As New clsXml
                        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                        errlab.Text = objXm.GetLabelName("StatusMessage", "BB-HintMsg-Suc")
                        errlab.Visible = True
                        MessageBox1.Alert(errlab.Text)
                    Else
                        Response.Redirect("~/PresentationLayer/Error.aspx")
                    End If
                End If
            End If
        Next
        viewaddressviewbond()
        viewcontactviewbond()
        viewroviewbond()
    End Function

    Protected Sub LinkButton2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton2.Click
        If custid.Text <> "" Then
            Dim ctryid As String = ctrid.SelectedValue.ToString.ToUpper()
            ctryid = Server.UrlEncode(ctryid)
            Dim Prefix As String = Me.CustomerPrefix.Text.Trim()
            Prefix = Server.UrlEncode(Prefix)
            Dim custname As String = Me.CustomerName.Text.Trim()
            custname = Server.UrlEncode(custname)
            Dim symbol As String = 0
            symbol = Server.UrlEncode(symbol)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim objXm As New clsXml
            objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            Dim msginfo As String = objXm.GetLabelName("StatusMessage", "SAVEORNOT")
            Dim msgtitle As String = objXm.GetLabelName("StatusMessage", "POCTITLE")
            MessageboxSaveCon.Confirm(msginfo)
            viewaddressviewbond()
            viewcontactviewbond()
            viewroviewbond()
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Else
            Dim objXm As New clsXml
            objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            errlab.Text = objXm.GetLabelName("StatusMessage", "IFSAVE")
            errlab.Visible = True
            MessageBox1.Alert(errlab.Text)
            viewaddressviewbond()
            viewcontactviewbond()
            viewroviewbond()
        End If
    End Sub

    Protected Sub MessageboxSaveCon_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageboxSaveCon.GetMessageBoxResponse
        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
            SaveContact()
        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
        End If

    End Sub


    Protected Sub lblViewInst_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblViewInst.Click
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim rouid As String = ""
        Dim gSelectedValue As String
        Dim strCusID As String = ""
        Dim strPrefix As String = Session("login_ctryid")
        Dim strSerial As String = ""
        Dim strMod As String = ""
        Dim strTempURL As String = ""

        strCusID = Me.custid.Text
        strSerial = Me.TextBox12.Text
        strMod = Me.TextBox12.Text

        strCusID = Me.custid.Text
        'try get the ROUID
        Dim CustomerEntity As New clsCustomerRecord()
        CustomerEntity.CustomerID = strCusID
        CustomerEntity.RoSerialNo = strSerial
        CustomerEntity.modelid = strMod
        CustomerEntity.Customerpf = strPrefix
        rouid = CustomerEntity.GetInstallROUID()

        If rouid Is Nothing Then
            'addinfo.Text = "No records found associated with the selected customer."
            errlab.Text = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRRO")
            errlab.Visible = True
            MessageBox1.Alert(errlab.Text)
        Else

            strTempURL = "rouid=" + rouid
            strTempURL = "~/PresentationLayer/masterrecord/ModifyinstallBase.aspx?" + strTempURL
            Response.Redirect(strTempURL)
        End If
    End Sub

    Protected Sub lblCreateApp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblCreateApp.Click
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim strTempURL As String

        Dim gSelectedValue As String

        Dim strCusID As String = ""
        Dim strPrefix As String = Session("login_ctryid")
        Dim strCusName As String = ""
        strCusID = Me.custid.Text
        strCusName = Me.CustomerName.Text
        strCusID = Server.UrlEncode(strCusID)
        strPrefix = Server.UrlEncode(strPrefix)
        strTempURL = "custID=" + strCusID + "&custPf=" + strPrefix
        strTempURL = "~/PresentationLayer/function/addappointment.aspx?" + strTempURL
        Response.Redirect(strTempURL)
    End Sub

    Protected Sub areaid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles areaid.SelectedIndexChanged

    End Sub

    Protected Sub SVCID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles SVCID.SelectedIndexChanged

    End Sub

    Sub changeCustomerType(ByVal strCustomerType As String)

        If strCustomerType = "INDIVIDUAL" Then
            Me.Races.Items.Clear()
            Dim race As New clsrlconfirminf()
            Dim raceParam As ArrayList = New ArrayList
            raceParam = race.searchconfirminf("PERSONRACE")
            Dim racecount As Integer
            Dim raceid As String
            Dim racenm As String
            For racecount = 0 To raceParam.Count - 1
                raceid = raceParam.Item(racecount)
                racenm = raceParam.Item(racecount + 1)
                racecount = racecount + 1
                If Not raceid.Equals("ZNONAPPL") Then


                    Me.Races.Items.Add(New ListItem(racenm.ToString(), raceid.ToString()))
                End If

            Next

        Else

            Me.Races.Items.Clear()
            Dim race As New clsrlconfirminf()
            Dim raceParam As ArrayList = New ArrayList
            raceParam = race.searchconfirminf("PERSONRACE")
            Dim racecount As Integer
            Dim raceid As String
            Dim racenm As String
            For racecount = 0 To raceParam.Count - 1
                raceid = raceParam.Item(racecount)
                racenm = raceParam.Item(racecount + 1)
                racecount = racecount + 1
                ' Modified by Ryan Estandarte 1/10/2012
                'If raceid.Equals("ZNONAPPL") Then
                '    Me.Races.Items.Add(New ListItem(racenm.ToString(), raceid.ToString()))
                'End If

                If Not raceid.Equals("ZNONAPPL") Then
                    Races.Items.Add(New ListItem(racenm.ToString(), raceid.ToString()))
					else 'added by Tomas 20130116
					Me.Races.Items.Add(New ListItem(racenm.ToString(), raceid.ToString())) 'added by Tomas 20130116
                End If

            Next

        End If
    End Sub

    Protected Sub saveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveButton.Click
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim msginfo As String = objXm.GetLabelName("StatusMessage", "SAVEORNOT")
        Dim msgtitle As String = objXm.GetLabelName("StatusMessage", "SAVETITLE")

        Dim selectedareaid As String = Request.Form("areaid")

        Me.areaid.Items.Clear()
        Dim rank As String = Session("login_rank")
        Dim area As New clsCommonClass
        If (ctrid.SelectedValue().ToString() <> "") Then
            area.spctr = Me.ctrid.SelectedValue().ToString().ToUpper()
        End If
        If (staid.SelectedValue().ToString() <> "") Then
            area.spstat = Me.staid.SelectedValue().ToString().ToUpper()
        End If
        area.rank = rank
        Dim areads As New DataSet
        areads = area.Getcomidname("BB_MASAREA_IDNAME")
        If areads.Tables.Count <> 0 Then
            databonds(areads, Me.areaid)
        End If
        Me.areaid.Items.Insert(0, "")

        'areaid.SelectedIndex = areaid.Items.IndexOf(areaid.Items.FindByValue(selectedareaid))
        areaid.SelectedValue = selectedareaid

        Dim selectedRaces As String = Request.Form("Races")
        changeCustomerType(Me.CustomerType.SelectedValue)
        'Races.SelectedIndex = Races.Items.IndexOf(Races.Items.FindByValue(selectedRaces))
        Races.SelectedValue = selectedRaces

        If IsDuplicatedAddress() = False Then
            saveRecord()
        End If
        'MessageBox1.Confirm(msginfo)

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'viewaddressviewbond()
        'viewcontactviewbond()
        'viewroviewbond()

    End Sub

#Region "Ajax"

    <AjaxPro.AjaxMethod()> _
    Public Function GetAreas(ByVal CountryID As String, ByVal StateID As String) As ArrayList
        Dim area As New clsCommonClass

        area.spctr = CountryID
        area.spstat = StateID

        area.rank = 7
        Dim areads As New DataSet
        areads = area.Getcomidname("BB_MASAREA_IDNAME")
        Dim strTemp As String


        Dim Listas As New ArrayList(areads.Tables(0).Rows.Count)
        Dim i As Integer = 0
        Listas.Add("")
        Try
            For i = 0 To areads.Tables(0).Rows.Count - 1
                strTemp = areads.Tables(0).Rows(i).Item(0).ToString & ":" & areads.Tables(0).Rows(i).Item(0).ToString & "-" & areads.Tables(0).Rows(i).Item(1).ToString
                Listas.Add(strTemp)
            Next
        Catch
        End Try

        Return Listas
    End Function

    <AjaxPro.AjaxMethod()> _
        Public Function GetRaces(ByVal strCustomerType As String) As ArrayList
        Dim race As New clsrlconfirminf()
        Dim raceParam As ArrayList = New ArrayList
        raceParam = race.searchconfirminf("PERSONRACE")
        Dim racecount As Integer
        Dim raceid As String
        Dim racenm As String
        Dim Listas As New ArrayList(raceParam.Count / 2)
        Dim icnt As Integer = 0
        Dim strTemp As String

        ''Lyann 20180601 SMR 1805/2423 Changes in Customer Type. 
        ''If strCustomerType = "CORPORATE" or strcustomertype = "GOVERNMENT (2)" or strcustomertype = "GOVERNMENT (3)" or strcustomertype = "GOVERNMENT (8)" Then 'Tomas 20150302
        If strCustomerType = "CORPORATE" Or strCustomerType = "GOVERNMENT" Or strCustomerType = "RENTAL (CORPORATE)" Then

            For racecount = 0 To raceParam.Count - 1
                raceid = raceParam.Item(racecount)
                racenm = raceParam.Item(racecount + 1)
                racecount = racecount + 1
                If raceid.Equals("ZNONAPPL") Then
                    strTemp = raceid & ":" & racenm
                    Listas.Add(strTemp)
                End If

            Next
        Else

            For racecount = 0 To raceParam.Count - 1
                raceid = raceParam.Item(racecount)
                racenm = raceParam.Item(racecount + 1)
                racecount = racecount + 1
                If Not raceid.Equals("ZNONAPPL") Then
                    strTemp = raceid & ":" & racenm
                    Listas.Add(strTemp)
                End If

            Next

        End If

        Return Listas
    End Function
#End Region
End Class
