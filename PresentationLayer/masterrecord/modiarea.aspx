﻿<%@ Page Language="VB" AutoEventWireup="false" EnableEventValidation ="False"  CodeFile="modiarea.aspx.vb" Inherits="PresentationLayer_masterrecord_modiarea" %>

<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <LINK href="../css/style.css" type="text/css" rel="stylesheet">
</head>
<body>
    <form id="form1" runat="server">
    <TABLE border="0" id=countrytab style="width: 100%">
			<tr>
                <td style="width: 95%">
                    <table cellspacing="0" cellpadding="0" width="100%" border="0" style="width: 100%">
                        <tr>
                            <td width="1%" background="../graph/title_bg.gif">
                                <img height="24" src="../graph/title1.gif" width="5"></td>
                            <td class="style2" background="../graph/title_bg.gif" style="width: 100%">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="right" background="../graph/title_bg.gif" style="width: 4%">
                                <img height="24" src="../graph/title_2.gif" width="5"></td>
                        </tr>
                    </table>
            </tr>
             <tr>
           <td style="width: 100%">
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>					
							<TD class="style2" background="../graph/title_bg.gif" style="width: 110%">
                                <font color=red style="width: 100%"><asp:Label ID="errlab" runat="server" Text="Label" Visible=false></asp:Label></font></TD>
						</TR>
			 </TABLE>
               <asp:Label ID="Label5" runat="server" ForeColor="Red"></asp:Label>
             </tr>
			<TR>
				<TD>
					<TABLE id="area" cellSpacing="1" cellPadding="2" bgColor="#b7e6e6" border="0" style="width: 100%">
									  <TR bgColor="#ffffff">
											<TD  align=right style="width: 15%">
                                                &nbsp;<asp:Label ID="ctryidlab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left width="35%">
                                                <asp:TextBox ID="ctrid" runat="server" CssClass="textborder" style="width: 78%"></asp:TextBox></TD>
											<TD  align=right width="15%">
                                                <asp:Label ID="staidLab" runat="server" Text="Label"></asp:Label></TD>
											<TD  align=left style="width: 35%">
                                                <asp:TextBox ID="staid" runat="server" CssClass="textborder" style="width: 78%"></asp:TextBox></TD>
										</TR>
										<TR bgColor="#ffffff">
											<TD  align=right style="width: 15%">
                                                <asp:Label ID="areaidLab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left width="35%" >
                                                <asp:TextBox ID="areaidbox" runat="server" CssClass="textborder" style="width: 78%"></asp:TextBox></TD>
                                                <TD  align=right width="15%">
                                                <asp:Label ID="areanmlab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%" >
                                                <asp:TextBox ID="areanm" runat="server" CssClass="textborder" MaxLength="50" style="width: 78%"></asp:TextBox>
                                                <font color=red style="font-weight: bold">*</font>
                                                </TD>
										</TR>
										<TR bgColor="#ffffff">
											<TD align=right style="width: 15%">
                                                <asp:Label ID="alterLab" runat="server" Text="Label"></asp:Label></TD>
											<TD align="left" width="85%" colspan="3">
                                                <asp:TextBox ID="alter" runat="server" CssClass="textborder" MaxLength="50" style="width: 31%"></asp:TextBox><font color=red></font></TD>
											 
										</TR>
										<TR bgColor="#ffffff">
											<TD  align=right style="height: 22px; width: 15%;">
                                                <asp:Label ID="serviceidlab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left width="35%" style="height: 22px"><asp:DropDownList ID="serviceid" runat="server" CssClass="textborder" style="width: 78%">
                                            </asp:DropDownList></TD>
											<TD align=right width="15%" style="height: 22px">
                                                <asp:Label ID="statusLab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="height: 22px; width: 35%;">
                                                <asp:DropDownList ID="ctrystat" runat="server" CssClass="textborder" style="width: 78%">
                                                </asp:DropDownList>
											</TD>
										</TR>
										
										<TR bgColor="#ffffff">
											<TD align=right style="width: 15%">
                                                <asp:Label ID="creatby" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left width="35%">
                                                <asp:TextBox ID="creatbybox" runat="server" CssClass="textborder" ReadOnly=true style="width: 78%"></asp:TextBox></TD>
											<TD align=right width="15%">
                                                <asp:Label ID="creatdate" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%">
                                                <asp:TextBox ID="creatdtbox" runat="server" CssClass="textborder" ReadOnly=true style="width: 78%"></asp:TextBox></TD>
										</TR>
										<TR bgColor="#ffffff">
											<TD align=right style="width: 15%">
                                                <asp:Label ID="modfyby" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left width="35%">
                                                <asp:TextBox ID="modfybybox" runat="server" CssClass="textborder" ReadOnly=true style="width: 78%"></asp:TextBox></TD>
											<TD align=right width="15%">
                                                <asp:Label ID="mdfydate" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%">
                                                <asp:TextBox ID="mdfydtbox" runat="server" CssClass="textborder" ReadOnly=true style="width: 78%"></asp:TextBox></TD>
										</TR>
                        
                         <tr bgcolor="#ffffff">
                            
                            <td align="left" colspan="4" width="35%" style="width: 100%;">
                            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                <tr>
                                    <td class="style2" background="../graph/title_bg.gif" style="width: 110%; height: 14px;">
                                        <font   style="width: 100%">
                                            <asp:Label ID="potitle" runat="server" Text="Label"></asp:Label></font></td>
                                </tr>
                            </table>
                            
                            </td>
                        </tr>
                        
                        
										<TR bgColor="#ffffff">
											<TD align=right style="height: 30px; width: 15%;">
                                                <asp:Label ID="POlab" runat="server" Text="Label"></asp:Label></TD>
											<TD align="left" width="35%" colspan="3" style="width: 100%;">
                                                <asp:TextBox ID="PObox" runat="server" CssClass="textborder" MaxLength="10"></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="poerr" runat="server" ControlToValidate="PObox"
                                                    ForeColor="White" ValidationExpression="\d{0,20}">*</asp:RegularExpressionValidator>
                                                <asp:DropDownList ID="POstat" runat="server" CssClass="textborder" Width="104px">
                                                </asp:DropDownList>
                                                <asp:LinkButton ID="addButton" runat="server"></asp:LinkButton></TD>
											
										</TR>
										 
						 	</TABLE>&nbsp;&nbsp;
						 	
					<div>	
                    <asp:GridView ID="POView" runat="server">
                        <SelectedRowStyle BackColor="White" />
                    </asp:GridView> 
                 
                    </div>	
						</TD>
					</TR>
						<tr>
					    <td>
                            <asp:Label ID="Label1" runat="server" ForeColor="Red"></asp:Label></td>
					</tr>
				<TR>
						<TD style="height: 37px; width: 416px;">
							<TABLE id="Table1" cellSpacing="1" cellPadding="1"  border="0">
									 
							</TABLE>
                                <asp:LinkButton ID="saveButton" runat="server"></asp:LinkButton>
                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            <asp:HyperLink ID="cancelLink" runat="server" NavigateUrl="~/PresentationLayer/masterrecord/locale.aspx">[cancelLink]</asp:HyperLink></TD>
					</TR>
			</TABLE>
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
            ShowSummary="False" />
                                                <asp:RequiredFieldValidator ID="arenmerr" runat="server" ControlToValidate="areanm" ForeColor="White">*</asp:RequiredFieldValidator>
        <cc1:messagebox id="MessageBox1" runat="server"></cc1:messagebox>
        <cc1:MessageBox ID="MessageBox2" runat="server" />

    </form>
</body>
</html>
