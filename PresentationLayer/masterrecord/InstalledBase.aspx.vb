﻿
Imports BusinessEntity
Imports System.Configuration
Imports System.Xml
Imports System.Data.SqlClient
Imports System.Data
Partial Class PresentationLayer_masterrecord_InstalledBase
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    'Response.Redirect("~/PresentationLayer/logon.aspx")
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                'Response.Redirect("~/PresentationLayer/logon.aspx")
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try
            Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            'linkbutton

            LBadd.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Add")
            LBsearch.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Search")
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Installtl")
            modelidlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0001")
            serialnoLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0002")
            statusLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0019")
            Label1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Maslbleinfo")
            Label1.Visible = False
            Me.addinfo.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-AR")
            Me.addinfo.Visible = False
            cusidlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0003")
            cusnamelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0005")
            telelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0021")
            arealab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0022")
            Label2.Text = objXmlTr.GetLabelName("EngLabelMsg", "CUST_FOUND")


            'create the dropdownlist
            Dim stat As New clsrlconfirminf()
            Dim param As ArrayList = New ArrayList
            param = stat.searchconfirminf("STATUS")
            Dim count As Integer
            Dim statid As String
            Dim statnm As String
            count = 0
            For count = 0 To param.Count - 1
                statid = param.Item(count)
                statnm = param.Item(count + 1)
                count = count + 1
                If statid.Equals("ACTIVE") Or statid.Equals("OBSOLETE") Or statid.Equals("SUSPENDED") Or statid.Equals("TERMINATED") Then
                    ctrystat.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                End If
            Next

            Dim installEntity As New ClsInstalledBase()
            installEntity.ModifiedBy = Session("username")

            installEntity.logrank = Session("login_rank")
            installEntity.logctrid = Session("login_ctryID")
            installEntity.logcmpid = Session("login_cmpID")
            installEntity.logsvcid = Session("login_svcID")
            'set right 
            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "05")
            Me.LBadd.Enabled = purviewArray(0)
            'Me.LBsearch.Visible = purviewArray(2)
            'editcol.Visible = purviewArray(1)
            'Me.installbaseView.Visible = purviewArray(2)

            installEntity.Status = "ACTIVE"

            'Dim ctryall As DataSet = installEntity.Getinstallbase()
            'If ctryall.Tables(0).Rows.Count = 0 Then
            '    Label1.Visible = True
            'Else
            Label1.Visible = False
            'End If


            'If ctryall.Tables(0).Rows.Count <> 0 Then
            'If True = False Then
            '    installbaseView.DataSource = ctryall
            '    'Session("installbaseView") = ctryall
            '    installbaseView.AllowPaging = True
            '    installbaseView.AllowSorting = True
            '    Dim editcol As New CommandField
            '    editcol.EditText = objXmlTr.GetLabelName("EngLabelMsg", "BB-Edit") '"edit"
            '    editcol.HeaderImageUrl = "~/PresentationLayer/graph/edit.gif"
            '    editcol.ShowEditButton = True
            '    installbaseView.Columns.Add(editcol)
            '    installbaseView.DataBind()
            '    'set right
            '    editcol.Visible = purviewArray(2)

            '    Call DisplayGridHeader()
            '    installbaseView.PageIndex = 0
            '    LblTotRecNo.Text = ctryall.Tables(1).Rows(0).Item(0)
            'Else
            LblTotRecNo.Text = 0

            installbaseView.AllowPaging = True
            installbaseView.AllowSorting = True
            Dim editcol As New CommandField
            editcol.EditText = objXmlTr.GetLabelName("EngLabelMsg", "BB-Edit") '"edit"
            editcol.HeaderImageUrl = "~/PresentationLayer/graph/edit.gif"
            editcol.ShowEditButton = True
            installbaseView.Columns.Add(editcol)

            editcol.Visible = purviewArray(2)

            'End If

            'added by deyb 21-Aug-1006
            DDArea.DataSource = installEntity.GetAreaByCountry(Session("Login_Ctryid"))
            DDArea.DataBind()
            DDArea.Items.Insert(0, "")
            '==================

        End If
    End Sub

    Sub DisplayGridHeader()
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")

        If installbaseView.Rows.Count <> 0 Then
            installbaseView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-INSTALLHD0")
            installbaseView.HeaderRow.Cells(1).Font.Size = 8
            installbaseView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-INSTALLHD1")
            installbaseView.HeaderRow.Cells(2).Font.Size = 8
            installbaseView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-INSTALLHD2")
            installbaseView.HeaderRow.Cells(3).Font.Size = 8
            installbaseView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-INSTALLHD6")
            installbaseView.HeaderRow.Cells(4).Font.Size = 8
            installbaseView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-INSTALLHD3")
            installbaseView.HeaderRow.Cells(5).Font.Size = 8
            installbaseView.HeaderRow.Cells(6).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-INSTALLHD4")
            installbaseView.HeaderRow.Cells(6).Font.Size = 8
            installbaseView.HeaderRow.Cells(7).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-INSTALLHD5")
            installbaseView.HeaderRow.Cells(7).Font.Size = 8
        End If
    End Sub

    Protected Sub LBsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LBsearch.Click
        
        Dim selDS As DataSet = GetSearchDs()
        installbaseView.DataSource = selDS
        installbaseView.DataBind()
        'Session("installbaseView") = selDS
        If selDS.Tables(0).Rows.Count = 0 Then
            Label1.Visible = True
        Else
            Label1.Visible = False
        End If
        Call DisplayGridHeader()

        LblTotRecNo.Text = selDS.Tables(1).Rows(0).Item(0)

    End Sub

    Function GetSearchDs() As DataSet
        Dim installEntity As New ClsInstalledBase()
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")

        If (Trim(modelid.Text) <> "") Then
            installEntity.ModelID = modelid.Text
        End If
        If (Trim(serialnumber.Text) <> "") Then
            installEntity.SerialNo = serialnumber.Text
        End If
        If (ctrystat.SelectedItem.Value.ToString() <> "") Then
            installEntity.Status = ctrystat.SelectedItem.Value.ToString()
        End If

        If (Trim(custid.Text) <> "") Then
            installEntity.CustomerID = custid.Text
        End If
        If (Trim(custname.Text) <> "") Then
            installEntity.custname = custname.Text
        End If

        If (Trim(teleno.Text) <> "") Then
            Dim telepass As New clsCommonClass()
            installEntity.custtelpho = telepass.telconvertpass(Trim(teleno.Text))
        End If

        If DDArea.SelectedValue <> "" Then
            installEntity.areaid = DDArea.SelectedValue
        End If

        If Trim(modelid.Text) = "" And Trim(serialnumber.Text) = "" And Trim(custid.Text) = "" And Trim(custname.Text) = "" And DDArea.SelectedValue = "" And Trim(teleno.Text) = "" Then
            addinfo.Visible = True
        Else
            addinfo.Visible = False
        End If

        installEntity.ModifiedBy = Session("username")
        installEntity.logrank = Session("login_rank")
        installEntity.logctrid = Session("login_ctryID")
        installEntity.logcmpid = Session("login_cmpID")
        installEntity.logsvcid = Session("login_svcID")

        GetSearchDs = installEntity.Getinstallbase()
    End Function

    Protected Sub installbaseView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles installbaseView.PageIndexChanging
        installbaseView.PageIndex = e.NewPageIndex
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        Dim installEntity As New ClsInstalledBase()
        If (Trim(modelid.Text) <> "") Then
            installEntity.ModelID = modelid.Text
        End If
        If (Trim(serialnumber.Text) <> "") Then
            installEntity.SerialNo = serialnumber.Text
        End If
        If (ctrystat.SelectedItem.Value.ToString() <> "") Then
            installEntity.Status = ctrystat.SelectedItem.Value.ToString()
        End If

        If (Trim(custid.Text) <> "") Then
            installEntity.CustomerID = custid.Text
        End If
        If (Trim(custname.Text) <> "") Then
            installEntity.custname = custname.Text
        End If
        If (Trim(teleno.Text) <> "") Then
            Dim telepass As New clsCommonClass()
            installEntity.custtelpho = telepass.telconvertpass(Trim(teleno.Text))
        End If

        If DDArea.SelectedValue <> "" Then
            installEntity.areaid = DDArea.SelectedValue
        End If

        installEntity.ModifiedBy = Session("username")
        installEntity.logrank = Session("login_rank")
        installEntity.logctrid = Session("login_ctryID")
        installEntity.logcmpid = Session("login_cmpID")
        installEntity.logsvcid = Session("login_svcID")
        Dim selDS As DataSet = installEntity.Getinstallbase()

        'Dim selDS As DataSet = GetSearchDs()
        installbaseView.DataSource = selDS
        installbaseView.DataBind()
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        Call DisplayGridHeader()
    End Sub

    Protected Sub installbaseView_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles installbaseView.RowEditing
        'Dim ds As DataSet = Session("installbaseView")
        Dim page As Integer = installbaseView.PageIndex * installbaseView.PageSize
        'Dim rouid As String = ds.Tables(0).Rows(page + e.NewEditIndex).Item(0).ToString()
        Dim rouid As String = Me.installbaseView.Rows(e.NewEditIndex).Cells(1).Text
        rouid = Server.UrlEncode(rouid)
        Dim strTempURL As String = "rouid=" + rouid
        strTempURL = "~/PresentationLayer/masterrecord/ModifyinstallBase.aspx?" + strTempURL
        Response.Redirect(strTempURL)

    End Sub
End Class