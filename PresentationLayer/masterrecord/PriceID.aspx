<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PriceID.aspx.vb" Inherits="PresentationLayer_masterrecord_PriceID" %>

<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Price ID Search</title>
    
     <style type="text/css">A:link { COLOR: #336666; TEXT-DECORATION: none }
	BODY { FONT-SIZE: 9pt; FONT-FAMILY: "Arial", "Verdana", "Tahoma"; BACKGROUND-COLOR: #ffffff }
	TD { FONT-SIZE: 9pt; FONT-FAMILY: Arial,Verdana,Tahoma }
	</style>
	<link href="../css/style.css" type="text/css" rel="stylesheet">
</head>
<body id="PriceID">
    <form id="form1" runat="server">    
    <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>
							<TD width="1%" background="../graph/title_bg.gif"><IMG height="24" src="../graph/title1.gif" width="5"></TD>
							<TD class="style2" width="98%" background="../graph/title_bg.gif">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></TD>
							<TD align="right" width="1%" background="../graph/title_bg.gif"><IMG height="24" src="../graph/title_2.gif" width="5"></TD>
						</TR>
	  </TABLE>
	   
	   <tr>
           <td>
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<tr>
                <td background="../graph/title_bg.gif" style="width: 1%">
                </td>
                <td background="../graph/title_bg.gif" class="style2" width="98%">
                    <asp:Label ID="addinfo" runat="server" ForeColor="Red" Text="Label" Visible="False"></asp:Label></td>
                <td align="right" background="../graph/title_bg.gif" width="1%">
                </td>
            </tr>
			 </TABLE>
           </tr>
           <TR>
        
        <asp:Label ID="PriceIDlab" runat="server" Text="label" Width="59px"></asp:Label>
        <asp:TextBox ID="PriceIDBox" runat="server" Width="94px" CssClass="textborder" MaxLength="10"></asp:TextBox>
        <asp:Label ID="PriceNamelab" runat="server" Text="label" Width="71px"></asp:Label>
        <asp:TextBox ID="PriceNameBox" runat="server" Width="130px" CssClass="textborder" Rows="50"></asp:TextBox>
        <asp:Label ID="Statuslab" runat="server" Text="label" Width="47px"></asp:Label>
        <asp:DropDownList ID="StatusDrop" runat="server" Width="15%" CssClass="textborder">
        </asp:DropDownList>
        <asp:LinkButton ID="LinkButton2" runat="server">Seach</asp:LinkButton>
        <asp:LinkButton ID="LinkButton1" runat="server" PostBackUrl="~/PresentationLayer/masterrecord/addPriceID.aspx">add</asp:LinkButton><br />
        <hr />  
        <asp:GridView ID="PriceIDView" runat="server" Width="100%" AllowPaging="True" Font-Size="Smaller">
        </asp:GridView>
        <cc1:MessageBox ID="MessageBox1" runat="server" />
        <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="Label"></asp:Label>
    </form>
</body>
</html>
