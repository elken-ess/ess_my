<%@ Page Language="VB" AutoEventWireup="false" EnableEventValidation="false"   CodeFile="addPriceID.aspx.vb" Inherits="PresentationLayer_masterrecord_addPriceID" %>

<%@ Register Assembly="JCalendar" Namespace="JCalendar" TagPrefix="cc2" %>

<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Price ID - Add</title>
     <style type="text/css">A:link { COLOR: #336666; TEXT-DECORATION: none }
	BODY { FONT-SIZE: 9pt; FONT-FAMILY: "Arial", "Verdana", "Tahoma"; BACKGROUND-COLOR: #ffffff }
	TD { FONT-SIZE: 9pt; FONT-FAMILY: Arial,Verdana,Tahoma }
	</style>
	<link href="../css/style.css" type="text/css" rel="stylesheet"></link>
    <script language="JavaScript" src="../js/common.js"></script> 
</head>
<body  id="PriceIDbody">
   <form id="PriceIDform"  method="post" action=""  runat="server">
     <TABLE border="0" id=priceidtab style="width: 100%">
           <tr>
           <td >
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>
							<TD background="../graph/title_bg.gif" style="height: 24px; width: 1%;"><IMG height="24" src="../graph/title1.gif" width="5"></TD>
							<TD class="style2" width="98%" background="../graph/title_bg.gif" style="height: 24px">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></TD>
							<TD align="right" background="../graph/title_bg.gif" style="height: 24px; width: 1%;"><IMG height="24" src="../graph/title_2.gif" width="5"></TD>
						</TR>
									 </TABLE>
           </tr>
           <tr>
           <td >
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>					
							<TD class="style2" width="98%" background="../graph/title_bg.gif" style="height: 20px">
                                <font color=red><asp:Label ID="errlab" runat="server" Text="Label" Visible=false></asp:Label></font>
                                </TD>
						</TR>
			 </TABLE>
           </tr>
           <TR>
				<TD >
     <TABLE border="0" id=PriceIDtab style="width: 100%">
			<TR>
				<TD style="width: 100%" >
					<TABLE id="PriceID" cellSpacing="1" cellPadding="2" bgColor="#b7e6e6" border="0" style="width: 100%;" >
					  <tr bgcolor="#ffffff">
<td colspan = 4>
                            <asp:Label ID="lblCompTop" runat="server" ForeColor="Red" Text="Label" Visible="True"></asp:Label>
</td>
             </tr>
             <tr bgcolor="#ffffff">
<td colspan = 4>
                        	&nbsp;
                     	</td>
               </tr> 
									  <TR bgColor="#ffffff">
											<TD  align=LEFT style="width: 20%;" >
                                                <asp:Label ID="PriceIDLab" runat="server" Text="Label" Height="19px" Width="100%"></asp:Label></TD>
											<TD align=left style="width: 30%;"  >
                                                <asp:TextBox ID="PriceIDbox" runat="server" CssClass="textborder" Width="84%" MaxLength="10"></asp:TextBox>
                                                <font color=red><strong>*</strong></font>
                                                <asp:RequiredFieldValidator ID="PriceIDVA" runat="server" ControlToValidate="PriceIDbox"
                                                    ForeColor="White">*</asp:RequiredFieldValidator></TD>
											<TD  align=LEFT style="width: 20%;"  >
                                                <asp:Label ID="PriceNameLab" runat="server" Text="Label" Width="100%"></asp:Label></TD>
											<TD  align=left style="width: 39%;"  >
                                                <asp:TextBox ID="PriceNamebox" runat="server" CssClass="textborder" Width="85%" MaxLength="50"></asp:TextBox>
                                                <font color=red><strong>*</strong><asp:RequiredFieldValidator ID="Pricenameva" runat="server" ControlToValidate="PriceNamebox"
                                                    ForeColor="White">*</asp:RequiredFieldValidator></font>
                                                </TD>
										</TR>
										<TR bgColor="#ffffff">
											<TD  align=LEFT style="width: 20%;"  >
                                                <asp:Label ID="ALterNamelab" runat="server" Text="Label" Width="100%"></asp:Label></TD>
											<TD align=left width="85%" colspan=3 style="width: 80%;" >
                                                <asp:TextBox ID="ALterNamebox" runat="server" Width="96%" CssClass="textborder" MaxLength="50"></asp:TextBox></TD>
										</TR>
										<TR bgColor="#ffffff">
											<TD  align=LEFT style="width: 20%; height: 38px;" >
                                                <asp:Label ID="CountryIDlab" runat="server" Text="Label" Width="100%"></asp:Label></TD>
											<TD align=left style="width: 30%; height: 38px;" >
                                                <asp:DropDownList ID="CountryIDDrop" runat="server" Width="84%" CssClass="textborder" AutoPostBack="True">
                                                </asp:DropDownList>
                                                <font color=red style="width: 30%"><strong>*</strong></font>
                                                <asp:RequiredFieldValidator ID="CountryIDVa" runat="server" ControlToValidate="CountryIDDrop"
                                                    ForeColor="White">*</asp:RequiredFieldValidator></TD>
											<TD align=LEFT style="width: 20%; height: 38px;">
                                                <asp:Label ID="CurrencyNamelab" runat="server" Text="Label" Width="100%" ></asp:Label></TD>
											<TD align=left style="width: 39%; height: 38px;"  >
                                                <asp:TextBox ID="CurrencyNamebox" runat="server" Enabled="False" CssClass="textborder" Width="90%"></asp:TextBox></TD>
										</TR>
										<TR bgColor="#ffffff">
											<TD align=LEFT style="width: 20%" >
                                                <asp:Label ID="EffectiveDatelab" runat="server" Text="Label" Width="100%"></asp:Label></TD>
											<TD align=left style="width: 30%"   >
                                                <asp:TextBox ID="Efftext" runat="server" Width="72%" CssClass="textborder" ReadOnly="True"></asp:TextBox><font color=red><strong>*</strong><asp:HyperLink
                                                    ID="HypCal" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                                                    ToolTip="Choose a Date">Choose a Date</asp:HyperLink><cc2:JCalendar ID="Calendareffe" runat="server" ControlToAssign="Efftext" ImgURL="~/PresentationLayer/graph/calendar.gif" Visible="False" />
                                                
                                                <asp:RequiredFieldValidator ID="EFFDATVA" runat="server" ControlToValidate="Efftext"
                                                    ForeColor="White">*</asp:RequiredFieldValidator></font>
                                                
                                            </TD>
											<TD align=LEFT style="width: 20%" >
                                                <asp:Label ID="ObsoleteDatelab" runat="server" Text="Label" Width="100%"></asp:Label></TD>
											<TD align=left style="width: 39%;" >
                                                <asp:TextBox ID="Obsotext" runat="server" Width="62%" CssClass="textborder" ReadOnly="True"></asp:TextBox>&nbsp;
                                                <font color=red><strong>*<asp:HyperLink ID="HypCal2" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                                                    ToolTip="Choose a Date">Choose a Date</asp:HyperLink></strong></font><cc2:JCalendar ID="CalendarObs" runat="server" ImgURL="~/PresentationLayer/graph/calendar.gif" ControlToAssign="Obsotext" Visible="False" />
                                                <asp:RequiredFieldValidator ID="OBSLVA" runat="server" ControlToValidate="Obsotext"
                                                    ForeColor="White">*</asp:RequiredFieldValidator>
                                                
                                            </TD>
										</TR>
										<TR bgColor="#ffffff">
											<TD  align=LEFT style="width: 20% ;" >
                                                <asp:Label ID="Statuslab" runat="server" Text="Label" ></asp:Label></TD>
											<TD align=left width="85%" colspan=3 style="width: 80%;">
                                                <asp:DropDownList ID="StatusDrop" runat="server" Width="37%" CssClass="textborder">
                                                </asp:DropDownList></TD>
										</TR>
										<TR bgColor="#ffffff">
											<TD  align=LEFT style="width: 20%" >
                                                <asp:Label ID="CreatedBylab" runat="server" Text="Label" Width="100%"></asp:Label></TD>
											<TD align=left style="width: 30%"   >
                                                <asp:TextBox ID="CreatedBy" runat="server" CssClass="textborder" ReadOnly="True" Width="84%"></asp:TextBox></TD>
											<TD  align=LEFT style="width: 20%"  >
                                                <asp:Label ID="CreatedDatelab" runat="server" Text="Label" Width="100%"></asp:Label></TD>
											<TD  align=left style=" width: 39%;" >
                                                <asp:TextBox ID="CreatedDate" runat="server" Width="90%" CssClass="textborder" ReadOnly="True"></asp:TextBox>
                                               
                                            </TD>
										</TR>
										<TR bgColor="#ffffff">
											<TD  align=LEFT style="width: 20%" >
                                                <asp:Label ID="ModifiedBylab" runat="server" Text="Label" Width="100%"></asp:Label></TD>
											<TD align=left style="width: 30%"  >
                                                <asp:TextBox ID="ModifiedBy" runat="server" CssClass="textborder" ReadOnly="True" Width="84%"></asp:TextBox></TD>
											<TD  align=LEFT style="width: 20%"  >
                                                <asp:Label ID="ModifiedDatelab" runat="server" Text="Label" Width="100%"></asp:Label></TD>
											<TD  align=left style="width: 39%;" >
                                                <asp:TextBox ID="ModifiedDate" runat="server" Width="90%" CssClass="textborder" ReadOnly="True"></asp:TextBox>
                                               
                                            </TD>
										</TR>
										
										
								</TABLE>
						</TD>
					</TR>
					<TR>
						<TD style="width: 418px; height: 15px;" >
						<font color=red><asp:Label ID="lblCompBottom" runat="server" Text="Label" Visible="true"></asp:Label></font>
                            <br /><asp:LinkButton ID="saveButton" runat="server"></asp:LinkButton>
                            &nbsp; &nbsp; &nbsp; 
                                                      
                            <asp:HyperLink ID="cancelLink" runat="server" NavigateUrl="~/PresentationLayer/masterrecord/PriceID.aspx">[cancelLink]</asp:HyperLink>
                            
                            &nbsp; &nbsp; &nbsp; <asp:HyperLink ID="AddLink" runat="server" >[Add]</asp:HyperLink>
                            </TD>
                            
					</TR>
			</TABLE>
			         </TD>
							</TR>
							</TABLE>
                            <asp:ValidationSummary ID="ErrorMessage" runat="server" ShowMessageBox="True" ShowSummary="False" />
                    <cc1:MessageBox ID="MessageBox1" runat="server" />

    </form>
</body>
</html>
