Imports System.Data.SqlClient
Imports BusinessEntity
Imports System.Configuration
Imports SQLDataAccess
Imports System.Data




Partial Class PresentationLayer_masterrecord_modifyPriceID


    Inherits System.Web.UI.Page
    Shared fintdata As Integer  'data variable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try
        Try
            Dim str As String = Request.Params("PriceID").ToString
            str = Request.Params("CountryID").ToString()
        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try

        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        If Not Page.IsPostBack Then
            Dim mdfyid As String = Request.Params("PriceID").ToString()
            Dim conid As String = Request.Params("CountryID").ToString()
            'Dim statusid As String = Request.Params("statusid").ToString()
           

            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            PriceIDLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRID-0001")
            PriceNameLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRID-0002")
            ALterNamelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRID-0003")
            CountryIDlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRID-0004")
            CurrencyNamelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRID-0005")
            EffectiveDatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRID-0006")
            ObsoleteDatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRID-0007")
            CreatedBylab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATBY")
            CreatedDatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
            ModifiedBylab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFBY")
            ModifiedDatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFDATE")
            Statuslab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Status")
            saveButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Save")
            cancelLink.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Cancel")
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRID-0009")
            LinkButton1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRID-0011")
            lblNoteUP.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            lblNoteDown.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")

            Dim objXmlTr1 As New clsXml
            objXmlTr1.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            EFFDATVA.ErrorMessage = objXmlTr1.GetLabelName("StatusMessage", "BB-HintMsg-Effecdate")
            OBSLVA.ErrorMessage = objXmlTr1.GetLabelName("StatusMessage", "BB-HintMsg-Obsedate")
            'COMOBSL.ErrorMessage = objXmlTr1.GetLabelName("StatusMessage", "BB-HintMsg-COMPAREEFFOBDATE")
            'CountryIDVa.ErrorMessage = objXmlTr1.GetLabelName("StatusMessage", "CountryID")

            '''access control'''''''''''''''''''''''''''''''''''''''''''''''
            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "17")
            Me.savebutton.Enabled = purviewArray(1) 'Edit
            If purviewArray(2) = False Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return

            End If




            'Dim strPanm As String = objXmlTr1.GetLabelID("StatusMessage", statusid)
            Dim CounID As New clsCommonClass()
            Dim connm As String = CounID.getsubstring(conid)
            If mdfyid.Trim() <> "" Then
                Dim PriceIDEntity As New clsPriceID()
                PriceIDEntity.PriceID = mdfyid
                'PriceIDEntity.PriceStatus = strPanm
                PriceIDEntity.CountryID = connm
                Dim retnArray As ArrayList = PriceIDEntity.GetPriceIDDetailsByID()
                PriceIDbox.Text = retnArray(0).ToString()
                PriceIDbox.ReadOnly = True
                PriceNamebox.Text = retnArray(1).ToString()
                ALterNamebox.Text = retnArray(2).ToString()
                'create the dropdownlist
                Dim priceidStat As New clsrlconfirminf()
                Dim statParam As ArrayList = New ArrayList
                statParam = priceidStat.searchconfirminf("STATUS")
                Dim count As Integer
                Dim statid As String
                Dim statnm As String
                For count = 0 To statParam.Count - 1
                    statid = statParam.Item(count)
                    statnm = statParam.Item(count + 1)
                    If (statid.Equals("ACTIVE") Or statid.Equals("DELETE") Or statid.Equals("OBSOLETE")) Then
                        StatusDrop.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                    End If
                    If statid.Equals(retnArray(3).ToString()) Then
                        StatusDrop.Items(count / 2).Selected = True
                    End If
                    count = count + 1
                Next
                '''access control'''''''''''''''''''''''''''''''''''''''''''''''

                If purviewArray(4) = False Then
                    Me.StatusDrop.Items.Remove(Me.StatusDrop.Items.FindByValue("DELETE"))

                End If



                Dim userIDNamestr As String = Session("userID").ToString().ToUpper + "-" + Session("username").ToString.ToUpper
                ModifiedBy.Text = userIDNamestr

                Dim Rcreatby As String = retnArray.Item(8).ToString().ToUpper()
                If Rcreatby <> "ADMIN" Then
                    Dim Rcreat As New clsCommonClass
                    Rcreat.userid() = Rcreatby
                    Dim Rcreatds As New DataSet()
                    Rcreatds = Rcreat.Getuseridname("BB_MASUSER_SelByIDName")

                    Me.CreatedBy.Text = Rcreatds.Tables(0).Rows(0).Item(0)
                Else
                    Me.CreatedBy.Text = "ADMIN-ADMIN"
                End If




                CurrencyNamebox.Text = retnArray(5).ToString()
                'CreatedBy.Text = retnArray(8).ToString()
                CreatedDate.Text = retnArray(9).ToString()
                CurrencyNamebox.ReadOnly = True

                'ModifiedBy.Text = retnArray(10).ToString()
                'ModifiedBy.Text = Session("username").ToString().ToUpper()
                CreatedBy.ReadOnly = True
                CreatedDate.ReadOnly = True
                ModifiedDate.ReadOnly = True
                ModifiedDate.Text = retnArray(11).ToString()
                Efftext.Text = retnArray(6).ToString()
                Obsotext.Text = retnArray(7).ToString()
                Countrybox.Text = retnArray(4).ToString()



            End If

        End If
        HypCal.NavigateUrl = "javascript:DoCal(document.form1.Efftext);"
        HypCal2.NavigateUrl = "javascript:DoCal(document.form1.Obsotext);"

    End Sub
#Region " id bond"
    Public Function databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        Dim row As DataRow
        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            NewItem.Text = row("id") & "-" & row("name")
            NewItem.Value = row("id")
            dropdown.Items.Add(NewItem)
        Next

    End Function
#End Region
    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveButton.Click
        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim temparr As Array = Request.Form("Efftext").Split("/") 'Efftext.Text.Split("/")
        Dim streff As String
        Dim strobs As String
        streff = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
        temparr = Request.Form("Obsotext").Split("/") 'Obsotext.Text.Split("/")
        strobs = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
        If Convert.ToDateTime(strobs) < Convert.ToDateTime(streff) Then
            errlab.Text = objXm.GetLabelName("StatusMessage", "BB-HintMsg-COMPAREEFFOBDATE")
            errlab.Visible = True
            Return
        End If
        Dim msginfo As String = objXm.GetLabelName("StatusMessage", "SAVEORNOT")
        Dim msgtitle As String = objXm.GetLabelName("StatusMessage", "SAVETITLE")
        Efftext.Text = Request.Form("Efftext")
        Obsotext.Text = Request.Form("Obsotext")
        MessageBox1.Confirm(msginfo)
        Dim dsPriceIDall As DataSet = Session("PriceIDView")
        GridView1.DataSource = dsPriceIDall
        GridView1.DataBind()
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        If dsPriceIDall.Tables(0).Rows.Count <> 0 Then
            GridView1.HeaderRow.Cells(0).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0002")
            GridView1.HeaderRow.Cells(0).Font.Size = 8
            GridView1.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRID-0012")
            GridView1.HeaderRow.Cells(1).Font.Size = 8
            GridView1.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0003")
            GridView1.HeaderRow.Cells(2).Font.Size = 8
            GridView1.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0014")
            GridView1.HeaderRow.Cells(3).Font.Size = 8
            GridView1.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0015")
            GridView1.HeaderRow.Cells(4).Font.Size = 8
            GridView1.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0016")
            GridView1.HeaderRow.Cells(5).Font.Size = 8
        End If
        
    End Sub
   
    Protected Sub MessageBox1_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox1.GetMessageBoxResponse
        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
            Dim PriceIDEntity As New clsPriceID()

            If (Trim(PriceIDbox.Text) <> "") Then
                PriceIDEntity.PriceID = PriceIDbox.Text.ToUpper()
            End If
            If (Trim(Countrybox.Text) <> "") Then
                PriceIDEntity.CountryID = Countrybox.Text.ToUpper()
            End If
            If (Trim(PriceNamebox.Text) <> "") Then
                PriceIDEntity.PriceName = PriceNamebox.Text.ToUpper()
            End If
            If (Trim(ALterNamebox.Text) <> "") Then
                PriceIDEntity.PriceAlternateName = ALterNamebox.Text.ToUpper()
            End If
            If (StatusDrop.SelectedItem.Value.ToString() <> "") Then
                PriceIDEntity.PriceStatus = StatusDrop.SelectedItem.Value.ToString()
            End If


            If Me.StatusDrop.SelectedValue = "DELETE" Then
                If check() = 1 Then
                    Dim objXm As New clsXml
                    objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    errlab.Text = objXm.GetLabelName("StatusMessage", "CHECKPRICEID")
                    errlab.Visible = True
                    Return
                End If

            End If


            If (Trim(CurrencyNamebox.Text) <> "") Then
                PriceIDEntity.CurrencyName = CurrencyNamebox.Text.ToUpper()
            End If
            Dim retnArray As ArrayList = PriceIDEntity.GetPriceIDDetailsByID()
            ' elect Calendar
            Dim prcidDate As New clsCommonClass()
           

            If (Trim(CreatedBy.Text) <> "") Then
                PriceIDEntity.CreatedBy = CreatedBy.Text.ToUpper()
            End If
            If (Trim(Request.Form("Obsotext")) <> "") Then
                Dim temparr As Array = Request.Form("Obsotext").Split("/") 'Obsotext.Text.Split("/")
                PriceIDEntity.ObsoleteDate = temparr(2) + "-" + temparr(1) + "-" + temparr(0)

            End If
            If (Trim(Request.Form("Efftext")) <> "") Then
                Dim temparr As Array = Request.Form("Efftext").Split("/") 'Efftext.Text.Split("/")
                PriceIDEntity.EffectiveDate = temparr(2) + "-" + temparr(1) + "-" + temparr(0)

            End If
            If (Trim(CreatedDate.Text) <> "") Then
                PriceIDEntity.CreatedDate = prcidDate.DatetoDatabase(CreatedDate.Text)
            End If
            'If (Trim(ModifiedBy.Text) <> "") Then
            '    PriceIDEntity.ModifiedBy = ModifiedBy.Text.ToUpper()
            'End If
            If (Trim(Me.ModifiedBy.Text) <> "") Then
                PriceIDEntity.ModifiedBy = Session("userID").ToString().ToUpper
            End If
            If (Trim(ModifiedDate.Text) <> "") Then
                PriceIDEntity.ModifiedDate = prcidDate.DatetoDatabase(ModifiedDate.Text)
            End If
            PriceIDEntity.svcid = Session("login_svcID").ToString().ToUpper
            PriceIDEntity.ip = Request.UserHostAddress.ToString()

           
           
            Dim insPRIDnCnt As Integer = PriceIDEntity.GetPriceIDDetailsByPriceNM()
            If insPRIDnCnt <> 0 Then
                Dim objXm As New clsXml
                objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                errlab.Text = objXm.GetLabelName("StatusMessage", "DUPPRICENAME")
                errlab.Visible = True
            ElseIf insPRIDnCnt.Equals(0) Then
                Dim updPriceIDCnt As Integer = PriceIDEntity.Update()
                Dim objXmlT As New clsXml
                objXmlT.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                If updPriceIDCnt = 0 Then
                    Dim objXm As New clsXml
                    objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    errlab.Text = objXm.GetLabelName("StatusMessage", "SuccessfullySaved")
                    errlab.Visible = True
                Else
                    Response.Redirect("~/PresentationLayer/Error.aspx")
                End If
            End If
            Dim dsPriceIDall As DataSet = Session("PriceIDView")
            GridView1.DataSource = dsPriceIDall
            GridView1.DataBind()
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            If dsPriceIDall.Tables(0).Rows.Count <> 0 Then
                GridView1.HeaderRow.Cells(0).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0002")
                GridView1.HeaderRow.Cells(0).Font.Size = 8
                GridView1.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRID-0012")
                GridView1.HeaderRow.Cells(1).Font.Size = 8
                GridView1.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0003")
                GridView1.HeaderRow.Cells(2).Font.Size = 8
                GridView1.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0014")
                GridView1.HeaderRow.Cells(3).Font.Size = 8
                GridView1.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0015")
                GridView1.HeaderRow.Cells(4).Font.Size = 8
                GridView1.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0016")
                GridView1.HeaderRow.Cells(5).Font.Size = 8
            End If
            Efftext.Text = Request.Form("Efftext")
            Obsotext.Text = Request.Form("Obsotext")
        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
            Efftext.Text = Request.Form("Efftext")
            Obsotext.Text = Request.Form("Obsotext")
        End If
    End Sub
#Region "check delete"
    Function check() As Integer

        Dim PriceIDEntity As New clsPriceID()
        If (Trim(Me.PriceIDbox.Text) <> "") Then
            PriceIDEntity.PriceID = Me.PriceIDbox.Text
        End If
        Dim sqldr As SqlDataReader = PriceIDEntity.GetPriceSetUp()
        'if there are identical record then tishi error
        Dim i As Integer = 0
        While (sqldr.Read())
            i = 1
        End While
        Return i
    End Function
#End Region

    Protected Sub LinkButton1_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton1.Click
        GridView1.Visible = True
        Dim PriceIDEntity As New clsPriceID()

        Dim rank As String = Session("login_rank")
        If rank <> 0 Then
            PriceIDEntity.CountryID = Session("login_ctryID").ToString().ToUpper
            
        End If
        PriceIDEntity.rank = rank
        PriceIDEntity.PriceID = PriceIDbox.Text.ToUpper()
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        Dim dsPriceIDall As DataSet = PriceIDEntity.GetProductPriceListing()
        GridView1.DataSource = dsPriceIDall
        Session("PriceIDView") = dsPriceIDall
        GridView1.AllowPaging = True
        'GridView1.AllowSorting = True
        GridView1.DataBind()
        If dsPriceIDall.Tables(0).Rows.Count <> 0 Then
            GridView1.HeaderRow.Cells(0).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0002")
            GridView1.HeaderRow.Cells(0).Font.Size = 8
            GridView1.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRID-0012")
            GridView1.HeaderRow.Cells(1).Font.Size = 8
            GridView1.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0003")
            GridView1.HeaderRow.Cells(2).Font.Size = 8
            GridView1.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0014")
            GridView1.HeaderRow.Cells(3).Font.Size = 8
            GridView1.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0015") & " %"
            GridView1.HeaderRow.Cells(4).Font.Size = 8
            GridView1.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0016")
            GridView1.HeaderRow.Cells(5).Font.Size = 8
        End If
        Efftext.Text = Request.Form("Efftext")
        Obsotext.Text = Request.Form("Obsotext")
    End Sub

    Protected Sub GridView1_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        GridView1.PageIndex = e.NewPageIndex
        'Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        'System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Dim dsPriceIDall As DataSet = Session("PriceIDView")
        GridView1.DataSource = dsPriceIDall
        GridView1.DataBind()
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        If dsPriceIDall.Tables(0).Rows.Count <> 0 Then
            GridView1.HeaderRow.Cells(0).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0002")
            GridView1.HeaderRow.Cells(0).Font.Size = 8
            GridView1.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRID-0012")
            GridView1.HeaderRow.Cells(1).Font.Size = 8
            GridView1.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0003")
            GridView1.HeaderRow.Cells(2).Font.Size = 8
            GridView1.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0014")
            GridView1.HeaderRow.Cells(3).Font.Size = 8
            GridView1.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0015")
            GridView1.HeaderRow.Cells(4).Font.Size = 8
            GridView1.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0016")
            GridView1.HeaderRow.Cells(5).Font.Size = 8
        End If
        Efftext.Text = Request.Form("Efftext")
        Obsotext.Text = Request.Form("Obsotext")
    End Sub

    

    Protected Sub CalendarObs_SelectedDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CalendarObs.SelectedDateChanged
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Dim prcidDate As New clsCommonClass()
        Obsotext.Text = CalendarObs.SelectedDate.Date.ToString().Substring(0, 10)
        Dim dsPriceIDall As DataSet = Session("PriceIDView")
        GridView1.DataSource = dsPriceIDall
        GridView1.DataBind()
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        If dsPriceIDall.Tables(0).Rows.Count <> 0 Then
            GridView1.HeaderRow.Cells(0).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0002")
            GridView1.HeaderRow.Cells(0).Font.Size = 8
            GridView1.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRID-0012")
            GridView1.HeaderRow.Cells(1).Font.Size = 8
            GridView1.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0003")
            GridView1.HeaderRow.Cells(2).Font.Size = 8
            GridView1.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0014")
            GridView1.HeaderRow.Cells(3).Font.Size = 8
            GridView1.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0015")
            GridView1.HeaderRow.Cells(4).Font.Size = 8
            GridView1.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0016")
            GridView1.HeaderRow.Cells(5).Font.Size = 8
        End If
    End Sub

    Protected Sub CalendarEffe_SelectedDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CalendarEffe.SelectedDateChanged
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Dim prcidDate As New clsCommonClass()
        Efftext.Text = CalendarEffe.SelectedDate.Date.ToString().Substring(0, 10)
        Dim dsPriceIDall As DataSet = Session("PriceIDView")
        GridView1.DataSource = dsPriceIDall
        GridView1.DataBind()
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        If dsPriceIDall.Tables(0).Rows.Count <> 0 Then
            GridView1.HeaderRow.Cells(0).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0002")
            GridView1.HeaderRow.Cells(0).Font.Size = 8
            GridView1.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRID-0012")
            GridView1.HeaderRow.Cells(1).Font.Size = 8
            GridView1.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0003")
            GridView1.HeaderRow.Cells(2).Font.Size = 8
            GridView1.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0014")
            GridView1.HeaderRow.Cells(3).Font.Size = 8
            GridView1.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0015")
            GridView1.HeaderRow.Cells(4).Font.Size = 8
            GridView1.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0016")
            GridView1.HeaderRow.Cells(5).Font.Size = 8
        End If
    End Sub

    Protected Sub CalendarEffe_CalendarVisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CalendarEffe.CalendarVisibleChanged
        StatusDrop.Visible = Not CalendarEffe.CalendarVisible
    End Sub

    Protected Sub GridView1_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles GridView1.Sorting

    End Sub
End Class
