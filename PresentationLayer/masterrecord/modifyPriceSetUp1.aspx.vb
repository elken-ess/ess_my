Imports System.Data.SqlClient
Imports BusinessEntity
Imports System.Configuration
Imports SQLDataAccess
Imports System.Data




Partial Class PresentationLayer_masterrecord_modifyPriceSetUp1


    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try

        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle



        If Not Page.IsPostBack Then

            Dim mdfyid As String = Request.Params("PriceSetUpID").ToString()
            Dim time As String = Request.Params("time").ToString()

            Me.priceidBox.Text = Request.Params("price").ToString().ToUpper()
            Me.partidBox.Text = Request.Params("part").ToString().ToUpper()
            Me.countryidBox.Text = Request.Params("country").ToString().ToUpper()




            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "14")
            Me.saveButton.Enabled = purviewArray(1)
            If purviewArray(2) = False Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If


            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            amountlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0005")
            effectivelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRID-0006")
            createby.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATBY")
            creatdate.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
            modifyby.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFBY")
            mdifydate.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFDATE")
            statusLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Status")
            saveButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Save")
            cancelLink.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Cancel")
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0012")
            priceidlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0001")
            partidlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0002")
            countryidLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0003")
            lblNoteUP.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            lblNoteDown.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")

            Dim objXmlTr1 As New clsXml
            objXmlTr1.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            AMOUNTVA.ErrorMessage = objXmlTr1.GetLabelName("StatusMessage", "AMOUNTERROR")
            'EFFDATVA.ErrorMessage = objXmlTr1.GetLabelName("StatusMessage", "BB-HintMsg-Effecdate")
            va.ErrorMessage = objXmlTr1.GetLabelName("StatusMessage", "AMOUNT")
            'COUNTRYIDVA.ErrorMessage = objXmlTr1.GetLabelName("StatusMessage", "CountryID")
            'partidVA.ErrorMessage = objXmlTr1.GetLabelName("StatusMessage", "PARTID")



            Dim PriceSetUpEntity As New ClsPriceSetup1()
            PriceSetUpEntity.PriceSetUpID = Convert.ToUInt32(mdfyid)

            PriceSetUpEntity.crdate = CType(time, System.DateTime)


            Dim retnArray As ArrayList = PriceSetUpEntity.GetPriceSetUpIDDetailsByID1()
            pricesetupidbox.Text = retnArray(0)
            amountbox.Text = retnArray(1)

            Dim cntryStat As New clsrlconfirminf()
            Dim statParam As ArrayList = New ArrayList
            statParam = cntryStat.searchconfirminf("STATUS")
            Dim count As Integer
            Dim statid As String
            Dim statnm As String
            For count = 0 To statParam.Count - 1
                statid = statParam.Item(count)
                statnm = statParam.Item(count + 1)
                If (statid.Equals("ACTIVE") Or statid.Equals("DELETE") Or statid.Equals("OBSOLETE")) Then
                    statusDrop.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                End If
                If statid.Equals(retnArray(3).ToString()) Then
                    statusDrop.Items(count / 2).Selected = True
                End If
                count = count + 1
            Next
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If purviewArray(4) = False Then
                Me.statusDrop.Items.Remove(Me.statusDrop.Items.FindByValue("DELETE"))
            End If
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''  
            Dim userIDNamestr As String = Session("userID").ToString().ToUpper + "-" + Session("username").ToString.ToUpper
            modfybybox.Text = userIDNamestr

            Dim Rcreatby As String = retnArray.Item(4).ToString().ToUpper()
            If Rcreatby <> "ADMIN" Then
                Dim Rcreat As New clsCommonClass
                Rcreat.userid() = Rcreatby
                Dim Rcreatds As New DataSet()
                Rcreatds = Rcreat.Getuseridname("BB_MASUSER_SelByIDName")

                Me.creatbybox.Text = Rcreatds.Tables(0).Rows(0).Item(0)
            Else
                Me.creatbybox.Text = "ADMIN-ADMIN"
            End If
            'creatbybox.Text = retnArray(4).ToString()
            creatbybox.ReadOnly = True
            'modfybybox.Text = Session("username").ToString().ToUpper()
            modfybybox.ReadOnly = True
            creatdtbox.Text = retnArray(5).ToString()
            creatdtbox.ReadOnly = True
            mdfydtbox.Text = retnArray(7).ToString()
            mdfydtbox.Text = retnArray(7).ToString()
            effectivebox.Text = retnArray(2).ToString()
            'txtOri.Text = retnArray(8).ToString()
            'txtGstAmt.Text = retnArray(9).ToString()
            'txtGstExc.Text = retnArray(10).ToString()





        End If



    End Sub
#Region " id bond"
    Public Function databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        Dim row As DataRow
        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            NewItem.Text = row("id") & "-" & row("name")
            NewItem.Value = row("id")
            dropdown.Items.Add(NewItem)
        Next

    End Function
#End Region
    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveButton.Click
        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim msginfo As String = objXm.GetLabelName("StatusMessage", "SAVEORNOT")
        Dim msgtitle As String = objXm.GetLabelName("StatusMessage", "SAVETITLE")
        MessageBox1.Confirm(msginfo)

      
    End Sub


    'Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
    '    Calendar.Visible = True
    '    Calendar.Attributes.Add("style", " POSITION: absolute")
    'End Sub

    'Protected Sub Calendar_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendar.SelectionChanged
    '    Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
    '    System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
    '    Dim prcidDate As New clsCommonClass()
    '    effectivebox.Text = Calendar.SelectedDate.Date.ToString().Substring(0, 10)
    '    Calendar.Visible = False
    'End Sub

    Protected Sub cancelLink_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cancelLink.Click
        Dim PriceSetUpEntity As New clsPriceSetUp()
        Dim mdfyid As String = Request.Params("PriceSetUpID").ToString()
        Dim url As String = "~/PresentationLayer/masterrecord/modifyPriceSetUp.aspx?"
        url &= "pricesetupid1=" & Convert.ToUInt32(mdfyid) & "&"
        url &= "PriceSetUpID=" & "" & "&"
        url &= "time=" & ""

        Response.Redirect(url)
      
    End Sub

    Protected Sub MessageBox1_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox1.GetMessageBoxResponse
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
            Dim time As String = Request.Params("time")
            Dim PriceSetUp1Entity As New ClsPriceSetup1()
            Dim prcidDate As New clsCommonClass()

            If (Trim(amountbox.Text) <> "") Then
                PriceSetUp1Entity.Amount = amountbox.Text
            End If
            If (Trim(pricesetupidbox.Text) <> "") Then
                PriceSetUp1Entity.PriceSetUpID = pricesetupidbox.Text
            End If
            If (statusDrop.SelectedItem.Value.ToString() <> "") Then
                PriceSetUp1Entity.PriceSetUpStatus = statusDrop.SelectedItem.Value.ToString()
            End If
            If (Trim(creatbybox.Text) <> "") Then
                PriceSetUp1Entity.CreatedBy = creatbybox.Text.ToUpper()
            End If


            'If (Trim(modfybybox.Text) <> "") Then
            '    PriceSetUp1Entity.ModifiedBy = modfybybox.Text.ToUpper()
            'End If
            If (Trim(Me.modfybybox.Text) <> "") Then
                PriceSetUp1Entity.ModifiedBy = Session("userID").ToString().ToUpper
            End If
            If (Trim(effectivebox.Text) <> "") Then
                Dim temparr As Array = effectivebox.Text.Split("/")
                PriceSetUp1Entity.EffectiveDate = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
            End If
            'If (Trim(Me.txtOri.Text) <> "") Then
            '    PriceSetUp1Entity.AmountOri = txtOri.Text
            'End If
            'If (Trim(Me.txtGstAmt.Text) <> "") Then
            '    PriceSetUp1Entity.AmountGst = txtGstAmt.Text
            'End If
            'If (Trim(Me.txtGstExc.Text) <> "") Then
            '    PriceSetUp1Entity.AmountExc = txtGstExc.Text
            'End If


            PriceSetUp1Entity.crdate = CType(time, System.DateTime)
            PriceSetUp1Entity.svcid = Session("login_svcID").ToString().ToUpper
            PriceSetUp1Entity.ip = Request.UserHostAddress.ToString()

            'Dim insPRIDnCnt As Integer = PriceSetUp1Entity.GetDuplicatedIDAmount()
            'If insPRIDnCnt <> 0 Then
            '    'Response.Redirect("Error.aspx")
            '    Dim objXm As New clsXml
            '    objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            '    errlab.Text = objXm.GetLabelName("StatusMessage", "DUPAMOUNT")
            '    errlab.Visible = True
            'ElseIf insPRIDnCnt.Equals(0) Then
            Dim updPriceIDCnt As Integer = PriceSetUp1Entity.Update1()
            Dim objXmlT As New clsXml
            objXmlT.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            If updPriceIDCnt = 0 Then
                ' Response.Redirect("country.aspx")
                Dim objXm As New clsXml
                objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                errlab.Text = objXm.GetLabelName("StatusMessage", "SuccessfullySaved")
                errlab.Visible = True
            Else
                Response.Redirect("~/PresentationLayer/Error.aspx")
            End If

        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
        End If
    End Sub
End Class
