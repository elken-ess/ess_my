Imports BusinessEntity
Imports System.Configuration
Imports System.Data
Imports System.Data.SqlClient
Imports System.Xml
Partial Class PresentationLayer_masterrecord_modifyTechnician
    Inherits System.Web.UI.Page



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.MaintainScrollPositionOnPostBack = True
        If Not Page.IsPostBack Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try

            Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "11")
            If purviewArray(2) = False Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return

            End If
            Me.LinkButton1.Enabled = purviewArray(1)

            Dim mdfyid As String
            Try
                mdfyid = Request.Params("techid").ToString()
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return

            End Try

            Dim objXmlTr As New clsXml

            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "MODTECHNI")
            Me.Statuslab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Status")
            Me.Address1lab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0006")
            Me.Address2lab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0007")
            AlternateNameLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0005")

            POCodelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0008")
            AreaIDlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0009")
            CountryIDlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCTRY-0001")
            StateIDlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0011")
            Telephone1lab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0012")
            Telephone2lab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0013")

            Me.AreaID2lab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0009")
            Faxlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0015")
            Me.CountryID2lab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCTRY-0001")
            Me.DrivingLicenselab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMTCH-0008")

            Me.MailAddress1lab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMTCH-0006")
            Me.MailAddress2lab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMTCH-0007")
            Me.MaxJobdaylab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMTCH-0009")
            Me.Mobilelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMTCH-0010")
            Me.NRIClab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMTCH-0004")
            Me.POCode2lab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0008")
            Me.ServiceCenterlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0001")
            Me.StateID2lab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0011")
            Me.TechnicianIDlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMTCH-0001")
            Me.TechnicianNamelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMTCH-0002")
            Me.TechnicianTypelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMTCH-0005")
            Me.HRIDlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMTCH-0011") 'LLY 20160404 


            lblNoteUP.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            lblNoteDown.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")

            CreatedBylab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATBY")
            CreatedDatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
            ModiBylab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFBY")
            ModifiedDatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFDATE")
            LinkButton1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Save")
            HyperLink1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Cancel")

            Dim statXmlTr As New clsXml
            statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            Me.POCodeMsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-POCodein")
            'Me.POCodeMsg2.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-POCode")
            Me.FaxMsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Fax")
            Me.Telephone1Msg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Tele")
            Me.Telephone2Msg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Tele")
            Me.mobileMsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Mobile")
            Me.MaxJobdayMsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-MaxJobday")
            Me.TechnicianIDMsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Techid")
            Me.NRICMsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Technric")
            Me.AlternateNamemsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-AlterName")

            Me.TechnicianNamemsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Techname")
            Me.countrymsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "MAS-COUNTRY")
            Me.statmsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "MAS-STAT")
            Me.areamsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "MAS-AREA")
            Me.svcmsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "MAS-SVC")

            Me.TechnicianTypemsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "TECHTMSG")
            Me.DrivingLicensemsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "DRIVEMSG")
            Dim rank As String = Session("login_rank")
            If mdfyid.Trim() <> "" Then
                Dim TechnicianEntity As New clsTechnician()
                TechnicianEntity.TechnicianID = mdfyid
                Dim edtReader As SqlDataReader = TechnicianEntity.GetTechnicianDetailsByID()
                Dim telepass As New clsCommonClass()
                If edtReader.Read() Then
                    Me.TechnicianID.Text = edtReader.GetValue(0).ToString()
                    Me.TechnicianID.ReadOnly = True
                    Me.TechnicianName.Text = edtReader.GetValue(1).ToString()

                    'LLY 20160405
                    'Me.HRID.Text = edtReader.GetValue(29).ToString() commented by Tomas 20180606 because not in used.

                    Me.AlternateName.Text = edtReader.GetValue(2).ToString()

                    Dim cntryStat As New clsrlconfirminf()
                    Dim statParam As New ArrayList
                    statParam = cntryStat.searchconfirminf("TECHSTATUS")
                    Dim count As Integer
                    Dim statid As String
                    Dim statnm As String
                    For count = 0 To statParam.Count - 1
                        statid = statParam.Item(count)
                        statnm = statParam.Item(count + 1)

                        Me.Status.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))

                        'Status.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                        If statid.Equals(edtReader.GetValue(3).ToString()) Then
                            Status.Items(count / 2).Selected = True
                        End If
                        count = count + 1
                    Next
                    If purviewArray(4) = False Then
                        Me.Status.Items.Remove(Me.Status.Items.FindByValue("RESIGNED"))
                        Me.Status.Items.Remove(Me.Status.Items.FindByValue("SUSPEND"))
                        Me.Status.Items.Remove(Me.Status.Items.FindByValue("TERMINATE"))
                    End If

                    Me.NRIC.Text = edtReader.GetValue(4).ToString()

                    Dim mtchty As New clsrlconfirminf()
                    Dim tchtyParam As New ArrayList
                    tchtyParam = mtchty.searchconfirminf("TECHNICIAN")
                    Dim tchtycount As Integer
                    Dim tchtyid As String
                    Dim tchtynm As String
                    For tchtycount = 0 To tchtyParam.Count - 1
                        tchtyid = tchtyParam.Item(tchtycount)
                        tchtynm = tchtyParam.Item(tchtycount + 1)
                        Me.TechnicianType.Items.Add(New ListItem(tchtynm.ToString(), tchtyid.ToString()))
                        If tchtyid.Equals(edtReader.GetValue(5).ToString()) Then
                            TechnicianType.Items(tchtycount / 2).Selected = True
                        End If
                        tchtycount = tchtycount + 1
                    Next

                    Me.Address1.Text = edtReader.GetValue(6).ToString()
                    Me.Address2.Text = edtReader.GetValue(7).ToString()
                    Me.POCode.Text = edtReader.GetValue(8).ToString()

                    'create the dropdownlist
                    'create  country the dropdownlist
                    Dim country As New clsCommonClass

                    If rank <> 0 Then
                        country.spctr = Session("login_ctryID").ToString().ToUpper

                    End If
                    country.rank = rank

                    Dim countryds As New DataSet


                    countryds = country.Getcomidname("BB_MASCTRY_IDNAME")
                    If countryds.Tables.Count <> 0 Then
                        databonds(countryds, Me.CountryID)
                    End If
                    Me.CountryID.Items.Insert(0, "")
                    Me.CountryID.Text = edtReader.GetValue(10).ToString()

                    'create  state the dropdownlist
                    Dim stat As New clsCommonClass
                    If (CountryID.SelectedValue().ToString() <> "") Then
                        stat.spctr = Me.CountryID.SelectedValue().ToString().ToUpper()
                    End If

                    stat.rank = rank
                    Dim stateds As New DataSet

                    stateds = stat.Getcomidname("BB_MASSTAT_IDNAME")
                    If stateds.Tables.Count <> 0 Then
                        databonds(stateds, Me.StateID)
                    End If
                    Me.StateID.Items.Insert(0, "")
                    Me.StateID.Text = edtReader.GetValue(11).ToString()

                    Dim area As New clsCommonClass

                    If (CountryID.SelectedValue().ToString() <> "") Then
                        area.spctr = Me.CountryID.SelectedValue().ToString().ToUpper()
                    End If

                    If (StateID.SelectedValue().ToString() <> "") Then
                        area.spstat = Me.StateID.SelectedValue().ToString().ToUpper()
                    End If
                    area.rank = rank
                    Dim areads As New DataSet

                    areads = area.Getcomidname("BB_MASAREA_IDNAME")
                    If areads.Tables.Count <> 0 Then
                        databonds(areads, Me.AreaID)
                    End If
                    Me.AreaID.Items.Insert(0, "")
                    Me.AreaID.Text = edtReader.GetValue(9).ToString()

                    Me.Telephone1.Text = telepass.passconverttel(edtReader.GetValue(12).ToString())
                    Me.Telephone2.Text = telepass.passconverttel(edtReader.GetValue(13).ToString())
                    Me.Mobile.Text = telepass.passconverttel(edtReader.GetValue(14).ToString())
                    Me.Fax.Text = telepass.passconverttel(edtReader.GetValue(15).ToString())
                    Me.MailAddress1.Text = edtReader.GetValue(16).ToString()

                    Me.MailAddress2.Text = edtReader.GetValue(17).ToString()



                    Me.POCode2.Text = edtReader.GetValue(18).ToString()

                    Dim country2 As New clsCommonClass
                    Dim country2ds As New DataSet
                    If rank <> 0 Then
                        country2.spctr = Session("login_ctryID").ToString().ToUpper

                    End If
                    country2.rank = rank
                    country2ds = country.Getcomidname("BB_MASCTRY_IDNAME")
                    If country2ds.Tables(0).Rows.Count <> 0 Then
                        databonds(country2ds, Me.CountryID2)
                        Me.CountryID2.Items.Insert(0, "")
                    End If
                    Me.CountryID2.Text = edtReader.GetValue(20).ToString()

                    Dim state2 As New clsCommonClass
                    Dim state2ds As New DataSet



                    If (CountryID2.SelectedValue().ToString() <> "") Then
                        state2.spctr = Me.CountryID2.SelectedValue().ToString().ToUpper()
                    End If
                    state2.rank = rank

                    state2ds = state2.Getcomidname("BB_MASSTAT_IDNAME")
                    If state2ds.Tables.Count <> 0 Then
                        databonds(state2ds, Me.StateID2)
                        Me.StateID2.Items.Insert(0, "")
                    End If
                    Me.StateID2.Text = edtReader.GetValue(21).ToString()

                    Dim area2 As New clsCommonClass

                    If (CountryID2.SelectedValue().ToString() <> "") Then
                        area2.spctr = Me.CountryID2.SelectedValue().ToString().ToUpper()
                    End If

                    If (StateID2.SelectedValue().ToString() <> "") Then
                        area2.spstat = Me.StateID2.SelectedValue().ToString().ToUpper()
                    End If
                    area2.rank = rank
                    Dim areads2 As New DataSet

                    areads2 = area2.Getcomidname("BB_MASAREA_IDNAME")
                    If areads2.Tables.Count <> 0 Then
                        databonds(areads2, Me.AreaID2)
                        Me.AreaID2.Items.Insert(0, "")
                    End If
                    Me.AreaID2.Text = edtReader.GetValue(19).ToString()

                    Dim mtchdl As New clsrlconfirminf()
                    Dim tchdlParam As New ArrayList
                    tchdlParam = mtchdl.searchconfirminf("YESNO")
                    Dim tchdlcount As Integer
                    Dim tchdlid As String
                    Dim tchdlnm As String
                    For tchdlcount = 0 To tchdlParam.Count - 1
                        tchdlid = tchdlParam.Item(tchdlcount)
                        tchdlnm = tchdlParam.Item(tchdlcount + 1)
                        Me.DrivingLicense.Items.Add(New ListItem(tchdlnm.ToString(), tchdlid.ToString()))
                        If tchdlid.Equals(edtReader.GetValue(22).ToString()) Then
                            DrivingLicense.Items(tchdlcount / 2).Selected = True
                        End If
                        tchdlcount = tchdlcount + 1
                    Next

                    'Me.ServiceCenter.Text = edtReader.GetValue(23).ToString()

                    Dim service As New clsCommonClass
                    If rank = 7 Then
                        service.spctr = Me.CountryID.SelectedValue().ToString().ToUpper()

                    End If
                    If rank = 8 Then
                        service.spctr = Me.CountryID.SelectedValue().ToString().ToUpper()
                        service.spstat = Session("login_cmpID")
                    End If
                    If rank = 9 Then
                        service.spctr = Me.CountryID.SelectedValue().ToString().ToUpper()
                        service.spstat = Session("login_cmpID")
                        service.sparea = Session("login_svcID")
                    End If
                    If rank = 0 Then
                        service.spctr = Me.CountryID.SelectedValue().ToString().ToUpper()
                    End If
                    service.rank = rank
                    Dim serviceds As New DataSet

                    serviceds = service.Getcomidname("BB_MASSVRC_IDNAME")
                    If serviceds.Tables.Count <> 0 Then
                        databonds(serviceds, Me.ServiceCenter)
                    End If


                    Dim servicelist As New ArrayList


                    If serviceds.Tables(0).Rows.Count <> 0 Then
                        servicelist = databonds(serviceds, Me.ServiceCenter)
                    End If
                    Me.ServiceCenter.Items.Insert(0, "")
                    Dim servicecount As Integer
                    Dim serviceid As String

                    For servicecount = 0 To servicelist.Count - 1
                        serviceid = servicelist.Item(servicecount)

                        If serviceid.Equals(edtReader.GetValue(23).ToString()) Then
                            ServiceCenter.Items(servicecount).Selected = True
                            Me.ServiceCenter.Text = edtReader.GetValue(23).ToString()
                        End If

                    Next



                    Me.MaxJobday.Text = edtReader.GetValue(24).ToString()
                    Dim Rcreatby As String = edtReader.Item(25).ToString().ToUpper()
                    If Rcreatby <> "ADMIN" Then
                        Dim Rcreat As New clsCommonClass
                        Rcreat.userid() = Rcreatby
                        Dim Rcreatds As New DataSet()
                        Rcreatds = Rcreat.Getuseridname("BB_MASUSER_SelByIDName")

                        Me.CreatedBy.Text = Rcreatds.Tables(0).Rows(0).Item(0)
                    Else
                        Me.CreatedBy.Text = "ADMIN-ADMIN"
                    End If
                    'Me.CreatedBy.Text = edtReader.GetValue(25).ToString()
                    CreatedBy.ReadOnly = True
                    Me.CreatedDate.Text = edtReader.GetValue(26).ToString()
                    CreatedDate.ReadOnly = True
                    Dim userIDNamestr As String = Session("userID").ToString().ToUpper + "-" + Session("username").ToString.ToUpper

                    Me.ModiBy.Text = userIDNamestr
                    ModiBy.ReadOnly = True
                    Dim technician As New clsTechnician()
                    technician.username = userIDNamestr
                    Me.ModifiedDate.Text = edtReader.GetValue(28).ToString()

                    ModifiedDate.ReadOnly = True
                End If
                TechnicianName.Focus()
            End If
            'If (Trim(Me.MailAddress1.Text) <> "") Then

            '    Me.POCode2.Enabled = True
            '    Me.MailAddress2.Enabled = True


            'Else
            '    Me.POCode2.Text = "".ToString()
            '    Me.POCode2.Enabled = False
            '    Me.MailAddress2.Text = "".ToString()
            '    Me.MailAddress2.Enabled = False

            '    Me.AreaID2.Items.Clear()
            '    Me.StateID2.Items.Clear()
            '    Me.CountryID2.Items.Clear()
            'End If

        End If
    End Sub


    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton1.Click
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim msginfo As String = objXm.GetLabelName("StatusMessage", "SAVEORNOT")
        Dim msgtitle As String = objXm.GetLabelName("StatusMessage", "SAVETITLE")
        MessageBox1.Confirm(msginfo)

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        
    End Sub
#Region " id bond"
    Public Function databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList) As ArrayList
        dropdown.Items.Clear()
        Dim row As DataRow
        Dim infon As ArrayList = New ArrayList()
        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            NewItem.Text = row("id") & "-" & row("name")
            NewItem.Value = row("id")
            dropdown.Items.Add(NewItem)
            infon.Add(NewItem.Value)
        Next
        Return infon

    End Function
#End Region

    Protected Sub CountryID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CountryID.SelectedIndexChanged
        Me.StateID.Items.Clear()
        Me.AreaID.Items.Clear()
        Me.ServiceCenter.Items.Clear()
        Dim rank As String = Session("login_rank")
        Dim state As New clsCommonClass

        If (CountryID.SelectedValue().ToString() <> "") Then
            state.spctr = Me.CountryID.SelectedValue().ToString().ToUpper()
        End If
        state.rank = rank

        'Dim state As New clsCommonClass
        Dim stateds As New DataSet

        stateds = state.Getcomidname("BB_MASSTAT_IDNAME")
        If stateds.Tables.Count <> 0 Then
            databonds(stateds, Me.StateID)
        End If
        Me.StateID.Items.Insert(0, "")

        Dim area As New clsCommonClass
        'Dim ctridname As String
        If (CountryID.SelectedValue().ToString() <> "") Then
            area.spctr = Me.CountryID.SelectedValue().ToString().ToUpper()
        End If
        'Dim staid As String
        If (StateID.SelectedValue().ToString() <> "") Then
            area.spstat = Me.StateID.SelectedValue().ToString().ToUpper()
        End If
        area.rank = rank

        'create  area the dropdownlist
        'Dim area As New clsCommonClass
        Dim areads As New DataSet

        areads = area.Getcomidname("BB_MASAREA_IDNAME")
        If areads.Tables.Count <> 0 Then
            databonds(areads, Me.AreaID)
        End If
        Me.AreaID.Items.Insert(0, "")

        Dim service As New clsCommonClass
        If rank = 7 Then
            service.spctr = Me.CountryID.SelectedValue().ToString().ToUpper()

        End If
        If rank = 8 Then
            service.spctr = Me.CountryID.SelectedValue().ToString().ToUpper()
            service.spstat = Session("login_cmpID")
        End If
        If rank = 9 Then
            service.spctr = Me.CountryID.SelectedValue().ToString().ToUpper()
            service.spstat = Session("login_cmpID")
            service.sparea = Session("login_svcID")
        End If
        If rank = 0 Then
            service.spctr = Me.CountryID.SelectedValue().ToString().ToUpper()
        End If
        service.rank = rank
        Dim serviceds As New DataSet

        serviceds = service.Getcomidname("BB_MASSVRC_IDNAME")
        If serviceds.Tables.Count <> 0 Then
            databonds(serviceds, Me.ServiceCenter)
        End If
        Me.ServiceCenter.Items.Insert(0, "")
    End Sub

    Protected Sub StateID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles StateID.SelectedIndexChanged
        Me.AreaID.Items.Clear()
        Dim rank As String = Session("login_rank")
        Dim area As New clsCommonClass
        'Dim ctridname As String
        If (CountryID.SelectedValue().ToString() <> "") Then
            area.spctr = Me.CountryID.SelectedValue().ToString().ToUpper()
        End If
        'Dim staid As String
        If (StateID.SelectedValue().ToString() <> "") Then
            area.spstat = Me.StateID.SelectedValue().ToString().ToUpper()
        End If
        area.rank = rank

        'create  area the dropdownlist
        'Dim area As New clsCommonClass
        Dim areads As New DataSet

        areads = area.Getcomidname("BB_MASAREA_IDNAME")
        If areads.Tables.Count <> 0 Then
            databonds(areads, Me.AreaID)
        End If
        Me.AreaID.Items.Insert(0, "")
    End Sub

    Protected Sub CountryID2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CountryID2.SelectedIndexChanged
        Me.AreaID2.Items.Clear()
        Me.StateID2.Items.Clear()
        Dim rank As String = Session("login_rank")
        Dim state As New clsCommonClass

        If (CountryID2.SelectedValue().ToString() <> "") Then
            state.spctr = Me.CountryID2.SelectedValue().ToString().ToUpper()
        End If
        state.rank = rank

        'Dim state As New clsCommonClass
        Dim stateds As New DataSet

        stateds = state.Getcomidname("BB_MASSTAT_IDNAME")
        If stateds.Tables.Count <> 0 Then
            databonds(stateds, Me.StateID2)
        End If
        Me.StateID2.Items.Insert(0, "")
        Dim area As New clsCommonClass
        'Dim ctridname As String
        If (CountryID2.SelectedValue().ToString() <> "") Then
            area.spctr = Me.CountryID2.SelectedValue().ToString().ToUpper()
        End If
        'Dim staid As String
        If (StateID2.SelectedValue().ToString() <> "") Then
            area.spstat = Me.StateID2.SelectedValue().ToString().ToUpper()
        End If
        area.rank = rank

        'create  area the dropdownlist
        'Dim area As New clsCommonClass
        Dim areads As New DataSet

        areads = area.Getcomidname("BB_MASAREA_IDNAME")
        If areads.Tables.Count <> 0 Then
            databonds(areads, Me.AreaID2)
        End If
        Me.AreaID2.Items.Insert(0, "")
        'Dim script As String = "self.location='#POCode';"
        'Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "POCode", script, True)
    End Sub

    Protected Sub StateID2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles StateID2.SelectedIndexChanged
        Me.AreaID2.Items.Clear()
        Dim rank As String = Session("login_rank")
        Dim area As New clsCommonClass
        'Dim ctridname As String
        If (CountryID2.SelectedValue().ToString() <> "") Then
            area.spctr = Me.CountryID2.SelectedValue().ToString().ToUpper()
        End If
        'Dim staid As String
        If (StateID2.SelectedValue().ToString() <> "") Then
            area.spstat = Me.StateID2.SelectedValue().ToString().ToUpper()
        End If
        area.rank = rank

        'create  area the dropdownlist
        'Dim area As New clsCommonClass
        Dim areads As New DataSet

        areads = area.Getcomidname("BB_MASAREA_IDNAME")
        If areads.Tables.Count <> 0 Then
            databonds(areads, Me.AreaID2)
        End If
        Me.AreaID2.Items.Insert(0, "")
        'Dim script As String = "self.location='#POCode';"
        'Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "POCode", script, True)
    End Sub

    Protected Sub MessageBox1_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox1.GetMessageBoxResponse
        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
            Dim TechnicianEntity As New clsTechnician()
            Dim telepass As New clsCommonClass()
            If (Trim(Me.Address1.Text) <> "") Then
                TechnicianEntity.Address1 = Me.Address1.Text.ToString().ToUpper()
            End If
            If (Trim(Me.Address2.Text) <> "") Then
                TechnicianEntity.Address2 = Me.Address2.Text.ToString().ToUpper()
            End If
            If (Trim(Me.AlternateName.Text) <> "") Then
                TechnicianEntity.AlternateName = Me.AlternateName.Text.ToString().ToUpper()
            End If
            If (Status.SelectedValue().ToString() <> "") Then

                TechnicianEntity.Status = Me.Status.SelectedValue().ToString()
            End If

            If (AreaID.SelectedValue().ToString() <> "") Then
                TechnicianEntity.AreaID = Me.AreaID.SelectedValue().ToString()
            End If
            If (AreaID2.SelectedValue().ToString() <> "") Then
                TechnicianEntity.AreaID2 = Me.AreaID2.SelectedValue().ToString()
            End If
            If (DrivingLicense.SelectedValue().ToString() <> "") Then
                TechnicianEntity.DrivingLicense = Me.DrivingLicense.SelectedValue().ToString()
            End If
            If (Trim(Me.MailAddress1.Text) <> "") Then
                TechnicianEntity.MailingAddress1 = Me.MailAddress1.Text.ToString().ToUpper()
            End If
            If (Trim(Me.MailAddress2.Text) <> "") Then
                TechnicianEntity.MailingAddress2 = Me.MailAddress2.Text.ToString().ToUpper()
            End If
            If (CountryID.SelectedValue().ToString() <> "") Then
                TechnicianEntity.CountryID = Me.CountryID.SelectedValue().ToString()
            End If
            If (CountryID2.SelectedValue().ToString() <> "") Then
                TechnicianEntity.CountryID2 = Me.CountryID2.SelectedValue().ToString()
            End If
            If (StateID.SelectedValue().ToString() <> "") Then
                TechnicianEntity.StateID = Me.StateID.SelectedValue().ToString()
            End If
            If (StateID2.SelectedValue().ToString() <> "") Then
                TechnicianEntity.StateID2 = Me.StateID2.SelectedValue().ToString()
            End If

            If (Trim(Me.Fax.Text) <> "") Then
                TechnicianEntity.Fax = telepass.telconvertpass(Me.Fax.Text.ToString())
            End If
            If (Trim(Me.Mobile.Text) <> "") Then
                TechnicianEntity.Mobile = telepass.telconvertpass(Me.Mobile.Text.ToString())
            End If
            If (Trim(Me.MaxJobday.Text.ToString()) <> "") Then
                TechnicianEntity.MaxJobday = Convert.ToInt32(Me.MaxJobday.Text.ToString())
            End If

            If (Trim(Me.NRIC.Text) <> "") Then
                TechnicianEntity.NRIC = Me.NRIC.Text.ToString().ToUpper()
            End If
            If (Trim(Me.TechnicianID.Text) <> "") Then
                TechnicianEntity.TechnicianID = Me.TechnicianID.Text.ToString().ToUpper()
            End If

            'LLY 20140404
            If (Trim(Me.HRID.Text) <> "") Then
                TechnicianEntity.HRID = Me.HRID.Text.ToString().ToUpper()
            End If

            If (Trim(Me.POCode.Text) <> "") Then
                TechnicianEntity.POCode = Me.POCode.Text.ToString()
            End If
            If (Trim(Me.POCode2.Text) <> "") Then
                TechnicianEntity.POCode2 = Me.POCode2.Text.ToString()
            End If
            If (Trim(Me.Telephone1.Text) <> "") Then
                TechnicianEntity.Telephone1 = telepass.telconvertpass(Me.Telephone1.Text.ToString())
            End If
            If (Trim(Me.Telephone2.Text) <> "") Then
                TechnicianEntity.Telephone2 = telepass.telconvertpass(Me.Telephone2.Text.ToString())
            End If
            If (TechnicianType.SelectedValue().ToString() <> "") Then
                TechnicianEntity.TechnicianType = Me.TechnicianType.SelectedValue().ToString()
            End If
            If (ServiceCenter.SelectedValue().ToString() <> "") Then
                TechnicianEntity.ServiceCenter = Me.ServiceCenter.SelectedValue().ToString()
            End If
            If (Trim(Me.TechnicianName.Text) <> "") Then
                TechnicianEntity.TechnicianName = Me.TechnicianName.Text.ToString().ToUpper()
            End If

            If (Trim(ModiBy.Text) <> "") Then
                TechnicianEntity.ModifyBy = Session("userID").ToString().ToUpper
            End If

            If (Trim(CreatedBy.Text) <> "") Then
                TechnicianEntity.CreateBy = Me.CreatedBy.Text().ToUpper()
            End If
            TechnicianEntity.Ipadress = Request.UserHostAddress.ToString()
            TechnicianEntity.svcid = Session("login_svcID")
            TechnicianEntity.username = Session("userID")
            If Me.MailAddress1.Text <> "" Then
                If Me.POCode2.Text = "" Then
                    Dim objXm As New clsXml
                    objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    errlab.Text = objXm.GetLabelName("StatusMessage", "TECHMAILPO")
                    errlab.Visible = True
                    Return
                End If
                If CountryID2.SelectedValue().ToString() = "" Then
                    Dim objXm As New clsXml
                    objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    errlab.Text = objXm.GetLabelName("StatusMessage", "TECHMAILCOUNTRY")
                    errlab.Visible = True
                    Return
                End If
                If StateID2.SelectedValue().ToString() = "" Then
                    Dim objXm As New clsXml
                    objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    errlab.Text = objXm.GetLabelName("StatusMessage", "TECHMAILSTATE")
                    errlab.Visible = True
                    Return
                End If
                If AreaID2.SelectedValue().ToString() = "" Then
                    Dim objXm As New clsXml
                    objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    errlab.Text = objXm.GetLabelName("StatusMessage", "TECHMAILAREA")
                    errlab.Visible = True
                    Return
                End If
            End If
            If Me.MailAddress1.Text = "" And Me.MailAddress2.Text = "" Then
                If Me.POCode2.Text <> "" Then
                    Dim objXm As New clsXml
                    objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    errlab.Text = objXm.GetLabelName("StatusMessage", "TECHMAILADRES1")
                    errlab.Visible = True
                    Return
                End If
                If CountryID2.SelectedValue().ToString() <> "" Then
                    Dim objXm As New clsXml
                    objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    errlab.Text = objXm.GetLabelName("StatusMessage", "TECHMAILADRES1")
                    errlab.Visible = True
                    Return
                End If
                If StateID2.SelectedValue().ToString() <> "" Then
                    Dim objXm As New clsXml
                    objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    errlab.Text = objXm.GetLabelName("StatusMessage", "TECHMAILADRES1")
                    errlab.Visible = True
                    Return
                End If
                If AreaID2.SelectedValue().ToString() <> "" Then
                    Dim objXm As New clsXml
                    objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    errlab.Text = objXm.GetLabelName("StatusMessage", "TECHMAILADRES1")
                    errlab.Visible = True
                    Return
                End If
            End If
            Dim NRICCount As New clsTechnician()
            Dim count As New Integer
            count = NRICCount.GetNRICCount(Me.NRIC.Text.ToString(), Me.TechnicianID.Text.ToString())

            If (count <> 0) Then
                Dim objXm As New clsXml
                objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                errlab.Text = objXm.GetLabelName("StatusMessage", "DUPTCHNM")
                errlab.Visible = True
               
            ElseIf (count = 0) Then
                If Me.Status.SelectedValue().ToString() = "DELETE" Then
                    Dim deletech As Integer = TechnicianEntity.GetTelDel()
                    If deletech.Equals(-1) Then
                        Dim objXm As New clsXml
                        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                        errlab.Text = objXm.GetLabelName("StatusMessage", "DUPTECHROU")
                        errlab.Visible = True
                    ElseIf deletech.Equals(0) Then
                        Dim updCtryCnt As Integer = TechnicianEntity.Update()
                        Dim objXmlTr As New clsXml
                        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                        If updCtryCnt = 0 Then
                            ' Response.Redirect("country.aspx")
                            Dim objXm As New clsXml
                            objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                            errlab.Text = objXm.GetLabelName("StatusMessage", "BB-HintMsg-Suc")
                            errlab.Visible = True
                        Else
                            Response.Redirect("~/PresentationLayer/Error.aspx")
                        End If
                    End If
                Else
                    Dim updCtryCnt As Integer = TechnicianEntity.Update()
                    Dim objXmlTr As New clsXml
                    objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    If updCtryCnt = 0 Then
                        ' Response.Redirect("country.aspx")
                        Dim objXm As New clsXml
                        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                        errlab.Text = objXm.GetLabelName("StatusMessage", "BB-HintMsg-Suc")
                        errlab.Visible = True
                    Else
                        Response.Redirect("~/PresentationLayer/Error.aspx")
                    End If
                End If
                
            End If
        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
        End If

    End Sub

    'Protected Sub MailAddress1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles MailAddress1.TextChanged
    '    If (Trim(Me.MailAddress1.Text) <> "") Then

    '        Me.POCode2.Enabled = True
    '        Me.MailAddress2.Enabled = True

    '        Dim country As New clsCommonClass
    '        Dim rank As String = Session("login_rank")
    '        If rank <> 0 Then
    '            country.spctr = Session("login_ctryID").ToString().ToUpper

    '        End If
    '        country.rank = rank

    '        Dim countryds As New DataSet


    '        countryds = country.Getcomidname("BB_MASCTRY_IDNAME")
    '        If countryds.Tables.Count <> 0 Then

    '            databonds(countryds, Me.CountryID2)
    '            Me.CountryID2.Items.Insert(0, "")

    '        End If
    '    Else
    '        Me.POCode2.Text = "".ToString()
    '        Me.POCode2.Enabled = False
    '        Me.MailAddress2.Text = "".ToString()
    '        Me.MailAddress2.Enabled = False

    '        Me.AreaID2.Items.Clear()
    '        Me.StateID2.Items.Clear()
    '        Me.CountryID2.Items.Clear()
    '    End If
    'End Sub

End Class
