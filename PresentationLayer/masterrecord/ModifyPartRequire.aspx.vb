Imports BusinessEntity
Imports System.Configuration
Imports System.Data
Imports System.Data.SqlClient
Partial Class PresentationLayer_masterrecord_ModifyPartRequire
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        If Not Page.IsPostBack Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Response.Redirect("~/PresentationLayer/logon.aspx")
                End If
            Catch ex As Exception
                Response.Redirect("~/PresentationLayer/logon.aspx")
            End Try
            partnametbox.Focus()

            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "06")
            Me.saveButton.Enabled = purviewArray(1)
            If purviewArray(2) = False Then
                Response.Redirect("~/PresentationLayer/logon.aspx")
            End If
            partidtbox.Focus()

            'set value for LABEls
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            partidlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCAT-0001")
            partnameLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCAT-0002")
            partanamelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCAT-0003")
            countryidlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCAT-0004")
            CategoryIDLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCAT-0005")
            ModelIDlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCAT-0006")
            FiniProdulab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCAT-0007")
            uomlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCAT-0008")
            EffectDatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCAT-0009")
            ObsolDatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCAT-0010")
            suppnamelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCAT-0011")
            TradingItemlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCAT-0012")
            commonpartlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCAT-0013")
            ServiceItemlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCAT-0014")
            KitItemlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCAT-0015")
            Statuslab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCAT-0016")
            remarkslab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCAT-0017")
            createbylab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATBY")
            creatdatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
            modifybylab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFBY")
            modifydatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFDATE")
            Label8.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            Label9.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")

            saveButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Save")
            HyperLink1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Cancel")
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Modifyparttl")
            Addkit.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-AddKitBT")
            'SET MESSAGE FOR ERRORS
            Dim XmlTr As New clsXml
            XmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            partiderr.ErrorMessage = XmlTr.GetLabelName("StatusMessage", "BB-HintMsg-PARTID")
            countryerr.ErrorMessage = XmlTr.GetLabelName("StatusMessage", "BB-HintMsg-COUNTRYID")
            partnameerr.ErrorMessage = XmlTr.GetLabelName("StatusMessage", "BB-HintMsg-PARTNAMEERR")
            partcateerr.ErrorMessage = XmlTr.GetLabelName("StatusMessage", "BB-HintMsg-partcategoryerr")
            modelerr.ErrorMessage = XmlTr.GetLabelName("StatusMessage", "BB-HintMsg-MID")

            'for modify partrequirement contents
            Dim mdfyid As String
            Dim countrid As String

            Dim editkit As String = Request.QueryString("editkit")
            'Dim editkit As String = Request.Params("editkit").ToString()
            If editkit = "1" Then
                mdfyid = Request.Params("partid").ToString()
                countrid = Request.Params("countryid").ToString()
            Else
                mdfyid = Request.Params("partid").ToString()
                countrid = Request.Params("ctrid").ToString()
            End If

            Try
                If editkit = "1" Then
                    Dim str As String = Request.Params("partid").ToString()
                    str = Request.Params("countryid").ToString()
                Else
                    Dim str As String = Request.Params("partid").ToString()
                    str = Request.Params("ctrid").ToString()
                End If
            Catch ex As Exception
                Response.Redirect("~/PresentationLayer/logon.aspx")
            End Try

            If mdfyid.Trim() <> "" Then
                Dim partEntity As New ClsPartRequirement()
                Dim Stat As New clsrlconfirminf()
                partEntity.partid = mdfyid
                partEntity.Countryid = countrid
                Dim edtReader As SqlDataReader = partEntity.GetpartDetailsByID()
                If edtReader.Read() Then
                    Dim partbond As New clsCommonClass
                    Dim count As Integer
                    Dim statid As String
                    Dim statnm As String
                    partidtbox.Text = edtReader.GetValue(0).ToString() 'part id
                    partidtbox.ReadOnly = True
                    partnametbox.Text = edtReader.GetValue(1).ToString() 'part name
                    partanametbox.Text = edtReader.GetValue(2).ToString() 'altername
                    'bond country id and name

                    'Dim partds As New DataSet
                    'partds = partbond.Getidname("BB_MASCTRY_IDNAME")
                    'countryiddrpd.Items.Clear()
                    'databonds(partds, countryiddrpd)
                    'countryiddrpd.Text = edtReader.GetValue(3).ToString() 'country id
                    'countryiddrpd.Enabled = False

                    Dim rank As String = Session("login_rank")
                    partbond.rank = rank
                    If rank <> 0 Then
                        partbond.spctr = Session("login_ctryID")
                        partbond.spstat = Session("login_cmpID")
                        partbond.sparea = Session("login_svcID")
                    End If
                    Dim serviceds As New DataSet
                    serviceds = partbond.Getcomidname("BB_MASCTRY_IDNAME")
                    If serviceds.Tables.Count <> 0 Then
                        databonds(serviceds, Me.countryiddrpd)
                    End If
                    countryiddrpd.Text = edtReader.GetValue(3).ToString() 'country id
                    countryiddrpd.Enabled = False

                    'bond  catage id and name
                    Dim catege As New DataSet
                    catege = partbond.Getcomidname("BB_MASPCAT_IDNAME")
                    If catege.Tables.Count <> 0 Then
                        databonds(catege, CategoryIDdrpd)
                    End If
                    CategoryIDdrpd.Text = edtReader.GetValue(4).ToString() 'category id

                    'bond parent/model id and name
                    Dim centerds As New DataSet
                    centerds = partbond.Getcomidname("BB_MASMOTY_IDNAME")
                    If centerds.Tables.Count <> 0 Then
                        databonds(centerds, ModelIDdrpd)
                    End If
                    ModelIDdrpd.Text = edtReader.GetValue(5).ToString() 'parent/model id

                    'set value for "yes/no" items
                    Dim yesnopar As ArrayList = New ArrayList
                    yesnopar = Stat.searchconfirminf("YESNO")
                    count = 0
                    For count = 0 To yesnopar.Count - 1
                        statid = yesnopar.Item(count)
                        statnm = yesnopar.Item(count + 1)

                        FiniProdudrpd.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                        TradingItemdrpd.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                        commonpartdrpd.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                        ServiceItemdrpd.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                        KitItemdrpd.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                        If statid.Equals(edtReader.GetValue(6).ToString()) Then
                            FiniProdudrpd.Items(count / 2).Selected = True
                        End If
                        If statid.Equals(edtReader.GetValue(11).ToString()) Then
                            TradingItemdrpd.Items(count / 2).Selected = True
                        End If
                        If statid.Equals(edtReader.GetValue(12).ToString()) Then
                            commonpartdrpd.Items(count / 2).Selected = True
                        End If
                        If statid.Equals(edtReader.GetValue(13).ToString()) Then
                            ServiceItemdrpd.Items(count / 2).Selected = True
                        End If
                        If statid.Equals(edtReader.GetValue(14).ToString()) Then
                            KitItemdrpd.Items(count / 2).Selected = True
                        End If
                        count = count + 1
                    Next

                    'set the uom dropdownlist
                    Dim techtypepar As ArrayList = New ArrayList
                    techtypepar = Stat.searchconfirminf("UOM")
                    count = 0
                    For count = 0 To techtypepar.Count - 1
                        statid = techtypepar.Item(count)
                        statnm = techtypepar.Item(count + 1)
                        uomdrpd.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                        If statid.Equals(edtReader.GetValue(7).ToString()) Then
                            uomdrpd.Items(count / 2).Selected = True
                        End If
                        count = count + 1
                    Next

                    If edtReader.GetValue(8).ToString.Trim() <> "" Then
                        EffectDatetbox.Text = edtReader.GetValue(8).ToString().Substring(0, 10)
                    End If

                    If edtReader.GetValue(9).ToString.Trim() <> "" Then
                        ObsolDatetbox.Text = edtReader.GetValue(9).ToString().Substring(0, 10)

                    End If
                    suppnametbox.Text = edtReader.GetValue(10).ToString() 'Supplier Name 

                    'set status dropdownlist
                    Dim statParam As ArrayList = New ArrayList
                    statParam = Stat.searchconfirminf("STATUS")
                    count = 0
                    For count = 0 To statParam.Count - 1
                        statid = statParam.Item(count)
                        statnm = statParam.Item(count + 1)
                        If statid.Equals("ACTIVE") Or statid.Equals("OBSOLETE") Or statid.Equals("DELETE") Then
                            Statusdrpd.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                        End If
                        If statid.Equals(edtReader.GetValue(15).ToString()) Then
                            Statusdrpd.Items(count / 2).Selected = True
                        End If
                        count = count + 1
                    Next
                    If Convert.ToString(purviewArray(4)).ToUpper = "FALSE" Then
                        Me.Statusdrpd.Items.Remove(Me.Statusdrpd.Items.FindByValue("DELETE"))
                    End If

                    remarkstbox.Text = edtReader.GetValue(16).ToString() 'remarks 

                    Dim Rcreatby As String = edtReader.GetValue(17).ToString().ToUpper()
                    If Rcreatby <> "ADMIN" Then
                        Dim Rcreat As New clsCommonClass
                        Rcreat.userid() = Rcreatby
                        Dim Rcreatds As New DataSet()
                        Rcreatds = Rcreat.Getuseridname("BB_MASUSER_SelByIDName")

                        Me.creatbytbox.Text = Rcreatds.Tables(0).Rows(0).Item(0)
                    Else
                        Me.creatbytbox.Text = "ADMIN-ADMIN"
                    End If

                    'creatbytbox.Text = edtReader.GetValue(17).ToString()
                    creatbytbox.ReadOnly = True

                    creatdatetbox.Text = edtReader.GetValue(18).ToString()
                    creatdatetbox.ReadOnly = True
                    'modifybytbox.Text = edtReader.GetValue(19).ToString()
                    modifybytbox.ReadOnly = True
                    modifydatetbox.ReadOnly = True
                    Dim userIDNamestr As String = Session("userID").ToString().ToUpper + "-" + Session("username").ToString.ToUpper
                    modifybytbox.Text = userIDNamestr
                    modifydatetbox.Text = edtReader.GetValue(20).ToString()
                    retrievekit()

                End If
            End If
            If KitItemdrpd.SelectedItem.Value.ToString() = "Y" Then
                Addkit.Enabled = True
            Else
                Addkit.Enabled = False
            End If

            'BY PWTAI
            Dim PartEntity2 As New ClsPartRequirement()
            If (Trim(partidtbox.Text) <> "") Then
                PartEntity2.partid = partidtbox.Text.ToUpper()
                PartEntity2.Countryid = Session("login_ctryID")
            End If

            Dim dupCount As Integer = PartEntity2.GetPartUse()

            If dupCount.Equals(-1) Then 'Part Being Use then the part' kit cannot change to Y
                KitItemdrpd.Text = "NO"
                KitItemdrpd.Enabled = False
            Else
                KitItemdrpd.Enabled = True
            End If

            HypCal.NavigateUrl = "javascript:DoCal(document.AddPartRequire.EffectDatetbox);"
            HypCal2.NavigateUrl = "javascript:DoCal(document.AddPartRequire.ObsolDatetbox);"
            'BY PWTAI
        End If
    End Sub

    Protected Sub saveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveButton.Click

        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")

        Dim temparr As Array = Request.Form("EffectDatetbox").Split("/") 'EffectDatetbox.Text.Split("/")
        Dim streff As String
        Dim strobs As String
        streff = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
        temparr = Request.Form("ObsolDatetbox").Split("/") 'ObsolDatetbox.Text.Split("/")
        strobs = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
        If Convert.ToDateTime(strobs) < Convert.ToDateTime(streff) Then
            errlab.Text = objXm.GetLabelName("StatusMessage", "BB-HintMsg-COMPAREEFFOBDATE")
            errlab.Visible = True
            Return
        End If

        If Me.Statusdrpd.SelectedValue = "DELETE" Then
            If check() = 1 Then
                errlab.Text = objXm.GetLabelName("StatusMessage", "PART_CHECKDELSTAT")
                errlab.Visible = True
                Return
            End If
        End If
        EffectDatetbox.Text = Request.Form("EffectDatetbox")
        ObsolDatetbox.Text = Request.Form("ObsolDatetbox")
        Dim msginfo As String = objXm.GetLabelName("StatusMessage", "SAVEORNOT")
        Dim msgtitle As String = objXm.GetLabelName("StatusMessage", "SAVETITLE")
        MessageBox1.Confirm(msginfo)
    End Sub
#Region " bonds"
    Public Function databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        dropdown.Items.Add(" ")
        Dim row As DataRow
        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            NewItem.Text = row("id") & "-" & row("name")
            NewItem.Value = row("id")
            dropdown.Items.Add(NewItem)
        Next
    End Function
#End Region
    Protected Sub kititemview_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles kititemview.PageIndexChanging
        kititemview.PageIndex = e.NewPageIndex
        Dim kitEntity As New ClsPartRequirement()
        kitEntity.partid = partidtbox.Text.ToUpper()
        kitEntity.Countryid = Session("login_ctryID")

        Dim ctryall As DataSet = kitEntity.GetKititem()
        If ctryall.Tables(0).Rows.Count <> 0 Then
            kititemview.DataSource = ctryall
            Session("kitView") = ctryall
            kititemview.Visible = True
            kititemview.AllowPaging = True
            'kititemview.AllowSorting = True
            kititemview.DataBind()
            'Addkit.Visible = True
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            kititemview.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-KITITEMHD0")
            kititemview.HeaderRow.Cells(1).Font.Size = 8
            kititemview.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-KITITEMHD1")
            kititemview.HeaderRow.Cells(2).Font.Size = 8
            kititemview.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-KITITEMHD2")
            kititemview.HeaderRow.Cells(3).Font.Size = 8
            kititemview.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCAT-0001")
            kititemview.HeaderRow.Cells(4).Font.Size = 8
        End If
        EffectDatetbox.Text = Request.Form("EffectDatetbox")
        ObsolDatetbox.Text = Request.Form("ObsolDatetbox")
    End Sub
#Region " LOAD retrieve kititem message"
    Public Function retrievekit() ' As Integer
        Dim kitEntity As New ClsPartRequirement()
        kitEntity.partid = partidtbox.Text.ToUpper()
        kitEntity.Countryid = Session("login_ctryID")

        Dim ctryall As DataSet = kitEntity.GetKititem()
        kititemview.Visible = True
        kititemview.DataSource = ctryall
        Session("kitView") = ctryall
        kititemview.AllowPaging = True
        'kititemview.AllowSorting = True
        If ctryall.Tables(0).Rows.Count > 0 Then
            Dim XmlTr As New clsXml
            Dim editcol As New CommandField
            XmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            editcol.EditText = XmlTr.GetLabelName("EngLabelMsg", "BB-Edit") '"edit"
            editcol.HeaderImageUrl = "~/PresentationLayer/graph/edit.gif"
            editcol.ShowEditButton = True
            kititemview.Columns.Add(editcol)
            kititemview.DataBind()
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            kititemview.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-KITITEMHD0")
            kititemview.HeaderRow.Cells(1).Font.Size = 8
            kititemview.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-KITITEMHD1")
            kititemview.HeaderRow.Cells(2).Font.Size = 8
            kititemview.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-KITITEMHD2")
            kititemview.HeaderRow.Cells(3).Font.Size = 8
            kititemview.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCAT-0001")
            kititemview.HeaderRow.Cells(4).Font.Size = 8
            'Return 1
        Else
            'Return 0
        End If
    End Function
#End Region

    Protected Sub KitItemdrpd_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles KitItemdrpd.TextChanged
        'retrievekitITEM()
        Dim kitEntity As New ClsPartRequirement()
        kitEntity.partid = partidtbox.Text.ToUpper()
        kitEntity.Countryid = Session("login_ctryID")

        Dim ctryall As DataSet = kitEntity.GetKititem()
        kititemview.DataSource = ctryall
        Session("kitView") = ctryall
        kititemview.DataBind()

        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        If ctryall.Tables(0).Rows.Count <> 0 Then
            kititemview.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-KITITEMHD0")
            kititemview.HeaderRow.Cells(1).Font.Size = 8
            kititemview.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-KITITEMHD1")
            kititemview.HeaderRow.Cells(2).Font.Size = 8
            kititemview.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-KITITEMHD2")
            kititemview.HeaderRow.Cells(3).Font.Size = 8
            kititemview.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCAT-0001")
            kititemview.HeaderRow.Cells(4).Font.Size = 8
        End If

        If KitItemdrpd.SelectedItem.Value.ToString() = "Y" Then
            kititemview.Visible = True
            kititemview.AllowPaging = True
            'kititemview.AllowSorting = True
            Addkit.Enabled = True
        Else
            If ctryall.Tables(0).Rows.Count <> 0 Then
                Dim objXm As New clsXml
                objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                errlab.Text = objXm.GetLabelName("StatusMessage", "BB-HintMsg-KITITEMYN")
                errlab.Visible = True
                KitItemdrpd.SelectedIndex = 0
                Return
            End If
            kititemview.Visible = False
            Addkit.Enabled = False

        End If
        EffectDatetbox.Text = Request.Form("EffectDatetbox")
        ObsolDatetbox.Text = Request.Form("ObsolDatetbox")
    End Sub

#Region " retrieve kititem message"
    Public Function retrievekitITEM()
        'If KitItemdrpd.SelectedItem.Value.ToString() = "Y" Then
        Dim kitEntity As New ClsPartRequirement()
        kitEntity.partid = partidtbox.Text.ToUpper()
        kitEntity.Countryid = Session("login_ctryID")
        Dim ctryall As DataSet = kitEntity.GetKititem()

        kititemview.DataSource = ctryall
        Session("kitView") = ctryall
        kititemview.DataBind()

        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        If ctryall.Tables(0).Rows.Count <> 0 Then
            kititemview.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-KITITEMHD0")
            kititemview.HeaderRow.Cells(1).Font.Size = 8
            kititemview.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-KITITEMHD1")
            kititemview.HeaderRow.Cells(2).Font.Size = 8
            kititemview.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-KITITEMHD2")
            kititemview.HeaderRow.Cells(3).Font.Size = 8
            kititemview.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCAT-0001")
            kititemview.HeaderRow.Cells(4).Font.Size = 8
        End If

        If KitItemdrpd.SelectedItem.Value.ToString() = "Y" Then
            kititemview.Visible = True
            kititemview.AllowPaging = True
            'kititemview.AllowSorting = True
            Addkit.Enabled = True
        Else
            kititemview.Visible = False
            Addkit.Enabled = False

            End If
    End Function
#End Region

    Protected Sub MessageBox1_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox1.GetMessageBoxResponse
        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")

        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
            Dim PartEntity As New ClsPartRequirement()
            Dim calendatetime As New clsCommonClass()
            If (Trim(partidtbox.Text) <> "") Then
                PartEntity.partid = partidtbox.Text.ToUpper()
            End If
            If (Trim(partnametbox.Text) <> "") Then
                PartEntity.partname = partnametbox.Text.ToUpper()
            End If
            If (Trim(partanametbox.Text) <> "") Then
                PartEntity.altname = partanametbox.Text.ToUpper()
            End If
            If (Trim(countryiddrpd.SelectedValue().ToString()) <> "") Then
                PartEntity.Countryid = countryiddrpd.SelectedItem.Value.ToString()
            End If
            If (Trim(CategoryIDdrpd.SelectedValue().ToString()) <> "") Then
                PartEntity.catecode = CategoryIDdrpd.SelectedItem.Value.ToString()
            End If
            If (Trim(ModelIDdrpd.SelectedValue().ToString()) <> "") Then
                PartEntity.modelid = ModelIDdrpd.SelectedItem.Value.ToString()
            End If
            If (Trim(FiniProdudrpd.SelectedValue().ToString()) <> "") Then
                PartEntity.finipro = FiniProdudrpd.SelectedItem.Value.ToString()
            End If
            If (Trim(uomdrpd.SelectedValue().ToString()) <> "") Then
                PartEntity.uom = uomdrpd.SelectedItem.Value.ToString()
            End If
            If (Trim(Request.Form("EffectDatetbox")) <> "") Then
                'PartEntity.effectivedate = calendatetime.DatetoDatabase(EffectDatetbox.Text)
                Dim temparr As Array = Request.Form("EffectDatetbox").Split("/") 'EffectDatetbox.Text.Split("/")
                PartEntity.effectivedate = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
            End If
            If (Trim(Request.Form("ObsolDatetbox")) <> "") Then
                'PartEntity.obsoleteDate = calendatetime.DatetoDatabase(ObsolDatetbox.Text)
                Dim temparr As Array = Request.Form("ObsolDatetbox").Split("/") 'ObsolDatetbox.Text.Split("/")
                PartEntity.obsoleteDate = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
            End If
            If (Trim(suppnametbox.Text) <> "") Then
                PartEntity.suppliername = suppnametbox.Text.ToUpper()
            End If

            If (TradingItemdrpd.SelectedValue().ToString() <> "") Then
                PartEntity.traditem = TradingItemdrpd.SelectedItem.Value.ToString()
            End If
            If (commonpartdrpd.SelectedValue().ToString() <> "") Then
                PartEntity.commonpart = commonpartdrpd.SelectedItem.Value.ToString()
            End If
            If (ServiceItemdrpd.SelectedValue().ToString() <> "") Then
                PartEntity.serviceitem = ServiceItemdrpd.SelectedItem.Value.ToString()
            End If
            If (KitItemdrpd.SelectedValue().ToString() <> "") Then
                PartEntity.kititem = KitItemdrpd.SelectedItem.Value.ToString()
            End If
            If (Statusdrpd.SelectedValue().ToString() <> "") Then
                PartEntity.Status = Statusdrpd.SelectedItem.Value.ToString()
            End If
            If (Trim(remarkstbox.Text) <> "") Then
                PartEntity.Remarks = remarkstbox.Text.ToUpper()
            End If
            If (Trim(creatbytbox.Text) <> "") Then
                PartEntity.CreatedBy = Session("userID").ToString().ToUpper
            End If

            If (Trim(creatdatetbox.Text) <> "") Then
                PartEntity.CreatedDate = calendatetime.DatetoDatabase(creatdatetbox.Text)
            End If

            If (Trim(modifybytbox.Text) <> "") Then
                PartEntity.ModifiedBy = Session("userID").ToString().ToUpper
            End If

            If (Trim(modifydatetbox.Text) <> "") Then
                PartEntity.ModifiedDate = calendatetime.DatetoDatabase(modifydatetbox.Text)
            End If

            If UCase(PartEntity.Status) = UCase("OBSOLETE") Or UCase(PartEntity.Status) = UCase("DELETE") Then
                Dim dupCount As Integer = PartEntity.GetExistKit()
                If dupCount.Equals(-1) Then 'Have Kit
                    Dim msginfo As String = objXm.GetLabelName("StatusMessage", "BB-HintMsg-SAVEDUPKIT")
                    MessageBox2.Confirm(msginfo)
                Else 'Don hv kit
                    Dim dupCount1 As Integer = PartEntity.GetDuplicatedModifypartName()
                    If dupCount1.Equals(-1) Then
                        Dim objXm1 As New clsXml
                        objXm1.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                        errlab.Text = objXm1.GetLabelName("StatusMessage", "DUPPARTNAME")
                        errlab.Visible = True
                    Else
                        PartEntity.logsercenter = Session("login_svcID")
                        PartEntity.ipadress = Request.UserHostAddress.ToString()
                        Dim updCtryCnt As Integer = PartEntity.Update()
                        Dim objXmlTr As New clsXml
                        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                        If updCtryCnt = 0 Then
                            errlab.Text = objXm.GetLabelName("StatusMessage", "BB-HintMsg-Suc")
                            errlab.Visible = True
                            retrievekitITEM()
                        Else
                            Response.Redirect("~/PresentationLayer/Error.aspx")
                        End If
                    End If
                End If
            Else
                Dim dupCount2 As Integer = PartEntity.GetDuplicatedModifypartName()
                If dupCount2.Equals(-1) Then
                    Dim objXm1 As New clsXml
                    objXm1.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    errlab.Text = objXm1.GetLabelName("StatusMessage", "DUPPARTNAME")
                    errlab.Visible = True
                Else
                    PartEntity.logsercenter = Session("login_svcID")
                    PartEntity.ipadress = Request.UserHostAddress.ToString()
                    Dim updCtryCnt As Integer = PartEntity.Update()
                    Dim objXmlTr As New clsXml
                    objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    If updCtryCnt = 0 Then
                        errlab.Text = objXm.GetLabelName("StatusMessage", "BB-HintMsg-Suc")
                        errlab.Visible = True
                        retrievekitITEM()
                    Else
                        Response.Redirect("~/PresentationLayer/Error.aspx")
                    End If
                End If
            End If
        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
            EffectDatetbox.Text = Request.Form("EffectDatetbox")
            ObsolDatetbox.Text = Request.Form("ObsolDatetbox")
        End If
    End Sub

    Protected Sub kititemview_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles kititemview.RowEditing
        Dim ds As DataSet = Session("kitView")
        Dim page As Integer = kititemview.PageIndex * kititemview.PageSize
        Dim partid As String = ds.Tables(0).Rows(page + e.NewEditIndex).Item(3).ToString()
        partid = Server.UrlEncode(partid)
        Dim kitid As String = ds.Tables(0).Rows(page + e.NewEditIndex).Item(0).ToString()
        kitid = Server.UrlEncode(kitid)
        Dim status As String = ds.Tables(0).Rows(page + e.NewEditIndex).Item(2).ToString
        status = Server.UrlEncode(status)
        Dim countryid As String = countryiddrpd.SelectedItem.Value.ToString()
        countryid = Server.UrlEncode(countryid)
       
        Dim strTempURL As String = "partid=" + partid + "&kitid=" + kitid + "&status=" + status + "&countryid=" + countryid
        strTempURL = "~/PresentationLayer/masterrecord/ModifyKitItem.aspx?" + strTempURL
        Response.Redirect(strTempURL)
    End Sub
#Region "check delete"
    Function check() As Integer
        Dim PartEntity As New ClsPartRequirement()
        If (Trim(partidtbox.Text) <> "") Then
            PartEntity.partid = partidtbox.Text.ToUpper()
        End If
        Dim sqldr As SqlDataReader = PartEntity.GetPartdel()
        Dim i As Integer = 0
        While (sqldr.Read())
            i = 1
        End While
        Return i
    End Function
#End Region

    Protected Sub Addkit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Addkit.Click
        Dim partid As String = partidtbox.Text.ToUpper().ToString
        partid = Server.UrlEncode(partid)
        Dim countryid As String = countryiddrpd.SelectedItem.Value.ToString()
        countryid = Server.UrlEncode(countryid)
        Dim type As String = "edit"
        type = Server.UrlEncode(type)
        Dim strTempURL As String = "partid=" + partid + "&countryid=" + countryid + "&type=" + type
        strTempURL = "~/PresentationLayer/masterrecord/AddKitItem.aspx?" + strTempURL
        Response.Redirect(strTempURL)

    End Sub

 
    Protected Sub JCalendar2_SelectedDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles JCalendar2.SelectedDateChanged
        retrievekitITEM()
        Dim script As String = "self.location='#cal';"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "cal", script, True)
    End Sub

    Protected Sub JCalendar1_SelectedDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles JCalendar1.SelectedDateChanged
        retrievekitITEM()
        Dim script As String = "self.location='#cal';"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "cal", script, True)
    End Sub

    Protected Sub MessageBox2_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox2.GetMessageBoxResponse
        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")

        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
            Dim PartEntity As New ClsPartRequirement()
            Dim calendatetime As New clsCommonClass()
            If (Trim(partidtbox.Text) <> "") Then
                PartEntity.partid = partidtbox.Text.ToUpper()
            End If
            If (Trim(partnametbox.Text) <> "") Then
                PartEntity.partname = partnametbox.Text.ToUpper()
            End If
            If (Trim(partanametbox.Text) <> "") Then
                PartEntity.altname = partanametbox.Text.ToUpper()
            End If
            If (Trim(countryiddrpd.SelectedValue().ToString()) <> "") Then
                PartEntity.Countryid = countryiddrpd.SelectedItem.Value.ToString()
            End If
            If (Trim(CategoryIDdrpd.SelectedValue().ToString()) <> "") Then
                PartEntity.catecode = CategoryIDdrpd.SelectedItem.Value.ToString()
            End If
            If (Trim(ModelIDdrpd.SelectedValue().ToString()) <> "") Then
                PartEntity.modelid = ModelIDdrpd.SelectedItem.Value.ToString()
            End If
            If (Trim(FiniProdudrpd.SelectedValue().ToString()) <> "") Then
                PartEntity.finipro = FiniProdudrpd.SelectedItem.Value.ToString()
            End If
            If (Trim(uomdrpd.SelectedValue().ToString()) <> "") Then
                PartEntity.uom = uomdrpd.SelectedItem.Value.ToString()
            End If
            If (Trim(Request.Form("EffectDatetbox")) <> "") Then
                'PartEntity.effectivedate = calendatetime.DatetoDatabase(EffectDatetbox.Text)
                Dim temparr As Array = Request.Form("EffectDatetbox").Split("/") 'EffectDatetbox.Text.Split("/")
                PartEntity.effectivedate = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
            End If
            If (Trim(Request.Form("ObsolDatetbox")) <> "") Then
                'PartEntity.obsoleteDate = calendatetime.DatetoDatabase(ObsolDatetbox.Text)
                Dim temparr As Array = Request.Form("ObsolDatetbox").Split("/") 'ObsolDatetbox.Text.Split("/")
                PartEntity.obsoleteDate = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
            End If
            If (Trim(suppnametbox.Text) <> "") Then
                PartEntity.suppliername = suppnametbox.Text.ToUpper()
            End If

            If (TradingItemdrpd.SelectedValue().ToString() <> "") Then
                PartEntity.traditem = TradingItemdrpd.SelectedItem.Value.ToString()
            End If
            If (commonpartdrpd.SelectedValue().ToString() <> "") Then
                PartEntity.commonpart = commonpartdrpd.SelectedItem.Value.ToString()
            End If
            If (ServiceItemdrpd.SelectedValue().ToString() <> "") Then
                PartEntity.serviceitem = ServiceItemdrpd.SelectedItem.Value.ToString()
            End If
            If (KitItemdrpd.SelectedValue().ToString() <> "") Then
                PartEntity.kititem = KitItemdrpd.SelectedItem.Value.ToString()
            End If
            If (Statusdrpd.SelectedValue().ToString() <> "") Then
                PartEntity.Status = Statusdrpd.SelectedItem.Value.ToString()
            End If
            If (Trim(remarkstbox.Text) <> "") Then
                PartEntity.Remarks = remarkstbox.Text.ToUpper()
            End If
            If (Trim(creatbytbox.Text) <> "") Then
                PartEntity.CreatedBy = Session("userID").ToString().ToUpper
            End If

            If (Trim(creatdatetbox.Text) <> "") Then
                PartEntity.CreatedDate = calendatetime.DatetoDatabase(creatdatetbox.Text)
            End If

            If (Trim(modifybytbox.Text) <> "") Then
                PartEntity.ModifiedBy = Session("userID").ToString().ToUpper
            End If

            If (Trim(modifydatetbox.Text) <> "") Then
                PartEntity.ModifiedDate = calendatetime.DatetoDatabase(modifydatetbox.Text)
            End If

            PartEntity.logsercenter = Session("login_svcID")
            PartEntity.ipadress = Request.UserHostAddress.ToString()

            Dim updCtryCnt As Integer = PartEntity.UpdatePartKit()
            If updCtryCnt = 0 Then
                errlab.Text = objXm.GetLabelName("StatusMessage", "BB-HintMsg-Suc")
                errlab.Visible = True
                retrievekitITEM()
            Else
                Response.Redirect("~/PresentationLayer/Error.aspx")
            End If

        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
            EffectDatetbox.Text = Request.Form("EffectDatetbox")
            ObsolDatetbox.Text = Request.Form("ObsolDatetbox")
        End If
    End Sub
End Class
