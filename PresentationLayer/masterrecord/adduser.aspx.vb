Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Partial Class PresentationLayer_masterrecord_adduser
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then

            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "12")
            Me.LinkButton1.Enabled = purviewArray(0)
            If purviewArray(0) = False Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return

            End If
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
            'display label message
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            lblCompTop.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            lblCompBottom.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            Me.UserIDLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASUSER-0001")
            Me.StaffStatusLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASUSER-0002")
            Me.CountryIDLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASUSER-0003")
            Me.UserNameLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASUSER-0004")
            Me.AlterNameLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASUSER-0005")
            Me.AccessProGroupLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASUSER-0006")
            Me.RankLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASUSER-0007")
            Me.FailedLogonLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASUSER-0008")
            Me.ChangePassWdLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASUSER-0009")
            Me.CompanIDLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASUSER-0010")
            Me.DepartmentLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASUSER-0011")
            Me.PermanentStaffLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASUSER-0012")
            Me.SvcIDLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASUSER-0013")
            Me.PassWdLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASUSER-0014")
            ConfirPasWdLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASUSER-0018")
            Me.EmailAdressLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASUSER-0015")
            creatby.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATBY")
            creatdate.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
            modfyby.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFBY")
            mdfydate.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFDATE")
            Me.titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASUSER-0017")
            LinkButton1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Save")
            HyperLink1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Cancel")
            AddLink.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Add")
            AddLink.NavigateUrl = Page.Request.RawUrl
            Me.ChangePassWdDDL.Text = "0"
            Me.UserIDBox.Focus()
            'create by create date 
            Dim userIDNamestr As String = Session("userID").ToString().ToUpper + "-" + Session("username").ToString.ToUpper


            creatbybox.Text = userIDNamestr
            creatdtbox.Text = Date.Now
            modfybybox.Text = userIDNamestr
            mdfydtbox.Text = Date.Now
            ' \\\\\\\\\\\\\\\\\\\\\\\\
            Dim statXmlTr As New clsXml
            statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            '\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
            'Me.RangeValidator1.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "MDWRD")
            Me.RFuserid.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "CHUSID")
            Me.RFusername.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "CHUSNM")
            Me.RFVCPSW.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "CHUCPSW")
            Me.RFVPSW.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "CHUPSW")
            Me.REVEmail.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "CHUEMAIL")
            Me.CompareValidator1.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "CHUPSWCPSW")
            Me.RegularExpressionValidator1.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "PSWLNTH")
            Me.RFVCTRY.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "USERCHCTRY")
            Me.RFVCOMP.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "USERCHCOM")
            Me.RFVSVC.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "USERCHSVC")
            Me.RequiredFieldValidator1.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "CHUSACCEG")
            '\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
            'create the dropdownlist of staff status
            Dim cntryStat As New clsrlconfirminf()
            Dim statParam As ArrayList = New ArrayList
            statParam = cntryStat.searchconfirminf("STAFFSTAT")
            Dim count As Integer
            Dim statid As String
            Dim statnm As String
            For count = 0 To statParam.Count - 1
                statid = statParam.Item(count)
                statnm = statParam.Item(count + 1)
                count = count + 1
                If Not statid.Equals("DELETE") Then
                    Me.StaffStatusDDL.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                End If
            Next
            'deptment dropdownlist bond\
            Dim detpParam As ArrayList = New ArrayList()
            detpParam = cntryStat.searchconfirminf("DEPT")
            Dim deptid As String
            Dim deptnm As String
            'For count = 1 To detpParam.Count
            '    deptid = detpParam.Item(count)
            '    'deptnm = detpParam.Item(count + 1)
            '    deptnm = detpParam.Item(count)
            '    count = count + 1
            '    If UCase(deptid) <> "ALL" Then
            '        Me.DepartmentDDL.Items.Add(New ListItem(deptnm.ToString(), deptid.ToString()))
            '    End If
            'Next
            For count = 0 To detpParam.Count - 1
                deptid = detpParam.Item(count)
                deptnm = detpParam.Item(count + 1)
                count = count + 1
                Me.DepartmentDDL.Items.Add(New ListItem(deptnm.ToString(), deptid.ToString()))
            Next

            'Change passwd and permenent staff  list bond  2 item y/n
            Dim pcdtclass As ArrayList = New ArrayList
            pcdtclass = cntryStat.searchconfirminf("YESNO")
            Dim pcdtclsID As String
            Dim pcdtclsName As String
            Me.PermanentStaffDDL.Items.Clear()
            Me.ChangePassWdDDL.Items.Clear()
            For count = 0 To pcdtclass.Count - 1
                pcdtclsID = pcdtclass.Item(count)
                pcdtclsName = pcdtclass.Item(count + 1)
                count = count + 1
                Me.PermanentStaffDDL.Items.Add(New ListItem(pcdtclsName.ToString(), pcdtclsID.ToString()))
                Me.ChangePassWdDDL.Items.Add(New ListItem(pcdtclsName.ToString(), pcdtclsID.ToString()))
            Next
            'Rank ddl bond 
            Dim rankcls As ArrayList = New ArrayList
            rankcls = cntryStat.searchconfirminf("RANK")
            Dim rankid As String
            Dim rankname As String
            Me.RankDDL.Items.Clear()
            Dim login_rank As Integer = Session("login_rank")
            Dim startint As Integer
            If login_rank <> 0 Then
                Select Case (login_rank)
                    Case 9
                        startint = 4
                    Case 8
                        startint = 2

                    Case 7
                        startint = 0
                End Select
            Else
            End If
            For count = startint To rankcls.Count - 1
                rankid = rankcls.Item(count)
                rankname = rankcls.Item(count + 1)
                count = count + 1
                Me.RankDDL.Items.Add(New ListItem(rankname.ToString(), rankid.ToString()))
            Next
            'bonds country  id  name
            Dim clscom As New clsCommonClass()
            Dim idnameds As New DataSet()
            Dim rank As Integer = Session("login_rank")
            clscom.rank = rank
            If rank <> 0 Then
                clscom.spctr = Session("login_ctryID")
                clscom.spstat = Session("login_cmpID")
                clscom.sparea = Session("login_svcID")
            End If
            idnameds = clscom.Getcomidname("BB_MASCTRY_IDNAME")
            databonds(idnameds, Me.CountryIDDDL)
            Me.CountryIDDDL.Items.Insert(0, "")
            CountryIDDDL.SelectedValue = Session("login_ctryID")

            'bonds svcid name
            '///////////////////////////////////////// 
            Dim clscomds As New DataSet
            clscom.rank = rank
            If rank <> 0 Then
                clscom.spctr = Session("login_ctryID")
                clscom.spstat = Session("login_cmpID")
                clscom.sparea = Session("login_svcID")
            End If
            clscomds = clscom.Getcomidname("BB_MASSVRC_IDNAME")
            If clscomds.Tables.Count <> 0 Then
                databonds(clscomds, Me.SvcIDDDL)
            End If
            Me.SvcIDDDL.Items.Insert(0, "")
            SvcIDDDL.SelectedValue = Session("login_svcID")

            'bonds company id name\ 
            Dim compidnameds As New DataSet()
            compidnameds = clscom.Getcomidname("BB_MASCOMP_IDNAME")
            databonds(compidnameds, Me.CompanIDDDL)
            Me.CompanIDDDL.Items.Insert(0, "")
            CompanIDDDL.SelectedValue = Session("login_cmpID")


            'bonds access profile group \           
            Dim accessprofidnameds As New DataSet()
            accessprofidnameds = clscom.Getidname("BB_MASMUA_SELBYIDNM")
            databonds(accessprofidnameds, Me.AccessProGroupDDL)
            Me.AccessProGroupDDL.Items.Insert(0, "")
            Me.FailedLogonBox.Text = 0
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        End If
    End Sub

#Region " command bond function "
    Public Function databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        Dim row As DataRow
        If (ds.Tables.Count > 0) Then
            For Each row In ds.Tables(0).Rows
                Dim NewItem As New ListItem()
                NewItem.Text = Trim(row(0).ToString()) & "-" & Trim(row(1).ToString())
                NewItem.Value = row(0)
                dropdown.Items.Add(NewItem)
            Next
        End If
    End Function
#End Region

#Region "insert record"
    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton1.Click
        If (Trim(Me.PassWdBox.Text) <> "") Then
            Session("user_psw") = FormsAuthentication.HashPasswordForStoringInConfigFile(Me.PassWdBox.Text, "md5")
        End If
        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim msginfo As String = objXm.GetLabelName("StatusMessage", "SAVEORNOT")
        Dim msgtitle As String = objXm.GetLabelName("StatusMessage", "SAVETITLE")
        MessageBox1.Font.Name = "Verdana"
        MessageBox1.Confirm(msginfo, msgtitle)

    End Sub
#End Region

#Region "add user"
    Public Sub adduser()
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim userEntity As New clsUser()
        If (Trim(Me.CountryIDDDL.Text) <> "") Then
            userEntity.CountryID = Me.CountryIDDDL.SelectedValue
        End If
        If (Trim(Me.RankDDL.Text) <> "") Then
            userEntity.Rank = RankDDL.SelectedValue
        End If
        If (Trim(Me.PermanentStaffDDL.Text) <> "") Then
            userEntity.PenentStuffID = PermanentStaffDDL.SelectedValue
        End If
        If (Trim(Me.SvcIDDDL.Text) <> "") Then
            userEntity.SVCID = SvcIDDDL.SelectedValue
        End If
        If (Trim(Me.AccessProGroupDDL.Text) <> "") Then
            userEntity.AccessProfileGrop = AccessProGroupDDL.SelectedValue
        End If
        If (Trim(Me.ChangePassWdDDL.Text) <> "") Then
            userEntity.ChangeWd = ChangePassWdDDL.SelectedValue
        End If
        If (Trim(Me.CompanIDDDL.Text) <> "") Then
            userEntity.CompID = CompanIDDDL.SelectedValue
        End If
        If (Trim(Me.DepartmentDDL.Text) <> "") Then
            userEntity.DepmentID = DepartmentDDL.SelectedValue
        End If
        If (Trim(Me.StaffStatusDDL.Text) <> "") Then
            userEntity.StaffStatus = StaffStatusDDL.SelectedValue
        End If
        If (Trim(Me.UserIDBox.Text) <> "") Then
            userEntity.UserID = UserIDBox.Text.ToUpper()
        End If
        If (Trim(Me.UserNameBox.Text) <> "") Then
            userEntity.UserName = Me.UserNameBox.Text.ToUpper()
        End If
        If (Trim(Me.AlterNameBox.Text) <> "") Then
            userEntity.UserAlternateName = AlterNameBox.Text.ToUpper()
        End If
        'If (Trim(Me.PassWdBox.Text) <> "") Then
        '    userEntity.PassWord = PassWdBox.Text
        'End If
        If (Trim(Me.EmailAdressBox.Text) <> "") Then
            userEntity.EmailAddrss = EmailAdressBox.Text
        End If
        If (Trim(Me.creatbybox.Text) <> "") Then
            userEntity.Createdby = Session("userID").ToString.ToUpper()
        End If
        If (Trim(Me.modfybybox.Text) <> "") Then
            userEntity.Modifyby = Session("userID").ToString.ToUpper()
        End If
        userEntity.PassWord = Session("user_psw").ToString()
        Dim CHID As Integer = checkname()
        Dim CHNM As Integer = checkid()
        If CHNM = 1 Then
            Me.addinfo.Text = objXmlTr.GetLabelName("StatusMessage", "DUPNAMEID")
            addinfo.Visible = True
        ElseIf CHID = 1 Then
            Me.addinfo.Text = objXmlTr.GetLabelName("StatusMessage", "DUPUSERNM")
            addinfo.Visible = True
        Else
            Dim insJobCnt As Integer = userEntity.Insert()
            If insJobCnt = 0 Then
                addinfo.Text = objXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Suc")
                addinfo.Visible = True
            Else
                Response.Redirect("~/PresentationLayer/Error.aspx")
            End If
        End If
        'End If
    End Sub
#End Region

#Region "check  if there are same USER  name "
    Public Function checkname() As Integer
        Dim userEntity As New clsUser()
        If (Trim(Me.UserNameBox.Text) <> "") Then
            userEntity.UserName = Me.UserNameBox.Text.ToUpper()
        End If
        Dim chckname As Integer = 0
        Dim sqldr As SqlDataReader = userEntity.UserCheckName()
        If (sqldr.Read()) Then
            chckname = 1
        End If
        Return chckname
    End Function
#End Region

#Region "check  if there are same user id "
    Public Function checkid() As Integer
        Dim userEntity As New clsUser()
        If (Trim(Me.UserIDBox.Text) <> "") Then
            userEntity.UserID = UserIDBox.Text.ToUpper()
        End If
        Dim chkid As Integer = 0
        Dim sqldr As SqlDataReader = userEntity.UserCheckID()
        If (sqldr.Read()) Then
            chkid = 1
        End If
        Return chkid
    End Function

#End Region

    Protected Sub MessageBox1_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox1.GetMessageBoxResponse
        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
            adduser()
        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
        End If
    End Sub

    Protected Sub CountryIDDDL_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CountryIDDDL.SelectedIndexChanged
        'bonds company id name\ 

        If Me.CountryIDDDL.Text <> "" Then
            Dim rank As String = Session("login_rank")
            Dim clscom As New clsCommonClass()
            Dim compidnameds As New DataSet()
            If rank <> 0 Then
                clscom.spctr = Session("login_ctryID")
                clscom.spstat = Session("login_cmpID")
                clscom.sparea = Session("login_svcID")
            Else
                If (Me.CountryIDDDL.Text <> "") Then
                    clscom.spctr = Me.CountryIDDDL.SelectedValue().ToString().ToUpper()
                End If
            End If
            clscom.rank = rank
            compidnameds = clscom.Getcomidname("BB_MASCOMP_IDNAME")
            databonds(compidnameds, Me.CompanIDDDL)
            Me.CompanIDDDL.Items.Insert(0, "")
        Else

            Me.CompanIDDDL.Items.Clear()
            Me.SvcIDDDL.Items.Clear()

        End If


    End Sub

    Protected Sub CompanIDDDL_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CompanIDDDL.SelectedIndexChanged

        If Me.CompanIDDDL.Text <> "" Then
            Dim rank As String = Session("login_rank")
            Dim clscom As New clsCommonClass()
            Dim compidnameds As New DataSet()
            'If rank <> 0 Then
            '    clscom.spctr = Session("login_ctryID")
            '    clscom.spstat = Session("login_cmpID")
            '    clscom.sparea = Session("login_svcID")
            'Else
            If (Me.CountryIDDDL.Text <> "") Then
                clscom.spctr = Me.CountryIDDDL.SelectedValue().ToString().ToUpper()
            End If
            If (Me.CompanIDDDL.Text <> "") Then
                clscom.spstat = Me.CompanIDDDL.SelectedValue().ToUpper.ToUpper
            End If

            clscom.rank = rank
            compidnameds = clscom.Getcomidname("BB_MASSVRC_IDNAME")
            databonds(compidnameds, Me.SvcIDDDL)
            Me.SvcIDDDL.Items.Insert(0, "")
        Else
            Me.SvcIDDDL.Items.Clear()
        End If
    End Sub

    Protected Sub SvcIDDDL_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles SvcIDDDL.SelectedIndexChanged

    End Sub
End Class
