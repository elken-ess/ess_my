Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data

Partial Class PresentationLayer_masterrecord_SalTaxID
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Page.IsPostBack) Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try
            BindGrid()
        End If
    End Sub
    Protected Sub BindGrid()
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        SalTaxIDlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTAX-0001")
        SalTaxNamelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTAX-0002")
        Statuslab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Status")
        LinkButton1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Add")
        LinkButton2.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Search")
        titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTAX-0010")
        Me.Label1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-NR")
        Me.Label1.Visible = False
        Me.addinfo.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-AR")
        Me.addinfo.Visible = False


        Me.SalTaxIDBox.Focus()
        'create the dropdownlist
        Dim saltaxStat As New clsrlconfirminf()
        Dim statParam As ArrayList = New ArrayList
        statParam = saltaxStat.searchconfirminf("STATUS")
        Dim count As Integer
        Dim statid As String
        Dim statnm As String
        For count = 0 To statParam.Count - 1
            statid = statParam.Item(count)
            statnm = statParam.Item(count + 1)
            count = count + 1
            If (statid.Equals("ACTIVE") Or statid.Equals("OBSOLETE")) Then
                Me.StatusDrop.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
            End If
        Next
        StatusDrop.Items(0).Selected = True

        Dim SalTaxIDEntity As New clsSalTaxID()
        Dim rank As String = Session("login_rank").ToString().ToUpper()
        If rank <> 0 Then
            SalTaxIDEntity.CountryID = Session("login_ctryID").ToString().ToUpper()
        End If
        SalTaxIDEntity.rank = rank
        If (StatusDrop.SelectedValue().ToString() <> "") Then
            SalTaxIDEntity.SalTaxStatus = StatusDrop.SelectedItem.Value.ToString()
        End If
        Dim SalTaxidall As DataSet = SalTaxIDEntity.GetSalTaxID()
        SalTaxIDView.DataSource = SalTaxidall
        Session("SalTaxIDView") = SalTaxidall
        checknorecord(SalTaxidall)
        SalTaxIDView.AllowPaging = True
        SalTaxIDView.AllowSorting = True

        Dim editcol As New CommandField
        editcol.EditText = objXmlTr.GetLabelName("EngLabelMsg", "BB-Edit") '"edit"
        editcol.HeaderImageUrl = "~/PresentationLayer/graph/edit.gif"
        editcol.ShowEditButton = True
        SalTaxIDView.Columns.Add(editcol)

        '''access control'''''''''''''''''''''''''''''''''''''''''''''''
        Dim accessgroup As String = Session("accessgroup").ToString
        Dim purviewArray As Array = New Boolean() {False, False, False, False, False}
        purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "18")
        Me.LinkButton1.Enabled = purviewArray(0)
        editcol.Visible = purviewArray(2)


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


        SalTaxIDView.DataBind()
        If SalTaxidall.Tables(0).Rows.Count <> 0 Then
            SalTaxIDView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTAX-0013")
            SalTaxIDView.HeaderRow.Cells(1).Font.Size = 8
            SalTaxIDView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTAX-0001")
            SalTaxIDView.HeaderRow.Cells(2).Font.Size = 8
            SalTaxIDView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTAX-0002")
            SalTaxIDView.HeaderRow.Cells(3).Font.Size = 8
            SalTaxIDView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Status")
            SalTaxIDView.HeaderRow.Cells(4).Font.Size = 8
            SalTaxIDView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTAX-0004")
            SalTaxIDView.HeaderRow.Cells(5).Font.Size = 8
           

        End If

    End Sub
#Region " id bond"
    Public Function databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        Dim row As DataRow
        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            NewItem.Text = row("id") & "-" & row("name")
            NewItem.Value = row("id")
            dropdown.Items.Add(NewItem)
        Next

    End Function
#End Region
    Protected Sub LinkButton2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton2.Click
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        SalTaxIDView.PageIndex = 0
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        Dim SalTaxIDEntity As New clsSalTaxID()
        Dim rank As String = Session("login_rank").ToString().ToUpper()
        If rank <> 0 Then
            SalTaxIDEntity.CountryID = Session("login_ctryID").ToString().ToUpper()
        End If
        SalTaxIDEntity.rank = rank
        If (Trim(SalTaxIDBox.Text) <> "") Then
            SalTaxIDEntity.SalTaxID = SalTaxIDBox.Text
        End If
        If (Trim(SalTaxNameBox.Text) <> "") Then
            SalTaxIDEntity.SalTaxName = SalTaxNameBox.Text
        End If
        If (StatusDrop.SelectedValue().ToString() <> "") Then
            SalTaxIDEntity.SalTaxStatus = StatusDrop.SelectedItem.Value.ToString()
        End If
        checkinfo()
        SalTaxIDEntity.ModifiedBy = Session("username")
        Dim selDS As DataSet = SalTaxIDEntity.GetSalTaxID()
        SalTaxIDView.DataSource = selDS
        SalTaxIDView.DataBind()
        Session("SalTaxIDView") = selDS
        checknorecord(selDS)
        If selDS.Tables(0).Rows.Count <> 0 Then
            SalTaxIDView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTAX-0013")
            SalTaxIDView.HeaderRow.Cells(1).Font.Size = 8
            SalTaxIDView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTAX-0001")
            SalTaxIDView.HeaderRow.Cells(2).Font.Size = 8
            SalTaxIDView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTAX-0002")
            SalTaxIDView.HeaderRow.Cells(3).Font.Size = 8
            SalTaxIDView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Status")
            SalTaxIDView.HeaderRow.Cells(4).Font.Size = 8
            SalTaxIDView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTAX-0004")
            SalTaxIDView.HeaderRow.Cells(5).Font.Size = 8

        End If

    End Sub



    Protected Sub SalTaxIDView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles SalTaxIDView.PageIndexChanging
        SalTaxIDView.PageIndex = e.NewPageIndex
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Dim SalTaxIDEntity As New clsSalTaxID()
        Dim rank As String = Session("login_rank").ToString().ToUpper()
        If rank <> 0 Then
            SalTaxIDEntity.CountryID = Session("login_ctryID").ToString().ToUpper()
        End If
        SalTaxIDEntity.rank = rank
        If (Trim(SalTaxIDBox.Text) <> "") Then
            SalTaxIDEntity.SalTaxID = SalTaxIDBox.Text
        End If
        If (Trim(SalTaxNameBox.Text) <> "") Then
            SalTaxIDEntity.SalTaxName = SalTaxNameBox.Text
        End If
        If (StatusDrop.SelectedValue().ToString() <> "") Then
            SalTaxIDEntity.SalTaxStatus = StatusDrop.SelectedItem.Value.ToString()
        End If


        Dim selDS As DataSet = SalTaxIDEntity.GetSalTaxID()
        SalTaxIDView.DataSource = selDS
        SalTaxIDView.DataBind()
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        If selDS.Tables(0).Rows.Count <> 0 Then
            SalTaxIDView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTAX-0013")
            SalTaxIDView.HeaderRow.Cells(1).Font.Size = 8
            SalTaxIDView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTAX-0001")
            SalTaxIDView.HeaderRow.Cells(2).Font.Size = 8
            SalTaxIDView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTAX-0002")
            SalTaxIDView.HeaderRow.Cells(3).Font.Size = 8
            SalTaxIDView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Status")
            SalTaxIDView.HeaderRow.Cells(4).Font.Size = 8
            SalTaxIDView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTAX-0004")
            SalTaxIDView.HeaderRow.Cells(5).Font.Size = 8
        End If
    End Sub

    Protected Sub SalTaxIDView_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles SalTaxIDView.RowEditing
        Dim ds As DataSet = Session("SalTaxIDView")
        Dim page As Integer = SalTaxIDView.PageIndex * SalTaxIDView.PageSize
        Dim salsetid As String = Me.SalTaxIDView.Rows(e.NewEditIndex).Cells(1).Text
        salsetid = Server.UrlEncode(salsetid)
        'Dim statusid As String = ds.Tables(0).Rows(page + e.NewEditIndex).Item(3).ToString
        'statusid = Server.UrlEncode(statusid)
        Dim strTempURL As String = "SalTaxsetID=" + salsetid
        '+ "&statusid=" + statusid
        strTempURL = "~/PresentationLayer/masterrecord/modifySalTaxID.aspx?" + strTempURL
        Response.Redirect(strTempURL)
    End Sub
#Region "if have no record "
    Public Function checknorecord(ByVal ds As DataSet) As Integer
        If ds.HasErrors Then
            If ds.Tables(0).Rows.Count = 0 Then
                Me.Label1.Visible = True
                ' Me.addinfo.Visible = False
            Else
                Me.Label1.Visible = False
            End If

        End If
        If ds.Tables(0).Rows.Count = 0 Then
            Me.Label1.Visible = True
            ' Me.addinfo.Visible = False
        Else
            Me.Label1.Visible = False
        End If

    End Function
#End Region
#Region "��ʾ��Ϣ"
    Public Function checkinfo() As Integer
        If Me.SalTaxIDBox.Text = "" And Me.SalTaxNameBox.Text = "" Then
            Me.addinfo.Visible = True
        Else
            Me.addinfo.Visible = False
        End If
    End Function
#End Region
End Class
