Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Partial Class PresentationLayer_masterrecord_AddPartRequire
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        If Not Page.IsPostBack Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Response.Redirect("~/PresentationLayer/logon.aspx")
                End If
            Catch ex As Exception
                Response.Redirect("~/PresentationLayer/logon.aspx")
            End Try
            partidtbox.Focus()
            Dim coid As String
            Dim paid As String
            'paid = Request.Params("partid").ToString()
            'coid = Request.Params("countryid").ToString()

            paid = Request.QueryString("partid")
            coid = Request.QueryString("countryid")

            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "06")
            Me.saveButton.Enabled = purviewArray(0)
            If purviewArray(0) = False Then
                Response.Redirect("~/PresentationLayer/logon.aspx")
            End If

            'Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
            'System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
            '给界面上LABEL的赋值
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            partidlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCAT-0001")
            partnameLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCAT-0002")
            partanamelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCAT-0003")
            countryidlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCAT-0004")
            CategoryIDLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCAT-0005")
            ModelIDlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCAT-0006")
            FiniProdulab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCAT-0007")
            uomlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCAT-0008")
            EffectDatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCAT-0009")
            ObsolDatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCAT-0010")
            suppnamelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCAT-0011")
            TradingItemlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCAT-0012")
            commonpartlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCAT-0013")
            ServiceItemlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCAT-0014")
            KitItemlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCAT-0015")
            Statuslab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCAT-0016")
            remarkslab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCAT-0017")
            createbylab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATBY")
            creatdatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
            modifybylab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFBY")
            modifydatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFDATE")

            Me.lblCompTop.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            Me.lblCompBottom.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")

            saveButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Save")
            HyperLink1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Cancel")
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Addparttl")
            Addkit.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-AddKitBT")
            AddLink.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Add")
            AddLink.NavigateUrl = Page.Request.RawUrl
            'SET MESSAGE FOR ERRORS
            Dim XmlTr As New clsXml
            XmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")

            partiderr.ErrorMessage = XmlTr.GetLabelName("StatusMessage", "BB-HintMsg-PARTID")
            countryiderr.ErrorMessage = XmlTr.GetLabelName("StatusMessage", "BB-HintMsg-COUNTRYID")
            partnameerr.ErrorMessage = XmlTr.GetLabelName("StatusMessage", "BB-HintMsg-PARTNAMEERR")
            efferr.ErrorMessage = XmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Effecdate")
            obserr.ErrorMessage = XmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Obsedate")
            partcateerr.ErrorMessage = XmlTr.GetLabelName("StatusMessage", "BB-HintMsg-partcategoryerr")
            modelerr.ErrorMessage = XmlTr.GetLabelName("StatusMessage", "BB-HintMsg-MID")
            'set value for createbydatetbox and modifybydatetbox
            Dim userIDNamestr As String = Session("userID").ToString().ToUpper + "-" + Session("username").ToString.ToUpper
            creatbytbox.Text = userIDNamestr
            creatdatetbox.Text = Date.Now()
            modifybytbox.Text = userIDNamestr
            modifydatetbox.Text = Date.Now()

            'create the status dropdownlist
            Dim cntryStat As New clsrlconfirminf()
            Dim statParam As ArrayList = New ArrayList
            statParam = cntryStat.searchconfirminf("STATUS")
            Dim count As Integer
            Dim statid As String
            Dim statnm As String
            For count = 0 To statParam.Count - 1
                statid = statParam.Item(count)
                statnm = statParam.Item(count + 1)
                count = count + 1
                If statid.Equals("ACTIVE") Or statid.Equals("OBSOLETE") Then
                    Statusdrpd.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                End If
            Next
            Statusdrpd.Items(0).Selected = True

            'create the Finished Product\Trading Item\Common Part\ Service Itemand \Kit Item dropdownlist
            Dim yesnopar As ArrayList = New ArrayList
            yesnopar = cntryStat.searchconfirminf("YESNO")
            count = 0
            For count = 0 To yesnopar.Count - 1
                statid = yesnopar.Item(count)
                statnm = yesnopar.Item(count + 1)
                count = count + 1
                FiniProdudrpd.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                TradingItemdrpd.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                commonpartdrpd.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                ServiceItemdrpd.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                KitItemdrpd.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
            Next

            commonpartdrpd.Items(1).Selected = True
            ServiceItemdrpd.Items(1).Selected = True
            KitItemdrpd.Items(1).Selected = True
            'SET KIT ITEM MESSAGE 
            If KitItemdrpd.SelectedItem.Value.ToString() = "Y" Then
                Dim kitEntity As New ClsPartRequirement()
                kitEntity.partid = Me.partidtbox.Text
                kitEntity.Countryid = Session("login_ctryID")
                Dim ctryall As DataSet = kitEntity.GetKititem()
                kititemview.DataSource = ctryall
                kititemview.DataBind()
            End If

            'create the uom dropdownlist
            Dim techtypepar As ArrayList = New ArrayList
            techtypepar = cntryStat.searchconfirminf("UOM")
            count = 0
            For count = 0 To techtypepar.Count - 1
                statid = techtypepar.Item(count)
                statnm = techtypepar.Item(count + 1)
                count = count + 1
                uomdrpd.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
            Next
            uomdrpd.Items(0).Selected = True

            ''bond country id and name
            Dim partbond As New clsCommonClass
            Dim rank As String = Session("login_rank")
            partbond.rank = rank
            If rank <> 0 Then
                partbond.spctr = Session("login_ctryID")
                partbond.spstat = Session("login_cmpID")
                partbond.sparea = Session("login_svcID")
            End If
            Dim serviceds As New DataSet
            serviceds = partbond.Getcomidname("BB_MASCTRY_IDNAME")
            If serviceds.Tables.Count <> 0 Then
                databonds(serviceds, Me.countryiddrpd)
            End If
            countryiddrpd.SelectedValue = Session("login_ctryID")


            'bond parent/model id and name
            Dim centerds As New DataSet
            centerds = partbond.Getcomidname("BB_MASMOTY_IDNAME")
            If centerds.Tables.Count <> 0 Then
                databonds(centerds, ModelIDdrpd)
            End If
            'bond  catage id and name
            Dim catege As New DataSet
            catege = partbond.Getcomidname("BB_MASPCAT_IDNAME")
            If catege.Tables.Count <> 0 Then
                databonds(catege, CategoryIDdrpd)
            End If

            'return from addkititem,set value
            If Trim(paid).Length > 0 Then
                Dim partEntity As New ClsPartRequirement()
                Dim CLANER As New clsCommonClass
                partEntity.partid = paid
                partEntity.Countryid = coid
                Dim edtReader As SqlDataReader = partEntity.GetpartDetailsByID()
                If edtReader.Read() Then
                    partidtbox.Text = edtReader.GetValue(0).ToString() 'part id
                    partidtbox.ReadOnly = True
                    partnametbox.Text = edtReader.GetValue(1).ToString() 'part name
                    partanametbox.Text = edtReader.GetValue(2).ToString() 'altername
                    countryiddrpd.Text = edtReader.GetValue(3).ToString() 'country id
                    countryiddrpd.Enabled = False
                    CategoryIDdrpd.Text = edtReader.GetValue(4).ToString() 'category id
                    ModelIDdrpd.Text = edtReader.GetValue(5).ToString() 'parent/model id
                    FiniProdudrpd.Text = edtReader.GetValue(6).ToString()
                    uomdrpd.Text = edtReader.GetValue(7).ToString()
                    EffectDatetbox.Text = edtReader.GetValue(8).ToString().Substring(0, 10)
                    ObsolDatetbox.Text = edtReader.GetValue(9).ToString().Substring(0, 10)
                    suppnametbox.Text = edtReader.GetValue(10).ToString()
                    TradingItemdrpd.Text = edtReader.GetValue(11).ToString()
                    commonpartdrpd.Text = edtReader.GetValue(12).ToString()
                    ServiceItemdrpd.Text = edtReader.GetValue(13).ToString()
                    KitItemdrpd.Text = edtReader.GetValue(14).ToString()
                    Statusdrpd.Text = edtReader.GetValue(15).ToString()
                    remarkstbox.Text = edtReader.GetValue(16).ToString() 'remarks               
                    creatbytbox.Text = edtReader.GetValue(17).ToString()
                    creatdatetbox.Text = edtReader.GetValue(18).ToString()
                    modifybytbox.Text = edtReader.GetValue(19).ToString()
                    modifydatetbox.Text = edtReader.GetValue(20).ToString()

                End If
                retrievekit()
            End If
            HypCal.NavigateUrl = "javascript:DoCal(document.AddPartRequire.EffectDatetbox);"
            HypCal2.NavigateUrl = "javascript:DoCal(document.AddPartRequire.ObsolDatetbox);"

        End If

    End Sub
#Region " bonds"
    Public Function databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        dropdown.Items.Add(" ")
        Dim row As DataRow
        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            NewItem.Text = row("id") & "-" & row("name")
            NewItem.Value = row("id")
            dropdown.Items.Add(NewItem)
        Next
    End Function
#End Region

    Protected Sub saveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveButton.Click
        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")

        Dim temparr As Array = Request.Form("EffectDatetbox").Split("/") 'EffectDatetbox.Text.Split("/")
        Dim streff As String
        Dim strobs As String
        streff = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
        temparr = Request.Form("ObsolDatetbox").Split("/") 'ObsolDatetbox.Text.Split("/")
        strobs = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
        If Convert.ToDateTime(strobs) < Convert.ToDateTime(streff) Then
            errlab.Text = objXm.GetLabelName("StatusMessage", "BB-HintMsg-COMPAREEFFOBDATE")
            errlab.Visible = True
            Return
        End If
        EffectDatetbox.Text = Request.Form("EffectDatetbox")
        ObsolDatetbox.Text = Request.Form("ObsolDatetbox")
        Dim msginfo As String = objXm.GetLabelName("StatusMessage", "SAVEORNOT")
        Dim msgtitle As String = objXm.GetLabelName("StatusMessage", "SAVETITLE")
        MessageBox1.Confirm(msginfo)



    End Sub


    'Protected Sub KitItemdrpd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles KitItemdrpd.SelectedIndexChanged
    '    retrievekit()
    'End Sub

    'Protected Sub partidtbox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles partidtbox.TextChanged
    '    retrievekit()
    'End Sub

    Protected Sub kititemview_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles kititemview.PageIndexChanging
        kititemview.PageIndex = e.NewPageIndex
        Dim kitEntity As New ClsPartRequirement()
        kitEntity.partid = partidtbox.Text.ToUpper()
        kitEntity.Countryid = Session("login_ctryID")

        Dim ctryall As DataSet = kitEntity.GetKititem()
        kititemview.DataSource = ctryall
        kititemview.Visible = True
        kititemview.DataBind()
        Addkit.Visible = True
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        If ctryall.Tables(0).Rows.Count <> 0 Then
            kititemview.HeaderRow.Cells(0).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-KITITEMHD0")
            kititemview.HeaderRow.Cells(0).Font.Size = 8
            kititemview.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-KITITEMHD1")
            kititemview.HeaderRow.Cells(1).Font.Size = 8
            kititemview.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-KITITEMHD2")
            kititemview.HeaderRow.Cells(2).Font.Size = 8
            kititemview.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCAT-0001")
            kititemview.HeaderRow.Cells(3).Font.Size = 8
        End If

        EffectDatetbox.Text = Request.Form("EffectDatetbox")
        ObsolDatetbox.Text = Request.Form("ObsolDatetbox")
    End Sub

    Protected Sub Addkit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Addkit.Click
        'Dim url As String = "~/PresentationLayer/masterrecord/AddKitItem.aspx?"
        'url &= "partid=" & Trim(Me.partidtbox.Text.ToUpper().ToString) & "&"
        'url &= "countryid=" & Trim(Me.countryiddrpd.SelectedItem.Value.ToString())
        'Response.Redirect(url)
        Dim partid As String = partidtbox.Text.ToUpper().ToString
        partid = Server.UrlEncode(partid)
        Dim countryid As String = countryiddrpd.SelectedItem.Value.ToString()
        countryid = Server.UrlEncode(countryid)
        Dim type As String = "add"
        type = Server.UrlEncode(type)
        Dim strTempURL As String = "partid=" + partid + "&countryid=" + countryid + "&type=" + type
        strTempURL = "~/PresentationLayer/masterrecord/AddKitItem.aspx?" + strTempURL
        Response.Redirect(strTempURL)



    End Sub
#Region " retrieve kititem message"
    Public Function retrievekit()
        If KitItemdrpd.SelectedItem.Value.ToString() = "Y" Then
            Dim kitEntity As New ClsPartRequirement()
            kitEntity.partid = partidtbox.Text.ToUpper()
            kitEntity.Countryid = Session("login_ctryID")
            Dim ctryall As DataSet = kitEntity.GetKititem()
            kititemview.DataSource = ctryall
            kititemview.DataBind()
            kititemview.Visible = True
            Addkit.Visible = True
            Addkit.Enabled = False
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            If ctryall.Tables(0).Rows.Count <> 0 Then
                kititemview.HeaderRow.Cells(0).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-KITITEMHD0")
                kititemview.HeaderRow.Cells(0).Font.Size = 8
                kititemview.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-KITITEMHD1")
                kititemview.HeaderRow.Cells(1).Font.Size = 8
                kititemview.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-KITITEMHD2")
                kititemview.HeaderRow.Cells(2).Font.Size = 8
                kititemview.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCAT-0001")
                kititemview.HeaderRow.Cells(3).Font.Size = 8
            End If
        Else
            kititemview.Visible = False
            Addkit.Visible = False
            Addkit.Enabled = False
        End If
    End Function
#End Region

    Protected Sub MessageBox1_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox1.GetMessageBoxResponse
        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
            Dim PartEntity As New ClsPartRequirement()
            Dim calendatetime As New clsCommonClass()
            If (Trim(partidtbox.Text) <> "") Then
                PartEntity.partid = partidtbox.Text.ToUpper()

            End If
            If (Trim(partnametbox.Text) <> "") Then
                PartEntity.partname = partnametbox.Text.ToUpper()
            End If
            If (Trim(partanametbox.Text) <> "") Then
                PartEntity.altname = partanametbox.Text.ToUpper()
            End If
            If (Trim(countryiddrpd.SelectedValue().ToString()) <> "") Then
                PartEntity.Countryid = countryiddrpd.SelectedItem.Value.ToString()
            End If
            If (Trim(CategoryIDdrpd.SelectedValue().ToString()) <> "") Then
                PartEntity.catecode = CategoryIDdrpd.SelectedItem.Value.ToString()
            End If
            If (Trim(ModelIDdrpd.SelectedValue().ToString()) <> "") Then
                PartEntity.modelid = ModelIDdrpd.SelectedItem.Value.ToString()
            End If
            If (Trim(FiniProdudrpd.SelectedValue().ToString()) <> "") Then
                PartEntity.finipro = FiniProdudrpd.SelectedItem.Value.ToString()
            End If
            If (Trim(uomdrpd.SelectedValue().ToString()) <> "") Then
                PartEntity.uom = uomdrpd.SelectedItem.Value.ToString()
            End If
            If (Trim(Request.Form("EffectDatetbox")) <> "") Then
                'PartEntity.effectivedate = calendatetime.DatetoDatabase(EffectDatetbox.Text)
                Dim temparr As Array = Request.Form("EffectDatetbox").Split("/") 'EffectDatetbox.Text.Split("/")
                PartEntity.effectivedate = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
            End If
            If (Trim(Request.Form("ObsolDatetbox")) <> "") Then
                'PartEntity.obsoleteDate = calendatetime.DatetoDatabase(ObsolDatetbox.Text)
                Dim temparr As Array = Request.Form("ObsolDatetbox").Split("/") 'ObsolDatetbox.Text.Split("/")
                PartEntity.obsoleteDate = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
            End If
            If (Trim(suppnametbox.Text) <> "") Then
                PartEntity.suppliername = suppnametbox.Text.ToUpper()
            End If

            If (TradingItemdrpd.SelectedValue().ToString() <> "") Then
                PartEntity.traditem = TradingItemdrpd.SelectedItem.Value.ToString()
            End If
            If (commonpartdrpd.SelectedValue().ToString() <> "") Then
                PartEntity.commonpart = commonpartdrpd.SelectedItem.Value.ToString()
            End If
            If (ServiceItemdrpd.SelectedValue().ToString() <> "") Then
                PartEntity.serviceitem = ServiceItemdrpd.SelectedItem.Value.ToString()
            End If
            If (KitItemdrpd.SelectedValue().ToString() <> "") Then
                PartEntity.kititem = KitItemdrpd.SelectedItem.Value.ToString()
            End If
            If (Statusdrpd.SelectedValue().ToString() <> "") Then
                PartEntity.Status = Statusdrpd.SelectedItem.Value.ToString()
            End If
            If (Trim(remarkstbox.Text) <> "") Then
                PartEntity.Remarks = remarkstbox.Text.ToUpper()
            End If
            If (Trim(creatbytbox.Text) <> "") Then
                PartEntity.CreatedBy = Session("userID").ToString.ToUpper()
            End If

            If (Trim(creatdatetbox.Text) <> "") Then
                PartEntity.CreatedDate = calendatetime.DatetoDatabase(creatdatetbox.Text)
            End If

            If (Trim(modifybytbox.Text) <> "") Then
                PartEntity.ModifiedBy = Session("userID").ToString.ToUpper()
            End If

            If (Trim(modifydatetbox.Text) <> "") Then
                PartEntity.ModifiedDate = calendatetime.DatetoDatabase(modifydatetbox.Text)
            End If



            Dim dupCount As Integer = PartEntity.GetDuplicatedpart()
            Dim dupCount1 As Integer = PartEntity.GetDuplicatedpartName()

            If dupCount1.Equals(-1) Then
                Dim objXm1 As New clsXml
                objXm1.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                errlab.Text = objXm1.GetLabelName("StatusMessage", "DUPPARTNAME")
                errlab.Visible = True
            Else
                If dupCount.Equals(-1) Then
                    Dim objXm As New clsXml
                    objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    errlab.Text = objXm.GetLabelName("StatusMessage", "DUPPARTIDORCTRID")
                    errlab.Visible = True
                ElseIf dupCount.Equals(0) Then
                    Dim insCtryCnt As Integer = PartEntity.Insert()
                    Dim objXmlTr As New clsXml
                    objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    If insCtryCnt = 0 Then
                        If KitItemdrpd.SelectedItem.Value.ToString() = "Y" Then
                            retrievekit()
                            Addkit.Visible = True
                            Addkit.Enabled = True
                        Else
                            Addkit.Visible = True
                            Addkit.Enabled = False
                        End If
                        'retrievekit()
                        'Addkit.Visible = True
                        'Addkit.Enabled = True
                        Dim objXm As New clsXml
                        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                        errlab.Text = objXm.GetLabelName("StatusMessage", "BB-HintMsg-Suc")
                        errlab.Visible = True
                    Else
                        Response.Redirect("~/PresentationLayer/Error.aspx")
                    End If
                End If
            End If
        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
            EffectDatetbox.Text = Request.Form("EffectDatetbox")
            ObsolDatetbox.Text = Request.Form("ObsolDatetbox")
        End If
    End Sub

   
   
    Protected Sub JCalendar1_SelectedDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles JCalendar1.SelectedDateChanged
        retrievekit()
        Dim script As String = "self.location='#cal';"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "cal", script, True)
    End Sub

    Protected Sub JCalendar2_SelectedDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles JCalendar2.SelectedDateChanged
        retrievekit()
        Dim script As String = "self.location='#cal';"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "cal", script, True)
    End Sub

    Protected Sub JCalendar1_CalendarVisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles JCalendar1.CalendarVisibleChanged
        ServiceItemdrpd.Visible = Not JCalendar1.CalendarVisible
        Statusdrpd.Visible = Not JCalendar1.CalendarVisible
        Dim script As String = "self.location='#cal';"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "cal", script, True)
    End Sub

    Protected Sub EffectDatetbox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles EffectDatetbox.TextChanged
        ServiceItemdrpd.Visible = Not JCalendar1.CalendarVisible
        Statusdrpd.Visible = Not JCalendar1.CalendarVisible
        Dim script As String = "self.location='#cal';"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "cal", script, True)
    End Sub

    Protected Sub ObsolDatetbox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ObsolDatetbox.TextChanged
        retrievekit()
        Dim script As String = "self.location='#cal';"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "cal", script, True)
    End Sub
End Class
