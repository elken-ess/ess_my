Imports BusinessEntity
Imports System.Configuration
Imports System.Windows.Forms
Imports System.Data
Imports System.Data.SqlClient
Partial Class PresentationLayer_masterrecord_Instsearchcustomer
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Page.IsPostBack) Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try
            'added by deyb 11-08-2006

            BindGrid()
            ctryView.Visible = False
        Else
            ctryView.Visible = True
        End If
    End Sub


    Sub DisplayGridHeader()
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        If ctryView.Rows.Count >= 1 Then
            ctryView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0001")
            ctryView.HeaderRow.Cells(2).Font.Size = 8
            ctryView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0002")
            ctryView.HeaderRow.Cells(3).Font.Size = 8
            ctryView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASADD1Z")
            ctryView.HeaderRow.Cells(4).Font.Size = 8
            ctryView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASADD2Z")
            ctryView.HeaderRow.Cells(5).Font.Size = 8
            ctryView.HeaderRow.Cells(6).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPOCODEZ")
            ctryView.HeaderRow.Cells(6).Font.Size = 8
            ctryView.HeaderRow.Cells(7).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0009")
            ctryView.HeaderRow.Cells(7).Font.Size = 8
            ctryView.HeaderRow.Cells(8).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0005")
            ctryView.HeaderRow.Cells(8).Font.Size = 8
            ctryView.HeaderRow.Cells(9).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-ModelID")
            ctryView.HeaderRow.Cells(9).Font.Size = 8
            ctryView.HeaderRow.Cells(10).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-Country")
            ctryView.HeaderRow.Cells(10).Font.Size = 8
            ctryView.HeaderRow.Cells(11).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0008")
            ctryView.HeaderRow.Cells(11).Font.Size = 8
            ctryView.HeaderRow.Cells(12).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0004")
            ctryView.HeaderRow.Cells(12).Font.Size = 8
        End If
    End Sub


    Protected Sub BindGrid()
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        customerIDlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0001")
        customernamelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0002")
        areaIDlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0003")
        contactlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0004")
      
        searchButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Search")
        titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BROCUSTOM")
        Label1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Maslbleinfo")
        Label1.Visible = False
        Me.addinfo.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-AR")
        Me.addinfo.Visible = False


        Dim userIDNamestr As String = Session("userID").ToString().ToUpper + "-" + Session("username").ToString.ToUpper
        Dim rank As String = Request.QueryString("rank")
        Dim customer As New clsCustomerRecord()
        customer.username = userIDNamestr
        Dim area As New clsCommonClass
        If rank <> "0" Then
            area.spctr = Request.QueryString("countryid")
        End If
        area.rank = rank

        'create  area the dropdownlist
        'Dim area As New clsCommonClass
        Dim areads As New DataSet

        areads = area.Getcomidname("BB_MASAREA_CUSTIDNAME")
        If areads.Tables.Count <> 0 Then
            databonds(areads, Me.areaID)
        End If
        Me.areaID.Items.Insert(0, "")

        Dim cntryStat As New clsrlconfirminf()

        Dim telepass As New clsCommonClass()

        customer.ModBy = userIDNamestr
        If (Trim(Me.contactbox.Text) <> "") Then

            customer.Contact = telepass.telconvertpass(contactbox.Text.ToUpper())
            customer.Contacttype = "Y"
        Else
            customer.Contacttype = "N"
        End If
       
        customer.ctryid = Request.QueryString("countryid")
        customer.compid = Request.QueryString("cmpid")
        customer.usvcid = Request.QueryString("svcid")
        customer.rank = Request.QueryString("rank")

        Dim ctryall As DataSet = customer.GetSearchCustomer()
        ctryView.DataSource = ctryall
        Session("Customer") = ctryall
        ctryView.AllowPaging = True
        ctryView.AllowSorting = True

        'Dim editcol As New CommandField
        'editcol.EditText = "selected"
        'editcol.HeaderImageUrl = "~/PresentationLayer/graph/edit.gif"
        'editcol.ShowEditButton = True
        'ctryView.Columns.Add(editcol)

        'Dim viewcol As New CommandField
        'viewcol.EditText = "view"
        'viewcol.HeaderImageUrl = "~/PresentationLayer/graph/edit.gif"
        'viewcol.ShowEditButton = True
        'ctryView.Columns.Add(viewcol)

        ctryView.DataBind()

        If ctryall.Tables(0).Rows.Count = 0 Then
            Label1.Visible = True
        Else
            Label1.Visible = False
        End If
        Call DisplayGridHeader()

    End Sub
    Protected Sub search_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles searchButton.Click
        Dim CustomerEntity As New clsCustomerRecord()
        Dim telepass As New clsCommonClass()

        If (Trim(Me.customerIDbox.Text) <> "") Then

            CustomerEntity.CustomerID = customerIDbox.Text.ToUpper()

        End If

        If (Trim(Me.customernamebox.Text) <> "") Then

            CustomerEntity.CustomerName = customernamebox.Text.ToUpper()

        End If

        If (Me.areaID.SelectedValue().ToString() <> "") Then
            CustomerEntity.AreaID = Me.areaID.SelectedValue().ToString().ToUpper()
        End If



        CustomerEntity.RoSerialNo = "%"

        If (Trim(Me.contactbox.Text) <> "") Then

            CustomerEntity.Contact = telepass.telconvertpass(contactbox.Text.ToUpper())
            CustomerEntity.Contacttype = "Y"
        Else
            CustomerEntity.Contacttype = "N"
        End If
        CustomerEntity.ctryid = Request.QueryString("countryid")
        CustomerEntity.compid = Request.QueryString("cmpid")
        CustomerEntity.usvcid = Request.QueryString("svcid")
        CustomerEntity.rank = Request.QueryString("rank")

        CustomerEntity.ModBy = Session("userID").ToString().ToUpper

        Dim ctryall As DataSet = CustomerEntity.GetSearchCustomer()
        ctryView.DataSource = ctryall
        Session("Customer") = ctryall
        'CustomerEntity.TORS = ctryall.Tables(0).Rows.Count()
        ctryView.DataBind()
        If Me.customerIDbox.Text = "" And Me.customernamebox.Text = "" And Me.contactbox.Text = "" And Me.areaID.Text = "" Then
            addinfo.Visible = True
        Else
            addinfo.Visible = False
        End If
        If ctryall.Tables(0).Rows.Count = 0 Then
            Label1.Visible = True
        Else
            Label1.Visible = False
        End If
        Call DisplayGridHeader()
    End Sub

    Protected Sub ctryView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles ctryView.PageIndexChanging
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        Dim CustomerEntity As New clsCustomerRecord()
        ctryView.PageIndex = e.NewPageIndex

        Dim Customerall As DataSet = Session("Customer")
        ctryView.DataSource = Customerall
        ctryView.DataBind()
        Call DisplayGridHeader()
    End Sub
#Region " id bond"
    Public Function databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList) As ArrayList
        dropdown.Items.Clear()
        Dim row As DataRow
        Dim infon As ArrayList = New ArrayList()
        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            NewItem.Text = row("id") & "-" & row("name")
            NewItem.Value = row("id")
            dropdown.Items.Add(NewItem)
            infon.Add(NewItem.Value)
        Next
        Return infon

    End Function
#End Region

    Protected Sub ctryView_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles ctryView.RowCommand
        Dim xRow As Integer = e.CommandArgument - (ctryView.PageIndex * 10)
        'initialize querystrings
        Dim type As String = Request.QueryString("type")
        Dim ds As DataSet = Session("Customer")
        Dim searchcustid As String = ds.Tables(0).Rows(e.CommandArgument).Item(0).ToString()
        searchcustid = Server.UrlEncode(searchcustid)
        Dim searchPrefix As String = Session("login_ctryID")
        searchPrefix = Server.UrlEncode(searchPrefix)
        Dim searchcustname As String = ds.Tables(0).Rows(e.CommandArgument).Item(1).ToString()
        searchcustname = Server.UrlEncode(searchcustname)
        Dim searchcustjde As String = ds.Tables(0).Rows(e.CommandArgument).Item(10).ToString()
        searchcustjde = Server.UrlEncode(searchcustjde)

        Dim searchmodelid As String = Request.QueryString("searchmodelid")
        searchmodelid = Server.UrlEncode(searchmodelid)
        Dim searchserialno As String = Request.QueryString("searchserialno")
        searchserialno = Server.UrlEncode(searchserialno)
        Dim searchContract As String = Request.QueryString("searchContract")
        searchContract = Server.UrlEncode(searchContract)
        Dim searchInstalleddate As String = Request.QueryString("searchInstalleddate")
        searchInstalleddate = Server.UrlEncode(searchInstalleddate)
        Dim searchproducedate As String = Request.QueryString("searchproducedate")
        searchproducedate = Server.UrlEncode(searchproducedate)
        Dim searchTechnicianid As String = Request.QueryString("searchTechnicianid")
        searchTechnicianid = Server.UrlEncode(searchTechnicianid)
        Dim searchfreeserentitle As String = Request.QueryString("searchfreeserentitle")
        searchfreeserentitle = Server.UrlEncode(searchfreeserentitle)
        Dim searchproductclass As String = Request.QueryString("searchproductclass")
        searchproductclass = Server.UrlEncode(searchproductclass)
        Dim searchTechnicianType As String = Request.QueryString("searchTechnicianType")
        searchTechnicianType = Server.UrlEncode(searchTechnicianType)
        Dim searchSercenterid As String = Request.QueryString("searchSercenterid")
        searchSercenterid = Server.UrlEncode(searchSercenterid)
        Dim searchSerType As String = Request.QueryString("searchSerType")
        searchSerType = Server.UrlEncode(searchSerType)
        Dim searchWarrExpdate As String = Request.QueryString("searchWarrExpdate")
        searchWarrExpdate = Server.UrlEncode(searchWarrExpdate)
        Dim searchFrSRemDt As String = Request.QueryString("searchFrSRemDt")
        searchFrSRemDt = Server.UrlEncode(searchFrSRemDt)
        Dim searchstatus As String = Request.QueryString("searchstatus")
        searchstatus = Server.UrlEncode(searchstatus)
        Dim searchRemarks As String = Request.QueryString("searchRemarks")
        searchRemarks = Server.UrlEncode(searchRemarks)

        Select Case e.CommandName
            Case Is = "Select"

                If Trim(type) = "add" Then
                    Dim strTempURL As String = "searchcustid=" + searchcustid + "&searchPrefix=" + searchPrefix + "&searchcustname=" + searchcustname + "&searchmodelid=" + searchmodelid + "&searchserialno=" + searchserialno + "&searchContract=" + searchContract + "&searchInstalleddate=" + searchInstalleddate + "&searchproducedate=" + searchproducedate + "&searchTechnicianid=" + searchTechnicianid + "&searchfreeserentitle=" + searchfreeserentitle + "&searchproductclass=" + searchproductclass + "&searchTechnicianType=" + searchTechnicianType + "&searchSercenterid=" + searchSercenterid + "&searchSerType=" + searchSerType + "&searchWarrExpdate=" + searchWarrExpdate + "&searchFrSRemDt=" + searchFrSRemDt + "&searchstatus=" + searchstatus + "&searchRemarks=" + searchRemarks + "&type=" + type
                    strTempURL = "~/PresentationLayer/masterrecord/addinstalleBase.aspx?" + strTempURL + "&searchcustjde=" + searchcustjde
                    Response.Redirect(strTempURL)
                End If
                If Trim(type) = "edit" Then
                    Dim rouid As String = Request.QueryString("rouid")
                    rouid = Server.UrlEncode(rouid)
                    Dim strTempURL As String = "searchcustid=" + searchcustid + "&searchPrefix=" + searchPrefix + "&searchcustname=" + searchcustname + "&searchmodelid=" + searchmodelid + "&searchserialno=" + searchserialno + "&searchContract=" + searchContract + "&searchInstalleddate=" + searchInstalleddate + "&searchproducedate=" + searchproducedate + "&searchTechnicianid=" + searchTechnicianid + "&searchfreeserentitle=" + searchfreeserentitle + "&searchproductclass=" + searchproductclass + "&searchTechnicianType=" + searchTechnicianType + "&searchSercenterid=" + searchSercenterid + "&searchSerType=" + searchSerType + "&searchWarrExpdate=" + searchWarrExpdate + "&searchFrSRemDt=" + searchFrSRemDt + "&searchstatus=" + searchstatus + "&searchRemarks=" + searchRemarks + "&type=" + type + "&rouid=" + rouid
                    strTempURL = "~/PresentationLayer/masterrecord/ModifyinstallBase.aspx?" + strTempURL + "&searchcustjde=" + searchcustjde
                    Response.Redirect(strTempURL)
                End If
            Case Is = "View"
                Dim rouid As String = ""
                Dim strCusID As String = ctryView.Rows(xRow).Cells(2).Text
                Dim strPrefix As String = Session("login_ctryid")
                Dim strSerial As String = ctryView.Rows(xRow).Cells(8).Text
                Dim strMod As String = ctryView.Rows(xRow).Cells(9).Text
                Dim strTempURL As String = ""
                Dim CustomerEntity As New clsCustomerRecord()
                CustomerEntity.CustomerID = strCusID
                CustomerEntity.RoSerialNo = strSerial
                CustomerEntity.modelid = strMod
                CustomerEntity.Customerpf = strPrefix
                rouid = CustomerEntity.GetInstallROUID()
                If rouid Is Nothing Then
                    'addinfo.Text = "No records found associated with the selected customer."
                    Dim objXmlTr As New clsXml
                    objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    LblErr.Text = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRRO")
                    LblErr.Visible = True
                Else
                    strTempURL = "rouid=" + rouid
                    'strTempURL = "~/PresentationLayer/masterrecord/ModifyinstallBase.aspx?" + strTempURL
                    'strTempURL = "searchcustid=" + searchcustid + "&searchPrefix=" + searchPrefix + "&searchcustname=" + searchcustname + "&searchmodelid=" + searchmodelid + "&searchserialno=" + searchserialno + "&searchContract=" + searchContract + "&searchInstalleddate=" + searchInstalleddate + "&searchproducedate=" + searchproducedate + "&searchTechnicianid=" + searchTechnicianid + "&searchfreeserentitle=" + searchfreeserentitle + "&searchproductclass=" + searchproductclass + "&searchTechnicianType=" + searchTechnicianType + "&searchSercenterid=" + searchSercenterid + "&searchSerType=" + searchSerType + "&searchWarrExpdate=" + searchWarrExpdate + "&searchFrSRemDt=" + searchFrSRemDt + "&searchstatus=" + searchstatus + "&searchRemarks=" + searchRemarks + "&type=" + type + "&rouid=" + rouid
                    strTempURL = "~/PresentationLayer/masterrecord/ModifyinstallBase.aspx?" + strTempURL + "&search=true"
                    Response.Redirect(strTempURL)
                End If
        End Select
    End Sub

    'Protected Sub ctryView_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles ctryView.RowEditing
    '    Dim type As String = Request.QueryString("type")
    '    Dim ds As DataSet = Session("Customer")
    '    Dim searchcustid As String = ds.Tables(0).Rows(ctryView.PageIndex * ctryView.PageSize + e.NewEditIndex).Item(0).ToString()
    '    searchcustid = Server.UrlEncode(searchcustid)
    '    Dim searchPrefix As String = Session("login_ctryID")
    '    searchPrefix = Server.UrlEncode(searchPrefix)
    '    Dim searchcustname As String = ds.Tables(0).Rows(ctryView.PageIndex * ctryView.PageSize + e.NewEditIndex).Item(1).ToString()
    '    searchcustname = Server.UrlEncode(searchcustname)
    '    Dim searchcustjde As String = ds.Tables(0).Rows(ctryView.PageIndex * ctryView.PageSize + e.NewEditIndex).Item(10).ToString()
    '    searchcustjde = Server.UrlEncode(searchcustjde)

    '    Dim searchmodelid As String = Request.QueryString("searchmodelid")
    '    searchmodelid = Server.UrlEncode(searchmodelid)
    '    Dim searchserialno As String = Request.QueryString("searchserialno")
    '    searchserialno = Server.UrlEncode(searchserialno)
    '    Dim searchContract As String = Request.QueryString("searchContract")
    '    searchContract = Server.UrlEncode(searchContract)
    '    Dim searchInstalleddate As String = Request.QueryString("searchInstalleddate")
    '    searchInstalleddate = Server.UrlEncode(searchInstalleddate)
    '    Dim searchproducedate As String = Request.QueryString("searchproducedate")
    '    searchproducedate = Server.UrlEncode(searchproducedate)
    '    Dim searchTechnicianid As String = Request.QueryString("searchTechnicianid")
    '    searchTechnicianid = Server.UrlEncode(searchTechnicianid)
    '    Dim searchfreeserentitle As String = Request.QueryString("searchfreeserentitle")
    '    searchfreeserentitle = Server.UrlEncode(searchfreeserentitle)
    '    Dim searchproductclass As String = Request.QueryString("searchproductclass")
    '    searchproductclass = Server.UrlEncode(searchproductclass)
    '    Dim searchTechnicianType As String = Request.QueryString("searchTechnicianType")
    '    searchTechnicianType = Server.UrlEncode(searchTechnicianType)
    '    Dim searchSercenterid As String = Request.QueryString("searchSercenterid")
    '    searchSercenterid = Server.UrlEncode(searchSercenterid)
    '    Dim searchSerType As String = Request.QueryString("searchSerType")
    '    searchSerType = Server.UrlEncode(searchSerType)
    '    Dim searchWarrExpdate As String = Request.QueryString("searchWarrExpdate")
    '    searchWarrExpdate = Server.UrlEncode(searchWarrExpdate)
    '    Dim searchFrSRemDt As String = Request.QueryString("searchFrSRemDt")
    '    searchFrSRemDt = Server.UrlEncode(searchFrSRemDt)
    '    Dim searchstatus As String = Request.QueryString("searchstatus")
    '    searchstatus = Server.UrlEncode(searchstatus)
    '    Dim searchRemarks As String = Request.QueryString("searchRemarks")
    '    searchRemarks = Server.UrlEncode(searchRemarks)


    '    If Trim(type) = "add" Then
    '        Dim strTempURL As String = "searchcustid=" + searchcustid + "&searchPrefix=" + searchPrefix + "&searchcustname=" + searchcustname + "&searchmodelid=" + searchmodelid + "&searchserialno=" + searchserialno + "&searchContract=" + searchContract + "&searchInstalleddate=" + searchInstalleddate + "&searchproducedate=" + searchproducedate + "&searchTechnicianid=" + searchTechnicianid + "&searchfreeserentitle=" + searchfreeserentitle + "&searchproductclass=" + searchproductclass + "&searchTechnicianType=" + searchTechnicianType + "&searchSercenterid=" + searchSercenterid + "&searchSerType=" + searchSerType + "&searchWarrExpdate=" + searchWarrExpdate + "&searchFrSRemDt=" + searchFrSRemDt + "&searchstatus=" + searchstatus + "&searchRemarks=" + searchRemarks + "&type=" + type
    '        strTempURL = "~/PresentationLayer/masterrecord/addinstalleBase.aspx?" + strTempURL + "&searchcustjde=" + searchcustjde
    '        Response.Redirect(strTempURL)
    '    End If
    '    If Trim(type) = "edit" Then
    '        Dim rouid As String = Request.QueryString("rouid")
    '        rouid = Server.UrlEncode(rouid)
    '        Dim strTempURL As String = "searchcustid=" + searchcustid + "&searchPrefix=" + searchPrefix + "&searchcustname=" + searchcustname + "&searchmodelid=" + searchmodelid + "&searchserialno=" + searchserialno + "&searchContract=" + searchContract + "&searchInstalleddate=" + searchInstalleddate + "&searchproducedate=" + searchproducedate + "&searchTechnicianid=" + searchTechnicianid + "&searchfreeserentitle=" + searchfreeserentitle + "&searchproductclass=" + searchproductclass + "&searchTechnicianType=" + searchTechnicianType + "&searchSercenterid=" + searchSercenterid + "&searchSerType=" + searchSerType + "&searchWarrExpdate=" + searchWarrExpdate + "&searchFrSRemDt=" + searchFrSRemDt + "&searchstatus=" + searchstatus + "&searchRemarks=" + searchRemarks + "&type=" + type + "&rouid=" + rouid
    '        strTempURL = "~/PresentationLayer/masterrecord/ModifyinstallBase.aspx?" + strTempURL + "&searchcustjde=" + searchcustjde
    '        Response.Redirect(strTempURL)
    '    End If
    'End Sub

End Class
