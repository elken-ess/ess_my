<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PartCategory.aspx.vb" Inherits="PresentationLayer_masterrecord_PartCategory" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Part Category - Search</title>
       <LINK href="../css/style.css" type="text/css" rel="stylesheet">
     <STYLE type="text/css">A:link { COLOR: #336666; TEXT-DECORATION: none }
	BODY { FONT-SIZE: 9pt; FONT-FAMILY: "Arial", "Verdana", "Tahoma"; BACKGROUND-COLOR: #ffffff }
	TD { FONT-SIZE: 9pt; FONT-FAMILY: Arial,Verdana,Tahoma }
	</STYLE>
	<LINK href="../css/style.css" type="text/css" rel="stylesheet">
	<script language="JavaScript" src="../js/common.js"></script> 
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div>
            <table id="TABLE2" border="0" width ="100%">
                <tr>
                    <td style="width: 100%; height: 13px">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td background="../graph/title_bg.gif" width="1%">
                                    <img height="24" src="../graph/title1.gif" width="5" /></td>
                                <td background="../graph/title_bg.gif" class="style2" width="98%">
                                    <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="right" background="../graph/title_bg.gif" width="1%">
                                    <img height="24" src="../graph/title_2.gif" width="5" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="width: 100%; height: 12px">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td background="../graph/title_bg.gif" class="style2" style="height: 1px" width="98%">
                                    <font color="red">
                                        <asp:Label ID="addinfo" runat="server" Text="Label" Visible="false"></asp:Label></font></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <asp:Label ID="CategoryIDlab" runat="server" Text="Label"></asp:Label>
            <asp:TextBox ID="CategoryIDBox" runat="server" CssClass="textborder" Width="13%" MaxLength="10"></asp:TextBox>
            <asp:Label ID="Categorynamelab" runat="server" Text="Label"></asp:Label>
            <asp:TextBox ID="CategorynameBox" runat="server" CssClass="textborder" Width="15%" MaxLength="50"></asp:TextBox>
            &nbsp;<asp:Label ID="ctryStat" runat="server" Text="Label"></asp:Label>&nbsp;<asp:DropDownList ID="ctryStatDrop" runat="server" CssClass="textborder" Width="104px">
            </asp:DropDownList>
            <asp:LinkButton ID="searchButton" runat="server"></asp:LinkButton>
            &nbsp; &nbsp;&nbsp;<asp:LinkButton ID="addButton" runat="server" PostBackUrl="~/PresentationLayer/masterrecord/AddPartCategory.aspx"></asp:LinkButton>&nbsp;
            <hr />
        </div>
        <asp:GridView ID="partcategoryView" runat="server" AllowPaging="True" AllowSorting="True"
            Width="100%" Font-Size="Smaller">
        </asp:GridView>
        <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="Label" Visible="False"></asp:Label>
        <asp:TextBox ID="catidtbox" runat="server" Visible="False"></asp:TextBox>
        <asp:TextBox ID="countryid" runat="server" Visible="False"></asp:TextBox></div>
    </form>
</body>
</html>
