Imports System.Data
Imports System.Data.SqlClient

Partial Class PresentationLayer_masterrecord_modifyCustomer_SMS
    Inherits System.Web.UI.Page
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)
        Return connection
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        CountTxt.Text = MessageTxt.Text.Length

        If Normal.Checked = True Then
            If MessageTxt.Text.Length <= 160 Then
                SubmitBtn.Enabled = True
            Else
                SubmitBtn.Enabled = False
                Label1.Text = "Character count is over 160!"
                Label1.Visible = True
            End If
        End If

        If Chinese.Checked = True Then
            If MessageTxt.Text.Length <= 70 Then
                SubmitBtn.Enabled = True
            Else
                SubmitBtn.Enabled = False
                Label1.Text = "Character count is over 70!"
                Label1.Visible = True
            End If
        End If
    End Sub

    Protected Sub Normal_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Normal.CheckedChanged
        Chinese.Checked = False
        MessageTxt.Text = "Elken Service appointment: xx/xx/xxxx. Confirmation number:35XXXXXXX  Any doubt pls call XX-XXXXXXXX. From Elken Service (computer generated, do not reply)"
        CountTxt.Text = MessageTxt.Text.Length
        Label1.Visible = False
    End Sub

    Protected Sub Chinese_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Chinese.CheckedChanged
        Normal.Checked = False
        MessageTxt.Text = ""
        CountTxt.Text = MessageTxt.Text.Length
        Label1.Visible = False
    End Sub

    Protected Sub SubmitBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles SubmitBtn.Click
        Dim PhoneNum = Session("tele")

        Dim _con As SqlConnection
        _con = GetConnection(ConfigurationManager.AppSettings("ConnectionString"))

        If Normal.Checked = True Then
            If MessageTxt.Text.Length <= 160 Then
                Using command As New SqlCommand(" " _
                        & "DECLARE @sms_mobile varchar(20) " _
                        & "DECLARE @sms_text nvarchar(640) " _
                        & "DECLARE @source_page varchar(20) " _
                        & "DECLARE @sql nvarchar(4000) " _
                        & "DECLARE @sms_type char(1) " _
                        & "DECLARE @priority nvarchar(1) " _
                        & " " _
                        & "SET @sms_mobile = '+6' +  @Mobile " _
                        & "SET @sms_text = @SMSText " _
                        & "SET @priority = '1' " _
                        & "SET @sms_type = 'N' " _
                        & " " _
                        & "IF UNICODE(LEFT(@sms_text,1)) > 127 BEGIN SET @sms_type = 'U' End " _
                        & "SET @sql = 'SELECT * " _
                        & "FROM OPENQUERY(MYHQBPP001, ''EXECUTE  " _
                        & "SYSMGR.dbo.usp_SMS_Insert 0, ''''' + @sms_mobile +  " _
                        & "''''', N''''' + @sms_text + ''''', ' +  " _
                        & "'''''' + @sms_type + ''''', 73, ''''S'''', ' +  " _
                        & "@priority + ', NULL, ''''N'''', ''''1oyas@*&d9''''; " _
                        & "COMMIT;'')' " _
                        & " " _
                        & "EXEC (@sql) ", _con)
                    command.Parameters.AddWithValue("@Mobile", PhoneNum)
                    command.Parameters.AddWithValue("@SMSText", MessageTxt.Text)
                    command.Parameters.AddWithValue("@EntBy", "1")
                    _con.Open()

                    command.ExecuteNonQuery()
                End Using

                SubmitBtn.Enabled = False
                Label1.Text = "Message added to send queue!"
                Label1.Visible = True
            End If
        End If



        If Chinese.Checked = True Then
            If MessageTxt.Text.Length <= 70 Then
                Using command As New SqlCommand(" " _
                        & "DECLARE @sms_mobile varchar(20) " _
                        & "DECLARE @sms_text nvarchar(640) " _
                        & "DECLARE @source_page varchar(20) " _
                        & "DECLARE @sql nvarchar(4000) " _
                        & "DECLARE @sms_type char(1) " _
                        & "DECLARE @priority nvarchar(1) " _
                        & " " _
                        & "SET @sms_mobile = '+6' +  @Mobile " _
                        & "SET @sms_text = @SMSText " _
                        & "SET @priority = '1' " _
                        & "SET @sms_type = 'N' " _
                        & " " _
                        & "IF UNICODE(LEFT(@sms_text,1)) > 127 BEGIN SET @sms_type = 'U' End " _
                        & "SET @sql = 'SELECT * " _
                        & "FROM OPENQUERY(MYHQBPP001, ''EXECUTE  " _
                        & "SYSMGR.dbo.usp_SMS_Insert 0, ''''' + @sms_mobile +  " _
                        & "''''', N''''' + @sms_text + ''''', ' +  " _
                        & "'''''' + @sms_type + ''''', 73, ''''S'''', ' +  " _
                        & "@priority + ', NULL, ''''N'''', ''''1oyas@*&d9''''; " _
                        & "COMMIT;'')' " _
                        & " " _
                        & "EXEC (@sql) ", _con)
                    command.Parameters.AddWithValue("@Mobile", PhoneNum)
                    command.Parameters.AddWithValue("@SMSText", MessageTxt.Text)
                    command.Parameters.AddWithValue("@EntBy", "1")
                    _con.Open()

                    command.ExecuteNonQuery()
                End Using

                SubmitBtn.Enabled = False
                Label1.Text = "Message added to send queue!"
                Label1.Visible = True
            End If
        End If



    End Sub

    Protected Sub MessageTxt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles MessageTxt.TextChanged
        If Normal.Checked = True Then
            If MessageTxt.Text.Length <= 160 Then
                SubmitBtn.Enabled = True
                Label1.Visible = False
            Else
                SubmitBtn.Enabled = False
                Label1.Text = "Character count is over 160!"
                Label1.Visible = True
            End If
        End If

        If Chinese.Checked = True Then
            If MessageTxt.Text.Length <= 70 Then
                SubmitBtn.Enabled = True
                Label1.Visible = False
            Else
                SubmitBtn.Enabled = False
                Label1.Text = "Character count is over 70!"
                Label1.Visible = True
            End If
        End If

    End Sub
End Class
