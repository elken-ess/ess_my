Imports System.Data.SqlClient
Imports BusinessEntity
Imports System.Configuration
Imports SQLDataAccess
Imports System.Data
Partial Class PresentationLayer_masterrecord_modifySalTaxID
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try
        Try
            Dim str As String = Request.Params("SalTaxsetID").ToString

        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try

        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        If Not Page.IsPostBack Then
            Dim mdfyid As String = Request.Params("SalTaxsetID").ToString()

            Dim returnprisid As Integer
            returnprisid = Request.Params("salsetupid1")
           


            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            SalTaxIDLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTAX-0001")
            salnameLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTAX-0002")
            ALterNamelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTAX-0003")
            CountryIDlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTAX-0004")
            TaxPectgLabel.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTAX-0005")
            CreatedBylab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATBY")
            CreatedDatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
            ModifiedBylab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFBY")
            ModifiedDatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFDATE")
            StatusLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Status")
            saveButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Save")
            cancelLink.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Cancel")
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTAX-0009")
            AddButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTAX-0011")
            lblNoteUP.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            lblNoteDown.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")

            Dim objXmlTr1 As New clsXml
            objXmlTr1.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            salnameva.ErrorMessage = objXmlTr1.GetLabelName("StatusMessage", "SALTAXNAME")
            'Dim strPanm As String = objXmlTr1.GetLabelID("StatusMessage", statusid)




            '''access control'''''''''''''''''''''''''''''''''''''''''''''''
            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "18")
            Me.saveButton.Enabled = purviewArray(1)
            Me.AddButton.Enabled = purviewArray(1)
            If purviewArray(2) = False Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return

            End If



            If (mdfyid <> "") Then
                Dim SalTaxIDEntity As New clsSalTaxID()
                SalTaxIDEntity.SalTaxSetUpID = Convert.ToUInt32(mdfyid)
                'SalTaxIDEntity.SalTaxStatus = strPanm
                Dim retnArray As ArrayList = SalTaxIDEntity.GetSalTaxIDDetailsByID()
                SetUpID.Text = retnArray(0).ToString()
                saltaxidbox.Text = retnArray(1).ToString()
                saltaxidbox.ReadOnly = True
                salnamebox.Text = retnArray(2).ToString()
                ALterNamebox.Text = retnArray(3).ToString()

                'create the dropdownlist
                Dim saltaxStat As New clsrlconfirminf()
                Dim statParam As ArrayList = New ArrayList
                statParam = saltaxStat.searchconfirminf("STATUS")
                Dim count As Integer
                Dim statid As String
                Dim statnm As String
                For count = 0 To statParam.Count - 1
                    statid = statParam.Item(count)
                    statnm = statParam.Item(count + 1)
                    If (statid.Equals("ACTIVE") Or statid.Equals("DELETE") Or statid.Equals("OBSOLETE")) Then
                        StatusDrop.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                    End If
                    If statid.Equals(retnArray(5).ToString()) Then
                        StatusDrop.Items(count / 2).Selected = True
                    End If
                    count = count + 1
                Next

                '''access control'''''''''''''''''''''''''''''''''''''''''''''''

                If purviewArray(4) = False Then
                    Me.StatusDrop.Items.Remove(Me.StatusDrop.Items.FindByValue("DELETE"))

                End If

                Dim userIDNamestr As String = Session("userID").ToString().ToUpper + "-" + Session("username").ToString.ToUpper
                ModifiedBy.Text = userIDNamestr

                Dim Rcreatby As String = retnArray.Item(6).ToString().ToUpper()
                If Rcreatby <> "ADMIN" Then
                    Dim Rcreat As New clsCommonClass
                    Rcreat.userid() = Rcreatby
                    Dim Rcreatds As New DataSet()
                    Rcreatds = Rcreat.Getuseridname("BB_MASUSER_SelByIDName")

                    Me.CreatedBy.Text = Rcreatds.Tables(0).Rows(0).Item(0)
                Else
                    Me.CreatedBy.Text = "ADMIN-ADMIN"
                End If

                CreatedBy.ReadOnly = True

                CreatedDate.Text = retnArray(7).ToString()
                CreatedDate.ReadOnly = True
                ModifiedDate.Text = retnArray(9).ToString()
                countryidBox.Text = retnArray(4).ToString()


                'display  detail table

                Dim objXmlTr3 As New clsXml
                objXmlTr3.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
                Dim SalSetUpEntity3 As New clsSalTaxID()
                SalSetUpEntity3.SalTaxSetUpID = Convert.ToUInt32(mdfyid)
                'SalSetUpEntity3.SalTaxStatus = statusid
                Dim SalSetUpall3 As DataSet = SalSetUpEntity3.GetSalTaxIDdetail()
                If SalSetUpall3.Tables(0).Rows.Count <> 0 Then
                    salSetUp1View.DataSource = SalSetUpall3
                    Session("salSetUp1View") = SalSetUpall3
                    salSetUp1View.AllowPaging = True
                    'salSetUp1View.AllowSorting = True


                    Dim editcol As New CommandField
                    editcol.EditText = objXmlTr.GetLabelName("EngLabelMsg", "BB-Edit") '"edit"
                    editcol.HeaderImageUrl = "~/PresentationLayer/graph/edit.gif"
                    editcol.ShowEditButton = True
                    salSetUp1View.Columns.Add(editcol)
                    editcol.Visible = purviewArray(2)

                    salSetUp1View.DataBind()

                    salSetUp1View.HeaderRow.Cells(1).Text = objXmlTr3.GetLabelName("EngLabelMsg", "BB-MASSTAX-0013")
                    salSetUp1View.HeaderRow.Cells(1).Font.Size = 8
                    salSetUp1View.HeaderRow.Cells(2).Text = objXmlTr3.GetLabelName("EngLabelMsg", "BB-MASSTAX-0005")
                    salSetUp1View.HeaderRow.Cells(2).Font.Size = 8
                    salSetUp1View.HeaderRow.Cells(3).Text = objXmlTr3.GetLabelName("EngLabelMsg", "BB-MASSTAX-0006")
                    salSetUp1View.HeaderRow.Cells(3).Font.Size = 8
                    salSetUp1View.HeaderRow.Cells(4).Text = objXmlTr3.GetLabelName("EngLabelMsg", "BB-Status")
                    salSetUp1View.HeaderRow.Cells(4).Font.Size = 8
                    salSetUp1View.HeaderRow.Cells(5).Text = objXmlTr3.GetLabelName("EngLabelMsg", "BB-CREATDATE")
                    salSetUp1View.HeaderRow.Cells(5).Font.Size = 8

                End If
            End If
            'return display modify detail table
            If (returnprisid <> 0) Then
                Dim salSetUpEntity4 As New clsSalTaxID()
                salSetUpEntity4.SalTaxSetUpID = Convert.ToUInt32(returnprisid)

                Dim retnArray4 As ArrayList = salSetUpEntity4.GetReturnsalSetUpIDDetailsByID()
                SetUpID.Text = retnArray4(0)
                Dim cntryStat As New clsrlconfirminf()
                Dim statParam As ArrayList = New ArrayList
                statParam = cntryStat.searchconfirminf("STATUS")
                Dim count As Integer
                Dim statid As String
                Dim statnm As String
                For count = 0 To statParam.Count - 1
                    statid = statParam.Item(count)
                    statnm = statParam.Item(count + 1)
                    If (statid.Equals("ACTIVE") Or statid.Equals("DELETE") Or statid.Equals("OBSOLETE")) Then
                        StatusDrop.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                    End If
                    If statid.Equals(retnArray4(5).ToString()) Then
                        StatusDrop.Items(count / 2).Selected = True
                    End If
                    count = count + 1
                Next
                '''access control'''''''''''''''''''''''''''''''''''''''''''''''

                If purviewArray(4) = False Then
                    Me.StatusDrop.Items.Remove(Me.StatusDrop.Items.FindByValue("DELETE"))

                End If


                Dim userIDNamestr As String = Session("userID").ToString().ToUpper + "-" + Session("username").ToString.ToUpper
                ModifiedBy.Text = userIDNamestr

                Dim Rcreatby As String = retnArray4.Item(6).ToString().ToUpper()
                If Rcreatby <> "ADMIN" Then
                    Dim Rcreat As New clsCommonClass
                    Rcreat.userid() = Rcreatby
                    Dim Rcreatds As New DataSet()
                    Rcreatds = Rcreat.Getuseridname("BB_MASUSER_SelByIDName")

                    Me.CreatedBy.Text = Rcreatds.Tables(0).Rows(0).Item(0)
                Else
                    Me.CreatedBy.Text = "ADMIN-ADMIN"
                End If

                CreatedBy.ReadOnly = True
                CreatedDate.ReadOnly = True
                CreatedDate.Text = retnArray4(7).ToString()
                CreatedDate.ReadOnly = True
                ModifiedDate.Text = retnArray4(9).ToString()
                countryidBox.Text = retnArray4(4).ToString()
                saltaxidbox.Text = retnArray4(1).ToString()
                ALterNamebox.Text = retnArray4(3).ToString()
                salnamebox.Text = retnArray4(2).ToString()


                Dim objXmlTr2 As New clsXml
                objXmlTr2.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
                Dim salSetUpEntity1 As New clsSalTaxID()
                salSetUpEntity1.SalTaxSetUpID = Convert.ToUInt32(returnprisid)
                'salSetUpEntity1.crdate = stat
                Dim salSetUpall As DataSet = salSetUpEntity1.GetSalTaxIDdetail()

                salSetUp1View.DataSource = salSetUpall
                Session("salSetUp1View") = salSetUpall
                salSetUp1View.AllowPaging = True
                    'salSetUp1View.AllowSorting = True
               
                Dim editcol1 As New CommandField
                editcol1.EditText = objXmlTr2.GetLabelName("EngLabelMsg", "BB-Edit") '"edit"
                editcol1.HeaderImageUrl = "~/PresentationLayer/graph/edit.gif"
                editcol1.ShowEditButton = True
                salSetUp1View.Columns.Add(editcol1)
                'editcol1.Visible = purviewArray(1)

                salSetUp1View.DataBind()
                If salSetUpall.Tables(0).Rows.Count <> 0 Then
                    salSetUp1View.HeaderRow.Cells(1).Text = objXmlTr2.GetLabelName("EngLabelMsg", "BB-MASSTAX-0013")
                    salSetUp1View.HeaderRow.Cells(1).Font.Size = 8
                    salSetUp1View.HeaderRow.Cells(2).Text = objXmlTr2.GetLabelName("EngLabelMsg", "BB-MASSTAX-0005")
                    salSetUp1View.HeaderRow.Cells(2).Font.Size = 8
                    salSetUp1View.HeaderRow.Cells(3).Text = objXmlTr2.GetLabelName("EngLabelMsg", "BB-MASSTAX-0006")
                    salSetUp1View.HeaderRow.Cells(3).Font.Size = 8
                    salSetUp1View.HeaderRow.Cells(4).Text = objXmlTr2.GetLabelName("EngLabelMsg", "BB-Status")
                    salSetUp1View.HeaderRow.Cells(4).Font.Size = 8
                    salSetUp1View.HeaderRow.Cells(5).Text = objXmlTr2.GetLabelName("EngLabelMsg", "BB-CREATDATE")
                    salSetUp1View.HeaderRow.Cells(5).Font.Size = 8
                End If

            End If
        End If


    End Sub
#Region " id bond"
    Public Function databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        Dim row As DataRow
        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            NewItem.Text = row("id") & "-" & row("name")
            NewItem.Value = row("id")
            dropdown.Items.Add(NewItem)
        Next

    End Function
#End Region
    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveButton.Click
        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim msginfo As String = objXm.GetLabelName("StatusMessage", "SAVEORNOT")
        Dim msgtitle As String = objXm.GetLabelName("StatusMessage", "SAVETITLE")
        MessageBox1.Confirm(msginfo)
        Dim dsPriceIDall As DataSet = Session("salSetUp1View")
        salSetUp1View.DataSource = dsPriceIDall
        salSetUp1View.AllowPaging = True
                    'salSetUp1View.AllowSorting = True
        salSetUp1View.DataBind()
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        If dsPriceIDall.Tables(0).Rows.Count <> 0 Then
            salSetUp1View.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTAX-0013")
            salSetUp1View.HeaderRow.Cells(1).Font.Size = 8
            salSetUp1View.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTAX-0005")
            salSetUp1View.HeaderRow.Cells(2).Font.Size = 8
            salSetUp1View.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTAX-0006")
            salSetUp1View.HeaderRow.Cells(3).Font.Size = 8
            salSetUp1View.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Status")
            salSetUp1View.HeaderRow.Cells(4).Font.Size = 8
            salSetUp1View.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
            salSetUp1View.HeaderRow.Cells(5).Font.Size = 8
        End If

       
      
    End Sub
   

   

    Protected Sub AddButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles AddButton.Click

        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        If Me.StatusDrop.SelectedValue <> "ACTIVE" Then
            errlab.Text = objXm.GetLabelName("StatusMessage", "PRICESETUPADDERROR")
            errlab.Visible = True
            Dim dsPriceIDall As DataSet = Session("salSetUp1View")
            salSetUp1View.DataSource = dsPriceIDall
            salSetUp1View.AllowPaging = True
                    'salSetUp1View.AllowSorting = True
            salSetUp1View.DataBind()
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            If dsPriceIDall.Tables(0).Rows.Count <> 0 Then
                salSetUp1View.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTAX-0013")
                salSetUp1View.HeaderRow.Cells(1).Font.Size = 8
                salSetUp1View.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTAX-0005")
                salSetUp1View.HeaderRow.Cells(2).Font.Size = 8
                salSetUp1View.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTAX-0006")
                salSetUp1View.HeaderRow.Cells(3).Font.Size = 8
                salSetUp1View.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Status")
                salSetUp1View.HeaderRow.Cells(4).Font.Size = 8
                salSetUp1View.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
                salSetUp1View.HeaderRow.Cells(5).Font.Size = 8
            End If
          
            Return
        End If
        Dim url As String = "~/PresentationLayer/masterrecord/addSalTaxID1.aspx?"
        url &= "saltaxsetupid=" & Me.SetUpID.Text & "&"
        url &= "symbl=" & 1
        Response.Redirect(url)
       
       
       

    End Sub

    Protected Sub MessageBox1_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox1.GetMessageBoxResponse
        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
            Dim SalTaxIDEntity As New clsSalTaxID()
            Dim saltaxStat As New clsCommonClass()
            If (Trim(SetUpID.Text) <> "") Then
                SalTaxIDEntity.SalTaxSetUpID = SetUpID.Text
            End If
            If (Trim(saltaxidbox.Text) <> "") Then
                SalTaxIDEntity.SalTaxID = saltaxidbox.Text.ToUpper()
            End If
            If (Trim(salnamebox.Text) <> "") Then
                SalTaxIDEntity.SalTaxName = salnamebox.Text.ToUpper()
            End If
            If (Trim(ALterNamebox.Text) <> "") Then
                SalTaxIDEntity.SalTaxAlternateName = ALterNamebox.Text.ToUpper()
            End If
            If (Trim(countryidBox.Text) <> "") Then
                SalTaxIDEntity.CountryID = countryidBox.Text
            End If
            If (StatusDrop.SelectedItem.Value.ToString() <> "") Then
                SalTaxIDEntity.SalTaxStatus = StatusDrop.SelectedItem.Value.ToString()
            End If

            If Me.StatusDrop.SelectedValue = "DELETE" Then
                If check() = 1 Then
                    Dim objXm As New clsXml
                    objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    errlab.Text = objXm.GetLabelName("StatusMessage", "CHECKTAXSTATE")
                    errlab.Visible = True
                    Dim dsPriceIDall1 As DataSet = Session("salSetUp1View")
                    salSetUp1View.DataSource = dsPriceIDall1
                    salSetUp1View.AllowPaging = True
                    'salSetUp1View.AllowSorting = True
                    salSetUp1View.DataBind()
                    Dim objXmlTr1 As New clsXml
                    objXmlTr1.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
                    If dsPriceIDall1.Tables(0).Rows.Count <> 0 Then
                        salSetUp1View.HeaderRow.Cells(1).Text = objXmlTr1.GetLabelName("EngLabelMsg", "BB-MASSTAX-0013")
                        salSetUp1View.HeaderRow.Cells(1).Font.Size = 8
                        salSetUp1View.HeaderRow.Cells(2).Text = objXmlTr1.GetLabelName("EngLabelMsg", "BB-MASSTAX-0005")
                        salSetUp1View.HeaderRow.Cells(2).Font.Size = 8
                        salSetUp1View.HeaderRow.Cells(3).Text = objXmlTr1.GetLabelName("EngLabelMsg", "BB-MASSTAX-0006")
                        salSetUp1View.HeaderRow.Cells(3).Font.Size = 8
                        salSetUp1View.HeaderRow.Cells(4).Text = objXmlTr1.GetLabelName("EngLabelMsg", "BB-Status")
                        salSetUp1View.HeaderRow.Cells(4).Font.Size = 8
                        salSetUp1View.HeaderRow.Cells(5).Text = objXmlTr1.GetLabelName("EngLabelMsg", "BB-CREATDATE")
                        salSetUp1View.HeaderRow.Cells(5).Font.Size = 8
                    End If
                    Return

                End If

            End If
           
            If (Trim(CreatedBy.Text) <> "") Then
                SalTaxIDEntity.CreatedBy = CreatedBy.Text.ToUpper()
            End If
            If (Trim(CreatedDate.Text) <> "") Then
                SalTaxIDEntity.CreatedDate = saltaxStat.DatetoDatabase(CreatedDate.Text)
            End If
            'If (Trim(ModifiedBy.Text) <> "") Then
            '    SalTaxIDEntity.ModifiedBy = ModifiedBy.Text.ToUpper()
            'End If
            If (Trim(Me.ModifiedBy.Text) <> "") Then
                SalTaxIDEntity.ModifiedBy = Session("userID").ToString().ToUpper
            End If
            If (Trim(ModifiedDate.Text) <> "") Then
                SalTaxIDEntity.ModifiedDate = saltaxStat.DatetoDatabase(ModifiedDate.Text)
            End If


            SalTaxIDEntity.svcid = Session("login_svcID").ToString().ToUpper
            SalTaxIDEntity.ip = Request.UserHostAddress.ToString()

           

            Dim insSLTXCnt As Integer = SalTaxIDEntity.GetSalTaxIDDetailsBySalTaxNM()
            If insSLTXCnt <> 0 Then
                Dim objXm As New clsXml
                objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                errlab.Text = objXm.GetLabelName("StatusMessage", "DUPSALNAME")
                errlab.Visible = True
            ElseIf insSLTXCnt.Equals(0) Then
                Dim updSalTaxCnt As Integer = SalTaxIDEntity.Update()
                Dim objXmlT As New clsXml
                objXmlT.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                If updSalTaxCnt = 0 Then
                    Dim objXm As New clsXml
                    objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    errlab.Text = objXm.GetLabelName("StatusMessage", "SuccessfullySaved")
                    errlab.Visible = True
                   Else
                    Response.Redirect("~/PresentationLayer/Error.aspx")
                End If
            End If
            Dim dsPriceIDall As DataSet = Session("salSetUp1View")
            salSetUp1View.DataSource = dsPriceIDall
            salSetUp1View.AllowPaging = True
                    'salSetUp1View.AllowSorting = True
            salSetUp1View.DataBind()
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            If dsPriceIDall.Tables(0).Rows.Count <> 0 Then
                salSetUp1View.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTAX-0013")
                salSetUp1View.HeaderRow.Cells(1).Font.Size = 8
                salSetUp1View.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTAX-0005")
                salSetUp1View.HeaderRow.Cells(2).Font.Size = 8
                salSetUp1View.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTAX-0006")
                salSetUp1View.HeaderRow.Cells(3).Font.Size = 8
                salSetUp1View.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Status")
                salSetUp1View.HeaderRow.Cells(4).Font.Size = 8
                salSetUp1View.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
                salSetUp1View.HeaderRow.Cells(5).Font.Size = 8
            End If
          
        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
        End If


    End Sub

    Protected Sub salSetUp1View_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles salSetUp1View.PageIndexChanging
        salSetUp1View.PageIndex = e.NewPageIndex
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Dim dsPriceIDall As DataSet = Session("salSetUp1View")
        salSetUp1View.DataSource = dsPriceIDall
        salSetUp1View.AllowPaging = True
                    'salSetUp1View.AllowSorting = True
        salSetUp1View.DataBind()
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        If dsPriceIDall.Tables(0).Rows.Count <> 0 Then
            salSetUp1View.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTAX-0013")
            salSetUp1View.HeaderRow.Cells(1).Font.Size = 8
            salSetUp1View.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTAX-0005")
            salSetUp1View.HeaderRow.Cells(2).Font.Size = 8
            salSetUp1View.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTAX-0006")
            salSetUp1View.HeaderRow.Cells(3).Font.Size = 8
            salSetUp1View.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Status")
            salSetUp1View.HeaderRow.Cells(4).Font.Size = 8
            salSetUp1View.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
            salSetUp1View.HeaderRow.Cells(5).Font.Size = 8
        End If
     
    End Sub

    Protected Sub salSetUp1View_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles salSetUp1View.RowEditing
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Dim ds As DataSet = Session("salSetUp1View")
        Dim page As Integer = salSetUp1View.PageIndex * salSetUp1View.PageSize
        Dim salid As String = Me.salSetUp1View.Rows(e.NewEditIndex).Cells(1).Text
        salid = Server.UrlEncode(salid)
        Dim time As String = Me.salSetUp1View.Rows(e.NewEditIndex).Cells(5).Text
        time = Server.UrlEncode(time)
        Dim strTempURL As String = "SalSetUpID=" + salid + "&time=" + time
        strTempURL = "~/PresentationLayer/masterrecord/modifySalTaxID1.aspx?" + strTempURL
        Response.Redirect(strTempURL)
    End Sub
    
#Region "check delete"
    Function check() As Integer

        Dim SalTaxIDEntity As New clsSalTaxID()
        If (Trim(Me.saltaxidbox.Text) <> "") Then
            SalTaxIDEntity.SalTaxID = Me.saltaxidbox.Text
        End If
        Dim sqldr As SqlDataReader = SalTaxIDEntity.Getsate()
        'if there are identical record then tishi error
        Dim i As Integer = 0
        While (sqldr.Read())
            i = 1
        End While
        Return i
    End Function
#End Region
   
    Protected Sub salSetUp1View_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles salSetUp1View.SelectedIndexChanged

    End Sub
End Class
