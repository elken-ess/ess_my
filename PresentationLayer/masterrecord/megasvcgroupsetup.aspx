<%@ Page Language="VB" AutoEventWireup="false" CodeFile="megasvcgroupsetup.aspx.vb" Inherits="PresentationLayer_masterrecord_megasvcgroupsetup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Service Internal Setup</title>
    <meta http-equiv="Content-Type" content="text/html; charset=gb2312"></meta>
    <LINK href="../css/style.css" type="text/css" rel="stylesheet"></LINK>
     <STYLE type="text/css">A:link { COLOR: #336666; TEXT-DECORATION: none }
	BODY { FONT-SIZE: 9pt; FONT-FAMILY: "Arial", "Verdana", "Tahoma"; BACKGROUND-COLOR: #ffffff }
	TD { FONT-SIZE: 9pt; FONT-FAMILY: Arial,Verdana,Tahoma }
	</STYLE>

</head>
<body>
    <form id="form1" runat="server">
    <div style="width: 100%">
        <table id="TABLE2" border="0" width ="100%">
            <tr>
                <td style="width: 100%; height: 13px">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title1.gif" width="5" /></td>
                            <td background="../graph/title_bg.gif" class="style2" width="100%">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="right" background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title_2.gif" width="5" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100%; height: 2px">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" class="style2" style="height: 1px" width="98%">
                                <font color="red">
                                    <asp:Label ID="addinfo" runat="server" Text="Label" Visible="false"></asp:Label></font></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <asp:Label ID="lblGroup" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="txtGroup" runat="server" Width=15% CssClass="textborder"></asp:TextBox>
        <asp:Label ID="lblModel" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="txtModel" runat="server" Width=15% CssClass="textborder" MaxLength="50"></asp:TextBox>
        <asp:Label ID="lblMonth" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="txtMonths" runat="server" Width=15% CssClass="textborder" MaxLength="50"></asp:TextBox>
        <asp:Label ID="lblstat" runat="server" Text="Label"></asp:Label>
        <asp:DropDownList ID="ddlstat" runat="server">
            <asp:ListItem>ACTIVE</asp:ListItem>
            <asp:ListItem>OBSOLETE</asp:ListItem>
        </asp:DropDownList>
        <asp:LinkButton ID="searchButton" runat="server"></asp:LinkButton>
        &nbsp; &nbsp;<asp:LinkButton ID="addButton" runat="server" PostBackUrl="~/PresentationLayer/masterrecord/megasvcgroupsetup_add.aspx"></asp:LinkButton>&nbsp;
        <br />
        <hr />
    
    </div>
        <asp:GridView ID="ctryView" runat="server" Width="100%" AllowPaging="True" AllowSorting="True" Font-Size="Smaller" AutoGenerateColumns="False" EnableModelValidation="True" DataKeyNames="ID" OnRowEditing="ctryView_RowEditing">
            <Columns>
                <asp:CommandField ShowEditButton="True" />  
                <asp:BoundField DataField="ID" HeaderText="ID" Visible="False" >
                <HeaderStyle Font-Size="Medium" />
                </asp:BoundField>
                <asp:BoundField DataField="Group" HeaderText="Group" >              
                <HeaderStyle Font-Size="8pt" />
                </asp:BoundField>
                <asp:BoundField DataField="model" HeaderText="Model" >              
                <HeaderStyle Font-Size="8pt" />
                </asp:BoundField>
                <asp:BoundField DataField="months" HeaderText="Months" >              
                <HeaderStyle Font-Size="8pt" />
                </asp:BoundField>
                <asp:BoundField DataField="interval" HeaderText="Interval" >              
                <HeaderStyle Font-Size="8pt" />
                </asp:BoundField>
                <asp:BoundField DataField="frequency" HeaderText="Frequency"  >              
                <HeaderStyle Font-Size="8pt" />
                </asp:BoundField>
                <asp:BoundField DataField="stat" HeaderText="Status" >              
                <HeaderStyle Font-Size="8pt" />
                </asp:BoundField>
            </Columns>
        </asp:GridView>
        <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="Label"></asp:Label>
    </form>
</body>
</html>
