Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Partial Class PresentationLayer_masterrecord_ModifyinstallBase_aspx
    Inherits System.Web.UI.Page
    Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Page.MaintainScrollPositionOnPostBack = True
        AjaxPro.Utility.RegisterTypeForAjax(GetType(PresentationLayer_masterrecord_ModifyinstallBase_aspx))



        If Not Page.IsPostBack Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try
            serialnotbox.Focus()

            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "05")
            Me.LinkButton1.Enabled = purviewArray(1)
            If purviewArray(2) = False Then
                Response.Redirect("~/PresentationLayer/logon.aspx")
            End If
            Try
                Dim str As String = Request.Params("rouid").ToString
            Catch ex As Exception
                Response.Redirect("~/PresentationLayer/logon.aspx")
            End Try


            'set value for LABEls
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            modelidlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0001")
            serialnoLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0002")
            customeridlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0003")
            CustomerjdeLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0004")
            Customernamelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0005")
            Contractedlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0006")
            freeserentitlelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0007")
            Installeddatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0008")
            producedatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0009")
            ProductClasslab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0010")
            WarrExpdatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0011")
            TechnicianIDlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0012")
            TechnicianTypelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0013")
            Sercenteridlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0014")
            SerTypeidlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0015")
            FreSerRemDatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0016")
            lastserdatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0017")
            lastremidatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0018")
            Statuslab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0019")
            Remarkslab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0020")
            Createdbylab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATBY")
            createbydatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
            ModifiedBylab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFBY")
            modifybydatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFDATE")
            Label8.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            Label9.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            jderoidlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0023")

            LinkButton1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Save")
            HyperLink1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Cancel")
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-ModifyInstalltl")

            Dim XmlTr As New clsXml
            XmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            modelidRFValid.ErrorMessage = XmlTr.GetLabelName("StatusMessage", "BB-HintMsg-MID")
            serialidRFValid.ErrorMessage = XmlTr.GetLabelName("StatusMessage", "BB-HintMsg-SEID")
            cusiderr.ErrorMessage = XmlTr.GetLabelName("StatusMessage", "BB-HintMsg-CUSTOMERID")
            inserr.ErrorMessage = XmlTr.GetLabelName("StatusMessage", "BB-HintMsg-INSTALLDATE")
            proerr.ErrorMessage = XmlTr.GetLabelName("StatusMessage", "BB-HintMsg-PRODUCEDATE")
            CompDateVal.ErrorMessage = XmlTr.GetLabelName("StatusMessage", "INVALID_DATE")
            CompDateVal2.ErrorMessage = XmlTr.GetLabelName("StatusMessage", "INVALID_DATE")

            sercenteriderr.ErrorMessage = XmlTr.GetLabelName("StatusMessage", "BB-HintMsg-sercenteriderr")

            'added by deyb 22-Aug
            HypCal.NavigateUrl = "javascript:DoCal(document.Modifyinstallform.Installeddatetbox);"
            HypCal2.NavigateUrl = "javascript:DoCal(document.Modifyinstallform.producedatetbox);"
            '===============

            'for modify istallbase contents
            Dim mdfyid As String = Request.Params("rouid").ToString()
            If mdfyid.Trim() <> "" Then
                Dim installEntity As New ClsInstalledBase()
                Dim calendatetime As New clsCommonClass()
                installEntity.RoID = mdfyid
                Dim edtReader As SqlDataReader = installEntity.GetinstallbaseDetailsByRoID()
                If edtReader.Read() Then
                    Dim Stat As New clsrlconfirminf()
                    Dim installbond As New clsCommonClass
                    Dim count As Integer
                    Dim statid As String
                    Dim statnm As String
                    rouidtbox.Text = edtReader.GetValue(0).ToString()
                    rouidtbox.ReadOnly = True

                    Dim rank As String = Session("login_rank")
                    installbond.rank = rank
                    If rank <> 0 Then
                        installbond.spctr = Session("login_ctryID")
                        installbond.spstat = Session("login_cmpID")
                        installbond.sparea = Session("login_svcID")
                    End If

                    'bond  model id and name
                    Dim modelds As New DataSet
                    modelds = installbond.Getcomidname("BB_MASMOTY_IDNAME")
                    If modelds.Tables.Count <> 0 Then
                        databondsModel(modelds, modelidtdrp, edtReader.GetValue(1).ToString())
                    End If
                    modelidtdrp.Text = edtReader.GetValue(1).ToString()
                    modelidtdrp.Enabled = True
                    serialnotbox.Text = edtReader.GetValue(2).ToString() 'serial  no
                    cusid.Text = edtReader.GetValue(4).ToString()

                    'set customer name


                    Dim cusReader As SqlDataReader = installEntity.selcusname(Trim(cusid.Text), Session("login_ctryID"))
                    If cusReader.Read() Then
                        Customerjdetbox.Text = cusReader.GetValue(0).ToString()
                        Customernametbox.Text = cusReader.GetValue(1).ToString()
                    End If

                    'set the RO JDE ID
                    If Trim(edtReader.GetValue(31).ToString()) <> "" Then
                        jderoidtbox.Text = edtReader.GetValue(31).ToString()
                    End If

                    'set contracted dropdownlist
                    Dim contractpar As ArrayList = New ArrayList
                    contractpar = Stat.searchconfirminf("YESNO")
                    count = 0
                    For count = 0 To contractpar.Count - 1
                        statid = contractpar.Item(count)
                        statnm = contractpar.Item(count + 1)
                        Contractdrpd.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                        freeserentitledrpd.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                        If statid.Equals(edtReader.GetValue(5).ToString()) Then
                            Contractdrpd.Items(count / 2).Selected = True
                        End If
                        If statid.Equals(edtReader.GetValue(6).ToString()) Then
                            freeserentitledrpd.Items(count / 2).Selected = True
                        End If
                        count = count + 1
                    Next

                    If Trim(edtReader.GetValue(7).ToString()) <> "" Then
                        Installeddatetbox.Text = edtReader.GetValue(7).ToString().Substring(0, 10)

                    End If
                    If Trim(edtReader.GetValue(8).ToString()) <> "" Then
                        producedatetbox.Text = edtReader.GetValue(8).ToString().Substring(0, 10)
                    End If

                    'set the product class dropdownlist
                    Dim productcl As ArrayList = New ArrayList
                    productcl = Stat.searchconfirminf("PRODUCTCL")
                    count = 0
                    For count = 0 To productcl.Count - 1
                        statid = productcl.Item(count)
                        statnm = productcl.Item(count + 1)
                        productclassdrpd.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                        If statid.Equals(edtReader.GetValue(9).ToString()) Then
                            productclassdrpd.Items(count / 2).Selected = True
                        End If
                        count = count + 1
                    Next
                    If edtReader.GetValue(10).ToString().Trim <> "" Then
                        WarrExpdatetbox.Text = edtReader.GetValue(10).ToString().Substring(0, 10)

                    End If




                    'set the technician type dropdownlist
                    Dim techtypepar As ArrayList = New ArrayList
                    techtypepar = Stat.searchconfirminf("TECHTYPE")
                    count = 0
                    For count = 0 To techtypepar.Count - 1
                        statid = techtypepar.Item(count)
                        statnm = techtypepar.Item(count + 1)
                        TechnicianTypedrpd.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                        If statid.Equals(edtReader.GetValue(12).ToString()) Then
                            TechnicianTypedrpd.Items(count / 2).Selected = True
                        End If
                        count = count + 1
                    Next


                    Dim service As New clsCommonClass
                    If rank = "7" Then 'country
                        service.spctr = Session("login_ctryID")
                    End If
                    If rank = "8" Then 'company
                        service.spctr = Session("login_ctryID")
                        service.spstat = Session("login_cmpID")
                    End If
                    If rank = "9" Then 'company
                        service.spctr = Session("login_ctryID")
                        service.spstat = Session("login_cmpID")
                        service.sparea = Session("login_svcID")
                    End If
                    service.rank = rank
                    ''bond service center id and name
                    Dim serviceds As New DataSet
                    serviceds = service.Getcomidname("BB_MASSVRC_IDNAME")
                    If serviceds.Tables.Count <> 0 Then
                        databonds(serviceds, Me.Sercenteriddrp)
                    End If
                    Sercenteriddrp.Text = edtReader.GetValue(13).ToString()


                    'bond  technician id and name
                    Dim tech As New DataSet
                    installbond.spctr = Session("login_ctryID")
                    installbond.spstat = Session("login_cmpID")
                    installbond.sparea = Sercenteriddrp.SelectedValue
                    tech = installbond.Getcomidname("BB_MASTECH_NAMEID")
                    If tech.Tables.Count <> 0 Then
                        databondsNameID(tech, Technicianiddrp)
                    End If

                    Dim aa As String = edtReader.GetValue(11).ToString()
                    If Trim(aa) = "" Then
                        Technicianiddrp.Text = " "
                    Else
                        Technicianiddrp.Text = edtReader.GetValue(11).ToString()
                    End If


                    'bond  service type id and name'there are some problems in this bonds????
                    Dim sertype As New DataSet
                    sertype = installbond.Getcomidname("BB_MASSRCT_IDNAME")
                    If sertype.Tables.Count <> 0 Then
                        databonds(sertype, SerTypeiddrpd)
                    End If

                    Dim bb As String = edtReader.GetValue(14).ToString()
                    If Trim(bb) = "" Then
                        SerTypeiddrpd.Text = " "
                    Else
                        SerTypeiddrpd.SelectedValue = edtReader.GetValue(14).ToString()
                    End If

                    'SerTypeiddrpd.SelectedValue = edtReader.GetValue(14).ToString()
                    If edtReader.GetValue(15).ToString().Trim <> "" Then
                        FreSerRemDatetbox.Text = edtReader.GetValue(15).ToString().Substring(0, 10)
                    End If
                    If edtReader.GetValue(16).ToString().Trim <> "" Then
                        Me.lastserdatetbox.Text = edtReader.GetValue(16).ToString().Substring(0, 10)
                    End If
                    If edtReader.GetValue(17).ToString().Trim <> "" Then
                        lastremidatetbox.Text = edtReader.GetValue(17).ToString().Substring(0, 10)

                    End If


                    'set status dropdownlist
                    Dim statParam As ArrayList = New ArrayList
                    statParam = Stat.searchconfirminf("STATUS")
                    count = 0
                    For count = 0 To statParam.Count - 1
                        statid = statParam.Item(count)
                        statnm = statParam.Item(count + 1)
                        If statid.Equals("ACTIVE") Or statid.Equals("OBSOLETE") Or statid.Equals("DELETE") Or statid.Equals("SUSPENDED") Or statid.Equals("TERMINATED") Then
                            statusdrpd.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                        End If
                        If statid.Equals(edtReader.GetValue(18).ToString()) Then
                            statusdrpd.SelectedValue = edtReader.GetValue(18).ToString()
                            'statusdrpd.Items(count / 2).Selected = True
                        End If
                        count = count + 1
                    Next
                    If Convert.ToString(purviewArray(4)).ToUpper = "FALSE" Then
                        Me.statusdrpd.Items.Remove(Me.statusdrpd.Items.FindByValue("DELETE"))
                    End If

                    Remarkstbox.Text = edtReader.GetValue(19).ToString()

                    '�����ֶ�
                    'MROU_BATCH.Text = edtReader.GetValue(20.ToString()
                    ' MROU_APTDT.Text = edtReader.GetValue(21.ToString()
                    'MROU_NCADT.Text = edtReader.GetValue(22.ToString()
                    'MROU_NSVDT.Text = edtReader.GetValue(23).ToString()
                    'MROU_RELFG.Text = edtReader.GetValue(24).ToString()
                    'MROU_CSTAT.Text = edtReader.GetValue(25).ToString()
                    'MROU_CAREM.Text = edtReader.GetValue(26).ToString()
                    Dim Rcreat As New clsCommonClass
                    Dim Rcreatds As New DataSet()

                    Dim Rcreatby As String = edtReader.GetValue(27).ToString().ToUpper()
                    If Rcreatby <> "ADMIN" Then

                        Rcreat.userid() = Rcreatby

                        Rcreatds = Rcreat.Getuseridname("BB_MASUSER_SelByIDName")

                        Me.CreatedBytbox.Text = Rcreatds.Tables(0).Rows(0).Item(0)
                    Else
                        Me.CreatedBytbox.Text = "ADMIN-ADMIN"
                    End If
                    'CreatedBytbox.Text = edtReader.GetValue(27).ToString()
                    CreatedBytbox.ReadOnly = True
                    createbydatetbox.Text = edtReader.GetValue(28).ToString()
                    createbydatetbox.ReadOnly = True

                    Rcreat.userid() = edtReader.GetValue(29).ToString()
                    Rcreatds = Rcreat.Getuseridname("BB_MASUSER_SelByIDName")

                    Dim userIDNamestr As String = Rcreatds.Tables(0).Rows(0).Item(0)

                    ModifiedBytbox.Text = userIDNamestr
                    ModifiedBytbox.ReadOnly = True

                    modifybydatetbox.Text = edtReader.GetValue(30).ToString()
                    modifybydatetbox.ReadOnly = True
                End If
            End If
            Dim type As String = Request.QueryString("type")
            If Trim(type) = "edit" Then
                cusid.Text = Request.QueryString("searchcustid")
                Customerjdetbox.Text = Request.QueryString("searchcustjde")
                Customernametbox.Text = Request.QueryString("searchcustname")
                modelidtdrp.Text = Request.QueryString("searchmodelid")
                serialnotbox.Text = Request.QueryString("searchserialno")
                Contractdrpd.Text = Request.QueryString("searchContract")
                Installeddatetbox.Text = Request.QueryString("searchInstalleddate")
                producedatetbox.Text = Request.QueryString("searchproducedate")

                freeserentitledrpd.Text = Request.QueryString("searchfreeserentitle")
                productclassdrpd.Text = Request.QueryString("searchproductclass")
                TechnicianTypedrpd.Text = Request.QueryString("searchTechnicianType")
                Sercenteriddrp.Text = Request.QueryString("searchSercenterid")
                SerTypeiddrpd.Text = Request.QueryString("searchSerType")
                WarrExpdatetbox.Text = Request.QueryString("searchWarrExpdate")
                FreSerRemDatetbox.Text = Request.QueryString("searchFrSRemDt")
                statusdrpd.Text = Request.QueryString("searchstatus")
                Remarkstbox.Text = Request.QueryString("searchRemarks")

                Dim tech As New DataSet
                Dim lstrTechnicianID As String = Request.QueryString("searchTechnicianid")
                Dim installbond As New clsCommonClass

                installbond.sparea = Sercenteriddrp.SelectedItem.Value.ToString()
                installbond.spctr = Session("login_ctryID")
                installbond.spstat = Session("login_cmpID")
                installbond.rank = Session("login_rank")

                tech = installbond.Getcomidname("BB_MASTECH_IDNAME")
                If tech.Tables.Count <> 0 Then
                    databondsNameID(tech, Technicianiddrp)
                End If

                If lstrTechnicianID <> "" Then
                    Technicianiddrp.Text = lstrTechnicianID
                End If

            End If
            'added by deyb
            Dim xRQS As String = Request.Params("modcust")
            Dim CID As String = Request.Params("customid")
            Dim Pref As String = Request.Params("prefix")
            If xRQS = "" Then
                'HyperLink1.NavigateUrl = "~/PresentationLayer/masterrecord/InstalledBase.aspx"
                'added by deyb 14-08-2006
                Dim xPrm As String = Request.Params("search")
                If xPrm <> "" Or xPrm <> Nothing Then
                    HyperLink1.NavigateUrl = "javascript:history.back();"
                    LinkButton1.Enabled = False
                    LinkButton2.Enabled = False
                Else
                    HyperLink1.NavigateUrl = "~/PresentationLayer/masterrecord/InstalledBase.aspx"
                End If
                '=================
            Else
                Dim url As String = "~/PresentationLayer/masterrecord/modifycustomer.aspx?"
                url &= "custid=" & CID & "&"
                url &= "Prefix=" & Pref & "&"
                url &= "modisy=" & 1
                'Response.Redirect(url)
                HyperLink1.NavigateUrl = url
            End If
            '=========
        End If


    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton1.Click
        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")

        RequestFromValue()
        VerifyRemWarrDate()

        Dim temparr As Array = Request.Form("producedatetbox").Split("/") '.Text.Split("/")
        Dim strpro As String
        Dim strinst As String
        strpro = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
        temparr = Request.Form("Installeddatetbox").Split("/") 'Installeddatetbox.Text.Split("/")
        strinst = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
        If Convert.ToDateTime(strinst) < Convert.ToDateTime(strpro) Then
            errlab.Text = objXm.GetLabelName("StatusMessage", "BB-HintMsg-COMPAREPROINSDATE")
            errlab.Visible = True
            MessageBox1.Alert(errlab.Text)
            Return
        End If


        If Me.statusdrpd.SelectedValue = "DELETE" Then
            If check() = 1 Then
                errlab.Text = objXm.GetLabelName("StatusMessage", "rou_CHECKDELSTAT")
                errlab.Visible = True
                MessageBox1.Alert(errlab.Text)
                Return
            End If
        End If

        Dim msginfo As String = objXm.GetLabelName("StatusMessage", "SAVEORNOT")
        Dim msgtitle As String = objXm.GetLabelName("StatusMessage", "SAVETITLE")
        MessageBox1.Confirm(msginfo)


    End Sub

    Sub GetTechnicianDropdown()

        Dim Technician As New clsCommonClass
        Dim rank As String = Session("login_rank")
        Technician.spctr = Session("login_ctryID")
        Technician.spstat = Session("login_cmpID")
        Technician.sparea = Sercenteriddrp.SelectedValue
        Technician.rank = rank


        Dim dsTechnician As New DataSet
        Technicianiddrp.Items.Clear()
        dsTechnician = Technician.Getcomidname("BB_MASTECH_NAMEID")
        If dsTechnician.Tables.Count <> 0 Then
            databondsNameID(dsTechnician, Me.Technicianiddrp)
        End If
        'Me.Technicianiddrp.Items.Insert(0, "")
        'Me.Technicianiddrp.SelectedIndex = 0



    End Sub

    Sub RequestFromValue()
        Installeddatetbox.Text = Request.Form("Installeddatetbox")
        producedatetbox.Text = Request.Form("producedatetbox")
        FreSerRemDatetbox.Text = Request.Form("FreSerRemDatetbox")

        Dim lstrTechnicianID As String = Request.Form("Technicianiddrp")

        GetTechnicianDropdown()

        If lstrTechnicianID <> "" Then
            Me.Technicianiddrp.SelectedValue = lstrTechnicianID
        End If
    End Sub


#Region " bonds"

    Public Function databondsNameID(ByVal ds As DataSet, ByVal dropdown As DropDownList) As ArrayList
        dropdown.Items.Clear()
        Dim row As DataRow
        Dim infon As ArrayList = New ArrayList()
        dropdown.Items.Add(" ")
        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            NewItem.Text = Trim(row("name") & "-" & row("id"))
            NewItem.Value = Trim(row("id"))
            dropdown.Items.Add(NewItem)
            infon.Add(NewItem.Value)
        Next
        Return infon
    End Function


    Public Function databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList) As ArrayList
        dropdown.Items.Clear()
        Dim row As DataRow
        Dim infon As ArrayList = New ArrayList()
        dropdown.Items.Add(" ")
        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            NewItem.Text = Trim(row("id") & "-" & row("name"))
            NewItem.Value = Trim(row("id"))
            dropdown.Items.Add(NewItem)
            infon.Add(NewItem.Value)
        Next
        Return infon
    End Function

    Public Function databondsModel(ByVal ds As DataSet, ByVal dropdown As DropDownList, ByVal modelid As String) As ArrayList
        dropdown.Items.Clear()
        Dim row As DataRow
        Dim infon As ArrayList = New ArrayList()
        dropdown.Items.Add(modelid)
        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            NewItem.Text = Trim(row("id") & "-" & row("name"))
            NewItem.Value = Trim(row("id"))
            dropdown.Items.Add(NewItem)
            infon.Add(NewItem.Value)
        Next
        Return infon
    End Function
#End Region

    Protected Sub freeserentitledrpd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles freeserentitledrpd.SelectedIndexChanged


        'ChangeServiceType

    End Sub



    Protected Sub MessageBox1_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox1.GetMessageBoxResponse
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")

        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
            VerifyRemWarrDate()
            Dim installbaseEntity As New ClsInstalledBase()
            Dim calendatetime As New clsCommonClass()
            installbaseEntity.RoID = rouidtbox.Text
            If (Trim(modelidtdrp.SelectedValue().ToString()) <> "") Then
                installbaseEntity.ModelID = modelidtdrp.SelectedItem.Value.ToString()
            End If
            If (Trim(serialnotbox.Text) <> "") Then
                installbaseEntity.SerialNo = serialnotbox.Text.ToUpper()
            End If
            If (Trim(cusid.Text) <> "") Then
                installbaseEntity.CustomerID = cusid.Text.ToString()
            End If

            If (Trim(Session("LOGIN_CTRYID")) <> "") Then
                installbaseEntity.CustomerPre = Session("LOGIN_CTRYID")
            End If

            'If (Trim(Customerjdetbox.Text) <> "") Then
            'installbaseEntity.CustomerJde = jderoidtbox.Text.ToUpper()
            'End If

            If (Trim(Contractdrpd.SelectedValue().ToString()) <> "") Then
                installbaseEntity.Contracted = Contractdrpd.SelectedItem.Value.ToString()
            End If
            If (Trim(Installeddatetbox.Text) <> "") Then
                'installbaseEntity.Installdate = calendatetime.DatetoDatabase(Installeddatetbox.Text)
                Dim temparr As Array = Request.Form("Installeddatetbox").Split("/")
                installbaseEntity.Installdate = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
            End If
            If (Trim(producedatetbox.Text) <> "") Then
                'installbaseEntity.ProductionDate = calendatetime.DatetoDatabase(producedatetbox.Text)
                Dim temparr As Array = Request.Form("producedatetbox").Split("/")
                installbaseEntity.ProductionDate = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
            End If
            If (Technicianiddrp.SelectedValue().ToString() <> "") Then
                installbaseEntity.TechID = Technicianiddrp.SelectedItem.Value.ToString()
            End If
            If (freeserentitledrpd.SelectedValue().ToString() <> "") Then
                installbaseEntity.Freeserviceen = freeserentitledrpd.SelectedItem.Value.ToString()
            End If
            If (productclassdrpd.SelectedValue().ToString() <> "") Then
                installbaseEntity.ProductClass = productclassdrpd.SelectedItem.Value.ToString()
            End If
            If (TechnicianTypedrpd.SelectedValue().ToString() <> "") Then
                installbaseEntity.TechType = TechnicianTypedrpd.SelectedItem.Value.ToString()
            End If
            If (Sercenteriddrp.SelectedValue().ToString() <> "") Then
                installbaseEntity.SerCenterID = Sercenteriddrp.SelectedItem.Value.ToString()
            End If
            If (SerTypeiddrpd.SelectedValue().ToString() <> "") Then
                installbaseEntity.SerTypeID = SerTypeiddrpd.SelectedItem.Value.ToString()
            End If
            If (Trim(WarrExpdatetbox.Text) <> "") Then
                'installbaseEntity.WarrantyExDate = calendatetime.DatetoDatabase(WarrExpdatetbox.Text)
                Dim temparr As Array = WarrExpdatetbox.Text.Split("/")
                installbaseEntity.WarrantyExDate = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
            End If
            If (FreSerRemDatetbox.Text <> "") Then
                'installbaseEntity.FrSerRemiDate = calendatetime.DatetoDatabase(FreSerRemDatetbox.Text)
                Dim temparr As Array = FreSerRemDatetbox.Text.Split("/")
                installbaseEntity.FrSerRemiDate = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
            End If
            If (lastserdatetbox.Text <> "") Then
                'installbaseEntity.LastSerDate = calendatetime.DatetoDatabase(lastserdatetbox.Text)
                Dim temparr As Array = lastserdatetbox.Text.Split("/")
                installbaseEntity.LastSerDate = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
            End If
            If (lastremidatetbox.Text <> "") Then
                'installbaseEntity.LastRemiDate = calendatetime.DatetoDatabase(lastremidatetbox.Text)
                Dim temparr As Array = lastremidatetbox.Text.Split("/")
                installbaseEntity.LastRemiDate = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
            End If

            If (statusdrpd.SelectedValue().ToString() <> "") Then
                installbaseEntity.Status = statusdrpd.SelectedItem.Value.ToString()
            End If
            If (Trim(Remarkstbox.Text) <> "") Then
                installbaseEntity.Remarks = Remarkstbox.Text.ToUpper()
            End If
            If (Trim(CreatedBytbox.Text) <> "") Then
                installbaseEntity.CreatedBy = Session("userID").ToString().ToUpper
            End If
            If (Trim(ModifiedBytbox.Text) <> "") Then
                installbaseEntity.ModifiedBy = Session("userID").ToString().ToUpper
            End If

            If (freeserentitledrpd.SelectedValue().ToString().Trim = "Y" And Me.SerTypeiddrpd.SelectedItem.Text.Trim = "") Then
                Dim msg As String = objXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-freeserTypeerr")
                Dim msgBox As New MessageBox(Me)
                msgBox.Show(msg)
                Return
            End If

            If Format(Request.Form("Installeddatetbox"), "dd/MM/yyyy") > Format(Request.Form("producedatetbox"), "dd/MM/yyyy") Then

                errlab.Text = objXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-COMPAREPROINSDATE")
                errlab.Visible = True
                MessageBox1.Alert(errlab.Text)
            Else

                Dim dupCount As Integer = installbaseEntity.GetDuplicatedinstall()
                If dupCount.Equals(-1) Then


                    errlab.Text = objXmlTr.GetLabelName("StatusMessage", "DUPINSTALLIDORNM")
                    errlab.Visible = True
                    MessageBox1.Alert(errlab.Text)
                    Exit Sub
                End If


                installbaseEntity.logsvcid = Session("login_svcID")
                installbaseEntity.ipadress = Request.UserHostAddress.ToString()
                Dim updinstallbase As Integer = installbaseEntity.Update()

                objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                If updinstallbase = 0 Then
                    errlab.Text = objXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Suc")
                    errlab.Visible = True
                    MessageBox1.Alert(errlab.Text)
                Else
                    Dim msg As String = objXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Fail")
                    Dim msgBox As New MessageBox(Me)
                    msgBox.Show(msg)
                    Response.Redirect("~/PresentationLayer/Error.aspx")
                End If
            End If
        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
        End If
        Installeddatetbox.Text = Request.Form("Installeddatetbox")
        producedatetbox.Text = Request.Form("producedatetbox")
    End Sub

    Sub ChangeServiceType()
        Me.RequestFromValue()
        Dim lstrInstallDate As String
        Dim FreeServiceID As String
        Dim CountryID, FreeEntitled As String

        FreeEntitled = freeserentitledrpd.SelectedItem.Value.ToString()
        lstrInstallDate = Installeddatetbox.Text
        FreeServiceID = SerTypeiddrpd.SelectedItem.Value.ToString()
        CountryID = Session("login_ctryID")

        If FreeEntitled = "Y" Then
            SerTypeiddrpd.Enabled = True
            Dim instaEntity As New ClsInstalledBase()
            If Trim(lstrInstallDate) <> "" And FreeServiceID <> "" Then
                Dim datestyle1 As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
                System.Threading.Thread.CurrentThread.CurrentCulture = datestyle1

                FreSerRemDatetbox.Text = CalculateServiceReminderDate(CountryID, lstrInstallDate, FreeServiceID, FreeEntitled)
            End If
        Else
            SerTypeiddrpd.Enabled = False
            SerTypeiddrpd.SelectedValue = " "
            FreSerRemDatetbox.Text = ""
        End If
    End Sub



    Protected Sub SerTypeiddrpd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles SerTypeiddrpd.SelectedIndexChanged
        'ChangeServiceType
    End Sub

    Protected Sub LinkButton2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton2.Click
        RequestFromValue()

        Dim svcid As String = Session("login_svcID")
        svcid = Server.UrlEncode(svcid)
        Dim countryid As String = Session("login_ctryID")
        countryid = Server.UrlEncode(countryid)
        Dim cmpid As String = Session("login_cmpID")
        cmpid = Server.UrlEncode(cmpid)
        Dim rank As String = Session("login_rank")
        rank = Server.UrlEncode(rank)
        Dim type As String = "edit"
        type = Server.UrlEncode(type)
        Dim searchmodelid As String = Trim(modelidtdrp.SelectedValue().ToString())
        searchmodelid = Server.UrlEncode(searchmodelid)
        Dim searchserialno As String = Trim(serialnotbox.Text)
        searchserialno = Server.UrlEncode(searchserialno)
        Dim searchContract As String = Trim(Contractdrpd.SelectedValue().ToString())
        searchContract = Server.UrlEncode(searchContract)
        Dim searchInstalleddate As String = Trim(Request.Form("Installeddatetbox"))
        searchInstalleddate = Server.UrlEncode(searchInstalleddate)
        Dim searchproducedate As String = Trim(Request.Form("producedatetbox"))
        searchproducedate = Server.UrlEncode(searchproducedate)
        Dim searchTechnicianid As String = Technicianiddrp.SelectedValue().ToString()
        searchTechnicianid = Server.UrlEncode(searchTechnicianid)
        Dim searchfreeserentitle As String = freeserentitledrpd.SelectedValue().ToString()
        searchfreeserentitle = Server.UrlEncode(searchfreeserentitle)
        Dim searchproductclass As String = productclassdrpd.SelectedValue().ToString()
        searchproductclass = Server.UrlEncode(searchproductclass)
        Dim searchTechnicianType As String = TechnicianTypedrpd.SelectedValue().ToString()
        searchTechnicianType = Server.UrlEncode(searchTechnicianType)
        Dim searchSercenterid As String = Sercenteriddrp.SelectedValue().ToString()
        searchSercenterid = Server.UrlEncode(searchSercenterid)
        Dim searchSerType As String = SerTypeiddrpd.SelectedValue().ToString()
        searchSerType = Server.UrlEncode(searchSerType)
        Dim searchWarrExpdate As String = Trim(WarrExpdatetbox.Text)
        searchWarrExpdate = Server.UrlEncode(searchWarrExpdate)
        Dim searchFrSRemDt As String = Trim(FreSerRemDatetbox.Text)
        searchFrSRemDt = Server.UrlEncode(searchFrSRemDt)
        Dim searchstatus As String = statusdrpd.SelectedValue().ToString()
        searchstatus = Server.UrlEncode(searchstatus)
        Dim searchRemarks As String = Trim(Remarkstbox.Text)
        searchRemarks = Server.UrlEncode(searchRemarks)
        Dim rouid As String = Request.Params("rouid").ToString()
        rouid = Server.UrlEncode(rouid)
        Dim strTempURL As String = "svcid=" + svcid + "&countryid=" + countryid + "&cmpid=" + cmpid + "&rank=" + rank + "&type=" + type + "&searchmodelid=" + searchmodelid + "&searchserialno=" + searchserialno + "&searchContract=" + searchContract + "&searchInstalleddate=" + searchInstalleddate + "&searchproducedate=" + searchproducedate + "&searchTechnicianid=" + searchTechnicianid + "&searchfreeserentitle=" + searchfreeserentitle + "&searchproductclass=" + searchproductclass + "&searchTechnicianType=" + searchTechnicianType + "&searchSercenterid=" + searchSercenterid + "&searchSerType=" + searchSerType + "&searchWarrExpdate=" + searchWarrExpdate + "&searchFrSRemDt=" + searchFrSRemDt + "&searchstatus=" + searchstatus + "&searchRemarks=" + searchRemarks + "&rouid=" + rouid
        strTempURL = "~/PresentationLayer/masterrecord/Instsearchcustomer.aspx?" + strTempURL
        Response.Redirect(strTempURL)
    End Sub
#Region "check delete"
    Function check() As Integer
        Dim installbaseEntity As New ClsInstalledBase()
        If (Trim(rouidtbox.Text) <> "") Then
            installbaseEntity.RoID = rouidtbox.Text
        End If
        Dim sqldr As SqlDataReader = installbaseEntity.Getroudel()
        Dim i As Integer = 0
        While (sqldr.Read())
            i = 1
        End While
        Return i
    End Function
#End Region

    Protected Sub Sercenteriddrp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Sercenteriddrp.SelectedIndexChanged
        'bond  technician id and name
        'Dim tech As New DataSet
        'Dim installbond As New clsCommonClass
        'installbond.sparea = Sercenteriddrp.SelectedItem.Value.ToString()
        'installbond.spctr = Session("login_ctryID")
        'installbond.spstat = Session("login_cmpID")
        'installbond.rank = Session("login_rank")
        'tech = installbond.Getcomidname("BB_MASTECH_IDNAME")

        'If tech.Tables.Count <> 0 Then
        '    databonds(tech, Technicianiddrp)
        'End If
        'Installeddatetbox.Text = Request.Form("Installeddatetbox")
        'producedatetbox.Text = Request.Form("producedatetbox")
    End Sub

    Protected Sub InstalleddateCalendar_SelectedDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles InstalleddateCalendar.SelectedDateChanged

        Dim installEntity As New ClsInstalledBase()
        If (Trim(modelidtdrp.SelectedValue().ToString()) <> "") Then
            installEntity.ModelID = modelidtdrp.SelectedItem.Value.ToString()
        End If
        Dim days As Integer = installEntity.GetWardays()
        Dim warrdate As DateTime

        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        'Installeddatetbox.Text = Calendarinstall.SelectedDate.Date.ToString().Substring(0, 10)
        Dim installdate As Date = Convert.ToDateTime(Installeddatetbox.Text)
        'warrdate = DateAdd(DateInterval.Day, days, Calendarinstall.SelectedDate.Date)
        warrdate = DateAdd(DateInterval.Day, days, installdate)
        WarrExpdatetbox.Text = Convert.ToString(warrdate).Substring(0, 10)


        If freeserentitledrpd.SelectedItem.Value.ToString() = "Y" Then
            SerTypeiddrpd.Enabled = True
            If Trim(Installeddatetbox.Text) <> "" And SerTypeiddrpd.SelectedValue().ToString() <> "" Then
                installEntity.SerTypeID = SerTypeiddrpd.SelectedItem.Value.ToString()
                Dim REdays As Integer = installEntity.GetReminderdays()
                Dim remdate As DateTime
                remdate = DateAdd(DateInterval.Day, REdays, installdate)
                FreSerRemDatetbox.Text = Convert.ToString(remdate).Substring(0, 10)
            End If
        Else
            SerTypeiddrpd.Enabled = False
            SerTypeiddrpd.SelectedValue = " "
            FreSerRemDatetbox.Text = ""
        End If

        'Dim script As String = "self.location='#svcctr';"
        'Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "svcctr", script, True)

    End Sub

    Protected Sub InstalleddateCalendar_CalendarVisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles InstalleddateCalendar.CalendarVisibleChanged

        productclassdrpd.Visible = Not InstalleddateCalendar.CalendarVisible
        Me.Sercenteriddrp.Visible = Not InstalleddateCalendar.CalendarVisible

    End Sub

    Protected Sub Customernametbox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Customernametbox.TextChanged

    End Sub

    Protected Sub producedateCalendar_CalendarVisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles producedateCalendar.CalendarVisibleChanged
        Me.Technicianiddrp.Visible = Not producedateCalendar.CalendarVisible
        Me.TechnicianTypedrpd.Visible = Not producedateCalendar.CalendarVisible
        Me.SerTypeiddrpd.Visible = Not producedateCalendar.CalendarVisible
    End Sub

    Protected Sub producedateCalendar_SelectedDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles producedateCalendar.SelectedDateChanged
        'Dim script As String = "self.location='#svcctr';"
        'Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "svcctr", script, True)

    End Sub

    Sub VerifyRemWarrDate()
        Dim installEntity As New ClsInstalledBase()
        If (Trim(modelidtdrp.SelectedValue().ToString()) <> "") And Request.Form("Installeddatetbox") <> "" Then
            installEntity.ModelID = modelidtdrp.SelectedItem.Value.ToString()
            installEntity.logctrid = Session("login_ctryid")
            Dim days As Integer = installEntity.GetWardays()
            Dim warrdate As DateTime

            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
            'Installeddatetbox.Text = Calendarinstall.SelectedDate.Date.ToString().Substring(0, 10)
            Dim installdate As Date = Convert.ToDateTime(Request.Form("Installeddatetbox"))
            'warrdate = DateAdd(DateInterval.Day, days, Calendarinstall.SelectedDate.Date)
            warrdate = DateAdd(DateInterval.Day, days, installdate)
            WarrExpdatetbox.Text = Convert.ToString(warrdate).Substring(0, 10)
            'SET MROU_NCADT
            'TextBox1.Text = DateAdd(DateInterval.Day, 240, installdate)
        End If

        If freeserentitledrpd.SelectedItem.Value.ToString() = "Y" Then
            SerTypeiddrpd.Enabled = True
            SerTypeiddrpd.SelectedIndex = 1
            Dim instaEntity As New ClsInstalledBase()
            If Trim(Request.Form("Installeddatetbox")) <> "" And SerTypeiddrpd.SelectedValue().ToString() <> "" Then
                Dim datestyle1 As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
                System.Threading.Thread.CurrentThread.CurrentCulture = datestyle1
                instaEntity.SerTypeID = SerTypeiddrpd.SelectedItem.Value.ToString()
                instaEntity.logctrid = Session("login_ctryid")
                Dim Nodays As Integer = instaEntity.GetReminderdays()
                Dim remdate As DateTime
                Dim installeddate As Date = Convert.ToDateTime(Request.Form("Installeddatetbox"))
                remdate = DateAdd(DateInterval.Day, Nodays, installeddate)
                FreSerRemDatetbox.Text = Convert.ToString(remdate).Substring(0, 10)
            End If

        Else
            SerTypeiddrpd.Enabled = False
            SerTypeiddrpd.SelectedValue = " "
            FreSerRemDatetbox.Text = ""
            Installeddatetbox.Text = Request.Form("Installeddatetbox")
            producedatetbox.Text = Request.Form("producedatetbox")
        End If

    End Sub

    Protected Sub Installeddatetbox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Installeddatetbox.TextChanged
        VerifyRemWarrDate()
    End Sub

    Function CalculateServiceReminderDate(ByVal CountryID As String, ByVal InstallDate As String, ByVal FreeServiceID As String, ByVal FreeServiceEntitled As String) As String
        Dim FreSerRemDate As String
        FreSerRemDate = ""
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        If FreeServiceEntitled = "Y" Then
            If Trim(InstallDate) <> "" And FreeServiceID.Trim <> "" Then
                Dim instaEntity As New ClsInstalledBase()

                instaEntity.SerTypeID = FreeServiceID
                instaEntity.logctrid = CountryID

                Dim days As Integer = instaEntity.GetReminderdays()

                Dim SS As DateTime
                Dim temparr As Array = InstallDate.Split("/")
                SS = temparr(2) + "-" + temparr(1) + "-" + temparr(0)

                Dim remdate As DateTime
                remdate = DateAdd(DateInterval.Day, days, SS.Date)
                'FreSerRemDatetbox.Text = Convert.ToString(remdate).Substring(0, 10)
                FreSerRemDate = Convert.ToString(remdate).Substring(0, 10)
            End If
        End If
        CalculateServiceReminderDate = FreSerRemDate

    End Function

#Region "Ajax"


    <AjaxPro.AjaxMethod()> _
        Function GetTechnicianList(ByVal CountryID As String, ByVal CompanyID As String, ByVal ServiceCenterID As String, ByVal Rank As String) As ArrayList

        Dim Technician As New clsCommonClass

        Technician.spctr = CountryID
        Technician.spstat = CompanyID
        Technician.sparea = ServiceCenterID
        Technician.rank = Rank


        Dim dsTechnician As New DataSet
        dsTechnician = Technician.Getcomidname("BB_MASTECH_NAMEID")


        Dim strTemp As String
        Dim Listas As New ArrayList(dsTechnician.Tables(0).Rows.Count)
        Dim i As Integer = 0
        Listas.Add("")
        Try
            For i = 0 To dsTechnician.Tables(0).Rows.Count - 1
                strTemp = dsTechnician.Tables(0).Rows(i).Item(0).ToString & ":" & dsTechnician.Tables(0).Rows(i).Item(1).ToString & "-" & dsTechnician.Tables(0).Rows(i).Item(0).ToString
                Listas.Add(strTemp)
            Next
        Catch
        End Try

        Return Listas


    End Function

    <AjaxPro.AjaxMethod()> _
        Function GetReminderDate(ByVal CountryID As String, ByVal InstallDate As String, ByVal FreeServiceID As String, ByVal FreeServiceEntitled As String) As String

        GetReminderDate = CalculateServiceReminderDate(CountryID, InstallDate, FreeServiceID, FreeServiceEntitled)


    End Function
#End Region
End Class
