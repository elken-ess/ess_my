Imports BusinessEntity
Imports System.Configuration
Imports System.Data
Partial Class PresentationLayer_masterrecord_company
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Page.IsPostBack) Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try
            Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
            'display label message
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            Me.compID.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCOMP-0007")
            Me.compname.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCOMP-0008")
            ctryStat.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCOMP-0004")
            addButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Add")
            searchButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Search")
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BROCOMPTL")

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.addinfo.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-AR")
            Me.addinfo.Visible = False
            Me.Label1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-NR")
            Me.Label1.Visible = False
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'create the dropdownlist
            Dim cntryStat As New clsrlconfirminf()

            Dim statParam As ArrayList = New ArrayList
            statParam = cntryStat.searchconfirminf("STATUS")
            Dim count As Integer
            Dim statid As String
            Dim statnm As String
            For count = 0 To statParam.Count - 1
                statid = statParam.Item(count)
                statnm = statParam.Item(count + 1)
                count = count + 1
                If (statid.Equals("ACTIVE") Or statid.Equals("OBSOLETE")) Then
                    ctryStatDrop.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                End If
                'If Not statid.Equals("DELETE") Then
                '    ctryStatDrop.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                'End If
            Next
            'ctryStatDrop.Items(0).Selected = True


            Dim companyEntity As New clsCompany()
            ''''''''''user info'''''''''''''xxl
            companyEntity.ModifyBy = Session("userID")

            '''''''''''''''''''''''''''
            '''''''''''''''''''''''''''''''''''
            If (Session("userID") <> "ADMIN") Then
                companyEntity.CountryID = Session("login_ctryID").ToString.ToUpper()
            Else
                companyEntity.CountryID = "%"
            End If

            If (ctryStatDrop.SelectedValue().ToString() <> "") Then
                companyEntity.CompStatus = ctryStatDrop.SelectedItem.Value.ToString()
            End If

            Dim compall As DataSet = companyEntity.GetComp()
            Me.compView.DataSource = compall
            Session("CompView") = compall
            checknorecord(compall)

            compView.AllowPaging = True
            compView.AllowSorting = True

            'Dim editcol As System.Web.UI.WebControls.HyperLinkField = New HyperLinkField()
            'editcol.Text = "edit"
            'editcol.DataNavigateUrlFormatString = "~/PresentationLayer/masterrecord/modifycompany.aspx?comid={0}&comstat={1}"
            'editcol.HeaderImageUrl = "~/PresentationLayer/graph/edit.gif"
            'Dim comid As String = compall.Tables.Item(0).Columns(0).ColumnName.ToString()
            ''Dim ctrid As String = ctryall.Tables.Item(0).Columns(5).ColumnName.ToString()
            ''Dim staid As String = ctryall.Tables.Item(0).Columns(6).ColumnName.ToString()
            ''Dim areaid As String = ctryall.Tables.Item(0).Columns(7).ColumnName.ToString()
            'Dim comstat As String = compall.Tables.Item(0).Columns(3).ColumnName.ToString()

            'Dim NavigateUrls As Array = Array.CreateInstance(GetType(String), 2)
            'NavigateUrls(0) = comid
            ''NavigateUrls(1) = ctrid
            ''NavigateUrls(2) = staid
            ''NavigateUrls(3) = areaid
            'NavigateUrls(1) = comstat

            'editcol.DataNavigateUrlFields = NavigateUrls
            'compView.Columns.Add(editcol)
            Dim editcol As New CommandField
            editcol.EditText = objXmlTr.GetLabelName("EngLabelMsg", "BB-Edit") '"edit"
            editcol.HeaderImageUrl = "~/PresentationLayer/graph/edit.gif"
            editcol.ShowEditButton = True
            compView.Columns.Add(editcol)

            '''access control'''''''''''''''''''''''''''''''''''''''''''''''
            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "19")
            Me.addButton.Enabled = purviewArray(0)
            'Me.searchButton.Visible = purviewArray(2)
            editcol.Visible = purviewArray(2)
            'Me.compView.Visible = purviewArray(2)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            compView.DataBind()
            If (compall.Tables(0).Rows.Count <> 0) Then
                compView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "COMPHD0")
                compView.HeaderRow.Cells(1).Font.Size = 8
                compView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "COMPHD1")
                compView.HeaderRow.Cells(2).Font.Size = 8
                compView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "COMPHD2")
                compView.HeaderRow.Cells(3).Font.Size = 8
                compView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "COMPHD3")
                compView.HeaderRow.Cells(4).Font.Size = 8
                compView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "COMPHD4")
                compView.HeaderRow.Cells(5).Font.Size = 8
                compView.HeaderRow.Cells(6).Text = objXmlTr.GetLabelName("EngLabelMsg", "COMPHD5")
                compView.HeaderRow.Cells(6).Font.Size = 8
                compView.HeaderRow.Cells(7).Text = objXmlTr.GetLabelName("EngLabelMsg", "COMPHD6")
                compView.HeaderRow.Cells(7).Font.Size = 8
                compView.HeaderRow.Cells(8).Text = objXmlTr.GetLabelName("EngLabelMsg", "COMPHD7")
                compView.HeaderRow.Cells(8).Font.Size = 8
                'compView.HeaderRow.Cells(9).Text = objXmlTr.GetLabelName("EngLabelMsg", "COMPHD8")
                'compView.HeaderRow.Cells(9).Font.Size = 8
                'compView.HeaderRow.Cells(10).Text = objXmlTr.GetLabelName("EngLabelMsg", "COMPHD9")
                'compView.HeaderRow.Cells(10).Font.Size = 8


            End If

        End If
    End Sub
#Region "if have no record "
    Public Function checknorecord(ByVal ds As DataSet) As Integer

        If ds.HasErrors Then
            If ds.Tables(0).Rows.Count = 0 Then
                Me.Label1.Visible = True
                'Me.addinfo.Visible = False
            Else
                Me.Label1.Visible = False

            End If

        End If
        If ds.Tables(0).Rows.Count = 0 Then
            Me.Label1.Visible = True
            'Me.addinfo.Visible = False
        Else
            Me.Label1.Visible = False

        End If


    End Function
#End Region
#Region "if have all record"
    Public Function checkinfo() As Integer
        If Me.compIDBox.Text = "" And Me.compnmBox.Text = "" Then
            Me.addinfo.Visible = True
        Else
            Me.addinfo.Visible = False
        End If
    End Function
#End Region

    Protected Sub searchButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles searchButton.Click
        checkinfo()
        compView.PageIndex = 0

        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        Dim compEntity As New clsCompany()
        If (Trim(Me.compIDBox.Text) <> "") Then
            compEntity.CompID = compIDBox.Text
        End If
        If (Trim(Me.compnmBox.Text) <> "") Then
            compEntity.CompName = compnmBox.Text
        End If
        If (ctryStatDrop.SelectedValue().ToString() <> "") Then
            compEntity.CompStatus = ctryStatDrop.SelectedItem.Value.ToString()
        End If
        If (Session("userID") <> "ADMIN") Then
            compEntity.CountryID = Session("login_ctryID").ToString.ToUpper()
        Else
            compEntity.CountryID = "%"
        End If
        Dim selDS As DataSet = compEntity.GetComp()
        compView.DataSource = selDS
        compView.DataBind()
        Session("CompView") = selDS
        checknorecord(selDS)

        ''''''''''user info'''''''''''''xxl
        compEntity.ModifyBy = Session("userID")

        '''''''''''''''''''''''''''
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        If (selDS.Tables(0).Rows.Count <> 0) Then
            compView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "COMPHD0")
            compView.HeaderRow.Cells(1).Font.Size = 8
            compView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "COMPHD1")
            compView.HeaderRow.Cells(2).Font.Size = 8
            compView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "COMPHD2")
            compView.HeaderRow.Cells(3).Font.Size = 8
            compView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "COMPHD3")
            compView.HeaderRow.Cells(4).Font.Size = 8
            compView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "COMPHD4")
            compView.HeaderRow.Cells(5).Font.Size = 8
            compView.HeaderRow.Cells(6).Text = objXmlTr.GetLabelName("EngLabelMsg", "COMPHD5")
            compView.HeaderRow.Cells(6).Font.Size = 8
            compView.HeaderRow.Cells(7).Text = objXmlTr.GetLabelName("EngLabelMsg", "COMPHD6")
            compView.HeaderRow.Cells(7).Font.Size = 8
            compView.HeaderRow.Cells(8).Text = objXmlTr.GetLabelName("EngLabelMsg", "COMPHD7")
            compView.HeaderRow.Cells(8).Font.Size = 8
            'compView.HeaderRow.Cells(9).Text = objXmlTr.GetLabelName("EngLabelMsg", "COMPHD8")
            'compView.HeaderRow.Cells(9).Font.Size = 8
            'compView.HeaderRow.Cells(10).Text = objXmlTr.GetLabelName("EngLabelMsg", "COMPHD9")
            'compView.HeaderRow.Cells(10).Font.Size = 8

        End If
    End Sub
    Protected Sub compView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles compView.PageIndexChanging
        compView.PageIndex = e.NewPageIndex

        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        Dim compEntity As New clsCompany()
        If (Trim(Me.compIDBox.Text) <> "") Then
            compEntity.CompID = compIDBox.Text
        End If
        If (Trim(Me.compnmBox.Text) <> "") Then
            compEntity.CompName = compnmBox.Text
        End If
        If (ctryStatDrop.SelectedValue().ToString() <> "") Then
            compEntity.CompStatus = ctryStatDrop.SelectedItem.Value.ToString()
        End If
        If (Session("userID") <> "ADMIN") Then
            compEntity.CountryID = Session("login_ctryID").ToString.ToUpper()
        Else
            compEntity.CountryID = "%"
        End If

        'Dim ctryall As DataSet = companyEntity.GetAllComp()
        Dim stall As DataSet = compEntity.GetComp()
        compView.DataSource = stall
        compView.DataBind()
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        If (stall.Tables(0).Rows.Count <> 0) Then
            compView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "COMPHD0")
            compView.HeaderRow.Cells(1).Font.Size = 8
            compView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "COMPHD1")
            compView.HeaderRow.Cells(2).Font.Size = 8
            compView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "COMPHD2")
            compView.HeaderRow.Cells(3).Font.Size = 8
            compView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "COMPHD3")
            compView.HeaderRow.Cells(4).Font.Size = 8
            compView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "COMPHD4")
            compView.HeaderRow.Cells(5).Font.Size = 8
            compView.HeaderRow.Cells(6).Text = objXmlTr.GetLabelName("EngLabelMsg", "COMPHD5")
            compView.HeaderRow.Cells(6).Font.Size = 8
            compView.HeaderRow.Cells(7).Text = objXmlTr.GetLabelName("EngLabelMsg", "COMPHD6")
            compView.HeaderRow.Cells(7).Font.Size = 8
            compView.HeaderRow.Cells(8).Text = objXmlTr.GetLabelName("EngLabelMsg", "COMPHD7")
            compView.HeaderRow.Cells(8).Font.Size = 8
            'compView.HeaderRow.Cells(9).Text = objXmlTr.GetLabelName("EngLabelMsg", "COMPHD8")
            'compView.HeaderRow.Cells(9).Font.Size = 8
            'compView.HeaderRow.Cells(10).Text = objXmlTr.GetLabelName("EngLabelMsg", "COMPHD9")
            'compView.HeaderRow.Cells(10).Font.Size = 8

        End If
    End Sub

    Protected Sub compView_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles compView.RowEditing
        'Dim ds As DataSet = Session("CompView")

        ' ctryid As String = Me.compView.Rows(e.NewEditIndex).Cells(1).Text
        Dim comid As String = Me.compView.Rows(e.NewEditIndex).Cells(1).Text
        comid = Server.UrlEncode(comid)
        Dim comstat As String = Me.compView.Rows(e.NewEditIndex).Cells(4).Text
        comstat = Server.UrlEncode(comstat)
        Dim i As Integer
        For i = 0 To Me.ctryStatDrop.Items.Count - 1
            If comstat = Me.ctryStatDrop.Items(i).Text Then
                comstat = Me.ctryStatDrop.Items(i).Value
            End If
        Next
        Dim strTempURL As String = "comid=" + comid + "&comstat=" + comstat
        strTempURL = "~/PresentationLayer/masterrecord/modifycompany.aspx?" + strTempURL
        Response.Redirect(strTempURL)
    End Sub
End Class
