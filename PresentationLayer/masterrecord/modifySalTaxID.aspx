<%@ Page Language="VB" AutoEventWireup="false" EnableEventValidation="false"  CodeFile="modifySalTaxID.aspx.vb" Inherits="PresentationLayer_masterrecord_modifySalTaxID" %>

<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>modify Sales Tax ID</title>
    <meta http-equiv="Content-Type" content="text/html; charset=gb2312">
	<LINK href="../css/style.css" type="text/css" rel="stylesheet">
</head>
<body>
    <form id="form1" runat="server">
        <table id="Table3" border="0" style="width: 100%">
            <tr>
                <td style="width: 100%">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title1.gif" width="5" /></td>
                            <td background="../graph/title_bg.gif" class="style2" width="98%">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="right" background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title_2.gif" width="5" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width:100%">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" class="style2" width="98%">
                                <font color="red">
                                    <asp:Label ID="errlab" runat="server" Text="Label" Visible="false"></asp:Label></font></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100%">
                    <table id="SalTaxIDtab" border="0" style="width: 100%">
                        <tr>
                            <td >
                                <table id="SalTaxSetUp" bgcolor="#b7e6e6" border="0" cellpadding="2" cellspacing="1" style="width: 100%">
                                <tr bgcolor="#ffffff">
<td colspan = 4>
                            <asp:Label ID="lblNoteUP" runat="server" ForeColor="Red" Text="Label" Visible="True"></asp:Label>
</td>
             </tr>
             <tr bgcolor="#ffffff">
<td colspan = 4>
                        	&nbsp;
                     	</td>
               </tr>

                                    <tr bgcolor="#ffffff">
                                        <td align="right" width="15%" style="width: 20%">
                                            <asp:Label ID="SalTaxIDLab" runat="server" Text="Label"></asp:Label></td>
                                        <td align="left" style="width: 30%">
                                            <font color="red" style="width: 30%">
                                                <asp:TextBox ID="saltaxidbox" runat="server" ReadOnly="True" Width="90%" CssClass="textborder"></asp:TextBox>
                                                <asp:Label ID="Label2" runat="server" ForeColor="Red" Text="*"></asp:Label></font>
                                            </td>
                                        <td align="right" width="15%" style="width: 20%">
                                            <asp:Label ID="salnameLab" runat="server" Text="Label"></asp:Label></td>
                                        <td align="left" style="width: 30%">
                                            <asp:TextBox ID="salnamebox" runat="server" Width="90%" CssClass="textborder" MaxLength="50"></asp:TextBox>
                                            <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="*"></asp:Label>&nbsp;
                                        </td>
                                    </tr>
                                    <TR bgColor="#ffffff">
											<TD  align=right style="width: 20%; height: 30px"  >
                                                <asp:Label ID="ALterNamelab" runat="server" Text="Label" ></asp:Label></TD>
											<TD align=left colspan="5" style="width: 80%; height: 30px"  >
                                                <asp:TextBox ID="ALterNamebox" runat="server" Width="62%" CssClass="textborder" MaxLength="50"></asp:TextBox></TD>
										</TR>
                                    <tr bgcolor="#ffffff">
                                        <td align="right" width="15%" style="width: 20%; height: 30px">
                                            <asp:Label ID="CountryIDlab" runat="server" Text="Label"></asp:Label></td>
                                        <td align="left" style="width: 30%; height: 30px" >
                                            <asp:TextBox ID="countryidBox" runat="server" ReadOnly="True" Width="90%" CssClass="textborder"></asp:TextBox>
                                        </td>
                                        <td align="right" width="15%" style="width: 20%; height: 30px">
                                            <asp:Label ID="StatusLab" runat="server" Text="Label"></asp:Label></td>
                                        <td align="left" colspan="3" style="width: 30%; height: 30px">
                                            <asp:DropDownList ID="StatusDrop" runat="server" CssClass="textborder" Width="90%" >
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr bgcolor="#ffffff">
                                        <td align="right" width="15%" style="width: 20%; height: 30px">
                                            <asp:Label ID="CreatedBylab" runat="server" Text="Label"></asp:Label></td>
                                        <td align="left" style="width: 30%; height: 30px" >
                                            <asp:TextBox ID="CreatedBy" runat="server" CssClass="textborder" ReadOnly="True" Width="90%"></asp:TextBox></td>
                                        <td align="right" width="15%" style="width: 20%; height: 30px">
                                            <asp:Label ID="CreatedDatelab" runat="server" Text="Label"></asp:Label></td>
                                        <td align="left" style="width: 30%">
                                            <asp:TextBox ID="CreatedDate" runat="server" CssClass="textborder" ReadOnly="True" Width="90%" style="width: 90%" 
                                                ></asp:TextBox>
                                          
              </td>
                                    </tr>
                                    <tr bgcolor="#ffffff">
                                        <td align="right" width="15%" style="width: 20%">
                                            <asp:Label ID="ModifiedBylab" runat="server" Text="Label"></asp:Label></td>
                                        <td align="left" style="width: 30%">
                                            <asp:TextBox ID="ModifiedBy" runat="server" CssClass="textborder" ReadOnly="True" Width="90%"></asp:TextBox></td>
                                        <td align="right" width="15%" style="width: 20%">
                                            <asp:Label ID="ModifiedDatelab" runat="server" Text="Label"></asp:Label></td>
                                        <td align="left" style="width: 30%">
                                            <asp:TextBox ID="ModifiedDate" runat="server" CssClass="textborder" ReadOnly="True" Width="90%" style="width: 90%" 
                                               ></asp:TextBox>
                                           
                                        </td>
                                    </tr>
                                </table>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td background="../graph/title_bg.gif" width="1%" style="height: 24px">
                                            <img height="24" src="../graph/title1.gif" width="5" /></td>
                                        <td background="../graph/title_bg.gif" class="style2" width="98%" style="height: 24px">
                                            <asp:Label ID="TaxPectgLabel" runat="server" Text="Label"></asp:Label>
                                            <asp:LinkButton ID="AddButton" runat="server">LinkButton</asp:LinkButton></td>
                                        <td align="right" background="../graph/title_bg.gif" width="1%" style="height: 24px">
                                            <img height="24" src="../graph/title_2.gif" width="5" /></td>
                                    </tr>
                                </table>
                                <asp:GridView ID="salSetUp1View" runat="server" Width="100%" AllowPaging="True">
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table id="Table2" border="0" cellpadding="1" cellspacing="1">
                                             <tr bgcolor="#ffffff">
<td colspan = 2>
                        	&nbsp;
                            <asp:Label ID="lblNoteDown" runat="server" ForeColor="Red" Text="Label"></asp:Label></td>
               </tr>
                                <tr bgcolor="#ffffff">
<td colspan = 2>
    &nbsp;</td>
             </tr>


                                    <tr>
                                        <td style="width: 89px">
                                            &nbsp;<asp:LinkButton ID="saveButton" runat="server"></asp:LinkButton></td>
                                        <td align="center" style="height: 16px; width: 352px;">
                                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;<asp:HyperLink ID="cancelLink" runat="server" NavigateUrl="~/PresentationLayer/masterrecord/SalTaxID.aspx">[cancelLink]</asp:HyperLink></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    </td>
                        </tr>
                     </table>
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                        ShowSummary="False" />
                    <asp:TextBox ID="SetUpID" runat="server" Visible="False"></asp:TextBox>
                    <cc1:messagebox id="MessageBox1" runat="server"></cc1:messagebox>
                                            <asp:RequiredFieldValidator ID="salnameva" runat="server" ControlToValidate="salnamebox"
                                                ForeColor="White"></asp:RequiredFieldValidator>
              
       </form>
</body>
</html>
