<%@ Page Language="VB" AutoEventWireup="false" EnableEventValidation="false"  CodeFile="addPriceSetUp.aspx.vb" Inherits="PresentationLayer_masterrecord_addPriceSetUp" %>

<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Price Setup - Add</title>
     <style type="text/css">A:link { COLOR: #336666; TEXT-DECORATION: none }
	BODY { FONT-SIZE: 9pt; FONT-FAMILY: "Arial", "Verdana", "Tahoma"; BACKGROUND-COLOR: #ffffff }
	TD { FONT-SIZE: 9pt; FONT-FAMILY: Arial,Verdana,Tahoma }
	</style>
	<link href="../css/style.css" type="text/css" rel="stylesheet" />

</head>
<body  id="PriceSetUpbody">
   <form id="PriceSetUpform"  method="post" action=""  runat="server">
    <TABLE border="0" id=countrytab style="width: 100%">
           <tr>
           <td  >
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>
							<TD width="1%" background="../graph/title_bg.gif"><IMG height="24" src="../graph/title1.gif" width="5"></TD>
							<TD class="style2" width="98%" background="../graph/title_bg.gif">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></TD>
							<TD align="right" width="1%" background="../graph/title_bg.gif"><IMG height="24" src="../graph/title_2.gif" width="5"></TD>
						</TR>
			 </TABLE>
           </tr>
           <tr>
           <td >
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>					
							<TD class="style2" width="98%" background="../graph/title_bg.gif" >
                                <font color=red><asp:Label ID="errlab" runat="server" Text="Label" Visible="false"></asp:Label></font>
                               </TD>
						</TR>
			 </TABLE>
           </tr>
			<TR>
				<TD  >
			 
     <TABLE border="0" id=PriceIDtab style="width: 100%" >
			<TR>
				<TD   >
					<TABLE id="PriceSetUp" cellSpacing="1" cellPadding="2" bgColor="#b7e6e6" border="0" style="width: 100%"  >
					  <tr bgcolor="#ffffff">
<td colspan = 6>
                            <asp:Label ID="lblCompTop" runat="server" ForeColor="Red" Text="Label" Visible="True"></asp:Label>
</td>
             </tr>
             <tr bgcolor="#ffffff">
<td colspan = 6>
                        	&nbsp;
                     	</td>
               </tr> 
									  <TR bgColor="#ffffff">
											<TD  align=right style="width: 20%; height: 24px;"  >
                                                <asp:Label ID="priceidLab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 30%; height: 24px"  >
                                                <asp:DropDownList ID="priceidDrop" runat="server" Width="88%" CssClass="textborder">
                                                </asp:DropDownList>
                                                <font color=red><strong>*</strong></font><asp:RequiredFieldValidator ID="PriceIDva" runat="server" ForeColor="White" ControlToValidate="PriceIDDrop">*</asp:RequiredFieldValidator></TD>
                                                
                                               
                                                
                                                <TD  align=right style="width: 20%; height: 24px"   >
                                                <asp:Label ID="partidlab" runat="server" Text="Label"></asp:Label></TD>
											  <TD align=left  colspan=3 style="width: 30%; height: 24px"   >
                                                <asp:DropDownList ID="partidDrop" runat="server" Width="90%" CssClass="textborder">
                                                </asp:DropDownList>
                                                  <font color=red><strong>*</strong></font>
                                                <asp:RequiredFieldValidator ID="partidVA" runat="server" ForeColor="White" ControlToValidate="PartIDDrop">*</asp:RequiredFieldValidator></TD>
                                                
										</TR>
										
										<TR bgColor="#ffffff">
											<TD  align=right style="width: 20%; height: 27px"  >
                                                <asp:Label ID="countryidlab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 30%; height: 27px"    >
                                                <asp:DropDownList ID="CountryIDDrop" runat="server" Width="88%" CssClass="textborder">
                                                </asp:DropDownList>
                                                <font color=red><strong>*</strong></font><asp:RequiredFieldValidator ID="COUNTRYIDVA" runat="server" ErrorMessage="RequiredFieldValidator"
                                                    ForeColor="White" ControlToValidate="CountryIDDrop">*</asp:RequiredFieldValidator></TD>
                                                <TD  align=right style="width: 20%; height: 27px"  >
                                                <asp:Label ID="StatusLab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left colspan=3 style="width: 30%; height: 27px"  >
                                                <asp:DropDownList ID="StatusDrop" runat="server" Width="90%" CssClass="textborder">
                                                </asp:DropDownList></TD>
											
										</TR>
										
										
										<TR bgColor="#ffffff">
											<TD  align=right style="width: 20%; height: 30px"   >
                                                <asp:Label ID="CreatedBylab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 30%; height: 30px"   >
                                                <asp:TextBox ID="CreatedBy" runat="server" CssClass="textborder" ReadOnly="True" Width="88%"></asp:TextBox></TD>
											<TD  align=right style="width: 20%; height: 30px"   >
                                                <asp:Label ID="CreatedDatelab" runat="server" Text="Label"></asp:Label></TD>
											<TD  align=left colspan=3 style="width: 30%; height: 30px"    >
                                                <asp:TextBox ID="CreatedDate" runat="server" Width="97%" CssClass="textborder" ReadOnly="True"></asp:TextBox>
                                            
                                            </TD>
										</TR>
										<TR bgColor="#ffffff">
											<TD  align=right style="width: 20%"  >
                                                <asp:Label ID="ModifiedBylab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 30%"  >
                                                <asp:TextBox ID="ModifiedBy" runat="server" CssClass="textborder" ReadOnly="True" Width="88%"></asp:TextBox></TD>
											<TD  align=right style="width: 20%"   >
                                                <asp:Label ID="ModifiedDatelab" runat="server" Text="Label" Width="50px"></asp:Label></TD>
											<TD  align=left colspan=3 style="width: 30%"  >
                                                <asp:TextBox ID="ModifiedDate" runat="server" Width="97%" CssClass="textborder" ReadOnly="True"></asp:TextBox>
                                               
                                            </TD>
										</TR>
										<TR bgColor="#ffffff">
											<TD  align=right style="width: 20%"   >
                                                <asp:Label ID="amountlab" runat="server" Text="Label" Width="52px"></asp:Label></TD>
											<TD align=left width="85%" colspan=6   >
                                                <asp:LinkButton ID="LinkButton2" runat="server" Width="300px"></asp:LinkButton></TD>
										</TR>
										
										
								</TABLE>
                    <asp:GridView ID="PriceSetUp1View" runat="server" Width="100%" AllowPaging="True">
                    </asp:GridView>
                </TD>
					</TR>
					<TR>
						<TD   >
						<font color=red><asp:Label ID="lblCompBottom" runat="server" Text="Label" Visible="true"></asp:Label></font>
                            </TD>
					</TR>
			</TABLE>
                  <asp:LinkButton ID="savebutton" runat="server"></asp:LinkButton>
                   &nbsp; &nbsp;&nbsp;
                   
                   <asp:HyperLink ID="cancellink" runat="server" NavigateUrl="~/PresentationLayer/masterrecord/PriceSetUp.aspx">[cancelLink]</asp:HyperLink>&nbsp; &nbsp;&nbsp;
                   <asp:HyperLink ID="AddLink" runat="server" >[Add]</asp:HyperLink>
			</TD>
							</TR>
							</TABLE>
  

               <asp:TextBox ID="SetUpID" runat="server" Visible="False" Wrap="False"></asp:TextBox>
                    <asp:ValidationSummary ID="errormessage" runat="server" ShowMessageBox="True" ShowSummary="False" />
                    <cc1:messagebox id="MessageBox1" runat="server"></cc1:messagebox>

    </form>
</body>
</html>
