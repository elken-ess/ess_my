<%@ Page Language="VB" AutoEventWireup="false" EnableEventValidation="false" CodeFile="addinstalleBase.aspx.vb"
    Inherits="PresentationLayer_masterrecord_addinstalleBase_aspx" %>

<%@ Register Assembly="JCalendar" Namespace="JCalendar" TagPrefix="cc2" %>
<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>install base - Add</title>
      <script language="JavaScript" src="../js/common.js"></script>
  <meta http-equiv="Content-Type" content="text/html; charset=gb2312">
    <link href="../css/style.css" type="text/css" rel="stylesheet">
    <script language =javascript >
      
      
 function LoadTechnician_CallBack(response){
 
     //if the server-side code threw an exception
     if (response.error != null)
     {
      //we should probably do better than this
      alert(response.error); 
      
      return;
     }

     var ResponseValue = response.value;
     
   
     //if the response wasn't what we expected  
     if (ResponseValue == null || typeof(ResponseValue) != "object")
     {       
      return;
     }
          
     //Get the states drop down
     var ResultList = document.getElementById("<%=Technicianiddrp.ClientID%>");
     ResultList.options.length = 0; //reset the states dropdown
 
    
     //Remember, its length not Length in JavaScript
     for (var i = 0; i < ResponseValue.length; ++i)
     {
     
      //the columns of our rows are exposed like named properties
      var al = ResponseValue[i].split(":");
      var alid = al[0];
      var alname = al[1];
      ResultList.options[ResultList.options.length] =  new Option(alname, alid);
     }
     
     
}
 
 
 function LoadTechnician(objectClient)
{
 
 if (objectClient.selectedIndex > 0){
    var countryid =  '<%=Session("login_ctryID")%>';
    var companyid =  '<%=Session("login_cmpID")%>';
    var servicecenterid = document.getElementById("<%=Sercenteriddrpd.ClientID%>").options[document.getElementById("<%=Sercenteriddrpd.ClientID%>").selectedIndex].value;
    var rank = '<%=Session("login_rank")%>';
   
   
  
    PresentationLayer_masterrecord_addinstalleBase_aspx.GetTechnicianList( countryid, companyid,servicecenterid,rank, LoadTechnician_CallBack);
    
    
    
 }
 else
 {

    document.getElementById("<%=Technicianiddrp.ClientID%>").options.length = 0
 }
}

function LoadReminderDate_CallBack(response){
 
     //if the server-side code threw an exception
     if (response.error != null)
     {
      //we should probably do better than this
      alert(response.error); 
      
      return;
     }

     var ResponseValue = response.value;
     
     document.getElementById("<%=FrSRemDtbox.ClientID%>").value=ResponseValue;
        
     
}


 function LoadReminderDate(objectClient){
 
    var countryid =  '<%=Session("login_ctryID")%>';
    var installdate =  document.getElementById("<%=Installeddatetbox.ClientID%>").value;
    var freeserviceid = document.getElementById("<%=SerTypedrpd.ClientID%>").options[document.getElementById("<%=SerTypedrpd.ClientID%>").selectedIndex].value;
    var freeserviceentitle = document.getElementById("<%=freeserentitledrpd.ClientID%>").options[document.getElementById("<%=freeserentitledrpd.ClientID%>").selectedIndex].value;
    var modelid = document.getElementById("<%=modelidtdrp.ClientID%>").options[document.getElementById("<%=modelidtdrp.ClientID%>").selectedIndex].value;
    
    PresentationLayer_masterrecord_addinstalleBase_aspx.GetWarrantyDate( modelid, installdate,countryid, LoadWarrantyDate_CallBack);
    
    PresentationLayer_masterrecord_addinstalleBase_aspx.GetReminderDate( countryid, installdate,freeserviceid,freeserviceentitle, LoadReminderDate_CallBack);
    
 }
 
 function LoadFreeServiceEntitled(objectClient){
 
    var countryid =  '<%=Session("login_ctryID")%>';
    var installdate =  document.getElementById("<%=Installeddatetbox.ClientID%>").value;
    var freeserviceid = document.getElementById("<%=SerTypedrpd.ClientID%>").options[document.getElementById("<%=SerTypedrpd.ClientID%>").selectedIndex].value;
    var freeserviceentitle = document.getElementById("<%=freeserentitledrpd.ClientID%>").options[document.getElementById("<%=freeserentitledrpd.ClientID%>").selectedIndex].value;
    var modelid = document.getElementById("<%=modelidtdrp.ClientID%>").options[document.getElementById("<%=modelidtdrp.ClientID%>").selectedIndex].value;
    
    PresentationLayer_masterrecord_addinstalleBase_aspx.GetWarrantyDate( modelid, installdate,countryid, LoadWarrantyDate_CallBack);
       
    PresentationLayer_masterrecord_addinstalleBase_aspx.GetReminderDate( countryid, installdate,freeserviceid,freeserviceentitle, LoadReminderDate_CallBack);
    
    if (freeserviceentitle == "Y"){
        document.getElementById("<%=SerTypedrpd.ClientID%>").disabled=false;
    }
    else {
        document.getElementById("<%=SerTypedrpd.ClientID%>").selectedIndex=0;
        document.getElementById("<%=SerTypedrpd.ClientID%>").disabled=true;
    }
 }

function bodyOnload(){
    var freeserviceentitle = document.getElementById("<%=freeserentitledrpd.ClientID%>").options[document.getElementById("<%=freeserentitledrpd.ClientID%>").selectedIndex].value;
     
    if (freeserviceentitle == "Y"){
        document.getElementById("<%=SerTypedrpd.ClientID%>").disabled=false;
    }
    else {
        document.getElementById("<%=SerTypedrpd.ClientID%>").selectedIndex=0;
        document.getElementById("<%=SerTypedrpd.ClientID%>").disabled=true;
    }
    }
    
    
function LoadWarrantyDate_CallBack(response){
 
     //if the server-side code threw an exception
     if (response.error != null)
     {
      //we should probably do better than this
      alert(response.error); 
      
      return;
     }

     var ResponseValue = response.value;
     
     document.getElementById("<%=WarrExpdatetbox.ClientID%>").value=ResponseValue;
        
     
}


 function LoadWarrantyDate(objectClient){
 
    var countryid =  '<%=Session("login_ctryID")%>';
    var installdate =  document.getElementById("<%=Installeddatetbox.ClientID%>").value;
    var modelid = document.getElementById("<%=modelidtdrp.ClientID%>").options[document.getElementById("<%=modelidtdrp.ClientID%>").selectedIndex].value;
    
    PresentationLayer_masterrecord_addinstalleBase_aspx.GetWarrantyDate( modelid, installdate,countryid, LoadWarrantyDate_CallBack);
    
 }
 
  function  validate(){
    var installdate = document.Installform.Installeddatetbox.value;
    var producedatetbox = document.Installform.producedatetbox.value;
    if(installdate.length > 0){
        if(!checkdate(installdate)){
            return false;
        }
    }
    if(producedatetbox.length > 0){
        if(!checkdate(producedatetbox)){
            return false;
        }
    }
    return true;
 }
 
 function checkdate(input){
	var validformat=/^\d{2}\/\d{2}\/\d{4}$/ 
	var dayfield=input.split("/")[0];
	var monthfield=input.split("/")[1];
	var yearfield=input.split("/")[2];
	
	if (yearfield.length != 4){
		alert("Invalid Date Format, Date format should be dd/mm/yyyy");
		return false;
	}else{
		return true;
	}	
}
 
      </script>
</head>
<body id="Installbody" onload ="bodyOnload()">
    <form id="Installform" method="post" action="" runat="server">
        <table id="Table2" border="0" width="100%">
            <tr>
                <td style="width: 100%">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" style="width: 1%">
                                <img height="24" src="../graph/title1.gif" width="5" /></td>
                            <td background="../graph/title_bg.gif" class="style2" width="98%">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="right" background="../graph/title_bg.gif" style="width: 1%">
                                <img height="24" src="../graph/title_2.gif" width="5" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 30px; width: 100%;">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" class="style2" style="height: 5px" width="98%">
                                <font color="red">
                                    <asp:Label ID="errlab" runat="server" Text="Label" Visible="false"></asp:Label></font>
                                    <font color=red></font></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100%">
                    <table id="Install" cellspacing="1" width="100%" cellpadding="2" bgcolor="#b7e6e6"
                        border="0">
                          <tr bgcolor="#ffffff">
<td colspan = 4>
                            <asp:Label ID="lblCompTop" runat="server" ForeColor="Red" Text="Label" Visible="True"></asp:Label>
</td>
             </tr>
             <tr bgcolor="#ffffff">
<td colspan = 4>
                        	&nbsp;
                     	</td>
               </tr> 
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 20%;">
                                <asp:Label ID="Customernamelab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 30%;" colspan="3">
                                <asp:TextBox ID="Customernametbox" runat="server" CssClass="textborder" ReadOnly="True" Width="92%"></asp:TextBox>
                            </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 20%;">
                                <asp:Label ID="customeridlab" runat="server" Text="Label"></asp:Label>
                            </td>
                            <td align="left" style="width: 30%;" colspan="3">
                                <asp:TextBox ID="cusid" runat="server" CssClass="textborder" MaxLength="2" ReadOnly="True"
                                    Width="60%"></asp:TextBox>
                                <asp:LinkButton ID="LinkButton2" runat="server" BackColor="MediumPurple" BorderStyle="Outset"
                                    CausesValidation="False">...</asp:LinkButton>
                                <font color="red"><strong>*</strong></font>
                                <asp:RequiredFieldValidator ID="cusiderr" runat="server" ControlToValidate="cusid" ForeColor="White">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 20%;">
                                <asp:Label ID="CustomerjdeLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 30%;">
                                <asp:TextBox ID="Customerjdetbox" runat="server" CssClass="textborder" MaxLength="2"
                                    ReadOnly="True" Style="width: 90%"></asp:TextBox>&nbsp;
                            </td>
                            <td align="right" style="width: 20%;">
                                <asp:Label ID="jderoidlab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 30%;">
                                <asp:TextBox ID="jderoidtbox" runat="server" Width="150px" ReadOnly="True"></asp:TextBox>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 20%;">
                                <asp:Label ID="serialnoLab" runat="server" Text="Serial Number"></asp:Label></td>
                            <td align="left" style="width: 30%;">
                                <asp:TextBox ID="serialnotbox" runat="server" CssClass="textborder" MaxLength="15"
                                    Style="width: 90%"></asp:TextBox>
                                <font color="red"><strong>*</strong></font>
                                <asp:RequiredFieldValidator ID="serialidRFValid" runat="server" ControlToValidate="serialnotbox"
                                    ErrorMessage="RequiredFieldValidator" ForeColor="White">*</asp:RequiredFieldValidator>
                            </td>
                            <td align="right" style="width: 20%;">
                                <asp:Label ID="modelidlab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 30%;">
                                <asp:DropDownList ID="modelidtdrp" runat="server" CssClass="textborder" Width="90%" onchange="LoadWarrantyDate(this);" >
                                </asp:DropDownList>
                                <font color="red"><strong>*</strong></font>
                                <asp:RequiredFieldValidator ID="modelidRFValid" runat="server" ControlToValidate="modelidtdrp"
                                    ErrorMessage="RequiredFieldValidator" ForeColor="White">*</asp:RequiredFieldValidator>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 20%;">
                                <asp:Label ID="Contractedlab" runat="server" Text="Label"></asp:Label>
                            </td>
                            <td align="left" style="width: 30%;">
                                <asp:DropDownList ID="Contractdrpd" runat="server" CssClass="textborder" Width="90%">
                                </asp:DropDownList></td>
                            <td align="right" style="width: 20%;">
                                <asp:Label ID="freeserentitlelab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 30%;">
                                <asp:DropDownList ID="freeserentitledrpd" runat="server" CssClass="textborder" AutoPostBack="false"  onchange="LoadFreeServiceEntitled(this)"
                                    Width="90%">
                                </asp:DropDownList></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 20%;">
                                <asp:Label ID="Installeddatelab" runat="server" Text="Label"></asp:Label><br />
                                <asp:Label ID="Label11" runat="server" Text="DD/MM/YYYY"></asp:Label></td>
                            <td align="left" style="width: 30%;">
                                <asp:TextBox ID="Installeddatetbox" runat="server" CssClass="textborder" onblur="LoadFreeServiceEntitled(this)"
                                    Width="60%" MaxLength="10"></asp:TextBox>
                                <font color="red"><strong>*</strong></font>
                                <asp:RequiredFieldValidator ID="installerr" runat="server" ControlToValidate="Installeddatetbox"
                                    Width="1px" ForeColor="White">*</asp:RequiredFieldValidator><asp:HyperLink ID="HypCal" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif" ToolTip="Choose a Date">Choose a Date</asp:HyperLink><cc2:JCalendar ID="JCalendar1"
                                        runat="server" ControlToAssign="Installeddatetbox" ImgURL="~/PresentationLayer/graph/calendar.gif" Visible="False" />
                                <asp:CompareValidator ID="CompDateVal" runat="server" ControlToValidate="Installeddatetbox"
                                    Display="Dynamic" Operator="DataTypeCheck" SetFocusOnError="True" Type="Date"></asp:CompareValidator></td>
                            <td align="right" style="width: 20%;">
                                <asp:Label ID="producedatelab" runat="server" Text="Label"></asp:Label><br />
                                <asp:Label ID="Label1" runat="server" Text="DD/MM/YYYY"></asp:Label></td>
                            <td align="left" style="width: 30%;">
                                <asp:TextBox ID="producedatetbox" runat="server" CssClass="textborder"
                                    Width="60%" MaxLength="10"></asp:TextBox>
                                <font color="red"><strong>*</strong></font>
                                <asp:RequiredFieldValidator ID="proerr" runat="server" ControlToValidate="producedatetbox"
                                    Width="1px" ForeColor="White">*</asp:RequiredFieldValidator>
                                <asp:HyperLink ID="HypCal2" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                                    ToolTip="Choose a Date">Choose a Date</asp:HyperLink><cc2:JCalendar ID="JCalendar2" runat="server" ControlToAssign="producedatetbox" ImgURL="~/PresentationLayer/graph/calendar.gif" Visible="False">
                                </cc2:JCalendar>
                                <asp:CompareValidator ID="CompDateVal2" runat="server" ControlToValidate="producedatetbox"
                                    Display="Dynamic" Operator="DataTypeCheck" SetFocusOnError="True" Type="Date"></asp:CompareValidator></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 20%;">
                                <asp:Label ID="ProductClasslab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 30%;">
                                <asp:DropDownList ID="productclassdrpd" runat="server" CssClass="textborder" Width="90%">
                                </asp:DropDownList>&nbsp;
                            </td>
                            <td align="right" style="width: 20%;">
                                &nbsp;<asp:Label ID="TechnicianIDlab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 30%;">
                                <asp:DropDownList ID="Technicianiddrp" runat="server" CssClass="textborder" Style="width: 90%">
                                </asp:DropDownList></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 20%;" id="#svcctr">
                                <asp:Label ID="Sercenteridlab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 30%;">
                                <asp:DropDownList ID="Sercenteriddrpd" runat="server" CssClass="textborder" Width="90%"
                                    AutoPostBack="false" onchange="LoadTechnician(this)">
                                </asp:DropDownList><strong><span style="color: #ff0000">* </span></strong>
                                <asp:RequiredFieldValidator ID="sercenteriderr" runat="server" ControlToValidate="Sercenteriddrpd" ForeColor="White">*</asp:RequiredFieldValidator></td>
                            <td align="right" style="width: 20%;">
                                <asp:Label ID="TechnicianTypelab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 30%;">
                                <asp:DropDownList ID="TechnicianTypedrpd" runat="server" CssClass="textborder" Style="width: 90%">
                                </asp:DropDownList></td>
                        </tr>
                        
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 20%;">
                                <asp:Label ID="WarrExpdatelab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 30%;">
                                <asp:TextBox ID="WarrExpdatetbox" runat="server" CssClass="textborder" ReadOnly="True"
                                    Style="width: 90%"></asp:TextBox></td>
                            <td align="right" style="width: 20%;">
                                <asp:Label ID="SerTypeidlab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 30%;">
                                <asp:DropDownList ID="SerTypedrpd" runat="server" CssClass="textborder" Style="width: 90%"
                                    AutoPostBack="false" disabled="false" onchange="LoadReminderDate(this)" >
                                </asp:DropDownList></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 20%;">
                                <asp:Label ID="lastserdatelab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 30%;">
                                <asp:TextBox ID="lastserdatetbox" runat="server" CssClass="textborder" ReadOnly="True"
                                    Style="width: 90%"></asp:TextBox></td>
                            <td align="right" style="width: 20%;">
                                <asp:Label ID="lastremidatelab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 30%;">
                                <asp:TextBox ID="lastremidatetbox" runat="server" CssClass="textborder" ReadOnly="True"
                                    Style="width: 90%"></asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 20%;">
                                <asp:Label ID="FreSerRemDatelab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 30%;">
                                <asp:TextBox ID="FrSRemDtbox" runat="server" CssClass="textborder" ReadOnly="True"
                                    Style="width: 90%"></asp:TextBox>
                                &nbsp;
                            </td>
                            <td align="right" style="width: 20%;">
                                <asp:Label ID="Statuslab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 30%;">
                                <asp:DropDownList ID="statusdrpd" runat="server" CssClass="textborder" Style="width: 90%">
                                </asp:DropDownList></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 20%; height: 29px;">
                                <asp:Label ID="Createdbylab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 30%; height: 29px;">
                                <asp:TextBox ID="CreatedBytbox" runat="server" CssClass="textborder" ReadOnly="True"
                                    Style="width: 90%"></asp:TextBox></td>
                            <td align="right" style="width: 20%; height: 29px;">
                                <asp:Label ID="ModifiedBylab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 30%; height: 29px;">
                                <asp:TextBox ID="ModifiedBytbox" runat="server" CssClass="textborder" ReadOnly="True"
                                    Style="width: 90%"></asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 20%;">
                                <asp:Label ID="createbydatelab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 30%;">
                                <asp:TextBox ID="createbydatetbox" runat="server" CssClass="textborder" ReadOnly="True"
                                    Style="width: 90%"></asp:TextBox>&nbsp;
                            </td>
                            <td align="right" style="width: 20%;">
                                <asp:Label ID="modifybydatelab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 30%;">
                                <asp:TextBox ID="modifybydatetbox" runat="server" CssClass="textborder" ReadOnly="True"
                                    Style="width: 90%"></asp:TextBox>&nbsp;
                            </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 20%; height: 90px;">
                                <asp:Label ID="Remarkslab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" colspan="3" style="width: 30%; height: 90px;">
                                <asp:TextBox ID="Remarkstbox" runat="server" CssClass="textborder" MaxLength="400"  Width="90%" Height="80px"></asp:TextBox></td>
                        </tr>
                    </table>
                    <font color=red><asp:Label ID="lblCompBottom" runat="server" Text="Label" Visible="true"></asp:Label></font>
                    <br />
                    <asp:LinkButton ID="LinkButton1" runat="server" onclientclick="return validate();">save</asp:LinkButton>&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/PresentationLayer/masterrecord/InstalledBase.aspx">cancel</asp:HyperLink>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:HyperLink ID="AddLink" runat="server" >[Add]</asp:HyperLink></td>
</tr>            
        </table>
         
        <asp:ValidationSummary ID="ValidationSummary" runat="server" ShowMessageBox="True"
                        ShowSummary="False" />
        &nbsp;
        <asp:TextBox ID="TextBox1" runat="server" Visible="False" Width="1px"></asp:TextBox>
        <cc1:MessageBox ID="MessageBox1" runat="server" />
    </form>
</body>
</html>
