<%@ Page Language="VB" AutoEventWireup="false" EnableEventValidation="false"  CodeFile="addSalTaxID.aspx.vb" Inherits="PresentationLayer_masterrecord_addSalTaxID" %>

<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Add sales Tax</title>
    <LINK href="../css/style.css" type="text/css" rel="stylesheet">
</head>
<body>
    <form id="form1" runat="server">
   <table id="Table1" border="0" style="width: 100%">
            <tr>
                <td >
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title1.gif" width="5" /></td>
                            <td background="../graph/title_bg.gif" class="style2" width="98%">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="right" background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title_2.gif" width="5" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td >
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" class="style2" width="98%">
                                <font color="red">
                                    <asp:Label ID="errlab" runat="server" Text="Label" Visible="false"></asp:Label></font>
                                    </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td >
                    <table id="saltaxIDtab" border="0" style="width: 100%">
                        <tr>
                            <td >
                                <table id="saltax" bgcolor="#b7e6e6" border="0" cellpadding="2" cellspacing="1" style="width: 100%">
                                  <tr bgcolor="#ffffff">
<td colspan = 6>
                            <asp:Label ID="lblCompTop" runat="server" ForeColor="Red" Text="Label" Visible="True"></asp:Label>
</td>
             </tr>
             <tr bgcolor="#ffffff">
<td colspan = 6>
                        	&nbsp;
                     	</td>
               </tr> 
                                    <tr bgcolor="#ffffff">
                                        <td align="right" style="width: 20%">
                                            <asp:Label ID="saltaxIDLab" runat="server" Text="Label"></asp:Label></td>
                                        <td align="left" style="width: 30%">
                                            <asp:TextBox ID="saltaxIDBox" runat="server" CssClass="textborder" MaxLength="10" Width="90%"></asp:TextBox>
                                            <font color=red><strong>*</strong></font>
                                            </td>
                                        <td align="right" style="width: 20%">
                                            <asp:Label ID="salname" runat="server" Text="Label"></asp:Label></td>
                                        <td align="left" colspan="3" style="width: 30%">
                                            <asp:TextBox ID="salnameBox" runat="server" CssClass="textborder" MaxLength="50" Width="90%"></asp:TextBox>
                                            <span
                                                style="color: #ff0000"><strong>*</strong></span>
                                            </td>
                                    </tr>
                                    <TR bgColor="#ffffff">
											<TD  align=right style="width: 20%; height: 29px"   >
                                                <asp:Label ID="ALterNamelab" runat="server" Text="Label" Width="52px"></asp:Label></TD>
											<TD align=left width="85%" colspan=5 style="width: 80%; height: 29px"  >
                                                <asp:TextBox ID="ALterNamebox" runat="server" Width="62%" CssClass="textborder" MaxLength="50"></asp:TextBox></TD>
										</TR>
                                    <tr bgcolor="#ffffff">
                                        <td align="right" style="height: 24px; width: 20%;">
                                            <asp:Label ID="CountryIDlab" runat="server" Text="Label"></asp:Label></td>
                                        <td align="left" style="height: 24px; width: 30%;">
                                           <asp:DropDownList ID="countryiddrop" runat="server" CssClass="textborder" Width="90%">
                                            </asp:DropDownList>
                                            <font color=red><strong>*</strong></font>
                                            </td>
                                        <td align="right" style="height: 24px; width: 20%;">
                                            <asp:Label ID="StatusLab" runat="server" Text="Label"></asp:Label></td>
                                        <td align="left" colspan="3" style="width: 30%; height: 24px">
                                            <asp:DropDownList ID="StatusDrop" runat="server" CssClass="textborder" Width="97%">
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr bgcolor="#ffffff">
                                        <td align="right" style="width: 20%;" >
                                            <asp:Label ID="CreatedBylab" runat="server" Text="Label"></asp:Label></td>
                                        <td align="left" style="width: 30%"  >
                                            <asp:TextBox ID="CreatedBy" runat="server" CssClass="textborder" ReadOnly="True"
                                                Width="90%" MaxLength="20"></asp:TextBox></td>
                                        <td align="right" style="width: 20%"  >
                                            <asp:Label ID="CreatedDatelab" runat="server" Text="Label"></asp:Label></td>
                                        <td align="left" colspan="3" style="width: 30%"  >
                                            <asp:TextBox ID="CreatedDate" runat="server" CssClass="textborder" ReadOnly="True" Width="97%"
                                                ></asp:TextBox>
                                           
                                        </td>
                                    </tr>
                                    <tr bgcolor="#ffffff">
                                        <td align="right" style="width: 20%">
                                            <asp:Label ID="ModifiedBylab" runat="server" Text="Label"></asp:Label></td>
                                        <td align="left" style="width: 30%">
                                            <asp:TextBox ID="ModifiedBy" runat="server" CssClass="textborder" ReadOnly="True"
                                                Width="90%" MaxLength="20"></asp:TextBox></td>
                                        <td align="right" style="width: 20%">
                                            <asp:Label ID="ModifiedDatelab" runat="server" Text="Label"></asp:Label></td>
                                        <td align="left" colspan="3" style="width: 30%" >
                                            <asp:TextBox ID="ModifiedDate" runat="server" CssClass="textborder" ReadOnly="True" Width="97%"
                                                ></asp:TextBox>
                                           
                                        </td>
                                    </tr>
                                    <tr bgcolor="#ffffff">
                                        <td align="right">
                                            <asp:Label ID="perlab" runat="server" Text="Label" Width="52px"></asp:Label></td>
                                        <td align="left" colspan="5" width="85%">
                                            <asp:LinkButton ID="addperButton" runat="server" Width="273px"></asp:LinkButton></td>
                                    </tr>
                                </table>
                                <asp:GridView ID="saltaxsetupView" runat="server" Width="100%" AllowPaging="True">
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 753px" >
                               <asp:LinkButton ID="savebutton" runat="server"></asp:LinkButton>
                               &nbsp; &nbsp; &nbsp;<asp:HyperLink ID="cancellink" runat="server" NavigateUrl="~/PresentationLayer/masterrecord/SalTaxID.aspx">[cancelLink]</asp:HyperLink>
                               
                               &nbsp; &nbsp; &nbsp;<asp:HyperLink ID="AddLink" runat="server" >[Add]</asp:HyperLink>
                                
                            </td>
                        </tr>
                    </table>
                   
                    <asp:TextBox ID="SetUpID" runat="server" Visible="False" Wrap="False"></asp:TextBox>
                    <br />
                    <font color=red><asp:Label ID="lblCompBottom" runat="server" Text="Label" Visible="true"></asp:Label></font>
                </td>
            </tr>
        </table>
    
    
        <asp:ValidationSummary ID="ErrorMessage" runat="server" ShowMessageBox="True" ShowSummary="False" />
        <cc1:messagebox id="MessageBox1" runat="server"></cc1:messagebox>
        <asp:RequiredFieldValidator ID="saltaxIDva" runat="server" ControlToValidate="saltaxIDBox"
            ForeColor="White">*</asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator ID="salnameva" runat="server" ControlToValidate="salnamebox"
            ForeColor="White">*</asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator ID="COUNTRYIDVA" runat="server" ControlToValidate="CountryIDDrop"
            ForeColor="White">*</asp:RequiredFieldValidator>
    </form>
</body>
</html>
