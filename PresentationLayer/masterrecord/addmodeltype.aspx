<%@ Page Language="VB" AutoEventWireup="false" EnableEventValidation ="false"  CodeFile="addmodeltype.aspx.vb" Inherits="PresentationLayer_masterrecord_addmodeltype" %>

<%@ Register Assembly="JCalendar" Namespace="JCalendar" TagPrefix="cc2" %>

<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
  <title>Add Model Type</title>
	<LINK href="../css/style.css" type="text/css" rel="stylesheet">
    <script language="JavaScript" src="../js/common.js"></script> 
    
</head>
<body  id="model type">
   <form id="mdfymodeltypeform"  method="post" action=""  runat=server>
         <TABLE border="0" id=TABLE2 style="width: 100%">
           <tr>
           <td>
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>
							<TD width="1%" background="../graph/title_bg.gif"><IMG height="24" src="../graph/title1.gif" width="5"></TD>
							<TD class="style2" width="98%" background="../graph/title_bg.gif">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></TD>
							<TD align="right" width="1%" background="../graph/title_bg.gif"><IMG height="24" src="../graph/title_2.gif" width="5"></TD>
						</TR>
			 </TABLE></td>
           </tr>
           <tr>
           <td>
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>					
							<TD class="style2" width="98%" background="../graph/title_bg.gif" style="height: 14px">
                                <font color=red><asp:Label ID="addinfo" runat="server" Text="Label" Visible=false></asp:Label></font>
                                </TD>
						</TR>
			 </TABLE>
           </tr>
			<TR>
				<TD>
        <table id="countrytab" border="0" width ="100%">
            <tr>
                <td>
                    <table id="modeltype" bgcolor="#b7e6e6" border="0" cellpadding="2" cellspacing="1" width="100%">
                      <tr bgcolor="#ffffff">
<td colspan = 4>
                            <asp:Label ID="lblCompTop" runat="server" ForeColor="Red" Text="Label" Visible="True"></asp:Label>
</td>
             </tr>
             <tr bgcolor="#ffffff">
<td colspan = 4>
                        	&nbsp;
                     	</td>
               </tr> 
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 16%; height: 27px">
                                &nbsp;<asp:Label ID="CountryIDLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 34%; height: 27px">
                                &nbsp;<asp:DropDownList ID="CountryIDDDL" runat="server" CssClass="textborder" Width="62%">
                                </asp:DropDownList>
                                <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                            <td align="right" style="height: 27px">
                                <asp:Label ID="ProductClassLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 34%; height: 27px">
                                <asp:DropDownList ID="ProductClassDDL" runat="server" CssClass="textborder" Width="62%">
                                </asp:DropDownList></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 16%; height: 30px;">
                                &nbsp;<asp:Label ID="ModelIDLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 34%; height: 30px;">
                                <asp:TextBox ID="ModelIDBox" runat="server" CssClass="textborder" MaxLength="10" Width="62%"></asp:TextBox>
                                <asp:Label ID="Label2" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                            <td align="right" style="height: 30px;">
                                <asp:Label ID="ModelNameLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 34%; height: 30px;">
                                <asp:TextBox ID="ModelNameBox" runat="server" CssClass="textborder" MaxLength="50" Width="62%"></asp:TextBox>
                                <asp:Label ID="Label3" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="RequiredFieldValidator"
                                    ForeColor="White" ControlToValidate="ModelNameBox">*</asp:RequiredFieldValidator></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 16%">
                                <asp:Label ID="AlModelNamLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" colspan="3" width="85%">
                                <asp:TextBox ID="AlModelNamBox" runat="server" CssClass="textborder" MaxLength="50" Width="25%"></asp:TextBox>
                                <asp:Label ID="Label4" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 16%; height: 1px">
                                <asp:Label ID="WarrantyDayLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 34%; height: 1px">
                                <asp:TextBox ID="WarrantyDayBox" runat="server" CssClass="textborder" MaxLength="4" Width="62%"></asp:TextBox>
                                <asp:Label ID="Label7" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                <asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="RangeValidator"
                                    ForeColor="White" ControlToValidate="WarrantyDayBox" MaximumValue="999999" MinimumValue="0" Type="Integer">*</asp:RangeValidator>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="WarrantyDayBox"
                                    ErrorMessage="RequiredFieldValidator" ForeColor="White">*</asp:RequiredFieldValidator></td>
                            <td align="right" style="height: 1px">
                                <asp:Label ID="StatusLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 34%; height: 1px">
                                <asp:DropDownList ID="StatusDDL" runat="server" CssClass="textborder" Width="62%">
                                </asp:DropDownList></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 16%; height: 1px;">
                                <asp:Label ID="EffectiveDateLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 34%; height: 1px; color: red;">
                                <asp:TextBox ID="EffectiveDateBox" runat="server" CssClass="textborder" ReadOnly="True" Width="62%"></asp:TextBox>
                                <strong>*<asp:HyperLink ID="HypCal" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                                    ToolTip="Choose a Date">Choose a Date</asp:HyperLink></strong><cc2:JCalendar ID="EffectiveDateBoxJCalendar" runat="server" ImgURL="~/PresentationLayer/graph/calendar.gif" ControlToAssign="EffectiveDateBox" Visible="False" />
                            </td>
                            <td align="right" style="height: 1px;">
                                <asp:Label ID="ObsoleteDateLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 34%; height: 1px; color: red;">
                                <asp:TextBox ID="ObsoleteDateLabBox" runat="server" CssClass="textborder" ReadOnly="True" Width="62%"></asp:TextBox>
                                <strong>*<asp:HyperLink ID="HypCal2" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                                    ToolTip="Choose a Date">Choose a Date</asp:HyperLink></strong><cc2:JCalendar  ID="ObsoleteDateLabBoxJCalendar"  runat="server" ImgURL="~/PresentationLayer/graph/calendar.gif" ControlToAssign="ObsoleteDateLabBox" Visible="False" />
                            </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 16%; height: 30px;">
                                <asp:Label ID="creatby" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 34%; height: 30px;">
                                <asp:TextBox ID="creatbybox" runat="server" CssClass="textborder" ReadOnly="true" Width="62%"></asp:TextBox></td>
                            <td align="right" style="height: 30px;">
                                <asp:Label ID="creatdate" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 34%; height: 30px;">
                                <asp:TextBox ID="creatdtbox" runat="server" CssClass="textborder" ReadOnly="true" Width="62%"></asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 16%">
                                <asp:Label ID="modfyby" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 34%">
                                <asp:TextBox ID="modfybybox" runat="server" CssClass="textborder" ReadOnly="true" Width="62%"></asp:TextBox></td>
                            <td align="right">
                                <asp:Label ID="mdfydate" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 34%">
                                <asp:TextBox ID="mdfydtbox" runat="server" CssClass="textborder" ReadOnly="true" Width="62%"></asp:TextBox></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 865px; height: 2px">
                <font color=red><asp:Label ID="lblCompBottom" runat="server" Text="Label" Visible="true"></asp:Label></font>
                    <table id="Table1" border="0" cellpadding="1" cellspacing="1">
                        <tr>
                            <td align="center" style="width: 20%">
                                &nbsp;<asp:LinkButton ID="LinkButton1" runat="server">save</asp:LinkButton></td>
                            <td align="center">
                                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/PresentationLayer/masterrecord/modeltype.aspx">cancel</asp:HyperLink></td>
                        <td align=center><asp:HyperLink ID="AddLink" runat="server" >[Add]</asp:HyperLink></td>

</tr>
                    </table>
                </td>
            </tr>
        </table>
                     			</TD>
					</TR>
			</TABLE>
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                        ShowSummary="False" />
       &nbsp; &nbsp;&nbsp;
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ModelIDBox"
                        ErrorMessage="RequiredFieldValidator" ForeColor="White">*</asp:RequiredFieldValidator>
                    <cc1:messagebox id="MessageBox1" runat="server"></cc1:messagebox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="EffectiveDateBox"
                                    ErrorMessage="RequiredFieldValidator" ForeColor="White">*</asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ObsoleteDateLabBox"
                                    ErrorMessage="RequiredFieldValidator" ForeColor="White">*</asp:RequiredFieldValidator>
       <asp:RequiredFieldValidator ID="rfvctry" runat="server" ControlToValidate="CountryIDDDL"
           ForeColor="White">.</asp:RequiredFieldValidator>
       <asp:RequiredFieldValidator ID="rfvaname" runat="server" ControlToValidate="AlModelNamBox"
           ForeColor="White" Height="1px" Width="1px">.</asp:RequiredFieldValidator>
    
    </form>
 
</body>
</html>
