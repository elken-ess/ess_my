<%@ Page Language="VB" AutoEventWireup="false" EnableEventValidation="false" CodeFile="addPriceSetUp1.aspx.vb" Inherits="PresentationLayer_masterrecord_addPriceSetUp1" %>

<%@ Register Assembly="JCalendar" Namespace="JCalendar" TagPrefix="cc2" %>

<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>add country message</title>
    <meta http-equiv="Content-Type" content="text/html; charset=gb2312">
	<LINK href="../css/style.css" type="text/css" rel="stylesheet">
	<script language="JavaScript" src="../js/common.js"></script> 
</head>
<body  id="PriceSetUp1">
   <form id="PriceSetUp1form"  method="post" action=""  runat=server>
     <TABLE border="0" id=PriceSetUp1tab style="width: 100%">
           <tr>
           <td >
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>
							<TD width="1%" background="../graph/title_bg.gif"><IMG height="24" src="../graph/title1.gif" width="5"></TD>
							<TD class="style2" width="98%" background="../graph/title_bg.gif">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></TD>
							<TD align="right" width="1%" background="../graph/title_bg.gif"><IMG height="24" src="../graph/title_2.gif" width="5"></TD>
						</TR>
			 </TABLE>
           </tr>
           <tr>
           <td  >
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>					
							<TD class="style2" width="98%" background="../graph/title_bg.gif" >
                                <font color=red><asp:Label ID="errlab" runat="server" Text="Label" Visible="false"></asp:Label></font>
                               </TD>
						</TR>
			 </TABLE>
           </tr>
			<TR>
				<TD >
					<TABLE id="country" cellSpacing="1" cellPadding="2" bgColor="#b7e6e6" border="0" style="width: 100%">
									    <tr bgcolor="#ffffff">
<td colspan = 4>
                            <asp:Label ID="lblCompTop" runat="server" ForeColor="Red" Text="Label" Visible="True"></asp:Label>
</td>
             </tr>
             <tr bgcolor="#ffffff">
<td colspan = 4>
                        	&nbsp;
                     	</td>
               </tr> 
									  <TR bgColor="#ffffff">
											<TD  align=right style="width: 20%" >
                                                <asp:Label ID="priceidlab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 30%" >
                                                <asp:TextBox ID="priceidBox" runat="server" CssClass="textborder" Width="80%" MaxLength="10" ReadOnly="True"></asp:TextBox>
                                            </TD>
											<TD align=right style="width: 20%" >
                                                <asp:Label ID="partidlab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 28%" >
                                                <asp:TextBox ID="partidBox" runat="server" CssClass="textborder" ReadOnly="True" MaxLength="10" Width="90%"></asp:TextBox>
                     </TR>
										<TR bgColor="#ffffff">
											
                                            <TD  align=right style="width: 20%" >
                                                <asp:Label ID="countryidLab" runat="server">label</asp:Label></TD>
											<TD align=left width="85%" colspan=3 style="width: 80%">
                                                <asp:TextBox ID="countryidBox" runat="server" Width="63%" CssClass="textborder" ReadOnly="True" MaxLength="2"></asp:TextBox>
                                                
                                            </TD>
										</TR>
									  
									  
									  <TR bgColor="#ffffff">
											<TD  align=right width="15%" style="width: 15%;" >
                                                <asp:Label ID="amountlab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%; ">
                                                <asp:TextBox ID="amountbox" runat="server" CssClass="textborder" Width="80%" MaxLength="13"></asp:TextBox>
                                                <font color=red style="width: 34%"><strong>*</strong></font><asp:RangeValidator ID="amountva" runat="server" ControlToValidate="amountbox" ForeColor="White"
                                                    Type="Double" MaximumValue="9999999999.99" MinimumValue="0000000000.00">*</asp:RangeValidator><asp:RequiredFieldValidator ID="amountvava" runat="server" ControlToValidate="amountbox"
                                                    ErrorMessage="RequiredFieldValidator" ForeColor="White">*</asp:RequiredFieldValidator></TD>
											<TD align=right width="15%" style="width: 15%; ">
                                                <asp:Label ID="statusLab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%; ">
                                                <asp:DropDownList ID="statusDrop" runat="server" CssClass="textborder">
                                                </asp:DropDownList>
										</TR>
										<TR bgColor="#ffffff">
											
                                            <TD  align=right width="15%" style="width: 20%; height: 30px;">
                                                <asp:Label ID="effectivelab" runat="server"></asp:Label></TD>
											<TD align=left width="85%" colspan=3 style="width: 80%; height: 30px;">
                                                <asp:TextBox ID="effectivebox" runat="server" Width="63%" CssClass="textborder" ReadOnly="True"></asp:TextBox>
                                                <font color=red><strong>*<asp:HyperLink ID="HypCal" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                                                    ToolTip="Choose a Date">Choose a Date</asp:HyperLink></strong><cc2:JCalendar
                                                    ID="Calendar" runat="server" ImgURL="~/PresentationLayer/graph/calendar.gif" Visible="False" />
                                                </font>&nbsp;
                                                <asp:RequiredFieldValidator ID="EFFDATEVA" runat="server" ControlToValidate="effectivebox"
                                                    ForeColor="White">*</asp:RequiredFieldValidator>&nbsp;
                                            </TD>
										</TR>
										
										
										<TR bgColor="#ffffff">
											<TD align=right width="15%" style="width: 20%;">
                                                <asp:Label ID="createby" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 30%;">
                                                <asp:TextBox ID="creatbybox" runat="server" CssClass="textborder" ReadOnly=true Width="80%"></asp:TextBox></TD>
											<TD align=right width="15%" style="width: 20%;">
                                                <asp:Label ID="creatdate" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 28%;">
                                                <asp:TextBox ID="creatdtbox" runat="server" CssClass="textborder" ReadOnly=true Width="90%"></asp:TextBox></TD>
										</TR>
										<TR bgColor="#ffffff">
											<TD align=right width="15%" style="width: 20%; height: 30px">
                                                <asp:Label ID="modifyby" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 30%; height: 30px;">
                                                <asp:TextBox ID="modfybybox" runat="server" CssClass="textborder" ReadOnly=true Width="80%"></asp:TextBox></TD>
											<TD align=right width="15%" style="width: 20%; height: 30px">
                                                <asp:Label ID="mdifydate" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 28%; height: 30px">
                                                <asp:TextBox ID="mdfydtbox" runat="server" CssClass="textborder" ReadOnly=true Width="90%"></asp:TextBox></TD>
										</TR>
								</TABLE>
                    <asp:GridView ID="PriceSetUpView" runat="server" Width="100%" AllowPaging="True">
                    </asp:GridView>
                    <asp:TextBox ID="pricesetupidbox" runat="server" Visible="False"></asp:TextBox><br />
                    <font color=red><asp:Label ID="lblCompBottom" runat="server" Text="Label" Visible="true"></asp:Label></font>
                </TD>
					</TR>
					<TR>
						<TD style="width: 791px" >
							<TABLE id="Table1" cellSpacing="1" cellPadding="1"  border="0">
									<TR>
										<TD style="WIDTH: 20%; height: 18px;" align=center>
                                            &nbsp;<asp:LinkButton ID="saveButton" runat="server"></asp:LinkButton></TD>
										<td align=center style="height: 18px">
                                            &nbsp;<asp:LinkButton ID="cancel" runat="server" CausesValidation="False"></asp:LinkButton></td>
									</TR>
							</TABLE>
						</TD>
					</TR>
			</TABLE>
       <cc1:messagebox id="MessageBox1" runat="server"></cc1:messagebox>
       <asp:ValidationSummary ID="ERRORMESSAGE" runat="server" ShowMessageBox="True" ShowSummary="False" />
    </form>
</body>
</html>
