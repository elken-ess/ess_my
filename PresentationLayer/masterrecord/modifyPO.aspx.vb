﻿Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.data
Partial Class PresentationLayer_masterrecord_modifyPO
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then

            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try


            '''''''''''''''''''''''''''''''''''''''''''''''''''''
            Try
                Dim str As String = Request.Params("mdfyctrid").ToString()
                str = Request.Params("mdfystaid").ToString()
                str = Request.Params("areid").ToString()
                str = Request.Params("pocode").ToString()
                str = Request.Params("postat").ToString()
            Catch ex As Exception
                Response.Redirect("~/PresentationLayer/logon.aspx")
            End Try
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Dim mdfyctrid As String = Request.Params("mdfyctrid").ToString()
            Dim mdfystaid As String = Request.Params("mdfystaid").ToString()
            Dim mdfyareid As String = Request.Params("areid").ToString()
            Dim mdfypcode As String = Request.Params("pocode").ToString()
            Dim mdfystat As String = Request.Params("postat").ToString()
           

            Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle



            'display label message
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            Me.areidlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASAREA-0001")
            Me.pcodelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASAREA-0006")
            statusLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCTRY-0004")

            creatby.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATBY")
            creatdate.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
            modfyby.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFBY")
            mdfydate.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFDATE")

            saveButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Save")
            cancelLink.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Cancel")
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "MODPOTL")



            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")

            'Dim strPanm As String = objXmlTr.GetLabelID("StatusMessage", mdfystat)

            Dim strPanm As String = mdfystat
            Dim areaEntity As New clsarea()

            areaEntity.CountryID = mdfyctrid
            areaEntity.StateID = mdfystaid
            areaEntity.AreaID = mdfyareid
            areaEntity.POID = mdfypcode
            areaEntity.POStatus = strPanm

            Dim edtReader As SqlDataReader = areaEntity.GetPODetail_byAreaID()
            If edtReader.Read() Then

                Me.areidbox.Text = edtReader.GetValue(0).ToString()
                Me.areidbox.ReadOnly = True
                Me.pcodebox.Text = edtReader.GetValue(1).ToString()
                Me.pcodebox.ReadOnly = True
                Dim AA As String = edtReader.GetValue(2).ToString()
                'create the dropdownlist
                Dim cntryStat As New clsrlconfirminf()
                Dim statParam As ArrayList = New ArrayList
                statParam = cntryStat.searchconfirminf("STATUS")
                Dim count As Integer
                Dim statid As String
                Dim statnm As String
                For count = 0 To statParam.Count - 1
                    statid = statParam.Item(count)
                    statnm = statParam.Item(count + 1)
                    If (statid.Equals("ACTIVE") Or statid.Equals("DELETE")) Then
                        ctrystat.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                    End If
                    If statid.Equals(edtReader.GetValue(2).ToString()) Then
                        ctrystat.Items(count / 2).Selected = True
                    End If
                    count = count + 1
                Next



                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Dim accessgroup As String = Session("accessgroup").ToString
                Dim purviewArray As Array = New Boolean() {False, False, False, False, False}
                purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "10")

                If purviewArray(2) = False Then
                    Response.Redirect("~/PresentationLayer/logon.aspx")
                End If
                Me.saveButton.Enabled = purviewArray(1)
                If Convert.ToString(purviewArray(4)).ToUpper = "FALSE" Then
                    Me.ctrystat.Items.Remove(Me.ctrystat.Items.FindByValue("DELETE"))
                End If

                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                'creatbybox.Text = edtReader.GetValue(3).ToString().ToUpper()

                Dim Rcreatby As String = edtReader.GetValue(3).ToString().ToUpper()
                If Rcreatby <> "ADMIN" Then
                    Dim Rcreat As New clsCommonClass
                    Rcreat.userid() = Rcreatby
                    Dim Rcreatds As New DataSet()
                    Rcreatds = Rcreat.Getuseridname("BB_MASUSER_SelByIDName")

                    Me.creatbybox.Text = Rcreatds.Tables(0).Rows(0).Item(0)
                Else
                    Me.creatbybox.Text = "ADMIN-ADMIN"
                End If
                creatbybox.ReadOnly = True

                creatdtbox.Text = edtReader.GetValue(4).ToString()
                creatdtbox.ReadOnly = True
                'modfybybox.Text = edtReader.GetValue(5).ToString().ToUpper()
                modfybybox.Text = Session("userID").ToString().ToUpper + "-" + Session("username").ToString.ToUpper
                modfybybox.ReadOnly = True
                mdfydtbox.Text = edtReader.GetValue(6).ToString()
                'mdfydtbox.Text = Session("userID").ToString().ToUpper
                mdfydtbox.ReadOnly = True

            End If

            Dim poall As DataSet = areaEntity.GetPO()
            POView.DataSource = poall
            POView.DataBind()
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            If poall.Tables(0).Rows.Count >= 1 Then
                POView.HeaderRow.Cells(0).Text = objXmlTr.GetLabelName("EngLabelMsg", "POHD1")
                POView.HeaderRow.Cells(0).Font.Size = 8
                POView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "POHD2")
                POView.HeaderRow.Cells(1).Font.Size = 8
                POView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "POHD3")
                POView.HeaderRow.Cells(2).Font.Size = 8
            End If


        End If


    End Sub

    Protected Sub saveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveButton.Click

        Dim areaEntity As New clsarea()
        areaEntity.CountryID = Request.Params("mdfyctrid").ToString()
        areaEntity.StateID = Request.Params("mdfystaid").ToString()
        areaEntity.AreaID = Me.areidbox.Text
        areaEntity.POStatus = Me.ctrystat.SelectedItem.Value()
        areaEntity.POID = Me.pcodebox.Text
        areaEntity.ModifyBy = Session("userID").ToString().ToUpper
        areaEntity.IPaddr = Request.UserHostAddress.ToString()
        areaEntity.servid = Session("login_svcID")

        Dim updCtryCnt As Integer = areaEntity.Update_po()

        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        If updCtryCnt = 0 Then
            Dim objXm As New clsXml
            objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            errlab.Text = objXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Suc")
            errlab.Visible = True

            Dim poall As DataSet = areaEntity.GetPO()
            POView.DataSource = poall
            POView.DataBind()
            Dim obiXmlTr As New clsXml
            obiXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            If poall.Tables(0).Rows.Count >= 1 Then
                POView.HeaderRow.Cells(0).Text = obiXmlTr.GetLabelName("EngLabelMsg", "POHD1")
                POView.HeaderRow.Cells(0).Font.Size = 8
                POView.HeaderRow.Cells(1).Text = obiXmlTr.GetLabelName("EngLabelMsg", "POHD2")
                POView.HeaderRow.Cells(1).Font.Size = 8
                POView.HeaderRow.Cells(2).Text = obiXmlTr.GetLabelName("EngLabelMsg", "POHD3")
                POView.HeaderRow.Cells(2).Font.Size = 8
            End If
        Else

            Response.Redirect("~/PresentationLayer/Error.aspx")
        End If
    End Sub

    Protected Sub cancelLink_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cancelLink.Click
        Dim ctrid As String = Request.Params("mdfyctrid").ToString()
        Dim staid As String = Request.Params("mdfystaid").ToString()
        Dim areid As String = Request.Params("areid").ToString()
        Dim stat As String = Request.Params("mdfystat").ToString()
        Dim url As String = "~/PresentationLayer/masterrecord/modiarea.aspx?"
        url &= "areid=" & areid & "&"
        url &= "ctrid=" & ctrid & "&"
        url &= "staid=" & staid & "&"
        url &= "arestat=" & stat
        ' & "&"
        '       url &= "modisy=" & 1

        Response.Redirect(url)
    End Sub
End Class
