<%@ Page Language="VB" AutoEventWireup="false" EnableEventValidation ="false"  CodeFile="addcountry.aspx.vb" Inherits="PresentationLayer_masterrecord_addcountry" %>

<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>add country</title>
    <meta http-equiv="Content-Type" content="text/html; charset=gb2312">
	<LINK href="../css/style.css" type="text/css" rel="stylesheet">
</head>
<body  id="countrybody">
   <form id="Countryform"  method="post" action=""  runat=server>
     <TABLE border="0" id=countrytab style="width: 100%">
        <tr>
           <td>
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>
							<TD width="1%" background="../graph/title_bg.gif"><IMG height="24" src="../graph/title1.gif" width="5"></TD>
							<TD class="style2" width="98%" background="../graph/title_bg.gif">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></TD>
							<TD align="right" width="1%" background="../graph/title_bg.gif"><IMG height="24" src="../graph/title_2.gif" width="5"></TD>
						</TR>
			 </TABLE>
           </tr>
           <tr>
           <td>
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>					
							<TD class="style2" width="98%" background="../graph/title_bg.gif">
                                <font color=red><asp:Label ID="errlab" runat="server" Text="Label" Visible=false></asp:Label></font>
                                </TD>
						</TR>
			 </TABLE>
           </tr>
			<TR>
				<TD style="width: 0px">
					<TABLE id="country" cellSpacing="1" cellPadding="2" bgColor="#b7e6e6" border="0" style="width: 100%">
					<tr bgcolor="#ffffff">
<td colspan = 4>
                            <asp:Label ID="lblCompTop" runat="server" ForeColor="Red" Text="Label" Visible="True"></asp:Label>
</td>
             </tr>
             <tr bgcolor="#ffffff">
<td colspan = 4>
                        	&nbsp;
                     	</td>
               </tr>   
									  <TR bgColor="#ffffff">
											<TD  align=right width="15%" style="height: 30px">
                                                &nbsp;<asp:Label ID="ctryidlab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%; height: 30px;">
                                                <asp:TextBox ID="ctryidbox" runat="server" CssClass="textborder" MaxLength="2" Width="88%" style="width: 88%"></asp:TextBox>
                                                <font color=red><strong>*</strong></font>
                                                </TD>
											<TD  align=right width="15%" style="height: 30px">
                                                <asp:Label ID="ctrynameLab" runat="server" Text="Label"></asp:Label></TD>
											<TD  align=left style="height: 30px; width: 35%;">
                                                <asp:TextBox ID="ctrynmbox" runat="server" CssClass="textborder" Width="88%" MaxLength="50" style="width: 88%"></asp:TextBox>
                                                <font color=red><strong>*</strong></font>
                                                </TD>
										</TR>
										<TR bgColor="#ffffff">
											<TD  align=right width="15%">
                                                <asp:Label ID="ctryalterLab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left width="85%" colspan=3>
                                                <asp:TextBox ID="ctryaltbox" runat="server" CssClass="textborder" MaxLength="50" style="width: 35%"></asp:TextBox>
                                                <font color=red><strong>*</strong></font>
                                                <asp:RequiredFieldValidator ID="ctryalterr" runat="server" ControlToValidate="ctryaltbox" ForeColor = White>*</asp:RequiredFieldValidator>
                                                </TD>
										</TR>
										<TR bgColor="#ffffff">
											<TD  align=right width="15%">
                                                <asp:Label ID="ctrytelpf" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%">
                                                <asp:TextBox ID="ctrytelbox" runat="server" CssClass="textborder" MaxLength="10" style="width: 88%" Width="88%"></asp:TextBox></TD>
											<TD align=right width="15%">
                                                <asp:Label ID="statusLab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%">
                                                <asp:DropDownList ID="ctrystat" runat="server" CssClass="textborder" style="width: 88%" Width="88%">
                                                </asp:DropDownList>
											</TD>
										</TR>
										<TR bgColor="#ffffff">
											<TD align=right width="15%">
                                                <asp:Label ID="curridLab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%">
                                                <asp:TextBox ID="ctrycuridbox" runat="server" CssClass="textborder" MaxLength="10" style="width: 88%" Width="88%"></asp:TextBox></TD>
											<TD align=right width="15%">
                                                <asp:Label ID="currnmLab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%">
                                                <asp:TextBox ID="ctrycurnmbox" runat="server" CssClass="textborder" MaxLength="50" style="width: 88%" Width="88%"></asp:TextBox></TD>
										</TR>
										<TR bgColor="#ffffff">
											<TD align=right width="15%">
                                                <asp:Label ID="creatby" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%">
                                                <asp:TextBox ID="creatbybox" runat="server" CssClass="textborder" ReadOnly=true Rows="20" style="width: 88%" Width="88%"></asp:TextBox></TD>
											<TD align=right width="15%">
                                                <asp:Label ID="creatdate" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%">
                                                <asp:TextBox ID="creatdtbox" runat="server" CssClass="textborder" ReadOnly=true style="width: 88%" Width="88%"></asp:TextBox></TD>
										</TR>
										<TR bgColor="#ffffff">
											<TD align=right width="15%">
                                                <asp:Label ID="modfyby" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%">
                                                <asp:TextBox ID="modfybybox" runat="server" CssClass="textborder" ReadOnly=true Rows="20" style="width: 88%" Width="88%"></asp:TextBox></TD>
											<TD align=right width="15%">
                                                <asp:Label ID="mdfydate" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%">
                                                <asp:TextBox ID="mdfydtbox" runat="server" CssClass="textborder" ReadOnly=true style="width: 88%" Width="88%"></asp:TextBox></TD>
										</TR>
								</TABLE>
						</TD>
					</TR>
					<TR>
						<TD style="height: 37px; width: 409px;">
						<font color=red><asp:Label ID="lblCompBottom" runat="server" Text="Label" Visible="true"></asp:Label></font>
							<TABLE id="Table1" cellSpacing="1" cellPadding="1"  border="0">
									<TR>
										<TD style="WIDTH: 20%" align=center>
                                            &nbsp;<asp:LinkButton ID="saveButton" runat="server"></asp:LinkButton></TD>
										<td align=center>
                                            <asp:HyperLink ID="cancelLink" runat="server" NavigateUrl="~/PresentationLayer/masterrecord/country.aspx">[cancelLink]</asp:HyperLink></td>
									<td align=center><asp:HyperLink ID="AddLink" runat="server" >[Add]</asp:HyperLink></td> </TR>
							</TABLE>
						</TD>
					</TR>
			</TABLE>
       <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
           ShowSummary="False" />
       <cc1:messagebox id="MessageBox1" runat="server"></cc1:messagebox>
                                                <asp:RequiredFieldValidator ID="ctryiderr" runat="server" ControlToValidate="ctryidbox" Width="2px"  ForeColor = White>*</asp:RequiredFieldValidator>
                                                <asp:RequiredFieldValidator ID="ctrynmerr" runat="server" ControlToValidate="ctrynmbox"  ForeColor = White>*</asp:RequiredFieldValidator>
    </form>
</body>
</html>
