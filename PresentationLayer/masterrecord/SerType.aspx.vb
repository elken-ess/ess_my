Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data

Partial Class PresentationLayer_masterrecord_SerType
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Page.IsPostBack) Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try

            BindGrid()
        End If
    End Sub
    Protected Sub BindGrid()
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        Me.SerTypeIDlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSRCT-0001")
        Me.SerTypeNamelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSRCT-0002")
        Me.Statuslab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Status")
        Me.LinkButton1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Add")
        Me.LinkButton2.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Search")
        Me.titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSRCT-0005")
        Me.Label1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-NR")
        Me.Label1.Visible = False
        Me.addinfo.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-AR")
        Me.addinfo.Visible = False

        Me.SerTypeIDBox.Focus()
        Dim sertypeStat As New clsrlconfirminf()
        Dim statParam As ArrayList = New ArrayList
        statParam = sertypeStat.searchconfirminf("STATUS")
        Dim count As Integer
        Dim statid As String
        Dim statnm As String
        For count = 0 To statParam.Count - 1
            statid = statParam.Item(count)
            statnm = statParam.Item(count + 1)
            count = count + 1
            If (statid.Equals("ACTIVE") Or statid.Equals("OBSOLETE")) Then
                Me.StatusDrop.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
            End If
        Next
        StatusDrop.Items(0).Selected = True

        Dim SerTypeEntity As New clsSerType()
        Dim rank As String = Session("login_rank").ToString().ToUpper()
        If rank <> 0 Then
            SerTypeEntity.CountryID = Session("login_ctryID").ToString().ToUpper()
        End If
        SerTypeEntity.rank = rank
        If (StatusDrop.SelectedValue().ToString() <> "") Then
            SerTypeEntity.SerTypeStatus = StatusDrop.SelectedValue().ToString()
        End If

        Dim SerTypeIDall As DataSet = SerTypeEntity.GetSerTypeID1()
        SerTypeView.DataSource = SerTypeIDall
        Session("SerTypeView") = SerTypeIDall
        checknorecord(SerTypeIDall)
        SerTypeView.AllowPaging = True
        SerTypeView.AllowSorting = True

        Dim editcol As New CommandField
        editcol.EditText = objXmlTr.GetLabelName("EngLabelMsg", "BB-Edit") '"edit"
        editcol.HeaderImageUrl = "~/PresentationLayer/graph/edit.gif"
        editcol.ShowEditButton = True
        SerTypeView.Columns.Add(editcol)


        '''access control'''''''''''''''''''''''''''''''''''''''''''''''
        Dim accessgroup As String = Session("accessgroup").ToString
        Dim purviewArray As Array = New Boolean() {False, False, False, False, False}
        purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "21")
        Me.LinkButton1.Enabled = purviewArray(0)
        editcol.Visible = purviewArray(2)


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


        SerTypeView.DataBind()
        If SerTypeIDall.Tables(0).Rows.Count <> 0 Then
            SerTypeView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSRCT-0001")
            SerTypeView.HeaderRow.Cells(1).Font.Size = 8
            SerTypeView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSRCT-0002")
            SerTypeView.HeaderRow.Cells(2).Font.Size = 8
            SerTypeView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Status")
            SerTypeView.HeaderRow.Cells(3).Font.Size = 8
            SerTypeView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSRCT-0014")
            SerTypeView.HeaderRow.Cells(4).Font.Size = 8
            SerTypeView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSRCT-0015")
            SerTypeView.HeaderRow.Cells(5).Font.Size = 8
          

        End If

    End Sub
    Protected Sub Searchlink_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton2.Click
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        SerTypeView.PageIndex = 0
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        Dim SerTypeEntity As New clsSerType()
        Dim rank As String = Session("login_rank").ToString().ToUpper()
        If rank <> 0 Then
            SerTypeEntity.CountryID = Session("login_ctryID").ToString().ToUpper()
        End If
        SerTypeEntity.rank = rank
        If (Trim(SerTypeIDBox.Text) <> "") Then
            SerTypeEntity.SerTypeID = SerTypeIDBox.Text
        End If
        If (Trim(SerTypeNameBox.Text) <> "") Then
            SerTypeEntity.SerTypeName = SerTypeNameBox.Text
        End If
        If (StatusDrop.SelectedValue().ToString() <> "") Then
            SerTypeEntity.SerTypeStatus = StatusDrop.SelectedValue().ToString()
        End If
        checkinfo()
        SerTypeEntity.ModifiedBy = Session("username")
        Dim selDS As DataSet = SerTypeEntity.GetSerTypeID1()
        SerTypeView.DataSource = selDS
        SerTypeView.DataBind()
        Session("SerTypeView") = selDS
        checknorecord(selDS)
        If selDS.Tables(0).Rows.Count <> 0 Then
            SerTypeView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSRCT-0001")
            SerTypeView.HeaderRow.Cells(1).Font.Size = 8
            SerTypeView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSRCT-0002")
            SerTypeView.HeaderRow.Cells(2).Font.Size = 8
            SerTypeView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Status")
            SerTypeView.HeaderRow.Cells(3).Font.Size = 8
            SerTypeView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSRCT-0014")
            SerTypeView.HeaderRow.Cells(4).Font.Size = 8
            SerTypeView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSRCT-0015")
            SerTypeView.HeaderRow.Cells(5).Font.Size = 8
           


        End If
    End Sub

    Protected Sub SerTypeView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles SerTypeView.PageIndexChanging
        SerTypeView.PageIndex = e.NewPageIndex
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Dim SerTypeEntity As New clsSerType()
        Dim rank As String = Session("login_rank").ToString().ToUpper()
        If rank <> 0 Then
            SerTypeEntity.CountryID = Session("login_ctryID").ToString().ToUpper()
        End If
        SerTypeEntity.rank = rank
        If (Trim(SerTypeIDBox.Text) <> "") Then
            SerTypeEntity.SerTypeID = SerTypeIDBox.Text
        End If
        If (Trim(SerTypeNameBox.Text) <> "") Then
            SerTypeEntity.SerTypeName = SerTypeNameBox.Text
        End If
        If (StatusDrop.SelectedValue().ToString() <> "") Then
            SerTypeEntity.SerTypeStatus = StatusDrop.SelectedValue().ToString()
        End If
       
        Dim selDS As DataSet = SerTypeEntity.GetSerTypeID1()
        SerTypeView.DataSource = selDS
        SerTypeView.DataBind()
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        If selDS.Tables(0).Rows.Count <> 0 Then
            SerTypeView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSRCT-0001")
            SerTypeView.HeaderRow.Cells(1).Font.Size = 8
            SerTypeView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSRCT-0002")
            SerTypeView.HeaderRow.Cells(2).Font.Size = 8
            SerTypeView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Status")
            SerTypeView.HeaderRow.Cells(3).Font.Size = 8
            SerTypeView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSRCT-0014")
            SerTypeView.HeaderRow.Cells(4).Font.Size = 8
            SerTypeView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSRCT-0015")
            SerTypeView.HeaderRow.Cells(5).Font.Size = 8

        End If

    End Sub

    Protected Sub SerTypeView_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles SerTypeView.RowEditing
        Dim ds As DataSet = Session("SerTypeView")
        Dim page As Integer = SerTypeView.PageIndex * SerTypeView.PageSize
        Dim SerTypeid As String = Me.SerTypeView.Rows(e.NewEditIndex).Cells(1).Text
        SerTypeid = Server.UrlEncode(SerTypeid)
        'Dim statusid As String = ds.Tables(0).Rows(page + e.NewEditIndex).Item(2).ToString()
        'statusid = Server.UrlEncode(statusid)
        Dim strTempURL As String = "SerTypeID=" + SerTypeid
        '+ "&StatusID=" + statusid
        strTempURL = "~/PresentationLayer/masterrecord/modifySerType.aspx?" + strTempURL
        Response.Redirect(strTempURL)
    End Sub
#Region "if have no record "
    Public Function checknorecord(ByVal ds As DataSet) As Integer
        If ds.HasErrors Then
            If ds.Tables(0).Rows.Count = 0 Then
                Me.Label1.Visible = True
                'Me.addinfo.Visible = False
            Else
                Me.Label1.Visible = False
            End If

        End If
        If ds.Tables(0).Rows.Count = 0 Then
            Me.Label1.Visible = True
            'Me.addinfo.Visible = False
        Else
            Me.Label1.Visible = False
        End If

    End Function
#End Region
#Region "��ʾ��Ϣ"
    Public Function checkinfo() As Integer
        If Me.SerTypeIDBox.Text = "" And Me.SerTypeNameBox.Text = "" Then
            Me.addinfo.Visible = True
        Else
            Me.addinfo.Visible = False
        End If
    End Function
#End Region
End Class
