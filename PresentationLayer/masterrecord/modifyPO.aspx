﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="modifyPO.aspx.vb" Inherits="PresentationLayer_masterrecord_modifyPO" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    	<LINK href="../css/style.css" type="text/css" rel="stylesheet">
</head>
<body>
    <form id="form1" runat="server">
   <TABLE border="0" id=countrytab style="width: 100%">
           <tr>
           <td>
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>
							<TD width="1%" background="../graph/title_bg.gif"><IMG height="24" src="../graph/title1.gif" width="5"></TD>
							<TD class="style2" width="98%" background="../graph/title_bg.gif">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></TD>
							<TD align="right" width="1%" background="../graph/title_bg.gif"><IMG height="24" src="../graph/title_2.gif" width="5"></TD>
						</TR>
			 </TABLE>
           </tr>
           <tr>
           <td>
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>					
							<TD class="style2" width="98%" background="../graph/title_bg.gif">
                                <font color=red><asp:Label ID="errlab" runat="server" Text="Label" Visible=false></asp:Label></font></TD>
						</TR>
			 </TABLE>
           </tr>
			<TR>
				<TD>
					<TABLE id="country" cellSpacing="1" cellPadding="2" bgColor="#b7e6e6" border="0" style="width: 100%">
									  
									  <TR bgColor="#ffffff">
											<TD align=right width="15%">
                                                <asp:Label ID="areidlab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%" colspan =3>
                                                <asp:TextBox ID="areidbox" runat="server" CssClass="textborder" ReadOnly=true></asp:TextBox></TD>
											  
										</TR>
										<TR bgColor="#ffffff">
											<TD  align=right width="15%">
                                                <asp:Label ID="pcodelab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%">
                                                <asp:TextBox ID="pcodebox" runat="server" CssClass="textborder"></asp:TextBox></TD>
											<TD align=right width="15%">
                                                <asp:Label ID="statusLab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left width="35%">
                                                <asp:DropDownList ID="ctrystat" runat="server" CssClass="textborder">
                                                </asp:DropDownList>
											</TD>
										</TR>
										 
										<TR bgColor="#ffffff">
											<TD align=right width="15%">
                                                <asp:Label ID="creatby" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%">
                                                <asp:TextBox ID="creatbybox" runat="server" CssClass="textborder" ReadOnly=true></asp:TextBox></TD>
											<TD align=right width="15%">
                                                <asp:Label ID="creatdate" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left width="35%">
                                                <asp:TextBox ID="creatdtbox" runat="server" CssClass="textborder" ReadOnly=true></asp:TextBox></TD>
										</TR>
										<TR bgColor="#ffffff">
											<TD align=right width="15%">
                                                <asp:Label ID="modfyby" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%">
                                                <asp:TextBox ID="modfybybox" runat="server" CssClass="textborder" ReadOnly=true></asp:TextBox></TD>
											<TD align=right width="15%">
                                                <asp:Label ID="mdfydate" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left width="35%">
                                                <asp:TextBox ID="mdfydtbox" runat="server" CssClass="textborder" ReadOnly=true></asp:TextBox></TD>
										</TR>
								</TABLE>
								<div>	&nbsp;</div>
						</TD>
					</TR>
					
					 
					<TR>
						<TD>
							<TABLE id="Table1" cellSpacing="1" cellPadding="1"  border="0">
									<TR>
										<TD style="WIDTH: 20%; height: 16px;" align=center>
                                            &nbsp;<asp:LinkButton ID="saveButton" runat="server"></asp:LinkButton></TD>
										<td align=center style="width: 100%">
                                            <asp:LinkButton ID="cancelLink" runat="server">LinkButton</asp:LinkButton></td>
									</TR>
							</TABLE>&nbsp;&nbsp;
                            <br />
                    <asp:GridView ID="POView" runat="server">
                        <SelectedRowStyle BackColor="White" />
                    </asp:GridView> 
						</TD>
					</TR>
			</TABLE>
    </form>
</body>
</html>
