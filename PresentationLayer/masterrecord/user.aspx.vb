Imports System.Xml
Imports System.Web
Imports SQLDataAccess
Imports BusinessEntity
Imports System.Configuration
Imports System.Web.UI
Imports System.Data
Imports System.Data.SqlClient
Partial Class PresentationLayer_masterrecord_user
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            Me.addLink.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Add") 'add new
            Me.searchLink.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Search") 'search
            Me.ctryIDLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASUSER-0001")
            Me.StatffStatusLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASUSER-0002")
            Me.ctryIDLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASUSER-0003")
            Me.UserIDLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASUSER-0001")
            Me.RankLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASUSER-0007")
            Me.DepartmentLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASUSER-0011")
            'Me.UserNmaeLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASUSER-0004")
            Me.titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASUSER-0016")
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.addinfo.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-AR")
            Me.addinfo.Visible = False
            Me.Label1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-NR")
            Me.Label1.Visible = False
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'create the dropdownlist of staff status
            Dim cntryStat As New clsrlconfirminf()
            Dim statParam As ArrayList = New ArrayList
            statParam = cntryStat.searchconfirminf("STAFFSTAT")
            Dim count As Integer
            Dim statid As String
            Dim statnm As String
            For count = 0 To statParam.Count - 1
                statid = statParam.Item(count)
                statnm = statParam.Item(count + 1)
                count = count + 1
                If Not statid.Equals("DELETE") Then
                    Me.StatffStatusDDL.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                End If
            Next
            'deptment dropdownlist bond\
            Dim detpParam As ArrayList = New ArrayList()
            detpParam = cntryStat.searchconfirminf("DEPT")
            Dim deptid As String
            Dim deptnm As String

            Me.DepartmentDDL.Items.Add("")
            For count = 0 To detpParam.Count - 1
                deptid = detpParam.Item(count)
                deptnm = detpParam.Item(count + 1)
                count = count + 1
                Me.DepartmentDDL.Items.Add(New ListItem(deptnm.ToString(), deptid.ToString()))
            Next
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim userEntity As New clsUser()
            '//////////////////////////////////////////////////////////
            If (Trim(StatffStatusDDL.Text) <> "") Then
                userEntity.StaffStatus = StatffStatusDDL.SelectedValue
            End If
            If (Trim(Me.DepartmentDDL.Text) <> "") Then
                If Me.DepartmentDDL.SelectedValue = "AL" Then
                    userEntity.DepmentID = ""
                Else
                    userEntity.DepmentID = Me.DepartmentDDL.SelectedValue
                End If
            End If

            'If (Trim(Me.RankDDL.Text) <> "") Then
            '    userEntity.Rank = Me.RankDDL.SelectedValue
            'End If
            If (Trim(Me.UserIDBox.Text) <> "") Then
                userEntity.UserID = Trim(Me.UserIDBox.Text.ToString().ToUpper)
            End If
            '//////////////////////////////////////////////////
            Dim ctryIDPara As String = "%"
            Dim cmpIDPara As String = "%"
            Dim svcIDPara As String = "%"
            Dim login_rank As Integer = Session("login_rank")
            If login_rank <> 0 Then
                Select Case (login_rank)
                    Case 9
                        ctryIDPara = Session("login_ctryID").ToString.ToUpper
                        cmpIDPara = Session("login_cmpID").ToString.ToUpper
                        svcIDPara = Session("login_svcID").ToString.ToUpper
                    Case 8
                        ctryIDPara = Session("login_ctryID").ToString.ToUpper
                        cmpIDPara = Session("login_cmpID").ToString.ToUpper
                    Case 7
                        ctryIDPara = Session("login_ctryID").ToString.ToUpper

                End Select
            Else
            End If
            ''''''''''''''''''''''''''''''''''''''''''''''''''
            If (Trim(ctryIDDD.Text) <> "") Then
                ctryIDPara = ctryIDDD.SelectedValue
            End If
            userEntity.CountryID = ctryIDPara
            userEntity.CompID = cmpIDPara
            userEntity.SVCID = svcIDPara

            Dim usersall As DataSet = userEntity.GetUserSel()
            ''''username'''''''''''''
            userEntity.Modifyby = Session("userID").ToString.ToUpper
            ''''''''''''''''''''
            '''''''''''''''''''''''''

            userView.DataSource = usersall
            userView.DataBind()
            'page index changed
            checknorecord(usersall)
            userView.AllowPaging = True
            userView.AllowSorting = True
            '\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\           
            'bonds country  id  name


            Dim clscom As New clsCommonClass()
            Dim idnameds As New DataSet()

            Dim rank As Integer = Session("login_rank")
            clscom.rank = rank
            If rank <> 0 Then
                clscom.spctr = Session("login_ctryID")
                clscom.spstat = Session("login_cmpID")
                clscom.sparea = Session("login_svcID")
            End If
            idnameds = clscom.Getcomidname("BB_MASCTRY_IDNAME")
            
            databonds(idnameds, Me.ctryIDDD)
            Me.ctryIDDD.Items.Insert(0, "")

           

            'Rank ddl bond 

            Dim rankcls As ArrayList = New ArrayList
            rankcls = cntryStat.searchconfirminf("RANK")
            Dim rankid As String
            Dim rankname As String
            Me.RankDDL.Items.Clear()
            Dim startint As Integer = 0
            If login_rank <> 0 Then
                Select Case (login_rank)
                    Case 9
                        startint = 4
                    Case 8
                        startint = 2

                    Case 7
                        startint = 0
                End Select
            Else
            End If
            Me.RankDDL.Items.Add("")
            For count = startint To rankcls.Count - 1
                rankid = rankcls.Item(count)
                rankname = rankcls.Item(count + 1)
                count = count + 1
                Me.RankDDL.Items.Add(New ListItem(rankname.ToString(), rankid.ToString()))
            Next
            '\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

            Dim editcol As New CommandField
            editcol.EditText = objXmlTr.GetLabelName("EngLabelMsg", "BB-Edit") '"edit"
            editcol.HeaderImageUrl = "~/PresentationLayer/graph/edit.gif"
            editcol.ShowEditButton = True
            userView.Columns.Add(editcol)
            userView.DataBind()
            '''access control'''''''''''''''''''''''''''''''''''''''''''''''

            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "12")
            Me.addLink.Enabled = purviewArray(0)
            editcol.Visible = purviewArray(2)
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'add table head text
            If usersall.Tables(0).Rows.Count > 0 Then
                userView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-USER-0001")  'model id
                userView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-USER-0004")   'ctry id
                userView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-USER-0002")  'model name
                userView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-USER-0011") 'model alter name
                userView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-USER-0007") 'productclass
                userView.HeaderRow.Cells(6).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-USER-0012") 'effective date
                userView.HeaderRow.Cells(7).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-USER-0003")
                userView.HeaderRow.Cells(8).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-USER-0013") 'obsodate
                Dim i As Integer
                For i = 1 To 8
                    userView.HeaderRow.Cells(i).Font.Size = 8
                Next
            End If
        End If
    End Sub
#Region " command bond function "
    Public Function databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        Dim row As DataRow
        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            NewItem.Text = Trim(row(0).ToString()) & "-" & Trim(row(1).ToString())
            NewItem.Value = row(0)
            dropdown.Items.Add(NewItem)
        Next
    End Function

#End Region
#Region "search "
    Protected Sub searchLink_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles searchLink.Click
        Me.userView.PageIndex = 0
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        Dim userEntity As New clsUser()
        ''''username''       '''''''''''

        userEntity.Modifyby = Session("userID").ToString.ToUpper
        ''''''''''''''''''''

        If (Trim(StatffStatusDDL.Text) <> "") Then
            userEntity.StaffStatus = StatffStatusDDL.SelectedValue
        End If
        If (Trim(Me.DepartmentDDL.Text) <> "") Then
            If Me.DepartmentDDL.SelectedValue = "AL" Then
                userEntity.DepmentID = ""
            Else
                userEntity.DepmentID = Me.DepartmentDDL.SelectedValue
            End If
        End If

        If (Trim(Me.RankDDL.Text) <> "") Then
            userEntity.Rank = Me.RankDDL.SelectedValue
        End If
        If (Trim(Me.UserIDBox.Text) <> "") Then
            userEntity.UserID = Trim(Me.UserIDBox.Text.ToString().ToUpper)
        End If
        '//////////////////////////////////////////////////
        Dim ctryIDPara As String = "%"
        Dim cmpIDPara As String = "%"
        Dim svcIDPara As String = "%"
        Dim login_rank As Integer = Session("login_rank")
        If login_rank <> 0 Then
            Select Case (login_rank)
                Case 9
                    ctryIDPara = Session("login_ctryID").ToString.ToUpper
                    cmpIDPara = Session("login_cmpID").ToString.ToUpper
                    svcIDPara = Session("login_svcID").ToString.ToUpper
                Case 8
                    ctryIDPara = Session("login_ctryID").ToString.ToUpper
                    cmpIDPara = Session("login_cmpID").ToString.ToUpper
                Case 7
                    ctryIDPara = Session("login_ctryID").ToString.ToUpper

            End Select
        Else
        End If
        ''''''''''''''''''''''''''''''''''''''''''''''''''
        If (Trim(ctryIDDD.Text) <> "") Then
            ctryIDPara = ctryIDDD.SelectedValue
        End If
        userEntity.CountryID = ctryIDPara
        userEntity.CompID = cmpIDPara
        userEntity.SVCID = svcIDPara

        Dim selDS As DataSet = userEntity.GetUserSel()
        userView.DataSource = selDS
        userView.DataBind()
        'Session("userViewDS") = selDS
        checknorecord(selDS)
        '''''''''''''''''''''''''''''''''''''''''''
        userView.AllowPaging = True
        userView.AllowSorting = True
        If selDS.Tables(0).Rows.Count > 0 Then
            userView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-USER-0001")  'model id
            userView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-USER-0004")   'ctry id
            userView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-USER-0002")  'model name
            userView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-USER-0011") 'model alter name
            userView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-USER-0007") 'productclass
            userView.HeaderRow.Cells(6).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-USER-0012") 'effective date
            userView.HeaderRow.Cells(7).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-USER-0003")
            userView.HeaderRow.Cells(8).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-USER-0013") 'obsodate
            Dim i As Integer
            For i = 1 To 8
                userView.HeaderRow.Cells(i).Font.Size = 8
            Next
        End If
    End Sub
#End Region

#Region "��ʾ��Ϣ"
    Public Function checkinfo() As Integer
        If Me.UserIDBox.Text = "" Then
            Me.addinfo.Visible = True
        Else
            Me.addinfo.Visible = False
        End If
    End Function
#End Region

#Region "if have no record "
    Public Function checknorecord(ByVal ds As DataSet) As Integer
        If ds.HasErrors Then
            If ds.Tables(0).Rows.Count = 0 Then
                Me.Label1.Visible = True
                'Me.addinfo.Visible = False
            Else
                Me.Label1.Visible = False

            End If

        End If
        If ds.Tables(0).Rows.Count = 0 Then
            Me.Label1.Visible = True
            'Me.addinfo.Visible = False
        Else
            Me.Label1.Visible = False

        End If

    End Function

#End Region
    Protected Sub userView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles userView.PageIndexChanging
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        userView.PageIndex = e.NewPageIndex
        Dim userEntity As New clsUser()
        ''''username''       '''''''''''

        'userEntity.Modifyby = Session("userID").ToString.ToUpper
        ''''''''''''''''''''

        If (Trim(StatffStatusDDL.Text) <> "") Then
            userEntity.StaffStatus = StatffStatusDDL.SelectedValue
        End If
        If (Trim(Me.DepartmentDDL.Text) <> "") Then
            If Me.DepartmentDDL.SelectedValue = "AL" Then
                userEntity.DepmentID = ""
            Else
                userEntity.DepmentID = Me.DepartmentDDL.SelectedValue
            End If
        End If

        If (Trim(Me.RankDDL.Text) <> "") Then
            userEntity.Rank = Me.RankDDL.SelectedValue
        End If
        If (Trim(Me.UserIDBox.Text) <> "") Then
            userEntity.UserID = Trim(Me.UserIDBox.Text.ToString().ToUpper)
        End If
        '//////////////////////////////////////////////////
        Dim ctryIDPara As String = "%"
        Dim cmpIDPara As String = "%"
        Dim svcIDPara As String = "%"
        Dim login_rank As Integer = Session("login_rank")
        If login_rank <> 0 Then
            Select Case (login_rank)
                Case 9
                    ctryIDPara = Session("login_ctryID").ToString.ToUpper
                    cmpIDPara = Session("login_cmpID").ToString.ToUpper
                    svcIDPara = Session("login_svcID").ToString.ToUpper
                Case 8
                    ctryIDPara = Session("login_ctryID").ToString.ToUpper
                    cmpIDPara = Session("login_cmpID").ToString.ToUpper
                Case 7
                    ctryIDPara = Session("login_ctryID").ToString.ToUpper

            End Select
        Else
        End If
        ''''''''''''''''''''''''''''''''''''''''''''''''''
        If (Trim(ctryIDDD.Text) <> "") Then
            ctryIDPara = ctryIDDD.SelectedValue
        End If
        userEntity.CountryID = ctryIDPara
        userEntity.CompID = cmpIDPara
        userEntity.SVCID = svcIDPara
        Dim jobsall As DataSet = userEntity.GetUserSel()
        userView.DataSource = jobsall
        userView.DataBind()
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        If jobsall.Tables(0).Rows.Count > 0 Then
            userView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-USER-0001")  'model id
            userView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-USER-0004")   'ctry id
            userView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-USER-0002")  'model name
            userView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-USER-0011") 'model alter name
            userView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-USER-0007") 'productclass
            userView.HeaderRow.Cells(6).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-USER-0012") 'effective date
            userView.HeaderRow.Cells(7).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-USER-0003")
            userView.HeaderRow.Cells(8).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-USER-0013") 'obsodate
            Dim i As Integer
            For i = 1 To 8
                userView.HeaderRow.Cells(i).Font.Size = 8
            Next
        End If
    End Sub

    Protected Sub userView_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles userView.RowEditing

        Dim page As Integer = userView.PageIndex * userView.PageSize

        Dim ctryid As String = Me.userView.Rows(e.NewEditIndex).Cells(1).Text

        ctryid = Server.UrlEncode(ctryid)

        Dim stat As String = Me.userView.Rows(e.NewEditIndex).Cells(3).Text

        Dim i As Integer
        For i = 0 To Me.StatffStatusDDL.Items.Count - 1
            If stat = Me.StatffStatusDDL.Items(i).Text Then
                stat = Me.StatffStatusDDL.Items(i).Value
            End If
        Next
        stat = Server.UrlEncode(stat)
        Dim strTempURL As String = "ctryid=" + ctryid + "&STAFSTAT=" + stat
        strTempURL = "~/PresentationLayer/masterrecord/modifyuser.aspx?" + strTempURL
        Response.Redirect(strTempURL)
    End Sub
End Class


