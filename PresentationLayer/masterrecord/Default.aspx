﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="PresentationLayer_masterrecord_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table id="uagTab" border="0" style="height: 480px">
            <tr>
                <td style="width: 500px; height: 10px">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" style="width: 1%">
                                <img height="24" src="../graph/title1.gif" width="5" /></td>
                            <td background="../graph/title_bg.gif" class="style2" style="width: 98%">
                                <asp:Label ID="uagTitleLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="right" background="../graph/title_bg.gif" style="width: 1%">
                                <img height="24" src="../graph/title_2.gif" width="5" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 500px; height: 10px">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" class="style2" style="width: 98%; height: 15px">
                                <font color="red">
                                    <asp:Label ID="uagErrLab" runat="server" Text="Label" Visible="False"></asp:Label></font></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 500px; height: 280px">
                    <table id="userGroup" bgcolor="#b7e6e6" border="0" cellpadding="1" cellspacing="1"
                        style="width: 100%; height: 280px">
                        <tr bgcolor="#ffffff">
                            <td align="left" style="width: 42%; height: 1px">
                                <asp:TreeView ID="TreeView1" runat="server" Height="100%" ShowLines="True" Width="100%">
                                </asp:TreeView>
                            </td>
                            <td align="right" style="width: 16%; height: 1px">
                                <asp:LinkButton ID="LinkButton1" runat="server">LinkButton</asp:LinkButton>
                                &nbsp;<br />
                                <br />
                                <br />
                                <asp:LinkButton ID="LinkButton2" runat="server">LinkButton</asp:LinkButton>
                                &nbsp;<br />
                                <br />
                                <br />
                                <asp:LinkButton ID="LinkButton3" runat="server">LinkButton</asp:LinkButton>
                                &nbsp;<br />
                                <br />
                                <br />
                                <asp:LinkButton ID="LinkButton4" runat="server">LinkButton</asp:LinkButton>
                                &nbsp;
                            </td>
                            <td align="left" style="width: 45%; height: 1px">
                                <asp:TreeView ID="TreeView2" runat="server" Height="100%" ShowCheckBoxes="All" ShowLines="True"
                                    Width="100%">
                                </asp:TreeView>
                            </td>
                        </tr>
                    </table>
                    <asp:CheckBox ID="CheckBox1" runat="server" />
                    &nbsp;
                    <asp:CheckBox ID="CheckBox2" runat="server" />&nbsp; &nbsp;<asp:CheckBox ID="CheckBox3"
                        runat="server" />&nbsp; &nbsp;<asp:CheckBox ID="CheckBox4" runat="server" />&nbsp;
                    &nbsp;<asp:CheckBox ID="CheckBox5" runat="server" /></td>
            </tr>
            <tr>
                <td style="width: 500px; height: 20px">
                    <table id="Table1" border="0" cellpadding="1" cellspacing="1">
                        <tr>
                            <td align="center" style="width: 20%">
                                &nbsp;<asp:LinkButton ID="saveButton" runat="server"></asp:LinkButton></td>
                            <td align="center">
                                <asp:HyperLink ID="cancelLink" runat="server" NavigateUrl="~/PresentationLayer/masterrecord/UserAccessGroup.aspx">[cancelLink]</asp:HyperLink></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
