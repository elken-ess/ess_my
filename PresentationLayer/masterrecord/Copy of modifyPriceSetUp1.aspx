<%@ Page Language="VB" AutoEventWireup="false" EnableEventValidation="false"  CodeFile="modifyPriceSetUp1.aspx.vb" Inherits="PresentationLayer_masterrecord_modifyPriceSetUp1" %>

<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>add country message</title>
    <meta http-equiv="Content-Type" content="text/html; charset=gb2312">
	<LINK href="../css/style.css" type="text/css" rel="stylesheet">
</head>
<body  id="PriceSetUp1">
   <form id="PriceSetUp1form"  method="post" action=""  runat=server>
     <TABLE border="0" id=PriceSetUp1tab style="width: 100%">
           <tr>
           <td style="width: 100%" >
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>
							<TD width="1%" background="../graph/title_bg.gif"><IMG height="24" src="../graph/title1.gif" width="5"></TD>
							<TD class="style2" width="98%" background="../graph/title_bg.gif" style="width: 100%">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></TD>
							<TD align="right" width="1%" background="../graph/title_bg.gif"><IMG height="24" src="../graph/title_2.gif" width="5"></TD>
						</TR>
			 </TABLE>
           </tr>
           <tr>
           <td style="width: 100%"  >
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>					
							<TD class="style2" width="98%" background="../graph/title_bg.gif" >
                                <font color=red><asp:Label ID="errlab" runat="server" Text="Label" Visible=false></asp:Label></font></TD>
						</TR>
			 </TABLE>
           </tr>
			<TR>
				<TD style="width: 100%"   >
					<TABLE id="country" cellSpacing="1" cellPadding="2" bgColor="#b7e6e6" border="0" style="width: 100%" >
					
					<tr bgcolor="#ffffff">
<td colspan = 4>
                            <asp:Label ID="lblNoteUP" runat="server" ForeColor="Red" Text="Label" Visible="True"></asp:Label>
</td>
             </tr>
             <tr bgcolor="#ffffff">
<td colspan = 4>
                        	&nbsp;
                     	</td>
               </tr>

					
					 <TR bgColor="#ffffff">
											<TD  align=right style="width: 20%" >
                                                <asp:Label ID="priceidlab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 30%" >
                                                <asp:TextBox ID="priceidBox" runat="server" CssClass="textborder" Width="90%" MaxLength="10" ReadOnly="True"></asp:TextBox>
                                            </TD>
											<TD align=right style="width: 20%" >
                                                <asp:Label ID="partidlab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 30%" >
                                                <asp:TextBox ID="partidBox" runat="server" CssClass="textborder" ReadOnly="True" MaxLength="10" Width="90%"></asp:TextBox>
                     </TR>
										<TR bgColor="#ffffff">
											
                                            <TD  align=right style="width: 20%" >
                                                <asp:Label ID="countryidLab" runat="server">label</asp:Label></TD>
											<TD align=left width="85%" colspan=3 style="width: 80%">
                                                <asp:TextBox ID="countryidBox" runat="server" Width="62%" CssClass="textborder" ReadOnly="True" MaxLength="2"></asp:TextBox>
                                                
                                            </TD>
										</TR>
									  <TR bgColor="#ffffff">
											<TD  align=right style="width: 20%" >
                                                <asp:Label ID="amountlab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 30%" >
                                                <asp:TextBox ID="amountbox" runat="server" CssClass="textborder" Width="80%" MaxLength="13"></asp:TextBox>
                                                <asp:Label ID="Label2" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                <asp:RangeValidator ID="AMOUNTVA" runat="server" ControlToValidate="amountbox"
                                                    ForeColor="White" Type="Double" MaximumValue="9999999999.99" MinimumValue="0000000000.00">*</asp:RangeValidator>
                                                <asp:RequiredFieldValidator ID="va" runat="server" ControlToValidate="amountbox"
                                                    ForeColor="White">*</asp:RequiredFieldValidator></TD>
											<TD align=right style="width: 20%" >
                                                <asp:Label ID="statusLab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 30%" >
                                                <asp:DropDownList ID="statusDrop" runat="server" CssClass="textborder" Width="90%">
                                                </asp:DropDownList>
										</TR>
										<TR bgColor="#ffffff">
											
                                            <TD  align=right style="width: 20%; " >
                                                <asp:Label ID="effectivelab" runat="server"></asp:Label></TD>
											<TD align=left width="85%" colspan=3 style="width: 80%; ">
                                                <asp:TextBox ID="effectivebox" runat="server" Width="62%" CssClass="textborder" ReadOnly="True" Enabled="False"></asp:TextBox>
                                                <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                               
                                            </TD>
										</TR>
										
										
										<TR bgColor="#ffffff">
											<TD align=right style="width: 20%" >
                                                <asp:Label ID="createby" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 30%" >
                                                <asp:TextBox ID="creatbybox" runat="server" CssClass="textborder" ReadOnly=true Width="90%"></asp:TextBox></TD>
											<TD align=right style="width: 20%" >
                                                <asp:Label ID="creatdate" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 30%" >
                                                <asp:TextBox ID="creatdtbox" runat="server" CssClass="textborder" ReadOnly=true Width="90%"></asp:TextBox></TD>
										</TR>
										<TR bgColor="#ffffff">
											<TD align=right style="width: 20%" >
                                                <asp:Label ID="modifyby" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 30%" >
                                                <asp:TextBox ID="modfybybox" runat="server" CssClass="textborder" ReadOnly=true Width="90%"></asp:TextBox></TD>
											<TD align=right style="width: 20%" >
                                                <asp:Label ID="mdifydate" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 30%" >
                                                <asp:TextBox ID="mdfydtbox" runat="server" CssClass="textborder" ReadOnly=true Width="90%"></asp:TextBox></TD>
										</TR>
										             <tr bgcolor="#ffffff">
<td colspan = 4>
                        	&nbsp;
                     	</td>
               </tr>
										<tr bgcolor="#ffffff">
<td colspan = 4>
                            <asp:Label ID="lblNoteDown" runat="server" ForeColor="Red" Text="Label"></asp:Label>
</td>
             </tr>


								</TABLE>
                    <asp:TextBox ID="pricesetupidbox" runat="server" Visible="False"></asp:TextBox></TD>
					</TR>
					<TR>
						<TD style="width: 616px"  >
							<TABLE id="Table1" cellSpacing="1" cellPadding="1"  border="0">
									<TR>
										<TD style="WIDTH: 20%" align=center>
                                            &nbsp;<asp:LinkButton ID="saveButton" runat="server"></asp:LinkButton></TD>
										<td align=center >
                                            <asp:LinkButton ID="cancelLink" runat="server">LinkButton</asp:LinkButton></td>
									</TR>
							</TABLE>
						</TD>
					</TR>
			</TABLE>
       <asp:ValidationSummary ID="ERRORMESSAGE" runat="server" ShowMessageBox="True"
           ShowSummary="False" />
       <cc1:MessageBox ID="MessageBox1" runat="server" />
    </form>
</body>
</html>
