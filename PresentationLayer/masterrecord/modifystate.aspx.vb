Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Partial Class PresentationLayer_masterrecord_modifystate
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try


            '''''''''''''''''''''''''''''''''''''''''''''''''''''
            Try
                Dim str As String = Request.Params("staid").ToString()
                str = Request.Params("ctrid").ToString()
                str = Request.Params("stastat").ToString()
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            
            Dim mdfystaid As String = Request.Params("staid").ToString()
            Dim mdfyctrid As String = Request.Params("ctrid").ToString()
            Dim mdfystat As String = Request.Params("stastat").ToString()

            

            Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle


            'create  ctryid the dropdownlist

            'Dim ctry As New clsCommonClass
            'Dim ctryds As New DataSet

            'ctryds = ctry.Getidname("BB_MASCTRY_IDNAME")
            'databonds(ctryds, Me.ctrid)




            'display label message
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            ctryidlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCTRY-0001")
            Me.stateidlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTATE-0001")
            Me.statenmlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTATE-0002")
            statusLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCTRY-0004")
            Me.alterlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCTRY-0003")
            Me.taxidLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTATE-0003")

            creatby.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATBY")
            creatdate.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
            modfyby.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFBY")
            mdfydate.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFDATE")

            saveButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Save")
            cancelLink.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Cancel")
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "MODSTATL")
            lblNoteUP.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            lblNoteDown.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")



            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            Me.stanmerr.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "STANMER")
            'Me.stalterr.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "STAALTER")
            'Dim strPanm As String = objXmlTr.GetLabelID("StatusMessage", mdfystat)
            Dim strPanm As String = mdfystat

            If mdfystaid.Trim() <> "" Then
                Dim stateEntity As New clsState()
                stateEntity.StateID = mdfystaid
                stateEntity.CountryID = mdfyctrid
                stateEntity.StateStatus = strPanm

                Dim edtReader As SqlDataReader = stateEntity.GetStateDetailByStaID_CtrID()
                If edtReader.Read() Then

                    Me.stateidbox.Text = edtReader.GetValue(0).ToString()
                    Me.stateidbox.ReadOnly = True
                    Me.statenmbox.Text = edtReader.GetValue(1).ToString()
                    Me.statealtbox.Text = edtReader.GetValue(2).ToString()
                    Me.ctridbox.Text = edtReader.GetValue(3).ToString()

                    Me.ctridbox.Enabled = False
                    'create  taxid the dropdownlist
                    Dim tax As New clsCommonClass
                    Dim taxds As New DataSet
                    tax.spctr = Me.ctridbox.Text.ToString().ToUpper()
                    taxds = tax.Gettaxidname("BB_MASSTAX_IDNMPERC")
                    If taxds.Tables(0).Rows.Count <> 0 Then
                        databonds_tax(taxds, Me.taxid)

                    End If

                    'Me.ctry_id.Text = edtReader.GetValue(3).ToString()
                    'Me.ctry_id.Enabled = False
                    'Me.ctry_id.ReadOnly = True
                    'Me.taxid.Text
                    'create the dropdownlist
                    Me.taxid.Text = edtReader.GetValue(4).ToString()


                    'create the dropdownlist
                    Dim cntryStat As New clsrlconfirminf()
                    Dim statParam As ArrayList = New ArrayList
                    statParam = cntryStat.searchconfirminf("STATUS")
                    Dim count As Integer
                    Dim statid As String
                    Dim statnm As String
                    For count = 0 To statParam.Count - 1
                        statid = statParam.Item(count)
                        statnm = statParam.Item(count + 1)
                        If (statid.Equals("ACTIVE") Or statid.Equals("DELETE") Or statid.Equals("OBSOLETE")) Then
                            ctrystat.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                        End If
                        If statid.Equals(edtReader.GetValue(5).ToString()) Then
                            ctrystat.Items(count / 2).Selected = True
                        End If
                        count = count + 1
                    Next


                    Dim accessgroup As String = Session("accessgroup").ToString
                    Dim purviewArray As Array = New Boolean() {False, False, False, False}
                    purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "09")
                    If purviewArray(2) = False Then
                        Response.Redirect("~/PresentationLayer/logon.aspx")
                    End If
                    Me.saveButton.Enabled = purviewArray(1)
                    If Convert.ToString(purviewArray(4)).ToUpper = "FALSE" Then
                        Me.ctrystat.Items.Remove(Me.ctrystat.Items.FindByValue("DELETE"))
                    End If


                    'creatbybox.Text = edtReader.GetValue(6).ToString().ToUpper()
                    'creatbybox.ReadOnly = True
                    Dim Rcreatby As String = edtReader.GetValue(6).ToString().ToUpper()
                    If Rcreatby <> "ADMIN" Then
                        Dim Rcreat As New clsCommonClass
                        Rcreat.userid() = Rcreatby
                        Dim Rcreatds As New DataSet()
                        Rcreatds = Rcreat.Getuseridname("BB_MASUSER_SelByIDName")

                        Me.creatbybox.Text = Rcreatds.Tables(0).Rows(0).Item(0)
                    Else
                        Me.creatbybox.Text = "ADMIN-ADMIN"
                    End If
                    creatbybox.ReadOnly = True

                    creatdtbox.Text = edtReader.GetValue(7).ToString()
                    creatdtbox.ReadOnly = True
                    'modfybybox.Text = Session("username").ToString().ToUpper()
                    Dim userIDNamestr As String = Session("userID").ToString().ToUpper + "-" + Session("username").ToString.ToUpper
                    modfybybox.Text = userIDNamestr
                    modfybybox.ReadOnly = True
                    mdfydtbox.Text = edtReader.GetValue(9).ToString()
                    mdfydtbox.ReadOnly = True



                    'modfybybox.Text = "wang"
                    'modfybybox.ReadOnly = True
                    'mdfydtbox.Text = Date.Now()
                    'mdfydtbox.ReadOnly = True
                End If

            End If

        End If
    End Sub
#Region " id bond"
    Public Function databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        dropdown.Items.Add("")
        Dim row As DataRow
        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            NewItem.Text = row("id") & "-" & row("name")
            NewItem.Value = row("id")
            dropdown.Items.Add(NewItem)
        Next

    End Function
#End Region

#Region " taxid bond"
    Public Function databonds_tax(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        dropdown.Items.Add("")
        Dim row As DataRow
        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            NewItem.Text = row("id") & "-" & row("name") & "-" & row("perc")
            NewItem.Value = row("id")
            dropdown.Items.Add(NewItem)
        Next

    End Function
#End Region

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveButton.Click

        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim msginfo As String = objXm.GetLabelName("StatusMessage", "SAVEORNOT")
        Dim msgtitle As String = objXm.GetLabelName("StatusMessage", "SAVETITLE")
        MessageBox1.Confirm(msginfo)


       
    End Sub

    Protected Sub MessageBox1_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox1.GetMessageBoxResponse
        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
            Dim stateEntity As New clsState()
            'If (Me.ctry_id.SelectedItem.Value.ToString() <> "") Then
            '    stateEntity.CountryID = ctry_id.SelectedItem.Value.ToString().ToUpper()
            'End If

            If (Trim(Me.ctridbox.Text) <> "") Then
                stateEntity.CountryID = Me.ctridbox.Text.ToUpper()
            End If
 

            If (Trim(Me.stateidbox.Text) <> "") Then
                stateEntity.StateID = stateidbox.Text.ToUpper()
            End If
            If (Trim(Me.statenmbox.Text) <> "") Then
                stateEntity.StateName = statenmbox.Text.ToUpper()
            End If
            If (ctrystat.SelectedItem.Value.ToString() <> "") Then
                stateEntity.StateStatus = ctrystat.SelectedItem.Value.ToString()
            End If
            If (Trim(Me.statealtbox.Text) <> "") Then
                stateEntity.StateAlternateName = statealtbox.Text.ToUpper()
            End If

            If (Me.taxid.Text <> "") Then
                'If (Me.taxid.SelectedItem.Value.ToString() <> "") Then
                stateEntity.TaxID = taxid.SelectedItem.Value.ToString()
            End If

            'If (Trim(creatbybox.Text) <> "") Then
            '    stateEntity.CreateBy = creatbybox.Text.ToUpper()
            'End If
            Dim cntryDate As New clsCommonClass()
            If (Trim(creatdtbox.Text) <> "") Then
                stateEntity.CreateDate = cntryDate.DatetoDatabase(creatdtbox.Text)
            End If
            If (Trim(modfybybox.Text) <> "") Then
                stateEntity.ModifyBy = Session("userID").ToString().ToUpper
            End If
            If (Trim(mdfydtbox.Text) <> "") Then
                stateEntity.ModifyDate = cntryDate.DatetoDatabase(mdfydtbox.Text)
            End If

            stateEntity.IPaddr = Request.UserHostAddress.ToString()
            stateEntity.servid = Session("login_svcID")

            '''''''''delete'''''''''''''''''''''''''
            If stateEntity.StateStatus = "DELETE" Then
                Dim delstateid As Integer = stateEntity.GetareaByStateID()
                If delstateid <> 0 Then
                    Dim objXm As New clsXml
                    objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    errlab.Text = objXm.GetLabelName("StatusMessage", "DELSTAT")
                    errlab.Visible = True
                    Return
                End If
            End If
            '''''''''delete'''''''''''''''''''''''''

            Dim selvalue As Integer = stateEntity.GetCountryDetailsByStateNM()
            If selvalue <> 0 Then
                Dim objXm As New clsXml
                objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                errlab.Text = objXm.GetLabelName("StatusMessage", "DUPSTATNM")
                errlab.Visible = True
            ElseIf selvalue.Equals(0) Then
                Dim updCtryCnt As Integer = stateEntity.Update()
                Dim objXmlTr As New clsXml
                objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                If updCtryCnt = 0 Then
                    Dim objXm As New clsXml
                    objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    errlab.Text = objXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Suc")
                    errlab.Visible = True

                    'Response.Redirect("state.aspx")
                Else
                    'errlab.Text = objXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Fail")
                    'errlab.Visible = True
                    Response.Redirect("~/PresentationLayer/Error.aspx")
                End If
            End If
        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
        End If
    End Sub
End Class
