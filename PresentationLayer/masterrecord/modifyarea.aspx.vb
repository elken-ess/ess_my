﻿Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Partial Class PresentationLayer_masterrecord_modifyarea
    Inherits System.Web.UI.Page
    Dim flag As Boolean = False


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then

            Try
                If (Session("username").ToString() = Nothing Or Session("username").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try

            Dim mdfyareid As String = Request.Params("areid").ToString()
            Dim mdfystaid As String = Request.Params("staid").ToString()
            Dim mdfyctrid As String = Request.Params("ctrid").ToString()
            Dim mdfystat As String = Request.Params("arestat").ToString()

            Me.poBOXm.ReadOnly = True

            'create  ctryid the dropdownlist
            Dim ctry As New clsCommonClass
            Dim ctryds As New DataSet

            ctryds = ctry.Getidname("BB_MASCTRY_IDNAME")
            databonds(ctryds, Me.ctryid)

            Dim stat As New clsCommonClass
            Dim statds As New DataSet

            statds = stat.Getidname("BB_MASSTA_IDNAME")
            databonds(statds, Me.staid)

            Dim serv As New clsCommonClass
            Dim servds As New DataSet

            servds = serv.Getidname("BB_MASSVRC_IDNAME")
            databonds(servds, Me.serviceid)
            'display label message
            Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            ctryidlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCTRY-0001")
            Me.staidLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTATE-0001")
            Me.areaidLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASAREA-0001")
            Me.areanmlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASAREA-0002")
            Me.alterLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCTRY-0003")
            Me.serviceidlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASAREA-0005")
            Me.POlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASAREA-0004")
            statusLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCTRY-0004")
            Me.polabm.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASAREA-0006")
            creatby.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATBY")
            creatdate.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
            modfyby.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFBY")
            mdfydate.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFDATE")

            saveButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Save")
            cancelLink.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Cancel")
            addButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Add")
            Me.modiButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Modify")
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "MODCOMPTL")

            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            'ctryiderr.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "CRYIDER")
            'ctrynmerr.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "CRYNMER")
            'ctryalterr.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "CRYALTER")
            Dim strPanm As String = objXmlTr.GetLabelID("StatusMessage", mdfystat)
            If mdfystaid.Trim() <> "" Then
                Dim areaEntity As New clsarea()
                areaEntity.AreaID = mdfyareid
                areaEntity.StateID = mdfystaid
                areaEntity.CountryID = mdfyctrid
                areaEntity.AreaStatus = strPanm

                Dim edtReader As SqlDataReader = areaEntity.GetAreaDetailByID()
                If edtReader.Read() Then
                    Me.areaidbox.Text = edtReader.GetValue(0).ToString()
                    Me.areaidbox.ReadOnly = True
                    Me.areanm.Text = edtReader.GetValue(1).ToString()
                    Me.alter.Text = edtReader.GetValue(2).ToString()

                    Me.ctryid.Text = edtReader.GetValue(3).ToString()
                    Me.ctryid.Enabled = False
                    Me.staid.Text = edtReader.GetValue(4).ToString()
                    Me.staid.Enabled = False
                    'create the dropdownlist
                    Dim cntryStat As New clsrlconfirminf()
                    Dim statParam As ArrayList = New ArrayList
                    statParam = cntryStat.searchconfirminf("STATUS")
                    Dim count As Integer
                    Dim statid As String
                    Dim statnm As String
                    For count = 0 To statParam.Count - 1
                        statid = statParam.Item(count)
                        statnm = statParam.Item(count + 1)
                        ctrystat.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                        Me.POstat.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                        Me.postatm.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                        If statid.Equals(edtReader.GetValue(5).ToString()) Then
                            ctrystat.Items(count / 2).Selected = True
                        End If
                        count = count + 1
                    Next

                    Me.serviceid.Text = edtReader.GetValue(6).ToString()

                    creatbybox.Text = edtReader.GetValue(7).ToString().ToUpper()
                    creatbybox.ReadOnly = True
                    creatdtbox.Text = edtReader.GetValue(8).ToString()
                    creatdtbox.ReadOnly = True
                    modfybybox.Text = Session("username").ToString().ToUpper()
                    modfybybox.ReadOnly = True
                    mdfydtbox.Text = edtReader.GetValue(10).ToString()
                    mdfydtbox.ReadOnly = True
                    'pocode table                                                                                                                                                                                            
                    Dim poall As DataSet = areaEntity.GetPO()
                    'Dim cl3 As New DataColumn("No", GetType(String))
                    'poall.Tables(0).Columns.Add(cl3)
                    'Dim i As Integer
                    'For i = 0 To poall.Tables(0).Rows.Count - 1
                    '    poall.Tables(0).Rows(i).Item(0) = i + 1
                    'Next
                    POView.DataSource = poall
                    objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
                    POView.DataBind()

                    If poall.Tables(0).Rows.Count >= 1 Then
                        POView.HeaderRow.Cells(0).Text = objXmlTr.GetLabelName("EngLabelMsg", "POHD0")
                        POView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "POHD1")
                        'POView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "POHD2")
                    End If


                End If

                Dim stall As DataSet = areaEntity.GetPO()
                Session("ds") = stall

            End If
        End If
    End Sub
#Region " id bond"
    Public Function databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        Dim row As DataRow
        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            NewItem.Text = row("id") & "-" & row("name")
            NewItem.Value = row("id")
            dropdown.Items.Add(NewItem)
        Next
        dropdown.Items(0).Selected = True
    End Function
#End Region
Protected Sub saveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveButton.Click

        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim msginfo As String = objXm.GetLabelName("StatusMessage", "SAVEORNOT")
        Dim msgtitle As String = objXm.GetLabelName("StatusMessage", "SAVETITLE")
        MessageBox1.Confirm(msginfo)


       
    End Sub

   
    Protected Sub POView_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles POView.SelectedIndexChanged
        Me.poBOXm.Text = POView.SelectedRow.Cells(1).Text()
        Me.poBOXm.ReadOnly = True
        Dim postat As String = POView.SelectedRow.Cells(2).Text()
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim strPanm As String = objXmlTr.GetLabelID("StatusMessage", postat)
        Me.postatm.Text = strPanm
    End Sub

    Protected Sub addButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles addButton.Click

        Dim areaEntity As New clsarea()
        areaEntity.AreaID = Me.areaidbox.Text
        Dim stall As DataSet = Session("ds")
        Dim dr As DataRow
        dr = stall.Tables(0).NewRow()
        'dr(0) = stall.Tables(0).Rows.Count + 1
        dr(0) = Me.PObox.Text

        'dr("PO") = Me.PObox.Text
        Dim statXmlTr As New clsXml
        Dim strPanm As String
        statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim statusid As String = Me.POstat.SelectedItem.Value()
        strPanm = statXmlTr.GetLabelName("StatusMessage", statusid)
        dr(1) = strPanm
        stall.Tables(0).Rows.Add(dr)
        Session("ds") = stall
        POView.DataSource = Session("ds")
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        POView.DataBind()

        If stall.Tables(0).Rows.Count >= 1 Then
            POView.HeaderRow.Cells(0).Text = objXmlTr.GetLabelName("EngLabelMsg", "POHD0")
            POView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "POHD1")
            POView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "POHD2")

        End If


    End Sub

    Protected Sub modiButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles modiButton.Click
        Dim statXmlTr As New clsXml
        Dim strPanm As String
        statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim statusid As String = Me.postatm.SelectedItem.Value()
        strPanm = statXmlTr.GetLabelName("StatusMessage", statusid)
        POView.SelectedRow.Cells(2).Text = strPanm
        Dim areaEntity As New clsarea()
        areaEntity.AreaID = Me.areaidbox.Text
        areaEntity.POStatus = Me.postatm.SelectedItem.Value()
        areaEntity.POID = POView.SelectedRow.Cells(1).Text()
        areaEntity.ModifyBy = creatbybox.Text.ToUpper()
        Dim updCtryCnt As Integer = areaEntity.Update_po()
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        If updCtryCnt <> 0 Then

            Response.Redirect("~/PresentationLayer/Error.aspx")

        End If


    End Sub

    Protected Sub MessageBox1_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox1.GetMessageBoxResponse
        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
            Dim areaEntity As New clsarea()
            'If (Me.ctry_id.SelectedItem.Value.ToString() <> "") Then
            '    stateEntity.CountryID = ctry_id.SelectedItem.Value.ToString().ToUpper()
            'End If

            areaEntity.CountryID = Me.ctryid.SelectedItem.Value.ToString()
            areaEntity.StateID = Me.staid.SelectedItem.Value.ToString()
            areaEntity.AreaID = Me.areaidbox.Text.ToUpper()

            If (Trim(Me.areanm.Text) <> "") Then
                areaEntity.AreaName = areanm.Text.ToUpper()
            End If
            If (Trim(Me.alter.Text) <> "") Then
                areaEntity.AreaAlternateName = alter.Text.ToUpper()
            End If
            If (ctrystat.SelectedItem.Value.ToString() <> "") Then
                areaEntity.AreaStatus = ctrystat.SelectedItem.Value.ToString()
            End If


            If (Me.serviceid.Text <> "") Then
                'If (Me.taxid.SelectedItem.Value.ToString() <> "") Then
                areaEntity.ServiceID = serviceid.SelectedItem.Value.ToString()
            End If

            If (Trim(creatbybox.Text) <> "") Then
                areaEntity.CreateBy = creatbybox.Text.ToUpper()
            End If
            Dim cntryDate As New clsCommonClass()
            If (Trim(creatdtbox.Text) <> "") Then
                areaEntity.CreateDate = cntryDate.DatetoDatabase(creatdtbox.Text)
            End If
            If (Trim(modfybybox.Text) <> "") Then
                areaEntity.ModifyBy = modfybybox.Text.ToUpper()
            End If
            If (Trim(mdfydtbox.Text) <> "") Then
                areaEntity.ModifyDate = cntryDate.DatetoDatabase(mdfydtbox.Text)
            End If

            Dim selvalue As Integer = areaEntity.GetAreaDetailsByAreaNM()
            If selvalue <> 0 Then
                Dim objXm As New clsXml
                objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                errlab.Text = objXm.GetLabelName("StatusMessage", "DUPSTATNM")
                errlab.Visible = True
            ElseIf selvalue.Equals(0) Then
                Dim updCtryCnt As Integer = areaEntity.Update()
                Dim objXmlTr As New clsXml
                objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                If updCtryCnt = 0 Then
                    Dim objXm As New clsXml
                    objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    errlab.Text = objXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Suc")
                    errlab.Visible = True

                    'Response.Redirect("state.aspx")
                Else
                    'errlab.Text = objXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Fail")
                    'errlab.Visible = True
                    Response.Redirect("~/PresentationLayer/Error.aspx")
                End If
            End If
        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
        End If
    End Sub
End Class
