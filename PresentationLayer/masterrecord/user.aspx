<%@ Page Language="VB" AutoEventWireup="false" CodeFile="user.aspx.vb" Inherits="PresentationLayer_masterrecord_user" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
 <title>User - Search</title>
     <STYLE type="text/css">A:link { COLOR: #336666; TEXT-DECORATION: none }
	BODY { FONT-SIZE: 9pt; FONT-FAMILY: "Arial", "Verdana", "Tahoma"; BACKGROUND-COLOR: #ffffff }
	TD { FONT-SIZE: 9pt; FONT-FAMILY: Arial,Verdana,Tahoma }
	</STYLE>
	<LINK href="../css/style.css" type="text/css" rel="stylesheet">
</head>
<body>
    <form id="form1" runat="server">
     <TABLE border="0" id=TABLE2 width ="100%">
           <tr>
           <td>
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>
							<TD width="1%" background="../graph/title_bg.gif"><IMG height="24" src="../graph/title1.gif" width="5"></TD>
							<TD class="style2" width="98%" background="../graph/title_bg.gif">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></TD>
							<TD align="right" width="1%" background="../graph/title_bg.gif"><IMG height="24" src="../graph/title_2.gif" width="5"></TD>
						</TR>
			 </TABLE>
           </tr>
           <tr>
           <td style="width: 100%; height: 2px">
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>					
							<TD class="style2" width="98%" background="../graph/title_bg.gif" style="height: 1px">
                                <font color=red><asp:Label ID="addinfo" runat="server" Text="Label" Visible=false></asp:Label></font></TD>
						</TR>
			 </TABLE>
           </tr>			
				</table> 
    <div>
        &nbsp; &nbsp;<asp:Label ID="UserIDLab" runat="server" Text="Label" style="width: 12%"></asp:Label>&nbsp;&nbsp;<asp:TextBox ID="UserIDBox" runat="server" Width="15%" CssClass="textborder" MaxLength="20"></asp:TextBox>&nbsp;&nbsp;
        &nbsp;&nbsp; &nbsp;<asp:Label ID="StatffStatusLab" runat="server" Text="Label"></asp:Label>
        &nbsp;<asp:DropDownList ID="StatffStatusDDL" runat="server"  CssClass="textborder" Width="22%">
        </asp:DropDownList>&nbsp;&nbsp;
        <asp:Label ID="DepartmentLab" runat="server" Text="Label" style="width: 12%"></asp:Label>&nbsp;
        <asp:DropDownList ID="DepartmentDDL" runat="server" Width="23%" CssClass="textborder">
        </asp:DropDownList><br />
        &nbsp; &nbsp;&nbsp;<asp:Label ID="RankLab" runat="server" Text="Label" Width="46px"></asp:Label>&nbsp;&nbsp;<asp:DropDownList
            ID="RankDDL" runat="server" Width="18%" CssClass="textborder">
        </asp:DropDownList>&nbsp;&nbsp;<asp:Label ID="ctryIDLab" runat="server" Text="Label"></asp:Label>
        &nbsp;
        <asp:DropDownList
            ID="ctryIDDD" runat="server" Width="19%" CssClass="textborder">
        </asp:DropDownList>
        &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; 
        <asp:LinkButton ID="searchLink" runat="server">Search</asp:LinkButton>
        &nbsp; &nbsp; 
        <asp:LinkButton ID="addLink" runat="server" PostBackUrl="~/PresentationLayer/masterrecord/adduser.aspx">Label</asp:LinkButton><br />
        <hr />
    
    </div>
        <asp:GridView ID="userView" runat="server" Width="100%" AllowPaging="True" Font-Size="Smaller">
        </asp:GridView>
        <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="Label" Visible="False"></asp:Label>
    </form>
</body>
</html>