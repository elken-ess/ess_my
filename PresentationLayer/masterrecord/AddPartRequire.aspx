<%@ Page Language="VB" AutoEventWireup="false" EnableEventValidation ="false"  CodeFile="AddPartRequire.aspx.vb" Inherits="PresentationLayer_masterrecord_AddPartRequire" %>

<%@ Register Assembly="JCalendar" Namespace="JCalendar" TagPrefix="cc2" %>

<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Add Part</title>
   <meta http-equiv="Content-Type" content="text/html; charset=gb2312">
	<LINK href="../css/style.css" type="text/css" rel="stylesheet">
    <script language="JavaScript" src="../js/common.js"></script> 
</head>
<body>
    <form id="AddPartRequire" runat="server">
   <TABLE border="0" id=TABLE1 width="100%">
           <tr>
           <td style="height: 39px; width: 100%;">
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>
							<TD width="1%" background="../graph/title_bg.gif"><IMG height="24" src="../graph/title1.gif" width="5"></TD>
							<TD class="style2" width="98%" background="../graph/title_bg.gif">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></TD>
							<TD align="right" width="1%" background="../graph/title_bg.gif"><IMG height="24" src="../graph/title_2.gif" width="5"></TD>
						</TR>
			 </TABLE>
           </tr>
           <tr>
           <td style="width: 100%">
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>					
							<TD class="style2" width="98%" background="../graph/title_bg.gif">
                                <font color=red><asp:Label ID="errlab" runat="server" Text="Label" Visible=false></asp:Label></font>
                                </TD>
						</TR>
			 </TABLE>
           </tr>
			<TR>
				<TD style="width:100%">
       
          <table id="AddPartRequiretab" border="0" style="width: 100%">
                <tr>
                <td style="width: 100%" >
                    <table id="PartRequire" bgcolor="#b7e6e6" border="0" cellpadding="2" cellspacing="1" width="100%">
                      <tr bgcolor="#ffffff">
<td colspan = 4>
                            <asp:Label ID="lblCompTop" runat="server" ForeColor="Red" Text="Label" Visible="True"></asp:Label>
</td>
             </tr>
             <tr bgcolor="#ffffff">
<td colspan = 4>
                        	&nbsp;
                     	</td>
               </tr> 
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 10%; height: 34px">
                                &nbsp;<asp:Label ID="countryidlab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 40%; height: 34px;">
                                <asp:DropDownList ID="countryiddrpd" runat="server" CssClass="textborder" Width="168px" style="width: 78%">
                                </asp:DropDownList>
                                <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                <asp:RequiredFieldValidator ID="countryiderr" runat="server" ControlToValidate="countryiddrpd" Width="16px" ForeColor="White">*</asp:RequiredFieldValidator>
                                &nbsp;&nbsp;&nbsp;
                            </td>
                            <td align="right" style="width: 10%; height: 34px">
                                <asp:Label ID="CategoryIDLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 40%; height: 30px;">
                                <asp:DropDownList ID="CategoryIDdrpd" runat="server" CssClass="textborder" Width="248px" style="width: 78%">
                                </asp:DropDownList>
                                <asp:Label ID="Label4" runat="server" Text="*" ForeColor="Red"></asp:Label>
                                <asp:RequiredFieldValidator ID="partcateerr" runat="server" ControlToValidate="CategoryIDdrpd" ForeColor="White">*</asp:RequiredFieldValidator>
                                &nbsp;
                                &nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 10%; height: 30px;">
                                <asp:Label ID="partidlab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" colspan="3" width="85%" style="height: 30px; width: 90%;">
                                <asp:TextBox ID="partidtbox" runat="server" CssClass="textborder" Width="136px" MaxLength="50" AutoPostBack="False" style="width: 32%"></asp:TextBox>
                                <asp:Label ID="Label2" runat="server" ForeColor="Red" Text="*" Width="1px"></asp:Label>
                                <asp:RequiredFieldValidator ID="partiderr" runat="server" ControlToValidate="partidtbox" ForeColor="White">*</asp:RequiredFieldValidator>
                                &nbsp;&nbsp; &nbsp;
                            </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="height: 30px; width: 10%;">
                                <asp:Label ID="partnameLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" colspan="3" style="height: 30px; width: 90%;">
                                <asp:TextBox ID="partnametbox" runat="server" CssClass="textborder" Width="120px" style="width: 87%" MaxLength="50"></asp:TextBox>&nbsp;<asp:Label
                                    ID="Label3" runat="server" ForeColor="Red" Text="*"></asp:Label><asp:RequiredFieldValidator
                                        ID="partnameerr" runat="server" ControlToValidate="partnametbox" ForeColor="White">*</asp:RequiredFieldValidator></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="height: 30px; width: 10%;">
                                <asp:Label ID="partanamelab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" colspan="3" style="height: 30px; width: 80%;">
                                <asp:TextBox ID="partanametbox" runat="server" CssClass="textborder" MaxLength="50"
                                    Style="width: 87%" Width="144px"></asp:TextBox>
                                &nbsp;
                            </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 10%; height: 30px;">
                                <asp:Label ID="FiniProdulab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 40%; height: 30px;"><asp:DropDownList ID="FiniProdudrpd" runat="server" CssClass="textborder" Width="128px" style="width: 78%">
                            </asp:DropDownList></td>
                            <td align="right" style="width: 10%; height: 30px;">
                                <asp:Label ID="ModelIDlab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 40%; height: 30px;"><asp:DropDownList ID="ModelIDdrpd" runat="server" CssClass="textborder" Width="248px" style="width: 78%">
                            </asp:DropDownList>
                                <asp:Label ID="Label7" runat="server" ForeColor="Red" Text=""></asp:Label>
                                <asp:RequiredFieldValidator ID="modelerr" runat="server" ControlToValidate="ModelIDdrpd"  Enabled =false  ForeColor="White">*</asp:RequiredFieldValidator></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 10%; height: 30px" id="#cal">
                                <asp:Label ID="uomlab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 40%; height: 30px;"><asp:DropDownList ID="uomdrpd" runat="server" CssClass="textborder" Width="128px" style="width: 78%">
                            </asp:DropDownList></td>
                            <td align="right" style="width: 10%; height: 30px">
                                <asp:Label ID="TradingItemlab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 40%; height: 30px;"><asp:DropDownList ID="TradingItemdrpd" runat="server" CssClass="textborder" Width="128px" style="width: 78%">
                            </asp:DropDownList></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 10%; height: 30px">
                                <asp:Label ID="EffectDatelab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 40%; height: 30px">
                                <asp:TextBox ID="EffectDatetbox" runat="server" CssClass="textborder" Style="position: static; width: 78%;" Width="136px" ReadOnly="True" CausesValidation="True" AutoPostBack="True"></asp:TextBox>&nbsp;<asp:HyperLink
                                    ID="HypCal" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                                    ToolTip="Choose a Date">Choose a Date</asp:HyperLink>
                                <cc2:JCalendar ID="JCalendar1" runat="server" ControlToAssign="EffectDatetbox" ImgURL="~/PresentationLayer/graph/calendar.gif" Visible="False" />
                                <asp:Label ID="Label5" runat="server" ForeColor="Red" Text="*"></asp:Label><asp:RequiredFieldValidator ID="efferr" runat="server" ControlToValidate="EffectDatetbox" Width="8px" ForeColor="White">*</asp:RequiredFieldValidator>&nbsp;&nbsp;
                            </td>
                            <td align="right" style="width: 10%; height: 30px">
                                <asp:Label ID="ObsolDatelab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 40%; height: 30px">
                                <asp:TextBox ID="ObsolDatetbox" runat="server" CssClass="textborder" MaxLength="2"
                                    Width="75%" ReadOnly="True" CausesValidation="True" style="width: 68%" AutoPostBack="True"></asp:TextBox>&nbsp;<asp:HyperLink
                                        ID="HypCal2" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                                        ToolTip="Choose a Date">Choose a Date</asp:HyperLink>
                                <cc2:JCalendar ID="JCalendar2" runat="server" ControlToAssign="ObsolDatetbox" ImgURL="~/PresentationLayer/graph/calendar.gif" Visible="False" />
                                <asp:Label ID="Label6" runat="server" ForeColor="Red" Text="*" Width="1px"></asp:Label><asp:RequiredFieldValidator ID="obserr" runat="server" ControlToValidate="ObsolDatetbox" ForeColor="White">*</asp:RequiredFieldValidator></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 10%; height: 30px">
                                <asp:Label ID="suppnamelab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" colspan="3" style="height: 30px; width: 90%;">
                                <asp:TextBox ID="suppnametbox" runat="server" CssClass="textborder" Width="352px" style="width: 87%" MaxLength="50"></asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 10%; height: 30px">
                                <asp:Label ID="commonpartlab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 40%; height: 30px">
                                <asp:DropDownList ID="commonpartdrpd" runat="server" CssClass="textborder" Width="128px" style="width: 78%">
                                </asp:DropDownList></td>
                            <td align="right" style="width: 10%; height: 30px">
                                <asp:Label ID="ServiceItemlab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 40%; height: 30px">
                                <asp:DropDownList ID="ServiceItemdrpd" runat="server" CssClass="textborder" Width="176px" style="width: 78%">
                                </asp:DropDownList></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 10%; height: 39px" id="#kit">
                                <asp:Label ID="KitItemlab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 40%; height: 39px">
                                <asp:DropDownList ID="KitItemdrpd" runat="server" CssClass="textborder" Width="128px" style="width: 78%" AutoPostBack="False">
                                </asp:DropDownList></td>
                            <td align="right" style="width: 10%; height: 30px">
                                <asp:Label ID="Statuslab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 40%; height: 30px">
                                <asp:DropDownList ID="Statusdrpd" runat="server" CssClass="textborder" Width="176px" style="width: 78%">
                                </asp:DropDownList></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 10%; height: 30px">
                                <asp:Label ID="remarkslab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" colspan="3" style="height: 30px; width: 90%;">
                                <asp:TextBox ID="remarkstbox" runat="server" CssClass="textborder" Width="92%" Height="48px" MaxLength="400" style="width: 87%"></asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 10%; height: 30px">
                                <asp:Label ID="createbylab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 40%; height: 30px">
                                <asp:TextBox ID="creatbytbox" runat="server" CssClass="textborder" Width="144px" style="width: 78%" ReadOnly="True"></asp:TextBox></td>
                            <td align="right" style="width: 10%; height: 30px">
                                <asp:Label ID="creatdatelab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 40%; height: 30px">
                                <asp:TextBox ID="creatdatetbox" runat="server" CssClass="textborder" MaxLength="2"
                                    Width="73%" style="width: 78%" ReadOnly="True"></asp:TextBox>
                                </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 10%; height: 30px">
                                <asp:Label ID="modifybylab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 40%; height: 30px">
                                <asp:TextBox ID="modifybytbox" runat="server" CssClass="textborder" Width="144px" style="width: 78%" ReadOnly="True"></asp:TextBox>
                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;
                            </td>
                            <td align="right" style="width: 10%; height: 30px">
                                <asp:Label ID="modifydatelab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 40%; height: 30px">
                                <asp:TextBox ID="modifydatetbox" runat="server" CssClass="textborder" MaxLength="2"
                                    Width="72%" style="width: 78%" ReadOnly="True"></asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="left" colspan="4" rowspan="1">
                                &nbsp;<asp:GridView ID="kititemview" runat="server" AllowPaging="True" AllowSorting="True"
                                    Font-Size="Smaller" Width="100%">
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                    &nbsp;
                </td>
            </tr>
              <tr>
                  <td style="width: 100%">
                  <font color=red><asp:Label ID="lblCompBottom" runat="server" Text="Label" Visible="true"></asp:Label></font>
                  <br />
                                <asp:LinkButton ID="saveButton" runat="server">save</asp:LinkButton>
                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                      &nbsp; &nbsp;
                      <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/PresentationLayer/masterrecord/PartRequire.aspx">[cance]</asp:HyperLink>
                      &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                      &nbsp; &nbsp; &nbsp; &nbsp;
                                <asp:HyperLink ID="AddLink" runat="server" >[Add]</asp:HyperLink>&nbsp; &nbsp; &nbsp;<asp:LinkButton ID="Addkit" runat="server" Visible="False" CausesValidation="False">LinkB</asp:LinkButton></td>
               
</tr>
        </table>
                                  			</TD>
					</TR>
			</TABLE>
                    <cc1:messagebox id="MessageBox1" runat="server"></cc1:messagebox>
                    &nbsp; &nbsp; &nbsp; &nbsp;
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
            ShowSummary="False" />   
    
    </form>
</body>
</html>
