Imports SQLDataAccess
Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data





Partial Class PresentationLayer_masterrecord_addPriceSetUp

    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try


        If Not Page.IsPostBack Then
           
            Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            priceidLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0001")
            partidlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0002")
            countryidlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0003")
            LinkButton2.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0010")
            'EffectiveDatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRID-0006")
            CreatedBylab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATBY")
            CreatedDatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
            ModifiedBylab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFBY")
            ModifiedDatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFDATE")
            StatusLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Status")
            savebutton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Save")
            cancellink.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Cancel")
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0009")
            amountlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0005")
            LinkButton2.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0010")
            Me.lblCompTop.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            Me.lblCompBottom.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            AddLink.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Add")
            AddLink.NavigateUrl = Page.Request.RawUrl

            Dim objXmlTr1 As New clsXml
            objXmlTr1.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            PriceIDva.ErrorMessage = objXmlTr1.GetLabelName("StatusMessage", "PRICEID")
            COUNTRYIDVA.ErrorMessage = objXmlTr1.GetLabelName("StatusMessage", "CountryID")
            partidVA.ErrorMessage = objXmlTr1.GetLabelName("StatusMessage", "PARTID")


            '''access control'''''''''''''''''''''''''''''''''''''''''''''''
            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "14")
            Me.savebutton.Enabled = purviewArray(0)
            Me.LinkButton2.Enabled = purviewArray(0)
            If purviewArray(0) = False Then
                Response.Redirect("~/PresentationLayer/logon.aspx")

            End If

            Dim returnpsid As Integer
            returnpsid = Request.Params("pricesetupid")

            Dim requestFlag As Integer = 0
            If (Request.QueryString("flag") = Nothing) Then
                requestFlag = 0
            Else
                requestFlag = Request.Params("flag")
            End If

            If requestFlag = 1 Then
                Me.savebutton.Enabled = False
            End If
            Dim userIDNamestr As String = Session("userID").ToString().ToUpper + "-" + Session("username").ToString.ToUpper
            Me.CreatedBy.Text = userIDNamestr
            Me.ModifiedBy.Text = userIDNamestr
            CreatedDate.Text = Date.Now()
            ModifiedDate.Text = Date.Now()


            Dim rank As String = Session("login_rank").ToString().ToUpper()
            Dim country As New clsCommonClass
            If rank <> 0 Then
                country.spctr = Session("login_ctryID").ToString().ToUpper()
            End If
            country.rank = rank
            Dim countryds As New DataSet
            countryds = country.Getcomidname("BB_MASCTRY_IDNAME")
            If countryds.Tables.Count <> 0 Then
                databonds(countryds, Me.CountryIDDrop)
                Me.CountryIDDrop.Items.Insert(0, "")
            End If
            CountryIDDrop.SelectedValue = Session("login_ctryID").ToString().ToUpper()

            Dim priceid As New clsCommonClass
            If rank <> 0 Then
                priceid.spctr = Session("login_ctryID").ToString().ToUpper()

            End If
            priceid.rank = rank
            Dim priceidds As New DataSet
            priceidds = priceid.Getcomidname("BB_MASPRID_IDNAME")
            If priceidds.Tables.Count <> 0 Then
                databonds(priceidds, Me.priceidDrop)
                Me.priceidDrop.Items.Insert(0, "")
            End If


            Dim partid As New clsCommonClass
            If rank <> 0 Then
                partid.spctr = Session("login_ctryID").ToString().ToUpper()

            End If
            partid.rank = rank
            Dim partidds As New DataSet
            partidds = partid.Getcomidname("BB_MASPARTID_IDNAME")
            If partidds.Tables.Count <> 0 Then
                databonds(partidds, Me.partidDrop)
                Me.partidDrop.Items.Insert(0, "")
            End If


            Dim cntryStat As New clsrlconfirminf()
            Dim statParam As ArrayList = New ArrayList
            statParam = cntryStat.searchconfirminf("STATUS")
            Dim count As Integer
            Dim statid As String
            Dim statnm As String
            For count = 0 To statParam.Count - 1
                statid = statParam.Item(count)
                statnm = statParam.Item(count + 1)
                count = count + 1
                If (statid.Equals("ACTIVE") Or statid.Equals("OBSOLETE")) Then
                    Me.StatusDrop.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                End If
            Next
            StatusDrop.Items(0).Selected = True


            Dim setupid1 As New clsPriceSetUp
            Dim n As Integer
            n = setupid1.Getmaxid()
            SetUpID.Text = n + 1

            'return  master table
            If returnpsid <> 0 Then
                Dim PriceSetUpEntity1 As New clsPriceSetUp()
                PriceSetUpEntity1.PriceSetUpID = returnpsid

                Dim retnArray As ArrayList = PriceSetUpEntity1.GetReturnPriceSetUpIDDetailsByID()



                Dim userIDNamestr1 As String = Session("userID").ToString().ToUpper + "-" + Session("username").ToString.ToUpper
                ModifiedBy.Text = userIDNamestr1

                Dim Rcreatby As String = retnArray.Item(5).ToString().ToUpper()
                If Rcreatby <> "ADMIN" Then
                    Dim Rcreat As New clsCommonClass
                    Rcreat.userid() = Rcreatby
                    Dim Rcreatds As New DataSet()
                    Rcreatds = Rcreat.Getuseridname("BB_MASUSER_SelByIDName")

                    Me.CreatedBy.Text = Rcreatds.Tables(0).Rows(0).Item(0)
                Else
                    Me.CreatedBy.Text = "ADMIN-ADMIN"
                End If
                'create the dropdownlist
                SetUpID.Text = retnArray(0).ToString()

                StatusDrop.Text = retnArray(4).ToString()
                'CreatedBy.Text = retnArray(5).ToString()
                CreatedBy.ReadOnly = True
                CreatedDate.ReadOnly = True
                ModifiedBy.Text = retnArray(7).ToString()

                CreatedDate.Text = retnArray(6).ToString()
                CreatedDate.ReadOnly = True
                ModifiedDate.Text = retnArray(8).ToString()
                CountryIDDrop.Text = retnArray(3).ToString()
                priceidDrop.Text = retnArray(2).ToString()
                partidDrop.Text = retnArray(1).ToString()

            End If

            'display detail table

            Dim objXmlTr2 As New clsXml
            objXmlTr2.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            Dim PriceSetUpEntity As New clsPriceSetUp()
            PriceSetUpEntity.PriceSetUpID = returnpsid
            Dim PriceSetUpall As DataSet = PriceSetUpEntity.GetPriceSetUp1()
            PriceSetUp1View.DataSource = PriceSetUpall
            Session("PriceSetUp1View") = PriceSetUpall
            PriceSetUp1View.AllowPaging = True
            PriceSetUp1View.AllowSorting = True
            PriceSetUp1View.DataBind()
            If PriceSetUpall.Tables(0).Rows.Count <> 0 Then
                PriceSetUp1View.HeaderRow.Cells(0).Text = objXmlTr2.GetLabelName("EngLabelMsg", "BB-MASPRCE-0014")
                PriceSetUp1View.HeaderRow.Cells(0).Font.Size = 8
                PriceSetUp1View.HeaderRow.Cells(1).Text = objXmlTr2.GetLabelName("EngLabelMsg", "BB-MASPRCE-0006")
                PriceSetUp1View.HeaderRow.Cells(1).Font.Size = 8
                PriceSetUp1View.HeaderRow.Cells(2).Text = objXmlTr2.GetLabelName("EngLabelMsg", "BB-MASPRCE-0005")
                PriceSetUp1View.HeaderRow.Cells(2).Font.Size = 8
                PriceSetUp1View.HeaderRow.Cells(3).Text = objXmlTr2.GetLabelName("EngLabelMsg", "BB-Status")
                PriceSetUp1View.HeaderRow.Cells(3).Font.Size = 8
                PriceSetUp1View.HeaderRow.Cells(4).Text = objXmlTr2.GetLabelName("EngLabelMsg", "BB-CREATDATE")
                PriceSetUp1View.HeaderRow.Cells(4).Font.Size = 8
               

            End If
        End If


    End Sub

#Region " id bond"
    Public Function databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        Dim row As DataRow
        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            NewItem.Text = row("id") & "-" & row("name")
            NewItem.Value = row("id")
            dropdown.Items.Add(NewItem)
        Next

    End Function
#End Region


    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveButton.Click
        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim msginfo As String = objXm.GetLabelName("StatusMessage", "SAVEORNOT")
        Dim msgtitle As String = objXm.GetLabelName("StatusMessage", "SAVETITLE")
        MessageBox1.Confirm(msginfo)
        'display()
        Dim dsPriceIDall As DataSet = Session("PriceSetUp1View")
        PriceSetUp1View.DataSource = dsPriceIDall
        PriceSetUp1View.DataBind()
        PriceSetUp1View.AllowPaging = True
        PriceSetUp1View.AllowSorting = True
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        If dsPriceIDall.Tables(0).Rows.Count <> 0 Then
            PriceSetUp1View.HeaderRow.Cells(0).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0014")
            PriceSetUp1View.HeaderRow.Cells(0).Font.Size = 8
            PriceSetUp1View.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0006")
            PriceSetUp1View.HeaderRow.Cells(1).Font.Size = 8
            PriceSetUp1View.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0005")
            PriceSetUp1View.HeaderRow.Cells(2).Font.Size = 8
            PriceSetUp1View.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Status")
            PriceSetUp1View.HeaderRow.Cells(3).Font.Size = 8
            PriceSetUp1View.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
            PriceSetUp1View.HeaderRow.Cells(4).Font.Size = 8
           
        End If

      

    End Sub




    Protected Sub LinkButton2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton2.Click
       
        Dim objXm1 As New clsXml
        objXm1.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        If Me.StatusDrop.SelectedValue <> "ACTIVE" Then
            errlab.Text = objXm1.GetLabelName("StatusMessage", "PRICESETUPADDERROR")
            errlab.Visible = True
            Return
        End If
        If savebutton.Enabled = False Then

            Dim url As String = "~/PresentationLayer/masterrecord/addPriceSetUp1.aspx?"
            url &= "pricesetupid=" & Me.SetUpID.Text & "&"
            url &= "symbl=" & 0 & "&"
            url &= "priceid=" & priceidDrop.SelectedValue().ToString() & "&"
            url &= "parteid=" & partidDrop.SelectedValue().ToString() & "&"
            url &= "ctryid=" & CountryIDDrop.SelectedValue().ToString()
            Response.Redirect(url)
        Else
            Dim objXm As New clsXml
            objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            errlab.Text = objXm.GetLabelName("StatusMessage", "IFSAVE")
            errlab.Visible = True
        End If
    End Sub

    Protected Sub MessageBox1_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox1.GetMessageBoxResponse
        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
            Dim pricesidStat As New clsCommonClass()

            Dim PriceSetUpEntity As New clsPriceSetUp()
            If (Trim(SetUpID.Text) <> "") Then
                PriceSetUpEntity.PriceSetUpID = SetUpID.Text
            End If
            If (partidDrop.SelectedValue().ToString() <> "") Then
                PriceSetUpEntity.PartID = partidDrop.SelectedItem.Value.ToString()
            End If
            If (CountryIDDrop.SelectedValue().ToString() <> "") Then
                PriceSetUpEntity.CountryID = CountryIDDrop.SelectedItem.Value.ToString()
            End If
            If (priceidDrop.SelectedValue().ToString() <> "") Then
                PriceSetUpEntity.PriceID = priceidDrop.SelectedItem.Value.ToString().ToUpper()
            End If
            If (StatusDrop.SelectedItem.Value.ToString() <> "") Then
                PriceSetUpEntity.PriceSetUpStatus = StatusDrop.SelectedItem.Value.ToString()
            End If
            If (Trim(CreatedBy.Text) <> "") Then
                PriceSetUpEntity.CreatedBy = Session("userID").ToString().ToUpper
            End If
            If (Trim(CreatedDate.Text) <> "") Then
                PriceSetUpEntity.CreatedDate = pricesidStat.DatetoDatabase(CreatedDate.Text)
            End If
            If (Trim(ModifiedBy.Text) <> "") Then
                PriceSetUpEntity.ModifiedBy = Session("userID").ToString().ToUpper
            End If
            If (Trim(ModifiedDate.Text) <> "") Then
                PriceSetUpEntity.ModifiedDate = pricesidStat.DatetoDatabase(ModifiedDate.Text)
            End If
            'If (Trim(Me.CreatedBy.Text) <> "") Then
            '    PriceSetUpEntity.CreatedBy = Session("userID").ToString.ToUpper()
            'End If
            'If (Trim(Me.ModifiedBy.Text) <> "") Then
            '    PriceSetUpEntity.ModifiedBy = Session("userID").ToString.ToUpper()
            'End If
            Dim dupCount As Integer = PriceSetUpEntity.GetDuplicatedPriceSetUpID()
            If dupCount.Equals(-1) Then
                Dim objXm As New clsXml
                objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                errlab.Text = objXm.GetLabelName("StatusMessage", "DUPPPP")
                errlab.Visible = True
            ElseIf dupCount.Equals(0) Then
                Dim insPRIDCnt As Integer = PriceSetUpEntity.Insert()
                If insPRIDCnt = 0 Then
                    Dim objXm As New clsXml
                    objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    errlab.Text = objXm.GetLabelName("StatusMessage", "SuccessfullySaved")
                    errlab.Visible = True
                    savebutton.Enabled = False
                Else

                    Response.Redirect("~/PresentationLayer/Error.aspx")

                End If
            End If
           
        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
        End If

        Dim dsPriceIDall As DataSet = Session("PriceSetUp1View")
        PriceSetUp1View.DataSource = dsPriceIDall
        PriceSetUp1View.DataBind()
        PriceSetUp1View.AllowPaging = True
        PriceSetUp1View.AllowSorting = True
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        If dsPriceIDall.Tables(0).Rows.Count <> 0 Then
            PriceSetUp1View.HeaderRow.Cells(0).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0014")
            PriceSetUp1View.HeaderRow.Cells(0).Font.Size = 8
            PriceSetUp1View.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0006")
            PriceSetUp1View.HeaderRow.Cells(1).Font.Size = 8
            PriceSetUp1View.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0005")
            PriceSetUp1View.HeaderRow.Cells(2).Font.Size = 8
            PriceSetUp1View.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Status")
            PriceSetUp1View.HeaderRow.Cells(3).Font.Size = 8
            PriceSetUp1View.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
            PriceSetUp1View.HeaderRow.Cells(4).Font.Size = 8
          


        End If
    End Sub

    Protected Sub PriceSetUp1View_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles PriceSetUp1View.PageIndexChanging
        PriceSetUp1View.PageIndex = e.NewPageIndex
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        Dim dsPriceIDall As DataSet = Session("PriceSetUp1View")
        PriceSetUp1View.DataSource = dsPriceIDall
        PriceSetUp1View.DataBind()
        PriceSetUp1View.AllowPaging = True
        PriceSetUp1View.AllowSorting = True
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        If dsPriceIDall.Tables(0).Rows.Count <> 0 Then
            PriceSetUp1View.HeaderRow.Cells(0).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0014")
            PriceSetUp1View.HeaderRow.Cells(0).Font.Size = 8
            PriceSetUp1View.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0006")
            PriceSetUp1View.HeaderRow.Cells(1).Font.Size = 8
            PriceSetUp1View.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0005")
            PriceSetUp1View.HeaderRow.Cells(2).Font.Size = 8
            PriceSetUp1View.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Status")
            PriceSetUp1View.HeaderRow.Cells(3).Font.Size = 8
            PriceSetUp1View.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
            PriceSetUp1View.HeaderRow.Cells(4).Font.Size = 8
          


        End If
    End Sub
    'Protected Sub display()
    '    Dim returnpsid As Integer
    '    returnpsid = Request.QueryString("pricesetupid")
    '    Dim objXmlTr2 As New clsXml
    '    objXmlTr2.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
    '    Dim PriceSetUpEntity As New clsPriceSetUp()
    '    PriceSetUpEntity.PriceSetUpID = returnpsid
    '    Dim PriceSetUpall As DataSet = PriceSetUpEntity.GetPriceSetUp1()
    '    PriceSetUp1View.DataSource = PriceSetUpall
    '    session("PriceSetUp1View") = PriceSetUpall
    '    PriceSetUp1View.AllowPaging = True
    '    PriceSetUp1View.AllowSorting = True
    '    PriceSetUp1View.DataBind()
    '    If PriceSetUpall.Tables(0).Rows.Count <> 0 Then
    '        PriceSetUp1View.HeaderRow.Cells(0).Text = objXmlTr2.GetLabelName("EngLabelMsg", "BB-MASPRCE-0014")
    '        PriceSetUp1View.HeaderRow.Cells(0).Font.Size = 8
    '        PriceSetUp1View.HeaderRow.Cells(1).Text = objXmlTr2.GetLabelName("EngLabelMsg", "BB-MASPRCE-0006")
    '        PriceSetUp1View.HeaderRow.Cells(1).Font.Size = 8
    '        PriceSetUp1View.HeaderRow.Cells(2).Text = objXmlTr2.GetLabelName("EngLabelMsg", "BB-MASPRCE-0005")
    '        PriceSetUp1View.HeaderRow.Cells(2).Font.Size = 8
    '        PriceSetUp1View.HeaderRow.Cells(3).Text = objXmlTr2.GetLabelName("EngLabelMsg", "BB-Status")
    '        PriceSetUp1View.HeaderRow.Cells(3).Font.Size = 8
    '        PriceSetUp1View.HeaderRow.Cells(4).Text = objXmlTr2.GetLabelName("EngLabelMsg", "BB-CREATDATE")
    '        PriceSetUp1View.HeaderRow.Cells(4).Font.Size = 8

    '    End If
    'End Sub
End Class
