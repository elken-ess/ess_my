﻿<%@ Page Language="VB" AutoEventWireup="false" EnableEventValidation ="false"  CodeFile="modifyuser.aspx.vb" Inherits="PresentationLayer_masterrecord_modifyuser" %>

<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
  <title>Modify User</title>
	<LINK href="../css/style.css" type="text/css" rel="stylesheet">    
</head>
<body  id="model type">
   <form id="modeltypeform"  method="post" action=""   runat=server>
  <TABLE border="0" id=TABLE2 style="width: 100%">
           <tr>
           <td>
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>
							<TD width="1%" background="../graph/title_bg.gif"><IMG height="24" src="../graph/title1.gif" width="5"></TD>
							<TD class="style2" width="98%" background="../graph/title_bg.gif">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></TD>
							<TD align="right" width="1%" background="../graph/title_bg.gif"><IMG height="24" src="../graph/title_2.gif" width="5"></TD>
						</TR>
			 </TABLE>
           </tr>
           <tr>
           <td>
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>					
							<TD class="style2" width="98%" background="../graph/title_bg.gif">
                                <font color=red><asp:Label ID="addinfo" runat="server" Text="Label" Visible=false></asp:Label></font></TD>
						</TR>
			 </TABLE>
           </tr>
			<TR>
				<TD>
     <TABLE border="0" id=countrytab style="width: 100%">
			<TR>
				<TD style="width: 0px">
					<TABLE id="modeltype" cellSpacing="1" cellPadding="2" bgColor="#b7e6e6" border="0" style="width: 100%">
					<tr bgcolor="#ffffff">
<td colspan = 4>
                            <asp:Label ID="lblNoteUP" runat="server" ForeColor="Red" Text="Label"></asp:Label>
</td>
             </tr>
             <tr bgcolor="#ffffff">
<td colspan = 4>
                        	&nbsp;
                     	</td>
               </tr>

									  <TR bgColor="#ffffff">
											<TD  align=right style="width: 15%; height: 1px">
                                                <asp:Label ID="UserIDLab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%; height: 1px">
                                                <asp:TextBox ID="UserIDBox" runat="server" CssClass="textborder" Width="90%" ReadOnly="True"></asp:TextBox>
                                                <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="*" Width="14px" Font-Bold="True"></asp:Label>
                                                <asp:RequiredFieldValidator ID="RFuserid" runat="server"
                                                    ForeColor="White" ControlToValidate="UserIDBox" Height="6px" Width="1px">*</asp:RequiredFieldValidator></TD>
											<TD  align=right style="width: 15%; height: 1px">
                                                <asp:Label ID="StaffStatusLab" runat="server" Text="Label"></asp:Label></TD>
											<TD  align=left style="width: 35%; height: 1px">
                                                <asp:DropDownList ID="StaffStatusDDL" runat="server" CssClass="textborder" Width="90%">
                                                </asp:DropDownList></TD>
										</TR>
										<TR bgColor="#ffffff">
											<TD  align=right style="width: 15%; height: 5px;">
                                                <asp:Label ID="UserNameLab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%; height: 5px;">
                                                <asp:TextBox ID="UserNameBox" runat="server" CssClass="textborder" Width="90%" MaxLength="50"></asp:TextBox>
                                                <asp:Label ID="Label2" runat="server" ForeColor="Red" Text="*" Width="14px" Font-Bold="True"></asp:Label>
                                                <asp:RequiredFieldValidator ID="RFusername" runat="server" ErrorMessage=" "
                                                    ForeColor="White" ControlToValidate="UserNameBox" Height="2px" Width="1px">.</asp:RequiredFieldValidator></TD>
											<TD  align=right style="width: 15%; height: 5px;">
                                                <asp:Label ID="CountryIDLab" runat="server" Text="Label"></asp:Label></TD>
											<TD  align=left style="width: 35%; height: 5px;">
                                                <asp:DropDownList ID="CountryIDDDL" runat="server" CssClass="textborder" Width="90%" AutoPostBack="True">
                                                </asp:DropDownList>&nbsp;
                                                <asp:Label ID="Label8" runat="server" ForeColor="Red" Height="8px" Text="*" Width="1px" Font-Bold="True"></asp:Label></TD>
										</TR>
										<TR bgColor="#ffffff">
											<TD  align=right style="width: 15%; height: 30px;">
                                                <asp:Label ID="AccessProGroupLab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%; height: 30px;">
                                                <asp:DropDownList ID="AccessProGroupDDL" runat="server" CssClass="textborder" Width="90%">
                                                </asp:DropDownList>
                                                <asp:Label ID="Label9" runat="server" ForeColor="Red" Text="*" Width="14px" Font-Bold="True"></asp:Label></TD>
											<TD align=right style="width: 15%; height: 30px;">
                                                <asp:Label ID="AlterNameLab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%; height: 30px;">
                                                <asp:TextBox ID="AlterNameBox" runat="server" CssClass="textborder" Width="90%" MaxLength="50"></asp:TextBox>&nbsp;
                                            </TD>
										</TR>
										<TR bgColor="#ffffff">
											<TD align=right style="width: 15%; height: 30px">
                                                <asp:Label ID="RankLab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%; height: 30px">
                                                <asp:DropDownList ID="RankDDL" runat="server" CssClass="textborder" Width="90%">
                                                </asp:DropDownList></TD>
											<TD align=right style="width: 15%; height: 30px">
                                                <asp:Label ID="FailedLogonLab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%; height: 30px">
                                                <asp:TextBox ID="FailedLogonBox" runat="server" CssClass="textborder" ReadOnly="True" Width="90%"></asp:TextBox>
                                                <asp:Label ID="Label3" runat="server" ForeColor="Red" Height="8px" Text="*" Width="1px" Font-Bold="True"></asp:Label></TD>
										</TR>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%; height: 30px">
                                <asp:Label ID="ChangePassWdLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%; height: 30px">
                                <asp:DropDownList ID="ChangePassWdDDL" runat="server" CssClass="textborder" Width="90%">
                                </asp:DropDownList></td>
                            <td align="right" style="width: 15%; height: 30px">
                                <asp:Label ID="CompanIDLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%; height: 30px">
                                <asp:DropDownList ID="CompanIDDDL" runat="server" CssClass="textborder" AutoPostBack="True" Width="90%">
                                </asp:DropDownList>&nbsp;
                                <asp:Label ID="Label7" runat="server" ForeColor="Red" Height="8px" Text="*" Width="1px" Font-Bold="True"></asp:Label></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%; height: 30px">
                                <asp:Label ID="DepartmentLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%; height: 30px">
                                <asp:DropDownList ID="DepartmentDDL" runat="server" CssClass="textborder" Width="90%">
                                </asp:DropDownList></td>
                            <td align="right" style="width: 15%; height: 30px">
                                <asp:Label ID="PermanentStaffLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%; height: 30px">
                                <asp:DropDownList ID="PermanentStaffDDL" runat="server" CssClass="textborder" Width="90%">
                                </asp:DropDownList></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%; height: 30px">
                                <asp:Label ID="SvcIDLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%; height: 30px">
                                <asp:DropDownList ID="SvcIDDDL" runat="server" CssClass="textborder" Width="90%">
                                </asp:DropDownList>
                                <asp:Label ID="Label6" runat="server" ForeColor="Red" Height="8px" Text="*" Width="1px" Font-Bold="True"></asp:Label></td>
                            <td align="right" style="width: 15%; height: 30px">
                                <asp:Label ID="EmailAdressLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%; height: 30px">
                                <asp:TextBox ID="EmailAdressBox" runat="server" CssClass="textborder" Width="90%" MaxLength="30"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="REVEmail" runat="server" ControlToValidate="EmailAdressBox"
                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Font-Bold="True">*</asp:RegularExpressionValidator></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%; height: 30px">
                                <asp:Label ID="pswLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%; height: 30px">
                                <asp:TextBox ID="pswBox" runat="server" CssClass="textborder" TextMode="Password"
                                    Width="90%" MaxLength="12">********
*******</asp:TextBox>&nbsp;
                                </td>
                            <td align="right" style="width: 15%; height: 30px">
                                <asp:Label ID="confirmpswLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%; height: 30px">
                                <asp:TextBox ID="confirmpswBox" runat="server" CssClass="textborder"
                                    TextMode="Password" Width="90%" MaxLength="12"></asp:TextBox>
                                </td>
                        </tr>
										<TR bgColor="#ffffff">
											<TD align=right style="width: 15%; height: 30px;">
                                                <asp:Label ID="creatby" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%; height: 30px;">
                                                <asp:TextBox ID="creatbybox" runat="server" CssClass="textborder" ReadOnly=true Width="90%"></asp:TextBox></TD>
											<TD align=right style="width: 15%; height: 30px;">
                                                <asp:Label ID="creatdate" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%; height: 30px;">
                                                <asp:TextBox ID="creatdtbox" runat="server" CssClass="textborder" ReadOnly=true Width="90%"></asp:TextBox></TD>
										</TR>
										<TR bgColor="#ffffff">
											<TD align=right style="width: 15%; height: 28px;">
                                                <asp:Label ID="modfyby" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%; height: 28px;">
                                                <asp:TextBox ID="modfybybox" runat="server" CssClass="textborder" ReadOnly=true Width="90%"></asp:TextBox></TD>
											<TD align=right style="width: 15%; height: 28px;">
                                                <asp:Label ID="mdfydate" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%; height: 28px;">
                                                <asp:TextBox ID="mdfydtbox" runat="server" CssClass="textborder" ReadOnly=true Width="90%"></asp:TextBox></TD>
										</TR> <tr bgcolor="#ffffff">
<td colspan = 4>
                        	&nbsp;
                     	</td>
               </tr>
										<tr bgcolor="#ffffff">
<td colspan = 4>
                            <asp:Label ID="lblNoteDown" runat="server" ForeColor="Red" Text="Label"></asp:Label>
</td>
             </tr>
            

								</TABLE>
						</TD>
					</TR>
					<TR>
						<TD style="height: 28px; width: 100%;">
							<TABLE id="Table1" cellSpacing="1" cellPadding="1"  border="0">
									<TR>
										<TD style="WIDTH: 20%; height: 16px;" align=center>
                                            &nbsp;<asp:LinkButton ID="LinkButton1" runat="server">save</asp:LinkButton></TD>
										<td align=center style="height: 16px">
                                            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/PresentationLayer/masterrecord/user.aspx">cancel</asp:HyperLink></td>
									</TR>
							</TABLE>
						</TD>
					</TR>
			</TABLE>
			</TD>
					</TR>
			</TABLE>
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                        ShowSummary="False" />
       &nbsp;&nbsp;
                    <cc1:messagebox id="MessageBox1" runat="server"></cc1:messagebox>
       &nbsp;&nbsp;
       <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="pswBox"
           ControlToValidate="confirmpswBox" ForeColor="White"></asp:CompareValidator>
       <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="pswBox"
           ErrorMessage="RegularExpressionValidator" ForeColor="White" ValidationExpression="\w{6,20}"></asp:RegularExpressionValidator>
       <asp:RequiredFieldValidator ID="RFVCOTRY" runat="server" ControlToValidate="CountryIDDDL"
           ErrorMessage="RequiredFieldValidator" ForeColor="White"></asp:RequiredFieldValidator>
       <asp:RequiredFieldValidator ID="RFVCOMP" runat="server" ControlToValidate="CompanIDDDL"
           ErrorMessage="RequiredFieldValidator" ForeColor="White"></asp:RequiredFieldValidator>
       <asp:RequiredFieldValidator ID="RFVSERVICE" runat="server" ControlToValidate="SvcIDDDL"
           ForeColor="White"></asp:RequiredFieldValidator>
       <asp:RequiredFieldValidator ID="rfvaccessg" runat="server" ControlToValidate="AccessProGroupDDL"
           ForeColor="White"></asp:RequiredFieldValidator>&nbsp;

    </form>
</body>
</html>
