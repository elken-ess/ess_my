<%@ Page Language="VB" AutoEventWireup="false" EnableEventValidation ="false"  CodeFile="modifymodeltype.aspx.vb" Inherits="PresentationLayer_masterrecord_modifymodeltype" %>

<%@ Register Assembly="JCalendar" Namespace="JCalendar" TagPrefix="cc2" %>

<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
<title>Modify Model Type</title>
	<LINK href="../css/style.css" type="text/css" rel="stylesheet">
    <SCRIPT language="JavaScript">
		  function validate()
         {}
//			
		</SCRIPT>
		<script language="JavaScript" src="../js/common.js"></script>
</head>
<body  id="modifymodeltype">
   <form id="mdfymodeltypeform"  method="post" action="" onsubmit="return validate()" runat=server>
        <TABLE border="0" id=TABLE2 width="100%">
           <tr>
           <td>
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>
							<TD width="1%" background="../graph/title_bg.gif"><IMG height="24" src="../graph/title1.gif" width="5"></TD>
							<TD class="style2" width="98%" background="../graph/title_bg.gif">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></TD>
							<TD align="right" width="1%" background="../graph/title_bg.gif"><IMG height="24" src="../graph/title_2.gif" width="5"></TD>
						</TR>
			 </TABLE>
           </tr>
           <tr>
           <td>
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>					
							<TD class="style2" width="98%" background="../graph/title_bg.gif">
                                <font color=red><asp:Label ID="mdyinfo" runat="server" Text="Label" Visible=false></asp:Label></font></TD>
						</TR>
			 </TABLE>
               <asp:Label ID="Label9" runat="server" ForeColor="Red"></asp:Label>
           </tr>
			<TR>
				<TD>
        <table id="countrytab" border="0" width="100%">
            <tr>
                <td>
                    <table id="modeltype" bgcolor="#b7e6e6" border="0" cellpadding="2" cellspacing="1" style="width: 100%">
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 16%; height: 27px">
                                &nbsp;<asp:Label ID="CountryIDLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 34%; height: 27px">
                                <asp:TextBox ID="CountryIDDDL" runat="server" CssClass="textborder"></asp:TextBox>
                                <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="*" Font-Bold="True"></asp:Label></td>
                            <td align="right" style="height: 27px; width: 16%;">
                                <asp:Label ID="ProductClassLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 59%; height: 27px">
                                <asp:DropDownList ID="ProductClassDDL" runat="server" CssClass="textborder">
                                </asp:DropDownList></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 16%; height: 30px;">
                                &nbsp;<asp:Label ID="ModelIDLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 34%; height: 30px;">
                                <asp:TextBox ID="ModelIDBox" runat="server" CssClass="textborder" MaxLength="10"></asp:TextBox>
                                <asp:Label ID="Label2" runat="server" ForeColor="Red" Text="*" Font-Bold="True"></asp:Label></td>
                            <td align="right" style="height: 30px; width: 16%;">
                                <asp:Label ID="ModelNameLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 59%; height: 30px;">
                                <asp:TextBox ID="ModelNameBox" runat="server" CssClass="textborder" MaxLength="50"></asp:TextBox>
                                <asp:Label ID="Label3" runat="server" ForeColor="Red" Text="*" Font-Bold="True"></asp:Label>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="RequiredFieldValidator"
                                    ForeColor="White" ControlToValidate="ModelNameBox">*</asp:RequiredFieldValidator></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 16%">
                                <asp:Label ID="AlModelNamLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" colspan="3" width="85%">
                                <asp:TextBox ID="AlModelNamBox" runat="server" CssClass="textborder" MaxLength="50"></asp:TextBox>
                                <asp:Label ID="Label4" runat="server" ForeColor="Red" Text="*" Font-Bold="True"></asp:Label></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 16%; height: 1px">
                                <asp:Label ID="WarrantyDayLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 34%; height: 1px">
                                <asp:TextBox ID="WarrantyDayBox" runat="server" CssClass="textborder" MaxLength="4"></asp:TextBox>
                                <asp:Label ID="Label7" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                <asp:RangeValidator ID="RangeValidator1" runat="server"
                                    ForeColor="White" ControlToValidate="WarrantyDayBox" MaximumValue="999999" MinimumValue="0" Type="Integer">*</asp:RangeValidator>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="WarrantyDayBox"
                                    ErrorMessage="RequiredFieldValidator" ForeColor="White">*</asp:RequiredFieldValidator></td>
                            <td align="right" style="height: 1px; width: 16%;">
                                <asp:Label ID="StatusLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 59%; height: 1px">
                                <asp:DropDownList ID="StatusDDL" runat="server" CssClass="textborder">
                                </asp:DropDownList></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 16%; height: 1px;">
                                <asp:Label ID="EffectiveDateLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 34%; height: 1px; color: red;">
                                <asp:TextBox ID="EffectiveDateBox" runat="server" CssClass="textborder" ReadOnly="True"></asp:TextBox>
                                *<asp:HyperLink ID="HypCal" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                                    ToolTip="Choose a Date">Choose a Date</asp:HyperLink>
                                <cc2:JCalendar
                                    ID="JCalendar1" runat="server" ControlToAssign="EffectiveDateBox" ImgURL="~/PresentationLayer/graph/calendar.gif" Visible="False" />
                            </td>
                            <td align="right" style="height: 1px; width: 16%;">
                                <asp:Label ID="ObsoleteDateLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 59%; height: 1px; color: red;">
                                <asp:TextBox ID="ObsoleteDateLabBox" runat="server" CssClass="textborder" ReadOnly="True"></asp:TextBox>
                                <asp:Label ID="Label5" runat="server" Font-Bold="True" ForeColor="Red" Text="*"></asp:Label>
                                <asp:HyperLink ID="HypCal2" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                                    ToolTip="Choose a Date">Choose a Date</asp:HyperLink>
                                <cc2:JCalendar
                                    ID="JCalendar2" runat="server" ControlToAssign="ObsoleteDateLabBox" ImgURL="~/PresentationLayer/graph/calendar.gif" Visible="False" />
                            </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 16%; height: 30px;">
                                <asp:Label ID="creatby" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 34%; height: 30px;">
                                <asp:TextBox ID="creatbybox" runat="server" CssClass="textborder" ReadOnly="true"></asp:TextBox></td>
                            <td align="right" style="height: 30px; width: 16%;">
                                <asp:Label ID="creatdate" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 59%; height: 30px;">
                                <asp:TextBox ID="creatdtbox" runat="server" CssClass="textborder" ReadOnly="true"></asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 16%">
                                <asp:Label ID="modfyby" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 34%">
                                <asp:TextBox ID="modfybybox" runat="server" CssClass="textborder" ReadOnly="true"></asp:TextBox></td>
                            <td align="right" style="width: 16%">
                                <asp:Label ID="mdfydate" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 59%">
                                <asp:TextBox ID="mdfydtbox" runat="server" CssClass="textborder" ReadOnly="true"></asp:TextBox></td>
                        </tr>
                    </table>
                </td>
            </tr>
              <tr>
                <td>
                    <asp:Label ID="Label6" runat="server" ForeColor="Red"></asp:Label></td>
            </tr>
          <tr>
                <td style="width: 865px; height: 2px">
                    <table id="Table1" border="0" cellpadding="1" cellspacing="1">
                        <tr>
                            <td align="center" style="width: 20%">
                                &nbsp;<asp:LinkButton ID="LinkButton1" runat="server">save</asp:LinkButton></td>
                            <td align="center">
                                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/PresentationLayer/masterrecord/modeltype.aspx">cancel</asp:HyperLink></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
                     			</TD>
					</TR>
			</TABLE>
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                        ShowSummary="False" />
       &nbsp; &nbsp;&nbsp; &nbsp;
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="EffectiveDateBox"
                                    ErrorMessage="RequiredFieldValidator" ForeColor="White">*</asp:RequiredFieldValidator>
                    <cc1:messagebox id="MessageBox1" runat="server"></cc1:messagebox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ObsoleteDateLabBox"
                                    ErrorMessage="RequiredFieldValidator" ForeColor="White">*</asp:RequiredFieldValidator>
       <asp:RequiredFieldValidator ID="rfvctry" runat="server" ControlToValidate="CountryIDDDL"
           ForeColor="White">.</asp:RequiredFieldValidator>
       <asp:RequiredFieldValidator ID="rfvaname" runat="server" ControlToValidate="AlModelNamBox"
           ForeColor="White">.</asp:RequiredFieldValidator>
    
    </form>
</body>
</html>
