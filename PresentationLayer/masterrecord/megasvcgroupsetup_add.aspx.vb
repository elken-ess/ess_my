﻿Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Partial Class PresentationLayer_masterrecord_megasvcgroupsetup_add
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try

            'display label message
            Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            lblgroup.Text = "Group"
            lblmodel.Text = "Model"
            lblmonths.Text = "Months"
            lblinterval.Text = "Interval"
            lblfrequency.Text = "Frequency"
            lblcreatedby.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATBY")
            lblcreateddate.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
            lblmodifiedby.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFBY")
            lblmodifieddate.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFDATE")
            saveButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Save")
            cancelLink.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Cancel")
            titleLab.Text = "Service Internal Setup - Add"
            AddLink.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Add")
            Me.lblCompTop.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            Me.lblCompBottom.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")

            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            Dim userIDNamestr As String = Session("userID").ToString().ToUpper + "-" + Session("username").ToString.ToUpper
            txtcreatedby.Text = userIDNamestr
            Dim cntryStat As New clsUtil()
            txtcreateddate.Text = Date.Now()
            txtmodifiedby.Text = userIDNamestr
            txtmodifieddate.Text = Date.Now()

            Dim Conn As New SqlConnection
            Conn.ConnectionString = ConfigurationManager.AppSettings("ConnectionString").ToString

            If ddlmodel.SelectedValue = "" Then
                Dim Comm2 As SqlCommand
                Comm2 = Conn.CreateCommand
                Comm2.CommandText = "select mmod_modid from mmod_fil with (nolock) where mmod_ctrid = 'MY' and mmod_stat = 'active'"
                Conn.Open()

                Dim dr2 As SqlDataReader = Comm2.ExecuteReader(CommandBehavior.CloseConnection)

                Dim dt2 As DataTable = New DataTable()
                dt2.Load(dr2)

                Dim counter2 As Integer

                For counter2 = 0 To dt2.Rows.Count - 1
                    ddlmodel.Items.Add(dt2.Rows(counter2).Item("mmod_modid").ToString)
                Next
                Conn.Close()

                ddlmodel.SelectedIndex = 0
            End If

        End If
        Me.txtgroup.Focus()
    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveButton.Click
        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim msginfo As String = objXm.GetLabelName("StatusMessage", "SAVEORNOT")
        Dim msgtitle As String = objXm.GetLabelName("StatusMessage", "SAVETITLE")
        MessageBox1.Confirm(msginfo)
    End Sub

    Protected Sub MessageBox1_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox1.GetMessageBoxResponse
        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then

            Dim _class As New ClsMegaSVCGroupSetup()
            If (Trim(txtgroup.Text) <> "") Then
                _class.Group = txtgroup.Text
            End If
            If (Trim(ddlmodel.SelectedValue) <> "") Then
                _class.Model = ddlmodel.SelectedValue
            End If
            If (Trim(txtmonths.Text) <> "") Then
                _class.Months = txtmonths.Text
            End If
            If (Trim(txtinterval.Text) <> "") Then
                _class.Interval = txtinterval.Text
            End If
            If (Trim(txtfrequency.Text) <> "") Then
                _class.Frequency = txtfrequency.Text
            End If
            If (Trim(txtcreatedby.Text) <> "") Then
                _class.CreatedBy = Session("userID").ToString().ToUpper
            End If
            Dim cntryStat As New clsUtil()
            If (Trim(txtmodifiedby.Text) <> "") Then
                _class.ModifiedBy = Session("userID").ToString().ToUpper
            End If
            'Dim dupCount As Integer = _class.GetDuplicated()
            'If dupCount.Equals(-1) Then
            '    Dim objXm As New clsXml
            '    objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            '    errlab.Text = objXm.GetLabelName("StatusMessage", "DUPCTRYIDORNM")
            '    errlab.Visible = True
            'ElseIf dupCount.Equals(0) Then
            Dim insCtryCnt As Integer = _class.Insert()

                Dim objXmlTr As New clsXml
                objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                If insCtryCnt = 0 Then
                    Dim objXm As New clsXml
                    objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    errlab.Text = objXm.GetLabelName("StatusMessage", "BB-HintMsg-Suc")
                    errlab.Visible = True
                Else
                    Response.Redirect("~/PresentationLayer/Error.aspx")
                End If
            'End If
        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
        End If
    End Sub
End Class
