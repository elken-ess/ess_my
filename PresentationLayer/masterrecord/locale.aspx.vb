Imports BusinessEntity
Imports System.Configuration
Imports System.Data
Partial Class PresentationLayer_masterrecord_locale
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Page.IsPostBack) Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try

            Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
            'display label message
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            Me.areaID.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASAREA-0001")
            areanm.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASAREA-0002")
            areaStat.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCTRY-0004")

            addButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Add")
            searchButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Search")
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BROAREATL")


            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.addinfo.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-AR")
            Me.addinfo.Visible = False
            Me.Label1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-NR")
            Me.Label1.Visible = False
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'create the dropdownlist
            Dim cntryStat As New clsrlconfirminf()
            Dim statParam As ArrayList = New ArrayList
            statParam = cntryStat.searchconfirminf("STATUS")
            Dim count As Integer
            Dim statid As String
            Dim statnm As String
            For count = 0 To statParam.Count - 1
                statid = statParam.Item(count)
                statnm = statParam.Item(count + 1)
                count = count + 1
                If (statid.Equals("ACTIVE") Or statid.Equals("OBSOLETE")) Then
                    ctryStatDrop.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                End If
            Next
            'ctryStatDrop.Items(0).Selected = True

            Dim areaEntity As New clsarea()

            ''''''''''user info'''''''''''''xxl
            areaEntity.ModifyBy = Session("userID")

            '''''''''''''''''''''''''''
            If (Session("userID") <> "ADMIN") Then
                areaEntity.CountryID = Session("login_ctryID").ToString.ToUpper()
            Else
                areaEntity.CountryID = "%"
            End If

            If (ctryStatDrop.SelectedValue().ToString() <> "") Then
                areaEntity.AreaStatus = ctryStatDrop.SelectedItem.Value.ToString()
            End If

            Dim areall As DataSet = areaEntity.GetArea()
            areaView.DataSource = areall
            Session("areaView") = areall

            checknorecord(areall)
            areaView.AllowPaging = True
            areaView.AllowSorting = True

            'Dim editcol As System.Web.UI.WebControls.HyperLinkField = New HyperLinkField()
            'editcol.Text = "edit"
            ''editcol.DataNavigateUrlFormatString = "~/PresentationLayer/masterrecord/modifyarea.aspx?areid={0}&ctrid={1}&staid={2}&arestat={3}"
            'editcol.DataNavigateUrlFormatString = "~/PresentationLayer/masterrecord/modiarea.aspx?areid={0}&ctrid={1}&staid={2}&arestat={3}&modisy=0"
            'editcol.HeaderImageUrl = "~/PresentationLayer/graph/edit.gif"
            'Dim areid As String = areall.Tables.Item(0).Columns(0).ColumnName.ToString()
            'Dim ctrid As String = areall.Tables.Item(0).Columns(4).ColumnName.ToString()
            'Dim staid As String = areall.Tables.Item(0).Columns(5).ColumnName.ToString()
            'Dim arestat As String = areall.Tables.Item(0).Columns(3).ColumnName.ToString()
            'Dim NavigateUrls As Array = Array.CreateInstance(GetType(String), 4)
            'NavigateUrls(0) = areid
            'NavigateUrls(1) = ctrid
            'NavigateUrls(2) = staid
            'NavigateUrls(3) = arestat

            'editcol.DataNavigateUrlFields = NavigateUrls
            'areaView.Columns.Add(editcol)
            Dim editcol As New CommandField
            editcol.EditText = objXmlTr.GetLabelName("EngLabelMsg", "BB-Edit") '"edit"
            editcol.HeaderImageUrl = "~/PresentationLayer/graph/edit.gif"
            editcol.ShowEditButton = True
            areaView.Columns.Add(editcol)

            '''access control'''''''''''''''''''''''''''''''''''''''''''''''
            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "10")
            Me.addButton.Enabled = purviewArray(0)
            'Me.searchButton.Visible = purviewArray(2)
            editcol.Visible = purviewArray(2)
            'Me.areaView.Visible = purviewArray(2)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            areaView.DataBind()
            If (areall.Tables(0).Rows.Count <> 0) Then

                areaView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "AREAHD0")
                areaView.HeaderRow.Cells(1).Font.Size = 8
                areaView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "AREAHD1")
                areaView.HeaderRow.Cells(2).Font.Size = 8
                areaView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "AREAHD2")
                areaView.HeaderRow.Cells(3).Font.Size = 8
                areaView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "AREAHD3")
                areaView.HeaderRow.Cells(4).Font.Size = 8
                areaView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "AREAHD4")
                areaView.HeaderRow.Cells(5).Font.Size = 8
                areaView.HeaderRow.Cells(6).Text = objXmlTr.GetLabelName("EngLabelMsg", "AREAHD5")
                areaView.HeaderRow.Cells(6).Font.Size = 8
                areaView.HeaderRow.Cells(7).Text = objXmlTr.GetLabelName("EngLabelMsg", "AREAHD6")
                areaView.HeaderRow.Cells(7).Font.Size = 8
                'areaView.HeaderRow.Cells(8).Text = objXmlTr.GetLabelName("EngLabelMsg", "AREAHD7")
                'areaView.HeaderRow.Cells(9).Text = objXmlTr.GetLabelName("EngLabelMsg", "AREAHD8")


            End If
        End If
    End Sub
#Region "if have no record "
    Public Function checknorecord(ByVal ds As DataSet) As Integer

        If ds.HasErrors Then
            If ds.Tables(0).Rows.Count = 0 Then
                Me.Label1.Visible = True
                'Me.addinfo.Visible = False
            Else
                Me.Label1.Visible = False

            End If

        End If
        If ds.Tables(0).Rows.Count = 0 Then
            Me.Label1.Visible = True
            'Me.addinfo.Visible = False
        Else
            Me.Label1.Visible = False

        End If


    End Function
#End Region
#Region "if have all record"
    Public Function checkinfo() As Integer
        If Me.areaIDBox.Text = "" And Me.areanmBox.Text = "" Then
            Me.addinfo.Visible = True
        Else
            Me.addinfo.Visible = False
        End If
    End Function
#End Region
    Protected Sub areaView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles areaView.PageIndexChanging
        areaView.PageIndex = e.NewPageIndex
        Dim areaEntity As New clsarea()
        'If (Session("userID") <> "ADMIN") Then
        '    areaEntity.CountryID = Session("login_ctryID").ToString.ToUpper()
        'Else
        '    areaEntity.CountryID = "%"
        'End If
        If (Trim(Me.areaID.Text) <> "") Then
            areaEntity.AreaID = Me.areaIDBox.Text
        End If
        If (Trim(Me.areanm.Text) <> "") Then
            areaEntity.AreaName = Me.areanmBox.Text
        End If
        If (ctryStatDrop.SelectedValue().ToString() <> "") Then
            areaEntity.AreaStatus = ctryStatDrop.SelectedItem.Value.ToString()
        End If

        ''''''''''user info'''''''''''''xxl
        areaEntity.ModifyBy = Session("userID")

        '''''''''''''''''''''''''''
        If (Session("userID") <> "ADMIN") Then
            areaEntity.CountryID = Session("login_ctryID").ToString.ToUpper()
        Else
            areaEntity.CountryID = "%"
        End If

        Dim areall As DataSet = areaEntity.GetArea()
        areaView.DataSource = areall
        areaView.DataBind()
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        If (areall.Tables(0).Rows.Count <> 0) Then
            areaView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "AREAHD0")
            areaView.HeaderRow.Cells(1).Font.Size = 8
            areaView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "AREAHD1")
            areaView.HeaderRow.Cells(2).Font.Size = 8
            areaView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "AREAHD2")
            areaView.HeaderRow.Cells(3).Font.Size = 8
            areaView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "AREAHD3")
            areaView.HeaderRow.Cells(4).Font.Size = 8
            areaView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "AREAHD4")
            areaView.HeaderRow.Cells(5).Font.Size = 8
            areaView.HeaderRow.Cells(6).Text = objXmlTr.GetLabelName("EngLabelMsg", "AREAHD5")
            areaView.HeaderRow.Cells(6).Font.Size = 8
            areaView.HeaderRow.Cells(7).Text = objXmlTr.GetLabelName("EngLabelMsg", "AREAHD6")
            areaView.HeaderRow.Cells(7).Font.Size = 8

        End If
    End Sub

    Protected Sub searchButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles searchButton.Click
        checkinfo()
        areaView.PageIndex = 0
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        Dim areaEntity As New clsarea()

        If (Trim(Me.areaID.Text) <> "") Then
            areaEntity.AreaID = Me.areaIDBox.Text
        End If
        If (Trim(Me.areanm.Text) <> "") Then
            areaEntity.AreaName = Me.areanmBox.Text
        End If
        If (ctryStatDrop.SelectedValue().ToString() <> "") Then
            areaEntity.AreaStatus = ctryStatDrop.SelectedItem.Value.ToString()
        End If

        ''''''''''user info'''''''''''''xxl
        areaEntity.ModifyBy = Session("userID")

        '''''''''''''''''''''''''''
        If (Session("userID") <> "ADMIN") Then
            areaEntity.CountryID = Session("login_ctryID").ToString.ToUpper()
        Else
            areaEntity.CountryID = "%"
        End If
        Dim selDS As DataSet = areaEntity.GetArea()
        areaView.DataSource = selDS
        areaView.DataBind()
        Session("areaView") = selDS
        checknorecord(selDS)

        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")

        If (selDS.Tables(0).Rows.Count <> 0) Then
            areaView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "AREAHD0")
            areaView.HeaderRow.Cells(1).Font.Size = 8
            areaView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "AREAHD1")
            areaView.HeaderRow.Cells(2).Font.Size = 8
            areaView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "AREAHD2")
            areaView.HeaderRow.Cells(3).Font.Size = 8
            areaView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "AREAHD3")
            areaView.HeaderRow.Cells(4).Font.Size = 8
            areaView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "AREAHD4")
            areaView.HeaderRow.Cells(5).Font.Size = 8
            areaView.HeaderRow.Cells(6).Text = objXmlTr.GetLabelName("EngLabelMsg", "AREAHD5")
            areaView.HeaderRow.Cells(6).Font.Size = 8
            areaView.HeaderRow.Cells(7).Text = objXmlTr.GetLabelName("EngLabelMsg", "AREAHD6")
            areaView.HeaderRow.Cells(7).Font.Size = 8
        End If
    End Sub

    Protected Sub areaView_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles areaView.RowEditing
        'Dim ds As DataSet = Session("areaView")
        'Me.areaView.Rows(e.NewEditIndex).Cells(1).Text()
        Dim areid As String = Me.areaView.Rows(e.NewEditIndex).Cells(1).Text()
        areid = Server.UrlEncode(areid)
        Dim ctrid As String = Me.areaView.Rows(e.NewEditIndex).Cells(5).Text()
        ctrid = Server.UrlEncode(ctrid)
        Dim staid As String = Me.areaView.Rows(e.NewEditIndex).Cells(6).Text()
        staid = Server.UrlEncode(staid)
        Dim arestat As String = Me.areaView.Rows(e.NewEditIndex).Cells(4).Text()
        arestat = Server.UrlEncode(arestat)

        Dim i As Integer
        For i = 0 To Me.ctryStatDrop.Items.Count - 1
            If arestat = Me.ctryStatDrop.Items(i).Text Then
                arestat = Me.ctryStatDrop.Items(i).Value
            End If
        Next

        Dim strTempURL As String = "areid=" + areid + "&ctrid=" + ctrid + "&staid=" + staid + "&arestat=" + arestat
        strTempURL = "~/PresentationLayer/masterrecord/modiarea.aspx?" + strTempURL
        Response.Redirect(strTempURL)
    End Sub
End Class
