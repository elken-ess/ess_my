﻿Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Partial Class PresentationLayer_masterrecord_mdyPSW
    Inherits System.Web.UI.Page

    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            Me.saveButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Save")
            Me.cancelLink.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Cancel")
            Me.usenameLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MDFYPSW-01")
            Me.TextBox1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MDFYPSW-02")
            Me.newPSWLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MDFYPSW-03")
            Me.confirmPSWLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MDFYPSW-04")
            Me.Label1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MDFYPSW-02")
            Me.usenameBox.Enabled = True 
            Me.usenameBox.Text = Request.Params("name")
            '  Me.oldPSWBox.Text = Request.Params("psw")

            ' \\\\\\\\\\\\\\\\\\\\\\\\
            Dim statXmlTr As New clsXml
            statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            Me.RequiredFieldValidator1.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "LOGUSRBLNK")
            '  Me.RequiredFieldValidator2.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "LOGPSWBLNK")
            Me.RequiredFieldValidator3.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "MDYNEWPSWBLNK")
            Me.RequiredFieldValidator4.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "MDYCNFMPSWBLNK")
            Me.CompareValidator1.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "CHUPSWCPSW")
            Me.RegularExpressionValidator1.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "PSWLNTH")

            Me.rfvoldpsw.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "LOGPSWBLNK")
            '\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
            ' Added by Ryan Estandarte 29 Oct 2012
            CompareValidator2.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "CHNGPW-01")
        End If
        Me.usenameBox.Focus()
    End Sub
#Region "check old psw "
    Public Function check() As Integer
        Dim logoncls As New clsLogin()
        If (Me.usenameBox.Text <> "") Then
            logoncls.UseID = Me.usenameBox.Text.ToUpper
        End If
        If (Me.TextBox1.Text <> "") Then
            logoncls.PassWord = FormsAuthentication.HashPasswordForStoringInConfigFile(Me.TextBox1.Text, "md5")
        End If
        Dim drpsw As SqlDataReader = Nothing
        drpsw = logoncls.checkNamePSW()
        If (drpsw.Read()) Then
            Return 0
        Else
            Return 1
        End If
    End Function
#End Region

#Region "check old psw "
    Public Function Logincheck() As Integer
        Dim statXmlTr As New clsXml
        statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim logoncls As New clsLogin()
        If (Me.usenameBox.Text <> "") Then
            logoncls.UseID = Me.usenameBox.Text.ToUpper
        End If
        If (Me.TextBox1.Text <> "") Then
            logoncls.PassWord = FormsAuthentication.HashPasswordForStoringInConfigFile(Me.newpsw.Text, "md5")
        End If
        Dim drstat As SqlDataReader = Nothing
        Dim pswreader As SqlDataReader = Nothing
        pswreader = logoncls.checkNamePSW()
        drstat = logoncls.checkNameStat()
        If (pswreader.Read()) Then
            If pswreader("MUSR_USRID").ToString.ToUpper = "ADMIN" Then
                Session("username") = pswreader("MUSR_ENAME").ToString().ToUpper
                Session("userID") = pswreader("MUSR_USRID").ToString().ToUpper
                Session("accessgroup") = pswreader("MUSR_GRPID").ToString()
                Session("login_rank") = pswreader("MUSR_RANK").ToString()
                Session("login_svcID") = pswreader("MUSR_SVCID").ToString()
                logoncls.UseID = pswreader("MUSR_USRID").ToString()

                logoncls.LogTime = Date.Now()
                logoncls.LVTime = Date.Now()
                logoncls.IPAdress = Request.UserHostAddress.ToString()
                logoncls.PCName = System.Net.Dns.GetHostByAddress(Request.UserHostName()).HostName

                logoncls.UpdateSSYA_Add()

                Response.Redirect("~/PresentationLayer/Default.aspx")
            End If
        End If

        If (drstat.Read()) And (drstat("MUSR_STAT").ToString() = "ACTIVE") Then

            Session("username") = drstat("MUSR_ENAME").ToString().ToUpper
            Session("userID") = drstat("MUSR_USRID").ToString().ToUpper
            Session("accessgroup") = drstat("MUSR_GRPID").ToString()

            Session("login_rank") = drstat("MUSR_RANK").ToString()
            Session("login_ctryID") = drstat("MUSR_CTRCD").ToString()
            Session("login_cmpID") = drstat("MUSR_COMCD").ToString
            Session("login_svcID") = drstat("MUSR_SVCID").ToString()

            Session("login_datetime") = Date.Now()
            'modify failed logon value 
            logoncls.UseID = drstat("MUSR_USRID").ToString()
            logoncls.UseFLT = 0
            logoncls.ChangePSW = "N"
            ' Added by Ryan Estandarte 03 Oct 2012
            logoncls.PwStatus = "FLAG"

            logoncls.Update()
            'update ssya table
            logoncls.LogTime = Date.Now()
            logoncls.LVTime = Date.Now()

            logoncls.IPAdress = Request.UserHostAddress.ToString()

            logoncls.PCName = System.Net.Dns.GetHostByAddress(Request.UserHostName()).HostName
            logoncls.UpdateSSYA_Add()
            Response.Redirect("~/PresentationLayer/Default.aspx")
        Else
            Me.addinfo.Text = statXmlTr.GetLabelName("StatusMessage", "LOGUSRSTAT")
            Me.addinfo.Visible = True
        End If

    End Function
#End Region

#Region "save button "
    Protected Sub saveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveButton.Click
        Dim statXmlTr As New clsXml
        statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")

        Dim i As Integer = 0
        i = check()
        If i = 1 Then
            'old psw wrong
            addinfo.Text = statXmlTr.GetLabelName("StatusMessage", "LOGUSIDPSW")
            addinfo.Visible = True
        ElseIf i = 0 Then
            'user id and psw  is correct
            Dim clslogincls As New clsLogin()
            clslogincls.UseID = Me.usenameBox.Text.ToString()
            clslogincls.PassWord = FormsAuthentication.HashPasswordForStoringInConfigFile(Me.newpsw.Text, "md5")
            clslogincls.UseFLT = 0
            clslogincls.ChangePSW = "N"
            ' Added by Ryan Estandarte 3 Oct 2012
            clslogincls.PwStatus = "UPDATE"

            Dim upint As Integer = -1
            upint = clslogincls.Update()
            If (upint = 0) Then

                Logincheck()
                'addinfo.Text = statXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Suc")
                'addinfo.Visible = True
            Else
                Response.Redirect("mdyPSW.aspx")
            End If
        End If


        
    End Sub
#End Region


    Protected Sub cancelLink_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cancelLink.Click
        Response.Redirect("~/PresentationLayer/logon.aspx")
    End Sub


End Class
