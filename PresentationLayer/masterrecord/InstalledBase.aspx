﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="InstalledBase.aspx.vb" Inherits="PresentationLayer_masterrecord_InstalledBase" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Install Based - Search</title>
     <style type="text/css">A:link { COLOR: #336666; TEXT-DECORATION: none }
	BODY { FONT-SIZE: 9pt; FONT-FAMILY: "Arial", "Verdana", "Tahoma"; BACKGROUND-COLOR: #ffffff }
	TD { FONT-SIZE: 9pt; FONT-FAMILY: Arial,Verdana,Tahoma }
	</style>
	<LINK href="../css/style.css" type="text/css" rel="stylesheet">
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table id="TABLE2" border="0" width="100%">
            <tr>
                <td style="width: 100%; height: 13px">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title1.gif" width="5" /></td>
                            <td background="../graph/title_bg.gif" class="style2" width="98%" style="width: 100%">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="right" background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title_2.gif" width="5" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 90%; height: 12px">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" class="style2" style="height: 1px; width: 100%;" width="98%">
                                <font color="red">
                                    <asp:Label ID="addinfo" runat="server" Text="Label" Visible="false"></asp:Label></font></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        
        <table id="country" border="0" cellpadding="2" cellspacing="1">
            <tr>
                <td style="width: 113px">
        <asp:Label ID="cusidlab" runat="server" Text="Label"></asp:Label></td>
                <td style="width: 157px">
        <asp:TextBox ID="custid" runat="server" CssClass="textborder" MaxLength="20" Width="135px"></asp:TextBox></td>
                <td style="width: 100px">
                    <asp:Label ID="arealab" runat="server" Text="Label"></asp:Label></td>
                <td style="width: 135px">
                    <asp:DropDownList ID="DDArea" runat="server" DataTextField="MARE_AREID" DataValueField="MARE_AREID"
                        Width="125px">
                    </asp:DropDownList></td>
                <td style="width: 84px">
        <asp:Label ID="statusLab" runat="server" Text="Label"></asp:Label></td>
                <td style="width: 125px">
        <asp:DropDownList ID="ctrystat" runat="server" CssClass="textborder" Width="114px">
        </asp:DropDownList></td>
            </tr>
            <tr>
                <td style="width: 113px">
        <asp:Label ID="cusnamelab" runat="server" Text="Label"></asp:Label></td>
                <td style="width: 157px">
        <asp:TextBox ID="custname" runat="server" CssClass="textborder" MaxLength="50" Width="136px"></asp:TextBox></td>
                <td style="width: 100px">
                    <asp:Label ID="telelab" runat="server" Text="Label"></asp:Label></td>
                <td style="width: 135px">
                    <asp:TextBox ID="teleno" runat="server" CssClass="textborder" MaxLength="20" Width="119px"></asp:TextBox></td>
                <td style="width: 84px"></td>
                <td style="width: 125px">
                    <asp:TextBox ID="areano" runat="server" CssClass="textborder" MaxLength="20" Width="119px" Visible="False"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
        <asp:Label ID="modelidlab" runat="server" Text="Label"></asp:Label></td>
                <td style="width: 157px">
        <asp:TextBox ID="modelid" runat="server" CssClass="textborder" Width="135px" MaxLength="20"></asp:TextBox></td>
                <td style="width: 100px">
        <asp:Label ID="serialnoLab" runat="server" Text="Label"></asp:Label></td>
                <td style="width: 135px">
        <asp:TextBox ID="serialnumber" runat="server" CssClass="textborder" Width="118px" MaxLength="20"></asp:TextBox></td>
                <td colspan="2">
        <asp:LinkButton ID="LBsearch" runat="server">LinkButton</asp:LinkButton>
                    &nbsp;
        <asp:LinkButton ID="LBadd" runat="server" PostBackUrl="~/PresentationLayer/masterrecord/addinstalleBase.aspx">LinkButton</asp:LinkButton></td>
            </tr>
        </table> <br />
        <hr />
    
    </div>
        <asp:GridView ID="installbaseView" runat="server" Width="100%" AllowPaging="True" Font-Size="Smaller" style="width: 100%" AllowSorting="True">
        </asp:GridView><br />
        <asp:Label ID="Label2" runat="server"></asp:Label> <asp:Label
            ID="LblTotRecNo" runat="server"></asp:Label><br />
        <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="Label" Visible="False"
            Width="216px"></asp:Label>

         
    </form>
</body>
</html>
