Imports System.Data.SqlClient
Imports BusinessEntity
Imports System.Configuration
Imports SQLDataAccess
Imports System.Data




Partial Class PresentationLayer_masterrecord_modifyPriceSetUp


    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try
        Try
            Dim str As String = Request.Params("PriceSetUpID").ToString

        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try

        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle




        If Not Page.IsPostBack Then

            
           
            

            Dim mdfyid As String = Request.Params("PriceSetUpID").ToString()

            Dim returnprisid As Integer
            returnprisid = Request.Params("pricesetupid1")
            'Dim time As String
            'time = Request.Params("time")
           
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            PriceIDLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0001")
            PartIDLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0002")
            CountryIDlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0003")
            CreatedBylab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATBY")
            CreatedDatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
            ModifiedBylab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFBY")
            ModifiedDatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFDATE")
            StatusLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Status")
            saveButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Save")
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0015")
            amuntLabel.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0010")
            cancelLink.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Cancel")
            AddButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0017")
            lblNoteUP.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            lblNoteDown.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")

            Dim objXmlTr1 As New clsXml
            objXmlTr1.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            priceidva.ErrorMessage = objXmlTr1.GetLabelName("StatusMessage", "PRICEID")
            'Dim strPanm As String = objXmlTr1.GetLabelID("StatusMessage", statusid)


            '''access control'''''''''''''''''''''''''''''''''''''''''''''''
            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "14")
            Me.saveButton.Enabled = purviewArray(1)
            Me.AddButton.Enabled = purviewArray(1)
            If purviewArray(2) = False Then
                Response.Redirect("~/PresentationLayer/logon.aspx")

            End If



          
            If (mdfyid <> "") Then
                Dim PriceSetUpEntity As New clsPriceSetUp()
                PriceSetUpEntity.PriceSetUpID = Convert.ToUInt32(mdfyid)
                Dim retnArray As ArrayList = PriceSetUpEntity.GetPriceSetUpIDDetailsByID()

                'create the dropdownlist
                SetUpID.Text = retnArray(0)
                Dim cntryStat As New clsrlconfirminf()
                Dim statParam As ArrayList = New ArrayList
                statParam = cntryStat.searchconfirminf("STATUS")
                Dim count As Integer
                Dim statid As String
                Dim statnm As String
                For count = 0 To statParam.Count - 1
                    statid = statParam.Item(count)
                    statnm = statParam.Item(count + 1)
                    If (statid.Equals("ACTIVE") Or statid.Equals("DELETE") Or statid.Equals("OBSOLETE")) Then
                        StatusDrop.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                    End If
                    If statid.Equals(retnArray(4).ToString()) Then
                        StatusDrop.Items(count / 2).Selected = True
                    End If
                    count = count + 1
                Next
                '''access control'''''''''''''''''''''''''''''''''''''''''''''''

                If purviewArray(4) = False Then
                    Me.StatusDrop.Items.Remove(Me.StatusDrop.Items.FindByValue("DELETE"))

                End If

                Dim userIDNamestr As String = Session("userID").ToString().ToUpper + "-" + Session("username").ToString.ToUpper
                ModifiedBy.Text = userIDNamestr

                Dim Rcreatby As String = retnArray.Item(5).ToString().ToUpper()
                If Rcreatby <> "ADMIN" Then
                    Dim Rcreat As New clsCommonClass
                    Rcreat.userid() = Rcreatby
                    Dim Rcreatds As New DataSet()
                    Rcreatds = Rcreat.Getuseridname("BB_MASUSER_SelByIDName")

                    Me.CreatedBy.Text = Rcreatds.Tables(0).Rows(0).Item(0)
                Else
                    Me.CreatedBy.Text = "ADMIN-ADMIN"
                End If

                CreatedBy.ReadOnly = True
                CreatedDate.ReadOnly = True
               
                CreatedDate.Text = retnArray(6).ToString()
                CreatedDate.ReadOnly = True
                ModifiedDate.Text = retnArray(8).ToString()
                countryidBox.Text = retnArray(3).ToString()
                Dim rank As String = Session("login_rank").ToString().ToUpper()
                Dim priceid As New clsCommonClass
                If rank <> 0 Then
                    priceid.spctr = Session("login_ctryID").ToString().ToUpper()

                End If
                priceid.rank = rank
                Dim priceidds As New DataSet
                priceidds = priceid.Getcomidname("BB_MASPRID_IDNAME")
                If priceidds.Tables.Count <> 0 Then
                    databonds(priceidds, Me.PriceIDDrop)
                    Me.PriceIDDrop.Items.Insert(0, "  ")
                End If
                PriceIDDrop.Text = retnArray(2).ToString()

                partidbox.Text = retnArray(1).ToString()


                'display  detail table

                Dim objXmlTr3 As New clsXml
                objXmlTr3.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
                Dim PriceSetUpEntity3 As New clsPriceSetUp()
                PriceSetUpEntity3.PriceSetUpID = Convert.ToUInt32(mdfyid)

                Dim PriceSetUpall3 As DataSet = PriceSetUpEntity3.GetPriceSetUp1()

                PriceSetUp1View.DataSource = PriceSetUpall3
                Session("PriceSetUp1View") = PriceSetUpall3
                PriceSetUp1View.AllowPaging = True
                'PriceSetUp1View.AllowSorting = True

                Dim editcol As New CommandField
                editcol.EditText = objXmlTr.GetLabelName("EngLabelMsg", "BB-Edit") '"edit"
                editcol.HeaderImageUrl = "~/PresentationLayer/graph/edit.gif"
                editcol.ShowEditButton = True
                PriceSetUp1View.Columns.Add(editcol)
                editcol.Visible = purviewArray(1)

                PriceSetUp1View.DataBind()
                If PriceSetUpall3.Tables(0).Rows.Count <> 0 Then
                    PriceSetUp1View.HeaderRow.Cells(1).Text = objXmlTr3.GetLabelName("EngLabelMsg", "BB-MASPRCE-0014")
                    PriceSetUp1View.HeaderRow.Cells(1).Font.Size = 8
                    PriceSetUp1View.HeaderRow.Cells(2).Text = objXmlTr3.GetLabelName("EngLabelMsg", "BB-MASPRCE-0006")
                    PriceSetUp1View.HeaderRow.Cells(2).Font.Size = 8
                    PriceSetUp1View.HeaderRow.Cells(3).Text = objXmlTr3.GetLabelName("EngLabelMsg", "BB-MASPRCE-0005")
                    PriceSetUp1View.HeaderRow.Cells(3).Font.Size = 8
                    PriceSetUp1View.HeaderRow.Cells(4).Text = objXmlTr3.GetLabelName("EngLabelMsg", "BB-Status")
                    PriceSetUp1View.HeaderRow.Cells(4).Font.Size = 8
                    PriceSetUp1View.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
                    PriceSetUp1View.HeaderRow.Cells(5).Font.Size = 8
                  
                End If
            End If
            'return display modify detail table
            If (returnprisid <> 0) Then
                Dim PriceSetUpEntity4 As New clsPriceSetUp()
                PriceSetUpEntity4.PriceSetUpID = returnprisid

                Dim retnArray4 As ArrayList = PriceSetUpEntity4.GetReturnPriceSetUpIDDetailsByID()
                SetUpID.Text = retnArray4(0)
                Dim cntryStat As New clsrlconfirminf()
                Dim statParam As ArrayList = New ArrayList
                statParam = cntryStat.searchconfirminf("STATUS")
                Dim count As Integer
                Dim statid As String
                Dim statnm As String
                For count = 0 To statParam.Count - 1
                    statid = statParam.Item(count)
                    statnm = statParam.Item(count + 1)
                    If (statid.Equals("ACTIVE") Or statid.Equals("DELETE") Or statid.Equals("OBSOLETE")) Then
                        StatusDrop.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                    End If
                    If statid.Equals(retnArray4(4).ToString()) Then
                        StatusDrop.Items(count / 2).Selected = True
                    End If
                    count = count + 1
                Next
                '''access control'''''''''''''''''''''''''''''''''''''''''''''''

                If purviewArray(4) = False Then
                    Me.StatusDrop.Items.Remove(Me.StatusDrop.Items.FindByValue("DELETE"))

                End If

                Dim userIDNamestr As String = Session("userID").ToString().ToUpper + "-" + Session("username").ToString.ToUpper
                ModifiedBy.Text = userIDNamestr

                Dim Rcreatby As String = retnArray4.Item(5).ToString().ToUpper()
                If Rcreatby <> "ADMIN" Then
                    Dim Rcreat As New clsCommonClass
                    Rcreat.userid() = Rcreatby
                    Dim Rcreatds As New DataSet()
                    Rcreatds = Rcreat.Getuseridname("BB_MASUSER_SelByIDName")

                    Me.CreatedBy.Text = Rcreatds.Tables(0).Rows(0).Item(0)
                Else
                    Me.CreatedBy.Text = "ADMIN-ADMIN"
                End If

                CreatedBy.ReadOnly = True
                CreatedDate.ReadOnly = True
             
                CreatedDate.Text = retnArray4(6).ToString()
                CreatedDate.ReadOnly = True
                ModifiedDate.Text = retnArray4(8).ToString()
                countryidBox.Text = retnArray4(3).ToString()
                partidbox.Text = retnArray4(1).ToString()
                Dim rank As String = Session("login_rank").ToString().ToUpper()
                Dim priceid As New clsCommonClass
                If rank <> 0 Then
                    priceid.spctr = Session("login_ctryID").ToString().ToUpper()

                End If
                priceid.rank = rank
                Dim priceidds As New DataSet
                priceidds = priceid.Getcomidname("BB_MASPRID_IDNAME")
                If priceidds.Tables.Count <> 0 Then
                    databonds(priceidds, Me.PriceIDDrop)
                End If
                PriceIDDrop.Text = retnArray4(2).ToString()
               

                Dim objXmlTr2 As New clsXml
                objXmlTr2.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
                Dim PriceSetUpEntity1 As New clsPriceSetUp()
                PriceSetUpEntity1.PriceSetUpID = returnprisid
                'PriceSetUpEntity1.PriceSetUpStatus = stat
                Dim PriceSetUpall As DataSet = PriceSetUpEntity1.GetPriceSetUp1()

                PriceSetUp1View.DataSource = PriceSetUpall
                Session("PriceSetUp1View") = PriceSetUpall
                PriceSetUp1View.AllowPaging = True
                'PriceSetUp1View.AllowSorting = True
                Dim editcol1 As New CommandField
                editcol1.EditText = objXmlTr.GetLabelName("EngLabelMsg", "BB-Edit") '"edit"
                editcol1.HeaderImageUrl = "~/PresentationLayer/graph/edit.gif"
                editcol1.ShowEditButton = True
                PriceSetUp1View.Columns.Add(editcol1)
                'editcol1.Visible = purviewArray(1)
                PriceSetUp1View.DataBind()
                If PriceSetUpall.Tables(0).Rows.Count <> 0 Then
                    PriceSetUp1View.HeaderRow.Cells(1).Text = objXmlTr2.GetLabelName("EngLabelMsg", "BB-MASPRCE-0014")
                    PriceSetUp1View.HeaderRow.Cells(1).Font.Size = 8
                    PriceSetUp1View.HeaderRow.Cells(2).Text = objXmlTr2.GetLabelName("EngLabelMsg", "BB-MASPRCE-0006")
                    PriceSetUp1View.HeaderRow.Cells(2).Font.Size = 8
                    PriceSetUp1View.HeaderRow.Cells(3).Text = objXmlTr2.GetLabelName("EngLabelMsg", "BB-MASPRCE-0005")
                    PriceSetUp1View.HeaderRow.Cells(3).Font.Size = 8
                    PriceSetUp1View.HeaderRow.Cells(4).Text = objXmlTr2.GetLabelName("EngLabelMsg", "BB-Status")
                    PriceSetUp1View.HeaderRow.Cells(4).Font.Size = 8
                    PriceSetUp1View.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
                    PriceSetUp1View.HeaderRow.Cells(5).Font.Size = 8
                   

                End If

            End If
        End If


    End Sub
#Region " id bond"
    Public Function databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        Dim row As DataRow
        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            NewItem.Text = row("id") & "-" & row("name")
            NewItem.Value = row("id")
            dropdown.Items.Add(NewItem)
        Next

    End Function
#End Region
    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveButton.Click
        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim msginfo As String = objXm.GetLabelName("StatusMessage", "SAVEORNOT")
        Dim msgtitle As String = objXm.GetLabelName("StatusMessage", "SAVETITLE")
        MessageBox1.Confirm(msginfo)
        Dim dsPriceIDall As DataSet = Session("PriceSetUp1View")
        PriceSetUp1View.DataSource = dsPriceIDall
        PriceSetUp1View.AllowPaging = True
        'PriceSetUp1View.AllowSorting = True
        PriceSetUp1View.DataBind()
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        If dsPriceIDall.Tables(0).Rows.Count <> 0 Then
            PriceSetUp1View.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0014")
            PriceSetUp1View.HeaderRow.Cells(1).Font.Size = 8
            PriceSetUp1View.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0006")
            PriceSetUp1View.HeaderRow.Cells(2).Font.Size = 8
            PriceSetUp1View.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0005")
            PriceSetUp1View.HeaderRow.Cells(3).Font.Size = 8
            PriceSetUp1View.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Status")
            PriceSetUp1View.HeaderRow.Cells(4).Font.Size = 8
            PriceSetUp1View.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
            PriceSetUp1View.HeaderRow.Cells(5).Font.Size = 8
           
        End If
      

      

    End Sub
  
   
   

    Protected Sub AddButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles AddButton.Click

       Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        If Me.StatusDrop.SelectedValue <> "ACTIVE" Then
            errlab.Text = objXm.GetLabelName("StatusMessage", "PRICESETUPADDERROR")
            errlab.Visible = True
            Dim dsPriceIDall As DataSet = Session("PriceSetUp1View")
            PriceSetUp1View.DataSource = dsPriceIDall
            PriceSetUp1View.AllowPaging = True
            'PriceSetUp1View.AllowSorting = True
            PriceSetUp1View.DataBind()
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            If dsPriceIDall.Tables(0).Rows.Count <> 0 Then
                PriceSetUp1View.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0014")
                PriceSetUp1View.HeaderRow.Cells(1).Font.Size = 8
                PriceSetUp1View.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0006")
                PriceSetUp1View.HeaderRow.Cells(2).Font.Size = 8
                PriceSetUp1View.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0005")
                PriceSetUp1View.HeaderRow.Cells(3).Font.Size = 8
                PriceSetUp1View.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Status")
                PriceSetUp1View.HeaderRow.Cells(4).Font.Size = 8
                PriceSetUp1View.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
                PriceSetUp1View.HeaderRow.Cells(5).Font.Size = 8
              
            End If
            Return
        End If
       
        Dim url As String = "~/PresentationLayer/masterrecord/addPriceSetUp1.aspx?"
        url &= "pricesetupid=" & Me.SetUpID.Text & "&"
        url &= "priceid=" & PriceIDDrop.SelectedValue().ToString().ToUpper() & "&"
        url &= "parteid=" & partidbox.Text.ToUpper() & "&"
        url &= "ctryid=" & countryidBox.Text.ToUpper() & "&"
        url &= "symbl=" & 1
        Response.Redirect(url)
       
        

      
     
    End Sub

    Protected Sub MessageBox1_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox1.GetMessageBoxResponse
        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
            Dim PriceSetUpEntity As New clsPriceSetUp()
            If (Trim(SetUpID.Text) <> "") Then
                PriceSetUpEntity.PriceSetUpID = SetUpID.Text
            End If
            If (PriceIDDrop.SelectedValue().ToString() <> "") Then
                PriceSetUpEntity.PriceID = PriceIDDrop.SelectedItem.Value.ToString().ToUpper()
            End If
            If (StatusDrop.SelectedItem.Value.ToString() <> "") Then
                PriceSetUpEntity.PriceSetUpStatus = StatusDrop.SelectedItem.Value.ToString()
            End If
            If (Trim(CreatedBy.Text) <> "") Then
                PriceSetUpEntity.CreatedBy = CreatedBy.Text.ToUpper()
            End If
            Dim prcidDate As New clsCommonClass()

          
            If (Trim(Me.ModifiedBy.Text) <> "") Then
                PriceSetUpEntity.ModifiedBy = Session("userID").ToString().ToUpper
            End If
            PriceSetUpEntity.svcid = Session("login_svcID").ToString().ToUpper
            PriceSetUpEntity.ip = Request.UserHostAddress.ToString()


            Dim insPRIDnCnt As Integer = PriceSetUpEntity.GetPriceSetUpIDDetailsByPPPNM()
            If insPRIDnCnt <> 0 Then
                'Response.Redirect("Error.aspx")
                Dim objXm As New clsXml
                objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                errlab.Text = objXm.GetLabelName("StatusMessage", "PRICEPPID")
                errlab.Visible = True
            ElseIf insPRIDnCnt.Equals(0) Then
                Dim updPriceIDCnt As Integer = PriceSetUpEntity.Update()
                Dim objXmlT As New clsXml
                objXmlT.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                If updPriceIDCnt = 0 Then
                    ' Response.Redirect("country.aspx")
                    Dim objXm As New clsXml
                    objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    errlab.Text = objXm.GetLabelName("StatusMessage", "SuccessfullySaved")
                    errlab.Visible = True

                Else
                    Response.Redirect("~/PresentationLayer/Error.aspx")
                End If
                Dim dsPriceIDall As DataSet = Session("PriceSetUp1View")
                PriceSetUp1View.DataSource = dsPriceIDall
                PriceSetUp1View.AllowPaging = True
                'PriceSetUp1View.AllowSorting = True
                PriceSetUp1View.DataBind()
                Dim objXmlTr As New clsXml
                objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
                If dsPriceIDall.Tables(0).Rows.Count <> 0 Then
                    PriceSetUp1View.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0014")
                    PriceSetUp1View.HeaderRow.Cells(1).Font.Size = 8
                    PriceSetUp1View.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0006")
                    PriceSetUp1View.HeaderRow.Cells(2).Font.Size = 8
                    PriceSetUp1View.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0005")
                    PriceSetUp1View.HeaderRow.Cells(3).Font.Size = 8
                    PriceSetUp1View.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Status")
                    PriceSetUp1View.HeaderRow.Cells(4).Font.Size = 8
                    PriceSetUp1View.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
                    PriceSetUp1View.HeaderRow.Cells(5).Font.Size = 8
                  
                End If
            End If
        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
        End If

      
    End Sub

    Protected Sub PriceSetUp1View_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles PriceSetUp1View.PageIndexChanging
        PriceSetUp1View.PageIndex = e.NewPageIndex
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Dim dsPriceIDall As DataSet = Session("PriceSetUp1View")
        PriceSetUp1View.DataSource = dsPriceIDall
        PriceSetUp1View.AllowPaging = True
        'PriceSetUp1View.AllowSorting = True
        PriceSetUp1View.DataBind()
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        If dsPriceIDall.Tables(0).Rows.Count <> 0 Then
            PriceSetUp1View.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0014")
            PriceSetUp1View.HeaderRow.Cells(1).Font.Size = 8
            PriceSetUp1View.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0006")
            PriceSetUp1View.HeaderRow.Cells(2).Font.Size = 8
            PriceSetUp1View.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0005")
            PriceSetUp1View.HeaderRow.Cells(3).Font.Size = 8
            PriceSetUp1View.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Status")
            PriceSetUp1View.HeaderRow.Cells(4).Font.Size = 8
            PriceSetUp1View.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
            PriceSetUp1View.HeaderRow.Cells(5).Font.Size = 8
           
        End If

       
    End Sub

    Protected Sub PriceSetUp1View_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles PriceSetUp1View.RowEditing
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Dim ds As DataSet = Session("PriceSetUp1View")
        Dim page As Integer = PriceSetUp1View.PageIndex * PriceSetUp1View.PageSize
        Dim prcid As String = Me.PriceSetUp1View.Rows(e.NewEditIndex).Cells(1).Text
        prcid = Server.UrlEncode(prcid)
        Dim time As String = Me.PriceSetUp1View.Rows(e.NewEditIndex).Cells(5).Text
        time = Server.UrlEncode(time)
        Dim price As String = PriceIDDrop.SelectedValue().ToString()
        price = Server.UrlEncode(price)
        Dim part As String = partidbox.Text
        part = Server.UrlEncode(part)
        Dim country As String = countryidBox.Text
        country = Server.UrlEncode(country)
        Dim strTempURL As String = "PriceSetUpID=" + prcid + "&time=" + time + "&price=" + price + "&part=" + part + "&country=" + country
        strTempURL = "~/PresentationLayer/masterrecord/modifyPriceSetUp1.aspx?" + strTempURL
        Response.Redirect(strTempURL)
    End Sub
   
   
End Class
