<%@ Page Language="VB" AutoEventWireup="false"  EnableEventValidation ="false"  CodeFile="modifystate.aspx.vb" Inherits="PresentationLayer_masterrecord_modifystate" %>

<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <LINK href="../css/style.css" type="text/css" rel="stylesheet">
</head>
<body>
    <form id="form1" runat="server">
    
   <TABLE border="0" id=statetab style="width: 100%">
   <tr>
                <td style="width: 95%">
                    <table cellspacing="0" cellpadding="0" width="100%" border="0" style="width: 100%">
                        <tr>
                            <td width="1%" background="../graph/title_bg.gif">
                                <img height="24" src="../graph/title1.gif" width="5"></td>
                            <td class="style2" background="../graph/title_bg.gif" style="width: 100%">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="right" background="../graph/title_bg.gif" style="width: 4%">
                                <img height="24" src="../graph/title_2.gif" width="5"></td>
                        </tr>
                    </table>
            </tr>
             <tr>
           <td style="width: 100%">
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>					
							<TD class="style2" background="../graph/title_bg.gif" style="width: 110%">
                                <font color=red style="width: 100%"><asp:Label ID="errlab" runat="server" Text="Label" Visible=false></asp:Label></font></TD>
						</TR>
			 </TABLE>
           </tr>
			<TR>
				<TD style="width: 100%">
					<TABLE id="country" cellSpacing="1" cellPadding="2" bgColor="#b7e6e6" border="0" style="width: 100%">
					<tr bgcolor="#ffffff">
<td colspan = 4>
                            <asp:Label ID="lblNoteUP" runat="server" ForeColor="Red" Text="Label" Visible="True"></asp:Label>
</td>
             </tr>
             <tr bgcolor="#ffffff">
<td colspan = 4>
                        	&nbsp;
                     	</td>
               </tr>

									  <TR bgColor="#ffffff">
											 
											 
											 <TD  align=right style="height: 22px; width: 15%;">
                                                &nbsp;<asp:Label ID="ctryidlab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left width="85%" colspan=3 style="height: 22px">
                                                <asp:TextBox ID="ctridbox" runat="server" CssClass="textborder" style="width: 36%"></asp:TextBox></TD>
										</TR>
										<TR bgColor="#ffffff">
											<TD  align=right style="height: 22px; width: 15%;">
                                                <asp:Label ID="stateidlab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left  style="height: 31px; width: 35%;">
                                                <asp:TextBox ID="stateidbox" runat="server" CssClass="textborder" style="width: 90%"></asp:TextBox></TD>
										<TD  align=right width="15%" style="height: 22px">
                                                <asp:Label ID="statenmlab" runat="server" Text="Label"></asp:Label>
                                                </TD>
											<TD align=left style="height:22px; width: 79%;">
                                                <asp:TextBox  ID="statenmbox" runat="server" CssClass="textborder" style="width: 90%" MaxLength="50"></asp:TextBox>
                                                <asp:Label ID="Label2" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                </TD>
										</TR>
										<TR bgColor="#ffffff">
											<TD  align=right style="height: 22px; width: 15%;" >
                                                <asp:Label ID="alterlab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left width="35%"colspan=3 style="height: 30px">
                                                <asp:TextBox ID="statealtbox" runat="server" CssClass="textborder" MaxLength="50" style="width: 36%"></asp:TextBox><font color=red style="width: 28%; height: 20px"></font></TD>
										
											 
										</TR>
										<TR bgColor="#ffffff">
											<TD align=right style="width: 15%">
                                                <asp:Label ID="taxidLab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%"><asp:DropDownList ID="taxid" runat="server" CssClass="textborder" style="width: 90%">
                                            </asp:DropDownList></TD>
											
											<TD align=right width="15%">
                                                <asp:Label ID="statusLab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 79%">
                                                <asp:DropDownList ID="ctrystat" runat="server" CssClass="textborder" style="width: 90%">
                                                </asp:DropDownList></TD>
										</TR>
										<TR bgColor="#ffffff">
											<TD align=right style="width: 15%">
                                                <asp:Label ID="creatby" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%">
                                                <asp:TextBox ID="creatbybox" runat="server" CssClass="textborder" ReadOnly=true style="width: 90%"></asp:TextBox></TD>
											<TD align=right width="15%">
                                                <asp:Label ID="creatdate" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 79%">
                                                <asp:TextBox ID="creatdtbox" runat="server" CssClass="textborder" ReadOnly=true style="width: 90%"></asp:TextBox></TD>
										</TR>
										<TR bgColor="#ffffff">
											<TD align=right style="width: 15%">
                                                <asp:Label ID="modfyby" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%">
                                                <asp:TextBox ID="modfybybox" runat="server" CssClass="textborder" ReadOnly=true style="width: 90%"></asp:TextBox></TD>
											<TD align=right width="15%">
                                                <asp:Label ID="mdfydate" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 79%">
                                                <asp:TextBox ID="mdfydtbox" runat="server" CssClass="textborder" ReadOnly=true style="width: 90%"></asp:TextBox></TD>
										</TR>
										             <tr bgcolor="#ffffff">
<td colspan = 4>
                        	&nbsp;
                     	</td>
               </tr>

										<tr bgcolor="#ffffff">
<td colspan = 4>
                            <asp:Label ID="lblNoteDown" runat="server" ForeColor="Red" Text="Label" Visible="True"></asp:Label>
</td>
             </tr>

										 
								</TABLE>
						</TD>
					</TR>
					<TR>
						<TD style="width: 100%;">
							<TABLE id="Table1" cellSpacing="1" cellPadding="1"  border="0">
									<TR>
										<TD style="WIDTH: 20%" align=center>
                                            &nbsp;<asp:LinkButton ID="saveButton" runat="server"></asp:LinkButton></TD>
										<td align=center>
                                            <asp:HyperLink ID="cancelLink" runat="server" NavigateUrl="~/PresentationLayer/masterrecord/state.aspx">[HyperLink1]</asp:HyperLink></td>
									</TR>
							</TABLE>
						</TD>
					</TR>
			</TABLE>
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
            ShowSummary="False" />
                                                <asp:RequiredFieldValidator ID="stanmerr" runat="server" ControlToValidate="statenmbox" ForeColor="White"></asp:RequiredFieldValidator>
        <cc1:messagebox id="MessageBox1" runat="server"></cc1:messagebox>
    </form>
</body>
</html>
