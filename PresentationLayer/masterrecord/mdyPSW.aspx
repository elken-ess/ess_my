﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="mdyPSW.aspx.vb" Inherits="PresentationLayer_masterrecord_mdyPSW" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Change Password </title>
     <STYLE type="text/css">A:link { COLOR: #336666; TEXT-DECORATION: none }
	BODY { FONT-SIZE: 9pt; FONT-FAMILY: "Arial", "Verdana", "Tahoma"; BACKGROUND-COLOR: #ffffff }
	TD { FONT-SIZE: 9pt; FONT-FAMILY: Arial,Verdana,Tahoma }
	</STYLE>
	<LINK href="../css/style.css" type="text/css" rel="stylesheet">
</head>
<body  >
    <form id="form1" runat="server">
     <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>					
							<TD class="style2" width="98%" background="../layout/images/title_bg.gif" style="height: 1px">
                                <font color=red><asp:Label ID="addinfo" runat="server" Text="Label" Visible=false></asp:Label></font></TD>
						</TR>
						</TABLE>
						<table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="../layout/images/top1.jpg" width="760" height="77"></td>
  </tr>
  <tr>
    <td><img src="../layout/images/banner.jpg" width="760" height="179"></td>
  </tr>
</table>
<table width="760" border="0" cellspacing="0" cellpadding="0" align =center >
      <tr>
        <td colspan="4"><img src="../layout/images/box_top.jpg" width="760" height="47"></td>
        </tr>
      <tr>
        <td background="../layout/images/left_01.jpg" style="height: 39px; width: 354px;">&nbsp;</td>
        <td style="height: 39px; width: 106px;"><font color="#1e415e" size="2" face="Arial, Helvetica, sans-serif"><strong><asp:Label ID="usenameLab" runat="server" Text="Label"></asp:Label>:</strong></font></td>
        <td style="width: 130px; height: 39px;">
                                    <span style="color: #ff0000">
                                    <asp:TextBox ID="usenameBox" runat="server" Width="141px" MaxLength="20" CssClass="textborder"></asp:TextBox>
                                        *</span></td>
        <td width="217" rowspan="2" align="right" valign="top" style="text-align: left; vertical-align: bottom;">
            &nbsp;&nbsp;
                        &nbsp;&nbsp;</td>
      </tr>
      <tr>
        <td align="left" valign="top" background="../layout/images/left_02.jpg" style="height: 6px; width: 354px;">&nbsp;</td>
        <td style="height: 33px; width: 106px;"><font color="#1e415e" size="2" face="Arial, Helvetica, sans-serif"><strong>
            <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label><strong><span style="font-size: 10pt;
                color: #1e415e; font-family: Arial">:</span></strong></td>
        <td style="width: 130px; height: 6px">
                                    <span
                                        style="color: #ff0000">
                                    <asp:TextBox ID="TextBox1" runat="server" TextMode="Password" Width="141px" MaxLength="12" CssClass="textborder"></asp:TextBox>
                                        *</span>
        </td>
      </tr>
    <tr>
        <td align="left" style="width: 354px; height: 12px" valign="top">
        </td>         
           <td style="height: 12px; width: 106px;"><font color="#1e415e" size="2" face="Arial, Helvetica, sans-serif"><strong><asp:Label ID="newPSWLab" runat="server" Text="Label"></asp:Label>:</td>
        <td style="width: 130px; height: 12px">
            <asp:TextBox ID="newpsw" runat="server" MaxLength="12" TextMode="Password" Width="141px" CssClass="textborder"></asp:TextBox>
            <span
                style="color: #ff0000">*</span>
                <%--Added by Ryan Estandarte 29 Oct 2012--%>
            <asp:CompareValidator ID="CompareValidator2" 
                                  runat="server" 
                                  ErrorMessage=""
                                  Text="*"
                                  ForeColor="white"
                                  ControlToValidate="newpsw"
                                  ControlToCompare="TextBox1"
                                  Display="Dynamic" 
                                  Operator="NotEqual" />
        </td>
        <td align="left" rowspan="1" style="vertical-align: bottom; height: 12px" valign="top"
            width="217">
            
            
            </td>
    </tr>
        <tr>
            <td align="left"   style="height: 33px; width: 354px;" valign="top">
            </td>
            
          <td style="height: 33px; width: 106px;"><font color="#1e415e" size="2" face="Arial, Helvetica, sans-serif"><strong><asp:Label ID="confirmPSWLab" runat="server" Text="Label"></asp:Label></td>
            <td style="width: 130px; height: 33px">
            <asp:TextBox ID="cofirmpsw" runat="server" TextMode="Password" Width="139px" MaxLength="12" CssClass="textborder"></asp:TextBox>
                <span style="color: #ff0000">*</span></td>
            <td align="left" rowspan="1" valign="top" width="217" style="height: 33px; vertical-align: bottom;">                        
            <asp:LinkButton ID="saveButton" runat="server">LinkButton</asp:LinkButton>&nbsp;
                <asp:LinkButton ID="cancelLink" runat="server" CausesValidation="False" style="text-align: left; vertical-align: bottom;">LinkButton</asp:LinkButton>            
                        
                        </td>
        </tr>
      <tr align="left" valign="top">
        <td colspan="4" style="height: 25px"><table width="760" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td style="height: 26px"><img src="../layout/images/left_03.jpg" width="760" height="26"></td>
          </tr>
          <tr>
            <td align="center" valign="top" style="height: 15px"><font size="1" face="Arial, Helvetica, sans-serif">@Copyright
                  By ELKEN Sdn. Bhd. 2006. All Right Reserved&nbsp; </font></td>
          </tr>
        </table></td>
      </tr>
    </table>
        &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
            ShowSummary="False" />
        &nbsp;&nbsp;
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="usenameBox"
            ErrorMessage="RequiredFieldValidator" ForeColor="White">*</asp:RequiredFieldValidator>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="newpsw"
                                        ErrorMessage="RequiredFieldValidator" Width="15px" ForeColor="White">*</asp:RequiredFieldValidator>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="cofirmpsw"
                                        ErrorMessage="RequiredFieldValidator" ForeColor="White">*</asp:RequiredFieldValidator>
        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="newpsw"
            ControlToValidate="cofirmpsw" ErrorMessage="CompareValidator" ForeColor="White">*</asp:CompareValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="newpsw"
            ErrorMessage="RegularExpressionValidator" ForeColor="White" ValidationExpression="\w{6,10}">*</asp:RegularExpressionValidator>
        <asp:RequiredFieldValidator ID="rfvoldpsw" runat="server" ControlToValidate="TextBox1"
            ErrorMessage="RequiredFieldValidator" ForeColor="White">.</asp:RequiredFieldValidator>

    </form>
</body>
</html>
