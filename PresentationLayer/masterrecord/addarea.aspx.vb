Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Partial Class PresentationLayer_masterrecord_addarea
    Inherits System.Web.UI.Page
    Protected Sub add_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles addButton.Click

        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim msginfo As String = objXm.GetLabelName("StatusMessage", "ADDPOST")
        Dim msgtitle As String = objXm.GetLabelName("StatusMessage", "SAVETITLE")
        Dim msgAddPoCode As String = objXm.GetLabelName("StatusMessage", "AREACODE")

        If Me.ctrystat.SelectedValue <> "ACTIVE" Then
            errlab.Text = objXm.GetLabelName("StatusMessage", "DELPO")
            errlab.Visible = True
            Return
        End If

        If Me.PObox.Text.Trim <> "" Then
            MessageBox1.Confirm(msginfo)
        Else
            MessageBox1.Alert(msgAddPoCode)
        End If
    End Sub
#Region "add detail  record"
    Public Sub saverecord()
        Dim flag As Boolean = True
        Dim objXmlTr As New clsXml
        Dim ds As New DataSet()
        Dim dt As New DataTable("POTABLE")
        Dim dr As DataRow
        Dim cl1 As New DataColumn("NO", GetType(Integer))
        dt.Columns.Add(cl1)
        Dim cl2 As New DataColumn("PO", GetType(String))
        dt.Columns.Add(cl2)
        Dim cl3 As New DataColumn("stat", GetType(String))
        dt.Columns.Add(cl3)

        Dim i As Integer
        For i = 0 To POView.Rows.Count - 1
            dr = dt.NewRow()
            dr.Item(0) = i + 1
            dr.Item(1) = POView.Rows(i).Cells(1).Text.ToString()
            dr.Item(2) = POView.Rows(i).Cells(2).Text.ToString()
            If dr.Item(1) = Me.PObox.Text Then
                flag = False
            End If
            dt.Rows.Add(dr)
        Next


        If (Me.PObox.Text <> "" And flag = True) Then
            dr = dt.NewRow()
            dr("NO") = dt.Rows.Count + 1
            dr("PO") = Me.PObox.Text

            Me.PObox.Text = ""

            Dim statXmlTr As New clsXml
            Dim strPanm As String
            statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            Dim statusid As String = Me.POstat.SelectedItem.Value()
            strPanm = statXmlTr.GetLabelName("StatusMessage", statusid)
            dr("stat") = strPanm
            dt.Rows.Add(dr)
        End If

        Dim stateEntity As New clsState()
        Dim stall As DataSet = stateEntity.GetState()
        ds.Tables.Add(dt)
        POView.DataSource = ds
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        POView.DataBind()

        If ds.Tables(0).Rows.Count >= 1 Then
            POView.HeaderRow.Cells(0).Text = objXmlTr.GetLabelName("EngLabelMsg", "POHD0")
            POView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "POHD2")
            POView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "POHD3")

        End If
    End Sub
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try


            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "10")
            Me.saveButton.Enabled = purviewArray(0)
            Me.addButton.Enabled = purviewArray(0)
            If purviewArray(0) = False Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If


            'display label message
            Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
            Dim rank As String = Session("login_rank")
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            ctryidlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCTRY-0001")
            Me.staidLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTATE-0001")
            Me.areaidLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASAREA-0001")
            Me.areanmlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASAREA-0002")
            Me.alterLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCTRY-0003")
            Me.serviceidlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASAREA-0005")
            Me.POlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASAREA-0004")
            statusLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCTRY-0004")

            creatby.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATBY")
            creatdate.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
            modfyby.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFBY")
            mdfydate.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFDATE")

            saveButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Save")
            cancelLink.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Cancel")
            addButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Add")

            AddLink.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Add")
            AddLink.NavigateUrl = Page.Request.RawUrl

            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "ADDAREATL")
            Me.potitle.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASAREA-0004")
            Me.lblCompTop.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            Me.lblCompBottom.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")

            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            Me.ctryiderr.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "MAS-COUNTRY")
            Me.staiderr.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "MAS-STAT")
            Me.areiderr.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "AREIDER")
            Me.arenmerr.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "ARENMER")
            Me.poerr.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "AREACODE")
            'Me.altererr.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "AREALTER")


            Dim userIDNamestr As String = Session("userID").ToString().ToUpper + "-" + Session("username").ToString.ToUpper
            creatbybox.Text = userIDNamestr
            creatdtbox.Text = Date.Now()
            modfybybox.Text = userIDNamestr
            mdfydtbox.Text = Date.Now()

            'create the dropdownlist
            Dim cntryStat As New clsrlconfirminf()
            Dim statParam As ArrayList = New ArrayList
            statParam = cntryStat.searchconfirminf("STATUS")
            Dim count As Integer
            Dim statid As String
            Dim statnm As String
            For count = 0 To statParam.Count - 1
                statid = statParam.Item(count)
                statnm = statParam.Item(count + 1)
                count = count + 1
                If (statid.Equals("ACTIVE") Or statid.Equals("OBSOLETE")) Then
                    Me.ctrystat.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                End If

                If statid.Equals("ACTIVE") Then
                    POstat.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                End If


            Next
            ctrystat.Items(0).Selected = True
            POstat.Items(0).Selected = True
            ''bind ctryid
            ''create  ctryid the dropdownlist
            'Dim ctry As New clsCommonClass
            'Dim ctryds As New DataSet

            'ctryds = ctry.Getidname("BB_MASCTRY_IDNAME")
            'If ctryds.Tables.Count <> 0 Then
            '    databonds(ctryds, Me.ctryid)



            'End If

            'Dim state As New clsCommonClass

            'If (Me.ctryid.SelectedValue().ToString() <> "") Then
            '    state.spctr = Me.ctryid.SelectedValue().ToString().ToUpper()
            'End If
            ''Dim state As New clsCommonClass
            'Dim stateds As New DataSet

            'stateds = state.Getcomidname("BB_MASSTAT_IDNAME")
            'If stateds.Tables.Count <> 0 Then
            '    databonds(stateds, Me.staid)
            'End If

            ''Dim stat As New clsCommonClass
            ''Dim statds As New DataSet
            ''statds = stat.Getidname("BB_MASSTA_IDNAME")
            ''If statds.Tables(0).Rows.Count <> 0 Then

            ''    databonds(statds, Me.staid)
            ''End If

            'create  country the dropdownlist
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim country As New clsCommonClass

            If rank <> 0 Then
                country.spctr = Session("login_ctryID").ToString().ToUpper

            End If
            country.rank = rank

            Dim countryds As New DataSet


            countryds = country.Getcomidname("BB_MASCTRY_IDNAME")
            If countryds.Tables.Count <> 0 Then
                databonds(countryds, Me.ctryid)
            End If
            Me.ctryid.Items.Insert(0, "")
            ctryid.SelectedValue = Session("login_ctryID").ToString().ToUpper



            ''create  state the dropdownlist
            Dim stat As New clsCommonClass
            If (ctryid.SelectedValue().ToString() <> "") Then
                stat.spctr = Me.ctryid.SelectedValue().ToString().ToUpper()
            End If

            stat.rank = rank
            Dim stateds As New DataSet

            stateds = stat.Getcomidname("BB_MASSTAT_IDNAME")
            If stateds.Tables.Count <> 0 Then
                databonds(stateds, Me.staid)
            End If
            Me.staid.Items.Insert(0, "")

            Dim serv As New clsCommonClass
            Dim servds As New DataSet
            Dim rank1 As Integer = Session("login_rank")
            serv.rank = rank
            If rank <> 0 Then
                serv.spctr = Session("login_ctryID")
                serv.spstat = Session("login_cmpID")
                serv.sparea = Session("login_svcID")
            Else
                If (Me.ctryid.Text <> "") Then
                    serv.spctr = Me.ctryid.SelectedValue().ToString().ToUpper()

                End If
            End If

            servds = serv.Getcomidname("BB_MASSVRC_IDNAME")

            If servds.Tables(0).Rows.Count <> 0 Then

                databonds(servds, Me.serviceid)

            End If
            Me.serviceid.Items.Insert(0, "")

            serviceid.SelectedValue = Session("login_svcID")
            ''''''''''''' ''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Me.areaidbox.Focus()
        End If

    End Sub
#Region " id bond"
    Public Function databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        Dim row As DataRow
        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            NewItem.Text = row("id") & "-" & row("name")
            NewItem.Value = row("id")
            dropdown.Items.Add(NewItem)
        Next

    End Function
#End Region

    Protected Sub saveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveButton.Click
        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim msginfo As String = objXm.GetLabelName("StatusMessage", "SAVEORNOT")
        Dim msgtitle As String = objXm.GetLabelName("StatusMessage", "SAVE_TITLE")
        If Me.PObox.Text.Trim <> "" Then
            MessageBox1.Confirm(msgtitle)

        End If
        MessageBox2.Confirm(msginfo)
    End Sub
#Region "add basic record "
    Public Sub savebasicrcd()
        Me.addButton.Enabled = False
        Dim areaEntity As New clsarea()
        If (Trim(Me.areaidbox.Text) <> "") Then
            areaEntity.AreaID = areaidbox.Text.ToUpper()
        End If
        If (Trim(Me.areanm.Text) <> "") Then
            areaEntity.AreaName = areanm.Text.ToUpper()
        End If
        If (Trim(Me.alter.Text) <> "") Then
            areaEntity.AreaAlternateName = alter.Text.ToUpper()
        End If

        If (ctrystat.SelectedItem.Value.ToString() <> "") Then
            areaEntity.AreaStatus = ctrystat.SelectedItem.Value
        End If

        If (Me.ctryid.SelectedItem.Value.ToString() <> "") Then
            areaEntity.CountryID = ctryid.SelectedValue.ToString().ToUpper()
        End If

        If (Me.staid.SelectedValue.ToString() <> "") Then
            areaEntity.StateID = Me.staid.SelectedValue.ToString().ToUpper()
        End If


        If (Me.serviceid.SelectedValue.ToString() <> "") Then
            areaEntity.ServiceID = Me.serviceid.SelectedValue.ToString().ToUpper()
        End If

        If (Trim(creatbybox.Text) <> "") Then
            areaEntity.CreateBy = Session("userID").ToString.ToUpper()
        End If
        'If (Trim(creatdtbox.Text) <> "") Then
        '    stateEntity.CreateDate = creatdtbox.Text.ToUpper()
        'End If
        If (Trim(modfybybox.Text) <> "") Then
            areaEntity.ModifyBy = Session("userID").ToString.ToUpper()
        End If
        'If (Trim(mdfydtbox.Text) <> "") Then
        '    stateEntity.ModifyDate = mdfydtbox.Text.ToUpper()

        'End If
        'Dim j As Integer
        'For j = 0 To POView.Rows.Count

        'Next

        Dim dupCount As Integer = areaEntity.GetDuplicatedArea()
        'If selvalue.Read() <> 0 Then
        '    Response.Redirect("~/PresentationLayer/Error.aspx")
        'End If
        Dim objXm As New clsXml
        If dupCount.Equals(-1) Then

            objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            errlab.Text = objXm.GetLabelName("StatusMessage", "DUPAREAIDORNM")
            errlab.Visible = True
        ElseIf dupCount.Equals(0) Then


            Dim insStatCnt As Integer = areaEntity.Insert()
            Dim insPOCnt As Integer
            Dim n As Integer
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            For n = 0 To POView.Rows.Count - 1
                areaEntity.POID = POView.Rows(n).Cells(1).Text.ToString()
                Dim postat As String = POView.Rows(n).Cells(2).Text.ToString()
                'Dim strPanm As String = objXmlTr.GetLabelID("StatusMessage", postat)
                areaEntity.POStatus = "ACTIVE"
                insPOCnt = areaEntity.Insert_po()
                If (insPOCnt <> 0) Then
                    Response.Redirect("~/PresentationLayer/Error.aspx")
                Else
                    Me.areaidbox.ReadOnly = True
                End If

            Next


            If (insStatCnt = 0) Then
                errlab.Text = objXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Suc")
                errlab.Visible = True
                'Response.Redirect("locale.aspx")
            Else

                Response.Redirect("~/PresentationLayer/Error.aspx")
            End If
        End If
    End Sub
#End Region


    Protected Sub ctryid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ctryid.SelectedIndexChanged
        'Dim state As New clsCommonClass

        'If (Me.ctryid.SelectedValue().ToString() <> "") Then
        '    state.spctr = Me.ctryid.SelectedValue().ToString().ToUpper()
        'End If
        ''Dim state As New clsCommonClass
        'Dim stateds As New DataSet

        'stateds = state.Getcomidname("BB_MASSTAT_IDNAME")
        'If stateds.Tables.Count <> 0 Then
        '    databonds(stateds, Me.staid)
        'End If        
        If Me.ctryid.Text <> "" Then
            Dim rank As String = Session("login_rank")
            Dim state As New clsCommonClass

            If (Me.ctryid.SelectedValue().ToString() <> "") Then
                state.spctr = Me.ctryid.SelectedValue().ToString().ToUpper()

            End If
            state.rank = rank

            'Dim state As New clsCommonClass
            Dim stateds As New DataSet

            stateds = state.Getcomidname("BB_MASSTAT_IDNAME")
            If stateds.Tables.Count <> 0 Then
                databonds(stateds, Me.staid)
            End If

            Me.staid.Items.Insert(0, "")

            'Dim serv As New clsCommonClass

            'If rank <> 0 Then
            '    serv.spctr = Session("login_ctryID")
            '    serv.spstat = Session("login_cmpID")
            '    serv.sparea = Session("login_svcID")
            'Else
            Dim service As New clsCommonClass
            If rank = 7 Then
                service.spctr = Me.ctryid.SelectedValue().ToString().ToUpper()

            End If
            If rank = 8 Then
                service.spctr = Me.ctryid.SelectedValue().ToString().ToUpper()
                service.spstat = Session("login_cmpID")
            End If
            If rank = 9 Then
                service.spctr = Me.ctryid.SelectedValue().ToString().ToUpper()
                service.spstat = Session("login_cmpID")
                service.sparea = Session("login_svcID")
            End If
            If rank = 0 Then
                service.spctr = Me.ctryid.SelectedValue().ToString().ToUpper()
            End If
            service.rank = rank
            Dim serviceds As New DataSet

            serviceds = service.Getcomidname("BB_MASSVRC_IDNAME")
            If serviceds.Tables.Count <> 0 Then
                databonds(serviceds, Me.serviceid)
            End If
            Me.serviceid.Items.Insert(0, "")



            'serv.spctr = Me.ctryid.SelectedValue().ToString().ToUpper()
            ''End If

            'serv.rank = rank
            'Dim serviceds As New DataSet

            'serviceds = serv.Getcomidname("BB_MASSVRC_IDNAME")
            'If serviceds.Tables.Count <> 0 Then
            '    databonds(serviceds, Me.serviceid)
            'End If
            'Me.serviceid.Items.Insert(0, "")
        Else
            Me.serviceid.Items.Clear()
            Me.staid.Items.Clear()
            Me.staid.Items.Add(New ListItem("", ""))
            Me.serviceid.Items.Add(New ListItem("", ""))
        End If
    End Sub

    Protected Sub MessageBox1_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox1.GetMessageBoxResponse
        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
            saverecord()
        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
        End If
    End Sub

    Protected Sub MessageBox2_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox2.GetMessageBoxResponse
        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
            savebasicrcd()
        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
        End If
    End Sub
End Class
 