Imports BusinessEntity
Imports System.Configuration
Imports System.Xml
Imports SQLDataAccess
Imports System.Data.SqlClient
Imports System.Web
Imports System.Data

Partial Class PresentationLayer_masterrecord_addServiceCente
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try

        HypCal.NavigateUrl = "javascript:DoCal(document.ServiceCenter.EffectiveDate);"
        BindGrid()
    End Sub
    Protected Sub BindGrid()
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        If Not Page.IsPostBack Then


            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "07")
            If purviewArray(0) = False Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)

                Return
            End If

            Me.LinkButton1.Enabled = purviewArray(0)
            'errlab.Visible = False
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "ADDSERVICE")
            status.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCTRY-0004")
            ServiceCenterNamelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0002")
            ServiceCenterIDlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0001")
            PersonInChargelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0003")
            BranchTypelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0004")
            AlternateNamelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0005")
            Address1lab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0006")
            Address2lab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0007")
            POCodelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0008")
            AreaIDlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0009")
            CountryIDlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCTRY-0001")
            StateIDlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0011")
            Telephone1lab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0012")
            Telephone2lab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0013")
            picmobilelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0014")
            Faxlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0015")
            EffectiveDatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0017")
            JobsPeDaylab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0018")
            OTPremiumJobslab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0019")
            CompanyIDlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0020")
            CreatedBylab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATBY")
            CreatedDatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
            ModiBylab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFBY")
            ModifiedDatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFDATE")
            LinkButton1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Save")
            HyperLink1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Cancel")
            AddLink.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Add")
            AddLink.NavigateUrl = Page.Request.RawUrl

            Me.lblCompTop.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            Me.lblCompBottom.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")

            Dim statXmlTr As New clsXml
            statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")

            Me.ServiceCenterNamemsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-SVCname")
            Me.AlternateNamemsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-AlterName")
            'Me.PersonInChargemsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-PIC")
            Me.EffectiveDatemsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-EDate")
            Me.JPDaymsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-JPDay")
            Me.OTPmsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-OTPJ")
            Me.CompanyIDmsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "MAS-COMP")
            Me.countrymsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "MAS-COUNTRY")
            Me.statmsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "MAS-STAT")
            Me.areamsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "MAS-AREA")
            Me.ServiceCenterIDMsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-SVCid")
            Me.POCodeMsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-POCodein")
            Me.Telephone1Msg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Tele")
            Me.Telephone2Msg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Tele")
            Me.picmobileMsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Mobile")
            Me.FaxMsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Fax")
            Me.JobsPerDayMsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-JobsPerDay")
            Me.OTPremiumJobsMsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-OTPremiumJobs")
            Me.BranchTypemsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "BRANCHTMSG")

            Dim rank As String = Session("login_rank")
            Dim userIDNamestr As String = Session("userID").ToString().ToUpper + "-" + Session("username").ToString.ToUpper

            Me.CreatedBy.Text = userIDNamestr
            Me.CreatedDate.Text = Date.Now()
            Me.ModiBy.Text = userIDNamestr
            Me.ModifiedDate.Text = Date.Now()

            Me.CreatedBy.ReadOnly = True
            Me.CreatedDate.ReadOnly = True
            Me.ModiBy.ReadOnly = True
            Me.ModifiedDate.ReadOnly = True

            Dim service As New clsServiceCente()
            service.username = userIDNamestr
            'create  the dropdownlist
            'create the dropdownlist

            Dim cntryStat As New clsrlconfirminf()

            Dim statParam As ArrayList = New ArrayList
            statParam = cntryStat.searchconfirminf("STATUS")
            Dim count As Integer
            Dim statid As String
            Dim statnm As String
            For count = 0 To statParam.Count - 1
                statid = statParam.Item(count)
                statnm = statParam.Item(count + 1)
                count = count + 1
                If statid.Equals("ACTIVE") Or statid.Equals("OBSOLETE") Then
                    Me.ctrystat.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                End If
            Next
            'ctrystat.Items(0).Selected = True

            'create  BRANCHTYPE the dropdownlist
            Dim branchtype As New clsrlconfirminf()
            Dim branParam As ArrayList = New ArrayList
            branParam = branchtype.searchconfirminf("BRANCHTYPE")
            Dim brancount As Integer
            Dim branid As String
            Dim brantnm As String
            For brancount = 0 To branParam.Count - 1
                branid = branParam.Item(brancount)
                brantnm = branParam.Item(brancount + 1)
                brancount = brancount + 1
                Me.BranchType.Items.Add(New ListItem(brantnm.ToString(), branid.ToString()))
            Next


            'create  country the dropdownlist
            Dim country As New clsCommonClass

            If rank <> 0 Then
                country.spctr = Session("login_ctryID").ToString().ToUpper

            End If
            country.rank = rank

            Dim countryds As New DataSet


            countryds = country.Getcomidname("BB_MASCTRY_IDNAME")
            If countryds.Tables.Count <> 0 Then
                databonds(countryds, Me.CountryID)
            End If
            Me.CountryID.Items.Insert(0, "")

            CountryID.SelectedIndex = 1



            'create  state the dropdownlist
            Dim stat As New clsCommonClass
            If (CountryID.SelectedValue().ToString() <> "") Then
                stat.spctr = Me.CountryID.SelectedValue().ToString().ToUpper()
            End If

            stat.rank = rank
            Dim stateds As New DataSet

            stateds = stat.Getcomidname("BB_MASSTAT_IDNAME")
            If stateds.Tables.Count <> 0 Then
                databonds(stateds, Me.StateID)
            End If
            Me.StateID.Items.Insert(0, "")

            'StateID.SelectedIndex = 1
            Dim area As New clsCommonClass


            If (CountryID.SelectedValue().ToString() <> "") Then
                area.spctr = Me.CountryID.SelectedValue().ToString().ToUpper()
            End If

            If (StateID.SelectedValue().ToString() <> "") Then
                area.spstat = Me.StateID.SelectedValue().ToString().ToUpper()
            End If
            area.rank = rank

            'create  area the dropdownlist
            'Dim area As New clsCommonClass
            Dim areads As New DataSet

            areads = area.Getcomidname("BB_MASAREA_IDNAME")
            If areads.Tables.Count <> 0 Then
                databonds(areads, Me.AreaID)
            End If
            Me.AreaID.Items.Insert(0, "")
            Dim company As New clsCommonClass
            If rank <> 0 Then
                company.spctr = Me.CountryID.SelectedValue().ToString().ToUpper()
                company.spstat = Session("login_cmpID")
                company.sparea = Session("login_svcID")
            Else
                company.spctr = Me.CountryID.SelectedValue().ToString().ToUpper()
            End If

            company.rank = rank
            Dim companyds As New DataSet

            companyds = company.Getcomidname("BB_MASCOMP_IDNAME")
            If companyds.Tables.Count <> 0 Then
                databonds(companyds, Me.CompanyID)
            End If
            Me.CompanyID.Items.Insert(0, "")
            Me.ServiceCenterName.Focus()

        End If
    End Sub

    
    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton1.Click
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim msginfo As String = objXm.GetLabelName("StatusMessage", "SAVEORNOT")
        Dim msgtitle As String = objXm.GetLabelName("StatusMessage", "SAVETITLE")
        EffectiveDate.Text = Request.Form("EffectiveDate")
        MessageBox1.Confirm(msginfo)
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        
    End Sub

#Region " id bond"
    Public Sub databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        Dim row As DataRow
        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            NewItem.Text = row("id") & "-" & row("name")
            NewItem.Value = row("id")
            dropdown.Items.Add(NewItem)
        Next

    End Sub
#End Region

    'Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
    '    Me.Calendar.Visible = True
    '    Calendar.Attributes.Add("style", " POSITION: absolute;left:35%")

    'End Sub

    'Protected Sub Calendar1_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendar1.SelectionChanged
    '    Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
    '    System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
    '    EffectiveDate.Text = Calendar1.SelectedDate.Date.ToString().Substring(0, 10)
    '    Calendar1.Visible = False
    '    'Dim datetoview As New clsCommonClass()
    '    'Dim datastr As String
    '    'datastr = datetoview.DatetoDisplay(Calendar1.SelectedDate.ToString())
    '    'Dim dataarry As Array
    '    'dataarry = datastr.Split(" ")
    '    'Me.EffectiveDate.Text = dataarry(0)
    '    'Calendar1.Visible = False
    'End Sub

    Protected Sub CountryID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CountryID.SelectedIndexChanged
        Me.StateID.Items.Clear()
        Me.AreaID.Items.Clear()
        Me.CompanyID.Items.Clear()
        Dim rank As String = Session("login_rank")
        Dim state As New clsCommonClass

        If (CountryID.SelectedValue().ToString() <> "") Then
            state.spctr = Me.CountryID.SelectedValue().ToString().ToUpper()
        End If
        state.rank = rank

        'Dim state As New clsCommonClass
        Dim stateds As New DataSet

        stateds = state.Getcomidname("BB_MASSTAT_IDNAME")
        If stateds.Tables.Count <> 0 Then
            databonds(stateds, Me.StateID)
        End If
        Me.StateID.Items.Insert(0, "")
        Dim area As New clsCommonClass
        'Dim ctridname As String
        If (CountryID.SelectedValue().ToString() <> "") Then
            area.spctr = Me.CountryID.SelectedValue().ToString().ToUpper()
        End If
        'Dim staid As String
        If (StateID.SelectedValue().ToString() <> "") Then
            area.spstat = Me.StateID.SelectedValue().ToString().ToUpper()
        End If
        area.rank = rank

        'create  area the dropdownlist
        'Dim area As New clsCommonClass
        Dim areads As New DataSet

        areads = area.Getcomidname("BB_MASAREA_IDNAME")
        If areads.Tables.Count <> 0 Then
            databonds(areads, Me.AreaID)
        End If
        Me.AreaID.Items.Insert(0, "")
        Dim company As New clsCommonClass
        If rank = 7 Then
            company.spctr = Me.CountryID.SelectedValue().ToString().ToUpper()

        End If
        If rank = 8 Then
            company.spctr = Me.CountryID.SelectedValue().ToString().ToUpper()
            company.spstat = Session("login_cmpID")
        End If
        If rank = 9 Then
            company.spctr = Me.CountryID.SelectedValue().ToString().ToUpper()
            company.spstat = Session("login_cmpID")
            company.sparea = Session("login_svcID")
        End If
        If rank = 0 Then
            company.spctr = Me.CountryID.SelectedValue().ToString().ToUpper()
        End If

        company.rank = rank
        Dim companyds As New DataSet

        companyds = company.Getcomidname("BB_MASCOMP_IDNAME")
        If companyds.Tables.Count <> 0 Then
            databonds(companyds, Me.CompanyID)
        End If
        Me.CompanyID.Items.Insert(0, "")

        EffectiveDate.Text = Request.Form("EffectiveDate")
        Dim script As String = "self.location='#ctry';"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "AddrLoc", script, True)
    End Sub

    Protected Sub StateID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles StateID.SelectedIndexChanged
        Me.AreaID.Items.Clear()
        Dim rank As String = Session("login_rank")
        Dim area As New clsCommonClass
        'Dim ctridname As String
        If (CountryID.SelectedValue().ToString() <> "") Then
            area.spctr = Me.CountryID.SelectedValue().ToString().ToUpper()
        End If
        'Dim staid As String
        If (StateID.SelectedValue().ToString() <> "") Then
            area.spstat = Me.StateID.SelectedValue().ToString().ToUpper()
        End If
        area.rank = rank

        'create  area the dropdownlist
        'Dim area As New clsCommonClass
        Dim areads As New DataSet

        areads = area.Getcomidname("BB_MASAREA_IDNAME")
        If areads.Tables.Count <> 0 Then
            databonds(areads, Me.AreaID)
        End If
        Me.AreaID.Items.Insert(0, "")

        EffectiveDate.Text = Request.Form("EffectiveDate")
        Dim script As String = "self.location='#ctry';"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "AddrLoc", script, True)
    End Sub


    Protected Sub MessageBox1_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox1.GetMessageBoxResponse
       
        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
            Dim ServiceCenterEntity As New clsServiceCente()
            Dim telepass As New clsCommonClass()
            If (Trim(Me.ServiceCenterID.Text) <> "") Then
                ServiceCenterEntity.ServiceCenterID = Me.ServiceCenterID.Text.ToString().ToUpper()
            End If
            If (Trim(ServiceCenterName.Text) <> "") Then
                ServiceCenterEntity.ServiceCenterName = Me.ServiceCenterName.Text.ToString().ToUpper()
            End If
            If (Trim(Me.Address1.Text) <> "") Then
                ServiceCenterEntity.Address1 = Me.Address1.Text.ToString().ToUpper()
            End If
            If (ctrystat.SelectedValue().ToString() <> "") Then

                ServiceCenterEntity.Status = ctrystat.SelectedValue().ToString()
            End If
            If (Trim(Me.Address2.Text) <> "") Then
                ServiceCenterEntity.Address2 = Me.Address2.Text.ToString().ToUpper()
            End If
            If (Trim(Me.AlternateName.Text) <> "") Then
                ServiceCenterEntity.AlternateName = Me.AlternateName.Text.ToString().ToUpper()
            End If

            If (AreaID.SelectedValue().ToString() <> "") Then
                ServiceCenterEntity.AreaID = Me.AreaID.SelectedValue().ToString().ToUpper()
            End If
            If (BranchType.SelectedValue().ToString() <> "") Then
                ServiceCenterEntity.BranchType = Me.BranchType.SelectedValue().ToString()
            End If
            If (CompanyID.SelectedValue().ToString() <> "") Then
                ServiceCenterEntity.CompanyID = Me.CompanyID.SelectedValue().ToString().ToUpper()
            End If

            If (CountryID.SelectedValue().ToString() <> "") Then
                ServiceCenterEntity.CountryID = Me.CountryID.SelectedValue().ToString().ToUpper()
            End If
            If (Trim(Me.EffectiveDate.Text) <> "") Then
                'Dim datetobase As New clsCommonClass()

                ServiceCenterEntity.EffectiveDate = Request.Form("EffectiveDate") 'JCalendar1.SelectedDate.ToString()
                'datetobase.DatetoDatabase(Me.EffectiveDate.Text.ToString())
            End If
            If (Trim(Me.Fax.Text) <> "") Then
                ServiceCenterEntity.Fax = telepass.telconvertpass(Me.Fax.Text.ToString())
            End If
            If (Trim(Me.JobsPerDay.Text) <> "") Then
                ServiceCenterEntity.JobsPerDay = Convert.ToInt32(Me.JobsPerDay.Text.ToString())
            End If
            If (Trim(Me.OTPremiumJobs.Text) <> "") Then
                ServiceCenterEntity.OTPremiumJobs = Convert.ToInt32(Me.OTPremiumJobs.Text.ToString())
            End If
            If (Trim(Me.PersonInCharge.Text) <> "") Then
                ServiceCenterEntity.PersonInCharge = Me.PersonInCharge.Text.ToString().ToUpper()
            End If
            If (Trim(Me.picmobile.Text) <> "") Then
                ServiceCenterEntity.PICMobile = telepass.telconvertpass(Me.picmobile.Text.ToString())
            End If
            If (Trim(Me.POCode.Text) <> "") Then
                ServiceCenterEntity.POCode = Me.POCode.Text.ToString()
            End If
            If (Trim(Me.Telephone1.Text) <> "") Then
                ServiceCenterEntity.Telephone1 = telepass.telconvertpass(Me.Telephone1.Text.ToString())
            End If
            If (Trim(Me.Telephone2.Text) <> "") Then
                ServiceCenterEntity.Telephone2 = telepass.telconvertpass(Me.Telephone2.Text.ToString())
            End If
            If (StateID.SelectedValue().ToString() <> "") Then
                ServiceCenterEntity.StateID = Me.StateID.SelectedValue().ToString().ToUpper()
            End If
            If (Trim(CreatedBy.Text) <> "") Then
                ServiceCenterEntity.CreateBy = Session("userID").ToString().ToUpper
            End If
            'If (Trim(CreatedDate.Text) <> "") Then
            '    ServiceCenterEntity.CreateDate = Me.CreatedDate.Text()
            'End If
            If (Trim(ModiBy.Text) <> "") Then
                ServiceCenterEntity.ModifyBy = Session("userID").ToString().ToUpper
            End If
            ServiceCenterEntity.username = Session("userID")
            Dim dupCount As Integer = ServiceCenterEntity.GetDuplicatedServiceCenter()
            If dupCount.Equals(-1) Then
                Dim objXm As New clsXml
                objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                errlab.Text = objXm.GetLabelName("StatusMessage", "DUPSVCIDORNM")
                errlab.Visible = True
            ElseIf dupCount.Equals(0) Then

                Dim insCtryCnt As Integer = ServiceCenterEntity.Insert()

                Dim objXmlTr As New clsXml
                objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                If insCtryCnt = 0 Then

                    Dim objXm As New clsXml
                    objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    errlab.Text = objXm.GetLabelName("StatusMessage", "BB-HintMsg-Suc")
                    errlab.Visible = True
                Else

                    Response.Redirect("~/PresentationLayer/Error.aspx")
                End If
            End If

        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
            EffectiveDate.Text = Request.Form("EffectiveDate")
        End If
    End Sub


    'Protected Sub JCalendar_SelectedDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles JCalendar.SelectedDateChanged
    '    Me.Calendar1.Visible = True
    '    Calendar1.Attributes.Add("style", " POSITION: absolute;left:35%")
    'End Sub

    Protected Sub JCalendar1_SelectedDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles JCalendar1.SelectedDateChanged
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        EffectiveDate.Text = JCalendar1.SelectedDate.Date.ToString().Substring(0, 10)
    End Sub
End Class
