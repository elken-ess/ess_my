Imports SQLDataAccess
Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data

Partial Class PresentationLayer_masterrecord_addSalTaxID1
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try
        Try
            Dim str As String = Request.QueryString("saltaxsetupid")

        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try

        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        If Not Page.IsPostBack Then
            
            Dim salsetid As Integer
            salsetid = Request.QueryString("saltaxsetupid")
            salsetupidbox.Text = salsetid

            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            perlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTAX-0005")
            effectivelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0011")
            createby.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATBY")
            creatdate.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
            modifyby.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFBY")
            mdifydate.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFDATE")
            statusLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Status")
            saveButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Save")
            cancel.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Cancel")
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTAX-0011")
            Me.lblCompTop.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            Me.lblCompBottom.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")

            Dim objXmlTr1 As New clsXml
            objXmlTr1.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            perVa.ErrorMessage = objXmlTr1.GetLabelName("StatusMessage", "PERERROR")
            PERVAVA.ErrorMessage = objXmlTr1.GetLabelName("StatusMessage", "TaxPectg")
            EFFDATVA.ErrorMessage = objXmlTr1.GetLabelName("StatusMessage", "BB-HintMsg-Effecdate")

            'COUNTRYIDVA.ErrorMessage = objXmlTr1.GetLabelName("StatusMessage", "CountryID")
            'partidVA.ErrorMessage = objXmlTr1.GetLabelName("StatusMessage", "PARTID")
            Me.perbox.Focus()


            Dim userIDNamestr As String = Session("userID").ToString().ToUpper + "-" + Session("username").ToString.ToUpper


            creatbybox.Text = userIDNamestr
            modfybybox.Text = userIDNamestr
            creatdtbox.Text = Date.Now()
            mdfydtbox.Text = Date.Now()
            'creatbybox.Text = Session("username").ToString().ToUpper()
            'modfybybox.Text = Session("username").ToString().ToUpper()
            ' pricesetupidbox.Text = pricesetid
            Dim cntryStat As New clsrlconfirminf()
            Dim statParam As ArrayList = New ArrayList
            statParam = cntryStat.searchconfirminf("STATUS")
            Dim count As Integer
            Dim statid As String
            Dim statnm As String
            For count = 0 To statParam.Count - 1
                statid = statParam.Item(count)
                statnm = statParam.Item(count + 1)
                count = count + 1
                If (statid.Equals("ACTIVE") Or statid.Equals("OBSOLETE")) Then
                    Me.statusDrop.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                End If
            Next
            statusDrop.Items(0).Selected = True
            '''access control'''''''''''''''''''''''''''''''''''''''''''''''
            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "18")
            Me.saveButton.Enabled = purviewArray(1)

            If purviewArray(2) = False Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
            HypCal.NavigateUrl = "javascript:DoCal(document.SALTAXSetUp1form.effectivebox);"
            disp()

            End If
    End Sub

#Region " id bond"
    Public Function databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        Dim row As DataRow
        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            NewItem.Text = row("id") & "-" & row("name")
            NewItem.Value = row("id")
            dropdown.Items.Add(NewItem)
        Next

    End Function
#End Region


    Protected Sub saveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveButton.Click
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        effectivebox.Text = Request.Form("effectivebox")
        Dim msginfo As String = objXm.GetLabelName("StatusMessage", "SAVEORNOT")
        Dim msgtitle As String = objXm.GetLabelName("StatusMessage", "SAVETITLE")
        MessageBox1.Confirm(msginfo)
        disp()
       
    End Sub




   

    Protected Sub cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cancel.Click
        Dim symbl As Integer
        symbl = Request.QueryString("symbl")
        If symbl = 0 Then
            Dim url As String = "~/PresentationLayer/masterrecord/addSalTaxID.aspx?"
            url &= "saltaxsetupid=" & salsetupidbox.Text
            Response.Redirect(url)
        ElseIf symbl = 1 Then
            Dim url As String = "~/PresentationLayer/masterrecord/modifySalTaxID.aspx?"
            url &= "salsetupid1=" & salsetupidbox.Text & "&"
            url &= "SalTaxsetID=" & "" & "&"
            url &= "statusid=" & ""
            Response.Redirect(url)
        End If
    End Sub

    Protected Sub MessageBox1_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox1.GetMessageBoxResponse
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
            Dim pricesidStat As New clsCommonClass()

            Dim salSetUpEntity As New clsSalTaxID()
            If (Trim(salsetupidbox.Text) <> "") Then
                salSetUpEntity.SalTaxSetUpID = salsetupidbox.Text
            End If
            If (statusDrop.SelectedItem.Value.ToString() <> "") Then
                salSetUpEntity.SalTaxStatus = statusDrop.SelectedItem.Value.ToString()
            End If
            If (Trim(perbox.Text) <> "") Then
                salSetUpEntity.TaxPercentage = perbox.Text
            End If
            If (Trim(Request.Form("effectivebox")) <> "") Then
                Dim temparr As Array = Request.Form("effectivebox").Split("/") 'effectivebox.Text.Split("/")
                salSetUpEntity.EffectiveDate = temparr(2) + "-" + temparr(1) + "-" + temparr(0)

            End If

            'If (Trim(creatbybox.Text) <> "") Then
            '    salSetUpEntity.CreatedBy = creatbybox.Text.ToUpper()
            'End If

            If (Trim(creatdtbox.Text) <> "") Then
                salSetUpEntity.CreatedDate = pricesidStat.DatetoDatabase(creatdtbox.Text)
            End If
            'If (Trim(modfybybox.Text) <> "") Then
            '    salSetUpEntity.ModifiedBy = modfybybox.Text.ToUpper()
            'End If
            If (Trim(mdfydtbox.Text) <> "") Then
                salSetUpEntity.ModifiedDate = pricesidStat.DatetoDatabase(mdfydtbox.Text)
            End If

            If (Trim(Me.creatbybox.Text) <> "") Then
                salSetUpEntity.CreatedBy = Session("userID").ToString.ToUpper()
            End If
            If (Trim(Me.modfybybox.Text) <> "") Then
                salSetUpEntity.ModifiedBy = Session("userID").ToString.ToUpper()
            End If
            Dim dupCount As Integer = salSetUpEntity.GetDuplicatedPercentage()
            If dupCount.Equals(-1) Then
                Dim objXm As New clsXml
                objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                errlab.Text = objXm.GetLabelName("StatusMessage", "DUPSALPER")
                errlab.Visible = True
            ElseIf dupCount.Equals(0) Then
                Dim insPRIDCnt As Integer = salSetUpEntity.Insertdetail()
                If insPRIDCnt = 0 Then
                    Dim objXm As New clsXml
                    objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    errlab.Text = objXm.GetLabelName("StatusMessage", "SuccessfullySaved")
                    errlab.Visible = True
                Else

                    Response.Redirect("~/PresentationLayer/Error.aspx")

                End If
            End If
            effectivebox.Text = Request.Form("effectivebox")
            disp()

        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
            effectivebox.Text = Request.Form("effectivebox")
        End If



    End Sub

    Protected Sub SALSETUPSetUpView_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles SALSETUPSetUpView.PageIndexChanging
        SALSETUPSetUpView.PageIndex = e.NewPageIndex
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        effectivebox.Text = Request.Form("effectivebox")
        disp()
        effectivebox.Text = Request.Form("effectivebox")
    End Sub
    Protected Sub disp()
        Dim returnpsid As Integer
        returnpsid = Request.QueryString("saltaxsetupid")
        Dim objXmlTr2 As New clsXml
        objXmlTr2.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        Dim SalSetUpEntity1 As New clsSalTaxID()
        SalSetUpEntity1.SalTaxSetUpID = salsetupidbox.Text
        SalSetUpEntity1.SalTaxStatus = statusDrop.SelectedItem.Value.ToString()
        Dim SalSetUpall As DataSet = SalSetUpEntity1.GetSalTaxIDdetail()
        SALSETUPSetUpView.DataSource = SalSetUpall
        Session("SALSETUPSetUpView") = SalSetUpall
        SALSETUPSetUpView.AllowPaging = True
        'SALSETUPSetUpView.AllowSorting = True
        SALSETUPSetUpView.DataBind()
        If SalSetUpall.Tables(0).Rows.Count <> 0 Then
            SALSETUPSetUpView.HeaderRow.Cells(0).Text = objXmlTr2.GetLabelName("EngLabelMsg", "BB-MASSTAX-0013")
            SALSETUPSetUpView.HeaderRow.Cells(0).Font.Size = 8
            SALSETUPSetUpView.HeaderRow.Cells(1).Text = objXmlTr2.GetLabelName("EngLabelMsg", "BB-MASSTAX-0005")
            SALSETUPSetUpView.HeaderRow.Cells(1).Font.Size = 8
            SALSETUPSetUpView.HeaderRow.Cells(2).Text = objXmlTr2.GetLabelName("EngLabelMsg", "BB-MASSTAX-0006")
            SALSETUPSetUpView.HeaderRow.Cells(2).Font.Size = 8
            SALSETUPSetUpView.HeaderRow.Cells(3).Text = objXmlTr2.GetLabelName("EngLabelMsg", "BB-Status")
            SALSETUPSetUpView.HeaderRow.Cells(3).Font.Size = 8
            SALSETUPSetUpView.HeaderRow.Cells(4).Text = objXmlTr2.GetLabelName("EngLabelMsg", "BB-CREATDATE")
            SALSETUPSetUpView.HeaderRow.Cells(4).Font.Size = 8
        End If
    End Sub

   

    
   
    Protected Sub Calendar_SelectedDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendar.SelectedDateChanged
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Dim prcidDate As New clsCommonClass()
        effectivebox.Text = Calendar.SelectedDate.Date.ToString().Substring(0, 10)

        disp()
    End Sub

    Protected Sub Calendar_CalendarVisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendar.CalendarVisibleChanged
        disp()
    End Sub
End Class

