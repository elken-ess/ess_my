Imports BusinessEntity
Imports System.Configuration
Imports System.Xml
Imports System.Data
Imports System.Data.SqlClient
Partial Class PresentationLayer_masterrecord_modifycustomer
    Inherits System.Web.UI.Page
    Dim fstrErrorSaveMessage As String = "Record Saved but got some error, please contact administrator for condition "
    'Private ROID As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Page.MaintainScrollPositionOnPostBack = True
        AjaxPro.Utility.RegisterTypeForAjax(GetType(PresentationLayer_masterrecord_modifycustomer))

        
        If Not Page.IsPostBack Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try
            Dim statXmlTr As New clsXml
            statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")

            Me.custnamemsg.ErrorMessage = "Please enter a valid Customer Name"'statXmlTr.GetLabelName("StatusMessage", "Custnameerr")

            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "04")
            If purviewArray(2) = False Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return

            End If
            Me.saveButton.Enabled = purviewArray(1)

            If (accessgroup = "MY-ADMIN" Or accessgroup = "MY-HOC" Or accessgroup = "MY-CR-SU") Then
                SendSMS1.Enabled = True
                SendSMS2.Enabled = True
            End If

            Dim myid As String
            Dim mypf As String = ""

            Dim modisy As String

            Try
                mypf = Request.Params("custpf")

                If mypf = "" Then
                    mypf = Session("login_ctryID")
                End If

                myid = Request.Params("custid")
                modisy = Request.QueryString("modisy")

                'ROID = Request.Params("ROID")
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return

            End Try


            If modisy = 0 Then
                saveButton.Enabled = purviewArray(1)

            End If
            If modisy = 1 Then
                'saveButton.Enabled = False
                If saveButton.Enabled = False Then
                    HyperLink2.Visible = True
                    'LinkButton1.Visible = True

                End If
            End If


            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            custidlab.Text = "Customer ID" 'objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0001")
            JDECIDLab.Text = "JDE Customer ID" 'objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0009")
            CustomerNamelab.Text = "Customer Name" 'objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0010")
            statuslab.Text = "Status" 'objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCTRY-0004")
            AlternateNameLab.Text = "Alternate Name" 'objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0005")
            RacesLab.Text = "Race" 'objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0011")
            CustomerTypeLab.Text = "Customer Type" 'objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0012")
            noteslab.Text = "Notes" 'objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0013")
            titleLab.Text = "Customer Record Maintenance - Edit" 'objXmlTr.GetLabelName("EngLabelMsg", "MODCUSTOM")
            adresstitlLabel.Text = "Address Details" 'objXmlTr.GetLabelName("EngLabelMsg", "BROADDRESS")
            teltitllab.Text = "Contact Details" 'objXmlTr.GetLabelName("EngLabelMsg", "BROCONTACT")
            Me.roLab.Text = "RO Details" 'objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0017")
            Me.SvcidLab.Text = "Service Center ID" 'objXmlTr.GetLabelName("EngLabelMsg", "SVCRID")
            saveButton.Text = "Save" 'objXmlTr.GetLabelName("EngLabelMsg", "BB-Save")
            cancelLink.Text = "Back" 'objXmlTr.GetLabelName("EngLabelMsg", "BB-Cancel")
            HyperLink2.Text = "Create Install Base" 'objXmlTr.GetLabelName("EngLabelMsg", "CREATINBASE")
            'added by deyb 18-07-2006
            CustomerCtrLbl.Text = "Country ID" 'objXmlTr.GetLabelName("EngLabelMsg", "CTRYHD0")
            modelLbl.Text = "JDE RO ID" 'objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0023")
            customeremailLab.Text = "Email Address 1" 'objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0024")
            DistNameLbl.Text = "Distributor Name" 'objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0025")
            DistContactLbl.Text = "Distributor's Contact" 'objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0026")
            DistEmailLbl.Text = "Email Address 2" 'objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0027")
            Label1.Text = "Primary Address 1" 'objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0028")
            Label2.Text = "Address 2" 'objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0029")
            Label3.Text = "PO Code" 'objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0030")
            Label5.Text = "Country" 'objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0031")
            Label4.Text = "State" 'objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0032")
            Label6.Text = "Area" 'objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0033")
            Label8.Text = "Telephone (H)" 'objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0034")
            Label9.Text = "Telephone (O)" 'objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0035")
            Label11.Text = "Fax" 'objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0036")
            Label7.Text = "Mobile 1" 'objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0037")
            Label10.Text = "Mobile 2" 'objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0038")
            lblCreateApp.Text = "Create Appointment" 'objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0039")
            lblViewInst.Text = "View Install Base" 'objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0040")
            '---------------------------------

            creatby.Text = "Create By" 'objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATBY")
            creatdate.Text = "Create Date" 'objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
            modfyby.Text = "Modify By" 'objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFBY")
            mdfydate.Text = "Modify Date" 'objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFDATE")
            Me.lblCompBottom.Text = "Fields mark with Asterisk (*) are required." 'objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            Me.lblCompTop.Text = "Fields mark with Asterisk (*) are required." 'objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            Me.LblInfo.Text = "Any changes made on this Address portion will update the Customer's SITE Address" 'objXmlTr.GetLabelName("EngLabelMsg", "ADDRINFO")


            'Dim statXmlTr As New clsXml
            statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            'Me.custidmsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "Custiderr")
            Me.svcidmsg.ErrorMessage = "Please select Service Center ID" 'statXmlTr.GetLabelName("StatusMessage", "MAS-SVC")
            Me.Racesmsg.ErrorMessage = "Please select Race" 'statXmlTr.GetLabelName("StatusMessage", "RACEMSG")
            Me.CustomerTypemsg.ErrorMessage = "Please select Customer Type" 'statXmlTr.GetLabelName("StatusMessage", "CUSTTYPEMSG")
            Me.frvPostCode.ErrorMessage = "Please enter a valid Post Code" 'statXmlTr.GetLabelName("StatusMessage", "COMPCODE")
            Me.AdresRequired1.ErrorMessage = "Please enter a valid Address1" 'statXmlTr.GetLabelName("StatusMessage", "COMADDR1")
            Me.AdresRequired2.ErrorMessage = "Please enter a valid Address2" 'statXmlTr.GetLabelName("StatusMessage", "COMADDR2")

            Me.rfvCTRID.ErrorMessage = "Please select Country ID" 'statXmlTr.GetLabelName("StatusMessage", "MAS-COUNTRY")
            Me.rfvAREAID.ErrorMessage = "Please select Area ID" 'statXmlTr.GetLabelName("StatusMessage", "MAS-AREA")
            Me.StaIDValid.ErrorMessage = "Please select State ID" 'statXmlTr.GetLabelName("StatusMessage", "MAS-STAT")


            Dim cntryStat As New clsrlconfirminf()
            Dim statParam As ArrayList = New ArrayList
            statParam = cntryStat.searchconfirminf("STATUS")
            Dim count As Integer
            Dim statid As String
            Dim statnm As String
            For count = 0 To statParam.Count - 1
                statid = statParam.Item(count)
                statnm = statParam.Item(count + 1)
                count = count + 1
                If (statid.Equals("ACTIVE") Or statid.Equals("DELETE") Or statid.Equals("OBSOLETE")) Then
                    Me.status.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                End If
                'status.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))

            Next
            If purviewArray(4) = False Then
                Me.status.Items.Remove(Me.status.Items.FindByValue("DELETE"))
            End If

            Dim custtype As New clsrlconfirminf()
            Dim custParam As ArrayList = New ArrayList
            custParam = custtype.searchconfirminf("CUSTTYPE")
            Dim custcount As Integer
            Dim custid As String
            Dim custnm As String
            For custcount = 0 To custParam.Count - 1
                custid = custParam.Item(custcount)
                custnm = custParam.Item(custcount + 1)
                custcount = custcount + 1
                Me.CustomerType.Items.Add(New ListItem(custnm.ToString(), custid.ToString()))
            Next

            'load preferred language - Added by LLY 201702
            Me.prefLang.Items.Clear()
            Dim prefLang As New clsrlconfirminf()
            Dim langParam As ArrayList = New ArrayList
            langParam = prefLang.searchconfirminf("PREFLANG")
            Dim preflangcount As Integer
            Dim langid As String
            Dim langnm As String
            For preflangcount = 0 To langParam.Count - 1
                langid = langParam.Item(preflangcount)
                langnm = langParam.Item(preflangcount + 1)
                preflangcount = preflangcount + 1
                Me.prefLang.Items.Add(New ListItem(langnm.ToString(), langid.ToString()))
            Next


            Dim rank As String = Session("login_rank")
            Dim service As New clsCommonClass
            If rank = 7 Then
                service.spctr = Session("login_ctryID")

            End If
            If rank = 8 Then
                service.spctr = Session("login_ctryID")
                service.spstat = Session("login_cmpID")
            End If
            If rank = 9 Then
                service.spctr = Session("login_ctryID")
                service.spstat = Session("login_cmpID")
                service.sparea = Session("login_svcID")
            End If
            If rank = 0 Then
                'service.spctr = Me.CountryID.SelectedValue().ToString().ToUpper()
            End If
            service.rank = rank
            Dim serviceds As New DataSet

            serviceds = service.Getcomidname("BB_MASSVRC_IDNAME")
            If serviceds.Tables.Count <> 0 Then
                databonds(serviceds, Me.SVCID)
            End If
            Me.SVCID.Items.Insert(0, "")
            If myid.Trim() <> "" Then
                Dim CustomerEntity As New clsCustomerRecord()

                CustomerEntity.CustomerID = myid
                Me.custid.Text = myid
                CustomerEntity.Customerpf = mypf
                Dim edtReader As SqlDataReader = CustomerEntity.GetCustomerDetailsByID()
                If edtReader.Read() Then
                    Me.CustomerPrefix.Text = edtReader.GetValue(0).ToString()
                    Me.CustomerCtryID.Text = edtReader.GetValue(0).ToString()
                    Me.JDECID.Text = edtReader.GetValue(2).ToString() 'altername
                    Me.CustomerName.Text = edtReader.GetValue(3).ToString() 'country id
                    'countryiddrpd.Enabled = False
                    Me.AlternateName.Text = edtReader.GetValue(4).ToString() 'category id
                    '
                    Me.notes.Text = edtReader.GetValue(8).ToString()
                    Me.CustomerEmail.Text = edtReader.GetValue(14).ToString() 'email
                    Dim mtchdl As New clsrlconfirminf()
                    Dim tchdlParam As New ArrayList

                    Me.CustomerType.Text = edtReader.GetValue(6).ToString()
                    changeCustomerType(CustomerType.SelectedValue)
                    Races.SelectedValue = edtReader.GetValue(5).ToString()


                    'get preferred language - Added by LLY 201702
                    Me.prefLang.SelectedValue = "PREFLANG-" + edtReader.GetValue(15)


                    Me.status.Text = edtReader.GetValue(7).ToString()
                    If Me.status.SelectedValue.ToString <> "ACTIVE" Then
                        Me.HyperLink2.Enabled = False
                    End If
                    Me.notes.Text = edtReader.GetValue(8).ToString()
                    Me.SVCID.Text = edtReader.GetValue(9).ToString()
                    Dim Rcreatby As String = edtReader.Item(10).ToString().ToUpper()
                    Dim Rcreat As New clsCommonClass
                    Dim Rcreatds As New DataSet()

                    If Rcreatby <> "ADMIN" Then

                        Rcreat.userid() = Rcreatby

                        Rcreatds = Rcreat.Getuseridname("BB_MASUSER_SelByIDName")

                        Me.creatbybox.Text = Rcreatds.Tables(0).Rows(0).Item(0)
                    Else
                        Me.creatbybox.Text = "ADMIN-ADMIN"
                    End If
                    'Me.creatbybox.Text = edtReader.GetValue(9).ToString()
                    Me.creatdtbox.Text = edtReader.GetValue(11).ToString()
                    Dim userIDNamestr As String = Session("userID").ToString().ToUpper + "-" + Session("username").ToString.ToUpper

                    'Me.modfybybox.Text = userIDNamestr

                    Rcreat.userid() = edtReader.GetValue(12).ToString()
                    Rcreatds = Rcreat.Getuseridname("BB_MASUSER_SelByIDName")
                    Me.modfybybox.Text = Rcreatds.Tables(0).Rows(0).Item(0)

                    Dim customer As New clsCustomerRecord()
                    customer.username = userIDNamestr
                    Me.mdfydtbox.Text = edtReader.GetValue(13).ToString()
                End If

                'get the customer contacts
                Dim CustomerTeleEntity As New clsCustomerRecord()
                Dim telepass As New clsCommonClass()
                CustomerTeleEntity.CustomerID = myid
                Me.custid.Text = myid
                CustomerTeleEntity.Customerpf = mypf
                Dim intCtr As Integer
                Dim edtReaderTele As DataSet = CustomerEntity.GetCustomerTele()
                For intCtr = 0 To edtReaderTele.Tables(0).Rows.Count - 1

                    Me.DistEmail.Text = edtReaderTele.Tables(0).Rows(intCtr).Item(3).ToString 'email
                    Select Case edtReaderTele.Tables(0).Rows(intCtr).Item(0).ToString
                        Case "TELH" 'h tele
                            Me.tele2.Text = telepass.passconverttel(edtReaderTele.Tables(0).Rows(intCtr).Item(2).ToString)
                            Me.txtRemark2.Text = edtReaderTele.Tables(0).Rows(intCtr).Item(1).ToString
                        Case "TELO" 'o tele
                            Me.tele3.Text = telepass.passconverttel(edtReaderTele.Tables(0).Rows(intCtr).Item(2).ToString)
                            Me.txtRemark3.Text = edtReaderTele.Tables(0).Rows(intCtr).Item(1).ToString
                        Case "MOB1" 'mod 1
                            Me.tele5.Text = telepass.passconverttel(edtReaderTele.Tables(0).Rows(intCtr).Item(2).ToString)
                            Me.txtRemark5.Text = edtReaderTele.Tables(0).Rows(intCtr).Item(1).ToString
                        Case "FAX" 'fax
                            Me.tele4.Text = telepass.passconverttel(edtReaderTele.Tables(0).Rows(intCtr).Item(2).ToString)
                            Me.txtRemark4.Text = edtReaderTele.Tables(0).Rows(intCtr).Item(1).ToString
                        Case "DIST" 'dist con
                            Me.tele1.Text = telepass.passconverttel(edtReaderTele.Tables(0).Rows(intCtr).Item(2).ToString)
                            Me.DistName.Text = edtReaderTele.Tables(0).Rows(intCtr).Item(1).ToString 'PIC
                        Case "MOB2" 'mob 2
                            Me.tele6.Text = telepass.passconverttel(edtReaderTele.Tables(0).Rows(intCtr).Item(2).ToString)
                            Me.txtRemark6.Text = edtReaderTele.Tables(0).Rows(intCtr).Item(1).ToString
                    End Select
                Next

                'load the default add which is SITE
                CustomerTeleEntity.CustomerID = myid
                Dim CustomerAddressEntity As New clsCustomerRecord()
                CustomerEntity.CustomerID = myid
                Me.custid.Text = myid
                CustomerEntity.Customerpf = mypf
                Dim edtAddReader As SqlDataReader = CustomerEntity.GetCustomerAddress()
                If edtAddReader.Read() Then
                    Me.Address1.Text = edtAddReader.GetValue(0).ToString.Trim
                    Me.Address2.Text = edtAddReader.GetValue(1).ToString.Trim
                    GetCountryID()
                    Me.ctrid.SelectedValue = edtAddReader.GetValue(2).ToString.Trim
                    GetStateID()
                    Me.staid.SelectedValue = edtAddReader.GetValue(5).ToString.Trim
                    GetAreaID()
                    Me.areaid.SelectedValue = edtAddReader.GetValue(3).ToString.Trim
                    Me.POCode.Text = edtAddReader.GetValue(4).ToString.Trim
                End If

            End If
            loadaddressviewbond()
            loadcontactviewbond()
            loadroviewbond()



            If modisy = -1 Then
                saveButton.Enabled = True
                'LinkButton1.Enabled = False
                HyperLink2.Enabled = True
                'addressView.Columns(0).Visible = False
                'contactView.Columns(0).Visible = False
                'Me.addaddress.Enabled = False
                'Me.addtel.Enabled = False
                cancelLink.NavigateUrl = "../function/AppointmentCallingListing.aspx"

            End If

        End If
        Call DisplayGridHeader()

        
    End Sub

    Private Function GetCountryID()
        Dim country As New clsCommonClass
        Dim rankID As String = Session("login_rank")
        If rankID <> 0 Then
            country.spctr = Session("login_ctryID").ToString().ToUpper
        End If
        country.rank = rankID
        Dim countryds As New DataSet
        countryds = country.Getcomidname("BB_MASCTRY_IDNAME")
        If countryds.Tables.Count <> 0 Then
            databonds(countryds, Me.ctrid)
        End If
        'Me.ctrid.Items.Insert(0, "")
    End Function
#Region " id bond"
    Public Function databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList) As ArrayList
        dropdown.Items.Clear()
        Dim row As DataRow
        Dim infon As ArrayList = New ArrayList()
        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            NewItem.Text = row("id") & "-" & row("name")
            NewItem.Value = row("id")
            dropdown.Items.Add(NewItem)
            infon.Add(NewItem.Value)
        Next
        Return infon

    End Function
#End Region
#Region " load addressview bond"
    Public Sub loadaddressviewbond()
        Dim addressEntity As New clsAddress()

        If (Trim(Me.custid.Text) <> "") Then
            addressEntity.CustomerID = Me.custid.Text.ToUpper()

        End If
        If (Trim(Me.CustomerPrefix.Text) <> "") Then
            addressEntity.CustPrefix = Me.CustomerPrefix.Text.ToUpper()

        End If
        Dim addressall As DataSet = addressEntity.GetAddress()
        'If addressall.Tables(0).Rows.Count <> 0 Then
        Me.addressView.DataSource = addressall
        Session("addressView") = addressall
        addressView.AllowPaging = True
        addressView.AllowSorting = True
        Dim editcol As System.Web.UI.WebControls.HyperLinkField = New HyperLinkField()
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        If addressall.Tables(0).Rows.Count >= 1 Then
            Dim addresstype As String = addressall.Tables.Item(0).Columns(0).ColumnName.ToString()
            Dim NavigateUrls As Array = Array.CreateInstance(GetType(String), 1)
            NavigateUrls(0) = addresstype
            editcol.DataNavigateUrlFields = NavigateUrls
            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "04")
            editcol.Visible = purviewArray(2)
        End If
        Dim strAddType = ""
        addressView.DataBind()
        'added by deyb
        Dim x As Integer
        For x = 0 To addressView.Rows.Count - 1
            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "04")
            Dim xHyp As HyperLink = CType(addressView.Rows(x).FindControl("HypEdit"), HyperLink)
            strAddType = Trim(addressall.Tables(0).Rows(x).Item("MADR_ADRTY").ToString)
            xHyp.NavigateUrl = "~/PresentationLayer/masterrecord/modifyAddress.aspx?addresstype=" & strAddType & "&customid=" & Me.custid.Text.ToUpper() & "&Prefix=" & Me.CustomerPrefix.Text.ToUpper() & "&symbol=1"
            xHyp.Visible = purviewArray(2)
        Next
        '===========

        'CHECK IF GOT 3 ADDRESS ALREADY
        If addressView.Rows.Count >= 3 Then
            linkAddAddress.Enabled = False
        End If
        '=======================

        Call DisplayGridHeader()
    End Sub
#End Region
#Region " insert addressview bond"
    Public Sub addressviewbond()
        Dim addressEntity As New clsAddress()

        If (Trim(Me.custid.Text) <> "") Then
            addressEntity.CustomerID = Me.custid.Text.ToUpper()

        End If
        If (Trim(Me.CustomerPrefix.Text) <> "") Then
            addressEntity.CustPrefix = Me.CustomerPrefix.Text.ToUpper()

        End If
        Dim addressall As DataSet = addressEntity.GetAddress()
        'If addressall.Tables(0).Rows.Count <> 0 Then
        Me.addressView.DataSource = addressall

        addressView.AllowPaging = True
        addressView.AllowSorting = True
        addressView.DataBind()
        Call DisplayGridHeader()

        'Dim obiXmlTr As New clsXml
        'obiXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        'If addressall.Tables(0).Rows.Count >= 1 Then
        '    addressView.HeaderRow.Cells(1).Text = obiXmlTr.GetLabelName("EngLabelMsg", "BB-INSTALLHD1")
        '    addressView.HeaderRow.Cells(1).Font.Size = 8
        '    addressView.HeaderRow.Cells(2).Text = obiXmlTr.GetLabelName("EngLabelMsg", "BB-INSTALLHD2")
        '    addressView.HeaderRow.Cells(2).Font.Size = 8
        '    addressView.HeaderRow.Cells(3).Text = obiXmlTr.GetLabelName("EngLabelMsg", "ADDRESSTYPE")
        '    addressView.HeaderRow.Cells(3).Font.Size = 8
        '    addressView.HeaderRow.Cells(4).Text = obiXmlTr.GetLabelName("EngLabelMsg", "STATUS")
        '    addressView.HeaderRow.Cells(4).Font.Size = 8
        '    addressView.HeaderRow.Cells(5).Text = obiXmlTr.GetLabelName("EngLabelMsg", "ADDRESS1")
        '    addressView.HeaderRow.Cells(5).Font.Size = 8
        '    addressView.HeaderRow.Cells(6).Text = obiXmlTr.GetLabelName("EngLabelMsg", "ADDRESS2")
        '    addressView.HeaderRow.Cells(6).Font.Size = 8
        '    addressView.HeaderRow.Cells(7).Text = obiXmlTr.GetLabelName("EngLabelMsg", "POCODE")
        '    addressView.HeaderRow.Cells(7).Font.Size = 8
        'End If
    End Sub
#End Region
#Region " load contactview bond"
    Public Sub loadcontactviewbond()
        Dim contactEntity As New clsContact()
        If (Trim(Me.custid.Text) <> "") Then
            contactEntity.CustomerID = Me.custid.Text.ToUpper()
        End If
        If (Trim(Me.CustomerPrefix.Text) <> "") Then
            contactEntity.CustPrefix = Me.CustomerPrefix.Text.ToUpper()

        End If
        Dim contactall As DataSet = contactEntity.Getcontact()
        Me.contactView.DataSource = contactall
        Session("contactView") = contactall
        contactView.AllowPaging = True
        contactView.AllowSorting = True
        Dim editcol As System.Web.UI.WebControls.HyperLinkField = New HyperLinkField()
        Dim obiXmlTr As New clsXml
        obiXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        If contactall.Tables(0).Rows.Count >= 1 Then
            Dim contacttype As String = contactall.Tables.Item(0).Columns(0).ColumnName.ToString()
            Dim NavigateUrls As Array = Array.CreateInstance(GetType(String), 1) '时间和ID做主键
            NavigateUrls(0) = contacttype
            'NavigateUrls(1) = contactstat
            editcol.DataNavigateUrlFields = NavigateUrls
            editcol.DataNavigateUrlFields = NavigateUrls
            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "04")
            editcol.Visible = purviewArray(2)
        End If
        'editcol.Text = obiXmlTr.GetLabelName("EngLabelMsg", "BB-Edit") '"edit"
        ''Dim strContactType = contactall.Tables(0).Rows(
        'editcol.DataNavigateUrlFormatString = "~/PresentationLayer/masterrecord/modifyContact.aspx?contacttype={0}&customid=" & Me.custid.Text.ToUpper() & "&Prefix=" & Me.CustomerPrefix.Text.ToUpper() & "&consymbol=1"
        'editcol.HeaderImageUrl = "~/PresentationLayer/graph/edit.gif"
        'contactView.Columns.Add(editcol)
        contactView.DataBind()
        Dim x As Integer
        Dim strConType As String
        For x = 0 To contactView.Rows.Count - 1
            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "04")
            Dim xHyp As HyperLink = CType(contactView.Rows(x).FindControl("HypCon"), HyperLink)
            strConType = Trim(contactall.Tables(0).Rows(x).Item("MTEL_TELTY").ToString)
            xHyp.NavigateUrl = "~/PresentationLayer/masterrecord/modifyContact.aspx?contacttype=" & strConType & "&customid=" & Me.custid.Text.ToUpper() & "&Prefix=" & Me.CustomerPrefix.Text.ToUpper() & "&consymbol=1"
            xHyp.Visible = purviewArray(2)
        Next
        If contactall.Tables(0).Rows.Count >= 1 Then
            Call DisplayGridHeader()
            Dim modisy As String = Request.QueryString("modisy")
            If modisy = -1 Then
                contactView.Columns(0).Visible = False
            End If
        End If
    End Sub

#End Region
#Region " load roview bond"
    Public Sub loadroviewbond()
        Dim roEntity As New clsCustomerRecord()
        If (Trim(Me.custid.Text) <> "") Then
            roEntity.CustomerID = Me.custid.Text.ToUpper()
        End If
        If (Trim(Me.CustomerPrefix.Text) <> "") Then
            roEntity.Customerpf = Me.CustomerPrefix.Text.ToUpper()
        End If
        Dim roall As DataSet = roEntity.GetRO()
        'If addressall.Tables(0).Rows.Count <> 0 Then
        Me.roView.DataSource = roall
        Session("roView") = roall
        Me.roView.AllowPaging = True
        roView.AllowSorting = True
        Dim editcol As System.Web.UI.WebControls.HyperLinkField = New HyperLinkField()
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        If roall.Tables(0).Rows.Count >= 1 Then
            Dim rotype As String = roall.Tables.Item(0).Columns(0).ColumnName.ToString()
            Dim NavigateUrls As Array = Array.CreateInstance(GetType(String), 1)
            NavigateUrls(0) = rotype
            editcol.DataNavigateUrlFields = NavigateUrls
            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "04")
            editcol.Visible = purviewArray(2)
        End If
        roView.DataBind()
        'added by deyb
        Dim x As Integer
        Dim RoID As String
        For x = 0 To roView.Rows.Count - 1

            Dim strSerialNo As String = Trim(roall.Tables(0).Rows(x).Item("MROU_SERNO"))
            Dim strModNo As String = Trim(roall.Tables(0).Rows(x).Item("MROU_MODID"))
            RoID = Trim(roall.Tables(0).Rows(x).Item("ROUID"))
            Dim strTempURL As String = ""

            Dim xHyp As HyperLink = CType(roView.Rows(x).FindControl("HypViewRO"), HyperLink)
            xHyp.NavigateUrl = "~/PresentationLayer/masterrecord/modifyinstallBase.aspx?rouid=" & RoID & "&customid=" & Me.custid.Text.ToUpper() & "&Prefix=" & Me.CustomerPrefix.Text.ToUpper() & "&modcust=1"

            strTempURL = "ROID=" + RoID + "&ROINFO=" + strModNo + "-" + strSerialNo + _
                                   "&custName=" + Me.CustomerName.Text + "&custID=" + Me.custid.Text + "&custPf=" + Me.CustomerPrefix.Text
            strTempURL = "../function/serverHistoryPopup.aspx?" + strTempURL

            Dim lblViewService As Label = CType(roView.Rows(x).FindControl("ViewService"), Label)
            lblViewService.CssClass = "cursor"
            'strTempURL = "showModalDialog('" + strTempURL + "','_calPick','status=no;center=yes;dialogWidth=600pt;dialogHeight=400pt');"


            strTempURL = "ROID=" + RoID + "&CustomerID=" + Me.custid.Text + "&customerPrefix=" + Me.CustomerPrefix.Text
            strTempURL = "../function/ViewAppointmentServiceHistory.aspx?" + strTempURL
            lblViewService.ToolTip = strTempURL
            lblViewService.Attributes.Add("OnClick", "window.open (""" + strTempURL + """)")


        Next
        '===========
        Call DisplayGridHeader()

    End Sub

#Region " view ro bond"
    Public Sub viewroviewbond()
        Dim roEntity As New clsCustomerRecord()
        If (Trim(Me.custid.Text) <> "") Then
            roEntity.CustomerID = Me.custid.Text.ToUpper()

        End If
        If (Trim(Me.CustomerPrefix.Text) <> "") Then
            roEntity.Customerpf = Me.CustomerPrefix.Text.ToUpper()

        End If

        Dim roall As DataSet = roEntity.GetRO()
        'If addressall.Tables(0).Rows.Count <> 0 Then
        Me.roView.DataSource = roall
        roView.DataBind()
        Call DisplayGridHeader()

    End Sub
#End Region
#Region " view addressview bond"

    Sub DisplayGridHeader()
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")

        If addressView.Rows.Count >= 1 Then
            addressView.HeaderRow.Cells(1).Text = "Address Type"'objXmlTr.GetLabelName("EngLabelMsg", "ADDRESSTYPE")
            addressView.HeaderRow.Cells(1).Font.Size = 8
            addressView.HeaderRow.Cells(2).Text = "Status"'objXmlTr.GetLabelName("EngLabelMsg", "STATUS")
            addressView.HeaderRow.Cells(2).Font.Size = 8
            addressView.HeaderRow.Cells(3).Text = "Address1"'objXmlTr.GetLabelName("EngLabelMsg", "ADDRESS1")
            addressView.HeaderRow.Cells(3).Font.Size = 8
            addressView.HeaderRow.Cells(4).Text = "Address2"'objXmlTr.GetLabelName("EngLabelMsg", "ADDRESS2")
            addressView.HeaderRow.Cells(4).Font.Size = 8
            addressView.HeaderRow.Cells(5).Text = "PO Code"'objXmlTr.GetLabelName("EngLabelMsg", "POCODE")
            addressView.HeaderRow.Cells(5).Font.Size = 8
        End If

        If roView.Rows.Count >= 1 Then
            roView.HeaderRow.Cells(2).Text = "Model Type"'objXmlTr.GetLabelName("EngLabelMsg", "MODEL")
            roView.HeaderRow.Cells(2).Font.Size = 8
            roView.HeaderRow.Cells(3).Text = "Serial No"'objXmlTr.GetLabelName("EngLabelMsg", "SERIALNO")
            roView.HeaderRow.Cells(3).Font.Size = 8
            roView.HeaderRow.Cells(4).Text = "Status"'objXmlTr.GetLabelName("EngLabelMsg", "STATUS")
            roView.HeaderRow.Cells(4).Font.Size = 8
            roView.HeaderRow.Cells(5).Text = "Last Service Date"'objXmlTr.GetLabelName("EngLabelMsg", "LASTSERVICEDATE")
            roView.HeaderRow.Cells(5).Font.Size = 8
            roView.HeaderRow.Cells(6).Text = "Install Date"'objXmlTr.GetLabelName("EngLabelMsg", "INSTALLDATE")
            roView.HeaderRow.Cells(6).Font.Size = 8
            roView.HeaderRow.Cells(7).Text = "RO ID"'objXmlTr.GetLabelName("EngLabelMsg", "BB-INSTALLHD0")
            roView.HeaderRow.Cells(7).Font.Size = 8

        End If

        If contactView.Rows.Count >= 1 Then
            contactView.HeaderRow.Cells(1).Text = "Telephone Type" 'objXmlTr.GetLabelName("EngLabelMsg", "TELEPHONETYPE")
            contactView.HeaderRow.Cells(1).Font.Size = 8
            contactView.HeaderRow.Cells(2).Text = "Status" 'objXmlTr.GetLabelName("EngLabelMsg", "STATUS")
            contactView.HeaderRow.Cells(2).Font.Size = 8
            contactView.HeaderRow.Cells(3).Text = "PIC Name" 'objXmlTr.GetLabelName("EngLabelMsg", "PICNAME")
            contactView.HeaderRow.Cells(3).Font.Size = 8
            contactView.HeaderRow.Cells(4).Text = "Telephone" 'objXmlTr.GetLabelName("EngLabelMsg", "TELEPHONE")
            contactView.HeaderRow.Cells(4).Font.Size = 8
        End If
    End Sub

    Public Sub viewaddressviewbond()
        Dim addressEntity As New clsAddress()

        If (Trim(Me.custid.Text) <> "") Then
            addressEntity.CustomerID = Me.custid.Text.ToUpper()

        End If
        If (Trim(Me.CustomerPrefix.Text) <> "") Then
            addressEntity.CustPrefix = Me.CustomerPrefix.Text.ToUpper()

        End If
        Dim addressall As DataSet = addressEntity.GetAddress()
        'If addressall.Tables(0).Rows.Count <> 0 Then

        addressView.DataSource = addressall
        addressView.DataBind()
        Dim strAddType = ""
        addressView.DataBind()
        'added by deyb
        Dim x As Integer
        For x = 0 To addressView.Rows.Count - 1
            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "04")
            Dim xHyp As HyperLink = CType(addressView.Rows(x).FindControl("HypEdit"), HyperLink)
            strAddType = Trim(addressall.Tables(0).Rows(x).Item("MADR_ADRTY").ToString)
            xHyp.NavigateUrl = "~/PresentationLayer/masterrecord/modifyAddress.aspx?addresstype=" & strAddType & "&customid=" & Me.custid.Text.ToUpper() & "&Prefix=" & Me.CustomerPrefix.Text.ToUpper() & "&symbol=1"
            xHyp.Visible = purviewArray(2)
        Next
        '===========

        Call DisplayGridHeader()
    End Sub
#End Region

#Region " view contactview bond"
    Public Sub viewcontactviewbond()
        Dim contactEntity As New clsContact()
        If (Trim(Me.custid.Text) <> "") Then
            contactEntity.CustomerID = Me.custid.Text.ToUpper()

        End If
        If (Trim(Me.CustomerPrefix.Text) <> "") Then
            contactEntity.CustPrefix = Me.CustomerPrefix.Text.ToUpper()
        End If
        Dim contactall As DataSet = contactEntity.Getcontact()
        'If addressall.Tables(0).Rows.Count <> 0 Then
        Me.contactView.DataSource = contactall
        contactView.DataBind()
        Dim x As Integer
        Dim strConType As String
        For x = 0 To contactView.Rows.Count - 1
            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "04")
            Dim xHyp As HyperLink = CType(contactView.Rows(x).FindControl("HypCon"), HyperLink)
            strConType = Trim(contactall.Tables(0).Rows(x).Item("MTEL_TELTY").ToString)
            xHyp.NavigateUrl = "~/PresentationLayer/masterrecord/modifyContact.aspx?contacttype=" & strConType & "&customid=" & Me.custid.Text.ToUpper() & "&Prefix=" & Me.CustomerPrefix.Text.ToUpper() & "&consymbol=1"
            xHyp.Visible = purviewArray(2)
        Next
        Call DisplayGridHeader()
        Dim obiXmlTr As New clsXml
        obiXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        If contactall.Tables(0).Rows.Count >= 1 Then
            contactView.HeaderRow.Cells(1).Text = "Telephone Type" 'obiXmlTr.GetLabelName("EngLabelMsg", "TELEPHONETYPE")
            contactView.HeaderRow.Cells(1).Font.Size = 8
            contactView.HeaderRow.Cells(2).Text = "Status" 'obiXmlTr.GetLabelName("EngLabelMsg", "STATUS")
            contactView.HeaderRow.Cells(2).Font.Size = 8
            contactView.HeaderRow.Cells(3).Text = "PIC Name" 'obiXmlTr.GetLabelName("EngLabelMsg", "PICNAME")
            contactView.HeaderRow.Cells(3).Font.Size = 8
            contactView.HeaderRow.Cells(4).Text = "Telephone" 'obiXmlTr.GetLabelName("EngLabelMsg", "TELEPHONE")
            contactView.HeaderRow.Cells(4).Font.Size = 8
        End If
    End Sub
#End Region
#End Region

    Protected Sub addressView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles addressView.PageIndexChanging
        Me.addressView.PageIndex = e.NewPageIndex
        viewaddressviewbond()
        viewcontactviewbond()
        viewroviewbond()
    End Sub

    Protected Sub contactView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles contactView.PageIndexChanging
        Me.addressView.PageIndex = e.NewPageIndex
        viewaddressviewbond()
        viewcontactviewbond()
        viewroviewbond()
    End Sub

    Protected Sub roView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles roView.PageIndexChanging
        Me.addressView.PageIndex = e.NewPageIndex
        loadaddressviewbond()
        loadcontactviewbond()
        viewroviewbond()
    End Sub

    Protected Sub saveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveButton.Click

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim msginfo As String = "Do you really want to save?"'objXm.GetLabelName("StatusMessage", "SAVEORNOT")
        Dim msgtitle As String = "Do you really want to save?"'objXm.GetLabelName("StatusMessage", "SAVETITLE")

        Dim selectedareaid As String = Request.Form("areaid")

        Me.areaid.Items.Clear()
        Dim rank As String = Session("login_rank")
        Dim area As New clsCommonClass
        If (ctrid.SelectedValue().ToString() <> "") Then
            area.spctr = Me.ctrid.SelectedValue().ToString().ToUpper()
        End If
        If (staid.SelectedValue().ToString() <> "") Then
            area.spstat = Me.staid.SelectedValue().ToString().ToUpper()
        End If
        area.rank = rank
        Dim areads As New DataSet
        areads = area.Getcomidname("BB_MASAREA_IDNAME")
        If areads.Tables.Count <> 0 Then
            databonds(areads, Me.areaid)
        End If
        Me.areaid.Items.Insert(0, "")

        'areaid.SelectedIndex = areaid.Items.IndexOf(areaid.Items.FindByValue(selectedareaid))
        areaid.SelectedValue = selectedareaid

        Dim selectedRaces As String = Request.Form("Races")
        changeCustomerType(Me.CustomerType.SelectedValue)
        'Races.SelectedIndex = Races.Items.IndexOf(Races.Items.FindByValue(selectedRaces))
        Races.SelectedValue = selectedRaces


        MessageBox1.Confirm(msginfo)
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    End Sub

    Protected Sub HyperLink2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HyperLink2.Click
        Dim url As String = "~/PresentationLayer/masterrecord/addinstalleBase.aspx?"
        url &= "custid=" & Me.custid.Text.ToString() & "&"
        url &= "custpre=" & Me.CustomerPrefix.Text.ToString() & "&"
        url &= "custname=" & Me.CustomerName.Text.ToString()
        Response.Redirect(url)

    End Sub

    Protected Sub MessageBox1_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox1.GetMessageBoxResponse
        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then

            Dim CustomerEntity As New clsCustomerRecord()
            Dim addressEntity As New clsAddress()
            'initialize customer details info
            If (Trim(Me.custid.Text) <> "") Then
                CustomerEntity.CustomerID = Me.custid.Text.ToString().ToUpper()
            End If
            If (Trim(Me.CustomerPrefix.Text) <> "") Then
                CustomerEntity.Customerpf = Me.CustomerPrefix.Text.ToString().ToUpper()
            End If
            If (Trim(Me.JDECID.Text) <> "") Then
                CustomerEntity.JDECustID = Me.JDECID.Text.ToString().ToUpper()
            End If
            If (Trim(Me.CustomerName.Text) <> "") Then
                CustomerEntity.CustomerName = Me.CustomerName.Text.ToString().ToUpper()
            End If
            If (Trim(Me.AlternateName.Text) <> "") Then
                CustomerEntity.AlterName = Me.AlternateName.Text.ToString().ToUpper()
            End If
            If (Me.Races.SelectedValue().ToString() <> "") Then
                CustomerEntity.Races = Me.Races.SelectedValue().ToString().ToUpper()
            End If
            If (Me.CustomerType.SelectedValue().ToString() <> "") Then
                CustomerEntity.CustType = Me.CustomerType.SelectedValue().ToString()
            End If
            If (Me.status.SelectedValue().ToString() <> "") Then
                CustomerEntity.Status = Me.status.SelectedValue().ToString()
            End If
            If (Trim(Me.notes.Text) <> "") Then
                CustomerEntity.Notes = Me.notes.Text.ToString().ToUpper()
            End If
            If (Me.SVCID.SelectedValue().ToString() <> "") Then
                CustomerEntity.Svcid = Me.SVCID.SelectedValue().ToString()
            End If
            If (Trim(Me.creatby.Text) <> "") Then
                CustomerEntity.CreatBy = Session("userID").ToString().ToUpper
            End If
            If (Trim(Me.modfyby.Text) <> "") Then
                CustomerEntity.ModBy = Session("userID").ToString().ToUpper
            End If

            'get selected preferred language - Added by LLY 201702
            If (Me.prefLang.SelectedValue().ToString() <> "") Then
                CustomerEntity.PrefLang = Me.prefLang.SelectedValue.Substring(9, Me.prefLang.SelectedValue.ToString.Length - 9) 'PREFLANG-MALAY
            End If

            If (Trim(Me.CustomerEmail.Text) <> "") Then
                CustomerEntity.email = Me.CustomerEmail.Text.ToString().ToUpper
            End If
            CustomerEntity.Ipadress = Request.UserHostAddress.ToString()
            CustomerEntity.usvcid = Session("login_svcID")
            CustomerEntity.username = Session("userID")
            '----------------------------------------------------------

            'initialize customer address
            addressEntity.CustPrefix = Me.CustomerPrefix.Text.ToString().ToUpper()
            If (Trim(Me.custid.Text) <> "") Then
                addressEntity.CustomerID = custid.Text.ToUpper()
            End If
            If (Trim(Me.Address1.Text) <> "") Then
                addressEntity.Address1 = Address1.Text.ToUpper()
            End If
            If (Trim(Me.Address2.Text) <> "") Then
                addressEntity.Address2 = Me.Address2.Text.ToString().ToUpper()
            End If
            If (areaid.SelectedValue().ToString() <> "") Then
                addressEntity.AreaID = Me.areaid.SelectedValue().ToString().ToUpper()
            End If
            If (ctrid.SelectedValue().ToString() <> "") Then
                addressEntity.CountryID = Me.ctrid.SelectedValue().ToString().ToUpper()
            End If
            If (staid.SelectedValue().ToString() <> "") Then
                addressEntity.StateID = Me.staid.SelectedValue().ToString().ToUpper()
            End If
            If (Trim(Me.POCode.Text.ToString()) <> "") Then
                addressEntity.POCode = Me.POCode.Text.ToString().ToUpper()
            End If
            If (Trim(creatby.Text) <> "") Then
                addressEntity.CreatedBy = Session("userID").ToString().ToUpper
            End If
            If (Trim(modfyby.Text) <> "") Then
                addressEntity.ModifiedBy = Session("userID").ToString().ToUpper
            End If
            addressEntity.Ipadress = Request.UserHostAddress.ToString()
            addressEntity.Svcid = Session("login_svcID")
            addressEntity.username = Session("userID")
            '------------------------------

            If Me.status.SelectedValue().ToString() = "DELETE" Then
                Dim delecust As Integer = CustomerEntity.GetcustDel()
                If delecust.Equals(-1) Then
                    Dim objXm As New clsXml
                    objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    errlab.Text = "Failed to delete because the customer refer to other information!"'objXm.GetLabelName("StatusMessage", "DUPCUSTROU")
                    errlab.Visible = True
                    MessageBox1.Alert(errlab.Text)
                ElseIf delecust.Equals(0) Then
                    Dim insCtryCnt As Integer = CustomerEntity.Update()
                    Dim objXmlTr As New clsXml
                    objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    If insCtryCnt = 0 Then
                        ''added by deyb 18-Jul-2006
                        'Dim insUpd2 As Integer = addressEntity.Update2()
                        'If insUpd2 = 0 Then
                        Dim objXm As New clsXml
                        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                        errlab.Text = "Data have been saved successfully!"'objXm.GetLabelName("StatusMessage", "BB-HintMsg-Suc")
                        errlab.Visible = True
                        MessageBox1.Alert(errlab.Text)
                        'saveButton.Enabled = False
                       
                        '------------------------------
                    Else
                        MessageBox1.Alert(fstrErrorSaveMessage & " 1")
                        'Response.Redirect("~/PresentationLayer/Error.aspx")
                    End If
                End If
            Else
                Dim insCtryCnt As Integer = CustomerEntity.Update()
                Dim objXmlTr As New clsXml
                objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                If insCtryCnt = 0 Then
                    'added by deyb 18-Jul-2006
                    Dim insUpd2 As Integer = addressEntity.Update2()
                    If insUpd2 = 0 Then
                        Dim objXm As New clsXml
                        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                        errlab.Text = "Data have been saved successfully!"'objXm.GetLabelName("StatusMessage", "BB-HintMsg-Suc")
                        errlab.Visible = True
                        MessageBox1.Alert(errlab.Text)
                        'saveButton.Enabled = False
                    Else
                        MessageBox1.Alert(fstrErrorSaveMessage & " 2")
                        'Response.Redirect("~/PresentationLayer/Error.aspx")
                    End If
                    '------------------------------
                Else
                    MessageBox1.Alert(fstrErrorSaveMessage & " 3")
                    'Response.Redirect("~/PresentationLayer/Error.aspx")
                End If
                AddNewContact()

            End If
            viewaddressviewbond()
            viewcontactviewbond()
            viewroviewbond()
        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
        End If
        If Me.status.SelectedValue.ToString <> "ACTIVE" Then
            Me.HyperLink2.Enabled = False
        End If

        ' Added by Ryan Estandarte 8 Mar 2012

        If Not String.IsNullOrEmpty(Request.QueryString("type")) And Not String.IsNullOrEmpty(Request.QueryString("no")) Then
            Dim typeID As String = "type=" + Request.QueryString("type").ToString()
            Dim transNo As String = "no=" + Request.QueryString("no").ToString()

            Dim updateServBillURL As String = "~/PresentationLayer/Function/ServiceBillUpdate2.aspx?" + typeID + "&" + transNo

            Response.Redirect(updateServBillURL)
        End If

    End Sub

    Private Function AddNewContact()
        'tele1 - dist contact
        'tele2 - customer tele(H)
        'tele3 - customer tele(o)
        'tele4 - customer fax
        'tele5 - customer mob1
        'tele6 - Customer mob2
        'DistEmail - Distributor Email
        Dim contactEntity As New clsContact()
        Dim telepass As New clsCommonClass()

        If (Trim(Me.custid.Text) <> "") Then
            contactEntity.CustomerID = custid.Text.ToUpper()
            contactEntity.CustPrefix = Me.CustomerPrefix.Text.Trim()
        End If

        Dim i As Integer
        'add the records
        For i = 1 To 6 'loop through all the contacts provided          

            'get the tele text
            Select Case i
                Case 1

                    'If (Trim(tele1.Text) <> "") Then
                    contactEntity.Telephone = telepass.telconvertpass(tele1.Text)
                    contactEntity.TelephType = "DIST"

                    contactEntity.PICName = Me.DistName.Text.ToString()

                    'End If
                Case 2

                    'If (Trim(tele2.Text) <> "") Then
                    contactEntity.Telephone = telepass.telconvertpass(tele2.Text)
                    contactEntity.TelephType = "TELH"

                    contactEntity.PICName = Me.txtRemark2.Text.ToString()
                    'End If
                Case 3

                    'If (Trim(tele3.Text) <> "") Then
                    contactEntity.Telephone = telepass.telconvertpass(tele3.Text)
                    contactEntity.TelephType = "TELO"
                    contactEntity.PICName = Me.txtRemark3.Text.ToString()
                    'End If
                Case 4

                    'If (Trim(tele4.Text) <> "") Then
                    contactEntity.Telephone = telepass.telconvertpass(tele4.Text)
                    contactEntity.TelephType = "FAX"
                    contactEntity.PICName = Me.txtRemark4.Text.ToString()
                    'End If
                Case 5

                    'If (Trim(tele5.Text) <> "") Then
                    contactEntity.Telephone = telepass.telconvertpass(tele5.Text)
                    contactEntity.TelephType = "MOB1"
                    contactEntity.PICName = Me.txtRemark5.Text.ToString()
                    'End If
                Case 6

                    'If (Trim(tele6.Text) <> "") Then
                    contactEntity.Telephone = telepass.telconvertpass(tele6.Text)
                    contactEntity.TelephType = "MOB2"
                    contactEntity.PICName = Me.txtRemark6.Text.ToString()
                    'End If
            End Select

            If Not contactEntity.Telephone = "" Then

                If (Trim(Me.DistEmail.Text) <> "") Then
                    contactEntity.Email = Me.DistEmail.Text.ToString()
                End If
                If (Me.status.SelectedItem.Value.ToString() <> "") Then
                    contactEntity.Status = status.SelectedItem.Value
                End If
                'If (Trim(Me.DistName.Text) <> "") Then
                '    contactEntity.PICName = Me.DistName.Text.ToString()
                'End If
                If (Trim(creatbybox.Text) <> "") Then
                    contactEntity.CreatedBy = Session("userID").ToString().ToUpper
                End If
                If (Trim(modfybybox.Text) <> "") Then
                    contactEntity.ModifiedBy = Session("userID").ToString().ToUpper
                End If

                contactEntity.username = Session("userID")
                'added by deyb
                Dim contactall As DataSet = contactEntity.GetCustContacts(custid.Text.ToUpper(), Me.CustomerPrefix.Text.Trim())
                Dim insCtryCnt As Integer
                Dim x As Integer
                Dim xExists As Boolean
                'check if got existing record
                If contactall.Tables(0).Rows.Count > 0 Then
                    For x = 0 To contactall.Tables(0).Rows.Count - 1
                        Dim xTelType As String = Trim(contactall.Tables(0).Rows(x).Item("MTEL_TELTY").ToString)
                        If contactEntity.TelephType = xTelType Then
                            xExists = True
                            Exit For
                        Else
                            xExists = False
                        End If
                    Next
                    If xExists = True Then 'if got existing tel type - update
                        insCtryCnt = contactEntity.Update()
                    Else 'if no existing tel type - insert
                        insCtryCnt = contactEntity.Insert()
                    End If
                    Dim objXmlTr As New clsXml
                    objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    If insCtryCnt = 0 Then
                        Dim objXm As New clsXml
                        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                        errlab.Text = "Data have been saved successfully!"'objXm.GetLabelName("StatusMessage", "BB-HintMsg-Suc")
                        errlab.Visible = True
                        MessageBox1.Alert(errlab.Text)
                    Else
                        MessageBox1.Alert(fstrErrorSaveMessage & " 4")
                        'Response.Redirect("~/PresentationLayer/Error.aspx")
                    End If
                Else ' if not existing record
                    insCtryCnt = contactEntity.Insert()
                    Dim objXmlTr As New clsXml
                    objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    If insCtryCnt = 0 Then
                        Dim objXm As New clsXml
                        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                        errlab.Text = "Data have been saved successfully!"'objXm.GetLabelName("StatusMessage", "BB-HintMsg-Suc")
                        errlab.Visible = True
                        MessageBox1.Alert(errlab.Text)
                    Else
                        MessageBox1.Alert(fstrErrorSaveMessage & " 5")
                        'Response.Redirect("~/PresentationLayer/Error.aspx")
                    End If
                End If

            Else 'ADDED BY RAYMOND -- DELETE CONTACTS
                Dim contactall As DataSet = contactEntity.GetCustContacts(custid.Text.ToUpper(), Me.CustomerPrefix.Text.Trim())
                Dim insCtryCnt As Integer
                Dim x As Integer
                Dim xExists As Boolean
                'check if got existing record
                If contactall.Tables(0).Rows.Count > 0 Then
                    For x = 0 To contactall.Tables(0).Rows.Count - 1
                        Dim xTelType As String = Trim(contactall.Tables(0).Rows(x).Item("MTEL_TELTY").ToString)
                        If contactEntity.TelephType = xTelType Then
                            xExists = True
                            Exit For
                        Else
                            xExists = False
                        End If
                    Next
                    If xExists = True Then 'if got existing tel type - update
                        insCtryCnt = contactEntity.Delete()



                        Dim objXmlTr As New clsXml
                        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                        If insCtryCnt = 0 Then
                            Dim objXm As New clsXml
                            objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                            errlab.Text = "Data have been saved successfully!"'objXm.GetLabelName("StatusMessage", "BB-HintMsg-Suc")
                            errlab.Visible = True
                            MessageBox1.Alert(errlab.Text)
                        Else
                            MessageBox1.Alert(fstrErrorSaveMessage & " 6")
                            'Response.Redirect("~/PresentationLayer/Error.aspx")
                        End If

                    End If


                End If
            End If


        Next
    End Function


    Protected Sub CustomerType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CustomerType.SelectedIndexChanged
        If Me.CustomerType.SelectedValue.ToString = "CORPORATE" Then
            Me.Races.Items.Clear()
            Dim race As New clsrlconfirminf()
            Dim raceParam As ArrayList = New ArrayList
            raceParam = race.searchconfirminf("PERSONRACE")
            Dim racecount As Integer
            Dim raceid As String
            Dim racenm As String
            For racecount = 0 To raceParam.Count - 1
                raceid = raceParam.Item(racecount)
                racenm = raceParam.Item(racecount + 1)
                racecount = racecount + 1
                If raceid.Equals("ZNONAPPL") Then
                    Me.Races.Items.Add(New ListItem(racenm.ToString(), raceid.ToString()))
                End If
            Next
        Else
            Me.Races.Items.Clear()
            Dim race As New clsrlconfirminf()
            Dim raceParam As ArrayList = New ArrayList
            raceParam = race.searchconfirminf("PERSONRACE")
            Dim racecount As Integer
            Dim raceid As String
            Dim racenm As String
            For racecount = 0 To raceParam.Count - 1
                raceid = raceParam.Item(racecount)
                racenm = raceParam.Item(racecount + 1)
                racecount = racecount + 1

                Me.Races.Items.Add(New ListItem(racenm.ToString(), raceid.ToString()))
            Next
        End If
    End Sub

    Protected Sub ctrid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ctrid.SelectedIndexChanged
        GetStateID()
        'Dim script As String = "self.location='#ctry';"
        'Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "AddrLoc", script, True)
    End Sub

    Private Function GetStateID()
        Me.areaid.Items.Clear()
        'If Me.ctrid.Text <> "" Then
        Dim rank As String = Session("login_rank")
        Dim state As New clsCommonClass
        If (Me.ctrid.SelectedValue().ToString() <> "") Then
            state.spctr = Me.ctrid.SelectedValue().ToString().ToUpper()
        End If
        state.rank = rank
        Dim stateds As New DataSet
        stateds = state.Getcomidname("BB_MASSTAT_IDNAME")
        If stateds.Tables.Count <> 0 Then
            databonds(stateds, Me.staid)
        End If
        Me.staid.Items.Insert(0, "")
    End Function

    Protected Sub staid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles staid.SelectedIndexChanged
        GetAreaID()
        'Dim script As String = "self.location='#ctry';"
        'Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "AddrLoc", script, True)
    End Sub

    Private Function GetAreaID()
        Me.areaid.Items.Clear()
        Dim rank As String = Session("login_rank")
        Dim area As New clsCommonClass
        If (ctrid.SelectedValue().ToString() <> "") Then
            area.spctr = Me.ctrid.SelectedValue().ToString().ToUpper()
        End If
        If (staid.SelectedValue().ToString() <> "") Then
            area.spstat = Me.staid.SelectedValue().ToString().ToUpper()
        End If
        area.rank = rank
        Dim areads As New DataSet
        areads = area.Getcomidname("BB_MASAREA_IDNAME")
        If areads.Tables.Count <> 0 Then
            databonds(areads, Me.areaid)
        End If
        Me.areaid.Items.Insert(0, "")
    End Function

    Protected Sub lblViewInst_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblViewInst.Click
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim rouid As String = ""
        Dim gSelectedValue As String
        Dim strCusID As String = ""
        Dim strPrefix As String = Session("login_ctryid")
        Dim strSerial As String = ""
        Dim strMod As String = ""
        Dim strTempURL As String = ""

        strCusID = Me.custid.Text
        strSerial = Me.ModelSerial.Text
        strMod = Me.ModelSerial.Text

        strCusID = Me.custid.Text
        'try get the ROUID
        Dim CustomerEntity As New clsCustomerRecord()
        CustomerEntity.CustomerID = strCusID
        CustomerEntity.RoSerialNo = strSerial
        CustomerEntity.modelid = strMod
        CustomerEntity.Customerpf = strPrefix
        rouid = CustomerEntity.GetInstallROUID()


    End Sub

    Protected Sub lblCreateApp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblCreateApp.Click
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim strTempURL As String
        Dim strCusID As String = ""
        Dim strPrefix As String = Session("login_ctryid")
        Dim strCusName As String = ""
        strCusID = Me.custid.Text
        strCusName = Me.CustomerName.Text
        strCusID = Server.UrlEncode(strCusID)
        strPrefix = Server.UrlEncode(strPrefix)
        If Not Request.Params("ROID") = "" Then
            strTempURL = "custID=" + strCusID + "&custPf=" + strPrefix + "&ROID=" + Request.Params("ROID")
        Else
            strTempURL = "custID=" + strCusID + "&custPf=" + strPrefix
        End If

        strTempURL = "~/PresentationLayer/function/addappointment.aspx?" + strTempURL
        Response.Redirect(strTempURL)
    End Sub

    Protected Sub linkAddAddress_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles linkAddAddress.Click
        Dim strTempURL As String
        Dim strPrefix As String = Session("login_ctryid")
        Dim strCusName As String = Me.CustomerName.Text
        Dim strCusID As String = Me.custid.Text
        strCusID = Server.UrlEncode(strCusID)
        strPrefix = Server.UrlEncode(strPrefix)
        strTempURL = "custID=" + strCusID + "&custPf=" + strPrefix + "&custname=" + strCusName + "&symbol=1"
        strTempURL = "~/PresentationLayer/masterrecord/addaddress.aspx?" + strTempURL
        Response.Redirect(strTempURL)
    End Sub

    Protected Sub LinkButton2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton2.Click
        Dim strTempURL As String
        Dim strPrefix As String = Session("login_ctryid")
        Dim strCusName As String = Me.CustomerName.Text
        Dim strCusID As String = Me.custid.Text
        strCusID = Server.UrlEncode(strCusID)
        strPrefix = Server.UrlEncode(strPrefix)
        strTempURL = "custID=" + strCusID + "&custPf=" + strPrefix + "&custname=" + strCusName + "&consymbol=1"
        strTempURL = "~/PresentationLayer/masterrecord/addcontact.aspx?" + strTempURL
        Response.Redirect(strTempURL)
    End Sub



    Sub changeCustomerType(ByVal strCustomerType As String)

        ''Lyann 20180601 SMR 1805/2423 Changes in Customer Type. 
        'If strCustomerType = "INDIVIDUAL" Then
        If strCustomerType = "INDIVIDUAL" Or strCustomerType.ToUpper() = "RESIDENTIAL" Or strCustomerType.ToUpper() = "RENTAL (RESIDENTIAL)" Then
            Me.Races.Items.Clear()
            Dim race As New clsrlconfirminf()
            Dim raceParam As ArrayList = New ArrayList
            raceParam = race.searchconfirminf("PERSONRACE")
            Dim racecount As Integer
            Dim raceid As String
            Dim racenm As String
            For racecount = 0 To raceParam.Count - 1
                raceid = raceParam.Item(racecount)
                racenm = raceParam.Item(racecount + 1)
                racecount = racecount + 1
                If Not raceid.Equals("ZNONAPPL") Then


                    Me.Races.Items.Add(New ListItem(racenm.ToString(), raceid.ToString()))
                End If

            Next

        Else

            Me.Races.Items.Clear()
            Dim race As New clsrlconfirminf()
            Dim raceParam As ArrayList = New ArrayList
            raceParam = race.searchconfirminf("PERSONRACE")
            Dim racecount As Integer
            Dim raceid As String
            Dim racenm As String
            For racecount = 0 To raceParam.Count - 1
                raceid = raceParam.Item(racecount)
                racenm = raceParam.Item(racecount + 1)
                racecount = racecount + 1
                If raceid.Equals("ZNONAPPL") Then
                    Me.Races.Items.Add(New ListItem(racenm.ToString(), raceid.ToString()))
                End If

            Next

        End If
    End Sub

#Region "Ajax"

    <AjaxPro.AjaxMethod()> _
    Public Function GetAreas(ByVal CountryID As String, ByVal StateID As String) As ArrayList
        Dim area As New clsCommonClass

        area.spctr = CountryID
        area.spstat = StateID

        area.rank = 7
        Dim areads As New DataSet
        areads = area.Getcomidname("BB_MASAREA_IDNAME")
        Dim strTemp As String


        Dim Listas As New ArrayList(areads.Tables(0).Rows.Count)
        Dim i As Integer = 0
        Listas.Add("")
        Try
            For i = 0 To areads.Tables(0).Rows.Count - 1
                strTemp = areads.Tables(0).Rows(i).Item(0).ToString & ":" & areads.Tables(0).Rows(i).Item(0).ToString & "-" & areads.Tables(0).Rows(i).Item(1).ToString
                Listas.Add(strTemp)
            Next
        Catch
        End Try

        Return Listas
    End Function

    <AjaxPro.AjaxMethod()> _
        Public Function GetRaces(ByVal strCustomerType As String) As ArrayList
        Dim race As New clsrlconfirminf()
        Dim raceParam As ArrayList = New ArrayList
        raceParam = race.searchconfirminf("PERSONRACE")
        Dim racecount As Integer
        Dim raceid As String
        Dim racenm As String
        Dim Listas As New ArrayList(raceParam.Count / 2)
        Dim icnt As Integer = 0
        Dim strTemp As String

        ''Lyann 20180601 SMR 1805/2423 Changes in Customer Type. 
        ''If strCustomerType = "CORPORATE" Or strCustomerType = "GOVERNMENT (2)" Or strCustomerType = "GOVERNMENT (3)" Or strCustomerType = "GOVERNMENT (8)" Then 'Tomas 20150302
        If strCustomerType = "CORPORATE" Or strCustomerType = "GOVERNMENT" Or strCustomerType = "RENTAL (CORPORATE)" Then

            For racecount = 0 To raceParam.Count - 1
                raceid = raceParam.Item(racecount)
                racenm = raceParam.Item(racecount + 1)
                racecount = racecount + 1
                If raceid.Equals("ZNONAPPL") Then
                    strTemp = raceid & ":" & racenm
                    Listas.Add(strTemp)
                End If

            Next
        Else

            For racecount = 0 To raceParam.Count - 1
                raceid = raceParam.Item(racecount)
                racenm = raceParam.Item(racecount + 1)
                racecount = racecount + 1
                If Not raceid.Equals("ZNONAPPL") Then
                    strTemp = raceid & ":" & racenm
                    Listas.Add(strTemp)
                End If

            Next

        End If

        Return Listas
    End Function
#End Region

    Protected Sub contactView_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles contactView.SelectedIndexChanged

    End Sub


    Protected Sub SendSMS1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles SendSMS1.Click

        Session("Tele") = tele5.Text

        Const script As String = "window.open('../masterrecord/modifyCustomer_SMS.aspx')"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "modifyCustomer_SMS", script, True)
    End Sub

    Protected Sub SendSMS2_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles SendSMS2.Click
        Session("Tele") = tele6.Text

        Const script As String = "window.open('../masterrecord/modifyCustomer_SMS.aspx')"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "modifyCustomer_SMS", script, True)
    End Sub
End Class
