Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data

Partial Class PresentationLayer_masterrecord_PriceSetUp
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Page.IsPostBack) Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try
            Me.priceIDBox.Focus()
            BindGrid()
        End If
        Me.priceIDBox.Focus()
    End Sub
    Protected Sub BindGrid()
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        Me.statusLAB.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Status")
        Me.PartIDlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0002")
        Me.CountryIDlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0003")
        Me.LinkButton1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Add")
        Me.LinkButton2.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Search")
        Me.titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0008")
        Me.Label1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-NR")
        Me.Label1.Visible = False
        Me.addinfo.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-AR")
        Me.addinfo.Visible = False


        Dim rank As String = Session("login_rank").ToString().ToUpper()
        Dim country As New clsCommonClass
        If rank <> 0 Then
            country.spctr = Session("login_ctryID").ToString().ToUpper()
        End If
        country.rank = rank
        'Dim countryds As New DataSet
        'countryds = country.Getcomidname("BB_MASCTRY_IDNAME")
        'If countryds.Tables.Count <> 0 Then
        '    databonds(countryds, Me.CountryIDDrop)
        '    Me.CountryIDDrop.Items.Insert(0, "")
        'End If
     

        Dim prcsStat As New clsrlconfirminf()
        Dim statParam As ArrayList = New ArrayList
        statParam = prcsStat.searchconfirminf("STATUS")
        Dim count As Integer
        Dim statid As String
        Dim statnm As String
        For count = 0 To statParam.Count - 1
            statid = statParam.Item(count)
            statnm = statParam.Item(count + 1)
            count = count + 1
            If (statid.Equals("ACTIVE") Or statid.Equals("OBSOLETE")) Then
                Me.statusDrop.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
            End If
        Next
        statusDrop.Items(0).Selected = True
        Dim partid As New clsCommonClass
        If rank <> 0 Then
            partid.spctr = Session("login_ctryID").ToString().ToUpper()

        End If
        partid.rank = rank
        'Dim partidds As New DataSet
        'partidds = partid.Getcomidname("BB_MASPARTID_IDNAME")
        'If partidds.Tables.Count <> 0 Then
        '    databonds(partidds, Me.PartIDDrop)
        '    Me.PartIDDrop.Items.Insert(0, "")
        'End If

        



        Dim PriceSetUpEntity As New clsPriceSetUp()
        If rank <> 0 Then
            PriceSetUpEntity.CountryID = Session("login_ctryID").ToString().ToUpper()
        End If
        PriceSetUpEntity.rank = rank
        If (statusDrop.SelectedValue().ToString() <> "") Then
            PriceSetUpEntity.PriceSetUpStatus = statusDrop.SelectedItem.Value.ToString()
        End If
        Dim PriceSetUpall As DataSet = PriceSetUpEntity.GetPriceSetUp()
        PriceSetUpView.DataSource = PriceSetUpall
        Session("PriceSetUpView") = PriceSetUpall
        checknorecord(PriceSetUpall)

        Dim editcol As New CommandField
        editcol.EditText = objXmlTr.GetLabelName("EngLabelMsg", "BB-Edit") '"edit"
        editcol.HeaderImageUrl = "~/PresentationLayer/graph/edit.gif"
        editcol.ShowEditButton = True
        PriceSetUpView.Columns.Add(editcol)

        '''access control'''''''''''''''''''''''''''''''''''''''''''''''
        Dim accessgroup As String = Session("accessgroup").ToString
        Dim purviewArray As Array = New Boolean() {False, False, False, False, False}
        purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "14")
        Me.LinkButton1.Enabled = purviewArray(0)
        editcol.Visible = purviewArray(2)


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


        PriceSetUpView.DataBind()
        If PriceSetUpall.Tables(0).Rows.Count <> 0 Then
            PriceSetUpView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0014")
            PriceSetUpView.HeaderRow.Cells(1).Font.Size = 8
            PriceSetUpView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0001")
            PriceSetUpView.HeaderRow.Cells(2).Font.Size = 8
            PriceSetUpView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0002")
            PriceSetUpView.HeaderRow.Cells(3).Font.Size = 8
            PriceSetUpView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0003")
            PriceSetUpView.HeaderRow.Cells(4).Font.Size = 8
            PriceSetUpView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Status")
            PriceSetUpView.HeaderRow.Cells(5).Font.Size = 8


        End If

    End Sub
#Region " id bond"
    Public Function databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        Dim row As DataRow
        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            NewItem.Text = row("id") & "-" & row("name")
            NewItem.Value = row("id")
            dropdown.Items.Add(NewItem)
        Next

    End Function
#End Region
    Protected Sub LinkButton2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton2.Click
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        PriceSetUpView.PageIndex = 0
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        Dim PriceSetUpEntity As New clsPriceSetUp()
        Dim rank As String = Session("login_rank").ToString().ToUpper()
        PriceSetUpEntity.rank = rank
        If (Trim(Me.priceIDBox.Text) <> "") Then
            PriceSetUpEntity.PartID = priceIDBox.Text.ToString()
        
        End If

        'If (Trim(Me.ctryBox.Text) <> "") Then
        '    PriceSetUpEntity.CountryID = ctryBox.Text.ToString()
        'Else
        If rank = "0" Then
            PriceSetUpEntity.CountryID = "%"
        Else
            PriceSetUpEntity.CountryID = Session("login_ctryID").ToString().ToUpper()
        End If
        'End If
        If (statusDrop.SelectedValue().ToString() <> "") Then
            PriceSetUpEntity.PriceSetUpStatus = statusDrop.SelectedItem.Value.ToString()
        End If

        checkinfo()
        PriceSetUpEntity.ModifiedBy = Session("userID")
        Dim selDS As DataSet = PriceSetUpEntity.GetPriceSetUp()
        PriceSetUpView.DataSource = selDS
        PriceSetUpView.DataBind()
        Session("PriceSetUpView") = selDS
        checknorecord(selDS)
        If selDS.Tables(0).Rows.Count <> 0 Then
            PriceSetUpView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0014")
            PriceSetUpView.HeaderRow.Cells(1).Font.Size = 8
            PriceSetUpView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0001")
            PriceSetUpView.HeaderRow.Cells(2).Font.Size = 8
            PriceSetUpView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0002")
            PriceSetUpView.HeaderRow.Cells(3).Font.Size = 8
            PriceSetUpView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0003")
            PriceSetUpView.HeaderRow.Cells(4).Font.Size = 8
            PriceSetUpView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Status")
            PriceSetUpView.HeaderRow.Cells(5).Font.Size = 8



        End If

    End Sub




    Protected Sub PriceSetUpView_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles PriceSetUpView.PageIndexChanging
        PriceSetUpView.PageIndex = e.NewPageIndex
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Dim PriceSetUpEntity As New clsPriceSetUp()
        Dim rank As String = Session("login_rank").ToString().ToUpper()
        PriceSetUpEntity.rank = rank
        If (Trim(Me.priceIDBox.Text) <> "") Then
            PriceSetUpEntity.PartID = priceIDBox.Text.ToString()

        End If
        'If (Trim(Me.ctryBox.Text) <> "") Then
        '    PriceSetUpEntity.CountryID = ctryBox.Text.ToString()
        'Else
        If rank = "0" Then
            PriceSetUpEntity.CountryID = "%"
        Else
            PriceSetUpEntity.CountryID = Session("login_ctryID").ToString().ToUpper()
        End If
        'End If
        If (statusDrop.SelectedValue().ToString() <> "") Then
            PriceSetUpEntity.PriceSetUpStatus = statusDrop.SelectedItem.Value.ToString()
        End If
        Dim selDS As DataSet = PriceSetUpEntity.GetPriceSetUp()
        PriceSetUpView.DataSource = selDS
        PriceSetUpView.DataBind()
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        If selDS.Tables(0).Rows.Count <> 0 Then
            PriceSetUpView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0014")
            PriceSetUpView.HeaderRow.Cells(1).Font.Size = 8
            PriceSetUpView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0001")
            PriceSetUpView.HeaderRow.Cells(2).Font.Size = 8
            PriceSetUpView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0002")
            PriceSetUpView.HeaderRow.Cells(3).Font.Size = 8
            PriceSetUpView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0003")
            PriceSetUpView.HeaderRow.Cells(4).Font.Size = 8
            PriceSetUpView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Status")
            PriceSetUpView.HeaderRow.Cells(5).Font.Size = 8
        End If
    End Sub

    Protected Sub PriceSetUpView_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles PriceSetUpView.RowEditing
        Dim ds As DataSet = Session("PriceSetUpView")
        Dim page As Integer = PriceSetUpView.PageIndex * PriceSetUpView.PageSize
        Dim prcid As String = Me.PriceSetUpView.Rows(e.NewEditIndex).Cells(1).Text
        prcid = Server.UrlEncode(prcid)
        'Dim status As String = ds.Tables(0).Rows(page + e.NewEditIndex).Item(4).ToString
        'status = Server.UrlEncode(status)
        Dim strTempURL As String = "PriceSetUpID=" + prcid
        '+ "&statusid=" + status
        strTempURL = "~/PresentationLayer/masterrecord/modifyPriceSetUp.aspx?" + strTempURL
        Response.Redirect(strTempURL)
    End Sub
#Region "if have no record "
    Public Function checknorecord(ByVal ds As DataSet) As Integer
        If ds.HasErrors Then
            If ds.Tables(0).Rows.Count = 0 Then
                Me.Label1.Visible = True
                'Me.addinfo.Visible = False
            Else
                Me.Label1.Visible = False
            End If

        End If
        If ds.Tables(0).Rows.Count = 0 Then
            Me.Label1.Visible = True
            'Me.addinfo.Visible = False
        Else
            Me.Label1.Visible = False
        End If

    End Function
#End Region
#Region "��ʾ��Ϣ"
    Public Function checkinfo() As Integer
        If Me.ctryBox.Text = "" And Me.priceIDBox.Text = "" Then
            Me.addinfo.Visible = True
        Else
            Me.addinfo.Visible = False
        End If
    End Function
#End Region
End Class
