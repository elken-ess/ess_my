Imports BusinessEntity
Imports System.Configuration
Imports System.Xml
Imports System.Data
Imports System.Data.SqlClient
Partial Class PresentationLayer_masterrecord_modifyContact
    Inherits System.Web.UI.Page

    Dim contactEntity As New clsContact()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try

            Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "04")
            If purviewArray(2) = False Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return

            End If
            Me.LinkButton1.Enabled = purviewArray(1)
            Dim mdtype As String
            'Dim mdfystat As String = Request.Params("contactstat").ToString()
            Dim mdfyid As String
            Dim mdfypref As String
            '''''''''''''''''''''''''''''''''''''''''''''''''''''
            Try
                mdtype = Request.Params("contacttype").ToString()
                mdfyid = Request.QueryString("customid")
                mdfypref = Request.QueryString("Prefix")
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            custidlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0001")
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "MODCONTACT")
            Me.statusLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCTRY-0004")
            Me.TelepTypelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASTELETYPE")

            Me.PIClab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0003")
            Me.Telephonelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASTELE")
            Me.Emaillab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASEmail")

            Me.rouidLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0007")
            Me.rotypelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0006")
            CreatedBylab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATBY")
            CreatedDatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
            ModiBylab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFBY")
            ModifiedDatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFDATE")
            LinkButton1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Save")
            HyperLink1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Cancel")
            Label1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            Label2.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            'Dim address As String
            'address = objXmlTr.GetLabelName("EngLabelMsg", "INSTERPL")
            Dim statXmlTr As New clsXml
            statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            Me.TelephoneMsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Tele")
            Me.Emailmsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Email")
            Me.telnoerr.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "BB-NOTEL")
            Me.TelepTypemsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "TELTYPE")
            Dim styPanm As String = statXmlTr.GetLabelID("StatusMessage", mdtype)
            Dim userIDNamestr As String = Session("userID").ToString().ToUpper + "-" + Session("username").ToString.ToUpper

            If mdfyid.Trim() <> "" Then

                mdfypref = Request.QueryString("Prefix")
                Dim rotype As New clsCustomerRecord()
                rotype.CustomerID = mdfyid
                rotype.Customerpf = mdfypref
                Dim rotypeds As New DataSet

                'rotypeds = rotype.GetROidname("BB_MASROUM_MODSER")
                'If rotypeds.Tables(0).Rows.Count <> 0 Then
                '    databondros(rotypeds, Me.rotype)
                'End If
                Me.rotype.Items.Insert(0, "")
                'databondros(rotypeds, Me.rotype)
                'Me.rotype.Items.Insert(0, address)
                'Dim rotypestr As String
                'rotypestr = Me.rotype.Text
                'Dim ro As Array
                'ro = rotypestr.Split("-")
                'If ro.Length > 1 Then
                '    Dim roid As New clsAddress()
                '    roid.ModelType = ro(0)
                '    roid.SerialNo = ro(1)
                '    Dim rods As New DataSet
                '    rods = roid.GetROID()
                '    'Me.rouid.Text = rods.Tables(0).Rows(0).Item(0).ToString()
                'End If


                contactEntity.CustomerID = mdfyid
                contactEntity.TelephType = styPanm
                'contactEntity.Status = strPanm

                contactEntity.CustPrefix = mdfypref
                Dim edtReader As SqlDataReader = contactEntity.GetcontactDetailsByID()
                If edtReader.Read() Then
                    Dim telepass As New clsCommonClass()
                    Dim teletype As New clsrlconfirminf()
                    Dim teleParam As ArrayList = New ArrayList
                    teleParam = teletype.searchconfirminf("TELETYPE")
                    Dim telecount As Integer
                    Dim teleid As String
                    Dim telenm As String
                    For telecount = 0 To teleParam.Count - 1
                        teleid = teleParam.Item(telecount)
                        telenm = teleParam.Item(telecount + 1)
                        Me.TelepType.Items.Add(New ListItem(telenm.ToString(), teleid.ToString()))
                        If Me.TelepType.Equals(edtReader.GetValue(0).ToString()) Then
                            Me.TelepType.Items(telecount / 2).Selected = True
                        End If
                        Me.TelepType.Text = edtReader.GetValue(0).ToString()
                        telecount = telecount + 1
                    Next

                    'added by deyb 20-Jul-2006
                    Dim xStr As String = Request.Params("contacttype")
                    TelepType.SelectedItem.Text = xStr


                    '=================

                    Me.custid.Text = edtReader.GetValue(2).ToString()
                    Me.custid.ReadOnly = True
                    Me.rouid.Text = edtReader.GetValue(3).ToString()

                    Dim rocontact As New clsCustomerRecord()
                    If (Trim(Me.rouid.Text) <> "") Then
                        rocontact.RoSerialNo = rouid.Text
                    End If
                    Dim rocon As DataSet
                    rocon = rocontact.GetROidnamebyid("BB_MASROUM_MODSERBYROID")
                    Dim row As DataRow
                    Dim NewItem As New ListItem()
                    For Each row In rocon.Tables(0).Rows

                        NewItem.Text = row("id") & "-" & row("name")

                    Next
                    'If row.Table.Rows.Count <> 0 Then
                    Me.rotype.Text = NewItem.Text
                    'Else

                    'End If


                    Me.PIC.Text = edtReader.GetValue(4).ToString()
                    Me.Telephone.Text = telepass.passconverttel(edtReader.GetValue(5).ToString())

                    Me.Email.Text = edtReader.GetValue(6).ToString()


                    Dim cntryStat As New clsrlconfirminf()
                    Dim statParam As New ArrayList
                    statParam = cntryStat.searchconfirminf("STATUS")
                    Dim count As Integer
                    Dim statid As String
                    Dim statnm As String
                    For count = 0 To statParam.Count - 1
                        statid = statParam.Item(count)
                        statnm = statParam.Item(count + 1)
                        If (statid.Equals("ACTIVE") Or statid.Equals("DELETE") Or statid.Equals("OBSOLETE")) Then
                            Me.status.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                        End If
                        'Me.status.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                        If statid.Equals(edtReader.GetValue(7).ToString()) Then
                            status.Items(count / 2).Selected = True
                        End If
                        count = count + 1
                    Next
                    If purviewArray(4) = False Then
                        Me.status.Items.Remove(Me.status.Items.FindByValue("DELETE"))
                    End If

                    'Me.CreatedBy.Text = edtReader.GetValue(8).ToString()
                    Dim Rcreatby As String = edtReader.Item(8).ToString().ToUpper()
                    If Rcreatby <> "ADMIN" Then
                        Dim Rcreat As New clsCommonClass
                        Rcreat.userid() = Rcreatby
                        Dim Rcreatds As New DataSet()
                        Rcreatds = Rcreat.Getuseridname("BB_MASUSER_SelByIDName")

                        Me.CreatedBy.Text = Rcreatds.Tables(0).Rows(0).Item(0)
                    Else
                        Me.CreatedBy.Text = "ADMIN-ADMIN"
                    End If
                    CreatedBy.ReadOnly = True
                    Me.CreatedDate.Text = edtReader.GetValue(9).ToString()
                    CreatedDate.ReadOnly = True
                    Me.ModiBy.Text = userIDNamestr
                    ModiBy.ReadOnly = True
                    Dim contact As New clsContact()
                    contact.username = userIDNamestr
                    Me.ModifiedDate.Text = edtReader.GetValue(11).ToString()

                    ModifiedDate.ReadOnly = True
                    CreateGrid()
                End If
            End If
            End If
    End Sub
#Region " ro bond"
    Public Function databondros(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        Dim row As DataRow
        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            NewItem.Text = row("id") & "-" & row("name")
            NewItem.Value = row("id") & "-" & row("name")
            dropdown.Items.Add(NewItem)
        Next

    End Function
#End Region

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton1.Click
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim msginfo As String = objXm.GetLabelName("StatusMessage", "SAVEORNOT")
        Dim msgtitle As String = objXm.GetLabelName("StatusMessage", "SAVETITLE")
        MessageBox1.Confirm(msginfo)

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        CreateGrid()
    End Sub

    Protected Sub rotype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rotype.SelectedIndexChanged
        Dim rotypestr As String
        If Me.rotype.Text <> "" Then
            rotypestr = Me.rotype.Text
            Dim ro As Array
            ro = rotypestr.Split("-")
            Dim roid As New clsAddress()
            roid.ModelType = ro(0)
            roid.SerialNo = ro(1)
            Dim rods As New DataSet
            rods = roid.GetROID()
            Me.rouid.Text = rods.Tables(0).Rows(0).Item(0).ToString()
        Else
            Me.rouid.Text = 0
        End If
        GetGrid()
    End Sub

    Protected Sub HyperLink1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HyperLink1.Click
        Dim mdsymbol As String = Request.QueryString("consymbol")
        Dim mdfypref As String = Request.QueryString("Prefix")
        If mdsymbol = 0 Then
            Dim url As String = "~/PresentationLayer/masterrecord/addcustomer.aspx?"
            url &= "custid=" & Me.custid.Text.ToUpper().ToString & "&"
            url &= "Prefix=" & mdfypref
            Response.Redirect(url)
        End If
        If mdsymbol = 1 Then
            Dim url As String = "~/PresentationLayer/masterrecord/modifycustomer.aspx?"
            url &= "custid=" & Me.custid.Text.ToUpper().ToString & "&"
            url &= "Prefix=" & mdfypref & "&"
            url &= "modisy=" & 1
            Response.Redirect(url)
        End If
    End Sub

    Protected Sub contactView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles contactView.PageIndexChanging
        Me.contactView.PageIndex = e.NewPageIndex
        GetGrid()
    End Sub

#Region " view contactview bond"
    Public Function viewcontactviewbond()
        Dim contactall As DataSet = Session("contactView")
        Me.contactView.DataSource = contactall
        contactView.DataBind()
        Dim obiXmlTr As New clsXml
        obiXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        If contactall.Tables(0).Rows.Count >= 1 Then
            contactView.HeaderRow.Cells(1).Text = obiXmlTr.GetLabelName("EngLabelMsg", "ROID")
            contactView.HeaderRow.Cells(1).Font.Size = 8
            contactView.HeaderRow.Cells(2).Text = obiXmlTr.GetLabelName("EngLabelMsg", "TELEPHONETYPE")
            contactView.HeaderRow.Cells(2).Font.Size = 8
            contactView.HeaderRow.Cells(3).Text = obiXmlTr.GetLabelName("EngLabelMsg", "STATUS")
            contactView.HeaderRow.Cells(3).Font.Size = 8
            contactView.HeaderRow.Cells(4).Text = obiXmlTr.GetLabelName("EngLabelMsg", "PICNAME")
            contactView.HeaderRow.Cells(4).Font.Size = 8
            contactView.HeaderRow.Cells(5).Text = obiXmlTr.GetLabelName("EngLabelMsg", "TELEPHONE")
            contactView.HeaderRow.Cells(5).Font.Size = 8
        End If
    End Function
#End Region

    Protected Sub MessageBox1_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox1.GetMessageBoxResponse
        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then

            Dim contactEntity As New clsContact()
            'If (Trim(Me.CustomerPrefix.Text) <> "") Then
            '    addressEntity.CountryID = CustomerPrefix.Text.ToUpper()
            'End If
            Dim telepass As New clsCommonClass()
            Dim mdfypref As String = Request.QueryString("Prefix")
            contactEntity.CustPrefix = mdfypref
            If (Trim(Me.custid.Text) <> "") Then
                contactEntity.CustomerID = custid.Text.ToUpper()
            End If
            If (Trim(Me.rouid.Text) <> "") Then
                contactEntity.RouID = rouid.Text
            End If
            If (Trim(Me.Telephone.Text) <> "") Then
                contactEntity.Telephone = telepass.telconvertpass(Telephone.Text.ToUpper())
            End If

            If (Trim(Me.Email.Text) <> "") Then
                contactEntity.Email = Me.Email.Text.ToString().ToUpper()
            End If

            If (Trim(Me.TelepType.Text) <> "") Then
                contactEntity.TelephType = Me.TelepType.Text.ToString().ToUpper()
            End If
            If (Trim(Me.PIC.Text) <> "") Then
                contactEntity.PICName = Me.PIC.Text.ToString()
            End If

            If (Me.status.SelectedItem.Value.ToString() <> "") Then
                contactEntity.Status = status.SelectedItem.Value.ToString()
            End If

            If (Trim(CreatedBy.Text) <> "") Then
                contactEntity.CreatedBy = Session("userID").ToString().ToUpper
            End If

            If (Trim(ModiBy.Text) <> "") Then
                contactEntity.ModifiedBy = Session("userID").ToString().ToUpper
            End If
            contactEntity.Ipadress = Request.UserHostAddress.ToString()
            contactEntity.Svrcid = Session("login_svcID")
            contactEntity.username = Session("userID")
            Dim updCtryCnt As Integer = contactEntity.Update()
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            If updCtryCnt = 0 Then

                Dim objXm As New clsXml
                objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                errlab.Text = objXm.GetLabelName("StatusMessage", "BB-HintMsg-Suc")
                errlab.Visible = True
                CreateGrid()
            Else
                Response.Redirect("~/PresentationLayer/Error.aspx")
            End If
        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
        End If
    End Sub

    Private Function CreateGrid()
        Dim contactall As DataSet = contactEntity.Getcontact()
        ''If addressall.Tables(0).Rows.Count <> 0 Then
        Me.contactView.DataSource = contactall
        Session("contactView") = contactall
        contactView.AllowPaging = True
        contactView.AllowSorting = True
        contactView.DataBind()
        Dim obiXmlTr As New clsXml
        obiXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        If contactall.Tables(0).Rows.Count >= 1 Then
            'contactView.HeaderRow.Cells(0).Text = obiXmlTr.GetLabelName("EngLabelMsg", "BB-INSTALLHD1")
            'contactView.HeaderRow.Cells(0).Font.Size = 8
            'contactView.HeaderRow.Cells(1).Text = obiXmlTr.GetLabelName("EngLabelMsg", "BB-INSTALLHD2")
            'contactView.HeaderRow.Cells(1).Font.Size = 8
            contactView.HeaderRow.Cells(0).Text = obiXmlTr.GetLabelName("EngLabelMsg", "TELEPHONETYPE")
            contactView.HeaderRow.Cells(0).Font.Size = 8
            contactView.HeaderRow.Cells(1).Text = obiXmlTr.GetLabelName("EngLabelMsg", "STATUS")
            contactView.HeaderRow.Cells(1).Font.Size = 8
            contactView.HeaderRow.Cells(2).Text = obiXmlTr.GetLabelName("EngLabelMsg", "PICNAME")
            contactView.HeaderRow.Cells(2).Font.Size = 8
            contactView.HeaderRow.Cells(3).Text = obiXmlTr.GetLabelName("EngLabelMsg", "TELEPHONE")
            contactView.HeaderRow.Cells(3).Font.Size = 8
        End If
    End Function

    Private Function GetGrid()
        Dim contactall As DataSet = Session("contactView")
        contactView.DataSource = contactall
        contactView.DataBind()
        Dim obiXmlTr As New clsXml
        obiXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        If contactall.Tables(0).Rows.Count >= 1 Then
            'contactView.HeaderRow.Cells(0).Text = obiXmlTr.GetLabelName("EngLabelMsg", "BB-INSTALLHD1")
            'contactView.HeaderRow.Cells(0).Font.Size = 8
            'contactView.HeaderRow.Cells(1).Text = obiXmlTr.GetLabelName("EngLabelMsg", "BB-INSTALLHD2")
            'contactView.HeaderRow.Cells(1).Font.Size = 8
            contactView.HeaderRow.Cells(0).Text = obiXmlTr.GetLabelName("EngLabelMsg", "TELEPHONETYPE")
            contactView.HeaderRow.Cells(0).Font.Size = 8
            contactView.HeaderRow.Cells(1).Text = obiXmlTr.GetLabelName("EngLabelMsg", "STATUS")
            contactView.HeaderRow.Cells(1).Font.Size = 8
            contactView.HeaderRow.Cells(2).Text = obiXmlTr.GetLabelName("EngLabelMsg", "PICNAME")
            contactView.HeaderRow.Cells(2).Font.Size = 8
            contactView.HeaderRow.Cells(3).Text = obiXmlTr.GetLabelName("EngLabelMsg", "TELEPHONE")
            contactView.HeaderRow.Cells(3).Font.Size = 8
        End If
    End Function
End Class
