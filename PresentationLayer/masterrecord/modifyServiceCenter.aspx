<%@ Page Language="VB" AutoEventWireup="false" EnableEventValidation ="false" CodeFile="modifyServiceCenter.aspx.vb" Inherits="PresentationLayer_masterrecord_modifyServiceCenter" %>

<%@ Register Assembly="JCalendar" Namespace="JCalendar" TagPrefix="cc2" %>

<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Service Center - Edit</title>
    <link href="../css/style.css" rel="stylesheet" type="text/css" />
    <STYLE type="text/css">A:link { COLOR: #336666; TEXT-DECORATION: none }
	BODY { FONT-SIZE: 9pt; FONT-FAMILY: "Arial", "Verdana", "Tahoma"; BACKGROUND-COLOR: #ffffff }
	TD { FONT-SIZE: 9pt; FONT-FAMILY: Arial,Verdana,Tahoma }
	</STYLE>
<script language="JavaScript" src="../js/common.js"></script>
</head>
<body style="width: 100%">
    <form id="form1" runat="server">
        <table id="statetab" border="0" style="width: 100%">
            <tr>
                <td style="width: 100%">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width: 100%">
                        <tr>
                            <td background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title1.gif" width="5" /></td>
                            <td background="../graph/title_bg.gif" class="style2" width="100%">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="right" background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title_2.gif" width="5" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 844px">
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                        <tr>
                            <td background="../graph/title_bg.gif" class="style2" width="100%">
                                <font color="red">
                                    <asp:Label ID="errlab" runat="server" Text="Label" Visible="false"></asp:Label></font></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100%">
                    <table id="country" bgcolor="#b7e6e6" border="0" cellpadding="2" cellspacing="1"
                        style="width: 100%">
                        <tr bgcolor="#ffffff">
<td colspan = 4>
                            <asp:Label ID="lblNoteUP" runat="server" ForeColor="Red" Text="Label" Visible="True"></asp:Label>
</td>
             </tr>
             <tr bgcolor="#ffffff">
<td colspan = 4>
                        	&nbsp;
                     	</td>
               </tr>

                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%;">
                                <asp:Label ID="ServiceCenterNamelab" runat="server" bgColor="#b7e6e6" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%;">
                                <asp:TextBox ID="ServiceCenterName" runat="server" CssClass="textborder" Font-Overline="False"
                                    Width="89%" MaxLength="50"></asp:TextBox>
                                <asp:Label ID="Label2" runat="server" ForeColor="Red" Text="*"></asp:Label><span style="width: 35%;
                                        color: #000000"><asp:RequiredFieldValidator ID="ServiceCenterNamemsg" runat="server"
                                            ControlToValidate="ServiceCenterName" ForeColor="White">*</asp:RequiredFieldValidator></span></td>
                            <td align="right" style="width: 15%;">
                                <asp:Label ID="ServiceCenterIDlab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%; color: red;">
                                <asp:TextBox ID="ServiceCenterID" runat="server" CssClass="textborder" Width="88%"></asp:TextBox>
                                <asp:Label ID="Label3" runat="server" ForeColor="Red" Text="*"></asp:Label><span
                                    style="width: 100%; color: #ff0000"><span style="color: #000000"></span><asp:RequiredFieldValidator
                                        ID="ServiceCenterIDMsg" runat="server" ControlToValidate="ServiceCenterID"
                                        ForeColor="White">*</asp:RequiredFieldValidator></span>
                            </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%;">
                                <asp:Label ID="AlternateNamelab" runat="server" Text="Label" Width="48px"></asp:Label></td>
                            <td align="left" style="width: 35%">
                                <asp:TextBox ID="AlternateName" runat="server" CssClass="textborder" Width="89%" MaxLength="50"></asp:TextBox>
                                <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                <asp:RequiredFieldValidator ID="AlternateNamemsg" runat="server" ControlToValidate="AlternateName" ForeColor="White">*</asp:RequiredFieldValidator></td>
                            <td align="right" style="width: 15%;">
                                <asp:Label ID="BranchTypelab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%">
                                <asp:DropDownList ID="BranchType" runat="server" CssClass="textborder" Width="89%">
                                </asp:DropDownList>
                                <asp:Label ID="Label4" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                <asp:RequiredFieldValidator ID="BranchTypemsg" runat="server" ControlToValidate="BranchType"
                                    ForeColor="WhiteSmoke">*</asp:RequiredFieldValidator></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%;">
                                &nbsp;<asp:Label ID="PersonInChargelab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%">
                                <asp:TextBox ID="PersonInCharge" runat="server" CssClass="textborder" Width="89%" MaxLength="50"></asp:TextBox><span
                                    style="color: #ff0000"></span><span style="width: 115%; color: #000000"></span></td>
                            <td align="right" style="width: 15%;">
                                <asp:Label ID="status" runat="server" Text="Label"></asp:Label>
                            </td>
                            <td align="left" style="width: 35%">
                                <span style="color: #ff0000"><asp:DropDownList ID="ctrystat" runat="server" CssClass="textborder" Width="89%">
                                </asp:DropDownList></span></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%;">
                                <asp:Label ID="Address1lab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" colspan="3" style="height: 30px">
                                <asp:TextBox ID="Address1" runat="server" CssClass="textborder" Width="95%" MaxLength="100"></asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%;">
                                <asp:Label ID="Address2lab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" colspan="3">
                                <asp:TextBox ID="Address2" runat="server" CssClass="textborder" Width="95%" MaxLength="100"></asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%; height: 39px;">
                                <asp:Label ID="POCodelab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%; height: 39px;">
                                <asp:TextBox ID="POCode" runat="server" CssClass="textborder" Width="89%" MaxLength="10"></asp:TextBox>
                                <asp:Label ID="Label5" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                <span
                                    style="color: #ff0000"><asp:RequiredFieldValidator ID="POCodeMsg" runat="server"
                                        ControlToValidate="POCode" ForeColor="White">*</asp:RequiredFieldValidator></span></td>
                            <td align="right" style="width: 15%; height: 39px;">
                                <asp:Label ID="CountryIDlab" runat="server" Text="Label"></asp:Label>
                            </td>
                            <td align="left" style="width: 35%; height: 39px;" id="#ctry">
                                <asp:DropDownList ID="CountryID" runat="server" CssClass="textborder" Width="89%" AutoPostBack="True">
                                </asp:DropDownList>
                                <asp:Label ID="Label6" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                <asp:RequiredFieldValidator ID="countrymsg" runat="server" ControlToValidate="CountryID" ForeColor="White">*</asp:RequiredFieldValidator></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%; height: 26px;">
                                <asp:Label ID="StateIDlab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%; height: 26px;">
                                <asp:DropDownList ID="StateID" runat="server" CssClass="textborder" Width="91%" AutoPostBack="True">
                                </asp:DropDownList>
                                <asp:Label ID="Label7" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                <asp:RequiredFieldValidator ID="statmsg" runat="server" ControlToValidate="StateID" ForeColor="White">*</asp:RequiredFieldValidator></td>
                            <td align="right" style="width: 15%; height: 26px;">
                                <asp:Label ID="AreaIDlab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%; height: 26px;">
                                <asp:DropDownList ID="AreaID" runat="server" CssClass="textborder" Width="89%">
                                </asp:DropDownList>
                                <asp:Label ID="Label9" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                <asp:RequiredFieldValidator ID="areamsg" runat="server" ControlToValidate="AreaID" ForeColor="White">*</asp:RequiredFieldValidator></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%;">
                                <asp:Label ID="Telephone1lab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%">
                                <asp:TextBox ID="Telephone1" runat="server" CssClass="textborder" Width="89%" MaxLength="20"></asp:TextBox>
                                <asp:Label ID="Label8" runat="server" ForeColor="Red" Text="*"></asp:Label><span
                                    style="color: #ff0000"></span><asp:RegularExpressionValidator ID="Telephone1Msg"
                                        runat="server" ControlToValidate="Telephone1"
                                        ValidationExpression="\d{0,20}" ForeColor="White">*</asp:RegularExpressionValidator></td>
                            <td align="right" style="width: 15%;">
                                <asp:Label ID="picmobilelab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%">
                                <asp:TextBox ID="picmobile" runat="server" CssClass="textborder" Width="88%" MaxLength="20"></asp:TextBox><span
                                    style="color: #ff0000"></span><asp:RegularExpressionValidator ID="picmobileMsg"
                                        runat="server" ControlToValidate="picmobile"
                                        ValidationExpression="\d{0,20}" ForeColor="White">*</asp:RegularExpressionValidator></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%;">
                                <asp:Label ID="Telephone2lab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%;">
                                <asp:TextBox ID="Telephone2" runat="server" CssClass="textborder" Width="89%" MaxLength="20"></asp:TextBox><span
                                    style="color: #ff0000"></span><asp:RegularExpressionValidator ID="Telephone2Msg"
                                        runat="server" ControlToValidate="Telephone2" ErrorMessage="*"
                                        ValidationExpression="\d{0,20}">*</asp:RegularExpressionValidator></td>
                            <td align="right" style="width: 15%;">
                                <asp:Label ID="Faxlab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%;">
                                <asp:TextBox ID="Fax" runat="server" CssClass="textborder" Width="88%" MaxLength="20"></asp:TextBox>
                                <asp:Label ID="Label10" runat="server" ForeColor="Red" Text="*"></asp:Label><span
                                    style="color: #ff0000"></span><asp:RegularExpressionValidator ID="FaxMsg" runat="server"
                                        ControlToValidate="Fax" ValidationExpression="\d{0,20}" ForeColor="White">*</asp:RegularExpressionValidator></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%;">
                                <asp:Label ID="EffectiveDatelab" runat="server" Text="Label"></asp:Label>
                            </td>
                            <td align="left" style="width: 35%;">
                                <asp:TextBox ID="EffectiveDate" runat="server" CssClass="textborder"
                                    Width="85%" ReadOnly="True"></asp:TextBox>&nbsp;
                                <asp:HyperLink ID="HypCal" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                                    ToolTip="Choose a Date">Choose a Date</asp:HyperLink></td>
                            <td align="right" style="width: 15%;">
                                <asp:Label ID="CompanyIDlab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%;">
                                &nbsp;<asp:DropDownList ID="CompanyID" runat="server" CssClass="textborder" Width="89%">
                                </asp:DropDownList>
                                <asp:Label ID="Label11" runat="server" ForeColor="Red" Text="*"></asp:Label><span style="color: #ff0000"></span><span style="width: 115%;
                                    color: #000000"></span><asp:RequiredFieldValidator ID="CompanyIDmsg" runat="server"
                                        ControlToValidate="CompanyID" ForeColor="White">*</asp:RequiredFieldValidator>
                                &nbsp;
                            </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%;">
                                <asp:Label ID="JobsPeDaylab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%">
                                <asp:TextBox ID="JobsPerDay" runat="server" CssClass="textborder" Width="89%" MaxLength="9"></asp:TextBox>
                                <asp:Label ID="Label12" runat="server" ForeColor="Red" Text="*"></asp:Label>
                            </td>
                            <td align="right" style="width: 15%;">
                                <asp:Label ID="OTPremiumJobslab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%">
                                <asp:TextBox ID="OTPremiumJobs" runat="server" CssClass="textborder" Width="88%" MaxLength="9"></asp:TextBox>
                                <asp:Label ID="Label13" runat="server" ForeColor="Red" Text="*"></asp:Label><span
                                    style="color: #ff0000"></span></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%;">
                                <asp:Label ID="CreatedBylab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%;">
                                <asp:TextBox ID="CreatedBy" runat="server" CssClass="textborder" Width="89%"></asp:TextBox></td>
                            <td align="right" style="width: 15%;">
                                <asp:Label ID="CreatedDatelab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%;">
                                <asp:TextBox ID="CreatedDate" runat="server" CssClass="textborder" Width="88%"></asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%;">
                                <asp:Label ID="ModiBylab" runat="server" Text="Label"></asp:Label>
                            </td>
                            <td align="left" style="width: 35%">
                                <asp:TextBox ID="ModiBy" runat="server" CssClass="textborder" Width="89%"></asp:TextBox></td>
                            <td align="right" style="width: 15%;">
                                <asp:Label ID="ModifiedDatelab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%">
                                <asp:TextBox ID="ModifiedDate" runat="server" CssClass="textborder" Width="88%"></asp:TextBox></td>
                        </tr>
                                     <tr bgcolor="#ffffff">
<td colspan = 4>
                        	&nbsp;
                     	</td>
               </tr>

                        <tr bgcolor="#ffffff">
<td colspan = 4>
                            <asp:Label ID="lblNoteDown" runat="server" ForeColor="Red" Text="Label"></asp:Label>
</td>
             </tr>

                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100%;">
                    <table id="Table2" border="0" cellpadding="1" cellspacing="1">
                        <tr>
                            <td align="center" style="width: 20%">
                                &nbsp;<asp:LinkButton ID="LinkButton1" runat="server">save</asp:LinkButton></td>
                            <td align="center">
                                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/PresentationLayer/masterrecord/ServiceCenter.aspx">cancel</asp:HyperLink></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <asp:RangeValidator ID="JobsPerDayMsg" runat="server" ControlToValidate="JobsPerDay" ForeColor="White" MaximumValue="2147483647" MinimumValue="0"
            Type="Integer"></asp:RangeValidator>
        <asp:RequiredFieldValidator ID="JPDaymsg" runat="server" ControlToValidate="JobsPerDay" ForeColor="White"></asp:RequiredFieldValidator>
        <asp:RangeValidator ID="OTPremiumJobsMsg" runat="server" ControlToValidate="OTPremiumJobs" ForeColor="White" MaximumValue="2147483647" MinimumValue="0"
            Type="Integer"></asp:RangeValidator>
        <asp:RequiredFieldValidator ID="OTPmsg" runat="server" ControlToValidate="OTPremiumJobs" ForeColor="White"></asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator ID="EffectiveDatemsg" runat="server" ControlToValidate="EffectiveDate" ForeColor="White"></asp:RequiredFieldValidator>
        <cc1:MessageBox ID="MessageBox1" runat="server" />
        <br />
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
            ShowSummary="False" />
    </form>
</body>
</html>
