<%@ Page Language="VB" AutoEventWireup="false" EnableEventValidation="false" CodeFile="modifySerType.aspx.vb" Inherits="PresentationLayer_masterrecord_modifySerType" %>

<%@ Register Assembly="JCalendar" Namespace="JCalendar" TagPrefix="cc2" %>

<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Modify Service Type</title>
    <meta http-equiv="Content-Type" content="text/html; charset=gb2312">
	<LINK href="../css/style.css" type="text/css" rel="stylesheet">
	<script language="JavaScript" src="../js/common.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    
     <TABLE border="0" id=countrytab style="width: 100%">
           <tr>
           <td  >
               <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>
							<TD width="1%" background="../graph/title_bg.gif"><IMG height="24" src="../graph/title1.gif" width="5"></TD>
							<TD class="style2" width="98%" background="../graph/title_bg.gif">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></TD>
							<TD align="right" width="1%" background="../graph/title_bg.gif"><IMG height="24" src="../graph/title_2.gif" width="5"></TD>
						</TR>
			 </TABLE>
           </tr>
           <tr>
           <td style="width: 100%">
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>					
							<TD class="style2" width="98%" background="../graph/title_bg.gif"  >
                                <font color=red><asp:Label ID="errlab" runat="server" Text="Label" Visible=false></asp:Label></font></TD>
						</TR>
			 </TABLE>
           </tr>
			<TR>
				<TD style="width: 100%">
            <table id="SerTypeID" bgcolor="#b7e6e6" border="0" cellpadding="2" cellspacing="1" style="width: 100%">
              <tr bgcolor="#ffffff">
<td colspan = 4>
                            <asp:Label ID="lblNoteUp" runat="server" ForeColor="Red" Text="Label" Visible="True"></asp:Label>
</td>
             </tr>
             <tr bgcolor="#ffffff">
<td colspan = 4>
                        	&nbsp;
                     	</td>
               </tr> 
                <tr bgcolor="#ffffff">
                    <td align="right"  width="15%" style="width: 20%">
                        <asp:label id="SerTypeIDLab" runat="server" text="Label"></asp:label>
                    </td>
                    <td align="left" style="width: 30%" >
                        <asp:textbox id="SerTypeIDbox" runat="server" cssclass="textborder" ReadOnly="True" Width="90%"></asp:textbox>
                        <asp:Label ID="Label2" runat="server" ForeColor="Red" Text="*"></asp:Label>
                    </td>
                    <td align="right" style="color: #000000; width: 20%;">
                        <asp:label id="SerTypeNameLab" runat="server" text="Label"></asp:label>
                    </td>
                    <td align="left" style="width: 30%" >
                        <asp:textbox id="SerTypeNamebox" runat="server" cssclass="textborder" MaxLength="50" Width="85%"></asp:textbox>
                        <asp:Label ID="Label5" runat="server" ForeColor="Red" Text="*"></asp:Label>
                        <asp:RequiredFieldValidator ID="nameva" runat="server" ControlToValidate="SerTypeNamebox" ForeColor="White">*</asp:RequiredFieldValidator></td>
                </tr>
                <tr bgcolor="#ffffff">
                    <td align="right" width="15%" style="width: 20%; height: 30px">
                        <asp:label id="ALterNamelab" runat="server" text="Label"></asp:label>
                    </td>
                    <td align="left" colspan="3" width="85%" style="width: 80%; height: 30px">
                        <asp:textbox id="ALterNamebox" runat="server" cssclass="textborder" width="62%" MaxLength="50"></asp:textbox>
                    </td>
                </tr>
                <tr bgcolor="#ffffff">
                    <td align="right" width="15%" style="width: 20%">
                        <asp:label id="CountryIDlab" runat="server" text="Label"></asp:label>
                    </td>
                    <td align="left" style="width: 32%">
                        <font color=red style="width: 30%">
                            <asp:DropDownList ID="countryDrop" runat="server" Width="90%" AutoPostBack="True">
                            </asp:DropDownList>
                            <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="*"></asp:Label>
                            <asp:RequiredFieldValidator ID="contryidva" runat="server" ControlToValidate="countryDrop"
                                ForeColor="White">*</asp:RequiredFieldValidator></td>
                    <td align="right" style="width: 20%" >
                        <asp:label id="SerTypelab" runat="server" text="Label"></asp:label>
                    </td>
                    <td align="left" style="width: 30%" >
                        <asp:DropDownList ID="SerTypeDrop" runat="server" Width="90%" AutoPostBack="True" Enabled="False">
                        </asp:DropDownList></td>
                </tr>
                 <tr bgcolor="#ffffff">
                    <td align="right" style="width: 20%" >
                        <asp:label id="ModelIDlab" runat="server" text="LAB"></asp:label>
                    </td>
                    <td align="left" style="width: 30%" >
                        <asp:DropDownList ID="ModelIDDrop" runat="server" Width="90%">
                        </asp:DropDownList>
                        <asp:Label ID="Label3" runat="server" ForeColor="Red" Text=""></asp:Label>
                        <asp:RequiredFieldValidator ID="modva" runat="server" ControlToValidate="ModelIDDrop"
                            ForeColor="whitesmoke" Enabled =false  ></asp:RequiredFieldValidator></td>
                    <td align="right" style="width: 20%" >
                        <asp:label id="ReReSliplab" runat="server" text="Label"></asp:label>
                    </td>
                    <td align="left" style="width: 30%" >
                        <asp:DropDownList ID="ReReSlipDrop" runat="server" Width="90%">
                        </asp:DropDownList></td>
                </tr>
                <tr bgcolor="#ffffff">
                    <td align="right" style="width: 20%; height: 25px" >
                        <asp:label id="FrequencyLab" runat="server" text="LAB"></asp:label>
                    </td>
                    <td align="left" style="width: 30%; height: 25px" >
                        <asp:DropDownList ID="FrequencyDrop" runat="server" Width="90%" AutoPostBack="True" Enabled="False">
                        </asp:DropDownList></td>
                    <td align="right" style="width: 20%; height: 25px" >
                        <asp:label id="NoOfYearLab" runat="server" text="Label"></asp:label>
                    </td>
                    <td align="left" style="width: 30%; height: 25px;" >
                        <asp:DropDownList ID="NoOfYearDrop" runat="server" Width="90%" AutoPostBack="True" Enabled="False">
                        </asp:DropDownList></td>
                </tr>
                 
                <tr bgcolor="#ffffff">
                    <td align="right" style="width: 20%" >
                        <asp:label id="EffectiveDatelab" runat="server" text="Label"></asp:label>
                    </td>
                    <td align="left" style="width: 30%" >
                        <asp:textbox id="Efftext" runat="server" cssclass="textborder" width="48%" ReadOnly="True"></asp:textbox>
                        <asp:Label ID="Label4" runat="server" ForeColor="Red" Text="*"></asp:Label>
                        <asp:HyperLink ID="HypCal" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                            ToolTip="Choose a Date">Choose a Date</asp:HyperLink>
                        <cc2:JCalendar ID="Calendareffe" runat="server" ControlToAssign="Efftext"
                            ImgURL="~/PresentationLayer/graph/calendar.gif" Visible="False" />
                        
                       <asp:RequiredFieldValidator ID="EFFDATVA" runat="server" ControlToValidate="Efftext"
                            ForeColor="White">*</asp:RequiredFieldValidator>
                        
                    </td>
                    <td align="right" style="width: 20%" >
                        <asp:label id="ObsoleteDatelab" runat="server" text="Label"></asp:label>
                    </td>
                    <td align="left" style="width: 30%" >
                        <asp:textbox id="Obsotext" runat="server" cssclass="textborder" width="53%" ReadOnly="True"></asp:textbox>
                        <asp:Label ID="Label6" runat="server" ForeColor="Red" Text="*"></asp:Label>&nbsp;<asp:HyperLink
                            ID="HypCal2" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                            ToolTip="Choose a Date">Choose a Date</asp:HyperLink>
                        <cc2:JCalendar
                            ID="Calendarobs" runat="server" ControlToAssign="Obsotext" ImgURL="~/PresentationLayer/graph/calendar.gif" Visible="False" />
                        <asp:RequiredFieldValidator ID="OBSLVA" runat="server" ControlToValidate="Obsotext"
                            ForeColor="White">*</asp:RequiredFieldValidator>&nbsp;&nbsp;
                    </td>
                </tr>
                <tr bgcolor="#ffffff">
                    <td align="right" width="15%" style="width: 20%">
                        <asp:label id="Statuslab" runat="server" text="Label"></asp:label>
                    </td>
                    <td align="left" colspan="3" width="85%" style="width: 80%">
                        <asp:dropdownlist id="StatusDrop" runat="server" cssclass="textborder" width="35%">
                                                </asp:dropdownlist>
                    </td>
                </tr>
                 <tr bgcolor="#ffffff">
                    <td align="right" width="15%" style="width: 20%;">
                        <asp:label id="rmdtLab" runat="server" text="Label"></asp:label>
                    </td>
                    <td align="left" colspan="3" width="85%" style="width: 80%;">
                        <asp:TextBox ID="rmdtBox" runat="server" CssClass="textborder" MaxLength="10" Width="62%"></asp:TextBox>
                        <asp:RangeValidator ID="rmdtVA" runat="server" ControlToValidate="rmdtBox" ForeColor="DarkRed"
                            Type="Double" MaximumValue="9999999999" MinimumValue="0000000000">*</asp:RangeValidator></td>
                </tr>
                <tr bgcolor="#ffffff">
                    <td align="right" style="width: 20%;">
                        <asp:label id="PointCRLab" runat="server" text="Label"></asp:label>
                    </td>
                    <td align="left" style="width: 30%;" >
                        <asp:textbox id="PointCRbox" runat="server" cssclass="textborder" MaxLength="6" Width="90%"></asp:textbox>&nbsp;<asp:Label
                            ID="Label7" runat="server" ForeColor="Red">*</asp:Label>
                        <asp:RangeValidator
                            ID="PointCRVA" runat="server" ControlToValidate="PointCRbox" ForeColor="White"
                            Type="Double" MaximumValue="999.99" MinimumValue="000.00">*</asp:RangeValidator>
                    </td>
                    <td align="right" style="width: 20%;" >
                        <asp:label id="PointTeLab" runat="server" text="Label"></asp:label>
                    </td>
                    <td align="left" style="width: 30%;" >
                        <asp:textbox id="PointTebox" runat="server" cssclass="textborder" MaxLength="6" Width="90%"></asp:textbox>
                        <asp:Label ID="Label8" runat="server" ForeColor="Red" Text="*"></asp:Label>
                        <asp:RangeValidator ID="PointTeVA" runat="server" ControlToValidate="PointTebox"
                            ForeColor="White" Type="Double" MaximumValue="999.99" MinimumValue="000.00">*</asp:RangeValidator></td>
                </tr>
                <tr bgcolor="#ffffff">
                    <td align="right" width="15%" style="width: 20%; height: 23px;">
                        <asp:label id="RsdLab" runat="server" text="Label"></asp:label>
                    </td>
                    <td align="left" colspan="3" width="85%" style="width: 80%; height: 23px;">
                        <asp:dropdownlist id="RsdDrop" runat="server" cssclass="textborder" width="35%">
                                                </asp:dropdownlist>
                        </td>
                </tr>
                <tr bgcolor="#ffffff">
                    <td align="right" width="15%" style="width: 20%">
                        <asp:label id="ulsdLab" runat="server" text="Label"></asp:label>
                    </td>
                    <td align="left" colspan="3" width="85%" style="width: 80%">
                        <asp:dropdownlist id="ulsdDrop" runat="server" cssclass="textborder" width="35%">
                                                </asp:dropdownlist>
                    </td>
                </tr>
                <tr bgcolor="#ffffff">
                    <td align="right" style="width: 20%;" >
                        <asp:label id="CreatedBylab" runat="server" text="Label"></asp:label>
                    </td>
                    <td align="left" style="width: 30%;" >
                        <asp:textbox id="CreatedBy" runat="server" cssclass="textborder" Width="90%"></asp:textbox>
                    </td>
                    <td align="right" style="width: 20%;" >
                        <asp:label id="CreatedDatelab" runat="server" text="Label"></asp:label>
                    </td>
                    <td align="left" style="width: 30%;" >
                        <asp:textbox id="CreatedDate" runat="server" cssclass="textborder" width="97%" ReadOnly="True"></asp:textbox>
                        
                    </td>
                </tr>
                <tr bgcolor="#ffffff">
                    <td align="right" style="height: 30px; width: 20%;" >
                        <asp:label id="ModifiedBylab" runat="server" text="Label"></asp:label>
                    </td>
                    <td align="left" style="height: 30px; width: 30%;" >
                        <asp:textbox id="ModifiedBy" runat="server" cssclass="textborder" Width="90%"></asp:textbox>
                    </td>
                    <td align="right" style="height: 30px; width: 20%;" >
                        <asp:label id="ModifiedDatelab" runat="server" text="Label"></asp:label>
                    </td>
                    <td align="left" style="width: 30%; height: 30px" >
                        <asp:textbox id="ModifiedDate" runat="server" cssclass="textborder" width="97%"></asp:textbox>
                       
                    </td>
                </tr>
                <tr bgcolor="#ffffff">
<td colspan = 4>
                        	&nbsp;
                     	</td>
               </tr>  
                   <tr bgcolor="#ffffff">
<td colspan = 4>
                            <asp:Label ID="lblNoteDown" runat="server" ForeColor="Red" Text="Label" Visible="True"></asp:Label>
</td>
             </tr>
             
            </table>
      
                    <asp:GridView ID="duringview" runat="server" AllowPaging="True" style="width: 100%">
                    </asp:GridView>
       <asp:LinkButton ID="savebutton" runat="server">save</asp:LinkButton>
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                    &nbsp; &nbsp;
                   
                   
                                            <asp:HyperLink ID="cancellink" runat="server" NavigateUrl="~/PresentationLayer/masterrecord/SerType.aspx">cancel</asp:HyperLink></TD>
					</TR>
					</table>
					<TR>
						<TD style="height: 22px" >
                           <asp:ValidationSummary ID="ErrorMessage" runat="server" ShowMessageBox="True" ShowSummary="False" />
                            <cc1:MessageBox ID="MessageBox1" runat="server" />
						</TD>
					</TR>
			
    </form>
    
</body>
</html>
