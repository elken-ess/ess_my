<%@ Page Language="VB" AutoEventWireup="false" EnableEventValidation ="false"  CodeFile="AddPartCategory.aspx.vb" Inherits="PresentationLayer_masterrecord_AddPartCategory" %>

<%@ Register Assembly="JCalendar" Namespace="JCalendar" TagPrefix="cc2" %>

<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>add part category</title>
    <LINK href="../css/style.css" type="text/css" rel="stylesheet"> 
<script language="JavaScript" src="../js/common.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table id="countrytab" border="0" width="100%">
            <tr>
                <td style="width: 100%">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" style="height: 24px" width="1%">
                                <img height="24" src="../graph/title1.gif" width="5" /></td>
                            <td background="../graph/title_bg.gif" class="style2" style="height: 24px" width="98%">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="right" background="../graph/title_bg.gif" style="width: 1%; height: 24px">
                                <img height="24" src="../graph/title_2.gif" width="5" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100%">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" class="style2" style="width: 98%">
                                <font color="red">
                                    <asp:Label ID="errlab" runat="server" Text="Label" Visible="false"></asp:Label></font>
                                    </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100%">
                    <table id="country" bgcolor="#b7e6e6" border="0" cellpadding="2" cellspacing="1" width="100%">
                      <tr bgcolor="#ffffff">
<td colspan = 4>
                            <asp:Label ID="lblCompTop" runat="server" ForeColor="Red" Text="Label" Visible="True"></asp:Label>
</td>
             </tr>
             <tr bgcolor="#ffffff">
<td colspan = 4>
                        	&nbsp;
                     	</td>
               </tr> 
                        <tr bgcolor="#ffffff">
                            <td align="right" style="height: 30px; width: 15%;" width="15%">
                                &nbsp;<asp:Label ID="CategoryIDlab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 225px; height: 30px">
                                <asp:TextBox ID="CategoryIDbox" runat="server" CssClass="textborder" MaxLength="10" Width="81%"></asp:TextBox>&nbsp;
                                <asp:Label ID="Label3" runat="server" ForeColor="Red" Height="16px" Text="*" Width="8px"></asp:Label>
                                <asp:RequiredFieldValidator ID="categoryiderr" runat="server" ControlToValidate="CategoryIDbox" ForeColor="White">*</asp:RequiredFieldValidator></td>
                            <td align="right" style="height: 30px; width: 15%;" width="15%">
                                <asp:Label ID="CategoryNamelab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 36%; height: 30px">
                                <asp:TextBox ID="CategoryNamebox" runat="server" CssClass="textborder" Width="81%" MaxLength="50"></asp:TextBox>&nbsp;
                                <asp:Label ID="Label2" runat="server" ForeColor="Red" Height="8px" Text="*" Width="8px"></asp:Label>
                                <asp:RequiredFieldValidator ID="categorynmerr" runat="server" ControlToValidate="CategoryNamebox" ForeColor="White">*</asp:RequiredFieldValidator></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" width="15%" style="width: 15%">
                                <asp:Label ID="categoryalterLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" colspan="3" width="85%" style="width: 85%">
                                <asp:TextBox ID="categoryalterbox" runat="server" CssClass="textborder" Width="500px" MaxLength="50"></asp:TextBox>
                                <asp:Label ID="Label5" runat="server" ForeColor="Red" Height="16px" Text="*" Width="1px"></asp:Label>
                                <asp:RequiredFieldValidator ID="altererr" runat="server" ControlToValidate="categoryalterbox"
                                    Width="8px" ForeColor="White">*</asp:RequiredFieldValidator>&nbsp;
                            </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" width="15%" style="width: 15%; height: 30px">
                                <asp:Label ID="countryidlab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 225px; height: 30px;">
                                <asp:DropDownList ID="countryiddrpd" runat="server" CssClass="textborder" Width="176px" style="width: 80%">
                                </asp:DropDownList>&nbsp;<asp:Label ID="Label1" runat="server" ForeColor="Red" Height="16px"
                                    Text="*" Width="1px"></asp:Label>
                                <asp:RequiredFieldValidator ID="ctryiderr" runat="server" ControlToValidate="countryiddrpd" Width="8px" ForeColor="White">*</asp:RequiredFieldValidator>&nbsp;&nbsp;
                            </td>
                            <td align="right" width="15%" style="width: 15%; height: 30px">
                                <asp:Label ID="statuslab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 36%; height: 30px;">
                                <asp:DropDownList ID="statusdrpd" runat="server" CssClass="textborder" Width="168px" style="width: 82%">
                                </asp:DropDownList></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%" width="15%">
                                <asp:Label ID="effectivedatelab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 225px">
                                <asp:TextBox ID="effectibedatebox" runat="server" CssClass="textborder" ReadOnly="True"
                                    Width="168px" style="width: 72%"></asp:TextBox>
                                <asp:HyperLink ID="HypCal" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                                    ToolTip="Choose a Date">Choose a Date</asp:HyperLink>
                                <cc2:JCalendar ID="JCalendar1" runat="server" ControlToAssign="effectibedatebox"
                                    ImgURL="~/PresentationLayer/graph/calendar.gif" Visible="False" />
                                &nbsp;<asp:Label ID="Label4" runat="server" ForeColor="Red" Height="16px" Text="*"
                                    Width="1px"></asp:Label><asp:RequiredFieldValidator ID="efferr" runat="server" ControlToValidate="effectibedatebox"
                                    Width="8px">*</asp:RequiredFieldValidator>
                                &nbsp;
                            </td>
                            <td align="right" style="width: 15%" width="15%">
                                <asp:Label ID="obsoletedatelab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 36%">
                                <asp:TextBox ID="obsoletedatebox" runat="server" CssClass="textborder" ReadOnly="True"
                                    Width="128px" style="width: 72%"></asp:TextBox>&nbsp;<asp:HyperLink ID="HypCal2"
                                        runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif" ToolTip="Choose a Date">Choose a Date</asp:HyperLink>
                                <cc2:JCalendar ID="JCalendar2" runat="server" ControlToAssign="obsoletedatebox" ImgURL="~/PresentationLayer/graph/calendar.gif" Visible="False" />
                                <asp:Label ID="Label6" runat="server" ForeColor="Red" Height="16px" Text="*" Width="1px"></asp:Label><asp:RequiredFieldValidator ID="obserr" runat="server" ControlToValidate="obsoletedatebox"
                                    Width="8px" ForeColor="White">*</asp:RequiredFieldValidator>
                                &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" width="15%" style="width: 15%">
                                <asp:Label ID="creatby" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 225px">
                                <asp:TextBox ID="creatbybox" runat="server" CssClass="textborder" ReadOnly="true" Width="168px"></asp:TextBox></td>
                            <td align="right" width="15%" style="width: 15%">
                                <asp:Label ID="creatdate" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 36%">
                                <asp:TextBox ID="creatdtbox" runat="server" CssClass="textborder" ReadOnly="true" Width="168px" style="width: 82%"></asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" width="15%" style="width: 15%">
                                <asp:Label ID="modfyby" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 225px">
                                <asp:TextBox ID="modfybybox" runat="server" CssClass="textborder" ReadOnly="true" Width="168px"></asp:TextBox></td>
                            <td align="right" width="15%" style="width: 15%">
                                <asp:Label ID="mdfydate" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 36%">
                                <asp:TextBox ID="mdfydtbox" runat="server" CssClass="textborder" ReadOnly="true" Width="168px" style="width: 82%"></asp:TextBox></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 657px; height: 37px">
                <font color=red><asp:Label ID="lblCompBottom" runat="server" Text="Label" Visible="true"></asp:Label></font>
                    <table id="Table1" border="0" cellpadding="1" cellspacing="1">
                        <tr>
                            <td align="center" style="width: 20%; height: 16px">
                                &nbsp;<asp:LinkButton ID="saveButton" runat="server"></asp:LinkButton></td>
                            <td align="center" style="height: 16px">
                                <asp:HyperLink ID="cancelLink" runat="server" NavigateUrl="~/PresentationLayer/masterrecord/PartCategory.aspx">[cancelLink]</asp:HyperLink></td>
                       <td align=center>
<asp:HyperLink ID="AddLink" runat="server" >[Add]</asp:HyperLink>
</td> </tr>
                    </table>
                </td>
            </tr>
        </table>
        <cc1:MessageBox ID="MessageBox1" runat="server" />
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
            ShowSummary="False" />
    
    </div>
    </form>
</body>
</html>
