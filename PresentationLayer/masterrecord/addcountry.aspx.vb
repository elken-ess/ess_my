﻿Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Partial Class PresentationLayer_masterrecord_addcountry
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try

            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "08")
            Me.saveButton.Enabled = purviewArray(0)
            If purviewArray(0) = False Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If

            'display label message
            Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            ctryidlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCTRY-0001")
            ctrynameLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCTRY-0002")
            ctryalterLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCTRY-0003")
            statusLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCTRY-0004")
            ctrytelpf.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCTRY-0005")
            curridLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCTRY-0006")
            currnmLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCTRY-0007")
            creatby.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATBY")
            creatdate.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
            modfyby.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFBY")
            mdfydate.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFDATE")
            saveButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Save")
            cancelLink.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Cancel")
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "ADDCTRYTL")
            AddLink.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Add")
            AddLink.NavigateUrl = Page.Request.RawUrl
            Me.lblCompTop.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            Me.lblCompBottom.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")

            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            ctryiderr.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "CRYIDER")
            ctrynmerr.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "CRYNMER")
            ctryalterr.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "CRYALTER")
            Dim userIDNamestr As String = Session("userID").ToString().ToUpper + "-" + Session("username").ToString.ToUpper
            creatbybox.Text = userIDNamestr
            Dim cntryStat As New clsUtil()
            creatdtbox.Text = Date.Now()
            modfybybox.Text = userIDNamestr
            mdfydtbox.Text = Date.Now()

            'create the dropdownlist

            Dim statParam As ArrayList = New ArrayList
            statParam = cntryStat.searchconfirminf("STATUS")
            Dim count As Integer
            Dim statid As String
            Dim statnm As String
            For count = 0 To statParam.Count - 1
                statid = statParam.Item(count)
                statnm = statParam.Item(count + 1)
                count = count + 1
                If (statid.Equals("ACTIVE") Or statid.Equals("OBSOLETE")) Then
                    Me.ctrystat.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                End If
                'If Not statid.Equals("DELETE") Then
                '    ctrystat.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                'End If
            Next
            ctrystat.Items(0).Selected = True

        End If
        Me.ctryidbox.Focus()
    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveButton.Click
        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim msginfo As String = objXm.GetLabelName("StatusMessage", "SAVEORNOT")
        Dim msgtitle As String = objXm.GetLabelName("StatusMessage", "SAVETITLE")
        MessageBox1.Confirm(msginfo)
    End Sub

    Protected Sub MessageBox1_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox1.GetMessageBoxResponse
        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
       
            Dim countryEntity As New clsCountry()
            If (Trim(ctryidbox.Text) <> "") Then
                countryEntity.CountryID = ctryidbox.Text.ToUpper()
            End If
            If (Trim(ctrynmbox.Text) <> "") Then
                countryEntity.CountryName = ctrynmbox.Text.ToUpper()
            End If
            If (Trim(ctryaltbox.Text) <> "") Then
                countryEntity.CountryAlternateName = ctryaltbox.Text.ToUpper()
            End If
            If (ctrystat.SelectedItem.Value.ToString() <> "") Then
                countryEntity.CountryStatus = ctrystat.SelectedItem.Value.ToString()
            End If
            If (Trim(ctrytelbox.Text) <> "") Then
                countryEntity.TelPref = ctrytelbox.Text.ToUpper()
            End If
            If (Trim(ctrycuridbox.Text) <> "") Then
                countryEntity.CountryCurrencyid = ctrycuridbox.Text.ToUpper()
            End If
            If (Trim(ctrycurnmbox.Text) <> "") Then
                countryEntity.CountryCurrencyname = ctrycurnmbox.Text.ToUpper()
            End If
            If (Trim(creatbybox.Text) <> "") Then
                countryEntity.CreateBy = Session("userID").ToString().ToUpper
            End If
            Dim cntryStat As New clsUtil()
            If (Trim(modfybybox.Text) <> "") Then
                countryEntity.ModifyBy = Session("userID").ToString().ToUpper
            End If
            Dim dupCount As Integer = countryEntity.GetDuplicatedCountry()
            If dupCount.Equals(-1) Then
                Dim objXm As New clsXml
                objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                errlab.Text = objXm.GetLabelName("StatusMessage", "DUPCTRYIDORNM")
                errlab.Visible = True
            ElseIf dupCount.Equals(0) Then
                Dim insCtryCnt As Integer = countryEntity.Insert()
                Dim duplpfx As Integer = countryEntity.GetDuplicatedLPFX()
                If duplpfx.Equals(0) Then
                    Dim ins As Integer = countryEntity.InsertLFPX()
                End If

                Dim objXmlTr As New clsXml
                objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                If insCtryCnt = 0 Then
                    ' Dim msg As String = objXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Suc")
                    ' Dim msgBox As New MessageBox(Me)
                    ' msgBox.Show(msg)     'this is show a messagebox to tell us success
                    'Response.Redirect("country.aspx")
                    Dim objXm As New clsXml
                    objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    errlab.Text = objXm.GetLabelName("StatusMessage", "BB-HintMsg-Suc")
                    errlab.Visible = True
                Else
                    'Dim msg As String = objXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Fail")
                    'Dim msgBox As New MessageBox(Me)
                    'msgBox.Show(msg)
                    Response.Redirect("~/PresentationLayer/Error.aspx")
                End If
            End If
        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
        End If
    End Sub
End Class
