Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Partial Class PresentationLayer_masterrecord_addstate
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        BindGrid()
    End Sub
    Protected Sub BindGrid()
        If Not Page.IsPostBack Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try

            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "09")
            Me.saveButton.Enabled = purviewArray(0)
            If purviewArray(0) = False Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If


            Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
            'display label message
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            ctryidlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCTRY-0001")
            Me.stateidlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTATE-0001")
            Me.statenmlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTATE-0002")
            statusLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCTRY-0004")
            Me.alterlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCTRY-0003")
            Me.taxidLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTATE-0003")
            Me.lblCompTop.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            Me.lblCompBottom.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")

            saveButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Save")
            cancelLink.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Cancel")
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "ADDSTATL")


            creatby.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATBY")
            creatdate.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
            modfyby.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFBY")
            mdfydate.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFDATE")
            AddLink.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Add")
            AddLink.NavigateUrl = Page.Request.RawUrl
            Dim userIDNamestr As String = Session("userID").ToString().ToUpper + "-" + Session("username").ToString.ToUpper
            creatbybox.Text = userIDNamestr
            creatdtbox.Text = Date.Now()
            modfybybox.Text = userIDNamestr
            mdfydtbox.Text = Date.Now()



            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            Me.ctryiderr.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "MAS-COUNTRY")
            Me.staiderr.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "STAIDER")
            Me.stanmerr.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "STANMER")
            'Me.stalterr.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "STAALTER")


            'create the dropdownlist
            Dim cntryStat As New clsrlconfirminf()
            Dim statParam As ArrayList = New ArrayList
            statParam = cntryStat.searchconfirminf("STATUS")
            Dim count As Integer
            Dim statid As String
            Dim statnm As String
            For count = 0 To statParam.Count - 1
                statid = statParam.Item(count)
                statnm = statParam.Item(count + 1)
                count = count + 1
                If (statid.Equals("ACTIVE") Or statid.Equals("OBSOLETE")) Then
                    ctrystat.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                End If

            Next
            ctrystat.Items(0).Selected = True


            'bind ctryid
            'create  ctryid the dropdownlist
            Dim ctry As New clsCommonClass
            Dim ctryds As New DataSet

            Dim idnameds As New DataSet()
            Dim rank As Integer = Session("login_rank")
            ctry.rank = rank
            If rank <> 0 Then
                ctry.spctr = Session("login_ctryID")
                ctry.spstat = Session("login_cmpID")
                ctry.sparea = Session("login_svcID")
            End If
            ctryds = ctry.Getcomidname("BB_MASCTRY_IDNAME")
            If ctryds.Tables(0).Rows.Count <> 0 Then
                databonds(ctryds, Me.ctr_id)
            End If
            Me.ctr_id.Items.Insert(0, "")
            Me.ctr_id.SelectedValue = Session("login_ctryID")


            Me.taxid.Items.Add(New ListItem("", ""))
            ''create  taxid the dropdownlist
            Dim tax As New clsCommonClass
            Dim taxds As New DataSet

            If (Me.ctr_id.Text <> "") Then
                tax.spctr = Me.ctr_id.SelectedValue().ToString().ToUpper()
            End If

            taxds = tax.Gettaxidname("BB_MASSTAX_IDNMPERC")
            If taxds.Tables(0).Rows.Count <> 0 Then
                databonds_tax(taxds, Me.taxid)

            End If

            Me.taxid.Items.Insert(0, "")

            Me.stateidbox.Focus()
            'areads = area.Getidname("BB_MASARE_IDNAME")
            'databonds(areads, Me.AreaID)
        End If

    End Sub


    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveButton.Click
        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim msginfo As String = objXm.GetLabelName("StatusMessage", "SAVEORNOT")
        Dim msgtitle As String = objXm.GetLabelName("StatusMessage", "SAVETITLE")
        MessageBox1.Confirm(msginfo)
    End Sub

#Region "save record"
    Public Sub save()
        Dim stateEntity As New clsState()
        If (Trim(Me.stateidbox.Text) <> "") Then
            stateEntity.StateID = stateidbox.Text.ToUpper()
        End If
        If (Trim(Me.statenmbox.Text) <> "") Then
            stateEntity.StateName = statenmbox.Text.ToUpper()
        End If
        If (Trim(Me.statealtbox.Text) <> "") Then
            stateEntity.StateAlternateName = statealtbox.Text.ToUpper()
        End If

        If (ctrystat.SelectedItem.Value.ToString() <> "") Then
            stateEntity.StateStatus = ctrystat.SelectedItem.Value
        End If

        If (Me.ctr_id.SelectedItem.Value.ToString() <> "") Then
            stateEntity.CountryID = ctr_id.SelectedValue.ToString().ToUpper()
        End If

        If (Me.taxid.SelectedValue.ToString() <> "") Then
            stateEntity.TaxID = Me.taxid.SelectedValue.ToString().ToUpper()
        End If

        If (Trim(creatbybox.Text) <> "") Then
            stateEntity.CreateBy = Session("userID").ToString.ToUpper()
        End If
        'If (Trim(creatdtbox.Text) <> "") Then
        '    stateEntity.CreateDate = creatdtbox.Text.ToUpper()
        'End If
        If (Trim(modfybybox.Text) <> "") Then
            stateEntity.ModifyBy = Session("userID").ToString.ToUpper()
        End If
        'If (Trim(mdfydtbox.Text) <> "") Then
        '    stateEntity.ModifyDate = mdfydtbox.Text.ToUpper()
        'End If

        Dim dupCount As Integer = stateEntity.GetDuplicatedState()
        'If selvalue.Read() <> 0 Then
        '    Response.Redirect("~/PresentationLayer/Error.aspx")
        'End If
        If dupCount.Equals(-1) Then
            Dim objXm As New clsXml
            objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            errlab.Text = objXm.GetLabelName("StatusMessage", "DUPSTATIDORNM")
            errlab.Visible = True
        ElseIf dupCount.Equals(0) Then

            Dim insStatCnt As Integer = stateEntity.Insert()

            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            If insStatCnt = 0 Then

                errlab.Text = objXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Suc")
                errlab.Visible = True
                'Response.Redirect("state.aspx")
            Else

                Response.Redirect("~/PresentationLayer/Error.aspx")
            End If
        End If
    End Sub
#End Region
#Region " id bond"
    Public Function databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        Dim row As DataRow
        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            NewItem.Text = row("id") & "-" & row("name")
            NewItem.Value = row("id")
            dropdown.Items.Add(NewItem)
        Next
        'dropdown.Items(0).Selected = True
    End Function
#End Region

#Region " taxid bond"
    Public Function databonds_tax(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        'dropdown.Items.Add("")
        Dim row As DataRow
        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            NewItem.Text = row("id") & "-" & row("name") & "-" & row("perc")
            NewItem.Value = row("id")
            dropdown.Items.Add(NewItem)

        Next

    End Function
#End Region

    
    Protected Sub ctrystat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ctrystat.SelectedIndexChanged

    End Sub

    Protected Sub MessageBox1_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox1.GetMessageBoxResponse
        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
            save()
        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
        End If
    End Sub

    Protected Sub ctr_id_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ctr_id.SelectedIndexChanged
        If Me.ctr_id.Text <> "" Then
            Me.taxid.Items.Clear()
            Dim tax As New clsCommonClass
            Dim taxds As New DataSet
            Me.taxid.Items.Clear()
            If (Me.ctr_id.Text <> "") Then
                tax.spctr = Me.ctr_id.SelectedValue().ToString().ToUpper()
            End If

            taxds = tax.Gettaxidname("BB_MASSTAX_IDNMPERC")
            If taxds.Tables(0).Rows.Count <> 0 Then
                databonds_tax(taxds, Me.taxid)
            End If
            Me.taxid.Items.Insert(0, "")
        Else
            Me.taxid.Items.Clear()
            Me.taxid.Items.Add(New ListItem("", ""))
        End If
    End Sub
End Class
