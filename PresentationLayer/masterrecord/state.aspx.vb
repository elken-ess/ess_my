Imports BusinessEntity
Imports System.Configuration
Imports System.Data
Partial Class PresentationLayer_masterrecord_state
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Response.Redirect("~/PresentationLayer/logon.aspx")
            End If
        Catch ex As Exception
            Response.Redirect("~/PresentationLayer/logon.aspx")
        End Try
        BindGrid()
    End Sub

    Protected Sub BindGrid()
        If (Not Page.IsPostBack) Then
            'display label message
            Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            Me.staID.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTATE-0001")
            stanm.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTATE-0002")
            Me.staStat.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCTRY-0004")
            addButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Add")
            searchButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Search")
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BROSTATL")


            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.addinfo.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-AR")
            Me.addinfo.Visible = False
            Me.Label1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-NR")
            Me.Label1.Visible = False
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            'create the dropdownlist
            Dim cntryStat As New clsrlconfirminf()

            Dim statParam As ArrayList = New ArrayList
            statParam = cntryStat.searchconfirminf("STATUS")
            Dim count As Integer
            Dim statid As String
            Dim statnm As String
            For count = 0 To statParam.Count - 1
                statid = statParam.Item(count)
                statnm = statParam.Item(count + 1)
                count = count + 1
                If (statid.Equals("ACTIVE") Or statid.Equals("OBSOLETE")) Then
                    ctryStatDrop.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                End If
            Next
            'ctryStatDrop.Items(0).Selected = True

            Dim stateEntity As New clsState()
            ''''''''''''user info''''''''''''''
            stateEntity.ModifyBy = Session("userID")

            '''''''''''''''''''''''''''''''''''
            If (Session("userID") <> "ADMIN") Then
                stateEntity.CountryID = Session("login_ctryID").ToString.ToUpper()
            Else
                stateEntity.CountryID = "%"
            End If

            If (ctryStatDrop.SelectedValue().ToString() <> "") Then
                stateEntity.StateStatus = ctryStatDrop.SelectedItem.Value.ToString()
            End If

            Dim stall As DataSet = stateEntity.GetState()
            staView.DataSource = stall
            Session("statView") = stall

            checknorecord(stall)

            staView.AllowPaging = True
            staView.AllowSorting = True

            'Dim editcol As System.Web.UI.WebControls.HyperLinkField = New HyperLinkField()
            'editcol.Text = "edit"
            'editcol.DataNavigateUrlFormatString = "~/PresentationLayer/masterrecord/modifystate.aspx?staid={0}&ctrid={1}&stastat={2}"
            'editcol.HeaderImageUrl = "~/PresentationLayer/graph/edit.gif"
            'Dim staid As String = stall.Tables.Item(0).Columns(0).ColumnName.ToString()
            'Dim ctrid As String = stall.Tables.Item(0).Columns(4).ColumnName.ToString()
            'Dim stastat As String = stall.Tables.Item(0).Columns(3).ColumnName.ToString()
            'Dim NavigateUrls As Array = Array.CreateInstance(GetType(String), 3)
            'NavigateUrls(0) = staid
            'NavigateUrls(1) = ctrid
            'NavigateUrls(2) = stastat
            'editcol.DataNavigateUrlFields = NavigateUrls
            'staView.Columns.Add(editcol)

            Dim editcol As New CommandField
            editcol.EditText = objXmlTr.GetLabelName("EngLabelMsg", "BB-Edit") '"edit"
            editcol.HeaderImageUrl = "~/PresentationLayer/graph/edit.gif"
            editcol.ShowEditButton = True
            staView.Columns.Add(editcol)

            '''access control'''''''''''''''''''''''''''''''''''''''''''''''
            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "09")
            Me.addButton.Enabled = purviewArray(0)
            'Me.searchButton.Visible = purviewArray(2)
            editcol.Visible = purviewArray(2)
            'Me.staView.Visible = purviewArray(2)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            staView.DataBind()
            If (stall.Tables(0).Rows.Count <> 0) Then
               
                staView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "STAHD0")
                staView.HeaderRow.Cells(1).Font.Size = 8
                staView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "STAHD1")
                staView.HeaderRow.Cells(2).Font.Size = 8
                staView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "STAHD2")
                staView.HeaderRow.Cells(3).Font.Size = 8
                staView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "STAHD3")
                staView.HeaderRow.Cells(4).Font.Size = 8
                staView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "STAHD4")
                staView.HeaderRow.Cells(5).Font.Size = 8
                staView.HeaderRow.Cells(6).Text = objXmlTr.GetLabelName("EngLabelMsg", "STAHD5")
                staView.HeaderRow.Cells(6).Font.Size = 8
                'staView.HeaderRow.Cells(7).Text = objXmlTr.GetLabelName("EngLabelMsg", "STAHD6")
                'staView.HeaderRow.Cells(7).Font.Size = 8
                'staView.HeaderRow.Cells(8).Text = objXmlTr.GetLabelName("EngLabelMsg", "STAHD7")
                'staView.HeaderRow.Cells(8).Font.Size = 8


            End If

        End If
    End Sub
#Region "if have no record "
    Public Function checknorecord(ByVal ds As DataSet) As Integer
        If ds.HasErrors Then
            If ds.Tables(0).Rows.Count = 0 Then
                Me.Label1.Visible = True
                'Me.addinfo.Visible = False
            Else
                Me.Label1.Visible = False

            End If

        End If
        If ds.Tables(0).Rows.Count = 0 Then
            Me.Label1.Visible = True
            'Me.addinfo.Visible = False
        Else
            Me.Label1.Visible = False

        End If


    End Function
#End Region
#Region "IF have all record"
    Public Function checkinfo() As Integer
        If Me.stateIDBox.Text = "" And Me.statenmBox.Text = "" Then
            Me.addinfo.Visible = True
        Else
            Me.addinfo.Visible = False
        End If
    End Function
#End Region
    Protected Sub LinkButton2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles searchButton.Click
        checkinfo()
        staView.PageIndex = 0
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        Dim stateEntity As New clsState()
        ''''''''''''user info''''''''''''''
        stateEntity.ModifyBy = Session("userID")

        '''''''''''''''''''''''''''''''''''
        If (Trim(Me.stateIDBox.Text) <> "") Then
            stateEntity.StateID = stateIDBox.Text
        End If
        If (Trim(Me.statenmBox.Text) <> "") Then
            stateEntity.StateName = statenmBox.Text
        End If
        If (ctryStatDrop.SelectedValue().ToString() <> "") Then
            stateEntity.StateStatus = ctryStatDrop.SelectedItem.Value.ToString()
        End If
        If (Session("userID") <> "ADMIN") Then
            stateEntity.CountryID = Session("login_ctryID").ToString.ToUpper()
        Else
            stateEntity.CountryID = "%"
        End If

        Dim selDS As DataSet = stateEntity.GetState()
        staView.DataSource = selDS
        staView.DataBind()
        Session("statView") = selDS

        checknorecord(selDS)

        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        If (selDS.Tables(0).Rows.Count <> 0) Then
            staView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "STAHD0")
            staView.HeaderRow.Cells(1).Font.Size = 8
            staView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "STAHD1")
            staView.HeaderRow.Cells(2).Font.Size = 8
            staView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "STAHD2")
            staView.HeaderRow.Cells(3).Font.Size = 8
            staView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "STAHD3")
            staView.HeaderRow.Cells(4).Font.Size = 8
            staView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "STAHD4")
            staView.HeaderRow.Cells(5).Font.Size = 8
            staView.HeaderRow.Cells(6).Text = objXmlTr.GetLabelName("EngLabelMsg", "STAHD5")
            staView.HeaderRow.Cells(6).Font.Size = 8
            'staView.HeaderRow.Cells(7).Text = objXmlTr.GetLabelName("EngLabelMsg", "STAHD6")
            'staView.HeaderRow.Cells(7).Font.Size = 8
            'staView.HeaderRow.Cells(8).Text = objXmlTr.GetLabelName("EngLabelMsg", "STAHD7")
            'staView.HeaderRow.Cells(8).Font.Size = 8
        End If
    End Sub

    Protected Sub staView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles staView.PageIndexChanging
        staView.PageIndex = e.NewPageIndex
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        Dim stateEntity As New clsState()
        ''''''''''''user info''''''''''''''
        stateEntity.ModifyBy = Session("userID")

        '''''''''''''''''''''''''''''''''''
        If (Trim(Me.stateIDBox.Text) <> "") Then
            stateEntity.StateID = stateIDBox.Text
        End If
        If (Trim(Me.statenmBox.Text) <> "") Then
            stateEntity.StateName = statenmBox.Text
        End If
        If (ctryStatDrop.SelectedValue().ToString() <> "") Then
            stateEntity.StateStatus = ctryStatDrop.SelectedItem.Value.ToString()
        End If
        If (Session("userID") <> "ADMIN") Then
            stateEntity.CountryID = Session("login_ctryID").ToString.ToUpper()
        Else
            stateEntity.CountryID = "%"
        End If

        Dim stall As DataSet = stateEntity.GetState()
        staView.DataSource = stall
        staView.DataBind()

        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        If (stall.Tables(0).Rows.Count <> 0) Then
            'staView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRID-0001")
            staView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "STAHD0")
            staView.HeaderRow.Cells(1).Font.Size = 8
            staView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "STAHD1")
            staView.HeaderRow.Cells(2).Font.Size = 8
            staView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "STAHD2")
            staView.HeaderRow.Cells(3).Font.Size = 8
            staView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "STAHD3")
            staView.HeaderRow.Cells(4).Font.Size = 8
            staView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "STAHD4")
            staView.HeaderRow.Cells(5).Font.Size = 8
            staView.HeaderRow.Cells(6).Text = objXmlTr.GetLabelName("EngLabelMsg", "STAHD5")
            staView.HeaderRow.Cells(6).Font.Size = 8
            'staView.HeaderRow.Cells(7).Text = objXmlTr.GetLabelName("EngLabelMsg", "STAHD6")
            'staView.HeaderRow.Cells(7).Font.Size = 8
            'staView.HeaderRow.Cells(8).Text = objXmlTr.GetLabelName("EngLabelMsg", "STAHD7")
            'staView.HeaderRow.Cells(8).Font.Size = 8

        End If
    End Sub

    Protected Sub staView_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles staView.RowEditing
        'Dim ds As DataSet = Session("statView")
        Dim staid As String = Me.staView.Rows(e.NewEditIndex).Cells(1).Text
        staid = Server.UrlEncode(staid)
        Dim ctrid As String = Me.staView.Rows(e.NewEditIndex).Cells(5).Text
        ctrid = Server.UrlEncode(ctrid)
        Dim stastat As String = Me.staView.Rows(e.NewEditIndex).Cells(4).Text
        stastat = Server.UrlEncode(stastat)

        Dim i As Integer
        For i = 0 To Me.ctryStatDrop.Items.Count - 1
            If stastat = Me.ctryStatDrop.Items(i).Text Then
                stastat = Me.ctryStatDrop.Items(i).Value
            End If
        Next

        Dim strTempURL As String = "staid=" + staid + "&ctrid=" + ctrid + "&stastat=" + stastat
        strTempURL = "~/PresentationLayer/masterrecord/modifystate.aspx?" + strTempURL
        Response.Redirect(strTempURL)
    End Sub
End Class
