Imports System.Xml
Imports System.Web
Imports SQLDataAccess
Imports BusinessEntity
Imports System.Configuration
Imports System.Web.UI
Imports System.Data
Imports System.Data.SqlClient
Partial Class PresentationLayer_masterrecord_modeltype

    Inherits System.Web.UI.Page
    Public ds As New DataSet()
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try
            Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            Me.addLink.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Add") 'add new
            Me.searchLink.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Search") 'search
            Me.ctryIDLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-0001")
            Me.ProductClassLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-0006")
            Me.ModelIDLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-0002")
            Me.ModelNameLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-0007")
            Me.StatusLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-0009")
            Me.titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-0010")
            Me.LinkButton1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-all")
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.addinfo.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-AR")
            Me.addinfo.Visible = False
            Me.Label1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-NR")
            Me.Label1.Visible = False
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Dim ModelEntity As New clsModelType()
            '''''username''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
          
            'bonds country  id  name
            Dim clscom As New clsCommonClass()
            Dim idnameds As New DataSet()
            Dim rank As Integer = Session("login_rank")
            clscom.rank = rank
            If rank <> 0 Then
                clscom.spctr = Session("login_ctryID")
                clscom.spstat = Session("login_cmpID")
                clscom.sparea = Session("login_svcID")
            End If
            idnameds = clscom.Getcomidname("BB_MASCTRY_IDNAME")
            databonds(idnameds, Me.ctryIDDD)

            'STATUS DROPDROWN LIST BOND 
            Dim cntryStat As New clsrlconfirminf()
            Dim statParam As ArrayList = New ArrayList
            statParam = cntryStat.searchconfirminf("STATUS")
            Dim count As Integer
            Dim statid As String
            Dim statnm As String
            Me.StatusLabDDL.Items.Clear()
            For count = 0 To statParam.Count - 1
                statid = statParam.Item(count)
                statnm = statParam.Item(count + 1)
                count = count + 1
                If statid.Equals("ACTIVE") Or statid.Equals("OBSOLETE") Then
                    Me.StatusLabDDL.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                End If
            Next
            'product class dropdrown list bond 

            Dim pcdtclass As ArrayList = New ArrayList
            pcdtclass = cntryStat.searchconfirminf("PRODUCTCL")
            Dim pcdtclsID As String
            Dim pcdtclsName As String
            Me.ProductClassDDL.Items.Clear()
            Me.ProductClassDDL.Items.Add("")
            For count = 0 To pcdtclass.Count - 1
                pcdtclsID = pcdtclass.Item(count)
                pcdtclsName = pcdtclass.Item(count + 1)
                count = count + 1
                Me.ProductClassDDL.Items.Add(New ListItem(pcdtclsName.ToString(), pcdtclsID.ToString()))
            Next
            '///////////load  country////////////////////////////////////////


            If (Trim(ctryIDDD.Text) <> "") Then
                ModelEntity.CountryID = ctryIDDD.SelectedValue
            End If
            If (Trim(StatusLabDDL.Text) <> "") Then
                ModelEntity.ModelTypeStatus = StatusLabDDL.SelectedValue.ToString()
            End If
            If (Trim(ProductClassDDL.Text) <> "") Then
                ModelEntity.ProductClass = ProductClassDDL.SelectedValue
            End If
            ModelEntity.ModelTypeName = Trim(Me.ModelNameBox.Text.ToString())
            ModelEntity.ModelTypeID = Trim(Me.ModelIDBox.Text.ToString())
            '''''''''''''''''''''''''''''''''''''''''
            ModelEntity.Modifyby = Session("userID").ToString.ToUpper
            ''''''''''''''''''''''''''''''''''''''''''
            Dim modeltypeselds As DataSet = ModelEntity.GetModelTypeSel()
            modeltypeView.DataSource = modeltypeselds
            modeltypeView.DataBind()
            ds = modeltypeselds
            'Session("modeltypeViewDS") = modeltypesall
            modeltypeView.AllowPaging = True
            modeltypeView.AllowSorting = True
            Dim i As Integer = ds.Tables(0).Rows.Count
            'Session("modeltypeViewDS") = modeltypesall
            ''''''''''''''''''''''
            checknorecord(modeltypeselds)
            '''''''''''''''''''''''''''''
            Dim editcol As New CommandField
            editcol.EditText = objXmlTr.GetLabelName("EngLabelMsg", "BB-Edit") '"edit"
            editcol.HeaderImageUrl = "~/PresentationLayer/graph/edit.gif"
            editcol.ShowEditButton = True
            modeltypeView.Columns.Add(editcol)
            modeltypeView.DataBind()
            '''access control'''''''''''''''''''''''''''''''''''''''''''''''
            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "16")
            Me.addLink.Enabled = purviewArray(0)
            editcol.Visible = purviewArray(2)
           
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'add table head text
            If modeltypeselds.Tables(0).Rows.Count > 0 Then
                modeltypeView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODTY-0002")  'model id
                modeltypeView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODTY-0001")   'ctry id
                modeltypeView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODTY-0007")  'model name
                modeltypeView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODTY-0003") 'model alter name
                modeltypeView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODTY-0006") 'productclass
                modeltypeView.HeaderRow.Cells(6).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODTY-0004") 'effective date
                modeltypeView.HeaderRow.Cells(7).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODTY-0008") 'obsodate
                modeltypeView.HeaderRow.Cells(8).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODTY-0005") 'warrant period
                modeltypeView.HeaderRow.Cells(9).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODTY-0009") 'status

                Dim no As Integer
                For no = 1 To 9
                    modeltypeView.HeaderRow.Cells(no).Font.Size = 8
                Next
            End If
          
            Me.ModelIDBox.Focus()
            '//////////////////////////
        End If
    End Sub
#Region " command bond function "
    Public Function databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        Dim row As DataRow
        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            NewItem.Text = Trim(row(0).ToString()) & "-" & Trim(row(1).ToString())
            NewItem.Value = row(0)
            dropdown.Items.Add(NewItem)
        Next
    End Function

#End Region

#Region "��ʾ��Ϣ"
    Public Function checkinfo() As Integer
        If Me.ModelIDBox.Text = "" And Me.ModelNameBox.Text = "" Then
            Me.addinfo.Visible = True
        Else
            Me.addinfo.Visible = False
        End If
    End Function
#End Region
#Region "if have no record "
    Public Function checknorecord(ByVal ds As DataSet) As Integer
        If ds.HasErrors Then
            If ds.Tables(0).Rows.Count = 0 Then
                Me.Label1.Visible = True
                'Me.addinfo.Visible = False
            Else
                Me.Label1.Visible = False
            End If
        End If
        If ds.Tables(0).Rows.Count = 0 Then
            Me.Label1.Visible = True
            'Me.addinfo.Visible = False
        Else
            Me.Label1.Visible = False
        End If
    End Function
#End Region
#Region "search "
    Protected Sub searchLink_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles searchLink.Click
        checkinfo()
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        Dim ModelEntity As New clsModelType()
        If (Trim(ctryIDDD.Text) <> "") Then
            ModelEntity.CountryID = ctryIDDD.SelectedValue
        End If
        If (Trim(StatusLabDDL.Text) <> "") Then
            ModelEntity.ModelTypeStatus = StatusLabDDL.SelectedValue.ToString()
        End If
        If (Trim(ProductClassDDL.Text) <> "") Then
            ModelEntity.ProductClass = ProductClassDDL.SelectedValue
        End If
        ModelEntity.ModelTypeName = Trim(Me.ModelNameBox.Text.ToString())
        ModelEntity.ModelTypeID = Trim(Me.ModelIDBox.Text.ToString())
        '''''''''''''''''''''''''''''''''''''''''
        ModelEntity.Modifyby = Session("userID").ToString.ToUpper
        ''''''''''''''''''''''''''''''''''''''''''
        Dim modeltypeselds As DataSet = ModelEntity.GetModelTypeSel()
        modeltypeView.DataSource = modeltypeselds
        modeltypeView.DataBind()
        ds = modeltypeselds
        'Session("modeltypeViewDS") = modeltypeselds
        checknorecord(modeltypeselds)
        If modeltypeselds.Tables(0).Rows.Count > 0 Then
            modeltypeView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODTY-0002")  'model id
            modeltypeView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODTY-0001")   'ctry id
            modeltypeView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODTY-0007")  'model name
            modeltypeView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODTY-0003") 'model alter name
            modeltypeView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODTY-0006") 'productclass
            modeltypeView.HeaderRow.Cells(6).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODTY-0004") 'effective date
            modeltypeView.HeaderRow.Cells(7).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODTY-0008") 'obsodate
            modeltypeView.HeaderRow.Cells(8).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODTY-0005") 'warrant period
            modeltypeView.HeaderRow.Cells(9).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODTY-0009") 'status
            'modeltypeView.HeaderRow.Cells(10).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE") 'cr date
            'modeltypeView.HeaderRow.Cells(11).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFBY")
            'modeltypeView.HeaderRow.Cells(12).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFDATE")
            Dim no As Integer
            For no = 1 To 9
                modeltypeView.HeaderRow.Cells(no).Font.Size = 8
            Next
        End If
    End Sub
#End Region

    Protected Sub modeltypeView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles modeltypeView.PageIndexChanging
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        modeltypeView.PageIndex = e.NewPageIndex
        Dim ModelEntity As New clsModelType()
        If (Trim(ctryIDDD.Text) <> "") Then
            ModelEntity.CountryID = ctryIDDD.SelectedValue
        End If
        If (Trim(StatusLabDDL.Text) <> "") Then
            ModelEntity.ModelTypeStatus = StatusLabDDL.SelectedValue.ToString()
        End If
        If (Trim(ProductClassDDL.Text) <> "") Then
            ModelEntity.ProductClass = ProductClassDDL.SelectedValue
        End If
        ModelEntity.ModelTypeName = Trim(Me.ModelNameBox.Text.ToString())
        ModelEntity.ModelTypeID = Trim(Me.ModelIDBox.Text.ToString())
        '''''''''''''''''''''''''''''''''''''''''
        'ModelEntity.Modifyby = Session("userID").ToString.ToUpper
        ''''''''''''''''''''''''''''''''''''''''''
        Dim modeltypeselds As DataSet = ModelEntity.GetModelTypeSel()
        Dim modeltypesall As DataSet = modeltypeselds
        modeltypeView.DataSource = modeltypesall
        modeltypeView.DataBind()
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        If modeltypesall.Tables(0).Rows.Count > 0 Then
            modeltypeView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODTY-0002")  'model id
            modeltypeView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODTY-0001")   'ctry id
            modeltypeView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODTY-0007")  'model name
            modeltypeView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODTY-0003") 'model alter name
            modeltypeView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODTY-0006") 'productclass
            modeltypeView.HeaderRow.Cells(6).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODTY-0004") 'effective date
            modeltypeView.HeaderRow.Cells(7).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODTY-0008") 'obsodate
            modeltypeView.HeaderRow.Cells(8).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODTY-0005") 'warrant period
            modeltypeView.HeaderRow.Cells(9).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODTY-0009") 'status
            'modeltypeView.HeaderRow.Cells(10).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE") 'cr date
            'modeltypeView.HeaderRow.Cells(11).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFBY")
            'modeltypeView.HeaderRow.Cells(12).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFDATE")
            Dim no As Integer
            For no = 1 To 9
                modeltypeView.HeaderRow.Cells(no).Font.Size = 8
            Next
        End If
    End Sub

    Protected Sub modeltypeView_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles modeltypeView.RowEditing

        'Dim ds As DataSet = Session("modeltypeViewDS")
        Dim page As Integer = modeltypeView.PageIndex * modeltypeView.PageSize

        Dim ctryid As String = Me.modeltypeView.Rows(e.NewEditIndex).Cells(2).Text

        'Dim ctryid As String = ds.Tables(0).Rows(page + e.NewEditIndex).Item(1).ToString()
        ctryid = Server.UrlEncode(ctryid)
        Dim modeltypeid As String = Me.modeltypeView.Rows(e.NewEditIndex).Cells(1).Text

        modeltypeid = Server.UrlEncode(modeltypeid)
        Dim mdfystat As String = Me.modeltypeView.Rows(e.NewEditIndex).Cells(9).Text
        Dim i As Integer
        For i = 0 To Me.StatusLabDDL.Items.Count - 1
            If mdfystat = Me.StatusLabDDL.Items(i).Text Then
                mdfystat = Me.StatusLabDDL.Items(i).Value
            End If
        Next
        mdfystat = Server.UrlEncode(mdfystat)
        Dim strTempURL As String = "ctryid=" + ctryid + "&mdfystat=" + mdfystat + "&mdtyid=" + modeltypeid
        strTempURL = "~/PresentationLayer/masterrecord/modifymodeltype.aspx?" + strTempURL
        Response.Redirect(strTempURL)

    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton1.Click

        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        Dim ModelEntity As New clsModelType()

        ModelEntity.ModelTypeStatus = "%"
        ModelEntity.ProductClass = "%"


        If (Trim(ctryIDDD.Text) <> "") Then
            ModelEntity.CountryID = ctryIDDD.SelectedValue
        End If
        'If (Trim(StatusLabDDL.Text) <> "") Then
        '    ModelEntity.ModelTypeStatus = StatusLabDDL.SelectedValue.ToString()
        'End If
        'If (Trim(ProductClassDDL.Text) <> "") Then
        '    ModelEntity.ProductClass = ProductClassDDL.SelectedValue
        'End If
        ModelEntity.ModelTypeName = "%"
        ModelEntity.ModelTypeID = "%"
        '''''''''''''''''''''''''''''''''''''''''
        ModelEntity.Modifyby = Session("userID").ToString.ToUpper
        ''''''''''''''''''''''''''''''''''''''''''
        Dim modeltypeselds As DataSet = ModelEntity.GetModelTypeSel()
        modeltypeView.DataSource = modeltypeselds
        modeltypeView.DataBind()
        ds = modeltypeselds
        'Session("modeltypeViewDS") = modeltypeselds
        checknorecord(modeltypeselds)
        If modeltypeselds.Tables(0).Rows.Count > 0 Then
            modeltypeView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODTY-0002")  'model id
            modeltypeView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODTY-0001")   'ctry id
            modeltypeView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODTY-0007")  'model name
            modeltypeView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODTY-0003") 'model alter name
            modeltypeView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODTY-0006") 'productclass
            modeltypeView.HeaderRow.Cells(6).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODTY-0004") 'effective date
            modeltypeView.HeaderRow.Cells(7).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODTY-0008") 'obsodate
            modeltypeView.HeaderRow.Cells(8).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODTY-0005") 'warrant period
            modeltypeView.HeaderRow.Cells(9).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODTY-0009") 'status

            Dim no As Integer
            For no = 1 To 9
                modeltypeView.HeaderRow.Cells(no).Font.Size = 8
            Next
        End If
    End Sub
End Class
