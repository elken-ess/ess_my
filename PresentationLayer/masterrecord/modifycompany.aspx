<%@ Page Language="VB" AutoEventWireup="false" EnableEventValidation ="false"  CodeFile="modifycompany.aspx.vb" Inherits="PresentationLayer_masterrecord_modifycompany" %>

<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
        <LINK href="../css/style.css" type="text/css" rel="stylesheet">
</head>
<body>
    <form id="form1" runat="server">
    <TABLE border="0" id=companytab style="width: 100%">
    <tr>
           <td style="width: 100%">
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>
							<TD width="1%" background="../graph/title_bg.gif"><IMG height="24" src="../graph/title1.gif" width="5"></TD>
							<TD class="style2" width="98%" background="../graph/title_bg.gif">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></TD>
							<TD align="right" width="1%" background="../graph/title_bg.gif"><IMG height="24" src="../graph/title_2.gif" width="5"></TD>
						</TR>
			 </TABLE>
           </tr>
             <tr>
           <td style="width: 100%">
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>					
							<TD class="style2" width="98%" background="../graph/title_bg.gif">
                                <font color=red><asp:Label ID="errlab" runat="server" Text="Label" Visible=false></asp:Label></font></TD>
						</TR>
			 </TABLE>
               <asp:Label ID="Label1" runat="server" ForeColor="Red"></asp:Label>
             </tr>
			<TR>
				<TD>
					<TABLE id="company" cellSpacing="1" cellPadding="2" bgColor="#b7e6e6" border="0" style="width: 100%">
                        <tr bgcolor="#ffffff">
                            <td align="right" width="15%">
                                                <asp:Label ID="ctridlab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%">
                                                <asp:DropDownList ID="ctrid" runat="server" CssClass="textborder" AutoPostBack="True" style="width: 86%">
                                                </asp:DropDownList>
                                <span style="color: #ff0000; font-weight: bold;">*</span></td>
                            <td align="right" width="15%">
                                                <asp:Label ID="staidlab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%">
                                                <asp:DropDownList ID="staid" runat="server" CssClass="textborder" Width="160px" style="width: 86%" AutoPostBack="True">
                                                </asp:DropDownList>
                                <span style="color: #ff0000; font-weight: bold;">*</span></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" width="15%">
                                                <asp:Label ID="areaidlab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%"  colspan =3>
                                                <asp:DropDownList ID="areaid" runat="server" CssClass="textborder" style="width: 35%">
                                                </asp:DropDownList>
                                <span style="color: #ff0000; font-weight: bold;">*</span></td>
                            
                        </tr>
									  <TR bgColor="#ffffff">
											<TD  align=right width="15%">
                                                &nbsp;<asp:Label ID="compidlab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%">
                                                <asp:TextBox ID="compidbox" runat="server" CssClass="textborder" MaxLength="10" style="width: 86%"></asp:TextBox>
                                                <font color=red style="font-weight: bold">*</font>&nbsp;
                                                </TD>
											<TD  align=right width="15%">
                                                <asp:Label ID="compnameLab" runat="server" Text="Label"></asp:Label></TD>
											<TD  align=left style="width: 35%">
                                                <asp:TextBox ID="compnmbox" runat="server" CssClass="textborder" style="width: 86%" MaxLength="50"></asp:TextBox>
                                                <font color=red style="font-weight: bold">*</font>
                                                </TD>
										</TR>
										<TR bgColor="#ffffff">
											<TD  align=right width="15%">
                                                <asp:Label ID="alterLab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%"  >
                                                <asp:TextBox ID="altbox" runat="server" CssClass="textborder" MaxLength="50" style="width: 86%"></asp:TextBox>
                                                <font color=red style="font-weight: bold">*</font>
                                                <asp:RequiredFieldValidator ID="alterr" runat="server" ControlToValidate="altbox" ForeColor="White">*</asp:RequiredFieldValidator></TD>
											<TD align=right width="15%">
                                                <asp:Label ID="statusLab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%">
                                                <asp:DropDownList ID="ctrystat" runat="server" CssClass="textborder" style="width: 86%">
                                                </asp:DropDownList>
											</TD>
										</TR>
										<TR bgColor="#ffffff">
											<TD  align=right width="15%" style="height: 30px">
                                                <asp:Label ID="faxlab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%; height: 30px;">
                                                <asp:TextBox ID="faxbox" runat="server" CssClass="textborder" MaxLength="20" style="width: 86%"></asp:TextBox>
                                                <font color=red style="font-weight: bold">*</font>
                                                </TD>
                                            <TD  align=right width="15%" style="height: 30px">
                                                <asp:Label ID="pocodelab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="height: 30px; width: 35%;">
                                                <asp:TextBox ID="pocodebox" runat="server" CssClass="textborder" style="width: 86%" MaxLength="20"></asp:TextBox>
                                                <font color=red style="font-weight: bold">*</font>
                                                </TD>
										
										</TR>
										<TR bgColor="#ffffff">
											<TD align=right width="15%">
                                                <asp:Label ID="emailLab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left width="85%" colspan =3>
                                                <asp:TextBox ID="emailbox" runat="server" CssClass="textborder" Width="416px" style="width: 95%" MaxLength="30"></asp:TextBox>
                                                </TD>
											 
										</TR>
										<TR bgColor="#ffffff">
											<TD align=right width="15%" style="height: 30px">
                                                <asp:Label ID="ad1lab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left width="85%" colspan =3 style="height: 30px">
                                                <asp:TextBox ID="ad1box" runat="server" CssClass="textborder" Width="416px" style="width: 95%" MaxLength="100"></asp:TextBox>
                                                <font color=red style="font-weight: bold">*</font>
                                                </TD>
											 
										</TR>
										<TR bgColor="#ffffff">
											<TD align=right width="15%">
                                                <asp:Label ID="ad2lab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left width="85%" colspan =3>
                                                <asp:TextBox ID="ad2box" runat="server" CssClass="textborder" Width="416px" style="width: 95%" MaxLength="100"></asp:TextBox>
                                                <font color=red style="font-weight: bold">*</font>
                                                </TD>
											 
										</TR>
										<TR bgColor="#ffffff">
											<TD align=right width="15%">
                                                <asp:Label ID="tel1lab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%">
                                                <asp:TextBox ID="tel1box" runat="server" CssClass="textborder" MaxLength="20" style="width: 86%"  ></asp:TextBox>
                                                <font color=red style="font-weight: bold">*</font>
                                                </TD>
											<TD align=right width="15%">
                                                <asp:Label ID="tel2lab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%">
                                                <asp:TextBox ID="tel2box" runat="server" CssClass="textborder" Width="152px" style="width: 86%" MaxLength="20"  ></asp:TextBox>
                                            </TD>
										</TR>
										<TR bgColor="#ffffff">
											<TD align=right width="15%">
                                                <asp:Label ID="creatby" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%">
                                                <asp:TextBox ID="creatbybox" runat="server" CssClass="textborder" ReadOnly=true style="width: 86%"></asp:TextBox></TD>
											<TD align=right width="15%">
                                                <asp:Label ID="creatdate" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%">
                                                <asp:TextBox ID="creatdtbox" runat="server" CssClass="textborder" ReadOnly=true Width="152px" style="width: 86%"></asp:TextBox></TD>
										</TR>
										<TR bgColor="#ffffff">
											<TD align=right width="15%">
                                                <asp:Label ID="modfyby" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%">
                                                <asp:TextBox ID="modfybybox" runat="server" CssClass="textborder" ReadOnly=true style="width: 86%"></asp:TextBox></TD>
											<TD align=right width="15%">
                                                <asp:Label ID="mdfydate" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%">
                                                <asp:TextBox ID="mdfydtbox" runat="server" CssClass="textborder" ReadOnly=true Width="152px" style="width: 86%"></asp:TextBox></TD>
										</TR>
								</TABLE>
						</TD>
					</TR>
					<tr>
					    <td>
                            <asp:Label ID="Label2" runat="server" ForeColor="Red"></asp:Label></td>
					</tr>
					<TR>
						<TD>
							<TABLE id="Table1" cellSpacing="1" cellPadding="1"  border="0">
									<TR>
										<TD style="WIDTH: 20%" align=center>
                                            &nbsp;<asp:LinkButton ID="saveButton" runat="server"></asp:LinkButton></TD>
										<td align=center>
                                            <asp:HyperLink ID="cancelLink" runat="server" NavigateUrl="~/PresentationLayer/masterrecord/company.aspx">[cancelLink]</asp:HyperLink></td>
									</TR>
							</TABLE>
						</TD>
					</TR>
			</TABLE>
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
            ShowSummary="False" />
        &nbsp;<asp:RequiredFieldValidator ID="ctriderr" runat="server" ControlToValidate="ctrid"
            ForeColor="White">*</asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator ID="areiderr" runat="server" ControlToValidate="areaid"
            ForeColor="White">*</asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator ID="staiderr" runat="server" ControlToValidate="staid"
            ForeColor="White">*</asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="faxerr" runat="server" ValidationExpression="\d{0,20}" ControlToValidate="faxbox" ForeColor="White">*</asp:RegularExpressionValidator>
                                                <asp:RegularExpressionValidator ID="tel1err" runat="server" ValidationExpression="\d{0,20}" ControlToValidate="tel1box" ForeColor="White">*</asp:RegularExpressionValidator>
                                                <asp:RegularExpressionValidator ID="tel2err" runat="server" ValidationExpression="\d{0,20}" ControlToValidate="tel2box" ForeColor="White">*</asp:RegularExpressionValidator>
                                                <asp:RequiredFieldValidator ID="pocoderr" runat="server" ControlToValidate="pocodebox" ForeColor="White">*</asp:RequiredFieldValidator>
                                                <asp:RequiredFieldValidator ID="comnmerr" runat="server" ControlToValidate="compnmbox" ForeColor="White">*</asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="emailerr" runat="server" ControlToValidate="emailbox" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ForeColor="White">*</asp:RegularExpressionValidator>
                                                <asp:RequiredFieldValidator ID="ad1err" runat="server" ControlToValidate="ad1box" ForeColor="White">*</asp:RequiredFieldValidator>
                                                <asp:RequiredFieldValidator ID="ad2err" runat="server" ControlToValidate="ad2box" ForeColor="White">*</asp:RequiredFieldValidator>&nbsp;
        <asp:RegularExpressionValidator ID="poerr" runat="server" ControlToValidate="pocodebox"
            ForeColor="White" ValidationExpression="\d{0,20}">*</asp:RegularExpressionValidator>
        <asp:RequiredFieldValidator ID="telerr1" runat="server" ControlToValidate="tel1box"
            ForeColor="White">*</asp:RequiredFieldValidator>
        <cc1:messagebox id="MessageBox1" runat="server"></cc1:messagebox>
                                                <asp:RequiredFieldValidator ID="comiderr" runat="server" ControlToValidate="compidbox" ForeColor="White">*</asp:RequiredFieldValidator>
    </form>
</body>
</html>
