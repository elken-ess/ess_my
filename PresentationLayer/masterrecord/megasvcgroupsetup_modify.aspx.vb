﻿Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Partial Class PresentationLayer_masterrecord_megasvcgroupsetup_modify
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try


            Dim _id As String = Request.Params("id").ToString()


            Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            saveButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Save")
            cancelLink.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Cancel")
            Label1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            Label2.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")

            Dim Conn As New SqlConnection
            Conn.ConnectionString = ConfigurationManager.AppSettings("ConnectionString").ToString

            If ddlmodel.SelectedValue = "" Then
                Dim Comm2 As SqlCommand
                Comm2 = Conn.CreateCommand
                Comm2.CommandText = "select mmod_modid from mmod_fil with (nolock) where mmod_ctrid = 'MY' and mmod_stat = 'active'"
                Conn.Open()

                Dim dr2 As SqlDataReader = Comm2.ExecuteReader(CommandBehavior.CloseConnection)

                Dim dt2 As DataTable = New DataTable()
                dt2.Load(dr2)

                Dim counter2 As Integer

                For counter2 = 0 To dt2.Rows.Count - 1
                    ddlmodel.Items.Add(dt2.Rows(counter2).Item("mmod_modid").ToString)
                Next
                Conn.Close()

                ddlmodel.SelectedIndex = 0
            End If

            If _id.Trim() <> "" Then
                Dim _class As New ClsMegaSVCGroupSetup()
                _class.id = _id

                Dim retnArray As ArrayList = _class.GetDataDetails()
                Dim id As Integer

                txtid.Text = retnArray(0).ToString()
                txtgroup.Text = retnArray(1).ToString()
                ddlmodel.SelectedValue = retnArray(2).ToString()
                txtmonths.Text = retnArray(3).ToString()
                txtinterval.Text = retnArray(4).ToString()
                txtfrequency.Text = retnArray(5).ToString()
                ddlstat.SelectedValue = retnArray(10).ToString()
                'create the dropdownlist
                Dim cntryStat As New clsUtil()
                Dim statParam As ArrayList = New ArrayList
                statParam = cntryStat.searchconfirminf("STATUS")
                Dim count As Integer


                Dim Rcreatby As String = retnArray(6).ToString()
                If Rcreatby <> "ADMIN" Then
                    Dim Rcreat As New clsCommonClass
                    Rcreat.userid() = Rcreatby
                    Dim Rcreatds As New DataSet()
                    Rcreatds = Rcreat.Getuseridname("BB_MASUSER_SelByIDName")

                    Me.txtcreatedby.Text = Rcreatds.Tables(0).Rows(0).Item(0)
                Else
                    Me.txtcreatedby.Text = "ADMIN-ADMIN"
                End If

                Dim cntryDate As New clsUtil()
                Dim strs As String = retnArray(7).ToString()
                txtcreateddate.Text = retnArray(7).ToString()
                txtcreateddate.ReadOnly = True

                Dim userIDNamestr As String = Session("userID").ToString().ToUpper + "-" + Session("username").ToString.ToUpper
                txtmodifiedby.Text = userIDNamestr
                txtmodifiedby.ReadOnly = True
                txtmodifieddate.Text = retnArray(9).ToString()
                txtmodifieddate.ReadOnly = True
            End If
        End If
    End Sub

    Protected Sub saveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveButton.Click
        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim msginfo As String = objXm.GetLabelName("StatusMessage", "SAVEORNOT")
        Dim msgtitle As String = objXm.GetLabelName("StatusMessage", "SAVETITLE")
        MessageBox1.Confirm(msginfo)




    End Sub

    Protected Sub MessageBox1_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox1.GetMessageBoxResponse
        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
            Dim _class As New ClsMegaSVCGroupSetup()
            _class.ID = txtid.Text

            If (Trim(txtgroup.Text) <> "") Then
                _class.Group = txtgroup.Text
            End If
            If (Trim(ddlmodel.SelectedValue) <> "") Then
                _class.Model = ddlmodel.SelectedValue
            End If
            If (Trim(txtmonths.Text) <> "") Then
                _class.Months = txtmonths.Text
            End If
            If (Trim(txtinterval.Text) <> "") Then
                _class.Interval = txtinterval.Text
            End If
            If (Trim(txtfrequency.Text) <> "") Then
                _class.Frequency = txtfrequency.Text
            End If
            If (Trim(txtmodifiedby.Text) <> "") Then
                _class.ModifiedBy = Session("userID").ToString().ToUpper
            End If
            If (Trim(ddlstat.SelectedValue) <> "") Then
                _class.Stat = ddlstat.SelectedValue
            End If
            '''''''''''''''''''''
            Dim updateCount As Integer = _class.Update()
            If updateCount = 0 Then
                Dim objXm As New clsXml
                objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                errlab.Text = objXm.GetLabelName("StatusMessage", "BB-HintMsg-Suc")
                errlab.Visible = True
            Else
                Response.Redirect("~/PresentationLayer/Error.aspx")
            End If

        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
        End If
    End Sub
    Protected Sub txtmonths_TextChanged(sender As Object, e As EventArgs) Handles txtmonths.TextChanged

    End Sub
End Class
