Imports BusinessEntity
Imports System.Configuration
Imports System.Windows.Forms
Imports System.Data
Imports System.Data.SqlClient

Partial Class PresentationLayer_masterrecord_CustomerRecord
    Inherits System.Web.UI.Page
    Dim CustomerSet As String
    Dim ROIDSet As String
    Dim fstrDelimiter As String = "|$|"
    Dim fstrFirstDelimiter As String = "&"
    Dim fstrCustomerIDDelimiter As String = "**"
    'Dim tomastel As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try

        'LW Added part: To add for ajax
        AjaxPro.Utility.RegisterTypeForAjax(GetType(PresentationLayer_masterrecord_CustomerRecord))

        If (Not Page.IsPostBack) Then

            BindGrid()
        End If
        DisplayGridHeader()
    End Sub


    Protected Sub BindGrid()
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        customerIDlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0001")
        customernamelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0002")
        areaIDlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0003")
        contactlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0004")
        roserialnolab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0005")
        addButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Add")
        searchButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Search")
        titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BROCUSTOM")
        Label2.Text = objXmlTr.GetLabelName("EngLabelMsg", "CUST_FOUND")
        cboStatus.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Status")

        ' Added by Ryan Estandarte 17 July 2012
        lblModelID.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODTY-0002")
        lblState.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTPACK-0001")
        lblPostCode.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCOMP-0006")
        lblAddress1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_D_ADDR1")
        lblAddress2.Text = objXmlTr.GetLabelName("EngLabelmsg", "BB_FUN_D_ADDR2")

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Me.addinfo.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-AR")
        Me.addinfo.Visible = False
        Me.Label1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-NR")
        Me.Label1.Visible = False
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Dim userIDNamestr As String = Session("userID").ToString().ToUpper + "-" + Session("username").ToString.ToUpper
        Dim rank As String = Session("login_rank")
        Dim customer As New clsCustomerRecord()
        customer.username = userIDNamestr

        Dim area As New clsCommonClass
        If rank <> 0 Then
            area.spctr = Session("login_ctryID").ToString().ToUpper
        End If
        area.rank = rank

        Dim areads As New DataSet

        areads = area.Getcomidname("BB_MASAREA_CUSTIDNAME")
        If areads.Tables.Count <> 0 Then
            databonds(areads, Me.areaID)
        End If
        Me.areaID.Items.Insert(0, "")

        'Added by Ryan Estandarte 17 July 2012
        Dim clsrptcust As New ClsRptCUSR
        clsrptcust.userid = Session("userID").ToString()
        Dim stateDS As DataSet = clsrptcust.SVCnStatebyUserID()
        If stateDS.Tables.Count > 0 Then
            Dim stateid As String
            Dim statenm As String
            For i As Integer = 0 To stateDS.Tables(0).Rows.Count - 1
                statenm = stateDS.Tables(0).Rows(i).Item(0).ToString() + "-" + stateDS.Tables(0).Rows(i).Item(1).ToString()
                stateid = stateDS.Tables(0).Rows(i).Item(0).ToString()
                ddState.Items.Add(New ListItem(statenm, stateid))
            Next
        End If
        ddState.Items.Insert(0, "")

        ' Added by Ryan Estandarte 18 July 2012
        Dim common As New clsCommonClass
        common.spctr = Session("login_ctryID").ToString()
        common.spstat = String.Empty
        common.sparea = String.Empty
        common.rank = String.Empty

        Dim modelDS As DataSet = common.Getcomidname("BB_MASMOTY_IDNAME")
        If modelDS.Tables(0).Rows.Count > 0 Then
            databonds(modelDS, ddModelID)
        End If
        ddModelID.Items.Insert(0, String.Empty)

        'create the dropdownlist
        Dim stat As New clsrlconfirminf()
        Dim param As ArrayList = New ArrayList
        param = stat.searchconfirminf("STATUS")
        Dim count As Integer
        Dim statid As String
        Dim statnm As String
        count = 0
        For count = 0 To param.Count - 1
            statid = param.Item(count)
            statnm = param.Item(count + 1)
            count = count + 1
            If statid.Equals("ACTIVE") Or statid.Equals("OBSOLETE") Then
                cboStatus.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
            End If
        Next



        Dim telepass As New clsCommonClass()


        Dim CustomerEntity As New clsCustomerRecord()
        CustomerEntity.ModBy = userIDNamestr
        If (Trim(Me.contactbox.Text) <> "") Then

            CustomerEntity.Contact = contactbox.Text.ToUpper()
            CustomerEntity.Contacttype = "Y"
        Else
            CustomerEntity.Contacttype = "N"
        End If
        'Dim rank As String = Session("login_rank")
        If rank <> 0 Then
            CustomerEntity.ctryid = Session("login_ctryID")
            CustomerEntity.compid = Session("login_cmpID")
            CustomerEntity.usvcid = Session("login_svcID")

        End If
        CustomerEntity.rank = rank
        CustomerEntity.Status = "ACTIVE"

        ctryView.AllowPaging = True
        ctryView.AllowSorting = True


        '----------------------------------------------------------------------------------------
        'MODIFIED BY ASRAR
        '----------------------------------------------------------------------------------------
        Dim accessgroup As String = Session("accessgroup").ToString
        Dim purviewArray As Array = New Boolean() {False, False, False, False}
        purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "04")
        Me.addButton.Enabled = purviewArray(0)


        '----------------------------------------------------------------------------------------
        'LW Added
        '----------------------------------------------------------------------------------------

        'Label
        RacesLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0011")
        CustomerTypeLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0012")

        'List Customer type in drop down 
        Dim custtype As New clsrlconfirminf()
        Dim custParam As ArrayList = New ArrayList
        custParam = custtype.searchconfirminf("CUSTTYPE")
        Dim custcount As Integer
        Dim custid As String
        Dim custnm As String
        Me.CustomerTypeDLL.Items.Add(New ListItem("ALL", "ALL"))
        For custcount = 0 To custParam.Count - 1
            custid = custParam.Item(custcount)
            custnm = custParam.Item(custcount + 1)
            custcount = custcount + 1
            Me.CustomerTypeDLL.Items.Add(New ListItem(custnm.ToString(), custid.ToString()))

        Next

        'List Race in drop down 
        Dim race As New clsrlconfirminf()
        Dim raceParam As ArrayList = New ArrayList
        raceParam = race.searchconfirminf("PERSONRACE")
        Dim racecount As Integer
        Dim raceid As String
        Dim racenm As String
        Me.RacesDLL.Items.Add(New ListItem("ALL", "ALL"))
        For racecount = 0 To raceParam.Count - 1
            raceid = raceParam.Item(racecount)
            racenm = raceParam.Item(racecount + 1)
            racecount = racecount + 1
            'If Not raceid.Equals("ZNONAPPL") Then
            Me.RacesDLL.Items.Add(New ListItem(racenm.ToString(), raceid.ToString()))
            'End If
        Next

    End Sub

    Sub DisplayGridHeader()
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        If ctryView.Rows.Count >= 1 Then

            ctryView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0001")
            ctryView.HeaderRow.Cells(1).Font.Size = 8
            ctryView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0002")
            ctryView.HeaderRow.Cells(2).Font.Size = 8
            ctryView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASADD1Z")
            ctryView.HeaderRow.Cells(3).Font.Size = 8
            ctryView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASADD2Z")
            ctryView.HeaderRow.Cells(4).Font.Size = 8
            ctryView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPOCODEZ")
            ctryView.HeaderRow.Cells(5).Font.Size = 8
            ctryView.HeaderRow.Cells(6).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0005")
            ctryView.HeaderRow.Cells(6).Font.Size = 8
            ctryView.HeaderRow.Cells(7).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-ModelID")
            ctryView.HeaderRow.Cells(7).Font.Size = 8
            ctryView.HeaderRow.Cells(8).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0016")
            ctryView.HeaderRow.Cells(8).Font.Size = 8
            ctryView.HeaderRow.Cells(9).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0009")
            ctryView.HeaderRow.Cells(9).Font.Size = 8
            ctryView.HeaderRow.Cells(10).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0008")
            ctryView.HeaderRow.Cells(10).Font.Size = 8
            ctryView.HeaderRow.Cells(11).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASTELE")
            ctryView.HeaderRow.Cells(11).Font.Size = 8

            'ctryView.HeaderRow.Cells(12).Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-CUSTOMERPF")
            'ctryView.HeaderRow.Cells(12).Font.Size = 8
        End If
    End Sub
    Protected Sub search_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles searchButton.Click
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Dim obiXm As New clsXml
        'obiXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        'Dim msginfo As String = obiXm.GetLabelName("StatusMessage", "CUSTMSG")
        'Dim msgtitle As String = obiXm.GetLabelName("StatusMessage", "CUSTMSG")
        'MessageBox1.Confirm(msginfo)
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'modified by deyb
        checkinfo()
        Dim CustomerEntity As New clsCustomerRecord()
        Dim telepass As New clsCommonClass()

        If (Trim(Me.customerIDbox.Text) <> "") Then

            CustomerEntity.CustomerID = customerIDbox.Text.ToUpper()

        End If

        If (Trim(Me.customernamebox.Text) <> "") Then

            CustomerEntity.CustomerName = customernamebox.Text.ToUpper()

        End If

        If (Me.areaID.SelectedValue().ToString() <> "") Then
            CustomerEntity.AreaID = Me.areaID.SelectedValue().ToString().ToUpper()
        End If

        If (Trim(Me.roserialnobox.Text) <> "") Then

            CustomerEntity.RoSerialNo = roserialnobox.Text.ToUpper()

        End If
        If (Trim(Me.contactbox.Text) <> "") Then
            'tomastel = contactbox.Text.ToString()
            CustomerEntity.Contact = contactbox.Text.ToUpper()
            CustomerEntity.Contacttype = "Y"
        Else
            CustomerEntity.Contacttype = "N"
        End If
        Dim rank As String = Session("login_rank")
        If rank <> 0 Then
            CustomerEntity.ctryid = Session("login_ctryID")
            CustomerEntity.compid = Session("login_cmpID")
            CustomerEntity.usvcid = Session("login_svcID")

        End If
        CustomerEntity.rank = rank
        CustomerEntity.Status = cboStatus.SelectedValue


        'If (Me.ctryStatDrop.SelectedValue().ToString() <> "") Then
        '    CustomerEntity.Status = ctryStatDrop.SelectedItem.Value.ToString()
        'End If

        CustomerEntity.ModBy = Session("userID").ToString().ToUpper

        CustomerEntity.CustType = CustomerTypeDLL.SelectedValue
        CustomerEntity.Races = RacesDLL.SelectedValue

        ' Added by Ryan Estandarte 17 July 2012
        CustomerEntity.Address1 = txtAddress1.Text
        CustomerEntity.Address2 = txtAddress2.Text
        CustomerEntity.StateID = ddState.SelectedValue
        CustomerEntity.PostCode = txtPostCode.Text
        'Modified by Ryan Estandarte 18 July 2012
        'CustomerEntity.CustModelID = txtModelID.Text
        CustomerEntity.CustModelID = ddModelID.SelectedValue

        Dim ctryall As DataSet = CustomerEntity.GetAllCustomer()

        Dim iCtr As Integer

        If Not ctryall Is Nothing Then
            For iCtr = 0 To ctryall.Tables(0).Rows.Count - 1

                Dim lstrCustomerID As String = Session("login_ctryID") & fstrCustomerIDDelimiter & ctryall.Tables(0).Rows(iCtr).Item(0).ToString
                Dim lstrCustomerName As String = ConvertCustomerName(ctryall.Tables(0).Rows(iCtr).Item(1).ToString)
                Dim lstrSerNo As String = ctryall.Tables(0).Rows(iCtr).Item(5).ToString
                Dim lstrModelID As String = ctryall.Tables(0).Rows(iCtr).Item(6).ToString
                Dim lstrTelNo As String = contactbox.Text.ToUpper()

                CustomerSet = CustomerSet & fstrFirstDelimiter & lstrCustomerID & fstrDelimiter & lstrCustomerName
                CustomerSet = CustomerSet & fstrDelimiter & lstrSerNo & fstrDelimiter & lstrModelID
                CustomerSet = CustomerSet & fstrDelimiter & lstrTelNo
            Next

            ctryView.DataSource = ctryall

            LblTotRecNo.Text = ctryall.Tables(1).Rows(0).Item(0)
        End If
        checknorecord(ctryall)
        'CustomerEntity.TORS = ctryall.Tables(0).Rows.Count()
        Dim objXmlTr As New clsXml


        ctryView.DataBind()
        'added by deyb
        If ctryView.Rows.Count > 0 Then
            lblEdit.Enabled = True : lblCreateApp.Enabled = True : lblViewInst.Enabled = True : lblCreateInstall.Enabled = True : LnkViewAppt.Enabled = True
            Label2.Visible = True : LblTotRecNo.Visible = True
        Else
            lblEdit.Enabled = False : lblCreateApp.Enabled = False : lblViewInst.Enabled = False : lblCreateInstall.Enabled = False : LnkViewAppt.Enabled = False
            Label2.Visible = False : LblTotRecNo.Visible = False
        End If
        '=========


        Call DisplayGridHeader()
        '========================

        'ctryView.PageIndex = 0
        ''Dim CustomerEntity As New clsCustomerRecord()
        ''Dim rank As String = Session("login_rank")
        'If rank <> 0 Then
        '    CustomerEntity.ctryid = Session("login_ctryID")
        '    CustomerEntity.compid = Session("login_cmpID")
        '    CustomerEntity.usvcid = Session("login_svcID")

        'End If
        'CustomerEntity.rank = rank
        'CustomerEntity.Status = "w"
        'Dim ctryall As DataSet = CustomerEntity.GetAllCustomer()
        'ctryView.DataSource = ctryall
        'ctryView.DataBind()
        'Call DisplayGridHeader()

    End Sub

    Protected Sub ctryView_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles ctryView.DataBound
        Dim chkBox As Label
        Dim varCustomers
        Dim intPageNum As Integer = CInt(ctryView.PageIndex)
        Dim intRecordCount As Integer = 10 * intPageNum
        Dim strValue As String
        Dim telepass As New clsCommonClass()

        If Not Len(CustomerSet) <= 0 Then
            CustomerSet = Right(CustomerSet, (Len(CustomerSet) - 1))
            varCustomers = Split(CustomerSet, fstrFirstDelimiter)
            'varCustomers = Split(CustomerSet, fstrDelimiter)
            Dim i As Integer
            For i = 0 To ctryView.Rows.Count - 1
                chkBox = ctryView.Rows(i).FindControl("lblRadio")
                strValue = varCustomers(intRecordCount + i)
                chkBox.Text = "<input type=radio value=" & Server.UrlEncode(strValue) & " name='selCustom'>"

                ctryView.Rows(i).Cells(11).Text = telepass.passconverttel(ctryView.Rows(i).Cells(11).Text)
            Next
        End If
    End Sub

    Protected Sub ctryView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles ctryView.PageIndexChanging
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        Dim CustomerEntity As New clsCustomerRecord()
        ctryView.PageIndex = e.NewPageIndex


        Dim telepass As New clsCommonClass()

        If (Trim(Me.customerIDbox.Text) <> "") Then

            CustomerEntity.CustomerID = customerIDbox.Text.ToUpper()

        End If

        If (Trim(Me.customernamebox.Text) <> "") Then

            CustomerEntity.CustomerName = customernamebox.Text.ToUpper()

        End If

        If (Me.areaID.SelectedValue().ToString() <> "") Then
            CustomerEntity.AreaID = Me.areaID.SelectedValue().ToString().ToUpper()
        End If

        If (Trim(Me.roserialnobox.Text) <> "") Then

            CustomerEntity.RoSerialNo = roserialnobox.Text.ToUpper()

        End If
        If (Trim(Me.contactbox.Text) <> "") Then

            CustomerEntity.Contact = telepass.telconvertpass(contactbox.Text.ToUpper())
            CustomerEntity.Contacttype = "Y"
        Else
            CustomerEntity.Contacttype = "N"
        End If
        Dim rank As String = Session("login_rank")
        If rank <> 0 Then
            CustomerEntity.ctryid = Session("login_ctryID")
            CustomerEntity.compid = Session("login_cmpID")
            CustomerEntity.usvcid = Session("login_svcID")
        End If
        CustomerEntity.rank = rank
        CustomerEntity.Status = cboStatus.SelectedValue
        CustomerEntity.ModBy = Session("userID").ToString().ToUpper

        CustomerEntity.CustType = CustomerTypeDLL.SelectedValue
        CustomerEntity.Races = RacesDLL.SelectedValue

        ' Added by Ryan Estandarte 18 July 2012
        CustomerEntity.Address1 = txtAddress1.Text
        CustomerEntity.Address2 = txtAddress2.Text
        CustomerEntity.StateID = ddState.SelectedValue
        CustomerEntity.PostCode = txtPostCode.Text
        CustomerEntity.CustModelID = ddModelID.SelectedValue

        Dim Customerall As DataSet = CustomerEntity.GetAllCustomer()
        ctryView.DataSource = Customerall
        Dim iCtr As Integer

        For iCtr = 0 To Customerall.Tables(0).Rows.Count - 1
            'CustomerSet = CustomerSet & fstrFirstDelimiter & Customerall.Tables(0).Rows(iCtr).Item(0).ToString & fstrDelimiter & Customerall.Tables(0).Rows(iCtr).Item(1).ToString
            'CustomerSet = CustomerSet & fstrDelimiter & Customerall.Tables(0).Rows(iCtr).Item(5).ToString & fstrDelimiter & Customerall.Tables(0).Rows(iCtr).Item(6).ToString
            'CustomerSet = CustomerSet & fstrDelimiter & telepass.passconverttel(Customerall.Tables(0).Rows(iCtr).Item(10).ToString)

            Dim lstrCustomerID As String = Session("login_ctryID") & fstrCustomerIDDelimiter & Customerall.Tables(0).Rows(iCtr).Item(0).ToString
            Dim lstrCustomerName As String = ConvertCustomerName(Customerall.Tables(0).Rows(iCtr).Item(1).ToString)
            Dim lstrSerNo As String = Customerall.Tables(0).Rows(iCtr).Item(5).ToString
            Dim lstrModelID As String = Customerall.Tables(0).Rows(iCtr).Item(6).ToString
            Dim lstrTelNo As String = contactbox.Text.ToUpper() 'telepass.passconverttel(Customerall.Tables(0).Rows(iCtr).Item(10).ToString)

            CustomerSet = CustomerSet & fstrFirstDelimiter & lstrCustomerID & fstrDelimiter & lstrCustomerName
            CustomerSet = CustomerSet & fstrDelimiter & lstrSerNo & fstrDelimiter & lstrModelID
            CustomerSet = CustomerSet & fstrDelimiter & lstrTelNo
        Next
        ctryView.DataBind()

        Call DisplayGridHeader()

        Dim chkBox As Label
        Dim varCustomers
        Dim intPageNum As Integer = CInt(ctryView.PageIndex)
        Dim intRecordCount As Integer = 10 * intPageNum
        Dim strValue As String
        If Not Len(CustomerSet) <= 0 Then
            CustomerSet = Right(CustomerSet, (Len(CustomerSet) - 1))
            varCustomers = Split(CustomerSet, fstrFirstDelimiter)
            Dim i As Integer
            For i = 0 To ctryView.Rows.Count - 1
                chkBox = ctryView.Rows(i).FindControl("lblRadio")
                strValue = varCustomers(intRecordCount + i)
                chkBox.Text = "<input type=radio value=" & Server.UrlEncode(strValue) & " name='selCustom'>"
            Next
        End If
    End Sub
#Region " id bond"
    Public Function databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList) As ArrayList
        dropdown.Items.Clear()
        Dim row As DataRow
        Dim infon As ArrayList = New ArrayList()
        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            NewItem.Text = row("id") & "-" & row("name")
            NewItem.Value = row("id")
            dropdown.Items.Add(NewItem)
            infon.Add(NewItem.Value)
        Next
        Return infon

    End Function
#End Region
    Protected Sub ctryView_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles ctryView.RowEditing

        'Dim custid As String = ds.Tables(0).Rows(ctryView.PageIndex * ctryView.PageSize + e.NewEditIndex).Item(0).ToString()
        Dim custid As String = Me.ctryView.Rows(e.NewEditIndex).Cells(2).Text
        custid = Server.UrlEncode(custid)


        'Dim ctrystat As String = ds.Tables(0).Rows(e.NewEditIndex).Item(3).ToString
        'ctrystat = Server.UrlEncode(ctrystat)
        'Dim Prefix As String = custid.ToString.Substring(0, 2)
        'Prefix = Server.UrlEncode(Prefix)
        Dim strTempURL As String = "custid=" + custid '+ "&Prefix=" + Prefix
        strTempURL = "~/PresentationLayer/masterrecord/modifycustomer.aspx?" + strTempURL
        Response.Redirect(strTempURL)

    End Sub
#Region "��ʾ��Ϣ"
    Public Function checkinfo() As Integer
        If Me.customerIDbox.Text = "" And Me.customernamebox.Text = "" And Me.roserialnobox.Text = "" And Me.contactbox.Text = "" And Me.areaID.Text = "" Then
            Me.addinfo.Visible = True
        Else
            Me.addinfo.Visible = False
        End If
    End Function
#End Region
#Region "if have no record "
    Public Function checknorecord(ByVal ds As DataSet) As Integer
        If Not ds Is Nothing Then
            If ds.HasErrors Then
                If ds.Tables(0).Rows.Count = 0 Then
                    Me.Label1.Visible = True

                Else
                    Me.Label1.Visible = False

                End If

            End If

            If ds.Tables(0).Rows.Count = 0 Then
                Me.Label1.Visible = True

            Else
                Me.Label1.Visible = False

            End If
        Else
            Me.Label1.Visible = True
        End If

    End Function
#End Region

    Protected Sub lblEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblEdit.Click
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim gSelectedValue As String

        Dim lstrTemp As String = ""
        Dim strCusID As String = ""
        Dim strPrefix As String = Session("login_ctryid")
        gSelectedValue = Server.UrlDecode(Request.Form("selCustom"))
        If Not gSelectedValue = "" Then
            lstrTemp = Split(gSelectedValue, fstrDelimiter)(0)
            strCusID = Split(lstrTemp, fstrCustomerIDDelimiter)(1)
            strPrefix = Split(lstrTemp, fstrCustomerIDDelimiter)(0)

            Response.Redirect("modifycustomer.aspx?custid=" & strCusID & "&custpf=" & strPrefix)
        Else
            addinfo.Text = objXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-CUSTOMERID")

            addinfo.Visible = True
        End If
    End Sub



    Protected Sub lblCreateInstall_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblCreateInstall.Click
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim gSelectedValue As String

        Dim strCusID As String = ""
        Dim lstrTemp As String = ""

        Dim strPrefix As String = Session("login_ctryid")

        Dim strCusName As String = ""

        gSelectedValue = Server.UrlDecode(Request.Form("selCustom"))

        If Not gSelectedValue = "" Then

            lstrTemp = Split(gSelectedValue, fstrDelimiter)(0)
            strCusID = Split(lstrTemp, fstrCustomerIDDelimiter)(1)
            strPrefix = Split(lstrTemp, fstrCustomerIDDelimiter)(0)


            strCusName = Split(gSelectedValue, fstrDelimiter)(1)

            Dim url As String = "~/PresentationLayer/masterrecord/addinstalleBase.aspx?"

            url &= "custid=" & strCusID & "&"

            url &= "custpre=" & strPrefix & "&"

            url &= "custname=" & strCusName

            Response.Redirect(url)

        Else


            addinfo.Text = objXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-CUSTOMERID")

            addinfo.Visible = True

        End If

    End Sub

    Protected Sub lblCreateApp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblCreateApp.Click
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim strTempURL As String
        Dim gSelectedValue As String

        Dim strCusID As String = ""
        Dim strPrefix As String = Session("login_ctryid")
        Dim strCusName As String = ""
        Dim lstrTelno As String = ""
        Dim strSerial As String = ""
        Dim strMod As String = ""
        Dim lstrTemp As String = ""

        gSelectedValue = Server.UrlDecode(Request.Form("selCustom"))

        If Not gSelectedValue = "" Then

            lstrTemp = Split(gSelectedValue, fstrDelimiter)(0)
            strCusName = Split(gSelectedValue, fstrDelimiter)(1)
            lstrTelno = Split(gSelectedValue, fstrDelimiter)(4)
            strSerial = Split(gSelectedValue, fstrDelimiter)(2)
            strMod = Split(gSelectedValue, fstrDelimiter)(3)

            strCusID = Split(lstrTemp, fstrCustomerIDDelimiter)(1)
            strPrefix = Split(lstrTemp, fstrCustomerIDDelimiter)(0)

            strCusID = Server.UrlEncode(strCusID)
            strPrefix = Server.UrlEncode(strPrefix)

            Dim CustomerEntity As New clsCustomerRecord()
            Dim rouid As String

            CustomerEntity.CustomerID = strCusID
            CustomerEntity.RoSerialNo = strSerial
            CustomerEntity.modelid = strMod
            CustomerEntity.Customerpf = strPrefix
            rouid = CustomerEntity.GetInstallROUID()

            strTempURL = "custID=" + strCusID + "&custPf=" + strPrefix + "&telno=" + lstrTelno + "&ROID=" + rouid
            strTempURL = "~/PresentationLayer/function/addappointment.aspx?" + strTempURL

            Response.Redirect(strTempURL)

        Else
            addinfo.Text = objXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-CUSTOMERID")
            addinfo.Visible = True
        End If

    End Sub



    Protected Sub lblViewInst_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblViewInst.Click
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim rouid As String = ""
        Dim gSelectedValue As String
        Dim strCusID As String = ""
        Dim strPrefix As String = Session("login_ctryid")
        Dim strSerial As String = ""
        Dim strMod As String = ""
        Dim strTempURL As String = ""
        Dim lstrTemp As String = ""

        gSelectedValue = Server.UrlDecode(Request.Form("selCustom"))

        If Not gSelectedValue = "" Then
            lstrTemp = Split(gSelectedValue, fstrDelimiter)(0)
            strSerial = Split(gSelectedValue, fstrDelimiter)(2)
            strMod = Split(gSelectedValue, fstrDelimiter)(3)

            strCusID = Split(lstrTemp, fstrCustomerIDDelimiter)(1)
            strPrefix = Split(lstrTemp, fstrCustomerIDDelimiter)(0)

            'try get the ROUID

            Dim CustomerEntity As New clsCustomerRecord()
            CustomerEntity.CustomerID = strCusID
            CustomerEntity.RoSerialNo = strSerial
            CustomerEntity.modelid = strMod
            CustomerEntity.Customerpf = strPrefix
            rouid = CustomerEntity.GetInstallROUID()

            If rouid Is Nothing Then
                'addinfo.Text = "No records found associated with the selected customer."
                addinfo.Text = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRRO")
                addinfo.Visible = True
            Else

                strTempURL = "rouid=" + rouid
                strTempURL = "~/PresentationLayer/masterrecord/ModifyinstallBase.aspx?" + strTempURL
                Response.Redirect(strTempURL)
            End If
        Else
            addinfo.Text = objXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-CUSTOMERID")
            addinfo.Visible = True
        End If

    End Sub

    Function ConvertCustomerName(ByVal CustomerName As String) As String
        ConvertCustomerName = Server.UrlEncode(CustomerName)
    End Function

    Protected Sub MessageBox1_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox1.GetMessageBoxResponse
        'If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
        '    checkinfo()
        '    Dim CustomerEntity As New clsCustomerRecord()
        '    Dim telepass As New clsCommonClass()

        '    If (Trim(Me.customerIDbox.Text) <> "") Then

        '        CustomerEntity.CustomerID = customerIDbox.Text.ToUpper()

        '    End If

        '    If (Trim(Me.customernamebox.Text) <> "") Then

        '        CustomerEntity.CustomerName = customernamebox.Text.ToUpper()

        '    End If

        '    If (Me.areaID.SelectedValue().ToString() <> "") Then
        '        CustomerEntity.AreaID = Me.areaID.SelectedValue().ToString().ToUpper()
        '    End If

        '    If (Trim(Me.roserialnobox.Text) <> "") Then

        '        CustomerEntity.RoSerialNo = roserialnobox.Text.ToUpper()

        '    End If
        '    If (Trim(Me.contactbox.Text) <> "") Then

        '        CustomerEntity.Contact = contactbox.Text.ToUpper()
        '        CustomerEntity.Contacttype = "Y"
        '    Else
        '        CustomerEntity.Contacttype = "N"
        '    End If
        '    Dim rank As String = Session("login_rank")
        '    If rank <> 0 Then
        '        CustomerEntity.ctryid = Session("login_ctryID")
        '        CustomerEntity.compid = Session("login_cmpID")
        '        CustomerEntity.usvcid = Session("login_svcID")

        '    End If
        '    CustomerEntity.rank = rank

        '    'If (Me.ctryStatDrop.SelectedValue().ToString() <> "") Then
        '    '    CustomerEntity.Status = ctryStatDrop.SelectedItem.Value.ToString()
        '    'End If

        '    CustomerEntity.ModBy = Session("userID").ToString().ToUpper

        '    Dim ctryall As DataSet = CustomerEntity.GetAllCustomer()

        '    Dim iCtr As Integer
        '    For iCtr = 0 To ctryall.Tables(0).Rows.Count - 1
        '        'CustomerSet = CustomerSet & fstrFirstDelimiter & ctryall.Tables(0).Rows(iCtr).Item(0).ToString & fstrDelimiter & ctryall.Tables(0).Rows(iCtr).Item(1).ToString
        '        'CustomerSet = CustomerSet & fstrDelimiter & ctryall.Tables(0).Rows(iCtr).Item(5).ToString & fstrDelimiter & ctryall.Tables(0).Rows(iCtr).Item(6).ToString

        '        Dim lstrCustomerID As String = Session("login_ctryID") & fstrCustomerIDDelimiter & ctryall.Tables(0).Rows(iCtr).Item(0).ToString
        '        Dim lstrCustomerName As String = ConvertCustomerName(ctryall.Tables(0).Rows(iCtr).Item(1).ToString)
        '        Dim lstrSerNo As String = ctryall.Tables(0).Rows(iCtr).Item(5).ToString
        '        Dim lstrModelID As String = ctryall.Tables(0).Rows(iCtr).Item(6).ToString
        '        Dim lstrTelNo As String = contactbox.Text.ToUpper() 'telepass.passconverttel(ctryall.Tables(0).Rows(iCtr).Item(10).ToString)

        '        CustomerSet = CustomerSet & fstrFirstDelimiter & lstrCustomerID & fstrDelimiter & lstrCustomerName
        '        CustomerSet = CustomerSet & fstrDelimiter & lstrSerNo & fstrDelimiter & lstrModelID
        '        CustomerSet = CustomerSet & fstrDelimiter & lstrTelNo
        '    Next

        '    ctryView.DataSource = ctryall

        '    checknorecord(ctryall)
        '    'CustomerEntity.TORS = ctryall.Tables(0).Rows.Count()
        '    Dim objXmlTr As New clsXml


        '    ctryView.DataBind()
        '    'added by deyb
        '    If ctryView.Rows.Count > 0 Then
        '        lblEdit.Enabled = True : lblCreateApp.Enabled = True : lblViewInst.Enabled = True : lblCreateInstall.Enabled = True
        '    Else
        '        lblEdit.Enabled = False : lblCreateApp.Enabled = False : lblViewInst.Enabled = False : lblCreateInstall.Enabled = False
        '    End If
        '    '=========


        '    Call DisplayGridHeader()


        'ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
        '    Me.customerIDbox.Text = ""
        '    Me.customernamebox.Text = ""
        '    areaID.Items(0).Selected = True
        '    Me.roserialnobox.Text = ""
        '    Me.contactbox.Text = ""

        '    Return

        'End If
    End Sub

    Protected Sub LnkViewAppt_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LnkViewAppt.Click
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim strTempURL As String
        Dim gSelectedValue As String
        Dim strCusID As String = ""
        Dim strPrefix As String = Session("login_ctryid")
        Dim strCusName As String = ""
        Dim strSerial As String = ""
        Dim strMod As String = ""
        Dim rouid As String = ""
        Dim lstrTemp As String = ""

        gSelectedValue = Server.UrlDecode(Request.Form("selCustom"))

        If Not gSelectedValue = "" Then
            lstrTemp = Split(gSelectedValue, fstrDelimiter)(0)
            strCusName = Split(gSelectedValue, fstrDelimiter)(1)

            strCusID = Split(lstrTemp, fstrCustomerIDDelimiter)(1)
            strPrefix = Split(lstrTemp, fstrCustomerIDDelimiter)(0)

            strCusID = Server.UrlEncode(strCusID)
            strPrefix = Server.UrlEncode(strPrefix)


            strSerial = Split(gSelectedValue, fstrDelimiter)(2)
            strMod = Split(gSelectedValue, fstrDelimiter)(3)
            strPrefix = Server.UrlEncode(strPrefix)

            If strMod = "" And strSerial = "" Then
                rouid = 0
            Else

                Dim CustomerEntity As New clsCustomerRecord()
                CustomerEntity.CustomerID = strCusID
                CustomerEntity.RoSerialNo = strSerial
                CustomerEntity.modelid = strMod
                CustomerEntity.Customerpf = strPrefix
                rouid = CustomerEntity.GetInstallROUID()
            End If

            If rouid Is Nothing Then
                'addinfo.Text = "No records found associated with the selected customer."
                addinfo.Text = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRRO")
                addinfo.Visible = True
            Else
                strTempURL = "custID=" + strCusID + "&custPf=" + strPrefix + "&custName=" + strCusName + "&roid=" + rouid
                strTempURL = "~/PresentationLayer/function/appointmenthistorydetail.aspx?" + strTempURL
                Response.Redirect(strTempURL)
            End If
        Else
            addinfo.Text = objXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-CUSTOMERID")
            addinfo.Visible = True
        End If

    End Sub

    'LW Added part : Ajax 
    <AjaxPro.AjaxMethod()> _
        Public Function GetRaces(ByVal strCustomerType As String) As ArrayList
        Dim race As New clsrlconfirminf()
        Dim raceParam As ArrayList = New ArrayList
        raceParam = race.searchconfirminf("PERSONRACE")
        Dim racecount As Integer
        Dim raceid As String
        Dim racenm As String
        Dim Listas As New ArrayList(raceParam.Count / 2)
        Dim icnt As Integer = 0
        Dim strTemp As String

        If strCustomerType = "CORPORATE" Then

            Listas.Add("ALL:ALL")
            For racecount = 0 To raceParam.Count - 1
                raceid = raceParam.Item(racecount)
                racenm = raceParam.Item(racecount + 1)
                racecount = racecount + 1
                If raceid.Equals("ZNONAPPL") Then
                    strTemp = raceid & ":" & racenm
                    Listas.Add(strTemp)
                End If

            Next
        ElseIf strCustomerType = "INDIVIDUAL" Then

            Listas.Add("ALL:ALL")
            For racecount = 0 To raceParam.Count - 1
                raceid = raceParam.Item(racecount)
                racenm = raceParam.Item(racecount + 1)
                racecount = racecount + 1
                If Not raceid.Equals("ZNONAPPL") Then
                    strTemp = raceid & ":" & racenm
                    Listas.Add(strTemp)
                End If

            Next
        Else
            Listas.Add("ALL:ALL")
            For racecount = 0 To raceParam.Count - 1
                raceid = raceParam.Item(racecount)
                racenm = raceParam.Item(racecount + 1)
                racecount = racecount + 1

                strTemp = raceid & ":" & racenm
                Listas.Add(strTemp)

            Next

        End If

        Return Listas
    End Function

    Protected Sub areaID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles areaID.SelectedIndexChanged

    End Sub

    Protected Sub customerIDbox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles customerIDbox.TextChanged

    End Sub

    Protected Sub addButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles addButton.Click

    End Sub
End Class
