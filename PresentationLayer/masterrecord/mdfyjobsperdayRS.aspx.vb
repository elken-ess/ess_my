Imports BusinessEntity
Imports System.Configuration
Imports System.Data
Imports SQLDataAccess
Imports System.Data.SqlClient
Partial Class PresentationLayer_masterrecord_mdfyjobsperdayRS
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "13")
            Me.LinkButton1.Enabled = purviewArray(1)
            If purviewArray(2) = False Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return

            End If
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
            '''''''''''''''''''''''''''''''''''''''''''''''''''''
            Try
                Dim str As String = Request.Params("ctryid").ToString
                str = Request.Params("jdate").ToString()
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim mdfyid As String = Request.Params("ctryid").ToString()

            Dim mddate As Date = Request.Params("jdate")

            If mdfyid.Trim() <> "" Then
                Dim jobsEntity As New clsJobsPerDayRS()
                jobsEntity.ServiceCenterID = mdfyid
                jobsEntity.dtdate = mddate
                Dim edtReader As SqlDataReader = jobsEntity.GetJobsDetailsBySvcID()
                If edtReader.Read() Then
                    Me.SVCID.Text = edtReader.GetValue(0).ToString()
                    Me.SVCID.Enabled = False
                    Dim datetoview As New clsCommonClass()

                    Me.JDATEBox.Text = edtReader.GetValue(1).ToString()
                    Me.JDATEBox.ReadOnly = True

                    Me.TOTCHBox.Text = edtReader.GetValue(3).ToString()
                    Me.TOJOBBox.Text = edtReader.GetValue(4).ToString()
                    'Me.totaltdy.Text = edtReader.GetValue(5).ToString()

                    Dim Rcreatby As String = edtReader.Item(5).ToString().ToUpper()
                    If Rcreatby <> "ADMIN" Then
                        Dim Rcreat As New clsCommonClass
                        Rcreat.userid() = Rcreatby
                        Dim Rcreatds As New DataSet()
                        Rcreatds = Rcreat.Getuseridname("BB_MASUSER_SelByIDName")
                        Me.CUSERBox.Text = Rcreatds.Tables(0).Rows(0).Item(0)
                    Else
                        Me.CUSERBox.Text = "ADMIN-ADMIN"
                    End If
                    ' Me.CUSERBox.Text = edtReader.GetValue(5).ToString().ToUpper()

                    Me.CUSERBox.ReadOnly = True
                    Me.CREDTBox.Text = edtReader.GetValue(6).ToString()
                    Me.CREDTBox.ReadOnly = True
                    Dim userIDNamestr As String = Session("userID").ToString().ToUpper + "-" + Session("username").ToString.ToUpper
                    Me.LUSERtBox.Text = userIDNamestr

                    Me.LUSERtBox.ReadOnly = True

                    Me.LSTDTBox.Text = edtReader.GetValue(7).ToString()
                    Me.LSTDTBox.ReadOnly = True

                    'total no of taday job
                    '   Dim jobsEntity As New clsJobsPerDay()
                    If Me.SVCID.Text <> "" Then
                        jobsEntity.ServiceCenterID = SVCID.Text.ToString().Trim().Split("-")(0)
                    End If
                    If Me.JDATEBox.Text <> "" Then
                        Dim temparr As Array = Me.JDATEBox.Text.Split("/")
                        jobsEntity.dtdate = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
                    End If
                    totaltdy.Text = jobsEntity.GetNoTdJobs()

                End If
                Dim objXmlTr As New clsXml
                objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
                svcidLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-JOBS-0001")
                jdateLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-JOBS-0002")
                totchLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-JOBS-0003")
                totaljobLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-JOBS-0004")
                Label5.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
                Label6.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
                Me.totaljobtdyLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-JOBS-0005")

                crtdLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATBY")
                crtddtLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
                mdfyLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFBY")
                mdfydtLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFDATE")
                Me.LinkButton1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Save")
                Me.HyperLink1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Cancel")
                Me.titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASJOBS-0009")
                ' Me.CREDTBox.Text = Date.Now.ToString().Substring(0, 10) 'created date
                ' \\\\\\\\\\\\\\\\\\\\\\\\
                Dim statXmlTr As New clsXml
                statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                Me.RequiredFieldValidator4.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "TJBBSVC")
                Me.RangeValidator1.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "JOBCH")
                Me.RangeValidator2.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "JOBTOTAL")
                Me.RequiredFieldValidator1.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "TCHBLK")
                Me.RequiredFieldValidator2.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "TJBBLK")
                'Me.CompareValidator1.ErrorMessage = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASJOBS-0010")
                Me.CompareValidator1.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "BB-MASJOBS-0010")
                Me.CREDTBox.Enabled = False
                Me.LSTDTBox.Enabled = False
                Me.LUSERtBox.Enabled = False
                Me.CUSERBox.Enabled = False
                Me.totaltdy.Enabled = False
            End If

        End If
    End Sub
#Region "check no of date job >job no of afd when save record "
    Public Function checkno() As Integer
        If TOJOBBox.Text <> "" Then
            If (Convert.ToInt32(TOJOBBox.Text) < Convert.ToInt32(totaltdy.Text)) Then
                Return 1
            Else
                Return 0
            End If
        End If
    End Function
#End Region

#Region "update  record"
    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton1.Click
        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim msginfo As String = objXm.GetLabelName("StatusMessage", "SAVEORNOT")
        Dim msgtitle As String = objXm.GetLabelName("StatusMessage", "SAVETITLE")
        Dim msg As String = objXm.GetLabelName("StatusMessage", "BB-MASJOBS-0010")
        If checkno() = 1 Then
            Me.mdyinfo.Text = msg
            Me.mdyinfo.Visible = True
        Else
            MessageBox1.Confirm(msginfo, msgtitle)
        End If
    End Sub

#End Region

#Region "modify record"
    Public Sub modify()
        Dim jobsEntity As New clsJobsPerDayRS()

        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        If (Trim(Me.SVCID.Text) <> "") Then
            jobsEntity.ServiceCenterID = SVCID.Text.Substring(0, Me.SVCID.Text.IndexOf("-"))
        End If

        If (Trim(JDATEBox.Text) <> "") Then
            Dim datearr As Array = JDATEBox.Text.ToString().Split("/")
            Dim rtdate As String = datearr(2) + "-" + datearr(1) + "-" + datearr(0)
            jobsEntity.dtdate = rtdate
        End If
        If (Trim(TOJOBBox.Text) <> "") Then
            jobsEntity.TotalofJob = TOJOBBox.Text
        End If

        If (Trim(TOTCHBox.Text) <> "") Then
            jobsEntity.NoofTechnician = TOTCHBox.Text
        End If
        If (Trim(Me.LUSERtBox.Text) <> "") Then
            jobsEntity.Modifyby = Session("userID").ToString().ToUpper
        End If
        jobsEntity.LoginIP = Request.UserHostAddress.ToString
        jobsEntity.LoginSvc = Session("login_svcID").ToString

        'check no of job and today 
        '  checkno()
        Dim insJobCnt As Integer = jobsEntity.Update()
        If insJobCnt = 0 Then
            Dim objXm As New clsXml
            objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            mdyinfo.Text = objXm.GetLabelName("StatusMessage", "BB-HintMsg-Suc")
            mdyinfo.Visible = True
        Else
            Response.Redirect("~/PresentationLayer/Error.aspx")
        End If
    End Sub
#End Region

#Region "service center id bond"
    Public Function databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        Dim row As DataRow
        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            NewItem.Text = row("MJOB_SVCID") & "-" & row("MJOB_JDATE")
            NewItem.Value = row("MJOB_SVCID")
            dropdown.Items.Add(NewItem)
        Next
    End Function
#End Region



    Protected Sub MessageBox1_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox1.GetMessageBoxResponse
        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
            modify()
        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
        End If

    End Sub
End Class
