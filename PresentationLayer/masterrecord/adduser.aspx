<%@ Page Language="VB" AutoEventWireup="false" EnableEventValidation ="false"  CodeFile="adduser.aspx.vb" Inherits="PresentationLayer_masterrecord_adduser" %>

<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
  <title>Add User</title>
	<LINK href="../css/style.css" type="text/css" rel="stylesheet">
</head>
<body  id="model type">
   <form id="modeltypeform"  method="post" action=""  runat=server>
   <TABLE border="0" id=TABLE2 style="width: 100%">
           <tr>
           <td>
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>
							<TD width="1%" background="../graph/title_bg.gif"><IMG height="24" src="../graph/title1.gif" width="5"></TD>
							<TD class="style2" width="98%" background="../graph/title_bg.gif">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></TD>
							<TD align="right" width="1%" background="../graph/title_bg.gif"><IMG height="24" src="../graph/title_2.gif" width="5"></TD>
						</TR>
			 </TABLE>
           </tr>
           <tr>
           <td>
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>					
							<TD class="style2" width="98%" background="../graph/title_bg.gif" style="height: 14px">
                                <font color=red><asp:Label ID="addinfo" runat="server" Text="Label" Visible=false></asp:Label></font>
                                </TD>
						</TR>
			 </TABLE>
           </tr>
			<TR>
				<TD>
     <TABLE border="0" id=countrytab style="width: 100%">
			<TR>
				<TD style="width: 0px">
					<TABLE id="modeltype" cellSpacing="1" cellPadding="2" bgColor="#b7e6e6" border="0" style="width: 100%; height: 352px">
					 <tr bgcolor="#ffffff">
<td colspan = 4>
                            <asp:Label ID="lblCompTop" runat="server" ForeColor="Red" Text="Label" Visible="True"></asp:Label>
</td>
             </tr>
             <tr bgcolor="#ffffff">
<td colspan = 4>
                        	&nbsp;
                     	</td>
               </tr>   
									  <TR bgColor="#ffffff">
											<TD  align=right style="width: 15%; height: 30px">
                                                <asp:Label ID="UserIDLab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%; height: 30px">
                                                <asp:TextBox ID="UserIDBox" runat="server" CssClass="textborder" Width="90%" MaxLength="20"></asp:TextBox>
                                                <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="*" Width="14px"></asp:Label>
                                                <asp:RequiredFieldValidator ID="RFuserid" runat="server"
                                                    ForeColor="White" ControlToValidate="UserIDBox" Height="6px" Width="1px">.</asp:RequiredFieldValidator></TD>
											<TD  align=right style="width: 15%; height: 30px">
                                                <asp:Label ID="StaffStatusLab" runat="server" Text="Label"></asp:Label></TD>
											<TD  align=left style="width: 36%; height: 30px">
                                                <asp:DropDownList ID="StaffStatusDDL" runat="server" CssClass="textborder" Width="90%">
                                                </asp:DropDownList></TD>
										</TR>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%; height: 30px">
                                                <asp:Label ID="UserNameLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%; height: 30px">
                                                <asp:TextBox ID="UserNameBox" runat="server" CssClass="textborder" Width="90%" MaxLength="50"></asp:TextBox>
                                                <asp:Label ID="Label2" runat="server" ForeColor="Red" Text="*" Width="14px"></asp:Label>
                                                <asp:RequiredFieldValidator ID="RFusername" runat="server" ErrorMessage=" "
                                                    ForeColor="White" ControlToValidate="UserNameBox" Height="2px" Width="1px">.</asp:RequiredFieldValidator></td>
                            <td align="right" style="width: 15%; height: 30px">
                                                <asp:Label ID="CountryIDLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 36%; height: 30px">
                                                <asp:DropDownList ID="CountryIDDDL" runat="server" CssClass="textborder" AutoPostBack="True" Width="90%">
                                                </asp:DropDownList>
                                <asp:Label ID="Label8" runat="server" ForeColor="Red" Height="8px" Text="*" Width="1px"></asp:Label>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="EmailAdressBox"
                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ForeColor="White">*</asp:RegularExpressionValidator></td>
                        </tr>
										<TR bgColor="#ffffff">
											<TD  align=right style="width: 15%; height: 30px;">
                                                <asp:Label ID="AccessProGroupLab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%; height: 30px;">
                                                <asp:DropDownList ID="AccessProGroupDDL" runat="server" CssClass="textborder" Width="90%">
                                                </asp:DropDownList>
                                                <asp:Label ID="Label3" runat="server" ForeColor="Red" Height="8px" Text="*" Width="1px"></asp:Label></TD>
											<TD align=right style="width: 15%; height: 30px;">
                                                <asp:Label ID="AlterNameLab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 36%; height: 30px;">
                                                <asp:TextBox ID="AlterNameBox" runat="server" CssClass="textborder" Width="90%" MaxLength="50"></asp:TextBox>
                                                &nbsp;&nbsp;
                                            </TD>
										</TR>
										<TR bgColor="#ffffff">
											<TD align=right style="width: 15%; height: 30px">
                                                <asp:Label ID="RankLab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%; height: 30px">
                                                <asp:DropDownList ID="RankDDL" runat="server" CssClass="textborder" Width="90%">
                                                </asp:DropDownList></TD>
											<TD align=right style="width: 15%; height: 30px">
                                                <asp:Label ID="FailedLogonLab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 36%; height: 30px">
                                                <asp:TextBox ID="FailedLogonBox" runat="server" CssClass="textborder" ReadOnly="True" Width="90%"></asp:TextBox></TD>
										</TR>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%; height: 30px">
                                <asp:Label ID="ChangePassWdLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%; height: 30px">
                                <asp:DropDownList ID="ChangePassWdDDL" runat="server" CssClass="textborder" Width="90%">
                                </asp:DropDownList></td>
                            <td align="right" style="width: 15%; height: 30px">
                                <asp:Label ID="CompanIDLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 36%; height: 30px">
                                <asp:DropDownList ID="CompanIDDDL" runat="server" CssClass="textborder" AutoPostBack="True" Width="90%">
                                </asp:DropDownList>
                                <asp:Label ID="Label9" runat="server" ForeColor="Red" Height="8px" Text="*" Width="1px"></asp:Label>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="EmailAdressBox"
                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ForeColor="White">*</asp:RegularExpressionValidator></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%; height: 30px">
                                <asp:Label ID="DepartmentLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%; height: 30px">
                                <asp:DropDownList ID="DepartmentDDL" runat="server" CssClass="textborder" Width="90%">
                                </asp:DropDownList></td>
                            <td align="right" style="width: 15%; height: 30px">
                                <asp:Label ID="PermanentStaffLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 36%; height: 30px">
                                <asp:DropDownList ID="PermanentStaffDDL" runat="server" CssClass="textborder" Width="90%">
                                </asp:DropDownList></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%; height: 30px">
                                <asp:Label ID="SvcIDLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%; height: 30px">
                                <asp:DropDownList ID="SvcIDDDL" runat="server" CssClass="textborder" Width="90%">
                                </asp:DropDownList>
                                <asp:Label ID="Label6" runat="server" ForeColor="Red" Height="8px" Text="*" Width="1px"></asp:Label>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="EmailAdressBox"
                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ForeColor="White">*</asp:RegularExpressionValidator></td>
                            <td align="right" style="width: 15%; height: 30px">
                                <asp:Label ID="PassWdLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 36%; height: 30px">
                                <asp:TextBox ID="PassWdBox" runat="server" CssClass="textborder" TextMode="Password" MaxLength="12" Width="90%"></asp:TextBox>
                                <asp:Label ID="Label4" runat="server" ForeColor="Red" Height="8px" Text="*" Width="1px"></asp:Label></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%; height: 30px">
                                <asp:Label ID="EmailAdressLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%; height: 30px">
                                <asp:TextBox ID="EmailAdressBox" runat="server" CssClass="textborder" Width="90%" MaxLength="30"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="REVEmail" runat="server" ControlToValidate="EmailAdressBox"
                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ForeColor="White">*</asp:RegularExpressionValidator></td>
                            <td align="right" style="width: 15%; height: 30px">
                                <asp:Label ID="ConfirPasWdLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 36%; height: 30px">
                                <asp:TextBox ID="ConfirPasWdBox" runat="server" CssClass="textborder" TextMode="Password" MaxLength="12" Width="90%"></asp:TextBox>
                                <asp:Label ID="Label5" runat="server" ForeColor="Red" Height="8px" Text="*" Width="1px"></asp:Label></td>
                        </tr>
										<TR bgColor="#ffffff">
											<TD align=right style="width: 15%; height: 30px;">
                                                <asp:Label ID="creatby" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%; height: 30px;">
                                                <asp:TextBox ID="creatbybox" runat="server" CssClass="textborder" ReadOnly=true Width="90%"></asp:TextBox></TD>
											<TD align=right style="width: 15%; height: 30px;">
                                                <asp:Label ID="creatdate" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 36%; height: 30px;">
                                                <asp:TextBox ID="creatdtbox" runat="server" CssClass="textborder" ReadOnly=true Width="90%"></asp:TextBox></TD>
										</TR>
										<TR bgColor="#ffffff">
											<TD align=right style="width: 15%; height: 34px;">
                                                <asp:Label ID="modfyby" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%; height: 34px;">
                                                <asp:TextBox ID="modfybybox" runat="server" CssClass="textborder" ReadOnly=true Width="90%"></asp:TextBox></TD>
											<TD align=right style="width: 15%; height: 34px;">
                                                <asp:Label ID="mdfydate" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 36%; height: 34px;">
                                                <asp:TextBox ID="mdfydtbox" runat="server" CssClass="textborder" ReadOnly=true Width="90%"></asp:TextBox></TD>
										</TR>
								</TABLE>
						</TD>
					</TR>
					<TR>
						<TD style="height: 15px; width: 100%;">
						<font color=red><asp:Label ID="lblCompBottom" runat="server" Text="Label" Visible="true"></asp:Label></font>
							<TABLE id="Table1" cellSpacing="1" cellPadding="1"  border="0">
									<TR>
										<TD style="WIDTH: 20%" align=center>
                                            &nbsp;<asp:LinkButton ID="LinkButton1" runat="server">save</asp:LinkButton></TD>
										<td align=center>
                                            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/PresentationLayer/masterrecord/user.aspx">cancel</asp:HyperLink></td>
									<td align=center>
<asp:HyperLink ID="AddLink" runat="server" >[Add]</asp:HyperLink>
</td></TR>
							</TABLE>
						</TD>
					</TR>
			</TABLE>
				</TD>
					</TR>
			</TABLE>
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                        ShowSummary="False" />
                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="ConfirPasWdBox"
                        ControlToValidate="PassWdBox" ForeColor="White">.</asp:CompareValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="PassWdBox"
                        ForeColor="White" Height="1px" ValidationExpression="\w{6,14}" Width="1px">.</asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator ID="RFVCPSW" runat="server" ControlToValidate="ConfirPasWdBox" ForeColor="White">*</asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ID="RFVPSW" runat="server" ControlToValidate="PassWdBox" ForeColor="White">*</asp:RequiredFieldValidator>&nbsp;
                    <cc1:messagebox id="MessageBox1" runat="server"></cc1:messagebox>
       <asp:RequiredFieldValidator ID="RFVCTRY" runat="server" ControlToValidate="CountryIDDDL"
           ForeColor="White">.</asp:RequiredFieldValidator>
       <asp:RequiredFieldValidator ID="RFVCOMP" runat="server" ControlToValidate="CompanIDDDL"
           ForeColor="White">.</asp:RequiredFieldValidator>
       <asp:RequiredFieldValidator ID="RFVSVC" runat="server" ControlToValidate="SvcIDDDL"
           ForeColor="White">.</asp:RequiredFieldValidator>
       <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="AccessProGroupDDL"
           ForeColor="White">.</asp:RequiredFieldValidator>&nbsp;
                        </form>
</body>
</html>
