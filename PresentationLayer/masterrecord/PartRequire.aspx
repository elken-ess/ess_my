<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PartRequire.aspx.vb" Inherits="PresentationLayer_masterrecord_PartRequire" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
<STYLE type="text/css">A:link { COLOR: #336666; TEXT-DECORATION: none }
    <title>PartRequire Page</title>
    
    </STYLE>
	<LINK href="../css/style.css" type="text/css" rel="stylesheet">
</head>
<body>
    <form id="PartRequire" runat="server">
    <div>
        <div>
            <table id="TABLE2" border="0" width = 100%>
                <tr>
                    <td style="width: 100%; height: 13px">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td background="../graph/title_bg.gif" style="height: 24px" width="1%">
                                    <img height="24" src="../graph/title1.gif" width="5" /></td>
                                <td background="../graph/title_bg.gif" class="style2" style="height: 24px; width: 98%;">
                                    <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="right" background="../graph/title_bg.gif" style="height: 24px; width: 1%;">
                                    <img height="24" src="../graph/title_2.gif" width="5" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="width: 100%; height: 12px">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td background="../graph/title_bg.gif" class="style2" style="height: 1px" width="98%">
                                    <font color="red">
                                        <asp:Label ID="addinfo" runat="server" Text="Label" Visible="false"></asp:Label></font></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <asp:Label ID="PartIDlab" runat="server" Text="Label"></asp:Label>
            <asp:TextBox ID="PartIDBox" runat="server" CssClass="textborder" Width="15%" MaxLength="10"></asp:TextBox>
            <asp:Label ID="Partname" runat="server" Text="Label"></asp:Label>
            <asp:TextBox ID="PartnmBox" runat="server" CssClass="textborder" Width="15%" MaxLength="50"></asp:TextBox>
            <asp:Label ID="statusLab" runat="server" Text="Label"></asp:Label>
            <asp:DropDownList ID="ctryStatDrop" runat="server" CssClass="textborder" Width="104px">
            </asp:DropDownList>
            <asp:LinkButton ID="LBsearch" runat="server">search</asp:LinkButton>
            <asp:LinkButton ID="LBadd" runat="server" PostBackUrl="~/PresentationLayer/masterrecord/AddPartRequire.aspx"></asp:LinkButton>
            <hr />
            &nbsp; &nbsp; &nbsp;&nbsp;</div>
        <asp:GridView ID="partView" runat="server" AllowPaging="True" AllowSorting="True"
            Width="74%" style="width: 100%" Font-Size="Smaller">
        </asp:GridView>
        <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="Label" Visible="False"></asp:Label></div>
    </form>
</body>
</html>
