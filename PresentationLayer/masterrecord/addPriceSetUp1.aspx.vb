Imports SQLDataAccess
Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data


Partial Class PresentationLayer_masterrecord_addPriceSetUp1

    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try
        Try
            Dim str As String = Request.QueryString("pricesetupid")

        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try


        '''access control'''''''''''''''''''''''''''''''''''''''''''''''
        Dim accessgroup As String = Session("accessgroup").ToString
        Dim purviewArray As Array = New Boolean() {False, False, False, False, False}
        purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "14")
        Me.saveButton.Enabled = purviewArray(0)
        If purviewArray(0) = False Then
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return

        End If
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        If Not Page.IsPostBack Then


            Dim pricesetid As Integer
            pricesetid = Request.QueryString("pricesetupid")
            Me.priceidBox.Text = Request.Params("priceid").ToString().ToUpper()
            Me.partidBox.Text = Request.Params("parteid").ToString().ToUpper()
            Me.countryidBox.Text = Request.Params("ctryid").ToString().ToUpper()



            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            amountlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0005")
            effectivelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0011")
            createby.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATBY")
            creatdate.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
            modifyby.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFBY")
            mdifydate.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFDATE")
            statusLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Status")
            saveButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Save")
            cancel.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Cancel")
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0017")
            priceidlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0001")
            partidlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0002")
            countryidLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRCE-0003")
            Me.lblCompTop.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            Me.lblCompBottom.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            Dim objXmlTr1 As New clsXml
            objXmlTr1.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            amountva.ErrorMessage = objXmlTr1.GetLabelName("StatusMessage", "AMOUNTERROR")
            EFFDATEVA.ErrorMessage = objXmlTr1.GetLabelName("StatusMessage", "BB-HintMsg-Effecdate")
            amountvava.ErrorMessage = objXmlTr1.GetLabelName("StatusMessage", "AMOUNT")
            'COUNTRYIDVA.ErrorMessage = objXmlTr1.GetLabelName("StatusMessage", "CountryID")
            'partidVA.ErrorMessage = objXmlTr1.GetLabelName("StatusMessage", "PARTID")

            Dim userIDNamestr As String = Session("userID").ToString().ToUpper + "-" + Session("username").ToString.ToUpper


            creatbybox.Text = userIDNamestr
            modfybybox.Text = userIDNamestr
            mdfydtbox.Text = Date.Now
            creatdtbox.Text = Date.Now()

            'Me.creatbybox.Text = Session("username").ToString().ToUpper()
            'Me.modfybybox.Text = Session("username").ToString().ToUpper()

            pricesetupidbox.Text = pricesetid
            Dim cntryStat As New clsrlconfirminf()
            Dim statParam As ArrayList = New ArrayList
            statParam = cntryStat.searchconfirminf("STATUS")
            Dim count As Integer
            Dim statid As String
            Dim statnm As String
            For count = 0 To statParam.Count - 1
                statid = statParam.Item(count)
                statnm = statParam.Item(count + 1)
                count = count + 1
                If (statid.Equals("ACTIVE") Or statid.Equals("OBSOLETE")) Then
                    Me.statusDrop.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                End If
            Next
            statusDrop.Items(0).Selected = True
            disp()

        End If
        HypCal.NavigateUrl = "javascript:DoCal(document.PriceSetUp1form.effectivebox);"

    End Sub

#Region " id bond"
    Public Function databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        Dim row As DataRow
        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            NewItem.Text = row("id") & "-" & row("name")
            NewItem.Value = row("id")
            dropdown.Items.Add(NewItem)
        Next

    End Function
#End Region


    Protected Sub saveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveButton.Click
        'Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        'System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim msginfo As String = objXm.GetLabelName("StatusMessage", "SAVEORNOT")
        Dim msgtitle As String = objXm.GetLabelName("StatusMessage", "SAVETITLE")
        MessageBox1.Confirm(msginfo)
        disp()
        effectivebox.Text = Request.Form("effectivebox")

    End Sub

    Protected Sub cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cancel.Click
        Dim symbl As Integer
        symbl = Request.QueryString("symbl")
        If symbl = 0 Then
            Dim url As String = "~/PresentationLayer/masterrecord/addPriceSetUp.aspx?"
            url &= "pricesetupid=" & pricesetupidbox.Text & "&"
            url &= "flag=" & 1

            Response.Redirect(url)
        ElseIf symbl = 1 Then
            Dim url As String = "~/PresentationLayer/masterrecord/modifyPriceSetUp.aspx?"
            url &= "pricesetupid1=" & pricesetupidbox.Text & "&"
            url &= "PriceSetUpID=" & "" & "&"
            url &= "statusid=" & "&"
            url &= "flag=" & 1
            Response.Redirect(url)
        End If



    End Sub

    Protected Sub MessageBox1_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox1.GetMessageBoxResponse
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then

            Dim pricesidStat As New clsCommonClass()

            Dim PriceSetUpEntity As New ClsPriceSetup1()
            If (Trim(pricesetupidbox.Text) <> "") Then
                PriceSetUpEntity.PriceSetUpID = pricesetupidbox.Text
            End If
            If (statusDrop.SelectedItem.Value.ToString() <> "") Then
                PriceSetUpEntity.PriceSetUpStatus = statusDrop.SelectedItem.Value.ToString()
            End If
            If (Trim(amountbox.Text) <> "") Then
                PriceSetUpEntity.Amount = amountbox.Text
            End If
            If (Trim(Request.Form("effectivebox")) <> "") Then
                Dim temparr As Array = Request.Form("effectivebox").Split("/") 'effectivebox.Text.Split("/")
                PriceSetUpEntity.EffectiveDate = temparr(2) + "-" + temparr(1) + "-" + temparr(0)

            End If
           
            'If (Trim(creatbybox.Text) <> "") Then
            '    PriceSetUpEntity.CreatedBy = creatbybox.Text.ToUpper()
            'End If

            If (Trim(creatdtbox.Text) <> "") Then
                PriceSetUpEntity.CreatedDate = pricesidStat.DatetoDatabase(creatdtbox.Text)
            End If
            'If (Trim(modfybybox.Text) <> "") Then
            '    PriceSetUpEntity.ModifiedBy = modfybybox.Text.ToUpper()
            'End If
            If (Trim(mdfydtbox.Text) <> "") Then
                PriceSetUpEntity.ModifiedDate = pricesidStat.DatetoDatabase(mdfydtbox.Text)
            End If
            If (Trim(Me.creatbybox.Text) <> "") Then
                PriceSetUpEntity.CreatedBy = Session("userID").ToString.ToUpper()
            End If
            If (Trim(Me.modfybybox.Text) <> "") Then
                PriceSetUpEntity.ModifiedBy = Session("userID").ToString.ToUpper()
            End If
            'If (Trim(txtOri.Text) <> "") Then
            '    PriceSetUpEntity.AmountOri = txtOri.Text
            'End If
            'If (Trim(txtGstAmt.Text) <> "") Then
            '    PriceSetUpEntity.AmountGst = txtGstAmt.Text
            'End If
            'If (Trim(txtGstExc.Text) <> "") Then
            '    PriceSetUpEntity.AmountExc = txtGstExc.Text
            'End If



            Dim dupCount As Integer = PriceSetUpEntity.GetDuplicatedAmount()
            If dupCount.Equals(-1) Then
                Dim objXm As New clsXml
                objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                errlab.Text = objXm.GetLabelName("StatusMessage", "DUPAMOUNT")
                errlab.Visible = True
            ElseIf dupCount.Equals(0) Then
                Dim insPRIDCnt As Integer = PriceSetUpEntity.Insert1()
                If insPRIDCnt = 0 Then
                    Dim objXm As New clsXml
                    objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    errlab.Text = objXm.GetLabelName("StatusMessage", "SuccessfullySaved")
                    errlab.Visible = True
                Else

                    Response.Redirect("~/PresentationLayer/Error.aspx")

                End If
            End If

            Dim objXmlTr2 As New clsXml
            objXmlTr2.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            Dim PriceSetUpEntity1 As New clsPriceSetUp()
            PriceSetUpEntity1.PriceSetUpID = pricesetupidbox.Text
            PriceSetUpEntity1.PriceSetUpStatus = statusDrop.SelectedItem.Value.ToString()
            Dim PriceSetUpall As DataSet = PriceSetUpEntity1.GetPriceSetUp1()
            PriceSetUpView.DataSource = PriceSetUpall
            Session(" PriceSetUpView") = PriceSetUpall
            PriceSetUpView.AllowPaging = True
            'PriceSetUpView.AllowSorting = True
            PriceSetUpView.DataBind()
            If PriceSetUpall.Tables(0).Rows.Count <> 0 Then
                PriceSetUpView.HeaderRow.Cells(0).Text = objXmlTr2.GetLabelName("EngLabelMsg", "BB-MASPRCE-0014")
                PriceSetUpView.HeaderRow.Cells(0).Font.Size = 8
                PriceSetUpView.HeaderRow.Cells(1).Text = objXmlTr2.GetLabelName("EngLabelMsg", "BB-MASPRCE-0006")
                PriceSetUpView.HeaderRow.Cells(1).Font.Size = 8
                PriceSetUpView.HeaderRow.Cells(2).Text = objXmlTr2.GetLabelName("EngLabelMsg", "BB-MASPRCE-0005")
                PriceSetUpView.HeaderRow.Cells(2).Font.Size = 8
                PriceSetUpView.HeaderRow.Cells(3).Text = objXmlTr2.GetLabelName("EngLabelMsg", "BB-Status")
                PriceSetUpView.HeaderRow.Cells(3).Font.Size = 8
                PriceSetUpView.HeaderRow.Cells(4).Text = objXmlTr2.GetLabelName("EngLabelMsg", "BB-CREATDATE")
                PriceSetUpView.HeaderRow.Cells(4).Font.Size = 8
               


            End If
            effectivebox.Text = Request.Form("effectivebox")

        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
            effectivebox.Text = Request.Form("effectivebox")
        End If
            disp()

    End Sub

    Protected Sub PriceSetUpView_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles PriceSetUpView.PageIndexChanging

        PriceSetUpView.PageIndex = e.NewPageIndex
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        disp()
        effectivebox.Text = Request.Form("effectivebox")
    End Sub
    Protected Sub disp()
        Dim objXmlTr2 As New clsXml
        objXmlTr2.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        Dim PriceSetUpEntity1 As New clsPriceSetUp()
        PriceSetUpEntity1.PriceSetUpID = pricesetupidbox.Text
        PriceSetUpEntity1.PriceSetUpStatus = statusDrop.SelectedItem.Value.ToString()
        Dim PriceSetUpall As DataSet = PriceSetUpEntity1.GetPriceSetUp1()
        PriceSetUpView.DataSource = PriceSetUpall
        Session(" PriceSetUpView") = PriceSetUpall
        PriceSetUpView.AllowPaging = True
        'PriceSetUpView.AllowSorting = True
        PriceSetUpView.DataBind()
        If PriceSetUpall.Tables(0).Rows.Count <> 0 Then
            PriceSetUpView.HeaderRow.Cells(0).Text = objXmlTr2.GetLabelName("EngLabelMsg", "BB-MASPRCE-0014")
            PriceSetUpView.HeaderRow.Cells(0).Font.Size = 8
            PriceSetUpView.HeaderRow.Cells(1).Text = objXmlTr2.GetLabelName("EngLabelMsg", "BB-MASPRCE-0006")
            PriceSetUpView.HeaderRow.Cells(1).Font.Size = 8
            PriceSetUpView.HeaderRow.Cells(2).Text = objXmlTr2.GetLabelName("EngLabelMsg", "BB-MASPRCE-0005")
            PriceSetUpView.HeaderRow.Cells(2).Font.Size = 8
            PriceSetUpView.HeaderRow.Cells(3).Text = objXmlTr2.GetLabelName("EngLabelMsg", "BB-Status")
            PriceSetUpView.HeaderRow.Cells(3).Font.Size = 8
            PriceSetUpView.HeaderRow.Cells(4).Text = objXmlTr2.GetLabelName("EngLabelMsg", "BB-CREATDATE")
            PriceSetUpView.HeaderRow.Cells(4).Font.Size = 8
        End If

    End Sub

    Protected Sub Calendar_SelectedDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendar.SelectedDateChanged
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Calendar.Visible = True
        Calendar.Attributes.Add("style", " POSITION: absolute")
        effectivebox.Text = Calendar.SelectedDate.Date.ToString().Substring(0, 10)
        'disp()
    End Sub

    Protected Sub Calendar_CalendarVisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendar.CalendarVisibleChanged
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Calendar.Visible = True
        Calendar.Attributes.Add("style", " POSITION: absolute")
        effectivebox.Text = Calendar.SelectedDate.Date.ToString().Substring(0, 10)
        'disp()
    End Sub
End Class

