﻿Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Partial Class PresentationLayer_masterrecord_modifycountry
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try

            '''''''''''''''''''''''''''''''''''''''''''''''''''''
            Try
                Dim str As String = Request.Params("ctryid").ToString
                str = Request.Params("ctrystat").ToString()
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Dim mdfyid As String = Request.Params("ctryid").ToString()
            Dim mdfystat As String = Request.Params("ctrystat").ToString()
            

            Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            ctryidlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCTRY-0001")
            ctrynameLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCTRY-0002")
            ctryalterLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCTRY-0003")
            statusLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCTRY-0004")
            ctrytelpf.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCTRY-0005")
            curridLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCTRY-0006")
            currnmLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCTRY-0007")
            creatby.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATBY")
            creatdate.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
            modfyby.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFBY")
            mdfydate.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFDATE")
            saveButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Save")
            cancelLink.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Cancel")
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "MODCTRYTL")
            Label1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            Label2.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            ctryiderr.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "CRYIDER")
            ctrynmerr.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "CRYNMER")
            ctryalterr.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "CRYALTER")
            'Dim strPanm As String = objXmlTr.GetLabelID("StatusMessage", mdfystat)
            Dim strPanm As String = mdfystat
            If mdfyid.Trim() <> "" Then
                Dim countryEntity As New clsCountry()
                countryEntity.CountryID = mdfyid
                countryEntity.CountryStatus = strPanm
                Dim retnArray As ArrayList = countryEntity.GetCountryDetailsByCountryID()
                ctryidbox.Text = retnArray(0).ToString()
                ctryidbox.ReadOnly = True
                ctrynmbox.Text = retnArray(1).ToString()
                ctryaltbox.Text = retnArray(2).ToString()
                'create the dropdownlist
                Dim cntryStat As New clsUtil()
                Dim statParam As ArrayList = New ArrayList
                statParam = cntryStat.searchconfirminf("STATUS")
                Dim count As Integer
                Dim statid As String
                Dim statnm As String
                For count = 0 To statParam.Count - 1
                    statid = statParam.Item(count)
                    statnm = statParam.Item(count + 1)
                    If (statid.Equals("ACTIVE") Or statid.Equals("DELETE") Or statid.Equals("OBSOLETE")) Then
                        ctrystat.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                    End If
                    If statid.Equals(retnArray(3).ToString()) Then
                        ctrystat.Items(count / 2).Selected = True
                    End If
                    count = count + 1
                Next


                Dim accessgroup As String = Session("accessgroup").ToString
                Dim purviewArray As Array = New Boolean() {False, False, False, False, False}
                purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "08")
                If purviewArray(2) = False Then
                    Response.Redirect("~/PresentationLayer/logon.aspx")
                End If
                Me.saveButton.Enabled = purviewArray(1)
                If Convert.ToString(purviewArray(4)).ToUpper = "FALSE" Then
                    Me.ctrystat.Items.Remove(Me.ctrystat.Items.FindByValue("DELETE"))
                End If


                ctrycuridbox.Text = retnArray(4).ToString()
                ctrycurnmbox.Text = retnArray(5).ToString()
                ctrytelbox.Text = retnArray(6).ToString()
                'creatbybox.Text = retnArray(7).ToString()
                'creatbybox.ReadOnly = True

                Dim Rcreatby As String = retnArray(7).ToString()
                If Rcreatby <> "ADMIN" Then
                    Dim Rcreat As New clsCommonClass
                    Rcreat.userid() = Rcreatby
                    Dim Rcreatds As New DataSet()
                    Rcreatds = Rcreat.Getuseridname("BB_MASUSER_SelByIDName")

                    Me.creatbybox.Text = Rcreatds.Tables(0).Rows(0).Item(0)
                Else
                    Me.creatbybox.Text = "ADMIN-ADMIN"
                End If

                Dim cntryDate As New clsUtil()
                Dim strs As String = retnArray(8).ToString()
                creatdtbox.Text = retnArray(8).ToString()
                creatdtbox.ReadOnly = True

                Dim userIDNamestr As String = Session("userID").ToString().ToUpper + "-" + Session("username").ToString.ToUpper
                modfybybox.Text = userIDNamestr
                'modfybybox.Text = Session("username").ToString()
                modfybybox.ReadOnly = True
                mdfydtbox.Text = retnArray(10).ToString()
                mdfydtbox.ReadOnly = True
            End If
            Me.ctrynmbox.Focus()
        End If
    End Sub

    Protected Sub saveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveButton.Click
        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim msginfo As String = objXm.GetLabelName("StatusMessage", "SAVEORNOT")
        Dim msgtitle As String = objXm.GetLabelName("StatusMessage", "SAVETITLE")
        MessageBox1.Confirm(msginfo)


       

    End Sub

    Protected Sub MessageBox1_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox1.GetMessageBoxResponse
        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
            Dim countryEntity As New clsCountry()
            If (Trim(ctryidbox.Text) <> "") Then
                countryEntity.CountryID = ctryidbox.Text.ToUpper()
            End If
            If (Trim(ctrynmbox.Text) <> "") Then
                countryEntity.CountryName = ctrynmbox.Text.ToUpper()
            End If
            If (Trim(ctryaltbox.Text) <> "") Then
                countryEntity.CountryAlternateName = ctryaltbox.Text.ToUpper()
            End If
            If (ctrystat.SelectedItem.Value.ToString() <> "") Then
                countryEntity.CountryStatus = ctrystat.SelectedItem.Value.ToString()
            End If
            If (Trim(ctrytelbox.Text) <> "") Then
                countryEntity.TelPref = ctrytelbox.Text.ToUpper()
            End If
            If (Trim(ctrycuridbox.Text) <> "") Then
                countryEntity.CountryCurrencyid = ctrycuridbox.Text.ToUpper()
            End If
            If (Trim(ctrycurnmbox.Text) <> "") Then
                countryEntity.CountryCurrencyname = ctrycurnmbox.Text.ToUpper()
            End If
            'If (Trim(creatbybox.Text) <> "") Then
            '    countryEntity.CreateBy = creatbybox.Text.ToUpper()
            'End If
            Dim cntryDate As New clsUtil()
            'If (Trim(creatdtbox.Text) <> "") Then
            '    countryEntity.CreateDate = cntryDate.DatetoDatabase(creatdtbox.Text)
            'End If
            If (Trim(modfybybox.Text) <> "") Then
                countryEntity.ModifyBy = Session("userID").ToString().ToUpper
            End If
            If (Trim(mdfydtbox.Text) <> "") Then
                countryEntity.ModifyDate = cntryDate.DatetoDatabase(mdfydtbox.Text)
            End If

            countryEntity.IPaddr = Request.UserHostAddress.ToString()
            countryEntity.servid = Session("login_svcID")

            '''''''''delete'''''''''''''''''''''''''
            If countryEntity.CountryStatus = "DELETE" Then
                Dim delstateid As Integer = countryEntity.GetstateByCountryID("BB_MASCTRY_DELID")
                Dim delMPRCid As Integer = countryEntity.GetstateByCountryID("BB_MASMPRC_DELID")
                Dim delMTX1id As Integer = countryEntity.GetstateByCountryID("BB_MASMTX1_DELID")
                '  Dim delMPP1id As Integer = countryEntity.GetstateByCountryID("BB_MASMPP1_DELID")
                Dim delMCATid As Integer = countryEntity.GetstateByCountryID("BB_MASMCAT_DELID")
                ' Dim delMPARid As Integer = countryEntity.GetstateByCountryID("BB_MASMPAR_DELID")
                Dim delMMODid As Integer = countryEntity.GetstateByCountryID("BB_MASMMOD_DELID")
                ' Dim delMUSRid As Integer = countryEntity.GetstateByCountryID("BB_MASMUSR_DELID")

                If delstateid <> 0 Or delMPRCid <> 0 Or delMTX1id <> 0 Or delMCATid <> 0 Or delMMODid <> 0 Then
                    Dim objXm As New clsXml
                    objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    errlab.Text = objXm.GetLabelName("StatusMessage", "DELCTRY")
                    errlab.Visible = True
                    Return
                End If
            End If
            '''''''''delete'''''''''''''''''''''''''
            Dim selvalue As Integer = countryEntity.GetCountryDetailsByCountryNM()
            If selvalue <> 0 Then
                'Response.Redirect("~/PresentationLayer/Error.aspx")
                Dim objXm As New clsXml
                objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                errlab.Text = objXm.GetLabelName("StatusMessage", "DUPCTRYNM")
                errlab.Visible = True
            ElseIf selvalue.Equals(0) Then
                Dim updCtryCnt As Integer = countryEntity.Update()
                Dim objXmlTr As New clsXml
                objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                If updCtryCnt = 0 Then
                    ' Response.Redirect("country.aspx")
                    Dim objXm As New clsXml
                    objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    errlab.Text = objXm.GetLabelName("StatusMessage", "BB-HintMsg-Suc")
                    errlab.Visible = True
                Else
                    Response.Redirect("~/PresentationLayer/Error.aspx")
                End If
            End If
        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
        End If
    End Sub
End Class
