<%@ Page Language="VB" AutoEventWireup="false" EnableEventValidation ="false"  CodeFile="addstate.aspx.vb" Inherits="PresentationLayer_masterrecord_addstate" %>

<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <LINK href="../css/style.css" type="text/css" rel="stylesheet">
</head>
<body>
    <form id="form1" runat="server">
   
    <TABLE border="0" id=statetab style="width: 100%">
    <tr>
          <tr>
                <td style="width: 95%">
                    <table cellspacing="0" cellpadding="0" width="100%" border="0" style="width: 100%">
                        <tr>
                            <td width="1%" background="../graph/title_bg.gif">
                                <img height="24" src="../graph/title1.gif" width="5"></td>
                            <td class="style2" background="../graph/title_bg.gif" style="width: 100%">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="right" background="../graph/title_bg.gif" style="width: 4%">
                                <img height="24" src="../graph/title_2.gif" width="5"></td>
                        </tr>
                    </table>
            </tr>
             <tr>
           <td style="width: 100%">
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>					
							<TD class="style2" background="../graph/title_bg.gif" style="width: 110%">
                                <font color=red style="width: 100%"><asp:Label ID="errlab" runat="server" Text="Label" Visible=false></asp:Label></font>
                              </TD>
						</TR>
			 </TABLE>
           </tr>
			<TR>
				<TD>
					<TABLE id="country" cellSpacing="1" cellPadding="2" bgColor="#b7e6e6" border="0"  width="95%" style="width: 100%;">
					 <tr bgcolor="#ffffff">
<td colspan = 4>
                            <asp:Label ID="lblCompTop" runat="server" ForeColor="Red" Text="Label" Visible="True"></asp:Label>
</td>
             </tr>
             <tr bgcolor="#ffffff">
<td colspan = 4>
                        	&nbsp;
                     	</td>
               </tr>   
									  <TR bgColor="#ffffff">
																				 
											 <TD  align=right style="height: 20px; width: 15%;">
                                                &nbsp;<asp:Label ID="ctryidlab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left width="85%" colspan=3 style="height: 20px; width: 100%;">
                                                <asp:DropDownList ID="ctr_id" runat="server" CssClass="textborder" AppendDataBoundItems="True" Width="152px" AutoPostBack="True" style="width: 36%">
                                                </asp:DropDownList>
                                                <font color=red style="height: 21px"><strong>*</strong></font>
                                                <asp:RequiredFieldValidator ID="ctryiderr" runat="server" ControlToValidate="ctr_id"
                                                    ForeColor="White" Height="1px" Width="0px">*</asp:RequiredFieldValidator></TD>
										</TR>
										<TR bgColor="#ffffff">
											<TD  align=right style="height: 1px; width: 15%;">
                                                <asp:Label ID="stateidlab" runat="server" Text="Label" style="height: 100%"></asp:Label></TD>
                                                 
											<TD align=left   style="height: 1px; width: 35%;">
                                                <asp:TextBox ID="stateidbox" runat="server" CssClass="textborder" MaxLength="10" Width="65%" style="width: 90%"></asp:TextBox>
                                                <font color=red style="height: 21px"><strong>*</strong></font>
                                                </TD>
										<TD  align=right style="height: 1px; width: 15%;">
                                                <asp:Label ID="statenmlab" runat="server" Text="Label" Height="87%" Width="16px"></asp:Label></TD>
											<TD align=left style="height: 1px; width: 35%;">
                                                <asp:TextBox ID="statenmbox" runat="server" CssClass="textborder" Width="65%" style="width: 90%" MaxLength="50" ></asp:TextBox>
                                                <font color=red style="width: 28%; height: 20px"><strong>*</strong></font>
                                                </TD>
										</TR>
										<TR bgColor="#ffffff">
											<TD  align=right style="height: 30px; width: 15%;" >
                                                <asp:Label ID="alterlab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left width="35%"colspan=3 style="height: 30px">
                                                <asp:TextBox ID="statealtbox" runat="server" CssClass="textborder" Width="144px" MaxLength="50" style="width: 36%"></asp:TextBox><font color=red></font></TD>
										
											 
										</TR>
										<TR bgColor="#ffffff">
											<TD align=right style="width: 15%; height: 20px">
                                                <asp:Label ID="taxidLab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%; height: 20px;"><asp:DropDownList ID="taxid" runat="server" CssClass="textborder" Width="65%" style="width: 90%">
                                            </asp:DropDownList></TD>
											
											<TD align=right style="width: 15%; height: 20px;">
                                                <asp:Label ID="statusLab" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%; height: 20px">
                                                <asp:DropDownList ID="ctrystat" runat="server" CssClass="textborder" Width="65%" style="width: 90%">
                                                </asp:DropDownList></TD>
										</TR>
										<TR bgColor="#ffffff">
											<TD align=right style="width: 15%">
                                                <asp:Label ID="creatby" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%">
                                                <asp:TextBox ID="creatbybox" runat="server" CssClass="textborder" ReadOnly=true Width="65%" style="width: 90%"></asp:TextBox></TD>
											<TD align=right style="width: 15%">
                                                <asp:Label ID="creatdate" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%">
                                                <asp:TextBox ID="creatdtbox" runat="server" CssClass="textborder" ReadOnly=true style="width: 90%"></asp:TextBox></TD>
										</TR>
										<TR bgColor="#ffffff">
											<TD align=right style="width: 15%">
                                                <asp:Label ID="modfyby" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%">
                                                <asp:TextBox ID="modfybybox" runat="server" CssClass="textborder" ReadOnly=true Width="65%" style="width: 90%"></asp:TextBox></TD>
											<TD align=right style="width: 15%">
                                                <asp:Label ID="mdfydate" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%">
                                                <asp:TextBox ID="mdfydtbox" runat="server" CssClass="textborder" ReadOnly=true style="width: 90%" Width="65%"></asp:TextBox></TD>
										</TR>
										 
								</TABLE>
						</TD>
					</TR>
					<TR>
						<TD style="width: 100%;">
						<font color=red><asp:Label ID="lblCompBottom" runat="server" Text="Label" Visible="true"></asp:Label></font>
							<TABLE id="Table1" cellSpacing="1" cellPadding="1"  border="0">
									<TR>
										<TD style="WIDTH: 20%; height: 14px;" align=center>
                                            &nbsp;<asp:LinkButton ID="saveButton" runat="server"></asp:LinkButton></TD>
										<td align=center style="height: 14px">
                                            <asp:HyperLink ID="cancelLink" runat="server" NavigateUrl="~/PresentationLayer/masterrecord/state.aspx">[HyperLink1]</asp:HyperLink></td>
									<td align=center>
<asp:HyperLink ID="AddLink" runat="server" >[Add]</asp:HyperLink>
</td></TR>
							</TABLE>
						</TD>
					</TR>
			</TABLE>
        <cc1:messagebox id="MessageBox1" runat="server"></cc1:messagebox>
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
            ShowSummary="False" />
                                                <asp:RequiredFieldValidator ID="stanmerr" runat="server" ControlToValidate="statenmbox" Width="1px" Height="1px" ForeColor="White">*</asp:RequiredFieldValidator>
                                                <asp:RequiredFieldValidator ID="staiderr" runat="server" ControlToValidate="stateidbox" Height="1px" Width="0px" ForeColor="White">*</asp:RequiredFieldValidator>
    </form>
</body>
</html>
