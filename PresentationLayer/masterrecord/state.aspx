<%@ Page Language="VB" AutoEventWireup="false" CodeFile="state.aspx.vb" Inherits="PresentationLayer_masterrecord_state" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>State - Search</title>
     <LINK href="../css/style.css" type="text/css" rel="stylesheet" >
     <STYLE type="text/css">A:link { COLOR: #336666; TEXT-DECORATION: none }
	BODY { FONT-SIZE: 9pt; FONT-FAMILY: "Arial", "Verdana", "Tahoma"; BACKGROUND-COLOR: #ffffff }
	TD { FONT-SIZE: 9pt; FONT-FAMILY: Arial,Verdana,Tahoma }
	</STYLE>
 
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table id="TABLE2" border="0"  width ="100%">
            <tr>
                <td style="width: 100%; height: 13px">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title1.gif" width="5" /></td>
                            <td background="../graph/title_bg.gif" class="style2" width="98%">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="right" background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title_2.gif" width="5" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100%; height: 2px">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" class="style2" style="height: 1px" width="98%">
                                <font color="red">
                                    <asp:Label ID="addinfo" runat="server" Text="Label" Visible="false"></asp:Label></font></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        &nbsp;<asp:Label ID="staID" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="stateIDBox" runat="server" Width=15% CssClass="textborder" MaxLength="10"></asp:TextBox>
        <asp:Label ID="stanm" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="statenmBox" runat="server" Width=15% CssClass="textborder" MaxLength="50"></asp:TextBox>
        <asp:Label ID="staStat" runat="server" Text="Label"></asp:Label>
        <asp:DropDownList ID="ctryStatDrop" runat="server">
        </asp:DropDownList>
        <asp:LinkButton ID="searchButton" runat="server"></asp:LinkButton>
        <asp:LinkButton ID="addButton" runat="server" PostBackUrl="~/PresentationLayer/masterrecord/addstate.aspx"></asp:LinkButton>&nbsp;&nbsp;
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<br />
        <hr />
    
    </div>
        <asp:GridView ID="staView" runat="server" Width="80%" AllowPaging="True" style="width: 100%" Font-Size="Smaller">
        </asp:GridView>
        <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="Label"></asp:Label>
    </form>
     
</body>
</html>
