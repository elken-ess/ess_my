Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Partial Class PresentationLayer_masterrecord_modifycompany
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Response.Redirect("~/PresentationLayer/logon.aspx")
                End If
            Catch ex As Exception
                Response.Redirect("~/PresentationLayer/logon.aspx")
            End Try
         
            
            Dim rank As String = Session("login_rank")
            Dim country As New clsCommonClass

            If rank <> 0 Then
                country.spctr = Session("login_ctryID").ToString().ToUpper

            End If
            country.rank = rank

            Dim countryds As New DataSet

            countryds = country.Getcomidname("BB_MASCTRY_IDNAME")
            If countryds.Tables.Count <> 0 Then
                databonds(countryds, Me.ctrid)
            End If
            Me.ctrid.Items.Insert(0, "")

            '''''''''''''''''''''''''''''''''''''''''''''''''''''
            Try
                Dim str As String = Request.Params("comid").ToString()
                str = Request.Params("comstat").ToString()
            Catch ex As Exception
                Response.Redirect("~/PresentationLayer/logon.aspx")
            End Try
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
           
            Dim mdfycomid As String = Request.Params("comid").ToString()
            'Dim mdfyctrid As String = Request.Params("ctrid").ToString()
            'Dim mdfystaid As String = Request.Params("staid").ToString()
            'Dim mdfyareaid As String = Request.Params("areaid").ToString()
            Dim mdfystat As String = Request.Params("comstat").ToString()



            Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
            'display label message
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            Me.ctridlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCOMP-0001")
            Me.staidlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCOMP-0002")
            Me.areaidlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCOMP-0003")
            statusLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCOMP-0004")
            Me.alterLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCOMP-0005")
            Me.pocodelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCOMP-0006")
            Me.compidlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCOMP-0007")
            Me.compnameLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCOMP-0008")
            Me.faxlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCOMP-0009")
            Me.emailLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCOMP-0010")
            Me.ad1lab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCOMP-0011")
            Me.ad2lab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCOMP-0012")
            Me.tel1lab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCOMP-0013")
            Me.tel2lab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCOMP-0014")
            Label1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            Label2.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")

            saveButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Save")
            cancelLink.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Cancel")
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "MODCOMPTL")


            creatby.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATBY")
            creatdate.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
            modfyby.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFBY")
            mdfydate.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFDATE")
            'creatbybox.Text =  Session("username")
            'creatdtbox.Text = Date.Now()
            'modfybybox.Text =  Session("username")
            'mdfydtbox.Text = Date.Now()

            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            Me.ctriderr.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "CRYIDER")
            Me.staiderr.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "STAIDER")
            Me.areiderr.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "AREIDER")
            Me.comiderr.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "COMIDER")
            Me.comnmerr.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "COMNMER")
            Me.alterr.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "COMALTER")
            Me.ad1err.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "COMADDR1")
            Me.ad2err.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "COMADDR2")
            Me.pocoderr.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "COMPCODE")
            Me.tel1err.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "COMTEL1")
            Me.tel2err.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "COMTEL2")
            Me.faxerr.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "COMFAX")
            Me.emailerr.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "COMEMAIL")
            'Me.eemail_err.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "COMEMAIL11")
            Me.poerr.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "COMPCODE11")
            Me.telerr1.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "COMTEL11")

            Dim strPanm As String = mdfystat

            If mdfycomid.Trim() <> "" Then
                Dim compEntity As New clsCompany()
                'compEntity.StateID = mdfystaid
                'compEntity.CountryID = mdfyctrid
                compEntity.CompID = mdfycomid
                'compEntity.AreaID = mdfyareaid
                compEntity.CompStatus = strPanm

                Dim edtReader As SqlDataReader = compEntity.GetCompDetailByAreaID_StaID_CtrID()
                If edtReader.Read() Then

                    Me.compidbox.Text = edtReader.GetValue(0).ToString()
                    Me.compidbox.ReadOnly = True
                    Me.compnmbox.Text = edtReader.GetValue(1).ToString()
                    Me.altbox.Text = edtReader.GetValue(2).ToString()

                    Dim cntryStat As New clsrlconfirminf()
                    Dim statParam As ArrayList = New ArrayList
                    statParam = cntryStat.searchconfirminf("STATUS")
                    Dim count As Integer
                    Dim statid As String
                    Dim statnm As String
                    For count = 0 To statParam.Count - 1
                        statid = statParam.Item(count)
                        statnm = statParam.Item(count + 1)
                        If (statid.Equals("ACTIVE") Or statid.Equals("DELETE") Or statid.Equals("OBSOLETE")) Then
                            Me.ctrystat.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                        End If

                        If statid.Equals(edtReader.GetValue(3).ToString()) Then
                            ctrystat.Items(count / 2).Selected = True
                        End If
                        count = count + 1
                    Next

                    Dim accessgroup As String = Session("accessgroup").ToString
                    Dim purviewArray As Array = New Boolean() {False, False, False, False, False}
                    purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "19")
                    If purviewArray(2) = False Then
                        Response.Redirect("~/PresentationLayer/logon.aspx")
                    End If
                    Me.saveButton.Enabled = purviewArray(1)
                    If Convert.ToString(purviewArray(4)).ToUpper = "FALSE" Then
                        Me.ctrystat.Items.Remove(Me.ctrystat.Items.FindByValue("DELETE"))
                    End If



                    Dim TelEntity As New clsCommonClass
                    Me.tel1box.Text = TelEntity.passconverttel(edtReader.GetValue(4).ToString())
                    Me.tel2box.Text = TelEntity.passconverttel(edtReader.GetValue(5).ToString())
                    Me.faxbox.Text = TelEntity.passconverttel(edtReader.GetValue(6).ToString())
                    Me.emailbox.Text = edtReader.GetValue(7).ToString()
                    Me.ad1box.Text = edtReader.GetValue(8).ToString()
                    Me.ad2box.Text = edtReader.GetValue(9).ToString()
                    Me.pocodebox.Text = edtReader.GetValue(10).ToString()
                    Me.ctrid.Text = edtReader.GetValue(11).ToString()
                    'Dim state As New clsCommonClass

                    'If (Me.ctrid.SelectedValue().ToString() <> "") Then
                    '    state.spctr = Me.ctrid.SelectedValue().ToString().ToUpper()
                    'End If
                    ''Dim state As New clsCommonClass
                    'Dim stateds As New DataSet

                    'stateds = state.Getcomidname("BB_MASSTAT_IDNAME")
                    'If stateds.Tables.Count <> 0 Then
                    '    databonds(stateds, Me.staid)
                    'End If

                    Dim stat As New clsCommonClass
                    If (ctrid.SelectedValue().ToString() <> "") Then
                        stat.spctr = Me.ctrid.SelectedValue().ToString().ToUpper()
                    End If

                    stat.rank = rank
                    Dim stateds As New DataSet

                    stateds = stat.Getcomidname("BB_MASSTAT_IDNAME")
                    If stateds.Tables.Count <> 0 Then
                        databonds(stateds, Me.staid)
                    End If
                    Me.staid.Items.Insert(0, "")
                    Me.staid.Text = edtReader.GetValue(12).ToString()

                    'Dim area As New clsCommonClass
                    ''Dim ctridname As String
                    'If (Me.ctrid.SelectedValue().ToString() <> "") Then
                    '    area.spctr = Me.ctrid.SelectedValue().ToString().ToUpper()
                    'End If
                    ''Dim staid As String
                    'If (Me.staid.SelectedValue().ToString() <> "") Then
                    '    area.spstat = Me.staid.SelectedValue().ToString().ToUpper()
                    'End If
                    ''create  area the dropdownlist
                    ''Dim area As New clsCommonClass
                    'Dim areads As New DataSet
                    'areads = area.Getcomidname("BB_MASAREA_IDNAME")
                    'If areads.Tables.Count <> 0 Then
                    '    databonds(areads, Me.areaid)
                    'End If
                    Dim area As New clsCommonClass


                    If (ctrid.SelectedValue().ToString() <> "") Then
                        area.spctr = Me.ctrid.SelectedValue().ToString().ToUpper()
                    End If

                    If (staid.SelectedValue().ToString() <> "") Then
                        area.spstat = Me.staid.SelectedValue().ToString().ToUpper()
                    End If
                    area.rank = rank

                    'create  area the dropdownlist
                    'Dim area As New clsCommonClass
                    Dim areads As New DataSet

                    areads = area.Getcomidname("BB_MASAREA_IDNAME")
                    If areads.Tables.Count <> 0 Then
                        databonds(areads, Me.areaid)
                    End If
                    Me.areaid.Items.Insert(0, "")

                    Me.areaid.Text = edtReader.GetValue(13).ToString()
                    'Me.taxid.Text
                    'create the dropdownlist

                    'creatbybox.Text = edtReader.GetValue(14).ToString()

                    Dim Rcreatby As String = edtReader.GetValue(14).ToString().ToUpper()
                    If Rcreatby <> "ADMIN" Then
                        Dim Rcreat As New clsCommonClass
                        Rcreat.userid() = Rcreatby
                        Dim Rcreatds As New DataSet()
                        Rcreatds = Rcreat.Getuseridname("BB_MASUSER_SelByIDName")
                        Me.creatbybox.Text = Rcreatds.Tables(0).Rows(0).Item(0)
                    Else
                        Me.creatbybox.Text = "ADMIN-ADMIN"
                    End If
                    creatbybox.ReadOnly = True

                    creatdtbox.Text = edtReader.GetValue(15).ToString()
                    creatdtbox.ReadOnly = True
                    'modfybybox.Text = edtReader.GetValue(16).ToString()
                    'modfybybox.ReadOnly = True
                    Dim userIDNamestr As String = Session("userID").ToString().ToUpper + "-" + Session("username").ToString.ToUpper
                    modfybybox.Text = userIDNamestr
                    mdfydtbox.Text = edtReader.GetValue(17).ToString()
                    mdfydtbox.ReadOnly = True

                    'modfybybox.Text =  Session("username")
                    'modfybybox.ReadOnly = True
                    'mdfydtbox.Text = Date.Now()
                    'mdfydtbox.ReadOnly = True
                End If

            End If




            Me.compnmbox.Focus()

        End If

    End Sub
#Region " id bond"
    Public Function databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        Dim row As DataRow
        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            NewItem.Text = row("id") & "-" & row("name")
            NewItem.Value = row("id")
            dropdown.Items.Add(NewItem)
        Next
        'dropdown.Items(0).Selected = True
    End Function
#End Region

    Protected Sub saveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveButton.Click
        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim msginfo As String = objXm.GetLabelName("StatusMessage", "SAVEORNOT")
        Dim msgtitle As String = objXm.GetLabelName("StatusMessage", "SAVETITLE")
        MessageBox1.Confirm(msginfo)
    End Sub

    Protected Sub ctrid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ctrid.SelectedIndexChanged

        Me.staid.Items.Clear()
        Me.areaid.Items.Clear()
        'If Me.ctrid.Text <> "" Then
        Dim rank As String = Session("login_rank")
        Dim state As New clsCommonClass

        If (Me.ctrid.SelectedValue().ToString() <> "") Then
            state.spctr = Me.ctrid.SelectedValue().ToString().ToUpper()
        End If
        state.rank = rank

        'Dim state As New clsCommonClass
        Dim stateds As New DataSet

        stateds = state.Getcomidname("BB_MASSTAT_IDNAME")
        If stateds.Tables.Count <> 0 Then
            databonds(stateds, Me.staid)
        End If
        Me.staid.Items.Insert(0, "")


        
        'Else
        'Me.staid.Items.Clear()
        'Me.areaid.Items.Clear()
        'Me.staid.Items.Add(New ListItem("", ""))
        'Me.areaid.Items.Add(New ListItem("", ""))
        'End If
    End Sub

    Protected Sub staid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles staid.SelectedIndexChanged
        Me.areaid.Items.Clear()
        'If Me.staid.Text <> "" Then
        Dim rank As String = Session("login_rank")
        Dim area As New clsCommonClass
        'Dim ctridname As String
        If (ctrid.SelectedValue().ToString() <> "") Then
            area.spctr = Me.ctrid.SelectedValue().ToString().ToUpper()
        End If
        'Dim staid As String
        If (staid.SelectedValue().ToString() <> "") Then
            area.spstat = Me.staid.SelectedValue().ToString().ToUpper()
        End If
        area.rank = rank

        'create  area the dropdownlist
        'Dim area As New clsCommonClass
        Dim areads As New DataSet

        areads = area.Getcomidname("BB_MASAREA_IDNAME")
        If areads.Tables.Count <> 0 Then
            databonds(areads, Me.areaid)
        End If
        Me.areaid.Items.Insert(0, "")
        'Else

        'Me.areaid.Items.Clear()
        'Me.staid.Items.Add(New ListItem("", ""))
        'Me.areaid.Items.Add(New ListItem("", ""))
        'End If
    End Sub

    Protected Sub MessageBox1_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox1.GetMessageBoxResponse
        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
            Dim compEntity As New clsCompany()
            Dim TelEntity As New clsCommonClass
            If (Trim(Me.compidbox.Text) <> "") Then
                compEntity.CompID = compidbox.Text.ToUpper()
            End If

            If (Me.ctrid.Text.ToString() <> "") Then
                compEntity.CountryID = ctrid.SelectedItem.Value.ToString().ToUpper()
            End If

            If (Me.staid.Text.ToString() <> "") Then
                compEntity.StateID = staid.SelectedItem.Value.ToString().ToUpper()
            End If
            'compEntity.StateID = staid.SelectedItem.Value.ToString().ToUpper()

            If (Me.areaid.Text.ToString() <> "") Then
                compEntity.AreaID = areaid.SelectedItem.Value.ToString().ToUpper()
            End If

            'compEntity.AreaID = areaid.SelectedItem.Value.ToString().ToUpper()

            If (Trim(Me.compnmbox.Text) <> "") Then
                compEntity.CompName = compnmbox.Text.ToUpper()
            End If
            If (Trim(Me.altbox.Text) <> "") Then
                compEntity.CompAlternateName = altbox.Text.ToUpper()
            End If
            If (ctrystat.SelectedItem.Value.ToString() <> "") Then
                compEntity.CompStatus = ctrystat.SelectedItem.Value.ToString()
            End If
            If (Trim(Me.ad1box.Text) <> "") Then
                compEntity.Address1 = ad1box.Text
            End If
            If (Trim(Me.ad2box.Text) <> "") Then
                compEntity.Address2 = ad2box.Text
            End If

            If (Trim(Me.pocodebox.Text) <> "") Then
                compEntity.POcode = pocodebox.Text
            End If

            If (Trim(Me.tel1box.Text) <> "") Then
                compEntity.Tel1 = TelEntity.telconvertpass(tel1box.Text)
            End If
            If (Trim(Me.tel2box.Text) <> "") Then
                compEntity.Tel2 = TelEntity.telconvertpass(tel2box.Text)
            End If

            If (Trim(Me.emailbox.Text) <> "") Then
                compEntity.email = emailbox.Text
            End If
            If (Trim(Me.faxbox.Text) <> "") Then
                compEntity.Fax = TelEntity.telconvertpass(faxbox.Text)
            End If
            'If (Trim(creatbybox.Text) <> "") Then
            '    compEntity.CreateBy = creatbybox.Text.ToUpper()
            'End If
            Dim cntryDate As New clsCommonClass()
            If (Trim(creatdtbox.Text) <> "") Then
                compEntity.CreateDate = cntryDate.DatetoDatabase(creatdtbox.Text)
            End If
            If (Trim(modfybybox.Text) <> "") Then
                compEntity.ModifyBy = Session("userID").ToString().ToUpper
            End If
            If (Trim(mdfydtbox.Text) <> "") Then
                compEntity.ModifyDate = cntryDate.DatetoDatabase(mdfydtbox.Text)
            End If

            compEntity.IPaddr = Request.UserHostAddress.ToString()
            compEntity.servid = Session("login_svcID")

            '''''''''delete'''''''''''''''''''''''''
            If compEntity.CompStatus = "DELETE" Then

                Dim delcompid As Integer = compEntity.GetserviceByCOMPID()
                If delcompid <> 0 Then
                    Dim objXm As New clsXml
                    objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    errlab.Text = objXm.GetLabelName("StatusMessage", "DELCOMP")
                    errlab.Visible = True
                    Return
                End If
            End If
            '''''''''delete'''''''''''''''''''''''''

            Dim selvalue As Integer = compEntity.GetCompanyDetailsByCompNM()
            If selvalue <> 0 Then
                Dim objXm As New clsXml
                objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                errlab.Text = objXm.GetLabelName("StatusMessage", "DUPCOMPNM")
                errlab.Visible = True
            ElseIf selvalue.Equals(0) Then
                Dim updCtryCnt As Integer = compEntity.Update()

                If updCtryCnt = 0 Then
                    Dim objXm As New clsXml
                    objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    errlab.Text = objXm.GetLabelName("StatusMessage", "BB-HintMsg-Suc")
                    errlab.Visible = True
                    'Response.Redirect("company.aspx")
                Else
                    Response.Redirect("~/PresentationLayer/Error.aspx")
                End If
            End If
        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
        End If
    End Sub
End Class
