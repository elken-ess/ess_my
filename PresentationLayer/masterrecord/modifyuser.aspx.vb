﻿Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Partial Class PresentationLayer_masterrecord_modifyuser
    Inherits System.Web.UI.Page

#Region "page load "
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try

        If Not Page.IsPostBack Then

            '''''''''''''''''''''''''''''''''''''''''''''
            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "12")
            Me.LinkButton1.Enabled = purviewArray(1)
            If purviewArray(2) = False Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''
            '''''''''''''''''''''''''''''''''''''''''''''''''''''
            Try
                Dim str As String = Request.Params("ctryid").ToString
                str = Request.Params("STAFSTAT").ToString()
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


            Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
            'display label message
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            lblNoteUP.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            lblNoteDown.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            Me.UserIDLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASUSER-0001")
            Me.StaffStatusLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASUSER-0002")
            Me.CountryIDLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASUSER-0003")
            Me.UserNameLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASUSER-0004")
            Me.AlterNameLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASUSER-0005")
            Me.AccessProGroupLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASUSER-0006")
            Me.RankLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASUSER-0007")
            Me.FailedLogonLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASUSER-0008")
            Me.ChangePassWdLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASUSER-0009")
            Me.CompanIDLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASUSER-0010")
            Me.DepartmentLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASUSER-0011")
            Me.PermanentStaffLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASUSER-0012")
            Me.SvcIDLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASUSER-0013")
            Me.EmailAdressLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASUSER-0015")

            creatby.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATBY")

            creatdate.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
            modfyby.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFBY")
            mdfydate.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFDATE")
            Me.titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASUSER-0019")
            Me.pswLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASUSER-0014")
            Me.confirmpswLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASUSER-0018")
            LinkButton1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Save")
            HyperLink1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Cancel")

            'create by create date  
            Dim userIDNamestr As String = Session("userID").ToString().ToUpper + "-" + Session("username").ToString.ToUpper
            modfybybox.Text = userIDNamestr

            Me.UserNameBox.Focus()
            ' \\\\\\\\\\\\\\\\\\\\\\\\//////////////////////////////////
            Dim statXmlTr As New clsXml
            statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            '\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////
            Me.RFuserid.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "CHUSID")
            Me.RFusername.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "CHUSNM")
            Me.REVEmail.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "CHUEMAIL")
            'Me.RequiredFieldValidator2.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "CHUCPSW")
            'Me.RequiredFieldValidator1.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "CHUPSW")
            Me.CompareValidator1.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "CHUPSWCPSW")
            Me.RegularExpressionValidator1.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "PSWLNTH")
            Me.RFVCOTRY.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "USERCHCTRY")
            Me.RFVCOMP.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "USERCHCOM")
            Me.RFVSERVICE.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "USERCHSVC")
            Me.rfvaccessg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "CHUSACCEG")

            '\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
            Dim mdyuserid As String = Request.Params("ctryid").ToString()
            Dim mdyuserstat As String = Request.Params("STAFSTAT")

            Dim userEntity As New clsUser()
            'GET REQUEST VALUE
            If mdyuserid.Trim() <> "" Then
                userEntity.UserID = mdyuserid
            End If
            If mdyuserstat.Trim() <> "" Then
                'userEntity.StaffStatus = statXmlTr.GetLabelID("StatusMessage", mdyuserstat)
                userEntity.StaffStatus = mdyuserstat
            End If


            'create the dropdownlist of staff status

            Dim cntryStat As New clsrlconfirminf()
            Dim statParam As ArrayList = New ArrayList
            statParam = cntryStat.searchconfirminf("STAFFSTAT")
            Dim count As Integer
            Dim statid As String
            Dim statnm As String
            If purviewArray(4) = False Then
                Me.StaffStatusDDL.Items.Add(New ListItem(statParam.Item(1), statParam.Item(0).ToString()))
            Else
                For count = 0 To statParam.Count - 1
                    statid = statParam.Item(count)
                    statnm = statParam.Item(count + 1)
                    count = count + 1
                    Me.StaffStatusDDL.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                Next
            End If
            ''''''''''''''''''xxl quan xian'''''''''''''''''''''''''''''''''''''''''''
            'If purviewArray(4) = False Then
            '    Me.StaffStatusDDL.Items.Remove(Me.StaffStatusDDL.Items.FindByValue("DELETE"))
            'End If                '  
            'deptment dropdownlist bond\
            Dim detpParam As ArrayList = New ArrayList()
            detpParam = cntryStat.searchconfirminf("DEPT")
            Dim deptid As String
            Dim deptnm As String
            'For count = 1 To detpParam.Count
            '    deptid = detpParam.Item(count)
            '    deptnm = detpParam.Item(count)
            '    count = count + 1
            '    If UCase(deptid) <> "ALL" Then
            '        Me.DepartmentDDL.Items.Add(New ListItem(deptnm.ToString(), deptid.ToString()))
            '    End If

            'Next

            For count = 0 To detpParam.Count - 1
                deptid = detpParam.Item(count)
                deptnm = detpParam.Item(count + 1)
                count = count + 1
                Me.DepartmentDDL.Items.Add(New ListItem(deptnm.ToString(), deptid.ToString()))
            Next

            'Change passwd and permenent staff  list bond  2 item y/n
            Dim pcdtclass As ArrayList = New ArrayList
            pcdtclass = cntryStat.searchconfirminf("YESNO")
            Dim pcdtclsID As String
            Dim pcdtclsName As String
            Me.PermanentStaffDDL.Items.Clear()
            Me.ChangePassWdDDL.Items.Clear()
            For count = 0 To pcdtclass.Count - 1
                pcdtclsID = pcdtclass.Item(count)
                pcdtclsName = pcdtclass.Item(count + 1)
                count = count + 1
                Me.PermanentStaffDDL.Items.Add(New ListItem(pcdtclsName.ToString(), pcdtclsID.ToString()))
                Me.ChangePassWdDDL.Items.Add(New ListItem(pcdtclsName.ToString(), pcdtclsID.ToString()))
            Next
            'Rank ddl bond 
            Dim rankcls As ArrayList = New ArrayList
            rankcls = cntryStat.searchconfirminf("RANK")
            Dim rankid As String
            Dim rankname As String
            Me.RankDDL.Items.Clear()
            Dim startint As Integer = 0
            Dim login_rank As Integer = Session("login_rank")
            If login_rank <> 0 Then
                Select Case (login_rank)
                    Case 9
                        startint = 4
                    Case 8
                        startint = 2

                    Case 7
                        startint = 0
                End Select
            Else
            End If
            For count = startint To rankcls.Count - 1
                rankid = rankcls.Item(count)
                rankname = rankcls.Item(count + 1)
                count = count + 1
                Me.RankDDL.Items.Add(New ListItem(rankname.ToString(), rankid.ToString()))
            Next
            'bonds country  id  name
            Dim clscom As New clsCommonClass()
            Dim idnameds As New DataSet()
            Dim rank As Integer = Session("login_rank")
            clscom.rank = rank
            If rank <> 0 Then
                clscom.spctr = Session("login_ctryID")
                clscom.spstat = Session("login_cmpID")
                clscom.sparea = Session("login_svcID")
            End If

            idnameds = clscom.Getcomidname("BB_MASCTRY_IDNAME")
            databonds(idnameds, Me.CountryIDDDL)
            Me.CountryIDDDL.Items.Insert(0, "")
            'bonds svcid name
            Dim svcidnameds As New DataSet()

            svcidnameds = clscom.Getcomidname("BB_MASSVRC_IDNAME")
            databonds(svcidnameds, Me.SvcIDDDL)
            Me.SvcIDDDL.Items.Insert(0, "")
            'bonds company id name\
            If (Trim(Me.CountryIDDDL.Text) <> "") Then
                clscom.spctr = Me.CountryIDDDL.SelectedValue
            End If

            'Me.CompanIDDDL.Items.Insert(0, "")
            'bonds access profile group \
            Dim accessprofidnameds As New DataSet()
            accessprofidnameds = clscom.Getidname("BB_MASMUA_SELBYIDNM")
            databonds(accessprofidnameds, Me.AccessProGroupDDL)
            Me.AccessProGroupDDL.Items.Insert(0, "")

            ''''''''''''''''''''''''''''''''''''''''''''''''''''

            Dim edtReader As SqlDataReader = userEntity.GetUserDetailsByID()
            If edtReader.Read() Then
                Me.UserIDBox.Text = edtReader.Item(0).ToString()
                Me.UserNameBox.Text = edtReader.Item(1).ToString()
                Me.AlterNameBox.Text = edtReader.Item(2).ToString()
                Me.StaffStatusDDL.SelectedValue = edtReader.Item(3).ToString()
                Me.DepartmentDDL.SelectedValue = edtReader.Item(4).ToString()
                Me.RankDDL.SelectedValue = edtReader.Item(5).ToString()
                Me.PermanentStaffDDL.SelectedValue = edtReader.Item(6).ToString()
                Me.CountryIDDDL.SelectedValue = edtReader.Item(7).ToString()
                Me.SvcIDDDL.SelectedValue = edtReader.Item(8).ToString()
                Dim compidnameds As New DataSet()
                If rank <> 0 Then
                    clscom.spctr = Session("login_ctryID")
                    clscom.spstat = Session("login_cmpID")
                    clscom.sparea = Session("login_svcID")
                Else
                    If (Me.CountryIDDDL.Text <> "") Then
                        clscom.spctr = Me.CountryIDDDL.SelectedValue().ToString().ToUpper()
                    End If
                End If

                clscom.rank = rank
                compidnameds = clscom.Getcomidname("BB_MASCOMP_IDNAME")
                databonds(compidnameds, Me.CompanIDDDL)
                Me.CompanIDDDL.Items.Insert(0, "")

                Me.CompanIDDDL.SelectedValue = edtReader.Item(9).ToString()
                Me.AccessProGroupDDL.SelectedValue = edtReader.Item(10).ToString()
                Me.FailedLogonBox.Text = edtReader.Item(11).ToString()
                Me.ChangePassWdDDL.SelectedValue = edtReader.Item(12).ToString()
                Me.EmailAdressBox.Text = edtReader.Item(13).ToString()

                Dim Rcreatby As String = edtReader.Item(14).ToString().ToUpper()
                If Rcreatby <> "ADMIN" Then
                    Dim Rcreat As New clsCommonClass
                    Rcreat.userid() = Rcreatby
                    Dim Rcreatds As New DataSet()
                    Rcreatds = Rcreat.Getuseridname("BB_MASUSER_SelByIDName")

                    Me.creatbybox.Text = Rcreatds.Tables(0).Rows(0).Item(0)
                Else
                    Me.creatbybox.Text = "ADMIN-ADMIN"
                End If
                'Me.creatbybox.Text = edtReader.Item(14).ToString().ToUpper()

                Me.creatdtbox.Text = edtReader.Item(15).ToString()
                Session("old_psw") = edtReader.Item(16).ToString()
                'Me.ConfirPasWdBox.Text = edtReader.Item(16).ToString()
                'Me.mdfydtbox.Text = edtReader.Item(17).ToString()

                'Me.pswBox.TextMode = TextBoxMode.SingleLine
                'Me.pswBox.Text = "*********"
                'Me.pswBox.TextMode = TextBoxMode.Password


            End If
            '\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
        End If
    End Sub
#End Region


#Region " command bond function "
    Public Function databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        Dim row As DataRow
        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            NewItem.Text = Trim(row(0).ToString()) & "-" & Trim(row(1).ToString())
            NewItem.Value = row(0)
            dropdown.Items.Add(NewItem)
        Next
    End Function
#End Region

#Region "modify user  click '"
    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton1.Click
        If (Trim(Me.pswBox.Text) <> "") Then
            Session("user_psw") = FormsAuthentication.HashPasswordForStoringInConfigFile(Me.pswBox.Text, "md5")
        Else
            Session("user_psw") = Session("old_psw")
        End If
        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim msginfo As String = objXm.GetLabelName("StatusMessage", "SAVEORNOT")
        Dim msgtitle As String = objXm.GetLabelName("StatusMessage", "SAVETITLE")
        MessageBox1.Confirm(msginfo, msgtitle)
    End Sub

#End Region

#Region "modify user function "
    Public Sub addrecord()
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim userEntity As New clsUser()
        If (Trim(Me.CountryIDDDL.Text) <> "") Then
            userEntity.CountryID = Me.CountryIDDDL.SelectedValue
        End If
        If (Trim(Me.RankDDL.Text) <> "") Then
            userEntity.Rank = RankDDL.SelectedValue
        End If
        If (Trim(Me.PermanentStaffDDL.Text) <> "") Then
            userEntity.PenentStuffID = PermanentStaffDDL.SelectedValue
        End If
        If (Trim(Me.SvcIDDDL.Text) <> "") Then
            userEntity.SVCID = SvcIDDDL.SelectedValue
        End If
        If (Trim(Me.AccessProGroupDDL.Text) <> "") Then
            userEntity.AccessProfileGrop = AccessProGroupDDL.SelectedValue
        End If
        If (Trim(Me.ChangePassWdDDL.Text) <> "") Then
            userEntity.ChangeWd = ChangePassWdDDL.SelectedValue
        End If
        If (Trim(Me.CompanIDDDL.Text) <> "") Then
            userEntity.CompID = CompanIDDDL.SelectedValue
        End If
        If (Trim(Me.DepartmentDDL.Text) <> "") Then
            userEntity.DepmentID = DepartmentDDL.SelectedValue
        End If
        If (Trim(Me.StaffStatusDDL.Text) <> "") Then
            userEntity.StaffStatus = StaffStatusDDL.SelectedValue
        End If
        If (Trim(Me.UserIDBox.Text) <> "") Then
            userEntity.UserID = UserIDBox.Text
        End If
        If (Trim(Me.UserNameBox.Text) <> "") Then
            userEntity.UserName = Me.UserNameBox.Text
        End If
        If (Trim(Me.AlterNameBox.Text) <> "") Then
            userEntity.UserAlternateName = AlterNameBox.Text
        End If
        If (Trim(Me.EmailAdressBox.Text) <> "") Then
            userEntity.EmailAddrss = EmailAdressBox.Text
        End If
        If (Trim(Me.modfybybox.Text) <> "") Then
            userEntity.Modifyby = Session("userID").ToString().ToUpper
        End If

        userEntity.PassWord = Session("user_psw").ToString()
        userEntity.LoginIP = Request.UserHostAddress.ToString
        userEntity.LoginSvc = Session("login_svcID").ToString
        ' checkno()
        'insert recorde
        Dim chint As Integer = checkname()
        If chint = 1 Then
            Me.addinfo.Text = objXmlTr.GetLabelName("StatusMessage", "DUPNAMEID")
            addinfo.Visible = True
        Else
            Dim insJobCnt As Integer = userEntity.Update()
            If insJobCnt = 0 Then
                addinfo.Text = objXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Suc")
                addinfo.Visible = True
            Else
                Response.Redirect("~/PresentationLayer/Error.aspx")
            End If

        End If
    End Sub
#End Region
#Region "check  if there are same USER  name "
    Public Function checkname() As Integer
        Dim userEntity As New clsUser()
        If (Trim(Me.UserIDBox.Text) <> "") Then
            userEntity.UserID = UserIDBox.Text.ToUpper()
        End If
        If (Trim(Me.UserNameBox.Text) <> "") Then
            userEntity.UserName = Me.UserNameBox.Text.ToUpper()
        End If
        Dim chckname As Integer = 0
        Dim sqldr As SqlDataReader = userEntity.MdyUserCheckName()
        If (sqldr.Read()) Then
            chckname = 1
        End If
        Return chckname
    End Function
#End Region

    Protected Sub MessageBox1_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox1.GetMessageBoxResponse
        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
            'insert recorde
            addrecord()
        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
        End If
    End Sub

    

    Protected Sub CountryIDDDL_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CountryIDDDL.SelectedIndexChanged
        If CountryIDDDL.Text <> "" Then
            Dim rank As String = Session("login_rank")
            Dim clscom As New clsCommonClass()
            Dim compidnameds As New DataSet()
            If rank <> 0 Then
                clscom.spctr = Session("login_ctryID")
                clscom.spstat = Session("login_cmpID")
                clscom.sparea = Session("login_svcID")
            Else
                If (Me.CountryIDDDL.Text <> "") Then
                    clscom.spctr = Me.CountryIDDDL.SelectedValue().ToString().ToUpper()
                End If
            End If
            clscom.rank = rank
            compidnameds = clscom.Getcomidname("BB_MASCOMP_IDNAME")
            databonds(compidnameds, Me.CompanIDDDL)
            Me.CompanIDDDL.Items.Insert(0, "")
        Else
            Me.CompanIDDDL.Items.Clear()
            Me.SvcIDDDL.Items.Clear()
        End If
       
    End Sub

    Protected Sub CompanIDDDL_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CompanIDDDL.SelectedIndexChanged
        If CompanIDDDL.Text <> "" Then
            Dim rank As String = Session("login_rank")
            Dim clscom As New clsCommonClass()
            Dim compidnameds As New DataSet()
            'If rank <> 0 Then
            '    clscom.spctr = Session("login_ctryID")
            '    clscom.spstat = Session("login_cmpID")
            '    clscom.sparea = Session("login_svcID")
            'Else
            If (Me.CountryIDDDL.Text <> "") Then
                clscom.spctr = Me.CountryIDDDL.SelectedValue().ToString().ToUpper()
            End If
            If (Me.CompanIDDDL.Text <> "") Then
                clscom.spstat = Me.CompanIDDDL.SelectedValue().ToUpper.ToUpper
            End If
            'End If
            clscom.rank = rank
            compidnameds = clscom.Getcomidname("BB_MASSVRC_IDNAME")
            databonds(compidnameds, Me.SvcIDDDL)
            Me.SvcIDDDL.Items.Insert(0, "")
        Else
            Me.SvcIDDDL.Items.Clear()

        End If
       
    End Sub
End Class

