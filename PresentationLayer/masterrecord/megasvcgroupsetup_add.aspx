<%@ Page Language="VB" AutoEventWireup="false" EnableEventValidation ="false"  CodeFile="megasvcgroupsetup_add.aspx.vb" Inherits="PresentationLayer_masterrecord_megasvcgroupsetup_add" %>

<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>add country</title>
    <meta http-equiv="Content-Type" content="text/html; charset=gb2312">
	<LINK href="../css/style.css" type="text/css" rel="stylesheet">
</head>
<body  id="countrybody">
    <form id="Countryform" runat="server">
     <TABLE border="0" id=countrytab style="width: 100%">
        <tr>
           <td>
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>
							<TD width="1%" background="../graph/title_bg.gif"><IMG height="24" src="../graph/title1.gif" width="5"></TD>
							<TD class="style2" width="98%" background="../graph/title_bg.gif">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></TD>
							<TD align="right" width="1%" background="../graph/title_bg.gif"><IMG height="24" src="../graph/title_2.gif" width="5"></TD>
						</TR>
			 </TABLE>
           </tr>
           <tr>
           <td>
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>					
							<TD class="style2" width="98%" background="../graph/title_bg.gif">
                                <font color=red><asp:Label ID="errlab" runat="server" Text="Label" Visible=false></asp:Label></font>
                                </TD>
						</TR>
			 </TABLE>
           </tr>
			<TR>
				<TD style="width: 0px">
					<TABLE id="country" cellSpacing="1" cellPadding="2" bgColor="#b7e6e6" border="0" style="width: 100%">
					<tr bgcolor="#ffffff">
<td colspan = 4>
                            <asp:Label ID="lblCompTop" runat="server" ForeColor="Red" Text="Label" Visible="True"></asp:Label>
</td>
             </tr>
             <tr bgcolor="#ffffff">
<td colspan = 4>
                        	&nbsp;
                     	</td>
               </tr>   
									  <TR bgColor="#ffffff">
											<TD  align=right width="15%" style="height: 30px">
                                                &nbsp;<asp:Label ID="lblgroup" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%; height: 30px;">
                                                <asp:TextBox ID="txtgroup" runat="server" CssClass="textborder" MaxLength="50" Width="88%" style="width: 88%"></asp:TextBox>
                                                <font color=red style="font-weight: bold">*</font>
                                                <asp:RequiredFieldValidator ID="valgroup" runat="server" ControlToValidate="txtgroup" Display="Dynamic" ErrorMessage="cannot be empty!"></asp:RequiredFieldValidator>
                                                </TD>
											<TD  align=right width="15%" style="height: 30px">
                                                <asp:Label ID="lblmodel" runat="server" Text="Label"></asp:Label></TD>
											<TD  align=left style="height: 30px; width: 35%;">
                                                <font color=red><strong>
                                                <asp:DropDownList ID="ddlmodel" runat="server" Width="88%">
                                                </asp:DropDownList>
                                                *</strong></font>
                                                </TD>
										</TR>
										<TR bgColor="#ffffff">
											<TD  align=right width="15%">
                                                <asp:Label ID="lblmonths" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left width="85%" colspan=3>
                                                <asp:TextBox ID="txtmonths" runat="server" CssClass="textborder" MaxLength="3" style="width: 35%"></asp:TextBox>
                                                <font color=red style="font-weight: bold">*</font>
                                                <asp:RequiredFieldValidator ID="valmonths" runat="server" ControlToValidate="txtmonths" Display="Dynamic" ErrorMessage="cannot be empty!"></asp:RequiredFieldValidator>
&nbsp;<em><br />
                                                (Must be numerical)</em>
                                                <asp:RequiredFieldValidator ID="ctryalterr" runat="server" ControlToValidate="txtmonths" ForeColor = White>*</asp:RequiredFieldValidator>
                                                </TD>
										</TR>
										<TR bgColor="#ffffff">
											<TD  align=right width="15%">
                                                <asp:Label ID="lblinterval" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%">
                                                <asp:TextBox ID="txtinterval" runat="server" CssClass="textborder" MaxLength="3" style="width: 88%" Width="88%"></asp:TextBox>
                                                <font color=red style="font-weight: bold">*</font>
                                                <asp:RequiredFieldValidator ID="valinterval" runat="server" ControlToValidate="txtinterval" Display="Dynamic" ErrorMessage="cannot be empty!"></asp:RequiredFieldValidator>
                                                <em>(Must be numerical)</em></TD>
											<TD align=right width="15%">
                                                &nbsp;</TD>
											<TD align=left style="width: 35%">
                                                &nbsp;</TD>
										</TR>
										<TR bgColor="#ffffff">
											<TD align=right width="15%">
                                                <asp:Label ID="lblfrequency" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%">
                                                <asp:TextBox ID="txtfrequency" runat="server" CssClass="textborder" MaxLength="3" style="width: 88%" Width="88%"></asp:TextBox>
                                                <font color=red style="font-weight: bold">*</font>
                                                <asp:RequiredFieldValidator ID="valfrequency" runat="server" ControlToValidate="txtfrequency" Display="Dynamic" ErrorMessage="cannot be empty!"></asp:RequiredFieldValidator>
                                                <em>(Must be numerical)</em></TD>
											<TD align=right width="15%">
                                                &nbsp;</TD>
											<TD align=left style="width: 35%">
                                                &nbsp;</TD>
										</TR>
										<TR bgColor="#ffffff">
											<TD align=right width="15%">
                                                <asp:Label ID="lblcreatedby" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%">
                                                <asp:TextBox ID="txtcreatedby" runat="server" CssClass="textborder" ReadOnly=true Rows="20" style="width: 88%" Width="88%"></asp:TextBox></TD>
											<TD align=right width="15%">
                                                <asp:Label ID="lblcreateddate" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%">
                                                <asp:TextBox ID="txtcreateddate" runat="server" CssClass="textborder" ReadOnly=true style="width: 88%" Width="88%"></asp:TextBox></TD>
										</TR>
										<TR bgColor="#ffffff">
											<TD align=right width="15%">
                                                <asp:Label ID="lblmodifiedby" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%">
                                                <asp:TextBox ID="txtmodifiedby" runat="server" CssClass="textborder" ReadOnly=true Rows="20" style="width: 88%" Width="88%"></asp:TextBox></TD>
											<TD align=right width="15%">
                                                <asp:Label ID="lblmodifieddate" runat="server" Text="Label"></asp:Label></TD>
											<TD align=left style="width: 35%">
                                                <asp:TextBox ID="txtmodifieddate" runat="server" CssClass="textborder" ReadOnly=true style="width: 88%" Width="88%"></asp:TextBox></TD>
										</TR>
								</TABLE>
						</TD>
					</TR>
					<TR>
						<TD style="height: 37px; width: 409px;">
						<font color=red><asp:Label ID="lblCompBottom" runat="server" Text="Label" Visible="true"></asp:Label></font>
							<TABLE id="Table1" cellSpacing="1" cellPadding="1"  border="0">
									<TR>
										<TD style="WIDTH: 20%" align=center>
                                            &nbsp;<asp:LinkButton ID="saveButton" runat="server"></asp:LinkButton></TD>
										<td align=center>
                                            <asp:HyperLink ID="cancelLink" runat="server" NavigateUrl="~/PresentationLayer/masterrecord/megasvcgroupsetup.aspx">[cancelLink]</asp:HyperLink></td>
									<td align=center><asp:HyperLink ID="AddLink" runat="server" NavigateUrl="~/PresentationLayer/masterrecord/megasvcgroupsetup_add.aspx">[Add]</asp:HyperLink></td> </TR>
							</TABLE>
						</TD>
					</TR>
			</TABLE>
       <cc1:messagebox id="MessageBox1" runat="server"></cc1:messagebox>
                                               
                                                
    </form>
</body>
</html>
