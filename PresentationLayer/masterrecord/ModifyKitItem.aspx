<%@ Page Language="VB" AutoEventWireup="false" EnableEventValidation="false"  CodeFile="ModifyKitItem.aspx.vb" Inherits="PresentationLayer_masterrecord_ModifyKitItem" %>

<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
     <title>modify Kit item Page</title>
     <meta http-equiv="Content-Type" content="text/html; charset=gb2312">
	<LINK href="../css/style.css" type="text/css" rel="stylesheet">
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div>
            <table id="countrytab" border="0" width="100%">
                <tr>
                    <td style="width: 500px">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td background="../graph/title_bg.gif" width="1%">
                                    <img height="24" src="../graph/title1.gif" width="5" /></td>
                                <td background="../graph/title_bg.gif" class="style2" style="width: 100%" width="98%">
                                    <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="right" background="../graph/title_bg.gif" width="1%">
                                    <img height="24" src="../graph/title_2.gif" width="5" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="width: 500px; height: 30px">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td background="../graph/title_bg.gif" class="style2" style="height: 14px" width="98%">
                                    <font color="red" style="width: 100%">
                                        <asp:Label ID="errlab" runat="server" Text="Label" Visible="False" Width="496px"></asp:Label></font></td>
                            </tr>
                        </table>
                        <asp:Label ID="Label9" runat="server" ForeColor="Red"></asp:Label></td>
                </tr>
                <tr>
                    <td style="width: 500px">
                        <table id="country" bgcolor="#b7e6e6" border="0" cellpadding="2" cellspacing="1"
                            style="width: 100%">
                            <tr bgcolor="#ffffff">
                                <td align="right" style="height: 30px" width="15%">
                                    &nbsp;<asp:Label ID="kitidlab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 35%; height: 30px">
                                    <asp:TextBox ID="kitidbox" runat="server" CssClass="textborder" MaxLength="10" Width="82%" ReadOnly="True"></asp:TextBox>
                                    <font
                                        color="red" style="font-weight: bold">*</font><asp:RequiredFieldValidator ID="kitiderr" runat="server" ControlToValidate="kitidbox"
                                        Width="2px" ForeColor="White">*</asp:RequiredFieldValidator></td>
                                <td align="right" style="height: 30px" width="15%">
                                    <asp:Label ID="partidLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 35%; height: 30px"><asp:DropDownList ID="DpDpartid" runat="server" CssClass="textborder" Style="width: 82%"
                                        Width="136px" Enabled="False">
                                </asp:DropDownList>
                                    <font color="red" style="font-weight: bold">*</font><asp:RequiredFieldValidator ID="partiderr" runat="server" ControlToValidate="DpDpartid" ForeColor="White">*</asp:RequiredFieldValidator></td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" width="15%">
                                    <asp:Label ID="quantitylab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 35%">
                                    <asp:TextBox ID="quantitytbox" runat="server" CssClass="textborder" Style="width: 82%"
                                        Width="136px"></asp:TextBox>
                                    <strong><span style="color: #ff0000">*</span></strong><asp:RangeValidator ID="quantityerr" runat="server" ControlToValidate="quantitytbox" MaximumValue="247678" MinimumValue="0" Type="Integer" ForeColor="White">*</asp:RangeValidator></td>
                                <td align="right" width="15%">
                                    <asp:Label ID="statusLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 35%">
                                    <asp:DropDownList ID="statdrp" runat="server" CssClass="textborder" Style="width: 82%"
                                        Width="144px">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" width="15%">
                                    <asp:Label ID="creatby" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 35%">
                                    <asp:TextBox ID="creatbybox" runat="server" CssClass="textborder" ReadOnly="true"
                                        Style="width: 82%" Width="136px"></asp:TextBox></td>
                                <td align="right" width="15%">
                                    <asp:Label ID="creatdate" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 35%">
                                    <asp:TextBox ID="creatdtbox" runat="server" CssClass="textborder" ReadOnly="true"
                                        Style="width: 82%" Width="136px"></asp:TextBox></td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" width="15%">
                                    <asp:Label ID="modfyby" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 35%">
                                    <asp:TextBox ID="modfybybox" runat="server" CssClass="textborder" ReadOnly="true"
                                        Style="width: 82%" Width="136px"></asp:TextBox></td>
                                <td align="right" width="15%">
                                    <asp:Label ID="mdfydate" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 35%">
                                    <asp:TextBox ID="mdfydtbox" runat="server" CssClass="textborder" ReadOnly="true"
                                        Style="width: 82%"></asp:TextBox></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label1" runat="server" ForeColor="Red"></asp:Label></td>
                </tr>
                <tr>
                    <td style="width: 500px">
                                    <asp:LinkButton ID="saveButton" runat="server"></asp:LinkButton>
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                    <asp:LinkButton ID="cancelLink" runat="server" CausesValidation="False"></asp:LinkButton></td>
                </tr>
            </table>
            &nbsp;
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                ShowSummary="False" />
            <asp:TextBox ID="TextBox1" runat="server" Visible="False"></asp:TextBox>
            <cc1:MessageBox ID="MessageBox1" runat="server" />
        </div>
    
    </div>
    </form>
</body>
</html>
