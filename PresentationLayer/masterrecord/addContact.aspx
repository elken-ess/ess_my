<%@ Page Language="VB" AutoEventWireup="false" EnableEventValidation="false"CodeFile="addContact.aspx.vb" Inherits="PresentationLayer_masterrecord_addContact" %>

<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
      <LINK href="../css/style.css" type="text/css" rel="stylesheet">
      <STYLE type="text/css">A:link { COLOR: #336666; TEXT-DECORATION: none }
	BODY { FONT-SIZE: 9pt; FONT-FAMILY: "Arial", "Verdana", "Tahoma"; BACKGROUND-COLOR: #ffffff }
	TD { FONT-SIZE: 9pt; FONT-FAMILY: Arial,Verdana,Tahoma }
	</STYLE>
</head>
<body>
    <form id="form1" runat="server">
    <div style="width: 100%">
        <table id="countrytab" border="0" style="width: 100%">
            <tr>
                <td>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width: 100%">
                        <tr>
                            <td background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title1.gif" width="5" /></td>
                            <td background="../graph/title_bg.gif" class="style2" width="98%">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="right" background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title_2.gif" width="5" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width: 100%">
                        <tr>
                            <td background="../graph/title_bg.gif" class="style2" width="98%">
                                <font color="red">
                                    <asp:Label ID="errlab" runat="server" Text="Label" Visible="false"></asp:Label></font>
                                    </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 645%">
                    <table id="country" bgcolor="#b7e6e6" border="0" cellpadding="2" cellspacing="1" style="width: 100%">
                    <tr bgcolor="#ffffff">
<td colspan = 4>
                            <asp:Label ID="lblCompTop" runat="server" ForeColor="Red" Text="Label" Visible="True"></asp:Label>
</td>
             </tr>
             <tr bgcolor="#ffffff">
<td colspan = 4>
                        	&nbsp;
                     	</td>
               </tr> 
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%;" width="15%">
                                <asp:Label ID="custidlab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%;">
                                <asp:TextBox ID="custid" runat="server" CssClass="textborder" Enabled="False" MaxLength="2"
                                    Width="86%"></asp:TextBox></td>
                            <td align="right" style="width: 15%">
                                <asp:Label ID="statusLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%;">
                                <asp:DropDownList ID="status" runat="server" CssClass="textborder" Width="92%">
                                </asp:DropDownList></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%;" width="15%">
                                &nbsp;<asp:Label ID="TelepTypelab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%; color: red;">
                                <asp:DropDownList ID="TelepType" runat="server" CssClass="textborder" Width="92%">
                                </asp:DropDownList>
                                <strong>*</strong></td>
                            <td align="right" style="width: 15%">
                                <asp:Label ID="PIClab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%;">
                                <asp:TextBox ID="PIC" runat="server" CssClass="textborder" MaxLength="50" Width="86%"></asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 15%;" width="15%">
                                <asp:Label ID="rotypelab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%;">
                                <asp:DropDownList ID="rotype" runat="server" AutoPostBack="True" CssClass="textborder" Width="92%">
                                </asp:DropDownList></td>
                            <td align="right" style="width: 15%">
                                <asp:Label ID="rouidLab" runat="server" Text="Label" Visible="False"></asp:Label></td>
                            <td align="left" style="width: 35%;">
                                <asp:TextBox ID="rouid" runat="server" CssClass="textborder" Visible="False" Width="86%"></asp:TextBox>
                            </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" width="15%" style="width: 15%">
                                <asp:Label ID="Telephonelab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%; color: red">
                                <asp:TextBox ID="Telephone" runat="server" CssClass="textborder" MaxLength="30" Width="86%"></asp:TextBox>
                                <strong>*</strong><asp:RegularExpressionValidator ID="TelephoneMsg" runat="server" ControlToValidate="Telephone"
                                    ValidationExpression="\d{0,20}" ForeColor="White">*</asp:RegularExpressionValidator><asp:RequiredFieldValidator ID="telnoerr" runat="server" ControlToValidate="Telephone"
                                    ForeColor="White">*</asp:RequiredFieldValidator></td>
                            <td align="right" style="width: 15%">
                                <asp:Label ID="Emaillab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%;">
                                <asp:TextBox ID="Email" runat="server" CssClass="textborder" MaxLength="30" Width="86%"></asp:TextBox><asp:RegularExpressionValidator
                                    ID="Emailmsg" runat="server" ControlToValidate="Email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ForeColor="White">*</asp:RegularExpressionValidator></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" width="15%" style="width: 15%">
                                <asp:Label ID="CreatedBylab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%">
                                <asp:TextBox ID="CreatedBy" runat="server" CssClass="textborder" ReadOnly="true" Width="86%"></asp:TextBox></td>
                            <td align="right" style="width: 15%">
                                &nbsp;<asp:Label ID="CreatedDatelab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%">
                                <asp:TextBox ID="CreatedDate" runat="server" CssClass="textborder" Enabled="False"
                                    ReadOnly="true" Width="86%"></asp:TextBox>
                            </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" width="15%" style="width: 15%">
                                <asp:Label ID="ModiBylab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%">
                                <asp:TextBox ID="ModiBy" runat="server" CssClass="textborder" ReadOnly="true" Width="86%"></asp:TextBox></td>
                            <td align="right" style="width: 15%">
                                <asp:Label ID="ModifiedDatelab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%">
                                <asp:TextBox ID="ModifiedDate" runat="server" CssClass="textborder" Enabled="False"
                                    ReadOnly="true" Width="86%"></asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="left" colspan="4">
                                <asp:GridView ID="contactView" runat="server" AllowPaging="True" AllowSorting="True"
                                    Font-Size="Smaller" PageSize="2" Width="100%">
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                    <asp:RequiredFieldValidator ID="TelepTypemsg" runat="server" ControlToValidate="TelepType"
                        ForeColor="WhiteSmoke">*</asp:RequiredFieldValidator></td>
            </tr>
            <tr>
                <td style="height: 37%">
                <font color=red><asp:Label ID="lblCompBottom" runat="server" Text="Label" Visible="true"></asp:Label></font>
                    <table id="Table1" border="0" cellpadding="1" cellspacing="1">
                        <tr>
                            <td align="center" style="width: 20%">
                                &nbsp;<asp:LinkButton ID="LinkButton1" runat="server">save</asp:LinkButton></td>
                            <td align="center">
                                &nbsp;<asp:LinkButton ID="HyperLink1" runat="server" CausesValidation="False">LinkButton</asp:LinkButton></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <cc1:messagebox id="MessageBox1" runat="server"></cc1:messagebox>
        <br />
        &nbsp;
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
            ShowSummary="False" />
    
    </div>
    </form>
</body>
</html>
