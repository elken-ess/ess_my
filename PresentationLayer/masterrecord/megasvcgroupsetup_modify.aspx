<%@ Page Language="VB" AutoEventWireup="false" EnableEventValidation ="false"  CodeFile="megasvcgroupsetup_modify.aspx.vb" Inherits="PresentationLayer_masterrecord_megasvcgroupsetup_modify" %>

<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>modify country</title>
    <meta http-equiv="Content-Type" content="text/html; charset=gb2312">
	<LINK href="../css/style.css" type="text/css" rel="stylesheet">
    <style type="text/css">
        .auto-style1 {
            height: 45px;
        }
        .auto-style2 {
            width: 35%;
            height: 45px;
        }
    </style>
</head>
<body  id="countrybody">
    <form id="Countryform" runat="server">
     <TABLE border="0" id=countrytab style="width: 100%">
           <tr>
           <td>
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>
							<TD width="1%" background="../graph/title_bg.gif"><IMG height="24" src="../graph/title1.gif" width="5"></TD>
							<TD class="style2" width="98%" background="../graph/title_bg.gif">
                                <asp:Label ID="titleLab" runat="server" Text="Service Internal Setup - Modify"></asp:Label></TD>
							<TD align="right" width="1%" background="../graph/title_bg.gif"><IMG height="24" src="../graph/title_2.gif" width="5"></TD>
						</TR>
			 </TABLE>
           </tr>
           <tr>
           <td>
              <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>					
							<TD class="style2" width="98%" background="../graph/title_bg.gif">
                                <font color=red><asp:Label ID="errlab" runat="server" Text="Label" Visible=false></asp:Label></font></TD>
						</TR>
			 </TABLE>
               <asp:Label ID="Label1" runat="server" ForeColor="Red"></asp:Label>
           </tr>
			<TR>
				<TD>
					<TABLE id="country" cellSpacing="1" cellPadding="2" bgColor="#b7e6e6" border="0" style="width: 100%">
									  <TR bgColor="#ffffff">
											<TD  align=right width="15%">
                                                &nbsp;<asp:Label ID="lblgroup" runat="server" Text="Group"></asp:Label></TD>
											<TD align=left style="width: 35%">
                                                <asp:TextBox ID="txtgroup" runat="server" CssClass="textborder" Width="88%"></asp:TextBox>
                                                <font color=red style="font-weight: bold">*</font>
                                                <asp:RequiredFieldValidator ID="valgroup" runat="server" ControlToValidate="txtgroup" Display="Dynamic" ErrorMessage="cannot be empty!"></asp:RequiredFieldValidator>
                                                </TD>
											<TD  align=right width="15%">
                                                <asp:Label ID="lblmodel" runat="server" Text="Model"></asp:Label></TD>
											<TD  align=left style="width: 35%">
                                                <asp:DropDownList ID="ddlmodel" runat="server" Width="88%">
                                                </asp:DropDownList>
                                                <font color=red style="font-weight: bold">*</font>
                                                </TD>
										</TR>
										<TR bgColor="#ffffff">
											<TD  align=right width="15%">
                                                <asp:Label ID="lblmonths" runat="server" Text="Months"></asp:Label></TD>
											<TD align=left width="85%" colspan=3>
                                                <asp:TextBox ID="txtmonths" runat="server" CssClass="textborder" MaxLength="50" Width="35%"></asp:TextBox>
                                                <font color=red style="font-weight: bold">*</font>
                                                <asp:RequiredFieldValidator ID="valmonths" runat="server" ControlToValidate="txtmonths" Display="Dynamic" ErrorMessage="cannot be empty!"></asp:RequiredFieldValidator>
&nbsp;<em><br />
                                                (Must be numerical)</em></TD>
										</TR>
										<TR bgColor="#ffffff">
											<TD  align=right width="15%" class="auto-style1">
                                                <asp:Label ID="lblinterval" runat="server" Text="Interval"></asp:Label></TD>
											<TD align=left class="auto-style2">
                                                <asp:TextBox ID="txtinterval" runat="server" CssClass="textborder" MaxLength="10" Width="88%"></asp:TextBox>
                                                <font color=red style="font-weight: bold">*</font>
                                                <asp:RequiredFieldValidator ID="valinterval" runat="server" ControlToValidate="txtinterval" Display="Dynamic" ErrorMessage="cannot be empty!"></asp:RequiredFieldValidator>
                                                <em>(Must be numerical)</em></TD>
											<TD align=right width="15%" class="auto-style1">
                                                </TD>
											<TD align=left class="auto-style2">
                                                <asp:TextBox ID="txtid" runat="server" Visible="False"></asp:TextBox>
                                                </TD>
										</TR>
										<TR bgColor="#ffffff">
											<TD align=right width="15%">
                                                <asp:Label ID="lblfrequency" runat="server" Text="Frequency"></asp:Label></TD>
											<TD align=left style="width: 35%">
                                                <asp:TextBox ID="txtfrequency" runat="server" CssClass="textborder" MaxLength="10" Width="88%"></asp:TextBox>
                                                <font color=red style="font-weight: bold">*</font>
                                                <asp:RequiredFieldValidator ID="valfrequency" runat="server" ControlToValidate="txtfrequency" Display="Dynamic" ErrorMessage="cannot be empty!"></asp:RequiredFieldValidator>
                                                <em>(Must be numerical)</em></TD>
											<TD align=right width="15%">
                                                <asp:Label ID="lblstat" runat="server" Text="Status"></asp:Label>
                                            </TD>
											<TD align=left style="width: 35%">
                                                <asp:DropDownList ID="ddlstat" runat="server" Width="88%">
                                                    <asp:ListItem>ACTIVE</asp:ListItem>
                                                    <asp:ListItem>OBSOLETE</asp:ListItem>
                                                </asp:DropDownList>
                                                </TD>
										</TR>
										<TR bgColor="#ffffff">
											<TD align=right width="15%">
                                                <asp:Label ID="lblcreatedby" runat="server" Text="Created By"></asp:Label></TD>
											<TD align=left style="width: 35%">
                                                <asp:TextBox ID="txtcreatedby" runat="server" CssClass="textborder" ReadOnly=true Width="88%"></asp:TextBox></TD>
											<TD align=right width="15%">
                                                <asp:Label ID="lblcreateddate" runat="server" Text="Created Date"></asp:Label></TD>
											<TD align=left style="width: 35%">
                                                <asp:TextBox ID="txtcreateddate" runat="server" CssClass="textborder" ReadOnly=true Width="88%"></asp:TextBox></TD>
										</TR>
										<TR bgColor="#ffffff">
											<TD align=right width="15%">
                                                <asp:Label ID="lblmodifiedby" runat="server" Text="Modified By"></asp:Label></TD>
											<TD align=left style="width: 35%">
                                                <asp:TextBox ID="txtmodifiedby" runat="server" CssClass="textborder" ReadOnly=true Width="88%"></asp:TextBox></TD>
											<TD align=right width="15%">
                                                <asp:Label ID="lblmodifieddate" runat="server" Text="Modified Date"></asp:Label></TD>
											<TD align=left style="width: 35%">
                                                <asp:TextBox ID="txtmodifieddate" runat="server" CssClass="textborder" ReadOnly=true Width="88%"></asp:TextBox></TD>
										</TR>
								</TABLE>
						</TD>
					</TR>
					<tr>
					    <td>
                            <asp:Label ID="Label2" runat="server" ForeColor="Red"></asp:Label></td>
					</tr>
					<TR>
						<TD style="height: 37px">
							<TABLE id="Table1" cellSpacing="1" cellPadding="1"  border="0">
									<TR>
										<TD style="WIDTH: 20%" align=center>
                                            &nbsp;<asp:LinkButton ID="saveButton" runat="server"></asp:LinkButton></TD>
										<td align=center>
                                            <asp:HyperLink ID="cancelLink" runat="server" NavigateUrl="~/PresentationLayer/masterrecord/megasvcgroupsetup.aspx">[cancelLink]</asp:HyperLink></td>
									</TR>
							</TABLE>
						</TD>
					</TR>
			</TABLE>
       <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
           ShowSummary="False" />
       <cc1:messagebox id="MessageBox1" runat="server"></cc1:messagebox>

    </form>
</body>
</html>