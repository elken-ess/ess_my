﻿Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data

Partial Class PresentationLayer_masterrecord_addinstalleBase_aspx
    Inherits System.Web.UI.Page
    Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        ' Page.MaintainScrollPositionOnPostBack = True
        AjaxPro.Utility.RegisterTypeForAjax(GetType(PresentationLayer_masterrecord_addinstalleBase_aspx))

        If Not Page.IsPostBack Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try
            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "05")
            Me.LinkButton1.Enabled = purviewArray(0)
            If purviewArray(0) = False Then
                Response.Redirect("~/PresentationLayer/logon.aspx")
            End If

            '给界面上LABEL的赋值
            serialnotbox.Focus()

            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            modelidlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0001")
            serialnoLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0002")
            customeridlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0003")
            CustomerjdeLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0004")
            Customernamelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0005")
            Contractedlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0006")
            freeserentitlelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0007")
            Installeddatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0008")
            producedatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0009")
            ProductClasslab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0010")
            WarrExpdatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0011")
            TechnicianIDlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0012")
            TechnicianTypelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0013")
            Sercenteridlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0014")
            SerTypeidlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0015")
            FreSerRemDatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0016")
            lastserdatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0017")
            lastremidatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0018")
            Statuslab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0019")
            Remarkslab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0020")
            Createdbylab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATBY")
            createbydatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
            ModifiedBylab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFBY")
            modifybydatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFDATE")
            jderoidlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0023")

            Me.lblCompTop.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            Me.lblCompBottom.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")

            LinkButton1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Save")
            HyperLink1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Cancel")
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-AddInstalltl")
            AddLink.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Add")
            AddLink.NavigateUrl = Page.Request.RawUrl

            'added by deyb 22-Aug
            HypCal.NavigateUrl = "javascript:DoCal(document.Installform.Installeddatetbox);"
            HypCal2.NavigateUrl = "javascript:DoCal(document.Installform.producedatetbox);"
            '===============

            Dim XmlTr As New clsXml
            XmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            modelidRFValid.ErrorMessage = XmlTr.GetLabelName("StatusMessage", "BB-HintMsg-MID")
            serialidRFValid.ErrorMessage = XmlTr.GetLabelName("StatusMessage", "BB-HintMsg-SEID")
            cusiderr.ErrorMessage = XmlTr.GetLabelName("StatusMessage", "BB-HintMsg-CUSTOMERID")
            installerr.ErrorMessage = XmlTr.GetLabelName("StatusMessage", "BB-HintMsg-INSTALLDATE")
            proerr.ErrorMessage = XmlTr.GetLabelName("StatusMessage", "BB-HintMsg-PRODUCEDATE")
            CompDateVal.ErrorMessage = XmlTr.GetLabelName("StatusMessage", "INVALID_DATE")
            CompDateVal2.ErrorMessage = XmlTr.GetLabelName("StatusMessage", "INVALID_DATE")

            sercenteriderr.ErrorMessage = XmlTr.GetLabelName("StatusMessage", "BB-HintMsg-sercenteriderr")

            'set value for createbydatetbox and modifybydatetbox
            Dim userIDNamestr As String = Session("userID").ToString().ToUpper + "-" + Session("username").ToString.ToUpper
            CreatedBytbox.Text = userIDNamestr
            createbydatetbox.Text = Date.Now()
            ModifiedBytbox.Text = userIDNamestr
            modifybydatetbox.Text = Date.Now()

            'create the status dropdownlist
            Dim cntryStat As New clsrlconfirminf()
            Dim statParam As ArrayList = New ArrayList
            statParam = cntryStat.searchconfirminf("STATUS")
            Dim count As Integer
            Dim statid As String
            Dim statnm As String
            For count = 0 To statParam.Count - 1
                statid = statParam.Item(count)
                statnm = statParam.Item(count + 1)
                count = count + 1
                If statid.Equals("ACTIVE") Or statid.Equals("OBSOLETE") Then
                    statusdrpd.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                End If
            Next
            'create the contracted and free service entitled dropdownlist
            Dim contractpar As ArrayList = New ArrayList
            contractpar = cntryStat.searchconfirminf("YESNO")
            count = 0
            For count = 0 To contractpar.Count - 1
                statid = contractpar.Item(count)
                statnm = contractpar.Item(count + 1)
                count = count + 1
                Contractdrpd.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                freeserentitledrpd.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
            Next

            Contractdrpd.Items(1).Selected = True
            freeserentitledrpd.Items(1).Selected = True

            'create the technician type dropdownlist
            Dim techtypepar As ArrayList = New ArrayList
            techtypepar = cntryStat.searchconfirminf("TECHTYPE")
            count = 0
            For count = 0 To techtypepar.Count - 1
                statid = techtypepar.Item(count)
                statnm = techtypepar.Item(count + 1)
                count = count + 1
                TechnicianTypedrpd.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
            Next
            TechnicianTypedrpd.Items(0).Selected = True

            'create the product class dropdownlist
            Dim productcl As ArrayList = New ArrayList
            productcl = cntryStat.searchconfirminf("PRODUCTCL")
            count = 0
            For count = 0 To productcl.Count - 1
                statid = productcl.Item(count)
                statnm = productcl.Item(count + 1)
                count = count + 1
                productclassdrpd.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
            Next
            productclassdrpd.Items(0).Selected = True

            Dim installbond As New clsCommonClass
            Dim rank As String = Session("login_rank")
            installbond.rank = rank
            If rank <> "0" Then
                installbond.spctr = Session("login_ctryID")
                installbond.spstat = Session("login_cmpID")
                installbond.sparea = Session("login_svcID")
            End If

            ''bond model id and name

            Dim modelds As New DataSet
            modelds = installbond.Getcomidname("BB_MASMOTY_IDNAME")
            If modelds.Tables.Count <> 0 Then
                databonds(modelds, Me.modelidtdrp)
            End If

            ''bond service center id and name
            Dim service As New clsCommonClass
            If rank = "7" Then 'country
                service.spctr = Session("login_ctryID")
            End If
            If rank = "8" Then 'company
                service.spctr = Session("login_ctryID")
                service.spstat = Session("login_cmpID")
            End If
            If rank = "9" Then 'company
                service.spctr = Session("login_ctryID")
                service.spstat = Session("login_cmpID")
                service.sparea = Session("login_svcID")
            End If
            service.rank = rank
            Dim serviceds As New DataSet
            serviceds = service.Getcomidname("BB_MASSVRC_IDNAME")
            If serviceds.Tables.Count <> 0 Then
                databonds(serviceds, Me.Sercenteriddrpd)
            End If
            Sercenteriddrpd.SelectedValue = Session("login_svcID")



            'bond  service type id and name
            Dim sertype As New DataSet
            sertype = installbond.Getcomidname("BB_MASSRCT_IDNAME")
            If sertype.Tables.Count <> 0 Then
                databonds(sertype, SerTypedrpd)
            End If
            SerTypedrpd.SelectedIndex = 0

            cusid.Text = Request.QueryString("custid")
            Customerjdetbox.Text = Request.QueryString("custpre")
            Customernametbox.Text = Request.QueryString("custname")


            Dim lstrTechnicianID As String = ""
            Dim type As String = Request.QueryString("type")
            If Trim(type) = "add" Then
                cusid.Text = Request.QueryString("searchcustid")
                Customerjdetbox.Text = Request.QueryString("searchcustjde")
                Customernametbox.Text = Request.QueryString("searchcustname")
                modelidtdrp.Text = Request.QueryString("searchmodelid")
                serialnotbox.Text = Request.QueryString("searchserialno")
                Contractdrpd.Text = Request.QueryString("searchContract")
                Installeddatetbox.Text = Request.QueryString("searchInstalleddate")
                producedatetbox.Text = Request.QueryString("searchproducedate")
                lstrTechnicianID = Request.QueryString("searchTechnicianid")
                freeserentitledrpd.Text = Request.QueryString("searchfreeserentitle")
                productclassdrpd.Text = Request.QueryString("searchproductclass")
                TechnicianTypedrpd.Text = Request.QueryString("searchTechnicianType")
                Sercenteriddrpd.Text = Request.QueryString("searchSercenterid")
                SerTypedrpd.Text = Request.QueryString("searchSerType")
                WarrExpdatetbox.Text = Request.QueryString("searchWarrExpdate")
                FrSRemDtbox.Text = Request.QueryString("searchFrSRemDt")
                statusdrpd.Text = Request.QueryString("searchstatus")
                Remarkstbox.Text = Request.QueryString("searchRemarks")
            Else
                Installeddatetbox.Text = Request.Form("Installeddatetbox")
                producedatetbox.Text = Request.Form("producedatetbox")
            End If

            Dim tech As New DataSet
            installbond.sparea = Sercenteriddrpd.SelectedItem.Value.ToString()
            installbond.spctr = Session("login_ctryID")
            installbond.spstat = Session("login_cmpID")
            installbond.rank = Session("login_rank")

            tech = installbond.Getcomidname("BB_MASTECH_NAMEID")
            If tech.Tables.Count <> 0 Then
                databondsNameID(tech, Technicianiddrp)
            End If

            If lstrTechnicianID <> "" Then
                Technicianiddrp.SelectedValue = lstrTechnicianID
            End If
        End If



    End Sub



    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton1.Click
        RequestFromValue()
        VerifyRemWarrDate()

        If Me.FrSRemDtbox.Text.Trim = "" Then
            ChangeServiceType()
        End If

        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")

        Dim temparr As Array = Request.Form("producedatetbox").Split("/") 'producedatetbox.Text.Split("/")
        Dim strpro As String
        Dim strinst As String
        strpro = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
        temparr = Installeddatetbox.Text.Split("/")
        strinst = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
        If Convert.ToDateTime(strinst) < Convert.ToDateTime(strpro) Then
            'errlab.Text = objXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-COMPAREPROINSDATE")
            'errlab.Visible = True
            Dim msg As String = objXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-COMPAREPROINSDATE")
            Dim msgBox As New MessageBox(Me)
            msgBox.Show(msg)

            Return
        End If
        Dim msginfo As String = objXmlTr.GetLabelName("StatusMessage", "SAVEORNOT")
        Dim msgtitle As String = objXmlTr.GetLabelName("StatusMessage", "SAVETITLE")

        SaveRecord()
        'MessageBox1.Confirm(msginfo)
    End Sub

#Region " bonds"
    Public Function databondsNameID(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        Dim row As DataRow
        If ds.Tables(0).Rows.Count > 0 Then
            dropdown.Items.Add(" ")
            For Each row In ds.Tables(0).Rows
                Dim NewItem As New ListItem()
                NewItem.Text = row("name") & "-" & row("id")
                NewItem.Value = row("id")
                dropdown.Items.Add(NewItem)
            Next

        End If
    End Function

    Public Function databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        Dim row As DataRow
        If ds.Tables(0).Rows.Count > 0 Then
            dropdown.Items.Add(" ")
            For Each row In ds.Tables(0).Rows
                Dim NewItem As New ListItem()
                NewItem.Text = row("id") & "-" & row("name")
                NewItem.Value = row("id")
                dropdown.Items.Add(NewItem)
            Next

        End If
    End Function

#End Region
    Protected Sub SerTypedrpd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles SerTypedrpd.SelectedIndexChanged
        'If freeserentitledrpd.SelectedItem.Value.ToString() = "Y" Then
        '    SerTypedrpd.Enabled = True
        '    Dim instaEntity As New ClsInstalledBase()
        '    If Trim(Request.Form("Installeddatetbox")) <> "" And SerTypedrpd.SelectedValue().ToString() <> "" Then
        '        Dim datestyle1 As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        '        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle1
        '        instaEntity.SerTypeID = SerTypedrpd.SelectedItem.Value.ToString()
        '        Dim days As Integer = instaEntity.GetReminderdays()
        '        Dim remdate As DateTime
        '        Dim installdate As Date = Convert.ToDateTime(Request.Form("Installeddatetbox"))
        '        remdate = DateAdd(DateInterval.Day, days, installdate)
        '        FrSRemDtbox.Text = Convert.ToString(remdate).Substring(0, 10)
        '    End If
        'Else
        '    SerTypedrpd.Enabled = False
        '    SerTypedrpd.SelectedValue = " "
        '    FrSRemDtbox.Text = ""
        'End If

        'Installeddatetbox.Text = Request.Form("Installeddatetbox")
        'producedatetbox.Text = Request.Form("producedatetbox")
        'Dim script As String = "self.location='#svcctr';"
        'Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "SvcCtr", script, True)
    End Sub

    Protected Sub freeserentitledrpd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles freeserentitledrpd.SelectedIndexChanged
        ''Send limit info 
        'If freeserentitledrpd.SelectedItem.Value.ToString() = "Y" Then
        '    SerTypedrpd.Enabled = True
        '    SerTypedrpd.SelectedIndex = 1
        '    Dim instaEntity As New ClsInstalledBase()
        '    If Trim(Request.Form("Installeddatetbox")) <> "" And SerTypedrpd.SelectedValue().ToString() <> "" Then
        '        Dim datestyle1 As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        '        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle1
        '        instaEntity.SerTypeID = SerTypedrpd.SelectedItem.Value.ToString()
        '        instaEntity.logctrid = Session("login_ctryid")
        '        Dim days As Integer = instaEntity.GetReminderdays()
        '        Dim remdate As DateTime
        '        Dim installdate As Date = Convert.ToDateTime(Request.Form("Installeddatetbox"))
        '        remdate = DateAdd(DateInterval.Day, days, installdate)
        '        FrSRemDtbox.Text = Convert.ToString(remdate).Substring(0, 10)
        '    End If

        'Else
        '    SerTypedrpd.Enabled = False
        '    SerTypedrpd.SelectedValue = " "
        '    FrSRemDtbox.Text = ""
        '    Installeddatetbox.Text = Request.Form("Installeddatetbox")
        '    producedatetbox.Text = Request.Form("producedatetbox")
        'End If
        ''Dim script As String = "self.location='#svcctr';"
        ''Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "svcctr", script, True)

    End Sub


    Sub SaveRecord()
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")

        Dim installbaseEntity As New ClsInstalledBase()

        'added by deyb 22-Aug
        VerifyRemWarrDate()
        '==========
        If (Trim(modelidtdrp.SelectedValue().ToString()) <> "") Then
            installbaseEntity.ModelID = modelidtdrp.SelectedItem.Value.ToString()
        End If
        If (Trim(serialnotbox.Text) <> "") Then
            installbaseEntity.SerialNo = serialnotbox.Text.ToUpper()
        End If
        If (Trim(cusid.Text) <> "") Then
            installbaseEntity.CustomerID = cusid.Text.ToUpper
        End If
        If (Trim(Customerjdetbox.Text) <> "") Then
            installbaseEntity.CustomerPre = Customerjdetbox.Text.ToString()
        End If
        If (Trim(Contractdrpd.SelectedValue().ToString()) <> "") Then
            installbaseEntity.Contracted = Contractdrpd.SelectedItem.Value.ToString()
        End If
        If (Trim(Request.Form("Installeddatetbox")) <> "") Then
            Dim temparr As Array = Installeddatetbox.Text.Split("/")
            installbaseEntity.Installdate = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
        End If
        If (Trim(Request.Form("producedatetbox")) <> "") Then
            Dim temparr As Array = producedatetbox.Text.Split("/")
            installbaseEntity.ProductionDate = temparr(2) + "-" + temparr(1) + "-" + temparr(0)

        End If

        If (Trim(Session("LOGIN_CTRYID")) <> "") Then
            installbaseEntity.CustomerPre = Session("LOGIN_CTRYID")
        End If

        If (Technicianiddrp.SelectedValue().ToString() <> "") Then
            installbaseEntity.TechID = Technicianiddrp.SelectedItem.Value.ToString()
        End If
        If (freeserentitledrpd.SelectedValue().ToString() <> "") Then
            installbaseEntity.Freeserviceen = freeserentitledrpd.SelectedItem.Value.ToString()
        End If
        If (productclassdrpd.SelectedValue().ToString() <> "") Then
            installbaseEntity.ProductClass = productclassdrpd.SelectedItem.Value.ToString()
        End If
        If (TechnicianTypedrpd.SelectedValue().ToString() <> "") Then
            installbaseEntity.TechType = TechnicianTypedrpd.SelectedItem.Value.ToString()
        End If
        If (Sercenteriddrpd.SelectedValue().ToString() <> "") Then
            installbaseEntity.SerCenterID = Sercenteriddrpd.SelectedItem.Value.ToString()
        End If
        If (SerTypedrpd.SelectedValue().ToString() <> "") Then
            installbaseEntity.SerTypeID = SerTypedrpd.SelectedItem.Value.ToString()
        End If
        If (Trim(WarrExpdatetbox.Text) <> "") Then
            Dim temparr As Array = WarrExpdatetbox.Text.Split("/")
            installbaseEntity.WarrantyExDate = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
        End If
        If (FrSRemDtbox.Text <> "") Then
            Dim temparr As Array = FrSRemDtbox.Text.Split("/")
            installbaseEntity.FrSerRemiDate = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
        End If
        If (lastserdatetbox.Text <> "") Then
            Dim temparr As Array = lastserdatetbox.Text.Split("/")
            installbaseEntity.LastSerDate = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
        End If
        If (lastremidatetbox.Text <> "") Then
            Dim temparr As Array = lastremidatetbox.Text.Split("/")
            installbaseEntity.LastRemiDate = temparr(2) + "-" + temparr(1) + "-" + temparr(0)

        End If
        If (statusdrpd.SelectedValue().ToString() <> "") Then
            installbaseEntity.Status = statusdrpd.SelectedItem.Value.ToString()
        End If
        If (Trim(Remarkstbox.Text) <> "") Then
            installbaseEntity.Remarks = Remarkstbox.Text.ToUpper()
        End If
        If (Trim(CreatedBytbox.Text) <> "") Then
            installbaseEntity.CreatedBy = Session("userID").ToString.ToUpper()
        End If
        If (Trim(ModifiedBytbox.Text) <> "") Then
            installbaseEntity.ModifiedBy = Session("userID").ToString.ToUpper()
        End If
        'set default value
        installbaseEntity.relfg = "N"
        installbaseEntity.carem = " "
        Dim DATEARR As Array = TextBox1.Text.Split("/")
        installbaseEntity.ncadate = DATEARR(2) + "-" + DATEARR(1) + "-" + DATEARR(0)
        installbaseEntity.nsvdate = DATEARR(2) + "-" + DATEARR(1) + "-" + DATEARR(0)

        If (freeserentitledrpd.SelectedValue().ToString().Trim = "Y" And SerTypedrpd.SelectedItem.Text.Trim = "") Then
            Dim msg As String = objXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-freeserTypeerr")
            Dim msgBox As New MessageBox(Me)
            msgBox.Show(msg)
            Return
        End If

        If Format(Request.Form("Installeddatetbox"), "dd/MM/yyyy") > Format(Request.Form("producedatetbox"), "dd/MM/yyyy") Then

            errlab.Text = objXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-COMPAREPROINSDATE")
            errlab.Visible = True
            MessageBox1.Alert(errlab.Text)
        Else
            Dim maxid As Long
            Dim calendatetime As New clsCommonClass()
            maxid = installbaseEntity.selmaxid()
            maxid = maxid + 1
            installbaseEntity.RoID = maxid
            Dim updinstallbase As Integer = installbaseEntity.UpdatePfx()


            Dim dupRoCount As Integer = installbaseEntity.GetDuplicatedRo()
            If dupRoCount.Equals(-1) Then
                errlab.Text = objXmlTr.GetLabelName("StatusMessage", "DUPINSTALLROID")
                errlab.Visible = True
                MessageBox1.Alert(errlab.Text)
            ElseIf dupRoCount.Equals(0) Then
                Dim dupCount As Integer = installbaseEntity.GetDuplicatedinstall()
                If dupCount.Equals(-1) Then
                    errlab.Text = objXmlTr.GetLabelName("StatusMessage", "DUPINSTALLIDORNM")
                    errlab.Visible = True
                    MessageBox1.Alert(errlab.Text)
                ElseIf dupCount.Equals(0) Then
                    Dim insInatallCnt As Integer = installbaseEntity.Insert()

                    objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    If insInatallCnt = 0 Then
                        errlab.Text = objXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Suc")
                        errlab.Visible = True
                        MessageBox1.Alert(errlab.Text)
                        LinkButton1.Enabled = False
                    Else
                        Dim msg As String = objXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Fail")
                        Dim msgBox As New MessageBox(Me)
                        msgBox.Show(msg)
                        Response.Redirect("~/PresentationLayer/Error.aspx")
                    End If
                End If

            End If
        End If
        Installeddatetbox.Text = Request.Form("Installeddatetbox")
        producedatetbox.Text = Request.Form("producedatetbox")
    End Sub

    Protected Sub MessageBox1_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox1.GetMessageBoxResponse

        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
            SaveRecord()


        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
        End If

    End Sub


    Protected Sub LinkButton2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton2.Click
        RequestFromValue()

        Dim svcid As String = Session("login_svcID")
        svcid = Server.UrlEncode(svcid)
        Dim countryid As String = Session("login_ctryID")
        countryid = Server.UrlEncode(countryid)
        Dim cmpid As String = Session("login_cmpID")
        cmpid = Server.UrlEncode(cmpid)
        Dim rank As String = Session("login_rank")
        rank = Server.UrlEncode(rank)
        Dim type As String = "add"
        type = Server.UrlEncode(type)
        Dim searchmodelid As String = Trim(modelidtdrp.SelectedValue().ToString())
        searchmodelid = Server.UrlEncode(searchmodelid)
        Dim searchserialno As String = Trim(serialnotbox.Text)
        searchserialno = Server.UrlEncode(searchserialno)
        Dim searchContract As String = Trim(Contractdrpd.SelectedValue().ToString())
        searchContract = Server.UrlEncode(searchContract)
        Dim searchInstalleddate As String = Trim(Request.Form("Installeddatetbox"))
        searchInstalleddate = Server.UrlEncode(searchInstalleddate)
        Dim searchproducedate As String = Trim(Request.Form("producedatetbox"))
        searchproducedate = Server.UrlEncode(searchproducedate)
        Dim searchTechnicianid As String = Technicianiddrp.SelectedValue().ToString()
        searchTechnicianid = Server.UrlEncode(searchTechnicianid)
        Dim searchfreeserentitle As String = freeserentitledrpd.SelectedValue().ToString()
        searchfreeserentitle = Server.UrlEncode(searchfreeserentitle)
        Dim searchproductclass As String = productclassdrpd.SelectedValue().ToString()
        searchproductclass = Server.UrlEncode(searchproductclass)
        Dim searchTechnicianType As String = TechnicianTypedrpd.SelectedValue().ToString()
        searchTechnicianType = Server.UrlEncode(searchTechnicianType)
        Dim searchSercenterid As String = Sercenteriddrpd.SelectedValue().ToString()
        searchSercenterid = Server.UrlEncode(searchSercenterid)
        Dim searchSerType As String = SerTypedrpd.SelectedValue().ToString()
        searchSerType = Server.UrlEncode(searchSerType)
        Dim searchWarrExpdate As String = Trim(WarrExpdatetbox.Text)
        searchWarrExpdate = Server.UrlEncode(searchWarrExpdate)
        Dim searchFrSRemDt As String = Trim(FrSRemDtbox.Text)
        searchFrSRemDt = Server.UrlEncode(searchFrSRemDt)
        Dim searchstatus As String = statusdrpd.SelectedValue().ToString()
        searchstatus = Server.UrlEncode(searchstatus)
        Dim searchRemarks As String = Trim(Remarkstbox.Text)
        searchRemarks = Server.UrlEncode(searchRemarks)

        Dim strTempURL As String = "svcid=" + svcid + "&countryid=" + countryid + "&cmpid=" + cmpid + "&rank=" + rank + "&type=" + type + "&searchmodelid=" + searchmodelid + "&searchserialno=" + searchserialno + "&searchContract=" + searchContract + "&searchInstalleddate=" + searchInstalleddate + "&searchproducedate=" + searchproducedate + "&searchTechnicianid=" + searchTechnicianid + "&searchfreeserentitle=" + searchfreeserentitle + "&searchproductclass=" + searchproductclass + "&searchTechnicianType=" + searchTechnicianType + "&searchSercenterid=" + searchSercenterid + "&searchSerType=" + searchSerType + "&searchWarrExpdate=" + searchWarrExpdate + "&searchFrSRemDt=" + searchFrSRemDt + "&searchstatus=" + searchstatus + "&searchRemarks=" + searchRemarks
        strTempURL = "~/PresentationLayer/masterrecord/Instsearchcustomer.aspx?" + strTempURL
        Response.Redirect(strTempURL)
    End Sub

    Protected Sub Sercenteriddrpd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Sercenteriddrpd.SelectedIndexChanged
        ''bond  technician id and name

        'Dim tech As New DataSet
        'Dim installbond As New clsCommonClass
        'installbond.sparea = Sercenteriddrpd.SelectedItem.Value.ToString()
        'installbond.spctr = Session("login_ctryID")
        'installbond.spstat = Session("login_cmpID")
        'installbond.rank = Session("login_rank")

        'tech = installbond.Getcomidname("BB_MASTECH_IDNAME")
        'If tech.Tables.Count <> 0 Then
        '    databonds(tech, Technicianiddrp)
        'End If

        'Installeddatetbox.Text = Request.Form("Installeddatetbox")
        'producedatetbox.Text = Request.Form("producedatetbox")
        '''''''''''''''''''
        ''Dim script As String = "self.location='#svcctr';"
        ''Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "svcctr", script, True)

    End Sub

    Protected Sub JCalendar1_CalendarVisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles JCalendar1.CalendarVisibleChanged
        'If JCalendar1.CalendarVisible = False Then
        productclassdrpd.Visible = Not JCalendar1.CalendarVisible
        Sercenteriddrpd.Visible = Not JCalendar1.CalendarVisible
        'Else
        'productclassdrpd.Visible = False
        'Sercenteriddrpd.Visible = False

        'End If

    End Sub


    Protected Sub JCalendar1_SelectedDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles JCalendar1.SelectedDateChanged
        Dim installEntity As New ClsInstalledBase()
        If (Trim(modelidtdrp.SelectedValue().ToString()) <> "") Then
            installEntity.ModelID = modelidtdrp.SelectedItem.Value.ToString()
        End If
        Dim days As Integer = installEntity.GetWardays()
        Dim warrdate As DateTime

        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        'Installeddatetbox.Text = Calendarinstall.SelectedDate.Date.ToString().Substring(0, 10)
        Dim installdate As Date = Convert.ToDateTime(Installeddatetbox.Text)
        'warrdate = DateAdd(DateInterval.Day, days, Calendarinstall.SelectedDate.Date)
        warrdate = DateAdd(DateInterval.Day, days, installdate)
        WarrExpdatetbox.Text = Convert.ToString(warrdate).Substring(0, 10)

        'SET MROU_NCADT
        TextBox1.Text = DateAdd(DateInterval.Day, 240, installdate)

        If freeserentitledrpd.SelectedItem.Value.ToString() = "Y" Then
            SerTypedrpd.Enabled = True
            If Trim(Installeddatetbox.Text) <> "" And SerTypedrpd.SelectedValue().ToString() <> "" Then
                installEntity.SerTypeID = SerTypedrpd.SelectedItem.Value.ToString()
                Dim REdays As Integer = installEntity.GetReminderdays()
                Dim remdate As DateTime
                remdate = DateAdd(DateInterval.Day, REdays, installdate)
                FrSRemDtbox.Text = Convert.ToString(remdate).Substring(0, 10)
            End If
        Else
            SerTypedrpd.Enabled = False
            SerTypedrpd.SelectedValue = " "
            FrSRemDtbox.Text = ""
        End If
        'Dim script As String = "self.location='#svcctr';"
        'Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "svcctr", script, True)
    End Sub

    Protected Sub JCalendar2_CalendarVisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles JCalendar2.CalendarVisibleChanged

        Me.Technicianiddrp.Visible = Not JCalendar2.CalendarVisible
        Me.TechnicianTypedrpd.Visible = Not JCalendar2.CalendarVisible
        Me.SerTypedrpd.Visible = Not JCalendar2.CalendarVisible

    End Sub

    Protected Sub JCalendar2_SelectedDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles JCalendar2.SelectedDateChanged
        'Dim script As String = "self.location='#svcctr';"
        'Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "svcctr", script, True)
    End Sub

    Protected Sub Installeddatetbox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Installeddatetbox.TextChanged
        VerifyRemWarrDate()
    End Sub

    Sub VerifyRemWarrDate()
        Dim installEntity As New ClsInstalledBase()
        If (Trim(modelidtdrp.SelectedValue().ToString()) <> "") And Request.Form("Installeddatetbox") <> "" Then
            installEntity.ModelID = modelidtdrp.SelectedItem.Value.ToString()
            installEntity.logctrid = Session("login_ctryid")
            Dim days As Integer = installEntity.GetWardays()
            Dim warrdate As DateTime

            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
            'Installeddatetbox.Text = Calendarinstall.SelectedDate.Date.ToString().Substring(0, 10)
            Dim installdate As Date = Convert.ToDateTime(Request.Form("Installeddatetbox"))
            'warrdate = DateAdd(DateInterval.Day, days, Calendarinstall.SelectedDate.Date)
            warrdate = DateAdd(DateInterval.Day, days, installdate)
            WarrExpdatetbox.Text = Convert.ToString(warrdate).Substring(0, 10)
            'SET MROU_NCADT
            TextBox1.Text = DateAdd(DateInterval.Day, 240, installdate)
        End If

        'If freeserentitledrpd.SelectedItem.Value.ToString() = "Y" Then
        '    SerTypedrpd.Enabled = True
        '    SerTypedrpd.SelectedIndex = 1
        '    Dim instaEntity As New ClsInstalledBase()
        '    If Trim(Request.Form("Installeddatetbox")) <> "" And SerTypedrpd.SelectedValue().ToString() <> "" Then
        '        Dim datestyle1 As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        '        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle1
        '        instaEntity.SerTypeID = SerTypedrpd.SelectedItem.Value.ToString()
        '        instaEntity.logctrid = Session("login_ctryid")
        '        Dim Nodays As Integer = instaEntity.GetReminderdays()
        '        Dim remdate As DateTime
        '        Dim installeddate As Date = Convert.ToDateTime(Request.Form("Installeddatetbox"))
        '        remdate = DateAdd(DateInterval.Day, Nodays, installeddate)
        '        FrSRemDtbox.Text = Convert.ToString(remdate).Substring(0, 10)
        '    End If

        'Else
        '    SerTypedrpd.Enabled = False
        '    SerTypedrpd.SelectedValue = " "
        '    FrSRemDtbox.Text = ""
        '    Installeddatetbox.Text = Request.Form("Installeddatetbox")
        '    producedatetbox.Text = Request.Form("producedatetbox")
        'End If

    End Sub

    Sub RequestFromValue()
        Installeddatetbox.Text = Request.Form("Installeddatetbox")
        producedatetbox.Text = Request.Form("producedatetbox")
        FrSRemDtbox.Text = Request.Form("FrSRemDtbox")


        Dim lstrTechnicianID As String = Request.Form("Technicianiddrp")

        GetTechnicianDropdown()

        If lstrTechnicianID <> "" Then
            Me.Technicianiddrp.SelectedValue = lstrTechnicianID
        End If
    End Sub

    Sub GetTechnicianDropdown()

        Dim Technician As New clsCommonClass
        Dim rank As String = Session("login_rank")
        Technician.spctr = Session("login_ctryID")
        Technician.spstat = Session("login_cmpID")
        Technician.sparea = Sercenteriddrpd.SelectedValue

        Technician.rank = rank


        Dim dsTechnician As New DataSet
        Technicianiddrp.Items.Clear()
        dsTechnician = Technician.Getcomidname("BB_MASTECH_NAMEID")
        If dsTechnician.Tables.Count <> 0 Then
            databondsNameID(dsTechnician, Me.Technicianiddrp)
        End If

    End Sub

#Region "Ajax"


    <AjaxPro.AjaxMethod()> _
        Function GetTechnicianList(ByVal CountryID As String, ByVal CompanyID As String, ByVal ServiceCenterID As String, ByVal Rank As String) As ArrayList

        Dim Technician As New clsCommonClass

        Technician.spctr = CountryID
        Technician.spstat = CompanyID
        Technician.sparea = ServiceCenterID
        Technician.rank = Rank


        Dim dsTechnician As New DataSet
        dsTechnician = Technician.Getcomidname("BB_MASTECH_NAMEID")


        Dim strTemp As String
        Dim Listas As New ArrayList(dsTechnician.Tables(0).Rows.Count)
        Dim i As Integer = 0
        Listas.Add("")
        Try
            For i = 0 To dsTechnician.Tables(0).Rows.Count - 1
                strTemp = dsTechnician.Tables(0).Rows(i).Item(0).ToString & ":" & dsTechnician.Tables(0).Rows(i).Item(1).ToString & "-" & dsTechnician.Tables(0).Rows(i).Item(0).ToString
                Listas.Add(strTemp)
            Next
        Catch
        End Try

        Return Listas


    End Function

    Sub ChangeServiceType()
        Me.RequestFromValue()
        Dim lstrInstallDate As String
        Dim FreeServiceID As String
        Dim CountryID, FreeEntitled As String

        FreeEntitled = freeserentitledrpd.SelectedItem.Value.ToString()
        lstrInstallDate = Installeddatetbox.Text

        FreeServiceID = SerTypedrpd.SelectedItem.Value.ToString()
        CountryID = Session("login_ctryID")

        If FreeEntitled = "Y" Then
            SerTypedrpd.Enabled = True
            Dim instaEntity As New ClsInstalledBase()
            If Trim(lstrInstallDate) <> "" And FreeServiceID <> "" Then
                Dim datestyle1 As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
                System.Threading.Thread.CurrentThread.CurrentCulture = datestyle1

                FrSRemDtbox.Text = CalculateServiceReminderDate(CountryID, lstrInstallDate, FreeServiceID, FreeEntitled)
            End If
        Else


            SerTypedrpd.Enabled = False
            SerTypedrpd.SelectedValue = " "
            FrSRemDtbox.Text = ""
        End If
    End Sub


    Function CalculateServiceReminderDate(ByVal CountryID As String, ByVal InstallDate As String, ByVal FreeServiceID As String, ByVal FreeServiceEntitled As String) As String
        Dim FreSerRemDate As String
        FreSerRemDate = ""
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        If FreeServiceEntitled = "Y" Then
            If Trim(InstallDate) <> "" And FreeServiceID.Trim <> "" Then
                Dim instaEntity As New ClsInstalledBase()

                instaEntity.SerTypeID = FreeServiceID
                instaEntity.logctrid = CountryID

                Dim days As Integer = instaEntity.GetReminderdays()

                Dim SS As DateTime
                Dim temparr As Array = InstallDate.Split("/")
                SS = temparr(2) + "-" + temparr(1) + "-" + temparr(0)

                Dim remdate As DateTime
                remdate = DateAdd(DateInterval.Day, days, SS.Date)

                FreSerRemDate = Convert.ToString(remdate).Substring(0, 10)
            End If
        End If
        CalculateServiceReminderDate = FreSerRemDate

    End Function

    <AjaxPro.AjaxMethod()> _
        Function GetReminderDate(ByVal CountryID As String, ByVal InstallDate As String, ByVal FreeServiceID As String, ByVal FreeServiceEntitled As String) As String

        GetReminderDate = CalculateServiceReminderDate(CountryID, InstallDate, FreeServiceID, FreeServiceEntitled)


    End Function

    <AjaxPro.AjaxMethod()> _
    Function GetWarrantyDate(ByVal ModelID As String, ByVal InstallDate As String, ByVal CountryID As String) As String
        Dim warrdate As DateTime
        Dim lstrWarrDate As String = ""

        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        If Not IsDate(InstallDate) Then
            Return ""
        End If

        Dim installEntity As New ClsInstalledBase()
        If (Trim(ModelID) <> "") And InstallDate <> "" Then
            installEntity.ModelID = ModelID
            installEntity.logctrid = CountryID
            Dim days As Integer = installEntity.GetWardays()

            Dim lstrInstallDate As Date = Convert.ToDateTime(InstallDate)

            warrdate = DateAdd(DateInterval.Day, days, lstrInstallDate)
            'WarrExpdatetbox.Text = Convert.ToString(warrdate).Substring(0, 10)
            lstrWarrDate = warrdate
        End If


        GetWarrantyDate = lstrWarrDate
    End Function


#End Region
End Class
