Imports BusinessEntity
Imports System.Configuration
Imports System.Xml
Imports System.Data
Partial Class PresentationLayer_masterrecord_addAddress
    Inherits System.Web.UI.Page

    Dim addressEntity As New clsAddress()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try

        BindGrid()
    End Sub
    Protected Sub BindGrid()
        If Not Page.IsPostBack Then
            Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "04")
            If purviewArray(0) = False Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return

            End If

            Me.custid.Text = Request.Params("custid")
            Dim mdfypref As String = Request.Params("custPf")
            Me.PIC.Text = Request.Params("custname")
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            custidlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0001")
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "ADDADDRESS")
            Me.statusLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCTRY-0004")
            Me.AddressTypelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0016")
            Me.Address1lab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0006")
            Me.Address2lab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0007")
            Me.PIClab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0003")
            POCodelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0008")
            AreaIDlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0009")
            CountryIDlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCTRY-0001")
            StateIDlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0011")
            Me.rouidLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0007")
            Me.rotypelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0006")
            CreatedBylab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATBY")
            CreatedDatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
            ModiBylab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFBY")
            ModifiedDatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFDATE")
            LinkButton1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Save")
            HyperLink1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Cancel")
            Dim statXmlTr As New clsXml
            statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            Me.POCodeMsg1.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-POCodein")
            Me.address1msg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Address1")
            Me.address2msg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Address2")
            Me.countrymsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "MAS-COUNTRY")
            Me.statmsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "MAS-STAT")
            Me.areamsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "MAS-AREA")
            Me.AddressTypemsg.ErrorMessage = statXmlTr.GetLabelName("StatusMessage", "ADDTYPE")

            Me.lblCompTop.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            Me.lblCompBottom.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")

            Dim userIDNamestr As String = Session("userID").ToString().ToUpper + "-" + Session("username").ToString.ToUpper
            Dim rank As String = Session("login_rank")

            Me.CreatedBy.Text = userIDNamestr
            Me.CreatedDate.Text = Date.Now()
            Me.ModiBy.Text = userIDNamestr
            Me.ModifiedDate.Text = Date.Now()
            Dim address As New clsAddress()
            address.username = userIDNamestr
            'create  the dropdownlist
            'create the dropdownlist
            Dim techStat As New clsrlconfirminf()
            Dim statParam As ArrayList = New ArrayList
            statParam = techStat.searchconfirminf("STATUS")
            Dim count As Integer
            Dim statid As String
            Dim statnm As String
            For count = 0 To statParam.Count - 1
                statid = statParam.Item(count)
                statnm = statParam.Item(count + 1)
                count = count + 1
                If statid.Equals("ACTIVE") Or statid.Equals("OBSOLETE") Then
                    Me.status.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                End If
            Next
            'status.Items(0).Selected = True
            'create  BRANCHTYPE the dropdownlist
            addressEntity.CustomerID = custid.Text.ToUpper()
            addressEntity.CustPrefix = mdfypref
            addressEntity.AddressType = "SITE"
            Dim dupCount As Integer = addressEntity.GetDupaddresstype()
            If dupCount.Equals(-1) Then
                Dim Techniciantype As New clsrlconfirminf()
                Dim TechnicianParam As ArrayList = New ArrayList
                TechnicianParam = Techniciantype.searchconfirminf("ADDRESSTY")
                Dim Techniciancount As Integer
                Dim Technicianid As String
                Dim Techniciannm As String
                For Techniciancount = 0 To TechnicianParam.Count - 1
                    Technicianid = TechnicianParam.Item(Techniciancount)
                    Techniciannm = TechnicianParam.Item(Techniciancount + 1)
                    Techniciancount = Techniciancount + 1
                    If Technicianid <> "SITE" Then
                        Me.AddressType.Items.Add(New ListItem(Techniciannm.ToString(), Technicianid.ToString()))
                    End If
                Next
            ElseIf dupCount.Equals(0) Then
                Dim Techniciantype As New clsrlconfirminf()
                Dim TechnicianParam As ArrayList = New ArrayList
                TechnicianParam = Techniciantype.searchconfirminf("ADDRESSTY")
                Dim Techniciancount As Integer
                Dim Technicianid As String
                Dim Techniciannm As String
                For Techniciancount = 0 To TechnicianParam.Count - 1
                    Technicianid = TechnicianParam.Item(Techniciancount)
                    Techniciannm = TechnicianParam.Item(Techniciancount + 1)
                    Techniciancount = Techniciancount + 1
                    If Technicianid = "SITE" Then
                        Me.AddressType.Items.Add(New ListItem(Techniciannm.ToString(), Technicianid.ToString()))
                    End If

                Next
            End If

            'create  country the dropdownlist
            Dim country As New clsCommonClass

            If rank <> 0 Then
                country.spctr = Session("login_ctryID").ToString().ToUpper

            End If
            country.rank = rank

            Dim countryds As New DataSet


            countryds = country.Getcomidname("BB_MASCTRY_IDNAME")
            If countryds.Tables.Count <> 0 Then
                databonds(countryds, Me.CountryID)
            End If
            Me.CountryID.Items.Insert(0, "")
            'create  state the dropdownlist
            Dim stat As New clsCommonClass
            If (CountryID.SelectedValue().ToString() <> "") Then
                stat.spctr = Me.CountryID.SelectedValue().ToString().ToUpper()
            End If

            stat.rank = rank
            Dim stateds As New DataSet

            stateds = stat.Getcomidname("BB_MASSTAT_IDNAME")
            If stateds.Tables.Count <> 0 Then
                databonds(stateds, Me.StateID)
            End If
            Me.StateID.Items.Insert(0, "")
            Dim area As New clsCommonClass


            If (CountryID.SelectedValue().ToString() <> "") Then
                area.spctr = Me.CountryID.SelectedValue().ToString().ToUpper()
            End If

            If (StateID.SelectedValue().ToString() <> "") Then
                area.spstat = Me.StateID.SelectedValue().ToString().ToUpper()
            End If
            area.rank = rank

            'create  area the dropdownlist
            'Dim area As New clsCommonClass
            Dim areads As New DataSet

            areads = area.Getcomidname("BB_MASAREA_IDNAME")
            If areads.Tables.Count <> 0 Then
                databonds(areads, Me.AreaID)
            End If
            Me.AreaID.Items.Insert(0, "")

            Dim rotype As New clsCustomerRecord
            If (Trim(Me.custid.Text) <> "") Then

                rotype.CustomerID = custid.Text.ToUpper()

            End If
            rotype.Customerpf = mdfypref
            Dim rotypeds As New DataSet

            'rotypeds = rotype.GetROidname("BB_MASROUM_MODSER")
            'If rotypeds.Tables.Count <> 0 Then
            '    databondros(rotypeds, Me.rotype)
            'End If
            Me.rotype.Items.Insert(0, "")
            'Me.rotype.Items.Insert(0, "")
            'Dim rotypestr As String
            'rotypestr = Me.rotype.Text
            'Dim ro As Array
            'ro = rotypestr.Split("-")
            'If ro.Length > 1 Then
            '    Dim roid As New clsAddress()
            '    roid.ModelType = ro(0)
            '    roid.SerialNo = ro(1)
            '    'SerialNo.Text = roid.SerialNo
            '    Dim rods As New DataSet
            '    rods = roid.GetROID()
            '    Me.rouid.Text = rods.Tables(0).Rows(0).Item(0).ToString()
            'End If

            If (Trim(Me.custid.Text) <> "") Then
                addressEntity.CustomerID = custid.Text.ToUpper()
            End If
            addressEntity.CustPrefix = mdfypref
            CreateGrid()
            Me.PIC.Focus()
        End If
    End Sub
#Region " id bond"
    Public Sub databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        Dim row As DataRow
        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            NewItem.Text = row("id") & "-" & row("name")
            NewItem.Value = row("id")
            dropdown.Items.Add(NewItem)
        Next

    End Sub
#End Region
#Region " ro bond"
    Public Sub databondros(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        Dim row As DataRow
        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            NewItem.Text = row("id") & "-" & row("name")
            NewItem.Value = row("id") & "-" & row("name")
            dropdown.Items.Add(NewItem)
        Next

    End Sub
#End Region

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton1.Click

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim msginfo As String = objXm.GetLabelName("StatusMessage", "SAVEORNOT")
        Dim msgtitle As String = objXm.GetLabelName("StatusMessage", "POCTITLE")
        MessageBox1.Confirm(msginfo)
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        CreateGrid()

    End Sub

    Protected Sub rotype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rotype.SelectedIndexChanged
        'If Me.rotype.SelectedIndex <> 0 Then
        Dim rotypestr As String
        rotypestr = Me.rotype.Text
        If Me.rotype.Text <> "" Then
            'Dim ro As Array
            'ro = rotypestr.Split("-")
            'Dim roid As New clsAddress()
            'roid.ModelType = ro(0)
            'roid.SerialNo = ro(1)
            'Dim rods As New DataSet
            'rods = roid.GetROID()
            'Me.rouid.Text = rods.Tables(0).Rows(0).Item(0).ToString()
        Else
            Me.rouid.Text = 0
        End If
        GetGrid()
    End Sub

    Protected Sub addressView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles addressView.PageIndexChanging
        Me.addressView.PageIndex = e.NewPageIndex
        GetGrid()
    End Sub

    Protected Sub HyperLink1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HyperLink1.Click
        Dim mdsymbol As String = Request.Params("symbol")
        Dim mdfypref As String = Request.Params("Prefix")
        If mdsymbol = 0 Then
            Dim url As String = "~/PresentationLayer/masterrecord/addcustomer.aspx?"
            url &= "custid=" & Me.custid.Text.ToUpper().ToString & "&"
            url &= "Prefix=" & mdfypref
            Response.Redirect(url)
        End If
        If mdsymbol = 1 Then
            Dim url As String = "~/PresentationLayer/masterrecord/modifycustomer.aspx?"
            url &= "custid=" & Me.custid.Text.ToUpper().ToString & "&"
            url &= "Prefix=" & mdfypref & "&"
            url &= "modisy=" & 1
            Response.Redirect(url)
        End If
    End Sub

    Protected Sub CountryID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CountryID.SelectedIndexChanged
        Me.StateID.Items.Clear()
        Me.AreaID.Items.Clear()
        Dim rank As String = Session("login_rank")
        Dim state As New clsCommonClass

        If (CountryID.SelectedValue().ToString() <> "") Then
            state.spctr = Me.CountryID.SelectedValue().ToString().ToUpper()
        End If
        state.rank = rank

        'Dim state As New clsCommonClass
        Dim stateds As New DataSet

        stateds = state.Getcomidname("BB_MASSTAT_IDNAME")
        If stateds.Tables.Count <> 0 Then
            databonds(stateds, Me.StateID)
        End If
        Me.StateID.Items.Insert(0, "")
        Dim area As New clsCommonClass
        'Dim ctridname As String
        If (CountryID.SelectedValue().ToString() <> "") Then
            area.spctr = Me.CountryID.SelectedValue().ToString().ToUpper()
        End If
        'Dim staid As String
        If (StateID.SelectedValue().ToString() <> "") Then
            area.spstat = Me.StateID.SelectedValue().ToString().ToUpper()
        End If
        area.rank = rank

        'create  area the dropdownlist
        'Dim area As New clsCommonClass
        Dim areads As New DataSet

        areads = area.Getcomidname("BB_MASAREA_IDNAME")
        If areads.Tables.Count <> 0 Then
            databonds(areads, Me.AreaID)
        End If
        Me.AreaID.Items.Insert(0, "")
        GetGrid()
    End Sub

    Protected Sub StateID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles StateID.SelectedIndexChanged
        Me.AreaID.Items.Clear()
        Dim rank As String = Session("login_rank")
        Dim area As New clsCommonClass
        'Dim ctridname As String
        If (CountryID.SelectedValue().ToString() <> "") Then
            area.spctr = Me.CountryID.SelectedValue().ToString().ToUpper()
        End If
        'Dim staid As String
        If (StateID.SelectedValue().ToString() <> "") Then
            area.spstat = Me.StateID.SelectedValue().ToString().ToUpper()
        End If
        area.rank = rank

        'create  area the dropdownlist
        'Dim area As New clsCommonClass
        Dim areads As New DataSet

        areads = area.Getcomidname("BB_MASAREA1_IDNAME")
        If areads.Tables.Count <> 0 Then
            databonds(areads, Me.AreaID)
        End If
        Me.AreaID.Items.Insert(0, "")
        GetGrid()
    End Sub

    Protected Sub MessageBox1_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox1.GetMessageBoxResponse
        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
            Dim mdfypref As String = Request.Params("Prefix")
            If mdfypref Is Nothing Then mdfypref = Session("login_ctryID")
            Dim addressEntity As New clsAddress()

            If (Trim(Me.custid.Text) <> "") Then
                addressEntity.CustomerID = custid.Text.ToUpper()
                addressEntity.CustPrefix = mdfypref.ToUpper()
            End If
            If (Trim(Me.Address1.Text) <> "") Then
                addressEntity.Address1 = Address1.Text.ToUpper()
            End If

            If (Trim(Me.Address2.Text) <> "") Then
                addressEntity.Address2 = Me.Address2.Text.ToString().ToUpper()
            End If

            If (AreaID.SelectedValue().ToString() <> "") Then
                addressEntity.AreaID = Me.AreaID.SelectedValue().ToString().ToUpper()
            End If

            If (CountryID.SelectedValue().ToString() <> "") Then
                addressEntity.CountryID = Me.CountryID.SelectedValue().ToString().ToUpper()
            End If

            If (StateID.SelectedValue().ToString() <> "") Then
                addressEntity.StateID = Me.StateID.SelectedValue().ToString().ToUpper()
            End If

            If (Me.status.SelectedItem.Value.ToString() <> "") Then
                addressEntity.Status = status.SelectedItem.Value.ToString()
            End If

            If (Trim(Me.AddressType.Text) <> "") Then
                addressEntity.AddressType = Me.AddressType.Text.ToString().ToUpper()
            End If

            If (Trim(Me.PIC.Text) <> "") Then
                addressEntity.PICName = Me.PIC.Text.ToString().ToUpper()
            End If
            If (Trim(Me.POCode.Text.ToString()) <> "") Then
                addressEntity.POCode = Me.POCode.Text.ToString().ToUpper()
            End If

            If (Trim(CreatedBy.Text) <> "") Then
                addressEntity.CreatedBy = Session("userID").ToString().ToUpper
            End If
            'If (Trim(CreatedDate.Text) <> "") Then
            '    TechnicianEntity.CreateDate = Me.CreatedDate.Text()
            'End If
            If (Trim(ModiBy.Text) <> "") Then
                addressEntity.ModifiedBy = Session("userID").ToString().ToUpper
            End If
            If (Trim(rouid.Text) <> "") Then
                addressEntity.RouID = Convert.ToInt32(Me.rouid.Text)
                'Else
                '    addressEntity.RouID = Convert.ToInt32("")
            End If
            addressEntity.username = Session("userID")
            Dim dupCount As Integer = addressEntity.GetDuplicateAddress()

            If dupCount.Equals(-1) Then
                Dim objXm As New clsXml
                Dim existCust As String
                Dim errorMsg As String
                objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                existCust = addressEntity.ExistCustID

                errorMsg = objXm.GetLabelName("StatusMessage", "BB-HintMsg-DUPADDR") & " " & existCust & ". "
                errorMsg = errorMsg & objXm.GetLabelName("StatusMessage", "BB-HintMsg-NOTCREATE")

                'errlab.Text = objXm.GetLabelName("StatusMessage", "ADDRESSTYPEFAIL")
                errlab.Text = errorMsg
                errlab.Visible = True
                CreateGrid()
            ElseIf dupCount.Equals(0) Then
                Dim addressdupCount As Integer = addressEntity.GetDupaddress()
                If addressdupCount.Equals(-1) Then
                    Dim objXm As New clsXml
                    objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    errlab.Text = objXm.GetLabelName("StatusMessage", "ADDRESSFAIL")
                    errlab.Visible = True
                    CreateGrid()
                ElseIf addressdupCount.Equals(0) Then
                    Dim insCtryCnt As Integer = addressEntity.Insert()
                    Dim objXmlTr As New clsXml
                    objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    If insCtryCnt = 0 Then
                        ' Dim msg As String = objXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Suc")
                        ' Dim msgBox As New MessageBox(Me)
                        ' msgBox.Show(msg)     'this is show a messagebox to tell us success
                        'Response.Redirect("country.aspx")
                        Dim objXm As New clsXml
                        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                        errlab.Text = objXm.GetLabelName("StatusMessage", "BB-HintMsg-Suc")
                        errlab.Visible = True
                        'Dim addressEntity As New clsAddress()

                        CreateGrid()

                        Dim Techniciantype As New clsrlconfirminf()
                        Dim TechnicianParam As ArrayList = New ArrayList
                        TechnicianParam = Techniciantype.searchconfirminf("ADDRESSTY")
                        Dim Techniciancount As Integer
                        Dim Technicianid As String
                        Dim Techniciannm As String
                        For Techniciancount = 0 To TechnicianParam.Count - 1
                            Technicianid = TechnicianParam.Item(Techniciancount)
                            Techniciannm = TechnicianParam.Item(Techniciancount + 1)
                            Techniciancount = Techniciancount + 1
                            If Technicianid <> "SITE" Then
                                Me.AddressType.Items.Add(New ListItem(Techniciannm.ToString(), Technicianid.ToString()))
                            End If
                        Next
                    End If
                Else
                    'Dim msg As String = objXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Fail")
                    'Dim msgBox As New MessageBox(Me)
                    'msgBox.Show(msg)
                    Response.Redirect("~/PresentationLayer/Error.aspx")
                End If
            End If
        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
        End If

    End Sub

    Protected Sub MessageBox2_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox2.GetMessageBoxResponse
        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim objXm As New clsXml
            objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            Dim msginfo As String = objXm.GetLabelName("StatusMessage", "SAVEORNOT")
            Dim msgtitle As String = objXm.GetLabelName("StatusMessage", "SAVETITLE")
            MessageBox1.Confirm(msginfo)
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            CreateGrid()

        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
        
        End If
    End Sub

    Private Function CreateGrid()
        Dim addressall As DataSet = addressEntity.GetAddress()
        'If addressall.Tables(0).Rows.Count <> 0 Then
        Me.addressView.DataSource = addressall
        Session("addressView") = addressall
        addressView.AllowPaging = True
        addressView.AllowSorting = True
        addressView.DataBind()
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        If addressall.Tables(0).Rows.Count >= 1 Then
            addressView.HeaderRow.Cells(0).Text = objXmlTr.GetLabelName("EngLabelMsg", "ADDRESSTYPE")
            addressView.HeaderRow.Cells(0).Font.Size = 8
            addressView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "STATUS")
            addressView.HeaderRow.Cells(1).Font.Size = 8
            addressView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "ADDRESS1")
            addressView.HeaderRow.Cells(2).Font.Size = 8
            addressView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "ADDRESS2")
            addressView.HeaderRow.Cells(3).Font.Size = 8
            addressView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "POCODE")
            addressView.HeaderRow.Cells(4).Font.Size = 8
        End If
    End Function

    Private Function GetGrid()
        Dim addressall As DataSet = Session("addressView")
        addressView.DataSource = addressall
        addressView.DataBind()
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        If addressall.Tables(0).Rows.Count >= 1 Then
            addressView.HeaderRow.Cells(0).Text = objXmlTr.GetLabelName("EngLabelMsg", "ADDRESSTYPE")
            addressView.HeaderRow.Cells(0).Font.Size = 8
            addressView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "STATUS")
            addressView.HeaderRow.Cells(1).Font.Size = 8
            addressView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "ADDRESS1")
            addressView.HeaderRow.Cells(2).Font.Size = 8
            addressView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "ADDRESS2")
            addressView.HeaderRow.Cells(3).Font.Size = 8
            addressView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "POCODE")
            addressView.HeaderRow.Cells(4).Font.Size = 8
        End If
    End Function
End Class
