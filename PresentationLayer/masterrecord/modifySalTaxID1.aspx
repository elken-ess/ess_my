<%@ Page Language="VB" AutoEventWireup="false" EnableEventValidation="false" CodeFile="modifySalTaxID1.aspx.vb" Inherits="PresentationLayer_masterrecord_modifySalTaxID1" %>

<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <LINK href="../css/style.css" type="text/css" rel="stylesheet">
</head>
<body>
    <form id="form1" runat="server">
    
        <table id="PriceSetUp1tab" border="0" style="width: 100%" >
            <tr>
                <td  >
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title1.gif" width="5" /></td>
                            <td background="../graph/title_bg.gif" class="style2" width="98%">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="right" background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title_2.gif" width="5" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td  >
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" class="style2"  width="98%">
                                <font color="red">
                                    <asp:Label ID="errlab" runat="server" Text="Label" Visible="false"></asp:Label></font></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td  >
                    <table id="country" bgcolor="#b7e6e6" border="0" cellpadding="2" cellspacing="1" style="width: 100%">
                    <tr bgcolor="#ffffff">
<td colspan = 4>
                            <asp:Label ID="lblNoteUP" runat="server" ForeColor="Red" Text="Label" Visible="True"></asp:Label>
</td>
             </tr>
             <tr bgcolor="#ffffff">
<td colspan = 4>
                        	&nbsp;
                     	</td>
               </tr>

                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 20%" >
                               <asp:Label ID="perlab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 30%">
                                <asp:TextBox ID="perbox" runat="server" CssClass="textborder" Width="80%" MaxLength="6"></asp:TextBox>
                                <asp:Label ID="Label2" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                <asp:RangeValidator ID="PERVA" runat="server" ControlToValidate="perbox" ForeColor="White"
                                    Type="Double" MaximumValue="99.99" MinimumValue="000.00">*</asp:RangeValidator>
                                <asp:RequiredFieldValidator ID="va" runat="server" ControlToValidate="perbox"
                                    ForeColor="White">*</asp:RequiredFieldValidator></td>
                            <td align="right" width="15%" style="width: 20%">
                                <asp:Label ID="statusLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 30%">
                                <asp:DropDownList ID="statusDrop" runat="server" CssClass="textborder" Width="90%">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" width="15%" style="width: 20%" >
                                <asp:Label ID="effectivelab" runat="server">label</asp:Label></td>
                            <td align=left width="85%" colspan=3 style="width: 80%" >
                                <asp:TextBox ID="effectivebox" runat="server" CssClass="textborder" ReadOnly="True" Enabled="False" Width="62%"
                                   ></asp:TextBox>
                                <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                &nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 20%; height: 31px">
                                <asp:Label ID="createby" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 30%; height: 31px" >
                                <asp:TextBox ID="creatbybox" runat="server" CssClass="textborder" ReadOnly="true" Width="90%"></asp:TextBox></td>
                            <td align="right" style="width: 20%; height: 31px" >
                                <asp:Label ID="creatdate" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 30%; height: 31px" >
                                <asp:TextBox ID="creatdtbox" runat="server" CssClass="textborder" ReadOnly="true" Width="90%" ></asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 20%" >
                                <asp:Label ID="modifyby" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 30%" >
                                <asp:TextBox ID="modfybybox" runat="server" CssClass="textborder" ReadOnly="true" Width="90%"></asp:TextBox></td>
                            <td align="right" style="width: 20%" >
                                <asp:Label ID="mdifydate" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 30%" >
                                <asp:TextBox ID="mdfydtbox" runat="server" CssClass="textborder" ReadOnly="true" Width="90%"></asp:TextBox></td>
                        </tr>
                                     <tr bgcolor="#ffffff">
<td colspan = 4>
                        	&nbsp;
                     	</td>
               </tr>
                        <tr bgcolor="#ffffff">
                        
<td colspan = 4>
                            <asp:Label ID="lblNoteDown" runat="server" ForeColor="Red" Text="Label" Visible="True"></asp:Label>
</td>
             </tr>


                    </table>
                    <asp:TextBox ID="salsetupidbox" runat="server" Visible="False"></asp:TextBox></td>
            </tr>
            <tr>
                <td  >
                    <table id="Table1" border="0" cellpadding="1" cellspacing="1">
                        <tr>
                            <td align="center" style="width: 20%">
                                &nbsp;<asp:LinkButton ID="saveButton" runat="server"></asp:LinkButton></td>
                            <td align="center">
                                <asp:LinkButton ID="cancelLink" runat="server">LinkButton</asp:LinkButton></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <asp:ValidationSummary ID="ERRORMESSAGE" runat="server" ShowMessageBox="True" ShowSummary="False" />
        <cc1:messagebox id="MessageBox1" runat="server"></cc1:messagebox>
    
  
    </form>
</body>
</html>
