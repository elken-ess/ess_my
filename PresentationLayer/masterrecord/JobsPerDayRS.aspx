<%@ Page Language="VB" AutoEventWireup="false" CodeFile="JobsPerDayRS.aspx.vb" Inherits="PresentationLayer_masterrecord_JobsPerDayRS" %>

<%@ Register Assembly="JCalendar" Namespace="JCalendar" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>jobs per day (rental) - search</title>
     <STYLE type="text/css">A:link { COLOR: #336666; TEXT-DECORATION: none }
	BODY { FONT-SIZE: 9pt; FONT-FAMILY: "Arial", "Verdana", "Tahoma"; BACKGROUND-COLOR: #ffffff }
	TD { FONT-SIZE: 9pt; FONT-FAMILY: Arial,Verdana,Tahoma }
	</STYLE>
	<LINK href="../css/style.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../js/common.js"></script>
</head>
<body>
    <form id="form1" runat="server">   
        <table id="TABLE2" border="0" width="100%">
            <tr>
                <td style="width: 100%; height: 13px">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title1.gif" width="5" /></td>
                            <td background="../graph/title_bg.gif" class="style2" width="98%">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="right" background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title_2.gif" width="5" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100%; height: 2px">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" class="style2" style="height: 1px" width="98%">
                                <font color="red">
                                    <asp:Label ID="addinfo" runat="server" Text="Label" Visible="false"></asp:Label></font></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <asp:Label ID="DateLab" runat="server" Text="Label"></asp:Label>&nbsp;
        <asp:TextBox ID="dateBox" runat="server" CssClass="textborder" ReadOnly="True" Width="15%"></asp:TextBox>
        &nbsp;&nbsp;
        <asp:HyperLink ID="HypCalStart" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif" s
                                    ToolTip="Choose a Date">Choose a Date</asp:HyperLink>
        
        <cc1:JCalendar ID="JCalendar1" runat="server" ControlToAssign="dateBox"  Visible =false 
            ImgURL="~/PresentationLayer/graph/calendar.gif" />
        &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;
        <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label>
        &nbsp; &nbsp;<asp:TextBox ID="TextBox1" runat="server" CssClass="textborder" ReadOnly="True"
            Width="15%"></asp:TextBox>&nbsp; &nbsp;
            
            <asp:HyperLink ID="HypCalEnd" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                                    ToolTip="Choose a Date">Choose a Date</asp:HyperLink>
            
            <cc1:JCalendar ID="JCalendar2" runat="server" ControlToAssign="TextBox1" Visible =false 
            ImgURL="~/PresentationLayer/graph/calendar.gif" />
        &nbsp; &nbsp;&nbsp; &nbsp;<asp:Label ID="SVClab" runat="server" Text="Label"></asp:Label>
        &nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="SvcBox" runat="server" CssClass="textborder"
            Width="19%" MaxLength="10"></asp:TextBox>
        &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
        &nbsp;<asp:LinkButton ID="LinkButton2" runat="server">LinkButton</asp:LinkButton>
        &nbsp;&nbsp; &nbsp;<asp:LinkButton ID="LinkButton1" runat="server"  PostBackUrl="~/PresentationLayer/masterrecord/AddJobsPerDayRS.aspx">LinkButton</asp:LinkButton><br />
        <hr />
        <asp:GridView ID="jobView" runat="server" Width="100%" AllowPaging="True" Font-Size="Smaller">            
        </asp:GridView>
        &nbsp; &nbsp;
        &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;
        &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;
        &nbsp; &nbsp; &nbsp; &nbsp;<br />
        &nbsp;
        <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="Label"></asp:Label>
        
    </form>
</body>
</html>
