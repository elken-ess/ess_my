<%@ Page Language="VB" AutoEventWireup="false" CodeFile="logon.aspx.vb" Inherits="PresentationLayer_masterrecord_logon" %>
<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc1" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Welcome to ELKEN Customer Relations Management System</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body bgcolor="#FFFFFF" background="layout/images/bg.jpg" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="layout/images/top1.jpg" width="760" height="62"></td>
  </tr>
  <tr>
    <td><img src="layout/images/banner.jpg" width="760" height="179"></td>
  </tr>
</table>
<table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><form id="form1" runat="server">
    <table width="760" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="5"><img src="layout/images/box_top.jpg" width="760" height="47"></td>
        </tr>
      <tr>
        <td width="332" height="39" background="layout/images/left_01.jpg">&nbsp;</td>
        <td width="78"><font color="#1e415e" size="2" face="Arial, Helvetica, sans-serif"><strong>
        <asp:Label ID="UserNameLab" runat="server" Text="User ID" Font-Size="Small"></asp:Label>
         :</strong></font></td>
        <td width="133"><asp:TextBox ID="UserNameBox" runat="server" Width="79%" TabIndex="1"></asp:TextBox>
        </td>
        <td width="86" rowspan="2" align="left" valign="middle"><asp:ImageButton ID="logon" runat="server"  src="layout/images/right_bottom.jpg" width="86" height="58" border="0" usemap="#Map" /></td>
        <td width="131" rowspan="2" align="left" valign="top"><img src="layout/images/right_bottom2.jpg" width="131" height="72"></td>
      </tr>
      <tr>
        <td height="33" align="left" valign="top" background="layout/images/left_02.jpg">&nbsp;</td>
        <td><font color="#1e415e" size="2" face="Arial, Helvetica, sans-serif">
        <strong>
        <asp:Label ID="PSWBoxLab" runat="server" Text="Password" Font-Size="Small"></asp:Label>
            :</strong></font></td>
        <td>
        <asp:TextBox ID="PSWBox" runat="server" TextMode="Password" Width="81%" TabIndex="2"></asp:TextBox>
        </td>
      </tr>
      <tr valign="top">
        <td height="25" colspan="5" align="right"><table width="760" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td background="layout/images/box_bg.jpg">
            <table width="760" align=center  border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="168" align =right >
                
                </td>
                <td width="244"><asp:Label ID="addinfo" runat="server" Text="Message" Visible=false   ForeColor="Red" Height="27px" Width="100%" Font-Size="Smaller"></asp:Label>
                </td>
                <td width="348" align =center  ><asp:LinkButton ID="mdfy" runat="server" TabIndex="4">Change Password</asp:LinkButton>&nbsp;&nbsp;&nbsp;</td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td align="center" valign="top">
            <img src="layout/images/left_03.jpg" width="760" height="25"></td>
          </tr>
        </table></td>
      </tr>
      <tr valign="top">
        <td colspan="5" align="left" style="height: 25px"><div align="center"><font size="1" face="Arial, Helvetica, sans-serif">@Copyright
            By ELKEN Sdn. Bhd. 2006. All Right Reserved 
            <br />
            V 1.1.0.20191203</font></div></td>
      </tr>
    </table>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
            ShowSummary="False" />
            <cc1:messagebox id="msgLogin" runat="server"/>
    </form></td>
  </tr>
</table>
<map name="Map">
<area shape="rect" coords="5,1,84,52" href="#">
</map>
</body>
</html>
