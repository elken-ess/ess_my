﻿// JScript 文件

function getXMLArray(xmlDoc, name) 
{  var keys = name.split('.'); 
   var node = xmlDoc.documentElement;      // 得到根节点  
   var rtn = new Array();  
   var n = 0;
   for(var i=0; i<keys.length; i++) 
   {    
     var childs = node.childNodes;       // 得到子节点APP2中的appscreen    
     var key = keys[i]; 
     for(var k=0; k<childs.length; k++) 
     {        
      var child = childs[k];   
      if(child.nodeName == key) 
        {     // 判断子节点是否符合  
          if(i == keys.length-1) 
             {          rtn[n] = child;    n++;  } 
          else {    node = child;    break;  } 
         } 
      } 
    }
     return rtn;
   }
/** * 得到由getXMLArray函数得到对象中的值 * @param node 节点对象 * @param name  * @return 返回 String  */
function getValue(node, name) 
{  
  var keys = name.split('.');   
  for(var i=0; i<keys.length; i++)
   {    
     var childs = node.childNodes;       // 得到子节点   
     var key = keys[i]; 
     for(var k=0; k<childs.length; k++) 
       {         
          var child = childs[k];   
          if(child.nodeName == key) 
            {     // 判断子节点是否符合  
                if(child.childNodes.length == 1) 
                {             // 如果没有字节点,返回值  
                  return child.text; 
                } 
                else {                                     // 还有子节点,继续分析   
                       node = child;   
                       break; 
                      }  
              } 
         }  
     }
  return "";
}