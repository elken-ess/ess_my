// global variables & constants
    var defURL = "";    
    var OUTLOOK = new Array();
    var doc = document; 
    var gNotyPADDINGBOTTOM = 0;
    var gNotyPADDINGTOP = gNotyTOP = 0;
    var gNotyPADDINGLEFT = gNotyLEFT = 0;
    var gNotyPADDINGRIGHT = 0;
    var gNotyPADDINGtb = gNotyPADDINGTOP + gNotyPADDINGBOTTOM;
    var gNotyPADDINGlr = gNotyPADDINGLEFT + gNotyPADDINGRIGHT;
    
    var gDarkColor = "#776b7b";
    var gLightColor = "#EBEBEB";
    var gITEMSPACING = defITEMSPACING = 28;
    
    var defICONWIDTH;  
    var defICONHEIGHT = defICONWIDTH;
    
    var gICONWIDTH = defICONWIDTH;		
    var gICONHEIGHT = defICONHEIGHT;	// reset later
    var gICONSRCWIDTH = defICONHEIGHT - 4;
    var gICONSRCHEIGHT = defICONHEIGHT - 4;	
    
    var gARROWWIDTH = 16;
    var gARROWHEIGHT = 16;
    var gBUTTONHEIGHT = 20;
    var gBUTTONMODE = 1;
    var gICONSIZEopt;
    var gMode2v = 0;
    var gCO = 0;	// Current Open
    var gTimerCount = 0;
    
    var gOldObj = null;
    var gArrowObj = null;
    var gActiveNow = false;
    var gMovOutlookX = 1.3;
    var gMovItemStep = 12;
    var gMovCount = 0;
    var gItemWindowHeight;
    
    var gMovButtonStep, gMovButtonX;
    var gMovButtonAcc = 1;
    var gMovButtonIndex = 2;
    var gMovButtonACCX = new Array(1.1, 1.25, 1.5, 2.2, 10);
    var gMovButtonINIT = (gItemWindowHeight > 200) ? new Array(Math.ceil(gItemWindowHeight/50), Math.ceil(gItemWindowHeight/25), 
            Math.ceil(gItemWindowHeight/10), Math.ceil(gItemWindowHeight/5), gItemWindowHeight) : new Array(5, 10, 20, 50, 160);
    var gRegExp = /rect\((\d*)px (\d*)px (\d*)px (\d*)px\)/;
    
    var gCookieExpDate = "Thu, 31-Dec-2020 23:00:00 GMT";
    //var gWinExamTitle = "BlueBerry";
    var gWinExamURL;
    //var gSETUPTOP = 0;
    var gSETUPHEIGHT = 0;
    var gSETUPARR = null; ; 
//===========================================================================
//  Subroutines
//===========================================================================
function createItem(i) {
    var docWidth = doc.body.clientWidth - gNotyPADDINGlr - 2;
	doc.write('<DIV ID="idOLIF' + i + '" style="position:absolute;left:0;top:' + (doc.all["idOLB"+i].style.pixelTop
	    + gBUTTONHEIGHT) + ';width:' + docWidth + ';height:' + (10+Math.floor(OUTLOOK[i].length)*gITEMSPACING) 
	    + ';clip:rect(0 ' + (docWidth-1) + ' 0 0); cursor:default">');
	for(var j=0; j<OUTLOOK[i].length; j++) {	// j = 1,4,7,10...
		var strTitle = OUTLOOK[i][j][5].split("/");
		if (gICONSIZEopt) {
			spanInnerHTML = '><IMG SRC="none.gif" WIDTH="1" HEIGHT="1" align="absmiddle">' + OUTLOOK[i][j][3];
			divTextAlign = "center";
		} else {		    			
			spanInnerHTML = ' STYLE="position:absolute;left:3;">' + Math.ceil(j) + '. ' + OUTLOOK[i][j][3];
			divTextAlign = "left";
		}		
		
		doc.write('<DIV STATUS="' + OUTLOOK[i][j][3] + '" HREF="' + OUTLOOK[i][j][5] + '" TITLE="' + OUTLOOK[i][j][5] 
		    + '" STATUS="' + strTitle[1] + '" CLASS="mOUT" STYLE="position:absolute;left:0;top:' 
		    + (j*(gITEMSPACING)) + ';width:' + docWidth 
		    + 'height:28" NOWRAP" CLASS="mOUT" onmouseover="mOVER(this)" onmouseout="mOUT(this)" onmousedown="mDOWN(this)" onmouseup="mOVER(this)" onclick="mCLICK(this)"><table border=0 cellspacing=0 cellpadding=0 width=100% height=26 CLASS="mOUT" style="font:normal 9pt Arial"><tr><td valign=center><IMG ALIGN="ABSMIDDLE" SRC="' 
		    + OUTLOOK[i][j][4] + '" STYLE="width:16;height:16"' + spanInnerHTML + '</td></tr></table></DIV>');
	}
	doc.write("</DIV>");
	
}
function writeFieldset(Ftitle, Fname, Fsub, Fid, Fchecked, Fpopup, Fvalue) {
	doc.write('<FIELDSET STYLE="border:1px solid #a69ea8" TITLE="' + Fpopup + '"><LEGEND STYLE="color:yellow">' + Ftitle + '</LEGEND>');
	for (i=0; i<Fvalue.length/2; i++) {
		tmpCheck = (eval(Fvalue[2*i]) == Fchecked) ? " checked" : "";
		doc.write('<LABEL FOR="' + Fid + Fvalue[2*i] + '"><INPUT TYPE="radio" NAME="' + Fname + '" ID="' + Fid 
		    + Fvalue[2*i] + '" onclick="' + Fsub + '(this)"' + tmpCheck + '> ' + Fvalue[2*i+1] + '</LABEL><BR>');
	}
	doc.write('</FIELDSET><BR>');
	gSETUPHEIGHT += (31+(Fvalue.length/2)*20);
}
function resetARROWS() {
	objARROWUP.style.pixelTop = doc.all["idOLB"+gCO].style.pixelTop + gBUTTONHEIGHT + 5;
	objARROWDN.style.pixelTop = doc.all["idOLB"+gCO].style.pixelTop + gBUTTONHEIGHT + gItemWindowHeight - (gARROWHEIGHT+5);
	objARROWUP.style.pixelLeft = objARROWDN.style.pixelLeft = (doc.body.clientWidth- gNotyPADDINGlr - 2) - (gARROWWIDTH+5);
	showARROWS();
}
function showARROWS() {
	if (gItemWindowHeight < (gARROWHEIGHT+5)*2) {
		objARROWUP.style.display = "none";
		objARROWDN.style.display = "none";
		return;
	}
	itemField = doc.all["idOLIF"+gCO].style;
	originTop = doc.all["idOLB"+gCO].style.pixelTop + gBUTTONHEIGHT;
	if (itemField.pixelTop < originTop-gMovItemStep) {
		objARROWUP.style.display = "";
	} else {
		objARROWUP.style.display = "none";
		if (gArrowObj != null) amUP(objARROWUP);
	}
	if (itemField.pixelHeight-(originTop-itemField.pixelTop)>gItemWindowHeight) {
		objARROWDN.style.display = "";
	} else {
		objARROWDN.style.display = "none";
		if (gArrowObj != null) amUP(objARROWDN);
	}
}
function mOUT(obj) {
///	if (gActiveNow || event.toElement.className == "mOUT") return;
	if (gActiveNow) return;
	if (gICONSIZEopt) {
		OBJ = (obj.tagName != "SPAN") ? obj : obj.parentElement.parentElement.children[obj.IDX];
		if (OBJ != gOldObj)  OBJ.className = "mOUT";
	} else {
		obj.style.textDecoration = "none";
	}
	window.status = "";
}
function mOVER(obj) {
	if (gActiveNow) return;
	if (gICONSIZEopt) {
		OBJ = (obj.tagName != "SPAN") ? obj : obj.parentElement.parentElement.children[obj.IDX];
		if (OBJ != gOldObj)  OBJ.className = "mOVER";
	} else {
		obj.style.textDecoration = "underline";
		obj.style.cursor = "hand";
	}
	window.status = obj.STATUS;
}
function mDOWN(obj) {
	if (gActiveNow || !gICONSIZEopt) return;
	OBJ = (obj.tagName != "SPAN") ? obj : obj.parentElement.parentElement.children[obj.IDX];
	if (OBJ != gOldObj)  OBJ.className = "mDOWN";
}
function mCLICK(obj) {
	if (gActiveNow) return;
	OBJ = (obj.tagName != "SPAN") ? obj : obj.parentElement.parentElement.children[obj.IDX];
	OBJ.className = "mDOWN";
	if (gOldObj != OBJ && gOldObj != null) gOldObj.className = "mOUT";
	gOldObj = OBJ;

	gWinExamURL = (OBJ.HREF).split("/");
	parent.right.location.replace(defURL + OBJ.HREF);
	gTimerCount = 0;
	setTimeout("setEXAMTITLE()", 50);
	cancelBubbling = true;
}
function setEXAMTITLE() {
}
function amUP(obj) {
	obj.blur();
	obj.className = "amUP";
	gArrowObj = null;
}
function amDOWN(obj) {
	obj.blur();
	obj.className = "amDOWN";
	if (gActiveNow) return;
	gArrowObj = obj;
	gActiveNow = true;
	gMovCount = gITEMSPACING;
	(gArrowObj.id == "idARROWUP") ? movItem(gMovItemStep) : movItem(-gMovItemStep);
	cancelBubbling = true;
}
function movItem(step) {
	doc.all["idOLIF"+gCO].style.pixelTop += step;
	offsetsubwin("idOLIF"+gCO, -step, -step);
	gMovCount -= gMovItemStep;
	if (gMovCount > gMovItemStep) {
		setTimeout("movItem("+step+")", 20);
	} else {
		showARROWS();
		if (gArrowObj != null) {
			gMovCount += gITEMSPACING;
			setTimeout("movItem("+step+")", 20);
		} else {
			doc.all["idOLIF"+gCO].style.pixelTop += (step/Math.abs(step)*(gMovCount));
			offsetsubwin("idOLIF"+gCO , -(step/Math.abs(step)*(gMovCount)), -(step/Math.abs(step)*(gMovCount)));
			gActiveNow = false;
		}
	}
}
function offsetsubwin(obj, OffsetTOP, OffsetBOTTOM) {
	C = doc.all[obj].style.clip.match(gRegExp);
	setsubwin(obj, eval(C[1])+OffsetTOP, eval(C[3])+OffsetBOTTOM);
}
function setsubwin(obj, TOP, BOTTOM) {
	doc.all[obj].style.clip = 'rect(' + TOP + ' ' + (doc.body.clientWidth- gNotyPADDINGlr - 2 - 1) + ' ' + BOTTOM + ' 0)';
}
function OLBCLICK(i) {				// Outlook Button Clicked
	event.srcElement.blur();
	if (gActiveNow || i == gCO) return;
	gActiveNow = true;
	setCookie("CurrentOpen", i);
	gMovButtonStep = (gMovButtonAcc) ? 1 : gMovButtonINIT[gMovButtonIndex];
	gMovButtonX = (gMovButtonAcc) ? gMovButtonACCX[gMovButtonIndex] : 1;
	gMovCount = gItemWindowHeight;
	movButton(i, (doc.all["idOLB"+i].POSITION == "DOWN"));
}
function movButton(I, movingUP) {
	if (movingUP) {
		gMovCount -= Math.floor(gMovButtonStep);
		if (gMovCount < 0) gMovButtonStep += gMovCount;
		step = Math.floor(gMovButtonStep);
		for(i=1; i<=I; i++) {
			if(doc.all["idOLB"+i].POSITION == "DOWN") {
				doc.all["idOLB"+i].style.pixelTop -= step;
				doc.all["idOLIF"+i].style.pixelTop -= step;
			}
		}

		if (gBUTTONMODE == 1) {
			offsetsubwin("idOLIF"+I, 0, step);
			offsetsubwin("idOLIF"+gCO, 0, -step);
			// deal with arrows
			if (objARROWDN.style.display == "" && objARROWDN.style.pixelTop > doc.all["idOLB"+I].style.pixelTop)
				objARROWDN.style.display = "none";
			if (objARROWUP.style.display == "" && objARROWUP.style.pixelTop > doc.all["idOLB"+I].style.pixelTop)
				objARROWUP.style.display = "none";
			//
		} else if (gBUTTONMODE == 2) {
			offsetsubwin("idOLIF"+I, 0, step);
			doc.all["idOLIF"+gCO].style.pixelTop -= step;
			offsetsubwin("idOLIF"+gCO, step, 0);
			// deal with arrows
			objARROWUP.style.pixelTop -= step;
			objARROWDN.style.pixelTop -= step;
			if (objARROWUP.style.display == "" && objARROWUP.style.pixelTop + gARROWHEIGHT > doc.all["idOLB"+I].style.pixelTop)
				objARROWUP.style.display = "none";
			if (objARROWDN.style.display == "" && objARROWDN.style.pixelTop + gARROWHEIGHT > doc.all["idOLB"+I].style.pixelTop)
				objARROWDN.style.display = "none";
			//
		}

		gMovButtonStep *= gMovButtonX;
		if (gMovCount > 0) {
			setTimeout("movButton(" + I + ", true)", 20);
		} else {
			for(i=1; i<=I; i++)
				doc.all["idOLB"+i].POSITION = "UP";
			if (gBUTTONMODE == 2) {
				doc.all["idOLIF"+gCO].style.pixelTop += gItemWindowHeight;
				offsetsubwin("idOLIF"+gCO, -gItemWindowHeight, -gItemWindowHeight);
			}
			gCO = I;
			resetARROWS();
			gActiveNow = false;
		}
	} else {	// movDOWN
		gMovCount -= Math.floor(gMovButtonStep);
		if (this.gMovCount < 0) gMovButtonStep += gMovCount;
		step = Math.floor(this.gMovButtonStep);
		for (i=I+1; i<OUTLOOK.length; i++) {
			if (doc.all["idOLB"+i].POSITION == "UP") {
				doc.all["idOLB"+i].style.pixelTop += step;
				doc.all["idOLIF"+i].style.pixelTop += step;
			}
		}

		if (gBUTTONMODE == 1) {
			offsetsubwin("idOLIF"+I, 0, step);
			offsetsubwin("idOLIF"+gCO, 0, -step);
		} else if (gBUTTONMODE == 2) {
			if (!gMode2v) {
				doc.all["idOLIF"+I].style.pixelTop -= gItemWindowHeight;
				offsetsubwin("idOLIF"+I, gItemWindowHeight, gItemWindowHeight);
				gMode2v = 1;
			}
			doc.all["idOLIF"+I].style.pixelTop += step;
			offsetsubwin("idOLIF"+I, -step, 0);
			offsetsubwin("idOLIF"+gCO, 0, -step);
		}
		// deal with arrows
		objARROWDN.style.pixelTop += step;
		objARROWUP.style.pixelTop += step;
		tmpLINE = (gCO == OUTLOOK.length-1) ? (doc.body.clientHeight - gNotyPADDINGtb - 2) : doc.all["idOLB"+(gCO+1)].style.pixelTop;
		if (objARROWDN.style.display == "" && objARROWDN.style.pixelTop > tmpLINE)
			objARROWDN.style.display = "none";
		if (objARROWUP.style.display == "" && objARROWUP.style.pixelTop > tmpLINE)
			objARROWUP.style.display = "none";
		//

		gMovButtonStep *= gMovButtonX;
		if (gMovCount > 0) {
			setTimeout("movButton(" + I + ", false)", 20);
		} else {
			for(i=I+1; i<OUTLOOK.length; i++)
				doc.all["idOLB"+i].POSITION = "DOWN";
			if (gBUTTONMODE == 2) gMode2v = 0;
			gCO = I;
			resetARROWS();
			gActiveNow = false;
		}
	}
}
function movOUTLOOK() {
	switch (Math.floor(Math.random()*4)) {
		case 0:	mvx = 1;  mvy = 0;  break;
		case 1:	mvx = -1; mvy = 0;  break;
		case 2:	mvx = 0;  mvy = 1;  break;
		case 3:	mvx = 0;  mvy = -1; break;
	}
	// show obj1 & hide obj2
	if (doc.all["idOUTLOOK"].style.display == "none") {
		obj1 = doc.all["idOUTLOOK"].style;
		obj2 = doc.all["idSETUP"].style;
	} else {
		obj1 = doc.all["idSETUP"].style;
		obj2 = doc.all["idOUTLOOK"].style;
	}
	obj1.display = "";
	obj1.pixelLeft = (doc.body.clientWidth- gNotyPADDINGlr - 2) * (-mvx);
	obj1.pixelTop = (doc.body.clientHeight - gNotyPADDINGtb - 2) * (-mvy);
	setTimeout("domovOUTLOOK(obj1,obj2,mvx,mvy)", 1);
}
function domovOUTLOOK(obj1, obj2, mvx, mvy) {
	obj2.pixelLeft += mvx;	obj2.pixelTop += mvy;
	obj1.pixelLeft += mvx;	obj1.pixelTop += mvy;

	if ((mvx && mvx*obj1.pixelLeft < 0) || (mvy && mvy*obj1.pixelTop < 0)) {
		setTimeout("domovOUTLOOK(obj1,obj2," + (mvx*gMovOutlookX) + "," + (mvy*gMovOutlookX) + ")", 20);
	} else {
		obj1.pixelLeft = 0;
		obj1.pixelTop = 0;
		obj2.display = "none";
	}
}
function iconSize(obj) {
	obj.blur();
	tmpA = obj.id.split("ids");
	oldITEMSPACING = gITEMSPACING;
	gICONSIZEopt = eval(tmpA[1]);
	setCookie("gICONSIZEopt", gICONSIZEopt);
	resetIconSize();

	for (i=0; i<OUTLOOK.length; i++) {
		doc.all["idOLIF"+i].style.display = "none";
		doc.all["idOLIF"+i].style.pixelHeight = 10 + Math.floor(OUTLOOK[i].length/3)*gITEMSPACING;
		diffTOP = doc.all["idOLIF"+i].style.pixelTop-(doc.all["idOLB"+i].style.pixelTop+gBUTTONHEIGHT);
		doc.all["idOLIF"+i].style.pixelTop = (doc.all["idOLB"+i].style.pixelTop+gBUTTONHEIGHT) + diffTOP*(gITEMSPACING/oldITEMSPACING);
		offsetsubwin("idOLIF"+i, diffTOP*(1-gITEMSPACING/oldITEMSPACING), diffTOP*(1-gITEMSPACING/oldITEMSPACING));
		resetItem(i);
		doc.all["idOLIF"+i].style.display = "";
		resetARROWS();
	}
}
function resetIconSize() {    
	switch (gICONSIZEopt) {
		case 0:	gITEMSPACING = 20;
			gICONWIDTH = gICONHEIGHT = gICONSRCWIDTH = gICONSRCHEIGHT = 0;
			break;
		case 2:	pixelBig = 28;
			gITEMSPACING = defITEMSPACING + pixelBig;
			gICONWIDTH = defICONWIDTH + pixelBig;
			gICONHEIGHT = defICONHEIGHT + pixelBig;
			gICONSRCWIDTH = gICONWIDTH - 4;
			gICONSRCHEIGHT = gICONHEIGHT - 4;
			break;
		default: //case 1
			gITEMSPACING = defITEMSPACING;
			gICONWIDTH = defICONWIDTH;
			gICONHEIGHT = defICONHEIGHT;
			gICONSRCWIDTH = gICONWIDTH - 4;
			gICONSRCHEIGHT = gICONHEIGHT - 4;
			break;
	}
}
function resetItem(I) {
	obj = doc.all["idOLIF"+I];
	for(j=0; j<Math.floor(OUTLOOK[I].length); j++) {
		obj.children[2*j].style.display = "none";
		obj.children[2*j].children[0].style.display = "none";
		obj.children[2*j+1].style.pixelTop = 10 + j*gITEMSPACING + gICONHEIGHT;
		if (gICONSIZEopt) {
			obj.children[2*j].style.pixelLeft = Math.floor(((doc.body.clientWidth- gNotyPADDINGlr - 2) - gICONWIDTH)/2);
			obj.children[2*j].style.pixelTop = 10 + j*gITEMSPACING;
			obj.children[2*j].style.pixelWidth = gICONWIDTH;
			obj.children[2*j].style.pixelHeight = gICONHEIGHT;
			obj.children[2*j].children[0].style.pixelWidth = gICONSRCWIDTH;
			obj.children[2*j].children[0].style.pixelHeight = gICONSRCHEIGHT;
			obj.children[2*j].style.display = "";
			obj.children[2*j].children[0].style.display = "";
			obj.children[2*j+1].style.textAlign = "center";
			obj.children[2*j+1].children[0].style.position = "";
			obj.children[2*j+1].children[0].innerHTML = '<IMG SRC="IMG/none.gif" WIDTH="1" HEIGHT="1"><BR>' + OUTLOOK[I][j][3];
		} else {
			obj.children[2*j+1].style.textAlign = "left";
			obj.children[2*j+1].children[0].style.position = "absolute";
			obj.children[2*j+1].children[0].style.pixelLeft = 3;
			obj.children[2*j+1].children[0].innerHTML = (j+1) + '. ' + OUTLOOK[I][j][3];
		}
	}
}
function setItemStep(obj) {
	obj.blur();
	tmpA = obj.id.split("idi");
	gMovItemStep = eval(tmpA[1]);
	setCookie("gMovItemStep", gMovItemStep);
}
function setButtonAcc(obj) {
	obj.blur();
	tmpA = obj.id.split("idv");
	gMovButtonAcc = eval(tmpA[1]);
	setCookie("gMovButtonAcc", gMovButtonAcc);
}
function setButtonIndex(obj) {
	obj.blur();
	tmpA = obj.id.split("idx");
	gMovButtonIndex = eval(tmpA[1]);
	gMovButtonStep = (gMovButtonAcc) ? 1 : gMovButtonINIT[gMovButtonIndex];
	gMovButtonX = (gMovButtonAcc) ? gMovButtonACCX[gMovButtonIndex] : 1;
	setCookie("gMovButtonIndex", gMovButtonIndex);
}
function setMode(obj) {
	obj.blur();
	tmpA = obj.id.split("idm");
	gBUTTONMODE = eval(tmpA[1]);
	setCookie("gBUTTONMODE", gBUTTONMODE);
}
function resetSETUP() {
	if (gSETUPHEIGHT < (doc.body.clientHeight - gNotyPADDINGtb - 2)) 
	    gSETUPHEIGHT = (doc.body.clientHeight - gNotyPADDINGtb - 2);
	    
	document.all["idSETUPD2"].style.pixelHeight = gSETUPHEIGHT;
	resetSETUPARROWS();
}
function resetSETUPARROWS() {
	if (doc.all["idSETUPD2"].style.pixelTop + gSETUPHEIGHT > (doc.body.clientHeight - gNotyPADDINGtb - 2)) {
		doc.all["idSETUPARROWDN"].style.pixelTop =  (doc.body.clientHeight - gNotyPADDINGtb - 2) - 20;
		doc.all["idSETUPARROWDN"].style.display =  "";
	} else {
		doc.all["idSETUPARROWDN"].style.display =  "none";
		mUPSETUP(gSETUPARR);
	}
	if (doc.all["idSETUPD2"].style.pixelTop < 0) {
		doc.all["idSETUPARROWUP"].style.pixelTop =  10;
		doc.all["idSETUPARROWUP"].style.display = "";
	} else {
		doc.all["idSETUPARROWUP"].style.display = "none";
		mUPSETUP(gSETUPARR);
	}
}
function mUPSETUP(obj) {
	gSETUPARR = null;
	cancelBubbling = true;
}
function mDNSETUP(obj) {
	gSETUPARR = obj;
	setTimeout("scrollSETUP()", 20);
	cancelBubbling = true;
}
function scrollSETUP() {
	if (gSETUPARR == null) return;
	if (gSETUPARR.id == "idSETUPARROWUP")
		doc.all["idSETUPD2"].style.pixelTop += 5;
	else
		doc.all["idSETUPD2"].style.pixelTop -= 5;
	resetSETUPARROWS();
	if (gSETUPARR != null) setTimeout("scrollSETUP()", 20);
}
//
function setCookie(name, value) {
	doc.cookie = name + '=' + value + '; expires=' + gCookieExpDate + '\n';
}
function getCookie() {
	if (doc.cookie != null && doc.cookie != "") {
		tmpA = (doc.cookie).split("; ");
		for (i=0; i<tmpA.length; i++) {
			tmpB = tmpA[i].split("=");
			switch (tmpB[0]) {
				case "CurrentOpen":
					gCO = eval(tmpB[1]); break;
				case "gICONSIZEopt":
					gICONSIZEopt = eval(tmpB[1]); break;
				case "gMovItemStep":
					gMovItemStep = eval(tmpB[1]); break;
				case "gMovButtonAcc":
					gMovButtonAcc = eval(tmpB[1]); break;
				case "gMovButtonIndex":
					gMovButtonIndex = eval(tmpB[1]); break;
				case "gBUTTONMODE":
					gBUTTONMODE = eval(tmpB[1]); break;
			}
		}
	}
}
function evWinResize() {    
	//gNotyHEIGHT = doc.body.clientHeight - gNotyPADDINGtb;	
	doc.all["idNOTYLOOK"].style.pixelHeight = doc.body.clientHeight;//gNotyHEIGHT;
	doc.all["idOUTLOOK"].style.pixelHeight = doc.all["idSETUP"].style.pixelHeight = doc.body.clientHeight - gNotyPADDINGtb - 2;
	oldItemWindowHeight = gItemWindowHeight;
	gItemWindowHeight = (doc.body.clientHeight - gNotyPADDINGtb - 2) - OUTLOOK.length*gBUTTONHEIGHT;	//-
	if (gItemWindowHeight <0 ) gItemWindowHeight = 0;		// \-- important
	diffHEIGHT = gItemWindowHeight - oldItemWindowHeight;		//-/

	if (!gItemWindowHeight) {
		if (!doc.all["idOLB0"].disabled) {
			for (i=gCO+1; i<OUTLOOK.length; i++) {
				tmpTOP = (doc.all["idOLB"+i].style.pixelTop+gBUTTONHEIGHT) - doc.all["idOLIF"+i].style.pixelTop;
				doc.all["idOLB"+i].style.pixelTop = i*gBUTTONHEIGHT;
				doc.all["idOLIF"+i].style.pixelTop = (i+1)*gBUTTONHEIGHT - tmpTOP;
			}
			offsetsubwin("idOLIF"+gCO, 0, diffHEIGHT);
			for (i=0; i<OUTLOOK.length; i++)
				doc.all["idOLB"+i].disabled = true;
		}
	} else {
		if (doc.all["idOLB0"].disabled)
			for (i=0; i<OUTLOOK.length; i++)
				doc.all["idOLB"+i].disabled = false;
		for (i=gCO+1; i<OUTLOOK.length; i++) {
			doc.all["idOLB"+i].style.pixelTop += diffHEIGHT;
			doc.all["idOLIF"+i].style.pixelTop += diffHEIGHT;
		}
		offsetsubwin("idOLIF"+gCO, 0, diffHEIGHT);
	}

	//offsetsubwin("idSETUP", 0, diffHEIGHT);
	resetARROWS();
	resetSETUPARROWS();
}

/*
函数功能：读取xml文件，将其中的 Module、Function和相关的".gif"文件名和".aspx"文件名的信息写进数组中
*/
function loadXMLFile() {
    
    
    var docXML = new ActiveXObject("Msxml2.DOMDocument");
	docXML.async = false;
	//docXML.resolveExternals = false;
	//docXML.validateOnParse = false;

	
	docXML.load(OutlookXML);

    if(docXML.xml=="") 
	{
	    window.alert("logon failed!");
		top.location="../logon.aspx";
		return false;
	}
	
	try{	  
	  if (docXML.parseError.errorCode == 0) {
        root = docXML.documentElement;                              
        //读XML文件生成三维数组 Module(Level_1)-->Function(Level_2)-->FunctionSet(Level_3) 
        for (i=0; i<root.childNodes.length; i++) {                       
              OUTLOOK[i] = new Array();                                                            
              for(j=0; j< root.childNodes.item(i).childNodes.length; j++){ 
                var iIndex = 4;   
                OUTLOOK[i][j] = new Array();
                OUTLOOK[i][j][0] = root.childNodes.item(i).getAttribute("id");
                OUTLOOK[i][j][1] = root.childNodes.item(i).getAttribute("name");
                
                OUTLOOK[i][j][2] = root.childNodes.item(i).childNodes.item(j).getAttribute("id");
                OUTLOOK[i][j][3] = root.childNodes.item(i).childNodes.item(j).getAttribute("name");
                //document.writeln(OUTLOOK[i][j][0]);
                for(k=0; k<root.childNodes.item(i).childNodes.item(j).childNodes.length; k++){
                    OUTLOOK[i][j][iIndex++] = root.childNodes.item(i).childNodes.item(j).childNodes.item(k).text;
                }
              }
           }
        //document.writeln(OUTLOOK.length);
        for (i=0; i<OUTLOOK.length; i++) {
	        if (i <= gCO) { tmpPOSITION = "UP";  tmpTOP = i*20; }
	        else {  tmpPOSITION = "DOWN";  tmpTOP = gItemWindowHeight + i*20; }
	        doc.write('<BUTTON POSITION="' + tmpPOSITION + '" STYLE="position:absolute;top:' + tmpTOP + ';left:0;width:' 
	            + (doc.body.clientWidth- gNotyPADDINGlr - 2) + ';height:' + gBUTTONHEIGHT + ';font:normal 9pt \uFFFDs灿\uFFFD\uFFFD砰;cursor:hand;color:#FFFFFF;border-width:1pt;border-color:#EBEBEB #460F0F #460F0F #EBEBEB;background-color:#a69ea8;z-index:200" onclick="OLBCLICK(' 
	            + i + ')" ID="idOLB' + i + '" onmouseover="this.style.color=\'#ffffce\'" onmouseout="this.style.color=\'#FFFFFF\'">' + OUTLOOK[i][0][1] + '</BUTTON>');
	        createItem(i);
        }
      }
        
	}
	catch(e)
	{
	  alert(e+e.description);
	  return false;
	}	
	return true;	
}

function initMenu(){
	
    gBUTTONHEIGHT = 20;   
    gICONSIZEopt = 1;
    gBUTTONMODE = 1;
    gCO = 0;
    gRegExp = /rect\((\d*)px (\d*)px (\d*)px (\d*)px\)/;
    gItemWindowHeight = (doc.body.clientHeight - gNotyPADDINGtb - 2) - OUTLOOK.length*20;
    gMovButtonINIT = (gItemWindowHeight > 200) ? new Array(Math.ceil(gItemWindowHeight/50), Math.ceil(gItemWindowHeight/25), 
            Math.ceil(gItemWindowHeight/10), Math.ceil(gItemWindowHeight/5), gItemWindowHeight) : new Array(5, 10, 20, 50, 160);
    gMovButtonAcc = 1;
    gMovButtonIndex = 2;
    gMovButtonACCX = new Array(1.1, 1.25, 1.5, 2.2, 10);
    gSETUPHEIGHT = 0;
    defITEMSPACING = 28;
    defICONWIDTH = defICONHEIGHT = 16;  
          
    var docWidth = doc.body.clientWidth- gNotyPADDINGlr - 2;
    var docHeight = doc.body.clientHeight - gNotyPADDINGtb - 2;
    //getCookie();		// comment this line to ignore cookie value        
    resetIconSize();
    //===========================================================================
    // begin rendering objects.
    //===========================================================================
    // NOTYLOOK DIV, the outter-most div
    doc.write('<DIV ID="idNOTYLOOK" CLASS="notyLOOK" STYLE="position:absolute;top:'+ gNotyTOP + ';left:' + gNotyLEFT 
        + ';width:' + (doc.body.clientWidth - gNotyPADDINGlr) + ';height:' 
        + (doc.body.clientHeight - gNotyPADDINGtb) + ';overflow:hidden">');

    // OUTLOOK DIV
    doc.write('<DIV ID="idOUTLOOK" STYLE="position:absolute;top:0;left:0;width:' + docWidth + ';height:' 
        + docHeight + ';background:' + gDarkColor + ';display:none;z-index:0">');
    if (gItemWindowHeight < 0) gItemWindowHeight = 0;
    
    if (loadXMLFile()!=true)return;    
    
    doc.write('<SPAN ID="idARROWUP" STYLE="position:absolute;width:16;height:16;top:0;left:0;display:none;z-index:199;background-color:' 
        + gLightColor +'" onmousedown="amDOWN(this)" onmouseup="amUP(this)" onmouseout="amUP(this)" CLASS="amUP"><IMG SRC="../graph/arup.gif" WIDTH="12" HEIGHT="12"></SPAN>');
    doc.write('<SPAN ID="idARROWDN" STYLE="position:absolute;width:16;height:16;top:0;left:0;display:none;z-index:199;background-color:' 
        + gLightColor +'" onmousedown="amDOWN(this)" onmouseup="amUP(this)" onmouseout="amUP(this)" CLASS="amUP"><IMG SRC="../graph/ardn.gif" WIDTH="12" HEIGHT="12"></SPAN>');
    doc.write('</DIV>');				// end of idOUTLOOK
    setsubwin("idOLIF"+gCO, 0, gItemWindowHeight);	// make itemField0 visible
    doc.all["idOUTLOOK"].style.display = "";	// make idOUTLOOK visible
    objARROWUP = doc.all["idARROWUP"];
    objARROWDN = doc.all["idARROWDN"];
    resetARROWS();	// OUTLOOK DIV ok!

    // SETUP DIV
    doc.write('<DIV ID="idSETUP" STYLE="position:absolute;top:0;left:' + docWidth + ';width:' + docWidth + ';height:' 
        + docHeight + ';background-color:#666666;z-index:300;border:1px solid black;display:none;clip:rect(0 ' 
        + (docWidth-1) + ' ' + docHeight + ' 0)">');
    doc.write('<DIV ID="idSETUPD2" STYLE="position:absolute;top:0;left:0;width:' + (docWidth-2) + ';height:' 
        + docHeight + ';font:normal 9pt Arial;line-height:120%;color:white;padding:3px"><FORM>');    
    doc.write('</FORM></DIV>');	// SETUP DIV ok
    doc.write('<IMG ID="idSETUPARROWUP" SRC="../graph/arup.gif" onmouseout="mUPSETUP(this);this.src=\'../graph/arup.gif\'" onmouseover="mDNSETUP(this);this.src=\'../graph/arup.gif\'" STYLE="position:absolute;width:21;height:12;top:0;left:75;display:none">');
    doc.write('<IMG ID="idSETUPARROWDN" SRC="../graph/arup.gif" onmouseout="mUPSETUP(this);this.src=\'../graph/arup.gif\'" onmouseover="mDNSETUP(this);this.src=\'../graph/arup.gif\'" STYLE="position:absolute;width:21;height:12;top:0;left:75;display:none">');
    doc.write('</DIV>');
    resetSETUP();

    doc.write('</DIV>');	// end of idNOTYLOOK
    doc.write('<SPAN onclick="movOUTLOOK(1)" ID="idASTP" STYLE="position:absolute;left:-426;width:0;height:0"></SPAN>');
    // All Done!  
    window.onresize = evWinResize;
    evWinResize();      
}    