﻿
Imports System.Data
Imports BusinessEntity

Namespace PresentationLayer.report
    Partial Class PresentationLayer_report_RPTWarrParts_VIEW
        Inherits Page
        'Implements IBasicReportPage

#Region "Declarations"
        Private ReadOnly _datestyle As New Globalization.CultureInfo("en-CA")
        Private ReadOnly _xml As New clsXml
        Private ReadOnly _clsReport As New ClsCommonReport
        Private _dtWarr As DataSet
        Private _rptWarr As New clsRptWarrParts
#End Region

#Region "Implements IBasicReportPage"
        Protected Sub PopulateLabels() 'Implements IBasicReportPage.PopulateLabels
            Throw New NotImplementedException()
        End Sub

        Public Sub PopulateLabels(ByVal strHead As String()) 'Implements IBasicReportPage.PopulateLabels
            Dim fromLabel As String = "From"'_xml.GetLabelName("EngLabelMsg", "BB-FROM")
            Dim toLabel As String = "To"'_xml.GetLabelName("EngLabelMsg", "BB-TO")

            titleLab.Text = "Warranty Parts Report"'_xml.GetLabelName("EngLabelMsg", "BB-WARRPARTS-TITLE")
            lnkExportToExcel.Text = "Export Records to XLS"'_xml.GetLabelName("EngLabelMsg", "BB-EXPORTPRF")

            strHead(0) = fromLabel
            strHead(1) = toLabel
            strHead(2) = "Service Center"'_xml.GetLabelName("EngLabelMsg", "BB_FUN_D_CENTER")
            strHead(5) = "Service Date"'_xml.GetLabelName("EngLabelMsg", "RPT_APT_SERDATE")
            strHead(8) = "Technician ID"'_xml.GetLabelName("EngLabelMsg", "SBA-TECHNICIANID")
            strHead(11) = "Price ID"'_xml.GetLabelName("EngLabelMsg", "BB-MASPRID-0001")

            strHead(14) = Session("userID").ToString() + " / " + Session("username").ToString()
            strHead(15) = "Elken Group of Companies"'_xml.GetLabelName("EngLabelMsg", "BB_REPORT_COMPANYNAME")
            strHead(16) = "Warranty Parts Report"'_xml.GetLabelName("EngLabelMsg", "BB-WARRPARTS-TITLE")

        End Sub

        Protected Sub GenerateReport() 'Implements IBasicReportPage.GenerateReport
            Throw New NotImplementedException()
        End Sub

        Protected Sub DefineDefaultValues() 'Implements IBasicReportPage.DefineDefaultValues
            Throw New NotImplementedException()
        End Sub

        Public Sub DefineDefaultValues(ByVal strHead As String()) 'Implements IBasicReportPage.DefineDefaultValues
            Const defaultValue As String = "ZZZ"

            If String.IsNullOrEmpty(_rptWarr.FromSvcId) Then
                strHead(3) = defaultValue
            Else
                strHead(3) = _rptWarr.FromSvcId
            End If

            If String.IsNullOrEmpty(_rptWarr.ToSvcId) Then
                strHead(4) = defaultValue
            Else
                strHead(4) = _rptWarr.ToSvcId
            End If

            If String.IsNullOrEmpty(_rptWarr.FromSvcDate.ToString("dd/MM/yyyy")) Then
                strHead(6) = defaultValue
            Else
                strHead(6) = _rptWarr.FromSvcDate.ToString("dd/MM/yyyy")
            End If

            If String.IsNullOrEmpty(_rptWarr.ToSvcDate.ToString("dd/MM/yyyy")) Then
                strHead(7) = defaultValue
            Else
                strHead(7) = _rptWarr.ToSvcDate.ToString("dd/MM/yyyy")
            End If

            If String.IsNullOrEmpty(_rptWarr.FromTechId) Then
                strHead(9) = defaultValue
            Else
                strHead(9) = _rptWarr.FromTechId
            End If

            If String.IsNullOrEmpty(_rptWarr.ToTechId) Then
                strHead(10) = defaultValue
            Else
                strHead(10) = _rptWarr.ToTechId
            End If

            If String.IsNullOrEmpty(_rptWarr.FromPriceId) Then
                strHead(12) = defaultValue
            Else
                strHead(12) = _rptWarr.FromPriceId
            End If

            If String.IsNullOrEmpty(_rptWarr.ToPriceId) Then
                strHead(13) = defaultValue
            Else
                strHead(13) = _rptWarr.ToPriceId
            End If


        End Sub

        Protected Sub GenerateReport(ByVal dataSet As DataSet) 'Implements IBasicReportPage.GenerateReport
            _rptWarr = CType(Session("WarrParts"), clsRptWarrParts)

            Dim strHead As String() = CType(Array.CreateInstance(GetType(String), 18), String())

            PopulateLabels(strHead)
            DefineDefaultValues(strHead)

            Try
                With _clsReport
                    .ReportFileName = "RptWarrParts.rpt"
                    .SetReport(CrystalReportViewer1, dataSet, strHead)
                End With
            Catch ex As Exception
                Response.Write("<BR>")
                Response.Write("Error Message: " & ex.Message.ToString)
                Response.Write("<BR>")
                Response.Write("Source : " & ex.Source)
                Response.Write("<BR>")
                Response.Write("Stack Trace Message: " & ex.StackTrace)
            End Try
        End Sub
#End Region

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
            Threading.Thread.CurrentThread.CurrentCulture = _datestyle

            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Const script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Const script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try

            Dim ds As New DataSet()
            If Not Page.IsPostBack Then
                _rptWarr = CType(Session("WarrParts"), clsRptWarrParts)
                _dtWarr = _rptWarr.RetrieveWarrantyParts()
                'ds.Tables.Add(_dtWarr)
                ds = _dtWarr
            End If

            If Not IsNothing(Session("dsWarr")) Then
                ds = CType(Session("dsWarr"), DataSet)
            End If

            GenerateReport(ds)
            Session("dsWarr") = ds
        End Sub

        Protected Sub lnkExportToExcel_Click(ByVal sender As Object, ByVal e As EventArgs)
            Throw New NotImplementedException()
        End Sub

        Protected Sub Page_Unload(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Unload
            _clsReport.UnloadReport()
            CrystalReportViewer1.Dispose()
        End Sub
    End Class
End Namespace