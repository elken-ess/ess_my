Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Web
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web
Imports BusinessEntity
Imports System.Data
Partial Class PresentationLayer_report_RPTTrace
    Inherits System.Web.UI.Page
    Private RPTTrace As clsRPTTrace
    Private TraceReport As DataSet
    Private objXmlTr As New clsXml
    Dim fstrMonth As String = ""
    Dim fstrDefaultStartDate As String = ""
    Dim fstrDefaultEndDate As String = ""
    Dim TraceReportDoc As New ReportDocument()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        fstrMonth = Month(System.DateTime.Today)
        fstrDefaultStartDate = System.DateTime.Today
        fstrDefaultEndDate = fstrDefaultStartDate 'DateAdd(DateInterval.Month, 1, CDate(fstrDefaultStartDate))

        If Not Page.IsPostBack Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    'Response.Redirect("~/PresentationLayer/logon.aspx")
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                'Response.Redirect("~/PresentationLayer/logon.aspx")
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try
            Session.Contents("AuditLogPrintDataFlag") = ""



            Session.Contents("AuditLogPrintDataFlag") = "0"

        ElseIf Session.Contents("AuditLogPrintDataFlag") = "1" Then
            BindGrid()
        End If

        'Try

        '    If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
        '        Dim script As String = "top.location='../logon.aspx';"
        '        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
        '        Return
        '    End If
        'Catch ex As Exception
        '    Dim script As String = "top.location='../logon.aspx';"
        '    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
        '    Return
        'End Try

        If Not Page.IsPostBack Then
            ''display label message
            Session("Packflag") = "0"
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            Me.lblPartCode.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_F_PARTCODE")
            ServiceDatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "RPT_APT_SERDATE")
            Modellab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0013")
            SerialNumberlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCHL-0013")
            JobTypelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "RPT_APT_SERTY")


            Dim installbond As New clsCommonClass
            'bond  service type id and name
            Dim sertype As New DataSet
            sertype = installbond.Getidname("BB_RPTPSLT_IDNAME '" & Session("userID") & "'")
            databonds(sertype, DropDownList1)


            Fromlab1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Fromlab2.Text = Fromlab1.Text
            Fromlab3.Text = Fromlab1.Text
            Fromlab4.Text = Fromlab1.Text

            Tolab1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Tolab2.Text = Tolab1.Text
            Tolab3.Text = Tolab1.Text
            Tolab4.Text = Tolab1.Text

            reportviewer.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-VIEWREPORT")
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTTRACE-titlelab")

            Me.mindate.Text = fstrDefaultStartDate
            Me.maxdate.Text = fstrDefaultEndDate

            '''access control'''''''''''''''''''''''''''''''''''''''''''''''
            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False, False}
            ' purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "47")
            Me.reportviewer.Enabled = True


        End If


        HypCalFrom.NavigateUrl = "javascript:DoCal(document.form1.mindate);"
        HypCalTo.NavigateUrl = "javascript:DoCal(document.form1.maxdate);"
    End Sub
#Region " bonds"
    Public Sub databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        dropdown.Items.Add(New ListItem("All", ""))

        Dim row As DataRow
        If ds.Tables(0).Rows.Count > 0 Then
            'dropdown.Items.Add("")
            For Each row In ds.Tables(0).Rows
                Dim NewItem As New ListItem()
                NewItem.Text = Trim(row("id")) & "-" & row("name")
                NewItem.Value = Trim(row("id"))
                dropdown.Items.Add(NewItem)
            Next
        End If
    End Sub
#End Region
#Region " BindGrid"
    Protected Sub BindGrid()

        Dim dsReport As DataSet = Session.Contents("TraceReport")

        Try
            TraceReportDoc.Load(MapPath("RPTTrace.rpt"))
            errlab.Visible = False
        Catch ex As Exception
            objXmlTr.XmlFile = ConfigurationManager.AppSettings("StatMsg")
            errlab.Text = objXmlTr.GetLabelName("StatusMessage", "BB_FUN_CALLLIST_LOADFAILED")
            errlab.Visible = True
            Return
        End Try



        TraceReportDoc.SetDataSource(dsReport.Tables(0))

        'Dim dr As DataRow
        'Dim ds As DataSet
        'Dim dt As DataTable

        'dt = TraceReport.Tables(0)
        'For Each dr In dt.Rows
        '    Console.WriteLine(dr("SerialNo"))
        '    Console.WriteLine(dr("CustomerID"))
        '    Console.WriteLine(dr("ServiceDate"))
        '    Console.WriteLine(dr("JobType"))
        '    Console.WriteLine(dr("SO#"))
        '    Console.WriteLine(dr("PartChanged"))
        'Next


        objXmlTr.XmlFile = System.Configuration.ConfigurationManager.AppSettings("XmlFilePath")
        TraceReportDoc.SetParameterValue("ReportHead", objXmlTr.GetLabelName _
                                            ("EngLabelMsg", "BB-RPTTRACE-titlelab"))


        TraceReportDoc.SetParameterValue("SerialNo", "SerialNo")
        TraceReportDoc.SetParameterValue("CustomerID", "CustomerID")
        TraceReportDoc.SetParameterValue("ServiceDate", "ServiceDate")
        TraceReportDoc.SetParameterValue("JobType", "JobType")
        TraceReportDoc.SetParameterValue("SO#", "SO#")
        TraceReportDoc.SetParameterValue("PartChanged", "PartChanged")
        TraceReportDoc.SetParameterValue("ModelID", "ModelID")


        TraceReportViewer.ReportSource = TraceReportDoc
        TraceReportViewer.HasCrystalLogo = False
        TraceReportViewer.HasExportButton = True
        TraceReportViewer.HasPageNavigationButtons = True
        TraceReportViewer.HasGotoPageButton = True

        Session.Contents("AuditLogPrintDataFlag") = "1"

    End Sub
#End Region




    Protected Sub reportviewer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles reportviewer.Click
        mindate.Text = Request.Form("Mindate")
        maxdate.Text = Request.Form("Maxdate")

        Dim UserID As String = Session("userID")
        RPTTrace = New clsRPTTrace()
        RPTTrace.UserID = UserID
        RPTTrace.FrPartCode = txtPartCodeFrom.Text
        RPTTrace.ToPartCode = txtPartCodeTo.Text
        RPTTrace.MinModelID = minmodelID.Text
        RPTTrace.MaxModelID = maxmodelID.Text
        RPTTrace.MinSerialNumber = minSerialNumber.Text
        RPTTrace.MaxSerialNumber = maxSerialNumber.Text
        RPTTrace.FrPriceType = PriceTypeFr.Text
        RPTTrace.ToPriceType = PriceTypeTo.Text
        If (Me.mindate.Text <> "") Then
            Dim datearr As Array = mindate.Text.ToString().Split("/")
            Dim rtdate As String = datearr(2) + "-" + datearr(1) + "-" + datearr(0)
            RPTTrace.Mindate = rtdate
        End If
        If (Me.maxdate.Text <> "") Then
            Dim datearr As Array = maxdate.Text.ToString().Split("/")
            Dim rtdate As String = datearr(2) + "-" + datearr(1) + "-" + datearr(0)
            RPTTrace.Maxdate = rtdate

        End If

        RPTTrace.ContractTy = Me.DropDownList1.SelectedValue



        TraceReport = RPTTrace.GetTraceReport()

        If TraceReport Is Nothing Then

            objXmlTr.XmlFile = System.Configuration.ConfigurationManager.AppSettings("StatMsg")
            errlab.Text = objXmlTr.GetLabelName("StatusMessage", "BB-View-Fail")
            errlab.Visible = True
            TraceReportViewer.Visible = False
            Return


        ElseIf TraceReport.Tables.Count = 0 Then

            objXmlTr.XmlFile = System.Configuration.ConfigurationManager.AppSettings("StatMsg")
            errlab.Text = objXmlTr.GetLabelName("StatusMessage", "BB-View-Fail")
            errlab.Visible = True
            Return
        Else : Session("TraceReport") = TraceReport
        End If
        BindGrid()

        TraceReportViewer.Visible = True

        'Return
    End Sub

    Protected Sub Calendar1_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendar1.SelectionChanged
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        mindate.Text = Calendar1.SelectedDate.Date.ToString().Substring(0, 10)
        Calendar1.Visible = False
        Session("Packflag") = "0"
    End Sub

    Protected Sub Calendar2_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendar2.SelectionChanged
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        maxdate.Text = Calendar2.SelectedDate.Date.ToString().Substring(0, 10)
        Calendar2.Visible = False
        Session("Packflag") = "0"
    End Sub

    Protected Sub TraceReportViewer_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles TraceReportViewer.Init

    End Sub
End Class
