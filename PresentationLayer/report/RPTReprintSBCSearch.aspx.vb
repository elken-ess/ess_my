Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Web
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web
Imports BusinessEntity
Imports System.Data
Partial Class PresentationLayer_report_RPTReprintSBCSearch
    Inherits System.Web.UI.Page
    Dim clsReport As New ClsCommonReport
    Dim packSBC As New ClsRptReprintSBC
    Dim packEntity As New ClsRptReprintPRF

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try


        Try


            If Trim(Session("userID")) = "" Then
                packSBC.ModifiedBy = "ADMIN"
            Else
                packSBC.ModifiedBy = Session("userID")
            End If
            Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
            Dim SBCNo As String
            SBCNo = Request.Params("FIV1_SBCNO").ToString
            Dim ds As New DataSet

            packSBC.sbcno = SBCNo
            ds = packSBC.GetPack()

            Dim objXmlTr As New clsXml


            ' Modified by Ryan Estandarte 3 September 2012
            'Dim strHead As Array = Array.CreateInstance(GetType(String), 31)
            Dim strHead As Array = Array.CreateInstance(GetType(String), 32)
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            'HyperLink1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Cancel")
            strHead(0) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_SERBITECH_DATE")
            strHead(1) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_SERBITECH_CAB")
            strHead(2) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_SERBITECH_CUSTNM")
            strHead(3) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_SERBITECH_BITO")
            strHead(4) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_SERBITECH_CASH")
            strHead(5) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_SERBITECH_CHQ")
            strHead(6) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_SERBITECH_CC")
            strHead(7) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_SERBITECH_CD")
            strHead(8) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_SERBITECH_DISC")
            strHead(9) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_SERBITECH_TOPAY")
            strHead(10) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_SERBITECH_VAR")
            strHead(11) = objXmlTr.GetLabelName("EngLabelMsg", "RPT-SERBITECH-TITLE1")
            strHead(12) = objXmlTr.GetLabelName("EngLabelMsg", "RPT-SERBITECH-TITLE4")
            strHead(13) = packSBC.Minivdate
            strHead(14) = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            strHead(15) = packSBC.Maxivdate
            strHead(16) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_SERBITECH_REPID")
            strHead(17) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_SERBITECH_TOT")

            If Session("userID") = "" Then
                strHead(18) = " "
            Else
                strHead(18) = Session("userID").ToString().ToUpper + " / " + Session("username").ToString.ToUpper

            End If

            strHead(19) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_APT_RECORD")
            strHead(20) = ds.Tables(0).Rows.Count
            strHead(21) = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_E_HEADER10")
            strHead(22) = SBCNo
            strHead(23) = packSBC.tecnicianid
            strHead(24) = packSBC.tecnicianname
            strHead(25) = packSBC.IsoDesc1
            strHead(26) = packSBC.IsoDesc2
            strHead(27) = packSBC.SVCID
            strHead(28) = packSBC.SBCUser
            strHead(29) = packSBC.SBCDate
            strHead(30) = packSBC.Col1
            ' Added by Ryan Estandarte 3 September 2012
            strHead(31) = "Type" 'objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0009")

            'End With
            ' Modified by Ryan Estandarte 3 September 2012
            'Dim lstrPdfFileName As String = "PrintSBCReport_" & Session("login_session") & ".pdf"
            Dim lstrPdfFileName As String = "PrintSBCReport_" & Session("login_session").ToString() & "_" & DateTime.Now.ToString("ddMMyyyyhhmm") & ".pdf"


            With clsReport
                .ReportFileName = "SBCReport.rpt"
                .SetReportDocument(ds, strHead)
                .PdfFileName = lstrPdfFileName
                .ExportPdf()
            End With

            Response.Redirect(clsReport.PdfUrl)



        Catch err As Exception
            'System.IO.File.Delete(lstrPhysicalFile)
            Response.Write("<BR>")
            Response.Write(err.Message.ToString)

        End Try

    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        clsReport.UnloadReport()
        CrystalReportViewer1.Dispose()
    End Sub


End Class
