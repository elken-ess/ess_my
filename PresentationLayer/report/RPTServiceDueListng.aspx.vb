﻿
Imports System.Data
Imports BusinessEntity


Namespace PresentationLayer.report
    Partial Class PresentationLayer_report_RPTServiceDueListng
        Inherits Page
        'Implements IBasicReportPage

#Region "Declaration"
        Private ReadOnly _datestyle As Globalization.CultureInfo = New Globalization.CultureInfo("en-CA")
        'Private ReadOnly _xml As New clsXml
        Private ReadOnly _clsCommon As New clsCommonClass
        Private ReadOnly _clsrptcust As New ClsRptCUSR

        Private _rptSvc As New ClsRptServiceDue
#End Region

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
            Threading.Thread.CurrentThread.CurrentCulture = _datestyle
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Const script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Const script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try



            If Not Page.IsPostBack Then
                'PopulateLabels()
                DefineDefaultValues()
            End If

            'PopulateStateAndAreaFromServiceCenter(ddFromSVCCenter.SelectedValue, ddFromState, ddFromArea)
            'PopulateStateAndAreaFromServiceCenter(ddToSVCCenter.SelectedValue, ddToState, ddToArea)

            HypCalFrom.NavigateUrl = "javascript:DoCal(document.form1.txtFromServiceDate);"
            HypCalTo.NavigateUrl = "javascript:DoCal(document.form1.txtToServiceDate);"
			
			Dim fromLabel As String = "From"'_xml.GetLabelName("EngLabelMsg", "BB-FROM")
            Dim toLabel As String = "To"'_xml.GetLabelName("EngLabelMsg", "BB-TO")

            titleLab.Text = "Service Due Listing"'_xml.GetLabelName("EngLabelMsg", "BB-RPTSERVICE-TITLE")

            lblSvcCenter.Text = "Service Center"'_xml.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-SERVCENTER")
            lblFromSVC.Text = fromLabel
            lblToSVC.Text =toLabel

            lblState.Text = "State"'_xml.GetLabelName("EngLabelMsg", "BB-MASROUM-0032")
            lblFromState.Text = fromLabel
            lblToState.Text = toLabel

            lblArea.Text = "Area"'_xml.GetLabelName("EngLabelMsg", "BB-MASROUM-0033")
            lblFromArea.Text = fromLabel
            lblToArea.Text = toLabel

            lblProduct.Text = "Product Class"'_xml.GetLabelName("EngLabelMsg", "BB-MASROUM-0010")
            lblFromProduct.Text = fromLabel
            lblToProduct.Text = toLabel

            lblModelID.Text = "Model"'_xml.GetLabelName("EngLabelMsg", "BB-RPTSOSA-MODEL")
            lblFromModelID.Text = fromLabel
            lblToModelID.Text = toLabel

            lblServiceDate.Text = "Service Due Date"'_xml.GetLabelName("EngLabelMsg", "BB-RPTSERVICE-0001")
            lblFromServiceDate.Text = fromLabel
            lblToServiceDate.Text = toLabel

            ' Added by Ryan Estandarte 8 Jan 2013
            reqValFromSvcDate.ErrorMessage = "Please enter a valid date"'_xml.GetLabelName("StatusMessage", "BB-MASJOBS-0008")
            regValFromSvcDate.ErrorMessage = "Invalid Date Format, Date format should be dd/mm/yyyy"'_xml.GetLabelName("StatusMessage", "INVALID_DATE")
            compValFromSvcDate.ErrorMessage = "Start date should be less than End date."'_xml.GetLabelName("StatusMessage", "BB-RPTSERVICE-01")

            reqValToSvcDate.ErrorMessage = "Please enter a valid date"'_xml.GetLabelName("StatusMessage", "BB-MASJOBS-0008")
            regValFromSvcDate.ErrorMessage = "Invalid Date Format, Date format should be dd/mm/yyyy"'_xml.GetLabelName("StatusMessage", "INVALID_DATE")

        End Sub

        Private Sub PopulateStateAndAreaFromServiceCenter(ByVal serviceCenterID As String, ByVal ddState As DropDownList, ByVal ddArea As DropDownList)
            Dim dtState As New DataTable
            Dim dtArea As New DataTable

            ddState.Items.Clear()
            ddArea.Items.Clear()

            If Not String.IsNullOrEmpty(serviceCenterID) Then
                _clsrptcust.Minsvcid = serviceCenterID
                _clsrptcust.ctryid = Session("login_ctryID").ToString().ToUpper()

                Dim ds As DataSet = _clsrptcust.getStateAndArea()
                dtState = ds.Tables(0)
                dtArea = ds.Tables(1)
            Else
                _clsrptcust.userid = Session("userID").ToString()

                Dim ds As DataSet = _clsrptcust.SVCnStatebyUserID()
                dtState = ds.Tables(0)
                dtArea = ds.Tables(2)
            End If

            BindValuesToDropdown(dtState, ddState)
            BindValuesToDropdown(dtArea, ddArea)
        End Sub

        

        REM Private Sub PopulateLabels() Implements IBasicReportPage.PopulateLabels
            
        REM End Sub

        REM Public Sub PopulateLabels(ByVal strHead As String()) Implements IBasicReportPage.PopulateLabels
            REM Throw New NotImplementedException()
        REM End Sub

        Private Sub GenerateReport() 'Implements IBasicReportPage.GenerateReport

            Dim startDate As DateTime
            DateTime.TryParse(txtFromServiceDate.Text, startDate)
            Dim endDate As DateTime
            DateTime.TryParse(txtToServiceDate.Text, endDate)

            _rptSvc.FromServiceDueDate = startDate
            _rptSvc.ToServiceDueDate = endDate
            _rptSvc.FromSvcCenter = ddFromSVCCenter.SelectedValue
            _rptSvc.ToSvcCenter = ddToSVCCenter.SelectedValue
            _rptSvc.FromState = ddFromState.SelectedValue
            _rptSvc.ToState = ddToState.SelectedValue
            _rptSvc.FromArea = ddFromArea.SelectedValue
            _rptSvc.ToArea = ddToArea.SelectedValue
            _rptSvc.FromProdClass = ddFromProduct.SelectedValue
            _rptSvc.ToProdClass = ddToProduct.SelectedValue
            _rptSvc.FromModelID = ddFromModelID.SelectedValue
            _rptSvc.ToModelID = ddToModelID.SelectedValue
            _rptSvc.Username = Session("userID").ToString()
            _rptSvc.CustType = ddlcusttype.SelectedValue

            Dim dt As New DataTable
            dt = _rptSvc.RetrieveServiceDue().Tables(0)

            If dt.Rows.Count > 0 Then
                Session("DataTable") = dt
            Else
                Session("DataTable") = "X"
            End If

            Session("SvcFr") = _rptSvc.FromSvcCenter
            Session("SvcTo") = _rptSvc.ToSvcCenter
            Session("StateFr") = _rptSvc.FromState
            Session("StateTo") = _rptSvc.ToState
            Session("AreaFr") = _rptSvc.FromArea
            Session("AreaTo") = _rptSvc.ToArea
            Session("ProdFr") = _rptSvc.FromProdClass
            Session("ProdTo") = _rptSvc.ToProdClass
            Session("ModelFr") = _rptSvc.FromModelID
            Session("ModelTo") = _rptSvc.ToModelID
            Session("DueFr") = _rptSvc.FromServiceDueDate
            Session("DueTo") = _rptSvc.ToServiceDueDate

            'Dim i As Integer
            'Dim Contact As String
            'Dim CustID As String
            'Dim No As String
            'Dim Name As String
            'Dim Serial As String
            'Dim SVC As String
            'Dim Area As String
            'Dim LastX As String
            'Dim DueType As String
            'Dim ModName As String

            'Contact = ""
            'CustID = ""
            'No = ""
            'Name = ""
            'Serial = ""
            'SVC = ""
            'Area = ""
            'LastX = ""
            'DueType = ""
            'ModName = ""


            'i = 1
            'For Each row As DataRow In dt.Rows
            '    Contact = Contact + "<br>" + row("MTEL_TELNO")
            '    CustID = CustID + "<br>" + row("MCUS_CUSID")
            '    No = No + "<br>" + i.ToString
            '    Name = Name + "<br>" + Left(row("MCUS_ENAME"), 20)
            '    Serial = Serial + "<br>" + row("MROU_SERNO")
            '    SVC = SVC + "<br>" + row("SVC_NAME")
            '    Area = Area + "<br>" + row("MARE_ENAME")
            '    ModName = ModName + "<br>" + row("MMOD_ENAME")
            '    LastX = LastX + "<br>" + row("MROU_APTDT")
            '    DueType = DueType + "<br>" + row("MROU_LSVDT")

            '    i = i + 1
            'Next

            'Session("CustID") = CustID
            'Session("No") = No
            'Session("Name") = Name
            'Session("Contact") = Contact
            'Session("ModName") = ModName
            'Session("Serial") = Serial
            'Session("SVC") = SVC
            'Session("Area") = Area
            'Session("LastX") = LastX
            'Session("DueType") = DueType




        End Sub

        Private Sub DefineDefaultValues() 'Implements IBasicReportPage.DefineDefaultValues
            _rptSvc.Username = Session("userID").ToString()
            Dim dt As DataTable
            dt = _rptSvc.PopulateProductClass("PRODUCTCL")
            BindValuesToDropdown(dt, ddFromProduct)
            BindValuesToDropdown(dt, ddToProduct)

            _clsCommon.spctr = Session("login_ctryID").ToString()
            _clsCommon.spstat = ""
            _clsCommon.sparea = ""
            _clsCommon.rank = ""

            dt = _clsCommon.Getcomidname("BB_MASMOTY_IDNAME").Tables(0)
            BindValuesToDropdown(dt, ddFromModelID)
            BindValuesToDropdown(dt, ddToModelID)

            _clsrptcust.userid = Session("userID").ToString()
            Dim ds As DataSet = _clsrptcust.SVCnStatebyUserID()
            Dim dtSvcCtr As DataTable = ds.Tables(1)
            Dim dtState As DataTable = ds.Tables(0)
            Dim dtArea As DataTable = ds.Tables(2)

            BindValuesToDropdown(dtSvcCtr, ddFromSVCCenter)
            BindValuesToDropdown(dtSvcCtr, ddToSVCCenter)
            BindValuesToDropdown(dtState, ddFromState)
            BindValuesToDropdown(dtState, ddToState)
            BindValuesToDropdown(dtArea, ddFromArea)
            BindValuesToDropdown(dtArea, ddToArea)

            txtFromServiceDate.Text = "01/" + DateTime.Today.ToString("MM") + "/" + DateTime.Today.Year.ToString()
            txtToServiceDate.Text = CDate(txtFromServiceDate.Text).AddMonths(1).ToShortDateString()

            Dim custtype As New clsrlconfirminf()
            Dim custParam As ArrayList = New ArrayList
            custParam = custtype.searchconfirminf("CUSTTYPE")
            Dim custcount As Integer
            Dim custid As String
            Dim custnm As String
            Me.ddlcusttype.Items.Add("ALL")
            For custcount = 0 To custParam.Count - 1
                custid = custParam.Item(custcount)
                custnm = custParam.Item(custcount + 1)
                custcount = custcount + 1
                Me.ddlcusttype.Items.Add(New ListItem(custnm.ToString(), custid.ToString()))
            Next
        End Sub

        Public Sub DefineDefaultValues(ByVal strHead As String()) 'Implements IBasicReportPage.DefineDefaultValues
            Throw New NotImplementedException()
        End Sub

        Private Sub GenerateReport(ByVal dataSet As DataSet) 'Implements IBasicReportPage.GenerateReport
            Throw New NotImplementedException()
        End Sub

        Private Sub PopulateAreaFromState(ByVal stateID As String, ByVal ddArea As DropDownList)
            ddArea.Items.Clear()
            Dim dtArea As New DataTable()
            Dim common As New clsCommonClass

            common.rank = Session("login_rank").ToString
            common.spctr = Session("login_ctryID").ToString()
            common.spstat = stateID

            dtArea = common.Getcomidname("BB_MASAREA_IDNAME").Tables(0)

            If dtArea.Rows.Count > 0 Then
                BindValuesToDropdown(dtArea, ddArea)
            End If

        End Sub

        Private Sub BindValuesToDropdown(ByVal dt As DataTable, ByVal dropdown As DropDownList)
            dropdown.Items.Clear()
            dropdown.Items.Add(New ListItem("All", ""))

            Dim row As DataRow
            If dt.Rows.Count > 0 Then
                For Each row In dt.Rows
                    Dim newItem As New ListItem()
                    newItem.Text = Trim(row(0).ToString()) & "-" & row(1).ToString()
                    newItem.Value = Trim(row(0).ToString())
                    dropdown.Items.Add(newItem)
                Next
            End If
        End Sub

        Private Sub PopulateModelFromProduct(ByVal productID As String, ByVal ddModel As DropDownList)
            ddModel.Items.Clear()
            Dim dtModel As New DataTable
            _rptSvc.ProductID = productID
            _rptSvc.CountryID = Session("login_ctryID").ToString()

            dtModel = _rptSvc.RetrieveModelFromProduct().Tables(0)

            If dtModel.Rows.Count > 0 Then
                BindValuesToDropdown(dtModel, ddModel)
            End If

        End Sub

        Protected Sub ddSVCCenter_SelectedIndexChange(ByVal sender As Object, ByVal e As EventArgs)
            Dim dd As DropDownList = CType(sender, DropDownList)

            Select Case dd.ID
                Case "ddFromSVCCenter"
                    PopulateStateAndAreaFromServiceCenter(ddFromSVCCenter.SelectedValue, ddFromState, ddFromArea)
                Case "ddToSVCCenter"
                    PopulateStateAndAreaFromServiceCenter(ddToSVCCenter.SelectedValue, ddToState, ddToArea)
            End Select
        End Sub

        Protected Sub ddState_SelectedIndexChange(ByVal sender As Object, ByVal e As EventArgs)
            Dim dd As DropDownList = CType(sender, DropDownList)

            Select Case dd.ID
                Case "ddFromState"
                    PopulateAreaFromState(ddFromState.SelectedValue, ddFromArea)
                Case "ddToState"
                    PopulateAreaFromState(ddToState.SelectedValue, ddToArea)
            End Select
        End Sub

        Protected Sub ddProduct_SelectedIndexChange(ByVal sender As Object, ByVal e As EventArgs)
            Dim dd As DropDownList = CType(sender, DropDownList)

            Select Case dd.ID
                Case "ddFromProduct"
                    PopulateModelFromProduct(ddFromProduct.SelectedValue, ddFromModelID)
                Case "ddToProduct"
                    PopulateModelFromProduct(ddToProduct.SelectedValue, ddToModelID)
            End Select
        End Sub

        Protected Sub reportViewer_Click(ByVal sender As Object, ByVal e As EventArgs) Handles reportviewer.Click
            GenerateReport()

            Const script As String = "window.open('../report/RPTServiceDueListng_VIEW1.aspx')"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "RPTServiceDueListng_VIEW1", script, True)



        End Sub


    End Class
End Namespace