﻿Imports CrystalDecisions.CrystalReports.Engine
Imports BusinessEntity
Imports System.Data

Partial Class PresentationLayer_report_AuditLogReportView
    Inherits System.Web.UI.Page

    Dim objXML As New clsXml
    Dim clsALR As New clsAuditLogReport
    Dim callListReportDoc As New ReportDocument()
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim script As String = "top.location='../logon.aspx';"
        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then

                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
        Catch ex As Exception

            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try

        If Not IsPostBack Then
            If Not SetUserPurview() Then
                'Response.Redirect("~/PresentationLayer/logon.aspx")
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
            objXML.XmlFile = ConfigurationManager.AppSettings("XmlFilePath")
            Me.titleLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_ALR_TITLE")
            'Me.backLink.Text = objXML.GetLabelName("EngLabelMsg", "BB-BACK")
            'backLink.Attributes("onclick") = "javascript:history.back();"
        End If
        LoadInfo()
    End Sub

    Private Sub LoadInfo()


        Dim strSortBy As String = ""

        'Dim strFHour, strFMin, strFSecond, strTHour, strTMin, strTSecond As String

        'strFHour = Request.Params("FHour").ToString()
        'strFMin = Request.Params("FMin").ToString()
        'strFSecond = Request.Params("FSecond").ToString()
        'strTHour = Request.Params("THour").ToString()
        'strTMin = Request.Params("TMin").ToString()
        'strTSecond = Request.Params("TSecond").ToString()

        'If (Request.Params.HasKeys()) Then
        strSortBy = Request.Params("SortByText").ToString()
        'End If

        'clsALR.FromServiceCenter = Session("FServerCenter_AuditReport")
        'clsALR.ToServiceCenter = Session("TServerCenter_AuditReport")
        'clsALR.FromDept = Session("FDept_AuditReport")
        'clsALR.ToDept = Session("TDept_AuditReport")
        'clsALR.FromUserID = Session("FUserID_AuditReport")
        'clsALR.ToUserID = Session("TUserID_AuditReport")
        'clsALR.FromDate = Session("FDate_AuditReport")
        'clsALR.ToDate = Session("TDate_AuditReport")
        'clsALR.FromLoginTime = Session("FTime_AuditReport")
        'clsALR.ToLoginTime = Session("TTime_AuditReport")
        'clsALR.FromScreenID = Session("FScreenID_AuditReport")
        'clsALR.ToScreenID = Session("TScreenID_AuditReport")
        'clsALR.FromSessionID = Session("FSessionID_AuditReport")
        'clsALR.ToSessionID = Session("TSessionID_AuditReport")
        'clsALR.SortBy = Session("SortBy_AuditReport")
        'clsALR.LoginUserID = Session("userID").ToString

        clsALR.FromServiceCenter = Request.Params("SVCFr")
        clsALR.ToServiceCenter = Request.Params("SVCTo")
        clsALR.FromDept = Request.Params("DeptFr")
        clsALR.ToDept = Request.Params("DeptTo")
        clsALR.FromUserID = Request.Params("UserFr")
        clsALR.ToUserID = Request.Params("UserTo")
        clsALR.FromDate = Request.Params("DateFr")
        clsALR.ToDate = Request.Params("DateTo")
        clsALR.FromLoginTime = Request.Params("TimeFr")
        clsALR.ToLoginTime = Request.Params("TimeTo")
        clsALR.FromScreenID = Request.Params("ScreenFr")
        clsALR.ToScreenID = Request.Params("ScreenTo")
        clsALR.FromSessionID = Request.Params("SessionFr")
        clsALR.ToSessionID = Request.Params("SessionTo")
        clsALR.SortBy = Request.Params("SortBy")
        clsALR.LoginUserID = Session("userID").ToString

        Dim dsReport As DataSet = clsALR.RetriveAuditLogInfo() 'Session.Contents("AuditLogReport")
        If dsReport Is Nothing Then
            objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
            ErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_FUN_CALLLIST_RERR")
            ErrLab.Visible = True
            Return
        End If
        If dsReport.Tables.Count = 0 Then
            objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
            ErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_FUN_CALLLIST_RERR")
            ErrLab.Visible = True
            Return
        End If

        Dim i As Integer
        If dsReport.Tables.Count > 0 Then
            For i = 0 To dsReport.Tables(0).Rows.Count - 1
                dsReport.Tables(0).Rows(i).Item(0) = i + 1
            Next
        End If

        Try
            callListReportDoc.Load(MapPath("AuditLogCrystalReport.rpt"))
            ErrLab.Visible = False
        Catch ex As Exception
            objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
            'ErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_FUN_CALLLIST_LOADFAILED")
            ErrLab.Text = ex.InnerException.Message.ToString
            ErrLab.Visible = True
            Return
        End Try

        Try
            callListReportDoc.SetDataSource(dsReport.Tables(0))
        Catch ex As Exception
            ErrLab.Text = "The error returned was " & ex.Message.ToString
        End Try

        objXML.XmlFile = ConfigurationManager.AppSettings("XmlFilePath")
        callListReportDoc.SetParameterValue("NO", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_NO"))
        callListReportDoc.SetParameterValue("ServiceCenter", objXML.GetLabelName("EngLabelMsg", "BB_FUN_ALR_SERC"))
        callListReportDoc.SetParameterValue("Date", objXML.GetLabelName("EngLabelMsg", "BB_FUN_ALR_DT"))
        callListReportDoc.SetParameterValue("UserInfo", objXML.GetLabelName("EngLabelMsg", "BB_FUN_ALR_USER"))
        callListReportDoc.SetParameterValue("SessionID", objXML.GetLabelName("EngLabelMsg", "BB_FUN_ALR_SESS"))
        callListReportDoc.SetParameterValue("IPAddress", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_IPA"))
        callListReportDoc.SetParameterValue("FuncID", objXML.GetLabelName("EngLabelMsg", "BB_FUN_ALR_SCR"))
        callListReportDoc.SetParameterValue("OldVal", objXML.GetLabelName("EngLabelMsg", "BB_FUN_ALR_OV"))
        callListReportDoc.SetParameterValue("NewVal", objXML.GetLabelName("EngLabelMsg", "BB_FUN_ALR_NV"))
        callListReportDoc.SetParameterValue("ReportIDAndUser", objXML.GetLabelName("EngLabelMsg", "BB_FUN_ALR_PRITITLE") & _
                                            " / " & Session("userID") & " / " & Session("userName"))
        'callListReportDoc.SetParameterValue("UserIDAndName", objXML.GetLabelName("EngLabelMsg", "BB_FUN_ALR_USERID") & _
        '                                 " " & Session("userID") & " / " & Session("userName"))
        callListReportDoc.SetParameterValue("ReportTitle", objXML.GetLabelName("EngLabelMsg", "BB_FUN_ALR_TITLE"))
        callListReportDoc.SetParameterValue("CompanyName", objXML.GetLabelName("EngLabelMsg", "BB_REPORT_COMPANYNAME"))
        'callListReportDoc.SetParameterValue("ReportTotal", objXML.GetLabelName("EngLabelMsg", "BB_REPORT_REPORTTOTAL") & _
        '                                " " & dsReport.Tables(0).Rows.Count)

        callListReportDoc.SetParameterValue("FromServiceCenterLab", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_FSC"))
        callListReportDoc.SetParameterValue("ToServiceCenterLab", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_TSC"))
        callListReportDoc.SetParameterValue("FromServiceCenterBox", dsReport.Tables(1).Rows(0).Item("FromServiceCenter"))
        callListReportDoc.SetParameterValue("ToServiceCenterBox", dsReport.Tables(2).Rows(0).Item("ToServiceCenter"))
        callListReportDoc.SetParameterValue("FromDeptLab", objXML.GetLabelName("EngLabelMsg", "BB_FUN_ALR_DEPF"))
        callListReportDoc.SetParameterValue("ToDeptLab", objXML.GetLabelName("EngLabelMsg", "BB_FUN_ALR_DEPT"))
        callListReportDoc.SetParameterValue("FromDeptBox", dsReport.Tables(3).Rows(0).Item("FromDept"))
        callListReportDoc.SetParameterValue("ToDeptBox", dsReport.Tables(4).Rows(0).Item("ToDept"))
        callListReportDoc.SetParameterValue("FromUserIDLab", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_FUI"))
        callListReportDoc.SetParameterValue("ToUserIDLab", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_TUI"))
        callListReportDoc.SetParameterValue("FromUserIDBox", dsReport.Tables(5).Rows(0).Item("FromUserID"))
        callListReportDoc.SetParameterValue("ToUserIDBox", dsReport.Tables(6).Rows(0).Item("ToUserID"))
        callListReportDoc.SetParameterValue("FromDateLab", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_FD"))
        callListReportDoc.SetParameterValue("ToDateLab", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_TD"))
        callListReportDoc.SetParameterValue("FromDateBox", dsReport.Tables(7).Rows(0).Item("FromDate"))
        callListReportDoc.SetParameterValue("ToDateBox", dsReport.Tables(8).Rows(0).Item("ToDate"))
        callListReportDoc.SetParameterValue("FromLoginTimeLab", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_FLT"))
        callListReportDoc.SetParameterValue("ToLoginTimeLab", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_TLT"))
        callListReportDoc.SetParameterValue("FromLoginTimeBox", Request.Params("TimeFr")) 'Session("FTime_AuditReport"))
        ' Modified by Ryan Estandarte 28 Mar 2012
        'callListReportDoc.SetParameterValue("ToLoginTimeBox", Request.Params("TimeFr")) 'Session("TTime_AuditReport"))
        callListReportDoc.SetParameterValue("ToLoginTimeBox", Request.Params("TimeTo"))
        callListReportDoc.SetParameterValue("FromScreenIDLab", objXML.GetLabelName("EngLabelMsg", "BB_FUN_ALR_SCRF"))
        callListReportDoc.SetParameterValue("ToScreenIDLab", objXML.GetLabelName("EngLabelMsg", "BB_FUN_ALR_SCRT"))
        callListReportDoc.SetParameterValue("FromScreenIDBOX", dsReport.Tables(9).Rows(0).Item("FromScreenID"))
        callListReportDoc.SetParameterValue("ToScreenIDBOX", dsReport.Tables(10).Rows(0).Item("ToScreenID"))
        callListReportDoc.SetParameterValue("FromSessionIDLab", objXML.GetLabelName("EngLabelMsg", "BB_FUN_ALR_SESF"))
        callListReportDoc.SetParameterValue("ToSessionIDLab", objXML.GetLabelName("EngLabelMsg", "BB_FUN_ALR_SEST"))
        callListReportDoc.SetParameterValue("FromSessionIDBOX", dsReport.Tables(11).Rows(0).Item("FromSessionID"))
        callListReportDoc.SetParameterValue("ToSessionIDBOX", dsReport.Tables(12).Rows(0).Item("ToSessionID"))
        callListReportDoc.SetParameterValue("SortByLab", objXML.GetLabelName("EngLabelMsg", "BB_FUN_ALR_SORT"))
        callListReportDoc.SetParameterValue("SortByBOX", strSortBy)

        CrystalReportViewer.HasCrystalLogo = False
        CrystalReportViewer.ReportSource = callListReportDoc




    End Sub

    Private Function SetUserPurview() As Boolean        
        Return clsUserAccessGroup.GetUserPurview(Session("accessgroup"), "54", "P")
    End Function

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        callListReportDoc.Dispose()
        CrystalReportViewer.Dispose()
    End Sub
End Class
