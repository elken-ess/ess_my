<%@ Page Language="VB" AutoEventWireup="false" CodeFile="RPTREPRINTPRFSINXLS.aspx.vb" Inherits="PresentationLayer_Report_RPTREPRINTPRFSINXLS" %>

<%@ Register Assembly="JCalendar" Namespace="JCalendar" TagPrefix="cc1" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Reprint PRF</title>
    <meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
	<link href="../css/style.css" type="text/css" rel="stylesheet" />     
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="JavaScript" src="../js/common.js"></script>
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
            
</head>
<body>
    <form id="form1" runat="server">
       <div>
     <table id="countrytab" border="0" width="100%">
            <tr>
                <td style="width: 100%">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title1.gif" width="5" /></td>
                            <td background="../graph/title_bg.gif" class="style2" width="98%">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="LEFT" background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title_2.gif" width="5" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100%">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" class="style2" width="98%">
                                <font color="red">
                                    <asp:Label ID="errlab" runat="server" Text="Label" Visible="false"></asp:Label></font></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100%; height: 188px;">
                    <table id="country" bgcolor="#b7e6e6" border="0" cellpadding="2" cellspacing="1" width ="100%">
                        <tr bgcolor="#ffffff">
                            <td align="LEFT" style="width: 15%">
                                <asp:Label ID="lblServCenter" runat="server" Text="Lable"></asp:Label></td>
                            <td align="LEFT" style="width: 6%">
                                <asp:Label ID="Fromlab2" runat="server" Text="From"></asp:Label></td>
                            <td align="left" style="width: 27%">
                                <asp:DropDownList ID="minsercenter" runat="server"></asp:DropDownList></td>
                            <td align="LEFT" style="width: 6%" colspan="2">
                                <asp:RequiredFieldValidator
                                     ID="misercentervalidator"
                                     runat="server"
                                     ControlToValidate="minsercenter"
                                     InitialValue="---Select---"
                                     ErrorMessage="Please select a service center."
                                ></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="LEFT" style="width: 15%; height: 30px;">
                                <asp:Label ID="lblDate" runat="server" Text="Label"></asp:Label></td>
                            <td align="LEFT" style="width: 6%; height: 30px;">
                                <asp:Label ID="Fromlab3" runat="server" Text="From"></asp:Label></td>
                            <td align="left" style="width: 27%; height: 30px;">
                                <asp:TextBox ID="mindate" runat="server" CssClass="textborder" MaxLength =10 Width="120px" style="width: 60%">01/01/2006</asp:TextBox>
                                <asp:HyperLink ID="HypCalFrom" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif" ToolTip="Choose a Date">Choose a Date</asp:HyperLink>
                                <cc1:JCalendar ID="JCalendar1" runat="server" ControlToAssign="mindate" ImgURL="~/PresentationLayer/graph/calendar.gif"  Visible="false" />
                                &nbsp;&nbsp;
                            </td>
                            <td align="LEFT" style="width: 6%; height: 30px;">
                                <asp:Label ID="Tolab3" runat="server" Text="To"></asp:Label></td>
                            <td align="left" style="width: 27%; height: 30px;">
                                <asp:TextBox ID="maxdate" runat="server" CssClass="textborder" MaxLength =10 Width="120px" style="width: 60%">01/01/2007</asp:TextBox>
                                <asp:HyperLink ID="HypCalTo" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif" ToolTip="Choose a Date">Choose a Date</asp:HyperLink> 
                                <cc1:JCalendar ID="JCalendar2" runat="server" ControlToAssign="maxdate" ImgURL="~/PresentationLayer/graph/calendar.gif"  Visible="false" />
                                &nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="LEFT" style="width: 15%">
                                <asp:Label ID="lblTechnicianID" runat="server" Text="Label"></asp:Label>
                            </td>
                            <td align="LEFT" style="width: 6%">
                                <asp:Label ID="Fromlab4" runat="server" Text="From" Height="16px"></asp:Label></td>
                            <td align="left" style="width: 27%">
                                <asp:TextBox ID="mintechnician" runat="server" CssClass="textborder" MaxLength="10" Width="60%"></asp:TextBox>
                                <asp:RequiredFieldValidator runat="server" id="reqmintechid" controltovalidate="mintechnician" errormessage="Required" /></td>
                            <td align="LEFT" style="width: 6%">
                                <asp:Label ID="Tolab4" runat="server" Text="To" Height="16px"></asp:Label></td>
                            <td align="left" width="27%" style="height: 30px">
                                <asp:TextBox ID="maxtechnician" runat="server" CssClass="textborder" MaxLength="10" Width="60%"></asp:TextBox>
                                <asp:RequiredFieldValidator runat="server" id="reqmaxtechid" controltovalidate="maxtechnician" errormessage="Required" /></td>
                        </tr>
                        
                         <tr bgcolor="#ffffff">
                            <td align="LEFT" style="width: 15%">
                                <asp:Label ID="lblPrfNo" runat="server" Text="Lable"></asp:Label>    
                            </td>
                            <td align="LEFT" style="width: 6%">
                                <asp:Label ID="Fromlab5" runat="server" Text="From" Height="16px"></asp:Label></td>
                            <td align="left" style="width: 27%">
                                <asp:TextBox ID="txtPrfNoFrom" runat="server" CssClass="textborder" MaxLength="13" Width="60%">PRF</asp:TextBox>
                                <asp:RequiredFieldValidator runat="server" id="reqtxtPrfNoFrom" controltovalidate="txtPrfNoFrom" errormessage="Required" /></td>
                            <td align="LEFT" style="width: 6%">
                                <asp:Label ID="Tolab5" runat="server" Text="To" Height="16px"></asp:Label></td>
                            <td align="left" width="27%">
                                <asp:TextBox ID="txtPrfNoTo" runat="server" CssClass="textborder" MaxLength="13" Width="60%">PRF</asp:TextBox>
                                <asp:RequiredFieldValidator runat="server" id="reqtxtPrfNoTo" controltovalidate="txtPrfNoTo" errormessage="Required" /></td>
                        </tr>                                              
                        
                    </table>
                    <asp:LinkButton ID="reportviewer" runat="server">LinkButton</asp:LinkButton></td>
            </tr>
        </table>   
    </div> 	
   <div>
   <table>
   <tr><td><asp:LinkButton ID="exportreport" runat="server">LinkButton</asp:LinkButton></td></tr>
   <tr>
		<td align="center">
		<asp:datagrid id="dg" runat="server" Height="40px" Width="830px" BorderColor="#400040" BorderStyle="None"
				BorderWidth="1px" BackColor="White" AutoGenerateColumns="False" PageSize="1" CellPadding="4" Font-Size="8pt"
				Font-Names="Verdana">
				<SelectedItemStyle Font-Bold="True" BorderWidth="1px" ForeColor="#CCFF99" BorderStyle="Solid" BorderColor="Teal"
					BackColor="Lavender"></SelectedItemStyle>
				<EditItemStyle BorderWidth="1px" BorderStyle="Solid" BorderColor="Teal"></EditItemStyle>
				<AlternatingItemStyle BorderWidth="1px" BorderStyle="Solid" BorderColor="Teal"></AlternatingItemStyle>
				<ItemStyle ForeColor="Black" BorderColor="White" BackColor="White"></ItemStyle>
				<HeaderStyle Font-Bold="True" ForeColor="Black" BackColor="Lavender"></HeaderStyle>
				<FooterStyle ForeColor="#003399" BackColor="Lavender"></FooterStyle>
				<Columns>
					<asp:BoundColumn DataField="BB_SVCID" HeaderText="BB Service ID"></asp:BoundColumn>
					<asp:BoundColumn DataField="JDE_SVCID" HeaderText="JDE Service ID"></asp:BoundColumn>
					<asp:BoundColumn DataField="TransactionDate" ReadOnly="True" HeaderText="Required Date" DataFormatString="{0:d}">
						<ItemStyle ForeColor="Black"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="PRFNumber" HeaderText="PRF Number"></asp:BoundColumn>
					<asp:BoundColumn DataField="TechnicianID" HeaderText="Technician ID"></asp:BoundColumn>
					<asp:BoundColumn DataField="ItemNumber" HeaderText="Part Number"></asp:BoundColumn>
					<asp:BoundColumn DataField="Quantity" HeaderText="Quantity"></asp:BoundColumn>
					<%--<asp:BoundColumn DataField="UnitOfMeasure" HeaderText="Unit of Measure"></asp:BoundColumn>--%>
					<asp:BoundColumn DataField="Location" HeaderText="Location"></asp:BoundColumn>
				</Columns>
				<PagerStyle HorizontalAlign="Left" ForeColor="#003399" BackColor="#99CCCC" Mode="NumericPages"></PagerStyle>
		</asp:datagrid></td>
	</tr>
	</table>
   </div>
    </form> 
</body>
</html>

