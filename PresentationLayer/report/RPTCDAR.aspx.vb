Imports BusinessEntity
Imports System.Configuration
Imports System.Data
Imports System.Data.SqlClient
Imports System.Xml
Partial Class PresentationLayer_Report_RPTCDAR
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        'display label message
        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try
        If Not Page.IsPostBack Then
            Session("RPTCDARflag") = "0"
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            Me.ServiceCenterlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDAR-SVC")
            Me.Deptlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDAR-DEPT")
            Me.UserIDlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDAR-USER")
            Me.datelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDAR-DATE")
            Me.LoginTimelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDAR-LOGTIME")
            Me.ScreenIDlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDAR-SCREEN")

            Me.Contactlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDAR-CONTACT")
            Me.Sortlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-SORTBY")
            Me.Fromlab1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Me.Fromlab2.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Me.Fromlab3.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Me.Fromlab4.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Me.Fromlab5.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Me.Fromlab6.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Me.Tolab1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Me.Tolab2.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Me.Tolab3.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Me.Tolab4.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Me.Tolab5.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Me.Tolab6.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")

            reportviewer1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-VIEWREPORT")

            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CUSTDATAACCESS")

            'Me.contact.Items.Clear()
            'If fintcdarid = 0 Then
            Dim dltype As New clsrlconfirminf()
            Dim dlParam As ArrayList = New ArrayList
            dlParam = dltype.searchconfirminf("YESNO")
            Dim dlcount As Integer
            Dim dlid As String
            Dim dlnm As String
            For dlcount = 0 To dlParam.Count - 1
                dlid = dlParam.Item(dlcount)
                dlnm = dlParam.Item(dlcount + 1)
                dlcount = dlcount + 1

                Me.contact.Items.Add(New ListItem(dlnm.ToString(), dlid.ToString()))

            Next
            Me.contact.Items.Insert(0, "")
            Dim sotrnm1 As String = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDAR-USER")
            Dim sotrnm2 As String = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDAR-SCREEN")
            Me.sorby.Items.Add(New ListItem(sotrnm1, "MUSR_USRID"))
            Me.sorby.Items.Add(New ListItem(sotrnm2, "SADC_FNCID"))

            '''access control'''''''''''''''''''''''''''''''''''''''''''''''
            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "53")
            Me.reportviewer1.Enabled = purviewArray(3)


            'End 
            'fintcdarid = 1
            Me.maxsrevice.Text = "ZZZ"
            Me.maxuserid.Text = "ZZZ"
            Me.maxscreenid.Text = "ZZZ"
            Me.maxdept.Text = "ZZZ"
            Me.mindate.Text = Convert.ToDateTime("01/01/2006")
            Me.minlogtime.Text = Convert.ToDateTime("01/01/2006")
            Me.maxdate.Text = Date.Today()
            Me.maxlogtime.Text = Date.Today()
            Me.minsrevice.Text = "AAA"
            Me.minscreenid.Text = "AAA"
            Me.mindept.Text = "AAA"
            Me.minuserid.Text = "AAA"
        ElseIf Session("RPTCDARflag") = "1" Then
            datasearch()
        End If

        HypCalmindate.NavigateUrl = "javascript:DoCal(document.form1.mindate);"
        HypCalminlogtime.NavigateUrl = "javascript:DoCal(document.form1.minlogtime);"

        HypCalmaxdate.NavigateUrl = "javascript:DoCal(document.form1.maxdate);"
        HypCalmaxlogtime.NavigateUrl = "javascript:DoCal(document.form1.maxlogtime);"
    End Sub

   
    Public Sub datasearch()
        Dim clsReport As New ClsCommonReport
        Dim clsrptcdar As New ClsRptCDAR
        clsrptcdar.userid = Session("userID")
        clsrptcdar.username = Session("username")


        mindate.Text = Request.Form("mindate")
        minlogtime.Text = Request.Form("minlogtime")

        maxdate.Text = Request.Form("maxdate")
        maxlogtime.Text = Request.Form("maxlogtime")


        If (Trim(Me.minsrevice.Text) <> "AAA") Then

            clsrptcdar.Minsvcid = minsrevice.Text.ToUpper()

        End If
        If (Trim(Me.maxsrevice.Text) <> "ZZZ") Then

            clsrptcdar.Maxsvcid = maxsrevice.Text.ToUpper()
        Else
            clsrptcdar.Maxsvcid = "ZZZZZZZZZZ"
        End If
        If (Trim(Me.minuserid.Text) <> "AAA") Then

            clsrptcdar.Minusrid = minuserid.Text.ToUpper()

        End If
        If (Trim(Me.maxuserid.Text) <> "ZZZ") Then

            clsrptcdar.Maxusrid = maxuserid.Text.ToUpper()
        Else
            clsrptcdar.Maxusrid = "ZZZZZZZZZZZZZZZZZZZZ"

        End If
        If (Trim(Me.mindept.Text) <> "AAA") Then

            clsrptcdar.Mindept = mindept.Text.ToUpper()

        End If
        If (Trim(Me.maxdept.Text) <> "ZZZ") Then

            clsrptcdar.Maxdept = maxdept.Text.ToUpper()
        Else
            clsrptcdar.Maxdept = "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ"


        End If



        If (Trim(Me.mindate.Text) <> "") Then
            If (Trim(Me.mindate.Text) <> "01/01/2006") Then
                Dim temparr As Array = mindate.Text.Split("/")
                clsrptcdar.Mindate = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
                'clsrptsosa.Mindate = Calendar1.SelectedDate.Date.ToString()
            Else
                clsrptcdar.Mindate = "2006-01-01"
            End If


            'If (Trim(Me.mindate.Text) <> "") Then
            '    If (Trim(Me.mindate.Text) <> "01/01/2006") Then
            '        clsrptcdar.Mindate = Calendar1.SelectedDate.Date.ToString()
            '    Else
            '        clsrptcdar.Mindate = "2006-01-01"
            '    End If


        End If
        If (Trim(Me.maxdate.Text) <> "") Then
            If (Trim(Me.maxdate.Text) <> Date.Today()) Then
                Dim temparr As Array = maxdate.Text.Split("/")
                clsrptcdar.Maxdate = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
            Else
                clsrptcdar.Maxdate = Date.Today()
            End If
        End If

        If (Trim(Me.minlogtime.Text) <> "") Then
            If (Trim(Me.minlogtime.Text) <> "01/01/2006") Then
                Dim temparr As Array = minlogtime.Text.Split("/")
                clsrptcdar.Minlogtime = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
            Else
                clsrptcdar.Minlogtime = "2006-01-01"
            End If
        End If

        If (Trim(Me.maxlogtime.Text) <> "") Then

            If (Trim(Me.maxlogtime.Text) <> Date.Today()) Then
                Dim temparr As Array = maxlogtime.Text.Split("/")

                clsrptcdar.Maxlogtime = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
            Else
                clsrptcdar.Maxlogtime = Date.Today()
            End If
        End If

        If (Trim(Me.minscreenid.Text) <> "AAA") Then

            clsrptcdar.Minscreenid = minscreenid.Text.ToUpper()

        End If
        If (Trim(Me.maxscreenid.Text) <> "ZZZ") Then

            clsrptcdar.Maxscreenid = maxscreenid.Text.ToUpper()
        Else
            clsrptcdar.Maxscreenid = "ZZZZZZZZZZ"
        End If

        If (Me.contact.SelectedItem.Value.ToString() <> "") Then
            clsrptcdar.Srtel = contact.SelectedItem.Value.ToString()
        End If
        If (Me.sorby.SelectedItem.Value.ToString() <> "") Then
            clsrptcdar.Sortby = sorby.SelectedItem.Value.ToString()
        End If


        Dim rank As String = Session("login_rank")
        If rank <> 0 Then
            clsrptcdar.ctryid = Session("login_ctryID")
            clsrptcdar.compid = Session("login_cmpID")
            clsrptcdar.svcid = Session("login_svcID")

        End If
        clsrptcdar.rank = rank
        clsrptcdar.rank = rank
        'Dim rptcdar As DataSet = clsrptcdar.GetRptsysa()
        'Session("rptcdar_ds") = rptcdar
        Session("rptcdar_search_condition") = clsrptcdar
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Dim clsrptcdarMinsvcid As String = clsrptcdar.Minsvcid
        'clsrptcdarMinsvcid = Server.UrlEncode(clsrptcdarMinsvcid)

        'Dim clsrptcdarMaxsvcid As String = clsrptcdar.Maxsvcid
        'clsrptcdarMaxsvcid = Server.UrlEncode(clsrptcdarMaxsvcid)

        'Dim clsrptcdarMinusrid As String = clsrptcdar.Minusrid
        'clsrptcdarMinusrid = Server.UrlEncode(clsrptcdarMinusrid)

        'Dim clsrptcdarMaxusrid As String = clsrptcdar.Maxusrid
        'clsrptcdarMaxusrid = Server.UrlEncode(clsrptcdarMaxusrid)

        'Dim clsrptcdarMindept As String = clsrptcdar.Mindept
        'clsrptcdarMindept = Server.UrlEncode(clsrptcdarMindept)

        'Dim clsrptcdarMaxdept As String = clsrptcdar.Maxdept
        'clsrptcdarMaxdept = Server.UrlEncode(clsrptcdarMaxdept)

        'Dim clsrptcdarMindate As String = clsrptcdar.Mindate
        'clsrptcdarMindate = Server.UrlEncode(clsrptcdarMindate)

        'Dim clsrptcdarMaxdate As String = clsrptcdar.Maxdate
        'clsrptcdarMaxdate = Server.UrlEncode(clsrptcdarMaxdate)

        'Dim clsrptcdarMinlogtime As String = clsrptcdar.Minlogtime
        'clsrptcdarMinlogtime = Server.UrlEncode(clsrptcdarMinlogtime)

        'Dim clsrptcdarMaxlogtime As String = clsrptcdar.Maxlogtime
        'clsrptcdarMaxlogtime = Server.UrlEncode(clsrptcdarMaxlogtime)

        'Dim clsrptcdarMinscreenid As String = clsrptcdar.Minscreenid
        'clsrptcdarMinscreenid = Server.UrlEncode(clsrptcdarMinscreenid)

        'Dim clsrptcdarMaxscreenid As String = clsrptcdar.Maxscreenid
        'clsrptcdarMaxscreenid = Server.UrlEncode(clsrptcdarMaxscreenid)

        'Dim clsrptcdarSrtel As String = clsrptcdar.Srtel
        'clsrptcdarSrtel = Server.UrlEncode(clsrptcdarSrtel)

        'Dim clsrptcdarSortby As String = clsrptcdar.Sortby
        'clsrptcdarSortby = Server.UrlEncode(clsrptcdarSortby)


        ''Dim ctrystat As String = ds.Tables(0).Rows(e.NewEditIndex).Item(3).ToString
        ''ctrystat = Server.UrlEncode(ctrystat)
        ''Dim Prefix As String = custid.ToString.Substring(0, 2)
        ''Prefix = Server.UrlEncode(Prefix)
        'Dim strTempURL As String = "&clsrptcdarMinsvcid=" + clsrptcdarMinsvcid + "&clsrptcdarMaxsvcid=" + clsrptcdarMaxsvcid + "&clsrptcdarMinusrid=" + clsrptcdarMinusrid + "&clsrptcdarMaxusrid=" + clsrptcdarMaxusrid
        'strTempURL = strTempURL + "&clsrptcdarMindept=" + clsrptcdarMindept + "&clsrptcdarMaxdept=" + clsrptcdarMaxdept + "&clsrptcdarMindate=" + clsrptcdarMindate + "&clsrptcdarMaxdate=" + clsrptcdarMaxdate
        'strTempURL = strTempURL + "&clsrptcdarMinlogtime=" + clsrptcdarMinlogtime + "&clsrptcdarMaxlogtime=" + clsrptcdarMaxlogtime + "&clsrptcdarMinscreenid=" + clsrptcdarMinscreenid + "&clsrptcdarMaxscreenid=" + clsrptcdarMaxscreenid
        'strTempURL = strTempURL + "&clsrptcdarSrtel=" + clsrptcdarSrtel + "&clsrptcdarSortby=" + clsrptcdarSortby
        ''strTempURL = "~/PresentationLayer/REPORT/RPTCADRVIEW.aspx" + strTempURL
        'Response.Redirect(strTempURL)
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      

    End Sub

   
    Protected Sub reportviewer1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles reportviewer1.Click
        Session("RPTCDARflag") = "1"
        datasearch()
        'Response.Redirect("~/PresentationLayer/REPORT/RPTCADRVIEW.aspx")
        Dim script As String = "window.open('../report/RPTCADRVIEW.aspx')"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "SystemAccessReport", script, True)
    End Sub

    Protected Sub Calendar1_CalendarVisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendar1.CalendarVisibleChanged
        
        sorby.Visible = Not Calendar1.CalendarVisible

    End Sub

    Protected Sub Calendar3_CalendarVisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendar3.CalendarVisibleChanged
        sorby.Visible = Not Calendar3.CalendarVisible
    End Sub
End Class
