﻿Imports BusinessEntity
Imports System.Configuration
Imports System.Data
Imports System.Data.SqlClient
Imports System.Xml
Partial Class PresentationLayer_Report_RPTTSKH
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If

        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try


        If Not Page.IsPostBack Then


            
            Session("rptTSKHflag") = "0"
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            Me.from1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Me.from2.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Me.from3.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")

            Me.to1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Me.to2.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Me.to3.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")

            Me.techlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "RPT_TSKH_TECHID")
            Me.serlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "RPT_TSKH_SER")
            Me.datelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "RPT_TSKH_DATE")
            'Me.osllab.Text = objXmlTr.GetLabelName("EngLabelMsg", "RPT_TSKH_CANBY")

            Me.LinkButton1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-VIEWREPORT")
            Me.titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "RPT_TSKH_TITLE")

            'Me.HyperLink1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-VIEWREPORT")

            Me.date1.Text = "01/01/2006"
            Me.date2.Text = "31/12/2006"

            '''access control'''''''''''''''''''''''''''''''''''''''''''''''
            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "50")
            Me.LinkButton1.Enabled = purviewArray(3)


            Dim Appbond As New clsCommonClass
            Dim rank As Integer = Session("login_rank")
            Appbond.rank = rank
            If rank <> 0 Then
                Appbond.spctr = Session("login_ctryID")
                Appbond.spstat = Session("login_cmpID")
                Appbond.sparea = Session("login_svcID")
            End If
        ElseIf Session("rptTSKHflag") = "1" Then
            print_report()
        End If



        HypCalFrom.NavigateUrl = "javascript:DoCal(document.form1.date1);"
        HypCalTo.NavigateUrl = "javascript:DoCal(document.form1.date2);"
    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton1.Click
        Session("rptTSKHflag") = "1"
        print_report()
        'Response.Redirect("~/PresentationLayer/report/RPTTSKH_SUB.aspx")
        Dim script As String = "window.open('../report/RPTTSKH_SUB.aspx')"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "SystemAccessReport", script, True)
    End Sub
    Public Function print_report()
        Try
            
            Dim RptTSKH As New ClsRptTSKH

            date1.Text = Request.Form("date1")
            date2.Text = Request.Form("date2")

           
            If (Trim(Me.sercen1.Text) <> "AAA") Then

                RptTSKH.Minserid = sercen1.Text.ToUpper()

            End If
            If (Trim(Me.sercen2.Text) <> "ZZZ") Then

                RptTSKH.Maxserid = sercen2.Text.ToUpper()
            Else
                RptTSKH.Maxserid = "ZZZZZZZZZZ"

            End If
            If (Trim(Me.techni1.Text) <> "AAA") Then

                RptTSKH.Mintechid = techni1.Text.ToUpper()

            End If
            If (Trim(Me.techni2.Text) <> "ZZZ") Then

                RptTSKH.Maxtechid = techni2.Text.ToUpper()
            Else
                RptTSKH.Maxtechid = "ZZZZZZZZZZ"


            End If
            If (Trim(Me.date1.Text) <> "") Then
                
                If (Trim(Me.date1.Text) <> "01/01/2006") Then
                    Dim temparr As Array = date1.Text.Split("/")
                    RptTSKH.Mindate = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
                Else
                    RptTSKH.Mindate = "2006-01-01"
                End If

            End If

            If (Trim(Me.date2.Text) <> "") Then
                If (Trim(Me.date2.Text) <> "31/12/2006") Then


                    Dim temparr As Array = date2.Text.Split("/")
                    RptTSKH.Maxdate = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
                Else
                    RptTSKH.Maxdate = "2006-12-31"
                End If
            End If

                Dim rank As String = Session("login_rank")
                If rank <> 0 Then
                    RptTSKH.ctrid = Session("login_ctryID").ToString().ToUpper
                    RptTSKH.comid = Session("login_cmpID").ToString().ToUpper
                    RptTSKH.svcid = Session("login_svcID").ToString().ToUpper
                End If
                RptTSKH.rank = rank

            'Dim masds As DataSet = RptTSKH.GetRptmas_TSKH()
                'masds.Relations.Add("dd",masds.Tables (0).Columns ("")

            'Session("rptTSKH_ds") = masds
                Session("rptTSKH_search_condition") = RptTSKH



                'Dim objXmlTr As New clsXml
                'Dim strHead As Array = Array.CreateInstance(GetType(String), 18)
                'objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
                'strHead(0) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_TSKH_PARD")
                'strHead(1) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_TSKH_SSL")
                'strHead(2) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_TSKH_SOTD")
                'strHead(3) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_TSKH_ASOH")
                'strHead(4) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_TSKH_PLQ")
                'strHead(5) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_TSKH_PS")
                'strHead(6) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_TSKH_NO")
                'strHead(7) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_TSKH_DT")
                'strHead(8) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_TSKH_QUAN")
                'strHead(9) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_TSKH_ACCU")
                'strHead(10) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_TSKH_TRTY")
                'strHead(11) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_TSKH_TRNO")
                'strHead(12) = objXmlTr.GetLabelName("EngLabelMsg", "RPT-TSKH-TITLE1")
                'strHead(13) = objXmlTr.GetLabelName("EngLabelMsg", "RPT-TSKH-TITLE2")
                'strHead(14) = Me.date1.Text
                'strHead(15) = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
                'strHead(16) = Me.date2.Text
                'strHead(17) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_TSKH_REPID")

                'If Me.IsPostBack Then
                '    With clsReport
                '        .ReportFileName = "RPTTSKH.rpt"
                '        .SetReport(CrystalReportViewer1, masds, strHead)
                '    End With

                'End If
        Catch ex As Exception
            Response.Write(ex.Message)

        End Try
    End Function

    'Protected Sub effecalender_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles effecalender.Click
    '    Me.Calendar1.Visible = True
    '    Calendar1.Attributes.Add("style", " POSITION: absolute")
    '    Session("rptSBTflag") = "0"
    'End Sub

    'Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
    '    Me.Calendar2.Visible = True
    '    Calendar2.Attributes.Add("style", " POSITION: absolute")
    '    Session("rptSBTflag") = "0"
    'End Sub

    'Protected Sub Calendar1_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendar1.SelectionChanged
    '    Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
    '    System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
    '    Dim prcidDate As New clsCommonClass()
    '    Me.date1.Text = Calendar1.SelectedDate.Date.ToString().Substring(0, 10)
    '    Calendar1.Visible = False
    '    Session("rptSBTflag") = "0"
    'End Sub

    'Protected Sub Calendar2_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendar2.SelectionChanged
    '    Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
    '    System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
    '    Dim prcidDate As New clsCommonClass()
    '    Me.date2.Text = Calendar2.SelectedDate.Date.ToString().Substring(0, 10)
    '    Calendar2.Visible = False
    '    Session("rptSBTflag") = "0"
    'End Sub
End Class
