﻿
Imports BusinessEntity
Imports System.Configuration
Imports System.Xml
Imports System.Data
Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web
Partial Class PresentationLayer_report_RPTRAPT_SUB2
    Inherits System.Web.UI.Page
    Dim clsReport As New ClsCommonReport
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        Try
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Response.Redirect("~/PresentationLayer/logon.aspx")
                End If
            Catch ex As Exception
                Response.Redirect("~/PresentationLayer/logon.aspx")
            End Try

            Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Dim obiXML As New clsXml
            'obiXML.XmlFile = ConfigurationManager.AppSettings("XmlFilePath")
            Me.titleLab.Text = "Repeated Appointment/Services" 'obiXML.GetLabelName("EngLabelMsg", "RPT_APT_TITLE")
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Dim RepeatEntity As New ClsRPTRAPT()

            If Trim(Session("userID")) = "" Then
                RepeatEntity.Username = "  "
            Else
                RepeatEntity.Username = Session("userID")
            End If

            RepeatEntity = Session("rptRAPT_search_condition")

            Dim reportds As DataSet = Nothing
            reportds = RepeatEntity.GetRepeatedSer()





            'Dim reportds As DataSet = RepeatEntity.GetRepeatedSer()
            ''Dim objXmlTr As New clsXml



            Dim strHead As Array = Array.CreateInstance(GetType(String), 41)
            ''objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            strHead(0) = "Serial No" ''objXmlTr.GetLabelName("EngLabelMsg", "RPT_APT_SERNO")
            strHead(1) = "Service Date" ''objXmlTr.GetLabelName("EngLabelMsg", "RPT_APT_SERDATE")
            strHead(2) = "Technician ID" ''objXmlTr.GetLabelName("EngLabelMsg", "RPT_APT_TECHID")
            ' Modified by Ryan Estandarte 5 Sept 2012
            'strHead(3) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_APT_APPSTAT")
            'strHead(4) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_APT_NSTDT") 
            strHead(3) = "Part Change" ''objXmlTr.GetLabelName("EngLabelMsg", "RPT_APT_PART") ' part change
            strHead(4) = "Action Taken" ''objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCHL-0027") ' action taken

            strHead(5) = "Remarks" ''objXmlTr.GetLabelName("EngLabelMsg", "RPT_APT_REM")
            strHead(6) = "Repeated Appointment/Services" ''objXmlTr.GetLabelName("EngLabelMsg", "RPT_APT_TITLE")
            strHead(7) = reportds.Tables(0).Rows.Count
            strHead(8) = "Total Records" ''objXmlTr.GetLabelName("EngLabelMsg", "RPT_APT_RECORD")
            strHead(9) = "RPTRAPT /" ''objXmlTr.GetLabelName("EngLabelMsg", "RPT_APT_REPID")
            strHead(10) = "Elken Group of Companies" ''objXmlTr.GetLabelName("EngLabelMsg", "RPT-APT-TITLE1")
            strHead(11) = "Processing for Repeated Appointment/Services from" ''objXmlTr.GetLabelName("EngLabelMsg", "RPT-APT-TITLE2")
            strHead(12) = RepeatEntity.Datetime1
            strHead(13) = "To" ''objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            strHead(14) = RepeatEntity.Datetime2

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            strHead(15) = "To" ''objXmlTr.GetLabelName("EngLabelMsg", "BB-TO") 'objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            strHead(16) = "From" ''objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            strHead(17) = "Service Center" ''objXmlTr.GetLabelName("EngLabelMsg", "RPT_APT_SER")
            strHead(18) = "Date" ''objXmlTr.GetLabelName("EngLabelMsg", "RPT_APT_DATE")
            strHead(19) = "Customer ID" ''objXmlTr.GetLabelName("EngLabelMsg", "RPT_APT_CUSID")
            strHead(20) = "Serial No" ''objXmlTr.GetLabelName("EngLabelMsg", "RPT_APT_SERLID")
            strHead(21) = "Model ID" ''objXmlTr.GetLabelName("EngLabelMsg", "RPT_APT_MODELID")
            strHead(22) = "Repeat HZ" ''objXmlTr.GetLabelName("EngLabelMsg", "RPT_APT_REP")
            strHead(23) = "Service Type" ''objXmlTr.GetLabelName("EngLabelMsg", "RPT_APT_SERTY")



            If (RepeatEntity.ServiceID1 = Nothing) Then
                strHead(24) = "AAA"
            Else

                strHead(24) = RepeatEntity.ServiceID1
            End If


            If (RepeatEntity.ServiceID2 = "ZZZZZZZZZZ") Then
                strHead(25) = "ZZZ"
            Else

                strHead(25) = RepeatEntity.ServiceID2
            End If

            If (RepeatEntity.CustomerID1 = Nothing) Then
                strHead(26) = "AAA"
            Else

                strHead(26) = RepeatEntity.CustomerID1
            End If

            If (RepeatEntity.CustomerID2 = "ZZZZZZZZZZ") Then
                strHead(27) = "ZZZ"
            Else

                strHead(27) = RepeatEntity.CustomerID2
            End If

            If (RepeatEntity.SerialNo1 = Nothing) Then
                strHead(28) = "AAA"
            Else

                strHead(28) = RepeatEntity.SerialNo1
            End If

            If (RepeatEntity.SerialNo2 = "ZZZZZZZZZZ") Then
                strHead(29) = "ZZZ"
            Else

                strHead(29) = RepeatEntity.SerialNo2
            End If

            If (RepeatEntity.Model1 = Nothing) Then
                strHead(30) = "AAA"
            Else

                strHead(30) = RepeatEntity.Model1
            End If

            If (RepeatEntity.Model2 = "ZZZZZZZZZZ") Then
                strHead(31) = "ZZZ"
            Else

                strHead(31) = RepeatEntity.Model2
            End If


            If (RepeatEntity.sertype_sub = Nothing) Then
                strHead(32) = " "
            Else

                strHead(32) = RepeatEntity.sertype_sub
            End If




            If (RepeatEntity.Datetime1 = Nothing) Then
                strHead(33) = " "
            Else

                strHead(33) = RepeatEntity.Datetime1
            End If


            If (RepeatEntity.Datetime2 = Nothing) Then
                strHead(34) = " "
            Else

                strHead(34) = RepeatEntity.Datetime2
            End If



            strHead(35) = RepeatEntity.Reapted

            If (RepeatEntity.Reaptednum = "w") Then
                strHead(36) = " "
            Else

                strHead(36) = RepeatEntity.Reaptednum
            End If

            If Session("userID") = "" Then
                strHead(37) = " "
            Else
                strHead(37) = Session("userID").ToString().ToUpper + " / " + Session("username").ToString.ToUpper

            End If
            strHead(38) = "User:" ''objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCUSR-USERID")
            ' Modified by Ryan Estandarte 4 Sept 2012
            'strHead(39) = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRID")
            strHead(39) = String.Empty ' remove

            strHead(40) = "Appointment No" ''objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSOSA-APPTNO")

            'Dim lstrPdfFileName As String = "RepeatedAppointmentReport_" & Session("login_session") & ".pdf"

            Try
                ' Modified by Ryan Estandarte
                'With clsReport
                '    .ReportFileName = "RPTRAPT.rpt"
                '    .SetReportDocument(reportds, strHead)
                '    .PdfFileName = lstrPdfFileName
                '    .ExportPdf()
                'End With

                'Response.Redirect(clsReport.PdfUrl)

                With clsReport
                    .ReportFileName = "RPTRAPT.rpt"
                    .SetReport(CrystalReportViewer1, reportds, strHead)
                End With

            Catch err As Exception
                'System.IO.File.Delete(lstrPhysicalFile)
                Response.Write("<BR>")
                Response.Write(err.Message.ToString)

            End Try

        Catch ex As Exception
            Response.Write(ex.Message)

        End Try
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        clsReport.UnloadReport()
        CrystalReportViewer1.Dispose()
    End Sub
End Class
