Imports SQLDataAccess
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
Imports BusinessEntity
Imports CrystalDecisions.Web
Imports CrystalDecisions.CrystalReports.Engine

Partial Class PresentationLayer_report_RPTCADRVIEW
    Inherits System.Web.UI.Page
    Dim clsReport As New ClsCommonReport

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Response.Redirect("~/PresentationLayer/logon.aspx")
            End If
        Catch ex As Exception
            Response.Redirect("~/PresentationLayer/logon.aspx")
        End Try
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Dim obiXML As New clsXml
        obiXML.XmlFile = ConfigurationManager.AppSettings("XmlFilePath")
        Me.titleLab.Text = obiXML.GetLabelName("EngLabelMsg", "BB-RPTCDAR-TITLE2")

        Dim clsrptcdar As New ClsRptCDAR
        clsrptcdar.userid = Session("userID")
        clsrptcdar.username = Session("username")
        
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        
        'clsrptcdar.Minsvcid = Request.Params("clsrptcdarMinsvcid")
        'clsrptcdar.Maxsvcid = Request.Params("clsrptcdarMaxsvcid")
        'clsrptcdar.Minusrid = Request.Params("clsrptcdarMinusrid")
        'clsrptcdar.Maxusrid = Request.Params("clsrptcdarMaxusrid")
        'clsrptcdar.Mindept = Request.Params("clsrptcdarMindept")
        'clsrptcdar.Maxdept = Request.Params("clsrptcdarMaxdept")
        'clsrptcdar.Mindate = Request.Params("clsrptcdarMindate")
        'clsrptcdar.Maxdate = Request.Params("clsrptcdarMaxdate")
        'clsrptcdar.Minlogtime = Request.Params("clsrptcdarMinlogtime")
        'clsrptcdar.Maxlogtime = Request.Params("clsrptcdarMaxlogtime")
        'clsrptcdar.Minscreenid = Request.Params("clsrptcdarMinscreenid")
        'clsrptcdar.Maxscreenid = Request.Params("clsrptcdarMaxscreenid")
        'clsrptcdar.Srtel = Request.Params("clsrptcdarSrtel")
        'clsrptcdar.Sortby = Request.Params("clsrptcdarSortby")




        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        

        clsrptcdar = Session("rptcdar_search_condition")
        Dim ds As DataSet = clsrptcdar.GetRptsysa()
        'ds = Session("rptcdar_ds")
        'ds = clsrptcdar.GetRptsysa()
        'clsrptcdar = Session("rptcdar_search_condition")


        Dim statXmlTr As New clsXml
        statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
       
       
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      
        Dim objXmlTr As New clsXml
        Dim strHead As Array = Array.CreateInstance(GetType(String), 43)
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        strHead(0) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDAR-NUMB")
        strHead(1) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDAR-USERNM")
        strHead(2) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDAR-USER")
        strHead(3) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDAR-DEPTID")

        strHead(4) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDAR-SCREEN")
        strHead(5) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDAR-SQL")
        strHead(6) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDAR-RECODE")
        strHead(7) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDAR-REPORT")
        strHead(8) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDAR-TITLE1")
        strHead(9) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDAR-REPORTID")
        strHead(10) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDAR-TITLE2")
        strHead(11) = clsrptcdar.Mindate
        strHead(12) = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
        strHead(13) = clsrptcdar.Maxdate
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        strHead(14) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDAR-SVC")
        strHead(15) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDAR-DEPT")
        strHead(16) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDAR-USER")
        strHead(17) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDAR-DATE")
        strHead(18) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDAR-LOGTIME")
        strHead(19) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDAR-SCREEN")

        strHead(20) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDAR-CONTACT")
        strHead(21) = objXmlTr.GetLabelName("EngLabelMsg", "BB-SORTBY")
        strHead(22) = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
        
        strHead(23) = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        If clsrptcdar.Minsvcid <> "" Then
            strHead(24) = clsrptcdar.Minsvcid
        Else
            strHead(24) = "AAA".ToString()
        End If
        If clsrptcdar.Maxsvcid <> "ZZZZZZZZZZ" Then
            strHead(25) = clsrptcdar.Maxsvcid
        Else
            strHead(25) = "ZZZ".ToString()
        End If
        If clsrptcdar.Minusrid <> "" Then
            strHead(26) = clsrptcdar.Minusrid
        Else
            strHead(26) = "AAA".ToString()
        End If
        If clsrptcdar.Maxusrid <> "ZZZZZZZZZZZZZZZZZZZZ" Then
            strHead(27) = clsrptcdar.Maxusrid
        Else
            strHead(27) = "ZZZ".ToString()
        End If
        If clsrptcdar.Mindept <> "" Then
            strHead(28) = clsrptcdar.Mindept
        Else
            strHead(28) = "AAA".ToString()
        End If
        If clsrptcdar.Maxdept <> "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ" Then
            strHead(29) = clsrptcdar.Maxdept
        Else
            strHead(29) = "ZZZ".ToString()
        End If
        If clsrptcdar.Mindate <> "" Then
            strHead(30) = clsrptcdar.Mindate
        Else
            strHead(30) = " ".ToString()
        End If

        If clsrptcdar.Maxdate <> "" Then
            strHead(31) = clsrptcdar.Maxdate
        Else
            strHead(31) = " ".ToString()
        End If

        If clsrptcdar.Minlogtime <> "" Then
            strHead(32) = clsrptcdar.Minlogtime
        Else
            strHead(32) = " ".ToString
        End If
        If clsrptcdar.Maxlogtime <> "" Then
            strHead(33) = clsrptcdar.Maxlogtime
        Else
            strHead(33) = " ".ToString()
        End If
        If clsrptcdar.Minscreenid <> "" Then
            strHead(34) = clsrptcdar.Minscreenid
        Else
            strHead(34) = "AAA".ToString()
        End If
        If clsrptcdar.Maxscreenid <> "ZZZZZZZZZZ" Then
            strHead(35) = clsrptcdar.Maxscreenid
        Else
            strHead(35) = "ZZZ".ToString()
        End If
        If clsrptcdar.Srtel <> "" Then

            If clsrptcdar.Srtel = "Y" Then
                strHead(36) = statXmlTr.GetLabelName("StatusMessage", "Y")
            End If
            If clsrptcdar.Srtel = "N" Then
                strHead(36) = statXmlTr.GetLabelName("StatusMessage", "N")
            End If
        Else
            strHead(36) = "ALL".ToString()
        End If
        If clsrptcdar.Sortby <> "" Then

            If clsrptcdar.Sortby = "MUSR_USRID" Then
                strHead(37) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDAR-USER")
            End If
            If clsrptcdar.Sortby = "SADC_FNCID" Then
                strHead(37) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDAR-SCREEN")
            End If
        Else
            strHead(37) = " ".ToString()
        End If
        strHead(38) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCUSR-USERID")

        strHead(39) = Session("userID") + "/" + Session("username")

        strHead(40) = ds.Tables(0).Rows.Count
        strHead(41) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_APT_RECORD")
        strHead(42) = "/"

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


        With clsReport
            .ReportFileName = "RptCDAR.rpt"  'TextBox1.Text
            .SetReport(CrystalReportViewer1, ds, strHead)


        End With


    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        clsReport.UnloadReport()
        CrystalReportViewer1.Dispose()
    End Sub
End Class
