<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ContractUnitEntryReport.aspx.vb" Inherits="PresentationLayer_report_ContractUnitEntryReport"  EnableEventValidation="false"%>

<%@ Register Assembly="JCalendar" Namespace="JCalendar" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Contract Unit Entry Report</title>
    <link href="../css/style.css" type="text/css" rel="stylesheet">
    <script language="JavaScript" src="../js/common.js"></script>
</head>
<body style="font-family: Verdana">
    <form id="form1" runat="server">
        <div style="vertical-align: top">
            <table style="width: 100%" border="0">
                <tr>
                    <td style="height: 40px">
                        <table cellspacing="0" cellpadding="0" width="100%" border="0">
                            <tr>
                                <td width="1%" background="../graph/title_bg.gif" style="height: 24px">
                                    <img height="24" src="../graph/title1.gif" width="5"></td>
                                <td class="style2" width="98%" background="../graph/title_bg.gif" style="width: 80%; height: 24px;">
                                    <asp:Label ID="titleLab" runat="server" Text="Contract Unit Entry Report"></asp:Label></td>
                                <td align="LEFT" background="../graph/title_bg.gif" style="width: 1%; height: 24px;">
                                    <img height="24" src="../graph/title_2.gif" width="5"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="1" cellpadding="2" bgcolor="#b7e6e6" border="0" style="width: 100%">
                            <tr bgcolor="#ffffff">
                                <td align="left" style="width: 20%; height: 30px">
                                    <asp:Label ID="lblCustomerName" runat="server" Text="Contact Type from"></asp:Label>&nbsp;</td>
                                <td style="width: 20%; height: 30px">
                                    <asp:DropDownList ID="ddlType" runat="server">
                                    </asp:DropDownList></td>
                                <td align="left" style="width: 10%; height: 30px">
                                    <asp:Label ID="Label1" runat="server" Text="Contact Type to"></asp:Label></td>
                                <td style="width: 314px; height: 30px"><asp:DropDownList ID="ddlTypeto" runat="server">
                                </asp:DropDownList></td>
                            </tr>
                            
                            <tr bgcolor="#ffffff">
                                <td align="LEFT" style="width: 20%">
                                    <asp:Label ID="apptStartDate" runat="server" Text="Contract Start Date from"></asp:Label>
                                </td>
                                <td style="width: 20%">
                                    <asp:TextBox ID="txtPreInsDateFr" runat="server" Width="70%" CssClass="textborder" MaxLength =10
                                        ReadOnly="false" Height="23px"></asp:TextBox><asp:HyperLink ID="HypCal" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif" ToolTip="Choose a Date" EnableTheming="True">Choose a Date</asp:HyperLink>
                                    
                                    <cc1:JCalendar ID="apptStartDateJCalendar" runat="server" Visible=false ImgURL="~/PresentationLayer/graph/calendar.gif" ControlToAssign="txtPreInsDateFr" />
                                </td>
                                <td align="LEFT" style="width: 20%">
                                    <asp:Label ID="apptEndDate" runat="server" Text="Contract Start Date to"></asp:Label>
                                </td>
                                <td style="width: 314px">
                                    <asp:TextBox ID="txtPreInsDateTo" runat="server" Width="70%" CssClass="textborder" MaxLength =10
                                        ReadOnly="false"></asp:TextBox><asp:HyperLink ID="HypCal2" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif" ToolTip="Choose a Date">Choose a Date</asp:HyperLink>
                                    
                                    <cc1:JCalendar ID="apptEndDateCalendar" runat="server" Visible=false ImgURL="~/PresentationLayer/graph/calendar.gif" ControlToAssign="txtPreInsDateTo" />  
                                </td>
                               
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="left" style="width: 20%; height: 46px;">
                                    <asp:Label ID="lblAppointmentType" runat="server" Text="Service Center from"></asp:Label></td>
                                <td style="width: 20%; height: 46px;">
                                    <asp:DropDownList ID="ddlSVCFr" runat="server" AutoPostBack="True" Width="99%">
                                    </asp:DropDownList></td>
                                <td align="left" style="width: 20%; height: 46px;">
                                    <asp:Label ID="lblTechnicianID" runat="server" Text="Service Center to"></asp:Label></td>
                                <td style="width: 314px; height: 46px;">
                                    <asp:DropDownList ID="ddlSVCTo" runat="server" Width="100%" AutoPostBack="True">
                                    </asp:DropDownList></td>
                            </tr>
                            <%--Modified by Ryan Estandarte 12 Oct 2012--%>
                            <%--<tr>
                                <td style="width: 10%">  
                                    
                                </td>
                            </tr>--%>
								
                            <tr bgcolor="#ffffff">
                                <td align="LEFT" style="width: 20%" rowspan="2">
                                    Customer Type</td>
                                <td align="left" rowspan="2" style="width: 20%"><asp:DropDownList ID="ddlcusttype" runat="server" AutoPostBack="True" Width="99%">
                                </asp:DropDownList></td>
                                <td style="width: 10%; height: 67px;">
                                    <asp:LinkButton ID="searchButton" runat="server">Search</asp:LinkButton></td>
                                <td style="height: 67px; width: 314px;">
                                    <%--Modified by Ryan Estandarte 12 Oct 2012--%>
                                    <%--<asp:CheckBox ID="chkIsInbound" runat="server" Text="Yes?" />--%>
                                    &nbsp;<asp:LinkButton ID="BtnExport" runat="server">Export to Excel</asp:LinkButton></td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td style="width: 10%">
                                    </td>
                                <td style="width: 314px">
                                    </td>
                            </tr>
                        </table>
                       
                        <asp:GridView ID="appsView" runat="server" Width="100%" AllowPaging="True" AllowSorting="True"
                            Font-Size="Smaller">
                        </asp:GridView>
                        &nbsp;
                        <br />
                        
                        <asp:Label ID="lblNoRecord" runat="server" ForeColor="Red" Text="No records available!"></asp:Label>
                    </td>
                </tr>
                 <tr>
                <td align=left>
                    <asp:Label ID="lblTotalDescription" runat="server" Visible="true"></asp:Label>
                    &nbsp;<asp:Label ID="LblTotRecNo" runat="server" Visible="true" Text =0></asp:Label>
                    &nbsp; &nbsp;
                </td>
            </tr>
            </table>
        </div>
    </form>
</body>
</html>
