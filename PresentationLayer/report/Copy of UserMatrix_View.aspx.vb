Imports System.Data
Imports System.Data.SqlClient
Imports System.Globalization
Imports SQLDataAccess
Imports System.Windows
Imports Microsoft.Win32
Partial Class PresentationLayer_report_UserMatrix_View
    Inherits System.Web.UI.Page

    Dim RankA As String
    Dim RankB As String
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)
        Return connection
    End Function


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim _con As SqlConnection
        _con = GetConnection(ConfigurationManager.AppSettings("ConnectionString"))

        UserIdFr.Text = Session("UserIDFrom").ToString
        UserIDTo.Text = Session("UserIDTo").ToString
        GroupFr.Text = Session("GroupFrom").ToString
        GroupTo.Text = Session("GroupTo").ToString
        DeptFr.Text = Session("DeptFrom").ToString
        DeptTo.Text = Session("DeptTo").ToString
        'RankFr.Text = Session("RankFrom").ToString
        'RankTo.Text = Session("RankTo").ToString
        SVCFr.Text = Session("SVCFrom").ToString
        SVCTo.Text = Session("SVCTo").ToString

        'If Session("RankFrom").ToString = "Country" Then
        '    RankA = "7"
        'ElseIf Session("RankFrom").ToString = "Company" Then
        '    RankA = "8"
        'Else
        '    RankA = "9"
        'End If

        'If Session("RankTo").ToString = "Country" Then
        '    RankB = "7"
        'ElseIf Session("RankTo").ToString = "Company" Then
        '    RankB = "8"
        'Else
        '    RankB = "9"
        'End If

        '& " and MUSR_RANK >= '" & RankA & "' and MUSR_RANK <= '" & RankB & "'" _

        Using Comm As New SqlClient.SqlCommand("select MUSR_USRID, MUSR_ENAME, MUSR_GRPID, MUSR_STAT, " _
            & " [MUSR_DEPT]= CASE WHEN MUSR_DEPT = 'AC' THEN 'Account' WHEN MUSR_DEPT = 'TS' THEN 'Technician' " _
            & " WHEN MUSR_DEPT = 'CR' THEN 'Customer Relation' ELSE 'Warehouse' END," _
            & " [MUSR_RANK] = CASE WHEN MUSR_RANK = 7 THEN 'Country' WHEN MUSR_RANK = 8 THEN 'Company' ELSE 'Service Center' END," _
            & " MUSR_SVCID from MUSR_FIL" _
            & " where MUSR_STAT = 'ACTIVE'" _
            & " and MUSR_USRID >= '" & UserIdFr.Text & "' and MUSR_USRID <= '" & UserIDTo.Text & "'" _
            & " and MUSR_GRPID >= '" & GroupFr.Text & "' and MUSR_GRPID <= '" & GroupTo.Text & "'" _
            & " and MUSR_DEPT >= '" & DeptFr.Text & "' and MUSR_DEPT <= '" & DeptTo.Text & "'" _
            & " and MUSR_SVCID >= '" & SVCFr.Text & "' and MUSR_SVCID <= '" & SVCTo.Text & "'", _con)
            _con.Open()

            Dim dr1 As SqlDataReader = Comm.ExecuteReader(CommandBehavior.CloseConnection)

            Dim dt1 As DataTable = New DataTable()
            dt1.Load(dr1)

            GridView1.DataSource = dt1
            GridView1.DataBind()

            GridView1.Visible = True

            _con.Close()
        End Using




        'If Session("DataTable").ToString = "X" Then
        '    Label1.Visible = True
        'Else
        '    GridView1.DataSource = (Session("DataTable"))
        '    GridView1.DataBind()

        '    GridView1.Visible = True
        'End If

    End Sub
    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub

    Protected Sub Export_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Export.Click
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=GridView1.xls")
        Response.Charset = ""
        Response.ContentType = "application/vnd.xls"
        Dim StringWriter As IO.StringWriter = New System.IO.StringWriter()
        Dim HtmlTextWriter As New HtmlTextWriter(StringWriter)
        Dim style As String = "<style> td { mso-number-format:\@; } </style> "

        GridView1.RenderControl(HtmlTextWriter)
        Response.Write(style) 'style is added dynamically
        Response.Write(StringWriter.ToString())
        Response.[End]()
    End Sub
End Class
