Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Web
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web
Imports BusinessEntity
Imports System.Data
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Drawing
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls


Partial Class PresentationLayer_Report_RPTREPRINTPRFSINXLS
    Inherits System.Web.UI.Page
    Dim fstrMonth As String = ""
    Dim fstrDefaultStartDate As String = ""
    Dim fstrDefaultEndDate As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        fstrMonth = Month(System.DateTime.Today)
        fstrDefaultStartDate = System.DateTime.Today
        fstrDefaultEndDate = fstrDefaultStartDate 'DateAdd(DateInterval.Month, 1, CDate(fstrDefaultStartDate))

        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try

        If Not Page.IsPostBack Then
            ''display label message
            Session("Packflag") = "0"
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")

            lblServCenter.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-EXPORTPRFTOXLS-0001")

            lblDate.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-EXPORTPRFTOXLS-0002")
            lblTechnicianID.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-EXPORTPRFTOXLS-0003")

            Me.lblPrfNo.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-EXPORTPRFTOXLS-0004")

            reportviewer.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-VIEWPRFFOREXPORT")
            exportreport.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-EXPORTPRF")
            exportreport.Visible = False
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-EXPORTPRFTOXLS-TITLE")

            Me.mindate.Text = fstrDefaultStartDate
            Me.maxdate.Text = fstrDefaultStartDate

            getServiceCenter()

            '''access control'''''''''''''''''''''''''''''''''''''''''''''''
            'Dim accessgroup As String = Session("accessgroup").ToString
            'Dim purviewArray As Array = New Boolean() {False, False, False, False, False}
            'purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "47")
            'Me.reportviewer.Enabled = purviewArray(3)

        ElseIf Session("Packflag") = "1" Then
            viewreport()
        End If


        HypCalFrom.NavigateUrl = "javascript:DoCal(document.form1.mindate);"
        HypCalTo.NavigateUrl = "javascript:DoCal(document.form1.maxdate);"
    End Sub
#Region " view report message"
    Public Function viewreport()
        'set params'
        Dim packEntity As New ClsExportPRFtoXLS

        mindate.Text = Request.Form("mindate")
        maxdate.Text = Request.Form("maxdate")

        If Trim(minsercenter.Text) = "" Then
            packEntity.minsercenter = ""
        Else
            packEntity.minsercenter = Trim(minsercenter.Text)
        End If

        If Trim(mindate.Text) <> "" Then
            Dim temparr As Array = mindate.Text.Split("/")
            packEntity.mindate = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
        Else
            Dim mind As String = "01/01/1910"
            Dim temparr As Array = mind.Split("/")
            packEntity.mindate = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
        End If
        If Trim(maxdate.Text) <> "" Then
            Dim temparr2 As Array = maxdate.Text.Split("/")
            Dim int1 As Integer
            int1 = temparr2(0)
            int1 = Convert.ToInt32(int1)
            int1 = int1 + 1
            int1 = Convert.ToString(int1)
            temparr2(0) = int1
            packEntity.maxdate = temparr2(2) + "-" + temparr2(1) + "-" + temparr2(0)
        Else
            Dim maxd As String = "31/12/2089"
            Dim temparr2 As Array = maxd.Split("/")
            packEntity.maxdate = temparr2(2) + "-" + temparr2(1) + "-" + temparr2(0)
        End If


        If Trim(mintechnician.Text) = "" Then
            packEntity.mintechnician = "%"
        Else
            packEntity.mintechnician = Trim(mintechnician.Text)
        End If

        If Trim(maxtechnician.Text) = "" Then
            packEntity.maxtechnician = "%"
        Else
            packEntity.maxtechnician = Trim(maxtechnician.Text)
        End If

        packEntity.txtPrfNoFrom = Me.txtPrfNoFrom.Text.Trim.ToUpper
        packEntity.txtPrfNoTo = Me.txtPrfNoTo.Text.Trim.ToUpper

        'If Trim(txtPrfNoFrom.Text) = "" Then
        '    packEntity.txtPrfNoFrom = "%"
        'Else
        '    packEntity.txtPrfNoFrom = Me.txtPrfNoFrom.Text.Trim.ToUpper
        '    'packEntity.txtPrfNoFrom = "PRF351019003"
        'End If

        'If Trim(txtPrfNoTo.Text) = "" Then
        '    packEntity.txtPrfNoTo = "%"
        'Else
        '    'packEntity.txtPrfNoTo = "PRF351019032"
        '    packEntity.txtPrfNoTo = Me.txtPrfNoTo.Text.Trim.ToUpper
        'End If

        'Fill the dataset
        Dim ds As DataSet = packEntity.GetPrfs()
        
        If Session("ExportReport") = "1" And Session("Packflag") = "0" Then
            ' Export to xls
            packEntity.Convert(ds, Response)
            Session.Remove("ExportReport")
            Session.Remove("Packflag")
        ElseIf Session("ExportReport") = "0" And Session("Packflag") = "1" Then
            dg.DataSource = ds
            dg.DataBind()
            Session.Remove("ExportReport")
            Session.Remove("Packflag")
        End If
        exportreport.Visible = True

        Return dg
    End Function

#End Region
    Protected Sub reportviewer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles reportviewer.Click
        Session("Packflag") = "1"
        Session("ExportReport") = "0"
        viewreport()
        'Dim script As String = "window.open('../report/RPTREPRINTPRFSINXLS.aspx')"
        'Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "RPTREPRINTPRFSINXLS", script, True)
        Return
    End Sub

    Protected Sub exportreport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles exportreport.Click
        Session("ExportReport") = "1"
        Session("Packflag") = "0"
        viewreport()
        Return
    End Sub

    Public Function databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        Dim row As DataRow
        If (ds.Tables.Count > 0) Then
            For Each row In ds.Tables(0).Rows
                Dim NewItem As New ListItem()
                NewItem.Text = Trim(row(0).ToString()) & "-" & Trim(row(1).ToString())
                NewItem.Value = row(0)
                dropdown.Items.Add(NewItem)
            Next
        End If
        Return 0
    End Function

    Public Sub getServiceCenter()
        Dim objConn As New SqlConnection(ConfigurationSettings.AppSettings.Get("ConnectionString"))
        Dim cmdRecords As New SqlCommand("BB_JDESRVC_IDNAME", objConn)
        Dim dataReader As SqlDataReader
        objConn.Open()
        dataReader = cmdRecords.ExecuteReader

        minsercenter.DataSource = dataReader
        minsercenter.DataTextField = "idname"
        minsercenter.DataValueField = "id"
        minsercenter.DataBind()
        minsercenter.Items.Insert(0, "---Select---")
        dataReader.Close()

    End Sub

End Class

