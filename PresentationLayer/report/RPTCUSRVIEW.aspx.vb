Imports SQLDataAccess
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
Imports BusinessEntity
Imports CrystalDecisions.Web
Imports CrystalDecisions.CrystalReports.Engine
Imports TransFile
Imports System.io
Imports CrystalDecisions.Shared


Partial Class PresentationLayer_report_RPTCUSRVIEW
    Inherits System.Web.UI.Page
    Dim clsReport As New ClsCommonReport




    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Response.Redirect("~/PresentationLayer/logon.aspx")
            End If
        Catch ex As Exception
            Response.Redirect("~/PresentationLayer/logon.aspx")
        End Try
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        Dim obiXML As New clsXml
        obiXML.XmlFile = ConfigurationManager.AppSettings("XmlFilePath")
        'Me.titleLab.Text = obiXML.GetLabelName("EngLabelMsg", "BB-RPTCUSR-APPHIS")
        'Me.backLink.Text = objXML.GetLabelName("EngLabelMsg", "BB-BACK")

        Dim clsrptcust As New ClsRptCUSR
        clsrptcust.userid = Session("userID")

        clsrptcust = Session("rptcust_search_condition")

        Dim ds As DataSet = Nothing
        ds = clsrptcust.GetRptcusr()



        Dim statXmlTr As New clsXml
        statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")


        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Dim objXmlTr As New clsXml
        Dim strHead As Array = Array.CreateInstance(GetType(String), 38)
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")

        strHead(0) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCUSR-DATABASE")
        strHead(1) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCUSR-CRRB")
        strHead(2) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCUSR-REPORTID")
        strHead(3) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCUSR-USERID")

        strHead(4) = Session("userID") + "/" + Session("username")
        strHead(5) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCUSR-TOTALBYR")
        strHead(6) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCUSR-SCIH")
        strHead(7) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDAR-TITLE1")
        strHead(8) = Session("login_cmpID")
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        strHead(9) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDAR-SVC")
        strHead(10) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSOSA-CUSTID")
        strHead(11) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTMDSL-0003")
        strHead(12) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDAR-DATE")
        strHead(13) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSOSA-MODEL")
        strHead(14) = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTATE-0001")

        strHead(15) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCUSR-CONTRACT")

        strHead(16) = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")

        strHead(17) = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        If clsrptcust.Minsvcid <> "%" Then
            strHead(18) = clsrptcust.Minsvcid
        Else
            strHead(18) = "ALL".ToString
        End If
        If clsrptcust.Maxsvcid <> "%" Then
            strHead(19) = clsrptcust.Maxsvcid
        Else
            strHead(19) = "ALL".ToString
        End If
        If clsrptcust.Mincustid <> "" Then
            strHead(20) = clsrptcust.Mincustid
        Else
            strHead(20) = "AAA".ToString()
        End If
        If clsrptcust.Maxcustid <> "ZZZZZZZZZZZZZZZZZZZZ" Then
            strHead(21) = clsrptcust.Maxcustid
        Else
            strHead(21) = "ZZZ".ToString()
        End If
        If clsrptcust.Minserialno <> "" Then
            strHead(22) = clsrptcust.Minserialno
        Else
            strHead(22) = "AAA".ToString()
        End If
        If clsrptcust.Maxserialno <> "ZZZZZZZZZZ" Then
            strHead(23) = clsrptcust.Maxserialno
        Else
            strHead(23) = "ZZZ".ToString()
        End If
        If clsrptcust.Mindate <> "" Then
            strHead(24) = clsrptcust.Mindate
        Else
            strHead(24) = " ".ToString()
        End If

        If clsrptcust.Maxdate <> "" Then
            strHead(25) = clsrptcust.Maxdate
        Else
            strHead(25) = " ".ToString()
        End If

        If clsrptcust.Minmodel <> "" Then
            strHead(26) = clsrptcust.Minmodel
        Else
            strHead(26) = "AAA".ToString
        End If
        If clsrptcust.Maxmodel <> "ZZZZZZZZZZ" Then
            strHead(27) = clsrptcust.Maxmodel
        Else
            strHead(27) = "ZZZ".ToString()
        End If
        If clsrptcust.Minstat <> "%" Then
            strHead(28) = clsrptcust.Minstat
        Else
            strHead(28) = "ALL".ToString()
        End If
        If clsrptcust.Maxstat <> "%" Then
            strHead(29) = clsrptcust.Maxstat
        Else
            strHead(29) = "ALL".ToString()
        End If

        If clsrptcust.Contracttext <> "" Then
            strHead(30) = clsrptcust.Contracttext
        Else
            strHead(30) = "ALL".ToString()
        End If

        strHead(31) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSOSA-MODEL")

        strHead(32) = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0011")
        strHead(33) = "/"
        strHead(34) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCUSR-TOTALBYM")
        'strHead(34) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCUSR-TOTALBYM")
        strHead(35) = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASAREA-0001")

        If clsrptcust.MinArea <> "%" Then
            strHead(36) = clsrptcust.MinArea
        Else
            strHead(36) = "ALL".ToString()
        End If
        If clsrptcust.MaxArea <> "%" Then
            strHead(37) = clsrptcust.MaxArea
        Else
            strHead(37) = "ALL".ToString()
        End If


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Dim lstrUrl As String = clsReport.ReportPath

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' original method
        'Dim sr As Stream = crReportDocument.ExportToStream(ExportFormatType.PortableDocFormat)
        'Response.Clear()
        'Dim bSuccess As Boolean = dowload.ResponseFile(Request, Response, "RptCUSR.pdf", sr, 1024000)

        'If bSuccess = False Then
        '    'fail
        'End If
        'sr.Close()
        'crReportDocument.Dispose()
        'Response.End()



        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' method 2 export pdf with physical file

        'Dim rpt As Report
        'Dim crReportDocument As ReportDocument
        Dim lstrPdfFileName As String = "CustomerReferenceReport_" & Session("login_session") & ".pdf"


        Try
            With clsReport
                .ReportFileName = "RptCUSR.rpt"  'TextBox1.Text
                '.SetReport(CrystalReportViewer1, ds, strHead)
                .SetReportDocument(ds, strHead)
                'crReportDocument = .GetReportDocument
                .PdfFileName = lstrPdfFileName
                .ExportPdf()
            End With

            'crReportDocument.Export()
            Response.Redirect(clsReport.PdfUrl)

        Catch err As Exception
            'System.IO.File.Delete(lstrPhysicalFile)
            Response.Write("<BR>")
            Response.Write(err.Message.ToString)


        End Try

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' method 1 

        'Dim oStream As System.IO.MemoryStream = crReportDocument.ExportToStream(ExportFormatType.PortableDocFormat)

        'Response.Clear()
        'Response.Buffer() = True

        ''set the content type to PDF
        'Response.ContentType = "application/pdf"

        '' add content type header
        'Response.AddHeader("Content-Type", "application/pdf")

        '' set the content disposition
        ''Response.AddHeader("content-disposition", "attachment;filename=" & lstrPdfFileName)
        ''Response.AddHeader("Content-Disposition", "inline;filename=" & lstrPdfFileName)

        '' write the buffer with pdf file to the output        
        'Try

        '    With HttpContext.Current.Response
        '        .ClearContent()
        '        .ClearHeaders()
        '        .ContentType = "application/pdf"
        '        .AddHeader("Content-Disposition", "inline; filename=" & lstrPdfFileName)
        '        .BinaryWrite(oStream.ToArray)

        '        .End()
        '    End With

        'Catch err As Exception
        '    Response.Write("< BR >")
        '    Response.Write(err.Message.ToString)
        'End Try
        'oStream.Close()
        'Response.End()



        'crReportDocument.Dispose()
        'Response.End()
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        clsReport.UnloadReport()
        'CrystalReportViewer1.Dispose()
    End Sub
End Class
