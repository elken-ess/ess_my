﻿Imports SQLDataAccess
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
Imports BusinessEntity
Imports CrystalDecisions.Web
Imports CrystalDecisions.CrystalReports.Engine
Partial Class PresentationLayer_report_RPTCDSLVIEW
    Inherits System.Web.UI.Page
    Dim clsReport As New ClsCommonReport

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If

        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
         
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        Dim rptcdsl As New clsRPTCDSL()

        Dim objXmlTr As New clsXml

        rptcdsl = Session("rpt_cdsl_searchcondition")
        Dim ds As DataSet = Nothing
        ds = rptcdsl.GetServiceOrderSum()
        ' switch  encrypt to telphone for display        
        Dim count As Integer
        Dim prdctclnameHOM As String
        Dim prdctclnameOFF As String
        Dim TLEOFF As String

        Dim clscomm As New clsCommonClass()

        If ds.Tables(0).Rows.Count > 0 Then
            For count = 0 To ds.Tables(0).Rows.Count - 1
                Dim prdctclsid As String = ds.Tables(0).Rows(count).Item(1).ToString 'item(8) is product class 
                If (prdctclsid <> "") Then
                    prdctclnameHOM = clscomm.passconverttel(prdctclsid)
                    ds.Tables(0).Rows(count).Item(1) = "HOM--" & prdctclnameHOM
                End If

                TLEOFF = ds.Tables(0).Rows(count).Item(2).ToString 'item(8) is product class 
                If (TLEOFF <> "") Then
                    prdctclnameOFF = clscomm.passconverttel(prdctclsid)
                    ds.Tables(0).Rows(count).Item(2) = "HP--" & prdctclnameOFF
                End If

            Next

            ''''''''''''''''''''''''''''

            Dim strHead As Array = Array.CreateInstance(GetType(String), 31)
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            strHead(0) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDSL-0001")
            strHead(1) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDSL-0009")
            strHead(2) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDSL-0017")
            strHead(3) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDSL-0018")
            strHead(4) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDSL-0002")
            strHead(5) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDSL-0020")
            If Session("userID") = "" Then
                strHead(6) = " "
            Else
                strHead(6) = "/" & Session("userID").ToString().ToUpper + "/" + Session("username").ToString.ToUpper

            End If
            strHead(7) = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO") 'objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            strHead(8) = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")

            strHead(9) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDSL-0010") 'Customer ID
            strHead(10) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDSL-0011") 'Model ID
            strHead(11) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDSL-0012") 'RO Serial No
            strHead(12) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDSL-0013") 'State ID
            strHead(13) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDSL-0014") 'Obsolete Date
            strHead(14) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDSL-0015") 'Service Center ID
            strHead(15) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDSL-0016") 'Contract Type
            If rptcdsl.minCustmID = Nothing Then
                strHead(16) = "AAA"
            Else
                strHead(16) = rptcdsl.minCustmID
            End If
            If rptcdsl.maxCustmID = Nothing Then
                strHead(17) = "AAA"
            ElseIf rptcdsl.maxCustmID = "ZZZZZZZZZZZZZZZZZZZZ" Then
                strHead(17) = "ZZZ"
            Else
                strHead(17) = rptcdsl.maxCustmID
            End If
            If rptcdsl.minSvcID = Nothing Then
                strHead(18) = "AAA"
            Else
                strHead(18) = rptcdsl.minSvcID
            End If
            If rptcdsl.maxSvcID = Nothing Then
                strHead(19) = "AAA"
            ElseIf rptcdsl.maxSvcID = "ZZZZZZZZZZZZZZZZZZZZ" Then
                strHead(19) = "ZZZ"
            Else
                strHead(19) = rptcdsl.maxSvcID
            End If
            If rptcdsl.minPrdctMd = Nothing Then
                strHead(20) = "AAA"
            Else
                strHead(20) = rptcdsl.minPrdctMd
            End If
            If rptcdsl.maxPrdctMd = Nothing Then
                strHead(21) = "AAA"
            ElseIf rptcdsl.maxPrdctMd = "ZZZZZZZZZZZZZZZZZZZZ" Then
                strHead(21) = "ZZZ"
            Else
                strHead(21) = rptcdsl.maxPrdctMd
            End If
            strHead(22) = rptcdsl.mindate
            strHead(23) = rptcdsl.maxdate

            If rptcdsl.minState = Nothing Then
                strHead(24) = "AAA"
            Else
                strHead(24) = rptcdsl.minState
            End If
            If rptcdsl.maxState = Nothing Then
                strHead(25) = "AAA"
            ElseIf rptcdsl.maxState = "ZZZZZZZZZZZZZZZZZZZZ" Then
                strHead(25) = "ZZZ"
            Else
                strHead(25) = rptcdsl.maxState
            End If
            If rptcdsl.minRONo = Nothing Then
                strHead(26) = "AAA"
            Else
                strHead(26) = rptcdsl.minRONo
            End If
            If rptcdsl.maxRONo = Nothing Then
                strHead(27) = "AAA"
            ElseIf rptcdsl.maxRONo = "ZZZZZZZZZZZZZZZZZZZZ" Then
                strHead(27) = "ZZZ"
            Else
                strHead(27) = rptcdsl.maxRONo
            End If
            ''
            Dim str As String
            str = rptcdsl.ContractTy
            If (rptcdsl.ContractTy = "%") Then
                strHead(28) = "All"
            Else
                strHead(28) = rptcdsl.ContractTy.ToString
            End If
            strHead(29) = "/"
            If (ds.Tables.Count <> 0) Then
                Dim strtotal As String = objXmlTr.GetLabelName("EngLabelMsg", "RPT_APT_RECORD")
                strHead(30) = strtotal & " : " ' & ds.Tables(0).Rows.Count
            End If


            'With clsReport
            '    .ReportFileName = "RPTCDSL.rpt"
            '    .SetReport(CrystalReportViewer1, ds, strHead)
            'End With
            Dim lstrPdfFileName As String = "ContractDueServiceList_" & Session("login_session") & ".pdf"

            Try
                With clsReport
                    .ReportFileName = "RPTCDSL.rpt"
                    .SetReportDocument(ds, strHead)
                    .PdfFileName = lstrPdfFileName
                    .ExportPdf()
                End With

                Response.Redirect(clsReport.PdfUrl)

            Catch err As Exception
                'System.IO.File.Delete(lstrPhysicalFile)
                Response.Write("<BR>")
                Response.Write(err.Message.ToString)

            End Try


            Me.titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDSL-0019")
        Else
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            Dim lstrMessage = objXmlTr.GetLabelName("StatusMessage", "NoData")
            Dim script As String = "alert('" & lstrMessage & "');window.close()"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
        End If

    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        clsReport.UnloadReport()
        CrystalReportViewer1.Dispose()
    End Sub

End Class
