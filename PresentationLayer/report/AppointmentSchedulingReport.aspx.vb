Imports System.Data
Imports System.Data.SqlClient
Imports System.Globalization
Imports SQLDataAccess


Partial Class PresentationLayer_report_AppointmentSchedulingReport
    Inherits System.Web.UI.Page

    Private ReadOnly _common As New BusinessEntity.clsCommonClass()
    Private ReadOnly _datestyle As Globalization.CultureInfo = New Globalization.CultureInfo("en-CA")

    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)
        Return connection
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim _con As SqlConnection
        _con = GetConnection(ConfigurationManager.AppSettings("ConnectionString"))

        If TypeFromDDL.SelectedValue = "" Then
            Dim Comm2 As SqlCommand
            Comm2 = _con.CreateCommand
            Comm2.CommandText = "SELECT  MSV1_SVTID, MSV1_ENAME FROM MSV1_FIL WHERE MSV1_CTRID='MY' AND MSV1_SVTID NOT IN ('ME','SA') ORDER BY MSV1_SVTID "
            _con.Open()

            Dim dr1 As SqlDataReader = Comm2.ExecuteReader(CommandBehavior.CloseConnection)

            Dim dt1 As DataTable = New DataTable()
            dt1.Load(dr1)

            TypeFromDDL.Items.Add(New ListItem("ALL", "ALL"))
            TypeToDDL.Items.Add(New ListItem("ALL", "ALL"))

            Dim row As DataRow
            If dt1.Rows.Count > 0 Then
                For Each row In dt1.Rows
                    Dim newItem As New ListItem()
                    newItem.Text = Trim(row(0).ToString()) & " - " & row(1).ToString()
                    newItem.Value = Trim(row(0).ToString())
                    TypeFromDDL.Items.Add(newItem)
                    TypeToDDL.Items.Add(newItem)
                Next
            End If

            _con.Close()
        End If

        If StatusFromDDL.SelectedValue = "" Then
            Dim Comm2 As SqlCommand
            Comm2 = _con.CreateCommand
            Comm2.CommandText = "select DISTINCT LCOD_CODID, LCOD_REM from lcod_fil where lcod_codty='APPTStatus'"
            _con.Open()

            Dim dr1 As SqlDataReader = Comm2.ExecuteReader(CommandBehavior.CloseConnection)

            Dim dt1 As DataTable = New DataTable()
            dt1.Load(dr1)

            StatusFromDDL.Items.Add(New ListItem("ALL", "ALL"))
            StatusToDDL.Items.Add(New ListItem("ALL", "ALL"))

            Dim row As DataRow
            If dt1.Rows.Count > 0 Then
                For Each row In dt1.Rows
                    Dim newItem As New ListItem()
                    newItem.Text = Trim(row(0).ToString()) & " - " & row(1).ToString()
                    newItem.Value = Trim(row(0).ToString())
                    StatusFromDDL.Items.Add(newItem)
                    StatusToDDL.Items.Add(newItem)
                Next
            End If

            _con.Close()
        End If


        If SVCFromDDL.SelectedValue = "" Then
            Dim Comm2 As SqlCommand
            Comm2 = _con.CreateCommand
            Comm2.CommandText = "Select MSVC_SVCID, MSVC_ENAME from MSVC_FIL where MSVC_STAT = 'ACTIVE' and MSVC_SVCID not in ('901','951','ZZZ') order by MSVC_SVCID"
            _con.Open()

            Dim dr1 As SqlDataReader = Comm2.ExecuteReader(CommandBehavior.CloseConnection)

            Dim dt1 As DataTable = New DataTable()
            dt1.Load(dr1)

            SVCFromDDL.Items.Add(New ListItem("ALL", "ALL"))
            SVCToDDL.Items.Add(New ListItem("ALL", "ALL"))

            Dim row As DataRow
            If dt1.Rows.Count > 0 Then
                For Each row In dt1.Rows
                    Dim newItem As New ListItem()
                    newItem.Text = Trim(row(0).ToString()) & " - " & row(1).ToString()
                    newItem.Value = Trim(row(0).ToString())
                    SVCFromDDL.Items.Add(newItem)
                    SVCToDDL.Items.Add(newItem)
                Next
            End If

            _con.Close()
        End If

        If CRFromDDL.SelectedValue = "" Then
            Dim Comm2 As SqlCommand
            Comm2 = _con.CreateCommand
            Comm2.CommandText = "Select MUSR_USRID, MUSR_ENAME from MUSR_FIL where MUSR_STAT in ('ACTIVE','BARRED') and	MUSR_DEPT = 'CR' and MUSR_SVCID >= '" & SVCFromDDL.SelectedValue.ToString & "'" _
            & " AND MUSR_SVCID <= '" & SVCToDDL.SelectedValue.ToString & "' order by MUSR_ENAME"
            _con.Open()

            Dim dr1 As SqlDataReader = Comm2.ExecuteReader(CommandBehavior.CloseConnection)

            Dim dt1 As DataTable = New DataTable()
            dt1.Load(dr1)

            CRFromDDL.Items.Add(New ListItem("ALL", "ALL"))
            CRToDDL.Items.Add(New ListItem("ALL", "ALL"))

            Dim row As DataRow
            If dt1.Rows.Count > 0 Then
                For Each row In dt1.Rows
                    Dim newItem As New ListItem()
                    newItem.Text = Trim(row(0).ToString()) & " - " & row(1).ToString()
                    newItem.Value = Trim(row(0).ToString())
                    CRFromDDL.Items.Add(newItem)
                    CRToDDL.Items.Add(newItem)
                Next
            End If

            _con.Close()


        End If



    End Sub

    Protected Sub RetrieveBtn1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles RetrieveBtn1.Click
        Session("FromDate") = FromDateTxt.Text
        Session("ToDate") = ToDateTxt.Text
        Session("SVCFrom") = SVCFromDDL.SelectedValue.ToString
        Session("SVCTo") = SVCToDDL.SelectedValue.ToString
        Session("TypeFrom") = TypeFromDDL.SelectedValue.ToString
        Session("TypeTo") = TypeToDDL.SelectedValue.ToString
        Session("StatusFrom") = StatusFromDDL.SelectedValue.ToString
        Session("StatusTo") = StatusToDDL.SelectedValue.ToString
        Session("CRFrom") = CRFromDDL.SelectedValue.ToString
        Session("CRTo") = CRToDDL.SelectedValue.ToString
        

        Const script As String = "window.open('../report/AppointmentSchedulingReport_View.aspx')"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "AppointmentSchedulingReport_View", script, True)

    End Sub

    Protected Sub SVCFromDDL_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles SVCFromDDL.SelectedIndexChanged
        Dim _con As SqlConnection
        _con = GetConnection(ConfigurationManager.AppSettings("ConnectionString"))

        CRFromDDL.Items.Clear()
        CRToDDL.Items.Clear()

        CRFromDDL.Items.Add(New ListItem("ALL", "ALL"))
        CRToDDL.Items.Add(New ListItem("ALL", "ALL"))

        Dim Comm2 As SqlCommand
        Comm2 = _con.CreateCommand
        Comm2.CommandText = "Select MUSR_USRID, MUSR_ENAME from MUSR_FIL where MUSR_STAT in ('ACTIVE','BARRED') and	MUSR_DEPT = 'CR' and MUSR_SVCID >= '" & SVCFromDDL.SelectedValue.ToString & "'" _
        & " AND MUSR_SVCID <= '" & SVCToDDL.SelectedValue.ToString & "' order by MUSR_ENAME "
        _con.Open()

        Dim dr1 As SqlDataReader = Comm2.ExecuteReader(CommandBehavior.CloseConnection)

        Dim dt1 As DataTable = New DataTable()
        dt1.Load(dr1)

        Dim row As DataRow
        If dt1.Rows.Count > 0 Then
            For Each row In dt1.Rows
                Dim newItem As New ListItem()
                newItem.Text = Trim(row(0).ToString()) & " - " & row(1).ToString()
                newItem.Value = Trim(row(0).ToString())
                CRFromDDL.Items.Add(newItem)
                CRToDDL.Items.Add(newItem)
            Next
        End If

        _con.Close()

    End Sub
End Class
