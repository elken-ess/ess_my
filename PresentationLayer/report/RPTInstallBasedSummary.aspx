<%@ Page Language="VB" AutoEventWireup="false" CodeFile="RPTInstallBasedSummary.aspx.vb" Inherits="PresentationLayer.report.PresentationLayer_report_RPTInstallBasedSummary" %>
<%@ Register TagPrefix="cc1" Namespace="JCalendar" Assembly="JCalendar" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Install Base Summary</title>
    <link href="../css/style.css" type="text/css" rel="stylesheet" />
    <script type="text/javascript" language="JavaScript" src="../js/common.js"></script>
    <style type="text/css">
    	.rowStyle {
    		background-color: #ffffff;
    	}
    	.labelStyle {
    		text-align: left;
    		width: 15%
    	}
    	.fromToStyle {
    		text-align: left;
    		width: 6%;
    	}
    	.dataStyle {
    		text-align: left;
    		width: 27%
    	}
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table id="countrytab" border="0" width="100%">
            <tr>
                <td style="width: 100%">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title1.gif" width="5" />
                            </td>
                            <td background="../graph/title_bg.gif" class="style2" width="98%">
                                <asp:Label ID="titleLab" runat="server" Text="Label" />
                            </td>
                            <td align="LEFT" background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title_2.gif" width="5" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100%">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" class="style2" width="98%">
                                
                                <asp:Label ID="errlab" runat="server" Text="Label" Visible="false" ForeColor="Red"/></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100%">
                    <table id="country" style="background-color: #b7e6e6" border="0" cellpadding="2" cellspacing="1" width ="100%">

                        
                        <tr class="rowStyle">
                            <td class="labelStyle">
                                <asp:Label ID="lblInstallDate" runat="server" Text="Label"/>
                                </td>
                            <td class="fromToStyle">
                                <asp:Label ID="Fromlab3" runat="server" Text="From"/>
                                </td>
                            <td class="dataStyle">
                                <asp:TextBox ID="txtFromDate" runat="server" CssClass="textborder" MaxLength ="10" Width="120px" style="width: 60%">01/01/2006</asp:TextBox>
                                <asp:HyperLink ID="HypCalFrom" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif" ToolTip="Choose a Date">Choose a Date</asp:HyperLink>
                                <cc1:JCalendar ID="JCalendar1" runat="server" ControlToAssign="txtFromDate" ImgURL="~/PresentationLayer/graph/calendar.gif"  Visible="false" />
                                &nbsp;&nbsp;
                            </td>
                            <td class="fromToStyle">
                                <asp:Label ID="Tolab3" runat="server" Text="To"/>
                                </td>
                            <td class="dataStyle">
                                <asp:TextBox ID="txtToDate" runat="server" CssClass="textborder" MaxLength ="10" Width="120px" style="width: 60%">01/01/2007</asp:TextBox>
                                <asp:HyperLink ID="HypCalTo" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif" ToolTip="Choose a Date">Choose a Date</asp:HyperLink> 
                                <cc1:JCalendar ID="JCalendar2" runat="server" ControlToAssign="txtToDate" ImgURL="~/PresentationLayer/graph/calendar.gif"  Visible="false" />
                                &nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr class="rowStyle">
                            <td class="labelStyle">
                                <asp:Label ID="lblSvcCenter" runat="server" Text="Label"/></td>
                            <td class="fromToStyle">
                                From</td>
                            <td class="dataStyle">
                                <asp:DropDownList runat="server" 
                                                  ID="ddSVCCenter" 
                                                  Width="41%" 
                                                  CssClass="textborder" 
                                                  AutoPostBack="True"
                                                  /></td>
                            <td class="fromToStyle">
                                To</td>
                            <td class="dataStyle">
                                <asp:DropDownList ID="ddSVCCenterTo" runat="server" AutoPostBack="True" CssClass="textborder"
                                    Width="41%">
                                </asp:DropDownList></td>
                        </tr>
                        
                        <tr class="rowStyle">
                            <td class="labelStyle">
                                <asp:Label ID="lblModelID" runat="server" Text="Label"/></td>
                            <td class="fromToStyle">
                                From</td>
                            <td class="dataStyle">
                                <asp:DropDownList ID="ddModelID" runat="server" CssClass="textborder"/></td>
                            <td class="fromToStyle">
                                To</td>
                            <td class="dataStyle">
                                <asp:DropDownList ID="ddModelIDTo" runat="server" CssClass="textborder">
                                </asp:DropDownList></td>
                        </tr>
                        <tr class="rowStyle">
                            <td class="labelStyle">
                                <asp:Label ID="lblProdClass" runat="server" Text="Label"/>
                            </td>
                            <td class="fromToStyle" colspan="2">
                                <asp:DropDownList ID="ddProdClass" runat="server" CssClass="textborder" AutoPostBack="True"/>
                            </td>
                            <td class="fromToStyle">
                                &nbsp;</td>
                            <td class="dataStyle">
                                &nbsp;</td>
                        </tr>
                         <tr class="rowStyle">
                            <td class="labelStyle">
                                <asp:Label ID="lblCustType" runat="server" Text="Label"/>
                            </td>
                            <td class="fromToStyle" colspan="2">
                                <asp:DropDownList ID="ddCustType" runat="server" CssClass="textborder" AutoPostBack="True"/>
                            </td>
                            <td class="fromToStyle">
                                Status</td>
                            <td class="dataStyle"><asp:DropDownList ID="ddlstatus" runat="server" CssClass="textborder">
                                <asp:ListItem>ALL</asp:ListItem>
                                <asp:ListItem>ACTIVE</asp:ListItem>
                                <asp:ListItem>OBSOLETE</asp:ListItem>
                                <asp:ListItem>SUSPENDED</asp:ListItem>
                                <asp:ListItem>TERMINATED</asp:ListItem>
                            </asp:DropDownList></td>
                        </tr>
                    </table>
                    <asp:LinkButton ID="reportviewer" runat="server" Text="ViewReport" OnClick="reportViewer_Click"/>
                    </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
