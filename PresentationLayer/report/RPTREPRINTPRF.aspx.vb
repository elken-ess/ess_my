Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Web
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web
Imports BusinessEntity
Imports System.Data
Partial Class PresentationLayer_Report_RPTREPRINTPRF
    Inherits System.Web.UI.Page
    Dim fstrMonth As String = ""
    Dim fstrDefaultStartDate As String = ""
    Dim fstrDefaultEndDate As String = ""

    'Added by Ryan Estandarte 21 Sept 2012
    Private ReadOnly _common As New clsCommonClass()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        fstrMonth = Month(System.DateTime.Today)
        fstrDefaultStartDate = System.DateTime.Today
        fstrDefaultEndDate = fstrDefaultStartDate 'DateAdd(DateInterval.Month, 1, CDate(fstrDefaultStartDate))

        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try

        If Not Page.IsPostBack Then
            ''display label message
            Session("Packflag") = "0"
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")

            lblServCenter.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTREPRINTPRF-0001")

            lblDate.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTREPRINTPRF-0002")
            lblTechnicianID.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTREPRINTPRF-0003")

            Me.lblPrfNo.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTREPRINTPRF-0004")

            reportviewer.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-VIEWREPORT")
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTREPRINTPRF-TITLE")

            Me.mindate.Text = fstrDefaultStartDate

            Dim clscomds As New DataSet
            Dim clscom As New clsCommonClass()
            Dim rank As Integer = Session("login_rank")
            clscom.rank = rank
            If rank <> 0 Then
                clscom.spctr = Session("login_ctryID")
                clscom.spstat = Session("login_cmpID")
                clscom.sparea = Session("login_svcID")
            End If
            clscomds = clscom.Getcomidname("BB_MASSVRC_IDNAME")
            If clscomds.Tables.Count <> 0 Then
                databonds(clscomds, Me.minsercenter)
            End If
            ' Removed by Ryan Estandarte 21 Sept 2012
            'Me.minsercenter.Items.Insert(0, "")

            '''access control'''''''''''''''''''''''''''''''''''''''''''''''
            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "58")
            Me.reportviewer.Enabled = purviewArray(3)

        ElseIf Session("Packflag") = "1" Then
            viewreport()
        End If


        HypCalFrom.NavigateUrl = "javascript:DoCal(document.form1.mindate);"
    End Sub
#Region " view report message"
    Public Function viewreport()
        'set params'
        Dim packEntity As New ClsRptReprintPRF
        'Dim packEntity As New ClsRptPack

        mindate.Text = Request.Form("mindate")

        'Remove later - WEY
        'mindate.Text = Request.Form("mindate")
        'If Trim(mindate.Text).ToUpper = "AAA" Then
        'packEntity.statemin = ""
        'Else
        'packEntity.statemin = Trim(mindate.Text)
        'End If
        'Remove later - WEY

        If Trim(minsercenter.Text) = "" Then
            packEntity.servcenterid = ""
        Else
            packEntity.servcenterid = Trim(minsercenter.Text)
        End If

        If Trim(mindate.Text) <> "" Then
            Dim temparr As Array = mindate.Text.Split("/")
            packEntity.datemin = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
        Else
            Dim mind As String = "01/01/1910"
            Dim temparr As Array = mind.Split("/")
            packEntity.datemin = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
        End If
        If Trim(mindate.Text) <> "" Then
            Dim temparr2 As Array = mindate.Text.Split("/")
            packEntity.datemin = temparr2(2) + "-" + temparr2(1) + "-" + temparr2(0)
        Else
            Dim maxd As String = "31/12/2089"
            Dim temparr2 As Array = maxd.Split("/")
            packEntity.datemin = temparr2(2) + "-" + temparr2(1) + "-" + temparr2(0)
        End If
        ' Modified by Ryan Estandarte 21 Sept 2012
        'If Trim(mintechnician.Text) = "AAA" Then
        '    packEntity.tecnicianidbegin = ""
        'Else
        '    packEntity.tecnicianidbegin = Trim(mintechnician.Text)
        'End If
        packEntity.tecnicianidbegin = ddTechnician.SelectedValue

        'Remove later - WEY
        'packEntity.tecnicianidend = Trim(maxtechnician.Text)
        'If Trim(minsercenter.Text) = "ZZZ" Then
        ' packEntity.stateend = "ZZZZZZZZZZZ"
        'Else
        'packEntity.stateend = Trim(minsercenter.Text).ToUpper.ToString()
        'End If

        'If Trim(minsercenter.Text) = "ZZZ" Then
        'packEntity.sercidend = "ZZZZZZZZZZZ"
        'Else
        'packEntity.sercidend = Trim(minsercenter.Text).ToUpper.ToString()
        'End If
        'If Trim(minsercenter.Text) = "ZZZ" Then
        'packEntity.tecnicianidend = "ZZZZZZZZZZZ"
        'Else
        'packEntity.tecnicianidend = Trim(minsercenter.Text).ToUpper.ToString()
        'End If
        'Remove later - WEY


        If Trim(txtPrfNoFrom.Text) = "AAA" Then
            packEntity.prfno = ""
        Else
            packEntity.prfno = Me.txtPrfNoFrom.Text.Trim.ToUpper
        End If

        'If Trim(txtPrfNoTo.Text) = "ZZZ" Then
        'packEntity.PartCodeTo = "ZZZZZZZZZZZ"
        'Else
        'packEntity.PartCodeTo = Me.txtPrfNoTo.Text.Trim.ToUpper
        'End If






        'Dim days As Integer = -270
        'mdslEntity.instdatemin270 = DateAdd(DateInterval.Day, days, Convert.ToDateTime(mdslEntity.datebegin))
        'mdslEntity.instdatemax270 = DateAdd(DateInterval.Day, days, Convert.ToDateTime(mdslEntity.dateend))

        If Trim(Session("username")) = "" Then
            packEntity.ModifiedBy = "ADMIN"
        Else
            packEntity.ModifiedBy = Session("username")
        End If
        packEntity.logrank = Session("login_rank")
        Dim login_rank As Integer = Session("login_rank")
        If login_rank <> 0 Then
            Select Case (login_rank)
                Case 9
                    packEntity.logctrid = Session("login_ctryID").ToString.ToUpper
                    packEntity.logcompanyid = Session("login_cmpID").ToString.ToUpper
                    packEntity.logserviceid = Session("login_svcID").ToString.ToUpper
                Case 8
                    packEntity.logctrid = Session("login_ctryID").ToString.ToUpper
                    packEntity.logcompanyid = Session("login_cmpID").ToString.ToUpper
                Case 7
                    packEntity.logctrid = Session("login_ctryID").ToString.ToUpper
            End Select
        End If


        'Dim ds As DataSet = packEntity.GetPack()


        'Session("rptpack_ds") = ds
        'Session("rptpack_search_condition") = packEntity
        'Session("rptreprintprf_search_condition") = packEntity
        Session("rptreprintprf_search_condition") = packEntity

    End Function
#End Region

    Protected Sub reportviewer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles reportviewer.Click
        Session("Packflag") = "1"
        viewreport()
        'Response.Redirect("~/PresentationLayer/REPORT/RptPACKSearch.aspx")
        'Dim script As String = "window.open('../report/RptReprintPRFSearch.aspx')"
        Dim script As String = "window.open('../report/RptReprintPRFSearch.aspx')"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "RptReprintPRFSearch", script, True)
        Return
    End Sub

    Public Function databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        Dim row As DataRow
        If (ds.Tables.Count > 0) Then
            For Each row In ds.Tables(0).Rows
                Dim NewItem As New ListItem()
                NewItem.Text = Trim(row(0).ToString()) & "-" & Trim(row(1).ToString())
                NewItem.Value = row(0)
                dropdown.Items.Add(NewItem)
            Next
        End If
        ' Added by Ryan Estandarte 21 Sept 2012
        dropdown.Items.Insert(0, "")
    End Function

    ''' <remarks>Added by Ryan Estandarte 21 Sept 2012</remarks>
    Protected Sub minsercenter_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        PopulateTechnicians(minsercenter.SelectedValue)
    End Sub

    ''' <remarks>Added by Ryan Estandarte 21 Sept 2012</remarks>
    Private Sub PopulateTechnicians(ByVal servID As String)
        Dim ds As New DataSet
        _common.spctr = Session("login_ctryID").ToString().ToUpper
        _common.spstat = "ACTIVE"
        _common.rank = Session("login_rank").ToString()
        _common.sparea = servID

        ds = _common.Getcomidname("dbo.BB_FNCAPPS_SelTechArea")

        databonds(ds, ddTechnician)
    End Sub
End Class
