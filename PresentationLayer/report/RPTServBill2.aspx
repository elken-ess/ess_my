<%@ Page Language="VB" AutoEventWireup="false" CodeFile="RPTServBill2.aspx.vb" Inherits="PresentationLayer.report.PresentationLayerReportRptServBill2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="JCalendar" Namespace="JCalendar" TagPrefix="cc1" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Service Bill Report</title>
    <link href="../css/style.css" rel="stylesheet" type="text/css" />
    <style type="text/css">A:link { COLOR: #336666; TEXT-DECORATION: none }
	BODY { FONT-SIZE: 9pt; FONT-FAMILY: "Arial", "Verdana", "Tahoma"; BACKGROUND-COLOR: #ffffff }
	TD { FONT-SIZE: 9pt; FONT-FAMILY: Arial,Verdana,Tahoma }
	</style>
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />

    <script language="JavaScript" src="../js/common.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div>
                <table id="countrytab" style="width: 100%; border: 0">
                    <tr>
                        <td style="width: 0px">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td background="../graph/title_bg.gif" style="width: 1%">
                                        <img height="24" src="../graph/title1.gif" width="5" /></td>
                                    <td background="../graph/title_bg.gif" class="style2" width="98%">
                                        <asp:Label ID="titleLab" runat="server" Text="Service Bill Report"></asp:Label></td>
                                    <td align="left" background="../graph/title_bg.gif" width="1%">
                                        <img height="24" src="../graph/title_2.gif" width="5" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 0px">
                            <table id="country" bgcolor="#b7e6e6" border="0" cellpadding="2" cellspacing="1"
                                style="width: 100%">
                                <tr bgcolor="#ffffff">
                                    <td align="left" style="width: 18%">
                                        <asp:Label ID="ServCtrLabel" runat="server" Text="Service Center ID" /></td>
                                    <td align="left" style="width: 6%">
                                        <asp:Label ID="FromServiceCtrLabel" runat="server" Text="From" /></td>
                                    <td align="left" style="width: 34%">
                                        <asp:TextBox ID="FromServiceCtrTextbox" runat="server" CssClass="textborder" />
                                    </td>
                                    <td align="left" style="width: 11%">
                                        <asp:Label ID="ToServiceCtrLabel" runat="server" Text="To" /></td>
                                    <td align="left" width="35%">
                                        <asp:TextBox ID="ToServiceCtrTextbox" runat="server" CssClass="textborder"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr bgcolor="#ffffff">
                                    <td align="left" style="width: 18%">
                                        <asp:Label ID="TechnicianLabel" runat="server" Text="Technician ID" />
                                    </td>
                                    <td align="left" style="width: 6%">
                                        <asp:Label ID="FromTechIDLabel" runat="server" Text="From" /></td>
                                    <td align="left" style="width: 34%">
                                        <asp:TextBox ID="FromTechIDTextbox" runat="server" CssClass="textborder" /></td>
                                    <td align="left" style="width: 11%">
                                        <asp:Label ID="ToTechIDLabel" runat="server" Text="To" />
                                    </td>
                                    <td align="left" width="35%">
                                        <asp:TextBox ID="ToTechIDTextbox" runat="server" CssClass="textborder" />
                                    </td>
                                </tr>
                                <tr bgcolor="#ffffff">
                                    <td align="left" style="width: 18%">
                                        <asp:Label ID="CustomerIDLabel" runat="server" Text="Customer ID" />
                                    </td>
                                    <td align="left" style="width: 6%">
                                        <asp:Label ID="FromCustLabel" runat="server" Text="From" />
                                    </td>
                                    <td align="left" style="width: 34%">
                                        <asp:TextBox ID="FromCustIDTextbox" runat="server" CssClass="textborder" />
                                    </td>
                                    <td align="left" style="width: 11%">
                                        <asp:Label ID="ToCustLabel" runat="server" Text="To" />
                                    </td>
                                    <td align="left" width="35%">
                                        <asp:TextBox ID="ToCustIDTextbox" runat="server" CssClass="textborder" />
                                    </td>
                                </tr>
                                <tr bgcolor="#ffffff">
                                    <td align="left" style="width: 18%">
                                        <asp:Label ID="InvoiceDateLabel" runat="server" Text="Invoice Date"></asp:Label></td>
                                    <td align="left" style="width: 6%">
                                        <asp:Label ID="FromInvoiceDateLabel" runat="server" Text="From" /></td>
                                    <td align="left" style="width: 34%">
                                        <asp:TextBox ID="FromInvoiceDateTextbox" runat="server" CssClass="textborder" />
                                        <asp:HyperLink ID="HypCalFrom" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                                            ToolTip="Choose a Date">Choose a Date</asp:HyperLink>
                                        <cc1:JCalendar ID="FromInvoiceDateCal" runat="server" ControlToAssign="FromInvoiceDateTextbox"
                                            ImgURL="~/PresentationLayer/graph/calendar.gif" Visible="false" 
                                            OnSelectedDateChanged="FromInvoiceDateCalSelectedDateChanged" />
                                    </td>
                                    <td align="left" style="width: 11%">
                                        <asp:Label ID="ToInvoiceDateLabel" runat="server" Text="To" /></td>
                                    <td align="left" width="35%">
                                        <asp:TextBox ID="ToInvoiceDateTextbox" runat="server" CssClass="textborder" />&nbsp;
                                        <asp:HyperLink ID="HypCalTo" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                                            ToolTip="Choose a Date">Choose a Date</asp:HyperLink>
                                        <cc1:JCalendar ID="ToInvoiceDateCal" runat="server" ControlToAssign="ToInvoiceDateTextbox"
                                            ImgURL="~/PresentationLayer/graph/calendar.gif" Visible="false"
                                            OnSelectedDateChanged="ToInvoiceDateCalSelectedDateChanged" />
                                        &nbsp;&nbsp;
                                    </td>
                                </tr>
                                
                                <tr bgcolor="#ffffff">
                                    <td align="left" style="width: 18%">
                                        <asp:Label ID="InvoiceNoLabel" runat="server" Text="Invoice Number" />
                                    </td>
                                    <td align="left" style="width: 6%">
                                        <asp:Label ID="FromInvoiceNoLabel" runat="server" Text="From" />
                                    </td>
                                    <td align="left" style="width: 34%">
                                        <asp:TextBox ID="FromInvoiceNoTextbox" runat="server" CssClass="textborder" />
                                    </td>
                                    <td align="left" style="width: 11%">
                                        <asp:Label ID="ToInvoiceNoLabel" runat="server" Text="To" />
                                    </td>
                                    <td align="left" width="35%">
                                        <asp:TextBox ID="ToInvoiceNoTextbox" runat="server" CssClass="textborder" />
                                    </td>
                                </tr>
                                <tr bgcolor="#ffffff">
                                    <td align="left" style="width: 18%">
                                        <asp:Label ID="ReportTypeLabel" runat="server" Text="Report Type" /></td>
                                    <td align="left" style="width: 6%" colspan="4">
                                        <asp:DropDownList ID="ReportDropdown" runat="server"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6" style="background-color: #ffffff">
                                        <asp:LinkButton ID="lnkViewReport" runat="server" Text="ViewReport" OnClick="LnkViewReportClick" />
                                    </td>
                                </tr>
                            </table>
                            <CR:CrystalReportViewer ID="CrystalReportViewerSBP" runat="server" AutoDataBind="true" />
                            </td>
                    </tr>
                </table>
            </div>
        </div>
    </form>
</body>
</html>
