<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SystemAccessReport.aspx.vb" Inherits="PresentationLayer_report_SystemAccessReport" EnableEventValidation="false"%>

<%@ Register Assembly="JCalendar" Namespace="JCalendar" TagPrefix="cc1" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>System Access Report</title>
    <meta http-equiv="Content-Type" content="text/html; charset=gb2312"/>
	<link href="../css/style.css" type="text/css" rel="stylesheet"/>
	<link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
        <script language="JavaScript" src="../js/common.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table id="uagTab" border="0" style="width: 100%">
            <tr>
                <td style="width: 100%">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title1.gif" width="5" /></td>
                            <td background="../graph/title_bg.gif" class="style2" width="98%">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="right" background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title_2.gif" width="5" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100%">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" class="style2" width="98%">
                                <font color="red">
                                    <asp:Label ID="ErrLab" runat="server" Text="Label" Visible="False"></asp:Label></font></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100%; height: 200px">
                    <table id="userGroup" bgcolor="#b7e6e6" border="0" cellpadding="1" cellspacing="1"
                        style="border-bottom-width: 1px; border-bottom-color: black" width="100%">
                        <tr bgcolor="#ffffff">
                            <td align="left" style="width: 20%; height: 100%">
                                &nbsp;<asp:Label ID="ServiceCenterFLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 30%; height: 100%;">
                                <asp:TextBox ID="ServiceCenterFTextBox" runat="server" Width="95%" MaxLength="10" CssClass="textborder"></asp:TextBox></td>
                            <td align="left" style="width: 20%; height: 100%;">
                                &nbsp;<asp:Label ID="ServiceCenterTLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 30%; height: 100%;">
                                <asp:TextBox ID="ServiceCenterTTextBox" runat="server" Width="95%" MaxLength="10" CssClass="textborder"></asp:TextBox></td>
                            <%--<td align="left" style="width: 20%; height: 100%;">
                                </td>--%>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="left" style="width: 20%; height: 100%;">
                                &nbsp;<asp:Label ID="UserIDFLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 30%; height: 100%;">
                                <asp:TextBox ID="UserIDFTextBox" runat="server" Width="95%" MaxLength="20" CssClass="textborder"></asp:TextBox></td>
                            <td align="left" style="width: 20%; height: 100%;">
                                &nbsp;<asp:Label ID="UserIDTLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 30%; height: 100%;">
                                <asp:TextBox ID="UserIDTTextBox" runat="server" Width="95%" MaxLength="20" CssClass="textborder"></asp:TextBox></td>
                            <%--<td align="left" style="width: 20%; height: 100%;">
                                </td>--%>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="left" style="width: 20%; height: 100%;">
                                &nbsp;<asp:Label ID="DateFLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 30%; height: 100%;">
                                <asp:TextBox ID="DateFTextBox" runat="server" Width="80%" MaxLength="10" CssClass="textborder" ReadOnly="True"></asp:TextBox>
                                
                                
                                <asp:HyperLink ID="HypCalFrom" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif" ToolTip="Choose a Date">Choose a Date</asp:HyperLink>

                                
                                <cc1:JCalendar ID="DTFJCalendar" runat="server" ControlToAssign="DateFTextBox" ImgURL="~/PresentationLayer/graph/calendar.gif" Visible =false  />
                                <%--<asp:ImageButton ID="SADateFButton" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif" />
                                <asp:Calendar ID="SCFCalendar" runat="server" BackColor="White" BorderColor="#3366CC"
                                    BorderWidth="1px" CellPadding="1" DayNameFormat="Shortest" Font-Names="Verdana"
                                    Font-Size="8pt" ForeColor="#003399" Height="56px" Visible="False" Width="120px">
                                    <SelectedDayStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                                    <TodayDayStyle BackColor="#99CCCC" ForeColor="White" />
                                    <SelectorStyle BackColor="#99CCCC" ForeColor="#336666" />
                                    <WeekendDayStyle BackColor="#CCCCFF" />
                                    <OtherMonthDayStyle ForeColor="#999999" />
                                    <NextPrevStyle Font-Size="8pt" ForeColor="#CCCCFF" />
                                    <DayHeaderStyle BackColor="#99CCCC" ForeColor="#336666" Height="1px" />
                                    <TitleStyle BackColor="#003399" BorderColor="#3366CC" BorderWidth="1px" Font-Bold="True"
                                        Font-Size="10pt" ForeColor="#CCCCFF" Height="25px" />
                                </asp:Calendar>--%>
                            </td>
                            <td align="left" style="width: 20%; height: 100%;">
                                &nbsp;<asp:Label ID="DateTLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 30%; height: 100%;">
                                <asp:TextBox ID="DateTTextBox" runat="server" Width="80%" MaxLength="10" CssClass="textborder" ReadOnly="True"></asp:TextBox>
                                
                                
                                <asp:HyperLink ID="HypCalTo" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif" ToolTip="Choose a Date">Choose a Date</asp:HyperLink>

                                
                                
                                <cc1:JCalendar ID="DTTJCalendar" runat="server" ControlToAssign="DateTTextBox" ImgURL="~/PresentationLayer/graph/calendar.gif" Visible =false  />
                                <%--<asp:ImageButton ID="SADateTButton" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif" />
                                <asp:Calendar ID="SCTCalendar" runat="server" BackColor="White" BorderColor="#3366CC"
                                    BorderWidth="1px" CellPadding="1" DayNameFormat="Shortest" Font-Names="Verdana"
                                    Font-Size="8pt" ForeColor="#003399" Height="56px" Visible="False" Width="120px">
                                    <SelectedDayStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                                    <TodayDayStyle BackColor="#99CCCC" ForeColor="White" />
                                    <SelectorStyle BackColor="#99CCCC" ForeColor="#336666" />
                                    <WeekendDayStyle BackColor="#CCCCFF" />
                                    <OtherMonthDayStyle ForeColor="#999999" />
                                    <NextPrevStyle Font-Size="8pt" ForeColor="#CCCCFF" />
                                    <DayHeaderStyle BackColor="#99CCCC" ForeColor="#336666" Height="1px" />
                                    <TitleStyle BackColor="#003399" BorderColor="#3366CC" BorderWidth="1px" Font-Bold="True"
                                        Font-Size="10pt" ForeColor="#CCCCFF" Height="25px" />
                                </asp:Calendar>--%>
                            </td>
                            <%--<td align="left" style="width: 20%; height: 100%;">
                                </td>--%>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="left" style="width: 20%">
                                &nbsp;<asp:Label ID="LoginTimeFLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 30%">
                                <asp:TextBox ID="HourFTextBox" runat="server" Width="15px" MaxLength="2">00</asp:TextBox>:<asp:TextBox
                                    ID="MinuteFTextBox" runat="server" MaxLength="2" Width="15px">00</asp:TextBox>:<asp:TextBox
                                        ID="SecondFTextBox" runat="server" MaxLength="2" Width="15px">00</asp:TextBox>
                            </td>
                            <td align="left" style="width: 20%">
                                &nbsp;<asp:Label ID="LoginTimeTLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 30%">
                                <asp:TextBox ID="HourTTextBox" runat="server" MaxLength="2" Width="15px">23</asp:TextBox>:<asp:TextBox
                                    ID="MinuteTTextBox" runat="server" MaxLength="2" Width="15px">59</asp:TextBox>:<asp:TextBox
                                        ID="SecondTTextBox" runat="server" MaxLength="2" Width="15px">59</asp:TextBox></td>
                            <%--<td align="left" style="width: 20%">
                                </td>--%>
                        </tr>
                        <tr style ="background-color: #ffffff">
                            <td align="left" style="width: 20%">
                                &nbsp;<asp:Label ID="PCIDFLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 30%">
                                <asp:TextBox ID="PCIDFTextBox" runat="server" Width="95%" MaxLength="50" CssClass="textborder"></asp:TextBox></td>
                            <td align="left" style="width: 20%">
                                &nbsp;<asp:Label ID="PCIDTLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 30%">
                                <asp:TextBox ID="PCIDTTextBox" runat="server" Width="95%" MaxLength="50" CssClass="textborder"></asp:TextBox></td>
                        </tr>
                        <tr style ="background-color: #ffffff">
                            <td align="left" style="width: 20%">
                                &nbsp;<asp:Label ID="LoginTimeLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 30%">
                                <asp:TextBox ID="LoginTimeTextBox" runat="server" Width="80%" MaxLength="20" CssClass="textborder" ReadOnly="True"></asp:TextBox>
                                
                                <asp:HyperLink ID="HypCalLog" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif" ToolTip="Choose a Date">Choose a Date</asp:HyperLink>

                                
                                <cc1:JCalendar ID="LoginTimeJCalendar" runat="server" ControlToAssign="LoginTimeTextBox"
                                    ImgURL="~/PresentationLayer/graph/calendar.gif" Visible =false  />
                               
                            </td>
                            <td align="left" style="width: 20%">
                            <td align="left" style="width: 20%">
                                </td>
                             </tr>
                        <tr style ="background-color: #ffffff">  
                        <td align="left" style="width: 30%" colspan =4>
                                <asp:LinkButton ID="retrieveButton" runat="server">retriveButton</asp:LinkButton></td>
                                
                        </tr>
                        
                    </table>
                    <table id="View" border="0" cellpadding="1" cellspacing="1">
                        <tr>
                            <td align="left">
                                &nbsp;<asp:Label ID="stateLab" runat="server" Text="Label" Visible="False"></asp:Label></td>
                        </tr>
                    </table>
                    <table id="GridView" border="0" cellpadding="1" cellspacing="1" width="100%">
                        <tr>
                            <td align="left" style="width: 100%; height: 100%">
                                <asp:GridView ID="SystemAccessGridView" runat="server" Height="100%" Width="100%" AllowPaging="True" Visible="False">                                    
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                    <table id="Print">
                        <tr>
                            <td align="right" style="width: 550px">
                                <asp:LinkButton ID="printButton" runat="server" Visible="False">Print</asp:LinkButton>&nbsp;</td>                        
                        </tr>
                    </table>
                    
                </td>
            </tr>
        </table>
        &nbsp;&nbsp;
        <asp:RangeValidator ID="RangeValidatorHF" runat="server" ControlToValidate="HourFTextBox" MaximumValue="23" MinimumValue="00" Type="Integer" Display="None"></asp:RangeValidator>&nbsp;
        <asp:RangeValidator ID="RangeValidatorMF" runat="server" ControlToValidate="MinuteFTextBox" MaximumValue="59" MinimumValue="00" Type="Integer" Display="None"></asp:RangeValidator>
        <asp:RangeValidator ID="RangeValidatorSF" runat="server" ControlToValidate="SecondFTextBox" MaximumValue="59" MinimumValue="00" Type="Integer" Display="None"></asp:RangeValidator>
        <asp:RangeValidator ID="RangeValidatorMT" runat="server" ControlToValidate="MinuteTTextBox"
            Display="None" MaximumValue="59" MinimumValue="00" Type="Integer"></asp:RangeValidator>
        <asp:RangeValidator ID="RangeValidatorST" runat="server" ControlToValidate="SecondTTextBox"
            Display="None" MaximumValue="59" MinimumValue="00" Type="Integer"></asp:RangeValidator>
        <asp:RangeValidator ID="RangeValidatorHT" runat="server" ControlToValidate="HourTTextBox"
            Display="None" MaximumValue="23" MinimumValue="00" Type="Integer"></asp:RangeValidator>
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
            ShowSummary="False" />
    <br />
        &nbsp;<CR:CrystalReportViewer ID="SARReportViewer" runat="server" AutoDataBind="true" DisplayGroupTree="False" />
    </div>
    </form>
</body>
</html>
