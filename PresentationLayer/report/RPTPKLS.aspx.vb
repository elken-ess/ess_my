Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Web
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web
Imports BusinessEntity
Imports System.Data

Partial Class PresentationLayer_report_RPTPKLS
    Inherits System.Web.UI.Page
    Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
    Dim fstrMonth As String = ""
    Dim fstrDefaultStartDate As String = ""
    Dim fstrDefaultEndDate As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        fstrMonth = Month(System.DateTime.Today)
        fstrDefaultStartDate = System.DateTime.Today
        fstrDefaultEndDate = fstrDefaultStartDate 'DateAdd(DateInterval.Month, 1, CDate(fstrDefaultStartDate))

        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try

        If Not Page.IsPostBack Then
            ''display label message
            'Session("pklsflag") = "0"
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
           

            Me.lblFrom1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            lblFrom2.Text = Me.lblFrom1.Text
            lblFrom3.Text = Me.lblFrom1.Text
            lblFrom4.Text = Me.lblFrom1.Text
            lblFrom5.Text = Me.lblFrom1.Text
            lblFrom6.Text = Me.lblFrom1.Text
            lblFrom7.Text = Me.lblFrom1.Text
            lblFrom8.Text = Me.lblFrom1.Text

            lblTo1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            lblTo2.Text = Me.lblTo1.Text
            lblTo3.Text = Me.lblTo1.Text
            lblTo4.Text = Me.lblTo1.Text
            lblTo5.Text = Me.lblTo1.Text
            lblTo6.Text = Me.lblTo1.Text
            lblTo7.Text = Me.lblTo1.Text
            lblTo8.Text = Me.lblTo1.Text

            Me.lblTechnician.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0012")
            Me.lblPklDate.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTPACKHD7")
            Me.lblCollectionDate.Text = objXmlTr.GetLabelName("EngLabelMsg", "PRF_COLLECTIONDATE")
            Me.lblPrfDate.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-STOCK-08")

            Me.lblServiceCenter.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0014")
            Me.lblState.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0011")
            Me.lblPart.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCAT-0001")
            Me.lblPrfNo.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-STOCK-07")
            Me.lblOnlyPkl.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB_RPTBDPT_ONLYPKL")
            Me.lblSortBy.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-SORTBY")
            Me.lblThenBy.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-THENBY")


            Me.txtPklDateFrom.Text = fstrDefaultStartDate
            Me.txtPklDateTo.Text = fstrDefaultEndDate

            Me.txtCollectionDateFrom.Text = fstrDefaultStartDate
            Me.txtCollectionDateTo.Text = fstrDefaultEndDate

            Me.txtPrfDateFrom.Text = fstrDefaultStartDate
            Me.txtPrfDateTo.Text = fstrDefaultEndDate



            With Me.cboOnlyPkl
                .ClearSelection()
                .Items.Add(New ListItem("Yes", "Y"))
                .Items.Add(New ListItem("No", "N"))
                .SelectedIndex = 1
            End With


            With Me.cboSortBy
                .ClearSelection()
                '.Items.Add(New ListItem(Me.lblPklDate.Text, "FSR1_PKLDT"))
                '.Items.Add(New ListItem(Me.lblCollectionDate.Text, "FSR1_COLDT"))
                '.Items.Add(New ListItem(Me.lblPrfDate.Text, "FSR1_PRFDT"))
                .Items.Add(New ListItem(Me.lblPart.Text, "FSR2_PARID"))
            End With

            With Me.cboThenBy
                .ClearSelection()
                '.Items.Add(New ListItem(Me.lblPklDate.Text, "FSR1_PKLDT"))
                '.Items.Add(New ListItem(Me.lblCollectionDate.Text, "FSR1_COLDT"))
                '.Items.Add(New ListItem(Me.lblPrfDate.Text, "FSR1_PRFDT"))
                .Items.Add(New ListItem(Me.lblPart.Text, "FSR2_PARID"))
            End With



            lnkViewReport.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-VIEWREPORT")
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB_RPTPKLS_TITLE")

            '''access control'''''''''''''''''''''''''''''''''''''''''''''''
            'Dim accessgroup As String = Session("accessgroup").ToString
            'Dim purviewArray As Array = New Boolean() {False, False, False, False, False}
            'purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "57")
            'Me.lnkViewReport.Enabled = purviewArray(3)

            'ElseIf Session("pklsflag") = "1" Then
            '    viewreport()
        End If


        HypCalPklDateFrom.NavigateUrl = "javascript:DoCal(document.form1.txtPklDateFrom);"
        HypCalPklDateTo.NavigateUrl = "javascript:DoCal(document.form1.txtPklDateTo);"

        HypCalPrfDateFrom.NavigateUrl = "javascript:DoCal(document.form1.txtPrfDateFrom);"
        HypCalPrfDateTo.NavigateUrl = "javascript:DoCal(document.form1.txtPrfDateTo);"

        HypCalCollectionDateFrom.NavigateUrl = "javascript:DoCal(document.form1.txtCollectionDateFrom);"
        HypCalCollectionDateTo.NavigateUrl = "javascript:DoCal(document.form1.txtCollectionDateTo);"

    End Sub

    Sub RequestFormValue()
        txtPklDateFrom.Text = Request.Form("txtPklDateFrom")

        If Not IsDate(txtPklDateFrom.Text) Then
            txtPklDateFrom.Text = fstrDefaultStartDate
        End If

        txtPklDateTo.Text = Request.Form("txtPklDateTo")
        If Not IsDate(txtPklDateTo.Text) Then
            txtPklDateTo.Text = fstrDefaultEndDate
        End If

        txtPrfDateFrom.Text = Request.Form("txtPrfDateFrom")
        If Not IsDate(txtPrfDateFrom.Text) Then
            txtPrfDateFrom.Text = fstrDefaultStartDate
        End If

        txtPrfDateTo.Text = Request.Form("txtPrfDateTo")
        If Not IsDate(txtPrfDateTo.Text) Then
            txtPrfDateTo.Text = fstrDefaultEndDate
        End If

        txtCollectionDateFrom.Text = Request.Form("txtCollectionDDateFrom")
        If Not IsDate(txtCollectionDateFrom.Text) Then
            txtCollectionDateFrom.Text = fstrDefaultStartDate
        End If

        txtCollectionDateTo.Text = Request.Form("txtCollectionDDateTo")
        If Not IsDate(txtCollectionDateTo.Text) Then
            txtCollectionDateTo.Text = fstrDefaultEndDate
        End If

    End Sub
#Region " view report message"
    Public Sub viewreport()

        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        Dim objRptPkls As New clsRptPKLS


        If Trim(Me.txtTechnicianFrom.Text) = "AAA" Then
            objRptPkls.TechnicianFrom = ""
        Else
            objRptPkls.TechnicianFrom = Trim(txtTechnicianFrom.Text).ToUpper.ToString()
        End If

        'If Trim(Me.txtTechnicianTo.Text) = "ZZZ" Then
        '    objRptPkls.TechnicianTo = "ZZZZZZZZZZZ"
        'Else
        objRptPkls.TechnicianTo = Trim(txtTechnicianTo.Text).ToUpper.ToString()
        'End If

        If Trim(Me.txtServiceCenterFrom.Text) = "AAA" Then
            objRptPkls.ServiceCenterFrom = ""
        Else
            objRptPkls.ServiceCenterFrom = Trim(txtServiceCenterFrom.Text)
        End If

        'If Trim(Me.txtServiceCenterTo.Text) = "ZZZ" Then
        '    objRptPkls.ServiceCenterTo = "ZZZZZZZZZZZ"
        'Else
        objRptPkls.ServiceCenterTo = Trim(txtServiceCenterTo.Text).ToUpper.ToString()
        'End If

        If Trim(Me.txtPartFrom.Text) = "AAA" Then
            objRptPkls.PartFrom = ""
        Else
            objRptPkls.PartFrom = Trim(txtPartFrom.Text)
        End If

        'If Trim(Me.txtPartTo.Text) = "ZZZ" Then
        '    objRptPkls.PartTo = "ZZZZZZZZZZZ"
        'Else
        objRptPkls.PartTo = Trim(txtPartTo.Text).ToUpper.ToString()
        'End If

        If Trim(Me.txtStateFrom.Text) = "AAA" Then
            objRptPkls.StateFrom = ""
        Else
            objRptPkls.StateFrom = Trim(txtStateFrom.Text)
        End If

        'If Trim(Me.txtStateTo.Text) = "ZZZ" Then
        '    objRptPkls.StateTo = "ZZZZZZZZZZZ"
        'Else
        objRptPkls.StateTo = Trim(txtStateTo.Text).ToUpper.ToString()
        'End If

        If Trim(Me.txtPrfFrom.Text) = "AAA" Then
            objRptPkls.PrfNoFrom = ""
        Else
            objRptPkls.PrfNoFrom = Trim(txtPrfFrom.Text)
        End If

        'If Trim(Me.txtPrfTo.Text) = "ZZZ" Then
        '    objRptPkls.PrfNoTo = "ZZZZZZZZZZZ"
        'Else
        objRptPkls.PrfNoTo = Trim(txtPrfTo.Text).ToUpper.ToString()
        'End If

        objRptPkls.OnlyPkl = cboOnlyPkl.SelectedValue
        objRptPkls.OnlyPklText = cboOnlyPkl.SelectedItem.Text

        objRptPkls.SortBy = cboSortBy.SelectedValue
        objRptPkls.SortByText = cboSortBy.SelectedItem.Text

        objRptPkls.ThenBy = cboThenBy.SelectedValue
        objRptPkls.ThenByText = cboThenBy.SelectedItem.Text

        If IsDate(Me.txtPklDateFrom.Text) Then
            objRptPkls.PklDateFrom = txtPklDateFrom.Text
        Else
            objRptPkls.PklDateFrom = fstrDefaultStartDate
        End If

        If IsDate(Me.txtPklDateTo.Text) Then
            objRptPkls.PklDateTo = txtPklDateTo.Text
        Else
            objRptPkls.PklDateTo = fstrDefaultEndDate
        End If

        If IsDate(Me.txtPrfDateFrom.Text) Then
            objRptPkls.PrfDateFrom = txtPrfDateFrom.Text
        Else
            objRptPkls.PrfDateFrom = fstrDefaultStartDate
        End If

        If IsDate(Me.txtPrfDateTo.Text) Then
            objRptPkls.PrfDateTo = txtPrfDateTo.Text
        Else
            objRptPkls.PrfDateTo = fstrDefaultEndDate
        End If

        If IsDate(Me.txtCollectionDateFrom.Text) Then
            objRptPkls.CollectionDateFrom = txtCollectionDateFrom.Text
        Else
            objRptPkls.CollectionDateFrom = fstrDefaultStartDate
        End If

        If IsDate(Me.txtCollectionDateTo.Text) Then
            objRptPkls.CollectionDateTo = txtCollectionDateTo.Text
        Else
            objRptPkls.CollectionDateTo = fstrDefaultEndDate
        End If

        objRptPkls.UserID = Session("userID")

        Session("RptPKLS") = objRptPkls
        'Session("pklsflag") = "1"
    End Sub
#End Region

    Protected Sub lnkViewReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkViewReport.Click

        viewreport()
        Dim script As String = "window.open('../report/RptPKLSView.aspx')"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "RPTBDPTVIEW", script, True)
        Return
    End Sub

End Class
