﻿Imports BusinessEntity
Imports System.Configuration
Imports System.Xml
Imports System.Data
Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web
Partial Class PresentationLayer_report_RPTSVBT_SUB
    Inherits System.Web.UI.Page
    Dim clsReport As New ClsCommonReport

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try

            Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim obiXML As New clsXml
            obiXML.XmlFile = ConfigurationManager.AppSettings("XmlFilePath")
            Me.titleLab.Text = obiXML.GetLabelName("EngLabelMsg", "RPT_SERBITECH_TITLE")
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


            
            Dim RptSVBC As New ClsRptSVBC
            RptSVBC = Session("rptSVBC_search_condition")

            Dim reportds As DataSet = Nothing
            reportds = RptSVBC.GetRptSVBT()
           


            Dim objXmlTr As New clsXml
          
            Dim strHead As Array = Array.CreateInstance(GetType(String), 50)
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            strHead(0) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_SERBITECH_DATE")
            strHead(1) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_SERBITECH_CAB")
            strHead(2) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_SERBITECH_CUSTNM")
            strHead(3) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_SERBITECH_BITO")
            strHead(4) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_SERBITECH_CASH")
            strHead(5) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_SERBITECH_CHQ")
            strHead(6) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_SERBITECH_CC")
            strHead(7) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_SERBITECH_CD")
            strHead(8) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_SERBITECH_DISC")
            strHead(9) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_SERBITECH_TOPAY")
            strHead(10) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_SERBITECH_VAR")
            strHead(11) = objXmlTr.GetLabelName("EngLabelMsg", "RPT-SERBITECH-TITLE1")
            strHead(12) = objXmlTr.GetLabelName("EngLabelMsg", "RPT-SERBITECH-TITLE3")
            strHead(13) = RptSVBC.Minivdate
            strHead(14) = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            strHead(15) = RptSVBC.Maxivdate
            strHead(16) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_SERBITECH_REPID")
            strHead(17) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_SERBITECH_TOT")
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            strHead(18) = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            strHead(19) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_SERBITECH_STA")
            strHead(20) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_SERBITECH_SER")
            strHead(21) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_SERBITECH_INVDATE")
            strHead(22) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_SERBITECH_TECHID")
            strHead(23) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_SERBITECH_SUBDATE")
            strHead(24) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_SERBITECH_CUSNM")
            strHead(25) = objXmlTr.GetLabelName("EngLabelMsg", "BB-SORTBY")
            strHead(26) = objXmlTr.GetLabelName("EngLabelMsg", "BB-THENBY")
            strHead(27) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_SERBITECH_CANBY")



            If (RptSVBC.Minstaid = Nothing) Then
                strHead(28) = "AAA"
            Else

                strHead(28) = RptSVBC.Minstaid
            End If

            If (RptSVBC.Maxstaid = "ZZZZZZZZZZ") Then
                strHead(29) = "ZZZ"
            Else

                strHead(29) = RptSVBC.Maxstaid
            End If

            If (RptSVBC.Minserid = Nothing) Then
                strHead(30) = "AAA"
            Else

                strHead(30) = RptSVBC.Minserid
            End If


            If (RptSVBC.Maxserid = "ZZZZZZZZZZ") Then
                strHead(31) = "ZZZ"
            Else

                strHead(31) = RptSVBC.Maxserid
            End If


            If (RptSVBC.Minivdate = Nothing) Then
                strHead(32) = " "
            Else

                strHead(32) = RptSVBC.Minivdate
            End If

            If (RptSVBC.Maxivdate = Nothing) Then
                strHead(33) = " "
            Else

                strHead(33) = RptSVBC.Maxivdate
            End If

            If (RptSVBC.Mintecid = Nothing) Then
                strHead(34) = "AAA"
            Else

                strHead(34) = RptSVBC.Mintecid
            End If

            If (RptSVBC.Maxtecid = "ZZZZZZZZZZ") Then
                strHead(35) = "ZZZ"
            Else

                strHead(35) = RptSVBC.Maxtecid
            End If


            If (RptSVBC.Minsubtime = Nothing) Then
                strHead(36) = " "
            Else

                strHead(36) = RptSVBC.Minsubtime
            End If

            If (RptSVBC.Maxsubtime = Nothing) Then
                strHead(37) = " "
            Else

                strHead(37) = RptSVBC.Maxsubtime
            End If


            If (RptSVBC.Mincusid = Nothing) Then
                strHead(38) = "AAA"
            Else

                strHead(38) = RptSVBC.Mincusid
            End If

            If (RptSVBC.Maxcusid = "ZZZZZZZZZZ") Then
                strHead(39) = "ZZZ"
            Else

                strHead(39) = RptSVBC.Maxcusid
            End If
            strHead(40) = RptSVBC.Sortby1

            strHead(41) = RptSVBC.Thenby1

            strHead(42) = RptSVBC.Cancel1

            If Session("userID") = "" Then
                strHead(43) = " "
            Else
                strHead(43) = Session("userID").ToString().ToUpper + " / " + Session("username").ToString.ToUpper

            End If

            strHead(44) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCUSR-USERID")

            strHead(45) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_APT_RECORD")
            strHead(46) = reportds.Tables(0).Rows.Count
            strHead(47) = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_E_HEADER10")

            strHead(48) = objXmlTr.GetLabelName("EngLabelMsg", "SBA-SERVICEBILLTYPE")
            strHead(49) = RptSVBC.ServiceBillTypeText


            With clsReport
                .ReportFileName = "RPTSVBT.rpt"
                .SetReport(CrystalReportViewer1, reportds, strHead)
            End With
            'Dim lstrPdfFileName As String = "ServiceBillCollection_" & Session("login_session") & ".pdf"
            'Try
            '    With clsReport
            '        .ReportFileName = "RPTSVBT.rpt"
            '        .SetReportDocument(reportds, strHead)
            '        .PdfFileName = lstrPdfFileName
            '        .ExportPdf()
            '    End With

            '    Response.Redirect(clsReport.PdfUrl)

            'Catch err As Exception
            '    System.IO.File.Delete(lstrPhysicalFile)
            '    Response.Write("<BR>")
            '    Response.Write(err.Message.ToString)

            'End Try


        Catch ex As Exception
            Response.Write(ex.Message)

        End Try
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        clsReport.UnloadReport()
        CrystalReportViewer1.Dispose()
    End Sub

End Class
