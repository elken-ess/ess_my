<%@ Page Language="VB" AutoEventWireup="false" CodeFile="RptBDPT.aspx.vb" Inherits="PresentationLayer_report_RPTBDPT" %>

<%@ Register Assembly="JCalendar" Namespace="JCalendar" TagPrefix="cc1" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Breakdown on part by technician report</title>
    <meta http-equiv="Content-Type" content="text/html; charset=gb2312">
	<LINK href="../css/style.css" type="text/css" rel="stylesheet">     
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
      <script language="JavaScript" src="../js/common.js"></script>
            
</head>
<body>
    <form id="form1" runat="server">
    <div>
     <table id="countrytab" border="0" width="100%">
            <tr>
                <td style="width: 100%">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title1.gif" width="5" /></td>
                            <td background="../graph/title_bg.gif" class="style2" width="98%">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title_2.gif" width="5" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100%">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" class="style2" width="98%">
                                <font color="red">
                                    <asp:Label ID="errlab" runat="server" Text="Label" Visible="false"></asp:Label></font></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100%">
                    <table id="country" bgcolor="#b7e6e6" border="0" cellpadding="2" cellspacing="1" width ="100%">
                        <tr bgcolor="#ffffff">
                            <tr bgcolor="#ffffff">
                            <td align="left" style="width: 18%">
                                <asp:Label ID="lblTechnician" runat="server" Text="Technician ID" Font-Bold="False" Height="16px"></asp:Label></td>
                            <td align="left" style="width: 5%">
                                <asp:Label ID="lblFrom1" runat="server" Text="From" Height="16px"></asp:Label></td>
                            <td align="left" style="width: 27%">
                                <asp:TextBox ID="txtTechnicianFrom" runat="server" CssClass="textborder" MaxLength="10" style="width: 80%">AAA</asp:TextBox>
                               
                            </td>
                            <td align="left" style="width: 9%">
                                <asp:Label ID="lblTo1" runat="server" Text="To"></asp:Label></td>
                            <td align="left" width="35%">
                                <asp:TextBox ID="txtTechnicianTo" runat="server" CssClass="textborder" MaxLength="10" style="width: 80%">ZZZ</asp:TextBox>
                               
                            </td>
                        </tr>
                        
                         
                        <tr bgcolor="#ffffff">
                            <td align="left" style="width: 18%">
                                <asp:Label ID="lblPklDate" runat="server" Text="PKL Date" Font-Bold="False" Height="16px"></asp:Label></td>
                            <td align="left" style="width: 5%">
                                <asp:Label ID="lblFrom2" runat="server" Text="From" Height="16px"></asp:Label></td>
                            <td align="left" style="width: 27%">
                                <asp:TextBox ID="txtPklDateFrom" runat="server" CssClass="textborder" MaxLength="10" style="width: 80%">AAA</asp:TextBox>
                               <asp:HyperLink ID="HypCalPklDateFrom" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif" ToolTip="Choose a Date">Choose a Date</asp:HyperLink>

                            </td>
                            <td align="left" style="width: 9%">
                                <asp:Label ID="lblTo2" runat="server" Text="To"></asp:Label></td>
                            <td align="left" width="35%">
                                <asp:TextBox ID="txtPklDateTo" runat="server" CssClass="textborder" MaxLength="10" style="width: 80%">ZZZ</asp:TextBox>
                               <asp:HyperLink ID="HypCalPklDateTo" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif" ToolTip="Choose a Date">Choose a Date</asp:HyperLink>

                            </td>                            
                        </tr>
                        
                        <tr bgcolor="#ffffff">
                            <td align="left" style="width: 18%">
                                <asp:Label ID="lblCollectionDate" runat="server" Text="Collection Date" Font-Bold="False" Height="16px"></asp:Label></td>
                            <td align="left" style="width: 5%">
                                <asp:Label ID="lblFrom3" runat="server" Text="From" Height="16px"></asp:Label></td>
                            <td align="left" style="width: 27%">
                                <asp:TextBox ID="txtCollectionDateFrom" runat="server" CssClass="textborder" MaxLength="10" style="width: 80%">AAA</asp:TextBox>
                               <asp:HyperLink ID="HypCalCollectionDateFrom" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif" ToolTip="Choose a Date">Choose a Date</asp:HyperLink>

                            </td>
                            <td align="left" style="width: 9%">
                                <asp:Label ID="lblTo3" runat="server" Text="To"></asp:Label></td>
                            <td align="left" width="35%">
                                <asp:TextBox ID="txtCollectionDateTo" runat="server" CssClass="textborder" MaxLength="10" style="width: 80%">ZZZ</asp:TextBox>
                               <asp:HyperLink ID="HypCalCollectionDateTo" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif" ToolTip="Choose a Date">Choose a Date</asp:HyperLink>

                            </td>
                        </tr>
                        
                        <tr bgcolor="#ffffff">
                            <td align="left" style="width: 18%">
                                <asp:Label ID="lblPrfDate" runat="server" Text="PRF Date" Font-Bold="False" Height="16px" Visible =false ></asp:Label></td>
                            <td align="left" style="width: 5%">
                                <asp:Label ID="lblFrom4" runat="server" Text="From" Height="16px" Visible =false ></asp:Label></td>
                            <td align="left" style="width: 27%">
                               <asp:TextBox ID="txtPrfDateFrom" runat="server" CssClass="textborder" MaxLength="10" style="width: 80%" Visible =false >AAA</asp:TextBox>
                               <asp:HyperLink ID="HypCalPrfDateFrom" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif" ToolTip="Choose a Date" Visible =false >Choose a Date</asp:HyperLink>

                            </td>
                            <td align="left" style="width: 9%">
                                <asp:Label ID="lblTo4" runat="server" Text="To" Visible =false ></asp:Label></td>
                            <td align="left" width="35%">
                                <asp:TextBox ID="txtPrfDateTo" runat="server" CssClass="textborder" MaxLength="10" style="width: 80%" Visible =false >ZZZ</asp:TextBox>
                               <asp:HyperLink ID="HypCalPrfDateTo" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif" ToolTip="Choose a Date" Visible =false >Choose a Date</asp:HyperLink>

                            </td>
                        </tr>
                        
                        <tr bgcolor="#ffffff">
                            <td align="left" style="width: 18%">
                                <asp:Label ID="lblServiceCenter" runat="server" Text="Service Center ID" Font-Bold="False" Height="16px"></asp:Label></td>
                            <td align="left" style="width: 5%">
                                <asp:Label ID="lblFrom5" runat="server" Text="From" Height="16px"></asp:Label></td>
                            <td align="left" style="width: 27%">
                                <asp:TextBox ID="txtServiceCenterFrom" runat="server" CssClass="textborder" MaxLength="10" style="width: 80%">AAA</asp:TextBox>
                               
                            </td>
                            <td align="left" style="width: 9%">
                                <asp:Label ID="lblTo5" runat="server" Text="To"></asp:Label></td>
                            <td align="left" width="35%">
                                <asp:TextBox ID="txtServiceCenterTo" runat="server" CssClass="textborder" MaxLength="10" style="width: 80%">ZZZ</asp:TextBox>
                               
                            </td>
                        </tr>
                        
                        
                        <tr bgcolor="#ffffff">
                            <td align="left" style="width: 18%">
                                <asp:Label ID="lblState" runat="server" Text="State ID" Font-Bold="False" Height="16px"></asp:Label></td>
                            <td align="left" style="width: 5%">
                                <asp:Label ID="lblFrom6" runat="server" Text="From" Height="16px"></asp:Label></td>
                            <td align="left" style="width: 27%">
                                <asp:TextBox ID="txtStateFrom" runat="server" CssClass="textborder" MaxLength="10" style="width: 80%">AAA</asp:TextBox>
                               
                            </td>
                            <td align="left" style="width: 9%">
                                <asp:Label ID="lblTo6" runat="server" Text="To"></asp:Label></td>
                            <td align="left" width="35%">
                                <asp:TextBox ID="txtStateTo" runat="server" CssClass="textborder" MaxLength="10" style="width: 80%">ZZZ</asp:TextBox>
                               
                            </td>
                        </tr>
                        
                        <tr bgcolor="#ffffff">
                            <td align="left" style="width: 18%">
                                <asp:Label ID="lblPart" runat="server" Text="Part Code" Font-Bold="False" Height="16px"></asp:Label></td>
                            <td align="left" style="width: 5%">
                                <asp:Label ID="lblFrom7" runat="server" Text="From" Height="16px"></asp:Label></td>
                            <td align="left" style="width: 27%">
                                <asp:TextBox ID="txtPartFrom" runat="server" CssClass="textborder" MaxLength="10" style="width: 80%">AAA</asp:TextBox>
                               
                            </td>
                            <td align="left" style="width: 9%">
                                <asp:Label ID="lblTo7" runat="server" Text="To"></asp:Label></td>
                            <td align="left" width="35%">
                                <asp:TextBox ID="txtPartTo" runat="server" CssClass="textborder" MaxLength="10" style="width: 80%">ZZZ</asp:TextBox>
                               
                            </td>
                        </tr>
                        
                        <tr bgcolor="#ffffff">
                            <td align="left" style="width: 18%">
                                <asp:Label ID="lblOnlyPkl" runat="server" Text="Only PKL" Font-Bold="False" Height="16px"></asp:Label></td>
                            <td align="left" style="width: 5%">                               
                            <td align="left" style="width: 27%">
                                <asp:DropDownList ID="cboOnlyPkl" runat="server" Width="80%">
                                </asp:DropDownList>
                            </td>
                            <td align="left" style="width: 9%">                                
                            <td align="left" width="35%">                                
                            </td>
                        </tr>
                        
                        <tr bgcolor="#ffffff">
                            <td align="left" style="width: 18%">
                                <asp:Label ID="lblSortBy" runat="server" Text="Sort By" Font-Bold="False" Height="16px"></asp:Label></td>
                            <td align="left" style="width: 5%">                               
                            <td align="left" style="width: 27%">
                                <asp:DropDownList ID="cboSortBy" runat="server" Width="80%">
                                </asp:DropDownList>
                            </td>
                            <td align="left" style="width: 9%">                                
                            <td align="left" width="35%">                                
                            </td>
                        </tr>
                        
                        <tr bgcolor="#ffffff">
                            <td align="left" style="width: 18%">
                                <asp:Label ID="lblThenBy" runat="server" Text="Then By" Font-Bold="False" Height="16px"></asp:Label></td>
                            <td align="left" style="width: 5%">                               
                            <td align="left" style="width: 27%">
                                <asp:DropDownList ID="cboThenBy" runat="server" Width="80%">
                                </asp:DropDownList>
                            </td>
                            <td align="left" style="width: 9%">                                
                            <td align="left" width="35%">                                
                            </td>
                        </tr>
                        
                        
                        
                    </table>
                    <asp:LinkButton ID="lnkViewReport" runat="server">View Report</asp:LinkButton></td>
            </tr>
        </table>    
    </div>
        <cr:crystalreportviewer id="CrystalReportViewer1" runat="server" autodatabind="true" Height="50px" Width="350px"></cr:crystalreportviewer>
    </form>
</body>
</html>
