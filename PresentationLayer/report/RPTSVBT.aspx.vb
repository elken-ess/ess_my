﻿Imports BusinessEntity
Imports System.Configuration
Imports System.Data
Imports System.Data.SqlClient
Imports System.Xml
Partial Class PresentationLayer_Report_SerBillTech
    Inherits System.Web.UI.Page
    Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
    Dim fstrMonth As String = ""
    Dim fstrDefaultStartDate As String = ""
    Dim fstrDefaultEndDate As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        fstrMonth = Month(System.DateTime.Today)
        fstrDefaultStartDate = "01/" & IIf(Len(fstrMonth) = 1, "0" & fstrMonth, fstrMonth) & "/" & Year(System.DateTime.Today)
        fstrDefaultEndDate = DateAdd(DateInterval.Month, 1, CDate(fstrDefaultStartDate))

        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If

        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try

        If Not Page.IsPostBack Then

            Session("rptSBTflag") = "0"
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            Me.from1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Me.from2.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Me.from3.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Me.from4.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Me.from5.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Me.from6.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Me.to1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Me.to2.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Me.to3.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Me.to4.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Me.to5.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Me.to6.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")

            Me.stalab.Text = objXmlTr.GetLabelName("EngLabelMsg", "RPT_SERBITECH_STA")
            Me.sercelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "RPT_SERBITECH_SER")
            Me.invdtlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "RPT_SERBITECH_INVDATE")
            Me.tenicanlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "RPT_SERBITECH_TECHID")
            Me.subdtlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "RPT_SERBITECH_SUBDATE")
            Me.custlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "RPT_SERBITECH_CUSNM")
            Me.sortlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-SORTBY")
            Me.thenlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-THENBY")
            Me.cancelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "RPT_SERBITECH_CANBY")
            Me.LinkButton1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-VIEWREPORT")
            Me.titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "RPT_SERBITECH_TITLE")
            Me.lblServiceBillType.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-SERVICEBILLTYPE")

            '''access control'''''''''''''''''''''''''''''''''''''''''''''''
            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "48")
            Me.LinkButton1.Enabled = purviewArray(3)


            Dim dltype As New clsrlconfirminf()
            Dim dlParam As ArrayList = New ArrayList
            dlParam = dltype.searchconfirminf("YESNOALL")
            Dim dlcount As Integer
            Dim dlid As String
            Dim dlnm As String
            For dlcount = 0 To dlParam.Count - 1
                dlid = dlParam.Item(dlcount)
                dlnm = dlParam.Item(dlcount + 1)
                dlcount = dlcount + 1

                Me.cancby.Items.Add(New ListItem(dlnm.ToString(), dlid.ToString()))

            Next
            cancby.SelectedIndex = 1

            Dim sotrnm1 As String = objXmlTr.GetLabelName("EngLabelMsg", "RPT_SERBITECH_DATE")
            Dim sotrnm2 As String = objXmlTr.GetLabelName("EngLabelMsg", "RPT_SERBITECH_CUSTNM")
            Dim sotrnm3 As String = objXmlTr.GetLabelName("EngLabelMsg", "SBA-SERVICEBILLNO")
            Me.sorby.Items.Add(New ListItem(sotrnm1, "FIV1_INVDT"))
            Me.sorby.Items.Add(New ListItem(sotrnm2, "MCUS_ENAME"))
            Me.sorby.Items.Add(New ListItem(sotrnm3, "A.FIV1_SVBIL"))
            sorby.SelectedIndex = 0

            Me.thenby.Items.Add(New ListItem(sotrnm1, "FIV1_INVDT"))
            Me.thenby.Items.Add(New ListItem(sotrnm2, "MCUS_ENAME"))
            Me.thenby.Items.Add(New ListItem(sotrnm3, "A.FIV1_SVBIL"))
            thenby.SelectedIndex = 1


            Me.invdate1.Text = fstrDefaultStartDate
            Me.subdt1.Text = fstrDefaultStartDate
            Me.invdate2.Text = fstrDefaultEndDate
            Me.subdt2.Text = fstrDefaultEndDate

            Dim Appbond As New clsCommonClass
            Dim rank As Integer = Session("login_rank")
            Appbond.rank = rank
            If rank <> 0 Then
                Appbond.spctr = Session("login_ctryID")
                Appbond.spstat = Session("login_cmpID")
                Appbond.sparea = Session("login_svcID")
            End If

            Dim Util As New clsUtil()
            Dim statParam As ArrayList = New ArrayList


            '---- SERVICE BILL TYPE
            Me.cboServiceBillType.ClearSelection()
            Me.cboServiceBillType.Items.Add(New ListItem("All", ""))
            statParam = Util.searchconfirminf("SERBILLTY")
            Dim count As Integer
            Dim statid As String
            Dim statnm As String
            For count = 0 To statParam.Count - 1
                statid = statParam.Item(count)
                statnm = statParam.Item(count + 1)
                count = count + 1
                Me.cboServiceBillType.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
            Next


        ElseIf Session("rptSBTflag") = "1" Then
            print_report()
        End If


        HypCalinvdate1.NavigateUrl = "javascript:DoCal(document.form1.invdate1);"
        HypCalsubdt1.NavigateUrl = "javascript:DoCal(document.form1.subdt1);"


        HypCalinvdate2.NavigateUrl = "javascript:DoCal(document.form1.invdate2);"
        HypCalsubdt2.NavigateUrl = "javascript:DoCal(document.form1.subdt2);"


    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton1.Click
        Session("rptSBTflag") = "1"
        print_report()
        'Response.Redirect("~/PresentationLayer/report/RPTSVBT_SUB.aspx")
        Dim script As String = "window.open('../report/RPTSVBT_SUB.aspx')"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "SystemAccessReport", script, True)
    End Sub
    Public Function print_report()
        Try
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

            Dim clsReport As New ClsCommonReport
            Dim RptSVBC As New ClsRptSVBC



            invdate1.Text = Request.Form("invdate1")
            subdt1.Text = Request.Form("subdt1")
            invdate2.Text = Request.Form("invdate2")
            subdt2.Text = Request.Form("subdt2")

            If Not IsDate(invdate1.Text) Then
                invdate1.Text = fstrDefaultStartDate
            End If
            If Not IsDate(subdt1.Text) Then
                subdt1.Text = fstrDefaultStartDate
            End If
            If Not IsDate(invdate2.Text) Then
                invdate2.Text = fstrDefaultEndDate
            End If
            If Not IsDate(subdt2.Text) Then
                subdt2.Text = fstrDefaultEndDate
            End If

            If (Trim(Me.stat1.Text) <> "AAA") Then

                RptSVBC.Minstaid = stat1.Text.ToUpper()

            End If
            If (Trim(Me.stat2.Text) <> "ZZZ") Then

                RptSVBC.Maxstaid = stat2.Text.ToUpper()
            Else
                RptSVBC.Maxstaid = "ZZZZZZZZZZ"
            End If
            If (Trim(Me.serce1.Text) <> "AAA") Then

                RptSVBC.Minserid = serce1.Text.ToUpper()

            End If
            If (Trim(Me.serce2.Text) <> "ZZZ") Then

                RptSVBC.Maxserid = serce2.Text.ToUpper()
            Else
                RptSVBC.Maxserid = "ZZZZZZZZZZ"

            End If
            If (Trim(Me.techni1.Text) <> "AAA") Then

                RptSVBC.Mintecid = techni1.Text.ToUpper()

            End If
            If (Trim(Me.techni2.Text) <> "ZZZ") Then

                RptSVBC.Maxtecid = techni2.Text.ToUpper()
            Else
                RptSVBC.Maxtecid = "ZZZZZZZZZZ"
            End If

            If (Trim(Me.invdate1.Text) <> "") Then
                RptSVBC.Minivdate = invdate1.Text
                'If (Trim(Me.invdate1.Text) <> "01/01/2006") Then
                '    Dim temparr As Array = invdate1.Text.Split("/")
                '    RptSVBC.Minivdate = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
                'Else
                '    RptSVBC.Minivdate = "2006-01-01"
                'End If
            End If

            If (Trim(Me.invdate2.Text) <> "") Then
                RptSVBC.Maxivdate = invdate2.Text
                'If (Trim(Me.invdate2.Text) <> "31/12/2006") Then
                '    Dim temparr As Array = invdate2.Text.Split("/")
                '    RptSVBC.Maxivdate = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
                'Else
                '    RptSVBC.Maxivdate = "2006-12-31"
                'End If
            End If

            If (Trim(Me.subdt1.Text) <> "") Then
                RptSVBC.Minsubtime = subdt1.Text
                'If (Trim(Me.subdt1.Text) <> "01/01/2006") Then
                '    Dim temparr As Array = subdt1.Text.Split("/")
                '    RptSVBC.Minsubtime = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
                'Else
                '    RptSVBC.Minsubtime = "2006-01-01"
                'End If
            End If

            If (Trim(Me.subdt2.Text) <> "") Then
                RptSVBC.Maxsubtime = Me.subdt2.Text
                'If (Trim(Me.subdt2.Text) <> "31/12/2006") Then
                '    Dim temparr As Array = subdt2.Text.Split("/")
                '    RptSVBC.Maxsubtime = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
                'Else
                '    RptSVBC.Maxsubtime = "2006-12-31"

                'End If
            End If

            If (Trim(Me.cusid1.Text) <> "AAA") Then

                RptSVBC.Mincusid = cusid1.Text.ToUpper()

            End If
            If (Trim(Me.cusid2.Text) <> "ZZZ") Then

                RptSVBC.Maxcusid = cusid2.Text.ToUpper()
            Else
                RptSVBC.Maxcusid = "ZZZ"
            End If

            RptSVBC.ServiceBillType = cboServiceBillType.SelectedValue
            RptSVBC.ServiceBillTypeText = cboServiceBillType.SelectedItem.Text.Trim


            Dim rank As String = Session("login_rank")
            If rank <> 0 Then
                RptSVBC.ctrid = Session("login_ctryID").ToString().ToUpper
                RptSVBC.comid = Session("login_cmpID").ToString().ToUpper
                RptSVBC.svcid = Session("login_svcID").ToString().ToUpper
            End If
            RptSVBC.rank = rank

            If (Me.thenby.SelectedItem.Value.ToString() <> "") Then
                RptSVBC.Thenby = thenby.SelectedItem.Value.ToString()
            End If
            RptSVBC.Thenby1 = thenby.SelectedItem.Text.ToString()

            If (Me.sorby.SelectedItem.Value.ToString() <> "") Then
                RptSVBC.Sortby = sorby.SelectedItem.Value.ToString()
            End If
            RptSVBC.Sortby1 = sorby.SelectedItem.Text.ToString()

            If (Me.cancby.SelectedItem.Value.ToString() <> "") Then
                RptSVBC.Cancel = cancby.SelectedItem.Value.ToString()

            End If
            RptSVBC.Cancel1 = cancby.SelectedItem.Text.ToString()


            Session("rptSVBC_search_condition") = RptSVBC



        Catch ex As Exception
            Response.Write(ex.Message)

        End Try
    End Function

    'Protected Sub effecalender_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles effecalender.Click
    '    Me.Calendar1.Visible = True
    '    Calendar1.Attributes.Add("style", " POSITION: absolute")
    '    Session("rptSBTflag") = "0"
    'End Sub

    'Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
    '    Me.Calendar2.Visible = True
    '    Calendar2.Attributes.Add("style", " POSITION: absolute")
    '    Session("rptSBTflag") = "0"
    'End Sub

    'Protected Sub ImageButton2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton2.Click
    '    Me.Calendar3.Visible = True
    '    Calendar3.Attributes.Add("style", " POSITION: absolute")
    '    Session("rptSBTflag") = "0"
    'End Sub

    'Protected Sub ImageButton3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton3.Click
    '    Me.Calendar4.Visible = True
    '    Calendar4.Attributes.Add("style", " POSITION: absolute")
    '    Session("rptSBTflag") = "0"
    'End Sub

    'Protected Sub Calendar1_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendar1.SelectionChanged
    '    Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
    '    System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
    '    Dim prcidDate As New clsCommonClass()
    '    Me.invdate1.Text = Calendar1.SelectedDate.Date.ToString().Substring(0, 10)
    '    Calendar1.Visible = False
    '    Session("rptSBTflag") = "0"
    'End Sub

    'Protected Sub Calendar2_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendar2.SelectionChanged
    '    Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
    '    System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
    '    Dim prcidDate As New clsCommonClass()
    '    Me.invdate2.Text = Calendar2.SelectedDate.Date.ToString().Substring(0, 10)
    '    Calendar2.Visible = False
    '    Session("rptSBTflag") = "0"
    'End Sub

    'Protected Sub Calendar3_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendar3.SelectionChanged
    '    Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
    '    System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
    '    Dim prcidDate As New clsCommonClass()
    '    Me.subdt1.Text = Calendar3.SelectedDate.Date.ToString().Substring(0, 10)
    '    Calendar3.Visible = False
    '    Session("rptSBTflag") = "0"
    'End Sub

    'Protected Sub Calendar4_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendar4.SelectionChanged
    '    Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
    '    System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
    '    Dim prcidDate As New clsCommonClass()
    '    Me.subdt2.Text = Calendar4.SelectedDate.Date.ToString().Substring(0, 10)
    '    Calendar4.Visible = False
    '    Session("rptSBTflag") = "0"
    'End Sub

    
 
    
End Class
