Imports SQLDataAccess
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
Imports BusinessEntity
Imports CrystalDecisions.Web
Imports CrystalDecisions.CrystalReports.Engine
Partial Class PresentationLayer_report_RPTPADLVIEW_aspx
    Inherits System.Web.UI.Page
    Dim clsReport As New ClsCommonReport

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Buffer = True
        Response.Expires = 100000
        Dim lstrDebugMsg As String = ""

        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Response.Redirect("~/PresentationLayer/logon.aspx")
            End If
        Catch ex As Exception
            Response.Redirect("~/PresentationLayer/logon.aspx")
        End Try
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Dim obiXML As New clsXml
        obiXML.XmlFile = ConfigurationManager.AppSettings("XmlFilePath")
        Me.titleLab.Text = obiXML.GetLabelName("EngLabelMsg", "BB-RPTPADL-TITLE2")

        Try

            lstrDebugMsg = "Assign Data from session"

            Dim clsrptpadl As New ClsRptPADL
            clsrptpadl.username = Session("username")
            clsrptpadl.userid = Session("userID")

            clsrptpadl = Session("rptpadl_search_condition")

            Dim ds As DataSet = Nothing
            ds = clsrptpadl.GetRptpadl()

            Dim statXmlTr As New clsXml
            statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")


            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            lstrDebugMsg = "set report label"
            Dim objXmlTr As New clsXml
            Dim strHead As Array = Array.CreateInstance(GetType(String), 33)
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")

            strHead(0) = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0013") + ":"
            strHead(1) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTPADL-ADDRE1")
            strHead(2) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCUSR-USERID")
            strHead(3) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTPADL-REPORTID")

            strHead(4) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDAR-TITLE1")
            strHead(5) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTPADL-PRAD")
            strHead(6) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTPADL-TITLE2")
            strHead(7) = clsrptpadl.Mindate
            strHead(8) = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            strHead(9) = clsrptpadl.Maxdate
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            strHead(10) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDAR-SVC")
            strHead(11) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSOSA-CUSTID")
            strHead(12) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTMDSL-0003")
            strHead(13) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDAR-DATE")
            strHead(14) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSOSA-MODEL")
            strHead(15) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTPADL-AREA")

            strHead(16) = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTATE-0001")

            strHead(17) = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")

            strHead(18) = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If clsrptpadl.Minsvcid <> "" Then
                strHead(19) = clsrptpadl.Minsvcid
            Else
                strHead(19) = "AAA".ToString()
            End If
            If clsrptpadl.Maxsvcid <> "ZZZZZZZZZZ" Then
                strHead(20) = clsrptpadl.Maxsvcid
            Else
                strHead(20) = "ZZZ".ToString()
            End If
            If clsrptpadl.Mincustid <> "" Then
                strHead(21) = clsrptpadl.Mincustid
            Else
                strHead(21) = "AAA".ToString()
            End If
            If clsrptpadl.Maxcustid <> "ZZZZZZZZZZZZZZZZZZZZ" Then
                strHead(22) = clsrptpadl.Maxcustid
            Else
                strHead(22) = "ZZZ".ToString()
            End If
            If clsrptpadl.Minserialno <> "" Then
                strHead(23) = clsrptpadl.Minserialno
            Else
                strHead(23) = "AAA".ToString()
            End If
            If clsrptpadl.Maxserialno <> "ZZZZZZZZZZ" Then
                strHead(24) = clsrptpadl.Maxserialno
            Else
                strHead(24) = "ZZZ".ToString()
            End If
            If clsrptpadl.Mindate <> "" Then
                strHead(25) = clsrptpadl.Mindate
            Else
                strHead(25) = " ".ToString()
            End If

            If clsrptpadl.Maxdate <> "" Then
                strHead(26) = clsrptpadl.Maxdate
            Else
                strHead(26) = " ".ToString()
            End If

            If clsrptpadl.Minmodel <> "" Then
                strHead(27) = clsrptpadl.Minmodel
            Else
                strHead(27) = "AAA".ToString
            End If
            If clsrptpadl.Maxmodel <> "ZZZZZZZZZZ" Then
                strHead(28) = clsrptpadl.Maxmodel
            Else
                strHead(28) = "ZZZ".ToString()
            End If
            If clsrptpadl.Minarea <> "" Then
                strHead(29) = clsrptpadl.Minarea
            Else
                strHead(29) = "AAA".ToString()
            End If
            If clsrptpadl.Maxarea <> "ZZZZZZZZZZ" Then
                strHead(30) = clsrptpadl.Maxarea
            Else
                strHead(30) = "ZZZ".ToString()
            End If
            If clsrptpadl.Minstat <> "" Then
                strHead(31) = clsrptpadl.Minstat
            Else
                strHead(31) = "AAA".ToString()
            End If
            If clsrptpadl.Maxstat <> "ZZZZZZZZZZ" Then
                strHead(32) = clsrptpadl.Maxstat
            Else
                strHead(32) = "ZZZ".ToString()
            End If




            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


            'With clsReport
            '    .ReportFileName = "RptPADL.rpt"  'TextBox1.Text
            '    .SetReport(CrystalReportViewer1, ds, strHead)


            'End With
            lstrDebugMsg = "Export Pdf report"
            Dim lstrPdfFileName As String = "AddressLabel_" & Session("login_session") & ".pdf"


            With clsReport
                .ReportFileName = "RptPADL.rpt"
                .SetReportDocument(ds, strHead)
                .PdfFileName = lstrPdfFileName
                .ExportPdf()
            End With

            Response.Redirect(clsReport.PdfUrl)

        Catch err As Exception
            'System.IO.File.Delete(lstrPhysicalFile)
            Response.Write("<BR>")
            Response.Write("<BR>")
            Response.Write("Error Message: " & err.Message.ToString)
            Response.Write("<BR>")
            Response.Write("Source : " & err.Source)
            Response.Write("<BR>")
            Response.Write("Stack Trace Message: " & err.StackTrace)
            Response.Write("<BR>")
            Response.Write("Report Message: " & clsReport.GetMessage)

        End Try


    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        clsReport.UnloadReport()
        CrystalReportViewer1.Dispose()
    End Sub
End Class
