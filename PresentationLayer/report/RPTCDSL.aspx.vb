﻿Imports SQLDataAccess
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
Imports BusinessEntity
Imports CrystalDecisions.Web
Imports CrystalDecisions.CrystalReports.Engine
Imports BusinessEntity.clsCommonClass
Partial Class PresentationLayer_Report_RPTCDSL
    Inherits System.Web.UI.Page
    Dim fstrMonth As String = ""
    Dim fstrDefaultStartDate As String = ""
    Dim fstrDefaultEndDate As String = ""
    Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        fstrMonth = Month(System.DateTime.Today)
        fstrDefaultStartDate = "01/" & IIf(Len(fstrMonth) = 1, "0" & fstrMonth, fstrMonth) & "/" & Year(System.DateTime.Today)
        fstrDefaultEndDate = DateAdd(DateInterval.Month, 1, CDate(fstrDefaultStartDate))

        If Not Me.IsPostBack Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
                
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try


            '''access control'''''''''''''''''''''''''''''''''''''''''''''''
            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "37")
            Me.saveButton.Enabled = purviewArray(3)
            '''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim objXmlTr As New clsXml



            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            Me.Label10.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Me.Label7.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Me.Label12.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Me.Label14.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Me.Label17.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")

            Me.Label21.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Me.Label22.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")


            Me.Label11.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Me.Label13.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Me.Label15.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Me.Label16.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Me.Label18.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")

            'Me.Label23.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Me.Label24.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")

            Me.Label1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDSL-0010")
            Me.Label2.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDSL-0015")
            Me.Label3.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDSL-0011")
            Me.Label4.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDSL-0012")
            Me.Label5.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDSL-0013")
            Me.Label9.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CNT-CNTSDATE")


            Me.Label21.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDSL-0016")
            Me.titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDSL-0019")

            Dim cntryStat As New clsrlconfirminf()  'contract tp
            Dim contractpar As ArrayList = New ArrayList
            Dim statid As String
            Dim statnm As String
            Dim count As Integer



            'contractpar = cntryStat.searchconfirminf("YESNO")
            'count = 0
            'For count = 0 To contractpar.Count - 1
            '    statid = contractpar.Item(count)
            '    statnm = contractpar.Item(count + 1)
            '    count = count + 1
            '    Me.DropDownList1.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
            'Next
            'Me.DropDownList1.Items.Add(New ListItem("All", "%"))

            Dim objContractType As New clsSerType
            objContractType.CountryID = Session("login_ctryID")

            Dim dsContractType As DataSet = objContractType.GetContractTypeID
            'Me.DropDownList1.Items.Add(New ListItem("All", "%"))
            databonds(dsContractType.Tables(0), DropDownList1)


            Me.maxroserial.Text = "ZZZ"
            Me.maxcustmbox.Text = "ZZZ"
            Me.maxmodelbox.Text = "ZZZ"
            Me.maxstatebox.Text = "ZZZ"
            Me.maxsvsbox.Text = "ZZZ"

            Me.minroserial.Text = "AAA"
            Me.minsvsbox.Text = "AAA"
            Me.minstatebox.Text = "AAA"
            Me.minmodelbox.Text = "AAA"
            Me.mincustmbox.Text = "AAA"

            'LW Added
            Me.minareabox.Text = "AAA"
            Me.maxareabox.Text = "ZZZ"

            'Dim lstrMonth As String = System.DateTime.Today.Month
            'If Len(lstrMonth) = 1 Then
            '    lstrMonth = "0" & lstrMonth
            'End If

            'Me.mindatebox.Text = "01/" & lstrMonth & "/" & System.DateTime.Today.Year
            'Me.maxdatebox.Text = DateAdd(DateInterval.Day, -DateAdd(DateInterval.Month, 1, System.DateTime.Today).Day, DateAdd(DateInterval.Month, 1, System.DateTime.Today))
            Me.mindatebox.Text = fstrDefaultStartDate
            Me.maxdatebox.Text = fstrDefaultEndDate

            Me.saveButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-VIEWREPORT")
 
            'PrintReport()
        End If

        HypCalFrom.NavigateUrl = "javascript:DoCal(document.form1.mindatebox);"
        HypCalTo.NavigateUrl = "javascript:DoCal(document.form1.maxdatebox);"


    End Sub

    Public Sub databonds(ByRef dt As DataTable, ByRef dropdown As DropDownList)
        Dim count As Integer = 0
        dropdown.Items.Clear()
        dropdown.Items.Add(New ListItem("All", "%"))
        Dim row As DataRow
        For Each row In dt.Rows
            Dim NewItem As New ListItem()
            NewItem.Text = row("id") & "-" & row("name")
            NewItem.Value = row("id")
            dropdown.Items.Add(NewItem)
            count = count + 1
        Next
        'If dt.Rows.Count = 0 Then '处理数据表为空的情况
        '    dropdown.Items.Insert(0, New ListItem("", ""))
        'End If
    End Sub

    Public Function PrintReport()
        Dim rptcdsl As New clsRPTCDSL
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        mindatebox.Text = Request.Form("mindatebox")
        maxdatebox.Text = Request.Form("maxdatebox")

        If Not IsDate(mindatebox.Text) Then
            mindatebox.Text = fstrDefaultStartDate
        End If

        If Not IsDate(maxdatebox.Text) Then
            mindatebox.Text = fstrDefaultEndDate
        End If


        If (Me.mincustmbox.Text <> "AAA") Then
            rptcdsl.minCustmID = Me.mincustmbox.Text.ToUpper
        Else
            rptcdsl.minCustmID = ""
        End If
        If (Me.maxcustmbox.Text <> "ZZZ") Then
            rptcdsl.maxCustmID = Me.maxcustmbox.Text.ToUpper
        Else
            rptcdsl.maxCustmID = "ZZZZZZZZZZZZZZZZZZZZ"
        End If
        If (Me.minsvsbox.Text <> "AAA") Then
            rptcdsl.minSvcID = Me.minsvsbox.Text.ToUpper
        Else
            rptcdsl.minSvcID = ""
        End If
        If (Me.maxsvsbox.Text <> "ZZZ") Then
            rptcdsl.maxSvcID = Me.maxsvsbox.Text.ToUpper
        Else
            rptcdsl.maxSvcID = "ZZZZZZZZZZZZZZZZZZZZ"
        End If
        If (Me.minmodelbox.Text <> "AAA") Then
            rptcdsl.minPrdctMd = Me.minmodelbox.Text.ToUpper
        Else
            rptcdsl.minPrdctMd = ""
        End If
        If (Me.maxmodelbox.Text <> "ZZZ") Then
            rptcdsl.maxPrdctMd = Me.maxmodelbox.Text.ToUpper
        Else
            rptcdsl.maxPrdctMd = "ZZZZZZZZZZZZZZZZZZZZ"
        End If
        If (Me.minroserial.Text <> "AAA") Then
            rptcdsl.minRONo = Me.minroserial.Text.ToUpper
        Else
            rptcdsl.minRONo = ""
        End If
        If (Me.maxroserial.Text <> "ZZZ") Then
            rptcdsl.maxRONo = Me.maxroserial.Text.ToUpper
        Else
            rptcdsl.maxRONo = "ZZZZZZZZZZZZZZZZZZZZ"
        End If
        If (Me.minstatebox.Text <> "AAA") Then
            rptcdsl.minState = Me.minstatebox.Text.ToUpper
        Else
            rptcdsl.minState = ""
        End If
        If (Me.maxstatebox.Text <> "ZZZ") Then
            rptcdsl.maxState = Me.maxstatebox.Text.ToUpper
        Else
            rptcdsl.maxState = "ZZZZZZZZZZZZZZZZZZZZ"
        End If

        'LW Added
        If (Me.minareabox.Text <> "AAA") Then
            rptcdsl.minArea1 = Me.minareabox.Text.ToUpper
        Else
            rptcdsl.minArea1 = ""

        End If
        If (Me.maxareabox.Text <> "ZZZ") Then
            rptcdsl.maxArea1 = Me.maxareabox.Text.ToUpper
        Else
            rptcdsl.maxArea1 = "ZZZZZZZZZZZZZZZZZZZZ"
        End If

        If (Me.mindatebox.Text <> "") Then
            Dim datearr As Array = mindatebox.Text.ToString().Split("/")
            Dim rtdate As String = datearr(2) + "-" + datearr(1) + "-" + datearr(0)

            rptcdsl.mindate = rtdate
        Else
            rptcdsl.mindate = fstrDefaultStartDate
        End If
        If (Me.maxdatebox.Text <> "") Then
            Dim datearr As Array = maxdatebox.Text.ToString().Split("/")
            Dim rtdate As String = datearr(2) + "-" + datearr(1) + "-" + datearr(0)
            rptcdsl.maxdate = rtdate
        Else
            rptcdsl.maxdate = fstrDefaultEndDate
        End If
        rptcdsl.ContractTy = Me.DropDownList1.SelectedValue

        Dim ctryIDPara As String = "%"
        Dim cmpIDPara As String = "%"
        Dim svcIDPara As String = "%"
        Dim login_rank As Integer = Session("login_rank")
        If login_rank <> 0 Then
            Select Case (login_rank)
                Case 9
                    ctryIDPara = Session("login_ctryID").ToString.ToUpper
                    cmpIDPara = Session("login_cmpID").ToString.ToUpper
                    svcIDPara = Session("login_svcID").ToString.ToUpper
                Case 8
                    ctryIDPara = Session("login_ctryID").ToString.ToUpper
                    cmpIDPara = Session("login_cmpID").ToString.ToUpper
                Case 7
                    ctryIDPara = Session("login_ctryID").ToString.ToUpper

            End Select
        Else


        End If
        ''''''''''''''''''''''''''''''''''''''''''''''''''
        rptcdsl.CountryID = ctryIDPara
        rptcdsl.CompID = cmpIDPara
        rptcdsl.SVCID = svcIDPara
        '''''''''''''''''''''''''''''''''''''''''''''''''''





        rptcdsl.ContractTy = Me.DropDownList1.SelectedValue  'contract type  value
        Dim str As String = rptcdsl.ContractTy

        'Session("rpt_cdsl_ds") = rptds
        Session("rpt_cdsl_searchcondition") = rptcdsl


    End Function

    Protected Sub saveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveButton.Click 
        PrintReport()
        Dim script As String = "window.open('../report/RPTCDSLVIEW.aspx')"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "RPTCDSLVIEW", script, True)

        Return
end sub

  
End Class
