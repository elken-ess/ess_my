Imports System.Data
Imports BusinessEntity
Imports System.Windows
Imports Microsoft.Win32

Partial Class PresentationLayer_report_RPTWarrParts_VIEW1
    Inherits System.Web.UI.Page

    Private ReadOnly _datestyle As New Globalization.CultureInfo("en-CA")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Threading.Thread.CurrentThread.CurrentCulture = _datestyle

        If (Session("SvcFr") Is "") Then
            SvcFr.Text = "AAA"
        Else
            SvcFr.Text = Session("SvcFr").ToString
        End If

        If (Session("SvcTo") Is "") Then
            SvcTo.Text = "ZZZ"
        Else
            SvcTo.Text = Session("SvcTo").ToString
        End If

        If (Session("SvcDateFr") Is "") Then
            SvcDateFr.Text = "AAA"
        Else
            SvcDateFr.Text = Session("SvcDateFr").ToString
        End If

        If (Session("SvcDateTo") Is "") Then
            SvcDateTo.Text = "ZZZ"
        Else
            SvcDateTo.Text = Session("SvcDateTo").ToString
        End If

        If (Session("TechFr") Is "") Then
            TechFr.Text = "AAA"
        Else
            TechFr.Text = Session("TechFr").ToString
        End If

        If (Session("TechTo") Is "") Then
            TechTo.Text = "ZZZ"
        Else
            TechTo.Text = Session("TechTo").ToString
        End If

        If (Session("PriceFr") Is "") Then
            PriceFr.Text = "AAA"
        Else
            PriceFr.Text = Session("PriceFr").ToString
        End If

        If (Session("PriceTo") Is "") Then
            PriceTo.Text = "ZZZ"
        Else
            PriceTo.Text = Session("PriceTo").ToString
        End If

        If Session("DataTable").ToString = "X" Then
            Label1.Visible = True
        Else
            GridView1.DataSource = (Session("DataTable"))
            GridView1.DataBind()

            GridView1.Visible = True
        End If

    End Sub
    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub

    Protected Sub Export_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Export.Click
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=GridView1.xls")
        Response.Charset = ""
        Response.ContentType = "application/vnd.xls"
        Dim StringWriter As IO.StringWriter = New System.IO.StringWriter()
        Dim HtmlTextWriter As New HtmlTextWriter(StringWriter)
        Dim style As String = "<style> td { mso-number-format:\@; } </style> "

        GridView1.RenderControl(HtmlTextWriter)
        Response.Write(style) 'style is added dynamically
        Response.Write(StringWriter.ToString())
        Response.[End]()
    End Sub
End Class
