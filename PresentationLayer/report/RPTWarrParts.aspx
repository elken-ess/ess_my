﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="RPTWarrParts.aspx.vb" Inherits="PresentationLayer.report.PresentationLayer_report_RPTWarrParts" %>

<%@ Register Assembly="JCalendar" Namespace="JCalendar" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Warranty Parts Report</title>
    <link href="../css/style.css" type="text/css" rel="stylesheet">
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="JavaScript" src="../js/common.js"></script>
    <style type="text/css">
    	.rowStyle {
    		background-color: #ffffff;
    	}
    	.labelStyle {
    		text-align: left;
    		width: 26%
    	}
    	.fromToStyle {
    		text-align: left;
    		width: 7%;
    	}
    	.dataStyle {
    		text-align: left;
    		width: 30%
    	}
    	.countryStyle {
    		border: 0px;
    		width: 100%
    	}
    	.tableStyle {
    		border: 0px;
    		padding: 0px;
    		margin: 0px;
    		width: 100%;
    	}
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table id="countrytab" class="countryStyle">
            <tr>
                <td style="width: 100%">
                    <table class="tableStyle">
                        <tr>
                            <td background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title1.gif" width="5" />
                            </td>
                            <td background="../graph/title_bg.gif" class="style2" width="98%" style="width: 100%">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label>
                            </td>
                            <td align="LEFT" background="../graph/title_bg.gif" width="1%">
                                <img height="1" src="../graph/title_2.gif" width="5" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100%">
                    <table class="tableStyle">
                        <tr>
                            <td background="../graph/title_bg.gif" class="style2" width="98%">
                                <font color="red" style="width: 100%">
                                    <asp:Label ID="errlab" runat="server" Text="Label" Visible="false"></asp:Label></font>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100%; height: 1px;">
                    <table id="country" bgcolor="#b7e6e6" border="0" cellpadding="2" cellspacing="1"
                        style="width: 100%">
                        <tr class="rowStyle">
                            <td class="labelStyle">
                                <asp:Label ID="ServiceCenterLabel" runat="server" Text=""/>
                            </td>
                            <td class="fromToStyle">
                                <asp:Label ID="FromSvcCenterLabel" runat="server" Text="From"/>
                            </td>
                            <td class="dataStyle">
                                <asp:DropDownList ID="FromSvcCenterDropdown" 
                                                  runat="server"
                                                  style="width:300px;"
                                                  CssClass="textborder" AutoPostBack="True"/>
                            </td>
                            <td class="fromToStyle">
                                <asp:Label ID="ToSvcCenterLabel" runat="server" Text="To"/>
                            </td>
                            <td class="dataStyle">
                                <asp:DropDownList ID="ToSvcCenterDropdown" 
                                                  runat="server"
                                                  style="width:300px;"
                                                  CssClass="textborder" AutoPostBack="True"/>
                            </td>
                        </tr>
                        <tr class="rowStyle">
                            <td class="labelStyle">
                                <asp:Label ID="ServiceDateLabel" runat="server"/>
                            </td>
                            <td class="fromToStyle">
                                <asp:Label ID="FromSvcDateLabel" runat="server" Text="From"/>
                            </td>
                            <td class="dataStyle">
                                <asp:TextBox ID="FromSvcDateTextBox" 
                                             runat="server" 
                                             CssClass="textborder" 
                                             ReadOnly="false"
                                             MaxLength="10"/>
                                <asp:HyperLink ID="HypCalFrom" 
                                               runat="server" 
                                               ImageUrl="~/PresentationLayer/graph/calendar.gif"
                                               ToolTip="Choose a Date">Choose a Date</asp:HyperLink>
                                <cc1:JCalendar ID="JCalendar1" 
                                               runat="server" 
                                               ControlToAssign="FromSvcDateTextBox" 
                                               ImgURL="~/PresentationLayer/graph/calendar.gif"
                                               Visible="false" />
                            </td>
                            <td class="fromToStyle">
                                <asp:Label ID="ToSvcDateLabel" 
                                           runat="server" 
                                           Text="To"/>
                            </td>
                            <td class="dataStyle">
                                <asp:TextBox ID="ToSvcDateTextBox" 
                                             runat="server" 
                                             CssClass="textborder" 
                                             ReadOnly="false"
                                             MaxLength="10"/>
                                <asp:HyperLink ID="HypCalTo" 
                                               runat="server" 
                                               ImageUrl="~/PresentationLayer/graph/calendar.gif"
                                               ToolTip="Choose a Date">Choose a Date</asp:HyperLink>
                                <cc1:JCalendar ID="JCalendar2" 
                                               runat="server" 
                                               ControlToAssign="ToSvcDateTextBox" 
                                               ImgURL="~/PresentationLayer/graph/calendar.gif"
                                               Visible="false" />
                            </td>
                        </tr>
                        <tr class="rowStyle">
                            <td class="labelStyle">
                                <asp:Label runat="server"
                                           ID="TechnicianLabel"/>
                            </td>
                            <td class="fromToStyle">
                                <asp:Label ID="FromTechLabel" 
                                           runat="server" 
                                           Text="From"/>
                            </td>
                            <td class="dataStyle">
                                <asp:DropDownList ID="FromTechDropDown" 
                                                  runat="server"
                                                  CssClass="textborder"
                                                  Width="260px"/>
                            </td>
                            <td class="fromToStyle">
                                <asp:Label ID="ToTechLabel" 
                                           runat="server" 
                                           Text="To"/>
                            </td>
                            <td class="dataStyle">
                                <asp:DropDownList ID="ToTechDropDown" 
                                                  runat="server"
                                                  Width="260px"
                                                  CssClass="textborder"/>
                            </td>
                        </tr>
                        <tr class="rowStyle">
                            <td class="labelStyle">
                                <asp:Label runat="server" 
                                           ID="PriceIDLabel"/>
                            </td>
                            <td class="fromToStyle">
                                <asp:Label ID="FromPriceIDLabel" 
                                           runat="server" 
                                           Text="From"/>
                            </td>
                            <td class="dataStyle">
                                <asp:DropDownList runat="server"
                                                  ID="FromPriceIDDropDown"
                                                  Width="260px"
                                                  CssClass="textborder"/>
                            </td>
                            <td class="fromToStyle">
                                <asp:Label ID="ToPriceIDLabel" 
                                           runat="server" 
                                           Text="To"/>
                            </td>
                            <td class="dataStyle">
                                <asp:DropDownList runat="server"
                                                  ID="ToPriceIDDropDown"
                                                  Width="260px"
                                                  CssClass="textborder"/>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100%;">
                    <asp:LinkButton ID="saveButton" 
                                    runat="server" 
                                    Text="ViewReport"
                                    OnClick="saveButton_Click"/>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
