﻿Imports BusinessEntity
Imports System.Configuration
Imports System.Xml
Imports System.Data
Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web
Partial Class PresentationLayer_report_salesByProduct_sub
    Inherits System.Web.UI.Page
    Dim clsReport As New ClsCommonReport

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try

        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        Dim sbpEntity As New clsRPTSBP()
        sbpEntity = Session("rptSBPD_search_condition")
        Dim sbpds As DataSet = sbpEntity.GetSalesByProduct()





        Dim objXmlTr As New clsXml
        Dim strHead As Array = Array.CreateInstance(GetType(String), 55)
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        strHead(0) = objXmlTr.GetLabelName("EngLabelMsg", "BB-SBP-0002")
        strHead(1) = objXmlTr.GetLabelName("EngLabelMsg", "BB-SBP-0013")
        strHead(2) = objXmlTr.GetLabelName("EngLabelMsg", "BB-SBP-0003")
        strHead(3) = objXmlTr.GetLabelName("EngLabelMsg", "BB-SBP-0014")
        strHead(4) = objXmlTr.GetLabelName("EngLabelMsg", "BB-SBP-0015")
        strHead(5) = objXmlTr.GetLabelName("EngLabelMsg", "BB-SBP-0016")
        strHead(6) = objXmlTr.GetLabelName("EngLabelMsg", "BB-SBP-0017")
        strHead(7) = objXmlTr.GetLabelName("EngLabelMsg", "BB-SBP-0001")
        strHead(8) = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0017")
        strHead(9) = objXmlTr.GetLabelName("EngLabelMsg", "BB-SBP-0018")
        strHead(10) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_APT_RECORD")
        strHead(11) = sbpds.Tables(0).Rows.Count
        strHead(12) = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0021")
        strHead(13) = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0018")
        strHead(14) = objXmlTr.GetLabelName("EngLabelMsg", "BB-SBP-0019")
        strHead(15) = objXmlTr.GetLabelName("EngLabelMsg", "BB-SBP-0020")
        strHead(16) = Convert.ToString(sbpEntity.StartDate) + "     " + "To" + "   " + Convert.ToString(sbpEntity.EndDate)
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        strHead(17) = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
        strHead(18) = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
        Me.titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-SBP-0001")

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        strHead(19) = objXmlTr.GetLabelName("EngLabelMsg", "BB-SBP-0003")
        strHead(20) = objXmlTr.GetLabelName("EngLabelMsg", "BB-SBP-0005")
        strHead(21) = objXmlTr.GetLabelName("EngLabelMsg", "BB-SBP-0021")
        strHead(22) = objXmlTr.GetLabelName("EngLabelMsg", "BB-SBP-0006")
        strHead(23) = objXmlTr.GetLabelName("EngLabelMsg", "BB-SBP-0007")
        strHead(24) = objXmlTr.GetLabelName("EngLabelMsg", "BB-SBP-0002")
        strHead(25) = objXmlTr.GetLabelName("EngLabelMsg", "BB-SBP-0004")
        strHead(26) = objXmlTr.GetLabelName("EngLabelMsg", "BB-SBP-0008")
        strHead(27) = objXmlTr.GetLabelName("EngLabelMsg", "BB-SBP-0010")
        strHead(44) = Session("userID") + "/" + Session("username")
        strHead(45) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCUSR-USERID")
        strHead(46) = objXmlTr.GetLabelName("EngLabelMsg", "BB-SBP-0009")
        strHead(47) = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0018")
        strHead(48) = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0018")
        strHead(49) = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0018")


        If (sbpEntity.PriceFrom = Nothing) Then
            strHead(28) = "AAA"
        Else

            strHead(28) = sbpEntity.PriceFrom
        End If

        If (sbpEntity.PriceTo = "ZZZZZZZZZZ") Then
            strHead(29) = "ZZZ"
        Else

            strHead(29) = sbpEntity.PriceTo
        End If


        If (sbpEntity.StateFrom = Nothing) Then
            strHead(30) = "AAA"
        Else

            strHead(30) = sbpEntity.StateFrom
        End If



        If (sbpEntity.StateTo = "ZZZZZZZZZZ") Then
            strHead(31) = "ZZZ"
        Else

            strHead(31) = sbpEntity.StateTo
        End If


        If (sbpEntity.StartDate = Nothing) Then
            strHead(32) = " "
        Else

            strHead(32) = sbpEntity.StartDate
        End If

        If (sbpEntity.EndDate = Nothing) Then
            strHead(33) = " "
        Else

            strHead(33) = sbpEntity.EndDate
        End If


        If (sbpEntity.SvcIDFrom = Nothing) Then
            strHead(34) = "AAA"
        Else

            strHead(34) = sbpEntity.SvcIDFrom
        End If


        If (sbpEntity.SvcIDTo = "ZZZZZZZZZZ") Then
            strHead(35) = "ZZZ"
        Else

            strHead(35) = sbpEntity.SvcIDTo
        End If


        If (sbpEntity.CategoryFrom = Nothing) Then
            strHead(36) = "AAA"
        Else

            strHead(36) = sbpEntity.CategoryFrom
        End If

        If (sbpEntity.CategoryTo = "ZZZZZZZZZZ") Then
            strHead(37) = "ZZZ"
        Else

            strHead(37) = sbpEntity.CategoryTo
        End If

        If (sbpEntity.PartCodeFrom = Nothing) Then
            strHead(38) = "AAA"
        Else

            strHead(38) = sbpEntity.PartCodeFrom
        End If

        If (sbpEntity.PartCodeTo = "ZZZZZZZZZZ") Then
            strHead(39) = "ZZZ"
        Else

            strHead(39) = sbpEntity.PartCodeTo
        End If


        If (sbpEntity.TechnicianFrom = Nothing) Then
            strHead(40) = "AAA"
        Else

            strHead(40) = sbpEntity.TechnicianFrom
        End If


        If (sbpEntity.TechnicianTo = "ZZZZZZZZZZ") Then
            strHead(41) = "ZZZ"
        Else

            strHead(41) = sbpEntity.TechnicianTo
        End If

        If (sbpEntity.Groupbypice1 = Nothing) Then
            strHead(42) = " "
        Else

            strHead(42) = sbpEntity.Groupbypice1
        End If

        If (sbpEntity.IncludeSeritem1 = Nothing) Then
            strHead(43) = " "
        Else

            strHead(43) = sbpEntity.IncludeSeritem1
        End If


        strHead(50) = objXmlTr.GetLabelName("EngLabelMsg", "SBA-SERVICEBILLTYPE")
        strHead(51) = sbpEntity.ServiceBillTypeText

        With clsReport
            .ReportFileName = "SalesByProduct.rpt"  'TextBox1.Text
            .SetReport(CrystalReportViewer1, sbpds, strHead)

        End With

    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        clsReport.UnloadReport()
        CrystalReportViewer1.Dispose()
    End Sub
End Class
