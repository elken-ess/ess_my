﻿Imports BusinessEntity
Imports System.Configuration
Imports System.Xml
Imports System.Data
Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web

Partial Class PresentationLayer_Report_ReaptedApp
    Inherits System.Web.UI.Page
    Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
    Dim fstrMonth As String = ""
    Dim fstrDefaultStartDate As String = ""
    Dim fstrDefaultEndDate As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        fstrMonth = Month(System.DateTime.Today)
        fstrDefaultStartDate = "01/01/" & Year(System.DateTime.Today)
        fstrDefaultEndDate = "31/12/" & Year(System.DateTime.Today)

        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If

        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try

        If Not Page.IsPostBack Then

           
            
            Session("rptflag") = "0"

            Me.from1.Text = "From"
            Me.from2.Text = "From"
            Me.from3.Text = "From"
            Me.from4.Text = "From"
            Me.from5.Text = "From"
            Me.to1.Text = "To"
            Me.to2.Text = "To"
            Me.to3.Text = "To"
            Me.to4.Text = "To"
            Me.to5.Text = "To"
            Me.serlab.Text = "Service Center"
            Me.serilab.Text = "Serial No"
            Me.sertylab.Text = "Service Type"
            Me.datelab.Text = "Date"
            Me.cuslab.Text = "Customer ID"
            Me.modelab.Text = "Model ID"
            Me.repelab.Text = "Repeat HZ"
            Me.LinkButton1.Text = "ViewReport"

            Me.titleLab.Text = "Repeated Appointment/Services"

            Me.repnumerr.ErrorMessage = "Please enter a valid Repeat Hz"

            Me.date1.Text = fstrDefaultStartDate
            Me.date2.Text = fstrDefaultEndDate

            '''access control'''''''''''''''''''''''''''''''''''''''''''''''
            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "46")
            Me.LinkButton1.Enabled = purviewArray(3)

            Me.repeat.Text = 2

            Dim Appbond As New clsCommonClass
            Dim sertype As New DataSet
            Dim rank As Integer = Session("login_rank")
            Appbond.rank = rank
            If rank <> 0 Then
                Appbond.spctr = Session("login_ctryID")
                Appbond.spstat = Session("login_cmpID")
                Appbond.sparea = Session("login_svcID")
            End If
            sertype = Appbond.Getcomidname("BB_MASSRCT_IDNAME")
            databonds(sertype, servity)
            Me.servity.Items.Insert(0, "")

        ElseIf Session("rptflag") = "1" Then
            print_report()
        End If

        HypCalFrom.NavigateUrl = "javascript:DoCal(document.form1.date1);"
        HypCalTo.NavigateUrl = "javascript:DoCal(document.form1.date2);"
    End Sub
#Region " bonds"
    Public Function databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        Dim row As DataRow
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row In ds.Tables(0).Rows
                Dim NewItem As New ListItem()
                NewItem.Text = row("id") & "-" & row("name")
                NewItem.Value = row("id")
                dropdown.Items.Add(NewItem)
            Next
        End If
    End Function
#End Region

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton1.Click
        Session("rptflag") = "1"
        print_report()

        Dim script As String = "window.open('../report/RPTRAPT_SUB.aspx')"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "SystemAccessReport", script, True)
    End Sub

    Public Function print_report()
        Dim _rptsvc As New ClsRPTRAPT
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        date1.Text = Request.Form("date1")
        date2.Text = Request.Form("date2")

        If Not IsDate(date1.Text) Then
            date1.Text = fstrDefaultStartDate
        End If

        If Not IsDate(date2.Text) Then
            date2.Text = fstrDefaultEndDate
        End If

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        If (Me.sercen1.Text.Trim() <> "AAA") Then
            _rptsvc.ServiceID1 = Me.sercen1.Text.ToUpper()
        Else
            _rptsvc.ServiceID1 = String.Empty
        End If

        If (Trim(Me.sercen2.Text) <> "ZZZ") Then
            _rptsvc.ServiceID2 = sercen2.Text.ToUpper()
        Else
            _rptsvc.ServiceID2 = "ZZZ"
        End If

        If (Me.custom1.Text.Trim() <> "AAA") Then
            _rptsvc.CustomerID1 = Me.custom1.Text.ToUpper()
        Else
            _rptsvc.CustomerID1 = String.Empty
        End If

        If (Trim(Me.custom2.Text) <> "ZZZ") Then
            _rptsvc.CustomerID2 = custom2.Text.ToUpper()
        Else
            _rptsvc.CustomerID2 = "ZZZ"
        End If

        If (Me.seri1.Text.Trim() <> "AAA") Then
            _rptsvc.SerialNo1 = Me.seri1.Text.ToUpper()
        Else
            _rptsvc.SerialNo1 = String.Empty
        End If

        If (Trim(Me.seri2.Text) <> "ZZZ") Then
            _rptsvc.SerialNo2 = seri2.Text.ToUpper()
        Else
            _rptsvc.SerialNo2 = "ZZZ"
        End If

        If (Me.modelid1.Text.Trim() <> "AAA") Then
            _rptsvc.Model1 = Me.modelid1.Text.ToUpper()
        Else
            _rptsvc.Model1 = String.Empty
        End If

        If (Trim(Me.modelid2.Text) <> "ZZZ") Then
            _rptsvc.Model2 = modelid2.Text.ToUpper()
        Else
            _rptsvc.Model2 = "ZZZ"
        End If

        If (servity.Text.ToString() <> "") Then
            _rptsvc.SerType = Me.servity.SelectedItem.Value.ToString()
        Else
            _rptsvc.sertype_sub = String.Empty
        End If

        If (servity.Text.ToString() <> "") Then
            Dim type As Array = Me.servity.SelectedItem.Text.Split("-")
            _rptsvc.sertype_sub = type(0).ToString.Trim() + "-" + type(1).ToString.Trim()
        End If

        _rptsvc.Datetime1 = Me.date1.Text
        _rptsvc.Datetime2 = Me.date2.Text

        If Me.repeat.Text = "" Then
            Me.repeat.Text = 2
        End If

        _rptsvc.Reapted = Me.Reaptedp.SelectedItem.Text
        If (Trim(Me.repeat.Text) <> "") Then
            _rptsvc.Reaptednum = Me.repeat.Text
        Else
            _rptsvc.Reaptednum = "w"
        End If
        Dim rank As String = Session("login_rank")
        If rank <> 0 Then
            _rptsvc.ctrid = Session("login_ctryID").ToString().ToUpper
            _rptsvc.comid = Session("login_cmpID").ToString().ToUpper
            _rptsvc.svcid = Session("login_svcID").ToString().ToUpper
        End If
        _rptsvc.rank = rank

        'Session("rptRAPT_search_condition") = _rptsvc

        Dim dt As New DataTable
        dt = _rptsvc.GetRepeatedSer().Tables(0)

        If dt.Rows.Count > 0 Then
            Session("DataTable") = dt
        Else
            Session("DataTable") = "X"
        End If

        Session("SvcFr") = _rptsvc.ServiceID1
        Session("SvcTo") = _rptsvc.ServiceID2
        Session("DateFr") = _rptsvc.Datetime1
        Session("DateTo") = _rptsvc.Datetime2
        Session("CustFr") = _rptsvc.CustomerID1
        Session("CustTo") = _rptsvc.CustomerID2
        Session("SerialFr") = _rptsvc.SerialNo1
        Session("SerialTo") = _rptsvc.SerialNo2
        Session("ModelFr") = _rptsvc.Model1
        Session("ModelTo") = _rptsvc.Model2
        Session("Repeat") = _rptsvc.Reaptednum
        Session("ServiceType") = _rptsvc.sertype_sub
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


    End Function

    Private Function ReportPath() As String
        ReportPath = ConfigurationSettings.AppSettings("ReportPath")
    End Function
End Class
