<%@ Page Language="VB" AutoEventWireup="false" CodeFile="RPTProductFeedbackSummary.aspx.vb" Inherits="PresentationLayer_report_RPTProductFeedbackSummary" %>

<%@ Register Assembly="JCalendar" Namespace="JCalendar" TagPrefix="cc1" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Product Feedback Summary Report</title>
    <link href="../css/style.css" type="text/css" rel="stylesheet" />
    <script type="text/javascript" language="JavaScript" src="../js/common.js"></script>
    <style type="text/css">
    	.rowStyle {
    		background-color: #ffffff;
    	}
    	.labelStyle {
    		text-align: left;
    		width: 15%
    	}
    	.fromToStyle {
    		text-align: left;
    		width: 6%;
    	}
    	.dataStyle {
    		text-align: left;
    		width: 27%
    	}
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <table id="PRL" border="0" style="width: 100%">
            <tr>
                <td style="width: 100%">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title1.gif" width="5" /></td>
                            <td background="../graph/title_bg.gif" class="style2" width="98%">
                                <asp:Label ID="titleLab" runat="server" Text="Product Feedback Summary Report"></asp:Label></td>
                            <td align="right" background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title_2.gif" width="5" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" class="style2" width="98%">
                                <font color="red">
                                    <asp:Label ID="errlab" runat="server" Text="Label" Visible="false"></asp:Label></font></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100%;">
                    <table id="crn" bgcolor="#b7e6e6" border="0" cellpadding="2" cellspacing="1" style="width: 100%">
                        <tr bgcolor="#ffffff">
                            <td class="labelStyle">
                                <asp:Label ID="lblServCtr" runat="server" Text="Service Center" /></td>
                            <td class="fromToStyle">
                                <asp:Label ID="lblServCtrFr" runat="server" Text="From" /></td>
                            <td class="dataStyle">
                                <asp:DropDownList runat="server" ID="ddSVCCenter" Width="41%" CssClass="textborder" 
                                    AutoPostBack="True">
                                </asp:DropDownList></td>
                            <td class="fromToStyle">
                                <asp:Label ID="lblServCtrTo" runat="server" Text="To" /></td>
                            <td class="dataStyle">
                                <asp:DropDownList ID="ddSVCCenterTo" runat="server" AutoPostBack="True" CssClass="textborder"
                                    Width="41%">
                                </asp:DropDownList></td>
                        </tr>                    
                        <tr bgcolor="#ffffff" class="rowStyle">
                            <td class="labelStyle">
                                <asp:Label ID="lblApptDate" runat="server" Text="Appointment Date"></asp:Label></td>
                            <td class="fromToStyle">
                                <asp:Label ID="lblApptDateFr" runat="server" Text="From" /></td>
                            <td class="dataStyle">
                                <asp:TextBox ID="txtApptDateFr" runat="server" CssClass="textborder" ReadOnly="False"/>
                                <asp:HyperLink ID="HypCalFr" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                                    ToolTip="Choose a Date" Visible="False">Choose a Date</asp:HyperLink>
                                <cc1:jcalendar id="calApptDateFr" runat="server" controltoassign="txtApptDateFr"
                                    imgurl="~/PresentationLayer/graph/calendar.gif"/>
                            </td>
                            <td class="fromToStyle">
                                <asp:Label ID="lblApptDateTo" runat="server" Text="To" /></td>
                            <td class="dataStyle">
                                <asp:TextBox ID="txtApptDateTo" runat="server" CssClass="textborder" ReadOnly="False"/>&nbsp;
                                <asp:HyperLink ID="HypCalTo" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                                    ToolTip="Choose a Date" Visible="False">Choose a Date</asp:HyperLink>
                                <cc1:jcalendar id="calApptDateTo" runat="server" controltoassign="txtApptDateTo"
                                    imgurl="~/PresentationLayer/graph/calendar.gif"/>
                                &nbsp;&nbsp;
                            </td>
                        </tr>
                       </tr>
                        <tr bgcolor="#ffffff">
                            <td align="LEFT" style="width: 24%; height: 28px">
                                <asp:Label ID="lblStatus" runat="server" Text="Status"></asp:Label>
                            </td>
                            <td align="LEFT" style="width: 45px; height: 28px">
                            </td>
                            <td align="left" style="width: 34%; height: 28px">
                                <asp:DropDownList ID="cboStatus" Width="80%" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td align="LEFT" style="width: 9%; height: 28px">
                            </td>
                            <td align="left" style="width: 40%; height: 28px">
                            </td>
                        </tr>               
                        <tr bgcolor="#ffffff">
                           <td class="labelStyle">
                               <asp:Label ID="lblPriceId2" runat="server" Text="Price ID"></asp:Label></td>
                           <td class="fromToStyle" >
                               </td>
                            <td class="dataStyle">
                               <table width="100%" height="200" align="left" >
                                    <tr><td><asp:Label ID="Label4" runat="server" Text="Available Price List"></asp:Label></td></tr>
                                    <tr><td><asp:ListBox ID="lbPriceId" runat="server" SelectionMode="Multiple" Height="160px" Width="60%"></asp:ListBox></td></tr>
                                    <tr><td><asp:Label ID="Label1" runat="server" Text="*NOTE:Shift+Click to select multiple items." Font-Size="X-Small"></asp:Label></td></tr>
                               </table>
                            </td>
                               
                             <td class="fromToStyle">
                                <table width="100%" height="200" align="left" >
                                    <tr><td align="center" valign="bottom"><asp:Button ID="btnToLeft" runat="server" Text=">>" /></td></tr>
                                    <tr><td align="center" valign="top"><asp:Button ID="btnToRight" runat="server" Text="<<" /></td></tr>
                                </table>
                              </td>   
                              <td class="dataStyle">
                                 <table width="100%" height="200" align="left" >
                                    <tr><td><asp:Label ID="Label3" runat="server" Text="Selected Price List"></asp:Label></td></tr>
                                    <tr><td><asp:ListBox ID="lbSelectedPriceId" runat="server" SelectionMode="Multiple" Height="160px" Width="60%"></asp:ListBox></td></tr>
                                    <tr><td><asp:Label ID="Label2" runat="server" Text=""></asp:Label></td></tr>
                               </table>
                              </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                           <td class="labelStyle">
                               <asp:Label ID="lblModelId" runat="server" Text="Model ID"></asp:Label></td>
                           <td class="fromToStyle" >
                               </td>
                            <td class="dataStyle">
                               <table width="100%" height="200" align="left" >
                                    <tr><td><asp:Label ID="Label5" runat="server" Text="Available Model List"></asp:Label></td></tr>
                                    <tr><td><asp:ListBox ID="lbModelId" runat="server" SelectionMode="Multiple" Height="160px" Width="60%"></asp:ListBox></td></tr>
                                    <tr><td><asp:Label ID="Label6" runat="server" Text="*NOTE:Shift+Click to select multiple items." Font-Size="X-Small"></asp:Label></td></tr>
                               </table>
                            </td>
                               
                             <td class="fromToStyle">
                                <table width="100%" height="200" align="left" >
                                    <tr><td align="center" valign="bottom"><asp:Button ID="btnToLeftModel" runat="server" Text=">>" /></td></tr>
                                    <tr><td align="center" valign="top"><asp:Button ID="btnToRightModel" runat="server" Text="<<" /></td></tr>
                                </table>
                              </td>   
                              <td class="dataStyle">
                                 <table width="100%" height="200" align="left" >
                                    <tr><td><asp:Label ID="Label7" runat="server" Text="Selected Model List"></asp:Label></td></tr>
                                    <tr><td><asp:ListBox ID="lbSelectedModelId" runat="server" SelectionMode="Multiple" Height="160px" Width="60%"></asp:ListBox></td></tr>
                                    <tr><td><asp:Label ID="Label8" runat="server" Text=""></asp:Label></td></tr>
                               </table>
                              </td>
                        </tr>                        
                    </table>
                    <asp:LinkButton ID="LinkReport" runat="server">View Report</asp:LinkButton>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
