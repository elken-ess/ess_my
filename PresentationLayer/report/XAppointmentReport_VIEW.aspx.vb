Imports System.Data
Imports System.Data.SqlClient
Imports System.Globalization
Imports SQLDataAccess
Imports System.Windows
Imports Microsoft.Win32
Partial Class PresentationLayer_report_XAppointmentReport_VIEW
    Inherits System.Web.UI.Page

    Dim RankA As String
    Dim RankB As String
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)
        Return connection
    End Function


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim _con As SqlConnection
        _con = GetConnection(ConfigurationManager.AppSettings("ConnectionString"))

        FromDate.Text = Session("FromDate").ToString
        ToDate.Text = Session("ToDate").ToString
        SVCFrom.Text = Session("SVCFrom").ToString
        SVCTo.Text = Session("SVCTo").ToString
        FromCreateDate.Text = Session("FromCreateDate").ToString
        ToCreateDate.Text = Session("ToCreateDate").ToString
        ProductFrom.Text = IIf(Session("ProductFrom").ToString = "", "ALL", Session("ProductFrom").ToString)
        ModelFrom.Text = IIf(Session("ModelFrom").ToString = "", "ALL", Session("ModelFrom").ToString)
        ModelTo.Text = IIf(Session("ModelTo").ToString = "", "ALL", Session("ModelTo").ToString)
        CRFrom.Text = Session("CRID").ToString
        ReasonObjection.Text = IIf(Session("ReasonObjectionDesc").ToString = "", "ALL", Session("ReasonObjectionDesc").ToString)

        Dim sql As String = String.Empty
        sql = " declare @reasonobjection varchar(10),@CRFrom varchar(100),@SVCFrom varchar(20), @SVCto varchar(20),@ModelFrom varchar(20), @ModelTo varchar(20), @ProductFrom VARCHAR(20) " _
            & " set @SVCFrom = '" & SVCFrom.Text & "'" _
            & " set @SVCTo = '" & SVCTo.Text & "'" _
            & " set @reasonobjection = '" & IIf(Session("ReasonObjection").ToString = "", "ALL", Session("ReasonObjection").ToString) & "'" _
            & " set @CRFrom = '" & CRFrom.Text & "'" _
            & " set @ModelFrom = '" & ModelFrom.Text & "'" _
            & " set @ModelTo = '" & ModelTo.Text & "'" _
            & " set @ProductFrom = '" & ProductFrom.Text & "'" _
            & " if @SVCFrom = 'ALL' begin set @SVCFrom = '000' end " _
            & " if @SVCTo = 'ALL' begin set @SVCTo = 'ZZZ' end " _
            & " if @reasonobjection = 'ALL' begin set @reasonobjection = '%' end " _
            & " if @ProductFrom = 'ALL' begin set @ProductFrom = '%' end " _
            & " if @CRFrom = 'ALL' begin set @CRFrom = '%' end " _
            & " if @ModelFrom = 'ALL' begin set @ModelFrom = '' end " _
            & " if @ModelTo = 'ALL' begin set @ModelTo = 'ZZZ' end " _
            & " select   fapt_svcid as svcCenter, convert(varchar, fapt_aptdt, 103) as appointmentDate, " _
            & " MROU_MODID,fapt_cusid,MCUS_ENAME,ReasonObj_Desc as reasonobjection,FAPT_CRID,FAPT_CRID + '-' +MUSR_ENAME AS CR_UsrNAME,[FAPT_CREDT] = convert(varchar, FAPT_CREDT, 103)  " _
            & " from FAPT_FIL with (nolock) left join MCUS_FIL with (nolock) on FAPT_CUSID = MCUS_CUSID  and FAPT_CUSPF = MCUS_CUSPF " _
            & " left join MUSR_FIL with (nolock) on FAPT_CRID = MUSR_USRID  " _
            & " left join MROU_FIL with (nolock) on MROU_ROUID = FAPT_ROUID and MROU_CUSPF = FAPT_CUSPF " _
            & " left join ReasonObjection with (nolock) on reasonobjection = ReasonObj_ID " _
            & " where FAPT_SVCID >= @SVCFrom and FAPT_SVCID <= @SVCTo "

        If FromDate.Text <> "DD/MM/YYYY" And FromDate.Text <> "" And ToDate.Text <> "DD/MM/YYYY" And ToDate.Text <> "" Then
            sql = sql + " and FAPT_APTDT >= '" & Right(FromDate.Text, 4) + FromDate.Text.Substring(3, 2) + Left(FromDate.Text, 2) & "' "
            sql = sql + " and FAPT_APTDT <= dateadd(d,1,'" & Right(ToDate.Text, 4) + ToDate.Text.Substring(3, 2) + Left(ToDate.Text, 2) & "') "
        End If
        If FromCreateDate.Text <> "DD/MM/YYYY" And FromCreateDate.Text <> "" And ToCreateDate.Text <> "DD/MM/YYYY" And ToCreateDate.Text <> "" Then
            sql = sql + " and fapt_credt >= '" & Right(FromCreateDate.Text, 4) + FromCreateDate.Text.Substring(3, 2) + Left(FromCreateDate.Text, 2) & "' "
            sql = sql + " and fapt_credt <= dateadd(d,1,'" & Right(ToCreateDate.Text, 4) + ToCreateDate.Text.Substring(3, 2) + Left(ToCreateDate.Text, 2) & "') "
        End If
           
        sql = sql + " and MROU_MODID >= @ModelFrom and MROU_MODID <= @ModelTo  " _
            & " and FAPT_CRID like @CRFrom   " _
            & " and isnull(reasonobjection,'') like @reasonobjection  " _
            & " and MUSR_STAT <> 'DELETE' and MCUS_STAT = 'ACTIVE' and fapt_stat='X' " _
            & " and isnull(MROU_CLSID,'') like @ProductFrom " _
            & " order by fapt_svcid,FAPT_CREDT,MROU_MODID" _
            & " --and MROU_MODID IN (SELECT distinct MMOD_MODID FROM dbo.MMOD_FIL where MMOD_STAT = 'ACTIVE'  and MMOD_CLSID LIKE @ProductFrom) "


        Using Comm As New SqlClient.SqlCommand(sql, _con)
            _con.Open()

            Dim dr1 As SqlDataReader = Comm.ExecuteReader(CommandBehavior.CloseConnection)

            Dim dt1 As DataTable = New DataTable()
            dt1.Load(dr1)

            GridView1.DataSource = dt1
            GridView1.DataBind()

            GridView1.Visible = True

            _con.Close()
        End Using


    End Sub
    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub

    Protected Sub Export_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Export.Click
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=GridView1.xls")
        Response.Charset = ""
        Response.ContentType = "application/vnd.xls"
        Response.Buffer = False
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        'Dim StringWriter As IO.StringWriter = New System.IO.StringWriter()
        'Dim HtmlTextWriter As New HtmlTextWriter(StringWriter)
        Dim HtmlTextWriter As New HtmlTextWriter(Response.Output)
        Dim style As String = "<style> td { mso-number-format:\@; } </style> "

        GridView1.RenderControl(HtmlTextWriter)
        Response.Write(style) 'style is added dynamically
        ' Response.Write(StringWriter.ToString())
        Response.[End]()
    End Sub
End Class
