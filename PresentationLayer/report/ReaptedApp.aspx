﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ReaptedApp.aspx.vb" Inherits="PresentationLayer_Report_2ReaptedApp" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
     <table id="countrytab" border="0" style="width: 90%">
            <tr>
                <td style="width: 535px">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title1.gif" width="5" /></td>
                            <td background="../graph/title_bg.gif" class="style2" width="98%">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="right" background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title_2.gif" width="5" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 535px">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" class="style2" width="98%">
                                <font color="red">
                                    <asp:Label ID="errlab" runat="server" Text="Label" Visible="false"></asp:Label></font></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 535px">
                    <table id="country" bgcolor="#b7e6e6" border="0" cellpadding="2" cellspacing="1" style="width: 101%">
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 27%; height: 28px;">
                                <asp:Label ID="serlab" runat="server" Text="Label" Font-Size="Smaller"></asp:Label></td>
                            <td align="right" style="width: 13%; height: 28px;">
                                <asp:Label ID="from1" runat="server" Text="Label" Font-Size="Smaller"></asp:Label></td>
                            <td align="left" style="width: 34%; height: 28px;">
                                <asp:TextBox ID="sercen1" runat="server" CssClass="textborder" style="width: 90%"></asp:TextBox></td>
                            <td align="right" style="width: 12%; height: 28px;">
                                <asp:Label ID="to1" runat="server" Text="Label" Font-Size="Smaller"></asp:Label></td>
                            <td align="left" style="width: 37%; height: 28px">
                                <asp:TextBox ID="sercen2" runat="server" CssClass="textborder">ZZZZZZZZZZZZZ</asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 27%; height: 28px;">
                                <asp:Label ID="datelab" runat="server" Text="Label" Font-Size="Smaller"></asp:Label></td>
                            <td align="right" style="width: 13%; height: 28px;">
                                &nbsp;<asp:Label ID="from2" runat="server" Text="Label" Font-Size="Smaller"></asp:Label></td>
                            <td align="left" style="width: 34%; height: 28px;">
                                <asp:TextBox ID="date1" runat="server" CssClass="textborder" style="width: 80%" ReadOnly="True">01/01/2006</asp:TextBox>
                                <asp:ImageButton ID="effecalender" runat="server" CausesValidation="False" ImageUrl="~/PresentationLayer/graph/calendar.gif" />
                                <asp:Calendar ID="Calendar1" runat="server" BackColor="White" BorderColor="#3366CC"
                                    BorderWidth="1px" CellPadding="1" DayNameFormat="Shortest" Font-Names="Verdana"
                                    Font-Size="8pt" ForeColor="#003399" Height="56px" Visible="False" Width="128px">
                                    <SelectedDayStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                                    <TodayDayStyle BackColor="#99CCCC" ForeColor="White" />
                                    <SelectorStyle BackColor="#99CCCC" ForeColor="#336666" />
                                    <WeekendDayStyle BackColor="#CCCCFF" />
                                    <OtherMonthDayStyle ForeColor="#999999" />
                                    <NextPrevStyle Font-Size="8pt" ForeColor="#CCCCFF" />
                                    <DayHeaderStyle BackColor="#99CCCC" ForeColor="#336666" Height="1px" />
                                    <TitleStyle BackColor="#003399" BorderColor="#3366CC" BorderWidth="1px" Font-Bold="True"
                                        Font-Size="10pt" ForeColor="#CCCCFF" Height="25px" />
                                </asp:Calendar>
                            </td>
                            <td align="right" style="width: 12%; height: 28px;">
                                <asp:Label ID="to2" runat="server" Text="Label" Font-Size="Smaller"></asp:Label></td>
                            <td align="left" style="width: 37%; height: 28px">
                                <asp:TextBox ID="date2" runat="server" CssClass="textborder" ReadOnly="True" style="width: 80%">31/12/2006</asp:TextBox>
                                <asp:ImageButton ID="ImageButton1" runat="server" CausesValidation="False" ImageUrl="~/PresentationLayer/graph/calendar.gif" />
                                <asp:Calendar ID="Calendar2" runat="server" BackColor="White" BorderColor="#3366CC"
                                    BorderWidth="1px" CellPadding="1" DayNameFormat="Shortest" Font-Names="Verdana"
                                    Font-Size="8pt" ForeColor="#003399" Height="56px" Visible="False" Width="128px">
                                    <SelectedDayStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                                    <TodayDayStyle BackColor="#99CCCC" ForeColor="White" />
                                    <SelectorStyle BackColor="#99CCCC" ForeColor="#336666" />
                                    <WeekendDayStyle BackColor="#CCCCFF" />
                                    <OtherMonthDayStyle ForeColor="#999999" />
                                    <NextPrevStyle Font-Size="8pt" ForeColor="#CCCCFF" />
                                    <DayHeaderStyle BackColor="#99CCCC" ForeColor="#336666" Height="1px" />
                                    <TitleStyle BackColor="#003399" BorderColor="#3366CC" BorderWidth="1px" Font-Bold="True"
                                        Font-Size="10pt" ForeColor="#CCCCFF" Height="25px" />
                                </asp:Calendar>
                            </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 27%">
                                <asp:Label ID="cuslab" runat="server" Text="Label" Font-Size="Smaller"></asp:Label></td>
                            <td align="right" style="width: 13%">
                                &nbsp;<asp:Label ID="from3" runat="server" Text="Label" Font-Size="Smaller"></asp:Label></td>
                            <td align="left" style="width: 34%">
                                <asp:TextBox ID="custom1" runat="server" CssClass="textborder" style="width: 90%"></asp:TextBox></td>
                            <td align="right" style="width: 12%">
                                <asp:Label ID="to3" runat="server" Text="Label" Font-Size="Smaller"></asp:Label></td>
                            <td align="left" style="width: 37%">
                                <asp:TextBox ID="custom2" runat="server" CssClass="textborder">ZZZZZZZZZZZZZ</asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 27%">
                                <asp:Label ID="serilab" runat="server" Text="Label" Font-Size="Smaller"></asp:Label></td>
                            <td align="right" style="width: 13%">
                                <asp:Label ID="from4" runat="server" Text="Label" Font-Size="Smaller"></asp:Label></td>
                            <td align="left" style="width: 34%">
                                <asp:TextBox ID="seri1" runat="server" CssClass="textborder" style="width: 90%"></asp:TextBox></td>
                            <td align="right" style="width: 12%">
                                <asp:Label ID="to4" runat="server" Text="Label" Font-Size="Smaller"></asp:Label></td>
                            <td align="left" style="width: 37%">
                                <asp:TextBox ID="seri2" runat="server" CssClass="textborder">ZZZZZZZZZZZZZ</asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 27%; height: 28px;">
                                <asp:Label ID="modelab" runat="server" Text="Label" Font-Size="Smaller"></asp:Label></td>
                            <td align="right" style="width: 13%; height: 28px;">
                                <asp:Label ID="from5" runat="server" Text="Label" Font-Size="Smaller"></asp:Label></td>
                            <td align="left" style="width: 34%; height: 28px;">
                                <asp:TextBox ID="modelid1" runat="server" CssClass="textborder" style="width: 90%"></asp:TextBox></td>
                            <td align="right" style="width: 12%; height: 28px;">
                                <asp:Label ID="to5" runat="server" Text="Label" Font-Size="Smaller"></asp:Label></td>
                            <td align="left" style="height: 28px; width: 37%;">
                                <asp:TextBox ID="modelid2" runat="server" CssClass="textborder">ZZZZZZZZZZZZZ</asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 27%; height: 28px">
                                <asp:Label ID="repelab" runat="server" Text="Label" Font-Size="Smaller"></asp:Label></td>
                            <td align="right" style="width: 13%; height: 28px">
                                <asp:DropDownList ID="Reaptedp" runat="server" Style="width: 100%">
                                    <asp:ListItem>&gt;</asp:ListItem>
                                    <asp:ListItem>&gt;=</asp:ListItem>
                                    <asp:ListItem>&lt;</asp:ListItem>
                                    <asp:ListItem>&lt;=</asp:ListItem>
                                </asp:DropDownList></td>
                            <td align="left" style="width: 35%; height: 28px"  colspan=3>
                                <asp:TextBox ID="repeat" runat="server"></asp:TextBox></td>
                           
                           
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 27%; height: 28px;">
                                <asp:Label ID="sertylab" runat="server" Text="Label" Font-Size="Smaller"></asp:Label></td>
                            <td align="left" style="width: 13%; height: 28px;" colspan =4>
                                &nbsp;<asp:DropDownList ID="servity" runat="server" Width="128px">
                                </asp:DropDownList></td>
                             
                            
                        </tr>
                    </table>
                    <asp:LinkButton ID="LinkButton1" runat="server">LinkButton</asp:LinkButton>
        </td>
            </tr>
        </table>
        
    </div>
        <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" AutoDataBind="true" HasExportButton="False" />
        <asp:RegularExpressionValidator ID="repnumerr" runat="server" ControlToValidate="repeat"
            ForeColor="White" ValidationExpression="\d{0,20}" Width="8px">*</asp:RegularExpressionValidator>
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
            ShowSummary="False" />
        &nbsp;
    </form>
</body>
</html>
