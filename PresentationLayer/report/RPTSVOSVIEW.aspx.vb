﻿Imports System.Web
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web
Imports BusinessEntity
Imports System.Data
Partial Class PresentationLayer_report_RPTSVOSVIEW_ASPX
    Inherits System.Web.UI.Page

    Dim clsReport As New ClsCommonReport

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If

        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Dim rptsvos As New clsRPTSVOS
        rptsvos = Session("rptsvos_searchcondition")


        Dim rptds As DataSet = rptsvos.GetServiceOrderSum()

        ' switch  encrypt to telphone for display        
        Dim count As Integer
        Dim prdctclname As String
        Dim clscomm As New clsCommonClass()
        For count = 0 To rptds.Tables(0).Rows.Count - 1
            Dim prdctclsid As String = rptds.Tables(0).Rows(count).Item(3).ToString 'item(8) is product class 
            prdctclname = clscomm.passconverttel(prdctclsid)
            rptds.Tables(0).Rows(count).Item(3) = prdctclname
        Next
        '''''''''''''''''''''''''''''''''''''''''''''''''''


        Dim objXmlTr As New clsXml

        Dim strHead As Array = Array.CreateInstance(GetType(String), 52)
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        strHead(0) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0002")
        strHead(1) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0003")
        strHead(2) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0004")
        strHead(3) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0001")
        strHead(4) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0005")
        strHead(5) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0006")
        strHead(6) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0007")
        strHead(7) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0008")
        strHead(8) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0009")
        strHead(9) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0010")

        strHead(10) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0021")
        strHead(11) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0022")
        strHead(12) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0023")
        strHead(13) = rptsvos.mindate
        strHead(14) = rptsvos.maxdate
        strHead(15) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0024")

        Dim str As String = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_E_TOTAL") ' RPT_APT_RECORD
        If (rptds.Tables(0).Rows.Count > 0) Then

            strHead(16) = str & " " & rptds.Tables(0).Rows(0).Item("TotalRecord").ToString
        Else
            strHead(16) = str & " 0"
        End If
        If Session("userID") = "" Then
            strHead(17) = " "
        Else
            strHead(17) = "/" & Session("userID").ToString().ToUpper + "/" + Session("username").ToString.ToUpper

        End If
        'strHead(17) = Session("userID")
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        strHead(18) = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
        strHead(19) = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''      

        strHead(20) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0012") 'Customer
        strHead(21) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0013") 'Service CenterID
        strHead(22) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0014") 'Model
        strHead(23) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0015") 'RO Serial No.
        strHead(24) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0016") 'State  

        strHead(25) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0020") 'Appoint Date
        strHead(26) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0017") 'Technician
        strHead(27) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0018") 'Area
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        If rptsvos.minCustmID = Nothing Then
            strHead(28) = "AAA"
        Else
            strHead(28) = rptsvos.minCustmID.ToString
        End If
        If rptsvos.maxCustmID = Nothing Then
            strHead(29) = "AAA"
        ElseIf rptsvos.maxCustmID = "ZZZZZZZZZZZZZZZZZZZZ" Then
            strHead(29) = "ZZZ"
        Else
            strHead(29) = rptsvos.maxCustmID.ToString
        End If

        If rptsvos.minSvcID = Nothing Then
            strHead(30) = "AAA"
        Else
            strHead(30) = rptsvos.minSvcID.ToString
        End If
        If rptsvos.maxSvcID = Nothing Then
            strHead(31) = "AAA"
        ElseIf rptsvos.maxSvcID = "ZZZZZZZZZZZZZZZZZZZZ" Then
            strHead(31) = "ZZZ"
        Else
            strHead(31) = rptsvos.maxSvcID.ToString
        End If

        If rptsvos.minPrdctMd = Nothing Then
            strHead(32) = "AAA"
        Else
            strHead(32) = rptsvos.minPrdctMd.ToString
        End If
        If rptsvos.maxPrdctMd = Nothing Then
            strHead(33) = "AAA"
        ElseIf rptsvos.maxPrdctMd = "ZZZZZZZZZZZZZZZZZZZZ" Then
            strHead(33) = "ZZZ"
        Else
            strHead(33) = rptsvos.maxPrdctMd.ToString
        End If
        If rptsvos.minRONo = Nothing Then
            strHead(34) = "AAA"
        Else
            strHead(34) = rptsvos.minRONo.ToString
        End If
        If rptsvos.maxRONo = Nothing Then
            strHead(35) = "AAA"
        ElseIf rptsvos.maxRONo = "ZZZZZZZZZZZZZZZZZZZZ" Then
            strHead(35) = "ZZZ"
        Else
            strHead(35) = rptsvos.maxRONo.ToString
        End If
        '''''''''''''''''''''''''''''''''''''''''''''
        If rptsvos.minState = Nothing Then
            strHead(36) = "AAA"
        Else
            strHead(36) = rptsvos.minState.ToString
        End If
        If rptsvos.maxState = Nothing Then
            strHead(37) = "AAA"
        ElseIf rptsvos.maxState = "ZZZZZZZZZZZZZZZZZZZZ" Then
            strHead(37) = "ZZZ"
        Else
            strHead(37) = rptsvos.maxState.ToString
        End If
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''' 
        '''''''''''''''''''''''''''''''''''''''''''''      　
        strHead(38) = rptsvos.mindate

        strHead(39) = rptsvos.maxdate
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        If rptsvos.minTchID = Nothing Then
            strHead(40) = "AAA"
        Else
            strHead(40) = rptsvos.minTchID.ToString
        End If
        If rptsvos.maxTchID = Nothing Then
            strHead(41) = "AAA"
        ElseIf rptsvos.maxTchID = "ZZZZZZZZZZZZZZZZZZZZ" Then
            strHead(41) = "ZZZ"
        Else
            strHead(41) = rptsvos.maxTchID.ToString
        End If
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        If rptsvos.minArea = Nothing Then
            strHead(42) = "AAA"
        Else
            strHead(42) = rptsvos.minArea.ToString
        End If
        If rptsvos.maxArea = Nothing Then
            strHead(43) = "AAA"
        ElseIf rptsvos.maxArea = "ZZZZZZZZZZZZZZZZZZZZ" Then
            strHead(43) = "ZZZ"
        Else
            strHead(43) = rptsvos.maxArea.ToString
        End If
        strHead(44) = "/"
        strHead(45) = objXmlTr.GetLabelName("EngLabelMsg", "STATUS")

        strHead(46) = rptsvos.StatusText
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        strHead(47) = objXmlTr.GetLabelName("EngLabelMsg", "APPOINTMENT_CREATEDATE")

        strHead(48) = objXmlTr.GetLabelName("EngLabelMsg", "SBA-SERVICETYPE")
        strHead(49) = rptsvos.ServiceTypeText

        'With clsReport
        '    .ReportFileName = "RPTSVOS.rpt"  'TextBox1.Text
        '    .SetReport(CrystalReportViewer1, rptds, strHead)
        'End With

        Dim lstrPdfFileName As String = "ServiceOrderSummary_" & Session("login_session") & ".pdf"
        Try
            With clsReport
                .ReportFileName = "RPTSVOS.rpt"
                .SetReportDocument(rptds, strHead)
                .PdfFileName = lstrPdfFileName
                .ExportPdf()
            End With

            Response.Redirect(clsReport.PdfUrl)

        Catch err As Exception
            'System.IO.File.Delete(lstrPhysicalFile)
            Response.Write("<BR>")
            Response.Write("Error Message: " & err.Message.ToString)
            Response.Write("<BR>")
            Response.Write("Source : " & err.Source)
            Response.Write("<BR>")
            Response.Write("Stack Trace Message: " & err.StackTrace)


        End Try

        titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0022") & " " & clsReport.GetMessage

    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload

        clsReport.UnloadReport()
        titleLab.Text = titleLab.Text & " " & clsReport.GetMessage
        CrystalReportViewer1.Dispose()
    End Sub
End Class
