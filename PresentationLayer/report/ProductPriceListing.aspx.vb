Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Web
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web
Imports BusinessEntity
Imports System.Data

Partial Class PresentationLayer_report_ProductPriceListing
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        If Not Page.IsPostBack Then
            Session("CTRNWL-FLAG") = "0"
            Dim clsReport As New ClsCommonReport

           
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            Me.partcodelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0002")
            Me.partfromlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Me.parttolab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Me.prictypelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0003")
            Me.pricefromlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Me.pricetolab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Me.sortLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0004")
            Me.sortbyLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0005")
            Me.thenbyLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0006")
            Me.commlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0007")
            Me.onlylab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0008")
            Me.partlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0009")
            Me.statuslab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0010")
            Me.LinkButton1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-VIEWREPORT")
            Me.titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0001")


            Me.sortbyDrop.Items.Add(New ListItem(Me.partcodelab.Text.ToString(), "MPAR_PARID"))
            Me.sortbyDrop.Items.Add(New ListItem(Me.prictypelab.Text.ToString(), "MPP1_PRCID"))
            Me.thenbyDrop.Items.Add(New ListItem(Me.partcodelab.Text.ToString(), "MPAR_PARID"))
            Me.thenbyDrop.Items.Add(New ListItem(Me.prictypelab.Text.ToString(), "MPP1_PRCID"))


            Dim namey As String
            Dim namen As String
            namey = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0011")
            namen = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0012")
            Me.onlyDrop.Items.Add(New ListItem(namey, "Y"))
            Me.onlyDrop.Items.Add(New ListItem(namen, "N"))

            Dim statusa As String
            Dim statuso As String
            Dim statusl As String
            Dim statusn As String
            statusa = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0019")
            statuso = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0020")
            statusl = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0021")
            statusn = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0012")
            Me.statusDrop.Items.Add(New ListItem(statusa, "ACTIVE"))
            Me.statusDrop.Items.Add(New ListItem(statuso, "OBSOLETE"))
            Me.statusDrop.Items.Add(New ListItem(statusl, "ACTIVELL"))
            Me.LinkButton1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-VIEWREPORT")
            Me.titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0001")


            '''access control'''''''''''''''''''''''''''''''''''''''''''''''
            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "49")
            Me.LinkButton1.Enabled = purviewArray(3)

            Me.parttoBox.Text = "ZZZ"
            Me.pricetoBox.Text = "ZZZ"
            Me.partfrombox.Text = "AAA"
            Me.pricefrombox.Text = "AAA"


          
        ElseIf Session("CTRNWL-FLAG") = "1" Then
            Print_Report()
        End If



    End Sub


    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton1.Click

        Session("CTRNWL-FLAG") = "1"
        Print_Report()

        'Response.Redirect("~/PresentationLayer/REPORT/ProductPriceListingView.aspx")
        Dim script As String = "window.open('../report/ProductPriceListingView.aspx')"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "SystemAccessReport", script, True)

    End Sub

    

    Protected Sub Print_Report()
        Dim pplEntity As New clsRPTPPL()
        If (Trim(Me.partfrombox.Text.ToUpper) <> "AAA") Then
            pplEntity.PtcodeFrom = Me.partfrombox.Text
        End If
        If (Trim(Me.parttoBox.Text.ToUpper) <> "ZZZ") Then
            pplEntity.PtcodeTo = Me.parttoBox.Text
        Else
            pplEntity.PtcodeTo = "ZZZZZZZZZZZZZZZZZZZZ"
        End If
        If (Trim(Me.pricefrombox.Text.ToString.ToUpper) <> "AAA") Then
            pplEntity.PriceFrom = Me.pricefrombox.Text
        End If
        If (Trim(Me.pricetoBox.Text.ToUpper) <> "ZZZ") Then
            pplEntity.PriceTo = Me.pricetoBox.Text
        Else
            pplEntity.PriceTo = "ZZZZZZZZZZZZZZZZZZZZ"
        End If
       
      
        If (sortbyDrop.SelectedItem.Value.ToString() <> "") Then
            pplEntity.Sortby = sortbyDrop.SelectedItem.Value.ToString()
        End If
        If (thenbyDrop.SelectedItem.Value.ToString() <> "") Then
            pplEntity.Thenby = thenbyDrop.SelectedItem.Value.ToString()
        End If
        If (onlyDrop.SelectedItem.Value.ToString() <> "") Then
            pplEntity.CommPart = onlyDrop.SelectedItem.Value.ToString()
        End If
        If (statusDrop.SelectedItem.Value.ToString() <> "") Then
            pplEntity.SerialNoFrom = Me.statusDrop.SelectedValue.ToString
        End If

        Dim rank As String = Session("login_rank")
        If rank <> 0 Then
            pplEntity.ctrid = Session("login_ctryID").ToString().ToUpper
            pplEntity.comid = Session("login_cmpID").ToString().ToUpper
            pplEntity.svcid = Session("login_svcID").ToString().ToUpper
        End If
        pplEntity.rank = rank
        pplEntity.SortByText = Me.sortbyDrop.SelectedItem.Text
        pplEntity.ThenByText = Me.thenbyDrop.SelectedItem.Text
        pplEntity.PdctStatus = Me.statusDrop.SelectedValue.ToString
        pplEntity.OnlyTest = Me.onlyDrop.SelectedItem.Text

        Try
            'Dim pplds As New DataSet
            'pplds = pplEntity.GetProductPriceListing()

            Session("rpt_pplEntity") = pplEntity


            'Dim clsReport As New ClsCommonReport
            'Dim objXmlTr As New clsXml
            'Dim strHead As Array = Array.CreateInstance(GetType(String), 13)
            'objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            'strHead(0) = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0002")
            'strHead(1) = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0013")
            'strHead(2) = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0003")
            'strHead(3) = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0014")
            'strHead(4) = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0015")
            'strHead(5) = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0016")
            'strHead(6) = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0001")
            'strHead(7) = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0017")
            'strHead(8) = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0017")
            'strHead(9) = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0018")
            'strHead(10) = pplds.Tables(0).Rows.Count
            'strHead(11) = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0018")
            'strHead(12) = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0021")
            'If Me.IsPostBack Then
            '    With clsReport
            '        .ReportFileName = "ProductPriceListing.rpt"  'TextBox1.Text
            '        .SetReport(CrystalReportViewerPPL, pplds, strHead)
            '    End With
            'End If

        Catch ex As Exception
            Response.Redirect("~/PresentationLayer/Error.aspx")

        End Try

    End Sub


End Class
