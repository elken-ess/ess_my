
Imports BusinessEntity
Imports System.Data

Namespace PresentationLayer.report
    Partial Class PresentationLayer_report_RPTINSTALL_VIEW
        Inherits Page
        'Implements IBasicReportPage
#Region "Declaration"
        Private ReadOnly _xml As New clsXml
        Private _rptInstall As New ClsRptInstall
        Private _dsInstall As New dataset
        Private ReadOnly _datestyle As Globalization.CultureInfo = New Globalization.CultureInfo("en-CA")
        Private ReadOnly _clsReport As New ClsCommonReport

#End Region

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
            Threading.Thread.CurrentThread.CurrentCulture = _datestyle

            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Const script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Const script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try

            Dim datestyle As New Globalization.CultureInfo("en-CA")
            Threading.Thread.CurrentThread.CurrentCulture = datestyle

            _rptInstall = CType(Session("InstallBase"), ClsRptInstall)
            _dsInstall = _rptInstall.RetrieveInstallBase()

            GenerateReport(_dsInstall)
            End Sub


        Private Sub PopulateLabels() 'Implements IBasicReportPage.PopulateLabels
            Throw New NotImplementedException()
        End Sub

        Public Sub PopulateLabels(ByVal strHead As String()) 'Implements IBasicReportPage.PopulateLabels
            Throw New NotImplementedException()
        End Sub

        Private Sub GenerateReport() 'Implements IBasicReportPage.GenerateReport
            Throw New NotImplementedException()
        End Sub

        Private Sub DefineDefaultValues() 'Implements IBasicReportPage.DefineDefaultValues
            Throw New NotImplementedException()
        End Sub

        Public Sub DefineDefaultValues(ByVal strHead As String()) 'Implements IBasicReportPage.DefineDefaultValues
            'Values
            If String.IsNullOrEmpty(_rptInstall.ProdClass) Then
                strHead(8) = "ZZZ"
            Else
                strHead(8) = _rptInstall.ProdClass
            End If

            If String.IsNullOrEmpty(_rptInstall.ModID) Then
                strHead(9) = "ZZZ"
            Else
                strHead(9) = _rptInstall.ModID
            End If

            strHead(10) = _rptInstall.StartInstallDate.ToShortDateString()
            strHead(11) = _rptInstall.EndInstallDate.ToShortDateString()

            If String.IsNullOrEmpty(_rptInstall.SvcId) Then
                strHead(12) = "ZZZ"
            Else
                strHead(12) = _rptInstall.SvcId
            End If
            If String.IsNullOrEmpty(_rptInstall.SvcIdTo) Then
                strHead(18) = "ZZZ"
            Else
                strHead(18) = _rptInstall.SvcIdTo
            End If

            If String.IsNullOrEmpty(_rptInstall.StateID) Then
                strHead(13) = "ZZZ"
            Else
                strHead(13) = _rptInstall.StateID
            End If
            If String.IsNullOrEmpty(_rptInstall.StateIDTo) Then
                strHead(20) = "ZZZ"
            Else
                strHead(20) = _rptInstall.StateIDTo
            End If


            If String.IsNullOrEmpty(_rptInstall.AreaID) Then
                strHead(14) = "ZZZ"
            Else
                strHead(14) = _rptInstall.AreaID
            End If
            If String.IsNullOrEmpty(_rptInstall.AreaIDTo) Then
                strHead(19) = "ZZZ"
            Else
                strHead(19) = _rptInstall.AreaIDTo
            End If

            If String.IsNullOrEmpty(_rptInstall.ModID) Then
                strHead(15) = "ZZZ"
            Else
                strHead(15) = _rptInstall.ModID
            End If
            If String.IsNullOrEmpty(_rptInstall.ModIDTo) Then
                strHead(21) = "ZZZ"
            Else
                strHead(21) = _rptInstall.ModIDTo
            End If

        End Sub

        Private Sub GenerateReport(ByVal dataSet As DataSet) 'Implements IBasicReportPage.GenerateReport
            titleLab.Text = _xml.GetLabelName("EngLabelMsg", "BB-INSTALLBASE-TITLE")
            
            _rptInstall = CType(Session("InstallBase"), ClsRptInstall)

            Dim strHead As String() = CType(Array.CreateInstance(GetType(String), 22), String())
            strHead(0) = "From"
            strHead(1) = "To"
            strHead(2) = "Product Class" '_xml.GetLabelName("EngLabelMsg", "BB-MASROUM-0010")
            strHead(3) = "Model" '_xml.GetLabelName("EngLabelMsg", "BB-RPTSOSA-MODEL")
            strHead(4) = "Installation Date" '_xml.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0007")
            strHead(5) = "Service Center" '_xml.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-SERVCENTER")
            strHead(6) = "State" '_xml.GetLabelName("EngLabelMsg", "BB-MASROUM-0032")
            strHead(7) = "Area" '_xml.GetLabelName("EngLabelMsg", "BB-MASROUM-0033")

            If dataSet.Tables(0).Rows.Count > 0 Then
                DefineDefaultValues(strHead)

                strHead(15) = "Elken Group of Companies"
                strHead(16) = _xml.GetLabelName("EngLabelMsg", "BB-INSTALLBASE-TITLE")
                strHead(17) = Session("userID").ToString() + " / " + Session("username").ToString()

                With _clsReport
                    .ReportFileName = "RPTInstall.rpt"  'TextBox1.Text
                    .SetReport(CrystalReportViewer1, dataSet, strHead)

                End With
            End If

        End Sub

        Protected Sub Page_Unload(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Unload
            _clsReport.UnloadReport()
            CrystalReportViewer1.Dispose()
        End Sub
    End Class
End Namespace