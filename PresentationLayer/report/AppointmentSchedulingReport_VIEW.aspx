<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AppointmentSchedulingReport_VIEW.aspx.vb" Inherits="PresentationLayer_report_AppointmentSchedulingReport_VIEW" EnableEventValidation="false"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Untitled Page</title>
    <script language="javascript" type="text/javascript">
// <!CDATA[

function TABLE1_onclick() {

}
function printpage()
  {
  window.print()
  }
// ]]>
</script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table id="TABLE1" onclick="return TABLE1_onclick()" style="width: 1047px; height: 209px">
            <tr>
                <td align="left" colspan="8" style="height: 25px" valign="bottom">
                    &nbsp;
                    <input id="Button1" onclick="printpage()" size="21" style="color: black; border-top-style: solid;
                        border-right-style: solid; border-left-style: solid; background-color: transparent;
                        border-bottom-style: solid" type="button" value="Print" />
                    <asp:Button ID="Export" runat="server" BackColor="Transparent" BorderStyle="Solid"
                        Text="Export to Excel" />
                    <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="No data is available for selected range of criteria! "
                        Visible="False"></asp:Label></td>
            </tr>
            <tr>
                <td align="center" colspan="8" style="height: 22px" valign="bottom">
                    <asp:Label ID="Label29" runat="server" Font-Size="Medium" Text="Appointment Scheduling Report"></asp:Label></td>
            </tr>
            <tr>
                <td style="width: 100px; height: 48px" valign="bottom">
                </td>
                <td style="width: 100px; height: 48px" valign="bottom">
                </td>
                <td style="width: 111px; height: 48px">
                </td>
                <td style="width: 45px; height: 48px">
                </td>
                <td style="width: 131px; height: 48px">
                </td>
                <td style="width: 56px; height: 48px">
                </td>
                <td style="width: 149px; height: 48px">
                </td>
                <td style="width: 100px; height: 48px">
                </td>
            </tr>
            <tr>
                <td style="width: 100px; height: 21px">
                </td>
                <td style="width: 100px; height: 21px">
                </td>
                <td style="width: 111px; height: 21px">
                    <asp:Label ID="Label23" runat="server" Font-Size="Smaller" Text="Appointment Date"></asp:Label></td>
                <td style="width: 45px; height: 21px">
                    <asp:Label ID="Label11" runat="server" Font-Size="Smaller" Text="From"></asp:Label></td>
                <td style="width: 131px; height: 21px">
                    <asp:Label ID="FromDate" runat="server" Font-Size="Smaller" Text="Label"></asp:Label></td>
                <td style="width: 56px; height: 21px">
                    <asp:Label ID="Label17" runat="server" Font-Size="Smaller" Text="To"></asp:Label></td>
                <td style="width: 149px; height: 21px">
                    <asp:Label ID="ToDate" runat="server" Font-Size="Smaller" Text="Label"></asp:Label></td>
                <td style="width: 100px; height: 21px">
                </td>
            </tr>
            <tr>
                <td style="width: 100px; height: 21px">
                </td>
                <td style="width: 100px; height: 21px">
                </td>
                <td style="width: 111px; height: 21px">
                    <asp:Label ID="Label24" runat="server" Font-Size="Smaller" Text="Service Center"></asp:Label></td>
                <td style="width: 45px; height: 21px">
                    <asp:Label ID="Label12" runat="server" Font-Size="Smaller" Text="From"></asp:Label></td>
                <td style="width: 131px; height: 21px">
                    <asp:Label ID="SVCFrom" runat="server" Font-Size="Smaller" Text="Label"></asp:Label></td>
                <td style="width: 56px; height: 21px">
                    <asp:Label ID="Label18" runat="server" Font-Size="Smaller" Text="To"></asp:Label></td>
                <td style="width: 149px; height: 21px">
                    <asp:Label ID="SVCTo" runat="server" Font-Size="Smaller" Text="Label"></asp:Label></td>
                <td style="width: 100px; height: 21px">
                </td>
            </tr>
            <tr>
                <td style="width: 100px; height: 21px">
                </td>
                <td style="width: 100px; height: 21px">
                </td>
                <td style="width: 111px; height: 21px">
                    <asp:Label ID="Label25" runat="server" Font-Size="Smaller" Text="Service Type"></asp:Label></td>
                <td style="width: 45px; height: 21px">
                    <asp:Label ID="Label13" runat="server" Font-Size="Smaller" Text="From"></asp:Label></td>
                <td style="width: 131px; height: 21px">
                    <asp:Label ID="TypeFrom" runat="server" Font-Size="Smaller" Text="Label"></asp:Label></td>
                <td style="width: 56px; height: 21px">
                    <asp:Label ID="Label19" runat="server" Font-Size="Smaller" Text="To"></asp:Label></td>
                <td style="width: 149px; height: 21px">
                    <asp:Label ID="TypeTo" runat="server" Font-Size="Smaller" Text="Label"></asp:Label></td>
                <td style="width: 100px; height: 21px">
                </td>
            </tr>
            <tr>
                <td style="width: 100px; height: 21px">
                </td>
                <td style="width: 100px; height: 21px">
                </td>
                <td style="width: 111px; height: 21px">
                    <asp:Label ID="Label2" runat="server" Font-Size="Smaller" Text="Appointment Status"></asp:Label></td>
                <td style="width: 45px; height: 21px">
                    <asp:Label ID="Label3" runat="server" Font-Size="Smaller" Text="From"></asp:Label></td>
                <td style="width: 131px; height: 21px">
                    <asp:Label ID="StatusFrom" runat="server" Font-Size="Smaller" Text="Label"></asp:Label></td>
                <td style="width: 56px; height: 21px">
                    <asp:Label ID="Label5" runat="server" Font-Size="Smaller" Text="To"></asp:Label></td>
                <td style="width: 149px; height: 21px">
                    <asp:Label ID="StatusTo" runat="server" Font-Size="Smaller" Text="Label"></asp:Label></td>
                <td style="width: 100px; height: 21px">
                </td>
            </tr>
            <tr>
                <td style="width: 100px; height: 21px">
                </td>
                <td style="width: 100px; height: 21px">
                </td>
                <td style="width: 111px; height: 21px">
                    <asp:Label ID="Label4" runat="server" Font-Size="Smaller" Text="CR ID"></asp:Label></td>
                <td style="width: 45px; height: 21px">
                    <asp:Label ID="Label6" runat="server" Font-Size="Smaller" Text="From"></asp:Label></td>
                <td style="width: 131px; height: 21px">
                    <asp:Label ID="CRFrom" runat="server" Font-Size="Smaller" Text="Label"></asp:Label></td>
                <td style="width: 56px; height: 21px">
                    <asp:Label ID="Label7" runat="server" Font-Size="Smaller" Text="To"></asp:Label></td>
                <td style="width: 149px; height: 21px">
                    <asp:Label ID="CRTo" runat="server" Font-Size="Smaller" Text="Label"></asp:Label></td>
                <td style="width: 100px; height: 21px">
                </td>
            </tr>
        </table>
        <br />
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False">
            <Columns>
                <asp:TemplateField HeaderText="No">
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" />
                    <ItemStyle Font-Size="Smaller" Width="20px" />
                    <ItemTemplate>
                        <%# Container.DataItemIndex + 1 %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="FAPT_TRNTY" HeaderText="Job Service Type" ReadOnly="True">
                    <ItemStyle Font-Size="Smaller" Width="45px" />
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="FAPT_TRNNO" HeaderText="Service Order No" ReadOnly="True">
                    <ItemStyle Font-Size="Smaller" Width="45px" />
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="FAPT_IsInBound" HeaderText="Inbound/ Outbound" ReadOnly="True">
                    <ItemStyle Font-Size="Smaller" Width="55px" />
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="FAPT_CREDT" HeaderText="Create Date" ReadOnly="True">
                    <ItemStyle Font-Size="Smaller" Width="70px" />
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="FAPT_TIME" HeaderText="Create Time" ReadOnly="True">
                    <ItemStyle Font-Size="Smaller" Width="40px" />
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="FAPT_APTDT" HeaderText="Appointment Date" ReadOnly="True">
                    <ItemStyle Font-Size="Smaller" Width="50px" />
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="FAPT_CUSID" HeaderText="Cust ID" ReadOnly="True">
                    <ItemStyle Font-Size="Smaller" Width="50px" />
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="MCUS_ENAME" HeaderText="Cust Name" ReadOnly="True">
                    <ItemStyle Font-Size="Smaller" Width="150px" />
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="MROU_MODID" HeaderText="Model" ReadOnly="True">
                    <ItemStyle Font-Size="Smaller" Width="70px" />
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="MROU_SERNO" HeaderText="Serial Number" ReadOnly="True">
                    <ItemStyle Font-Size="Smaller" Width="70px" />
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="FAPT_SVCID" HeaderText="Service Center ID" ReadOnly="True">
                    <ItemStyle Font-Size="Smaller" Width="45px" />
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="FAPT_ZONID" HeaderText="Area Name" ReadOnly="True">
                    <ItemStyle Font-Size="Smaller" Width="40px" />
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="FAPT_APTTY" HeaderText="Service Type" ReadOnly="True">
                    <ItemStyle Font-Size="Smaller" Width="45px" />
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="FAPT_STAT" HeaderText="Appointment Status" ReadOnly="True">
                    <ItemStyle Font-Size="Smaller" Width="50px" />
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="MUSR_ENAME" HeaderText="CR Name" ReadOnly="True">
                    <ItemStyle Font-Size="Smaller" Width="120px" />
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="MTCH_ENAME" HeaderText="Technician Name" ReadOnly="True">
                    <ItemStyle Font-Size="Smaller" Width="120px" />
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="FAPT_REM" HeaderText="Appointment Remarks" ReadOnly="True">
                    <ItemStyle Font-Size="Smaller" Width="200px" />
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" />
                </asp:BoundField>
            </Columns>
            <EditRowStyle Font-Size="Smaller" />
        </asp:GridView>
    
    </div>
    </form>
</body>
</html>
