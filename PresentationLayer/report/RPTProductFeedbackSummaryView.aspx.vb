Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Web
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web
Imports BusinessEntity
Imports System.Data

Partial Class PresentationLayer_report_RPTProductFeedbackSummaryView
    Inherits System.Web.UI.Page
    Dim clsReport As New ClsCommonReport
    Dim objXML As New clsXml
    Dim clsPFS As New clsRPTProductFeedbackSummary()
    Dim callListReportDoc As New ReportDocument()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try

        Me.titleLab.Text = "Product Feedback Summary Report"
        clsPFS = Session("rpt_ProductFeedbackSummary")

        Dim dsReport As DataSet = clsPFS.RetriveProductFeedbackSummary()
        If dsReport Is Nothing Then
            objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
            ErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_FUN_CALLLIST_RERR")
            ErrLab.Visible = True
            Return
        End If
        If dsReport.Tables.Count = 0 Then
            objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
            ErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_FUN_CALLLIST_RERR")
            ErrLab.Visible = True
            Return
        End If

        Try
            callListReportDoc.Load(MapPath("RPTProductFeedbackSummary.rpt"))
            ErrLab.Visible = False
        Catch ex As Exception
            objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
            ErrLab.Text = ex.InnerException.Message.ToString
            ErrLab.Visible = True
            Return
        End Try

        Try
            callListReportDoc.SetDataSource(dsReport.Tables(0))
        Catch ex As Exception
            ErrLab.Text = "The error returned was " & ex.Message.ToString
        End Try

        objXML.XmlFile = ConfigurationManager.AppSettings("XmlFilePath")
        callListReportDoc.SetParameterValue("CompanyName", objXML.GetLabelName("EngLabelMsg", "BB_REPORT_COMPANYNAME"))
        callListReportDoc.SetParameterValue("ReportTitle", "Product Feedback Summary Reports")
        callListReportDoc.SetParameterValue("UserId", objXML.GetLabelName("EngLabelMsg", "BB-RPTCUSR-USERID") & " " & Session("userID"))
        callListReportDoc.SetParameterValue("ServiceBillDateFr", clsPFS.FB_ServBillDateFr.ToString("dd/MM/yyyy"))
        callListReportDoc.SetParameterValue("ServiceBillDateTo", clsPFS.FB_ServBillDateTo.ToString("dd/MM/yyyy"))
        callListReportDoc.SetParameterValue("ServiceCenterFr", clsPFS.FB_ServCtrFr)
        callListReportDoc.SetParameterValue("ServiceCenterTo", clsPFS.FB_ServCtrTo)
        callListReportDoc.SetParameterValue("PriceId", clsPFS.FB_PriceId)
        callListReportDoc.SetParameterValue("ModelId", clsPFS.FB_ModelId)
        callListReportDoc.SetParameterValue("Status", clsPFS.FB_Status)
        callListReportDoc.SetParameterValue("IsoDesc1", clsPFS.IsoDesc1)
        callListReportDoc.SetParameterValue("IsoDesc2", clsPFS.IsoDesc2)
        callListReportDoc.SetParameterValue("IsoDesc3", clsPFS.Col1)

        CrystalReportViewer.HasCrystalLogo = False
        CrystalReportViewer.ReportSource = callListReportDoc
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        clsReport.UnloadReport()
        CrystalReportViewer.Dispose()
    End Sub
End Class
