Imports BusinessEntity
Imports System.Data
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Windows
Imports Microsoft.Win32

Partial Class PresentationLayer_report_ContractUnitEntryReport
    Inherits System.Web.UI.Page
    Dim objXmlTr As New clsXml
    Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)
        Return connection
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.MaintainScrollPositionOnPostBack = True
        If Not Page.IsPostBack Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try

            Dim UserID As String = Session("userID")
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle


            Dim CommonClass As New clsCommonClass()
            If Session("login_rank") <> 0 Then
                CommonClass.spctr = Session("login_ctryID").ToString().ToUpper
            End If
            CommonClass.rank = Session("login_rank")

            Dim userIDNamestr As String = Session("userID").ToString().ToUpper + "-" + Session("username").ToString.ToUpper
            Dim rank As String = Session("login_rank")
            CommonClass.spstat = Session("login_cmpID")
            CommonClass.sparea = Session("login_svcID")

            If rank <> 0 Then
                CommonClass.spctr = Session("login_ctryID").ToString().ToUpper
            End If
            CommonClass.rank = rank
            CommonClass.spstat = "ACTIVE"

            lblTotalDescription.Visible = False
            BindData()
            BindGrid(UserID)

        ElseIf appsView.Rows.Count > 0 Then
            DispGridViewHead()
        End If
        HypCal.NavigateUrl = "javascript:DoCal(document.form1.txtPreInsDateFr);"
        HypCal2.NavigateUrl = "javascript:DoCal(document.form1.txtPreInsDateTo);"

        searchButton.Enabled = True
        searchButton.Visible = True
    End Sub
    Protected Sub BindGrid(ByVal strUserID As String)
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")

        appsView.AllowPaging = True
        appsView.AllowSorting = True

        lblNoRecord.Visible = False

        'access control'''''''''''''''''''''''''''''''''''''''''''''''
        Dim accessgroup As String = Session("accessgroup").ToString
        Dim purviewArray As Array = New Boolean() {False, False, False, False}
        purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "22")

        If txtPreInsDateFr.Text = "" Then
            txtPreInsDateFr.Text = System.DateTime.Today
        End If

        If txtPreInsDateTo.Text = "" Then
            txtPreInsDateTo.Text = System.DateTime.Today
        End If
    End Sub
    Private Sub DispGridViewHead()
        appsView.HeaderRow.Cells(0).Text = "CustomerID"
        appsView.HeaderRow.Cells(0).Font.Size = 8
        appsView.HeaderRow.Cells(1).Text = "CustomerName"
        appsView.HeaderRow.Cells(1).Font.Size = 8
        appsView.HeaderRow.Cells(2).Text = "ModelID"
        appsView.HeaderRow.Cells(2).Font.Size = 8
        appsView.HeaderRow.Cells(3).Text = "SerialNum"
        appsView.HeaderRow.Cells(3).Font.Size = 8
        appsView.HeaderRow.Cells(4).Text = "ContractStartDate"
        appsView.HeaderRow.Cells(4).Font.Size = 8
        appsView.HeaderRow.Cells(5).Text = "ContractNo"
        appsView.HeaderRow.Cells(5).Font.Size = 8
        appsView.HeaderRow.Cells(6).Text = "ServiceCenter"
        appsView.HeaderRow.Cells(6).Font.Size = 8
        appsView.HeaderRow.Cells(7).Text = "Technician"
        appsView.HeaderRow.Cells(7).Font.Size = 8
    End Sub

    Protected Sub searchButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles searchButton.Click
        Dim UserID As String = Session("userID")
        Dim objCommonTel As New clsCommonClass
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        txtPreInsDateFr.Text = Request.Form("txtPreInsDateFr")
        txtPreInsDateTo.Text = Request.Form("txtPreInsDateTo")
        'search condition

        Dim Conn As New SqlConnection
        Conn.ConnectionString = ConfigurationManager.AppSettings("ConnectionString").ToString

        Using Comm As New SqlClient.SqlCommand("" _
            & "exec dbo.BB_RPTContractUnitEntryReport '" + Trim(ddlType.SelectedValue) + "','" + Trim(ddlTypeto.SelectedValue) + "','" + txtPreInsDateFr.Text + "','" + txtPreInsDateTo.Text + "','" + Left(ddlSVCFr.SelectedValue, 3) + "','" + Left(ddlSVCTo.SelectedValue, 3) + "','" + ddlcusttype.SelectedValue + "'", Conn)
            Conn.Open()

            Try
                Using sda As New SqlDataAdapter
                    sda.SelectCommand = Comm

                    If sda Is Nothing Then
                        appsView.Visible = False
                        lblNoRecord.Visible = True
                        Return
                    Else
                        Using selds As New DataSet
                            sda.Fill(selds)
                            appsView.Visible = True
                            appsView.DataSource = selds
                            appsView.DataBind()
                            DispGridViewHead()
                            If selds.Tables(0).Rows.Count > 0 Then
                                lblNoRecord.Visible = False
                                DispGridViewHead()
                                LblTotRecNo.Text = selds.Tables(0).Rows.Count
                            Else
                                LblTotRecNo.Text = "0"
                            End If
                            Session("selds") = selds
                        End Using
                    End If
                End Using
            Catch ex As Exception
                Return
            End Try
        End Using
        Conn.Close()
    End Sub
    Protected Sub appsView_PageIndexChanging(ByVal sender As Object, ByVal e As GridViewPageEventArgs) Handles appsView.PageIndexChanging
        appsView.PageIndex = e.NewPageIndex
        Threading.Thread.CurrentThread.CurrentCulture = datestyle

        If Session("selds") IsNot Nothing Then
            Dim ds As DataSet = CType(Session("selds"), DataSet)
            'Dim ds As DataSet = Session("appsView")
            appsView.DataSource = ds
            appsView.DataBind()
            If ds.Tables(0).Rows.Count > 0 Then
                DispGridViewHead()
            End If
            txtPreInsDateFr.Text = Request.Form("apptStartDateBox")
            txtPreInsDateTo.Text = Request.Form("apptEndDateBox")
        End If

    End Sub

    Protected Sub appsView_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles appsView.RowEditing

        'Dim transtype As String = appsView.Rows(e.NewEditIndex).Cells(1).Text.Trim()
        'transtype = Server.UrlEncode(transtype)
        'Dim transno As String = appsView.Rows(e.NewEditIndex).Cells(2).Text.Trim()
        'transno = Server.UrlEncode(transno)
        'Dim strTempURL As String = "transtype=" + transtype + "&transno=" + transno
        'strTempURL = "~/PresentationLayer/function/modifyappointment.aspx?" + strTempURL
        'Response.Redirect(strTempURL)
    End Sub

    Protected Sub appsView_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles appsView.SelectedIndexChanged

    End Sub

    Private Sub BindData()
        Dim Conn As New SqlConnection
        Conn.ConnectionString = ConfigurationManager.AppSettings("ConnectionString").ToString

        If ddlSVCFr.SelectedValue = "" Then
            Dim Comm2 As SqlCommand
            Comm2 = Conn.CreateCommand
            Comm2.CommandText = "Select [ServiceCenter] = MSVC_SVCID + '-' + MSVC_ENAME from dbo.MSVC_FIL where MSVC_CTRID = '" + Session("login_ctryID") + "' and MSVC_SVCID <> 'zzz' and MSVC_STAT = 'ACTIVE' order by MSVC_SVCID"
            Conn.Open()

            Dim dr2 As SqlDataReader = Comm2.ExecuteReader(CommandBehavior.CloseConnection)

            Dim dt2 As DataTable = New DataTable()
            dt2.Load(dr2)

            Dim counter2 As Integer

            For counter2 = 0 To dt2.Rows.Count - 1
                ddlSVCFr.Items.Add(dt2.Rows(counter2).Item("ServiceCenter").ToString)
                ddlSVCTo.Items.Add(dt2.Rows(counter2).Item("ServiceCenter").ToString)
            Next
            Conn.Close()

            ddlSVCFr.SelectedIndex = 0
            ddlSVCTo.SelectedIndex = ddlSVCTo.Items.Count - 1
        End If

        If ddlType.SelectedValue = "" Then
            Dim Comm2 As SqlCommand
            Comm2 = Conn.CreateCommand
            Comm2.CommandText = "Select [ContractType] = MSV1_SVTID from MSV1_FIL where MSV1_SVTY = 'CONTRACT' and MSV1_CTRID = '" + Session("login_ctryID") + "' and MSV1_STAT = 'ACTIVE' order by MSV1_SVTID"

            Conn.Open()

            Dim dr2 As SqlDataReader = Comm2.ExecuteReader(CommandBehavior.CloseConnection)

            Dim dt2 As DataTable = New DataTable()
            dt2.Load(dr2)

            Dim counter2 As Integer

            For counter2 = 0 To dt2.Rows.Count - 1
                ddlType.Items.Add(dt2.Rows(counter2).Item("ContractType").ToString)
            Next
            Conn.Close()

            ddlType.SelectedIndex = 0
        End If

        If ddlTypeto.SelectedValue = "" Then
            Dim Comm2 As SqlCommand
            Comm2 = Conn.CreateCommand
            Comm2.CommandText = "Select [ContractType] = MSV1_SVTID from MSV1_FIL where MSV1_SVTY = 'CONTRACT' and MSV1_CTRID = '" + Session("login_ctryID") + "' and MSV1_STAT = 'ACTIVE' order by MSV1_SVTID"

            Conn.Open()

            Dim dr2 As SqlDataReader = Comm2.ExecuteReader(CommandBehavior.CloseConnection)

            Dim dt2 As DataTable = New DataTable()
            dt2.Load(dr2)

            Dim counter2 As Integer

            For counter2 = 0 To dt2.Rows.Count - 1
                ddlTypeto.Items.Add(dt2.Rows(counter2).Item("ContractType").ToString)
            Next
            Conn.Close()

            ddlTypeto.SelectedIndex = ddlTypeto.Items.Count - 1
        End If

        Dim custtype As New clsrlconfirminf()
        Dim custParam As ArrayList = New ArrayList
        custParam = custtype.searchconfirminf("CUSTTYPE")
        Dim custcount As Integer
        Dim custid As String
        Dim custnm As String
        Me.ddlcusttype.Items.Add("ALL")
        For custcount = 0 To custParam.Count - 1
            custid = custParam.Item(custcount)
            custnm = custParam.Item(custcount + 1)
            custcount = custcount + 1
            Me.ddlcusttype.Items.Add(New ListItem(custnm.ToString(), custid.ToString()))
        Next

    End Sub

    Protected Sub ddlSVCFr_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSVCFr.SelectedIndexChanged
        BindData()
    End Sub

    Protected Sub ddlSVCTo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSVCTo.SelectedIndexChanged
        BindData()
    End Sub

    
    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub
    Protected Sub BtnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnExport.Click
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=GridView1.xls")
        Response.Charset = ""
        Response.ContentType = "application/vnd.xls"
        Dim StringWriter As IO.StringWriter = New System.IO.StringWriter()
        Dim HtmlTextWriter As New HtmlTextWriter(StringWriter)
        Dim style As String = "<style> td { mso-number-format:\@; } </style> "

        appsView.AllowPaging = False
        appsView.DataSource = Session("selds")
        appsView.DataBind()

        appsView.RenderControl(HtmlTextWriter)
        Response.Write(style) 'style is added dynamically
        Response.Write(StringWriter.ToString())
        Response.[End]()
    End Sub
End Class
