<%@ Page Language="VB" AutoEventWireup="false" CodeFile="RPTSOSA.aspx.vb" Inherits="PresentationLayer_Report_RPTSOSA" %>

<%@ Register Assembly="JCalendar" Namespace="JCalendar" TagPrefix="cc1" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Appointment History</title>
    <link href="../css/style.css" rel="stylesheet" type="text/css" />
     <STYLE type="text/css">A:link { COLOR: #336666; TEXT-DECORATION: none }
	BODY { FONT-SIZE: 9pt; FONT-FAMILY: "Arial", "Verdana", "Tahoma"; BACKGROUND-COLOR: #ffffff }
	TD { FONT-SIZE: 9pt; FONT-FAMILY: Arial,Verdana,Tahoma }
	</STYLE>
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
        <script language="JavaScript" src="../js/common.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div>
            <table id="countrytab" border="0" style="width: 100%">
                <tr>
                    <td style="width: 0px">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td background="../graph/title_bg.gif" width="1%">
                                    <img height="24" src="../graph/title1.gif" width="5" /></td>
                                <td background="../graph/title_bg.gif" class="style2" width="98%">
                                    <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" background="../graph/title_bg.gif" width="1%">
                                    <img height="24" src="../graph/title_2.gif" width="5" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="width: 0px">
                        <table id="country" bgcolor="#b7e6e6" border="0" cellpadding="2" cellspacing="1" style="width: 100%">
                            <tr bgcolor="#ffffff">
                                <td align="left" style="width: 18%">
                                    <asp:Label ID="ServiceCenterlab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 6%">
                                    &nbsp;<asp:Label ID="Fromlab1" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 34%">
                                    <asp:TextBox ID="minsrevice" runat="server" CssClass="textborder"></asp:TextBox></td>
                                <td align="left" style="width: 11%">
                                    <asp:Label ID="Tolab1" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" width="35%">
                                    <asp:TextBox ID="maxsrevice" runat="server" CssClass="textborder"></asp:TextBox></td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="left" style="width: 18%">
                                    <asp:Label ID="Customeridlab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 6%">
                                    <asp:Label ID="Fromlab2" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 34%">
                                    <asp:TextBox ID="minCustomerid" runat="server" CssClass="textborder"></asp:TextBox></td>
                                <td align="left" style="width: 11%">
                                    <asp:Label ID="Tolab2" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" width="35%">
                                    <asp:TextBox ID="maxCustomerid" runat="server" CssClass="textborder"></asp:TextBox></td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="left" style="width: 18%">
                                    <asp:Label ID="datelab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 6%">
                                    <asp:Label ID="Fromlab4" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 34%">
                                    <asp:TextBox ID="mindate" runat="server" CssClass="textborder"></asp:TextBox>
                                    <asp:HyperLink ID="HypCalFrom" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif" ToolTip="Choose a Date">Choose a Date</asp:HyperLink>

                                    
                                    <cc1:JCalendar ID="JCalendar1" runat="server" ControlToAssign="mindate" ImgURL="~/PresentationLayer/graph/calendar.gif"  Visible =false />
                                </td>
                                <td align="left" style="width: 11%">
                                    <asp:Label ID="Tolab4" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" width="35%">
                                    <asp:TextBox ID="maxdate" runat="server" CssClass="textborder"></asp:TextBox>&nbsp;
                                    
                                    <asp:HyperLink ID="HypCalTo" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif" ToolTip="Choose a Date">Choose a Date</asp:HyperLink>

                                    <cc1:JCalendar
                                        ID="JCalendar2" runat="server" ControlToAssign="maxdate" ImgURL="~/PresentationLayer/graph/calendar.gif" Visible =false  />
                                    &nbsp;&nbsp;
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="left" style="width: 18%">
                                    <asp:Label ID="Apptnolab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 6%">
                                    <asp:Label ID="Fromlab5" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 34%">
                                    <asp:TextBox ID="minApptno" runat="server" CssClass="textborder"></asp:TextBox>
                                    &nbsp;
                                </td>
                                <td align="left" style="width: 11%">
                                    <asp:Label ID="Tolab5" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" width="35%">
                                    <asp:TextBox ID="maxApptno" runat="server" CssClass="textborder"></asp:TextBox>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="left" style="width: 18%">
                                    <asp:Label ID="Modellab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 6%">
                                    <asp:Label ID="Fromlab6" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 34%">
                                    <asp:TextBox ID="minModel" runat="server" CssClass="textborder"></asp:TextBox></td>
                                <td align="left" style="width: 11%">
                                    <asp:Label ID="Tolab6" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" width="35%">
                                    <asp:TextBox ID="maxModel" runat="server" CssClass="textborder"></asp:TextBox></td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="left" style="width: 18%">
                                    <asp:Label ID="ApptStatuslab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" colspan="2">
                                    <asp:DropDownList ID="ApptStatus" runat="server" Width="128px">
                                    </asp:DropDownList></td>
                                <td align="left" style="width: 11%">
                                    <asp:Label ID="ApptTypelab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" width="35%">
                                    <asp:DropDownList ID="ApptType" runat="server" Width="160px">
                                    </asp:DropDownList></td>
                            </tr>
                            
                            <tr bgcolor="#ffffff">
                                <td align="left" style="width: 18%">
                                    <asp:Label ID="lblTransactionType" runat="server" Text="Transaction Type"></asp:Label></td>
                                <td align="left" colspan="2">
                                    <asp:DropDownList ID="cboTransactionType" runat="server" Width="128px">
                                    </asp:DropDownList></td>
                                <td align="left" style="width: 11%">
                                   
                                    </td>
                                <td align="left" width="35%">
                                    
                                    </td>
                            </tr>
                            
                            
                        </table>
                        <asp:LinkButton ID="reportviewer" runat="server">LinkButton</asp:LinkButton>&nbsp;
                    </td>
                </tr>
            </table>
        </div>
    
    </div>
    </form>
</body>
</html>
