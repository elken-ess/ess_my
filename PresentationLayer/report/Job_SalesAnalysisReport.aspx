<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Job_SalesAnalysisReport.aspx.vb" Inherits="PresentationLayer_function_Job_SalesAnalysisReport" %>

<%@ Register Assembly="JCalendar" Namespace="JCalendar" TagPrefix="cc1" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Calculate Incentive</title>
    <link href="../css/style.css" type="text/css" rel="stylesheet"/>
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <script language="JavaScript" src="../js/common.js">function HR1_onclick() {

}

</script>    

    <link href="/aspnet_client/system_web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
<table id="uagTab" border="0" width="100%">
            <tr>
                <td>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td background="../graph/title_bg.gif" style="width: 1%; height: 24px;">
                        <img height="24" src="../graph/title1.gif" width="5" /></td>
                    <td background="../graph/title_bg.gif" class="style2" width="98%" style="height: 24px">
                        <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
                    <td align="right" background="../graph/title_bg.gif" width="1%" style="height: 24px">
                        <img height="24" src="../graph/title_2.gif" width="5" /></td>
                </tr>                        
                </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" class="style2" width="98%">
                                <font color="red">
                                    <asp:Label ID="errlab" runat="server" Text="Label" Visible="False"></asp:Label></font></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>   
        <table cellSpacing="1" cellPadding="2" bgColor="#b7e6e6" border="0">
            <tr bgColor=white >
                <td style="width: 100px; height: 28px;">
                    <asp:Label ID="FrDate" runat="server" Width="108px" ></asp:Label></td>
                <td style="width: 120px; height: 28px;">
                    <asp:TextBox ID="FrDateBox" runat="server" Width="91px" ReadOnly="false" MaxLength =10></asp:TextBox>            
                    <%-- <asp:ImageButton ID="DisplayFrDate" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif" CausesValidation="False" />
                        <asp:Calendar ID="CalFrDate" runat="server" Visible="False" BackColor="White" BorderColor="#3366CC" BorderWidth="1px" CellPadding="1" DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="8pt" ForeColor="#003399" Height="56px" Width="120px" style="position: absolute;">
                                                    <SelectedDayStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                                                    <TodayDayStyle BackColor="#99CCCC" ForeColor="White" />
                                                    <SelectorStyle BackColor="#99CCCC" ForeColor="#336666" />
                                                    <WeekendDayStyle BackColor="#CCCCFF" />
                                                    <OtherMonthDayStyle ForeColor="#999999" />
                                                    <NextPrevStyle Font-Size="8pt" ForeColor="#CCCCFF" />
                                                    <DayHeaderStyle BackColor="#99CCCC" ForeColor="#336666" Height="1px" />
                                                    <TitleStyle BackColor="#003399" BorderColor="#3366CC" BorderWidth="1px" Font-Bold="True"
                                                        Font-Size="10pt" ForeColor="#CCCCFF" Height="25px" />
                        </asp:Calendar> --%>
                        <cc1:JCalendar ID="FrDateCalendar" runat="server" ImgURL="~/PresentationLayer/graph/calendar.gif" ControlToAssign="FrDateBox" Visible="False" />  
                    <asp:HyperLink ID="HypCal" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                        ToolTip="Choose a Date" Enabled=true>Choose a Date</asp:HyperLink>
                    </td>
                <td style="width: 100px; height: 28px;">
                    <asp:Label ID="ToDate" runat="server"></asp:Label></td>
                <td style="width: 120px; height: 28px;">
                    <asp:TextBox ID="ToDateBox" runat="server" Width="91px" ReadOnly="false" MaxLength =10></asp:TextBox>         
                    <%-- <asp:ImageButton ID="DisplayToDate" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif" CausesValidation="False" />
                        <asp:Calendar ID="CalToDate" runat="server" Visible="False" BackColor="White" BorderColor="#3366CC" BorderWidth="1px" CellPadding="1" DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="8pt" ForeColor="#003399" Height="56px" Width="120px" style="position: offset;">
                            <SelectedDayStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                            <TodayDayStyle BackColor="#99CCCC" ForeColor="White" />
                            <SelectorStyle BackColor="#99CCCC" ForeColor="#336666" />
                            <WeekendDayStyle BackColor="#CCCCFF" />
                            <OtherMonthDayStyle ForeColor="#999999" />
                            <NextPrevStyle Font-Size="8pt" ForeColor="#CCCCFF" />
                            <DayHeaderStyle BackColor="#99CCCC" ForeColor="#336666" Height="1px" />
                            <TitleStyle BackColor="#003399" BorderColor="#3366CC" BorderWidth="1px" Font-Bold="True"
                                Font-Size="10pt" ForeColor="#CCCCFF" Height="25px" />
                        </asp:Calendar>--%>
                        <cc1:JCalendar ID="ToDateCalendar" runat="server" ImgURL="~/PresentationLayer/graph/calendar.gif" ControlToAssign="ToDateBox" Visible="False" />  
                    <asp:HyperLink ID="HypCal2" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                        ToolTip="Choose a Date" Enabled="true" >Choose a Date</asp:HyperLink>
                    </td>
                <td style="width: 100px; height: 28px;">
                    <asp:Label ID="StaffTy" runat="server"></asp:Label></td>
                <td style="width: 191px; height: 28px;">
                    <asp:DropDownList ID="StaffTyDrop" runat="server" Width="120px" AutoPostBack="True" OnSelectedIndexChanged="StaffTyDrop_SelectedIndexChanged">
                    </asp:DropDownList></td>
                <td style="width: 10%" rowspan="3">
                </td>
            </tr>
            <tr bgColor=white >
                <td style="width: 14%; height: 21px">
                    <asp:Label ID="lblSearchStaff" runat="server" Text="Search Staff ID"/>
                    </td>
                <td style="height: 21px" colspan="2">
                    <asp:TextBox ID="txtSearchStaff" runat="server" />&nbsp; 
                    <asp:LinkButton ID="lnkSearchStaff" runat="server" OnClick="lnkSearchStaff_Click"/>
                </td>
                <td style="width: 17%; height: 21px">
                    &nbsp;</td>
                <td style="width: 14%; height: 21px">
                    Status</td>
                <td style="width: 191px; height: 21px">
                    <asp:DropDownList ID="ddlstaffstat" runat="server" AutoPostBack="True">
                        <asp:ListItem Selected="True">ALL</asp:ListItem>
                        <asp:ListItem>ACTIVE</asp:ListItem>
                        <asp:ListItem>DELETE</asp:ListItem>
                        <asp:ListItem>RESIGNED</asp:ListItem>
                        <asp:ListItem>SUSPEND</asp:ListItem>
                        <asp:ListItem>TERMINATE</asp:ListItem>
                    </asp:DropDownList></td>
            </tr>
            <tr bgColor=white >
                <td style="width: 14%; height: 21px">
                    </td>
                <td style="width: 17%; height: 21px">
                    <table style="width: 100%">
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="lblAllStaffs"/>
                            </td>
                        </tr>
                        <tr>
                           <td>
                               <asp:ListBox ID="lstAllStaffs" runat="server" Rows="10" Width="100%" SelectionMode="Multiple"/>
                           </td> 
                        </tr>
                    </table>
                </td>
                <td style="width: 14%; height: 21px; text-align: center; vertical-align: middle;">
                    <table>
                        <tr>
                            <td>
                                <asp:Button ID="btnAddOne" runat="server" Text=">" Width="50px" 
                                    OnClick="btnAddOne_Click"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button ID="btnAddAll" runat="server" Text=">>" Width="50px" 
                                    OnClick="btnAddAll_Click"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button ID="btnRemoveOne" runat="server" Text="<" Width="50px" 
                                    OnClick="btnRemoveOne_Click"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button ID="btnRemoveAll" runat="server" Text="<<" Width="50px" 
                                    OnClick="btnRemoveAll_Click"/>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="width: 17%; height: 21px">
                    <table style="width: 100%">
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="lblSelectedStaffs"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:ListBox ID="lstSelectedStaffs" runat="server" Rows="10" Width="100%" SelectionMode="Multiple"/>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="width: 14%; height: 21px">
                    <asp:Label ID="SerStats" runat="server"></asp:Label>
                </td>
                <td style="width: 191px; height: 21px">
                    <asp:DropDownList ID="SerStatsDrop" runat="server" Width="120px">
                    </asp:DropDownList></td>
            </tr>
            <tr bgColor=white >
                <td style="width: 14%; height: 25px">
                    <asp:Label ID="FrSerCent" runat="server" Width="107px"></asp:Label></td>
                <td style="width: 17%; height: 25px">
                    <asp:TextBox ID="FrSerCentBox" runat="server" MaxLength="10"></asp:TextBox></td>
                <td style="width: 14%; height: 25px">
                    <asp:Label ID="ToSerCent" runat="server"></asp:Label></td>
                <td style="width: 17%; height: 25px">
                    <asp:TextBox ID="ToSerCentBox" runat="server" MaxLength="10"></asp:TextBox></td>
                <td style="width: 14%; height: 25px">
                    <asp:Label ID="ServiceTy" runat="server" Visible="False"></asp:Label></td>
                <td style="width: 191px; height: 25px">
                    <asp:DropDownList ID="ServiceTyDrop" runat="server" Width="120px" Visible="False">
                    </asp:DropDownList></td>
                <td style="width: 10%; height: 25px">
                    &nbsp; &nbsp;&nbsp;
                    <asp:LinkButton ID="BtnRetrieve" runat="server"></asp:LinkButton></td>
            </tr>
        </table>
        <hr />
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
        <asp:Label ID="TableHead" runat="server" Visible="False"></asp:Label>
        <asp:Label ID="DateRange" runat="server" Visible="False"></asp:Label>
        <asp:GridView ID="IncentInfoView" runat="server" Width="100%" Visible="False">
        </asp:GridView>
        <hr id="HR1" onclick="return HR1_onclick()" />
        <table cellSpacing="1" cellPadding="2" bgColor="#b7e6e6" border="0" style="width: 100%">
            <tr bgColor=white>
                <td rowspan="2" style="width: 80%">
                    <asp:Label ID="Formula" runat="server" Width="80%"></asp:Label>&nbsp;
                </td>
                <td style="width: 20%; height: 21px">
                    &nbsp; &nbsp;&nbsp;
                    <asp:LinkButton ID="btnPrint" runat="server" Width="20%"></asp:LinkButton></td>
            </tr>
            <tr bgColor=white>
                <td style="width: 20%">
                    &nbsp; &nbsp;&nbsp;
                    <asp:LinkButton ID="btnExport" runat="server" Width="20%"></asp:LinkButton></td>
            </tr>
        </table>
        <CR:CrystalReportViewer ID="IncentiveReportViewer" runat="server" AutoDataBind="true" DisplayGroupTree="False" />
        &nbsp;
    </form>
</body>
</html>
