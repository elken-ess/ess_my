Imports SQLDataAccess
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
Imports BusinessEntity
Imports CrystalDecisions.Web
Imports CrystalDecisions.CrystalReports.Engine
Partial Class PresentationLayer_report_RPTSOSAVIEW
    Inherits System.Web.UI.Page
    Dim clsReport As New ClsCommonReport

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Response.Redirect("~/PresentationLayer/logon.aspx")
            End If
        Catch ex As Exception
            Response.Redirect("~/PresentationLayer/logon.aspx")
        End Try
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        Dim obiXML As New clsXml
        obiXML.XmlFile = ConfigurationManager.AppSettings("XmlFilePath")
        Me.titleLab.Text = obiXML.GetLabelName("EngLabelMsg", "BB-RPTSOSA-TITLE2")


        Dim clsrptsosa As New ClsRptSOSA
        clsrptsosa.userid = Session("userID")
        clsrptsosa.username = Session("username")

        clsrptsosa = Session("rptsosa_search_condition")

        Dim ds As DataSet = Nothing
        ds = clsrptsosa.GetRptsosa()



        Dim statXmlTr As New clsXml
        statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")


        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Dim objXmlTr As New clsXml
        Dim strHead As Array = Array.CreateInstance(GetType(String), 50)
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")

        strHead(0) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDAR-NUMB")
        strHead(1) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSOSA-APPTNO")
        strHead(2) = objXmlTr.GetLabelName("EngLabelMsg", "BB-ENTRYDATE")
        strHead(3) = objXmlTr.GetLabelName("EngLabelMsg", "BB-TECHID")

        strHead(4) = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRID")
        strHead(5) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSOSA-APPTSTAT")
        strHead(6) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSOSA-APPTDATE")
        strHead(7) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSOSA-REMARKS")
        strHead(8) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDAR-TITLE1")
        strHead(9) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSOSA-APPHIS")
        strHead(10) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSOSA-TITLE2")
        strHead(11) = clsrptsosa.Mindate
        strHead(12) = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
        strHead(13) = clsrptsosa.Maxdate
        strHead(14) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSOSA-ID")
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        strHead(15) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDAR-SVC")
        strHead(16) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSOSA-CUSTID")
        strHead(17) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSOSA-APPTNO")
        strHead(18) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDAR-DATE")
        strHead(19) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSOSA-MODEL")
        strHead(20) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSOSA-APPTTYPE")

        strHead(21) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSOSA-APPTSTAT")

        strHead(22) = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")

        strHead(23) = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
       
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        If clsrptsosa.Minsvcid <> "" Then
            strHead(24) = clsrptsosa.Minsvcid
        Else
            strHead(24) = "AAA".ToString()
        End If
        If clsrptsosa.Maxsvcid <> "ZZZZZZZZZZ" Then
            strHead(25) = clsrptsosa.Maxsvcid
        Else
            strHead(25) = "ZZZ".ToString()
        End If
        If clsrptsosa.Mincustid <> "" Then
            strHead(26) = clsrptsosa.Mincustid
        Else
            strHead(26) = "AAA".ToString()
        End If
        If clsrptsosa.Maxcustid <> "ZZZZZZZZZZZZZZZZZZZZ" Then
            strHead(27) = clsrptsosa.Maxcustid
        Else
            strHead(27) = "ZZZ".ToString()
        End If
        If clsrptsosa.Minapptno <> "" Then
            strHead(28) = clsrptsosa.Minapptno
        Else
            strHead(28) = "AAA".ToString()
        End If
        If clsrptsosa.Maxapptno <> "ZZZZZZZZZZ" Then
            strHead(29) = clsrptsosa.Maxapptno
        Else
            strHead(29) = "ZZZ".ToString()
        End If
        If clsrptsosa.Mindate <> "" Then
            strHead(30) = clsrptsosa.Mindate
        Else
            strHead(30) = " ".ToString()
        End If

        If clsrptsosa.Maxdate <> "" Then
            strHead(31) = clsrptsosa.Maxdate
        Else
            strHead(31) = " ".ToString()
        End If

        If clsrptsosa.Minmodel <> "" Then
            strHead(32) = clsrptsosa.Minmodel
        Else
            strHead(32) = "AAA".ToString
        End If
        If clsrptsosa.Maxmodel <> "ZZZZZZZZZZ" Then
            strHead(33) = clsrptsosa.Maxmodel
        Else
            strHead(33) = "ZZZ".ToString()
        End If
        If clsrptsosa.ApptStatustext <> "" Then
            strHead(34) = clsrptsosa.ApptStatustext
        Else
            strHead(34) = "ALL".ToString()
        End If
        If clsrptsosa.ApptTypetext <> "" Then
            strHead(35) = clsrptsosa.ApptTypetext
        Else
            strHead(35) = "ALL".ToString()
        End If
        
        strHead(36) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCUSR-USERID")
        strHead(37) = Session("userID") + "/" + Session("username")

        strHead(38) = ds.Tables(0).Rows.Count
        strHead(39) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_APT_RECORD")
        strHead(40) = "/"
        strHead(41) = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-RESREASON")

        strHead(42) = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-TRANSTYPE")
        strHead(43) = clsrptsosa.TransactionType

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Dim lstrPdfFileName As String = "AppointmentHistory_" & Session("login_session") & ".pdf"

        Try
            With clsReport
                .ReportFileName = "RptSosa.rpt"
                .SetReportDocument(ds, strHead)
                .PdfFileName = lstrPdfFileName
                .ExportPdf()
            End With

            Response.Redirect(clsReport.PdfUrl)

        Catch err As Exception
            'System.IO.File.Delete(lstrPhysicalFile)
            Response.Write("<BR>")
            Response.Write(err.Message.ToString)

        End Try


    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        clsReport.UnloadReport()
        CrystalReportViewer1.Dispose()
    End Sub
End Class
