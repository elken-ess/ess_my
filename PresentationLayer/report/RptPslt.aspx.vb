Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Web
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web
Imports BusinessEntity
Imports System.Data

'*************************************************************************
'Date       Modified By     Remarks
'24/10/2017 Lyann           SMR1710/2254 BB Enhance Product Summary Listing rpt view. For UI screen, hide up State & Appointment Type selection.    
'*************************************************************************

Partial Class PresentationLayer_Report_RptPslt_aspx
    Inherits System.Web.UI.Page
    Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
    Dim lstrDefaultStartDate As String
    Dim lstrDefaultEndDate As String
    Dim lstrDefault6MthAfter As String
    Dim servEntity As New ClsRptPslt()
    Dim objXmlTr As New clsXml


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        AjaxPro.Utility.RegisterTypeForAjax(GetType(PresentationLayer_Report_RptPslt_aspx))
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        ' Modified by Ryan Estandarte 24 Sept 2012
        'lstrDefaultStartDate = System.DateTime.Today
        'lstrDefaultEndDate = DateAdd(DateInterval.Day, 5, System.DateTime.Today)
        'lstrDefaultStartDate = "01/06/2012"
        'lstrDefaultEndDate = "01/01/2013"

        ' Modified by Lyann 02 Nov 2017
        ' to display open date style. Today is StartDate and not limitation to the EndDate 
        lstrDefaultStartDate = System.DateTime.Today
        lstrDefaultEndDate = System.DateTime.Today

        lstrDefault6MthAfter = DateAdd(DateInterval.Month, 5, System.DateTime.Today)
        Dim script As String = "top.location='../logon.aspx';"
        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
                'Response.Redirect("~/PresentationLayer/logon.aspx")
            End If
        Catch ex As Exception
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try


        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")

        ''validator
        servCIDError.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "FUN-APPT-APPTSEVCIDFROM")
        servCIDError2.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "FUN-APPT-APPTSEVCIDTO")

        If Not Page.IsPostBack Then

            ''display label message
            Session("Psltflag") = "0"
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            Statelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTPSLT-0001")
            Sercenterlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTPSLT-0002")
            techlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTPSLT-0003")
            Partcodelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTPSLT-0004")
            applab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTPSLT-0005")
            RadioButtonList1.Items(0).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTPSLT-0006")
            RadioButtonList1.Items(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTPSLT-0007")
            ServBilllab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTPSLT-0008")


            Fromlab1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Fromlab2.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Fromlab3.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Fromlab4.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            datefrom.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Tolab1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Tolab2.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Tolab3.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Tolab4.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            dateto.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")

            reportviewer.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-VIEWREPORT")
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTPSLT-titlelab")

            Dim installbond As New clsCommonClass
            'bond  service type id and name
            Dim sertype As New DataSet
            sertype = installbond.Getidname("BB_RPTPSLT_IDNAME '" & Session("userID") & "'")
            databonds(sertype, Drpapptype)

            'bond service center dropdownlist
            Dim dsServerCenter As DataSet = servEntity.GetSVCIDByUserID(Session("userID"))
            databonds(dsServerCenter, servCenterDrpListFrom)
            databonds(dsServerCenter, servCenterDrpListTo)

            '''access control'''''''''''''''''''''''''''''''''''''''''''''''
            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "44")
            Me.reportviewer.Enabled = purviewArray(3)

            datemin.Text = lstrDefaultStartDate
            datemax.Text = lstrDefaultEndDate

        ElseIf Session("Psltflag") = "1" Then
            viewreport()
        End If
        HypCalFrom.NavigateUrl = "javascript:DoCal(document.form1.datemin);"
        HypCalTo.NavigateUrl = "javascript:DoCal(document.form1.datemax);"

    End Sub
#Region " bonds"
    Public Function databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        Dim row As DataRow
        If ds.Tables(0).Rows.Count > 0 Then
            dropdown.Items.Add("")
            For Each row In ds.Tables(0).Rows
                Dim NewItem As New ListItem()
                NewItem.Text = Trim(row("id")) & "-" & row("name")
                NewItem.Value = Trim(row("id"))
                dropdown.Items.Add(NewItem)
            Next
        End If
    End Function
#End Region

#Region " view report message"
    Public Function viewreport()
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle


        '    'set params'
        Dim psltEntity As New ClsRptPslt

        'datemin.Text = Request.Form("datemin")

        If Trim(minstate.Text).ToUpper = "AAA" Then
            psltEntity.minstate = ""
        Else
            psltEntity.minstate = Trim(minstate.Text)
        End If
        If Trim(minsercenter.Text).ToUpper = "AAA" Or minsercenter.Text = "" Then
            psltEntity.minsercenter = ""
        Else
            psltEntity.minsercenter = Trim(servCenterDrpListFrom.SelectedValue.ToString()) 'Trim(minsercenter.Text)
        End If
        If Trim(mintech.Text).ToUpper = "AAA" Or mintech.Text = "" Then
            psltEntity.mintech = ""
        Else
            psltEntity.mintech = Trim(TechnicianDrpListFrom.SelectedValue.ToString())  'Trim(mintech.Text)
        End If
        If Trim(minPartcode.Text).ToUpper = "AAA" Then
            psltEntity.minPartcode = ""
        Else
            psltEntity.minPartcode = Trim(minPartcode.Text)
        End If

        If RadioButtonList1.SelectedIndex = 0 Then 'date
            If Trim(datemin.Text) <> "" And datemin.Text.Length = 10 Then
                Dim temparr As Array = datemin.Text.Split("/")
                psltEntity.datemin = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
            Else
                Dim mind As String = lstrDefaultStartDate
                Dim temparr As Array = mind.Split("/")
                psltEntity.datemin = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
            End If

            If Trim(datemax.Text) <> "" And datemax.Text.Length = 10 Then
                Dim temparr2 As Array = datemax.Text.Split("/")
                psltEntity.datemax = temparr2(2) + "-" + temparr2(1) + "-" + temparr2(0)
            Else
                Dim maxd As String = lstrDefaultEndDate
                Dim temparr2 As Array = maxd.Split("/")
                psltEntity.datemax = temparr2(2) + "-" + temparr2(1) + "-" + temparr2(0)
            End If


        Else
            If Trim(datemin.Text) <> "" Then
                Dim dateminmonth As String = "01/" + Right(datemin.Text, 7)
                Dim temparr As Array = dateminmonth.Split("/")
                psltEntity.datemin = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
            Else
                Dim mind As String = lstrDefaultStartDate
                Dim temparr As Array = mind.Split("/")
                psltEntity.datemin = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
            End If

            If Trim(datemax.Text) <> "" Then
                Dim dateleft As String = ""
                Dim datetest As String = Right(datemax.Text, 7)
                datetest = Left(datetest, 2)
                If datetest = "01" Or datetest = "03" Or datetest = "05" Or datetest = "07" Or datetest = "08" Or datetest = "10" Or datetest = "12" Then
                    dateleft = "31/"
                End If
                If datetest = "04" Or datetest = "06" Or datetest = "09" Or datetest = "11" Then
                    dateleft = "30/"
                End If
                If datetest = "02" Then
                    dateleft = "28/"
                End If
                Dim datemaxmonth As String = dateleft + Right(datemax.Text, 7)
                Dim temparr2 As Array = datemaxmonth.Split("/")
                psltEntity.datemax = temparr2(2) + "-" + temparr2(1) + "-" + temparr2(0)
            Else
                Dim maxd As String = lstrDefault6MthAfter
                Dim temparr2 As Array = maxd.Split("/")
                psltEntity.datemax = temparr2(2) + "-" + temparr2(1) + "-" + temparr2(0)
            End If

        End If

        If Drpapptype.SelectedValue().ToString() = "" Then
            psltEntity.servicetype = "all"
        Else
            psltEntity.servicetype = Drpapptype.SelectedValue().ToString
            'send Drpapptype.text to viewreport
            Dim app As Array = Drpapptype.SelectedItem.Text.Split("-")
            psltEntity.apptype = app(0).ToString.Trim() + "-" + app(1).ToString.Trim()
        End If

        If Trim(maxstate.Text) = "ZZZ" Then
            psltEntity.maxstate = "ZZZZZZZZZZZ"
        Else
            psltEntity.maxstate = Trim(maxstate.Text).ToUpper.ToString()
        End If

        If Trim(maxsercenter.Text) = "ZZZ" Or maxsercenter.Text = "" Then
            psltEntity.maxsercenter = "ZZZZZZZZZZZ"
        Else
            psltEntity.maxsercenter = Trim(servCenterDrpListTo.SelectedValue.ToString()) 'Trim(maxsercenter.Text).ToUpper.ToString() 
        End If

        If Trim(maxtech.Text) = "ZZZ" Or maxtech.Text = "" Then
            psltEntity.maxtech = "ZZZZZZZZZZZ"
        Else
            psltEntity.maxtech = Trim(TechnicianDrpListTo.SelectedValue.ToString()) 'Trim(maxtech.Text).ToUpper.ToString()
        End If

        If Trim(maxPartcode.Text) = "ZZZ" Then
            psltEntity.maxPartcode = "ZZZZZZZZZZZ"
        Else
            psltEntity.maxPartcode = Trim(maxPartcode.Text).ToUpper.ToString()
        End If

        If Trim(Session("username")) = "" Then
            psltEntity.uname = "ADMIN"
        Else
            psltEntity.uname = Session("username")
        End If

        psltEntity.logrank = Session("login_rank")
        Dim login_rank As Integer = Session("login_rank")
        If login_rank <> 0 Then
            Select Case (login_rank)
                Case 9
                    psltEntity.logctrid = Session("login_ctryID").ToString.ToUpper
                    psltEntity.logcompid = Session("login_cmpID").ToString.ToUpper
                    psltEntity.logsvrcenterid = Session("login_svcID").ToString.ToUpper
                Case 8
                    psltEntity.logctrid = Session("login_ctryID").ToString.ToUpper
                    psltEntity.logcompid = Session("login_cmpID").ToString.ToUpper
                Case 7
                    psltEntity.logctrid = Session("login_ctryID").ToString.ToUpper
            End Select
        End If



        'Dim ds As DataSet = psltEntity.Getpslt()
        If RadioButtonList1.SelectedIndex = 0 Then 'date
            psltEntity.datetype = "Date"
        Else
            psltEntity.datetype = "Month"
        End If


        Session("rptpslt_search_condition") = psltEntity
    End Function
#End Region

    ' Modified by Ryan Estandarte 25 Sept 2012
    'Protected Sub reportviewer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles reportviewer.Click
    '    If IsDate(datemin.Text) Then
    '        Session("Psltflag") = "1"
    '        viewreport()
    '        'Response.Redirect("~/PresentationLayer/REPORT/RptPsltsearch.aspx")
    '        Dim script As String = "window.open('../report/RptPsltsearch.aspx')"
    '        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "RptPsltsearch", script, True)
    '        Return
    '    End If
    'End Sub
    Protected Sub ViewReport_Click(ByVal sender As Object, ByVal e As EventArgs)
        If IsDate(datemin.Text) Then
            Session("Psltflag") = "1"
            viewreport()
            'Response.Redirect("~/PresentationLayer/REPORT/RptPsltsearch.aspx")
            Dim buttonID As String = CType(sender, LinkButton).ID

            Dim script As String = String.Empty
            Select Case buttonID
                Case "reportviewer"
                    script = "window.open('../report/RptPsltsearch.aspx?view=rpt')"
                Case "lnkExportExcel"
                    script = "window.open('../report/RptPsltsearch.aspx?view=xls')"
            End Select

            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "RptPsltsearch", script, True)
            Return
        End If
    End Sub

    'Protected Sub Calendar1_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendar1.SelectionChanged

    '    System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
    '    If RadioButtonList1.SelectedIndex = 0 Then 'date
    '        datemin.Text = Calendar1.SelectedDate.Date.ToString().Substring(0, 10)
    '        datemax.Text = DateAdd(DateInterval.Day, 5, Calendar1.SelectedDate.Date)
    '        Calendar1.Visible = False
    '    Else 'Month
    '        Dim aa As String
    '        aa = Calendar1.SelectedDate.Date.ToString().Substring(0, 10)
    '        datemin.Text = Right(aa, 7)
    '        aa = DateAdd(DateInterval.Month, 5, Calendar1.SelectedDate.Date)
    '        datemax.Text = Right(aa, 7)
    '        Calendar1.Visible = False
    '    End If
    '    Session("Psltflag") = "0"

    'End Sub    

    Sub ChangeDate()

        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        If RadioButtonList1.SelectedIndex = 0 Then 'date
            'datemin.Text = Calendar1.SelectedDate.Date.ToString().Substring(0, 10)
            Dim dateis As Date = Convert.ToDateTime(datemin.Text)
            datemax.Text = DateAdd(DateInterval.Day, 5, dateis.Date)
        Else 'Month
            Dim aa As String
            aa = JCalendar1.SelectedDate.Date.ToString().Substring(0, 10)
            datemin.Text = Right(aa, 7)
            aa = DateAdd(DateInterval.Month, 5, JCalendar1.SelectedDate.Date)
            datemax.Text = Right(aa, 7)
        End If
        Session("Psltflag") = "0"
    End Sub
    Protected Sub JCalendar1_SelectedDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles JCalendar1.SelectedDateChanged
        'ChangeDate()
    End Sub

    Protected Sub datemin_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles datemin.TextChanged
        datemin.Text = Request.Form("datemin")


        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        If RadioButtonList1.SelectedIndex = 0 Then 'date
            'datemin.Text = Calendar1.SelectedDate.Date.ToString().Substring(0, 10)

            If datemin.Text.Length = 7 Then datemin.Text = "01/" & datemin.Text

            If IsDate(datemin.Text) Then
                Dim dateis As Date = Convert.ToDateTime(datemin.Text)
                ''datemax.Text = DateAdd(DateInterval.Day, 5, dateis.Date)
            Else

            End If
        Else 'Month
            If IsDate(datemin.Text) Then
                Dim aa As String
                aa = datemin.Text
                ''datemin.Text = Right(aa, 7)
                aa = DateAdd(DateInterval.Month, 5, CDate(aa))
                ''datemax.Text = Right(aa, 7)
            End If
            Session("Psltflag") = "0"
        End If
    End Sub

    Protected Sub RadioButtonList1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RadioButtonList1.SelectedIndexChanged
        datemin.Text = Request.Form("datemin")


        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        ' Modified by Ryan Estandarte 24 Sept 2012
        'If RadioButtonList1.SelectedIndex = 0 Then 'date
        '    'datemin.Text = Calendar1.SelectedDate.Date.ToString().Substring(0, 10)

        '    If datemin.Text.Length = 7 Then datemin.Text = "01/" & datemin.Text
        '    If IsDate(datemin.Text) Then
        '        Dim dateis As Date = Convert.ToDateTime(datemin.Text)
        '        datemax.Text = DateAdd(DateInterval.Day, 5, dateis.Date)
        '    End If
        'Else 'Month
        '    If IsDate(datemin.Text) Then
        '        Dim aa As String
        '        aa = datemin.Text
        '        datemin.Text = Right(aa, 7)
        '        aa = DateAdd(DateInterval.Month, 5, CDate(aa))
        '        datemax.Text = Right(aa, 7)
        '    End If
        '    Session("Psltflag") = "0"
        'End If
        If RadioButtonList1.SelectedIndex = 0 Then 'date
            'datemin.Text = Calendar1.SelectedDate.Date.ToString().Substring(0, 10)

            If datemin.Text.Length = 7 Then datemin.Text = "01/" & datemin.Text
            If IsDate(datemin.Text) Then
                ' Modified by Ryan Estandarte 25 Sept 2012
                'Dim dateis As Date = Convert.ToDateTime(datemin.Text)
                'datemax.Text = DateAdd(DateInterval.Day, 5, dateis.Date)
                datemax.Text = lstrDefaultEndDate
            End If
        Else 'Month
            If IsDate(datemin.Text) Then
                Dim aa As String
                ' Modified by Ryan Estandarte 25 Sept 2012
                'aa = datemin.Text
                'datemin.Text = Right(aa, 7)
                'aa = DateAdd(DateInterval.Month, 5, CDate(aa))
                'datemax.Text = Right(aa, 7)
                aa = lstrDefaultStartDate
                datemin.Text = Right(aa, 7)
                aa = lstrDefaultEndDate
                datemax.Text = Right(aa, 7)
                
            End If
            Session("Psltflag") = "0"
        End If
    End Sub


    '#Region "Ajax"

    '    <AjaxPro.AjaxMethod()> _
    '    Function GetTechnicianIDList(ByVal lstrServiceCenterIDFrom As String, ByVal lstrServiceCenterIDTo As String) As ArrayList
    '        servEntity.minsercenter = lstrServiceCenterIDFrom
    '        servEntity.maxsercenter = lstrServiceCenterIDTo
    '        Dim dsTechnician As DataSet = servEntity.GetTCHINFODrpListData()

    '        Dim strTemp As String
    '        Dim Listas As New ArrayList(dsTechnician.Tables(0).Rows.Count)
    '        Dim i As Integer = 0
    '        Listas.Add("")
    '        Try
    '            For i = 0 To dsTechnician.Tables(0).Rows.Count - 1
    '                strTemp = dsTechnician.Tables(0).Rows(i).Item(0).ToString & ":" & dsTechnician.Tables(0).Rows(i).Item(1).ToString & "-" & dsTechnician.Tables(0).Rows(i).Item(0).ToString
    '                Listas.Add(strTemp)
    '            Next
    '        Catch
    '        End Try

    '        Return Listas
    '    End Function

    '#End Region

    Protected Sub servCenterDrpListFrom_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles servCenterDrpListFrom.SelectedIndexChanged, servCenterDrpListTo.SelectedIndexChanged
        Dim controlID As DropDownList = DirectCast(sender, DropDownList) '.ID
        If controlID.SelectedIndex <> 0 Then
            'If servCenterDrpListFrom.SelectedIndex <> 0 Then
            servEntity.minsercenter = servCenterDrpListFrom.SelectedValue
            servEntity.maxsercenter = servCenterDrpListTo.SelectedValue
            Dim ds As DataSet = servEntity.GetTCHINFODrpListData()

            TechnicianDrpListFrom.Items.Clear()
            TechnicianDrpListTo.Items.Clear()
            Dim dt As DataTable
            'Dim dr As DataRow
            dt = ds.Tables(0)

            TechnicianDrpListFrom.Items.Add("")
            TechnicianDrpListTo.Items.Add("")
            For i As Integer = 0 To dt.Rows.Count - 1
                Dim technm As String = dt.Rows(i).Item(1) + "-" + dt.Rows(i).Item(0)
                TechnicianDrpListFrom.Items.Add(New ListItem(technm, dt.Rows(i).Item(0)))
                TechnicianDrpListTo.Items.Add(New ListItem(technm, dt.Rows(i).Item(0)))
            Next

            If servCenterDrpListFrom.SelectedValue > servCenterDrpListTo.SelectedValue Then
                servCenterDrpListTo.SelectedIndex = -1
            End If

            minsercenter.Text = servCenterDrpListFrom.SelectedValue
            maxsercenter.Text = servCenterDrpListTo.SelectedValue
        End If
    End Sub

    Protected Sub TechnicianDrpListFrom_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TechnicianDrpListFrom.SelectedIndexChanged, TechnicianDrpListTo.SelectedIndexChanged
        Dim controlID As DropDownList = DirectCast(sender, DropDownList) '.ID
        If controlID.SelectedIndex <> 0 Then
            mintech.Text = TechnicianDrpListFrom.SelectedValue
            maxtech.Text = TechnicianDrpListTo.SelectedValue
        End If
    End Sub

End Class


