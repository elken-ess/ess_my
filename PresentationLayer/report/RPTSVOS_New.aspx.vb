Imports BusinessEntity
Imports System.Data

Namespace PresentationLayer.report
    ''' <summary>
    ''' Codes are originated from RPTSVOS.aspx.vb. Revised and cleaned the code.
    ''' </summary>
    ''' <remarks>Created by Ryan Aimel J. Estandarte 17 April 2012</remarks>
    Partial Class PresentationLayer_report_RPTSVOS_New
        Inherits Page
        Private _fstrDefaultStartDate As String = ""
        Private _fstrDefaultEndDate As String = ""
        Private ReadOnly _datestyle As Globalization.CultureInfo = New Globalization.CultureInfo("en-CA")

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load

            Threading.Thread.CurrentThread.CurrentCulture = _datestyle

            'Dim fstrMonth As String = CType(Month(System.DateTime.Today), String)
            _fstrDefaultStartDate = DateTime.Today.ToShortDateString()
            'fstrDefaultEndDate = DateAdd(DateInterval.Month, 1, CDate(fstrDefaultStartDate))
            _fstrDefaultEndDate = _fstrDefaultStartDate
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Const script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If

            Catch ex As Exception
                Const script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try

            If Not Page.IsPostBack Then
                'Session("SVOSflag") = "0"

                '''access control'''''''''''''''''''''''''''''''''''''''''''''''
                Dim accessgroup As String = Session("accessgroup").ToString
                Dim purviewArray As Boolean() = New Boolean() {False, False, False, False}
                'purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "41")
                purviewArray = CType(clsUserAccessGroup.GetUserPurview(accessgroup, "62"), Boolean())
                saveButton.Enabled = purviewArray(3)
                '''''''''''''''''''''''''''''''''''''''''''''''''''''
                Dim objXmlTr As New clsXml ''
                objXmlTr.XmlFile = ConfigurationManager.AppSettings("XmlFilePath")
                Label10.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
                'Label7.Text = Label10.Text
                'Label12.Text = Label10.Text
                'Label14.Text = Label10.Text
                ' Commented by Ryan Estandarte 17 April 2012
                ' Description: Remove State Filter row
                'Me.Label17.Text = Me.Label10.Text
                Label19.Text = Label10.Text
                Label21.Text = Label10.Text
                Label22.Text = Label10.Text
                'Label11.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
                Label13.Text = "To"
                'Label15.Text = Label11.Text
                'Label16.Text = Label11.Text
                ' Commented by Ryan Estandarte 17 April 2012
                ' Description: Remove State Filter row
                'Me.Label18.Text = Me.Label11.Text
                Label20.Text = "To"
                Label23.Text = "To"
                Label24.Text = "To"

                'Label1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0012")
                Label2.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0013")
                'Label3.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0014")
                'Label4.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0015")
                ' Commented by Ryan Estandarte 17 April 2012
                ' Description: Remove State Filter row
                'Me.Label5.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0016")
                Label6.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0017")
                Label8.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0018")
                Label9.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0020")
                lblServiceType.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-SERVICETYPE")
                lblStatus.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-STATUS")

                'maxroserial.Text = "ZZZ"
                maxareabox.Text = "ZZZ"
                'maxcustmbox.Text = "ZZZ"
                'maxmodelbox.Text = "ZZZ"
                ' Commented by Ryan Estandarte 17 April 2012
                ' Description: Remove State Filter row
                'Me.maxstatebox.Text = "ZZZ"
                maxsvsbox.Text = "ZZZ"
                ' Modified by Ryan Estandarte 16 April 2012
                ' Description: Change of requirement for SMR 9912/35
                'Me.maxtchnicanbox.Text = "ZZZ"
                maxtchnicanbox.Text = "ESSAA"

                'minroserial.Text = "AAA"
                minsvsbox.Text = "AAA"
                ' Commented by Ryan Estandarte 17 April 2012
                ' Description: Remove State Filter row
                'Me.minstatebox.Text = "AAA"
                'minmodelbox.Text = "AAA"
                'mincustmbox.Text = "AAA"
                mintchnicanbox.Text = "AAA"
                minareabox.Text = "AAA"

                mindatebox.Text = _fstrDefaultStartDate
                maxdatebox.Text = _fstrDefaultEndDate

                Dim apptStat As New clsUtil()


                cboStatus.Items.Add(New ListItem("All", ""))
                Dim statParam As ArrayList = apptStat.searchconfirminf("APPTStatus")
                Dim count As Integer
                Dim statid As String
                Dim statnm As String
                For count = 0 To statParam.Count - 1
                    statid = CType(statParam.Item(count), String)
                    statnm = CType(statParam.Item(count + 1), String)
                    count = count + 1

                    If statid <> "UA" And statid <> "X" Then
                        cboStatus.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                    End If
                Next

                cboStatus.Items(0).Selected = True


                Dim installbond As New clsCommonClass
                'bond  service type id and name
                Dim sertype As DataSet = installbond.Getidname("BB_RPTPSLT_IDNAME '" & Session("userID") & "'")
                databonds(sertype, cboServiceType)
			
                ' saveButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-VIEWREPORT")
                ' Me.HyperLink1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-VIEWREPORT")
                titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0022-NEW")

            End If
            HypCalFrom.NavigateUrl = "javascript:DoCal(document.form1.mindatebox);"
            HypCalTo.NavigateUrl = "javascript:DoCal(document.form1.maxdatebox);"


        End Sub

#Region " bonds"
        Public Sub databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList)
            dropdown.Items.Clear()
            dropdown.Items.Add(New ListItem("All", ""))

            Dim row As DataRow
            If ds.Tables(0).Rows.Count > 0 Then
                'dropdown.Items.Add("")
                For Each row In ds.Tables(0).Rows
                    Dim NewItem As New ListItem()
                    NewItem.Text = Trim(row("id").ToString()) & "-" & row("name").ToString()
                    NewItem.Value = Trim(row("id").ToString())
                    dropdown.Items.Add(NewItem)
                Next
            End If
        End Sub
#End Region

        Protected Sub saveButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles saveButton.Click
            Session("SVOSflag") = "1"
            viewreport()
            Const script As String = "window.open('../report/RPTSVOSVIEW_New.aspx')"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "RPTSVOSVIEW", script, True)
            Return

        End Sub

#Region " view report message"
        Public Sub viewreport()
            Threading.Thread.CurrentThread.CurrentCulture = _datestyle

            Dim rptsvos As New ClsRptsvosNew

            mindatebox.Text = Request.Form("mindatebox")
            maxdatebox.Text = Request.Form("maxdatebox")

            If Not IsDate(mindatebox.Text) Then
                mindatebox.Text = _fstrDefaultStartDate
            End If

            If Not IsDate(maxdatebox.Text) Then
                maxdatebox.Text = _fstrDefaultEndDate
            End If

            'If (mincustmbox.Text <> "AAA") Then
            '    rptsvos.MinCustmID = mincustmbox.Text.ToUpper
            'Else
            '    rptsvos.MinCustmID = String.Empty
            'End If

            'If (maxcustmbox.Text <> "ZZZ") Then
            '    rptsvos.MaxCustmID = maxcustmbox.Text.ToUpper
            'Else
            '    rptsvos.MaxCustmID = "ZZZZZZZZZZZZZZZZZZZZ"
            'End If
            If (minsvsbox.Text <> "AAA") Then
                rptsvos.MinSvcID = minsvsbox.Text.ToUpper
            Else
                rptsvos.MinSvcID = String.Empty
            End If
            If (maxsvsbox.Text <> "ZZZ") Then
                rptsvos.MaxSvcID = maxsvsbox.Text.ToUpper
            Else
                rptsvos.MaxSvcID = "ZZZZZZZZZZZZZZZZZZZZ"
            End If
            'If (minmodelbox.Text <> "AAA") Then
            '    rptsvos.MinPrdctMd = minmodelbox.Text.ToUpper
            'Else
            '    rptsvos.MinPrdctMd = String.Empty
            'End If
            'If (maxmodelbox.Text <> "ZZZ") Then
            '    rptsvos.MaxPrdctMd = maxmodelbox.Text.ToUpper
            'Else
            '    rptsvos.MaxPrdctMd = "ZZZZZZZZZZZZZZZZZZZZ"
            'End If
            'If (minroserial.Text <> "AAA") Then
            '    rptsvos.MinRoNo = minroserial.Text.ToUpper
            'Else
            '    rptsvos.MinRoNo = String.Empty
            'End If
            'If (maxroserial.Text <> "ZZZ") Then
            '    rptsvos.MaxRoNo = maxroserial.Text.ToUpper
            'Else
            '    rptsvos.MaxRoNo = "ZZZZZZZZZZZZZZZZZZZZ"
            'End If
            ' Commented by Ryan Estandarte 17 April 2012
            ' Description: Remove State Filter row
            'If (Me.minstatebox.Text <> "AAA") Then
            '    rptsvos.minState = Me.minstatebox.Text.ToUpper
            'Else
            '    rptsvos.minState = ""
            'End If
            'If (Me.maxstatebox.Text <> "ZZZ") Then
            '    rptsvos.maxState = Me.maxstatebox.Text.ToUpper
            'Else
            '    rptsvos.maxState = "ZZZZZZZZZZZZZZZZZZZZ"
            'End If

            ' Modified by Ryan Estandarte 17 April 2012
            ' Description: Change default value for Technician to ESSAA
            'If (Me.mintchnicanbox.Text <> "AAA") Then
            '    rptsvos.minTchID = Me.mintchnicanbox.Text.ToUpper
            'Else
            '    rptsvos.minTchID = ""
            'End If
            'If (Me.maxtchnicanbox.Text <> "ZZZ") Then
            '    rptsvos.maxTchID = Me.maxtchnicanbox.Text.ToUpper
            'Else
            '    rptsvos.maxTchID = "ZZZZZZZZZZZZZZZZZZZZ"
            'End If

            ' Modified by Ryan Estandarte 6/21/2012
            ' Description: unbalanced records
            'If (mintchnicanbox.Text <> "ESSAA") Then
            '    rptsvos.MinTchID = mintchnicanbox.Text.ToUpper
            'Else
            '    rptsvos.MinTchID = String.Empty
            'End If
            If (mintchnicanbox.Text <> "AAA") Then
                rptsvos.MinTchID = mintchnicanbox.Text.ToUpper
            Else
                rptsvos.MinTchID = String.Empty
            End If
            If (maxtchnicanbox.Text <> "ESSAA") Then
                rptsvos.MaxTchID = maxtchnicanbox.Text.ToUpper
            Else
                ' Modified by Ryan Estandarte 10 July 2012' 
                'rptsvos.MaxTchID = "ZZZZZZZZZZZZZZZZZZZZ"
                rptsvos.MaxTchID = "ESSAA"
            End If

            If (minareabox.Text <> "AAA") Then
                rptsvos.MinArea = minareabox.Text.ToUpper
            Else
                rptsvos.MinArea = String.Empty
            End If

            If (maxareabox.Text <> "ZZZ") Then
                rptsvos.MaxArea = maxareabox.Text.ToUpper
            Else
                rptsvos.MaxArea = "ZZZZZZZZZZZZZZZZZZZZ"
            End If

            rptsvos.Status = cboStatus.SelectedValue
            rptsvos.StatusText = cboStatus.SelectedItem.Text

            rptsvos.ServiceType = cboServiceType.SelectedValue
            rptsvos.ServiceTypeText = cboServiceType.SelectedItem.Text

            rptsvos.StaffType = stafftypeddl.SelectedValue
            Session("stafftype") = stafftypeddl.SelectedValue

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ' Modified by Ryan Estandarte 22 May 2012
            'If (mindatebox.Text <> "") Then
            '    Dim datearr As String() = mindatebox.Text.ToString().Split(CType("/", Char))
            '    Dim rtdate As String = datearr(2) + "-" + datearr(1) + "-" + datearr(0)

            '    rptsvos.mindate = DateTime.Parse(rtdate)
            'Else
            '    rptsvos.mindate = DateTime.Parse(_fstrDefaultStartDate)
            'End If
            'If (maxdatebox.Text <> "") Then
            '    Dim datearr As String() = maxdatebox.Text.ToString().Split(CType("/", Char))
            '    Dim rtdate As String = datearr(2) + "-" + datearr(1) + "-" + datearr(0)
            '    rptsvos.maxdate = DateTime.Parse(rtdate)
            'Else
            '    rptsvos.maxdate = DateTime.Parse(_fstrDefaultStartDate)
            'End If
            Dim minDate As DateTime = DateTime.Today
            DateTime.TryParse(mindatebox.Text, minDate)

            Dim maxDate As DateTime = DateTime.Today
            DateTime.TryParse(maxdatebox.Text, maxDate)

            rptsvos.Mindate = minDate
            rptsvos.Maxdate = maxDate

            '////////////////////////////////////////////////

            Dim ctryIDPara As String = "%"
            Dim cmpIDPara As String = "%"
            Dim svcIDPara As String = "%"
            Dim loginRank As Integer = CType(Session("login_rank"), Integer)
            If loginRank <> 0 Then
                Select Case (loginRank)
                    Case 9
                        ctryIDPara = Session("login_ctryID").ToString.ToUpper
                        cmpIDPara = Session("login_cmpID").ToString.ToUpper
                        svcIDPara = Session("login_svcID").ToString.ToUpper
                    Case 8
                        ctryIDPara = Session("login_ctryID").ToString.ToUpper
                        cmpIDPara = Session("login_cmpID").ToString.ToUpper
                    Case 7
                        ctryIDPara = Session("login_ctryID").ToString.ToUpper
                End Select
            Else


            End If
            ''''''''''''''''''''''''''''''''''''''''''''''''''
            rptsvos.Rank = loginRank.ToString
            rptsvos.CountryID = ctryIDPara
            rptsvos.CompID = cmpIDPara
            rptsvos.SVCID = svcIDPara
            '////////////////////////////////////////////////


            If Trim(Session("userID").ToString()) = "" Then
                rptsvos.UserName = "  "
            Else
                rptsvos.UserName = Session("userID").ToString()
            End If

            Session("rptsvos_searchcondition") = rptsvos

        End Sub
#End Region


        Protected Sub ExcelBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ExcelBtn.Click
            Session("SVOSflag") = "1"
            viewreport()
            Const script As String = "window.open('../report/RPTSVOSVIEW_ExcelNew.aspx')"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "RPTSVOSVIEW", script, True)
        End Sub
    End Class
End Namespace