
Imports BusinessEntity
Imports System.Data

Namespace PresentationLayer.report
    ''' <summary>
    ''' Codes are originated from RPTSVOSVIEW.aspx.vb Revised and cleaned the code.
    ''' </summary>
    ''' <remarks>Created by Ryan Estandarte 17 April 2012</remarks>
    Partial Class PresentationLayer_report_RPTSVOSVIEW_New
        Inherits Page

        Dim clsReport As New ClsCommonReport

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
            Dim datestyle As Globalization.CultureInfo = New Globalization.CultureInfo("en-CA")
            Threading.Thread.CurrentThread.CurrentCulture = datestyle
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Const script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If

            Catch ex As Exception
                Const script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Dim rptsvos As clsRPTSVOSNew = CType(Session("rptsvos_searchcondition"), clsRPTSVOSNew)


            Dim rptds As DataSet = rptsvos.GetServiceOrderSum()

            ' switch  encrypt to telphone for display        
            Dim count As Integer
            Dim prdctclname As String
            Dim clscomm As New clsCommonClass()
            For count = 0 To rptds.Tables(0).Rows.Count - 1
                Dim prdctclsid As String = rptds.Tables(0).Rows(count).Item(3).ToString 'item(8) is product class 
                prdctclname = clscomm.passconverttel(prdctclsid)
                rptds.Tables(0).Rows(count).Item(3) = prdctclname
            Next
            '''''''''''''''''''''''''''''''''''''''''''''''''''


            Dim objXmlTr As New clsXml

            Dim strHead As String() = CType(Array.CreateInstance(GetType(String), 52), String())
            objXmlTr.XmlFile = ConfigurationManager.AppSettings("XmlFilePath")
            strHead(0) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0002")
            strHead(1) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0003")
            strHead(2) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0004")
            strHead(3) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0001")
            strHead(4) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0005")
            strHead(5) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0006-NEW")
            strHead(6) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0007")
            strHead(7) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0008")
            strHead(8) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0009-NEW")
            strHead(9) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0010-NEW")

            strHead(10) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0021-NEW")
            strHead(11) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0022-NEW")
            strHead(12) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0023")
            strHead(13) = rptsvos.Mindate.ToString()
            strHead(14) = rptsvos.Maxdate.ToString()
            strHead(15) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0024")

            Dim str As String = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_E_TOTAL") ' RPT_APT_RECORD
            If (rptds.Tables(0).Rows.Count > 0) Then
                strHead(16) = str & " " & rptds.Tables(0).Rows(0).Item("TotalRecord").ToString
            Else
                strHead(16) = str & " 0"
            End If

            If Session("userID") = "" Then
                strHead(17) = " "
            Else
                strHead(17) = "/" & Session("userID").ToString().ToUpper + "/" + Session("username").ToString.ToUpper

            End If
            'strHead(17) = Session("userID")
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            strHead(18) = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            strHead(19) = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''      

            strHead(20) = "Staff Type" 'objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0012") 'Customer
            strHead(21) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0013") 'Service CenterID
            strHead(22) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0014") 'Model
            strHead(23) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0015") 'RO Serial No.
            strHead(24) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0016") 'State  

            strHead(25) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0020") 'Appoint Date
            strHead(26) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0017") 'Technician
            strHead(27) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0018") 'Area
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            strHead(28) = Session("stafftype").ToString

            'If rptsvos.MinCustmID = Nothing Then
            '    strHead(28) = "AAA"
            'Else
            '    strHead(28) = rptsvos.MinCustmID.ToString
            'End If
            If rptsvos.MaxCustmID = Nothing Then
                strHead(29) = "AAA"
            ElseIf rptsvos.MaxCustmID = "ZZZZZZZZZZZZZZZZZZZZ" Then
                strHead(29) = "ZZZ"
            Else
                strHead(29) = rptsvos.MaxCustmID.ToString
            End If

            If rptsvos.MinSvcID = Nothing Then
                strHead(30) = "AAA"
            Else
                strHead(30) = rptsvos.MinSvcID.ToString
            End If
            If rptsvos.MaxSvcID = Nothing Then
                strHead(31) = "AAA"
            ElseIf rptsvos.MaxSvcID = "ZZZZZZZZZZZZZZZZZZZZ" Then
                strHead(31) = "ZZZ"
            Else
                strHead(31) = rptsvos.MaxSvcID.ToString
            End If

            If rptsvos.MinPrdctMd = Nothing Then
                strHead(32) = "AAA"
            Else
                strHead(32) = rptsvos.MinPrdctMd.ToString
            End If
            If rptsvos.MaxPrdctMd = Nothing Then
                strHead(33) = "AAA"
            ElseIf rptsvos.MaxPrdctMd = "ZZZZZZZZZZZZZZZZZZZZ" Then
                strHead(33) = "ZZZ"
            Else
                strHead(33) = rptsvos.MaxPrdctMd.ToString
            End If
            If rptsvos.MinRoNo = Nothing Then
                strHead(34) = "AAA"
            Else
                strHead(34) = rptsvos.MinRoNo.ToString
            End If
            If rptsvos.MaxRoNo = Nothing Then
                strHead(35) = "AAA"
            ElseIf rptsvos.MaxRoNo = "ZZZZZZZZZZZZZZZZZZZZ" Then
                strHead(35) = "ZZZ"
            Else
                strHead(35) = rptsvos.MaxRoNo.ToString
            End If
            '''''''''''''''''''''''''''''''''''''''''''''
            If rptsvos.MinState = Nothing Then
                strHead(36) = "AAA"
            Else
                strHead(36) = rptsvos.MinState.ToString
            End If
            If rptsvos.MaxState = Nothing Then
                strHead(37) = "AAA"
            ElseIf rptsvos.MaxState = "ZZZZZZZZZZZZZZZZZZZZ" Then
                strHead(37) = "ZZZ"
            Else
                strHead(37) = rptsvos.MaxState.ToString
            End If
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''' 
            '''''''''''''''''''''''''''''''''''''''''''''      　
            strHead(38) = rptsvos.Mindate.ToString("dd/MM/yyyy")

            strHead(39) = rptsvos.Maxdate.ToString("dd/MM/yyyy")
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If rptsvos.MinTchID = Nothing Then
                strHead(40) = "AAA"
            Else
                strHead(40) = rptsvos.MinTchID.ToString
            End If

            ' Modified by Ryan Estandarte 11 June 2012 
            ' Description: Set default to ESSAA
            'If rptsvos.MaxTchID = Nothing Then
            '    strHead(41) = "AAA"
            'ElseIf rptsvos.MaxTchID = "ZZZZZZZZZZZZZZZZZZZZ" Then
            '    strHead(41) = "ZZZ"
            'Else
            '    strHead(41) = rptsvos.MaxTchID.ToString
            'End If
            If rptsvos.MaxTchID = Nothing Then
                strHead(41) = "ESSAA"
            ElseIf rptsvos.MaxTchID = "ZZZZZZZZZZZZZZZZZZZZ" Then
                strHead(41) = "ESSAA"
            Else
                strHead(41) = rptsvos.MaxTchID.ToString
            End If
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If rptsvos.MinArea = Nothing Then
                strHead(42) = "AAA"
            Else
                strHead(42) = rptsvos.MinArea.ToString
            End If
            If rptsvos.MaxArea = Nothing Then
                strHead(43) = "AAA"
            ElseIf rptsvos.MaxArea = "ZZZZZZZZZZZZZZZZZZZZ" Then
                strHead(43) = "ZZZ"
            Else
                strHead(43) = rptsvos.MaxArea.ToString
            End If
            strHead(44) = "/"
            strHead(45) = objXmlTr.GetLabelName("EngLabelMsg", "STATUS")

            strHead(46) = rptsvos.StatusText
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            strHead(47) = "RMS No"

            strHead(48) = objXmlTr.GetLabelName("EngLabelMsg", "SBA-SERVICETYPE")
            strHead(49) = rptsvos.ServiceTypeText
            strHead(50) = ""

            'With clsReport
            '    .ReportFileName = "RPTSVOS.rpt"  'TextBox1.Text
            '    .SetReport(CrystalReportViewer1, rptds, strHead)
            'End With

            ' Modified by Ryan Estandarte 02 April 2012        
            'Dim lstrPdfFileName As String = "ServiceOrderSummary_" & Session("login_session") & ".pdf""
            Dim lstrPdfFileName As String = "ServiceOrderSummary_" & Session("login_session").ToString() & "_" & DateTime.Now.ToString("HHmmsstt") & ".pdf"
            Try
                With clsReport
                    .ReportFileName = "RPTSVOS_New.rpt"
                    .SetReportDocument(rptds, strHead)
                    .PdfFileName = lstrPdfFileName
                    .ExportPdf()
                End With

                Response.Redirect(clsReport.PdfUrl)

            Catch err As Exception
                'System.IO.File.Delete(lstrPhysicalFile)
                Response.Write("<BR>")
                Response.Write("Error Message: " & err.Message.ToString)
                Response.Write("<BR>")
                Response.Write("Source : " & err.Source)
                Response.Write("<BR>")
                Response.Write("Stack Trace Message: " & err.StackTrace)


            End Try

            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0022") & " " & clsReport.GetMessage

        End Sub

        Protected Sub Page_Unload(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Unload

            clsReport.UnloadReport()
            titleLab.Text = titleLab.Text & " " & clsReport.GetMessage
            CrystalReportViewer1.Dispose()
        End Sub
    End Class
End Namespace