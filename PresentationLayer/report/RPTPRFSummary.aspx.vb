Imports System.Data

Imports BusinessEntity

Namespace PresentationLayer.report
    Partial Class PresentationLayer_report_RPTPRFSummary
        Inherits Page
        'Implements IBasicReportPage
#Region "Declaration"
        Private _common As New clsCommonClass()
        Private _xml As New clsXml
#End Region
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Const script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try

            If Not IsPostBack Then
                PopulateLabels()
                'DefineDefaultValues()
                InitializeDataTable()
                ViewReportLinkButton.Enabled = False
            End If

        End Sub

        Private Sub InitializeDataTable()
            Dim dtPRF As New DataTable("PRF")
            dtPRF.Columns.Add("ID")
            dtPRF.Columns.Add("ServiceCenter")
            dtPRF.Columns.Add("Technician")
            dtPRF.Columns.Add("Prf")

            ViewState("PRF") = dtPRF
            ViewState("ID") = 0
        End Sub

        Private Sub PopulateTechnicians(ByVal svcID As String)
            Dim dt As New DataTable
            _common.spctr = Session("login_ctryID").ToString().ToUpper
            _common.spstat = "ACTIVE"
            _common.rank = Session("login_rank").ToString()
            _common.sparea = svcID

            dt = _common.Getcomidname("dbo.BB_FNCAPPS_SelTechArea").Tables(0)

            PopulateDropdownFromDataTable(dt, TechnicianDropdown)

        End Sub

        Public Sub PopulateLabels() 'Implements IBasicReportPage.PopulateLabels
            titleLab.Text = "PRF Summary" ''_xml.GetLabelName("EngLabelMsg", "BB-PRFSUMM")
            ServCenterLabel.Text = "Service Center" ''_xml.GetLabelName("EngLabelMsg", "BB-RPTREPRINTPRF-0001")
            TechnicianLabel.Text = "Technician ID" ''_xml.GetLabelName("EngLabelMsg", "BB-RPTREPRINTPRF-0003")
            PRFLabel.Text = "PRF No" ''_xml.GetLabelName("EngLabelMsg", "BB-RPTREPRINTPRF-0004")

            Dim dt As New DataTable

            _common.sparea = Session("login_svcID").ToString()
            _common.spctr = Session("login_ctryID").ToString()
            _common.spstat = Session("login_cmpID").ToString()
            _common.rank = Session("login_rank").ToString()

            dt = _common.Getcomidname("BB_TOMAS_SVC").Tables(0)

            PopulateDropdownFromDataTable(dt, ServCenterDropdown)

        End Sub

        Public Sub PopulateLabels(ByVal strHead As String()) 'Implements IBasicReportPage.PopulateLabels
            Throw New NotImplementedException()
        End Sub

        Private Sub PopulateDropdownFromDataTable(ByVal dataTable As DataTable, ByVal dropDownList As DropDownList)
            dropDownList.Items.Clear()

            Dim row As DataRow
            If dataTable.Rows.Count > 0 Then
                dropDownList.Items.Add(New ListItem(String.Empty, String.Empty))
                For Each row In dataTable.Rows
                    Dim NewItem As New ListItem()
                    NewItem.Text = row("id").ToString() & " - " & row("name").ToString()
                    NewItem.Value = row("id").ToString()
                    dropDownList.Items.Add(NewItem)
                Next

            End If

            dataTable.Dispose()
        End Sub

        Public Sub GenerateReport() 'Implements IBasicReportPage.GenerateReport
            Session("PRF") = ViewState("PRF")
            Dim dt As DataTable = CType(Session("PRF"), DataTable)
            'Dim prfSum As New ClsRptPrfSummary()
            'prfSum.PrfDataTable = dt

            'Session("PRFSummary") = prfSum

            'Response.Redirect("~/PresentationLayer/report/RPTPRFSummary_VIEW.aspx")
            Const script As String = "window.open('../report/RPTPRFSummary_VIEW.aspx')"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "SystemAccessReport", script, True)
        End Sub

        Public Sub DefineDefaultValues() 'Implements IBasicReportPage.DefineDefaultValues
        End Sub

        Public Sub DefineDefaultValues(ByVal strHead As String()) 'Implements IBasicReportPage.DefineDefaultValues
            Throw New NotImplementedException()
        End Sub

        Public Sub GenerateReport(ByVal dataSet As DataSet) 'Implements IBasicReportPage.GenerateReport
            Throw New NotImplementedException()
        End Sub

        Protected Sub AddLinkButton_Click(ByVal sender As Object, ByVal e As EventArgs)
            If ViewState("PRF") Is Nothing Then
                InitializeDataTable()
            End If

            Dim dtPrf As DataTable = CType(ViewState("PRF"), DataTable)
            dtPrf.TableName = "PRF"
            Dim intID As Integer = CInt(ViewState("ID")) + 1

            If String.IsNullOrEmpty(IDHiddenField.Value) Then
                ' on insert
                If Not CheckIfExists(ServCenterDropdown.SelectedItem.Text, TechnicianDropdown.SelectedItem.Text, PRFTextbox.Text) Then
                    Dim dr As DataRow
                    dr = dtPrf.NewRow()
                    dr("ID") = intID
                    dr("ServiceCenter") = ServCenterDropdown.SelectedItem.Text
                    dr("Technician") = TechnicianDropdown.SelectedItem.Text
                    dr("Prf") = PRFTextbox.Text

                    ViewState("ID") = intID
                    dtPrf.Rows.Add(dr)
                End If
            Else
                'on update
                Dim dr As DataRow()
                dr = dtPrf.Select("ID = '" + IDHiddenField.Value + "'")
                dr(0)("ServiceCenter") = ServCenterDropdown.SelectedItem.Text
                dr(0)("Technician") = TechnicianDropdown.SelectedItem.Text
                dr(0)("Prf") = PRFTextbox.Text

            End If
            PRFGridView.DataSource = dtPrf
            ViewState("PRF") = dtPrf
            PRFGridView.DataBind()

            PRFTextbox.Text = String.Empty
            ViewReportLinkButton.Enabled = True

        End Sub

        Private Function CheckIfExists(ByVal svcID As String, ByVal techID As String, ByVal prf As String) As Boolean
            Dim dt As DataTable = CType(ViewState("PRF"), DataTable)
            Dim dr As DataRow()

            dr = dt.Select("ServiceCenter = '" + svcID + "' and Technician = '" + techID + "' and Prf = '" + prf + "'")

            If dr.Length < 1 Then
                Return False
            End If

            Return True
        End Function

        Protected Sub ViewReportLinkButton_Click(ByVal sender As Object, ByVal e As EventArgs)
            GenerateReport()
        End Sub

        Protected Sub ServCenterDropdown_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ServCenterDropdown.SelectedIndexChanged
            PopulateTechnicians(ServCenterDropdown.SelectedValue)
        End Sub

        Protected Sub PRFGridView_RowDeleting(ByVal sender As Object, ByVal e As GridViewDeleteEventArgs) Handles PRFGridView.RowDeleting
            Dim id = PRFGridView.Rows(e.RowIndex).Cells(1).Text
            Dim dt As DataTable = CType(ViewState("PRF"), DataTable)
            Dim dr As DataRow()

            dr = dt.Select("ID = '" + id + "'")

            dt.Rows.Remove(dr(0))

            ViewState("PRF") = dt
            PRFGridView.DataSource = dt
            PRFGridView.DataBind()

            If dt.Rows.Count < 1 Then
                ViewReportLinkButton.Enabled = False
            End If

        End Sub

        Protected Sub PRFGridView_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles PRFGridView.SelectedIndexChanged
            Dim dr As GridViewRow = PRFGridView.SelectedRow

            PRFTextbox.Text = dr.Cells(4).Text
            ServCenterDropdown.SelectedItem.Text = dr.Cells(2).Text
            TechnicianDropdown.SelectedItem.Text = dr.Cells(3).Text
            IDHiddenField.Value = dr.Cells(1).Text

        End Sub
    End Class
End Namespace