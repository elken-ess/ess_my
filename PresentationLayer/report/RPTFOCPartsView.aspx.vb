Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Web
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web
Imports BusinessEntity
Imports System.Data

Partial Class PresentationLayer_report_RPTFOCPartsView
    Inherits System.Web.UI.Page
    Dim clsReport As New ClsCommonReport
    Dim objXML As New clsXml
    Dim clsFOC As New clsRPTFOCParts()
    Dim callListReportDoc As New ReportDocument()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try

        Me.titleLab.Text = "FOC Parts Report"
        clsFOC = Session("rpt_FocParts")

        Dim dsReport As DataSet = clsFOC.RetriveFOCPartsListing()
        If dsReport Is Nothing Then
            objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
            ErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_FUN_CALLLIST_RERR")
            ErrLab.Visible = True
            Return
        End If
        If dsReport.Tables.Count = 0 Then
            objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
            ErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_FUN_CALLLIST_RERR")
            ErrLab.Visible = True
            Return
        End If

        Try
            callListReportDoc.Load(MapPath("RPTFOCParts.rpt"))
            ErrLab.Visible = False
        Catch ex As Exception
            objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
            ErrLab.Text = ex.InnerException.Message.ToString
            ErrLab.Visible = True
            Return
        End Try

        Try
            callListReportDoc.SetDataSource(dsReport.Tables(0))
        Catch ex As Exception
            ErrLab.Text = "The error returned was " & ex.Message.ToString
        End Try

        objXML.XmlFile = ConfigurationManager.AppSettings("XmlFilePath")
        callListReportDoc.SetParameterValue("CompanyName", objXML.GetLabelName("EngLabelMsg", "BB_REPORT_COMPANYNAME"))
        callListReportDoc.SetParameterValue("ReportTitle", "FOC Parts Reports")
        callListReportDoc.SetParameterValue("UserId", objXML.GetLabelName("EngLabelMsg", "BB-RPTCUSR-USERID") & " " & Session("userID"))
        callListReportDoc.SetParameterValue("ServiceBillDateFr", clsFOC.FOC_ServBillDateFr.ToString("dd/MM/yyyy"))
        callListReportDoc.SetParameterValue("ServiceBillDateTo", clsFOC.FOC_ServBillDateTo.ToString("dd/MM/yyyy"))
        callListReportDoc.SetParameterValue("ServiceCenterFr", clsFOC.FOC_ServCtrFr)
        callListReportDoc.SetParameterValue("ServiceCenterTo", clsFOC.FOC_ServCtrTo)
        callListReportDoc.SetParameterValue("TechnicianFr", clsFOC.FOC_TechIDFr)
        callListReportDoc.SetParameterValue("TechnicianTo", clsFOC.FOC_TechIDTo)
        callListReportDoc.SetParameterValue("PartCodeFr", clsFOC.FOC_PartCodeFr)
        callListReportDoc.SetParameterValue("PartCodeTo", clsFOC.FOC_PartCodeTo)
        callListReportDoc.SetParameterValue("PriceId", clsFOC.FOC_PriceId)
        'callListReportDoc.SetParameterValue("PriceIdFr", clsFOC.FOC_PriceIdFr)
        'callListReportDoc.SetParameterValue("PriceIdTo", clsFOC.FOC_PriceIdTo)
        callListReportDoc.SetParameterValue("ServiceType", clsFOC.FOC_ServType)

        CrystalReportViewer.HasCrystalLogo = False
        CrystalReportViewer.ReportSource = callListReportDoc
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        clsReport.UnloadReport()
        CrystalReportViewer.Dispose()
    End Sub
End Class
