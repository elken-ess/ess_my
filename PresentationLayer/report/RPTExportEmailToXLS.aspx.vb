Imports System.Data

Imports App_Code

Imports BusinessEntity

'******************************************************************************
' History
' -----------------------------------------------------------------------------
' Date(YYMMDD)  ModBy   	        Remarks
' =============================================================================
' 120217        Ryan Estandarte		Created
'******************************************************************************

Namespace PresentationLayer.report
    ''' <summary>
    ''' Created by Ryan Aimel J. Estandarte
    ''' Date Created: 15 Feb 2012
    ''' Description: This module exports the email addresses of the customers to xls.
    ''' </summary>
    ''' <remarks></remarks>
    Partial Class PresentationLayer_report_RPTExportEmailToXLS
        Inherits Page

        Private ReadOnly _clsrptcust As New ClsRptCUSR
        Private ReadOnly _objXmlTr As New clsXml
        Private ReadOnly _clsCommon As New clsCommonClass

        Private ReadOnly _exportEmailToXLS As New ClsExportEmailToXLS()

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
            If Not Page.IsPostBack Then

                PopulateLabels()
                PopulateDropdown()

            End If

        End Sub

        ''' <summary>
        ''' Populates the labels in the page. Labels are retrieved from the database.
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub PopulateLabels()

            titleLab.Text = _objXmlTr.GetLabelName("EngLabelMsg", "BB-EXPORTEMAILTOXML-TITLE")
            lblServCenter.Text = _objXmlTr.GetLabelName("EngLabelMsg", "BB-EXPORTEMAILTOXML-SRVCCENTER")
            lblStateID.Text = _objXmlTr.GetLabelName("EngLabelMsg", "BB-EXPORTEMAILTOXML-STATEID")
            lblAreaID.Text = _objXmlTr.GetLabelName("EngLabelMsg", "BB-EXPORTEMAILTOXML-AREAID")
            lblModel.Text = _objXmlTr.GetLabelName("EngLabelMsg", "BB-EXPORTEMAILTOXML-MODEL")
            lblCustomerType.Text = _objXmlTr.GetLabelName("EngLabelMsg", "BB-EXPORTEMAILTOXML-CUSTTYPE")
            lblRace.Text = _objXmlTr.GetLabelName("EngLabelMsg", "BB-EXPORTEMAILTOXML-RACE")
            lblStatus.Text = _objXmlTr.GetLabelName("EngLabelMsg", "BB-EXPORTEMAILTOXML-STATUS")
            lblTotalCustomers.Text = _objXmlTr.GetLabelName("EngLabelMsg", "BB-EXPORTEMAILTOXML-TOTALCUST")
        End Sub

        ''' <summary>
        ''' Populates the following drop down list boxes:
        ''' * Service center
        ''' * Model
        ''' * Customer type
        ''' * Race
        ''' * Status
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub PopulateDropdown()

            _clsrptcust.userid = Session("userID").ToString
            Dim rlconfirminf As New clsrlconfirminf()

            Dim ds As DataSet = _clsrptcust.SVCnStatebyUserID()
            Dim dtSvcCtr As DataTable = ds.Tables(1)
            Dim dtState As DataTable = ds.Tables(0)
            Dim dtArea As DataTable = ds.Tables(2)


            ' service center
            minsercenter.Items.Clear()
            maxsercenter.Items.Clear()

            minsercenter.DataSource = PopulateListItems(dtSvcCtr, New ListItem(_objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), "%"))
            minsercenter.DataTextField = "Text"
            minsercenter.DataValueField = "Value"
            minsercenter.DataBind()

            maxsercenter.DataSource = PopulateListItems(dtSvcCtr, New ListItem(_objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), "%"))
            maxsercenter.DataTextField = "Text"
            maxsercenter.DataValueField = "Value"
            maxsercenter.DataBind()

            cboFromStateID.Items.Clear()
            cboToStateID.Items.Clear()
            cboFromAreaID.Items.Clear()
            cboToAreaID.Items.Clear()


            cboFromStateID.DataSource = PopulateListItems(dtState, New ListItem(_objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), "%"))
            cboFromStateID.DataTextField = "Text"
            cboFromStateID.DataValueField = "Value"
            cboFromStateID.DataBind()

            cboToStateID.DataSource = PopulateListItems(dtState, New ListItem(_objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), "%"))
            cboToStateID.DataTextField = "Text"
            cboToStateID.DataValueField = "Value"
            cboToStateID.DataBind()

            cboFromAreaID.DataSource = PopulateListItems(dtArea, New ListItem(_objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), "%"))
            cboFromAreaID.DataTextField = "Text"
            cboFromAreaID.DataValueField = "Value"
            cboFromAreaID.DataBind()

            cboToAreaID.DataSource = PopulateListItems(dtArea, New ListItem(_objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), "%"))
            cboToAreaID.DataTextField = "Text"
            cboToAreaID.DataValueField = "Value"
            cboToAreaID.DataBind()

            ' model
            _clsCommon.spctr = CType(Session("login_ctryID"), String)
            _clsCommon.spstat = ""
            _clsCommon.sparea = ""
            _clsCommon.rank = ""

            Dim modelDT As DataTable = _clsCommon.Getcomidname("BB_MASMOTY_IDNAME").Tables(0)

            cboFromModel.Items.Clear()
            cboFromModel.DataSource = PopulateListItems(modelDT, New ListItem(_objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), ""))
            cboFromModel.DataTextField = "Text"
            cboFromModel.DataValueField = "Value"
            cboFromModel.DataBind()

            cboToModel.Items.Clear()
            cboToModel.DataSource = PopulateListItems(modelDT, New ListItem(_objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), ""))
            cboToModel.DataTextField = "Text"
            cboToModel.DataValueField = "Value"
            cboToModel.DataBind()

            ' customer type
            Dim custParam As ArrayList = rlconfirminf.searchconfirminf("CUSTTYPE")
            Dim custcount As Integer
            Dim custid As String
            Dim custnm As String
            cboCustomerType.Items.Add(New ListItem("All", "All"))
            For custcount = 0 To (custParam.Count / 2) - 1
                Dim rec As Integer = custcount * 2
                custid = CType(custParam.Item(rec), String)
                custnm = CType(custParam.Item(rec + 1), String)
                cboCustomerType.Items.Add(New ListItem(custnm.ToString(), custid.ToString()))
            Next

            ' race
            Dim raceParam As ArrayList = rlconfirminf.searchconfirminf("PERSONRACE")
            Dim racecount As Integer
            Dim raceid As String
            Dim racenm As String
            cboRace.Items.Add(New ListItem("All", "All"))
            For racecount = 0 To (raceParam.Count / 2) - 1
                Dim rec As Integer = racecount * 2
                raceid = CType(raceParam.Item(rec), String)
                racenm = CType(raceParam.Item(rec + 1), String)
                cboRace.Items.Add(New ListItem(racenm.ToString(), raceid.ToString()))
            Next

            ' status
            Dim param As ArrayList = rlconfirminf.searchconfirminf("STATUS")
            Dim count As Integer
            Dim statid As String
            Dim statnm As String
            For count = 0 To (param.Count / 2) - 1
                Dim rec As Integer = count * 2
                statid = CType(param.Item(rec), String)
                statnm = CType(param.Item(rec + 1), String)
                If statid.Equals("ACTIVE") Or statid.Equals("OBSOLETE") Then
                    cboStatus.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                End If
            Next
        End Sub

        ''' <summary>
        ''' Creates a list item collection to be added to the dropdown list box
        ''' </summary>
        ''' <param name="dt">Data Table</param>
        ''' <param name="defaultList">List item to be displayed in index 0</param>
        ''' <returns>List item collection to be added to the drop down list box</returns>
        ''' <remarks></remarks>
        Private Function PopulateListItems(ByVal dt As DataTable, ByVal defaultList As ListItem) As ListItemCollection
            Dim dropdownLists As New ListItemCollection()
            
            For i As Integer = 0 To dt.Rows.Count - 1
                dropdownLists.Add(New ListItem(CType((dt.Rows(i)(0) + "-" + dt.Rows(i)(1)), String), dt.Rows(i)(0).ToString()))
            Next

            dropdownLists.Insert(0, defaultList)

            Return dropdownLists
        End Function


        Protected Sub minsercenter_OnSelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
            Dim dtState As DataTable
            Dim dtArea As DataTable

            cboFromStateID.Items.Clear()
            cboFromAreaID.Items.Clear()

            If Not minsercenter.SelectedValue = "%" Then
                _clsrptcust.Minsvcid = minsercenter.SelectedValue
                _clsrptcust.ctryid = Session("login_ctryID").ToString().ToUpper()

                Dim ds As DataSet = _clsrptcust.getStateAndArea()
                dtState = ds.Tables(0)
                dtArea = ds.Tables(1)
            Else
                _clsrptcust.userid = Session("userID").ToString()

                Dim ds As DataSet = _clsrptcust.SVCnStatebyUserID()
                dtState = ds.Tables(0)
                dtArea = ds.Tables(2)

            End If

            cboFromStateID.DataSource = PopulateListItems(dtState, New ListItem(_objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), "%"))
            cboFromStateID.DataTextField = "Text"
            cboFromStateID.DataValueField = "Value"
            cboFromStateID.DataBind()

            cboFromAreaID.DataSource = PopulateListItems(dtArea, New ListItem(_objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), "%"))
            cboFromAreaID.DataTextField = "Text"
            cboFromAreaID.DataValueField = "Value"
            cboFromAreaID.DataBind()

        End Sub

        Protected Sub maxsercenter_OnSelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
            Dim dtState As DataTable
            Dim dtArea As DataTable

            cboToStateID.Items.Clear()
            cboToAreaID.Items.Clear()

            If Not maxsercenter.SelectedValue = "%" Then
                _clsrptcust.Minsvcid = maxsercenter.SelectedValue
                _clsrptcust.ctryid = Session("login_ctryID").ToString().ToUpper()

                Dim ds As DataSet = _clsrptcust.getStateAndArea()
                dtState = ds.Tables(0)
                dtArea = ds.Tables(1)
            Else
                _clsrptcust.userid = Session("userID").ToString()

                Dim ds As DataSet = _clsrptcust.SVCnStatebyUserID()
                dtState = ds.Tables(0)
                dtArea = ds.Tables(2)
            End If

            cboToStateID.DataSource = PopulateListItems(dtState, New ListItem(_objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), "%"))
            cboToStateID.DataTextField = "Text"
            cboToStateID.DataValueField = "Value"
            cboToStateID.DataBind()

            cboToAreaID.DataSource = PopulateListItems(dtArea, New ListItem(_objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), "%"))
            cboToAreaID.DataTextField = "Text"
            cboToAreaID.DataValueField = "Value"
            cboToAreaID.DataBind()

        End Sub

        Protected Sub lnkViewReport_Click(ByVal sender As Object, ByVal e As EventArgs)
            _exportEmailToXLS.FromServCenter = minsercenter.SelectedValue
            _exportEmailToXLS.ToServCenter = maxsercenter.SelectedValue
            _exportEmailToXLS.FromStateId = cboFromStateID.SelectedValue
            _exportEmailToXLS.ToStateId = cboToStateID.SelectedValue
            _exportEmailToXLS.FromAreaId = cboFromAreaID.SelectedValue
            _exportEmailToXLS.ToAreaId = cboToAreaID.SelectedValue
            _exportEmailToXLS.FromModelId = cboFromModel.SelectedValue
            _exportEmailToXLS.ToModelId = cboToModel.SelectedValue
            _exportEmailToXLS.CustType = cboCustomerType.SelectedValue
            _exportEmailToXLS.Race = cboRace.SelectedValue
            _exportEmailToXLS.Status = cboStatus.SelectedValue

            _exportEmailToXLS.GetCustomerEmails()

            txtTotalCustomers.Text = _exportEmailToXLS.TotalCust.ToString()

            Session("dataTable") = _exportEmailToXLS.ExportEmailDataTable

            lnkExport.Visible = True

            ' Added by Ryan Estandarte 9 April 2012
            lnkExportWord.Visible = True
            lnkExportNotepad.Visible = True
        End Sub


        Protected Sub lnkExport_Click(ByVal sender As Object, ByVal e As EventArgs)
            Dim exportToXLS As New ClsExportPRFtoXLS()
            Dim dataTable As DataTable = DirectCast(Session("dataTable"), DataTable)

            Dim dataSet As New DataSet

            dataSet.Tables.Add(dataTable.Copy())

            exportToXLS.Convert(dataSet, Response)

        End Sub

        Protected Sub cboFromStateID_OnSelectedIndexChanged (ByVal sender As Object, ByVal e As EventArgs)
            cboFromAreaID.Items.Clear()

            If cboFromStateID.SelectedValue <> "%" Then
                _clsrptcust.Minsvcid = minsercenter.SelectedValue
                _clsrptcust.Minstat = cboFromStateID.SelectedValue
            Else
                _clsrptcust.Minsvcid = minsercenter.SelectedValue
                _clsrptcust.Minstat = "%"
            End If

            Dim ds As DataSet = _clsrptcust.getAreafromState()

            cboFromAreaID.DataSource = PopulateListItems(ds.Tables(0), New ListItem(_objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), "%"))
            cboFromAreaID.DataTextField = "Text"
            cboFromAreaID.DataValueField = "Value"
            cboFromAreaID.DataBind()

        End Sub

        Protected Sub cboToStateID_OnSelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
            cboToAreaID.Items.Clear()

            If cboToStateID.SelectedValue <> "%" Then
                _clsrptcust.Minsvcid = maxsercenter.SelectedValue
                _clsrptcust.Minstat = cboToStateID.SelectedValue
            Else
                _clsrptcust.Minsvcid = maxsercenter.SelectedValue
                _clsrptcust.Minstat = "%"
            End If

            Dim ds As DataSet = _clsrptcust.getAreafromState()

            cboToAreaID.DataSource = PopulateListItems(ds.Tables(0), New ListItem(_objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), "%"))
            cboToAreaID.DataTextField = "Text"
            cboToAreaID.DataValueField = "Value"
            cboToAreaID.DataBind()

        End Sub

        ''' <remarks>Added by Ryan Estandarte 9 April 2012</remarks>
        Protected Sub lnkExportWord_Click(ByVal sender As Object, ByVal e As EventArgs)
            Dim exportToWord As New ClsExportEmailToXLS()
            Dim dataTable As DataTable = DirectCast(Session("dataTable"), DataTable)

            exportToWord.ExportToMSWord(dataTable, Response)

        End Sub

        ''' <remarks>Added by Ryan Estandarte 9 April 2012</remarks>
        Protected Sub lnkExportNotePad_Click(ByVal sender As Object, ByVal e As EventArgs)
            Dim exportToNotepad As New ClsExportEmailToXLS()
            Dim dataTable As DataTable = DirectCast(Session("dataTable"), DataTable)

            exportToNotepad.ExportToNotepad(dataTable, Response)
        End Sub
    End Class
End Namespace