Imports System.Data
Imports System.Data.SqlClient
Imports System.Globalization
Imports SQLDataAccess
Imports System.Windows
Imports Microsoft.Win32
Partial Class PresentationLayer_report_UserMatrix_View2
    Inherits System.Web.UI.Page
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)
        Return connection
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim _con As SqlConnection
        _con = GetConnection(ConfigurationManager.AppSettings("ConnectionString"))

        Dim _spName As String
        _spName = "dbo.UserMatrix"

        Dim dr1 As SqlDataReader = SqlHelper.ExecuteReader(_con, CommandType.StoredProcedure, _spName)

        Dim dt1 As DataTable = New DataTable()
        dt1.Load(dr1)

        GridView1.DataSource = dt1
        GridView1.DataBind()

        GridView1.Visible = True

        _con.Close()
    End Sub
    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub

    Protected Sub Export_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Export.Click
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=GridView1.xls")
        Response.Charset = ""
        Response.ContentType = "application/vnd.xls"
        Dim StringWriter As IO.StringWriter = New System.IO.StringWriter()
        Dim HtmlTextWriter As New HtmlTextWriter(StringWriter)
        Dim style As String = "<style> td { mso-number-format:\@; } </style> "

        GridView1.RenderControl(HtmlTextWriter)
        Response.Write(style) 'style is added dynamically
        Response.Write(StringWriter.ToString())
        Response.[End]()
    End Sub
End Class
