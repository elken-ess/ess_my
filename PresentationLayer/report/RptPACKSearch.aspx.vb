Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Web
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web
Imports BusinessEntity
Imports System.Data
Partial Class PresentationLayer_report_RptPACKSearch
    Inherits System.Web.UI.Page
    Dim clsReport As New ClsCommonReport

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try

        Dim packEntity As New ClsRptPack

        If Trim(Session("userID")) = "" Then
            packEntity.ModifiedBy = "ADMIN"
        Else
            packEntity.ModifiedBy = Session("userID")
        End If
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        packEntity = Session("rptpack_search_condition")
        Dim ds As DataSet = Nothing
        ds = packEntity.GetPack()
      
        Dim objXmlTr As New clsXml


        Dim strHead As Array = Array.CreateInstance(GetType(String), 42)
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTPACK-titlelab")
        'HyperLink1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Cancel")
        strHead(0) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTPACKHD0")
        strHead(1) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTPACKHD1")
        strHead(2) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTPACKHD2")
        strHead(3) = objXmlTr.GetLabelName("EngLabelMsg", "BB_PKLQT")
        strHead(4) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTPACKHD4")
        strHead(5) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTPACKHD5")
        strHead(6) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTPACKHD6")

        strHead(7) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTPACKHD7")
        strHead(8) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTPACKHD8")
        strHead(9) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTPACKHD9")
        strHead(10) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTPACKHD10")
        strHead(11) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTPACKHD11") + "" & Session("userID").ToString().ToUpper + "/" + Session("username").ToString.ToUpper
        strHead(12) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTPACKHD12")
        strHead(13) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTPACKHD13")

        strHead(14) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTPACK-0001")
        strHead(15) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTPACK-0002")
        strHead(16) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTPACK-0003")
        strHead(17) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTPACK-0004")
        strHead(18) = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
        strHead(19) = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
        If Trim(packEntity.statemin) <> "" Then
            strHead(20) = packEntity.statemin
        Else
            strHead(20) = "AAA".ToString
        End If
        If Trim(packEntity.sercidmin) <> "" Then
            strHead(21) = packEntity.sercidmin
        Else
            strHead(21) = "AAA".ToString
        End If
        If Trim(packEntity.datemin) <> "" Then
            strHead(22) = packEntity.datemin
        Else
            strHead(22) = " ".ToString
        End If
        If Trim(packEntity.tecnicianidmin) <> "" Then
            strHead(23) = packEntity.tecnicianidmin
        Else
            strHead(23) = "AAA".ToString
        End If
        If Trim(packEntity.stateend) <> "ZZZZZZZZZZZ" Then
            strHead(24) = packEntity.stateend
        Else
            strHead(24) = "ZZZ".ToString
        End If
        If Trim(packEntity.sercidend) <> "ZZZZZZZZZZZ" Then
            strHead(25) = packEntity.sercidend
        Else
            strHead(25) = "ZZZ".ToString
        End If
        If Trim(packEntity.dateend) <> "" Then
            strHead(26) = packEntity.dateend
        Else
            strHead(26) = " ".ToString
        End If
        If Trim(packEntity.tecnicianidend) <> "ZZZZZZZZZZZ" Then
            strHead(27) = packEntity.tecnicianidend
        Else
            strHead(27) = "ZZZ".ToString
        End If
        'BB-FUN-STOCK-14
        strHead(28) = objXmlTr.GetLabelName("EngLabelMsg", "BB_PRFQT")

        strHead(29) = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CRYSTALREP-11") ' request by
        strHead(30) = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CRYSTALREP-12") 'Cheked by
        strHead(31) = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CRYSTALREP-13") 'Approved by
        strHead(32) = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CRYSTALREP-14") 'Store Keeper
        strHead(33) = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CRYSTALREP-15") 'Received by
        strHead(34) = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CRYSTALREP-06") 'Name

        strHead(35) = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_F_PARTCODE")

        If Trim(packEntity.PartCodeFrom) <> "" Then
            strHead(36) = packEntity.PartCodeFrom
        Else
            strHead(36) = "AAA".ToString
        End If

        If Trim(packEntity.PartCodeTo) <> "ZZZZZZZZZZZ" Then
            strHead(37) = packEntity.PartCodeTo
        Else
            strHead(37) = "ZZZ".ToString
        End If

        strHead(38) = objXmlTr.GetLabelName("EngLabelMsg", "PRF_COLLECTIONDATE")
        strHead(39) = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-STOCK-07")

        strHead(40) = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-STOCK-08")

        'strHead(28) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTPACKHD14")
        'strHead(29) = Convert.ToString(packEntity.datemin) + "To" + Convert.ToString(packEntity.dateend)
        'strHead(30) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTUSER") 'add user id user name
        'strHead(31) = Session("userID") + "/" + Session("username")  'add user id user name
        'If (ds.Tables.Count <> 0) Then
        '    Dim strtotal As String = objXmlTr.GetLabelName("EngLabelMsg", "RPT_APT_RECORD")
        '    strHead(32) = strtotal & ": " & ds.Tables(0).Rows.Count
        'End If
        'strHead(33) = "/"


        'With clsReport
        '    .ReportFileName = "CrystPack.rpt"  'TextBox1.Text
        '    .SetReport(CrystalReportViewer1, ds, strHead)

        'End With
        Dim lstrPdfFileName As String = "PackingListReport_" & Session("login_session") & ".pdf"

        Try
            With clsReport
                .ReportFileName = "CrystPack.rpt"
                .SetReportDocument(ds, strHead)
                .PdfFileName = lstrPdfFileName
                .ExportPdf()
            End With

            Response.Redirect(clsReport.PdfUrl)

        Catch err As Exception
            'System.IO.File.Delete(lstrPhysicalFile)
            Response.Write("<BR>")
            Response.Write(err.Message.ToString)

        End Try

    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        clsReport.UnloadReport()
        CrystalReportViewer1.Dispose()
    End Sub
End Class
