
Imports System.Data

Namespace PresentationLayer.report
    Partial Class PresentationLayer_report_RPTPRFSummary_VIEW
        Inherits Page

#Region "Declaration"
        Private _prfDataTable As New DataTable("PRF")
        Private _prf As New ClsPrfSummary
        Private _report As New ClsCommonReport
#End Region
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
            Dim datestyle As Globalization.CultureInfo = New Globalization.CultureInfo("en-CA")
            Threading.Thread.CurrentThread.CurrentCulture = datestyle

            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Response.Redirect("~/PresentationLayer/logon.aspx")
                End If
            Catch ex As Exception
                Response.Redirect("~/PresentationLayer/logon.aspx")
            End Try

            Dim dt As DataTable = CType(Session("PRF"), DataTable)

            If dt.Rows.Count > 0 Then
                _prfDataTable = GenerateResultSet(dt)'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' datatable
            End If

            Dim strHead As String() = CType(Array.CreateInstance(GetType(String), 3), String())
            strHead(0) = _prf.IsoDesc1 ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' ClsPrfSummary
            strHead(1) = _prf.IsoDesc2 ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' ClsPrfSummary
            strHead(2) = _prf.Col1 ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' ClsPrfSummary

            'If _prfDataTable.Rows.Count > 0 Then

            Dim ds As New DataSet
            ds.Tables.Add(_prfDataTable) ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' datatable

            Dim reportName As String = "PRFSummary_" + Session("login_session").ToString() + "_" + DateTime.Now.ToString("ddMMyyyyhhmm") + ".pdf"

            With _report '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' ClsCommonReport
                .ReportFileName = "PRFSummary.rpt"
                .SetReportDocument(ds, strHead)
                .PdfFileName = reportName
                .ExportPdf()
            End With
            Response.Redirect(_report.PdfUrl) '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' ClsCommonReport
            'Else
            'Response.Redirect("empty.aspx")
            'End If

            

        End Sub

        Private Function GenerateResultSet(ByVal dt As DataTable) As DataTable
            Dim newPRF As New DataTable("PRFReport")
            newPRF.Columns.Add("SVBIL_LIST")
            newPRF.Columns.Add("FIV1_SVCID")
            newPRF.Columns.Add("FIV1_TCHID")
            newPRF.Columns.Add("MTCH_ENAME")
            newPRF.Columns.Add("FIV2_PARID")
            newPRF.Columns.Add("MPAR_ENAME")
            newPRF.Columns.Add("FIV1_PRFNO")
            newPRF.Columns.Add("FIV1_PRFPRNCNT")
            newPRF.Columns.Add("FIV1_PRFUSER")
            newPRF.Columns.Add("FIV1_PRFDT")
            newPRF.Columns.Add("WARR_PRICE")
            newPRF.Columns.Add("NONWARR_PRICE")
			    REM 'TODO: pass values to ClsRptReprintPRF
                _prf.TechId = GetIDs(dt.Rows(0)("Technician").ToString()) ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' ClsPrfSummary
                _prf.SvcId = GetIDs(dt.Rows(0)("ServiceCenter").ToString()) ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' ClsPrfSummary
                _prf.PrfNo = dt.Rows(0)("Prf").ToString() ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' ClsPrfSummary
                _prf.CtryId = Session("login_ctryID").ToString.ToUpper ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' ClsPrfSummary
                _prf.LoggedUser = Session("userID").ToString() ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' ClsPrfSummary
                REM 'TODO: generate datatable
                REM Dim dr As DataRow() = _prf.GetPack() ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' ClsPrfSummary
				
                REM 'TODO: retrieve row and add to _prfDataTable ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' datatable
                REM For Each row As DataRow In dr
                    REM newPRF.ImportRow(row)
                REM Next

            For i As Integer = 1 To dt.Rows.Count - 1
                'TODO: pass values to ClsRptReprintPRF
                _prf.TechId = _prf.TechId + "','" + Left(dt.Rows(i)("Technician").ToString(), 6) 'GetIDs(dt.Rows(i)("Technician").ToString()) ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' ClsPrfSummary
                _prf.SvcId = _prf.SvcId + "','" + Left(dt.Rows(i)("ServiceCenter").ToString(), 3) 'GetIDs(dt.Rows(i)("ServiceCenter").ToString()) ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' ClsPrfSummary
                _prf.PrfNo = _prf.PrfNo + "','" + dt.Rows(i)("Prf").ToString() ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' ClsPrfSummary
                REM _prf.CtryId = Session("login_ctryID").ToString.ToUpper ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' ClsPrfSummary
                REM _prf.LoggedUser = Session("userID").ToString() ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' ClsPrfSummary
                'TODO: generate datatable
			Next
            Dim dr As DataRow() = _prf.GetPack() ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' ClsPrfSummary

            If dr Is Nothing Then
                Response.Redirect("empty.aspx")
            End If

            'TODO: retrieve row and add to _prfDataTable ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' datatable
            For Each row As DataRow In dr
                newPRF.ImportRow(row)
            Next

            Return newPRF
        End Function

        Private Function GetIDs(ByVal value As String) As String
            Dim values As String() = value.Split(CType(" - ", Char))
            Return values(0)
        End Function

        Protected Sub Page_Unload(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Unload
            _report.UnloadReport() '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' ClsCommonReport
            CrystalReportViewer1.Dispose()
        End Sub
    End Class
End Namespace