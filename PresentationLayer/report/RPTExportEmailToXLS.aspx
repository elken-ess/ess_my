<%@ Page Language="VB" AutoEventWireup="false" CodeFile="RPTExportEmailToXLS.aspx.vb" ViewStateEncryptionMode="Always"
    Inherits="PresentationLayer.report.PresentationLayer_report_RPTExportEmailToXLS" %>

<%--
'******************************************************************************
' History
' -----------------------------------------------------------------------------
' Date(YYMMDD)  ModBy   	        Remarks
' =============================================================================
' 120217        Ryan Estandarte		Created
'******************************************************************************
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Export Email to Excel</title>
    <link href="../css/style.css" type="text/css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table id="countrytab" border="0" width="100%">
            <tr>
                <td style="width: 100%">
                    <table style="border: 0; padding: 0; width: 100%; border-spacing: 0">
                        <tr>
                            <td background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title1.gif" width="5" />
                            </td>
                            <td background="../graph/title_bg.gif" class="style2" width="98%">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label>
                            </td>
                            <td align="LEFT" background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title_2.gif" width="5" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100%">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" class="style2" width="98%">
                                <font color="red">
                                    <asp:Label ID="errlab" runat="server" Text="Label" Visible="false"></asp:Label></font>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100%; height: 188px;">
                    <table id="country" bgcolor="#b7e6e6" border="0" cellpadding="2" cellspacing="1"
                        width="100%">
                        <tr bgcolor="#ffffff">
                            <td align="LEFT" style="width: 15%">
                                <asp:Label ID="lblServCenter" runat="server" Text="Lable"></asp:Label>
                            </td>
                            <td align="LEFT" style="width: 6%">
                                <asp:Label ID="Fromlab2" runat="server" Text="From"></asp:Label>
                            </td>
                            <td align="left" style="width: 27%">
                                <asp:DropDownList ID="minsercenter" runat="server" Width="70%" CssClass="textborder" AutoPostBack="true"
                                                  OnSelectedIndexChanged="minsercenter_OnSelectedIndexChanged"/>
                            </td>
                            <td align="LEFT" style="width: 6%">
                                <asp:Label ID="Label1" runat="server" Text="To" />
                            </td>
                            <td align="left" style="width: 27%; height: 30px;">
                                <asp:DropDownList runat="server" ID="maxsercenter" AutoPostBack="true" Width="70%" CssClass="textborder" 
                                                                 OnSelectedIndexChanged="maxsercenter_OnSelectedIndexChanged"/>
                            </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="LEFT" style="width: 15%; height: 30px;">
                                <asp:Label ID="lblStateID" runat="server" Text="Label"></asp:Label>
                            </td>
                            <td align="LEFT" style="width: 6%; height: 30px;">
                                <asp:Label ID="Fromlab3" runat="server" Text="From"></asp:Label>
                            </td>
                            <td align="left" style="width: 27%; height: 30px;">
                                <asp:DropDownList runat="server" ID="cboFromStateID" AutoPostBack="true" Width="70%"
                                    CssClass="textborder" OnSelectedIndexChanged="cboFromStateID_OnSelectedIndexChanged"/>
                            </td>
                            <td align="LEFT" style="width: 6%; height: 30px;">
                                <asp:Label ID="Tolab3" runat="server" Text="To"></asp:Label>
                            </td>
                            <td align="left" style="width: 27%; height: 30px;">
                                <asp:DropDownList runat="server" ID="cboToStateID" AutoPostBack="true" Width="70%"
                                    CssClass="textborder" OnSelectedIndexChanged="cboToStateID_OnSelectedIndexChanged"/>
                            </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="LEFT" style="width: 15%">
                                <asp:Label ID="lblAreaID" runat="server" Text="Label"></asp:Label>
                            </td>
                            <td align="LEFT" style="width: 6%">
                                <asp:Label ID="Fromlab4" runat="server" Text="From" Height="16px"></asp:Label>
                            </td>
                            <td align="left" style="width: 27%">
                                <asp:DropDownList runat="server" ID="cboFromAreaID" Width="70%" CssClass="textborder" />
                            </td>
                            <td align="LEFT" style="width: 6%">
                                <asp:Label ID="Tolab4" runat="server" Text="To" Height="16px"></asp:Label>
                            </td>
                            <td align="left" width="27%" style="height: 30px">
                                <asp:DropDownList runat="server" ID="cboToAreaID" Width="70%" CssClass="textborder" />
                            </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="LEFT" style="width: 15%">
                                <asp:Label ID="lblModel" runat="server" Text="Lable"></asp:Label>
                            </td>
                            <td align="LEFT" style="width: 6%">
                                <asp:Label ID="Fromlab5" runat="server" Text="From" Height="16px"></asp:Label>
                            </td>
                            <td align="left" style="width: 27%">
                                <asp:DropDownList runat="server" ID="cboFromModel" Width="70%" CssClass="textborder" />
                            </td>
                            <td align="LEFT" style="width: 6%">
                                <asp:Label ID="Tolab5" runat="server" Text="To" Height="16px"></asp:Label>
                            </td>
                            <td align="left" width="27%">
                                <asp:DropDownList runat="server" ID="cboToModel" Width="70%" CssClass="textborder" />
                            </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="LEFT" style="width: 15%">
                                <asp:Label ID="lblCustomerType" runat="server" Text="Lable" />
                            </td>
                            <td align="left" style="width: 27%" colspan="2">
                                <asp:DropDownList runat="server" ID="cboCustomerType" Width="70%" CssClass="textborder" />
                            </td>
                            <td align="LEFT" style="width: 6%">
                                <asp:Label ID="lblRace" runat="server" Text="" Height="16px" />
                            </td>
                            <td align="left" width="27%">
                                <asp:DropDownList runat="server" ID="cboRace" Width="70%" CssClass="textborder" />
                            </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td style="width: 15%; text-align: left">
                                <asp:Label ID="lblStatus" runat="server" Text="" />
                            </td>
                            <td align="LEFT" style="width: 6%" colspan="2">
                                <asp:DropDownList runat="server" ID="cboStatus" Width="70%" CssClass="textborder" />
                            </td>
                            <td align="LEFT" style="width: 6%">
                                &nbsp;
                            </td>
                            <td align="left" width="27%">
                                &nbsp;
                            </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="LEFT" style="width: 15%">
                                <asp:LinkButton runat="server" ID="lnkViewReport" OnClick="lnkViewReport_Click">View Report</asp:LinkButton>
                            </td>
                            <td align="LEFT" style="width: 6%" colspan="2">
                                &nbsp;
                            </td>
                            <td align="LEFT" style="width: 6%">
                                &nbsp;
                            </td>
                            <td align="left" width="27%">
                                &nbsp;
                            </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="LEFT" style="width: 15%">
                                <asp:Label runat="server" ID="lblTotalCustomers" Text=""></asp:Label>
                            </td>
                            <td align="LEFT" style="width: 6%" colspan="2">
                                <asp:TextBox runat="server" ID="txtTotalCustomers" ReadOnly="true" CssClass="textborder" />
                            </td>
                            <td align="LEFT" style="width: 6%">
                                &nbsp;
                            </td>
                            <td align="left" width="27%">
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                    <asp:LinkButton ID="lnkExport" runat="server" OnClick="lnkExport_Click" Visible="false">Export To Excel</asp:LinkButton><br/>
                    <asp:LinkButton ID="lnkExportWord" runat="server" OnClick="lnkExportWord_Click" Visible="false">Export To MS Word</asp:LinkButton><br/>
                    <asp:LinkButton ID="lnkExportNotepad" runat="server" OnClick="lnkExportNotePad_Click" Visible="false">Export To Notepad</asp:LinkButton>
                    
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>