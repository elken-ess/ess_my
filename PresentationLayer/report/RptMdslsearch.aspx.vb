Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Web
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web
Imports BusinessEntity
Imports System.Data

Imports System.Threading
Imports System
Imports System.Web.UI


Partial Class PresentationLayer_report_RptMdslsearch_aspx
    Inherits System.Web.UI.Page
    Dim clsReport As New ClsCommonReport

    Private cmd As SqlCommand
    Private cnx As SqlConnection

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Buffer = True
        Response.Expires = 10000

        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try


        ViewReportWithOriginalMethod()
       
    End Sub


    Sub ViewReportWithOriginalMethod()

        Dim lstrDebugMsg As String = ""

        Try
            lstrDebugMsg = "Start"

            Dim mdslEntity As New ClsRptMdsl
            If Trim(Session("userID")) = "" Then
                mdslEntity.uname = "ADMIN"
            Else
                mdslEntity.uname = Session("userID")
            End If


            lstrDebugMsg = "Assign Entity from session"
            mdslEntity = Session("rptmdsl_search_condition")
            Session.Remove("rptmdsl_search_condition")

            lstrDebugMsg = "Get Report Label"

            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTMDSL-titlelab")

            Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle


            lstrDebugMsg = "Assign Data from session"
            Dim ds As New DataSet

            '-------------------------------------------------------------------------------------------------------------


            ds = mdslEntity.Getmodeldue()

            ' switch  encrypt to telphone for display
            Dim count As Integer
            Dim prdctclname As String
            Dim clscomm As New clsCommonClass()
            Dim statXmlTr As New clsXml
            Dim strPanm As String
            Dim strcontpar As String
            statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")

            lstrDebugMsg = "Start Tel Conversion"

            Dim lstrNw, lstrUA, lstrSA, lstrUM, lstrTG, lstrSM, lstrBL, lstrX As String
            Dim lstrNwLabel, lstrUALabel, lstrSALabel, lstrUMLabel, lstrTGLabel, lstrSMLabel, lstrBLLabel, lstrXLabel As String

            lstrNw = "NW"
            lstrUA = "UA"
            lstrSA = "SA"
            lstrUM = "UM"
            lstrTG = "TG"
            lstrSM = "SM"
            lstrBL = "BL"
            lstrX = "X"

            lstrNwLabel = statXmlTr.GetLabelName("StatusMessage", lstrNw)
            lstrUALabel = statXmlTr.GetLabelName("StatusMessage", lstrUA)
            lstrSALabel = statXmlTr.GetLabelName("StatusMessage", lstrSA)
            lstrUMLabel = statXmlTr.GetLabelName("StatusMessage", lstrUM)
            lstrTGLabel = statXmlTr.GetLabelName("StatusMessage", lstrTG)
            lstrSMLabel = statXmlTr.GetLabelName("StatusMessage", lstrSM)
            lstrBLLabel = statXmlTr.GetLabelName("StatusMessage", lstrBL)
            lstrXLabel = statXmlTr.GetLabelName("StatusMessage", lstrX)


            If Not ds Is Nothing Then

                If ds.Tables(0).Rows.Count > 0 Then
                    For count = 0 To ds.Tables(0).Rows.Count - 1
                        Dim statusid As String = ds.Tables(0).Rows(count).Item(4).ToString 'item(4) is status
                        'strPanm = statXmlTr.GetLabelName("StatusMessage", statusid)

                        If statusid = lstrNw Then
                            strPanm = lstrNwLabel

                        ElseIf statusid = lstrUA Then
                            strPanm = lstrUALabel

                        ElseIf statusid = lstrSA Then
                            strPanm = lstrSALabel

                        ElseIf statusid = lstrUM Then
                            strPanm = lstrUMLabel

                        ElseIf statusid = lstrTG Then
                            strPanm = lstrTGLabel

                        ElseIf statusid = lstrSM Then
                            strPanm = lstrSMLabel

                        ElseIf statusid = lstrBL Then
                            strPanm = lstrBLLabel

                        ElseIf statusid = lstrX Then
                            strPanm = lstrXLabel

                        Else
                            strPanm = ""
                        End If

                        ds.Tables(0).Rows(count).Item(4) = strPanm


                        'Dim prdctclsid As String = ds.Tables(0).Rows(count).Item(5).ToString
                        'If prdctclsid <> "" Then
                        '    prdctclname = clscomm.passconverttel(prdctclsid)
                        '    If prdctclname <> "" Then
                        '        ds.Tables(0).Rows(count).Item(5) = prdctclname
                        '    End If
                        'End If
                    Next
                End If
            End If

            'LOOP 2ND TABLE
            If Not ds Is Nothing Then
                If ds.Tables(1).Rows.Count > 0 Then
                    For count = 0 To ds.Tables(1).Rows.Count - 1
                        Dim prdctclsid As String = ds.Tables(1).Rows(count).Item(1).ToString
                        If prdctclsid <> "" Then
                            prdctclname = clscomm.passconverttel(prdctclsid)
                            If prdctclname <> "" Then
                                ds.Tables(1).Rows(count).Item(1) = prdctclname
                            End If
                        End If
                    Next
                End If
            End If


            '-------------------------------------------------------------------------------------------------------------

            lstrDebugMsg = "Start Report Label"

            Dim strHead As Array = Array.CreateInstance(GetType(String), 36)
            ''HyperLink1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Cancel")
            strHead(0) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTMDSLHD0")
            strHead(1) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTMDSLHD1")
            strHead(2) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTMDSLHD2")
            strHead(3) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTMDSLHD3")
            strHead(4) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTMDSLHD4")
            strHead(5) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTMDSLHD5")
            strHead(6) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTMDSLHD6")

            strHead(7) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTMDSLHD7")
            'strHead(8) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTMDSLHD8")'set company
            strHead(8) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTMDSLHD8")
            strHead(9) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTMDSLHD9")
            strHead(10) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTMDSLHD10")
            strHead(11) = "  " + Convert.ToString(mdslEntity.datebegin) + " To " + Convert.ToString(mdslEntity.dateend)

            strHead(12) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTMDSL-0001")
            strHead(13) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTMDSL-0002")
            strHead(14) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTMDSL-0003")

            strHead(15) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTMDSL-0004")
            strHead(16) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTMDSL-0005")
            strHead(17) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTMDSL-0006")

            strHead(18) = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            strHead(19) = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")

            If Trim(mdslEntity.custmorid) <> "" Then
                strHead(20) = mdslEntity.custmorid
            Else
                strHead(20) = "AAA".ToString
            End If
            If Trim(mdslEntity.modelid) <> "" Then
                strHead(21) = mdslEntity.modelid
            Else
                strHead(21) = "AAA".ToString
            End If
            If Trim(mdslEntity.serialid) <> "" Then
                strHead(22) = mdslEntity.serialid
            Else
                strHead(22) = "AAA".ToString
            End If
            If Trim(mdslEntity.state) <> "" Then
                strHead(23) = mdslEntity.state
            Else
                strHead(23) = "AAA".ToString
            End If
            If Trim(mdslEntity.datebegin) <> "" Then
                strHead(24) = mdslEntity.datebegin
            Else
                strHead(24) = " ".ToString
            End If
            If Trim(mdslEntity.sercid) <> "" Then
                strHead(25) = mdslEntity.sercid
            Else
                strHead(25) = "AAA".ToString
            End If
            If Trim(mdslEntity.custmoridend) <> "ZZZZZZZZZZ" Then
                strHead(26) = mdslEntity.custmoridend
            Else
                strHead(26) = "ZZZ".ToString
            End If
            If Trim(mdslEntity.modelidend) <> "ZZZZZZZZZZ" Then
                strHead(27) = mdslEntity.modelidend
            Else
                strHead(27) = "ZZZ".ToString
            End If
            If Trim(mdslEntity.serialidend) <> "ZZZZZZZZZZ" Then
                strHead(28) = mdslEntity.serialidend
            Else
                strHead(28) = "ZZZ".ToString
            End If
            If Trim(mdslEntity.stateend) <> "ZZZZZZZZZZ" Then
                strHead(29) = mdslEntity.stateend
            Else
                strHead(29) = "ZZZ".ToString
            End If
            If Trim(mdslEntity.dateend) <> "" Then
                strHead(30) = mdslEntity.dateend
            Else
                strHead(30) = " ".ToString
            End If
            If Trim(mdslEntity.sercidend) <> "ZZZZZZZZZZ" Then
                strHead(31) = mdslEntity.sercidend
            Else
                strHead(31) = "ZZZ".ToString
            End If
            strHead(32) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTUSER") 'add user id user name
            strHead(33) = Session("userID") + "/" + Session("username")  'add user id user name


            lstrDebugMsg = "Get record count"

            If Not ds Is Nothing Then
                If (ds.Tables.Count <> 0) Then
                    lstrDebugMsg = "Get record count 2"
                    Dim strtotal As String = objXmlTr.GetLabelName("EngLabelMsg", "RPT_APT_RECORD")
                    strHead(34) = strtotal & ": " & ds.Tables(0).Rows.Count
                End If
            End If
            strHead(35) = "/"


            Dim lstrPdfFileName As String = "ModelDueServiceList_" & Session("login_session") & ".pdf"

            If Not ds Is Nothing Then
                With clsReport
                    .ReportFileName = "CrystMDSL.rpt"
                    lstrDebugMsg = "Pass Data To Report"
                    .SetMdstReportDocument(ds, strHead)
                    .PdfFileName = lstrPdfFileName

                    lstrDebugMsg = "Export PDF Data"
                    .ExportPdf()
                End With

                Response.Redirect(clsReport.PdfUrl)
            Else
                'lstrDebugMsg = "Dataset Error"
                'Response.Write("Debug Mode Message: " & lstrDebugMsg)
                Response.Write("No Record Found, Dataset Empty")
            End If



        Catch err As Exception
            'System.IO.File.Delete(lstrPhysicalFile)
            Response.Write("<BR>")
            Response.Write("Error Message: " & err.Message.ToString)
            Response.Write("<BR>")
            Response.Write("Source : " & err.Source)
            Response.Write("<BR>")
            Response.Write("Stack Trace Message: " & err.StackTrace)

            Response.Write("<BR>")
            Response.Write("Debug Mode Message: " & lstrDebugMsg)


        End Try

    End Sub


    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        clsReport.UnloadReport()
        CrystalReportViewer1.Dispose()
    End Sub

    Protected Sub CrystalReportViewer1_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles CrystalReportViewer1.Init

    End Sub
End Class
