﻿
Imports System.Data
Imports BusinessEntity

Namespace PresentationLayer.report
    Partial Class PresentationLayer_report_RPTWarrParts
        Inherits Page
        'Implements IBasicReportPage

#Region "Declarations"
        Private ReadOnly _datestyle As Globalization.CultureInfo = New Globalization.CultureInfo("en-CA")
        Private ReadOnly _xml As New clsXml
        Private ReadOnly _common As New clsCommonClass()
        Private _stocklist As clsAdjTechStock
        Private _warr As New clsRptWarrParts()
#End Region

#Region "Report Page Implementation"
        Private Sub PopulateLabels() 'Implements IBasicReportPage.PopulateLabels
            Dim fromLabel As String = "From" '_xml.GetLabelName("EngLabelMsg", "BB-FROM")
            Dim toLabel As String = "To"'_xml.GetLabelName("EngLabelMsg", "BB-TO")

            titleLab.Text = "Warranty Parts Report"'_xml.GetLabelName("EngLabelMsg", "BB-WARRPARTS-TITLE")

            ServiceCenterLabel.Text = "Service Center"'_xml.GetLabelName("EngLabelMsg", "BB_FUN_D_CENTER")
            FromSvcCenterLabel.Text = fromLabel
            ToSvcCenterLabel.Text = toLabel

            ServiceDateLabel.Text = "Service Date"'_xml.GetLabelName("EngLabelMsg", "RPT_APT_SERDATE")
            FromSvcDateLabel.Text = fromLabel
            ToSvcDateLabel.Text = toLabel

            TechnicianLabel.Text = "Technician ID"'_xml.GetLabelName("EngLabelMsg", "BB-FUN-DETAIL-TCHID")
            FromTechLabel.Text = fromLabel
            ToTechLabel.Text = toLabel

            PriceIDLabel.Text = "Price ID"'_xml.GetLabelName("EngLabelMsg", "BB-MASPRID-0001")
            FromPriceIDLabel.Text = fromLabel
            ToPriceIDLabel.Text = toLabel
        End Sub

        Public Sub PopulateLabels(ByVal strHead As String()) 'Implements IBasicReportPage.PopulateLabels
            Throw New NotImplementedException()
        End Sub

        Private Sub GenerateReport() 'Implements IBasicReportPage.GenerateReport
            Dim fromSvcDate As DateTime
            Dim toSvcDate As DateTime

            DateTime.TryParse(FromSvcDateTextBox.Text, fromSvcDate)
            DateTime.TryParse(ToSvcDateTextBox.Text, toSvcDate)

            _warr.FromSvcId = FromSvcCenterDropdown.SelectedValue
            _warr.ToSvcId = ToSvcCenterDropdown.SelectedValue
            _warr.FromSvcDate = fromSvcDate
            _warr.ToSvcDate = toSvcDate
            _warr.FromTechId = FromTechDropDown.SelectedValue
            _warr.ToTechId = ToTechDropDown.SelectedValue
            _warr.FromPriceId = FromPriceIDDropDown.SelectedValue
            _warr.ToPriceId = ToPriceIDDropDown.SelectedValue

            Session("SvcFr") = _warr.FromSvcId
            Session("SvcTo") = _warr.ToSvcId
            Session("SvcDateFr") = _warr.FromSvcDate
            Session("SvcDateTo") = _warr.ToSvcDate
            Session("TechFr") = _warr.FromTechId
            Session("TechTo") = _warr.ToTechId
            Session("PriceFr") = _warr.FromPriceId
            Session("PriceTo") = _warr.ToPriceId

            Dim dt As New DataTable
            dt = _warr.RetrieveWarrantyParts().Tables(0)

            If dt.Rows.Count > 0 Then
                Session("DataTable") = dt
            Else
                Session("DataTable") = "X"
            End If

        End Sub

        Private Sub DefineDefaultValues() 'Implements IBasicReportPage.DefineDefaultValues
            FromSvcDateTextBox.Text = DateTime.Today.ToString("dd/MM/yyyy")
            ToSvcDateTextBox.Text = DateTime.Today.AddMonths(1).ToString("dd/MM/yyyy")

            Dim ds As New DataSet
            _common.rank = CType(Session("login_rank"), String)
            If CInt(_common.rank) <> 0 Then
                _common.spctr = Session("login_ctryID").ToString()
                _common.spstat = Session("login_cmpID").ToString()
                _common.sparea = Session("login_svcID").ToString()
            End If
            ds = _common.Getcomidname("BB_MASSVRC_IDNAME")
            BindDropDownItems(ds, FromSvcCenterDropdown)
            BindDropDownItems(ds, ToSvcCenterDropdown)

            _stocklist = New clsAdjTechStock(Session("userID").ToString())
            Dim dsPriceList As DataSet = _stocklist.GetPriceID(Session("Login_CtryID").ToString())
            'FromPriceIDDropDown.DataSource = dsPriceList
            'FromPriceIDDropDown.DataTextField = "MPRC_ENAME"
            'FromPriceIDDropDown.DataValueField = "MPRC_PRCID"
            'FromPriceIDDropDown.DataBind()
            BindDropDownItems(dsPriceList, FromPriceIDDropDown)

            'ToPriceIDDropDown.DataSource = dsPriceList
            'ToPriceIDDropDown.DataTextField = "MPRC_ENAME"
            'ToPriceIDDropDown.DataValueField = "MPRC_PRCID"
            'ToPriceIDDropDown.DataBind()
            BindDropDownItems(dsPriceList, ToPriceIDDropDown)

        End Sub

        Public Sub DefineDefaultValues(ByVal strHead As String()) 'Implements IBasicReportPage.DefineDefaultValues
            Throw New NotImplementedException()
        End Sub

        Private Sub GenerateReport(ByVal dataSet As DataSet) 'Implements IBasicReportPage.GenerateReport
            Throw New NotImplementedException()
        End Sub
#End Region

#Region "Private Methods"
        Private Sub PopulateTechnicians(ByVal svcID As String, ByVal dropDown As DropDownList)
            Dim ds As New DataSet
            _common.spctr = Session("login_ctryID").ToString().ToUpper
            _common.spstat = "ACTIVE"
            _common.rank = Session("login_rank").ToString()
            _common.sparea = svcID

            ds = _common.Getcomidname("dbo.BB_FNCAPPS_SelTechArea")

            BindDropDownItems(ds, dropDown)
        End Sub
        Private Sub BindDropDownItems(ByVal ds As DataSet, ByVal dropdown As DropDownList)
            dropdown.Items.Clear()
            Dim row As DataRow
            If (ds.Tables.Count > 0) Then
                For Each row In ds.Tables(0).Rows
                    Dim NewItem As New ListItem()
                    NewItem.Text = Trim(row(0).ToString()) & "-" & Trim(row(1).ToString())
                    NewItem.Value = row(0).ToString()
                    dropdown.Items.Add(NewItem)
                Next
            End If

            dropdown.Items.Insert(0, "")
        End Sub
#End Region

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
            Threading.Thread.CurrentThread.CurrentCulture = _datestyle
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Const script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Const script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try


            If Not Page.IsPostBack Then
                PopulateLabels()
                DefineDefaultValues()
            End If

            PopulateTechnicians(FromSvcCenterDropdown.SelectedValue, FromTechDropDown)
            PopulateTechnicians(ToSvcCenterDropdown.SelectedValue, ToTechDropDown)

            HypCalFrom.NavigateUrl = "javascript:DoCal(document.form1.FromSvcDateTextBox);"
            HypCalTo.NavigateUrl = "javascript:DoCal(document.form1.ToSvcDateTextBox);"
        End Sub

        Protected Sub saveButton_Click(ByVal sender As Object, ByVal e As EventArgs)
            GenerateReport()
            Const script As String = "window.open('../report/RPTWarrParts_VIEW1.aspx')"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "RPTWarrParts_VIEW1", script, True)
        End Sub
    End Class
End Namespace