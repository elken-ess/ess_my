Imports System
Imports System.Data
Imports System.Collections.Generic
Imports System.Configuration
Imports BusinessEntity

Partial Class PresentationLayer_report_RPTdebtor
    Inherits System.Web.UI.Page

#Region "Declarations"
    Private _datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
    Private _debtor As New clsDebtorReport
    Private _clsReport As New ClsCommonReport
    Private _errorLog As ArrayList = New ArrayList
    Private _writeErrLog As New clsLogFile()

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load

        Threading.Thread.CurrentThread.CurrentCulture = _datestyle
        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Const script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
        Catch ex As Exception
            Const script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try

        If (Not Page.IsPostBack) Then
            FromDateTxt.Text = System.DateTime.Today
            ToDateTxt.Text = System.DateTime.Today
            BindGrid()

            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Boolean()
            purviewArray = CType(clsUserAccessGroup.GetUserPurview(accessgroup, "60"), Boolean())
            lnkViewReport.Enabled = purviewArray(3)
            lnkViewReport.Enabled = "true"
            LoadDefaultValues()
        End If

        HypCalFrom.NavigateUrl = "javascript:DoCal(document.form1.FromDateTxt);"
        HypCalTo.NavigateUrl = "javascript:DoCal(document.form1.ToDateTxt);"
    End Sub

    Protected Sub BindGrid()

        Dim SVC As New DataSet
        Dim installSVC As New clsCommonClass
        installSVC.sparea = Session("login_svcID")
        installSVC.spctr = Session("login_ctryID")
        installSVC.spstat = Session("login_cmpID")
        installSVC.rank = Session("login_rank")

        SVC = installSVC.Getcomidname("BB_TOMAS_SVC")
        If SVC.Tables.Count <> 0 Then
            BindValuesToDropdown(SVC, Me.SVCFromDDL)
            BindValuesToDropdown(SVC, Me.SVCToDDL)
        End If

        Dim custtype As New clsrlconfirminf()
        Dim custParam As ArrayList = New ArrayList
        custParam = custtype.searchconfirminf("CUSTTYPE")
        Dim custcount As Integer
        Dim custid As String
        Dim custnm As String
        Me.CustomerTypeDDL.Items.Add(New ListItem("ALL", "ALL"))
        For custcount = 0 To custParam.Count - 1
            custid = custParam.Item(custcount)
            custnm = custParam.Item(custcount + 1)
            custcount = custcount + 1
            Me.CustomerTypeDDL.Items.Add(New ListItem(custnm.ToString(), custid.ToString()))

        Next

    End Sub


    Private Sub LoadDefaultValues()

        Me.lblNoRecord.Text = "No Record Found."
        Me.lblNoRecord.Visible = False

        Dim list1 As New Dictionary(Of String, String)()
        list1.Add("Outstanding Report", "Outstanding Report")
        list1.Add("Ageing Summary Report", "Ageing Summary Report")
        list1.Add("Reminder Letter", "Reminder Letter")

        ReportDropdown.DataSource = list1
        ReportDropdown.DataTextField = "Key"
        ReportDropdown.DataValueField = "Value"
        ReportDropdown.DataBind()

        '>30-60 days / >60 -90 days / >90 days
        Dim list2 As New Dictionary(Of String, String)()
        list2.Add(">30-60 days", "30")
        list2.Add(">60-90 days", "60")
        list2.Add(">90 days", "90")

        AgeingDDL.DataSource = list2
        AgeingDDL.DataTextField = "Key"
        AgeingDDL.DataValueField = "Value"
        AgeingDDL.DataBind()

        Me.FromDateTxt.Enabled = True
        Me.ToDateTxt.Enabled = True
        Me.CustomerTypeDDL.Enabled = True
        Me.SVCToDDL.Enabled = True
        Me.SVCFromDDL.Enabled = True
        Me.AgeingDDL.Enabled = False
        Me.lnkDebtorList.Visible = False
        Me.CheckAll.Visible = False
        Me.UncheckAll.Visible = False
        Me.lblNoRecord.Visible = False

    End Sub

    'Private Sub LoadLabels()
    '    Dim objXmlTr As New clsXml

    '    ' Load Labels
    '    objXmlTr.XmlFile = ConfigurationManager.AppSettings("XmlFilePath")

    '    Dim fromLabel As String = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
    '    Dim toLabel As String = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")

    '    ServCtrLabel.Text = "Service Center ID"
    '    FromServiceCtrLabel.Text = fromLabel
    '    ToServiceCtrLabel.Text = toLabel

    '    TechnicianLabel.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TECHID")
    '    FromTechIDLabel.Text = fromLabel
    '    ToTechIDLabel.Text = toLabel

    '    CustomerIDLabel.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CUSTID")
    '    FromCustLabel.Text = fromLabel
    '    ToCustLabel.Text = toLabel

    '    InvoiceDateLabel.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-03")
    '    FromInvoiceDateLabel.Text = fromLabel
    '    ToInvoiceDateLabel.Text = toLabel

    '    InvoiceNoLabel.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-INVOICENO")
    '    FromInvoiceNoLabel.Text = fromLabel
    '    ToInvoiceNoLabel.Text = toLabel

    '    ReportTypeLabel.Text = "Report Type" 'objXmlTr.GetLabelName("EngLabelMsg", "RPT-REPORTTYPE")
    'End Sub


    Protected Sub LnkViewReportClick(ByVal sender As Object, ByVal e As EventArgs)

        Dim startDate As DateTime = DateTime.ParseExact(Me.FromDateTxt.Text.ToString(), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture)
        Dim endDate As DateTime = DateTime.ParseExact(Me.ToDateTxt.Text.ToString(), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture)
        _debtor.StartInvDate = startDate.ToString("yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture)
        _debtor.EndInvDate = endDate.ToString("yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture)

        _debtor.CustType = Me.CustomerTypeDDL.SelectedValue.ToString()
        _debtor.ToSvcID = Me.SVCToDDL.SelectedValue.ToString()
        _debtor.FromSvcID = Me.SVCFromDDL.SelectedValue.ToString()

        Session("rptDebtor") = _debtor
        Dim script As String

        Select Case ReportDropdown.SelectedIndex
            Case 0
                script = "window.open('../report/RPTdebtor_VIEW.aspx?report=outstanding')"
            Case 1
                script = "window.open('../report/RPTdebtor_VIEW.aspx?report=ageing')"
            Case 2

            Case Else
                script = "window.open('../report/RPTdebtor_VIEW.aspx?report=outstanding')"
        End Select

        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "RPTdebtor_VIEW", script, True)
    End Sub

   
    Private Sub GetReportDetails()
        Dim fromSvcCtr As String = String.Empty
        Dim toSvcCtr As String = String.Empty

        'If FromServiceCtrTextbox.Text <> "AAA" Then
        '    fromSvcCtr = FromServiceCtrTextbox.Text
        'End If

        'If ToServiceCtrTextbox.Text <> "ZZZ" Then
        '    toSvcCtr = ToServiceCtrTextbox.Text
        'End If

        'If FromTechIDTextbox.Text <> "AAA" And ToTechIDTextbox.Text <> "ZZZ" Then
        '    If CompareIDValues(FromTechIDTextbox.Text.Substring(3, FromTechIDTextbox.Text.Length - 3), ToTechIDTextbox.Text.Substring(3, ToTechIDTextbox.Text.Length - 3)) Then
        '        _servBill.FromTechID = ToTechIDTextbox.Text
        '        _servBill.ToTechID = FromTechIDTextbox.Text
        '    Else
        '        _servBill.FromTechID = FromTechIDTextbox.Text
        '        _servBill.ToTechID = ToTechIDTextbox.Text
        '    End If
        'Else
        '    If FromTechIDTextbox.Text = "AAA" Then
        '        _servBill.FromTechID = String.Empty
        '    End If

        '    If ToTechIDTextbox.Text = "ZZZ" Then
        '        _servBill.ToTechID = String.Empty
        '    End If
        'End If

        'If FromCustIDTextbox.Text <> "AAA" And ToCustIDTextbox.Text <> "ZZZ" Then

        '    If CompareIDValues(FromCustIDTextbox.Text, ToCustIDTextbox.Text) Then
        '        _servBill.FromCustID = ToCustIDTextbox.Text
        '        _servBill.ToCustID = FromCustIDTextbox.Text
        '    Else
        '        _servBill.FromCustID = FromCustIDTextbox.Text
        '        _servBill.ToCustID = ToCustIDTextbox.Text
        '    End If
        'Else

        '    If FromCustIDTextbox.Text = "AAA" Then
        '        _servBill.FromCustID = String.Empty
        '    End If

        '    If ToCustIDTextbox.Text = "ZZZ" Then
        '        _servBill.ToCustID = String.Empty
        '    End If

        'End If

        'If FromInvoiceNoTextbox.Text = "AAA" Then
        '    _servBill.FromServBillNo = String.Empty
        'Else
        '    _servBill.FromServBillNo = FromInvoiceNoTextbox.Text
        'End If

        'If ToInvoiceNoTextbox.Text = "ZZZ" Then
        '    _servBill.ToServBillNo = String.Empty
        'Else
        '    _servBill.ToServBillNo = ToServiceCtrTextbox.Text
        'End If

        '_servBill.StartInvDate = FromInvoiceDateTextbox.Text
        '_servBill.EndInvDate = ToInvoiceDateTextbox.Text

        '_servBill.UserName = Session("userID").ToString()
        '_servBill.ReportType = ReportDropdown.SelectedValue

        'Dim rank As String = Session("login_rank").ToString()
        'Select Case rank
        '    Case "0"
        '        _servBill.CtryID = String.Empty
        '        _servBill.ComID = String.Empty
        '        _servBill.FromSvcID = fromSvcCtr
        '        _servBill.ToSvcID = toSvcCtr
        '    Case "7"
        '        _servBill.CtryID = Session("login_ctryID").ToString()
        '        _servBill.ComID = String.Empty
        '        _servBill.FromSvcID = fromSvcCtr
        '        _servBill.ToSvcID = toSvcCtr
        '    Case "8"
        '        _servBill.CtryID = Session("login_ctryID").ToString()
        '        _servBill.ComID = Session("login_cmpID").ToString()
        '        _servBill.FromSvcID = fromSvcCtr
        '        _servBill.ToSvcID = toSvcCtr
        '    Case "9"
        '        _servBill.CtryID = Session("login_ctryID").ToString()
        '        _servBill.ComID = Session("login_cmpID").ToString()
        '        _servBill.FromSvcID = Session("login_svcID").ToString()
        '        _servBill.ToSvcID = Session("login_svcID").ToString()
        'End Select

        'Session("rptServBill") = _servBill

    End Sub

    ''' <summary>
    ''' Compares if the fromString is greater than the toString
    ''' </summary>
    ''' <param name="fromString"></param>
    ''' <param name="toString"></param>
    ''' <returns></returns>
    ''' <remarks>Added by Ryan Estandarte 8 May 2012</remarks>
    Private Function CompareIDValues(ByVal fromString As String, ByVal toString As String) As Boolean
        Dim fromID As Integer = Integer.Parse(fromString)
        Dim toID As Integer = Integer.Parse(toString)

        If fromID > toID Then
            Return True
        End If

        Return False

    End Function

    Private Sub BindValuesToDropdown(ByVal dt As DataTable, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        dropdown.Items.Add(New ListItem("ALL", ""))

        Dim row As DataRow
        If dt.Rows.Count > 0 Then
            For Each row In dt.Rows
                Dim newItem As New ListItem()
                newItem.Text = Trim(row(0).ToString()) & "-" & row(1).ToString()
                newItem.Value = Trim(row(0).ToString())
                dropdown.Items.Add(newItem)
            Next
        End If
    End Sub

    Public Sub BindValuesToDropdown(ByVal ds As DataSet, ByVal dropdown As DropDownList)

        dropdown.Items.Clear()
        dropdown.Items.Add(New ListItem("ALL", ""))

        Dim row As DataRow
        If ds.Tables(0).Rows.Count > 0 Then
            dropdown.Items.Add(" ")
            For Each row In ds.Tables(0).Rows
                Dim NewItem As New ListItem()
                NewItem.Text = row("id") & "-" & row("name")
                NewItem.Value = row("id")
                dropdown.Items.Add(NewItem)
            Next

        End If
    End Sub

    Protected Sub FromDateTxt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FromDateTxt.TextChanged
        Threading.Thread.CurrentThread.CurrentCulture = _datestyle
        'FromDateTxt.Text = HypCalFrom.SelectedDate.Date.ToString().Substring(0, 10)
    End Sub

    Protected Sub ToDateTxt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ToDateTxt.TextChanged
        Threading.Thread.CurrentThread.CurrentCulture = _datestyle
        'ToDateTxt.Text = HypCalTo.SelectedDate.Date.ToString().Substring(0, 10)
    End Sub

    Protected Sub ReportDropdown_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ReportDropdown.SelectedIndexChanged

        Select Case ReportDropdown.SelectedIndex
            Case 0
                'outstanding report
                Me.FromDateTxt.Enabled = True
                Me.ToDateTxt.Enabled = True
                Me.CustomerTypeDDL.Enabled = True
                Me.SVCToDDL.Enabled = True
                Me.SVCFromDDL.Enabled = True
                Me.AgeingDDL.Enabled = False
                Me.lnkViewReport.Visible = True
                Me.lnkDebtorList.Visible = False
                Me.CheckAll.Visible = False
                Me.UncheckAll.Visible = False
                Me.grdAgeingDetails.Visible = False
            Case 1
                'ageing - need start time and end time only
                Me.FromDateTxt.Enabled = True
                Me.ToDateTxt.Enabled = True
                Me.CustomerTypeDDL.Enabled = False
                Me.SVCToDDL.Enabled = False
                Me.SVCFromDDL.Enabled = False
                Me.AgeingDDL.Enabled = False
                Me.lnkViewReport.Visible = True
                Me.lnkDebtorList.Visible = False
                Me.CheckAll.Visible = False
                Me.UncheckAll.Visible = False
                Me.grdAgeingDetails.Visible = False
            Case 2
                'reminder letter
                Me.FromDateTxt.Enabled = False
                Me.ToDateTxt.Enabled = False
                Me.CustomerTypeDDL.Enabled = True
                Me.SVCToDDL.Enabled = False
                Me.SVCFromDDL.Enabled = False
                Me.AgeingDDL.Enabled = True
                Me.lnkViewReport.Visible = False
                Me.lnkDebtorList.Visible = True
                Me.CheckAll.Visible = False
                Me.UncheckAll.Visible = False
                Me.grdAgeingDetails.Visible = True
            Case Else
                'outstanding report
                Me.FromDateTxt.Enabled = True
                Me.ToDateTxt.Enabled = True
                Me.CustomerTypeDDL.Enabled = True
                Me.SVCToDDL.Enabled = True
                Me.SVCFromDDL.Enabled = True
                Me.AgeingDDL.Enabled = False
                Me.lnkViewReport.Visible = True
                Me.lnkDebtorList.Visible = False
                Me.CheckAll.Visible = False
                Me.UncheckAll.Visible = False
                Me.grdAgeingDetails.Visible = False
        End Select

    End Sub


    Protected Sub LnkViewDebtorListClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkDebtorList.Click

        Dim startDate As DateTime = DateTime.ParseExact(Me.FromDateTxt.Text.ToString(), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture)
        Dim endDate As DateTime = DateTime.ParseExact(Me.ToDateTxt.Text.ToString(), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture)
        _debtor.StartInvDate = startDate.ToString("yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture)
        _debtor.EndInvDate = endDate.ToString("yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture)

        _debtor.CustType = Me.CustomerTypeDDL.SelectedValue.ToString()
        _debtor.ToSvcID = Me.SVCToDDL.SelectedValue.ToString()
        _debtor.FromSvcID = Me.SVCFromDDL.SelectedValue.ToString()
        _debtor.Ageing = Me.AgeingDDL.SelectedValue()

        Session("rptDebtor") = _debtor

        _debtor = CType(Session("rptDebtor"), clsDebtorReport)
        _debtor.ReportType = "debtor"
        Dim debtorDataSet As DataSet = _debtor.getDebtorListbyAgeing

        If Not debtorDataSet Is Nothing Then
            grdAgeingDetails.DataSource = debtorDataSet
            grdAgeingDetails.DataBind()
            Session("rptDebtor") = debtorDataSet
            If debtorDataSet.Tables(0).Rows.Count = 0 Then
                Me.CheckAll.Visible = False
                Me.UncheckAll.Visible = False
            Else

                Me.CheckAll.Visible = True
                Me.UncheckAll.Visible = True
            End If


        Else
            Me.CheckAll.Visible = False
            Me.UncheckAll.Visible = False
        End If

    End Sub

    Protected Sub grdAgeingDetails_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdAgeingDetails.RowCommand
        Dim ageing As String = Me.AgeingDDL.SelectedValue
        Dim list As New ArrayList
        Dim printlist As String = String.Empty
        If e.CommandName = "print" Then
            'get checked checkbox
            For Each gvr As GridViewRow In Me.grdAgeingDetails.Rows
                'Get a programmatic reference to the CheckBox control
                Dim cb As CheckBox = CType(gvr.FindControl("cbPrint"), CheckBox)
                Dim custID As Label = CType(gvr.FindControl("lblcustID"), Label)
                Dim trnno As Label = CType(gvr.FindControl("lblTrnno"), Label)

                'CheckBox chk = row.Cells[0].Controls[0] as CheckBox;
                If (Not (cb Is Nothing) And cb.Checked) Then
                    list.Add(trnno.Text)
                    If printlist = "" Then
                        printlist = Trim(trnno.Text)
                    Else
                        printlist = printlist + "," + Trim(trnno.Text)
                    End If

                End If

            Next
            If printlist <> "" Then
                _debtor.PrintList = printlist
                _debtor.Ageing = ageing
                Session("rptDebtor") = _debtor
                Dim script As String = "window.open('../report/RPTdebtor_VIEW.aspx?report=debtor')"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "RPTdebtor_VIEW", script, True)
            Else
                'no cb checked.
                'alert message
                Me.lblNoRecord.Visible = True
            End If
        End If
    End Sub

    Protected Sub grdAgeingDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdAgeingDetails.RowDataBound


        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim custID As String = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "custID"))
            Dim trnNo As String = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "trnno"))

            Dim rowIndex As Int16 = e.Row.RowIndex
            If rowIndex = 0 And custID = "" Then

            Else
                Dim lnkbtnresult As CheckBox = DirectCast(e.Row.FindControl("cbPrint"), CheckBox)
                If lnkbtnresult IsNot Nothing Then
                    'lnkbtnresult.Attributes.Add("onclick", (Convert.ToString("javascript:return deleteConfirm('") & custID) + "')")
                End If
            End If
        End If


    End Sub

    Protected Sub grdAgeingDetails_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdAgeingDetails.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then

            ' Retrieve the Label from the first column.
            Dim myLabel As Label = DirectCast(e.Row.FindControl("lblNo"), Label)

            ' Set Label text equal to the rowIndex +1 
            myLabel.Text = e.Row.RowIndex.ToString() + 1

        End If
    End Sub

    Private Sub ToggleCheckState(ByVal checkState As Boolean)
        ' Iterate through the Products.Rows property
        For Each row As GridViewRow In Me.grdAgeingDetails.Rows
            ' Access the CheckBox
            Dim cb As CheckBox = row.FindControl("cbPrint")
            If cb IsNot Nothing Then
                cb.Checked = checkState
            End If
        Next
    End Sub

    Protected Sub CheckAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckAll.Click
        ToggleCheckState(True)
    End Sub

    Protected Sub UncheckAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles UncheckAll.Click
        ToggleCheckState(False)
    End Sub
End Class
