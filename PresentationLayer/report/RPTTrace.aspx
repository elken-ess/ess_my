<%@ Page Language="VB" AutoEventWireup="false" CodeFile="RPTTrace.aspx.vb" Inherits="PresentationLayer_report_RPTTrace" %>

<%@ Register Assembly="JCalendar" Namespace="JCalendar" TagPrefix="cc1" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>PRODUCT TRACEABILITY REPORTS</title>
    <meta http-equiv="Content-Type" content="text/html; charset=gb2312">
	<LINK href="../css/style.css" type="text/css" rel="stylesheet">     
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
      <script language="JavaScript" src="../js/common.js"></script>
            

    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
            
</head>
<body>
    <form id="form1" runat="server">
    <div>
     <table id="countrytab" border="0" width="100%">
            <tr>
                <td style="width: 98%;">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title1.gif" width="5" /></td>
                            <td background="../graph/title_bg.gif" class="style2" width="98%">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="LEFT" background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title_2.gif" width="5" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 98%;">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" class="style2" width="98%">
                                <font color="red">
                                    <asp:Label ID="errlab" runat="server" Text="Label" Visible="false"></asp:Label></font></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 98%; height: 1px;">
                    <table id="country" bgcolor="#b7e6e6" border="0" cellpadding="2" cellspacing="1" width ="100%">
                        <tr bgcolor="#ffffff">
                            <td align="LEFT" style="width: 13%">
                                <asp:Label ID="lblPartCode" runat="server" Text="Part Code"></asp:Label></td>
                            <td align="LEFT" style="width: 6%">
                                &nbsp;<asp:Label ID="Fromlab1" runat="server" Text="From" Height="16px"></asp:Label></td>
                            <td align="left" style="width: 25%">
                                <asp:TextBox ID="txtPartCodeFrom" runat="server" CssClass="textborder" MaxLength="10" style="width: 90%">AAA</asp:TextBox>
                                
                                <asp:Calendar  ID="Calendar1" runat="server" BackColor="White" BorderColor="#3366CC"
                                    BorderWidth="1px" CellPadding="1" DayNameFormat="Shortest" Font-Names="Verdana"
                                    Font-Size="8pt" ForeColor="#003399" Height="96px" Visible="False" Width="136px">
                                    <SelectedDayStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                                    <TodayDayStyle BackColor="#99CCCC" ForeColor="White" />
                                    <SelectorStyle BackColor="#99CCCC" ForeColor="#336666" />
                                    <WeekendDayStyle BackColor="#CCCCFF" />
                                    <OtherMonthDayStyle ForeColor="#999999" />
                                    <NextPrevStyle Font-Size="8pt" ForeColor="#CCCCFF" />
                                    <DayHeaderStyle BackColor="#99CCCC" ForeColor="#336666" Height="1px" />
                                    <TitleStyle BackColor="#003399" BorderColor="#3366CC" BorderWidth="1px" Font-Bold="True"
                                        Font-Size="10pt" ForeColor="#CCCCFF" Height="25px"  />
                                </asp:Calendar>
                            </td>
                            <td align="LEFT" style="width: 5%">
                                <asp:Label ID="Tolab1" runat="server" Text="To"></asp:Label></td>
                            <td align="left" style="width: 24%">
                                <asp:TextBox ID="txtPartCodeTo" runat="server" CssClass="textborder" MaxLength="10" style="width: 90%">ZZZ</asp:TextBox>
                                <asp:Calendar ID="Calendar2" runat="server" BackColor="White" BorderColor="#3366CC"
                                    BorderWidth="1px" CellPadding="1" DayNameFormat="Shortest" Font-Names="Verdana"
                                    Font-Size="8pt" ForeColor="#003399" Height="96px" Visible="False" Width="136px">
                                    <SelectedDayStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                                    <TodayDayStyle BackColor="#99CCCC" ForeColor="White" />
                                    <SelectorStyle BackColor="#99CCCC" ForeColor="#336666" />
                                    <WeekendDayStyle BackColor="#CCCCFF" />
                                    <OtherMonthDayStyle ForeColor="#999999" />
                                    <NextPrevStyle Font-Size="8pt" ForeColor="#CCCCFF" />
                                    <DayHeaderStyle BackColor="#99CCCC" ForeColor="#336666" Height="1px" />
                                    <TitleStyle BackColor="#003399" BorderColor="#3366CC" BorderWidth="1px" Font-Bold="True"
                                        Font-Size="10pt" ForeColor="#CCCCFF" Height="25px" />
                                </asp:Calendar>
                            </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="LEFT" style="width: 13%">
                                <asp:Label ID="ServiceDatelab" runat="server" Text="Lable"></asp:Label></td>
                            <td align="LEFT" style="width: 6%">
                                <asp:Label ID="Fromlab2" runat="server" Text="From"></asp:Label></td>
                            <td align="left" style="width: 25%">
                                <asp:TextBox ID="mindate" runat="server" CssClass="textborder" ReadOnly="False" MaxLength =10 style="width: 90%">01/01/2006</asp:TextBox>
                                
                                <asp:HyperLink ID="HypCalFrom" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif" ToolTip="Choose a Date">Choose a Date</asp:HyperLink>

                                
                                <cc1:JCalendar ID="JCalendar1" runat="server" ControlToAssign="mindate" ImgURL="~/PresentationLayer/graph/calendar.gif"  Visible =false />
                                &nbsp;&nbsp;</td>
                            <td align="LEFT" style="width: 5%">
                                <asp:Label ID="Tolab2" runat="server" Text="To"></asp:Label></td>
                            <td align="left" style="width: 24%">
                                <asp:TextBox ID="maxdate" runat="server" CssClass="textborder" ReadOnly="False" MaxLength =10 style="width: 90%">31/12/2006</asp:TextBox>&nbsp;
                                
                                <asp:HyperLink ID="HypCalTo" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif" ToolTip="Choose a Date">Choose a Date</asp:HyperLink>

                                <cc1:JCalendar
                                    ID="JCalendar2" runat="server" ControlToAssign="maxdate" ImgURL="~/PresentationLayer/graph/calendar.gif" Visible =false  />
                            </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="LEFT" style="width: 13%; height: 30px;">
                                <asp:Label ID="Modellab" runat="server" Text="Lable"></asp:Label></td>
                            <td align="LEFT" style="width: 6%; height: 30px;">
                                <asp:Label ID="Fromlab3" runat="server" Text="From"></asp:Label></td>
                            <td align="left" style="width: 25%; height: 30px;">
                                <asp:TextBox ID="minmodelID" runat="server" CssClass="textborder" MaxLength="10" Width="90%">AAA</asp:TextBox></td>
                            <td align="LEFT" style="width: 5%; height: 30px;">
                                <asp:Label ID="Tolab3" runat="server" Text="To"></asp:Label></td>
                            <td align="left" style="height: 30px; width: 24%;">
                                <asp:TextBox ID="maxmodelID" runat="server" CssClass="textborder" MaxLength="10" style="width: 90%" Width="90%">ZZZ</asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="LEFT" style="width: 13%; height: 30px;">
                                <asp:Label ID="SerialNumberlab" runat="server" Text="Lable"></asp:Label></td>
                            <td align="LEFT" style="width: 6%; height: 30px;">
                                <asp:Label ID="Fromlab4" runat="server" Text="From" Height="16px"></asp:Label></td>
                            <td align="left" style="width: 25%; height: 30px;">
                                <asp:TextBox ID="minSerialNumber" runat="server" CssClass="textborder" MaxLength="10" Width="90%">000</asp:TextBox></td>
                            <td align="LEFT" style="width: 5%; height: 30px;">
                                <asp:Label ID="Tolab4" runat="server" Text="To"></asp:Label></td>
                            <td align="left" style="width: 24%; height: 30px;">
                                <asp:TextBox ID="maxSerialNumber" runat="server" CssClass="textborder" MaxLength="10" Width="90%">ZZZ</asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="LEFT" style="width: 13%; height: 30px;">
                                <asp:Label ID="PriceTypelab" runat="server" Text="Lable"> Price Type</asp:Label></td>
                            <td align="LEFT" style="width: 6%; height: 30px;">
                                <asp:Label ID="Fromlab5" runat="server" Text="From" Height="16px"></asp:Label></td>
                            <td align="left" style="width: 25%; height: 30px;">
                                <asp:TextBox ID="PriceTypeFr" runat="server" CssClass="textborder" MaxLength="10" Width="90%">000</asp:TextBox></td>
                            <td align="LEFT" style="width: 5%; height: 30px;">
                                <asp:Label ID="Label3" runat="server" Text="To"></asp:Label></td>
                            <td align="left" style="width: 24%; height: 30px;">
                                <asp:TextBox ID="PriceTypeTo" runat="server" CssClass="textborder" MaxLength="10" Width="90%">ZZZ</asp:TextBox></td>
                        </tr>    
                         <tr bgcolor="#ffffff">
                            <td align="LEFT" style="width: 13%; height: 28px;" >
                                <asp:Label ID="JobTypelab" runat="server" Text="Lable"></asp:Label></td>
                               <td align="LEFT" style="width: 18%; height: 28px;" colspan =3>
                                   <asp:DropDownList ID="DropDownList1" runat="server">
                                   </asp:DropDownList> </td >
                                     <td  align="right" style="height: 28px"> <asp:LinkButton ID="reportviewer" runat="server" >LinkButton</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        </tr>   
                                                               
                        
                    </table>
                    <asp:GridView ID="TraceRPTView" runat="server" Visible="False" Width="100%">
                    </asp:GridView>
                    </td>
            </tr>
        </table>    
    </div>
        <cr:crystalreportviewer id="TraceReportViewer" runat="server" autodatabind="True" Height="50px" Width="350px" DisplayGroupTree="False"></cr:crystalreportviewer>
        &nbsp;
    </form>
</body>
</html>
