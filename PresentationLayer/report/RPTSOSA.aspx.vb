Imports BusinessEntity
Imports System.Configuration
Imports System.Data
Imports System.Data.SqlClient
Imports System.Xml
Partial Class PresentationLayer_Report_RPTSOSA
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'display label message
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If

        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try
        If Not Page.IsPostBack Then
            Session("RPTSOSAflag") = "0"
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            Me.ServiceCenterlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDAR-SVC")
            Me.Customeridlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSOSA-CUSTID")
            Me.Apptnolab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSOSA-APPTNO")
            Me.datelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDAR-DATE")
            Me.Modellab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSOSA-MODEL")
            Me.ApptTypelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSOSA-APPTTYPE")

            Me.ApptStatuslab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSOSA-APPTSTAT")

            Me.Fromlab1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Me.Fromlab2.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")

            Me.Fromlab4.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Me.Fromlab5.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Me.Fromlab6.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Me.Tolab1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Me.Tolab2.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")

            Me.Tolab4.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Me.Tolab5.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Me.Tolab6.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")

            reportviewer.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-VIEWREPORT")

            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSOSA-APPHIS")


            lblTransactionType.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-TRANSTYPE")

            '''access control'''''''''''''''''''''''''''''''''''''''''''''''
            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "45")
            Me.reportviewer.Enabled = purviewArray(3)


            'Me.contact.Items.Clear()

            Dim dlstatus As New clsrlconfirminf()
            Dim dlParam As ArrayList = New ArrayList
            dlParam = dlstatus.searchconfirminf("APPTStatus")
            Dim dlcount As Integer
            Dim dlid As String
            Dim dlnm As String
            For dlcount = 0 To dlParam.Count - 1
                dlid = dlParam.Item(dlcount)
                dlnm = dlParam.Item(dlcount + 1)
                dlcount = dlcount + 1

                Me.ApptStatus.Items.Add(New ListItem(dlnm.ToString(), dlid.ToString()))

            Next
            Me.ApptStatus.Items.Insert(0, "")

            Dim dltype As New clsrlconfirminf()
            Dim apParam As ArrayList = New ArrayList
            apParam = dltype.searchconfirminf("SERTYPE")
            Dim apcount As Integer
            Dim apid As String
            Dim apnm As String
            For apcount = 0 To apParam.Count - 1
                apid = apParam.Item(apcount)
                apnm = apParam.Item(apcount + 1)
                apcount = apcount + 1

                Me.ApptType.Items.Add(New ListItem(apnm.ToString(), apid.ToString()))

            Next

            Me.cboTransactionType.Items.Add(New ListItem("", ""))
            cboTransactionType.Items.Add(New ListItem("AS", "AS"))
            cboTransactionType.Items.Add(New ListItem("SS", "SS"))
            cboTransactionType.SelectedIndex = 2


            Me.ApptType.Items.Insert(0, "")
            Me.maxsrevice.Text = "ZZZ"
            Me.maxCustomerid.Text = "ZZZ"
            Me.maxModel.Text = "ZZZ"
            Me.maxApptno.Text = "ZZZ"
            Me.mindate.Text = "01/01/2006"

            Me.maxdate.Text = "31/12/2006"
            Me.minApptno.Text = "AAA"
            Me.minCustomerid.Text = "AAA"
            Me.minModel.Text = "AAA"
            Me.minsrevice.Text = "AAA"
        ElseIf Session("RPTSOSAflag") = "1" Then
            datasearch()
        End If
        HypCalFrom.NavigateUrl = "javascript:DoCal(document.form1.mindate);"
        HypCalTo.NavigateUrl = "javascript:DoCal(document.form1.maxdate);"
    End Sub

    Protected Sub reportviewer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles reportviewer.Click
        'Dim fintremark As Integer = 1 'report remark
        Session("RPTSOSAflag") = "1"
        datasearch()
        'Response.Redirect("~/PresentationLayer/REPORT/RPTSOSAVIEW.aspx")
        Dim script As String = "window.open('../report/RPTSOSAVIEW.aspx')"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "SystemAccessReport", script, True)

    End Sub
    Public Sub datasearch()
        Dim clsReport As New ClsCommonReport
        Dim clsrptsosa As New ClsRptSOSA
        clsrptsosa.userid = Session("userID")
        clsrptsosa.username = Session("username")

        mindate.Text = Request.Form("mindate")
        maxdate.Text = Request.Form("maxdate")


        If (Trim(Me.minsrevice.Text) <> "AAA") Then

            clsrptsosa.Minsvcid = minsrevice.Text.ToUpper()

        End If
        If (Trim(Me.maxsrevice.Text) <> "ZZZ") Then

            clsrptsosa.Maxsvcid = maxsrevice.Text.ToUpper()
        Else
            clsrptsosa.Maxsvcid = "ZZZZZZZZZZ"
        End If

        If (Trim(Me.minCustomerid.Text) <> "AAA") Then

            clsrptsosa.Mincustid = minCustomerid.Text.ToUpper()

        End If

        If (Trim(Me.maxCustomerid.Text) <> "ZZZ") Then

            clsrptsosa.Maxcustid = maxCustomerid.Text.ToUpper()
        Else
            clsrptsosa.Maxcustid = "ZZZZZZZZZZZZZZZZZZZZ"

        End If

        If (Trim(Me.minApptno.Text) <> "AAA") Then

            clsrptsosa.Minapptno = minApptno.Text.ToUpper()

        End If

        If (Trim(Me.maxApptno.Text) <> "ZZZ") Then

            clsrptsosa.Maxapptno = maxApptno.Text.ToUpper()
        Else
            clsrptsosa.Maxapptno = "ZZZZZZZZZZ"

        End If

        If (Trim(Me.mindate.Text) <> "") Then
            If (Trim(Me.mindate.Text) <> "01/01/2006") Then
                Dim temparr As Array = mindate.Text.Split("/")
                clsrptsosa.Mindate = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
                'clsrptsosa.Mindate = Calendar1.SelectedDate.Date.ToString()
            Else
                clsrptsosa.Mindate = "2006-01-01"
            End If


        End If

            If (Trim(Me.maxdate.Text) <> "") Then
            If (Trim(Me.maxdate.Text) <> "31/12/2006") Then
                Dim temparr As Array = maxdate.Text.Split("/")
                clsrptsosa.Maxdate = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
                'clsrptsosa.Maxdate = Calendar2.SelectedDate.Date.ToString()
            Else
                clsrptsosa.Maxdate = "2006-12-31"
            End If
        End If


        If (Trim(Me.minModel.Text) <> "AAA") Then

            clsrptsosa.Minmodel = minModel.Text.ToUpper()

        End If

        If (Trim(Me.maxModel.Text) <> "ZZZ") Then

            clsrptsosa.Maxmodel = maxModel.Text.ToUpper()
        Else
            clsrptsosa.Maxmodel = "ZZZZZZZZZZ"
        End If

        If (Me.ApptStatus.SelectedItem.Value.ToString() <> "") Then
            clsrptsosa.ApptStatus = ApptStatus.SelectedItem.Value.ToString()
            clsrptsosa.ApptStatustext = ApptStatus.SelectedItem.Text.ToString()
        End If

        If (Me.ApptType.SelectedItem.Value.ToString() <> "") Then
            clsrptsosa.ApptType = ApptType.SelectedItem.Value.ToString()
            clsrptsosa.ApptTypetext = ApptType.SelectedItem.Text.ToString()
        End If

        Dim rank As String = Session("login_rank")

        If rank <> 0 Then
            clsrptsosa.ctryid = Session("login_ctryID")
            clsrptsosa.compid = Session("login_cmpID")
            clsrptsosa.svcid = Session("login_svcID")
        End If

        clsrptsosa.TransactionType = Me.cboTransactionType.SelectedValue
            clsrptsosa.rank = rank
        'Dim rptsosa As DataSet = clsrptsosa.GetRptsysa()
        'Session("rptsosa_ds") = rptsosa
            Session("rptsosa_search_condition") = clsrptsosa
            'Dim objXmlTr As New clsXml
            'Dim strHead As Array = Array.CreateInstance(GetType(String), 15)
            'objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            'strHead(0) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDAR-NUMB")
            'strHead(1) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSOSA-APPTNO")
            'strHead(2) = objXmlTr.GetLabelName("EngLabelMsg", "BB-ENTRYDATE")
            'strHead(3) = objXmlTr.GetLabelName("EngLabelMsg", "BB-TECHID")

            'strHead(4) = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRID")
            'strHead(5) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSOSA-APPTSTAT")
            'strHead(6) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSOSA-APPTDATE")
            'strHead(7) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSOSA-REMARKS")
            'strHead(8) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDAR-TITLE1")
            'strHead(9) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSOSA-APPHIS")
            'strHead(10) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSOSA-TITLE2")
            'strHead(11) = Me.mindate.Text
            'strHead(12) = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            'strHead(13) = Me.maxdate.Text
            'strHead(14) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSOSA-REPORTID")
            'If Me.IsPostBack Then

            '    With clsReport
        '        .ReportFileName = "RptSosa.rpt"  'TextBox1.Text
            '        .SetReport(CrystalReportViewer1, rptsosa, strHead)


            '    End With
            'End If
    End Sub
 
    
End Class
