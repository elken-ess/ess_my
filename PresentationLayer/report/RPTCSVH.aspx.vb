﻿Imports SQLDataAccess
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
Imports BusinessEntity
Imports CrystalDecisions.Web
Imports CrystalDecisions.CrystalReports.Engine
Partial Class PresentationLayer_Report_RPTCSVH
    Inherits System.Web.UI.Page
    Dim fstrMonth As String = ""
    Dim fstrDefaultStartDate As String = ""
    Dim fstrDefaultEndDate As String = ""
    Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        fstrMonth = Month(System.DateTime.Today)
        fstrDefaultStartDate = "01/" & IIf(Len(fstrMonth) = 1, "0" & fstrMonth, fstrMonth) & "/" & Year(System.DateTime.Today)
        fstrDefaultEndDate = DateAdd(DateInterval.Month, 1, CDate(fstrDefaultStartDate))


        If Not Me.IsPostBack Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If

            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try
            Session("CHSLflag") = "1"

            '''access control'''''''''''''''''''''''''''''''''''''''''''''''
            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "38")
            Me.saveButton.Enabled = purviewArray(3)
            Dim objXmlTr As New clsXml

            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            Me.Label10.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Me.Label7.Text = Me.Label10.Text
            Me.Label12.Text = Me.Label10.Text
            Me.Label14.Text = Me.Label10.Text
            Me.Label17.Text = Me.Label10.Text
            Me.Label21.Text = Me.Label10.Text
            Me.Label22.Text = Me.Label10.Text

            Me.Label11.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Me.Label13.Text = Me.Label11.Text
            Me.Label15.Text = Me.Label11.Text
            Me.Label16.Text = Me.Label11.Text
            Me.Label18.Text = Me.Label11.Text
            Me.Label24.Text = Me.Label11.Text

            Me.Label1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDSL-0010")
            Me.Label2.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDSL-0015")
            Me.Label3.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDSL-0011")
            Me.Label4.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDSL-0012")
            Me.Label5.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDSL-0013")
            Me.Label9.Text = objXmlTr.GetLabelName("EngLabelMsg", "RPT_SERBITECH_INVDATE")
            '  Me.Label23.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCHL-0020")
            Me.Label21.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-SERVICETYPE")

            Me.Label6.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-SORTBY")

            Me.titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCHL-0022")
            Me.lblServiceBillType.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-SERVICEBILLTYPE")

            '////////////////////////////////////////////////////////////////////////
            'Dim cntryStat As New clsrlconfirminf()  'contract tp
            'Dim contractpar As ArrayList = New ArrayList
            Dim statid As String
            Dim statnm As String
            Dim count As Integer
            'contractpar = cntryStat.searchconfirminf("YESNO")
            'count = 0
            'For count = 0 To contractpar.Count - 1
            '    statid = contractpar.Item(count)
            '    statnm = contractpar.Item(count + 1)
            '    count = count + 1
            '    Me.DropDownList1.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
            'Next

            Dim installbond As New clsCommonClass
            'bond  service type id and name
            Dim sertype As New DataSet
            sertype = installbond.Getidname("BB_RPTPSLT_IDNAME '" & Session("userID") & "'")
            databonds(sertype, DropDownList1)



            '//////////////////////////////////////////////////////////////
            Dim sotrnm1 As String = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCHL-0017")
            Dim sotrnm2 As String = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0005")
            Dim sotrnm3 As String = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-0002") 'modty id
            Dim sotrnm4 As String = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCHL-0015")  'serial no

            Me.DropDownList3.Items.Add(New ListItem(sotrnm1, "FIV1_INVDT"))
            Me.DropDownList3.Items.Add(New ListItem(sotrnm2, "MCUS_ENAME"))
            Me.DropDownList3.Items.Add(New ListItem(sotrnm3, "MROU_MODID"))
            Me.DropDownList3.Items.Add(New ListItem(sotrnm4, "MROU_SERNO"))

            Me.maxroserial.Text = "ZZZ"
            Me.maxcustmbox.Text = "ZZZ"
            Me.maxmodelbox.Text = "ZZZ"
            Me.maxstatebox.Text = "ZZZ"
            Me.maxsvsbox.Text = "ZZZ"
            Me.minroserial.Text = "AAA"
            Me.minsvsbox.Text = "AAA"
            Me.minstatebox.Text = "AAA"
            Me.minmodelbox.Text = "AAA"
            Me.mincustmbox.Text = "AAA"
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Me.mindatebox.Text = fstrDefaultStartDate
            Me.maxdatebox.Text = fstrDefaultEndDate




            Dim Util As New clsUtil()
            Dim statParam As ArrayList = New ArrayList
            '---- SERVICE BILL TYPE
            Me.cboServiceBillType.ClearSelection()
            Me.cboServiceBillType.Items.Add(New ListItem("All", ""))
            statParam = Util.searchconfirminf("SERBILLTY")
            'Dim count As Integer
            'Dim statid As String
            'Dim statnm As String
            For count = 0 To statParam.Count - 1
                statid = statParam.Item(count)
                statnm = statParam.Item(count + 1)
                count = count + 1
                Me.cboServiceBillType.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
            Next

            Me.saveButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-VIEWREPORT")
        ElseIf Session("CHSLflag") = "1" Then
            PrintReport()
        End If

        HypCalFrom.NavigateUrl = "javascript:DoCal(document.form1.mindatebox);"
        HypCalTo.NavigateUrl = "javascript:DoCal(document.form1.maxdatebox);"
    End Sub

#Region " bonds"
    Public Sub databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        dropdown.Items.Add(New ListItem("All", ""))

        Dim row As DataRow
        If ds.Tables(0).Rows.Count > 0 Then
            'dropdown.Items.Add("")
            For Each row In ds.Tables(0).Rows
                Dim NewItem As New ListItem()
                NewItem.Text = Trim(row("id")) & "-" & row("name")
                NewItem.Value = Trim(row("id"))
                dropdown.Items.Add(NewItem)
            Next
        End If
    End Sub
#End Region

    Public Function PrintReport()
        Dim rptcsvh As New clsRPTCSVH
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        mindatebox.Text = Request.Form("mindatebox")
        maxdatebox.Text = Request.Form("maxdatebox")

        If Not IsDate(mindatebox.Text) Then
            mindatebox.Text = fstrDefaultStartDate
        End If
        If Not IsDate(maxdatebox.Text) Then
            maxdatebox.Text = fstrDefaultEndDate
        End If


        If (Me.mincustmbox.Text <> "AAA") Then
            rptcsvh.minCustmID = Me.mincustmbox.Text.ToUpper
        Else
            rptcsvh.minCustmID = ""
        End If
        If (Me.maxcustmbox.Text <> "ZZZ") Then
            rptcsvh.maxCustmID = Me.maxcustmbox.Text.ToUpper
        Else
            rptcsvh.maxCustmID = "ZZZZZZZZZZZZZZZZZZZZ"
        End If
        If (Me.minsvsbox.Text <> "AAA") Then
            rptcsvh.minSvcID = Me.minsvsbox.Text.ToUpper
        Else
            rptcsvh.minSvcID = ""
        End If
        If (Me.maxsvsbox.Text <> "ZZZ") Then
            rptcsvh.maxSvcID = Me.maxsvsbox.Text.ToUpper
        Else
            rptcsvh.maxSvcID = "ZZZZZZZZZZZZZZZZZZZZ"
        End If
        If (Me.minmodelbox.Text <> "AAA") Then
            rptcsvh.minPrdctMd = Me.minmodelbox.Text.ToUpper
        Else
            rptcsvh.minPrdctMd = ""
        End If
        If (Me.maxmodelbox.Text <> "ZZZ") Then
            rptcsvh.maxPrdctMd = Me.maxmodelbox.Text.ToUpper
        Else
            rptcsvh.maxPrdctMd = "ZZZZZZZZZZZZZZZZZZZZ"
        End If
        If (Me.minroserial.Text <> "AAA") Then
            rptcsvh.minRONo = Me.minroserial.Text.ToUpper
        Else
            rptcsvh.minRONo = ""
        End If
        If (Me.maxroserial.Text <> "ZZZ") Then
            rptcsvh.maxRONo = Me.maxroserial.Text.ToUpper
        Else
            rptcsvh.maxRONo = "ZZZZZZZZZZZZZZZZZZZZ"
        End If
        If (Me.minstatebox.Text <> "AAA") Then
            rptcsvh.minState = Me.minstatebox.Text.ToUpper
        Else
            rptcsvh.minState = ""
        End If
        If (Me.maxstatebox.Text <> "ZZZ") Then
            rptcsvh.maxState = Me.maxstatebox.Text.ToUpper
        Else
            rptcsvh.maxState = "ZZZZZZZZZZZZZZZZZZZZ"
        End If

        rptcsvh.mindate = Me.mindatebox.Text
      

        rptcsvh.maxdate = Me.maxdatebox.Text
      

        rptcsvh.ContractTy = Me.DropDownList1.SelectedValue
        rptcsvh.SortBy = Me.DropDownList3.SelectedValue
        Dim ctryIDPara As String = "%"
        Dim cmpIDPara As String = "%"
        Dim svcIDPara As String = "%"
        Dim login_rank As Integer = Session("login_rank")
        If login_rank <> 0 Then
            Select Case (login_rank)
                Case 9
                    ctryIDPara = Session("login_ctryID").ToString.ToUpper
                    cmpIDPara = Session("login_cmpID").ToString.ToUpper
                    svcIDPara = Session("login_svcID").ToString.ToUpper
                Case 8
                    ctryIDPara = Session("login_ctryID").ToString.ToUpper
                    cmpIDPara = Session("login_cmpID").ToString.ToUpper
                Case 7
                    ctryIDPara = Session("login_ctryID").ToString.ToUpper

            End Select
        Else


        End If

        rptcsvh.ServiceBillType = cboServiceBillType.SelectedValue
        rptcsvh.ServiceBillTypeText = cboServiceBillType.SelectedItem.Text.Trim

       


        ''''''''''''''''''''''''''''''''''''''''''''''''''
        rptcsvh.Rank = login_rank.ToString
        rptcsvh.CountryID = ctryIDPara
        rptcsvh.CompID = cmpIDPara
        rptcsvh.SVCID = svcIDPara


        rptcsvh.SortByText = Me.DropDownList3.SelectedItem.Text

        Session("rptcsvh_search_condition") = rptcsvh

    End Function

    Protected Sub saveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveButton.Click
        Session("CHSLflag") = "1"
        PrintReport()
        Dim script As String = "window.open('../report/RPTCSVHVIEW.aspx')"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "RPTCSVHVIEW", script, True)
        Return
    End Sub
 
End Class
