Imports System
Imports System.Collections.Generic
Imports System.Configuration

Imports BusinessEntity

Namespace PresentationLayer.report
    Partial Class PresentationLayerReportRptServBill2
        Inherits Page

#Region "Declarations"
        Private _datestyle As Globalization.CultureInfo = New Globalization.CultureInfo("en-CA")
        Private _servBill As New ClsServBillReport()
#End Region

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
            Threading.Thread.CurrentThread.CurrentCulture = _datestyle
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Const script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Const script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try

            If Not IsPostBack Then
                LoadLabels()
                LoadDefaultValues()

                Dim accessgroup As String = Session("accessgroup").ToString
                Dim purviewArray As Boolean()
                ' Modified by Ryan Estandarte 6 June 2012
                ' Changed access group value to "60"
                'purviewArray = CType(clsUserAccessGroup.GetUserPurview(accessgroup, "45"), Boolean())
                purviewArray = CType(clsUserAccessGroup.GetUserPurview(accessgroup, "60"), Boolean())
                lnkViewReport.Enabled = purviewArray(3)
				lnkViewReport.Enabled = "true"
            End If


            HypCalFrom.NavigateUrl = "javascript:DoCal(document.form1.FromInvoiceDateTextbox);"
            HypCalTo.NavigateUrl = "javascript:DoCal(document.form1.ToInvoiceDateTextbox);"
        End Sub

        Private Sub LoadDefaultValues()
            Const defaultFrom As String = "AAA"
            Const defaultTo As String = "ZZZ"

            FromServiceCtrTextbox.Text = defaultFrom
            ToServiceCtrTextbox.Text = defaultTo

            FromTechIDTextbox.Text = defaultFrom
            ToTechIDTextbox.Text = defaultTo

            FromCustIDTextbox.Text = defaultFrom
            ToCustIDTextbox.Text = defaultTo

            FromInvoiceDateTextbox.Text = DateTime.Now.ToString("dd/MM/yyyy")
            ToInvoiceDateTextbox.Text = DateTime.Now.AddMonths(1).ToString("dd/MM/yyyy")

            FromInvoiceNoTextbox.Text = defaultFrom
            ToInvoiceNoTextbox.Text = defaultTo

            Dim list As New Dictionary(Of String, String)()
            list.Add("Details Report", "Details Report")
            list.Add("Payment Report", "Payment Report")
            ' Added by Ryan Estandarte 19 Sept 2012
            list.Add("Credit Report", "Credit Report")

            ReportDropdown.DataSource = list
            ReportDropdown.DataTextField = "Key"
            ReportDropdown.DataValueField = "Value"
            ReportDropdown.DataBind()
        End Sub

        Private Sub LoadLabels()
            Dim objXmlTr As New clsXml

            ' Load Labels
            objXmlTr.XmlFile = ConfigurationManager.AppSettings("XmlFilePath")

            Dim fromLabel As String = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Dim toLabel As String = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")

            ServCtrLabel.Text = "Service Center ID"'objXmlTr.GetLabelName("EngLabelMsg", "RPT-SERVBILLREPORT")
            FromServiceCtrLabel.Text = fromLabel
            ToServiceCtrLabel.Text = toLabel

            TechnicianLabel.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TECHID")
            FromTechIDLabel.Text = fromLabel
            ToTechIDLabel.Text = toLabel

            CustomerIDLabel.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CUSTID")
            FromCustLabel.Text = fromLabel
            ToCustLabel.Text = toLabel

            InvoiceDateLabel.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-03")
            FromInvoiceDateLabel.Text = fromLabel
            ToInvoiceDateLabel.Text = toLabel

            InvoiceNoLabel.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-INVOICENO")
            FromInvoiceNoLabel.Text = fromLabel
            ToInvoiceNoLabel.Text = toLabel

            ReportTypeLabel.Text = "Report Type"'objXmlTr.GetLabelName("EngLabelMsg", "RPT-REPORTTYPE")

        End Sub

        Protected Sub LnkViewReportClick(ByVal sender As Object, ByVal e As EventArgs)
            Session("CTRNWL-FLAG") = "1"
            GetReportDetails()

            Dim script As String

            Select Case ReportDropdown.SelectedValue
                Case "Payment Report"
                    script = "window.open('../report/RPTServBill2_VIEW.aspx?report=payment')"
                Case "Details Report"
                    script = "window.open('../report/RPTServBill2_VIEW.aspx?report=details')"
                    ' Added by Ryan Estandarte 19 Sept 2012
                Case "Credit Report"
                    script = "window.open('../report/RPTServBill2_VIEW.aspx?report=credit')"
                Case Else
                    script = String.Empty
            End Select

            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "SystemAccessReport", script, True)
        End Sub

        Private Sub GetReportDetails()
            Dim fromSvcCtr As String = String.Empty
            Dim toSvcCtr As String = String.Empty

            If FromServiceCtrTextbox.Text <> "AAA" Then
                fromSvcCtr = FromServiceCtrTextbox.Text
            End If

            If ToServiceCtrTextbox.Text <> "ZZZ" Then
                toSvcCtr = ToServiceCtrTextbox.Text
            End If

            If FromTechIDTextbox.Text <> "AAA" And ToTechIDTextbox.Text <> "ZZZ" Then
                If CompareIDValues(FromTechIDTextbox.Text.Substring(3, FromTechIDTextbox.Text.Length - 3), ToTechIDTextbox.Text.Substring(3, ToTechIDTextbox.Text.Length - 3)) Then
                    _servBill.FromTechID = ToTechIDTextbox.Text
                    _servBill.ToTechID = FromTechIDTextbox.Text
                Else
                    _servBill.FromTechID = FromTechIDTextbox.Text
                    _servBill.ToTechID = ToTechIDTextbox.Text
                End If
            Else
                If FromTechIDTextbox.Text = "AAA" Then
                    _servBill.FromTechID = String.Empty
                End If

                If ToTechIDTextbox.Text = "ZZZ" Then
                    _servBill.ToTechID = String.Empty
                End If
            End If

            If FromCustIDTextbox.Text <> "AAA" And ToCustIDTextbox.Text <> "ZZZ" Then

                If CompareIDValues(FromCustIDTextbox.Text, ToCustIDTextbox.Text) Then
                    _servBill.FromCustID = ToCustIDTextbox.Text
                    _servBill.ToCustID = FromCustIDTextbox.Text
                Else
                    _servBill.FromCustID = FromCustIDTextbox.Text
                    _servBill.ToCustID = ToCustIDTextbox.Text
                End If
            Else

                If FromCustIDTextbox.Text = "AAA" Then
                    _servBill.FromCustID = String.Empty
                End If

                If ToCustIDTextbox.Text = "ZZZ" Then
                    _servBill.ToCustID = String.Empty
                End If

            End If

            If FromInvoiceNoTextbox.Text = "AAA" Then
                _servBill.FromServBillNo = String.Empty
            Else
                _servBill.FromServBillNo = FromInvoiceNoTextbox.Text
            End If

            If ToInvoiceNoTextbox.Text = "ZZZ" Then
                _servBill.ToServBillNo = String.Empty
            Else
                _servBill.ToServBillNo = ToServiceCtrTextbox.Text
            End If

            _servBill.StartInvDate = FromInvoiceDateTextbox.Text
            _servBill.EndInvDate = ToInvoiceDateTextbox.Text

            _servBill.UserName = Session("userID").ToString()
            _servBill.ReportType = ReportDropdown.SelectedValue

            Dim rank As String = Session("login_rank").ToString()
            Select Case rank
                Case "0"
                    _servBill.CtryID = String.Empty
                    _servBill.ComID = String.Empty
                    _servBill.FromSvcID = fromSvcCtr
                    _servBill.ToSvcID = toSvcCtr
                Case "7"
                    _servBill.CtryID = Session("login_ctryID").ToString()
                    _servBill.ComID = String.Empty
                    _servBill.FromSvcID = fromSvcCtr
                    _servBill.ToSvcID = toSvcCtr
                Case "8"
                    _servBill.CtryID = Session("login_ctryID").ToString()
                    _servBill.ComID = Session("login_cmpID").ToString()
                    _servBill.FromSvcID = fromSvcCtr
                    _servBill.ToSvcID = toSvcCtr
                Case "9"
                    _servBill.CtryID = Session("login_ctryID").ToString()
                    _servBill.ComID = Session("login_cmpID").ToString()
                    _servBill.FromSvcID = Session("login_svcID").ToString()
                    _servBill.ToSvcID = Session("login_svcID").ToString()
            End Select

            Session("rptServBill") = _servBill

        End Sub

        ''' <summary>
        ''' Compares if the fromString is greater than the toString
        ''' </summary>
        ''' <param name="fromString"></param>
        ''' <param name="toString"></param>
        ''' <returns></returns>
        ''' <remarks>Added by Ryan Estandarte 8 May 2012</remarks>
        Private Function CompareIDValues(ByVal fromString As String, ByVal toString As String) As Boolean
            Dim fromID As Integer = Integer.Parse(fromString)
            Dim toID As Integer = Integer.Parse(toString)

            If fromID > toID Then
                Return True
            End If

            Return False

        End Function

        Protected Sub FromInvoiceDateCalSelectedDateChanged(ByVal sender As Object, ByVal e As EventArgs)
            Threading.Thread.CurrentThread.CurrentCulture = _datestyle
            FromInvoiceDateTextbox.Text = FromInvoiceDateCal.SelectedDate.Date.ToString().Substring(0, 10)
        End Sub

        Protected Sub ToInvoiceDateCalSelectedDateChanged(ByVal sender As Object, ByVal e As EventArgs)
            Threading.Thread.CurrentThread.CurrentCulture = _datestyle
            ToInvoiceDateTextbox.Text = ToInvoiceDateCal.SelectedDate.Date.ToString().Substring(0, 10)
        End Sub
    End Class
End Namespace