Imports BusinessEntity
Imports System.Data
Imports System.Data.SqlClient
Imports System.Globalization
Imports SQLDataAccess
Imports System.Windows
Imports Microsoft.Win32
Partial Class PresentationLayer_report_RPTSVOSVIEW_ExcelNew
    Inherits System.Web.UI.Page
    Dim clsReport As New ClsCommonReport
    Dim RankA As String
    Dim RankB As String
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)
        Return connection
    End Function


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim datestyle As Globalization.CultureInfo = New Globalization.CultureInfo("en-CA")
        Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Const script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If

        Catch ex As Exception
            Const script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try

        Dim rptsvos As ClsRptsvosNew = CType(Session("rptsvos_searchcondition"), ClsRptsvosNew)
        Dim rptds As DataSet = rptsvos.GetServiceOrderSum()

        ' switch  encrypt to telphone for display        
        Dim count As Integer
        Dim prdctclname As String
        Dim clscomm As New clsCommonClass()
        For count = 0 To rptds.Tables(0).Rows.Count - 1
            Dim prdctclsid As String = rptds.Tables(0).Rows(count).Item(3).ToString 'item(8) is product class 
            prdctclname = clscomm.passconverttel(prdctclsid)
            rptds.Tables(0).Rows(count).Item(3) = prdctclname
        Next

        stafftype.Text = Session("stafftype").ToString

        If rptsvos.MinSvcID = Nothing Then
            svcidfr.Text = "AAA"
        Else
            svcidfr.Text = rptsvos.MinSvcID.ToString
        End If
        If rptsvos.MaxSvcID = Nothing Then
            svcidto.Text = "AAA"
        ElseIf rptsvos.MaxSvcID = "ZZZZZZZZZZZZZZZZZZZZ" Then
            svcidto.Text = "ZZZ"
        Else
            svcidto.Text = rptsvos.MaxSvcID.ToString
        End If

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''' 
        '''''''''''''''''''''''''''''''''''''''''''''      ?
        apptdtfr.Text = rptsvos.Mindate.ToString("dd/MM/yyyy")

        apptdtto.Text = rptsvos.Maxdate.ToString("dd/MM/yyyy")
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        If rptsvos.MinTchID = Nothing Then
            techidfr.Text = "AAA"
        Else
            techidfr.Text = rptsvos.MinTchID.ToString
        End If

        If rptsvos.MaxTchID = Nothing Then
            techidto.Text = "ESSAA"
        ElseIf rptsvos.MaxTchID = "ZZZZZZZZZZZZZZZZZZZZ" Then
            techidto.Text = "ESSAA"
        Else
            techidto.Text = rptsvos.MaxTchID.ToString
        End If
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        If rptsvos.MinArea = Nothing Then
            areaidfr.Text = "AAA"
        Else
            areaidfr.Text = rptsvos.MinArea.ToString
        End If
        If rptsvos.MaxArea = Nothing Then
            areaidto.Text = "AAA"
        ElseIf rptsvos.MaxArea = "ZZZZZZZZZZZZZZZZZZZZ" Then
            areaidto.Text = "ZZZ"
        Else
            areaidto.Text = rptsvos.MaxArea.ToString
        End If

        status.Text = rptsvos.StatusText
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        servicetype.Text = rptsvos.ServiceTypeText


        'Dim dt1 As DataTable = New DataTable()
        'dt1.Load(rptds)
        'dt1 = rptds.Tables(0)

        GridView1.DataSource = rptds
        GridView1.DataBind()

        GridView1.Visible = True


    End Sub
    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub

    Protected Sub Export_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Export.Click
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=GridView1.xls")
        Response.Charset = ""
        Response.ContentType = "application/vnd.xls"
        Dim StringWriter As IO.StringWriter = New System.IO.StringWriter()
        Dim HtmlTextWriter As New HtmlTextWriter(StringWriter)
        Dim style As String = "<style> td { mso-number-format:\@; } </style> "

        GridView1.RenderControl(HtmlTextWriter)
        Response.Write(style) 'style is added dynamically
        Response.Write(StringWriter.ToString())
        Response.[End]()
    End Sub
End Class
