<%@ Page Language="VB" AutoEventWireup="false" EnableEventValidation="false" CodeFile="RptMdsl.aspx.vb" Inherits="PresentationLayer_Report_RptMdsl_aspx" %>

<%@ Register Assembly="JCalendar" Namespace="JCalendar" TagPrefix="cc1" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Model due for service list</title>
   <meta http-equiv="Content-Type" content="text/html; charset=gb2312">
	<LINK href="../css/style.css" type="text/css" rel="stylesheet">      
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
<script language="JavaScript" src="../js/common.js"></script>

   <!--LW Added--> 
     <script language="javascript">  
    
        function LoadRaces_CallBack(response){
 

         //if the server-side code threw an exception
         if (response.error != null)
         {
          //we should probably do better than this
          alert(response.error); 
          
          return;
         }

         var ResponseValue = response.value;
         
       
         //if the response wasn't what we expected  
         if (ResponseValue == null || typeof(ResponseValue) != "object")
         {       
          return;
         }
              
         //Get the states drop down
         var ResultList = document.getElementById("<%=RacesDLL.ClientID%>");
         ResultList.options.length = 0; //reset the states dropdown
     
        
         //Remember, its length not Length in JavaScript
         for (var i = 0; i < ResponseValue.length; ++i)
         {
         
          //the columns of our rows are exposed like named properties
          var al = ResponseValue[i].split(":");
          var alid = al[0];
          var alname = al[1];
          ResultList.options[ResultList.options.length] =  new Option(alname, alid);
         }
     
        }
         
        function LoadRaces(ObjectClient)
        {
             var ObjectId = ObjectClient.options[ObjectClient.selectedIndex].value;
              
             PresentationLayer_Report_RptMdsl_aspx.GetRaces(ObjectId, LoadRaces_CallBack); 
        }
    </script>

    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
       <div>
        <table id="countrytab" border="0" width ="100%">
            <tr>
                <td style="width: 100%">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title1.gif" width="5" /></td>
                            <td background="../graph/title_bg.gif" class="style2" width="98%">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="right" background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title_2.gif" width="5" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100%">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" class="style2" width="98%">
                                <font color="red">
                                    <asp:Label ID="errlab" runat="server" Text="Label" Visible="false"></asp:Label></font></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100%">
                    <table id="country" bgcolor="#b7e6e6" border="0" cellpadding="2" cellspacing="1" width ="100%">
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 18%">
                                <asp:Label ID="Customerlab" runat="server" Text="Lable" Font-Bold="False"></asp:Label></td>
                            <td align="right" style="width: 6%">
                                &nbsp;<asp:Label ID="Fromlab1" runat="server" Text="From" Height="16px"></asp:Label></td>
                            <td align="left" style="width: 27%">
                                <asp:TextBox ID="mincustomer" runat="server" CssClass="textborder" MaxLength="10">AAA</asp:TextBox></td>
                            <td align="right" style="width: 9%">
                                <asp:Label ID="Tolab1" runat="server" Text="To"></asp:Label></td>
                            <td align="left" width="35%">
                                <asp:TextBox ID="maxcustomer" runat="server" CssClass="textborder" MaxLength="10">ZZZ</asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 18%">
                                <asp:Label ID="Modellab" runat="server" Text="Lable"></asp:Label></td>
                            <td align="right" style="width: 6%">
                                <asp:Label ID="Fromlab2" runat="server" Text="From"></asp:Label></td>
                            <td align="left" style="width: 27%">
                                <asp:DropDownList ID="minmodel" runat="server" CssClass="textborder" AutoPostBack="true">
                                </asp:DropDownList></td>
                            <td align="right" style="width: 9%">
                                <asp:Label ID="Tolab2" runat="server" Text="To"></asp:Label></td>
                            <td align="left" width="35%">
                                <asp:DropDownList ID="maxmodel" runat="server" CssClass="textborder" AutoPostBack="true">
                                </asp:DropDownList></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 18%">
                                <asp:Label ID="Seriallab" runat="server" Text="Lable"></asp:Label></td>
                            <td align="right" style="width: 6%">
                                <asp:Label ID="Fromlab3" runat="server" Text="From"></asp:Label></td>
                            <td align="left" style="width: 27%">
                                <asp:TextBox ID="minuserial" runat="server" CssClass="textborder" MaxLength="10">AAA</asp:TextBox>&nbsp;
                            </td>
                            <td align="right" style="width: 9%">
                                <asp:Label ID="Tolab3" runat="server" Text="To"></asp:Label></td>
                            <td align="left" width="35%">
                                <asp:TextBox ID="maxuserial" runat="server" CssClass="textborder" MaxLength="10">ZZZ</asp:TextBox>&nbsp;
                            </td>
                        </tr>
                       <tr bgcolor="#ffffff">
                            <td align="right" style="width: 18%">
                                <asp:Label ID="Sercenterlab" runat="server" Text="Lable"></asp:Label></td>
                            <td align="right" style="width: 6%">
                                <asp:Label ID="Fromlab6" runat="server" Text="From"></asp:Label></td>
                            <td align="left" style="width: 27%">
                                <asp:DropDownList ID="minsercenter" runat="server" CssClass="textborder" AutoPostBack="true">
                                </asp:DropDownList></td>
                            <td align="right" style="width: 9%">
                                <asp:Label ID="Tolab6" runat="server" Text="To"></asp:Label></td>
                            <td align="left" width="35%">
                                <asp:DropDownList ID="maxsercenter" runat="server" CssClass="textborder" AutoPostBack="true">
                                </asp:DropDownList></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 18%">
                                <asp:Label ID="Statelab" runat="server" Text="Lable"></asp:Label></td>
                            <td align="right" style="width: 6%">
                                <asp:Label ID="Fromlab4" runat="server" Text="From"></asp:Label></td>
                            <td align="left" style="width: 27%">
                                <asp:DropDownList ID="minstate" runat="server" CssClass="textborder" AutoPostBack="true">
                                </asp:DropDownList></td>
                            <td align="right" style="width: 9%">
                                <asp:Label ID="Tolab4" runat="server" Text="To"></asp:Label></td>
                            <td align="left" width="35%">
                                <asp:DropDownList ID="maxstate" runat="server" CssClass="textborder" AutoPostBack="true">
                                </asp:DropDownList></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 18%">
                                <asp:Label ID="Label1" runat="server" Text="Area ID"></asp:Label></td>
                            <td align="right" style="width: 6%">
                                <asp:Label ID="Label2" runat="server" Text="From"></asp:Label></td>
                            <td align="left" style="width: 27%">
                                <asp:DropDownList ID="minarea" runat="server" CssClass="textborder" AutoPostBack="true">
                                </asp:DropDownList></td>
                            <td align="right" style="width: 9%">
                                <asp:Label ID="Label3" runat="server" Text="To"></asp:Label></td>
                            <td align="left" width="35%">
                                <asp:DropDownList ID="maxarea" runat="server" CssClass="textborder" AutoPostBack="true">
                                </asp:DropDownList></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 18%; height: 30px;">
                                <asp:Label ID="Datelab" runat="server" Text="Lable"></asp:Label></td>
                            <td align="right" style="width: 6%; height: 30px;">
                                <asp:Label ID="Fromlab5" runat="server" Text="From"></asp:Label></td>
                            <td align="left" style="width: 27%; height: 30px;">
                                <asp:TextBox ID="mindate" runat="server" CssClass="textborder" ReadOnly="false" MaxLength =10 Width="120px">01/01/2006</asp:TextBox>
                                
                                
                                <asp:HyperLink ID="HypCalFrom" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif" ToolTip="Choose a Date">Choose a Date</asp:HyperLink>

                                <cc1:jcalendar id="JCalendar1" runat="server" controltoassign="mindate" imgurl="~/PresentationLayer/graph/calendar.gif" Visible =false ></cc1:jcalendar>
                                &nbsp;&nbsp;
                            </td>
                            <td align="right" style="width: 9%; height: 30px;">
                                <asp:Label ID="Tolab5" runat="server" Text="To"></asp:Label></td>
                            <td align="left" width="35%" style="height: 30px">
                                <asp:TextBox ID="maxdate" runat="server" CssClass="textborder" ReadOnly="false" MaxLength =10 Width="128px">31/12/2006</asp:TextBox>
                                
                                <asp:HyperLink ID="HypCalTo" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif" ToolTip="Choose a Date">Choose a Date</asp:HyperLink>

                                <cc1:jcalendar id="JCalendar2" runat="server" controltoassign="maxdate" imgurl="~/PresentationLayer/graph/calendar.gif" Visible =false  ></cc1:jcalendar>
                                &nbsp;
                            </td>
                        </tr>

                        <tr bgcolor="white">
                            <td align="right" style="width: 18%;">
                                <asp:Label ID="CustomerTypeLab" runat="server" Text="Label"></asp:Label>
                            </td>
                            <td align="left" colspan="4">
                                <asp:DropDownList ID="CustomerTypeDLL" runat="server" CssClass="textborder" AutoPostBack="false" onchange="LoadRaces(this)">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr bgcolor="white">
                            <td align="right" style="width: 18%;">
                                <asp:Label ID="RacesLab" runat="server" Text="Label"></asp:Label>
                            </td>
                            <td align="left" colspan="4">
                                <asp:DropDownList ID="RacesDLL" runat="server" CssClass="textborder" >
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr bgcolor="white">
                            <td align="right" style="width: 18%;">
                                <asp:Label ID="ProdClassLab" runat="server" Text="Label"></asp:Label>
                            </td>
                            <td align="left" colspan="4">
                                <asp:DropDownList ID="ProdClassDLL" runat="server" CssClass="textborder" AutoPostBack ="true"  >
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                    <asp:LinkButton ID="reportviewer" runat="server">LinkButton</asp:LinkButton>
            </tr>
        </table>
    
    </div>
        <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" AutoDataBind="true" />
    </form>
</body>
</html>
