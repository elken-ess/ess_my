﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="RPTCSVH.aspx.vb" Inherits="PresentationLayer_Report_RPTCSVH" %>

<%@ Register Assembly="JCalendar" Namespace="JCalendar" TagPrefix="cc1" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Customer Service History List</title>
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
        <LINK href="../css/style.css" type="text/css" rel="stylesheet">
        <script language="JavaScript" src="../js/common.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table id="countrytab" border="0" style="width: 100%">
            <tr>
                <td style="width: 100%">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title1.gif" width="5" /></td>
                            <td background="../graph/title_bg.gif" class="style2" width="98%" style="width: 100%">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title_2.gif" width="5" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100%">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width: 100%">
                        <tr>
                            <td background="../graph/title_bg.gif" class="style2" width="98%">
                                <font color="red" style="width: 100%">
                                    <asp:Label ID="errlab" runat="server" Text="Label" Visible="false"></asp:Label></font></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100%; height: 1px;">
                    <table id="country" bgcolor="#b7e6e6" border="0" cellpadding="2" cellspacing="1" style="width: 100%">
                        <tr bgcolor="#ffffff">
                            <td align="left" style="width: 13%">
                                <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 10%">
                                <asp:Label ID="Label7" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 36%">
                                <asp:TextBox ID="mincustmbox" runat="server" CssClass="textborder"></asp:TextBox></td>
                            <td align="left" style="width: 10%">
                                <asp:Label ID="Label11" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%">
                                <asp:TextBox ID="maxcustmbox" runat="server" CssClass="textborder"></asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="left" style="width: 13%">
                                <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 10%">
                                <asp:Label ID="Label10" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 36%">
                                <asp:TextBox ID="minsvsbox" runat="server" CssClass="textborder"></asp:TextBox></td>
                            <td align="left" style="width: 10%">
                                <asp:Label ID="Label13" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%">
                                <asp:TextBox ID="maxsvsbox" runat="server" CssClass="textborder"></asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="left" style="width: 13%; height: 28px;">
                                <asp:Label ID="Label3" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 10%; height: 28px;">
                                <asp:Label ID="Label12" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 36%; height: 28px;">
                                <asp:TextBox ID="minmodelbox" runat="server" CssClass="textborder"></asp:TextBox></td>
                            <td align="left" style="width: 10%; height: 28px;">
                                <asp:Label ID="Label15" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="height: 28px; width: 35%;">
                                <asp:TextBox ID="maxmodelbox" runat="server" CssClass="textborder"></asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="left" style="width: 13%; height: 27px;">
                                <asp:Label ID="Label9" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 10%; height: 27px;">
                                <asp:Label ID="Label22" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 36%; height: 27px;">
                                <asp:TextBox ID="mindatebox" runat="server" CssClass="textborder" ReadOnly="false" MaxLength=10></asp:TextBox>
                                &nbsp;
                                <asp:HyperLink ID="HypCalFrom" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif" ToolTip="Choose a Date">Choose a Date</asp:HyperLink>

                                <cc1:JCalendar ID="JCalendar1" runat="server" ControlToAssign="mindatebox"
                                    ImgURL="~/PresentationLayer/graph/calendar.gif" Visible =false  />
                            </td>
                            <td align="left" style="width: 10%; height: 27px;">
                                <asp:Label ID="Label24" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%; height: 27px">
                                <asp:TextBox ID="maxdatebox" runat="server" CssClass="textborder" ReadOnly="false" MaxLength=10></asp:TextBox>&nbsp;
                                
                                <asp:HyperLink ID="HypCalTo" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif" ToolTip="Choose a Date">Choose a Date</asp:HyperLink>

                                <cc1:JCalendar
                                    ID="JCalendar2" runat="server" ControlToAssign="maxdatebox" ImgURL="~/PresentationLayer/graph/calendar.gif" Visible =false  />
                                &nbsp;
                            </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="left" style="width: 13%; height: 28px">
                                <asp:Label ID="Label5" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 10%; height: 28px">
                                <asp:Label ID="Label17" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 36%; height: 28px">
                                <asp:TextBox ID="minstatebox" runat="server" CssClass="textborder"></asp:TextBox></td>
                            <td align="left" style="width: 10%; height: 28px">
                                <asp:Label ID="Label18" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%; height: 28px">
                                <asp:TextBox ID="maxstatebox" runat="server" CssClass="textborder"></asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="left" style="width: 13%; height: 28px">
                                <asp:Label ID="Label4" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 10%; height: 28px">
                                <asp:Label ID="Label14" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 36%; height: 28px">
                                <asp:TextBox ID="minroserial" runat="server" CssClass="textborder"></asp:TextBox></td>
                            <td align="left" style="width: 10%; height: 28px">
                                <asp:Label ID="Label16" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%; height: 28px">
                                <asp:TextBox ID="maxroserial" runat="server" CssClass="textborder"></asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="left" style="height: 2px" colspan="2">
                                <asp:Label ID="Label21" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 36%; height: 2px">
                                <asp:DropDownList ID="DropDownList1" runat="server" CssClass="textborder" Width="100%">
                                </asp:DropDownList></td>
                            <td align="left" style="width: 10%; height: 2px">
                                <asp:Label ID="Label6" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%; height: 2px">
                                <asp:DropDownList ID="DropDownList3" runat="server" CssClass="textborder" Width="100%">
                                </asp:DropDownList></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="left" style="width: 27%; height: 28px;" colspan =2>
                                <asp:Label ID="lblServiceBillType" runat="server" Text="Service Bill Type"></asp:Label></td>
                            <td align="left" style="width: 73%; height: 28px;" colspan =3>
                                <asp:DropDownList ID="cboServiceBillType" runat="server" Width="30%">
                                </asp:DropDownList></td>
                             
                            
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 37px; width: 100%;">
                    <table id="Table1" border="0" cellpadding="1" cellspacing="1">
                        <tr>
                            <td align="center" style="width: 20%; height: 21px;">
                                &nbsp;
        <asp:LinkButton ID="saveButton" runat="server">ViewReport</asp:LinkButton></td>
                            <td align="center" style="height: 21px">
                                </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        &nbsp;</div>
    </form>
</body>
</html>
