Imports SQLDataAccess
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
Imports BusinessEntity
Imports CrystalDecisions.Web
Imports CrystalDecisions.CrystalReports.Engine
Partial Class PresentationLayer_report_ContractRenewalLetterVIEW_aspx
    Inherits System.Web.UI.Page
    Dim clsReport As New ClsCommonReport

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        Dim crlEntity As New clsRPTCRL()
        crlEntity = Session("crlds_search_condition")
        Dim ds As DataSet = Nothing
        ds = crlEntity.GetContractRenewalLetter()



        Dim statXmlTr As New clsXml
        statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")



        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Dim objXmlTr As New clsXml
        Dim strHead As Array = Array.CreateInstance(GetType(String), 44)
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        strHead(0) = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0010")
        strHead(1) = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0016")
        strHead(2) = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0011")
        strHead(3) = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0012")
        strHead(4) = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0013")
        strHead(5) = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0014")
        strHead(6) = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0023") 'BB-CRL-0023
        strHead(7) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RENEWAL-LANG1A")
        strHead(8) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RENEWAL-LANG1B")
        strHead(9) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RENEWAL-LANG2A")
        strHead(10) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RENEWAL-LANG2B")
        strHead(11) = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0021")
        strHead(12) = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0017")
        strHead(13) = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0019")
        strHead(14) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_APT_RECORD")
        strHead(15) = ds.Tables(0).Rows.Count
        strHead(16) = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0023") 'BB-CRL-0023
        strHead(17) = Convert.ToString(crlEntity.StartDate) + "     " + "To" + "   " + Convert.ToString(crlEntity.EndDate)
        strHead(18) = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0006")
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        strHead(19) = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0001")
        strHead(20) = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0002")
        strHead(21) = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0003")
        strHead(22) = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0004")
        strHead(23) = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0023")
        strHead(24) = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0007")


        strHead(25) = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")

        strHead(26) = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
        strHead(40) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCUSR-USERID")
        strHead(39) = Session("userID") + "/" + Session("username")
        strHead(41) = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0018")
        strHead(42) = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0018")
        strHead(43) = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0018")



        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Me.titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0006")
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        If crlEntity.CustmIDFrom = Nothing Then
            strHead(27) = "AAA"
        Else
            strHead(27) = crlEntity.CustmIDFrom
        End If
        If crlEntity.CustmIDTo = "ZZZZZZZZZZ" Then
            strHead(28) = "ZZZ"
        Else
            strHead(28) = crlEntity.CustmIDTo
        End If
        If crlEntity.SvcIDFrom = Nothing Then
            strHead(29) = "AAA"
        Else
            strHead(29) = crlEntity.SvcIDFrom
        End If
        If crlEntity.SvcIDTo = "ZZZZZZZZZZ" Then
            strHead(30) = "ZZZ"
        Else
            strHead(30) = crlEntity.SvcIDTo
        End If
        If crlEntity.ModelIDFrom = Nothing Then
            strHead(31) = "AAA"
        Else
            strHead(31) = crlEntity.ModelIDFrom
        End If
        If crlEntity.ModelIDTo = "ZZZZZZZZZZ" Then
            strHead(32) = "ZZZ"
        Else
            strHead(32) = crlEntity.ModelIDTo
        End If
        If crlEntity.StateFrom = Nothing Then
            strHead(33) = "AAA"
        Else
            strHead(33) = crlEntity.StateFrom
        End If
        If crlEntity.StateTo = "ZZZZZZZZZZ" Then
            strHead(34) = "ZZZ"
        Else
            strHead(34) = crlEntity.StateTo
        End If
        If crlEntity.SerialNoFrom = Nothing Then
            strHead(37) = "AAA"
        Else
            strHead(37) = crlEntity.SerialNoFrom
        End If
        If crlEntity.SerialNoTo = "ZZZZZZZZZZ" Then
            strHead(38) = "ZZZ"
        Else
            strHead(38) = crlEntity.SerialNoTo
        End If


        If crlEntity.StartDate <> "" Then
            strHead(35) = crlEntity.StartDate
        Else
            strHead(35) = " ".ToString
        End If
        If crlEntity.EndDate <> "" Then
            strHead(36) = crlEntity.EndDate
        Else
            strHead(36) = " ".ToString()
        End If





        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


        'With clsReport
        '    .ReportFileName = "ContractRenewalLetter.rpt"  'TextBox1.Text
        '    .SetReport(CrystalReportViewer1, ds, strHead)


        'End With

        Dim lstrPdfFileName As String = "ContractRenewalLetter_" & Session("login_session") & ".pdf"


        Try
            With clsReport
                .ReportFileName = "ContractRenewalLetter.rpt"  'TextBox1.Text
                '.SetReport(CrystalReportViewer1, ds, strHead)
                .SetReportDocument(ds, strHead)
                'crReportDocument = .GetReportDocument
                .PdfFileName = lstrPdfFileName
                .ExportPdf()
            End With

            'crReportDocument.Export()
            Response.Redirect(clsReport.PdfUrl)

        Catch err As Exception
            'System.IO.File.Delete(lstrPhysicalFile)
            Response.Write("<BR>")
            Response.Write(err.Message.ToString)


        End Try
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Dim clsReport As New ClsCommonReport
        CrystalReportViewer1.Dispose()
    End Sub
End Class
