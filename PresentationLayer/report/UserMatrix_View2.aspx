<%@ Page Language="VB" AutoEventWireup="false" CodeFile="UserMatrix_View2.aspx.vb" Inherits="PresentationLayer_report_UserMatrix_View2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Untitled Page</title>
    <script language="javascript" type="text/javascript">
// <!CDATA[

function TABLE1_onclick() {

}
function printpage()
  {
  window.print()
  }
// ]]>
</script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table id="TABLE1" onclick="return TABLE1_onclick()" style="width: 1047px; height: 100px">
            <tr>
                <td align="left" colspan="8" style="height: 25px" valign="bottom">
                    &nbsp;
                    <input id="Button1" onclick="printpage()" size="21" style="color: black; border-top-style: solid;
                        border-right-style: solid; border-left-style: solid; background-color: transparent;
                        border-bottom-style: solid" type="button" value="Print" />
                    <asp:Button ID="Export" runat="server" BackColor="Transparent" BorderStyle="Solid"
                        Text="Export to Excel" />
                    <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="No data is available for selected range of criteria! "
                        Visible="False"></asp:Label></td>
            </tr>
            <tr>
                <td align="center" colspan="8" style="height: 22px" valign="bottom">
                    <asp:Label ID="Label29" runat="server" Font-Size="Medium" Text="Group Access Matrix"></asp:Label></td>
            </tr>
        </table>
        <br />
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False">
            <Columns>
                <asp:TemplateField HeaderText="No">
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" />
                    <ItemStyle Font-Size="Smaller" Width="35px" />
                    <ItemTemplate>
                        <%# Container.DataItemIndex + 1 %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="MENU" HeaderText="Menu" ReadOnly="True">
                    <ItemStyle Font-Size="Smaller" Width="85px" />
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="NO" HeaderText="ID" ReadOnly="True">
                    <ItemStyle Font-Size="Smaller" Width="25px" />
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="SCREEN" HeaderText="Screen" ReadOnly="True">
                    <ItemStyle Font-Size="Smaller" Width="200px" />
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="HOC" HeaderText="HOC" ReadOnly="True">
                    <ItemStyle Font-Size="Smaller" Width="50px" HorizontalAlign="Center"/>
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="AC-DE2" HeaderText="AC-DE2" ReadOnly="True">
                    <ItemStyle Font-Size="Smaller" Width="50px" HorizontalAlign="Center"/>
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="CR-BDE" HeaderText="CR-BDE" ReadOnly="True">
                    <ItemStyle Font-Size="Smaller" Width="50px" HorizontalAlign="Center"/>
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="CR-DE" HeaderText="CR-DE" ReadOnly="True">
                    <ItemStyle Font-Size="Smaller" Width="50px" HorizontalAlign="Center"/>
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="CR-SU" HeaderText="CR-SU" ReadOnly="True">
                    <ItemStyle Font-Size="Smaller" Width="50px" HorizontalAlign="Center"/>
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="TS-BDE" HeaderText="TS-BDE" ReadOnly="True">
                    <ItemStyle Font-Size="Smaller" Width="50px" HorizontalAlign="Center"/>
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="TS-BSU" HeaderText="TS-BSU" ReadOnly="True">
                    <ItemStyle Font-Size="Smaller" Width="50px" HorizontalAlign="Center"/>
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="TS-DE" HeaderText="TS-DE" ReadOnly="True">
                    <ItemStyle Font-Size="Smaller" Width="50px" HorizontalAlign="Center"/>
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="TS-SU" HeaderText="TS-SU" ReadOnly="True">
                    <ItemStyle Font-Size="Smaller" Width="50px" HorizontalAlign="Center"/>
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="TS-SU1" HeaderText="TS-SU1" ReadOnly="True">
                    <ItemStyle Font-Size="Smaller" Width="50px" HorizontalAlign="Center"/>
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="WH-BDE" HeaderText="WH-BDE" ReadOnly="True">
                    <ItemStyle Font-Size="Smaller" Width="60px" HorizontalAlign="Center"/>
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="WH-DE" HeaderText="WH-DE" ReadOnly="True">
                    <ItemStyle Font-Size="Smaller" Width="50px" HorizontalAlign="Center"/>
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="MYVIEW" HeaderText="MYVIEW" ReadOnly="True">
                    <ItemStyle Font-Size="Smaller" Width="50px" HorizontalAlign="Center"/>
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Center" />
                </asp:BoundField>
            </Columns>
            <EditRowStyle Font-Size="Smaller" />
        </asp:GridView>
    
    </div>
    </form>
</body>
</html>