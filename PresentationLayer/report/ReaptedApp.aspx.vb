﻿Imports BusinessEntity
Imports System.Configuration
Imports System.Xml
Imports System.Data
Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web

Partial Class PresentationLayer_Report_2ReaptedApp
    Inherits System.Web.UI.Page
    Dim clsReport As New ClsCommonReport

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Session("rptflag") = "0"
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            Me.from1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Me.from2.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Me.from3.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Me.from4.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Me.from5.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Me.to1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Me.to2.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Me.to3.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Me.to4.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Me.to5.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Me.serlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "RPT_APT_SER")
            Me.serilab.Text = objXmlTr.GetLabelName("EngLabelMsg", "RPT_APT_SERLID")
            Me.sertylab.Text = objXmlTr.GetLabelName("EngLabelMsg", "RPT_APT_SERTY")
            Me.datelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "RPT_APT_DATE")
            Me.cuslab.Text = objXmlTr.GetLabelName("EngLabelMsg", "RPT_APT_CUSID")
            Me.modelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "RPT_APT_MODELID")
            Me.repelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "RPT_APT_REP")
            Me.LinkButton1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-VIEWREPORT")
            Me.titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "RPT_APT_TITLE")

            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")

            Me.repnumerr.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "REPTER")


            Dim Appbond As New clsCommonClass
            Dim sertype As New DataSet
            sertype = Appbond.Getidname("BB_MASSRCT_IDNAME")
            databonds(sertype, servity)

        ElseIf Session("flag") = "1" Then
            print_report()
        End If
    End Sub
#Region " bonds"
    Public Function databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        Dim row As DataRow
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row In ds.Tables(0).Rows
                Dim NewItem As New ListItem()
                NewItem.Text = row("id") & "-" & row("name")
                NewItem.Value = row("id")
                dropdown.Items.Add(NewItem)
            Next
        End If
    End Function
#End Region

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton1.Click
        Session("rptflag") = "1"
        print_report()
    End Sub

    Public Function print_report()



        Try

            Dim RepeatEntity As New ClsReaptedApp()
            If (Me.sercen1.Text.Trim() <> "") Then
                RepeatEntity.ServiceID1 = Me.sercen1.Text.ToUpper()
            End If

            If (Trim(Me.sercen2.Text) <> "") Then

                RepeatEntity.ServiceID2 = sercen2.Text.ToUpper()
            Else
                RepeatEntity.ServiceID2 = "ZZZZZZZZZZ"
            End If
            'If (Me.sercen2.Text.Trim() <> "") Then
            '    RepeatEntity.ServiceID2 = Me.sercen2.Text.ToUpper()
            'End If

            If (Me.custom1.Text.Trim() <> "") Then
                RepeatEntity.CustomerID1 = Me.custom1.Text.ToUpper()
            End If
            If (Trim(Me.custom2.Text) <> "") Then

                RepeatEntity.CustomerID2 = custom2.Text.ToUpper()
            Else
                RepeatEntity.CustomerID2 = "ZZZZZZZZZZ"

            End If
            'If (Me.custom2.Text.Trim() <> "") Then
            '    RepeatEntity.CustomerID2 = Me.custom2.Text.ToUpper()
            'End If

            RepeatEntity.SerialNo1 = Me.seri1.Text.ToUpper()
            If (Trim(Me.seri2.Text) <> "") Then

                RepeatEntity.SerialNo2 = seri2.Text.ToUpper()
            Else
                RepeatEntity.SerialNo2 = "ZZZZZZZZZZ"

            End If

            'RepeatEntity.SerialNo2 = Me.seri2.Text.ToUpper()
            RepeatEntity.Model1 = Me.modelid1.Text.ToUpper()

            If (Trim(Me.modelid2.Text) <> "") Then

                RepeatEntity.Model2 = modelid2.Text.ToUpper()
            Else
                RepeatEntity.Model2 = "ZZZZZZZZZZ"

            End If
            'RepeatEntity.Model2 = Me.modelid2.Text.ToUpper()
            If (servity.Text.ToString() <> "") Then
                RepeatEntity.SerType = Me.servity.SelectedItem.Value.ToString()
            End If



            If (Trim(Me.date1.Text) <> "") Then
                Dim temparr As Array = date1.Text.Split("/")
                RepeatEntity.Datetime1 = temparr(2) + "-" + temparr(1) + "-" + temparr(0)

            End If
            If (Trim(Me.date2.Text) <> "") Then
                Dim temparr As Array = date2.Text.Split("/")
                RepeatEntity.Datetime2 = temparr(2) + "-" + temparr(1) + "-" + temparr(0)

            End If
            RepeatEntity.Reapted = Me.Reaptedp.SelectedItem.Text
            If (Trim(Me.repeat.Text) <> "") Then
                RepeatEntity.Reaptednum = Me.repeat.Text
            Else
                RepeatEntity.Reaptednum = "w"
            End If



            Dim reportds As DataSet = RepeatEntity.GetRepeatedSer()
            Dim objXmlTr As New clsXml
            Dim strHead As Array = Array.CreateInstance(GetType(String), 15)
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            strHead(0) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_APT_SERNO")
            strHead(1) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_APT_SERDATE")
            strHead(2) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_APT_TECHID")
            strHead(3) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_APT_APPSTAT")
            strHead(4) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_APT_NSTDT")
            strHead(5) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_APT_REM")
            strHead(6) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_APT_TITLE")
            strHead(7) = reportds.Tables(0).Rows.Count
            strHead(8) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_APT_RECORD")
            strHead(9) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_APT_REPID")
            strHead(10) = objXmlTr.GetLabelName("EngLabelMsg", "RPT-APT-TITLE1")
            strHead(11) = objXmlTr.GetLabelName("EngLabelMsg", "RPT-APT-TITLE2")
            strHead(12) = Me.date1.Text
            strHead(13) = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            strHead(14) = Me.date2.Text
            If Me.IsPostBack Then
                With clsReport
                    .ReportFileName = "RepeatSerRPT.rpt"
                    .SetReport(CrystalReportViewer1, reportds, strHead)
                End With

            End If
        Catch ex As Exception
            Response.Write(ex.Message)

        End Try

    End Function


    Protected Sub effecalender_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles effecalender.Click
        Me.Calendar1.Visible = True
        Calendar1.Attributes.Add("style", " POSITION: absolute")
        Session("rptflag") = "0"
    End Sub

    Protected Sub Calendar1_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendar1.SelectionChanged
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Dim prcidDate As New clsCommonClass()
        Me.date1.Text = Calendar1.SelectedDate.Date.ToString().Substring(0, 10)
        Calendar1.Visible = False
        Session("rptflag") = "0"

    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Me.Calendar2.Visible = True
        Calendar2.Attributes.Add("style", " POSITION: absolute")
        Session("rptflag") = "0"
    End Sub

    Protected Sub Calendar2_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendar2.SelectionChanged
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Dim prcidDate As New clsCommonClass()
        Me.date2.Text = Calendar2.SelectedDate.Date.ToString().Substring(0, 10)
        Calendar2.Visible = False
        Session("rptflag") = "0"
    End Sub



    Private Function ReportPath() As String
        ReportPath = ConfigurationSettings.AppSettings("ReportPath")
    End Function

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        clsReport.UnloadReport()
    End Sub
End Class
