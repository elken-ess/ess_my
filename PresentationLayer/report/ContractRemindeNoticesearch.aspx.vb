Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Web
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web
Imports BusinessEntity
Imports System.Data
Partial Class PresentationLayer_report_ContractRemindeNoticesearch_aspx
    Inherits System.Web.UI.Page
    Dim clsReport As New ClsCommonReport

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try

        Dim crnEntity As New clsRPTCRN

        If Trim(Session("userID")) = "" Then
            crnEntity.UserName = "ADMIN"
        Else
            crnEntity.UserName = Session("userID")
        End If

        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        crnEntity = Session("rptcontractremind_search_condition")
        Dim ds As DataSet = Nothing
        ds = crnEntity.GetContractRemindeNotice()


        Dim objXmlTr As New clsXml


        Dim strHead As Array = Array.CreateInstance(GetType(String), 42)
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        Me.titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRN-0001")


        If ds.Tables(0).Rows.Count <= 0 Then

            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            Dim lstrMessage = objXmlTr.GetLabelName("StatusMessage", "NoData")
            Dim script As String = "alert('" & lstrMessage & "');window.close()"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
        Else

            strHead(0) = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0010")
            strHead(1) = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0016")
            strHead(2) = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRN-0003")
            strHead(3) = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0014")
            strHead(4) = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0023") '& ":" 'BB-CRL-0023
            strHead(5) = objXmlTr.GetLabelName("EngLabelMsg", "BB-REMINDER-LANG1A")
            strHead(6) = objXmlTr.GetLabelName("EngLabelMsg", "BB-REMINDER-LANG1B")
            strHead(7) = objXmlTr.GetLabelName("EngLabelMsg", "BB-REMINDER-LANG2A")
            strHead(8) = objXmlTr.GetLabelName("EngLabelMsg", "BB-REMINDER-LANG2B")
            strHead(9) = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0021")
            strHead(10) = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0017")
            strHead(11) = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRN-0002")
            strHead(12) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_APT_RECORD")
            strHead(13) = ds.Tables(0).Rows.Count
            strHead(14) = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CNT-CNTSDATE") '& ":" '
            strHead(15) = Convert.ToString(crnEntity.StartDate) + "     " + "To" + "   " + Convert.ToString(crnEntity.EndDate)
            strHead(16) = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRN-0001")
            strHead(17) = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0001")
            strHead(18) = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0002")
            strHead(19) = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0003")
            strHead(20) = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0004")
            strHead(21) = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CNT-CNTSDATE")
            strHead(22) = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0007")
            strHead(23) = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            strHead(24) = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            strHead(37) = Session("userID") + "/" + Session("username")
            strHead(38) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCUSR-USERID")
            strHead(39) = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0018")
            strHead(40) = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0018")
            strHead(41) = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0018")





            If (crnEntity.CustmIDFrom = Nothing) Then
                strHead(25) = "AAA"
            Else
                strHead(25) = crnEntity.CustmIDFrom
            End If
            If crnEntity.CustmIDTo = "ZZZZZZZZZZ" Then
                strHead(31) = "ZZZ"
            Else
                strHead(31) = crnEntity.CustmIDTo
            End If
            If (crnEntity.SvcIDFrom = Nothing) Then
                strHead(26) = "AAA"
            Else
                strHead(26) = crnEntity.SvcIDFrom
            End If
            If crnEntity.SvcIDTo = "ZZZZZZZZZZ" Then
                strHead(32) = "ZZZ"
            Else
                strHead(32) = crnEntity.SvcIDTo
            End If
            If (crnEntity.ModelIDFrom = Nothing) Then
                strHead(27) = "AAA"
            Else
                strHead(27) = crnEntity.ModelIDFrom
            End If
            If crnEntity.ModelIDTo = "ZZZZZZZZZZ" Then
                strHead(33) = "ZZZ"
            Else
                strHead(33) = crnEntity.ModelIDTo
            End If
            If crnEntity.StateFrom = Nothing Then
                strHead(28) = "AAA"
            Else
                strHead(28) = crnEntity.StateFrom
            End If
            If crnEntity.StateTo = "ZZZZZZZZZZ" Then
                strHead(34) = "ZZZ"
            Else
                strHead(34) = crnEntity.StateTo
            End If
            If (crnEntity.SerialNoFrom = Nothing) Then
                strHead(30) = "AAA"
            Else
                strHead(30) = crnEntity.SerialNoFrom
            End If
            If crnEntity.SerialNoTo = "ZZZZZZZZZZ" Then
                strHead(36) = "ZZZ"
            Else
                strHead(36) = crnEntity.SerialNoTo
            End If
            ''''''''''''''''''''''''0
            If crnEntity.StartDate <> "" Then
                strHead(29) = crnEntity.StartDate
            Else
                strHead(29) = " ".ToString
            End If
            If crnEntity.EndDate <> "" Then
                strHead(35) = crnEntity.EndDate
            Else
                strHead(35) = " ".ToString()
            End If

            'If Trim(crnEntity.StartDate) <> "" Then
            '    strHead(29) = crnEntity.StartDate
            'Else
            '    strHead(29) = " ".ToString
            'End If

            'With clsReport
            '    .ReportFileName = "ContractReminderNotice.rpt"  'TextBox1.Text
            '    .SetReport(CrystalReportViewer1, ds, strHead)
            'End With

            Dim lstrPdfFileName As String = "ContractReminderNotice_" & Session("login_session") & ".pdf"


            Try
                With clsReport
                    .ReportFileName = "ContractReminderNotice.rpt"  'TextBox1.Text
                    '.SetReport(CrystalReportViewer1, ds, strHead)
                    .SetReportDocument(ds, strHead)
                    'crReportDocument = .GetReportDocument
                    .PdfFileName = lstrPdfFileName
                    .ExportPdf()
                End With

                'crReportDocument.Export()
                Response.Redirect(clsReport.PdfUrl)

            Catch err As Exception
                'System.IO.File.Delete(lstrPhysicalFile)
                Response.Write("<BR>")
                Response.Write(err.Message.ToString)


            End Try

        End If
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        clsReport.UnloadReport()
        CrystalReportViewer1.Dispose()
    End Sub
End Class
