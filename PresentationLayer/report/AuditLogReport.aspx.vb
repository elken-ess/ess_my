﻿Imports System.Configuration
Imports System.Globalization
Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports BusinessEntity

Partial Class PresentationLayer_report_AuditLogReport
    Inherits System.Web.UI.Page

    Dim objXML As New clsXml
    Dim clsALR As New clsAuditLogReport
    Dim dateStyle As CultureInfo = New System.Globalization.CultureInfo("en-CA")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim script As String = "top.location='../logon.aspx';"
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                    'Response.Redirect("~/PresentationLayer/logon.aspx")
                End If
            Catch ex As Exception
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try

            Session.Contents("AuditLogPrintDataFlag") = ""
            LoadInfo()

            'Set User Control Purview
            SetUserPurview()

            'printButton.Enabled = False
        ElseIf Session.Contents("AuditLogPrintDataFlag") = "1" Then
            PrintData()
        Else
            ErrLab.Visible = False
        End If
        HypCalFrom.NavigateUrl = "javascript:DoCal(document.form1.DateFTextBox);"
        HypCalTo.NavigateUrl = "javascript:DoCal(document.form1.DateTTextBox);"

    End Sub

    Protected Sub LoadInfo()
        'Display the Label or Button Text=================================================
        objXML.XmlFile = ConfigurationManager.AppSettings("XmlFilePath")
        Me.titleLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_ALR_TITLE")
        Me.ServiceCenterFLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_FSC")
        Me.ServiceCenterTLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_TSC")
        Me.DeptFLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_ALR_DEPF")
        Me.DeptTLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_ALR_DEPT")
        Me.UserIDFLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_FUI")
        Me.UserIDTLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_TUI")
        Me.DateFLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_FD")
        Me.DateTLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_TD")
        Me.LoginTimeFLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_FLT")
        Me.LoginTimeTLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_TLT")
        Me.ScreenIDFLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_ALR_SCRF")
        Me.ScreenIDTLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_ALR_SCRT")
        Me.SessionIDFLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_ALR_SESF")
        Me.SessionIDTLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_ALR_SEST")
        Me.SortByLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_ALR_SORT")
        Me.printReportButton.Text = objXML.GetLabelName("EngLabelMsg", "BB-VIEWREPORT")
        'Me.printButton.Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_PRI")
        Me.ServiceCenterFTextBox.Text = "AAA"
        Me.ServiceCenterTTextBox.Text = "ZZZ"
        Me.DeptFTextBox.Text = "AAA"
        Me.DeptTTextBox.Text = "ZZZ"
        Me.UserIDFTextBox.Text = "AAA"
        Me.UserIDTTextBox.Text = "ZZZ"
        Me.DateFTextBox.Text = "01/01/2006"
        Me.DateTTextBox.Text = "31/12/2006"
        Me.ScreenIDFTextBox.Text = "AAA"
        Me.ScreenIDTTextBox.Text = "ZZZ"
        Me.SessionIDFTextBox.Text = "AAA"
        Me.SessionIDTTextBox.Text = "ZZZ"

        Me.SortByDropDownList.Items.Add(New ListItem(objXML.GetLabelName("EngLabelMsg", "BB_ALR_SORTBYSC"), "MUSR_SVCID"))
        Me.SortByDropDownList.Items.Add(New ListItem(objXML.GetLabelName("EngLabelMsg", "BB_ALR_SORTBYUI"), "MUSR_USRID"))
        Me.SortByDropDownList.Items.Add(New ListItem(objXML.GetLabelName("EngLabelMsg", "BB_ALR_SORTBYSI"), "SADF_SESID"))
        Me.SortByDropDownList.Items.Add(New ListItem(objXML.GetLabelName("EngLabelMsg", "BB_ALR_SORTBYDT"), "SADF_LSTDT"))

    End Sub

    Protected Sub retrieveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles printReportButton.Click
        'Retrieve System Access Data from Server
        RetrieveData()
        'Dim strFromTime As String = "&FHour=" + HourFTextBox.Text + "&FMin=" + MinuteFTextBox.Text + "&FSecond=" + SecondFTextBox.Text
        'Dim strToTime As String = "&THour=" + HourTTextBox.Text + "&TMin=" + MinuteTTextBox.Text + "&TSecond=" + SecondTTextBox.Text
        'Dim strQuery As String = "&FServerCenter=" + Trim(ServiceCenterFTextBox.Text) + "&TServerCenter=" + Trim(ServiceCenterTTextBox.Text) _
        '                        + "&FDept=" + Trim(DeptFTextBox.Text) + "&TDept=" + Trim(DeptTTextBox.Text) _
        '                        + "&FUserID=" + Trim(UserIDFTextBox.Text) + "&TUserID=" + Trim(UserIDTTextBox.Text) _
        '                        + "&FDate=" + Trim(DateFTextBox.Text) + "&TDate=" + Trim(DateTTextBox.Text) _
        '                        + "&FScreenID=" + Trim(ScreenIDFTextBox.Text) + "&TScreenID=" + Trim(ScreenIDTTextBox.Text) _
        '                        + "&FSessionID=" + Trim(SessionIDFTextBox.Text) + "&TSessionID=" + Trim(SessionIDTTextBox.Text) _
        '                        + "&SortBy=" + Trim(SortByDropDownList.SelectedValue)

        'Response.Redirect("~/PresentationLayer/report/AuditLogReportView.aspx?SortBy=" + _
        '    Trim(SortByDropDownList.SelectedItem.ToString) + strFromTime + strToTime)
        Dim SVCFr As String = Trim(ServiceCenterFTextBox.Text)
        Dim SVCTo As String = Trim(ServiceCenterTTextBox.Text)
        Dim DeptFr As String = Trim(DeptFTextBox.Text)
        Dim DeptTo As String = Trim(DeptTTextBox.Text)
        Dim UserFr As String = Trim(UserIDFTextBox.Text)
        Dim UserTo As String = Trim(UserIDTTextBox.Text)
        Dim DateFr As String = Request.Form("DateFTextBox") 'Trim(DateFTextBox.Text)
        Dim DateTo As String = Request.Form("DateTTextBox") 'Trim(DateTTextBox.Text)
        Dim TimeFr As String = HourFTextBox.Text + ":" + MinuteFTextBox.Text + ":" + SecondFTextBox.Text
        Dim TimeTo As String = HourTTextBox.Text + ":" + MinuteTTextBox.Text + ":" + SecondTTextBox.Text
        Dim ScreenFr As String = Trim(ScreenIDFTextBox.Text)
        Dim ScreenTo As String = Trim(ScreenIDTTextBox.Text)
        Dim SessionFr As String = Trim(SessionIDFTextBox.Text)
        Dim SessionTo As String = Trim(SessionIDTTextBox.Text)
        Dim SortBy As String = Trim(SortByDropDownList.SelectedValue)
        Dim SortByText As String = Trim(SortByDropDownList.SelectedItem.Text)

        Dim script As String = "window.open('../report/AuditLogReportView.aspx?SVCFr=" & Server.UrlEncode(SVCFr) + _
            "&SVCTo=" & Server.UrlEncode(SVCTo) + _
            "&DeptFr=" & Server.UrlEncode(DeptFr) + _
            "&DeptTo=" & Server.UrlEncode(DeptTo) + _
            "&UserFR=" & Server.UrlEncode(UserFr) + _
            "&UserTo=" & Server.UrlEncode(UserTo) + _
            "&DateFr=" & Server.UrlEncode(DateFr) + _
            "&DateTo=" & Server.UrlEncode(DateTo) + _
            "&TimeFr=" & Server.UrlEncode(TimeFr) + _
            "&TimeTo=" & Server.UrlEncode(TimeTo) + _
            "&ScreenFr=" & Server.UrlEncode(ScreenFr) + _
            "&ScreenTo=" & Server.UrlEncode(ScreenTo) + _
            "&SortByText=" & Server.UrlEncode(SortByText) + _
            "&SortBy=" & Server.UrlEncode(SortBy) + " ')"

        'Dim script As String = "window.open('../report/AuditLogReportView.aspx?SVCFr=" & Server.UrlEncode(SVCFr) + _
        '   "&SVCTo=" & (SVCTo) + _
        '   "&DeptFr=" & (DeptFr) + _
        '   "&DeptTo=" & (DeptTo) + _
        '   "&UserFR=" & (UserFr) + _
        '   "&UserTo=" & (UserTo) + _
        '   "&DateFr=" & (DateFr) + _
        '   "&DateTo=" & (DateTo) + _
        '   "&TimeFr=" & (TimeFr) + _
        '   "&TimeTo=" & (TimeTo) + _
        '   "&ScreenFr=" & (ScreenFr) + _
        '   "&ScreenTo=" & (ScreenTo) + _
        '     "&SortBy=" & (SortBy) + " ')"
        DateFTextBox.Text = Request.Form("DateFTextBox")
        DateTTextBox.Text = Request.Form("DateTTextBox")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "AuditLogReport", script, True)
    End Sub

    Private Sub RetrieveData()
        'clsALR.FromServiceCenter = Trim(ServiceCenterFTextBox.Text)
        'clsALR.ToServiceCenter = Trim(ServiceCenterTTextBox.Text)
        'clsALR.FromDept = Trim(DeptFTextBox.Text)
        'clsALR.ToDept = Trim(DeptTTextBox.Text)
        'clsALR.FromUserID = Trim(UserIDFTextBox.Text)
        'clsALR.ToUserID = Trim(UserIDTTextBox.Text)
        'clsALR.FromDate = Trim(DateFTextBox.Text)
        'clsALR.ToDate = Trim(DateTTextBox.Text)
        'clsALR.FromLoginTime = HourFTextBox.Text + ":" + MinuteFTextBox.Text + ":" + SecondFTextBox.Text
        'clsALR.ToLoginTime = HourTTextBox.Text + ":" + MinuteTTextBox.Text + ":" + SecondTTextBox.Text
        'clsALR.FromScreenID = Trim(ScreenIDFTextBox.Text)
        'clsALR.ToScreenID = Trim(ScreenIDTTextBox.Text)
        'clsALR.FromSessionID = Trim(SessionIDFTextBox.Text)
        'clsALR.ToSessionID = Trim(SessionIDTTextBox.Text)
        'clsALR.SortBy = Trim(SortByDropDownList.SelectedValue)
        'clsALR.LoginUserID = Session("userID").ToString

        'Session("FServerCenter_AuditReport") = Trim(ServiceCenterFTextBox.Text)
        'Session("TServerCenter_AuditReport") = Trim(ServiceCenterTTextBox.Text)
        'Session("FDept_AuditReport") = Trim(DeptFTextBox.Text)
        'Session("TDept_AuditReport") = Trim(DeptTTextBox.Text)
        'Session("FUserID_AuditReport") = Trim(UserIDFTextBox.Text)
        'Session("TUserID_AuditReport") = Trim(UserIDTTextBox.Text)
        'Session("FDate_AuditReport") = Trim(DateFTextBox.Text)
        'Session("TDate_AuditReport") = Trim(DateTTextBox.Text)
        'Session("FTime_AuditReport") = HourFTextBox.Text + ":" + MinuteFTextBox.Text + ":" + SecondFTextBox.Text
        'Session("TTime_AuditReport") = HourTTextBox.Text + ":" + MinuteTTextBox.Text + ":" + SecondTTextBox.Text
        'Session("FScreenID_AuditReport") = Trim(ScreenIDFTextBox.Text)
        'Session("TScreenID_AuditReport") = Trim(ScreenIDTTextBox.Text)
        'Session("FSessionID_AuditReport") = Trim(SessionIDFTextBox.Text)
        'Session("TSessionID_AuditReport") = Trim(SessionIDTTextBox.Text)
        'Session("SortBy_AuditReport") = Trim(SortByDropDownList.SelectedValue)

        'Dim dsALR As DataSet = clsALR.RetriveAuditLogInfo()
        'If dsALR Is Nothing Then
        '    objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
        '    ErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_FUN_CALLLIST_RERR")
        '    ErrLab.Visible = True
        '    Return
        'End If

        'Dim i As Integer

        'If dsALR.Tables.Count > 0 Then
        '    For i = 0 To dsALR.Tables(0).Rows.Count - 1
        '        dsALR.Tables(0).Rows(i).Item(0) = i + 1
        '    Next
        '    'AuditLogGridView.DataSource = dsALR
        '    'AuditLogGridView.DataBind()
        'Else
        '    Return
        'End If

        'If dsALR.Tables(0).Rows.Count > 0 Then
        '    DisplayGridViewHeader()
        'End If

        'printButton.Enabled = (dsALR.Tables(0).Rows.Count > 0)
        'And (clsUserAccessGroup.GetUserPurview(Session("accessgroup"), "54", "P"))
        'Session("AuditLogReport") = dsALR
        'ALRReportViewer.Visible = False

        'PrintData()
    End Sub

#Region "Display GridView"
    Protected Sub DisplayGridViewHeader()
        objXML.XmlFile = ConfigurationManager.AppSettings("XmlFilePath")

        Me.AuditLogGridView.HeaderRow.Cells(0).Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_NO")
        Me.AuditLogGridView.HeaderRow.Cells(1).Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_ALR_SERC")
        Me.AuditLogGridView.HeaderRow.Cells(2).Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_ALR_DT")
        Me.AuditLogGridView.HeaderRow.Cells(3).Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_ALR_USER")
        Me.AuditLogGridView.HeaderRow.Cells(4).Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_ALR_SESS")
        Me.AuditLogGridView.HeaderRow.Cells(5).Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_IPA")
        Me.AuditLogGridView.HeaderRow.Cells(6).Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_ALR_SCR")
        Me.AuditLogGridView.HeaderRow.Cells(7).Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_ALR_OV")
        Me.AuditLogGridView.HeaderRow.Cells(8).Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_ALR_NV")

        Me.AuditLogGridView.HeaderRow.Font.Size = 8
    End Sub
#End Region

    Private Sub PrintData()
        Dim dsReport As DataSet = Session.Contents("AuditLogReport")
        Dim callListReportDoc As New ReportDocument()

        Try
            callListReportDoc.Load(MapPath("AuditLogCrystalReport.rpt"))
            ErrLab.Visible = False
        Catch ex As Exception
            objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
            ErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_FUN_CALLLIST_LOADFAILED")
            ErrLab.Visible = True
            Return
        End Try
        callListReportDoc.SetDataSource(dsReport.Tables(0))

        objXML.XmlFile = ConfigurationManager.AppSettings("XmlFilePath")
        callListReportDoc.SetParameterValue("NO", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_NO"))
        callListReportDoc.SetParameterValue("ServiceCenter", objXML.GetLabelName("EngLabelMsg", "BB_FUN_ALR_SERC"))
        callListReportDoc.SetParameterValue("Date", objXML.GetLabelName("EngLabelMsg", "BB_FUN_ALR_DT"))
        callListReportDoc.SetParameterValue("UserInfo", objXML.GetLabelName("EngLabelMsg", "BB_FUN_ALR_USER"))
        callListReportDoc.SetParameterValue("SessionID", objXML.GetLabelName("EngLabelMsg", "BB_FUN_ALR_SESS"))
        callListReportDoc.SetParameterValue("IPAddress", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_IPA"))
        callListReportDoc.SetParameterValue("FuncID", objXML.GetLabelName("EngLabelMsg", "BB_FUN_ALR_SCR"))
        callListReportDoc.SetParameterValue("OldVal", objXML.GetLabelName("EngLabelMsg", "BB_FUN_ALR_OV"))
        callListReportDoc.SetParameterValue("NewVal", objXML.GetLabelName("EngLabelMsg", "BB_FUN_ALR_NV"))
        callListReportDoc.SetParameterValue("ReportID", objXML.GetLabelName("EngLabelMsg", "BB_FUN_ALR_TITLE"))
        callListReportDoc.SetParameterValue("UserIDName", Session("userID").ToString)

        callListReportDoc.SetParameterValue("FromServiceCenterLab", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_FSC"))
        callListReportDoc.SetParameterValue("ToServiceCenterLab", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_TSC"))
        callListReportDoc.SetParameterValue("FromServiceCenterBox", dsReport.Tables(1).Rows(0).Item("FromServiceCenter"))
        callListReportDoc.SetParameterValue("ToServiceCenterBox", dsReport.Tables(2).Rows(0).Item("ToServiceCenter"))
        callListReportDoc.SetParameterValue("FromDeptLab", objXML.GetLabelName("EngLabelMsg", "BB_FUN_ALR_DEPF"))
        callListReportDoc.SetParameterValue("ToDeptLab", objXML.GetLabelName("EngLabelMsg", "BB_FUN_ALR_DEPT"))
        callListReportDoc.SetParameterValue("FromDeptBox", dsReport.Tables(3).Rows(0).Item("FromDept"))
        callListReportDoc.SetParameterValue("ToDeptBox", dsReport.Tables(4).Rows(0).Item("ToDept"))
        callListReportDoc.SetParameterValue("FromUserIDLab", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_FUI"))
        callListReportDoc.SetParameterValue("ToUserIDLab", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_TUI"))
        callListReportDoc.SetParameterValue("FromUserIDBox", dsReport.Tables(5).Rows(0).Item("FromUserID"))
        callListReportDoc.SetParameterValue("ToUserIDBox", dsReport.Tables(6).Rows(0).Item("ToUserID"))
        callListReportDoc.SetParameterValue("FromDateLab", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_FD"))
        callListReportDoc.SetParameterValue("ToDateLab", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_TD"))
        callListReportDoc.SetParameterValue("FromDateBox", dsReport.Tables(7).Rows(0).Item("FromDate"))
        callListReportDoc.SetParameterValue("ToDateBox", dsReport.Tables(8).Rows(0).Item("ToDate"))
        callListReportDoc.SetParameterValue("FromLoginTimeLab", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_FLT"))
        callListReportDoc.SetParameterValue("ToLoginTimeLab", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_TLT"))
        callListReportDoc.SetParameterValue("FromLoginTimeBox", HourFTextBox.Text + ":" + MinuteFTextBox.Text + ":" + SecondFTextBox.Text)
        callListReportDoc.SetParameterValue("ToLoginTimeBox", HourTTextBox.Text + ":" + MinuteTTextBox.Text + ":" + SecondTTextBox.Text)
        callListReportDoc.SetParameterValue("FromScreenIDLab", objXML.GetLabelName("EngLabelMsg", "BB_FUN_ALR_SCRF"))
        callListReportDoc.SetParameterValue("ToScreenIDLab", objXML.GetLabelName("EngLabelMsg", "BB_FUN_ALR_SCRT"))
        callListReportDoc.SetParameterValue("FromScreenIDBOX", dsReport.Tables(9).Rows(0).Item("FromScreenID"))
        callListReportDoc.SetParameterValue("ToScreenIDBOX", dsReport.Tables(10).Rows(0).Item("ToScreenID"))
        callListReportDoc.SetParameterValue("FromSessionIDLab", objXML.GetLabelName("EngLabelMsg", "BB_FUN_ALR_SESF"))
        callListReportDoc.SetParameterValue("ToSessionIDLab", objXML.GetLabelName("EngLabelMsg", "BB_FUN_ALR_SEST"))
        callListReportDoc.SetParameterValue("FromSessionIDBOX", dsReport.Tables(11).Rows(0).Item("FromSessionID"))
        callListReportDoc.SetParameterValue("ToSessionIDBOX", dsReport.Tables(12).Rows(0).Item("ToSessionID"))
        callListReportDoc.SetParameterValue("SortByLab", objXML.GetLabelName("EngLabelMsg", "BB_FUN_ALR_SORT"))
        callListReportDoc.SetParameterValue("SortByBOX", Trim(SortByDropDownList.SelectedItem.ToString))

        ALRReportViewer.ReportSource = callListReportDoc

        Session.Contents("AuditLogPrintDataFlag") = "1"
    End Sub

    Protected Sub AuditLogGridView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles AuditLogGridView.PageIndexChanging
        AuditLogGridView.PageIndex = e.NewPageIndex
        System.Threading.Thread.CurrentThread.CurrentCulture = dateStyle

        Dim dsALR As DataSet = Session.Contents("AuditLogReport")
        AuditLogGridView.DataSource = dsALR
        AuditLogGridView.DataBind()

        If (dsALR.Tables(0).Rows.Count > 0) Then
            DisplayGridViewHeader()
        End If
    End Sub

    Protected Sub printButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles printButton.Click
        PrintData()
        ALRReportViewer.Visible = True
    End Sub

    'Protected Sub AuditLogDateFButton_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles AuditLogDateFButton.Click
    '    AuditLogFCalendar.Visible = Not AuditLogFCalendar.Visible
    '    AuditLogFCalendar.Style.Item("position") = "absolute"
    'End Sub

    'Protected Sub AuditLogFCalendar_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles AuditLogFCalendar.SelectionChanged
    '    System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
    '    DateFTextBox.Text = AuditLogFCalendar.SelectedDate
    '    AuditLogFCalendar.Visible = False
    'End Sub

    'Protected Sub AuditLogDateTButton_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles AuditLogDateTButton.Click
    '    AuditLogTCalendar.Visible = Not AuditLogTCalendar.Visible
    '    AuditLogTCalendar.Style.Item("position") = "absolute"
    'End Sub

    'Protected Sub AuditLogTCalendar_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles AuditLogTCalendar.SelectionChanged
    '    System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
    '    DateTTextBox.Text = AuditLogTCalendar.SelectedDate
    '    AuditLogTCalendar.Visible = False
    'End Sub

    Private Sub SetUserPurview()
        Dim arrayPurview = New Boolean() {False, False, False, False, False}
        arrayPurview = clsUserAccessGroup.GetUserPurview(Session("accessgroup"), "54")

        printReportButton.Enabled = arrayPurview(3)
        'printButton.Enabled = arrayPurview(3)
    End Sub

    Protected Sub AuditLogDateFButton_SelectedDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles AuditLogDateFButton.SelectedDateChanged
        System.Threading.Thread.CurrentThread.CurrentCulture = dateStyle
        DateFTextBox.Text = AuditLogDateFButton.SelectedDate
    End Sub

    Protected Sub AuditLogDateTButton_SelectedDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles AuditLogDateTButton.SelectedDateChanged
        System.Threading.Thread.CurrentThread.CurrentCulture = dateStyle
        DateTTextBox.Text = AuditLogDateTButton.SelectedDate
    End Sub

    Protected Sub AuditLogDateFButton_CalendarVisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles AuditLogDateFButton.CalendarVisibleChanged
        SortByDropDownList.Visible = Not SortByDropDownList.Visible
    End Sub
End Class
