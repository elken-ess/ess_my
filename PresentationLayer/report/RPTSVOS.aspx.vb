﻿Imports System.Web
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web
Imports BusinessEntity
Imports System.Data
Partial Class PresentationLayer_Report_RPTSVOSNEW
    Inherits System.Web.UI.Page

    Dim fstrMonth As String = ""
    Dim fstrDefaultStartDate As String = ""
    Dim fstrDefaultEndDate As String = ""
    Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        fstrMonth = Month(System.DateTime.Today)
        fstrDefaultStartDate = System.DateTime.Today
        'fstrDefaultEndDate = DateAdd(DateInterval.Month, 1, CDate(fstrDefaultStartDate))
        fstrDefaultEndDate = fstrDefaultStartDate
        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If

        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try

        If Not Page.IsPostBack Then
            'Session("SVOSflag") = "0"

            '''access control'''''''''''''''''''''''''''''''''''''''''''''''
            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "41")
            Me.saveButton.Enabled = purviewArray(3)
            '''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim objXmlTr As New clsXml ''
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            Me.Label10.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Me.Label7.Text = Me.Label10.Text
            Me.Label12.Text = Me.Label10.Text
            Me.Label14.Text = Me.Label10.Text
            Me.Label17.Text = Me.Label10.Text
            Me.Label19.Text = Me.Label10.Text
            Me.Label21.Text = Me.Label10.Text
            Me.Label22.Text = Me.Label10.Text
            Me.Label11.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Me.Label13.Text = Me.Label11.Text
            Me.Label15.Text = Me.Label11.Text
            Me.Label16.Text = Me.Label11.Text
            Me.Label18.Text = Me.Label11.Text
            Me.Label20.Text = Me.Label11.Text
            Me.Label23.Text = Me.Label11.Text
            Me.Label24.Text = Me.Label11.Text

            Me.Label1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0012")
            Me.Label2.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0013")
            Me.Label3.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0014")
            Me.Label4.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0015")
            Me.Label5.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0016")
            Me.Label6.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0017")
            Me.Label8.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0018")
            Me.Label9.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0020")
            Me.lblServiceType.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-SERVICETYPE")
            Me.lblStatus.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-STATUS")

            Me.maxroserial.Text = "ZZZ"
            Me.maxareabox.Text = "ZZZ"
            Me.maxcustmbox.Text = "ZZZ"
            Me.maxmodelbox.Text = "ZZZ"
            Me.maxstatebox.Text = "ZZZ"
            Me.maxsvsbox.Text = "ZZZ"
            Me.maxtchnicanbox.Text = "ZZZ"

            Me.minroserial.Text = "AAA"
            Me.minsvsbox.Text = "AAA"
            Me.minstatebox.Text = "AAA"
            Me.minmodelbox.Text = "AAA"
            Me.mincustmbox.Text = "AAA"
            Me.mintchnicanbox.Text = "AAA"
            Me.minareabox.Text = "AAA"

            Me.mindatebox.Text = fstrDefaultStartDate
            Me.maxdatebox.Text = fstrDefaultEndDate

            Dim apptStat As New clsUtil()
            Dim statParam As ArrayList = New ArrayList

            cboStatus.Items.Add(New ListItem("All", ""))
            statParam = apptStat.searchconfirminf("APPTStatus")
            Dim count As Integer
            Dim statid As String
            Dim statnm As String
            For count = 0 To statParam.Count - 1
                statid = statParam.Item(count)
                statnm = statParam.Item(count + 1)
                count = count + 1

                If statid <> "UA" And statid <> "X" Then
                    cboStatus.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                End If
            Next

            cboStatus.Items(0).Selected = True


            Dim installbond As New clsCommonClass
            'bond  service type id and name
            Dim sertype As New DataSet
            sertype = installbond.Getidname("BB_RPTPSLT_IDNAME '" & Session("userID") & "'")
            databonds(sertype, Me.cboServiceType)

            Me.saveButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-VIEWREPORT")
            ' Me.HyperLink1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-VIEWREPORT")
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0022")

        End If
        HypCalFrom.NavigateUrl = "javascript:DoCal(document.form1.mindatebox);"
        HypCalTo.NavigateUrl = "javascript:DoCal(document.form1.maxdatebox);"


    End Sub

#Region " bonds"
    Public Function databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        dropdown.Items.Add(New ListItem("All", ""))

        Dim row As DataRow
        If ds.Tables(0).Rows.Count > 0 Then
            'dropdown.Items.Add("")
            For Each row In ds.Tables(0).Rows
                Dim NewItem As New ListItem()
                NewItem.Text = Trim(row("id")) & "-" & row("name")
                NewItem.Value = Trim(row("id"))
                dropdown.Items.Add(NewItem)
            Next
        End If
    End Function
#End Region

    Protected Sub saveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveButton.Click
        Session("SVOSflag") = "1"
        viewreport()
        Dim script As String = "window.open('../report/RPTSVOSVIEW.aspx')"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "RPTSVOSVIEW", script, True)
        Return

    End Sub

#Region " view report message"
    Public Function viewreport()
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        Dim rptsvos As New clsRPTSVOS

        mindatebox.Text = Request.Form("mindatebox")
        maxdatebox.Text = Request.Form("maxdatebox")


        If Not IsDate(mindatebox.Text) Then
            mindatebox.Text = fstrDefaultStartDate
        End If
        If Not IsDate(maxdatebox.Text) Then
            maxdatebox.Text = fstrDefaultEndDate
        End If

        If (Me.mincustmbox.Text <> "AAA") Then
            rptsvos.minCustmID = Me.mincustmbox.Text.ToUpper
        Else
            rptsvos.minCustmID = ""
        End If
        If (Me.maxcustmbox.Text <> "ZZZ") Then
            rptsvos.maxCustmID = Me.maxcustmbox.Text.ToUpper
        Else
            rptsvos.maxCustmID = "ZZZZZZZZZZZZZZZZZZZZ"
        End If
        If (Me.minsvsbox.Text <> "AAA") Then
            rptsvos.minSvcID = Me.minsvsbox.Text.ToUpper
        Else
            rptsvos.minSvcID = ""
        End If
        If (Me.maxsvsbox.Text <> "ZZZ") Then
            rptsvos.maxSvcID = Me.maxsvsbox.Text.ToUpper
        Else
            rptsvos.maxSvcID = "ZZZZZZZZZZZZZZZZZZZZ"
        End If
        If (Me.minmodelbox.Text <> "AAA") Then
            rptsvos.minPrdctMd = Me.minmodelbox.Text.ToUpper
        Else
            rptsvos.minPrdctMd = ""
        End If
        If (Me.maxmodelbox.Text <> "ZZZ") Then
            rptsvos.maxPrdctMd = Me.maxmodelbox.Text.ToUpper
        Else
            rptsvos.maxPrdctMd = "ZZZZZZZZZZZZZZZZZZZZ"
        End If
        If (Me.minroserial.Text <> "AAA") Then
            rptsvos.minRONo = Me.minroserial.Text.ToUpper
        Else
            rptsvos.minRONo = ""
        End If
        If (Me.maxroserial.Text <> "ZZZ") Then
            rptsvos.maxRONo = Me.maxroserial.Text.ToUpper
        Else
            rptsvos.maxRONo = "ZZZZZZZZZZZZZZZZZZZZ"
        End If
        If (Me.minstatebox.Text <> "AAA") Then
            rptsvos.minState = Me.minstatebox.Text.ToUpper
        Else
            rptsvos.minState = ""
        End If
        If (Me.maxstatebox.Text <> "ZZZ") Then
            rptsvos.maxState = Me.maxstatebox.Text.ToUpper
        Else
            rptsvos.maxState = "ZZZZZZZZZZZZZZZZZZZZ"
        End If
        If (Me.mintchnicanbox.Text <> "AAA") Then
            rptsvos.minTchID = Me.mintchnicanbox.Text.ToUpper
        Else
            rptsvos.minTchID = ""
        End If
        If (Me.maxtchnicanbox.Text <> "ZZZ") Then
            rptsvos.maxTchID = Me.maxtchnicanbox.Text.ToUpper
        Else
            rptsvos.maxTchID = "ZZZZZZZZZZZZZZZZZZZZ"
        End If

        If (Me.minareabox.Text <> "AAA") Then
            rptsvos.minArea = Me.minareabox.Text.ToUpper
        Else
            rptsvos.minArea = ""
        End If
        If (Me.maxareabox.Text <> "ZZZ") Then
            rptsvos.maxArea = Me.maxareabox.Text.ToUpper
        Else
            rptsvos.maxArea = "ZZZZZZZZZZZZZZZZZZZZ"
        End If

        rptsvos.Status = cboStatus.SelectedValue
        rptsvos.StatusText = cboStatus.SelectedItem.Text

        rptsvos.ServiceType = cboServiceType.SelectedValue
        rptsvos.ServiceTypeText = cboServiceType.SelectedItem.Text

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''
        If (Me.mindatebox.Text <> "") Then
            Dim datearr As Array = mindatebox.Text.ToString().Split("/")
            Dim rtdate As String = datearr(2) + "-" + datearr(1) + "-" + datearr(0)

            rptsvos.mindate = rtdate
        Else
            rptsvos.mindate = fstrDefaultStartDate
        End If
        If (Me.maxdatebox.Text <> "") Then
            Dim datearr As Array = maxdatebox.Text.ToString().Split("/")
            Dim rtdate As String = datearr(2) + "-" + datearr(1) + "-" + datearr(0)
            rptsvos.maxdate = rtdate
        Else
            rptsvos.maxdate = fstrDefaultEndDate
        End If
        '////////////////////////////////////////////////

        Dim ctryIDPara As String = "%"
        Dim cmpIDPara As String = "%"
        Dim svcIDPara As String = "%"
        Dim login_rank As Integer = Session("login_rank")
        If login_rank <> 0 Then
            Select Case (login_rank)
                Case 9
                    ctryIDPara = Session("login_ctryID").ToString.ToUpper
                    cmpIDPara = Session("login_cmpID").ToString.ToUpper
                    svcIDPara = Session("login_svcID").ToString.ToUpper
                Case 8
                    ctryIDPara = Session("login_ctryID").ToString.ToUpper
                    cmpIDPara = Session("login_cmpID").ToString.ToUpper
                Case 7
                    ctryIDPara = Session("login_ctryID").ToString.ToUpper
            End Select
        Else


        End If
        ''''''''''''''''''''''''''''''''''''''''''''''''''
        rptsvos.Rank = login_rank.ToString
        rptsvos.CountryID = ctryIDPara
        rptsvos.CompID = cmpIDPara
        rptsvos.SVCID = svcIDPara
        '////////////////////////////////////////////////


        If Trim(Session("userID")) = "" Then
            rptsvos.UserName = "  "
        Else
            rptsvos.UserName = Session("userID")
        End If

        Session("rptsvos_searchcondition") = rptsvos

    End Function
#End Region
   
 
End Class
