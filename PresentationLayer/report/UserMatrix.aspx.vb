Imports System.Data
Imports System.Data.SqlClient
Imports System.Globalization
Imports SQLDataAccess



Partial Class PresentationLayer_report_UserMatrix
    Inherits System.Web.UI.Page
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)
        Return connection
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim _con As SqlConnection
        _con = GetConnection(ConfigurationManager.AppSettings("ConnectionString"))

        If GroupFromDDL.SelectedValue = "" Then
            Dim Comm2 As SqlCommand
            Comm2 = _con.CreateCommand
            Comm2.CommandText = "Select distinct MUA1_GRPID from dbo.MUA1_FIL where MUA1_STAT = 'ACTIVE' and MUA1_GRPID not like '%ADMIN%' and MUA1_GRPID not like '%GROUP%' order by MUA1_GRPID"
            _con.Open()

            Dim dr1 As SqlDataReader = Comm2.ExecuteReader(CommandBehavior.CloseConnection)

            Dim dt1 As DataTable = New DataTable()
            dt1.Load(dr1)

            Dim counter2 As Integer

            For counter2 = 0 To dt1.Rows.Count - 1
                GroupFromDDL.Items.Add((dt1.Rows(counter2).Item("MUA1_GRPID").ToString))
                GroupToDDL.Items.Add((dt1.Rows(counter2).Item("MUA1_GRPID").ToString))
            Next

            _con.Close()

            GroupToDDL.SelectedIndex = GroupToDDL.Items.Count - 1
        End If

        If DeptFromDDL.SelectedValue = "" Then
            Dim Comm2 As SqlCommand
            Comm2 = _con.CreateCommand
            Comm2.CommandText = "Select distinct MUSR_DEPT from dbo.MUSR_FIL where MUSR_STAT = 'ACTIVE' and MUSR_DEPT <> '' order by MUSR_DEPT"
            _con.Open()

            Dim dr1 As SqlDataReader = Comm2.ExecuteReader(CommandBehavior.CloseConnection)

            Dim dt1 As DataTable = New DataTable()
            dt1.Load(dr1)

            Dim counter2 As Integer

            For counter2 = 0 To dt1.Rows.Count - 1
                DeptFromDDL.Items.Add((dt1.Rows(counter2).Item("MUSR_DEPT").ToString))
                DeptToDDL.Items.Add((dt1.Rows(counter2).Item("MUSR_DEPT").ToString))
            Next

            _con.Close()

            DeptToDDL.SelectedIndex = DeptToDDL.Items.Count - 1
        End If

        'If RankFromDDL.SelectedValue = "" Then
        '    Dim Comm2 As SqlCommand
        '    Comm2 = _con.CreateCommand
        '    Comm2.CommandText = "Select distinct [MUSR_RANK] = CASE WHEN MUSR_RANK = 7 THEN 'Country' WHEN MUSR_RANK = 8 THEN 'Company' ELSE 'Service Center' END from dbo.MUSR_FIL where MUSR_STAT = 'ACTIVE' order by MUSR_RANK"
        '    _con.Open()

        '    Dim dr1 As SqlDataReader = Comm2.ExecuteReader(CommandBehavior.CloseConnection)

        '    Dim dt1 As DataTable = New DataTable()
        '    dt1.Load(dr1)

        '    Dim counter2 As Integer

        '    For counter2 = 0 To dt1.Rows.Count - 1
        '        RankFromDDL.Items.Add((dt1.Rows(counter2).Item("MUSR_RANK").ToString))
        '        RankToDDL.Items.Add((dt1.Rows(counter2).Item("MUSR_RANK").ToString))
        '    Next

        '    _con.Close()

        'RankToDDL.SelectedIndex = RankToDDL.Items.Count - 1
        'End If

        If SVCFromDDL.SelectedValue = "" Then
            Dim Comm2 As SqlCommand
            Comm2 = _con.CreateCommand
            Comm2.CommandText = "Select MSVC_SVCID from MSVC_FIL where MSVC_STAT = 'ACTIVE' and MSVC_SVCID not in ('901','951','ZZZ') order by MSVC_SVCID"
            _con.Open()

            Dim dr1 As SqlDataReader = Comm2.ExecuteReader(CommandBehavior.CloseConnection)

            Dim dt1 As DataTable = New DataTable()
            dt1.Load(dr1)

            Dim counter2 As Integer

            For counter2 = 0 To dt1.Rows.Count - 1
                SVCFromDDL.Items.Add((dt1.Rows(counter2).Item("MSVC_SVCID").ToString))
                SVCToDDL.Items.Add((dt1.Rows(counter2).Item("MSVC_SVCID").ToString))
            Next

            _con.Close()

            SVCToDDL.SelectedIndex = SVCToDDL.Items.Count - 1
        End If






    End Sub

    Protected Sub RetrieveBtn1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles RetrieveBtn1.Click
        Session("UserIDFrom") = UserIDFromTxt.Text
        Session("UserIDTo") = UserIDToTxt.Text
        Session("GroupFrom") = GroupFromDDL.SelectedValue.ToString
        Session("GroupTo") = GroupToDDL.SelectedValue.ToString
        Session("DeptFrom") = DeptFromDDL.SelectedValue.ToString
        Session("DeptTo") = DeptToDDL.SelectedValue.ToString
        'Session("RankFrom") = RankFromDDL.SelectedValue.ToString
        'Session("RankTo") = RankToDDL.SelectedValue.ToString
        Session("SVCFrom") = SVCFromDDL.SelectedValue.ToString
        Session("SVCTo") = SVCToDDL.SelectedValue.ToString

        Const script As String = "window.open('../report/UserMatrix_View.aspx')"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "UserMatrix_View", script, True)

    End Sub

    Protected Sub RetrieveBtn2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles RetrieveBtn2.Click
        Const script As String = "window.open('../report/UserMatrix_View2.aspx')"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "UserMatrix_View2", script, True)
    End Sub
End Class
