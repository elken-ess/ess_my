<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AppointmentSchedulingReport.aspx.vb" Inherits="PresentationLayer_report_AppointmentSchedulingReport" %>

<%@ Register Assembly="JCalendar" Namespace="JCalendar" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>User Matrix</title>
    <link href="../css/style.css" type="text/css" rel="stylesheet">
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="JavaScript" src="../js/common.js"></script>
    <style type="text/css">
    	.rowStyle {
    		background-color: #ffffff;
    	}
    	.labelStyle {
    		text-align: left;
    		width: 26%
    	}
    	.fromToStyle {
    		text-align: left;
    		width: 7%;
    	}
    	.dataStyle {
    		text-align: left;
    		width: 30%
    	}
    	.countryStyle {
    		border: 0px;
    		width: 100%
    	}
    	.tableStyle {
    		border: 0px;
    		padding: 0px;
    		margin: 0px;
    		width: 100%;
    	}
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table id="countrytab" class="countryStyle">
            <tr>
                <td style="width: 100%">
                    <table class="tableStyle">
                        <tr>
                            <td background="../graph/title_bg.gif" style="width: 1%">
                                <img height="24" src="../graph/title1.gif" width="5" />
                            </td>
                            <td background="../graph/title_bg.gif" class="style2" width="98%" style="width: 100%">
                                <asp:Label ID="titleLab" runat="server" Text="Appointment Scheduling Report"></asp:Label>
                            </td>
                            <td align="LEFT" background="../graph/title_bg.gif" width="1%">
                                <img height="1" src="../graph/title_2.gif" width="5" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100%; height: 1px;">
                    <table id="country" bgcolor="#b7e6e6" border="0" cellpadding="2" cellspacing="1"
                        style="width: 100%">
                        <tr class="rowStyle">
                            <td class="labelStyle">
                                &nbsp;Appointment Date</td>
                            <td class="fromToStyle">
                                &nbsp;From</td>
                            <td class="dataStyle">
                                <asp:TextBox ID="FromDateTxt" 
                                             runat="server" 
                                             CssClass="textborder" 
                                             ReadOnly="false"
                                             MaxLength="10">DD/MM/YYYY</asp:TextBox>
                                <asp:HyperLink ID="HypCalFrom" 
                                               runat="server" 
                                               ImageUrl="~/PresentationLayer/graph/calendar.gif"
                                               ToolTip="Choose a Date" Visible="False">Choose a Date</asp:HyperLink>
                                <cc1:JCalendar ID="JCalendar1" 
                                               runat="server" 
                                               ControlToAssign="FromDateTxt" 
                                               ImgURL="~/PresentationLayer/graph/calendar.gif" /></td>
                            <td class="fromToStyle">
                                &nbsp;To</td>
                            <td class="dataStyle">
                                <asp:TextBox ID="ToDateTxt" 
                                             runat="server" 
                                             CssClass="textborder" 
                                             ReadOnly="false"
                                             MaxLength="10">DD/MM/YYYY</asp:TextBox>
                                <asp:HyperLink ID="HypCalTo" 
                                               runat="server" 
                                               ImageUrl="~/PresentationLayer/graph/calendar.gif"
                                               ToolTip="Choose a Date" Visible="False">Choose a Date</asp:HyperLink>
                                <cc1:JCalendar ID="JCalendar2" 
                                               runat="server" 
                                               ControlToAssign="ToDateTxt" 
                                               ImgURL="~/PresentationLayer/graph/calendar.gif" /></td>
                        </tr>
                        <tr class="rowStyle">
                            <td class="labelStyle">
                                &nbsp;Service Center</td>
                            <td class="fromToStyle">
                                &nbsp;From</td>
                            <td class="dataStyle">
                                <asp:DropDownList ID="SVCFromDDL" runat="server" AutoPostBack="True" Width="325px">
                                </asp:DropDownList></td>
                            <td class="fromToStyle">
                                &nbsp;To</td>
                            <td class="dataStyle">
                                <asp:DropDownList ID="SVCToDDL" runat="server" AutoPostBack="True" Width="325px">
                                </asp:DropDownList></td>
                        </tr>
                        <tr class="rowStyle">
                            <td class="labelStyle" style="height: 26px">
                                &nbsp;Service Type</td>
                            <td class="fromToStyle" style="height: 26px">
                                &nbsp;From</td>
                            <td class="dataStyle" style="height: 26px">
                                <asp:DropDownList ID="TypeFromDDL" runat="server" Width="325px">
                                </asp:DropDownList></td>
                            <td class="fromToStyle" style="height: 26px">
                                &nbsp;To</td>
                            <td class="dataStyle" style="height: 26px">
                                <asp:DropDownList ID="TypeToDDL" runat="server" Width="325px">
                                </asp:DropDownList></td>
                        </tr>
                        <tr class="rowStyle">
                            <td class="labelStyle">
                                &nbsp;Appointment Status</td>
                            <td class="fromToStyle">
                                &nbsp;From</td>
                            <td class="dataStyle">
                                <asp:DropDownList ID="StatusFromDDL" runat="server" Width="325px">
                                </asp:DropDownList></td>
                            <td class="fromToStyle">
                                &nbsp;To</td>
                            <td class="dataStyle">
                                <asp:DropDownList ID="StatusToDDL" runat="server" Width="325px">
                                </asp:DropDownList></td>
                        </tr>
                        <tr class="rowStyle">
                            <td class="labelStyle">
                                &nbsp;CR ID</td>
                            <td class="fromToStyle">
                                &nbsp;From</td>
                            <td class="dataStyle">
                                <asp:DropDownList ID="CRFromDDL" runat="server" Width="325px">
                                </asp:DropDownList></td>
                            <td class="fromToStyle">
                                &nbsp;To</td>
                            <td class="dataStyle">
                                <asp:DropDownList ID="CRToDDL" runat="server" Width="325px">
                                </asp:DropDownList></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100%; height: 16px;">
                    &nbsp;<asp:LinkButton ID="RetrieveBtn1" runat="server">Retrieve</asp:LinkButton><br />
                    <br />
                    Service Type:<br />
                    MF=Free Service/MM=Maintenance Service/MD=Reinstall+Service/MS=Sundry Job/MC=Ad-Hoc
                    Contract Service/A1=AC3 1st Service/A2=AC3 2nd Service/A3=AC3 3rd Service/MR=Warranty
                    Repair/M1-M6=Contract Service/ML=Normal Installation/ML2=B2-200Installation/CP=Collect
                    Payment/MB=Warranty Job/PB=POE Breakdown/<br />
                    PL=POE Installation/PM=POE Maintenance Service/PR=POE Repair/PS=Preventive Scheme/WM=Free
                    Installation Service<br />
                    <br />
                    Appointment Status:<br />
                    BL=Billed/SA=Confirmed/SM=Assigned/TG=Reschedule/UA=Cancel/UM=Completed/X=X Appointment<br />
                    <br />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
