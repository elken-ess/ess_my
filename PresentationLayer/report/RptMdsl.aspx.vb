Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Web
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web
Imports BusinessEntity
Imports System.Data

Partial Class PresentationLayer_Report_RptMdsl_aspx
    Inherits System.Web.UI.Page

    Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
    Dim fstrMonth As String = ""
    Dim fstrDefaultStartDate As String = ""
    Dim fstrDefaultEndDate As String = ""
    Dim clsCommon As New clsCommonClass
    Dim objXmlTr As New clsXml

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'LW Added part: To add for ajax
        AjaxPro.Utility.RegisterTypeForAjax(GetType(PresentationLayer_Report_RptMdsl_aspx))

        Response.Buffer = True
        Response.Expires = 10000

        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        fstrMonth = Month(System.DateTime.Today)
        fstrDefaultStartDate = System.DateTime.Today
        fstrDefaultEndDate = DateAdd(DateInterval.Month, 1, CDate(fstrDefaultStartDate))

        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try

        If Not Page.IsPostBack Then

            ''display label message
            Session("MDSLflag") = "0"
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            Customerlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTMDSL-0001")
            Modellab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTMDSL-0002")
            Seriallab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTMDSL-0003")
            Statelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTMDSL-0004")
            Datelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTMDSL-0005")
            Sercenterlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTMDSL-0006")


            Fromlab1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Fromlab2.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Fromlab3.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Fromlab4.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Fromlab5.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Fromlab6.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Tolab1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Tolab2.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Tolab3.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Tolab4.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Tolab5.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Tolab6.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")

            reportviewer.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-VIEWREPORT")
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTMDSL-titlelab")
            '''access control'''''''''''''''''''''''''''''''''''''''''''''''
            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "36")
            Me.reportviewer.Enabled = purviewArray(3)

            Me.mindate.Text = fstrDefaultStartDate
            Me.maxdate.Text = fstrDefaultEndDate

            'Label
            RacesLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0011")
            CustomerTypeLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0012")
            ProdClassLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-0006")

            'List Customer type in drop down 
            Dim custtype As New clsrlconfirminf()
            Dim custParam As ArrayList = New ArrayList
            custParam = custtype.searchconfirminf("CUSTTYPE")
            Dim custcount As Integer
            Dim custid As String
            Dim custnm As String
            Me.CustomerTypeDLL.Items.Add(New ListItem("ALL", "ALL"))
            For custcount = 0 To custParam.Count - 1
                custid = custParam.Item(custcount)
                custnm = custParam.Item(custcount + 1)
                custcount = custcount + 1
                Me.CustomerTypeDLL.Items.Add(New ListItem(custnm.ToString(), custid.ToString()))

            Next

            'List Race in drop down 
            Dim race As New clsrlconfirminf()
            Dim raceParam As ArrayList = New ArrayList
            raceParam = race.searchconfirminf("PERSONRACE")
            Dim racecount As Integer
            Dim raceid As String
            Dim racenm As String
            Me.RacesDLL.Items.Add(New ListItem("ALL", "ALL"))
            For racecount = 0 To raceParam.Count - 1
                raceid = raceParam.Item(racecount)
                racenm = raceParam.Item(racecount + 1)
                racecount = racecount + 1
                'If Not raceid.Equals("ZNONAPPL") Then
                Me.RacesDLL.Items.Add(New ListItem(racenm.ToString(), raceid.ToString()))
                'End If
            Next

            Dim strUserID As String = Session("userID").ToString
            clsCommon.userid = strUserID
            Dim ds As DataSet = clsCommon.SVCnStatebyUserID()

            minstate.Items.Add(New ListItem(objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), "%"))
            maxstate.Items.Add(New ListItem(objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), "%"))
            Dim stateid As String
            Dim statenm As String
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                statenm = ds.Tables(0).Rows(i).Item(0) + "-" + ds.Tables(0).Rows(i).Item(1)
                stateid = ds.Tables(0).Rows(i).Item(0)
                minstate.Items.Add(New ListItem(statenm.ToString(), stateid.ToString()))
                maxstate.Items.Add(New ListItem(statenm.ToString(), stateid.ToString()))
                'stateFDropDownList.Items.Add(dsCLA.Tables(0).Rows(i).Item(0))
                'stateTDropDownList.Items.Add(dsCLA.Tables(0).Rows(i).Item(0))
            Next
            If minstate.Items.Count > 0 Then
                minstate.SelectedIndex = 0
                maxstate.SelectedIndex = 0
            End If

            minsercenter.Items.Add(New ListItem(objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), "%"))
            maxsercenter.Items.Add(New ListItem(objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), "%"))
            Dim scid As String
            Dim scnm As String
            For i As Integer = 0 To ds.Tables(1).Rows.Count - 1
                scnm = ds.Tables(1).Rows(i).Item(0) + "-" + ds.Tables(1).Rows(i).Item(1)
                scid = ds.Tables(1).Rows(i).Item(0)
                minsercenter.Items.Add(New ListItem(scnm.ToString(), scid.ToString()))
                maxsercenter.Items.Add(New ListItem(scnm.ToString(), scid.ToString()))
            Next
            If minsercenter.Items.Count > 0 Then
                minsercenter.SelectedIndex = 0
                maxsercenter.SelectedIndex = 0
            End If

            minarea.Items.Add(New ListItem(objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), "%"))
            maxarea.Items.Add(New ListItem(objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), "%"))
            Dim areaid As String
            Dim areanm As String
            For i As Integer = 0 To ds.Tables(2).Rows.Count - 1
                areanm = ds.Tables(2).Rows(i).Item(0) + "-" + ds.Tables(2).Rows(i).Item(1)
                areaid = ds.Tables(2).Rows(i).Item(0)
                minarea.Items.Add(New ListItem(areanm.ToString(), areaid.ToString()))
                maxarea.Items.Add(New ListItem(areanm.ToString(), areaid.ToString()))
            Next

            If minarea.Items.Count > 0 Then
                minarea.SelectedIndex = 0
                maxarea.SelectedIndex = 0
            End If

            Dim modelds As New DataSet
            clsCommon.spctr = Session("login_ctryID")
            clsCommon.spstat = ""
            clsCommon.sparea = ""
            clsCommon.rank = ""
            modelds = clsCommon.Getcomidname("BB_MASMOTY_IDNAME")
            If modelds.Tables.Count <> 0 Then
                Dim row As DataRow
                minmodel.Items.Add(New ListItem(objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), ""))
                maxmodel.Items.Add(New ListItem(objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), ""))
                For Each row In modelds.Tables(0).Rows
                    Dim NewItem As New ListItem()
                    NewItem.Text = row("id") & "-" & row("name")
                    NewItem.Value = row("id")
                    minmodel.Items.Add(NewItem)
                    maxmodel.Items.Add(NewItem)
                Next
            End If
            Dim cntryStat As New clsrlconfirminf()
            Dim count As Integer
            Dim statid As String
            Dim statnm As String
            Dim productcl As ArrayList = New ArrayList
            productcl = cntryStat.searchconfirminf("PRODUCTCL")
            count = 0
            ProdClassDLL.Items.Add(New ListItem("", "%"))
            For count = 0 To productcl.Count - 1
                statid = productcl.Item(count)
                statnm = productcl.Item(count + 1)
                count = count + 1
                ProdClassDLL.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
            Next
            ProdClassDLL.Items(0).Selected = True

            'ElseIf Session("MDSLflag") = "1" Then
            '    'Dim Viewint As Integer = 0
            '    viewreport()
        End If

        HypCalFrom.NavigateUrl = "javascript:DoCal(document.form1.mindate);"
        HypCalTo.NavigateUrl = "javascript:DoCal(document.form1.maxdate);"
    End Sub

    Protected Sub BindGrid()

        If Not Page.IsPostBack Then
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")

            'Label
            RacesLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0011")
            CustomerTypeLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0012")
            ProdClassLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-0006")

            'List Customer type in drop down 
            Dim custtype As New clsrlconfirminf()
            Dim custParam As ArrayList = New ArrayList
            custParam = custtype.searchconfirminf("CUSTTYPE")
            Dim custcount As Integer
            Dim custid As String
            Dim custnm As String
            Me.CustomerTypeDLL.Items.Add(New ListItem("ALL", "ALL"))
            For custcount = 0 To custParam.Count - 1
                custid = custParam.Item(custcount)
                custnm = custParam.Item(custcount + 1)
                custcount = custcount + 1
                Me.CustomerTypeDLL.Items.Add(New ListItem(custnm.ToString(), custid.ToString()))

            Next

            'List Race in drop down 
            Dim race As New clsrlconfirminf()
            Dim raceParam As ArrayList = New ArrayList
            raceParam = race.searchconfirminf("PERSONRACE")
            Dim racecount As Integer
            Dim raceid As String
            Dim racenm As String
            Me.RacesDLL.Items.Add(New ListItem("ALL", "ALL"))
            For racecount = 0 To raceParam.Count - 1
                raceid = raceParam.Item(racecount)
                racenm = raceParam.Item(racecount + 1)
                racecount = racecount + 1
                'If Not raceid.Equals("ZNONAPPL") Then
                Me.RacesDLL.Items.Add(New ListItem(racenm.ToString(), raceid.ToString()))
                'End If
            Next
        End If
    End Sub

    Protected Sub reportviewer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles reportviewer.Click
        'Dim Viewint As Integer = 1
        Session("MDSLflag") = "1"

        Try

        
            viewreport()
            'Response.Redirect("~/PresentationLayer/REPORT/RptMdslsearch.aspx")


            Dim script As String = "window.open('../report/RptMdslsearch.aspx')"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "RptMdslsearch", script, True)
            Return
        Catch err As Exception
            'System.IO.File.Delete(lstrPhysicalFile)
            Response.Write("<BR>")
            Response.Write("Error Message: " & err.Message.ToString)
            Response.Write("<BR>")
            Response.Write("Source : " & err.Source)
            Response.Write("<BR>")
            Response.Write("Stack Trace Message: " & err.StackTrace)

            Response.Write("<BR>")
            Response.Write("Debug Mode Message: " & "Page 1 Dataset Error")
            Response.End()
        End Try
    End Sub

    Protected Sub minsrevice_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles minsercenter.SelectedIndexChanged
        If Not minsercenter.SelectedValue = "%" Then
            clsCommon.Minsvcid = minsercenter.SelectedValue
            clsCommon.ctryid = Session("login_ctryID").ToString.ToUpper()
            Dim ds As DataSet = clsCommon.getStateAndArea()

            minstate.Items.Clear()
            minarea.Items.Clear()
            Dim dt As DataTable
            Dim dt1 As DataTable
            'Dim dr As DataRow
            dt = ds.Tables(0)
            dt1 = ds.Tables(1)
            minstate.Items.Add(New ListItem(objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), "%"))
            For i As Integer = 0 To dt.Rows.Count - 1
                Dim statenm As String = dt.Rows(i).Item(0) + "-" + dt.Rows(i).Item(1)
                minstate.Items.Add(New ListItem(statenm, dt.Rows(i).Item(0)))
            Next
            minarea.Items.Add(New ListItem(objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), "%"))
            For i As Integer = 0 To dt1.Rows.Count - 1
                Dim areanm As String = dt1.Rows(i).Item(0) + "-" + dt1.Rows(i).Item(1)
                minarea.Items.Add(New ListItem(areanm, dt1.Rows(i).Item(0)))
            Next

            'stateid = dsCLA.Tables(0).Rows(i).Item(0)

        Else
            Dim strUserID As String = Session("userID").ToString
            clsCommon.userid = strUserID
            Dim ds As DataSet = clsCommon.SVCnStatebyUserID()
            minstate.Items.Clear()
            minarea.Items.Clear()
            minstate.Items.Add(New ListItem(objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), "%"))
            'stateTDropDownList.Items.Add(New ListItem("", ""))
            Dim stateid As String
            Dim statenm As String
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                statenm = ds.Tables(0).Rows(i).Item(0) + "-" + ds.Tables(0).Rows(i).Item(1)
                stateid = ds.Tables(0).Rows(i).Item(0)
                minstate.Items.Add(New ListItem(statenm.ToString(), stateid.ToString()))
                'stateTDropDownList.Items.Add(New ListItem(statenm.ToString(), stateid.ToString()))
            Next
            minarea.Items.Add(New ListItem(objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), "%"))
            'cboAreaIdTo.Items.Add(New ListItem(objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL")))
            Dim areaid As String
            Dim areanm As String
            For ii As Integer = 0 To ds.Tables(2).Rows.Count - 1
                areanm = ds.Tables(2).Rows(ii).Item(0) + "-" + ds.Tables(2).Rows(ii).Item(1)
                areaid = ds.Tables(2).Rows(ii).Item(0)
                minarea.Items.Add(New ListItem(areanm.ToString(), areaid.ToString()))
                'cboAreaIdTo.Items.Add(New ListItem(areanm.ToString(), areaid.ToString()))
            Next
        End If


    End Sub

    Protected Sub maxsrevice_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles maxsercenter.SelectedIndexChanged
        If Not maxsercenter.SelectedValue = "%" Then
            clsCommon.Minsvcid = maxsercenter.SelectedValue
            clsCommon.ctryid = Session("login_ctryID").ToString.ToUpper()
            Dim ds As DataSet = clsCommon.getStateAndArea()
            maxstate.Items.Clear()
            maxarea.Items.Clear()
            Dim dt As DataTable
            Dim dt1 As DataTable
            'Dim dr As DataRow
            dt = ds.Tables(0)
            dt1 = ds.Tables(1)
            maxstate.Items.Add(New ListItem(objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), "%"))
            For i As Integer = 0 To dt.Rows.Count - 1
                Dim statenm As String = dt.Rows(i).Item(0) + "-" + dt.Rows(i).Item(1)
                maxstate.Items.Add(New ListItem(statenm, dt.Rows(i).Item(0)))
            Next
            maxarea.Items.Add(New ListItem(objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), "%"))
            For i As Integer = 0 To dt1.Rows.Count - 1
                Dim areanm As String = dt1.Rows(i).Item(0) + "-" + dt1.Rows(i).Item(1)
                maxarea.Items.Add(New ListItem(areanm, dt1.Rows(i).Item(0)))
            Next

        Else
            Dim strUserID As String = Session("userID").ToString
            clsCommon.userid = strUserID
            Dim ds As DataSet = clsCommon.SVCnStatebyUserID()
            maxstate.Items.Clear()
            maxarea.Items.Clear()
            'stateFDropDownList.Items.Add(New ListItem("", ""))
            maxstate.Items.Add(New ListItem(objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), "%"))
            Dim stateid As String
            Dim statenm As String
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                statenm = ds.Tables(0).Rows(i).Item(0) + "-" + ds.Tables(0).Rows(i).Item(1)
                stateid = ds.Tables(0).Rows(i).Item(0)
                'stateFDropDownList.Items.Add(New ListItem(statenm.ToString(), stateid.ToString()))
                maxstate.Items.Add(New ListItem(statenm.ToString(), stateid.ToString()))
            Next
            'areaFDropDownList.Items.Add(New ListItem(objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL")))
            maxarea.Items.Add(New ListItem(objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), "%"))
            Dim areaid As String
            Dim areanm As String
            For ii As Integer = 0 To ds.Tables(2).Rows.Count - 1
                areanm = ds.Tables(2).Rows(ii).Item(0) + "-" + ds.Tables(2).Rows(ii).Item(1)
                areaid = ds.Tables(2).Rows(ii).Item(0)
                'areaFDropDownList.Items.Add(New ListItem(areanm.ToString(), areaid.ToString()))
                maxarea.Items.Add(New ListItem(areanm.ToString(), areaid.ToString()))
            Next
        End If

    End Sub

    Protected Sub minstatt_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles minstate.SelectedIndexChanged
        minarea.Items.Clear()
        If Not minstate.SelectedValue = "%" Then
            clsCommon.Minstat = minstate.SelectedValue
            clsCommon.Minsvcid = minsercenter.SelectedValue
        Else
            clsCommon.Minstat = "%"
            clsCommon.Minsvcid = minsercenter.SelectedValue
            minarea.Items.Add(New ListItem(objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), "%"))
        End If

        Dim ds As DataSet = clsCommon.getAreafromState()
        Dim dt As DataTable = ds.Tables(0)
        'Dim dr As DataRow
        Dim areanm As String
        minarea.Items.Add(New ListItem(objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), "%"))
        For i As Integer = 0 To dt.Rows.Count - 1
            areanm = dt.Rows(i).Item(0) + "-" + dt.Rows(i).Item(1)
            minarea.Items.Add(New ListItem(areanm, dt.Rows(i).Item(0)))
        Next

        'Dim areanm As String = ds.Tables(0).Rows(0).Item(0) + "-" + ds.Tables(0).Rows(0).Item(1)
        'areaFDropDownList.Items.Add(New ListItem(areanm, ds.Tables(0).Rows(0).Item(1)))
    End Sub

    Protected Sub maxstat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles maxstate.SelectedIndexChanged
        maxarea.Items.Clear()
        If Not maxstate.SelectedValue = "%" Then
            clsCommon.Minstat = maxstate.SelectedValue
            clsCommon.Minsvcid = maxsercenter.SelectedValue
        Else
            clsCommon.Minstat = "%"
            clsCommon.Minsvcid = maxsercenter.SelectedValue
            maxarea.Items.Add(New ListItem(objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), "%"))
        End If
        Dim ds As DataSet = clsCommon.getAreafromState()

        Dim dt As DataTable = ds.Tables(0)
        'Dim dr As DataRow
        Dim areanm As String
        maxarea.Items.Add(New ListItem(objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), "%"))
        For i As Integer = 0 To dt.Rows.Count - 1
            areanm = dt.Rows(i).Item(0) + "-" + dt.Rows(i).Item(1)
            maxarea.Items.Add(New ListItem(areanm, dt.Rows(i).Item(0)))
        Next
    End Sub


#Region " view report message"
    Public Function viewreport()
        Dim lstrDebugMsg As String = ""

        Try

            lstrDebugMsg = "Set Parameter to Entity"
            'set params'
            Dim mdslEntity As New ClsRptMdsl
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle



            mindate.Text = Request.Form("mindate")
            maxdate.Text = Request.Form("maxdate")

            If Not IsDate(mindate.Text) Then
                mindate.Text = fstrDefaultStartDate
            End If

            If Not IsDate(maxdate.Text) Then
                maxdate.Text = fstrDefaultEndDate
            End If

            If Trim(mincustomer.Text).ToUpper = "AAA" Then
                mdslEntity.custmorid = ""
            Else
                mdslEntity.custmorid = Trim(mincustomer.Text)
            End If
            'If Trim(minmodel.Text).ToUpper = "AAA" Then
            '    mdslEntity.modelid = ""
            'Else
            '    mdslEntity.modelid = Trim(minmodel.Text)
            'End If
            mdslEntity.modelid = minmodel.SelectedValue
            If Trim(minuserial.Text).ToUpper = "AAA" Then
                mdslEntity.serialid = ""
            Else
                mdslEntity.serialid = Trim(minuserial.Text)
            End If

            mdslEntity.state = minstate.SelectedValue


            If Trim(mindate.Text) <> "" Then
                Dim temparr As Array = mindate.Text.Split("/")
                mdslEntity.datebegin = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
            Else
                Dim mind As String = "01/01/1910"
                Dim temparr As Array = mind.Split("/")
                mdslEntity.datebegin = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
            End If
            If Trim(maxdate.Text) <> "" Then
                Dim temparr2 As Array = maxdate.Text.Split("/")
                mdslEntity.dateend = temparr2(2) + "-" + temparr2(1) + "-" + temparr2(0)
            Else
                Dim maxd As String = "31/12/2089"
                Dim temparr2 As Array = maxd.Split("/")
                mdslEntity.dateend = temparr2(2) + "-" + temparr2(1) + "-" + temparr2(0)
            End If


            mdslEntity.sercid = minsercenter.SelectedValue


            If Trim(maxcustomer.Text).ToUpper = "ZZZ" Then
                mdslEntity.custmoridend = "ZZZZZZZZZZZ"
            Else
                mdslEntity.custmoridend = Trim(maxcustomer.Text).ToUpper.ToString()
            End If
            'If Trim(maxmodel.Text).ToUpper = "ZZZ" Then
            '    mdslEntity.modelidend = "ZZZZZZZZZZZ"
            'Else
            '    mdslEntity.modelidend = Trim(maxmodel.Text).ToUpper.ToString()
            'End If
            mdslEntity.modelidend = maxmodel.SelectedValue
            If Trim(maxuserial.Text).ToUpper = "ZZZ" Then
                mdslEntity.serialidend = "ZZZZZZZZZZZ"
            Else
                mdslEntity.serialidend = Trim(maxuserial.Text).ToUpper.ToString()
            End If

            mdslEntity.stateend = maxstate.SelectedValue
            mdslEntity.sercidend = maxsercenter.SelectedValue


            'LW Added
            mdslEntity.customerType = CustomerTypeDLL.SelectedValue
            mdslEntity.customerRace = RacesDLL.SelectedValue


            mdslEntity.maxArea = maxarea.SelectedValue
            mdslEntity.minArea = minarea.SelectedValue
            mdslEntity.ClassID = ProdClassDLL.SelectedValue


            'Amened by KSLim on 08/Jul/2006
            'Dim days As Integer = -270
            Dim days As Integer = 0
            'Amened by KSLim on 08/Jul/2006
            mdslEntity.instdatemin270 = DateAdd(DateInterval.Day, days, Convert.ToDateTime(mdslEntity.datebegin))
            mdslEntity.instdatemax270 = DateAdd(DateInterval.Day, days, Convert.ToDateTime(mdslEntity.dateend))
            If Trim(Session("userID")) = "" Then
                mdslEntity.uname = "ADMIN"
            Else
                mdslEntity.uname = Session("userID")
            End If

            mdslEntity.logrank = Session("login_rank")
            Dim login_rank As Integer = Session("login_rank")
            If login_rank <> 0 Then
                Select Case (login_rank)
                    Case 9
                        mdslEntity.logctrid = Session("login_ctryID").ToString.ToUpper
                        mdslEntity.logcmpid = Session("login_cmpID").ToString.ToUpper
                        mdslEntity.logsvccid = Session("login_svcID").ToString.ToUpper
                    Case 8
                        mdslEntity.logctrid = Session("login_ctryID").ToString.ToUpper
                        mdslEntity.logcmpid = Session("login_cmpID").ToString.ToUpper
                    Case 7
                        mdslEntity.logctrid = Session("login_ctryID").ToString.ToUpper
                End Select
            End If



            '-------------------------------------------------------------------------------------------------------------


            lstrDebugMsg = "Process Data"

            'Dim ds As DataSet = mdslEntity.Getmodeldue()

            'If ds IsNot Nothing Then

            '    Session("rptmdsl_ds") = ds
            Session.Remove("rptmdsl_search_condition")
            Session("rptmdsl_search_condition") = mdslEntity
            'Else

            '    Response.Write("Debug Mode Message: " & "Dataset is empty")
            '    Response.End()
            'End If

            '-------------------------------------------------------------------------------------------------------------

        Catch err As Exception
            'System.IO.File.Delete(lstrPhysicalFile)
            Response.Write("<BR>")
            Response.Write("Error Message: " & err.Message.ToString)
            Response.Write("<BR>")
            Response.Write("Source : " & err.Source)
            Response.Write("<BR>")
            Response.Write("Stack Trace Message: " & err.StackTrace)

            Response.Write("<BR>")
            Response.Write("Debug Mode Message: " & lstrDebugMsg)
            Response.End()
        End Try
    End Function
#End Region
#Region "Ajax"
    'LW Added part : Ajax 
    <AjaxPro.AjaxMethod()> _
        Public Function GetRaces(ByVal strCustomerType As String) As ArrayList
        Dim race As New clsrlconfirminf()
        Dim raceParam As ArrayList = New ArrayList
        raceParam = race.searchconfirminf("PERSONRACE")
        Dim racecount As Integer
        Dim raceid As String
        Dim racenm As String
        Dim Listas As New ArrayList(raceParam.Count / 2)
        Dim icnt As Integer = 0
        Dim strTemp As String

        If strCustomerType = "CORPORATE" Then

            Listas.Add("ALL:ALL")
            For racecount = 0 To raceParam.Count - 1
                raceid = raceParam.Item(racecount)
                racenm = raceParam.Item(racecount + 1)
                racecount = racecount + 1
                If raceid.Equals("ZNONAPPL") Then
                    strTemp = raceid & ":" & racenm
                    Listas.Add(strTemp)
                End If

            Next
        ElseIf strCustomerType = "INDIVIDUAL" Then

            Listas.Add("ALL:ALL")
            For racecount = 0 To raceParam.Count - 1
                raceid = raceParam.Item(racecount)
                racenm = raceParam.Item(racecount + 1)
                racecount = racecount + 1
                If Not raceid.Equals("ZNONAPPL") Then
                    strTemp = raceid & ":" & racenm
                    Listas.Add(strTemp)
                End If

            Next
        Else
            Listas.Add("ALL:ALL")
            For racecount = 0 To raceParam.Count - 1
                raceid = raceParam.Item(racecount)
                racenm = raceParam.Item(racecount + 1)
                racecount = racecount + 1

                strTemp = raceid & ":" & racenm
                Listas.Add(strTemp)

            Next

        End If

        Return Listas
    End Function
#End Region

    Protected Sub ProdClassDLL_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ProdClassDLL.SelectedIndexChanged
        minmodel.Items.Clear()
        maxmodel.Items.Clear()
        clsCommon.ctryid = Session("login_ctryID").ToString.ToUpper()
        clsCommon.ClsID = ProdClassDLL.SelectedValue
        Dim ds As DataSet = clsCommon.getModelfrmClass()
        If ds.Tables.Count <> 0 Then
            Dim row As DataRow
            minmodel.Items.Add(New ListItem(objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), ""))
            maxmodel.Items.Add(New ListItem(objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), ""))
            For Each row In ds.Tables(0).Rows
                Dim NewItem As New ListItem()
                NewItem.Text = row("id") & "-" & row("name")
                NewItem.Value = row("id")
                minmodel.Items.Add(NewItem)
                maxmodel.Items.Add(NewItem)
            Next
        End If
    End Sub

End Class
