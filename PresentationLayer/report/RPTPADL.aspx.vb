Imports BusinessEntity
Imports System.Configuration
Imports System.Data
Imports System.Data.SqlClient
Imports System.Xml
Partial Class PresentationLayer_Report_RPTPADL
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If

        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try

        '''access control'''''''''''''''''''''''''''''''''''''''''''''''
        Dim accessgroup As String = Session("accessgroup").ToString
        Dim purviewArray As Array = New Boolean() {False, False, False, False}
        purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "42")
        Me.reportviewer.Enabled = purviewArray(3)
        If Not Page.IsPostBack Then
            Session("RPTPADLflag") = "0"
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            Me.ServiceCenterlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDAR-SVC")
            Me.Customeridlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSOSA-CUSTID")
            Me.SerialNolab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTMDSL-0003")
            Me.datelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDAR-DATE")
            Me.Modellab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSOSA-MODEL")
            Me.AreaLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTPADL-AREA")

            Me.statLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTATE-0001")
            Me.Fromlab1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Me.Fromlab2.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")

            Me.Fromlab4.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Me.Fromlab5.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Me.Fromlab6.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Me.Fromlab7.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Me.Fromlab8.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Me.Tolab1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Me.Tolab2.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")

            Me.Tolab4.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Me.Tolab5.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Me.Tolab6.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Me.ToLabel7.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Me.ToLabel8.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")

            reportviewer.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-VIEWREPORT")

            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTPADL-PRAD")

            'Me.contact.Items.Clear()

            Me.maxsrevice.Text = "ZZZ"
            Me.maxCustomerid.Text = "ZZZ"
            Me.maxModel.Text = "ZZZ"
            Me.maxSerialNo.Text = "ZZZ"
            Me.maxstat.Text = "ZZZ"
            Me.mindate.Text = "01/01/2006"

            Me.maxdate.Text = "31/12/2006"
            Me.maxarea.Text = "ZZZ"
            Me.minarea.Text = "AAA"
            Me.minCustomerid.Text = "AAA"
            Me.minModel.Text = "AAA"
            Me.minSerialNo.Text = "AAA"
            Me.minsrevice.Text = "AAA"
            Me.minstat.Text = "AAA"
        ElseIf Session("RPTPADLflag") = "1" Then
            datasearch()
        End If


        HypCalFrom.NavigateUrl = "javascript:DoCal(document.form1.mindate);"
        HypCalTo.NavigateUrl = "javascript:DoCal(document.form1.maxdate);"
    End Sub

    Protected Sub reportviewer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles reportviewer.Click
        Session("RPTPADLflag") = "1"
        datasearch()
        'Response.Redirect("~/PresentationLayer/REPORT/RPTPADLVIEW.aspx")
        Dim script As String = "window.open('../report/RPTPADLVIEW.aspx')"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "RPTPADLVIEW", script, True)

        Return
    End Sub

    Public Sub datasearch()
        Dim clsReport As New ClsCommonReport
        Dim clsrptpadl As New ClsRptPADL
        clsrptpadl.username = Session("username")
        clsrptpadl.userid = Session("userID")

        mindate.Text = Request.Form("mindate")
        maxdate.Text = Request.Form("maxdate")

        If (Trim(Me.minsrevice.Text) <> "AAA") Then
            clsrptpadl.Minsvcid = minsrevice.Text.ToUpper()
        End If

        If (Trim(Me.maxsrevice.Text) <> "ZZZ") Then
            clsrptpadl.Maxsvcid = maxsrevice.Text.ToUpper()
        Else
            clsrptpadl.Maxsvcid = "ZZZZZZZZZZ"
        End If

        If (Trim(Me.minCustomerid.Text) <> "AAA") Then
            clsrptpadl.Mincustid = minCustomerid.Text.ToUpper()
        End If

        If (Trim(Me.maxCustomerid.Text) <> "ZZZ") Then
            clsrptpadl.Maxcustid = maxCustomerid.Text.ToUpper()
        Else
            clsrptpadl.Maxcustid = "ZZZZZZZZZZZZZZZZZZZZ"
        End If

        If (Trim(Me.minSerialNo.Text) <> "AAA") Then
            clsrptpadl.Minserialno = minSerialNo.Text.ToUpper()
        End If

        If (Trim(Me.maxSerialNo.Text) <> "ZZZ") Then
            clsrptpadl.Maxserialno = maxSerialNo.Text.ToUpper()
        Else
            clsrptpadl.Maxserialno = "ZZZZZZZZZZ"
        End If

        If (Trim(Me.mindate.Text) <> "") Then
            If (Trim(Me.mindate.Text) <> "01/01/2006") Then
                Dim temparr As Array = mindate.Text.Split("/")
                clsrptpadl.Mindate = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
            Else
                clsrptpadl.Mindate = "2006-01-01"
            End If
        End If

        If (Trim(Me.maxdate.Text) <> "") Then
            If (Trim(Me.maxdate.Text) <> "31/12/2006") Then
                Dim temparr As Array = maxdate.Text.Split("/")
                clsrptpadl.Maxdate = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
            Else
                clsrptpadl.Maxdate = "2006-12-31"
            End If
        End If

        If (Trim(Me.minModel.Text) <> "AAA") Then
            clsrptpadl.Minmodel = minModel.Text.ToUpper()
        End If

        If (Trim(Me.maxModel.Text) <> "ZZZ") Then
            clsrptpadl.Maxmodel = maxModel.Text.ToUpper()
        Else
            clsrptpadl.Maxmodel = "ZZZZZZZZZZ"
        End If

        If (Trim(Me.minstat.Text) <> "AAA") Then
            clsrptpadl.Minstat = minstat.Text.ToUpper()
        End If

        If (Trim(Me.maxstat.Text) <> "ZZZ") Then

            clsrptpadl.Maxstat = maxstat.Text.ToUpper()
        Else
            clsrptpadl.Maxstat = "ZZZZZZZZZZ"
        End If

        If (Trim(Me.minarea.Text) <> "AAA") Then
            clsrptpadl.Minarea = minarea.Text.ToUpper()
        End If

        If (Trim(Me.maxarea.Text) <> "ZZZ") Then
            clsrptpadl.Maxarea = maxarea.Text.ToUpper()
        Else
            clsrptpadl.Maxarea = "ZZZZZZZZZZ"
        End If

        Dim rank As String = Session("login_rank")
        If rank <> 0 Then
            clsrptpadl.ctryid = Session("login_ctryID")
            clsrptpadl.compid = Session("login_cmpID")
            clsrptpadl.svcid = Session("login_svcID")
        End If
        clsrptpadl.rank = rank

        'Dim rptpadl As DataSet = clsrptpadl.GetRptpadl()
        'Session("rptpadl_ds") = rptpadl
        Session("rptpadl_search_condition") = clsrptpadl

        'Dim objXmlTr As New clsXml
        'Dim strHead As Array = Array.CreateInstance(GetType(String), 10)
        'objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        'strHead(0) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTPADL-CUSTNAME")
        'strHead(1) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTPADL-ADDRE1")
        'strHead(2) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCUSR-USERID")
        'strHead(3) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTPADL-REPORTID")

        'strHead(4) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDAR-TITLE1")
        'strHead(5) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTPADL-PRAD")
        'strHead(6) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTPADL-TITLE2")
        'strHead(7) = Me.mindate.Text
        'strHead(8) = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
        'strHead(9) = Me.maxdate.Text

        'If Me.IsPostBack Then

        '    With clsReport
        '        .ReportFileName = "RptPADL.rpt"  'TextBox1.Text
        '        .SetReport(CrystalReportViewer1, rptpadl, strHead)


        '    End With

        'End If

    End Sub

   
End Class
