﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="RPTTSKH.aspx.vb" Inherits="PresentationLayer_Report_RPTTSKH" %>

<%@ Register Assembly="JCalendar" Namespace="JCalendar" TagPrefix="cc1" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Technician stock on hand listing</title>
    <link href="../css/style.css" rel="stylesheet" type="text/css" />
     <STYLE type="text/css">A:link { COLOR: #336666; TEXT-DECORATION: none }
	BODY { FONT-SIZE: 9pt; FONT-FAMILY: "Arial", "Verdana", "Tahoma"; BACKGROUND-COLOR: #ffffff }
	TD { FONT-SIZE: 9pt; FONT-FAMILY: Arial,Verdana,Tahoma }
	</STYLE>
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
        
        <script language="JavaScript" src="../js/common.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table id="countrytab" border="0" style="width: 100%">
   <tr>
                <td style="width: 95%">
                    <table cellspacing="0" cellpadding="0" width="100%" border="0" style="width: 100%">
                        <tr>
                            <td width="1%" background="../graph/title_bg.gif">
                                <img height="24" src="../graph/title1.gif" width="5"></td>
                            <td class="style2" background="../graph/title_bg.gif" style="width: 100%">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="right" background="../graph/title_bg.gif" style="width: 4%">
                                <img height="24" src="../graph/title_2.gif" width="5"></td>
                        </tr>
                    </table>
            </tr>
            <tr>
                <td>
                    <table id="country" bgcolor="#b7e6e6" border="0" cellpadding="2" cellspacing="1" style="width: 100%">
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 27%; height: 28px;">
                                <asp:Label ID="techlab" runat="server" Text="Label"></asp:Label></td>
                            <td align="right" style="width: 13%; height: 28px;">
                                <asp:Label ID="from1" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 34%; height: 28px;">
                                <asp:TextBox ID="techni1" runat="server" CssClass="textborder" style="width: 90%">AAA</asp:TextBox></td>
                            <td align="right" style="width: 12%; height: 28px;">
                                <asp:Label ID="to1" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 37%; height: 28px">
                                <asp:TextBox ID="techni2" runat="server" CssClass="textborder">ZZZ</asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 27%; height: 28px">
                                <asp:Label ID="serlab" runat="server" Text="Label"></asp:Label>
                            </td>
                            <td align="right" style="width: 13%; height: 28px">
                                <asp:Label ID="from2" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 34%; height: 28px">
                                &nbsp;<asp:TextBox ID="sercen1" runat="server" CssClass="textborder" style="width: 90%">AAA</asp:TextBox></td>
                            <td align="right" style="width: 12%; height: 28px">
                                <asp:Label ID="to2" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 37%; height: 28px">
                                &nbsp;<asp:TextBox ID="sercen2" runat="server" CssClass="textborder">ZZZ</asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 27%; height: 28px;">
                                <asp:Label ID="datelab" runat="server" Text="Label"></asp:Label></td>
                            <td align="right" style="width: 13%; height: 28px;">
                                &nbsp;<asp:Label ID="from3" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 34%; height: 28px;">
                                <asp:TextBox ID="date1" runat="server" CssClass="textborder" style="width: 80%" ReadOnly="True">01/01/2006</asp:TextBox>
                                
                                
                                <asp:HyperLink ID="HypCalFrom" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif" ToolTip="Choose a Date">Choose a Date</asp:HyperLink>

                                <cc1:JCalendar
                                    ID="JCalendar1" runat="server" ControlToAssign="date1" ImgURL="~/PresentationLayer/graph/calendar.gif" Visible =false  />
                                &nbsp;
                            </td>
                            <td align="right" style="width: 12%; height: 28px;">
                                <asp:Label ID="to3" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 37%; height: 28px">
                                <asp:TextBox ID="date2" runat="server" CssClass="textborder" ReadOnly="True" style="width: 80%">31/12/2006</asp:TextBox>
                                
                                <asp:HyperLink ID="HypCalTo" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif" ToolTip="Choose a Date">Choose a Date</asp:HyperLink>

                                <cc1:JCalendar
                                    ID="JCalendar2" runat="server" ControlToAssign="date2" ImgURL="~/PresentationLayer/graph/calendar.gif" Visible =false  />
                            </td>
                        </tr>
                    </table>
                    <asp:LinkButton ID="LinkButton1" runat="server">LinkButton</asp:LinkButton></td>
            </tr>
        </table>
        
    </div>
        <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" AutoDataBind="true" />
    </form>
</body>
</html>
