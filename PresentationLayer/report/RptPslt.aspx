<%@ Page Language="VB" AutoEventWireup="false" CodeFile="RptPslt.aspx.vb" Inherits="PresentationLayer_Report_RptPslt_aspx" %>

<%@ Register Assembly="JCalendar" Namespace="JCalendar" TagPrefix="cc1" %>
<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc2" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>product summary list</title>
      <meta http-equiv="Content-Type" content="text/html; charset=gb2312">
	<LINK href="../css/style.css" type="text/css" rel="stylesheet">      
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
        
        <script language="JavaScript" src="../js/common.js"></script>
	<script language ="javascript" >

function changedateformat()
{
	alert(document.getElementById("RadioButtonList1").id);
}

 function LoadTechnician_CallBack(response){
    
     //if the server-side code threw an exception
     if (response.error != null)
     {
      //we should probably do better than this
      alert(response.error); 
      
      return;
     }

     var ResponseValue = response.value;
     
   
     //if the response wasn't what we expected  
     if (ResponseValue == null || typeof(ResponseValue) != "object")
     {       
      return;
     }
          
     //Get the states drop down
     var ResultList = document.getElementById("<%=TechnicianDrpListFrom.ClientID%>");
     var ResultTechTo = document.getElementById("<%=TechnicianDrpListTo.ClientID%>");
     ResultList.options.length = 0; //reset the techx dropdown     
     ResultTechTo.options.length = 0; //reset the techx dropdown
      
     //Remember, its length not Length in JavaScript
     for (var i = 0; i < ResponseValue.length; ++i)
     {
     
      //the columns of our rows are exposed like named properties
      var al = ResponseValue[i].split(":");
      var alid = al[0];
      var alname = al[1];
      ResultList.options[ResultList.options.length] =  new Option(alname, alid);
      ResultTechTo.options[ResultTechTo.options.length] =  new Option(alname, alid);
            
     }
}    

function LoadTechnician(objectClient)
{
 if (objectClient.selectedIndex > 0){

    var servicecenteridfrom = document.getElementById("<%=servCenterDrpListFrom.ClientID%>").options[document.getElementById("<%=servCenterDrpListFrom.ClientID%>").selectedIndex].value;
    var servicecenteridto = document.getElementById("<%=servCenterDrpListTo.ClientID%>").options[document.getElementById("<%=servCenterDrpListTo.ClientID%>").selectedIndex].value;    
     
    PresentationLayer_Report_RptPslt_aspx.GetTechnicianIDList(servicecenteridfrom,servicecenteridto,LoadTechnician_CallBack);    
    document.getElementById('minsercenter').value = servicecenteridfrom;     
 }
 else
 {

    document.getElementById("<%=TechnicianDrpListFrom.ClientID%>").options.length = 0
    document.getElementById("<%=TechnicianDrpListTo.ClientID%>").options.length = 0
 }
}

</script>

 
</head>
<body>
    <form id="form1" runat="server">
        <table id="countrytab" border="0" width="100%">
            <tr>
                <td style="width: 100%; height: 41px;">
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 103%">
                        <tr>
                            <td background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title1.gif" width="5" /></td>
                            <td background="../graph/title_bg.gif" class="style2" width="98%">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="right" background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title_2.gif" width="5" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100%; height: 1px;">
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                        <tr>
                            <td background="../graph/title_bg.gif" class="style2" width="98%" style="height: 1px">
                                <font color="red">
                                    <asp:Label ID="errlab" runat="server" Text="Label" Visible="false"></asp:Label></font></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100%; height: 1px;">
                    <table id="country" bgcolor="#b7e6e6" border="0" cellpadding="2" cellspacing="1" style="width: 100%">
                        <tr bgcolor="#ffffff" style="display:none;">
                            <td align="right" style="width: 10%; height: 22px;">
                                <asp:Label ID="Statelab" runat="server" Text="Lable" Font-Bold="False"></asp:Label></td>
                            <td align="right" style="width: 5%; height: 22px; font-size: 9pt;">
                                &nbsp;<asp:Label ID="Fromlab1" runat="server" Text="From" Height="16px"></asp:Label></td>
                            <td align="left" style="width: 20%; height: 22px; font-size: 9pt;">
                                <asp:TextBox ID="minstate" runat="server" CssClass="textborder" MaxLength="10" Width="104px">AAA</asp:TextBox></td>
                            <td align="right" style="width: 5%; height: 22px; font-size: 9pt;">
                                <asp:Label ID="Tolab1" runat="server" Text="To"></asp:Label></td>
                            <td align="left" style="width: 20%; height: 22px; font-size: 9pt;">
                                <asp:TextBox ID="maxstate" runat="server" CssClass="textborder" MaxLength="10" Width="104px">ZZZ</asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff" style="font-size: 9pt; display:none;">
                            <td align="right" style="width: 10%">
                                <asp:Label ID="Sercenterlab1" runat="server" Text="Lable"></asp:Label></td>
                            <td align="right" style="width: 5%">
                                <asp:Label ID="Fromlab2" runat="server" Text="From"></asp:Label></td>
                            <td align="left" style="width: 20%">
                                <asp:TextBox ID="minsercenter" runat="server" CssClass="textborder" MaxLength="10" Width="104px">AAA</asp:TextBox></td>
                            <td align="right" style="width: 5%">
                                <asp:Label ID="Tolab2" runat="server" Text="To"></asp:Label></td>
                            <td align="left" style="width: 20%">
                                <asp:TextBox ID="maxsercenter" runat="server" CssClass="textborder" MaxLength="10" Width="104px">ZZZ</asp:TextBox></td>                                                          
                        </tr>
                        <tr bgcolor="#ffffff" style="font-size: 9pt; display:none;">
                            <td align="right" style="width: 10%; height: 66px;">
                                <asp:Label ID="techlab2" runat="server" Text="Lable"></asp:Label></td>
                            <td align="right" style="width: 5%; height: 66px;">
                                <asp:Label ID="Fromlab3" runat="server" Text="From"></asp:Label></td>
                            <td align="left" style="width: 20%; height: 66px;">
                                <asp:TextBox ID="mintech" runat="server" CssClass="textborder" MaxLength="10" Width="104px">AAA</asp:TextBox></td>
                            <td align="right" style="width: 5%; height: 66px;">
                                <asp:Label ID="Tolab3" runat="server" Text="To"></asp:Label></td>
                            <td align="left" style="width: 20%; height: 66px;">
                                <asp:TextBox ID="maxtech" runat="server" CssClass="textborder" MaxLength="10" Width="104px">ZZZ</asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff" style="font-size: 9pt">
                            <td align="right" style="width: 10%">
                                <asp:Label ID="ServBilllab" runat="server" Text="Lable"></asp:Label></td>
                            <td align="right" style="width: 5%">
                                <asp:Label ID="Label4" runat="server" Text="From"></asp:Label></td>
                          
                            <td align="left" style="width: 20%;">
                                <asp:TextBox ID="datemin" runat="server" CssClass="textborder" AutoPostBack =true    ReadOnly="false" Width="80px">01/01/2006</asp:TextBox>
                                
                                <asp:HyperLink ID="HypCalFrom" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif" ToolTip="Choose a Date"  Visible =false >Choose a Date</asp:HyperLink>

                                <cc1:JCalendar ID="JCalendar1" runat="server" ControlToAssign="datemin" ImgURL="~/PresentationLayer/graph/calendar.gif" Visible =true  />
                                </td>                                
                            <td align="right" style="width: 5%">
                                <asp:Label ID="Label5" runat="server" Text="To"></asp:Label></td>
                            
                            <td align="left" style="width: 20%;">
                                <asp:TextBox ID="datemax" runat="server" CssClass="textborder" AutoPostBack =true    ReadOnly="false" Width="80px">01/01/2006</asp:TextBox>
                                
                                <asp:HyperLink ID="HypCalTo" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif" ToolTip="Choose a Date"  Visible =false >Choose a Date</asp:HyperLink>

                                <cc1:JCalendar ID="JCalendar2" runat="server" ControlToAssign="datemax" ImgURL="~/PresentationLayer/graph/calendar.gif" Visible =true  />
                                </td>  
                        </tr>                        
                        <tr bgcolor="#ffffff" style="font-size: 9pt">
                            <td align="right" style="width: 10%">
                                <asp:Label ID="Sercenterlab" runat="server" Text="Lable"></asp:Label></td>
                            <td align="right" style="width: 5%">
                                <asp:Label ID="Fromlab5" runat="server" Text="From"></asp:Label></td>
                          
                            <td align="left" style="width: 20%;">
                                    <asp:DropDownList ID="servCenterDrpListFrom" Width="93%" runat="server" CssClass="textborder"
                                        AutoPostBack="true" onchange="">
                                    </asp:DropDownList><font color="red">*</font>
                                    <asp:RequiredFieldValidator ID="servCIDError" runat="server" ControlToValidate="servCenterDrpListFrom"
                                        ForeColor="White">*</asp:RequiredFieldValidator>
                                </td>                                
                            <td align="right" style="width: 5%">
                                <asp:Label ID="Tolab5" runat="server" Text="To"></asp:Label></td>
                            
                            <td align="left" style="width: 20%;">
                                    <asp:DropDownList ID="servCenterDrpListTo" Width="93%" runat="server" CssClass="textborder"
                                        AutoPostBack="true" onchange="">
                                    </asp:DropDownList><font color="red">*</font>
                                    <asp:RequiredFieldValidator ID="servCIDError2" runat="server" ControlToValidate="servCenterDrpListTo"
                                        ForeColor="White">*</asp:RequiredFieldValidator>
                                </td>
                            <td align="left" rowspan="3"  valign ="top"  style="width: 38%; font-size: 9pt; display:none;">
                               
                                <asp:RadioButtonList ID="RadioButtonList1" runat="server" Height="1px" Width="64px" AutoPostBack =true >
                                    <asp:ListItem Selected="True">Date</asp:ListItem>
                                    <asp:ListItem>Month</asp:ListItem>
                                </asp:RadioButtonList><asp:Label ID="datefrom" runat="server" Text="From"></asp:Label>

                                &nbsp; &nbsp; &nbsp;<asp:Label ID="dateto"
                                        runat="server" Text="To"></asp:Label>
                                &nbsp; &nbsp;<asp:TextBox ID="datemax2" runat="server" CssClass="textborder" ReadOnly="True" Width="80px">06/01/2006</asp:TextBox>&nbsp;<br />
                                </td>                                 
                        </tr>         
                        <tr bgcolor="#ffffff" style="font-size: 9pt">
                            <td align="right" style="width: 10%">
                                <asp:Label ID="techlab" runat="server" Text="Lable"></asp:Label></td>
                            <td align="right" style="width: 5%">
                                <asp:Label ID="Label2" runat="server" Text="From"></asp:Label></td>
                          
                            <td align="left" style="width: 20%;">
                                    <asp:DropDownList ID="TechnicianDrpListFrom" Width="93%" runat="server" CssClass="textborder"
                                        AutoPostBack="true">
                                    </asp:DropDownList>
                                </td>                                
                            <td align="right" style="width: 5%">
                                <asp:Label ID="Label3" runat="server" Text="To"></asp:Label></td>
                            
                            <td align="left" style="width: 20%;">
                                    <asp:DropDownList ID="TechnicianDrpListTo" Width="93%" runat="server" CssClass="textborder"
                                        AutoPostBack="true">
                                    </asp:DropDownList>
                                </td>
                        </tr>                                       
                        <tr bgcolor="#ffffff" style="font-size: 9pt">
                            <td align="right" style="width: 10%">
                                <asp:Label ID="Partcodelab" runat="server" Text="Lable"></asp:Label></td>
                            <td align="right" style="width: 5%">
                                <asp:Label ID="Fromlab4" runat="server" Text="From"></asp:Label></td>
                            <td align="left" style="width: 20%">
                                <asp:TextBox ID="minPartcode" runat="server" CssClass="textborder" MaxLength="10" Width="104px">AAA</asp:TextBox></td>
                            <td align="right" style="width: 5%">
                                <asp:Label ID="Tolab4" runat="server" Text="To"></asp:Label></td>
                            <td align="left" style="width: 20%">
                                <asp:TextBox ID="maxPartcode" runat="server" CssClass="textborder" MaxLength="10" Width="104px">ZZZ</asp:TextBox></td>
                        </tr>                                                  
                        <tr bgcolor="#ffffff" style="font-size: 9pt">
                            <td colspan ="6"> &nbsp;</td>
                        </tr>
                        <tr bgcolor="#ffffff" style="font-size: 9pt">
                            <td colspan="3" style="display:none;">
                                &nbsp; &nbsp;
                                <asp:Label ID="applab" runat="server" Text="App Type" Width="200px"></asp:Label>
                            </td>
                            <td colspan="3" style="display:none;">
                                <asp:DropDownList ID="Drpapptype" runat="server" Width="80%" CssClass="textborder">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                    <%--Modified by Ryan Estandarte 25 Sept 2012--%>
                    <%--<asp:LinkButton ID="reportviewer" runat="server">View Report</asp:LinkButton>--%>
                    <asp:LinkButton ID="reportviewer" runat="server" Text="View Report" OnClick="ViewReport_Click"/>
                    <br/>
                    <%--Added by Ryan Estandarte 25 Sept 2012--%>
                    <asp:LinkButton ID="lnkExportExcel" runat="server" Text="Export to Excel" OnClick="ViewReport_Click" style="display:none;"/>
                    </td>
            </tr>
        </table>
        <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" AutoDataBind="true" />
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
            ShowSummary="False" />        
        <cc2:MessageBox ID="InfoMsgBox" runat="server" />
    </form>
</body>
</html>
