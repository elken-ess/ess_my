﻿Imports System.Configuration
Imports System.Globalization
Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports BusinessEntity

Partial Class PresentationLayer_report_SystemAccessReport
    Inherits System.Web.UI.Page

    Dim objXML As New clsXml
    Dim clsSAR As New clsSystemAccessReport
    Dim dateStyle As CultureInfo = New System.Globalization.CultureInfo("en-CA")
    'temporary set User ID
    Dim strUserID As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim script As String = "top.location='../logon.aspx';"
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                    'Response.Redirect("~/PresentationLayer/logon.aspx")
                End If
            Catch ex As Exception
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try

            Session.Contents("SystemAccessPrintDataFlag") = ""
            LoadInfo()
            strUserID = Session("userID").ToString

            'Set User Control Purview
            SetUserPurview()

            Me.printButton.Enabled = False
        ElseIf Session.Contents("SystemAccessPrintDataFlag") = "1" Then
            PrintData()
        Else
            ErrLab.Visible = False
        End If


        hypCalFrom.NavigateUrl = "javascript:DoCal(document.form1.DateFTextBox);"
        HypCalTo.NavigateUrl = "javascript:DoCal(document.form1.DateTTextBox);"
        HypCalLog.NavigateUrl = "javascript:DoCal(document.form1.LoginTimeTextBox);"


    End Sub

    Protected Sub LoadInfo()
        'Display the Label or Button Text=================================================
        objXML.XmlFile = ConfigurationManager.AppSettings("XmlFilePath")
        Me.titleLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_TITLE")
        Me.ServiceCenterFLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_FSC")
        Me.ServiceCenterTLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_TSC")
        Me.UserIDFLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_FUI")
        Me.UserIDTLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_TUI")
        Me.DateFLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_FD")
        Me.DateTLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_TD")
        Me.LoginTimeFLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_FLT")
        Me.LoginTimeTLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_TLT")
        Me.PCIDFLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_FPI")
        Me.PCIDTLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_TPI")
        Me.LoginTimeLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_LT")
        Me.retrieveButton.Text = objXML.GetLabelName("EngLabelMsg", "BB-VIEWREPORT")
        'Me.printButton.Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_PRI")
        Me.ServiceCenterFTextBox.Text = "AAA"
        Me.ServiceCenterTTextBox.Text = "ZZZ"
        Me.UserIDFTextBox.Text = "AAA"
        Me.UserIDTTextBox.Text = "ZZZ"
        Me.DateFTextBox.Text = "01/01/" & CStr(System.DateTime.Today.Year)
        Me.DateTTextBox.Text = "31/12/" & CStr(System.DateTime.Today.Year)
        Me.PCIDFTextBox.Text = "AAA"
        Me.PCIDTTextBox.Text = "ZZZ"
        Me.LoginTimeTextBox.Text = "01/01/" & CStr(System.DateTime.Today.Year)

        Me.RangeValidatorHF.ErrorMessage = objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_HER")
        Me.RangeValidatorMF.ErrorMessage = objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_MER")
        Me.RangeValidatorSF.ErrorMessage = objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_SER")
        Me.RangeValidatorHT.ErrorMessage = objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_HER")
        Me.RangeValidatorMT.ErrorMessage = objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_MER")
        Me.RangeValidatorST.ErrorMessage = objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_SER")
    End Sub

    Protected Sub retrieveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles retrieveButton.Click
        'Retrieve System Access Data from Server
        RetrieveData()
        'Dim strFromTime As String = "FHour=" + HourFTextBox.Text + "&FMin=" + MinuteFTextBox.Text + "&FSecond=" + SecondFTextBox.Text
        'Dim strToTime As String = "&THour=" + HourTTextBox.Text + "&TMin=" + MinuteTTextBox.Text + "&TSecond=" + SecondTTextBox.Text

        Dim script As String = "window.open('../report/SystemAccessReportView.aspx?" + "')"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "SystemAccessReport", script, True)
        'Response.Redirect("~/PresentationLayer/report/SystemAccessReportView.aspx?" + strFromTime + strToTime, True)
        'Me.Server.Transfer("SystemAccessReportView.aspx?" + strFromTime + strToTime)
    End Sub

    Private Sub RetrieveData()
        'clsSAR.FromServiceCenter = Trim(ServiceCenterFTextBox.Text)
        'clsSAR.ToServiceCenter = Trim(ServiceCenterTTextBox.Text)
        'clsSAR.FromUserID = Trim(UserIDFTextBox.Text)
        'clsSAR.ToUserID = Trim(UserIDTTextBox.Text)
        'clsSAR.FromDate = Trim(DateFTextBox.Text)
        'clsSAR.ToDate = Trim(DateTTextBox.Text)
        'clsSAR.FromLoginTime = HourFTextBox.Text + ":" + MinuteFTextBox.Text + ":" + SecondFTextBox.Text
        'clsSAR.ToLoginTime = HourTTextBox.Text + ":" + MinuteTTextBox.Text + ":" + SecondTTextBox.Text
        'clsSAR.FromPCID = Trim(PCIDFTextBox.Text)
        'clsSAR.ToPCID = Trim(PCIDTTextBox.Text)
        'clsSAR.LoginOutTime = Trim(LoginTimeTextBox.Text)
        'clsSAR.LoginUserID = Session("userID").ToString

        DateFTextBox.Text = Request.Form("DateFTextBox")
        DateTTextBox.Text = Request.Form("DateTTextBox")
        LoginTimeTextBox.Text = Request.Form("LoginTimeTextBox")

        Session("FServerCenter_SystemAccessReport") = Trim(ServiceCenterFTextBox.Text)
        Session("TServerCenter_SystemAccessReport") = Trim(ServiceCenterTTextBox.Text)
        Session("FUserID_SystemAccessReport") = Trim(UserIDFTextBox.Text)
        Session("TUserID_SystemAccessReport") = Trim(UserIDTTextBox.Text)
        Session("FDate_SystemAccessReport") = Trim(DateFTextBox.Text)
        Session("TDate_SystemAccessReport") = Trim(DateTTextBox.Text)
        Session("FTime_SystemAccessReport") = HourFTextBox.Text + ":" + MinuteFTextBox.Text + ":" + SecondFTextBox.Text
        Session("TTime_SystemAccessReport") = HourTTextBox.Text + ":" + MinuteTTextBox.Text + ":" + SecondTTextBox.Text
        Session("FPCID_SystemAccessReport") = Trim(PCIDFTextBox.Text)
        Session("TPCID_SystemAccessReport") = Trim(PCIDTTextBox.Text)
        Session("LoginOutTime_SystemAccessReport") = Trim(LoginTimeTextBox.Text)

        'Dim dsSAR As DataSet = clsSAR.RetriveSystemAccessInfo()
        'If dsSAR Is Nothing Then
        '    objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
        '    ErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_FUN_CALLLIST_RERR")
        '    ErrLab.Visible = True
        '    Return
        'End If

        'Dim i As Integer

        'If dsSAR.Tables.Count > 0 Then
        '    For i = 0 To dsSAR.Tables(0).Rows.Count - 1
        '        dsSAR.Tables(0).Rows(i).Item(0) = i + 1
        '    Next
        '    'SystemAccessGridView.DataSource = dsSAR
        '    'SystemAccessGridView.DataBind()

        '    'If dsSAR.Tables(0).Rows.Count > 0 Then
        '    '    DisplayGridViewHeader()
        '    'End If

        '    Session("SystemAccessReport") = dsSAR

        '    'printButton.Enabled = (dsSAR.Tables(0).Rows.Count > 0) And _
        '    '    (clsUserAccessGroup.GetUserPurview(Session("accessgroup"), "52", "P"))
        'Else
        '    'printButton.Enabled = False
        '    'SystemAccessGridView.DataSource = ""
        'End If

        'SARReportViewer.Visible = False
    End Sub

#Region "Display GridView"
    Protected Sub DisplayGridViewHeader()
        objXML.XmlFile = ConfigurationManager.AppSettings("XmlFilePath")

        Me.SystemAccessGridView.HeaderRow.Cells(0).Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_NO")
        Me.SystemAccessGridView.HeaderRow.Cells(0).Font.Size = 8
        Me.SystemAccessGridView.HeaderRow.Cells(1).Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_UN")
        Me.SystemAccessGridView.HeaderRow.Cells(1).Font.Size = 8
        Me.SystemAccessGridView.HeaderRow.Cells(2).Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_UI")
        Me.SystemAccessGridView.HeaderRow.Cells(2).Font.Size = 8
        Me.SystemAccessGridView.HeaderRow.Cells(3).Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_LG")
        Me.SystemAccessGridView.HeaderRow.Cells(3).Font.Size = 8
        Me.SystemAccessGridView.HeaderRow.Cells(4).Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_LO")
        Me.SystemAccessGridView.HeaderRow.Cells(4).Font.Size = 8
        Me.SystemAccessGridView.HeaderRow.Cells(5).Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_PCN")
        Me.SystemAccessGridView.HeaderRow.Cells(5).Font.Size = 8
        Me.SystemAccessGridView.HeaderRow.Cells(6).Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_IPA")
        Me.SystemAccessGridView.HeaderRow.Cells(6).Font.Size = 8
    End Sub
#End Region

    Protected Sub printButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles printButton.Click
        PrintData()
        SARReportViewer.Visible = True
    End Sub

    Private Sub PrintData()
        Dim dsReport As DataSet = Session.Contents("SystemAccessReport")
        Dim callListReportDoc As New ReportDocument()


        

        Try
            callListReportDoc.Load(MapPath("SystemAccessCrystalReport.rpt"))
            ErrLab.Visible = False
        Catch ex As Exception
            objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
            ErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_FUN_CALLLIST_LOADFAILED")
            ErrLab.Visible = True
            Return
        End Try
        callListReportDoc.SetDataSource(dsReport.Tables(0))

        objXML.XmlFile = ConfigurationManager.AppSettings("XmlFilePath")
        callListReportDoc.SetParameterValue("NO", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_NO"))
        callListReportDoc.SetParameterValue("UserName", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_UN"))
        callListReportDoc.SetParameterValue("UserID", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_UI"))
        callListReportDoc.SetParameterValue("LoginDate", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_LG"))
        callListReportDoc.SetParameterValue("OutDate", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_LO"))
        callListReportDoc.SetParameterValue("PCName", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_PCN"))
        callListReportDoc.SetParameterValue("IPAddress", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_IPA"))
        callListReportDoc.SetParameterValue("ReportID", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_PRITITLE"))
        callListReportDoc.SetParameterValue("UserIDName", Session("userID").ToString)

        callListReportDoc.SetParameterValue("FromServiceCenterLab", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_FSC"))
        callListReportDoc.SetParameterValue("ToServiceCenterLab", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_TSC"))
        callListReportDoc.SetParameterValue("FromServiceCenterBox", Trim(ServiceCenterFTextBox.Text))
        callListReportDoc.SetParameterValue("ToServiceCenterBox", Trim(ServiceCenterTTextBox.Text))
        callListReportDoc.SetParameterValue("FromUserIDLab", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_FUI"))
        callListReportDoc.SetParameterValue("ToUserIDLab", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_TUI"))
        callListReportDoc.SetParameterValue("FromUserIDBox", Trim(UserIDFTextBox.Text))
        callListReportDoc.SetParameterValue("ToUserIDBox", Trim(UserIDTTextBox.Text))
        callListReportDoc.SetParameterValue("FromDateLab", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_FD"))
        callListReportDoc.SetParameterValue("ToDateLab", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_TD"))
        callListReportDoc.SetParameterValue("FromDateBox", Trim(DateFTextBox.Text))
        callListReportDoc.SetParameterValue("ToDateBox", Trim(DateTTextBox.Text))
        callListReportDoc.SetParameterValue("FromLoginTimeLab", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_FLT"))
        callListReportDoc.SetParameterValue("ToLoginTimeLab", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_TLT"))
        callListReportDoc.SetParameterValue("FromLoginTimeBox", HourFTextBox.Text + ":" + MinuteFTextBox.Text + ":" + SecondFTextBox.Text)
        callListReportDoc.SetParameterValue("ToLoginTimeBox", HourTTextBox.Text + ":" + MinuteTTextBox.Text + ":" + SecondTTextBox.Text)
        callListReportDoc.SetParameterValue("FromPCIDLab", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_FPI"))
        callListReportDoc.SetParameterValue("ToPCIDLab", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_TPI"))
        callListReportDoc.SetParameterValue("FromPCIDBOX", Trim(PCIDFTextBox.Text))
        callListReportDoc.SetParameterValue("ToPCIDBOX", Trim(PCIDTTextBox.Text))
        callListReportDoc.SetParameterValue("LogOutTimeLab", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_LDT"))
        callListReportDoc.SetParameterValue("LogOutTimeBOX", Trim(LoginTimeTextBox.Text))

        SARReportViewer.ReportSource = callListReportDoc

        Session.Contents("SystemAccessPrintDataFlag") = "1"
    End Sub

    Protected Sub SystemAccessGridView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles SystemAccessGridView.PageIndexChanging
        SystemAccessGridView.PageIndex = e.NewPageIndex
        System.Threading.Thread.CurrentThread.CurrentCulture = dateStyle

        Dim dsSAR As DataSet = Session.Contents("SystemAccessReport")
        SystemAccessGridView.DataSource = dsSAR
        SystemAccessGridView.DataBind()

        If (dsSAR.Tables(0).Rows.Count > 0) Then
            DisplayGridViewHeader()
        End If
    End Sub

    'Protected Sub SADateFButton_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles SADateFButton.Click
    '    SCFCalendar.Visible = Not SCFCalendar.Visible
    '    SCFCalendar.Style.Item("position") = "absolute"
    'End Sub

    'Protected Sub SADateTButton_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles SADateTButton.Click
    '    SCTCalendar.Visible = Not SCTCalendar.Visible
    '    SCTCalendar.Style.Item("position") = "absolute"
    'End Sub

    'Protected Sub SCFCalendar_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles SCFCalendar.SelectionChanged        
    '    System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
    '    DateFTextBox.Text = SCFCalendar.SelectedDate
    '    SCFCalendar.Visible = False
    'End Sub

    'Protected Sub SCTCalendar_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles SCTCalendar.SelectionChanged
    '    System.Threading.Thread.CurrentThread.CurrentCulture = dateStyle
    '    DateTTextBox.Text = SCTCalendar.SelectedDate
    '    SCTCalendar.Visible = False
    'End Sub

    'Protected Sub LoginTimeButton_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles LoginTimeButton.Click
    '    LoginTimeCalendar.Visible = Not LoginTimeCalendar.Visible
    '    LoginTimeCalendar.Style.Item("position") = "absolute"
    'End Sub

    'Protected Sub LoginTimeCalendar_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles LoginTimeCalendar.SelectionChanged
    '    System.Threading.Thread.CurrentThread.CurrentCulture = dateStyle
    '    LoginTimeTextBox.Text = LoginTimeCalendar.SelectedDate
    '    LoginTimeCalendar.Visible = False
    'End Sub

    Private Sub SetUserPurview()        

        Dim arrayPurview = New Boolean() {False, False, False, False, False}
        arrayPurview = clsUserAccessGroup.GetUserPurview(Session("accessgroup"), "52")

        retrieveButton.Enabled = arrayPurview(3)
        'printButton.Enabled = arrayPurview(3)
    End Sub

    Protected Sub DTFJCalendar_SelectedDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DTFJCalendar.SelectedDateChanged
        System.Threading.Thread.CurrentThread.CurrentCulture = dateStyle
        DateFTextBox.Text = DTFJCalendar.SelectedDate
    End Sub

    Protected Sub DTTJCalendar_SelectedDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DTTJCalendar.SelectedDateChanged
        System.Threading.Thread.CurrentThread.CurrentCulture = dateStyle
        DateTTextBox.Text = DTTJCalendar.SelectedDate
    End Sub

    Protected Sub LoginTimeJCalendar_SelectedDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles LoginTimeJCalendar.SelectedDateChanged
        System.Threading.Thread.CurrentThread.CurrentCulture = dateStyle
        LoginTimeTextBox.Text = LoginTimeJCalendar.SelectedDate
    End Sub
End Class
