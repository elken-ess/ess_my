<%@ Page Language="VB" AutoEventWireup="false" EnableEventValidation="false" CodeFile="RPTCUSR.aspx.vb" Inherits="PresentationLayer_Report_RPTCUSR" %>

<%@ Register Assembly="JCalendar" Namespace="JCalendar" TagPrefix="cc1" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Customer Reference Report</title>
    <link href="../css/style.css" rel="stylesheet" type="text/css" />
     <STYLE type="text/css">A:link { COLOR: #336666; TEXT-DECORATION: none }
	BODY { FONT-SIZE: 9pt; FONT-FAMILY: "Arial", "Verdana", "Tahoma"; BACKGROUND-COLOR: #ffffff }
	TD { FONT-SIZE: 9pt; FONT-FAMILY: Arial,Verdana,Tahoma }
	</STYLE>
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
        <script language="JavaScript" src="../js/common.js"></script>
        
        <!--LW Added--> 
     <script language="javascript">  
    
        function LoadRaces_CallBack(response){
 

         //if the server-side code threw an exception
         if (response.error != null)
         {
          //we should probably do better than this
          alert(response.error); 
          
          return;
         }

         var ResponseValue = response.value;
         
       
         //if the response wasn't what we expected  
         if (ResponseValue == null || typeof(ResponseValue) != "object")
         {       
          return;
         }
              
         //Get the states drop down
         var ResultList = document.getElementById("<%=RacesDLL.ClientID%>");
         ResultList.options.length = 0; //reset the states dropdown
     
        
         //Remember, its length not Length in JavaScript
         for (var i = 0; i < ResponseValue.length; ++i)
         {
         
          //the columns of our rows are exposed like named properties
          var al = ResponseValue[i].split(":");
          var alid = al[0];
          var alname = al[1];
          ResultList.options[ResultList.options.length] =  new Option(alname, alid);
         }
     
        }
         
        function LoadRaces(ObjectClient)
        {
             var ObjectId = ObjectClient.options[ObjectClient.selectedIndex].value;
              
             PresentationLayer_Report_RPTCUSR.GetRaces(ObjectId, LoadRaces_CallBack); 
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table id="countrytab" border="0" style="width: 100%">
            <tr>
                <td style="width: 0px">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title1.gif" width="5" /></td>
                            <td background="../graph/title_bg.gif" class="style2" width="98%">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="right" background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title_2.gif" width="5" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 0px">
                    <table id="country" bgcolor="#b7e6e6" border="0" cellpadding="2" cellspacing="1" style="width: 100%">
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 18%">
                                <asp:Label ID="ServiceCenterlab" runat="server" Text="Label"></asp:Label></td>
                            <td align="right" style="width: 6%">
                                &nbsp;<asp:Label ID="Fromlab1" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 34%">
                            <asp:DropDownList ID="minsrevice" runat="server" CssClass="textborder" AutoPostBack =true Width ="70%">
                                </asp:DropDownList></td>
                            <td align="right" style="width: 11%">
                                <asp:Label ID="Tolab1" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%">
                                <asp:DropDownList ID="maxsrevice" runat="server" CssClass="textborder" AutoPostBack =true Width ="70%">
                                </asp:DropDownList></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 18%">
                                <asp:Label ID="statLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="right" style="width: 6%">
                                <asp:Label ID="Fromlab7" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 34%">
                            <asp:DropDownList ID="minstat" runat="server" CssClass="textborder" AutoPostBack =true  Width ="70%">
                                </asp:DropDownList></td>
                            <td align="right" style="width: 11%">
                                <asp:Label ID="ToLabel7" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%">
                                <asp:DropDownList ID="maxstat" runat="server" CssClass="textborder" AutoPostBack =true  Width ="70%">
                                </asp:DropDownList></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 18%">
                                <asp:Label ID="areaLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="right" style="width: 6%">
                                <asp:Label ID="Fromlab8" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 34%">
                                <asp:DropDownList ID="minArea" runat="server" CssClass="textborder" AutoPostBack =true  Width ="70%">
                            </asp:DropDownList></td>
                            <td align="right" style="width: 11%">
                                <asp:Label ID="ToLabel8" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%">
                                <asp:DropDownList ID="maxArea" runat="server" CssClass="textborder" AutoPostBack =true  Width ="70%">
                            </asp:DropDownList></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 18%">
                                <asp:Label ID="Customeridlab" runat="server" Text="Label"></asp:Label></td>
                            <td align="right" style="width: 6%">
                                <asp:Label ID="Fromlab2" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 34%">
                                <asp:TextBox ID="minCustomerid" runat="server" CssClass="textborder"></asp:TextBox></td>
                            <td align="right" style="width: 11%">
                                <asp:Label ID="Tolab2" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%">
                                <asp:TextBox ID="maxCustomerid" runat="server" CssClass="textborder"></asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 18%">
                                <asp:Label ID="datelab" runat="server" Text="Label"></asp:Label></td>
                            <td align="right" style="width: 6%">
                                <asp:Label ID="Fromlab4" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 34%">
                                <asp:TextBox ID="mindate" runat="server" CssClass="textborder"></asp:TextBox>
                                <asp:HyperLink ID="HypCalFrom" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif" ToolTip="Choose a Date">Choose a Date</asp:HyperLink>

                                
                                <cc1:JCalendar ID="JCalendar1" runat="server" ControlToAssign="mindate" ImgURL="~/PresentationLayer/graph/calendar.gif" Visible =false  />
                            </td>
                            <td align="right" style="width: 11%">
                                <asp:Label ID="Tolab4" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%">
                                <asp:TextBox ID="maxdate" runat="server" CssClass="textborder"></asp:TextBox>
                                
                                <asp:HyperLink ID="HypCalTo" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif" ToolTip="Choose a Date">Choose a Date</asp:HyperLink>

                                <cc1:JCalendar ID="JCalendar2" runat="server" ControlToAssign="maxdate" ImgURL="~/PresentationLayer/graph/calendar.gif" Visible =false  />
                            </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 18%">
                                <asp:Label ID="Modellab" runat="server" Text="Label"></asp:Label></td>
                            <td align="right" style="width: 6%">
                                <asp:Label ID="Fromlab6" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 34%">
                                 <asp:DropDownList ID="minmodel" runat="server" CssClass="textborder" AutoPostBack="true"></asp:DropDownList></td>
                            <td align="right" style="width: 11%">
                                <asp:Label ID="Tolab6" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%">
                             <asp:DropDownList ID="maxModel" runat="server" CssClass="textborder" AutoPostBack="true"></asp:DropDownList></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 18%">
                                <asp:Label ID="SerialNolab" runat="server" Text="Label"></asp:Label></td>
                            <td align="right" style="width: 6%">
                                <asp:Label ID="Fromlab5" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 34%">
                                <asp:TextBox ID="minSerialNo" runat="server" CssClass="textborder"></asp:TextBox></td>
                            <td align="right" style="width: 11%">
                                <asp:Label ID="Tolab5" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%">
                                <asp:TextBox ID="maxSerialNo" runat="server" CssClass="textborder"></asp:TextBox></td>
                        </tr>
                        
                        <tr bgcolor="white">
                            <td align="right" style="width: 18%;">
                                <asp:Label ID="CustomerTypeLab" runat="server" Text="Label"></asp:Label>
                            </td>
                            <td align="left" colspan="4">
                                <asp:DropDownList ID="CustomerTypeDLL" runat="server" CssClass="textborder" AutoPostBack="false" onchange="LoadRaces(this)">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr bgcolor="white">
                            <td align="right" style="width: 18%;">
                                <asp:Label ID="RacesLab" runat="server" Text="Label"></asp:Label>
                            </td>
                            <td align="left" colspan="4">
                                <asp:DropDownList ID="RacesDLL" runat="server" CssClass="textborder" >
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" style="width: 18%; height: 26px;">
                                <asp:Label ID="ContractTypelab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" colspan="4" style="height: 26px">
                                <asp:DropDownList ID="ContractType" runat="server" Width="128px">
                                </asp:DropDownList></td>
                        </tr>
                         <tr bgcolor="#ffffff">
                            <td align="right" style="width: 18%; height: 26px;">
                                <asp:Label ID="ProductClslab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" colspan="4" style="height: 26px">
                                <asp:DropDownList ID="ProductClass" runat="server" Width="128px" AutoPostBack ="true" >
                                </asp:DropDownList></td>
                        </tr>
                    </table>
                    <asp:LinkButton ID="reportviewer" runat="server">LinkButton</asp:LinkButton>&nbsp;&nbsp;&nbsp;
                    </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
