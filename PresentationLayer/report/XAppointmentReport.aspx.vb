Imports System.Data
Imports System.Data.SqlClient
Imports System.Globalization
Imports SQLDataAccess
Imports BusinessEntity

Partial Class PresentationLayer_report_AppointmentSchedulingReport
    Inherits System.Web.UI.Page

    Private ReadOnly _common As New BusinessEntity.clsCommonClass()
    Private ReadOnly _datestyle As Globalization.CultureInfo = New Globalization.CultureInfo("en-CA")
    Private _rptSvc As New ClsRptServiceDue
    Dim apptEntity As New clsAppointment()

    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)
        Return connection
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim _con As SqlConnection
        _con = GetConnection(ConfigurationManager.AppSettings("ConnectionString"))


        If SVCFromDDL.SelectedValue = "" Then
            Dim Comm2 As SqlCommand
            Comm2 = _con.CreateCommand
            Comm2.CommandText = "Select MSVC_SVCID, MSVC_ENAME from MSVC_FIL where MSVC_STAT = 'ACTIVE' and MSVC_SVCID not in ('901','951','ZZZ') order by MSVC_SVCID"
            _con.Open()

            Dim dr1 As SqlDataReader = Comm2.ExecuteReader(CommandBehavior.CloseConnection)

            Dim dt1 As DataTable = New DataTable()
            dt1.Load(dr1)

            SVCFromDDL.Items.Add(New ListItem("ALL", "ALL"))
            SVCToDDL.Items.Add(New ListItem("ALL", "ALL"))

            Dim row As DataRow
            If dt1.Rows.Count > 0 Then
                For Each row In dt1.Rows
                    Dim newItem As New ListItem()
                    newItem.Text = Trim(row(0).ToString()) & " - " & row(1).ToString()
                    newItem.Value = Trim(row(0).ToString())
                    SVCFromDDL.Items.Add(newItem)
                    SVCToDDL.Items.Add(newItem)
                Next
            End If

            _con.Close()
        End If

        

        If Page.IsPostBack = False Then
            If ddlUserNameFr.SelectedValue = "" Then
                Dim Comm2 As SqlCommand
                Comm2 = _con.CreateCommand
                Comm2.CommandText = "select distinct musr_usrid, musr_ename from MUSR_FIL where   MUSR_STAT in ('ACTIVE','BARRED') and MUSR_STAT <> 'DELETE' order by musr_usrid,musr_ename"
                _con.Open()

                Dim dr1 As SqlDataReader = Comm2.ExecuteReader(CommandBehavior.CloseConnection)

                Dim dt1 As DataTable = New DataTable()
                dt1.Load(dr1)

                ddlUserNameFr.Items.Add(New ListItem("ALL", "ALL"))

                Dim row As DataRow
                If dt1.Rows.Count > 0 Then
                    For Each row In dt1.Rows
                        Dim newItem As New ListItem()
                        newItem.Text = Trim(row(0).ToString()) & " - " & row(1).ToString()
                        newItem.Value = Trim(row(0).ToString())
                        ddlUserNameFr.Items.Add(newItem)
                    Next
                End If

                _con.Close()
            End If

            Dim dt As DataTable
            dt = _rptSvc.PopulateProductClass("PRODUCTCL")
            BindValuesToDropdown(dt, ddFromProduct)

            PopulateModelFromProduct(ddFromProduct.SelectedValue, ddFromModelID)
            PopulateModelFromProduct(ddFromProduct.SelectedValue, ddToModelID)

            Dim dtReason As DataTable
            dtReason = apptEntity.GetObjectionReason().Tables(0)
            ddlReasonObjection.DataSource = dtReason
            ddlReasonObjection.DataValueField = "ReasonObj_ID"
            ddlReasonObjection.DataTextField = "ReasonObj_Desc"
            ddlReasonObjection.DataBind()
            Dim list As New ListItem("ALL", "")
            ddlReasonObjection.Items.Insert(0, list)

            'FromDateTxt.Text = Now.ToString("dd/MM/yyyy")
            'ToDateTxt.Text = Now.ToString("dd/MM/yyyy")
            'FromCreateDateTxt.Text = Now.ToString("dd/MM/yyyy")
            'ToCreateDateTxt.Text = Now.ToString("dd/MM/yyyy")
        End If

    End Sub

    Protected Sub RetrieveBtn1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles RetrieveBtn1.Click
        If (FromDateTxt.Text = "DD/MM/YYYY" Or FromDateTxt.Text = "") And (ToDateTxt.Text = "DD/MM/YYYY" Or ToDateTxt.Text = "") and (FromCreateDateTxt.Text = "DD/MM/YYYY" or FromCreateDateTxt.Text = "") And (ToCreateDateTxt.Text = "DD/MM/YYYY" or ToCreateDateTxt.Text = "")  Then
            InfoMsgBox.Alert("Please choose either Appointment Date or Create Date ")
            Exit Sub
        End If

        Session("FromDate") = FromDateTxt.Text
        Session("ToDate") = ToDateTxt.Text
        Session("SVCFrom") = SVCFromDDL.SelectedValue.ToString
        Session("SVCTo") = SVCToDDL.SelectedValue.ToString
        Session("FromCreateDate") = FromCreateDateTxt.Text
        Session("ToCreateDate") = ToCreateDateTxt.Text
        Session("ProductFrom") = ddFromProduct.SelectedValue
        Session("ModelFrom") = ddFromModelID.SelectedValue
        Session("ModelTo") = ddToModelID.SelectedValue
        Session("CRID") = ddlUserNameFr.SelectedValue
        Session("ReasonObjection") = ddlReasonObjection.SelectedValue
        Session("ReasonObjectionDesc") = ddlReasonObjection.SelectedItem.Text


        Const script As String = "window.open('../report/XAppointmentReport_View.aspx')"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "XAppointmentReport_View", script, True)

    End Sub

    Private Sub PopulateModelFromProduct(ByVal productID As String, ByVal ddModel As DropDownList)
        'ddModel.Items.Clear()
        'Dim dtModel As New DataTable
        '_rptSvc.ProductID = productID
        '_rptSvc.CountryID = Session("login_ctryID").ToString()

        'dtModel = _rptSvc.RetrieveModelFromProduct().Tables(0)

        'If dtModel.Rows.Count > 0 Then
        '    BindValuesToDropdown(dtModel, ddModel)
        'End If
      
        Dim common As New clsCommonClass
        common.spctr = Session("login_ctryID").ToString()
        common.spstat = String.Empty
        common.sparea = String.Empty
        common.rank = String.Empty

        Dim modelDS As DataSet = common.Getcomidname("BB_MASMOTY_IDNAME")
        If modelDS.Tables(0).Rows.Count > 0 Then
            databonds(modelDS, ddModel)
        End If

    End Sub
    Public Function databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList) As ArrayList
        dropdown.Items.Clear()
        dropdown.Items.Add(New ListItem("ALL", ""))
        Dim row As DataRow
        Dim infon As ArrayList = New ArrayList()
        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            NewItem.Text = row("id") & "-" & row("name")
            NewItem.Value = row("id")
            dropdown.Items.Add(NewItem)
            infon.Add(NewItem.Value)
        Next
        Return infon

    End Function

    Private Sub BindValuesToDropdown(ByVal dt As DataTable, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        dropdown.Items.Add(New ListItem("ALL", ""))

        Dim row As DataRow
        If dt.Rows.Count > 0 Then
            For Each row In dt.Rows
                Dim newItem As New ListItem()
                newItem.Text = Trim(row(0).ToString()) & "-" & row(1).ToString()
                newItem.Value = Trim(row(0).ToString())
                dropdown.Items.Add(newItem)
            Next
        End If
    End Sub
End Class
