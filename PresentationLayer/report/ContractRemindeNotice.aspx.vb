Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Web
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web
Imports BusinessEntity
Imports System.Data

Partial Class PresentationLayer_report_ContractRemindeNotice_aspx
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try

        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Dim clsReport As New ClsCommonReport
        If Not Page.IsPostBack Then
            Session("CTRNWL-FLAG") = "0"

            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            Me.coustomlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0001")
            Me.coustomfromlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Me.coustomtolab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Me.sercenterlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0002")
            Me.sercenterfromlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Me.sercentertolab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Me.promodlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0003")
            Me.promodfromlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Me.promodtolab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Me.statelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0004")
            Me.statefromlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Me.statetolab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Me.datelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CNT-CNTSDATE")
            Me.datefromlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Me.datetolab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Me.sernoLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0007")
            Me.sernofromLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Me.sernotoLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Me.LinkButton1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-VIEWREPORT")
            'Me.LinkButton2.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0005")
            Me.titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRN-0001")

            '''access control'''''''''''''''''''''''''''''''''''''''''''''''
            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "40")
            Me.LinkButton1.Enabled = purviewArray(3)



            Me.coustomfrombox.Text = "AAA"
            Me.sercenterfrombox.Text = "AAA"
            Me.promodfrombox.Text = "AAA"
            Me.statefrombox.Text = "AAA"
            Me.sernofromBox.Text = "AAA"
            Me.coustomtoBox.Text = "ZZZ"
            Me.sercentertoBox.Text = "ZZZ"
            Me.promodtobox.Text = "ZZZ"
            Me.statetobox.Text = "ZZZ"
            Me.sernotoBox.Text = "ZZZ"
            Me.datetobox.Text = Date.Now().ToString().Substring(0, 10)
            Me.datefrombox.Text = "01/01/2006"
        ElseIf Session("CTRNWL-FLAG") = "1" Then
            Print_Report()
        End If


        HypCalFrom.NavigateUrl = "javascript:DoCal(document.form1.datefrombox);"

        HypCalTo.NavigateUrl = "javascript:DoCal(document.form1.datetobox);"

    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton1.Click
        Session("CTRNWL-FLAG") = "1"

        datefrombox.Text = Request.Form("datefrombox")
        datetobox.Text = Request.Form("datetobox")
        Print_Report()
        'Response.Redirect("~/PresentationLayer/REPORT/ContractRemindeNoticesearch.aspx")
        Dim script As String = "window.open('../report/ContractRemindeNoticesearch.aspx')"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "SystemAccessReport", script, True)
    End Sub





    Protected Sub Print_Report()
        Dim crnEntity As New clsRPTCRN

        If (Trim(Me.coustomfrombox.Text) <> "AAA") Then
            crnEntity.CustmIDFrom = Me.coustomfrombox.Text
        End If
        If (Trim(Me.coustomtoBox.Text) <> "ZZZ") Then
            crnEntity.CustmIDTo = Me.coustomtoBox.Text
        Else
            crnEntity.CustmIDTo = "ZZZZZZZZZZ"
        End If
        If (Trim(Me.sercenterfrombox.Text) <> "AAA") Then
            crnEntity.SvcIDFrom = Me.sercenterfrombox.Text
        End If
        If (Trim(Me.sercentertoBox.Text) <> "ZZZ") Then
            crnEntity.SvcIDTo = Me.sercentertoBox.Text
        Else : crnEntity.SvcIDTo = "ZZZZZZZZZZ"
        End If
        If (Trim(Me.promodfrombox.Text) <> "AAA") Then
            crnEntity.ModelIDFrom = Me.promodfrombox.Text
        End If
        If (Trim(Me.promodtobox.Text) <> "ZZZ") Then
            crnEntity.ModelIDTo = Me.promodtobox.Text
        Else : crnEntity.ModelIDTo = "ZZZZZZZZZZ"
        End If

        If (Trim(Me.statefrombox.Text) <> "AAA") Then
            crnEntity.StateFrom = Me.statefrombox.Text
        End If

        If (Trim(Me.statetobox.Text) <> "ZZZ") Then
            crnEntity.StateTo = Me.statetobox.Text
        Else : crnEntity.StateTo = "ZZZZZZZZZZ"
        End If
        If (Trim(sernofromBox.Text) <> "AAA") Then
            crnEntity.SerialNoFrom = Me.sernofromBox.Text
        End If
        If (Trim(Me.sernotoBox.Text) <> "ZZZ") Then
            crnEntity.SerialNoTo = Me.sernotoBox.Text
        Else : crnEntity.SerialNoTo = "ZZZZZZZZZZ"
        End If
        If (Trim(Me.datefrombox.Text) <> "") Then
            Dim temparr As Array = Me.datefrombox.Text.Split("/")
            crnEntity.StartDate = temparr(2) + "-" + temparr(1) + "-" + temparr(0)

        End If
        If (Trim(Me.datetobox.Text) <> "") Then
            Dim temparr As Array = Me.datetobox.Text.Split("/")
            crnEntity.EndDate = temparr(2) + "-" + temparr(1) + "-" + temparr(0)

        End If
        Dim rank As String = Session("login_rank")
        If rank <> 0 Then
            crnEntity.ctrid = Session("login_ctryID").ToString().ToUpper
            crnEntity.comid = Session("login_cmpID").ToString().ToUpper
            crnEntity.svcid = Session("login_svcID").ToString().ToUpper
        End If
        crnEntity.rank = rank
        crnEntity.UserName = Session("userID").ToString().ToUpper()

        Session("rptcontractremind_search_condition") = crnEntity
    End Sub






    Protected Sub Calendar1_SelectedDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendar1.SelectedDateChanged
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Dim prcidDate As New clsCommonClass()
        datefrombox.Text = Calendar1.SelectedDate.Date.ToString().Substring(0, 10)
    End Sub

    Protected Sub Calendar2_SelectedDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendar2.SelectedDateChanged
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Dim prcidDate As New clsCommonClass()
        datetobox.Text = Calendar2.SelectedDate.Date.ToString().Substring(0, 10)

    End Sub
End Class
