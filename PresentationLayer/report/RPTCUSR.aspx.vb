Imports BusinessEntity
Imports System.Configuration
Imports System.Data
Imports System.Data.SqlClient
Imports System.Xml
Partial Class PresentationLayer_Report_RPTCUSR
    Inherits System.Web.UI.Page

    Dim clsrptcust As New ClsRptCUSR
    Dim objXmlTr As New clsXml
    Dim clsCommon As New clsCommonClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If

        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try
        '-------------------------------access control------------------------------------------------

        'LW Added part: To add for ajax
        AjaxPro.Utility.RegisterTypeForAjax(GetType(PresentationLayer_Report_RPTCUSR))

        Dim accessgroup As String = Session("accessgroup").ToString
        Dim purviewArray As Array = New Boolean() {False, False, False, False}
        purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "35")
        Me.reportviewer.Enabled = purviewArray(3)
        If Not Page.IsPostBack Then
            Session("RPTCUSRflag") = "0"

            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            Me.ServiceCenterlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDAR-SVC")
            Me.Customeridlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSOSA-CUSTID")
            Me.SerialNolab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTMDSL-0003")
            Me.datelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDAR-DATE")
            Me.Modellab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSOSA-MODEL")
            Me.statLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSTATE-0001")
            Me.areaLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-AREAID")

            Me.ContractTypelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCUSR-CONTRACT")

            Me.Fromlab1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Me.Fromlab2.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")

            Me.Fromlab4.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Me.Fromlab5.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Me.Fromlab6.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Me.Fromlab7.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Me.Fromlab8.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Me.Tolab1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Me.Tolab2.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")

            Me.Tolab4.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Me.Tolab5.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Me.Tolab6.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Me.ToLabel7.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Me.ToLabel8.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            reportviewer.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-VIEWREPORT")

            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCUSR-APPHIS")
            ProductClslab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-0006")

            'Me.contact.Items.Clear()

            Dim dlstatus As New clsrlconfirminf()
            Dim dlParam As ArrayList = New ArrayList
            dlParam = dlstatus.searchconfirminf("YESNO")
            Dim dlcount As Integer
            Dim dlid As String
            Dim dlnm As String
            For dlcount = 0 To dlParam.Count - 1
                dlid = dlParam.Item(dlcount)
                dlnm = dlParam.Item(dlcount + 1)
                dlcount = dlcount + 1

                Me.ContractType.Items.Add(New ListItem(dlnm.ToString(), dlid.ToString()))
            Next


            Dim strUserID As String = Session("userID").ToString
            clsrptcust.userid = strUserID
            Dim ds As DataSet = clsrptcust.SVCnStatebyUserID()


            minstat.Items.Add(New ListItem(objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), "%"))
            maxstat.Items.Add(New ListItem(objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), "%"))
            Dim stateid As String
            Dim statenm As String
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                statenm = ds.Tables(0).Rows(i).Item(0) + "-" + ds.Tables(0).Rows(i).Item(1)
                stateid = ds.Tables(0).Rows(i).Item(0)
                minstat.Items.Add(New ListItem(statenm.ToString(), stateid.ToString()))
                maxstat.Items.Add(New ListItem(statenm.ToString(), stateid.ToString()))
                'stateFDropDownList.Items.Add(dsCLA.Tables(0).Rows(i).Item(0))
                'stateTDropDownList.Items.Add(dsCLA.Tables(0).Rows(i).Item(0))
            Next
            If minstat.Items.Count > 0 Then
                minstat.SelectedIndex = 0
                maxstat.SelectedIndex = 0
            End If

            minsrevice.Items.Add(New ListItem(objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), "%"))
            maxsrevice.Items.Add(New ListItem(objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), "%"))
            Dim scid As String
            Dim scnm As String
            For i As Integer = 0 To ds.Tables(1).Rows.Count - 1
                scnm = ds.Tables(1).Rows(i).Item(0) + "-" + ds.Tables(1).Rows(i).Item(1)
                scid = ds.Tables(1).Rows(i).Item(0)
                minsrevice.Items.Add(New ListItem(scnm.ToString(), scid.ToString()))
                maxsrevice.Items.Add(New ListItem(scnm.ToString(), scid.ToString()))
            Next
            If minsrevice.Items.Count > 0 Then
                minsrevice.SelectedIndex = 0
                maxsrevice.SelectedIndex = 0
            End If

            minArea.Items.Add(New ListItem(objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), "%"))
            maxArea.Items.Add(New ListItem(objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), "%"))
            Dim areaid As String
            Dim areanm As String
            For i As Integer = 0 To ds.Tables(2).Rows.Count - 1
                areanm = ds.Tables(2).Rows(i).Item(0) + "-" + ds.Tables(2).Rows(i).Item(1)
                areaid = ds.Tables(2).Rows(i).Item(0)
                minArea.Items.Add(New ListItem(areanm.ToString(), areaid.ToString()))
                maxArea.Items.Add(New ListItem(areanm.ToString(), areaid.ToString()))
            Next

            If minArea.Items.Count > 0 Then
                minArea.SelectedIndex = 0
                maxArea.SelectedIndex = 0
            End If

            Dim modelds As New DataSet
            clsCommon.spctr = Session("login_ctryID")
            clsCommon.spstat = ""
            clsCommon.sparea = ""
            clsCommon.rank = ""
            modelds = clsCommon.Getcomidname("BB_MASMOTY_IDNAME")
            If modelds.Tables.Count <> 0 Then
                Dim row As DataRow
                minmodel.Items.Add(New ListItem(objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), ""))
                maxModel.Items.Add(New ListItem(objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), ""))
                For Each row In modelds.Tables(0).Rows
                    Dim NewItem As New ListItem()
                    NewItem.Text = row("id") & "-" & row("name")
                    NewItem.Value = row("id")
                    minmodel.Items.Add(NewItem)
                    maxModel.Items.Add(NewItem)
                Next
            End If

            Dim cntryStat As New clsrlconfirminf()
            Dim count As Integer
            Dim prodid As String
            Dim prodnm As String
            Dim productcl As ArrayList = New ArrayList
            productcl = cntryStat.searchconfirminf("PRODUCTCL")
            count = 0
            ProductClass.Items.Add(New ListItem("", "%"))
            For count = 0 To productcl.Count - 1
                prodid = productcl.Item(count)
                prodnm = productcl.Item(count + 1)
                count = count + 1
                ProductClass.Items.Add(New ListItem(prodnm.ToString(), prodid.ToString()))
            Next
            ProductClass.Items(0).Selected = True


            '---------------------------------------------------------------------------------------

            'cboAreaIdTo.Items.Add(objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"))
            'For i = 0 To dsCLA.Tables(2).Rows.Count - 1
            '    cboAreaIdTo.Items.Add(dsCLA.Tables(2).Rows(i).Item(0))
            'Next

            'stDropDownList.Items.Add(objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"))
            ' For i = 0 To dsCLA.Tables(3).Rows.Count - 1
            'stDropDownList.Items.Add(dsCLA.Tables(3).Rows(i).Item(0))
            ' Next

            Me.ContractType.Items.Insert(0, "")
            'Me.maxsrevice.Text = "ZZZ"
            Me.maxCustomerid.Text = "ZZZ"
            'Me.maxModel.Text = "ZZZ"
            Me.maxSerialNo.Text = "ZZZ"
            'Me.maxstat.Text = "ZZZ"
            Me.mindate.Text = "01/01/2006"

            Me.maxdate.Text = "31/12/2006"
            Me.minCustomerid.Text = "AAA"
            'Me.minModel.Text = "AAA"
            Me.minSerialNo.Text = "AAA"
            'Me.minsrevice.Text = "AAA"
            'Me.minstat.Text = "AAA"
        ElseIf Session("RPTCUSRflag") = "1" Then
            datasearch()
        End If


        

        BindGrid()

        HypCalFrom.NavigateUrl = "javascript:DoCal(document.form1.mindate);"
        HypCalTo.NavigateUrl = "javascript:DoCal(document.form1.maxdate);"
    End Sub
    Protected Sub reportviewer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles reportviewer.Click
        Session("RPTCUSRflag") = "1"
        datasearch()
        'Response.Redirect("~/PresentationLayer/REPORT/RPTCUSRVIEW.aspx")
        'Dim script As String = "window.open('../report/RPTCUSRVIEW.aspx')"
        'Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "RPTCUSRVIEW", script, True)



        Dim script As String = "window.open('../report/RPTCUSRVIEW.aspx')"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "RPTCUSRVIEW", script, True)


        Return
    End Sub
    Protected Sub BindGrid()
        If Not Page.IsPostBack Then
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")

            'Label
            RacesLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0011")
            CustomerTypeLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0012")

            'List Customer type in drop down 
            Dim custtype As New clsrlconfirminf()
            Dim custParam As ArrayList = New ArrayList
            custParam = custtype.searchconfirminf("CUSTTYPE")
            Dim custcount As Integer
            Dim custid As String
            Dim custnm As String
            Me.CustomerTypeDLL.Items.Add(New ListItem("ALL", "ALL"))
            For custcount = 0 To custParam.Count - 1
                custid = custParam.Item(custcount)
                custnm = custParam.Item(custcount + 1)
                custcount = custcount + 1
                Me.CustomerTypeDLL.Items.Add(New ListItem(custnm.ToString(), custid.ToString()))

            Next

            'List Race in drop down 
            Dim race As New clsrlconfirminf()
            Dim raceParam As ArrayList = New ArrayList
            raceParam = race.searchconfirminf("PERSONRACE")
            Dim racecount As Integer
            Dim raceid As String
            Dim racenm As String
            Me.RacesDLL.Items.Add(New ListItem("ALL", "ALL"))
            For racecount = 0 To raceParam.Count - 1
                raceid = raceParam.Item(racecount)
                racenm = raceParam.Item(racecount + 1)
                racecount = racecount + 1
                'If Not raceid.Equals("ZNONAPPL") Then
                Me.RacesDLL.Items.Add(New ListItem(racenm.ToString(), raceid.ToString()))
                'End If
            Next

        End If
    End Sub

    Public Sub datasearch()
        Dim clsReport As New ClsCommonReport
        Dim clsrptcust As New ClsRptCUSR

        clsrptcust.CustType = CustomerTypeDLL.SelectedValue
        clsrptcust.CustRace = RacesDLL.SelectedValue

        mindate.Text = Request.Form("mindate")
        maxdate.Text = Request.Form("maxdate")

        clsrptcust.userid = Session("userID")
        'If (Trim(Me.minsrevice.Text) <> "AAA") Then

        clsrptcust.Minsvcid = minsrevice.SelectedValue

        'End If
        'If (Trim(Me.maxsrevice.Text) <> "ZZZ") Then

        clsrptcust.Maxsvcid = maxsrevice.SelectedValue
        'Else
        'clsrptcust.Maxsvcid = "ZZZZZZZZZZ"
        'End If

        If (Trim(Me.minCustomerid.Text) <> "AAA") Then

            clsrptcust.Mincustid = minCustomerid.Text.ToUpper()

        End If

        If (Trim(Me.maxCustomerid.Text) <> "ZZZ") Then

            clsrptcust.Maxcustid = maxCustomerid.Text.ToUpper()
        Else
            clsrptcust.Maxcustid = "ZZZZZZZZZZ"

        End If

        If (Trim(Me.minSerialNo.Text) <> "AAA") Then

            clsrptcust.Minserialno = minSerialNo.Text.ToUpper()

        End If

        If (Trim(Me.maxSerialNo.Text) <> "ZZZ") Then

            clsrptcust.Maxserialno = maxSerialNo.Text.ToUpper()
        Else
            clsrptcust.Maxserialno = "ZZZZZZZZZZ"

        End If

        If (Trim(Me.mindate.Text) <> "") Then
            If (Trim(Me.mindate.Text) <> "01/01/2006") Then

                Dim temparr As Array = mindate.Text.Split("/")
                clsrptcust.Mindate = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
            Else
                clsrptcust.Mindate = "2006-01-01"
            End If


        End If

        If (Trim(Me.maxdate.Text) <> "") Then
            If (Trim(Me.maxdate.Text) <> "31/12/2006") Then

                Dim temparr As Array = maxdate.Text.Split("/")
                clsrptcust.Maxdate = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
            Else
                clsrptcust.Maxdate = "2006-12-31"
            End If
        End If


        'If (Trim(Me.minModel.Text) <> "AAA") Then

        '    clsrptcust.Minmodel = minModel.Text.ToUpper()

        'End If
        clsrptcust.Minmodel = minmodel.SelectedValue
        clsrptcust.Maxmodel = maxModel.SelectedValue
        'If (Trim(Me.maxModel.Text) <> "ZZZ") Then

        '    clsrptcust.Maxmodel = maxModel.Text.ToUpper()
        'Else
        '    clsrptcust.Maxmodel = "ZZZZZZZZZZ"
        'End If
        'If (Trim(Me.minstat.Text) <> "AAA") Then

        clsrptcust.Minstat = minstat.SelectedValue

        'End If

        'If (Trim(Me.maxstat.Text) <> "ZZZ") Then

        clsrptcust.Maxstat = maxstat.SelectedValue
        clsrptcust.ClassID = ProductClass.SelectedValue
        'Else
        'clsrptcust.Maxstat = "ZZZZZZZZZZ"
        'End If
        If (Me.ContractType.SelectedItem.Value.ToString() <> "") Then
            clsrptcust.Contract = ContractType.SelectedItem.Value.ToString()
            clsrptcust.Contracttext = ContractType.SelectedItem.Text.ToString()
        End If
        Dim rank As String = Session("login_rank")
        If rank <> 0 Then
            clsrptcust.ctryid = Session("login_ctryID")
            clsrptcust.compid = Session("login_cmpID")
            clsrptcust.svcid = Session("login_svcID")

        End If
        clsrptcust.rank = rank

        clsrptcust.MinArea = minArea.SelectedValue
        clsrptcust.MaxArea = maxArea.SelectedValue


        'Dim rptcust As DataSet = clsrptcust.GetRptcusr()
        'Session("rptcust_ds") = rptcust
        Session("rptcust_search_condition") = clsrptcust


        'Dim objXmlTr As New clsXml
        'Dim strHead As Array = Array.CreateInstance(GetType(String), 9)
        'objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        'strHead(0) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCUSR-DATABASE")
        'strHead(1) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCUSR-CRRB")
        'strHead(2) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCUSR-REPORTID")
        'strHead(3) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCUSR-USERID")

        'strHead(4) = Session("userID") + Session("username")
        'strHead(5) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCUSR-TOTALBYR")
        'strHead(6) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCUSR-SCIH")
        'strHead(7) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDAR-TITLE1")
        'strHead(8) =  Session("login_cmpID")
        'If Me.IsPostBack Then

        '    With clsReport
        '        .ReportFileName = "RptCUSR.rpt"  'TextBox1.Text
        '        .SetReport(CrystalReportViewer1, rptcust, strHead)


        '    End With

        'End If

    End Sub

    'LW Added part : Ajax 
    <AjaxPro.AjaxMethod()> _
        Public Function GetRaces(ByVal strCustomerType As String) As ArrayList
        Dim race As New clsrlconfirminf()
        Dim raceParam As ArrayList = New ArrayList
        raceParam = race.searchconfirminf("PERSONRACE")
        Dim racecount As Integer
        Dim raceid As String
        Dim racenm As String
        Dim Listas As New ArrayList(raceParam.Count / 2)
        Dim icnt As Integer = 0
        Dim strTemp As String

        If strCustomerType = "CORPORATE" Then

            Listas.Add("ALL:ALL")
            For racecount = 0 To raceParam.Count - 1
                raceid = raceParam.Item(racecount)
                racenm = raceParam.Item(racecount + 1)
                racecount = racecount + 1
                If raceid.Equals("ZNONAPPL") Then
                    strTemp = raceid & ":" & racenm
                    Listas.Add(strTemp)
                End If

            Next
        ElseIf strCustomerType = "INDIVIDUAL" Then

            Listas.Add("ALL:ALL")
            For racecount = 0 To raceParam.Count - 1
                raceid = raceParam.Item(racecount)
                racenm = raceParam.Item(racecount + 1)
                racecount = racecount + 1
                If Not raceid.Equals("ZNONAPPL") Then
                    strTemp = raceid & ":" & racenm
                    Listas.Add(strTemp)
                End If

            Next
        Else
            Listas.Add("ALL:ALL")
            For racecount = 0 To raceParam.Count - 1
                raceid = raceParam.Item(racecount)
                racenm = raceParam.Item(racecount + 1)
                racecount = racecount + 1

                strTemp = raceid & ":" & racenm
                Listas.Add(strTemp)

            Next

        End If

        Return Listas
    End Function

    Protected Sub minsrevice_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles minsrevice.SelectedIndexChanged
        If Not minsrevice.SelectedValue = "%" Then
            clsrptcust.Minsvcid = minsrevice.SelectedValue
            clsrptcust.ctryid = Session("login_ctryID").ToString.ToUpper()
            Dim ds As DataSet = clsrptcust.getStateAndArea()

            minstat.Items.Clear()
            minArea.Items.Clear()
            Dim dt As DataTable
            Dim dt1 As DataTable
            'Dim dr As DataRow
            dt = ds.Tables(0)
            dt1 = ds.Tables(1)
            minstat.Items.Add(New ListItem(objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), "%"))
            For i As Integer = 0 To dt.Rows.Count - 1
                Dim statenm As String = dt.Rows(i).Item(0) + "-" + dt.Rows(i).Item(1)
                minstat.Items.Add(New ListItem(statenm, dt.Rows(i).Item(0)))
            Next
            minArea.Items.Add(New ListItem(objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), "%"))
            For i As Integer = 0 To dt1.Rows.Count - 1
                Dim areanm As String = dt1.Rows(i).Item(0) + "-" + dt1.Rows(i).Item(1)
                minArea.Items.Add(New ListItem(areanm, dt1.Rows(i).Item(0)))
            Next

            'stateid = dsCLA.Tables(0).Rows(i).Item(0)

        Else
            Dim strUserID As String = Session("userID").ToString
            clsrptcust.userid = strUserID
            Dim ds As DataSet = clsrptcust.SVCnStatebyUserID()
            minstat.Items.Clear()
            minArea.Items.Clear()
            minstat.Items.Add(New ListItem(objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), "%"))
            'stateTDropDownList.Items.Add(New ListItem("", ""))
            Dim stateid As String
            Dim statenm As String
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                statenm = ds.Tables(0).Rows(i).Item(0) + "-" + ds.Tables(0).Rows(i).Item(1)
                stateid = ds.Tables(0).Rows(i).Item(0)
                minstat.Items.Add(New ListItem(statenm.ToString(), stateid.ToString()))
                'stateTDropDownList.Items.Add(New ListItem(statenm.ToString(), stateid.ToString()))
            Next
            minArea.Items.Add(New ListItem(objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), "%"))
            'cboAreaIdTo.Items.Add(New ListItem(objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL")))
            Dim areaid As String
            Dim areanm As String
            For ii As Integer = 0 To ds.Tables(2).Rows.Count - 1
                areanm = ds.Tables(2).Rows(ii).Item(0) + "-" + ds.Tables(2).Rows(ii).Item(1)
                areaid = ds.Tables(2).Rows(ii).Item(0)
                minArea.Items.Add(New ListItem(areanm.ToString(), areaid.ToString()))
                'cboAreaIdTo.Items.Add(New ListItem(areanm.ToString(), areaid.ToString()))
            Next
        End If


    End Sub

    Protected Sub maxsrevice_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles maxsrevice.SelectedIndexChanged
        If Not maxsrevice.SelectedValue = "%" Then
            clsrptcust.Minsvcid = maxsrevice.SelectedValue
            clsrptcust.ctryid = Session("login_ctryID").ToString.ToUpper()
            Dim ds As DataSet = clsrptcust.getStateAndArea()
            maxstat.Items.Clear()
            maxArea.Items.Clear()
            Dim dt As DataTable
            Dim dt1 As DataTable
            'Dim dr As DataRow
            dt = ds.Tables(0)
            dt1 = ds.Tables(1)
            maxstat.Items.Add(New ListItem(objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), "%"))
            For i As Integer = 0 To dt.Rows.Count - 1
                Dim statenm As String = dt.Rows(i).Item(0) + "-" + dt.Rows(i).Item(1)
                maxstat.Items.Add(New ListItem(statenm, dt.Rows(i).Item(0)))
            Next
            maxArea.Items.Add(New ListItem(objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), "%"))
            For i As Integer = 0 To dt1.Rows.Count - 1
                Dim areanm As String = dt1.Rows(i).Item(0) + "-" + dt1.Rows(i).Item(1)
                maxArea.Items.Add(New ListItem(areanm, dt1.Rows(i).Item(0)))
            Next

        Else
            Dim strUserID As String = Session("userID").ToString
            clsrptcust.userid = strUserID
            Dim ds As DataSet = clsrptcust.SVCnStatebyUserID()
            maxstat.Items.Clear()
            maxArea.Items.Clear()
            'stateFDropDownList.Items.Add(New ListItem("", ""))
            maxstat.Items.Add(New ListItem(objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), "%"))
            Dim stateid As String
            Dim statenm As String
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                statenm = ds.Tables(0).Rows(i).Item(0) + "-" + ds.Tables(0).Rows(i).Item(1)
                stateid = ds.Tables(0).Rows(i).Item(0)
                'stateFDropDownList.Items.Add(New ListItem(statenm.ToString(), stateid.ToString()))
                maxstat.Items.Add(New ListItem(statenm.ToString(), stateid.ToString()))
            Next
            'areaFDropDownList.Items.Add(New ListItem(objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL")))
            maxArea.Items.Add(New ListItem(objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), "%"))
            Dim areaid As String
            Dim areanm As String
            For ii As Integer = 0 To ds.Tables(2).Rows.Count - 1
                areanm = ds.Tables(2).Rows(ii).Item(0) + "-" + ds.Tables(2).Rows(ii).Item(1)
                areaid = ds.Tables(2).Rows(ii).Item(0)
                'areaFDropDownList.Items.Add(New ListItem(areanm.ToString(), areaid.ToString()))
                maxArea.Items.Add(New ListItem(areanm.ToString(), areaid.ToString()))
            Next
        End If

    End Sub

    Protected Sub minstat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles minstat.SelectedIndexChanged
        minArea.Items.Clear()
        If Not minstat.SelectedValue = "%" Then
            clsrptcust.Minstat = minstat.SelectedValue
            clsrptcust.Minsvcid = minsrevice.SelectedValue
        Else
            clsrptcust.Minstat = "%"
            clsrptcust.Minsvcid = minsrevice.SelectedValue
            minArea.Items.Add(New ListItem(objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), "%"))
        End If

        Dim ds As DataSet = clsrptcust.getAreafromState()
        Dim dt As DataTable = ds.Tables(0)
        'Dim dr As DataRow
        Dim areanm As String
        minArea.Items.Add(New ListItem(objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), "%"))
        For i As Integer = 0 To dt.Rows.Count - 1
            areanm = dt.Rows(i).Item(0) + "-" + dt.Rows(i).Item(1)
            minArea.Items.Add(New ListItem(areanm, dt.Rows(i).Item(0)))
        Next

        'Dim areanm As String = ds.Tables(0).Rows(0).Item(0) + "-" + ds.Tables(0).Rows(0).Item(1)
        'areaFDropDownList.Items.Add(New ListItem(areanm, ds.Tables(0).Rows(0).Item(1)))
    End Sub

    Protected Sub maxstat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles maxstat.SelectedIndexChanged
        maxArea.Items.Clear()
        If Not maxstat.SelectedValue = "%" Then
            clsrptcust.Minstat = maxstat.SelectedValue
            clsrptcust.Minsvcid = maxsrevice.SelectedValue
        Else
            clsrptcust.Minstat = "%"
            clsrptcust.Minsvcid = maxsrevice.SelectedValue
            maxArea.Items.Add(New ListItem(objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), "%"))
        End If
        Dim ds As DataSet = clsrptcust.getAreafromState()

        Dim dt As DataTable = ds.Tables(0)
        'Dim dr As DataRow
        Dim areanm As String
        maxArea.Items.Add(New ListItem(objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), "%"))
        For i As Integer = 0 To dt.Rows.Count - 1
            areanm = dt.Rows(i).Item(0) + "-" + dt.Rows(i).Item(1)
            maxArea.Items.Add(New ListItem(areanm, dt.Rows(i).Item(0)))
        Next
    End Sub
  
    Protected Sub ProductClass_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ProductClass.SelectedIndexChanged
        minmodel.Items.Clear()
        maxModel.Items.Clear()
        clsCommon.ctryid = Session("login_ctryID").ToString.ToUpper()
        clsCommon.ClsID = ProductClass.SelectedValue
        Dim ds As DataSet = clsCommon.getModelfrmClass()
        If ds.Tables.Count <> 0 Then
            Dim row As DataRow
            minmodel.Items.Add(New ListItem(objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), ""))
            maxModel.Items.Add(New ListItem(objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), ""))
            For Each row In ds.Tables(0).Rows
                Dim NewItem As New ListItem()
                NewItem.Text = row("id") & "-" & row("name")
                NewItem.Value = row("id")
                minmodel.Items.Add(NewItem)
                maxModel.Items.Add(NewItem)
            Next
        End If
    End Sub
End Class
