
Imports BusinessEntity
Imports System.Data
Imports System


Namespace PresentationLayer.report
    Partial Class PresentationLayer_report_RPTINSTALL
        Inherits Page
        'Implements IBasicReportPage

#Region "Declaration"
        Private ReadOnly _xml As New clsXml
        Private ReadOnly _clsrptcust As New ClsRptCUSR
        Private ReadOnly _rptInstall As New ClsRptInstall
        Private ReadOnly _clsCommon As New clsCommonClass
        Private ReadOnly _datestyle As Globalization.CultureInfo = New Globalization.CultureInfo("en-CA")
#End Region

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
            Threading.Thread.CurrentThread.CurrentCulture = _datestyle

            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Const script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Const script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try

            If Not Page.IsPostBack Then
                PopulateLabels()
                DefineDefaultValues()
            End If

            HypCalFrom.NavigateUrl = "javascript:DoCal(document.form1.txtFromDate);"
            HypCalTo.NavigateUrl = "javascript:DoCal(document.form1.txtToDate);"
        End Sub

        private Sub PopulateLabels() 'Implements IBasicReportPage.PopulateLabels
            lblProdClass.Text = _xml.GetLabelName("EngLabelMsg", "BB-MASROUM-0010")
            lblModelID.Text = _xml.GetLabelName("EngLabelMsg", "BB-RPTSOSA-MODEL")
            lblInstallDate.Text = _xml.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0007")
            lblSvcCenter.Text = _xml.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-SERVCENTER")
            lblState.Text = _xml.GetLabelName("EngLabelMsg", "BB-MASROUM-0032")
            lblArea.Text = _xml.GetLabelName("EngLabelMsg", "BB-MASROUM-0033")
            titleLab.Text = _xml.GetLabelName("EngLabelMsg", "BB-INSTALLBASE-TITLE")
        End Sub

        Public Sub PopulateLabels(ByVal strHead As String()) 'Implements IBasicReportPage.PopulateLabels
            Throw New NotImplementedException()
        End Sub

        private Sub DefineDefaultValues() 'Implements IBasicReportPage.DefineDefaultValues
            _rptInstall.UserName = Session("userID").ToString()
            Dim dt As DataTable
            dt = _rptInstall.PopulateProductClass("PRODUCTCL")
            BindValuesToDropdown(dt, ddProdClass)

            _clsCommon.spctr = Session("login_ctryID").ToString()
            _clsCommon.spstat = ""
            _clsCommon.sparea = ""
            _clsCommon.rank = ""

            Dim modelds As New DataSet
            _clsCommon.spctr = Session("login_ctryID")
            _clsCommon.spstat = ""
            _clsCommon.sparea = ""
            _clsCommon.rank = ""
            modelds = _clsCommon.Getcomidname("BB_MASMOTY_IDNAME")
            If modelds.Tables.Count <> 0 Then
                Dim row As DataRow
                ddModelID.Items.Add(New ListItem(_xml.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), ""))
                ddModelIDTo.Items.Add(New ListItem(_xml.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), ""))
                For Each row In modelds.Tables(0).Rows
                    Dim NewItem As New ListItem()
                    NewItem.Text = row("id") & "-" & row("name")
                    NewItem.Value = row("id")
                    ddModelID.Items.Add(NewItem)
                    ddModelIDTo.Items.Add(NewItem)
                Next
            End If

            Dim custtype As New clsrlconfirminf()
            Dim custParam As ArrayList = New ArrayList
            custParam = custtype.searchconfirminf("CUSTTYPE")
            Dim custcount As Integer
            Dim custid As String
            Dim custnm As String
            Me.ddlcusttype.Items.Add("ALL")
            For custcount = 0 To custParam.Count - 1
                custid = custParam.Item(custcount)
                custnm = custParam.Item(custcount + 1)
                custcount = custcount + 1
                Me.ddlcusttype.Items.Add(New ListItem(custnm.ToString(), custid.ToString()))
            Next

            _clsrptcust.userid = Session("userID").ToString()
            Dim ds As DataSet = _clsrptcust.SVCnStatebyUserID()
            Dim dtSvcCtr As DataTable = ds.Tables(1)
            Dim dtState As DataTable = ds.Tables(0)
            Dim dtArea As DataTable = ds.Tables(2)
            Dim dtSvcCtrTo As DataTable = ds.Tables(1)
            Dim dtStateTo As DataTable = ds.Tables(0)
            Dim dtAreaTo As DataTable = ds.Tables(2)

            BindValuesToDropdown(dtSvcCtr, ddSVCCenter)
            BindValuesToDropdown(dtState, ddState)
            BindValuesToDropdown(dtArea, ddArea)
            BindValuesToDropdown(dtSvcCtrTo, ddSVCCenterTo)
            BindValuesToDropdown(dtStateTo, ddStateTo)
            BindValuesToDropdown(dtAreaTo, ddAreaTo)

            txtFromDate.Text = "01/" + DateTime.Today.ToString("MM") + "/" + DateTime.Today.Year.ToString()
            txtToDate.Text = CDate(txtFromDate.Text).AddMonths(1).ToShortDateString()
        End Sub

        Public Sub DefineDefaultValues(ByVal strHead As String()) 'Implements IBasicReportPage.DefineDefaultValues
            Throw New NotImplementedException()
        End Sub

        Public Sub GenerateReport(ByVal dataSet As DataSet) 'Implements IBasicReportPage.GenerateReport
            Throw New NotImplementedException()
        End Sub

        Private Sub GenerateReport() 'Implements IBasicReportPage.GenerateReport

            Dim startDate As DateTime
            DateTime.TryParse(txtFromDate.Text, startDate)
            Dim endDate As DateTime
            DateTime.TryParse(txtToDate.Text, endDate)

            _rptInstall.StartInstallDate = startDate
            _rptInstall.EndInstallDate = endDate
            _rptInstall.ProdClass = ddProdClass.SelectedValue
            _rptInstall.ModID = ddModelID.SelectedValue
            _rptInstall.SvcId = ddSVCCenter.SelectedValue
            _rptInstall.StateID = ddState.SelectedValue
            _rptInstall.AreaID = ddArea.SelectedValue
            _rptInstall.ModIDTo = ddModelIDTo.SelectedValue
            _rptInstall.SvcIdTo = ddSVCCenterTo.SelectedValue
            _rptInstall.StateIDTo = ddStateTo.SelectedValue
            _rptInstall.AreaIDTo = ddAreaTo.SelectedValue
            _rptInstall.UserName = Session("userID").ToString()
            _rptInstall.CustType = ddlcusttype.SelectedValue
            _rptInstall.Status = ddlstatus.SelectedValue

            '----------start---------------
            'added by LLY 
            Dim rank As String = Session("login_rank")
            If rank <> 0 Then
                _rptInstall.UserCtryid = Session("login_ctryID")
                _rptInstall.UserCompid = Session("login_cmpID")
                _rptInstall.UserSvcid = Session("login_svcID")

            End If
            _rptInstall.UserRank = rank
            '----------end---------------


            Session("InstallBase") = _rptInstall
        End Sub

        private Sub BindValuesToDropdown(ByVal dt As DataTable, ByVal dropdown As DropDownList)
            dropdown.Items.Clear()
            dropdown.Items.Add(New ListItem("All", ""))

            Dim row As DataRow
            If dt.Rows.Count > 0 Then
                'dropdown.Items.Add("")
                For Each row In dt.Rows
                    Dim NewItem As New ListItem()
                    NewItem.Text = Trim(row(0).ToString()) & "-" & row(1).ToString()
                    NewItem.Value = Trim(row(0).ToString())
                    dropdown.Items.Add(NewItem)
                Next
            End If
        End Sub

        Protected Sub ddSVCCenter_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
            Dim dtState As New DataTable
            Dim dtArea As New DataTable

            ddState.Items.Clear()
            ddArea.Items.Clear()

            If Not String.IsNullOrEmpty(ddSVCCenter.SelectedValue) Then
                _clsrptcust.Minsvcid = ddSVCCenter.SelectedValue
                _clsrptcust.ctryid = Session("login_ctryID").ToString().ToUpper()

                Dim ds As DataSet = _clsrptcust.getStateAndArea()
                dtState = ds.Tables(0)
                dtArea = ds.Tables(1)
            Else
                _clsrptcust.userid = Session("userID").ToString()

                Dim ds As DataSet = _clsrptcust.SVCnStatebyUserID()
                dtState = ds.Tables(0)
                dtArea = ds.Tables(2)
            End If

            BindValuesToDropdown(dtState, ddState)
            BindValuesToDropdown(dtArea, ddArea)

        End Sub

        Protected Sub reportViewer_Click(ByVal sender As Object, ByVal e As EventArgs) Handles reportviewer.Click
            GenerateReport()
            Dim script As String = "window.open('../report/RPTINSTALL_VIEW.aspx')"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "RPTINSTALL_VIEW", script, True)
        End Sub

        Protected Sub ddSVCCenterTo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddSVCCenterTo.SelectedIndexChanged
            Dim dtStateTo As New DataTable
            Dim dtAreaTo As New DataTable

            ddStateTo.Items.Clear()
            ddAreaTo.Items.Clear()

            If Not String.IsNullOrEmpty(ddSVCCenterTo.SelectedValue) Then
                _clsrptcust.Minsvcid = ddSVCCenterTo.SelectedValue
                _clsrptcust.ctryid = Session("login_ctryID").ToString().ToUpper()

                Dim ds As DataSet = _clsrptcust.getStateAndArea()
                dtStateTo = ds.Tables(0)
                dtAreaTo = ds.Tables(1)
            Else
                _clsrptcust.userid = Session("userID").ToString()

                Dim ds As DataSet = _clsrptcust.SVCnStatebyUserID()
                dtStateTo = ds.Tables(0)
                dtAreaTo = ds.Tables(2)
            End If

            BindValuesToDropdown(dtStateTo, ddStateTo)
            BindValuesToDropdown(dtAreaTo, ddAreaTo)
        End Sub

        Protected Sub ddProdClass_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddProdClass.SelectedIndexChanged
            ddModelID.Items.Clear()
            ddModelIDTo.Items.Clear()
            _clsCommon.ctryid = Session("login_ctryID").ToString.ToUpper()
            _clsCommon.ClsID = ddProdClass.SelectedValue
            Dim ds As DataSet = _clsCommon.getModelfrmClass()
            If ds.Tables.Count <> 0 Then
                Dim row As DataRow
                ddModelID.Items.Add(New ListItem(_xml.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), ""))
                ddModelIDTo.Items.Add(New ListItem(_xml.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), ""))
                For Each row In ds.Tables(0).Rows
                    Dim NewItem As New ListItem()
                    NewItem.Text = row("id") & "-" & row("name")
                    NewItem.Value = row("id")
                    ddModelID.Items.Add(NewItem)
                    ddModelIDTo.Items.Add(NewItem)
                Next
            End If
        End Sub
    End Class
End Namespace