﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="RPTServiceDueListng.aspx.vb" Inherits="PresentationLayer.report.PresentationLayer_report_RPTServiceDueListng" %>
<%@ Register TagPrefix="cc1" Namespace="JCalendar" Assembly="JCalendar" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Service Due Listing Report</title>
    <link href="../css/style.css" type="text/css" rel="stylesheet" />
    <script type="text/javascript" language="JavaScript" src="../js/common.js"></script>
    <style type="text/css">
    	.rowStyle {
    		background-color: #ffffff;
    	}
    	.labelStyle {
    		text-align: left;
    		width: 15%
    	}
    	.fromToStyle {
    		text-align: left;
    		width: 6%;
    	}
    	.dataStyle {
    		text-align: left;
    		width: 27%
    	}
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table id="countrytab" border="0" width="100%">
            <tr>
                <td style="width: 100%">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title1.gif" width="5" />
                            </td>
                            <td background="../graph/title_bg.gif" class="style2" width="98%">
                                <asp:Label ID="titleLab" runat="server" Text="Label" />
                            </td>
                            <td align="LEFT" background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title_2.gif" width="5" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100%">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" class="style2" width="98%" style="height: 14px">
                                
                                <asp:Label ID="errlab" runat="server" Text="Label" Visible="false" ForeColor="Red"/></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100%">
                    <table id="country" style="background-color: #b7e6e6; border: 0; padding:2; margin: 0; width: 100%">
                        <tr class="rowStyle">
                            <td class="labelStyle">
                                <asp:Label ID="lblSvcCenter" runat="server" Text="Service Center"/>
                            </td>
                            <td class="fromToStyle">
                                <asp:Label ID="lblFromSVC" runat="server"/>
                            </td>
                            <td class="dataStyle">
                                <asp:DropDownList ID="ddFromSVCCenter" 
                                                  runat="server"
                                                  AutoPostBack="True"
                                                  CssClass="textborder"
                                                  style="width:300px"
                                                  OnSelectedIndexChanged="ddSVCCenter_SelectedIndexChange"/>
                            </td>
                            <td class="fromToStyle">
                                <asp:Label ID="lblToSVC" runat="server"/>
                            </td>
                            <td class="dataStyle">
                                <asp:DropDownList ID="ddToSVCCenter" 
                                                  runat="server"
                                                  AutoPostBack="True"
                                                  CssClass="textborder"
                                                  style="width:300px"
                                                  OnSelectedIndexChanged="ddSVCCenter_SelectedIndexChange"/>
                            </td>
                        </tr>
                        <tr class="rowStyle">
                            <td class="labelStyle">
                                <asp:Label ID="lblState" runat="server" Text="State" />
                            </td>
                            <td class="fromToStyle">
                                <asp:Label ID="lblFromState" runat="server"/>
                            </td>
                            <td class="dataStyle">
                                <asp:DropDownList ID="ddFromState" 
                                                  runat="server" 
                                                  AutoPostBack="True"
                                                  CssClass="textborder"
                                                  style="width:200px"
                                                  OnSelectedIndexChanged="ddState_SelectedIndexChange"/>
                            </td>
                            <td class="fromToStyle">
                                <asp:Label runat="server" ID="lblToState"/>
                            </td>
                            <td class="dataStyle">
                                <asp:DropDownList ID="ddToState" 
                                                  runat="server" 
                                                  CssClass="textborder"
                                                  AutoPostBack="True"
                                                  style="width:200px"
                                                  OnSelectedIndexChanged="ddState_SelectedIndexChange"/>
                            </td>
                        </tr>
                        
                        <tr class="rowStyle">
                            <td class="labelStyle">
                                <asp:Label runat="server" ID="lblArea" Text="Area"/>
                            </td>
                            <td class="fromToStyle">
                                <asp:Label runat="server" ID="lblFromArea"/>
                            </td>
                            <td class="dataStyle">
                                <asp:DropDownList ID="ddFromArea" 
                                                  runat="server"
                                                  style="width:200px"
                                                  CssClass="textborder"/>
                            </td>
                            <td class="fromToStyle">
                                <asp:Label runat="server" 
                                           ID="lblToArea"/>
                            </td>
                            <td class="dataStyle">
                                <asp:DropDownList ID="ddToArea" 
                                                  runat="server"
                                                  style="width:200px"
                                                  CssClass="textborder"/>
                            </td>
                        </tr>
                        
                        <tr class="rowStyle">
                            <td class="labelStyle">
                                <asp:Label runat="server" ID="lblProduct" Text="Product Class"/>
                            </td>
                            <td class="fromToStyle">
                                <asp:Label runat="server" ID="lblFromProduct"/>
                            </td>
                            <td class="dataStyle">
                                <asp:DropDownList runat="server"
                                                  ID="ddFromProduct"
                                                  CssClass="textborder"
                                                  AutoPostBack="True"
                                                  OnSelectedIndexChanged="ddProduct_SelectedIndexChange"/>
                            </td>
                            <td class="fromToStyle">
                                <asp:Label runat="server" ID="lblToProduct"/>
                            </td>
                            <td class="dataStyle">
                                <asp:DropDownList runat="server"
                                                  ID="ddToProduct"
                                                  CssClass="textborder"
                                                  AutoPostBack="True"
                                                  OnSelectedIndexChanged="ddProduct_SelectedIndexChange"/>
                            </td>
                        </tr>
                        
                        <tr class="rowStyle">
                            <td class="labelStyle">
                                <asp:Label runat="server" ID="lblModelID" Text="Model"/>
                            </td>
                            <td class="fromToStyle">
                                <asp:Label runat="server" ID="lblFromModelID"/>
                            </td>
                            <td class="dataStyle">
                                <asp:DropDownList runat="server"
                                                  ID="ddFromModelID"
                                                  style="width:250px"
                                                  CssClass="textborder"/>
                            </td>
                            <td class="fromToStyle">
                                <asp:Label runat="server"
                                           ID="lblToModelID"/>
                            </td>
                            <td class="dataStyle">
                                <asp:DropDownList runat="server"
                                                  ID="ddToModelID"
                                                  style="width:250px"
                                                  CssClass="textborder"/>
                            </td>
                        </tr>
                        
                        <tr class="rowStyle">
                            <td class="labelStyle">
                                <asp:Label runat="server" ID="lblServiceDate" Text="Service Due Date"/>
                            </td>
                            <td class="fromToStyle">
                                <asp:Label runat="server" ID="lblFromServiceDate"/>
                            </td>
                            <td class="dataStyle">
                                <asp:TextBox ID="txtFromServiceDate" 
                                             MaxLength="10"
                                             CssClass="textborder"
                                             runat="server"
                                             ValidationGroup="ServiceDueGrp"/>
                                <asp:HyperLink ID="HypCalFrom" 
                                               runat="server" 
                                               ImageUrl="~/PresentationLayer/graph/calendar.gif" 
                                               ToolTip="Choose a Date" 
                                               Text="Choose a Date"/>
                                <cc1:JCalendar ID="JCalendar1" 
                                               runat="server" 
                                               ControlToAssign="txtFromServiceDate" 
                                               ImgURL="~/PresentationLayer/graph/calendar.gif" 
                                               Visible="false" />
                                <asp:RequiredFieldValidator ID="reqValFromSvcDate" 
                                                            runat="server" 
                                                            ControlToValidate="txtFromServiceDate"
                                                            ErrorMessage=""
                                                            Display="Dynamic"
                                                            Text="invalid"
                                                            ValidationGroup="ServiceDueGrp"/>
                                <asp:RegularExpressionValidator ID="regValFromSvcDate"
                                                                runat="server" 
                                                                ControlToValidate="txtFromServiceDate"
                                                                ErrorMessage=""
                                                                Display="Dynamic"
                                                                ValidationExpression="(((0[1-9]|[12][0-9]|3[01])([-./])(0[13578]|10|12)([-./])(\d{4}))|(([0][1-9]|[12][0-9]|30)([-./])(0[469]|11)([-./])(\d{4}))|((0[1-9]|1[0-9]|2[0-8])([-./])(02)([-./])(\d{4}))|((29)(\.|-|\/)(02)([-./])([02468][048]00))|((29)([-./])(02)([-./])([13579][26]00))|((29)([-./])(02)([-./])([0-9][0-9][0][48]))|((29)([-./])(02)([-./])([0-9][0-9][2468][048]))|((29)([-./])(02)([-./])([0-9][0-9][13579][26])))"
                                                                ValidationGroup="ServiceDueGrp"
                                                                Text="invalid"/>
                                <asp:CompareValidator ID="compValFromSvcDate" 
                                                      runat="server" 
                                                      ValidationGroup="ServiceDueGrp"
                                                      ErrorMessage=""
                                                      Text="invalid"
                                                      Display="Dynamic"
                                                      ControlToValidate="txtFromServiceDate"
                                                      ControlToCompare="txtToServiceDate"
                                                      Operator="LessThanEqual" Type="Date"/>
                            </td>
                            <td class="fromToStyle">
                                <asp:Label runat="server" ID="lblToServiceDate"/>
                            </td>
                            <td class="dataStyle">
                                <asp:TextBox ID="txtToServiceDate" 
                                             MaxLength="10"
                                             CssClass="textborder"
                                             ValidationGroup="ServiceDueGrp"
                                             runat="server"/>
                                <asp:HyperLink ID="HypCalTo" 
                                               runat="server" 
                                               ImageUrl="~/PresentationLayer/graph/calendar.gif" 
                                               ToolTip="Choose a Date" 
                                               Text="Choose a Date"/>
                                <cc1:JCalendar ID="JCalendar2" 
                                               runat="server" 
                                               ControlToAssign="txtToServiceDate" 
                                               ImgURL="~/PresentationLayer/graph/calendar.gif" 
                                               Visible="false" />
                                <asp:RequiredFieldValidator ID="reqValToSvcDate" 
                                                            runat="server" 
                                                            ControlToValidate="txtToServiceDate"
                                                            ErrorMessage=""
                                                            ValidationGroup="ServiceDueGrp"
                                                            Text="invalid"
                                                            Display="Dynamic"/>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" 
                                                                runat="server" 
                                                                ControlToValidate="txtToServiceDate"
                                                                ErrorMessage=""
                                                                Text="invalid"
                                                                ValidationExpression="(((0[1-9]|[12][0-9]|3[01])([-./])(0[13578]|10|12)([-./])(\d{4}))|(([0][1-9]|[12][0-9]|30)([-./])(0[469]|11)([-./])(\d{4}))|((0[1-9]|1[0-9]|2[0-8])([-./])(02)([-./])(\d{4}))|((29)(\.|-|\/)(02)([-./])([02468][048]00))|((29)([-./])(02)([-./])([13579][26]00))|((29)([-./])(02)([-./])([0-9][0-9][0][48]))|((29)([-./])(02)([-./])([0-9][0-9][2468][048]))|((29)([-./])(02)([-./])([0-9][0-9][13579][26])))"
                                                                Display="Dynamic"
                                                                ValidationGroup="ServiceDueGrp"/>
                            </td>
                        </tr>
                        <tr class="rowStyle">
                            <td class="labelStyle">
                                Customer Type</td>
                            <td class="fromToStyle">
                            </td>
                            <td class="dataStyle">
                                <asp:DropDownList ID="ddlcusttype" 
                                                  runat="server"
                                                  AutoPostBack="True"
                                                  CssClass="textborder"
                                                  style="width:300px"
                                                  OnSelectedIndexChanged="ddSVCCenter_SelectedIndexChange"/></td>
                            <td class="fromToStyle">
                            </td>
                            <td class="dataStyle">
                            </td>
                        </tr>
                    </table>
                    <asp:LinkButton ID="reportviewer" runat="server" Text="ViewReport" OnClick="reportViewer_Click" ValidationGroup="ServiceDueGrp"/>
                    <br />
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
                        ShowMessageBox="True" ValidationGroup="ServiceDueGrp" />
                    &nbsp;<br />
                    <br />
                    <br />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>