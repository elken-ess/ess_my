<%@ Page Language="VB" AutoEventWireup="false" CodeFile="RPTFOCParts.aspx.vb" Inherits="PresentationLayer_report_RPTFOCParts" %>

<%@ Register Assembly="JCalendar" Namespace="JCalendar" TagPrefix="cc1" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>FOC Parts Report</title>
    <link href="../css/style.css" type="text/css" rel="stylesheet" />
    <script type="text/javascript" language="JavaScript" src="../js/common.js"></script>
    <style type="text/css">
    	.rowStyle {
    		background-color: #ffffff;
    	}
    	.labelStyle {
    		text-align: left;
    		width: 15%
    	}
    	.fromToStyle {
    		text-align: left;
    		width: 6%;
    	}
    	.dataStyle {
    		text-align: left;
    		width: 27%
    	}
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <table id="PRL" border="0" style="width: 100%">
            <tr>
                <td style="width: 100%">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title1.gif" width="5" /></td>
                            <td background="../graph/title_bg.gif" class="style2" width="98%">
                                <asp:Label ID="titleLab" runat="server" Text="FOC Parts Report"></asp:Label></td>
                            <td align="right" background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title_2.gif" width="5" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" class="style2" width="98%">
                                <font color="red">
                                    <asp:Label ID="errlab" runat="server" Text="Label" Visible="false"></asp:Label></font></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100%;">
                    <table id="crn" bgcolor="#b7e6e6" border="0" cellpadding="2" cellspacing="1" style="width: 100%">
                        <tr bgcolor="#ffffff" class="rowStyle">
                            <td class="labelStyle">
                                <asp:Label ID="lblServBillDate" runat="server" Text="Service Bill Date"></asp:Label></td>
                            <td class="fromToStyle">
                                <asp:Label ID="lblServBillDateFr" runat="server" Text="From" /></td>
                            <td class="dataStyle">
                                <asp:TextBox ID="txtServBillDateFr" runat="server" CssClass="textborder" ReadOnly="True"/>
                                <asp:HyperLink ID="HypCalFr" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                                    ToolTip="Choose a Date" Visible="False">Choose a Date</asp:HyperLink>
                                <cc1:jcalendar id="calServBillDateFr" runat="server" controltoassign="txtServBillDateFr"
                                    imgurl="~/PresentationLayer/graph/calendar.gif"/>
                            </td>
                            <td class="fromToStyle">
                                <asp:Label ID="lblServBillDateTo" runat="server" Text="To" /></td>
                            <td class="dataStyle">
                                <asp:TextBox ID="txtServBillDateTo" runat="server" CssClass="textborder" ReadOnly="True"/>&nbsp;
                                <asp:HyperLink ID="HypCalTo" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                                    ToolTip="Choose a Date" Visible="False">Choose a Date</asp:HyperLink>
                                <cc1:jcalendar id="calServBillDateTo" runat="server" controltoassign="txtServBillDateTo"
                                    imgurl="~/PresentationLayer/graph/calendar.gif"/>
                                &nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td class="labelStyle">
                                <asp:Label ID="lblServCtr" runat="server" Text="Service Center" /></td>
                            <td class="fromToStyle">
                                <asp:Label ID="lblServCtrFr" runat="server" Text="From" /></td>
                            <td class="dataStyle">
                                <asp:DropDownList runat="server" ID="ddSVCCenter" Width="41%" CssClass="textborder" 
                                    AutoPostBack="True">
                                </asp:DropDownList></td>
                            <td class="fromToStyle">
                                <asp:Label ID="lblServCtrTo" runat="server" Text="To" /></td>
                            <td class="dataStyle">
                                <asp:DropDownList ID="ddSVCCenterTo" runat="server" AutoPostBack="True" CssClass="textborder"
                                    Width="41%">
                                </asp:DropDownList></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td class="labelStyle">
                                <asp:Label ID="lblTechID" runat="server" Text="Technician ID/Name" /></td>
                            <td class="fromToStyle">
                                <asp:Label ID="lblTechIDFr" runat="server" Text="From" /></td>
                            <td class="dataStyle">
                                <asp:DropDownList runat="server" ID="ddTechIDFr" Width="41%" CssClass="textborder" 
                                    AutoPostBack="True">
                                </asp:DropDownList></td>
                            <td class="fromToStyle">
                                <asp:Label ID="lblTechIDTo" runat="server" Text="To" /></td>
                            <td class="dataStyle">
                                <asp:DropDownList runat="server" ID="ddTechIDTo" Width="41%" CssClass="textborder" 
                                    AutoPostBack="True">
                                </asp:DropDownList></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td class="labelStyle">
                                <asp:Label ID="lblPartCode" runat="server" Text="Part Code"></asp:Label></td>
                            <td class="fromToStyle">
                                <asp:Label ID="lblPartCodeFr" runat="server" Text="From"></asp:Label></td>
                            <td class="dataStyle">
                                <asp:TextBox ID="txtPartCodeFr" runat="server" CssClass="textborder" MaxLength="10"></asp:TextBox></td>
                            <td class="fromToStyle">
                                <asp:Label ID="lblPartCodeTo" runat="server" Text="To"></asp:Label></td>
                            <td class="dataStyle">
                                <asp:TextBox ID="txtPartCodeTo" runat="server" CssClass="textborder" MaxLength="10"></asp:TextBox></td>
                        </tr>
                        
                       
                        <tr bgcolor="#ffffff">
                           <td class="labelStyle">
                               <asp:Label ID="lblPriceId2" runat="server" Text="Price ID"></asp:Label></td>
                           <td class="fromToStyle" >
                               </td>
                            <td class="dataStyle">
                               <table width="100%" height="200" align="left" >
                                    <tr><td><asp:Label ID="Label4" runat="server" Text="Available Price List"></asp:Label></td></tr>
                                    <tr><td><asp:ListBox ID="lbPriceId" runat="server" SelectionMode="Multiple" Height="160px" Width="60%"></asp:ListBox></td></tr>
                                    <tr><td><asp:Label ID="Label1" runat="server" Text="*NOTE:Shift+Click to select multiple items." Font-Size="X-Small"></asp:Label></td></tr>
                               </table>
                            </td>
                               
                             <td class="fromToStyle">
                                <table width="100%" height="200" align="left" >
                                    <tr><td align="center" valign="bottom"><asp:Button ID="btnToLeft" runat="server" Text=">>" /></td></tr>
                                    <tr><td align="center" valign="top"><asp:Button ID="btnToRight" runat="server" Text="<<" /></td></tr>
                                </table>
                              </td>   
                              <td class="dataStyle">
                                 <table width="100%" height="200" align="left" >
                                    <tr><td><asp:Label ID="Label3" runat="server" Text="Selected Price List"></asp:Label></td></tr>
                                    <tr><td><asp:ListBox ID="lbSelectedPriceId" runat="server" SelectionMode="Multiple" Height="160px" Width="60%"></asp:ListBox></td></tr>
                                    <tr><td><asp:Label ID="Label2" runat="server" Text=""></asp:Label></td></tr>
                               </table>
                              </td>
                        </tr>
                                
                        <tr bgcolor="#ffffff">
                            <td class="labelStyle">
                                <asp:Label ID="lblServType" runat="server" Text="Service Type"></asp:Label></td>
                            <td colspan="4" class="dataStyle">
                                <asp:DropDownList ID="ddlServType" runat="server" Width="42%">
                                </asp:DropDownList></td>
                        </tr>
                    </table>
                    <asp:LinkButton ID="LinkReport" runat="server">View Report</asp:LinkButton>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
