<%@ Page Language="VB" AutoEventWireup="false" CodeFile="RPTdebtor.aspx.vb" Inherits="PresentationLayer_report_RPTdebtor" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="JCalendar" Namespace="JCalendar" TagPrefix="cc1" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Debtor Report</title>
   <link href="../css/style.css" type="text/css" rel="stylesheet">
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="JavaScript" src="../js/common.js"></script>
    <style type="text/css">
    	.rowStyle {
    		background-color: #ffffff;
    	}
    	.labelStyle {
    		text-align: left;
    		width: 26%
    	}
    	.fromToStyle {
    		text-align: left;
    		width: 7%;
    	}
    	.dataStyle {
    		text-align: left;
    		width: 30%
    	}
    	.countryStyle {
    		border: 0px;
    		width: 100%
    	}
    	.tableStyle {
    		border: 0px;
    		padding: 0px;
    		margin: 0px;
    		width: 100%;
    	}
    </style>
    <link href="/aspnet_client/system_web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
</head>
<body>
     <form id="form1" runat="server">
    <div>
        <table id="countrytab" class="countryStyle">
            <tr>
                <td style="width: 100%">
                    <table class="tableStyle">
                        <tr>
                            <td background="../graph/title_bg.gif" style="width: 1%">
                                <img height="24" src="../graph/title1.gif" width="5" />
                            </td>
                            <td background="../graph/title_bg.gif" class="style2" width="98%" style="width: 100%">
                                <asp:Label ID="titleLab" runat="server" Text="Debtor Report"></asp:Label>
                            </td>
                            <td align="LEFT" background="../graph/title_bg.gif" width="1%">
                                <img height="1" src="../graph/title_2.gif" width="5" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100%; height: 1px;">
                    <table id="country" bgcolor="#b7e6e6" border="0" cellpadding="2" cellspacing="1"
                        style="width: 100%">
                        
                          <tr class="rowStyle">
                                   
                            <td class="labelStyle" style="height: 26px">
                                &nbsp;Report Type</td>
                            <td class="fromToStyle" style="height: 26px">
                                &nbsp;</td>
                            <td class="dataStyle" style="height: 26px">
                                <asp:DropDownList ID="ReportDropdown" runat="server"  Width="325px" AutoPostBack="True"/></td>
                            <td class="fromToStyle" style="height: 26px">
                                &nbsp;</td>
                            <td class="dataStyle" style="height: 26px">
                               </td>
                          </tr>
                    
                    
                        <tr class="rowStyle">
                            <td class="labelStyle">
                                &nbsp;Invoice Date</td>
                            <td class="fromToStyle">
                                &nbsp;From</td>
                            <td class="dataStyle">
                                <asp:TextBox ID="FromDateTxt" 
                                             runat="server" 
                                             CssClass="textborder" 
                                             ReadOnly="false"
                                             MaxLength="10">DD/MM/YYYY</asp:TextBox>
                                <asp:HyperLink ID="HypCalFrom" 
                                               runat="server" 
                                               ImageUrl="~/PresentationLayer/graph/calendar.gif"
                                               ToolTip="Choose a Date" Visible="False">Choose a Date</asp:HyperLink>
                                <cc1:JCalendar ID="JCalendar1" 
                                               runat="server" 
                                               ControlToAssign="FromDateTxt" 
                                               ImgURL="~/PresentationLayer/graph/calendar.gif" /></td>
                            <td class="fromToStyle">
                                &nbsp;To</td>
                            <td class="dataStyle">
                                <asp:TextBox ID="ToDateTxt" 
                                             runat="server" 
                                             CssClass="textborder" 
                                             ReadOnly="false"
                                             MaxLength="10">DD/MM/YYYY</asp:TextBox>
                                <asp:HyperLink ID="HypCalTo" 
                                               runat="server" 
                                               ImageUrl="~/PresentationLayer/graph/calendar.gif"
                                               ToolTip="Choose a Date" Visible="False">Choose a Date</asp:HyperLink>
                                <cc1:JCalendar ID="JCalendar2" 
                                               runat="server" 
                                               ControlToAssign="ToDateTxt" 
                                               ImgURL="~/PresentationLayer/graph/calendar.gif" /></td>
                        </tr>
                        <tr class="rowStyle">
                            <td class="labelStyle">
                                &nbsp;Service Center</td>
                            <td class="fromToStyle">
                                &nbsp;From</td>
                            <td class="dataStyle">
                                <asp:DropDownList ID="SVCFromDDL" runat="server" AutoPostBack="True" Width="325px">
                                </asp:DropDownList></td>
                            <td class="fromToStyle">
                                &nbsp;To</td>
                            <td class="dataStyle">
                                <asp:DropDownList ID="SVCToDDL" runat="server" AutoPostBack="True" Width="325px">
                                </asp:DropDownList></td>
                        </tr>
                        <tr class="rowStyle">
                            <td class="labelStyle" style="height: 26px">
                                &nbsp;Customer Type</td>
                            <td class="fromToStyle" style="height: 26px">
                                &nbsp;</td>
                            <td class="dataStyle" style="height: 26px">
                                <asp:DropDownList ID="CustomerTypeDDL" runat="server" Width="325px">
                                </asp:DropDownList></td>
                            <td class="fromToStyle" style="height: 26px">
                                &nbsp;</td>
                            <td class="dataStyle" style="height: 26px">
                               </td>
                        </tr>
                      
                       <tr class="rowStyle">
                            <td class="labelStyle" style="height: 26px">
                                &nbsp;Ageing</td>
                            <td class="fromToStyle" style="height: 26px">
                                &nbsp;</td>
                            <td class="dataStyle" style="height: 26px">
                                <asp:DropDownList ID="AgeingDDL" runat="server" Width="325px">
                                </asp:DropDownList></td>
                            <td class="fromToStyle" style="height: 26px">
                                &nbsp;</td>
                            <td class="dataStyle" style="height: 26px">
                               </td>
                        </tr>
                        
                        
                          <tr>
                              <td colspan="6" style="background-color: #ffffff">
                                        <asp:LinkButton ID="lnkViewReport" runat="server" Text="ViewReport" OnClick="LnkViewReportClick" />
                              </td>
                          </tr>
                          <tr>
                              <td colspan="6" style="background-color: #ffffff">
                                        <asp:LinkButton ID="lnkDebtorList" runat="server" Text="Reminder Letter List" OnClick="LnkViewDebtorListClick" />
                              </td>
                          </tr>
                    </table>
                    <asp:Button ID="CheckAll" runat="server" Text="Check All" />
 
                    <asp:Button ID="UncheckAll" runat="server" Text="Uncheck All" />
                    <asp:GridView ID="grdAgeingDetails" runat="server" AutoGenerateColumns="False" DataKeyNames="custID"
                        ShowFooter="True" Width="100%">
                        <RowStyle HorizontalAlign="Center" />
                        <Columns>
                            <asp:TemplateField HeaderText="No">
                                <ItemTemplate>
                                    <asp:Label ID="lblNo" runat="server" Text=''></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Customer ID">
                                <ItemTemplate>
                                    <asp:Label ID="lblcustID" runat="server" Text='<%#Eval("custID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Customer Name">
                                <ItemTemplate>
                                     <asp:Label ID="lblcustName" runat="server" Text='<%#Eval("custName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Amount">
                                <ItemTemplate>
                                    <asp:Label ID="lblAmt" runat="server" Text='<%#Eval("Amt") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Print">
                                <ItemTemplate>
                                    <asp:CheckBox ID="cbPrint" runat="server" />
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:LinkButton ID="lnkBtnPrint" runat="server" CommandName="print" CausesValidation="false">Print</asp:LinkButton>   
                                </FooterTemplate>
                                <FooterStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="TrnNo">
                                <ItemTemplate>
                                    <asp:Label ID="lblTrnno" runat="server" Text='<%#Eval("trnno") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    
                      <asp:Label ID="lblNoRecord" runat="server" ForeColor="Red" Text="Label"></asp:Label>
                     <CR:CrystalReportViewer ID="CRVReminderLetter" runat="server" AutoDataBind="true" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
