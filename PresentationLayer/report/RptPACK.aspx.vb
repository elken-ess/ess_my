Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Web
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web
Imports BusinessEntity
Imports System.Data
Partial Class PresentationLayer_Report_RptPACK
    Inherits System.Web.UI.Page
    Dim fstrMonth As String = ""
    Dim fstrDefaultStartDate As String = ""
    Dim fstrDefaultEndDate As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        fstrMonth = Month(System.DateTime.Today)
        fstrDefaultStartDate = System.DateTime.Today
        fstrDefaultEndDate = fstrDefaultStartDate 'DateAdd(DateInterval.Month, 1, CDate(fstrDefaultStartDate))

        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try

        If Not Page.IsPostBack Then
            ''display label message
            Session("Packflag") = "0"
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            Statelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTPACK-0001")
            Sercenterlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTPACK-0002")

            SubDatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTPACK-0003")
            TechnicianIDlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTPACK-0004")

            Me.lblPartCode.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_F_PARTCODE")

            Fromlab1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Fromlab2.Text = Fromlab1.Text
            Fromlab3.Text = Fromlab1.Text
            Fromlab4.Text = Fromlab1.Text
            Fromlab5.Text = Fromlab1.Text

            Tolab1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Tolab2.Text = Tolab1.Text
            Tolab3.Text = Tolab1.Text
            Tolab4.Text = Tolab1.Text
            Tolab5.Text = Tolab1.Text

            reportviewer.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-VIEWREPORT")
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTPACK-titlelab")

            Me.mindate.Text = fstrDefaultStartDate
            Me.maxdate.Text = fstrDefaultEndDate

            '''access control'''''''''''''''''''''''''''''''''''''''''''''''
            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "47")
            Me.reportviewer.Enabled = purviewArray(3)

        ElseIf Session("Packflag") = "1" Then
            viewreport()
        End If


        HypCalFrom.NavigateUrl = "javascript:DoCal(document.form1.mindate);"
        HypCalTo.NavigateUrl = "javascript:DoCal(document.form1.maxdate);"
    End Sub
#Region " view report message"
    Public Function viewreport()
        'set params'
        Dim packEntity As New ClsRptPack

        mindate.Text = Request.Form("mindate")
        maxdate.Text = Request.Form("maxdate")

        If Trim(minstate.Text).ToUpper = "AAA" Then
            packEntity.statemin = ""
        Else
            packEntity.statemin = Trim(minstate.Text)
        End If
       
        'packEntity.stateend = Trim(maxstate.Text)
        If Trim(minsercenter.Text) = "AAA" Then
            packEntity.sercidmin = ""
        Else
            packEntity.sercidmin = Trim(minsercenter.Text)
        End If

        'packEntity.sercidend = Trim(maxsercenter.Text)
        If Trim(mindate.Text) <> "" Then
            Dim temparr As Array = mindate.Text.Split("/")
            packEntity.datemin = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
        Else
            Dim mind As String = "01/01/1910"
            Dim temparr As Array = mind.Split("/")
            packEntity.datemin = temparr(2) + "-" + temparr(1) + "-" + temparr(0)
        End If
        If Trim(maxdate.Text) <> "" Then
            Dim temparr2 As Array = maxdate.Text.Split("/")
            packEntity.dateend = temparr2(2) + "-" + temparr2(1) + "-" + temparr2(0)
        Else
            Dim maxd As String = "31/12/2089"
            Dim temparr2 As Array = maxd.Split("/")
            packEntity.dateend = temparr2(2) + "-" + temparr2(1) + "-" + temparr2(0)
        End If
        If Trim(mintechnician.Text) = "AAA" Then
            packEntity.tecnicianidmin = ""
        Else
            packEntity.tecnicianidmin = Trim(mintechnician.Text)
        End If

        'packEntity.tecnicianidend = Trim(maxtechnician.Text)
        If Trim(maxstate.Text) = "ZZZ" Then
            packEntity.stateend = "ZZZZZZZZZZZ"
        Else
            packEntity.stateend = Trim(maxstate.Text).ToUpper.ToString()
        End If
        If Trim(maxsercenter.Text) = "ZZZ" Then
            packEntity.sercidend = "ZZZZZZZZZZZ"
        Else
            packEntity.sercidend = Trim(maxsercenter.Text).ToUpper.ToString()
        End If
        If Trim(maxtechnician.Text) = "ZZZ" Then
            packEntity.tecnicianidend = "ZZZZZZZZZZZ"
        Else
            packEntity.tecnicianidend = Trim(maxtechnician.Text).ToUpper.ToString()
        End If


        If Trim(txtPartCodeFrom.Text) = "AAA" Then
            packEntity.PartCodeFrom = ""
        Else
            packEntity.PartCodeFrom = Me.txtPartCodeFrom.Text.Trim.ToUpper
        End If

        If Trim(txtPartCodeTo.Text) = "ZZZ" Then
            packEntity.PartCodeTo = "ZZZZZZZZZZZ"
        Else
            packEntity.PartCodeTo = Me.txtPartCodeTo.Text.Trim.ToUpper
        End If






        'Dim days As Integer = -270
        'mdslEntity.instdatemin270 = DateAdd(DateInterval.Day, days, Convert.ToDateTime(mdslEntity.datebegin))
        'mdslEntity.instdatemax270 = DateAdd(DateInterval.Day, days, Convert.ToDateTime(mdslEntity.dateend))

        If Trim(Session("username")) = "" Then
            packEntity.ModifiedBy = "ADMIN"
        Else
            packEntity.ModifiedBy = Session("username")
        End If
        packEntity.logrank = Session("login_rank")
        Dim login_rank As Integer = Session("login_rank")
        If login_rank <> 0 Then
            Select Case (login_rank)
                Case 9
                    packEntity.logctrid = Session("login_ctryID").ToString.ToUpper
                    packEntity.logcompanyid = Session("login_cmpID").ToString.ToUpper
                    packEntity.logserviceid = Session("login_svcID").ToString.ToUpper
                Case 8
                    packEntity.logctrid = Session("login_ctryID").ToString.ToUpper
                    packEntity.logcompanyid = Session("login_cmpID").ToString.ToUpper
                Case 7
                    packEntity.logctrid = Session("login_ctryID").ToString.ToUpper
            End Select
        End If


        'Dim ds As DataSet = packEntity.GetPack()
      

        'Session("rptpack_ds") = ds
        Session("rptpack_search_condition") = packEntity

    End Function
#End Region

    Protected Sub reportviewer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles reportviewer.Click
        Session("Packflag") = "1"
        viewreport()
        'Response.Redirect("~/PresentationLayer/REPORT/RptPACKSearch.aspx")
        Dim script As String = "window.open('../report/RptPACKSearch.aspx')"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "RptPACKSearch", script, True)
        Return
    End Sub

    Protected Sub Calendar1_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendar1.SelectionChanged
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        mindate.Text = Calendar1.SelectedDate.Date.ToString().Substring(0, 10)
        Calendar1.Visible = False
        Session("Packflag") = "0"
    End Sub

    Protected Sub Calendar2_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendar2.SelectionChanged
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        maxdate.Text = Calendar2.SelectedDate.Date.ToString().Substring(0, 10)
        Calendar2.Visible = False
        Session("Packflag") = "0"
    End Sub
    
End Class
