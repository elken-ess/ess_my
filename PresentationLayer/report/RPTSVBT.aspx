﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="RPTSVBT.aspx.vb" Inherits="PresentationLayer_Report_SerBillTech" %>

<%@ Register Assembly="JCalendar" Namespace="JCalendar" TagPrefix="cc1" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Service Bill Collection</title>
     <link href="../css/style.css" rel="stylesheet" type="text/css" />
     <STYLE type="text/css">A:link { COLOR: #336666; TEXT-DECORATION: none }
	BODY { FONT-SIZE: 9pt; FONT-FAMILY: "Arial", "Verdana", "Tahoma"; BACKGROUND-COLOR: #ffffff }
	TD { FONT-SIZE: 9pt; FONT-FAMILY: Arial,Verdana,Tahoma }
	</STYLE>
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
 
 <script language="JavaScript" src="../js/common.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
     <table id="countrytab" border="0" style="width: 100%">
            <tr>
                <td>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title1.gif" width="5" /></td>
                            <td background="../graph/title_bg.gif" class="style2" width="98%">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title_2.gif" width="5" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table id="country" bgcolor="#b7e6e6" border="0" cellpadding="2" cellspacing="1" style="width: 100%">
                        <tr bgcolor="#ffffff">
                            <td align="left" style="width: 27%; height: 28px;">
                                <asp:Label ID="stalab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 13%; height: 28px;">
                                <asp:Label ID="from1" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 34%; height: 28px;">
                                <asp:TextBox ID="stat1" runat="server" CssClass="textborder" Width="160px">AAA</asp:TextBox></td>
                            <td align="left" style="width: 12%; height: 28px;">
                                <asp:Label ID="to1" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 37%; height: 28px">
                                <asp:TextBox ID="stat2" runat="server" CssClass="textborder" Width="152px">ZZZ</asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="left" style="width: 27%; height: 28px">
                                <asp:Label ID="sercelab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 13%; height: 28px">
                                <asp:Label ID="from6" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 34%; height: 28px">
                                <asp:TextBox ID="serce1" runat="server" CssClass="textborder" Width="160px">AAA</asp:TextBox></td>
                            <td align="left" style="width: 12%; height: 28px">
                                <asp:Label ID="to6" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 37%; height: 28px">
                                <asp:TextBox ID="serce2" runat="server" CssClass="textborder" Width="152px">ZZZ</asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="left" style="width: 27%; height: 28px;">
                                <asp:Label ID="invdtlab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 13%; height: 28px;">
                                <asp:Label ID="from2" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 34%; height: 28px;">
                                <asp:TextBox ID="invdate1" runat="server" CssClass="textborder" style="width: 50%" ReadOnly="false"  MaxLength =10></asp:TextBox>&nbsp;
                                
                                
                                <asp:HyperLink ID="HypCalinvdate1" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif" ToolTip="Choose a Date">Choose a Date</asp:HyperLink>

                                
                                <cc1:JCalendar
                                    ID="JCalendar1" runat="server" ControlToAssign="invdate1" ImgURL="~/PresentationLayer/graph/calendar.gif"  Visible =false />
                                &nbsp;&nbsp;
                            </td>
                            <td align="left" style="width: 12%; height: 28px;">
                                <asp:Label ID="to2" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 37%; height: 28px">
                                <asp:TextBox ID="invdate2" runat="server" CssClass="textborder" ReadOnly="false"  MaxLength =10 style="width: 75%"></asp:TextBox>&nbsp;
                                
                                <asp:HyperLink ID="HypCalinvdate2" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif" ToolTip="Choose a Date">Choose a Date</asp:HyperLink>

                                
                                
                                <cc1:JCalendar
                                    ID="JCalendar2" runat="server" ControlToAssign="invdate2" ImgURL="~/PresentationLayer/graph/calendar.gif" Visible =false  />
                                &nbsp;
                            </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="left" style="width: 27%">
                                <asp:Label ID="tenicanlab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 13%">
                                <asp:Label ID="from3" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 34%">
                                <asp:TextBox ID="techni1" runat="server" CssClass="textborder" Width="160px">AAA</asp:TextBox></td>
                            <td align="left" style="width: 12%">
                                <asp:Label ID="to3" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 37%">
                                <asp:TextBox ID="techni2" runat="server" CssClass="textborder" Width="152px">ZZZ</asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="left" style="width: 27%">
                                <asp:Label ID="subdtlab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 13%">
                                <asp:Label ID="from4" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 34%">
                                <asp:TextBox ID="subdt1" runat="server" CssClass="textborder" ReadOnly="false"  MaxLength =10 Style="width: 50%"></asp:TextBox>&nbsp;
                                                               
                                <asp:HyperLink ID="HypCalsubdt1" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif" ToolTip="Choose a Date">Choose a Date</asp:HyperLink>
                                
                                <cc1:JCalendar ID="JCalendar3" runat="server" ControlToAssign="subdt1" ImgURL="~/PresentationLayer/graph/calendar.gif" Visible =false  />
                            </td>
                            <td align="left" style="width: 12%">
                                <asp:Label ID="to4" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 37%">
                                <asp:TextBox ID="subdt2" runat="server" CssClass="textborder" ReadOnly="false"  MaxLength =10 Width="112px"></asp:TextBox>&nbsp;
                                
                                
                                <asp:HyperLink ID="HypCalsubdt2" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif" ToolTip="Choose a Date">Choose a Date</asp:HyperLink>

                                
                                <cc1:JCalendar
                                    ID="JCalendar4" runat="server" ControlToAssign="subdt2" ImgURL="~/PresentationLayer/graph/calendar.gif" Visible =false  />
                                
                            </td>
                        </tr>
                       
                        <tr bgcolor="#ffffff">
                            <td align="left" style="width: 27%; height: 28px;">
                                <asp:Label ID="custlab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 13%; height: 28px;">
                                <asp:Label ID="from5" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 34%; height: 28px;">
                                <asp:TextBox ID="cusid1" runat="server" CssClass="textborder" Width="160px">AAA</asp:TextBox></td>
                            <td align="left" style="width: 12%; height: 28px;">
                                <asp:Label ID="to5" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="height: 28px; width: 39%;">
                                <asp:TextBox ID="cusid2" runat="server" CssClass="textborder" Width="152px">ZZZ</asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="left" style="width: 27%; height: 28px">
                                <asp:Label ID="sortlab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" colspan="4" style="width: 73%; height: 28px">
                                <asp:DropDownList ID="sorby" runat="server" Width="128px">
                            </asp:DropDownList>
                                
                            </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="left" style="width: 27%; height: 28px">
                                 <asp:Label ID="thenlab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" colspan="4" style="width: 73%; height: 28px">
                                <asp:DropDownList ID="thenby" runat="server" Width="128px">
                            </asp:DropDownList></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="left" style="width: 27%; height: 28px;">
                                <asp:Label ID="cancelab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 73%; height: 28px;" colspan =4>
                                <asp:DropDownList ID="cancby" runat="server" Width="128px">
                                </asp:DropDownList></td>
                             
                            
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="left" style="width: 27%; height: 28px;">
                                <asp:Label ID="lblServiceBillType" runat="server" Text="Service Bill Type"></asp:Label></td>
                            <td align="left" style="width: 73%; height: 28px;" colspan =4>
                                <asp:DropDownList ID="cboServiceBillType" runat="server" Width="30%">
                                </asp:DropDownList></td>
                             
                            
                        </tr>
                    </table>
                    <asp:LinkButton ID="LinkButton1" runat="server">LinkButton</asp:LinkButton></td>
            </tr>
        </table>
        
    </div>
        <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" AutoDataBind="true" />
    </form>
</body>
</html>
