
Imports System.Data
Imports BusinessEntity
Imports System.Windows
Imports Microsoft.Win32





Partial Class PresentationLayer_report_RPTServiceDueListng_VIEW1
    Inherits System.Web.UI.Page
    Private _rptSvc As New ClsRptServiceDue
    Private ReadOnly _datestyle As New Globalization.CultureInfo("en-CA")




    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Threading.Thread.CurrentThread.CurrentCulture = _datestyle

        If (Session("SvcFr") Is "") Then
            SvcFr.Text = "AAA"
        Else
            SvcFr.Text = Session("SvcFr").ToString
        End If

        If (Session("SvcTo") Is "") Then
            SvcTo.Text = "ZZZ"
        Else
            SvcTo.Text = Session("SvcTo").ToString
        End If

        If (Session("StateFr") Is "") Then
            StateFr.Text = "AAA"
        Else
            StateFr.Text = Session("StateFr").ToString
        End If

        If (Session("StateTo") Is "") Then
            StateTo.Text = "ZZZ"
        Else
            StateTo.Text = Session("StateTo").ToString
        End If

        If (Session("AreaFr") Is "") Then
            AreaFr.Text = "AAA"
        Else
            AreaFr.Text = Session("AreaFr").ToString
        End If

        If (Session("AreaTo") Is "") Then
            AreaTo.Text = "ZZZ"
        Else
            AreaTo.Text = Session("AreaTo").ToString
        End If

        If (Session("ProdFr") Is "") Then
            ProdFr.Text = "AAA"
        Else
            ProdFr.Text = Session("ProdFr").ToString
        End If

        If (Session("ProdTo") Is "") Then
            ProdTo.Text = "ZZZ"
        Else
            ProdTo.Text = Session("ProdTo").ToString
        End If

        If (Session("ModelFr") Is "") Then
            ModelFr.Text = "AAA"
        Else
            ModelFr.Text = Session("ModelFr").ToString
        End If

        If (Session("ModelTo") Is "") Then
            ModelTo.Text = "ZZZ"
        Else
            ModelTo.Text = Session("ModelTo").ToString
        End If

        If (Session("DueFr") Is "") Then
            DueFr.Text = "AAA"
        Else
            DueFr.Text = Left(Session("DueFr"), 10).ToString
        End If

        If (Session("DueTo") Is "") Then
            DueTo.Text = "ZZZ"
        Else
            DueTo.Text = Left(Session("DueTo"), 10).ToString
        End If

        'CustID.Text = ""
        'No.Text = ""
        'CustName.Text = ""
        'Contact.Text = ""
        'ModelName.Text = ""
        'SerialNum.Text = ""
        'SVC.Text = ""
        'Area.Text = ""
        'LastX.Text = ""
        'DueType.Text = ""

        'If (Session("CustID") Is Nothing) Then
        '    CustID.Text = ""
        '    No.Text = ""
        '    CustName.Text = ""
        '    Contact.Text = ""
        '    ModelName.Text = ""
        '    SerialNum.Text = ""
        '    SVC.Text = ""
        '    Area.Text = ""
        '    LastX.Text = ""
        '    DueType.Text = ""
        'Else
        '    CustID.Text = (Session("CustID")).ToString
        '    No.Text = (Session("No")).ToString
        '    CustName.Text = (Session("Name")).ToString
        '    Contact.Text = (Session("Contact")).ToString
        '    ModelName.Text = (Session("ModName")).ToString
        '    SerialNum.Text = (Session("Serial")).ToString
        '    SVC.Text = (Session("SVC")).ToString
        '    Area.Text = (Session("Area")).ToString
        '    LastX.Text = (Session("LastX")).ToString
        '    DueType.Text = (Session("DueType")).ToString
        'End If

        If Session("DataTable").ToString = "X" Then
            Label1.Visible = True
        Else
            GridView1.DataSource = (Session("DataTable"))
            GridView1.DataBind()

            GridView1.Visible = True
        End If

        


    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub

    Protected Sub GridView1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.SelectedIndexChanged

    End Sub

    Protected Sub Export_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Export.Click
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=GridView1.xls")
        Response.Charset = ""
        Response.ContentType = "application/vnd.xls"
        Dim StringWriter As IO.StringWriter = New System.IO.StringWriter()
        Dim HtmlTextWriter As New HtmlTextWriter(StringWriter)
        Dim style As String = "<style> td { mso-number-format:\@; } </style> "

        GridView1.RenderControl(HtmlTextWriter)
        Response.Write(style) 'style is added dynamically
        Response.Write(StringWriter.ToString())
        Response.[End]()
    End Sub
End Class
