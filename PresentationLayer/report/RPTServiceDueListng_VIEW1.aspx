<%@ Page Language="VB" AutoEventWireup="false" CodeFile="RPTServiceDueListng_VIEW1.aspx.vb" Inherits="PresentationLayer_report_RPTServiceDueListng_VIEW1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
<script language="javascript" type="text/javascript">
// <!CDATA[

function TABLE1_onclick() {

}
function printpage()
  {
  window.print()
  }
// ]]>
</script>
</head>
<body>
    <form id="form1" runat="server">
        <table id="TABLE1" style="width: 1047px; height: 209px" onclick="return TABLE1_onclick()">
            <tr>
                <td align="left" colspan="8" style="height: 25px" valign="bottom">
                    &nbsp;
                    <input id="Button1" type="button" value="Print" onclick="printpage()" style="border-top-style: solid; border-right-style: solid; border-left-style: solid; background-color: transparent; border-bottom-style: solid; color: black;" size="21"/>
                    <asp:Button ID="Export" runat="server" BackColor="Transparent" BorderStyle="Solid" Text="Export to Excel" />
                    <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="No data is available for selected range of criteria! "
                        Visible="False"></asp:Label></td>
            </tr>
            <tr>
                <td align="center" colspan="8" style="height: 22px" valign="bottom" >
                    <asp:Label ID="Label29" runat="server" Font-Size="Medium" Text="Service Due Listing Report"></asp:Label></td>
            </tr>
            <tr>
                <td style="width: 100px; height: 48px;" valign="bottom">
                </td>
                <td style="width: 100px; height: 48px;" valign="bottom">
                </td>
                <td style="width: 111px; height: 48px;">
                </td>
                <td style="width: 45px; height: 48px;">
                </td>
                <td style="width: 131px; height: 48px;">
                </td>
                <td style="width: 56px; height: 48px;">
                </td>
                <td style="width: 149px; height: 48px;">
                </td>
                <td style="width: 100px; height: 48px;">
                </td>
            </tr>
            <tr>
                <td style="width: 100px; height: 21px;">
                </td>
                <td style="width: 100px; height: 21px;">
                </td>
                <td style="width: 111px; height: 21px;">
                    <asp:Label ID="Label23" runat="server" Font-Size="Smaller" Text="Service Centre"></asp:Label></td>
                <td style="width: 45px; height: 21px;">
                    <asp:Label ID="Label11" runat="server" Font-Size="Smaller" Text="From"></asp:Label></td>
                <td style="width: 131px; height: 21px;">
                    <asp:Label ID="SvcFr" runat="server" Text="Label" Font-Size="Smaller"></asp:Label></td>
                <td style="width: 56px; height: 21px;">
                    <asp:Label ID="Label17" runat="server" Font-Size="Smaller" Text="To"></asp:Label></td>
                <td style="width: 149px; height: 21px;">
                    <asp:Label ID="SvcTo" runat="server" Text="Label" Font-Size="Smaller"></asp:Label></td>
                <td style="width: 100px; height: 21px;">
                </td>
            </tr>
            <tr>
                <td style="width: 100px; height: 21px;">
                </td>
                <td style="width: 100px; height: 21px;">
                </td>
                <td style="width: 111px; height: 21px;">
                    <asp:Label ID="Label24" runat="server" Font-Size="Smaller" Text="State"></asp:Label></td>
                <td style="width: 45px; height: 21px;">
                    <asp:Label ID="Label12" runat="server" Font-Size="Smaller" Text="From"></asp:Label></td>
                <td style="width: 131px; height: 21px;">
                    <asp:Label ID="StateFr" runat="server" Text="Label" Font-Size="Smaller"></asp:Label></td>
                <td style="width: 56px; height: 21px;">
                    <asp:Label ID="Label18" runat="server" Font-Size="Smaller" Text="To"></asp:Label></td>
                <td style="width: 149px; height: 21px;">
                    <asp:Label ID="StateTo" runat="server" Text="Label" Font-Size="Smaller"></asp:Label></td>
                <td style="width: 100px; height: 21px;">
                </td>
            </tr>
            <tr>
                <td style="width: 100px; height: 21px">
                </td>
                <td style="width: 100px; height: 21px">
                </td>
                <td style="width: 111px; height: 21px">
                    <asp:Label ID="Label25" runat="server" Font-Size="Smaller" Text="Area"></asp:Label></td>
                <td style="width: 45px; height: 21px">
                    <asp:Label ID="Label13" runat="server" Font-Size="Smaller" Text="From"></asp:Label></td>
                <td style="width: 131px; height: 21px">
                    <asp:Label ID="AreaFr" runat="server" Text="Label" Font-Size="Smaller"></asp:Label></td>
                <td style="width: 56px; height: 21px">
                    <asp:Label ID="Label19" runat="server" Font-Size="Smaller" Text="To"></asp:Label></td>
                <td style="width: 149px; height: 21px">
                    <asp:Label ID="AreaTo" runat="server" Text="Label" Font-Size="Smaller"></asp:Label></td>
                <td style="width: 100px; height: 21px">
                </td>
            </tr>
            <tr>
                <td style="width: 100px; height: 21px;">
                </td>
                <td style="width: 100px; height: 21px;">
                </td>
                <td style="width: 111px; height: 21px;">
                    <asp:Label ID="Label26" runat="server" Font-Size="Smaller" Text="Product Class"></asp:Label></td>
                <td style="width: 45px; height: 21px;">
                    <asp:Label ID="Label14" runat="server" Font-Size="Smaller" Text="From"></asp:Label></td>
                <td style="width: 131px; height: 21px;">
                    <asp:Label ID="ProdFr" runat="server" Text="Label" Font-Size="Smaller"></asp:Label></td>
                <td style="width: 56px; height: 21px;">
                    <asp:Label ID="Label20" runat="server" Font-Size="Smaller" Text="To"></asp:Label></td>
                <td style="width: 149px; height: 21px;">
                    <asp:Label ID="ProdTo" runat="server" Text="Label" Font-Size="Smaller"></asp:Label></td>
                <td style="width: 100px; height: 21px;">
                </td>
            </tr>
            <tr>
                <td style="width: 100px; height: 21px">
                </td>
                <td style="width: 100px; height: 21px">
                </td>
                <td style="width: 111px; height: 21px">
                    <asp:Label ID="Label27" runat="server" Font-Size="Smaller" Text="Model"></asp:Label></td>
                <td style="width: 45px; height: 21px">
                    <asp:Label ID="Label15" runat="server" Font-Size="Smaller" Text="From"></asp:Label></td>
                <td style="width: 131px; height: 21px">
                    <asp:Label ID="ModelFr" runat="server" Text="Label" Font-Size="Smaller"></asp:Label></td>
                <td style="width: 56px; height: 21px">
                    <asp:Label ID="Label21" runat="server" Font-Size="Smaller" Text="To"></asp:Label></td>
                <td style="width: 149px; height: 21px">
                    <asp:Label ID="ModelTo" runat="server" Text="Label" Font-Size="Smaller"></asp:Label></td>
                <td style="width: 100px; height: 21px">
                </td>
            </tr>
            <tr>
                <td style="width: 100px; height: 21px">
                </td>
                <td style="width: 100px; height: 21px">
                </td>
                <td style="width: 111px; height: 21px">
                    <asp:Label ID="Label28" runat="server" Font-Size="Smaller" Text="Service Due Date"></asp:Label></td>
                <td style="width: 45px; height: 21px">
                    <asp:Label ID="Label16" runat="server" Font-Size="Smaller" Text="From"></asp:Label></td>
                <td style="width: 131px; height: 21px">
                    <asp:Label ID="DueFr" runat="server" Text="Label" Font-Size="Smaller"></asp:Label></td>
                <td style="width: 56px; height: 21px">
                    <asp:Label ID="Label22" runat="server" Font-Size="Smaller" Text="To"></asp:Label></td>
                <td style="width: 149px; height: 21px">
                    <asp:Label ID="DueTo" runat="server" Text="Label" Font-Size="Smaller"></asp:Label></td>
                <td style="width: 100px; height: 21px">
                </td>
            </tr>
        </table>
        <br />
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False">
        <Columns>
            <asp:TemplateField HeaderText="No"> <HeaderStyle HorizontalAlign="Left" Font-Size="Smaller" /> <ItemStyle Width="35px" Font-Size="Smaller"/>
            <ItemTemplate> <%# Container.DataItemIndex + 1 %> 
            </ItemTemplate>
</asp:TemplateField>
            <asp:BoundField DataField="MCUS_CUSID" HeaderText="CustomerID" ReadOnly="True">
                <ItemStyle Width="85px" Font-Size="Smaller"/>
                <HeaderStyle HorizontalAlign="Left" Font-Size="Smaller" />
            </asp:BoundField>
            <asp:BoundField DataField="MCUS_ENAME" HeaderText="Customer Name" ReadOnly="True">
                <ItemStyle Width="190px" Font-Size="Smaller"/>
                <HeaderStyle HorizontalAlign="Left" Font-Size="Smaller" />
            </asp:BoundField>
            <asp:BoundField DataField="MTEL_TELNO" HeaderText="Contact No" ReadOnly="True">
                <ItemStyle Width="90px" Font-Size="Smaller"/>
                <HeaderStyle HorizontalAlign="Left" Font-Size="Smaller" />
            </asp:BoundField>
            <asp:BoundField DataField="MMOD_ENAME" HeaderText="Model Name" ReadOnly="True">
                <ItemStyle Width="160px" Font-Size="Smaller"/>
                <HeaderStyle HorizontalAlign="Left" Font-Size="Smaller" />
            </asp:BoundField>
            <asp:BoundField DataField="MROU_SERNO" HeaderText="Serial Num" ReadOnly="True">
                <ItemStyle Width="85px" Font-Size="Smaller"/>
                <HeaderStyle HorizontalAlign="Left" Font-Size="Smaller" />
            </asp:BoundField>
            <asp:BoundField DataField="SVC_NAME" HeaderText="Service Centre" ReadOnly="True">
                <ItemStyle Width="46px" Font-Size="Smaller"/>
                <HeaderStyle HorizontalAlign="Left" Font-Size="Smaller" />
            </asp:BoundField>
            <asp:BoundField DataField="MARE_ENAME" HeaderText="Area" ReadOnly="True">
                <ItemStyle Width="128px" Font-Size="Smaller"/>
                <HeaderStyle HorizontalAlign="Left" Font-Size="Smaller" />
            </asp:BoundField>
             <asp:BoundField DataField="MROU_INSDT" HeaderText="Installation Date" ReadOnly="True">
                <ItemStyle Width="70px" Font-Size="Smaller"/>
                <HeaderStyle HorizontalAlign="Left" Font-Size="Smaller" />
            </asp:BoundField>
            <asp:BoundField DataField="MROU_APTDT" HeaderText="Last 'X' Appt" ReadOnly="True">
                <ItemStyle Width="70px" Font-Size="Smaller"/>
                <HeaderStyle HorizontalAlign="Left" Font-Size="Smaller" />
            </asp:BoundField>
            <asp:BoundField DataField="MROU_LSVDT" HeaderText="Last Service Date" ReadOnly="True">
                <ItemStyle Width="70px" Font-Size="Smaller"/>
                <HeaderStyle HorizontalAlign="Left" Font-Size="Smaller" />
            </asp:BoundField>
            <asp:BoundField DataField="SERVICE_DUE_TYPE" HeaderText="Due Type" ReadOnly="True">
                <ItemStyle Width="80px" Font-Size="Smaller"/>
                <HeaderStyle HorizontalAlign="Left" Font-Size="Smaller" />
            </asp:BoundField>
        </Columns>
            <EditRowStyle Font-Size="Smaller" />
        </asp:GridView>
    </form>
</body>
</html>
