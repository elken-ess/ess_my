Imports System
Imports System.Configuration
Imports System.Data

Imports BusinessEntity
Partial Class PresentationLayer_report_RPTdebtor_VIEW
    Inherits System.Web.UI.Page

    Private ReadOnly _datestyle As Globalization.CultureInfo = New Globalization.CultureInfo("en-CA")
    Private _debtor As New clsDebtorReport
    Private _clsReport As New ClsCommonReport
    Private _errorLog As ArrayList = New ArrayList
    Private _writeErrLog As New clsLogFile()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Threading.Thread.CurrentThread.CurrentCulture = _datestyle

        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Const script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
        Catch ex As Exception
            Const script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try

        _debtor = CType(Session("rptDebtor"), clsDebtorReport)
        _debtor.ReportType = Request.QueryString("report")

        Dim debtorDataSet As New DataSet()
        'Dim debtorDataSet1 As New DataSet()
        'Dim debtorDataSet2 As New DataSet()
        'Dim debtorTable As New DataTable
        'debtorTable.Columns.Add("Name")
        'debtorTable.Columns.Add("Addr1")
        'debtorTable.Columns.Add("Addr2")
        'debtorTable.Columns.Add("Postcode")
        'debtorTable.Columns.Add("Area")
        'debtorTable.Columns.Add("INV")
        'debtorTable.Columns.Add("Amt")
        'debtorTable.Columns.Add("fiv1_cr_amount")
        'debtorTable.Columns.Add("fiv1_cr_invdt")
        'debtorTable.Columns.Add("fiv1_cr_svbil")
        'debtorTable.Columns.Add("currency")
        'debtorTable.Columns.Add("finalAmt")
        'debtorTable.Columns(0).AutoIncrement = True
        'Dim R As DataRow = debtorTable.NewRow

        Select Case _debtor.ReportType
            Case "outstanding"
                debtorDataSet = _debtor.GetDebtorReport()
                PopulateOutstandingReport(debtorDataSet.Tables(0), _debtor)
            Case "ageing"
                debtorDataSet = _debtor.GetDebtorReport()
                PopulateAgeingReport(debtorDataSet.Tables(0), _debtor)
            Case "debtor"

                If _debtor.Ageing = "90" Then
                    debtorDataSet = _debtor.getDebtorFinalReminderLetterPrint()
                    PopulateFinalReminderLetterReport(debtorDataSet, _debtor)
                Else
                    debtorDataSet = _debtor.getDebtorReminderLetterPrint()
                    For value As Integer = 0 To debtorDataSet.Tables(0).Rows.Count - 1
                        'debtorTable.Clear()
                        _debtor.InvList = debtorDataSet.Tables(0).Rows(value).Item("INV").ToString()
                        'debtorDataSet2.Tables(value) = _debtor.getDebtorReminderLetterPrintAccSum()
                        'debtorDataSet1.Tables.Add(debtorDataSet2.Tables(value))
                        'debtorTable.ImportRow(debtorDataSet2.Tables(0).Rows(0))
                    Next
                    'debtorDataSet1.Tables.Add(debtorTable)

                    PopulateReminderLetterReport(debtorDataSet, _debtor)
                End If
              
            Case Else
                Throw New ArgumentException("Report Type is not valid")
        End Select

    End Sub

    Private Sub PopulateOutstandingReport(ByVal reportDataTable As DataTable, ByVal debtor As clsDebtorReport)
        Dim xml As New clsXml
        xml.XmlFile = ConfigurationManager.AppSettings("XmlFilePath")
        Dim strHead As String() = CType(Array.CreateInstance(GetType(String), 17), String())

        PopulateReportLabels(xml, strHead, debtor)

        Dim ds As New DataSet
        ds.Tables.Add(reportDataTable.Copy())
        GenerateReport(strHead, ds, "RPTDebtor_Outstanding.rpt")
    End Sub

    Private Sub PopulateAgeingReport(ByVal reportDataTable As DataTable, ByVal debtor As clsDebtorReport)
        Dim xml As New clsXml
        xml.XmlFile = ConfigurationManager.AppSettings("XmlFilePath")
        Dim strHead As String() = CType(Array.CreateInstance(GetType(String), 17), String())

        PopulateAgeingReportLabels(xml, strHead, debtor)

        Dim ds As New DataSet
        ds.Tables.Add(reportDataTable.Copy())
        GenerateReport(strHead, ds, "RPTDebtor_Ageing.rpt")
    End Sub

    Private Sub PopulateReminderLetterReport(ByVal reportDataSet As DataSet, ByVal debtor As clsDebtorReport)
        Dim xml As New clsXml
        xml.XmlFile = ConfigurationManager.AppSettings("XmlFilePath")
        Dim strHead As String() = CType(Array.CreateInstance(GetType(String), 23), String())

        PopulateReminderLetterLabels(xml, strHead, debtor)

        'Dim ds As New DataSet
        'ds.Tables.Add(reportDataTable.Copy())
        GenerateReminderLetterReport(strHead, reportDataSet, "RPTDebtor_ReminderLetter.rpt")


    End Sub
    Private Sub PopulateFinalReminderLetterReport(ByVal reportDataSet1 As DataSet, ByVal debtor As clsDebtorReport)
        Dim xml As New clsXml
        xml.XmlFile = ConfigurationManager.AppSettings("XmlFilePath")
        Dim strHead As String() = CType(Array.CreateInstance(GetType(String), 28), String())

        PopulateFinalReminderLetterLabels(xml, strHead, debtor)

        'Dim ds As New DataSet
        'ds.Tables.Add(reportDataTable.Copy())
        GenerateReport(strHead, reportDataSet1, "RPTDebtor_FinalReminderLetter.rpt")


    End Sub



    Private Sub PopulateReminderLetterLabels(ByVal xml As clsXml, ByVal strHead As String(), ByVal debtor As clsDebtorReport)
        strHead(12) = "We wish to remind you that according to our records, the above amount is overdue for payment. "
        strHead(0) = "Dear Sir / Madam,"
        strHead(1) = "REMINDER NOTICE FOR OUTSTANDING ACCOUNTS - RM"
        strHead(2) = "Date:"
        strHead(3) = "If you have paid within the last few days please accept our apologies for troubling you and disregard the reminder of this letter."
        strHead(4) = "If however, your account is outstanding then early settlement is appreciated. We would ask that full payment reaches our office within 7 days to ensure our continued service to you."
        strHead(5) = "Please do not hesitate to contact our Accounts & Finance Department at 03-7985 9377 should you have any queries on your account."
        strHead(6) = "Thank You."
        strHead(7) = "Notes :"
        strHead(8) = "1) We accept CASH , CHEQUE and  CREDIT CARD payment."
        strHead(9) = "2) All cheque to be crossed and made payable to Elken Service Sdn Bhd."
        strHead(10) = "3) Bank A/C : Public Bank 3124-9750-23 . Please notification to us when your successful the payment."
        strHead(11) = "**This is a computer generated which does not require any signature."
        strHead(13) = "Account Statement"
        strHead(14) = "Statement Date:"
        strHead(15) = "Account Code:"
        strHead(16) = "Transaction Date"
        strHead(17) = "Tranaction Reference"
        strHead(18) = "Description"
        strHead(19) = "Currency Code"
        strHead(20) = "Debit Amount"
        strHead(21) = "Credit Amount"
        strHead(22) = "MYR"
        titleLab.Text = "Debtors " + UCase(debtor.ReportType.Substring(0, 1)) + debtor.ReportType.Substring(1, debtor.ReportType.Length - 1) + " Report"


    End Sub

    Private Sub PopulateFinalReminderLetterLabels(ByVal xml As clsXml, ByVal strHead As String(), ByVal debtor As clsDebtorReport)
        strHead(12) = "1. We refer to the above matter, and to our reminder letters dated  "
        strHead(0) = "Dear Sir / Madam,"
        strHead(1) = "RE: FINAL REMINDER FOR OUTSTANDING PAYMENT TO ELKEN SERVICE SDN. BHD.(""THE COMPANY"")"
        strHead(2) = "Date:"
        strHead(3) = "2. We regret to note that despite our reminder letters, we have yet to receive any payment or part thereof from you, which is overdue since "
        strHead(4) = "3. TAKE NOTICE that unless you make prompt payment in the sum of RM"
        strHead(5) = "4. We will accept payment of the Outstanding Sum no latter than the above stipulated date in the following manner:-"
        strHead(6) = "a. Cash, Credit Card at out office (business hours from 09.30 am to 6:00pm); or"
        strHead(7) = "b. Online  bank-in/ Interbank GIRO to the following account : -"
        strHead(8) = "Account Holder: "
        strHead(9) = "Account No.:"
        strHead(10) = "Bank:"
        strHead(11) = "Elken Service Sdn Bhd"
        strHead(13) = "514235250389"
        strHead(14) = "Maybank Berhad."
        strHead(15) = "5. This letter is our FINAL REMINDER to you."
        strHead(16) = "6. Kindly ignore this letter if you have fully settled the outstanding sun or contact Evonne Kan/Vivian Voon at 03-7985 9377 immediately."
        strHead(17) = "Thank you."
        strHead(18) = "Yours faithfully,"
        strHead(19) = "For Elken Service Sdn Bhd"
        strHead(20) = "Authorised Signatory"
        strHead(21) = "and"
        strHead(22) = "to you respectively."
        strHead(23) = "(""Outstanding Sum"") to"
        strHead(24) = "the Company latest by"
        strHead(25) = ", failing which the company shall initiase legal action to recover the "
        strHead(26) = "Outstanding Sum without further reference to you, in which you would be further liable for all costs (including legal"
        strHead(27) = "cost) and interest arising therefrom."
        'strHead(28) = ""
        titleLab.Text = "Debtors " + UCase(debtor.ReportType.Substring(0, 1)) + debtor.ReportType.Substring(1, debtor.ReportType.Length - 1) + " Report"



    End Sub
    Private Sub PopulateReportLabels(ByVal xml As clsXml, ByVal strHead As String(), ByVal debtor As clsDebtorReport)
        strHead(0) = xml.GetLabelName("EngLabelMsg", "SBA-SERVICECENTER")
        strHead(1) = "From"
        strHead(2) = "To"

        If String.IsNullOrEmpty(debtor.FromSvcID) Then
            strHead(3) = "AAA"
        Else
            strHead(3) = debtor.FromSvcID
        End If

        If String.IsNullOrEmpty(debtor.ToSvcID) Then
            strHead(4) = "ZZZ"
        Else
            strHead(4) = debtor.ToSvcID
        End If

        strHead(5) = "Invoice Date"
        If String.IsNullOrEmpty(debtor.StartInvDate) Then
            strHead(6) = "AAA"
        Else
            strHead(6) = debtor.StartInvDate
        End If
        If String.IsNullOrEmpty(debtor.EndInvDate) Then
            strHead(7) = "ZZZ"
        Else
            strHead(7) = debtor.EndInvDate
        End If

        strHead(8) = "Customer Type"
        If String.IsNullOrEmpty(debtor.CustType) Then
            strHead(9) = "ALL"
        Else
            strHead(9) = debtor.CustType
        End If
        strHead(10) = "Report Type"
        strHead(11) = "Debtors " + UCase(debtor.ReportType.Substring(0, 1)) + debtor.ReportType.Substring(1, debtor.ReportType.Length - 1) + " Report"
        If String.IsNullOrEmpty(debtor.StartInvDate) Then
            strHead(12) = "AAA"
        Else
            strHead(12) = debtor.StartInvDate
        End If

        If String.IsNullOrEmpty(debtor.EndInvDate) Then
            strHead(13) = "ZZZ"
        Else
            strHead(13) = debtor.EndInvDate
        End If

        strHead(14) = "Elken Group of Companies"
        strHead(15) = "Debtors " + UCase(debtor.ReportType.Substring(0, 1)) + debtor.ReportType.Substring(1, debtor.ReportType.Length - 1) + " Report"
        strHead(16) = Session("userID").ToString() + " / " + Session("username").ToString()
        titleLab.Text = "Debtors " + UCase(debtor.ReportType.Substring(0, 1)) + debtor.ReportType.Substring(1, debtor.ReportType.Length - 1) + " Report"

    End Sub

    Private Sub PopulateAgeingReportLabels(ByVal xml As clsXml, ByVal strHead As String(), ByVal debtor As clsDebtorReport)
        strHead(0) = ""
        strHead(1) = "From"
        strHead(2) = "To"
        strHead(3) = ""
        strHead(4) = ""

        strHead(5) = "Invoice Date"
        If String.IsNullOrEmpty(debtor.StartInvDate) Then
            strHead(6) = "AAA"
        Else
            strHead(6) = debtor.StartInvDate
        End If
        If String.IsNullOrEmpty(debtor.EndInvDate) Then
            strHead(7) = "ZZZ"
        Else
            strHead(7) = debtor.EndInvDate
        End If

        strHead(8) = ""
        strHead(9) = ""

        strHead(10) = "Report Type"
        strHead(11) = "Debtors " + UCase(debtor.ReportType.Substring(0, 1)) + debtor.ReportType.Substring(1, debtor.ReportType.Length - 1) + " Report"
        If String.IsNullOrEmpty(debtor.StartInvDate) Then
            strHead(12) = "AAA"
        Else
            strHead(12) = debtor.StartInvDate
        End If

        If String.IsNullOrEmpty(debtor.EndInvDate) Then
            strHead(13) = "ZZZ"
        Else
            strHead(13) = debtor.EndInvDate
        End If

        strHead(14) = "Elken Group of Companies"
        strHead(15) = "Debtors " + UCase(debtor.ReportType.Substring(0, 1)) + debtor.ReportType.Substring(1, debtor.ReportType.Length - 1) + " Report"
        strHead(16) = Session("userID").ToString() + " / " + Session("username").ToString()
        titleLab.Text = "Debtors " + UCase(debtor.ReportType.Substring(0, 1)) + debtor.ReportType.Substring(1, debtor.ReportType.Length - 1) + " Report"



    End Sub

    Private Sub GenerateReport(ByVal strHead As Array, ByVal dataSet As DataSet, ByVal reportName As String)
        Try
            With _clsReport
                .ReportFileName = reportName
                .SetReport(CrystalReportViewer1, dataSet, strHead)

            End With

          
        Catch ex As Exception
            _errorLog.Add("Error").ToString()
            _errorLog.Add(ex.Message).ToString()
            _writeErrLog.ErrorLog(Session("username").ToString(), _errorLog.Item(1).ToString, "GenerateReport()", _writeErrLog.SELE, "RPTdebtor_VIEW.aspx.vb")
            _errorLog.Add("Error").ToString()
            _errorLog.Add(_clsReport.GetMessage).ToString()
            _writeErrLog.ErrorLog(Session("username").ToString(), _errorLog.Item(1).ToString, "GenerateReport()", _writeErrLog.SELE, "RPTdebtor_VIEW.aspx.vb")

        End Try
    End Sub
    Private Sub GenerateReminderLetterReport(ByVal strHead As Array, ByVal dataSet As DataSet, ByVal reportName As String)
        Try
            With _clsReport
                .ReportFileName = reportName
                .SetReminderLetterReport(CrystalReportViewer1, dataSet, strHead, _debtor.PrintList, _debtor.Ageing)

            End With


        Catch ex As Exception
            _errorLog.Add("Error").ToString()
            _errorLog.Add(ex.Message).ToString()
            _writeErrLog.ErrorLog(Session("username").ToString(), _errorLog.Item(1).ToString, "GenerateReport()", _writeErrLog.SELE, "RPTdebtor_VIEW.aspx.vb")
            _errorLog.Add("Error").ToString()
            _errorLog.Add(_clsReport.GetMessage).ToString()
            _writeErrLog.ErrorLog(Session("username").ToString(), _errorLog.Item(1).ToString, "GenerateReport()", _writeErrLog.SELE, "RPTdebtor_VIEW.aspx.vb")

        End Try
    End Sub


    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Unload
        _clsReport.UnloadReport()
        CrystalReportViewer1.Dispose()
    End Sub


End Class
