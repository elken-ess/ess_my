﻿Imports BusinessEntity
Imports System.Data
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Windows
Imports Microsoft.Win32

Partial Class PresentationLayer_report_MEGAServiceScheduleReport
    Inherits System.Web.UI.Page
    Dim objXmlTr As New clsXml
    Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)
        Return connection
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.MaintainScrollPositionOnPostBack = True
        If Not Page.IsPostBack Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try

            Dim UserID As String = Session("userID")
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle


            Dim CommonClass As New clsCommonClass()
            If Session("login_rank") <> 0 Then
                CommonClass.spctr = Session("login_ctryID").ToString().ToUpper
            End If
            CommonClass.rank = Session("login_rank")

            Dim userIDNamestr As String = Session("userID").ToString().ToUpper + "-" + Session("username").ToString.ToUpper
            Dim rank As String = Session("login_rank")
            CommonClass.spstat = Session("login_cmpID")
            CommonClass.sparea = Session("login_svcID")

            If rank <> 0 Then
                CommonClass.spctr = Session("login_ctryID").ToString().ToUpper
            End If
            CommonClass.rank = rank
            CommonClass.spstat = "ACTIVE"

            lblTotalDescription.Visible = False
            BindData()
            BindGrid(UserID)

        ElseIf appsView.Rows.Count > 0 Then
            DispGridViewHead()
        End If
        HypCal.NavigateUrl = "javascript:DoCal(document.form1.txtPreInsDateFr);"
        HypCal2.NavigateUrl = "javascript:DoCal(document.form1.txtPreInsDateTo);"

        searchButton.Enabled = True
        searchButton.Visible = True
    End Sub
    Protected Sub BindGrid(ByVal strUserID As String)
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")

        appsView.AllowPaging = True
        appsView.AllowSorting = True

        lblNoRecord.Visible = False

        'access control'''''''''''''''''''''''''''''''''''''''''''''''
        Dim accessgroup As String = Session("accessgroup").ToString
        Dim purviewArray As Array = New Boolean() {False, False, False, False}
        purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "22")

        If txtPreInsDateFr.Text = "" Then
            txtPreInsDateFr.Text = System.DateTime.Today
        End If

        If txtPreInsDateTo.Text = "" Then
            txtPreInsDateTo.Text = System.DateTime.Today
        End If
    End Sub
    Private Sub DispGridViewHead()
        appsView.HeaderRow.Cells(0).Text = "RMS No"
        appsView.HeaderRow.Cells(0).Font.Size = 8
        appsView.HeaderRow.Cells(1).Text = "SVC"
        appsView.HeaderRow.Cells(1).Font.Size = 8
        appsView.HeaderRow.Cells(2).Text = "Area"
        appsView.HeaderRow.Cells(2).Font.Size = 8
        appsView.HeaderRow.Cells(3).Text = "Cust. ID"
        appsView.HeaderRow.Cells(3).Font.Size = 8
        appsView.HeaderRow.Cells(4).Text = "Customer Name"
        appsView.HeaderRow.Cells(4).Font.Size = 8
        appsView.HeaderRow.Cells(5).Text = "Model"
        appsView.HeaderRow.Cells(5).Font.Size = 8
        appsView.HeaderRow.Cells(6).Text = "Serial No"
        appsView.HeaderRow.Cells(6).Font.Size = 8
        appsView.HeaderRow.Cells(7).Text = "Install Date"
        appsView.HeaderRow.Cells(7).Font.Size = 8
        appsView.HeaderRow.Cells(8).Text = "Group"
        appsView.HeaderRow.Cells(8).Font.Size = 8
        appsView.HeaderRow.Cells(9).Text = "Interval"
        appsView.HeaderRow.Cells(9).Font.Size = 8
        appsView.HeaderRow.Cells(10).Text = "Frequency"
        appsView.HeaderRow.Cells(10).Font.Size = 8
        appsView.HeaderRow.Cells(11).Text = "RM1"
        appsView.HeaderRow.Cells(11).Font.Size = 8
        appsView.HeaderRow.Cells(12).Text = "RM2"
        appsView.HeaderRow.Cells(12).Font.Size = 8
        appsView.HeaderRow.Cells(13).Text = "RM3"
        appsView.HeaderRow.Cells(13).Font.Size = 8
        appsView.HeaderRow.Cells(14).Text = "RM4"
        appsView.HeaderRow.Cells(14).Font.Size = 8
        appsView.HeaderRow.Cells(15).Text = "RM5"
        appsView.HeaderRow.Cells(15).Font.Size = 8
        appsView.HeaderRow.Cells(16).Text = "RM6"
        appsView.HeaderRow.Cells(16).Font.Size = 8
        appsView.HeaderRow.Cells(17).Text = "RM7"
        appsView.HeaderRow.Cells(17).Font.Size = 8
        appsView.HeaderRow.Cells(18).Text = "RM8"
        appsView.HeaderRow.Cells(18).Font.Size = 8
        appsView.HeaderRow.Cells(19).Text = "RM9"
        appsView.HeaderRow.Cells(19).Font.Size = 8
        appsView.HeaderRow.Cells(20).Text = "RM10"
        appsView.HeaderRow.Cells(20).Font.Size = 8
        appsView.HeaderRow.Cells(21).Text = "RM11"
        appsView.HeaderRow.Cells(21).Font.Size = 8
        appsView.HeaderRow.Cells(22).Text = "RM12"
        appsView.HeaderRow.Cells(22).Font.Size = 8
        appsView.HeaderRow.Cells(23).Text = "RM13"
        appsView.HeaderRow.Cells(23).Font.Size = 8
        appsView.HeaderRow.Cells(24).Text = "RM14"
        appsView.HeaderRow.Cells(24).Font.Size = 8
        appsView.HeaderRow.Cells(25).Text = "RM15"
        appsView.HeaderRow.Cells(25).Font.Size = 8
        appsView.HeaderRow.Cells(26).Text = "Status"
        appsView.HeaderRow.Cells(26).Font.Size = 8

    End Sub

    Protected Sub searchButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles searchButton.Click
        Dim UserID As String = Session("userID")
        Dim objCommonTel As New clsCommonClass
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        txtPreInsDateFr.Text = Request.Form("txtPreInsDateFr")
        txtPreInsDateTo.Text = Request.Form("txtPreInsDateTo")
        'search condition

        Dim listing As String
        Dim count As Integer

        count = ListBox2.Items.Count - 1
        listing = ""

        For i As Integer = 0 To count
            listing = listing + "," + ListBox2.Items(i).Value.ToString()
        Next

        Dim Conn As New SqlConnection
        Conn.ConnectionString = ConfigurationManager.AppSettings("ConnectionString").ToString

        Try
            Using Comm As New SqlClient.SqlCommand("" _
                        & "exec dbo.MEGASERVICESCHEDULEREPORT '" + listing + "','" + Left(ddlSVCFr.SelectedValue, 3) + "','" + Left(ddlSVCTo.SelectedValue, 3) + "','" + txtPreInsDateFr.Text + "','" + txtPreInsDateTo.Text + "'", Conn)
                Conn.Open()


                Using sda As New SqlDataAdapter

                    sda.SelectCommand = Comm

                    If sda Is Nothing Then
                        appsView.Visible = False
                        lblNoRecord.Visible = True
                        Return
                    Else
                        Using selds As New DataSet
                            sda.Fill(selds)
                            appsView.Visible = True
                            appsView.DataSource = selds
                            appsView.DataBind()
                            DispGridViewHead()
                            If selds.Tables(0).Rows.Count > 0 Then
                                lblTotalDescription.Visible = True
                                lblNoRecord.Visible = False
                                DispGridViewHead()
                                LblTotRecNo.Text = selds.Tables(0).Rows.Count.ToString + " (O) = Open, (C) = Closed"
                            Else
                                lblTotalDescription.Visible = True
                                LblTotRecNo.Text = "0"
                            End If
                            Session("selds") = selds
                        End Using
                    End If
                End Using

            End Using
            Conn.Close()
        Catch ex As Exception
            lblTotalDescription.Visible = True
            LblTotRecNo.Text = "0"
        End Try
        
    End Sub
    Protected Sub appsView_PageIndexChanging(ByVal sender As Object, ByVal e As GridViewPageEventArgs) Handles appsView.PageIndexChanging
        appsView.PageIndex = e.NewPageIndex
        Threading.Thread.CurrentThread.CurrentCulture = datestyle

        If Session("selds") IsNot Nothing Then
            Dim ds As DataSet = CType(Session("selds"), DataSet)
            'Dim ds As DataSet = Session("appsView")
            appsView.DataSource = ds
            appsView.DataBind()
            If ds.Tables(0).Rows.Count > 0 Then
                DispGridViewHead()
            End If
            txtPreInsDateFr.Text = Request.Form("apptStartDateBox")
            txtPreInsDateTo.Text = Request.Form("apptEndDateBox")
        End If

    End Sub
    Private Sub BindData()
        Dim Conn As New SqlConnection
        Conn.ConnectionString = ConfigurationManager.AppSettings("ConnectionString").ToString

        If ddlSVCFr.SelectedValue = "" Then
            Dim Comm2 As SqlCommand
            Comm2 = Conn.CreateCommand
            Comm2.CommandText = "Select [ServiceCenter] = MSVC_SVCID + '-' + MSVC_ENAME from dbo.MSVC_FIL where MSVC_CTRID = '" + Session("login_ctryID") + "' and MSVC_SVCID <> 'zzz' and MSVC_STAT = 'ACTIVE' order by MSVC_SVCID"
            Conn.Open()

            Dim dr2 As SqlDataReader = Comm2.ExecuteReader(CommandBehavior.CloseConnection)

            Dim dt2 As DataTable = New DataTable()
            dt2.Load(dr2)

            Dim counter2 As Integer

            For counter2 = 0 To dt2.Rows.Count - 1
                ddlSVCFr.Items.Add(dt2.Rows(counter2).Item("ServiceCenter").ToString)
                ddlSVCTo.Items.Add(dt2.Rows(counter2).Item("ServiceCenter").ToString)
            Next
            Conn.Close()

            ddlSVCFr.SelectedIndex = 0
            ddlSVCTo.SelectedIndex = ddlSVCTo.Items.Count - 1
        End If

        If ListBox1.SelectedValue = "" Then
            Dim Comm2 As SqlCommand
            Comm2 = Conn.CreateCommand
            Comm2.CommandText = "select distinct [group] from megasvcgroupsetup where stat = 'ACTIVE' order by [group]"

            Conn.Open()

            Dim dr2 As SqlDataReader = Comm2.ExecuteReader(CommandBehavior.CloseConnection)

            Dim dt2 As DataTable = New DataTable()
            dt2.Load(dr2)

            Dim counter2 As Integer

            For counter2 = 0 To dt2.Rows.Count - 1
                ListBox1.Items.Add(dt2.Rows(counter2).Item("group").ToString)
            Next
            Conn.Close()

            ListBox1.SelectedIndex = 0
        End If

    End Sub

    Protected Sub ddlSVCFr_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSVCFr.SelectedIndexChanged

    End Sub

    Protected Sub ddlSVCTo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSVCTo.SelectedIndexChanged

    End Sub


    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub
    Protected Sub BtnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnExport.Click
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=GridView1.xls")
        Response.Charset = ""
        Response.ContentType = "application/vnd.xls"
        Dim StringWriter As IO.StringWriter = New System.IO.StringWriter()
        Dim HtmlTextWriter As New HtmlTextWriter(StringWriter)
        Dim style As String = "<style> td { mso-number-format:\@; } </style> "

        appsView.AllowPaging = False
        appsView.DataSource = Session("selds")
        appsView.DataBind()

        appsView.RenderControl(HtmlTextWriter)
        Response.Write(style) 'style is added dynamically
        Response.Write(StringWriter.ToString())
        Response.[End]()
    End Sub
    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If ListBox1.SelectedItem IsNot Nothing Then
            ListBox2.Items.Add(ListBox1.SelectedItem)
            ListBox1.Items.Remove(ListBox1.SelectedItem)
        End If

        ListBox1.SelectedIndex = 0
    End Sub
    Protected Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click

        Dim listItem As Integer = ListBox1.Items.Count

        While ListBox1.Items.Count > 0
            For i As Integer = 0 To ListBox1.Items.Count - 1
                ListBox2.Items.Add(ListBox1.Items(i))
            Next
            ListBox1.Items.Clear()
        End While

    End Sub
    Protected Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        If ListBox2.SelectedItem IsNot Nothing Then
            ListBox1.Items.Add(ListBox2.SelectedItem)
            ListBox2.Items.Remove(ListBox2.SelectedItem)
        End If
    End Sub
    Protected Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Dim listItem As Integer = ListBox2.Items.Count

        While ListBox2.Items.Count > 0
            For i As Integer = 0 To ListBox2.Items.Count - 1
                ListBox1.Items.Add(ListBox2.Items(i))
            Next
            ListBox2.Items.Clear()
        End While
    End Sub
End Class
