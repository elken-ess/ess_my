<%@ Page Language="VB" AutoEventWireup="false" CodeFile="UserMatrix.aspx.vb" Inherits="PresentationLayer_report_UserMatrix" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>User Matrix</title>
    <link href="../css/style.css" type="text/css" rel="stylesheet">
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="JavaScript" src="../js/common.js"></script>
    <style type="text/css">
    	.rowStyle {
    		background-color: #ffffff;
    	}
    	.labelStyle {
    		text-align: left;
    		width: 26%
    	}
    	.fromToStyle {
    		text-align: left;
    		width: 7%;
    	}
    	.dataStyle {
    		text-align: left;
    		width: 30%
    	}
    	.countryStyle {
    		border: 0px;
    		width: 100%
    	}
    	.tableStyle {
    		border: 0px;
    		padding: 0px;
    		margin: 0px;
    		width: 100%;
    	}
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table id="countrytab" class="countryStyle">
            <tr>
                <td style="width: 100%">
                    <table class="tableStyle">
                        <tr>
                            <td background="../graph/title_bg.gif" style="width: 1%">
                                <img height="24" src="../graph/title1.gif" width="5" />
                            </td>
                            <td background="../graph/title_bg.gif" class="style2" width="98%" style="width: 100%">
                                <asp:Label ID="titleLab" runat="server" Text="User Matrix"></asp:Label>
                            </td>
                            <td align="LEFT" background="../graph/title_bg.gif" width="1%">
                                <img height="1" src="../graph/title_2.gif" width="5" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100%">
                    <table class="tableStyle">
                        <tr>
                            <td background="../graph/title_bg.gif" class="style2" width="98%">
                                <font color="red" style="width: 100%"></font>&nbsp; User Accounts and Information</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100%; height: 1px;">
                    <table id="country" bgcolor="#b7e6e6" border="0" cellpadding="2" cellspacing="1"
                        style="width: 100%">
                        <tr class="rowStyle">
                            <td class="labelStyle">
                                &nbsp;User ID</td>
                            <td class="fromToStyle">
                                &nbsp;From</td>
                            <td class="dataStyle">
                                <asp:TextBox ID="UserIDFromTxt" runat="server">AAA</asp:TextBox></td>
                            <td class="fromToStyle">
                                &nbsp;To</td>
                            <td class="dataStyle">
                                <asp:TextBox ID="UserIDToTxt" runat="server">ZZZ</asp:TextBox></td>
                        </tr>
                        <tr class="rowStyle">
                            <td class="labelStyle">
                                &nbsp;Group</td>
                            <td class="fromToStyle">
                                &nbsp;From</td>
                            <td class="dataStyle">
                                <asp:DropDownList ID="GroupFromDDL" runat="server">
                                </asp:DropDownList></td>
                            <td class="fromToStyle">
                                &nbsp;To</td>
                            <td class="dataStyle">
                                <asp:DropDownList ID="GroupToDDL" runat="server">
                                </asp:DropDownList></td>
                        </tr>
                        <tr class="rowStyle">
                            <td class="labelStyle">
                                &nbsp;Department</td>
                            <td class="fromToStyle">
                                &nbsp;From</td>
                            <td class="dataStyle">
                                <asp:DropDownList ID="DeptFromDDL" runat="server">
                                </asp:DropDownList></td>
                            <td class="fromToStyle">
                                &nbsp;To</td>
                            <td class="dataStyle">
                                <asp:DropDownList ID="DeptToDDL" runat="server">
                                </asp:DropDownList></td>
                        </tr>
                        <tr class="rowStyle">
                            <td class="labelStyle">
                                &nbsp;Service Center</td>
                            <td class="fromToStyle">
                                &nbsp;From</td>
                            <td class="dataStyle">
                                <asp:DropDownList ID="SVCFromDDL" runat="server">
                                </asp:DropDownList></td>
                            <td class="fromToStyle">
                                &nbsp;To</td>
                            <td class="dataStyle">
                                <asp:DropDownList ID="SVCToDDL" runat="server">
                                </asp:DropDownList></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100%; height: 16px;">
                    &nbsp;<asp:LinkButton ID="RetrieveBtn1" runat="server">Retrieve</asp:LinkButton><br />
                    <br />
                </td>
            </tr>
            <tr>
                <td style="width: 100%; height: 16px">
                    <table class="tableStyle">
                        <tr>
                            <td background="../graph/title_bg.gif" class="style2" width="98%">
                                <font color="red" style="width: 100%"></font>&nbsp; Group Access Matrix</td>
                        </tr>
                    </table>
                    <br />
                    &nbsp;<asp:LinkButton ID="RetrieveBtn2" runat="server">Retrieve</asp:LinkButton></td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

