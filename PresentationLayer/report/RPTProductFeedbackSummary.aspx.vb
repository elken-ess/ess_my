Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Web
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web
Imports BusinessEntity
Imports System.Data
Imports System.Collections.Generic ' lly 20170511

'Created on 02/01/2018
'SMR1711/2286 Lyann New report to view Product Feedback Summary

Partial Class PresentationLayer_report_RPTProductFeedbackSummary
    Inherits System.Web.UI.Page
    Dim _clsrptcust As New ClsRptCUSR()
    Private ReadOnly _clsCommon As New clsCommonClass

    Dim l As New List(Of Integer) 'lly 20170511

    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)
        Return connection
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try
 
        If Not Page.IsPostBack Then

            _clsrptcust.userid = Session("userID").ToString()
            Dim ds As DataSet = _clsrptcust.SVCnStatebyUserID()
            Dim dtSvcCtr As DataTable = ds.Tables(1)

            BindValuesToDropdown(dtSvcCtr, ddSVCCenter)
            BindValuesToDropdown(dtSvcCtr, ddSVCCenterTo)

            'LLY 
            Dim PriceIDEntity As New clsPriceID()
            Dim rank As String = Session("login_rank").ToString().ToUpper()
            If rank <> 0 Then
                PriceIDEntity.CountryID = Session("login_ctryID").ToString().ToUpper()
            End If
            PriceIDEntity.rank = rank
            Dim dsPriceIDall As DataSet = PriceIDEntity.GetPriceID()
            BindValuesToListBox(dsPriceIDall.Tables(0), lbPriceId)

            'For Model ID list
            Dim modelds As New DataSet
            _clsCommon.spctr = Session("login_ctryID")
            _clsCommon.spstat = ""
            _clsCommon.sparea = ""
            _clsCommon.rank = ""
            modelds = _clsCommon.Getcomidname("BB_MASMOTY_IDNAME")
            If modelds.Tables.Count <> 0 Then
                Dim row As DataRow
                For Each row In modelds.Tables(0).Rows
                    Dim NewItem As New ListItem()
                    NewItem.Text = row("id") & "-" & row("name")
                    NewItem.Value = row("id")
                    lbModelId.Items.Add(NewItem)
                Next
            End If

            txtApptDateFr.Text = DateTime.Today.ToString("dd/MM/yyyy")
            txtApptDateTo.Text = DateTime.Today.ToString("dd/MM/yyyy")
            ddSVCCenter.SelectedValue = "ALL"
            ddSVCCenterTo.SelectedValue = "ALL"

            Dim apptStat As New clsUtil()
            Dim statParam As ArrayList = apptStat.searchconfirminf("APPTStatus")
            Dim count As Integer
            Dim statid As String
            Dim statnm As String
            For count = 0 To statParam.Count - 1
                statid = CType(statParam.Item(count), String)
                statnm = CType(statParam.Item(count + 1), String)
                count = count + 1

                If statid = "BL" Or statid = "UM" Then
                    cboStatus.Items.Insert(0, New ListItem(statnm.ToString(), statid.ToString()))
                End If
            Next

            cboStatus.Items.Insert(0, New ListItem("All", ""))
            cboStatus.Items(0).Selected = True

        End If
    End Sub

    Protected Sub LinkReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkReport.Click
        'lly 20170516
        If Me.lbSelectedPriceId.Items.Count = 0 Then
            errlab.Text = "Please select at least 1 Price ID."
            errlab.Visible = True
        Else

            If Me.lbSelectedModelId.Items.Count = 0 Then
                errlab.Text = "Please select at least 1 Model ID."
                errlab.Visible = True
            Else
                errlab.Text = ""
                errlab.Visible = False
                Print_Report()

                Dim script As String = "window.open('../report/RPTProductFeedbackSummaryView.aspx')"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "RPTProductFeedbackSummary", script, True)
            End If
 
        End If



    End Sub

    Protected Sub Print_Report()
        Dim ProdFeedback As New clsRPTProductFeedbackSummary()

        Dim startDate As DateTime
        startDate = DateTime.ParseExact(txtApptDateFr.Text, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture)

        Dim endDate As DateTime
        endDate = DateTime.ParseExact(txtApptDateTo.Text, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture)

        If Me.txtApptDateFr.Text.Trim = "" Then
            ProdFeedback.FB_ServBillDateFr = DateTime.Today.ToString("dd/MM/yyyy")
        Else
            ProdFeedback.FB_ServBillDateFr = startDate
        End If

        If Me.txtApptDateTo.Text.Trim = "" Then
            ProdFeedback.FB_ServBillDateTo = DateTime.Today.ToString("dd/MM/yyyy")
        Else
            ProdFeedback.FB_ServBillDateTo = endDate
        End If

        If Me.ddSVCCenter.SelectedValue = "" Then
            ProdFeedback.FB_ServCtrFr = "ALL"
        Else
            ProdFeedback.FB_ServCtrFr = Me.ddSVCCenter.SelectedValue
        End If

        If Me.ddSVCCenterTo.SelectedValue = "" Then
            ProdFeedback.FB_ServCtrTo = "ALL"
        Else
            ProdFeedback.FB_ServCtrTo = Me.ddSVCCenterTo.SelectedValue
        End If

        If Me.cboStatus.SelectedValue = "" Then
            ProdFeedback.FB_Status = "ALL"
        Else
            ProdFeedback.FB_Status = Me.cboStatus.SelectedValue
        End If

        'lly 20170512 - prepare priceid string with delimiter ','
        If Me.lbSelectedPriceId.Items.Count = 0 Then
            ProdFeedback.FB_PriceId = ""
        Else
            For i As Integer = 0 To Me.lbSelectedPriceId.Items.Count - 1
                If (i = 0) Then
                    ProdFeedback.FB_PriceId = Me.lbSelectedPriceId.Items(i).Value
                Else
                    ProdFeedback.FB_PriceId = ProdFeedback.FB_PriceId + "," + Me.lbSelectedPriceId.Items(i).Value
                End If
            Next
        End If

        If Me.lbSelectedModelId.Items.Count = 0 Then
            ProdFeedback.FB_ModelId = ""
        Else
            For i As Integer = 0 To Me.lbSelectedModelId.Items.Count - 1
                If (i = 0) Then
                    ProdFeedback.FB_ModelId = Me.lbSelectedModelId.Items(i).Value
                Else
                    ProdFeedback.FB_ModelId = ProdFeedback.FB_ModelId + "," + Me.lbSelectedModelId.Items(i).Value
                End If
            Next
        End If

        Try
            Session("rpt_ProductFeedbackSummary") = ProdFeedback

        Catch ex As Exception
            Response.Redirect("~/PresentationLayer/Error.aspx")

        End Try
    End Sub

    Private Sub BindValuesToDropdown(ByVal dt As DataTable, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        dropdown.Items.Add(New ListItem("All", ""))

        Dim row As DataRow
        If dt.Rows.Count > 0 Then
            For Each row In dt.Rows
                Dim NewItem As New ListItem()

                If dt.Columns.Count = 1 Then
                    NewItem.Text = Trim(row(0).ToString())
                Else
                    NewItem.Text = Trim(row(0).ToString()) & "-" & row(1).ToString()
                End If
                NewItem.Value = Trim(row(0).ToString())
                dropdown.Items.Add(NewItem)
            Next
        End If
    End Sub

    'LLY 20170511
    Private Sub BindValuesToListBox(ByVal dt As DataTable, ByVal listbox As ListBox)
        listbox.Items.Clear()

        Dim row As DataRow
        If dt.Rows.Count > 0 Then
            For Each row In dt.Rows
                Dim NewItem As New ListItem()

                If dt.Columns.Count = 1 Then
                    NewItem.Text = Trim(row(0).ToString())
                Else
                    NewItem.Text = Trim(row(0).ToString()) & "-" & row(1).ToString()
                End If
                NewItem.Value = Trim(row(0).ToString())
                listbox.Items.Add(NewItem)
            Next
        End If

    End Sub
    'LLY 20170511
    Protected Sub btnToLeft_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnToLeft.Click

        Dim lstRemoveItem As New List(Of ListItem)

        For Each li As ListItem In Me.lbPriceId.Items
            If li.Selected Then
                lstRemoveItem.Add(New ListItem(li.Text, li.Value))
                ' can't remove from the collection while looping through it       
            End If
        Next

        For Each li As ListItem In lstRemoveItem
            Me.lbSelectedPriceId.Items.Add(li)       ' add to "selected" items
            Me.lbPriceId.Items.Remove(li)   ' remove from the original available items
        Next


    End Sub
    'LLY 20170511
    Protected Sub btnToRight_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnToRight.Click

        Dim lstRemoveItem As New List(Of ListItem)

        For Each li As ListItem In Me.lbSelectedPriceId.Items
            If li.Selected Then
                lstRemoveItem.Add(New ListItem(li.Text, li.Value))
                ' can't remove from the collection while looping through it       
            End If
        Next

        For Each li As ListItem In lstRemoveItem
            Me.lbPriceId.Items.Add(li)       ' add to "selected" items
            Me.lbSelectedPriceId.Items.Remove(li)   ' remove from the original available items
        Next


    End Sub

    Protected Sub btnToLeftModel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnToLeftModel.Click

        Dim lstRemoveItem As New List(Of ListItem)

        For Each li As ListItem In Me.lbModelId.Items
            If li.Selected Then
                lstRemoveItem.Add(New ListItem(li.Text, li.Value))
                ' can't remove from the collection while looping through it       
            End If
        Next

        For Each li As ListItem In lstRemoveItem
            Me.lbSelectedModelId.Items.Add(li)       ' add to "selected" items
            Me.lbModelId.Items.Remove(li)   ' remove from the original available items
        Next

    End Sub

    Protected Sub btnToRightModel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnToRightModel.Click

        Dim lstRemoveItem As New List(Of ListItem)

        For Each li As ListItem In Me.lbSelectedModelId.Items
            If li.Selected Then
                lstRemoveItem.Add(New ListItem(li.Text, li.Value))
                ' can't remove from the collection while looping through it       
            End If
        Next

        For Each li As ListItem In lstRemoveItem
            Me.lbModelId.Items.Add(li)       ' add to "selected" items
            Me.lbSelectedModelId.Items.Remove(li)   ' remove from the original available items
        Next

    End Sub

End Class
