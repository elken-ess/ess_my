﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="RPTRAPT.aspx.vb" Inherits="PresentationLayer_Report_ReaptedApp" %>

<%@ Register Assembly="JCalendar" Namespace="JCalendar" TagPrefix="cc1" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Repeated Appointment</title>
     <link href="../css/style.css" rel="stylesheet" type="text/css" />
     <STYLE type="text/css">A:link { COLOR: #336666; TEXT-DECORATION: none }
	BODY { FONT-SIZE: 9pt; FONT-FAMILY: "Arial", "Verdana", "Tahoma"; BACKGROUND-COLOR: #ffffff }
	TD { FONT-SIZE: 9pt; FONT-FAMILY: Arial,Verdana,Tahoma }
	</STYLE>
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
        <script language="JavaScript" src="../js/common.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
     <table id="countrytab" border="0" style="width: 100%">
            <tr>
                <td style="width: 100%">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title1.gif" width="5" /></td>
                            <td background="../graph/title_bg.gif" class="style2" width="98%">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title_2.gif" width="5" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table id="country" bgcolor="#b7e6e6" border="0" cellpadding="2" cellspacing="1" style="width: 100%">
                        <tr bgcolor="#ffffff">
                            <td align="left" style="width: 27%; height: 28px;">
                                <asp:Label ID="serlab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 5%; height: 28px;">
                                <asp:Label ID="from1" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 34%; height: 28px;">
                                <asp:TextBox ID="sercen1" runat="server" CssClass="textborder" style="width: 90%">AAA</asp:TextBox></td>
                            <td align="left" style="width: 5%; height: 28px;">
                                <asp:Label ID="to1" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 37%; height: 28px">
                                <asp:TextBox ID="sercen2" runat="server" CssClass="textborder">ZZZ</asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="left" style="width: 27%; height: 25px;">
                                <asp:Label ID="datelab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 5%; height: 25px;">
                                <asp:Label ID="from2" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 34%; height: 25px;">
                                <asp:TextBox ID="date1" runat="server" CssClass="textborder" style="width: 75%" ReadOnly="false" MaxLength =10></asp:TextBox>
                                <asp:HyperLink ID="HypCalFrom" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif" ToolTip="Choose a Date">Choose a Date</asp:HyperLink>

                                <cc1:JCalendar ID="JCalendar1" runat="server" ControlToAssign="date1" ImgURL="~/PresentationLayer/graph/calendar.gif" Visible =false  />
                            </td>
                            <td align="left" style="width: 5%; height: 25px;">
                                <asp:Label ID="to2" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 37%; height: 25px">
                                <asp:TextBox ID="date2" runat="server" CssClass="textborder" ReadOnly="false" MaxLength =10 Width="120px"></asp:TextBox>
                                
                                <asp:HyperLink ID="HypCalTo" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif" ToolTip="Choose a Date">Choose a Date</asp:HyperLink>

                                <cc1:JCalendar ID="JCalendar2" runat="server" ControlToAssign="date2" ImgURL="~/PresentationLayer/graph/calendar.gif"  Visible =false />
                                
                            </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="left" style="width: 27%">
                                <asp:Label ID="cuslab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 5%">
                                <asp:Label ID="from3" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 34%">
                                <asp:TextBox ID="custom1" runat="server" CssClass="textborder" style="width: 90%">AAA</asp:TextBox></td>
                            <td align="left" style="width: 5%">
                                <asp:Label ID="to3" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 37%">
                                <asp:TextBox ID="custom2" runat="server" CssClass="textborder">ZZZ</asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="left" style="width: 27%">
                                <asp:Label ID="serilab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 5%">
                                <asp:Label ID="from4" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 34%">
                                <asp:TextBox ID="seri1" runat="server" CssClass="textborder" style="width: 90%">AAA</asp:TextBox></td>
                            <td align="left" style="width: 5%">
                                <asp:Label ID="to4" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 37%">
                                <asp:TextBox ID="seri2" runat="server" CssClass="textborder">ZZZ</asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="left" style="width: 27%; height: 28px;">
                                <asp:Label ID="modelab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 5%; height: 28px;">
                                <asp:Label ID="from5" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 34%; height: 28px;">
                                <asp:TextBox ID="modelid1" runat="server" CssClass="textborder" style="width: 90%">AAA</asp:TextBox></td>
                            <td align="left" style="width: 5%; height: 28px;">
                                <asp:Label ID="to5" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="height: 28px; width: 37%;">
                                <asp:TextBox ID="modelid2" runat="server" CssClass="textborder">ZZZ</asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="left" style="width: 27%; height: 28px">
                                <asp:Label ID="repelab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 5%; height: 28px">
                                </td>
                            <td align="left" style="width: 35%; height: 28px"  colspan=3>
                            <asp:DropDownList ID="Reaptedp" runat="server" Style="width: 10%">
                                    <asp:ListItem>&gt;</asp:ListItem>
                                    <asp:ListItem>&gt;=</asp:ListItem>
                                </asp:DropDownList>
                                <asp:TextBox ID="repeat" runat="server"></asp:TextBox></td>
                           
                           
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="left" style="width: 27%; height: 28px;">
                                <asp:Label ID="sertylab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 5%; height: 28px">
                                </td>
                            <td align="left" style="width: 5%; height: 28px;" colspan =3>
                                <asp:DropDownList ID="servity" runat="server" Width="80%">
                                </asp:DropDownList></td>
                             
                            
                        </tr>
                    </table>
                    <asp:LinkButton ID="LinkButton1" runat="server">LinkButton</asp:LinkButton>
        </td>
            </tr>
        </table>
        
    </div>
        <asp:RegularExpressionValidator ID="repnumerr" runat="server" ControlToValidate="repeat"
            ForeColor="White" ValidationExpression="\d{0,20}" Width="8px">*</asp:RegularExpressionValidator>
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
            ShowSummary="False" />
        
    </form>
</body>
</html>
