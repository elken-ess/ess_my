Imports System.Data
Imports System.Data.SqlClient
Imports System.Globalization
Imports SQLDataAccess
Imports System.Windows
Imports Microsoft.Win32
Partial Class PresentationLayer_report_AppointmentSchedulingReport_VIEW
    Inherits System.Web.UI.Page

    Dim RankA As String
    Dim RankB As String
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)
        Return connection
    End Function


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim _con As SqlConnection
        _con = GetConnection(ConfigurationManager.AppSettings("ConnectionString"))

        FromDate.Text = Session("FromDate").ToString
        ToDate.Text = Session("ToDate").ToString
        SVCFrom.Text = Session("SVCFrom").ToString
        SVCTo.Text = Session("SVCTo").ToString
        TypeFrom.Text = Session("TypeFrom").ToString
        TypeTo.Text = Session("TypeTo").ToString
        StatusFrom.Text = Session("StatusFrom").ToString
        StatusTo.Text = Session("StatusTo").ToString
        CRFrom.Text = Session("CRFrom").ToString
        CRTo.Text = Session("CRTo").ToString

        'If Session("RankFrom").ToString = "Country" Then
        '    RankA = "7"
        'ElseIf Session("RankFrom").ToString = "Company" Then
        '    RankA = "8"
        'Else
        '    RankA = "9"
        'End If

        'If Session("RankTo").ToString = "Country" Then
        '    RankB = "7"
        'ElseIf Session("RankTo").ToString = "Company" Then
        '    RankB = "8"
        'Else
        '    RankB = "9"
        'End If

        '& " and MUSR_RANK >= '" & RankA & "' and MUSR_RANK <= '" & RankB & "'" _

        Using Comm As New SqlClient.SqlCommand(" " _
            & " declare @CRFrom varchar(20), @CRTo varchar(20), @SVCFrom varchar(20), @SVCto varchar(20), @TypeFrom varchar(20), @TypeTo varchar(20), @StatusFrom varchar(20), @StatusTo varchar(20) " _
            & " set @CRFrom = '" & CRFrom.Text & "'" _
            & " set @CRTo = '" & CRFrom.Text & "'" _
            & " set @SVCFrom = '" & SVCFrom.Text & "'" _
            & " set @SVCTo = '" & SVCTo.Text & "'" _
            & " set @TypeFrom = '" & TypeFrom.Text & "'" _
            & " set @TypeTo = '" & TypeTo.Text & "'" _
            & " set @StatusFrom = '" & StatusFrom.Text & "'" _
            & " set @StatusTo = '" & StatusTo.Text & "'" _
            & " if @CRFrom = 'ALL' begin set @CRFrom = '000' end " _
            & " if @CRTo = 'ALL' begin set @CRTo = 'ZZZ' end " _
            & " if @SVCFrom = 'ALL' begin set @SVCFrom = '000' end " _
            & " if @SVCTo = 'ALL' begin set @SVCTo = 'ZZZ' end " _
            & " if @TypeFrom = 'ALL' begin set @TypeFrom = '000' end " _
            & " if @TypeTo = 'ALL' begin set @TypeTo = 'ZZZ' end " _
            & " if @StatusFrom = 'ALL' begin set @StatusFrom = '000' end " _
            & " if @StatusTo = 'ALL' begin set @StatusTo = 'ZZZ' end " _
            & " select FAPT_TRNTY, FAPT_TRNNO, [FAPT_IsInBound] =  case FAPT_IsInbound when 1 then 'In' else 'Out' end,  " _
            & " [FAPT_CREDT] = convert(varchar, FAPT_CREDT, 103), [FAPT_TIME] = convert(char(5), FAPT_CREDT, 108), " _
            & " [FAPT_APTDT] = convert(varchar, FAPT_APTDT, 103), FAPT_CUSID, MCUS_ENAME,MROU_MODID,'=' +'""' + MROU_SERNO + '""' AS MROU_SERNO, FAPT_SVCID,  " _
            & " FAPT_ZONID, FAPT_APTTY, FAPT_STAT, MUSR_ENAME, MTCH_ENAME, FAPT_REM " _
            & " from FAPT_FIL with (nolock) left join MCUS_FIL with (nolock) on FAPT_CUSID = MCUS_CUSID and MCUS_CUSPF = FAPT_CUSPF " _
            & " left join MUSR_FIL with (nolock) on FAPT_CRID = MUSR_USRID " _
            & " left join MTCH_FIL with (nolock) on FAPT_TCHID = MTCH_TCHID " _
            & " left join MADR_FIL with (nolock) on FAPT_CUSID = MADR_CUSID and MADR_CUSPF = FAPT_CUSPF " _
            & " left join MROU_FIL with (nolock) on MROU_ROUID = FAPT_ROUID and MROU_CUSPF = MROU_CUSPF " _
            & " where FAPT_APTDT >= '" & DateTime.ParseExact(FromDate.Text, "dd/MM/yyyy", System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat) & "' " _
            & " and FAPT_APTDT <= '" & DateTime.ParseExact(ToDate.Text, "dd/MM/yyyy", System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat) & "' " _
            & " and FAPT_SVCID >= @SVCFrom and FAPT_SVCID <= @SVCTo " _
            & " and FAPT_APTTY >= @TypeFrom and FAPT_APTTY <= @TypeTo " _
            & " and FAPT_STAT >= @StatusFrom and FAPT_STAT <= @StatusTo " _
            & " and ((FAPT_CRID >= @CRFrom and FAPT_CRID <= @CRTo) or " _
            & " (FAPT_CRID <= @CRFrom and FAPT_CRID >= @CRTo)) " _
            & " and MUSR_STAT <> 'DELETE' and MCUS_STAT = 'ACTIVE' ", _con)
            _con.Open()

            Dim dr1 As SqlDataReader = Comm.ExecuteReader(CommandBehavior.CloseConnection)

            Dim dt1 As DataTable = New DataTable()
            dt1.Load(dr1)

            GridView1.DataSource = dt1
            GridView1.DataBind()

            GridView1.Visible = True

            _con.Close()
        End Using




        'If Session("DataTable").ToString = "X" Then
        '    Label1.Visible = True
        'Else
        '    GridView1.DataSource = (Session("DataTable"))
        '    GridView1.DataBind()

        '    GridView1.Visible = True
        'End If

    End Sub
    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub

    Protected Sub Export_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Export.Click
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=GridView1.xls")
        Response.Charset = ""
        Response.ContentType = "application/vnd.xls"
        Response.Buffer = False
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        'Dim StringWriter As IO.StringWriter = New System.IO.StringWriter()
        'Dim HtmlTextWriter As New HtmlTextWriter(StringWriter)
        Dim HtmlTextWriter As New HtmlTextWriter(Response.Output)
        Dim style As String = "<style> td { mso-number-format:\@; } </style> "

        GridView1.RenderControl(HtmlTextWriter)
        Response.Write(style) 'style is added dynamically
        ' Response.Write(StringWriter.ToString())
        Response.[End]()
    End Sub
End Class
