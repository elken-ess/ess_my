<%@ Page Language="VB" EnableViewState="true" AutoEventWireup="false" CodeFile="RPTPRFSummary.aspx.vb" Inherits="PresentationLayer.report.PresentationLayer_report_RPTPRFSummary" %>
<%@ Register TagPrefix="cr" Namespace="CrystalDecisions.Web" Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>PRF Summary</title>
	<LINK href="../css/style.css" type="text/css" rel="stylesheet">
	<script language="JavaScript" src="../js/common.js"></script>

    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table id="countrytab" style="border: 0; width: 100%;">
            <tr>
                <td style="width: 100%">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title1.gif" width="5" /></td>
                            <td background="../graph/title_bg.gif" class="style2" width="98%">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="LEFT" background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title_2.gif" width="5" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100%">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" class="style2" width="98%">
                                <font color="red">
                                    <asp:Label ID="errlab" runat="server" Text="Label" Visible="false"></asp:Label></font></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100%; height: 188px;">
                    <table id="country" bgcolor="#b7e6e6" border="0" cellpadding="2" cellspacing="1" width ="100%">
                        <tr bgcolor="#ffffff">
                            <td align="LEFT" style="width: 18%">
                                <asp:Label ID="ServCenterLabel" runat="server"/>
                                </td>
                            <td align="LEFT" style="width: 6%">
                                <asp:Label ID="Fromlab2" runat="server" Text="From"/>
                                </td>
                            <td align="left" style="width: 27%">
                                <asp:DropDownList ID="ServCenterDropdown" 
                                                  runat="server" 
                                                  Width="300px" 
                                                  AutoPostBack="True"
                                                  ValidationGroup="PRFGroup"/>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" 
                                                            runat="server" 
                                                            ErrorMessage="Service center is required"
                                                            ValidationGroup="PRFGroup"
                                                            ControlToValidate="ServCenterDropdown"
                                                            Text="*"/>
                            </td>
                            <td align="LEFT" style="width: 9%">
                                </td>
                            <td align="left" width="35%">
                                <asp:HiddenField runat="server" ID="IDHiddenField"/>
                                </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="LEFT" style="width: 18%">
                                <asp:Label ID="TechnicianLabel" runat="server"/>
                                </td>
                            <td align="LEFT" style="width: 6%">
                                <asp:Label ID="Fromlab4" runat="server" Text="From" Height="16px"/>
                                </td>
                            <td align="left" style="width: 27%">
                                <asp:DropDownList runat="server" 
                                                  ID="TechnicianDropdown" 
                                                  Width="300px" 
                                                  ValidationGroup="PRFGroup"/>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" 
                                                            runat="server" 
                                                            ErrorMessage="Technician is required"
                                                            Text="*"
                                                            ControlToValidate="TechnicianDropdown"
                                                            InitialValue=""
                                                            ValidationGroup="PRFGroup"/>
                            </td>
                            <td align="LEFT" style="width: 9%">
                                </td>
                            <td align="left" width="35%" style="height: 30px">
                                </td>
                        </tr>
                        
                         <tr bgcolor="#ffffff">
                            <td align="LEFT" style="width: 18%">
                                <asp:Label ID="PRFLabel" runat="server"/>
                                </td>
                            <td align="LEFT" style="width: 6%">
                                <asp:Label ID="Fromlab5" runat="server" Text="From" Height="16px"/>
                                </td>
                            <td align="left" style="width: 27%">
                                <asp:TextBox ID="PRFTextbox" 
                                             runat="server" 
                                             CssClass="textborder" 
                                             MaxLength="13" 
                                             ValidationGroup="PRFGroup"
                                             Width="300px"/>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" 
                                                            runat="server" 
                                                            ErrorMessage="PRF number is required"
                                                            ControlToValidate="PRFTextbox"
                                                            ValidationGroup="PRFGroup"
                                                            Text="*"/>
                                </td>
                            <td align="LEFT" style="width: 9%">
                                </td>
                            <td align="left" width="35%">
                                </td>
                        </tr>                                              
                        <tr bgcolor="#ffffff">
                            <td align="left" style="width: 18%">
                                <asp:LinkButton ID="AddLinkButton" runat="server" Text="Add" ValidationGroup="PRFGroup" OnClick="AddLinkButton_Click" />
                            </td>
                            <td align="left" style="width: 6%">
                                &nbsp;</td>
                            <td align="left" style="width: 27%">
                            </td>
                            <td align="left" style="width: 9%">
                            </td>
                            <td align="left" width="35%">
                            </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="left" colspan="5">
                                <asp:GridView ID="PRFGridView" runat="server" AutoGenerateColumns="False" DataKeyNames="ServiceCenter">
                                    <Columns>
                                        <asp:CommandField ButtonType="Image" SelectImageUrl="~/PresentationLayer/graph/edit.gif"
                                            ShowSelectButton="True" />
                                        <asp:BoundField DataField="ID" HeaderText="No"/>
                                        <asp:BoundField DataField="ServiceCenter" HeaderText="Service Center ID" />
                                        <asp:BoundField DataField="Technician" HeaderText="Technician ID" />
                                        <asp:BoundField DataField="Prf" HeaderText="PRF" />
                                        <asp:CommandField ButtonType="Image" DeleteImageUrl="~/PresentationLayer/graph/delete_row_small.gif"
                                            ShowDeleteButton="True" />
                                    </Columns>
                                    <EmptyDataTemplate>
                                        No records found
                                    </EmptyDataTemplate>
                                </asp:GridView>
								<br />
                                <span style="color: red">* Maximum of 5 PRFs only are allowed. </span>
                                <br />
                            </td>
                        </tr>
                        
                        
                    </table>
                    <asp:LinkButton ID="ViewReportLinkButton" runat="server" Text="ViewReport" OnClick="ViewReportLinkButton_Click"/>
                    </td>
            </tr>
            <tr>
                <td>
                    <asp:ValidationSummary ID="ValidationSummary1" 
                                                   ValidationGroup="PRFGroup" 
                                                   runat="server" 
                                                   ShowMessageBox="True" />
                </td>
            </tr>
            <tr>
                <td>
                    <cr:CrystalReportViewer id="CrystalReportViewer1" runat="server" autodatabind="true" Height="50px" Width="350px"/>
                </td>
            </tr>
        </table>    
    </div>
    </form>
</body>
</html>
