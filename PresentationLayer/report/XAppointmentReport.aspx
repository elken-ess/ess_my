<%@ Page Language="VB" AutoEventWireup="false" CodeFile="XAppointmentReport.aspx.vb"
    Inherits="PresentationLayer_report_AppointmentSchedulingReport" %>
<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc2" %>
<%@ Register Assembly="JCalendar" Namespace="JCalendar" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>User Matrix</title>
    <link href="../css/style.css" type="text/css" rel="stylesheet">
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="JavaScript" src="../js/common.js"></script>

    <style type="text/css">
    	.rowStyle {
    		background-color: #ffffff;
    	}
    	.labelStyle {
    		text-align: left;
    		width: 26%
    	}
    	.fromToStyle {
    		text-align: left;
    		width: 7%;
    	}
    	.dataStyle {
    		text-align: left;
    		width: 30%
    	}
    	.countryStyle {
    		border: 0px;
    		width: 100%
    	}
    	.tableStyle {
    		border: 0px;
    		padding: 0px;
    		margin: 0px;
    		width: 100%;
    	}
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table id="countrytab" class="countryStyle">
                <tr>
                    <td style="width: 100%">
                        <table class="tableStyle">
                            <tr>
                                <td background="../graph/title_bg.gif" style="width: 1%">
                                    <img height="24" src="../graph/title1.gif" width="5" />
                                </td>
                                <td background="../graph/title_bg.gif" class="style2" width="98%" style="width: 100%">
                                    <asp:Label ID="titleLab" runat="server" Text="X - Appointment Report"></asp:Label>
                                </td>
                                <td align="LEFT" background="../graph/title_bg.gif" width="1%">
                                    <img height="1" src="../graph/title_2.gif" width="5" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="width: 100%; height: 1px;">
                        <table id="country" bgcolor="#b7e6e6" border="0" cellpadding="2" cellspacing="1"
                            style="width: 100%">
                            <tr class="rowStyle">
                                <td class="labelStyle">
                                    &nbsp;Service Center</td>
                                <td class="dataStyle">
                                    <asp:DropDownList ID="SVCFromDDL" runat="server" AutoPostBack="True" Width="325px">
                                    </asp:DropDownList></td>
                                <td class="dataStyle">
                                    <asp:DropDownList ID="SVCToDDL" runat="server" AutoPostBack="True" Width="325px">
                                    </asp:DropDownList></td>
                            </tr>
                            <tr class="rowStyle">
                                <td class="labelStyle">
                                    &nbsp;Appointment Date</td>
                                <td class="dataStyle">
                                    <asp:TextBox ID="FromDateTxt" runat="server" CssClass="textborder" ReadOnly="false"
                                        MaxLength="10">DD/MM/YYYY</asp:TextBox>
                                    <asp:HyperLink ID="HypCalFrom" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                                        ToolTip="Choose a Date" Visible="False">Choose a Date</asp:HyperLink>
                                    <cc1:JCalendar ID="JCalendar1" runat="server" ControlToAssign="FromDateTxt" ImgURL="~/PresentationLayer/graph/calendar.gif" />
                                </td>
                                <td class="dataStyle">
                                    <asp:TextBox ID="ToDateTxt" runat="server" CssClass="textborder" ReadOnly="false"
                                        MaxLength="10">DD/MM/YYYY</asp:TextBox>
                                    <asp:HyperLink ID="HypCalTo" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                                        ToolTip="Choose a Date" Visible="False">Choose a Date</asp:HyperLink>
                                    <cc1:JCalendar ID="JCalendar2" runat="server" ControlToAssign="ToDateTxt" ImgURL="~/PresentationLayer/graph/calendar.gif" />
                                </td>
                            </tr>
                            <tr class="rowStyle">
                                <td class="labelStyle">
                                    &nbsp;Create Date</td>
                                <td class="dataStyle">
                                    <asp:TextBox ID="FromCreateDateTxt" runat="server" CssClass="textborder" ReadOnly="false"
                                        MaxLength="10">DD/MM/YYYY</asp:TextBox>
                                    <asp:HyperLink ID="HypCalFrCreateDate" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                                        ToolTip="Choose a Date" Visible="False">Choose a Date</asp:HyperLink>
                                    <cc1:JCalendar ID="JCalendar3" runat="server" ControlToAssign="FromCreateDateTxt" ImgURL="~/PresentationLayer/graph/calendar.gif" />
                                </td>
                                <td class="dataStyle">
                                    <asp:TextBox ID="ToCreateDateTxt" runat="server" CssClass="textborder" ReadOnly="false"
                                        MaxLength="10">DD/MM/YYYY</asp:TextBox>
                                    <asp:HyperLink ID="HypCalToCreateDate" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                                        ToolTip="Choose a Date" Visible="False">Choose a Date</asp:HyperLink>
                                    <cc1:JCalendar ID="JCalendar4" runat="server" ControlToAssign="ToCreateDateTxt" ImgURL="~/PresentationLayer/graph/calendar.gif" />
                                </td>
                            </tr>
                            <tr class="rowStyle">
                                <td class="labelStyle">
                                    &nbsp;Product Class</td>
                                <td class="dataStyle">
                                    <asp:DropDownList runat="server" ID="ddFromProduct" CssClass="textborder" 
                                        />
                                </td>
                                <td class="dataStyle">
                                </td>
                            </tr>
                            <tr class="rowStyle">
                                <td class="labelStyle">
                                    &nbsp;Model ID</td>
                                <td class="dataStyle">
                                    <asp:DropDownList runat="server" ID="ddFromModelID" Style="width: 250px" CssClass="textborder" />
                                </td>
                                <td class="dataStyle">
                                    <asp:DropDownList runat="server" ID="ddToModelID" Style="width: 250px" CssClass="textborder" />
                                </td>
                            </tr>
                            <tr class="rowStyle">
                                <td class="labelStyle" >
                                    &nbsp;CR ID-User Name</td>
                                <td class="dataStyle" colspan="2">
                                    <asp:DropDownList runat="server" ID="ddlUserNameFr" Style="width: 250px" CssClass="textborder" />
                                    </td>
                            </tr>
                            <tr class="rowStyle">
                                <td class="labelStyle">
                                    &nbsp;Reason of Objection</td>
                                <td class="dataStyle" colspan="2">
                                    <asp:DropDownList ID="ddlReasonObjection" runat="server" Width="325px">
                                    </asp:DropDownList></td>
                            </tr>
                           
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="width: 100%; height: 16px;">
                     <cc2:MessageBox ID="InfoMsgBox" runat="server" />
                        &nbsp;<asp:LinkButton ID="RetrieveBtn1" runat="server">Retrieve</asp:LinkButton><br />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
