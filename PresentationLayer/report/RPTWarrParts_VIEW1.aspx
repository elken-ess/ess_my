<%@ Page Language="VB" AutoEventWireup="false" CodeFile="RPTWarrParts_VIEW1.aspx.vb" Inherits="PresentationLayer_report_RPTWarrParts_VIEW1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <script language="javascript" type="text/javascript">
// <!CDATA[

function TABLE1_onclick() {

}
function printpage()
  {
  window.print()
  }
// ]]>
</script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table id="TABLE1" onclick="return TABLE1_onclick()" style="width: 1047px; height: 209px">
            <tr>
                <td align="left" colspan="8" style="height: 25px" valign="bottom">
                    &nbsp;
                    <input id="Button1" onclick="printpage()" size="21" style="color: black; border-top-style: solid;
                        border-right-style: solid; border-left-style: solid; background-color: transparent;
                        border-bottom-style: solid" type="button" value="Print" />
                    <asp:Button ID="Export" runat="server" BackColor="Transparent" BorderStyle="Solid"
                        Text="Export to Excel" />
                    <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="No data is available for selected range of criteria! "
                        Visible="False"></asp:Label></td>
            </tr>
            <tr>
                <td align="center" colspan="8" style="height: 22px" valign="bottom">
                    <asp:Label ID="Label29" runat="server" Font-Size="Medium" Text="Warranty Parts Report"></asp:Label></td>
            </tr>
            <tr>
                <td style="width: 100px; height: 48px" valign="bottom">
                </td>
                <td style="width: 100px; height: 48px" valign="bottom">
                </td>
                <td style="width: 111px; height: 48px">
                </td>
                <td style="width: 45px; height: 48px">
                </td>
                <td style="width: 131px; height: 48px">
                </td>
                <td style="width: 56px; height: 48px">
                </td>
                <td style="width: 149px; height: 48px">
                </td>
                <td style="width: 100px; height: 48px">
                </td>
            </tr>
            <tr>
                <td style="width: 100px; height: 21px">
                </td>
                <td style="width: 100px; height: 21px">
                </td>
                <td style="width: 111px; height: 21px">
                    <asp:Label ID="Label23" runat="server" Font-Size="Smaller" Text="Service Centre"></asp:Label></td>
                <td style="width: 45px; height: 21px">
                    <asp:Label ID="Label11" runat="server" Font-Size="Smaller" Text="From"></asp:Label></td>
                <td style="width: 131px; height: 21px">
                    <asp:Label ID="SvcFr" runat="server" Font-Size="Smaller" Text="Label"></asp:Label></td>
                <td style="width: 56px; height: 21px">
                    <asp:Label ID="Label17" runat="server" Font-Size="Smaller" Text="To"></asp:Label></td>
                <td style="width: 149px; height: 21px">
                    <asp:Label ID="SvcTo" runat="server" Font-Size="Smaller" Text="Label"></asp:Label></td>
                <td style="width: 100px; height: 21px">
                </td>
            </tr>
            <tr>
                <td style="width: 100px; height: 21px">
                </td>
                <td style="width: 100px; height: 21px">
                </td>
                <td style="width: 111px; height: 21px">
                    <asp:Label ID="Label24" runat="server" Font-Size="Smaller" Text="Service Date"></asp:Label></td>
                <td style="width: 45px; height: 21px">
                    <asp:Label ID="Label12" runat="server" Font-Size="Smaller" Text="From"></asp:Label></td>
                <td style="width: 131px; height: 21px">
                    <asp:Label ID="SvcDateFr" runat="server" Font-Size="Smaller" Text="Label"></asp:Label></td>
                <td style="width: 56px; height: 21px">
                    <asp:Label ID="Label18" runat="server" Font-Size="Smaller" Text="To"></asp:Label></td>
                <td style="width: 149px; height: 21px">
                    <asp:Label ID="SvcDateTo" runat="server" Font-Size="Smaller" Text="Label"></asp:Label></td>
                <td style="width: 100px; height: 21px">
                </td>
            </tr>
            <tr>
                <td style="width: 100px; height: 21px">
                </td>
                <td style="width: 100px; height: 21px">
                </td>
                <td style="width: 111px; height: 21px">
                    <asp:Label ID="Label25" runat="server" Font-Size="Smaller" Text="Technician ID"></asp:Label></td>
                <td style="width: 45px; height: 21px">
                    <asp:Label ID="Label13" runat="server" Font-Size="Smaller" Text="From"></asp:Label></td>
                <td style="width: 131px; height: 21px">
                    <asp:Label ID="TechFr" runat="server" Font-Size="Smaller" Text="Label"></asp:Label></td>
                <td style="width: 56px; height: 21px">
                    <asp:Label ID="Label19" runat="server" Font-Size="Smaller" Text="To"></asp:Label></td>
                <td style="width: 149px; height: 21px">
                    <asp:Label ID="TechTo" runat="server" Font-Size="Smaller" Text="Label"></asp:Label></td>
                <td style="width: 100px; height: 21px">
                </td>
            </tr>
            <tr>
                <td style="width: 100px; height: 21px">
                </td>
                <td style="width: 100px; height: 21px">
                </td>
                <td style="width: 111px; height: 21px">
                    <asp:Label ID="Label26" runat="server" Font-Size="Smaller" Text="Price ID"></asp:Label></td>
                <td style="width: 45px; height: 21px">
                    <asp:Label ID="Label14" runat="server" Font-Size="Smaller" Text="From"></asp:Label></td>
                <td style="width: 131px; height: 21px">
                    <asp:Label ID="PriceFr" runat="server" Font-Size="Smaller" Text="Label"></asp:Label></td>
                <td style="width: 56px; height: 21px">
                    <asp:Label ID="Label20" runat="server" Font-Size="Smaller" Text="To"></asp:Label></td>
                <td style="width: 149px; height: 21px">
                    <asp:Label ID="PriceTo" runat="server" Font-Size="Smaller" Text="Label"></asp:Label></td>
                <td style="width: 100px; height: 21px">
                </td>
            </tr>
        </table>
        <br />
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False">
            <Columns>
                <asp:TemplateField HeaderText="No">
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" />
                    <ItemStyle Font-Size="Smaller" Width="35px" />
                    <ItemTemplate>
                        <%# Container.DataItemIndex + 1 %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="MTCH_ENAME" HeaderText="Technician" ReadOnly="True">
                    <ItemStyle Font-Size="Smaller" Width="150px" />
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="MROU_MODID" HeaderText="Model" ReadOnly="True">
                    <ItemStyle Font-Size="Smaller" Width="60px" />
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="MROU_SERNO" HeaderText="Serial No" ReadOnly="True">
                    <ItemStyle Font-Size="Smaller" Width="50px" />
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="MROU_INSDT" HeaderText="Install Date" ReadOnly="True">
                    <ItemStyle Font-Size="Smaller" Width="60px" />
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="MROU_LSVDT" HeaderText="Service Date" ReadOnly="True">
                    <ItemStyle Font-Size="Smaller" Width="60px" />
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="WARR1" HeaderText="Warranty 1" ReadOnly="True">
                    <ItemStyle Font-Size="Smaller" Width="60px" />
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="WARR2" HeaderText="Warranty 2" ReadOnly="True">
                    <ItemStyle Font-Size="Smaller" Width="60px" />
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="WARR3" HeaderText="Warranty 3" ReadOnly="True">
                    <ItemStyle Font-Size="Smaller" Width="60px" />
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="WARR4" HeaderText="Warranty 4" ReadOnly="True">
                    <ItemStyle Font-Size="Smaller" Width="60px" />
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="WARR5" HeaderText="Warranty 5" ReadOnly="True">
                    <ItemStyle Font-Size="Smaller" Width="60px" />
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="WARR6" HeaderText="Warranty 6" ReadOnly="True">
                    <ItemStyle Font-Size="Smaller" Width="60px" />
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="WAC" HeaderText="W/A/C" ReadOnly="True">
                    <ItemStyle Font-Size="Smaller" Width="40px" />
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="AGING" HeaderText="P. Aging (Mnth)" ReadOnly="True">
                    <ItemStyle Font-Size="Smaller" Width="50px" />
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="FIV1_SVBIL" HeaderText="Service Bill" ReadOnly="True">
                    <ItemStyle Font-Size="Smaller" Width="45px" />
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="MCUS_ENAME" HeaderText="Customer Name" ReadOnly="True">
                    <ItemStyle Font-Size="Smaller" Width="150px" />
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="FIV1_SVTID" HeaderText="Job Type" ReadOnly="True">
                    <ItemStyle Font-Size="Smaller" Width="50px" />
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="FIV2_TOTAM" HeaderText="Total Amt" ReadOnly="True">
                    <ItemStyle Font-Size="Smaller" Width="25px" />
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" />
                </asp:BoundField>
            </Columns>
            <EditRowStyle Font-Size="Smaller" />
        </asp:GridView>
    
    </div>
    </form>
</body>
</html>
