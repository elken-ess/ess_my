Imports System
Imports System.Configuration
Imports System.Data

Imports BusinessEntity

Namespace PresentationLayer.report
    Partial Class PresentationLayerReportRptServBill2View
        Inherits Page

#Region "Declarations"
        Private ReadOnly _datestyle As Globalization.CultureInfo = New Globalization.CultureInfo("en-CA")
        Private _servBill As New ClsServBillReport()
        Private _clsReport As New ClsCommonReport
        Private _errorLog As ArrayList = New ArrayList
        Private _writeErrLog As New clsLogFile()

#End Region

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
            Threading.Thread.CurrentThread.CurrentCulture = _datestyle

            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Const script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Const script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try

            _servBill = CType(Session("rptServBill"), ClsServBillReport)

            Dim servBillDataSet As DataSet = _servBill.GetServiceBillReport()
            Dim reportType As String = Request.QueryString("report")

            ' Modified by Ryan Estandarte
            'If reportType = "details" Then
            '    PopulateDetailsReport(servBillDataSet.Tables(0), _servBill)
            'ElseIf reportType = "payment" Then
            '    PopulatePaymentReport(servBillDataSet.Tables(0), _servBill)
            'End If

            Select Case reportType
                Case "details"
                    PopulateDetailsReport(servBillDataSet.Tables(0), _servBill)
                Case "payment"
                    PopulatePaymentReport(servBillDataSet.Tables(0), _servBill)
                Case "credit"
                    PopulateCreditReport(servBillDataSet.Tables(0), _servBill)
            End Select

        End Sub

        ''' <remarks>Added by Ryan Estandarte 19 Sept 2012</remarks>
        Private Sub PopulateCreditReport(ByVal reportDataTable As DataTable, ByVal servBill As ClsServBillReport)
            Dim xml As New clsXml
            xml.XmlFile = ConfigurationManager.AppSettings("XmlFilePath")
            Dim strHead As String() = CType(Array.CreateInstance(GetType(String), 22), String())

            PopulateReportLabels(xml, strHead, servBill)

            Dim ds As New DataSet
            ds.Tables.Add(reportDataTable.Copy())
            GenerateReport(strHead, ds, "RPTServBill_Credit.rpt")
        End Sub

        Private Sub PopulatePaymentReport(ByVal reportDataTable As DataTable, ByVal servBill As ClsServBillReport)
            Dim xml As New clsXml
            xml.XmlFile = ConfigurationManager.AppSettings("XmlFilePath")
            Dim strHead As String() = CType(Array.CreateInstance(GetType(String), 22), String())

            PopulateReportLabels(xml, strHead, servBill)

            Dim ds As New DataSet
            ds.Tables.Add(reportDataTable.Copy())
            GenerateReport(strHead, ds, "RPTServBill_Payment.rpt")
        End Sub

        Private Sub PopulateDetailsReport(ByVal reportDataTable As DataTable, ByVal servBill As ClsServBillReport)
            Dim xml As New clsXml
            xml.XmlFile = ConfigurationManager.AppSettings("XmlFilePath")
            Dim strHead As String() = CType(Array.CreateInstance(GetType(String), 22), String())

            PopulateReportLabels(xml, strHead, servBill)

            Dim ds As New DataSet
            ds.Tables.Add(reportDataTable.Copy())
            GenerateReport(strHead, ds, "RPTServBill_Details.rpt")
        End Sub

        Private Sub PopulateReportLabels(ByVal xml As clsXml, ByVal strHead As String(), ByVal servBill As ClsServBillReport)
            strHead(0) = xml.GetLabelName("EngLabelMsg", "SBA-SERVICECENTER")
            strHead(1) = "From"
            strHead(2) = "To"

            If String.IsNullOrEmpty(servBill.FromSvcID) Then
                strHead(3) = "AAA"
            Else
                strHead(3) = servBill.FromSvcID
            End If

            If String.IsNullOrEmpty(servBill.ToSvcID) Then
                strHead(4) = "ZZZ"
            Else
                strHead(4) = servBill.ToSvcID
            End If

            strHead(5) = xml.GetLabelName("EngLabelMsg", "BB-TECHID")
            If String.IsNullOrEmpty(servBill.FromTechID) Then
                strHead(6) = "AAA"
            Else
                strHead(6) = servBill.FromTechID
            End If

            If String.IsNullOrEmpty(servBill.ToTechID) Then
                strHead(7) = "ZZZ"
            Else
                strHead(7) = servBill.ToTechID
            End If

            strHead(8) = xml.GetLabelName("EngLabelMsg", "BB-CUSTID")
            If String.IsNullOrEmpty(servBill.FromCustID) Then
                strHead(9) = "AAA"
            Else
                strHead(9) = servBill.FromCustID
            End If

            If String.IsNullOrEmpty(servBill.ToCustID) Then
                strHead(10) = "ZZZ"
            Else
                strHead(10) = servBill.ToCustID
            End If

            strHead(11) = xml.GetLabelName("EngLabelMsg", "RPT_SERBITECH_INVDATE")
            If String.IsNullOrEmpty(servBill.StartInvDate) Then
                strHead(12) = "AAA"
            Else
                strHead(12) = servBill.StartInvDate
            End If

            If String.IsNullOrEmpty(servBill.EndInvDate) Then
                strHead(13) = "ZZZ"
            Else
                strHead(13) = servBill.EndInvDate
            End If

            strHead(14) = xml.GetLabelName("EngLabelMsg", "SBA-INVOICENO")
            If String.IsNullOrEmpty(servBill.FromServBillNo) Then
                strHead(15) = "AAA"
            Else
                strHead(15) = servBill.FromServBillNo
            End If

            If String.IsNullOrEmpty(servBill.ToServBillNo) Then
                strHead(16) = "ZZZ"
            Else
                strHead(16) = servBill.ToServBillNo
            End If

            strHead(17) = xml.GetLabelName("EngLabelMsg", "RPT-REPORTTYPE")
            strHead(18) = servBill.ReportType

            titleLab.Text = xml.GetLabelName("EngLabelMsg", "RPT-SERVBILLREPORT")

            strHead(19) = "Elken Group of Companies"
            strHead(20) = xml.GetLabelName("EngLabelMsg", "RPT-SERVBILLREPORT")
            strHead(21) = Session("userID").ToString() + " / " + Session("username").ToString()

        End Sub

        Private Sub GenerateReport(ByVal strHead As Array, ByVal dataSet As DataSet, ByVal reportName As String)
            Try
                With _clsReport
                    .ReportFileName = reportName
                    .SetReport(CrystalReportViewer1, dataSet, strHead)

                End With
            Catch ex As Exception
                _errorLog.Add("Error").ToString()
                _errorLog.Add(ex.Message).ToString()
                _writeErrLog.ErrorLog(Session("username").ToString(), _errorLog.Item(1).ToString, "GenerateReport()", _writeErrLog.SELE, "RPTServBill2_VIEW.aspx.vb")
                _errorLog.Add("Error").ToString()
                _errorLog.Add(_clsReport.GetMessage).ToString()
                _writeErrLog.ErrorLog(Session("username").ToString(), _errorLog.Item(1).ToString, "GenerateReport()", _writeErrLog.SELE, "RPTServBill2_VIEW.aspx.vb")

            End Try
        End Sub


        Protected Sub Page_Unload(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Unload
            _clsReport.UnloadReport()
            CrystalReportViewer1.Dispose()
        End Sub
    End Class
End Namespace