﻿Imports CrystalDecisions.CrystalReports.Engine
Imports System.Globalization
Imports BusinessEntity
Imports System.Data

Partial Class PresentationLayer_report_SystemAccessReportView
    Inherits System.Web.UI.Page

    Dim objXML As New clsXml
    Dim clsSAR As New clsSystemAccessReport
    Dim callListReportDoc As New ReportDocument()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim script As String = "top.location='../logon.aspx';"
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                    'Response.Redirect("~/PresentationLayer/logon.aspx")
                End If
            Catch ex As Exception
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try

            If Not SetUserPurview() Then
                'Response.Redirect("~/PresentationLayer/logon.aspx")
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
            objXML.XmlFile = ConfigurationManager.AppSettings("XmlFilePath")
            Me.titleLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_TITLE")
            'Me.backLink.Text = objXML.GetLabelName("EngLabelMsg", "BB-BACK")
            'backLink.Attributes("onclick") = "javascript:history.back();"
        End If
        LoadInfo()
    End Sub

    Private Sub LoadInfo()
        'Dim dataStyle As CultureInfo = New CultureInfo("en-CA")
        'System.Threading.Thread.CurrentThread.CurrentCulture = dataStyle

        'Dim dsReport As DataSet = Session.Contents("SystemAccessReport")

        'Dim strFHour, strFMin, strFSecond, strTHour, strTMin, strTSecond As String

        'strFHour = Request.Params("FHour").ToString()
        'strFMin = Request.Params("FMin").ToString()
        'strFSecond = Request.Params("FSecond").ToString()
        'strTHour = Request.Params("THour").ToString()
        'strTMin = Request.Params("TMin").ToString()
        'strTSecond = Request.Params("TSecond").ToString()


        clsSAR.FromServiceCenter = Session("FServerCenter_SystemAccessReport")
        clsSAR.ToServiceCenter = Session("TServerCenter_SystemAccessReport")
        clsSAR.FromUserID = Session("FUserID_SystemAccessReport")
        clsSAR.ToUserID = Session("TUserID_SystemAccessReport")
        clsSAR.FromDate = Session("FDate_SystemAccessReport")
        clsSAR.ToDate = Session("TDate_SystemAccessReport")
        clsSAR.FromLoginTime = Session("FTime_SystemAccessReport")
        clsSAR.ToLoginTime = Session("TTime_SystemAccessReport")
        clsSAR.FromPCID = Session("FPCID_SystemAccessReport")
        clsSAR.ToPCID = Session("TPCID_SystemAccessReport")        
        clsSAR.LoginOutTime = Session("LoginOutTime_SystemAccessReport")
        clsSAR.LoginUserID = Session("userID").ToString

        Dim dsReport As DataSet = clsSAR.RetriveSystemAccessInfo() 'Session.Contents("AuditLogReport")
        If dsReport Is Nothing Then
            objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
            ErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_FUN_CALLLIST_RERR")
            ErrLab.Visible = True
            Return
        End If
        If dsReport.Tables.Count = 0 Then
            objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
            ErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_FUN_CALLLIST_RERR")
            ErrLab.Visible = True
            Return
        End If

        Dim i As Integer
        If dsReport.Tables.Count > 0 Then
            For i = 0 To dsReport.Tables(0).Rows.Count - 1
                dsReport.Tables(0).Rows(i).Item(0) = i + 1
            Next
        End If

        Try
            callListReportDoc.Load(MapPath("SystemAccessCrystalReport.rpt"))
            ErrLab.Visible = False
        Catch ex As Exception
            objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
            ErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_FUN_CALLLIST_LOADFAILED")
            ErrLab.Visible = True
            Return
        End Try

        callListReportDoc.SetDataSource(dsReport.Tables(0))

        objXML.XmlFile = ConfigurationManager.AppSettings("XmlFilePath")
        callListReportDoc.SetParameterValue("NO", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_NO"))
        callListReportDoc.SetParameterValue("UserName", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_UN"))
        callListReportDoc.SetParameterValue("UserID", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_UI"))
        callListReportDoc.SetParameterValue("LoginDate", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_LG"))
        callListReportDoc.SetParameterValue("OutDate", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_LO"))
        callListReportDoc.SetParameterValue("PCName", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_PCN"))
        callListReportDoc.SetParameterValue("IPAddress", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_IPA"))
        callListReportDoc.SetParameterValue("ReportIDAndUser", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_PRITITLE") + _
                                            " / " + Session("userID").ToString + " / " + Session("userName").ToString)
        callListReportDoc.SetParameterValue("ReportTitle", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_TITLE"))
        callListReportDoc.SetParameterValue("CompanyName", objXML.GetLabelName("EngLabelMsg", "BB_REPORT_COMPANYNAME"))

        callListReportDoc.SetParameterValue("FromServiceCenterLab", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_FSC"))
        callListReportDoc.SetParameterValue("ToServiceCenterLab", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_TSC"))
        callListReportDoc.SetParameterValue("FromServiceCenterBox", dsReport.Tables(1).Rows(0).Item("FromServiceCenter"))
        callListReportDoc.SetParameterValue("ToServiceCenterBox", dsReport.Tables(2).Rows(0).Item("ToServiceCenter"))
        callListReportDoc.SetParameterValue("FromUserIDLab", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_FUI"))
        callListReportDoc.SetParameterValue("ToUserIDLab", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_TUI"))
        callListReportDoc.SetParameterValue("FromUserIDBox", dsReport.Tables(3).Rows(0).Item("FromUserID"))
        callListReportDoc.SetParameterValue("ToUserIDBox", dsReport.Tables(4).Rows(0).Item("ToUserID"))
        callListReportDoc.SetParameterValue("FromDateLab", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_FD"))
        callListReportDoc.SetParameterValue("ToDateLab", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_TD"))
        callListReportDoc.SetParameterValue("FromDateBox", dsReport.Tables(5).Rows(0).Item("FromDate"))
        callListReportDoc.SetParameterValue("ToDateBox", dsReport.Tables(6).Rows(0).Item("ToDate"))
        callListReportDoc.SetParameterValue("FromLoginTimeLab", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_FLT"))
        callListReportDoc.SetParameterValue("ToLoginTimeLab", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_TLT"))
        callListReportDoc.SetParameterValue("FromLoginTimeBox", Session("FTime_SystemAccessReport"))
        callListReportDoc.SetParameterValue("ToLoginTimeBox", Session("TTime_SystemAccessReport"))
        callListReportDoc.SetParameterValue("FromPCIDLab", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_FPI"))
        callListReportDoc.SetParameterValue("ToPCIDLab", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_TPI"))
        callListReportDoc.SetParameterValue("FromPCIDBOX", dsReport.Tables(7).Rows(0).Item("FromPCID"))
        callListReportDoc.SetParameterValue("ToPCIDBOX", dsReport.Tables(8).Rows(0).Item("ToPCID"))
        callListReportDoc.SetParameterValue("LogOutTimeLab", objXML.GetLabelName("EngLabelMsg", "BB_FUN_SAR_LDT"))
        callListReportDoc.SetParameterValue("LogOutTimeBOX", dsReport.Tables(9).Rows(0).Item("LoginTime"))

        CrystalReportViewer.HasCrystalLogo = False
        CrystalReportViewer.ReportSource = callListReportDoc
    End Sub

    Private Function SetUserPurview() As Boolean
        Return clsUserAccessGroup.GetUserPurview(Session("accessgroup"), "52", "P")
    End Function

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        callListReportDoc.Dispose()
        CrystalReportViewer.Dispose()
    End Sub
End Class
