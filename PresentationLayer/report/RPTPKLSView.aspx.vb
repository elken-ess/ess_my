Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Web
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web
Imports BusinessEntity
Imports System.Data
Partial Class PresentationLayer_report_RptPKLSVIEW
    Inherits System.Web.UI.Page
    Dim clsReport As New ClsCommonReport
    Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try

        Dim objRptPkls As New clsRptPKLS

        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        objRptPkls = Session("RptPKLS")
        Session.Remove("RptPKLS")

        Dim ds As DataSet = Nothing
        ds = objRptPkls.GetPackingListSummaryReport

        Dim objXmlTr As New clsXml
        If ds.Tables(0).Rows.Count <= 0 Then

            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            Dim lstrMessage = objXmlTr.GetLabelName("StatusMessage", "NoData")
            Dim script As String = "alert('" & lstrMessage & "');window.close()"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)

        Else


            Dim strHead As Array = Array.CreateInstance(GetType(String), 45)
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB_RPTPKLS_TITLE")
            'HyperLink1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Cancel")
            strHead(0) = "RPTPKLS"
            strHead(1) = objRptPkls.UserID
            strHead(2) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTMDSLHD8")
            strHead(3) = titleLab.Text
            strHead(4) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTPSLT-0002")

            strHead(5) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTPACKHD0")
            strHead(6) = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCAT-0001")
            strHead(7) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTPACKHD2")

            strHead(8) = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-STOCK-14")
            strHead(9) = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_F_PRFQT")
            strHead(10) = objXmlTr.GetLabelName("EngLabelMsg", "BB_RPTPKLS_SHORTAGE")

            strHead(11) = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-STOCK-07")
            strHead(12) = IIf(objRptPkls.PrfNoFrom = "", "AAA", objRptPkls.PrfNoFrom)
            strHead(13) = IIf(objRptPkls.PrfNoTo = "", "AAA", objRptPkls.PrfNoTo)

            strHead(14) = ""
            strHead(15) = ""

            strHead(16) = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0012")
            strHead(17) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTPACKHD7")
            strHead(18) = objXmlTr.GetLabelName("EngLabelMsg", "PRF_COLLECTIONDATE")
            strHead(19) = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-STOCK-08")
            strHead(20) = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0014")
            strHead(21) = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0011")
            strHead(22) = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCAT-0001")
            strHead(23) = objXmlTr.GetLabelName("EngLabelMsg", "BB_RPTBDPT_ONLYPKL")

            strHead(24) = objXmlTr.GetLabelName("EngLabelMsg", "BB-SORTBY")
            strHead(25) = objXmlTr.GetLabelName("EngLabelMsg", "BB-THENBY")
            strHead(26) = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            strHead(27) = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")

            strHead(28) = IIf(objRptPkls.TechnicianFrom = "", "AAA", objRptPkls.TechnicianFrom)
            strHead(29) = IIf(objRptPkls.TechnicianTo = "", "AAA", objRptPkls.TechnicianTo)
            strHead(30) = objRptPkls.PklDateFrom
            strHead(31) = objRptPkls.PklDateTo
            strHead(32) = objRptPkls.CollectionDateFrom
            strHead(33) = objRptPkls.CollectionDateTo
            strHead(34) = objRptPkls.PrfDateFrom
            strHead(35) = objRptPkls.PrfDateTo
            strHead(36) = IIf(objRptPkls.ServiceCenterFrom = "", "AAA", objRptPkls.ServiceCenterFrom)
            strHead(37) = objRptPkls.ServiceCenterTo
            strHead(38) = IIf(objRptPkls.StateFrom = "", "AAA", objRptPkls.StateFrom)
            strHead(39) = objRptPkls.StateTo
            strHead(40) = IIf(objRptPkls.PartFrom = "", "AAA", objRptPkls.PartFrom)
            strHead(41) = objRptPkls.PartTo
            strHead(42) = objRptPkls.OnlyPklText
            strHead(43) = objRptPkls.SortByText
            strHead(44) = objRptPkls.ThenByText

            Dim lstrPdfFileName As String = "PackingListSummaryReport_" & Session("login_session") & ".pdf"

            Try
                With clsReport
                    .ReportFileName = "RPTPKLS.rpt"
                    .SetReportDocument(ds, strHead)
                    .PdfFileName = lstrPdfFileName
                    .ExportPdf()
                End With

                Response.Redirect(clsReport.PdfUrl)

            Catch err As Exception
                'System.IO.File.Delete(lstrPhysicalFile)
                Response.Write("<BR>")
                Response.Write(err.Message.ToString)

            End Try
        End If


    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        clsReport.UnloadReport()
        CrystalReportViewer1.Dispose()
    End Sub
End Class
