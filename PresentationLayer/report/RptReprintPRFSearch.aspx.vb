Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Web
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web
Imports BusinessEntity
Imports System.Data
Partial Class PresentationLayer_report_RptReprintPRFSearch
    Inherits System.Web.UI.Page
    Dim clsReport As New ClsCommonReport

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try

        Dim packEntity As New ClsRptReprintPRF

        If Trim(Session("userID")) = "" Then
            packEntity.ModifiedBy = "ADMIN"
        Else
            packEntity.ModifiedBy = Session("userID")
        End If
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        packEntity = Session("rptreprintprf_search_condition")
        Dim ds As DataSet = Nothing
        ds = packEntity.GetPack()

        Dim dsDetails As DataSet = Nothing
        dsDetails = packEntity.GetDetails()

        Dim objXmlTr As New clsXml


        Dim strHead As Array = Array.CreateInstance(GetType(String), 42)
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        'HyperLink1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Cancel")
        strHead(0) = packEntity.IsoDesc1
        strHead(1) = packEntity.IsoDesc2
        strHead(2) = packEntity.Col1

        If Trim(packEntity.servcenterid) <> "" Then
            strHead(21) = packEntity.servcenterid
        Else
            strHead(21) = "".ToString
        End If
        If Trim(packEntity.datemin) <> "" Then
            strHead(22) = packEntity.datemin
        Else
            strHead(22) = "".ToString
        End If
        If Trim(packEntity.tecnicianidbegin) <> "" Then
            strHead(23) = packEntity.tecnicianidbegin
        Else
            strHead(23) = "".ToString
        End If

        If Trim(packEntity.prfno) <> "" Then
            strHead(36) = packEntity.prfno
        Else
            strHead(36) = "AAA".ToString
        End If

        'End With
        ' Modified by Ryan Estandarte 21 Sept 2012
        'Dim lstrPdfFileName As String = "PrintPRFReport_" & Session("login_session") & ".pdf"
        Dim lstrPdfFileName As String = "PrintPRFReport_" & Session("login_session").ToString() & "_" & DateTime.Now.ToString("ddMMyyyyhhmm") & ".pdf"

        With clsReport
            .ReportFileName = "CrystPrintPRF.rpt"
            .SetReportDocument(ds, strHead)
            .PdfFileName = lstrPdfFileName
            .ExportPdf()
        End With

        Response.Redirect(clsReport.PdfUrl)

        Try

        Catch err As Exception
            'System.IO.File.Delete(lstrPhysicalFile)
            Response.Write("<BR>")
            Response.Write(err.Message.ToString)

        End Try

    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        clsReport.UnloadReport()
        CrystalReportViewer1.Dispose()
    End Sub
End Class
