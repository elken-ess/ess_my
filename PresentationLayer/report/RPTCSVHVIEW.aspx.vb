﻿Imports SQLDataAccess
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
Imports BusinessEntity
Imports CrystalDecisions.Web
Imports CrystalDecisions.CrystalReports.Engine
Partial Class PresentationLayer_report_RPTCSVHVIEW
    Inherits System.Web.UI.Page
    Dim clsReport As New ClsCommonReport

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If

        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try
      
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")

        Dim rptcsvh As New clsRPTCSVH
        rptcsvh = Session("rptcsvh_search_condition")
        If Trim(Session("userID")) = "" Then
            rptcsvh.UserName = "  "
        Else
            rptcsvh.UserName = Session("userID")
        End If


        Dim rptds As DataSet = rptcsvh.GetServiceOrderSum()


        If rptds.Tables(0).Rows.Count <= 0 Then

            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            Dim lstrMessage = objXmlTr.GetLabelName("StatusMessage", "NoData")
            Dim script As String = "alert('" & lstrMessage & "');window.close()"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
        Else

            ' switch  encrypt to telphone for display        
            Dim count As Integer
            Dim prdctclname As String
            Dim clscomm As New clsCommonClass()

            If Not rptds Is Nothing Then

                If rptds.Tables(0).Rows.Count > 0 Then
                    For count = 0 To rptds.Tables(0).Rows.Count - 1
                        Dim prdctclsid As String = rptds.Tables(0).Rows(count).Item(2).ToString 'item(8) is product class 
                        prdctclname = clscomm.passconverttel(prdctclsid)
                        If prdctclname <> "" Then
                            rptds.Tables(0).Rows(count).Item(2) = "HP--" & prdctclname
                        End If
                        'rptds.Tables(0).Rows(count).Item(2) = ""
                    Next
                End If
            End If

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

            Me.titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCHL-0022")
            Dim strHead As Array = Array.CreateInstance(GetType(String), 50)

            strHead(0) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCHL-0023")
            strHead(1) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCHL-0001")
            strHead(2) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCHL-0002")
            strHead(3) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCHL-0003")
            strHead(4) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCHL-0026")
            strHead(5) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCHL-0004")

            strHead(6) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCHL-0013")
            strHead(7) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCHL-0005")

            strHead(8) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCHL-0006")
            strHead(9) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCHL-0007")
            strHead(10) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCHL-0008")
            '
            strHead(11) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCHL-0009")
            strHead(12) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCHL-0010")

            strHead(13) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCHL-0011")
            strHead(14) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCHL-0012")
            strHead(15) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCHL-0024")
            strHead(16) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCHL-0022")

            strHead(17) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCHL-0014")
            If Session("userID") = "" Then
                strHead(18) = " "
            Else
                strHead(18) = "/" & Session("userID").ToString().ToUpper + "/" + Session("username").ToString.ToUpper
            End If
            strHead(19) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCHL-0027")
            strHead(20) = "/"
            '  strHead(24) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCUSR-USERID")
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            strHead(21) = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO") 'objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            strHead(22) = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")

            strHead(23) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDSL-0010")
            strHead(24) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDSL-0011")
            strHead(25) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDSL-0012")

            strHead(26) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDSL-0013")
            strHead(27) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_SERBITECH_INVDATE") 'INSTATALL DATE
            strHead(28) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDSL-0015")
            strHead(29) = objXmlTr.GetLabelName("EngLabelMsg", "SBA-SERVICETYPE")
            strHead(30) = objXmlTr.GetLabelName("EngLabelMsg", "BB-SORTBY")
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If (rptcsvh.ContractTy = "") Then
                strHead(31) = "All"
            Else
                strHead(31) = rptcsvh.ContractTy.ToString
            End If

            If rptcsvh.maxSvcID = Nothing Then
                strHead(32) = "AAA"
            ElseIf rptcsvh.maxSvcID = "ZZZZZZZZZZZZZZZZZZZZ" Then
                strHead(32) = "ZZZ"
            Else
                strHead(32) = rptcsvh.maxSvcID.ToString
            End If

            If rptcsvh.minSvcID = Nothing Then
                strHead(33) = "AAA"
            Else
                strHead(33) = rptcsvh.minSvcID.ToString
            End If

            If rptcsvh.maxCustmID = Nothing Then
                strHead(34) = "AAA"
            ElseIf rptcsvh.maxCustmID = "ZZZZZZZZZZZZZZZZZZZZ" Then
                strHead(34) = "ZZZ"
            Else
                strHead(34) = rptcsvh.maxCustmID.ToString
            End If

            If rptcsvh.minCustmID = Nothing Then
                strHead(35) = "AAA"
            Else
                strHead(35) = rptcsvh.minCustmID.ToString
            End If
            If rptcsvh.maxRONo = Nothing Then
                strHead(36) = "AAA"
            ElseIf rptcsvh.maxRONo = "ZZZZZZZZZZZZZZZZZZZZ" Then
                strHead(36) = "ZZZ"
            Else
                strHead(36) = rptcsvh.maxRONo.ToString
            End If
            If rptcsvh.minRONo = Nothing Then
                strHead(37) = "AAA"
            Else
                strHead(37) = rptcsvh.minRONo.ToString
            End If
            If rptcsvh.maxPrdctMd = Nothing Then
                strHead(38) = "AAA"
            ElseIf rptcsvh.maxPrdctMd = "ZZZZZZZZZZZZZZZZZZZZ" Then
                strHead(38) = "ZZZ"
            Else
                strHead(38) = rptcsvh.maxPrdctMd.ToString
            End If
            If rptcsvh.minPrdctMd = Nothing Then
                strHead(39) = "AAA"
            Else
                strHead(39) = rptcsvh.minPrdctMd.ToString
            End If
            If rptcsvh.maxState = Nothing Then
                strHead(40) = "AAA"
            ElseIf rptcsvh.maxState = "ZZZZZZZZZZZZZZZZZZZZ" Then
                strHead(40) = "ZZZ"
            Else
                strHead(40) = rptcsvh.maxState.ToString
            End If
            If rptcsvh.minState = Nothing Then
                strHead(41) = "AAA"
            Else
                strHead(41) = rptcsvh.minState.ToString
            End If
            strHead(42) = rptcsvh.maxdate
            strHead(43) = rptcsvh.mindate
            strHead(44) = rptcsvh.SortByText

            If Not rptds Is Nothing Then
                If (rptds.Tables(0).Rows.Count <> 0) Then
                    Dim strtotal As String = objXmlTr.GetLabelName("EngLabelMsg", "RPT_APT_RECORD")
                    strHead(45) = strtotal & ": " & rptds.Tables(0).Rows(0).Item("totalrecord")
                End If

                strHead(46) = objXmlTr.GetLabelName("EngLabelMsg", "SBA-SERVICEBILLTYPE")
                strHead(47) = rptcsvh.ServiceBillTypeText


                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''             

                strHead(18) = Session("userID")
               

                Dim lstrPdfFileName As String = "CustomerServiceHistory_" & Session("login_session") & ".pdf"

                Try
                    With clsReport
                        .ReportFileName = "RPTCSVH.rpt"
                        .SetReportDocument(rptds, strHead)
                        .PdfFileName = lstrPdfFileName
                        .ExportPdf()
                    End With

                    Response.Redirect(clsReport.PdfUrl)

                Catch err As Exception
                    'System.IO.File.Delete(lstrPhysicalFile)
                    Response.Write("<BR>")
                    Response.Write(err.Message.ToString)

                End Try


            End If
        End If
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        clsReport.UnloadReport()
        CrystalReportViewer1.Dispose()
    End Sub
End Class
