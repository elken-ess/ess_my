﻿Imports BusinessEntity
Imports System.Configuration
Imports System.Xml
Imports System.Data
Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web
Partial Class PresentationLayer_report_RPTTSKH_SUB
    Inherits System.Web.UI.Page
    Dim clsReport As New ClsCommonReport

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try

            Dim RptTSKH As New ClsRptTSKH
            RptTSKH = Session("rptTSKH_search_condition")
            Dim masds As DataSet = Nothing
            masds = RptTSKH.GetRptmas_TSKH()


            Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

            Dim obiXML As New clsXml
            obiXML.XmlFile = ConfigurationManager.AppSettings("XmlFilePath")
            Me.titleLab.Text = obiXML.GetLabelName("EngLabelMsg", "RPT_TSKH_TITLE")


            Dim date1 As String = RptTSKH.Mindate

            Dim date2 As String = RptTSKH.Maxdate



            Dim objXmlTr As New clsXml
            Dim strHead As Array = Array.CreateInstance(GetType(String), 30)
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            strHead(0) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_TSKH_PARD")
            strHead(1) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_TSKH_SSL")
            strHead(2) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_TSKH_SOTD")
            strHead(3) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_TSKH_ASOH")
            strHead(4) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_TSKH_PLQ")
            strHead(5) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_TSKH_PS")
            strHead(6) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_TSKH_NO")
            strHead(7) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_TSKH_DT")
            strHead(8) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_TSKH_QUAN")
            strHead(9) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_TSKH_ACCU")
            strHead(10) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_TSKH_TRTY")
            strHead(11) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_TSKH_TRNO")
            strHead(12) = objXmlTr.GetLabelName("EngLabelMsg", "RPT-TSKH-TITLE1")
            strHead(13) = objXmlTr.GetLabelName("EngLabelMsg", "RPT-TSKH-TITLE2")
            strHead(14) = date1
            strHead(15) = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            strHead(16) = date2
            strHead(17) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_TSKH_REPID")

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            strHead(18) = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            strHead(19) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_TSKH_TECHID")
            strHead(20) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_TSKH_SER")
            strHead(21) = objXmlTr.GetLabelName("EngLabelMsg", "RPT_TSKH_DATE")

           

            If (RptTSKH.Mintechid = Nothing) Then
                strHead(22) = "AAA"
            Else

                strHead(22) = RptTSKH.Mintechid
            End If


            If (RptTSKH.Maxtechid = "ZZZZZZZZZZ") Then
                strHead(23) = "ZZZ"
            Else

                strHead(23) = RptTSKH.Maxtechid
            End If


            If (RptTSKH.Minserid = Nothing) Then
                strHead(24) = "AAA"
            Else

                strHead(24) = RptTSKH.Minserid
            End If


            If (RptTSKH.Maxserid = "ZZZZZZZZZZ") Then
                strHead(25) = "ZZZ"
            Else

                strHead(25) = RptTSKH.Maxserid
            End If




            If (RptTSKH.Mindate = Nothing) Then
                strHead(26) = "AAA"
            Else

                strHead(26) = RptTSKH.Mindate
            End If

            If (RptTSKH.Maxdate = "ZZZZZZZZZZ") Then
                strHead(27) = "ZZZ"
            Else

                strHead(27) = RptTSKH.Maxdate
            End If

            If Session("userID") = "" Then
                strHead(28) = " "
            Else
                strHead(28) = Session("userID").ToString().ToUpper + " / " + Session("username").ToString.ToUpper

            End If

            strHead(29) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCUSR-USERID")

            'With clsReport
            '    .ReportFileName = "RPTTSKH.rpt"
            '    .SetReport(CrystalReportViewer1, masds, strHead)
            'End With

            Dim lstrPdfFileName As String = "TechnicianStockOnHandListing_" & Session("login_session") & ".pdf"

            Try
                With clsReport
                    .ReportFileName = "RPTTSKH.rpt"
                    .SetReportDocument(masds, strHead)
                    .PdfFileName = lstrPdfFileName
                    .ExportPdf()
                End With

                Response.Redirect(clsReport.PdfUrl)

            Catch err As Exception
                'System.IO.File.Delete(lstrPhysicalFile)
                Response.Write("<BR>")
                Response.Write(err.Message.ToString)

            End Try


        Catch ex As Exception
            Response.Write(ex.Message)

        End Try
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        clsReport.UnloadReport()
        CrystalReportViewer1.Dispose()
    End Sub
End Class
