Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Web
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web
Imports BusinessEntity
Imports System.Data

Partial Class PresentationLayer_report_ContractRenewalLetter_aspx
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        If Not Page.IsPostBack Then
            Session("CTRNWL-FLAG") = "0"
            Dim clsReport As New ClsCommonReport


            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            Me.coustomlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0001")
            Me.coustomfromlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Me.coustomtolab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Me.sercenterlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0002")
            Me.sercenterfromlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Me.sercentertolab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Me.promodlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0003")
            Me.promodfromlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Me.promodtolab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Me.statelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0004")
            Me.statefromlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Me.statetolab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Me.datelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0023")
            Me.datefromlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Me.datetolab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Me.sernoLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0007")
            Me.sernofromLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")
            Me.sernotoLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            Me.LinkButton1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-VIEWREPORT")
            'Me.LinkButton2.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0005")
            Me.titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0006")

            '''access control'''''''''''''''''''''''''''''''''''''''''''''''
            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "39")
            Me.LinkButton1.Enabled = purviewArray(3)


            Me.coustomfrombox.Text = "AAA"
            Me.sercenterfrombox.Text = "AAA"
            Me.promodfrombox.Text = "AAA"
            Me.statefrombox.Text = "AAA"
            Me.sernofromBox.Text = "AAA"
            Me.coustomtoBox.Text = "ZZZ"
            Me.sercentertoBox.Text = "ZZZ"
            Me.promodtobox.Text = "ZZZ"
            Me.statetobox.Text = "ZZZ"
            Me.sernotoBox.Text = "ZZZ"
            Me.datetobox.Text = Date.Now().ToString().Substring(0, 10)
            Me.datefrombox.Text = "01/01/2006"

        ElseIf Session("CTRNWL-FLAG") = "1" Then
            Print_Report()
        End If


        HypCalFrom.NavigateUrl = "javascript:DoCal(document.form1.datefrombox);"
        HypCalTo.NavigateUrl = "javascript:DoCal(document.form1.datetobox);"


    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton1.Click

        Session("CTRNWL-FLAG") = "1"
        Print_Report()
        'Response.Redirect("~/PresentationLayer/REPORT/ContractRenewalLetterVIEW.aspx")
        Dim script As String = "window.open('../report/ContractRenewalLetterVIEW.aspx')"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "SystemAccessReport", script, True)
    End Sub

    Protected Sub Print_Report()
        Dim crlEntity As New clsRPTCRL()

        datefrombox.Text = Request.Form("dateFrombox")
        datetobox.Text = Request.Form("datetobox")

        If (Trim(Me.coustomfrombox.Text) <> "AAA") Then
            crlEntity.CustmIDFrom = Me.coustomfrombox.Text
        End If
        If (Trim(Me.coustomtoBox.Text) <> "ZZZ") Then
            crlEntity.CustmIDTo = Me.coustomtoBox.Text
        Else : crlEntity.CustmIDTo = "ZZZZZZZZZZ"
        End If
        If (Trim(Me.sercenterfrombox.Text) <> "AAA") Then
            crlEntity.SvcIDFrom = Me.sercenterfrombox.Text
        End If
        If (Trim(Me.sercentertoBox.Text) <> "ZZZ") Then
            crlEntity.SvcIDTo = Me.sercentertoBox.Text
        Else : crlEntity.SvcIDTo = "ZZZZZZZZZZ"
        End If
        If (Trim(Me.promodfrombox.Text) <> "AAA") Then
            crlEntity.ModelIDFrom = Me.promodfrombox.Text
        End If
        If (Trim(Me.promodtobox.Text) <> "ZZZ") Then
            crlEntity.ModelIDTo = Me.promodtobox.Text
        Else : crlEntity.ModelIDTo = "ZZZZZZZZZZ"
        End If

        If (Trim(Me.statefrombox.Text) <> "AAA") Then
            crlEntity.StateFrom = Me.statefrombox.Text
        End If

        If (Trim(Me.statetobox.Text) <> "ZZZ") Then
            crlEntity.StateTo = Me.statetobox.Text
        Else : crlEntity.StateTo = "ZZZZZZZZZZ"
        End If
        If (Trim(sernofromBox.Text) <> "AAA") Then
            crlEntity.SerialNoFrom = Me.sernofromBox.Text
        End If
        If (Trim(Me.sernotoBox.Text) <> "ZZZ") Then
            crlEntity.SerialNoTo = Me.sernotoBox.Text
        Else : crlEntity.SerialNoTo = "ZZZZZZZZZZ"
        End If
        If (Trim(Me.datefrombox.Text) <> "") Then
            Dim temparr As Array = Me.datefrombox.Text.Split("/")
            crlEntity.StartDate = temparr(2) + "-" + temparr(1) + "-" + temparr(0)

        End If
        If (Trim(Me.datetobox.Text) <> "") Then
            Dim temparr As Array = Me.datetobox.Text.Split("/")
            crlEntity.EndDate = temparr(2) + "-" + temparr(1) + "-" + temparr(0)

        End If
        Dim rank As String = Session("login_rank")
        If rank <> 0 Then
            crlEntity.ctrid = Session("login_ctryID").ToString().ToUpper
            crlEntity.comid = Session("login_cmpID").ToString().ToUpper
            crlEntity.svcid = Session("login_svcID").ToString().ToUpper
        End If
        crlEntity.rank = rank
        crlEntity.UserName = Session("username").ToString().ToUpper()
        Try
            'Dim crlds As New DataSet
            'crlds = crlEntity.GetContractRenewalLetter()

            Session("crlds_search_condition") = crlEntity

            'Dim clsReport As New ClsCommonReport
            'Dim objXmlTr As New clsXml
            'Dim strHead As Array = Array.CreateInstance(GetType(String), 19)
            'objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            'strHead(0) = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0010")
            'strHead(1) = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0016")
            'strHead(2) = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0011")
            'strHead(3) = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0012")
            'strHead(4) = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0013")
            'strHead(5) = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0014")
            'strHead(6) = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0023")
            'strHead(7) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RENEWAL-LANG1A")
            'strHead(8) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RENEWAL-LANG1B")
            'strHead(9) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RENEWAL-LANG2A")
            'strHead(10) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RENEWAL-LANG2B")
            'strHead(11) = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0006")
            'strHead(12) = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0017")
            'strHead(13) = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0019")
            'strHead(14) = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0018")
            'strHead(15) = crlds.Tables(0).Rows.Count
            'strHead(16) = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0023")
            'strHead(17) = Convert.ToString(Me.datefrombox.Text) + "     " + "To" + "   " + Convert.ToString(Me.datetobox.Text)
            'strHead(18) = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRL-0021")

            'If Me.IsPostBack Then
            '    With clsReport
            '        .ReportFileName = "ContractRenewalLetter.rpt"  'TextBox1.Text
            '        .SetReport(CrystalReportViewerCRL, crlds, strHead)

            '    End With
            'End If

        Catch ex As Exception
            Response.Redirect("~/PresentationLayer/Error.aspx")

        End Try

    End Sub






    Protected Sub Calendar1_SelectedDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendar1.SelectedDateChanged
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Dim prcidDate As New clsCommonClass()
        datefrombox.Text = Calendar1.SelectedDate.Date.ToString().Substring(0, 10)

    End Sub

    Protected Sub Calendar2_SelectedDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendar2.SelectedDateChanged
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Dim prcidDate As New clsCommonClass()
        datetobox.Text = Calendar2.SelectedDate.Date.ToString().Substring(0, 10)

    End Sub
End Class
