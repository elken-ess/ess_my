<%@ Page Language="VB" AutoEventWireup="false" CodeFile="RPTSVOS_New.aspx.vb" Inherits="PresentationLayer.report.PresentationLayer_report_RPTSVOS_New" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="JCalendar" Namespace="JCalendar" TagPrefix="cc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Service Order Summary (overall)</title>
    <link href="../css/style.css" type="text/css" rel="stylesheet" />
    <link href="../css/default.css" rel="stylesheet" type="text/css" />

    <script language="JavaScript" src="../js/common.js"></script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table id="countrytab" style="width: 100%; border: 0;">
            <tr>
                <td style="width: 100%">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title1.gif" width="5" />
                            </td>
                            <td background="../graph/title_bg.gif" class="style2" width="98%" style="width: 100%">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label>
                            </td>
                            <td align="LEFT" background="../graph/title_bg.gif" width="1%">
                                <img height="1" src="../graph/title_2.gif" width="5" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100%">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" class="style2" width="98%">
                                <font color="red" style="width: 100%">
                                    <asp:Label ID="errlab" runat="server" Text="Label" Visible="false"></asp:Label></font>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100%; height: 1px;">
                    <table id="country" bgcolor="#b7e6e6" border="0" cellpadding="2" cellspacing="1"
                        style="width: 100%">
                        <tr bgcolor="#ffffff">
                            <td align="LEFT" style="width: 24%">
                                <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label>
                            </td>
                            <td align="LEFT" style="width: 45px">
                                <asp:Label ID="Label10" runat="server" Text="Label"></asp:Label>
                            </td>
                            <td align="left" style="width: 34%">
                                <asp:TextBox ID="minsvsbox" runat="server" CssClass="textborder"></asp:TextBox>
                            </td>
                            <td align="LEFT" style="width: 9%">
                                <asp:Label ID="Label13" runat="server" Text="Label"></asp:Label>
                            </td>
                            <td align="left" style="width: 40%">
                                <asp:TextBox ID="maxsvsbox" runat="server" CssClass="textborder"></asp:TextBox>
                            </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="LEFT" style="width: 24%; height: 28px;">
                                <asp:Label ID="Label9" runat="server" Text="Label"></asp:Label>
                            </td>
                            <td align="LEFT" style="width: 45px; height: 28px;">
                                <asp:Label ID="Label22" runat="server" Text="Label"></asp:Label>
                            </td>
                            <td align="left" style="width: 34%; height: 28px;">
                                <asp:TextBox ID="mindatebox" runat="server" CssClass="textborder" ReadOnly="false"
                                    MaxLength="10"></asp:TextBox>
                                <asp:HyperLink ID="HypCalFrom" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                                    ToolTip="Choose a Date">Choose a Date</asp:HyperLink>
                                <cc1:JCalendar ID="JCalendar1" runat="server" ControlToAssign="mindatebox" ImgURL="~/PresentationLayer/graph/calendar.gif"
                                    Visible="false" />
                                &nbsp;
                            </td>
                            <td align="LEFT" style="width: 9%; height: 28px;">
                                <asp:Label ID="Label24" runat="server" Text="Label"></asp:Label>
                            </td>
                            <td align="left" style="width: 40%; height: 28px">
                                <asp:TextBox ID="maxdatebox" runat="server" CssClass="textborder" ReadOnly="false"
                                    MaxLength="10"></asp:TextBox>
                                <asp:HyperLink ID="HypCalTo" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                                    ToolTip="Choose a Date">Choose a Date</asp:HyperLink>
                                <cc1:JCalendar ID="JCalendar2" runat="server" ControlToAssign="maxdatebox" ImgURL="~/PresentationLayer/graph/calendar.gif"
                                    Visible="false" />
                                &nbsp;
                            </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="left" style="width: 24%; height: 28px">
                                <asp:Label ID="Label1" runat="server" Text="Staff Type"></asp:Label></td>
                            <td align="left" style="width: 45px; height: 28px">
                            </td>
                            <td align="left" style="width: 34%; height: 28px">
                                <asp:DropDownList ID="stafftypeddl" Width="80%" runat="server">
                                    <asp:ListItem Selected="True">Technician</asp:ListItem>
                                    <asp:ListItem>Sub-con</asp:ListItem>
                                </asp:DropDownList></td>
                            <td align="left" style="width: 9%; height: 28px">
                            </td>
                            <td align="left" style="width: 40%; height: 28px">
                            </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="LEFT" style="width: 24%; height: 28px">
                                <asp:Label ID="Label6" runat="server" Text="Label"></asp:Label>
                            </td>
                            <td align="LEFT" style="width: 45px; height: 28px">
                                <asp:Label ID="Label19" runat="server" Text="Label"></asp:Label>
                            </td>
                            <td align="left" style="width: 34%; height: 28px">
                                <asp:TextBox ID="mintchnicanbox" runat="server" CssClass="textborder"></asp:TextBox>
                            </td>
                            <td align="LEFT" style="width: 9%; height: 28px">
                                <asp:Label ID="Label20" runat="server" Text="Label"></asp:Label>
                            </td>
                            <td align="left" style="width: 40%; height: 28px">
                                <asp:TextBox ID="maxtchnicanbox" runat="server" CssClass="textborder"></asp:TextBox>
                            </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="LEFT" style="width: 24%; height: 28px">
                                <asp:Label ID="Label8" runat="server" Text="Label"></asp:Label>
                            </td>
                            <td align="LEFT" style="width: 45px; height: 28px">
                                <asp:Label ID="Label21" runat="server" Text="Label"></asp:Label>
                            </td>
                            <td align="left" style="width: 34%; height: 28px">
                                <asp:TextBox ID="minareabox" runat="server" CssClass="textborder"></asp:TextBox>
                            </td>
                            <td align="LEFT" style="width: 9%; height: 28px">
                                <asp:Label ID="Label23" runat="server" Text="Label"></asp:Label>
                            </td>
                            <td align="left" style="width: 40%; height: 28px">
                                <asp:TextBox ID="maxareabox" runat="server" CssClass="textborder"></asp:TextBox>
                            </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="LEFT" style="width: 24%; height: 28px">
                                <asp:Label ID="lblStatus" runat="server" Text="Status"></asp:Label>
                            </td>
                            <td align="LEFT" style="width: 45px; height: 28px">
                            </td>
                            <td align="left" style="width: 34%; height: 28px">
                                <asp:DropDownList ID="cboStatus" Width="80%" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td align="LEFT" style="width: 9%; height: 28px">
                            </td>
                            <td align="left" style="width: 40%; height: 28px">
                            </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="LEFT" style="width: 24%; height: 28px">
                                <asp:Label ID="lblServiceType" runat="server" Text="Service Type"></asp:Label>
                            </td>
                            <td align="LEFT" style="width: 45px; height: 28px">
                            </td>
                            <td align="left" style="width: 34%; height: 28px">
                                <asp:DropDownList ID="cboServiceType" Width="80%" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td align="LEFT" style="width: 9%; height: 28px">
                            </td>
                            <td align="left" style="width: 40%; height: 28px">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 37px; width: 100%;">
                    <table id="Table1" border="0" cellpadding="1" cellspacing="1">
                        <tr>
                            <td align="center" style="width: 20%; height: 4px;">
                                <asp:LinkButton ID="saveButton" runat="server">PDF View</asp:LinkButton>&nbsp;
                            </td>
                            <td align="center" style="height: 4px">
                                <asp:LinkButton ID="ExcelBtn" runat="server">Excel View</asp:LinkButton></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
