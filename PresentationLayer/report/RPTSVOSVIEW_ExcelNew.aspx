<%@ Page Language="VB" AutoEventWireup="false" CodeFile="RPTSVOSVIEW_ExcelNew.aspx.vb" Inherits="PresentationLayer_report_RPTSVOSVIEW_ExcelNew" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Untitled Page</title>
    <script language="javascript" type="text/javascript">
// <!CDATA[

function TABLE1_onclick() {

}
function printpage()
  {
  window.print()
  }
// ]]>
</script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table id="TABLE1" onclick="return TABLE1_onclick()" style="width: 1047px; height: 209px">
            <tr>
                <td align="left" colspan="8" style="height: 25px" valign="bottom">
                    &nbsp;
                    <input id="Button1" onclick="printpage()" size="21" style="color: black; border-top-style: solid;
                        border-right-style: solid; border-left-style: solid; background-color: transparent;
                        border-bottom-style: solid" type="button" value="Print" />
                    <asp:Button ID="Export" runat="server" BackColor="Transparent" BorderStyle="Solid"
                        Text="Export to Excel" />
                    <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="No data is available for selected range of criteria! "
                        Visible="False"></asp:Label></td>
            </tr>
            <tr>
                <td align="center" colspan="8" style="height: 22px" valign="bottom">
                    <asp:Label ID="Label29" runat="server" Font-Size="Medium" Text="Service Order Summary"></asp:Label></td>
            </tr>
            <tr>
                <td align="center" colspan="8" style="height: 22px" valign="bottom">
                </td>
            </tr>
            <tr>
                <td style="width: 100px; height: 8px" valign="bottom">
                </td>
                <td style="width: 100px; height: 8px" valign="bottom">
                </td>
                <td style="width: 111px; height: 8px">
                    <asp:Label ID="Label4" runat="server" Font-Size="Smaller" Text="Staff Type"></asp:Label></td>
                <td style="width: 45px; height: 8px">
                    <asp:Label ID="stafftype" runat="server" Font-Size="Smaller" Text="Label"></asp:Label></td>
                <td style="width: 131px; height: 8px">
                </td>
                <td style="width: 56px; height: 8px">
                </td>
                <td style="width: 149px; height: 8px">
                </td>
                <td style="width: 100px; height: 8px">
                </td>
            </tr>
            <tr>
                <td style="width: 100px; height: 21px">
                </td>
                <td style="width: 100px; height: 21px">
                </td>
                <td style="width: 111px; height: 21px">
                    <asp:Label ID="Label23" runat="server" Font-Size="Smaller" Text="Service Center ID"></asp:Label></td>
                <td style="width: 45px; height: 21px">
                    <asp:Label ID="Label11" runat="server" Font-Size="Smaller" Text="From"></asp:Label></td>
                <td style="width: 131px; height: 21px">
                    <asp:Label ID="svcidfr" runat="server" Font-Size="Smaller" Text="Label"></asp:Label></td>
                <td style="width: 56px; height: 21px">
                    <asp:Label ID="Label17" runat="server" Font-Size="Smaller" Text="To"></asp:Label></td>
                <td style="width: 149px; height: 21px">
                    <asp:Label ID="svcidto" runat="server" Font-Size="Smaller" Text="Label"></asp:Label></td>
                <td style="width: 100px; height: 21px">
                </td>
            </tr>
            <tr>
                <td style="width: 100px; height: 21px">
                </td>
                <td style="width: 100px; height: 21px">
                </td>
                <td style="width: 111px; height: 21px">
                    <asp:Label ID="Label24" runat="server" Font-Size="Smaller" Text="Appointment Date"></asp:Label></td>
                <td style="width: 45px; height: 21px">
                    <asp:Label ID="Label12" runat="server" Font-Size="Smaller" Text="From"></asp:Label></td>
                <td style="width: 131px; height: 21px">
                    <asp:Label ID="apptdtfr" runat="server" Font-Size="Smaller" Text="Label"></asp:Label></td>
                <td style="width: 56px; height: 21px">
                    <asp:Label ID="Label18" runat="server" Font-Size="Smaller" Text="To"></asp:Label></td>
                <td style="width: 149px; height: 21px">
                    <asp:Label ID="apptdtto" runat="server" Font-Size="Smaller" Text="Label"></asp:Label></td>
                <td style="width: 100px; height: 21px">
                </td>
            </tr>
            <tr>
                <td style="width: 100px; height: 21px">
                </td>
                <td style="width: 100px; height: 21px">
                </td>
                <td style="width: 111px; height: 21px">
                    <asp:Label ID="Label25" runat="server" Font-Size="Smaller" Text="Technician ID"></asp:Label></td>
                <td style="width: 45px; height: 21px">
                    <asp:Label ID="Label13" runat="server" Font-Size="Smaller" Text="From"></asp:Label></td>
                <td style="width: 131px; height: 21px">
                    <asp:Label ID="techidfr" runat="server" Font-Size="Smaller" Text="Label"></asp:Label></td>
                <td style="width: 56px; height: 21px">
                    <asp:Label ID="Label19" runat="server" Font-Size="Smaller" Text="To"></asp:Label></td>
                <td style="width: 149px; height: 21px">
                    <asp:Label ID="techidto" runat="server" Font-Size="Smaller" Text="Label"></asp:Label></td>
                <td style="width: 100px; height: 21px">
                </td>
            </tr>
            <tr>
                <td style="width: 100px; height: 21px">
                </td>
                <td style="width: 100px; height: 21px">
                </td>
                <td style="width: 111px; height: 21px">
                    <asp:Label ID="Label2" runat="server" Font-Size="Smaller" Text="Area ID"></asp:Label></td>
                <td style="width: 45px; height: 21px">
                    <asp:Label ID="Label3" runat="server" Font-Size="Smaller" Text="From"></asp:Label></td>
                <td style="width: 131px; height: 21px">
                    <asp:Label ID="areaidfr" runat="server" Font-Size="Smaller" Text="Label"></asp:Label></td>
                <td style="width: 56px; height: 21px">
                    <asp:Label ID="Label5" runat="server" Font-Size="Smaller" Text="To"></asp:Label></td>
                <td style="width: 149px; height: 21px">
                    <asp:Label ID="areaidto" runat="server" Font-Size="Smaller" Text="Label"></asp:Label></td>
                <td style="width: 100px; height: 21px">
                </td>
            </tr>
            <tr>
                <td style="width: 100px; height: 21px">
                </td>
                <td style="width: 100px; height: 21px">
                </td>
                <td style="width: 111px; height: 21px">
                    <asp:Label ID="Label6" runat="server" Font-Size="Smaller" Text="Status"></asp:Label></td>
                <td style="width: 45px; height: 21px">
                    <asp:Label ID="status" runat="server" Font-Size="Smaller" Text="Label"></asp:Label></td>
                <td style="width: 131px; height: 21px">
                </td>
                <td style="width: 56px; height: 21px">
                </td>
                <td style="width: 149px; height: 21px">
                </td>
                <td style="width: 100px; height: 21px">
                </td>
            </tr>
            <tr>
                <td style="width: 100px; height: 21px">
                </td>
                <td style="width: 100px; height: 21px">
                </td>
                <td style="width: 111px; height: 21px">
                    <asp:Label ID="Label7" runat="server" Font-Size="Smaller" Text="Service Type"></asp:Label></td>
                <td style="width: 45px; height: 21px">
                    <asp:Label ID="servicetype" runat="server" Font-Size="Smaller" Text="Label"></asp:Label></td>
                <td style="width: 131px; height: 21px">
                </td>
                <td style="width: 56px; height: 21px">
                </td>
                <td style="width: 149px; height: 21px">
                </td>
                <td style="width: 100px; height: 21px">
                </td>
            </tr>
        </table>
        <br />
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False">
            <Columns>
                <asp:TemplateField HeaderText="No">
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" />
                    <ItemStyle Font-Size="Smaller" Width="35px" />
                    <ItemTemplate>
                        <%# Container.DataItemIndex + 1 %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="FAPT_CREDT" HeaderText="RMS No" ReadOnly="True" >
                    <ItemStyle Font-Size="Smaller" Width="90px" />
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="FAPT_APTDT" HeaderText="Appointment Date" ReadOnly="True" dataformatstring="{0:dd/MM/yyyy}">
                    <ItemStyle Font-Size="Smaller" Width="100px" />
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="FAPT_STRTM" HeaderText="Time Slot" ReadOnly="True">
                    <ItemStyle Font-Size="Smaller" Width="60px" />
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="ORDERNUMBER" HeaderText="Order Number" ReadOnly="True">
                    <ItemStyle Font-Size="Smaller" Width="100px" />
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="FAPT_APTTY" HeaderText="Job Type" ReadOnly="True">
                    <ItemStyle Font-Size="Smaller" Width="60px" />
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="FAPT_TCHID" HeaderText="Technician/ Auth. Installer Name" ReadOnly="True">
                    <ItemStyle Font-Size="Smaller" Width="140px" />
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="MCUS_ENAME" HeaderText="Customer Name" ReadOnly="True">
                    <ItemStyle Font-Size="Smaller" Width="140px" />
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="FAPT_TELNO" HeaderText="Tel. No" ReadOnly="True">
                    <ItemStyle Font-Size="Smaller" Width="90px" />
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="CUSADDR" HeaderText="Address" ReadOnly="True">
                    <ItemStyle Font-Size="Smaller" Width="200px" />
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="FAPT_REM" HeaderText="Remark" ReadOnly="True">
                    <ItemStyle Font-Size="Smaller" Width="200px" />
                    <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" />
                </asp:BoundField>
            </Columns>
            <EditRowStyle Font-Size="Smaller" />
        </asp:GridView>
    
    </div>
    </form>
</body>
</html>
