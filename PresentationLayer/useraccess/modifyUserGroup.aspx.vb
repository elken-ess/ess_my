﻿Imports System.Configuration
Imports System.Globalization
Imports BusinessEntity
Imports System.Data

Partial Class PresentationLayer_useraccess_modifyUserGroup

    Inherits System.Web.UI.Page

    Dim objXML As New clsXml
    Private bAdd As Boolean

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim script As String = "top.location='../logon.aspx';"
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                    'Response.Redirect("~/PresentationLayer/logon.aspx")
                End If
            Catch ex As Exception
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try


            'Set User Purview
            Try
                SetUserPurview()
            Catch ex As Exception
                objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
                uagErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_USERGROUP_GETPUR")
                uagErrLab.Visible = True
            End Try

            Try
                BindGrid()
            Catch ex As Exception
                objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
                uagErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_MASMUAG_RETINFO")
                uagErrLab.Visible = True
                saveButton.Enabled = False
                Return
            End Try
        End If
    End Sub

    Protected Sub BindGrid()
        Dim dataStyle As CultureInfo = New CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = dataStyle
        Dim uagID As String = ""
        Dim uagStat As String = ""
        'Accept parameters        
        If (Request.Params.HasKeys()) Then
            uagID = Request.Params("uagID").ToString()
            uagStat = Request.Params("uagSTAT").ToString()
        End If
        'if bAdd is True then Add User Access Group else Modify User Access Group
        bAdd = Not (uagID.Length > 0)
        Session.Contents("bAddUserAccessGroup") = bAdd
        'Display the Label or Button Text=================================================
        objXML.XmlFile = ConfigurationManager.AppSettings("XmlFilePath")
        If (bAdd) Then
            uagTitleLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_MASMUAG_TITLE_ADD")
        Else
            uagTitleLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_MASMUAG_TITLE_EDIT")
        End If

        uagIDLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_MASMUAG_UGI")
        uagNameLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_MASMUAG_GN")
        uagAlterNameLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_MASMUAG_AGN")
        uagStatusLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_MASMUAG_STATUS")
        uagGreatedByLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_MASMUAG_CREBY")
        uagCreatedDateLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_MASMUAG_CREDT")
        uagModifiedByLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_MASMUAG_LUSER")
        uagModifiedDateLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_MASMUAG_LSTDT")
        saveButton.Text = objXML.GetLabelName("EngLabelMsg", "BB_MASMUAG_SAVE")
        cancelLink.Text = objXML.GetLabelName("EngLabelMsg", "BB_MASMUAG_CANCEL")

        'Get Error Message
        objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
        uagIDError.ErrorMessage = objXML.GetLabelName("StatusMessage", "BB_MASMUAG_IDEMPTY")
        uagNameError.ErrorMessage = objXML.GetLabelName("StatusMessage", "BB_MASMUAG_NAMEEMPTY")

        'Create the DropDownList Items========================
        Dim userGroupStat As New clsUtil()
        Dim statParam As ArrayList = New ArrayList
        statParam = userGroupStat.searchconfirminf("STATUS")
        Dim count As Integer
        Dim statid As String
        Dim statnm As String
        For count = 0 To statParam.Count - 1
            statid = statParam.Item(count)
            statnm = statParam.Item(count + 1)
            count = count + 1
            If statid.Equals("DELETE") Then
                If Not bAdd Then
                    uagStatusDrop.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                End If
            ElseIf Not statid.Equals("CANCEL") Then
                uagStatusDrop.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
            End If
        Next

        If (bAdd) Then

            'Display as Add User Access Group 
            uagCreatedByBox.Text = Session("userID") + "-" + Session("username")
            uagCreatedDateBox.Text = Format(Date.Today, "dd/MM/yyyy")
            uagModifiedByBox.Text = Session("userID") + "-" + Session("username")
            uagModifiedDateBox.Text = uagCreatedDateBox.Text
            uagStatusDrop.Items(0).Selected = True
        Else
            'Display as Edit User Access Group
            Dim userGroupEntity As New clsUserAccessGroup()
            userGroupEntity.GroupID = Trim(uagID)
            userGroupEntity.GroupStatus = Trim(uagStat)
            Dim uagItem As DataSet = userGroupEntity.SelectedByID()
            If uagItem Is Nothing Then
                objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
                uagErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_FUN_CALLLIST_RERR")
                uagErrLab.Visible = True
                saveButton.Enabled = False
                Return
            End If
            If (uagItem.Tables.Count = 0) Or (uagItem.Tables(0).Rows.Count = 0) Then
                objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
                uagErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_FUN_CALLLIST_RERR")
                uagErrLab.Visible = True
                saveButton.Enabled = False
                Return
            End If
            uagIDBox.Text = uagID
            uagNameBox.Text = uagItem.Tables(0).Rows(0).Item(1).ToString
            uagAlterNameBox.Text = uagItem.Tables(0).Rows(0).Item(2).ToString

            For count = 0 To uagStatusDrop.Items.Count - 1
                If uagStatusDrop.Items(count).Value = uagItem.Tables(0).Rows(0).Item(3).ToString Then
                    uagStatusDrop.SelectedIndex = count
                End If
            Next
            If uagStatusDrop.SelectedIndex < 0 Then
                uagStatusDrop.SelectedIndex = 0
            End If

            'For count = 0 To statParam.Count - 1
            '    If statParam.Item(count).Equals(uagItem.Tables(0).Rows(0).Item(3).ToString) Then
            '        If (uagStatusDrop.Items.Count - 1) <= (count / 2) Then
            '            uagStatusDrop.Items(count / 2).Selected = True
            '        Else
            '            uagStatusDrop.SelectedIndex = 0
            '        End If
            '    End If
            '    count = count + 1
            'Next
            uagCreatedByBox.Text = uagItem.Tables(0).Rows(0).Item(4) + "-" + uagItem.Tables(0).Rows(0).Item(6)
            uagCreatedDateBox.Text = Format(uagItem.Tables(0).Rows(0).Item(5), "dd/MM/yyyy")
            uagModifiedByBox.Text = Session("userID") + "-" + Session("username")
            uagModifiedDateBox.Text = Format(Date.Today, "dd/MM/yyyy")
            uagIDBox.ReadOnly = True
        End If
    End Sub

    Protected Sub saveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveButton.Click
        objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
        Dim msgInfo As String = objXML.GetLabelName("StatusMessage", "BB_MASMUAG_QUESAVE")
        MessageBox1.Confirm(msgInfo)
    End Sub

    Protected Sub MessageBox1_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox1.GetMessageBoxResponse
        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
            Try
                SaveRecord()
            Catch ex As Exception
                objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
                uagErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_MASMUAG_FAILED")
                uagErrLab.Visible = True
            End Try

        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
            Return
        End If
    End Sub

    Protected Function SaveRecord() As Boolean
        Dim userGroupEntity As New clsUserAccessGroup()

        objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
        userGroupEntity.GroupID = uagIDBox.Text.ToUpper
        userGroupEntity.GroupName = uagNameBox.Text.ToUpper
        userGroupEntity.AlterGroupName = uagAlterNameBox.Text.ToUpper
        userGroupEntity.GroupStatus = uagStatusDrop.SelectedItem.Value.ToString
        userGroupEntity.ModifiedBy = Session("userID")
        userGroupEntity.ModifiedDate = uagModifiedDateBox.Text
        userGroupEntity.IPAddress = Request.UserHostAddress
        userGroupEntity.ServiceID = Session("login_svcID")
        userGroupEntity.SessionID = ""  'Session.SessionID

        Dim iCount As Integer = 0
        If (Session.Contents("bAddUserAccessGroup")) Then
            'Add User Access Group
            userGroupEntity.CreatedBy = Session("userID")
            userGroupEntity.CreatedDate = uagCreatedDateBox.Text

            iCount = userGroupEntity.SelectedByDup()
            If (iCount > 0) Then
                uagErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_MASMUAG_DUP")
                uagErrLab.Visible = True
                Return False
            Else
                iCount = userGroupEntity.AddUserGroup()
            End If
        Else
            'Update User Access Group
            iCount = userGroupEntity.SelectedByName()
            If (iCount > 0) Then
                uagErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_MASMUAG_DUP")
                uagErrLab.Visible = True
                Return False
            Else
                iCount = userGroupEntity.UpdatedUserGroup()
            End If
        End If

        If (iCount > 0) Then
            uagErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_MASMUAG_SUCCESS")
        Else
            uagErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_MASMUAG_FAILED")
        End If
        uagErrLab.Visible = True
    End Function

    Private Sub SetUserPurview()
        Dim arrayPurview = New Boolean() {False, False, False, False, False}
        arrayPurview = clsUserAccessGroup.GetUserPurview(Session("accessgroup"), "02")
        If Session.Contents("bAddUserAccessGroup") Then
            saveButton.Enabled = arrayPurview(0)    'New/Add
        Else
            saveButton.Enabled = arrayPurview(1)    'Edit
        End If
    End Sub

End Class
