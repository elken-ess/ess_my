﻿Imports System.Configuration
Imports System.Globalization
Imports System.Data
Imports BusinessEntity

Partial Class PresentationLayer_useraccess_UserAccessGroup
    Inherits System.Web.UI.Page
    Dim objXML As New clsXml
    Dim dataStyle As CultureInfo = New System.Globalization.CultureInfo("en-CA")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim script As String = "top.location='../logon.aspx';"
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                    'Response.Redirect("~/PresentationLayer/logon.aspx")
                End If
            Catch ex As Exception
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try

            BindGrid()
            'Set User Control Purview
            SetUserPurview()
        Else
            uagErrLab.Visible = False
        End If
    End Sub
#Region "Display GridView"
    Protected Sub DisplayGridViewHeader()
        Dim objXML As New clsXml
        objXML.XmlFile = ConfigurationManager.AppSettings("XmlFilePath")

        userGroupView.HeaderRow.Cells(2).Text = userGroupID.Text
        userGroupView.HeaderRow.Cells(2).Font.Size = 8
        userGroupView.HeaderRow.Cells(3).Text = userGroupName.Text
        userGroupView.HeaderRow.Cells(3).Font.Size = 8
        userGroupView.HeaderRow.Cells(4).Text = objXML.GetLabelName("EngLabelMsg", "BB_MASMUAG_AGN")
        userGroupView.HeaderRow.Cells(4).Font.Size = 8
        userGroupView.HeaderRow.Cells(5).Text = userGroupStatus.Text
        userGroupView.HeaderRow.Cells(5).Font.Size = 8
    End Sub
#End Region

    Protected Sub BindGrid()

        System.Threading.Thread.CurrentThread.CurrentCulture = dataStyle
        'Display the Label or Button Text=================================================
        objXML.XmlFile = ConfigurationManager.AppSettings("XmlFilePath")
        userGroupTitle.Text = objXML.GetLabelName("EngLabelMsg", "BB_MASMUAG_TITLE")
        userGroupID.Text = objXML.GetLabelName("EngLabelMsg", "BB_MASMUAG_UGI")
        userGroupName.Text = objXML.GetLabelName("EngLabelMsg", "BB_MASMUAG_GN")
        userGroupStatus.Text = objXML.GetLabelName("EngLabelMsg", "BB_MASMUAG_STATUS")
        searchButton.Text = objXML.GetLabelName("EngLabelMsg", "BB_MASMUAG_SEARCH")
        addButton.Text = objXML.GetLabelName("EngLabelMsg", "BB_MASMUAG_ADD")

        'Create the DropDownList Items and Dispalay the first item========================
        Dim userGroupStat As New clsUtil()
        Dim statParam As ArrayList = New ArrayList
        statParam = userGroupStat.searchconfirminf("STATUS")
        Dim count As Integer
        Dim statid As String
        Dim statnm As String
        For count = 0 To statParam.Count - 1
            statid = statParam.Item(count)
            statnm = statParam.Item(count + 1)
            count = count + 1
            If Not statid.Equals("CANCEL") And Not statid.Equals("DELETE") Then
                userGroupStatusDrop.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
            End If
        Next
        userGroupStatusDrop.Items(0).Selected = True

        'Display the userGroupView Information ============================================
        Dim userGroupEntity As New clsUserAccessGroup()
        userGroupEntity.GroupID = Trim(userGroupIDBox.Text)
        userGroupEntity.GroupName = Trim(userGroupNameBox.Text)
        userGroupEntity.GroupStatus = userGroupStatusDrop.SelectedItem.Value.ToString()

        Dim uagView As DataSet = userGroupEntity.GetUserGroup()
        If uagView Is Nothing Then
            objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
            uagErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_FUN_CALLLIST_RERR")
            uagErrLab.Visible = True
            Return
        End If

        'Session("userAccessGroupData") = uagView

        'Dim editCol As System.Web.UI.WebControls.HyperLinkField = New HyperLinkField()
        'editCol.Text = objXML.GetLabelName("EngLabelMsg", "BB_MASMUAG_EDIT")
        'editCol.DataNavigateUrlFormatString = "~/PresentationLayer/useraccess/modifyUserGroup.aspx?uagID={0}"
        'editCol.HeaderImageUrl = "~/PresentationLayer/graph/edit.gif"

        'Dim funCol As System.Web.UI.WebControls.HyperLinkField = New HyperLinkField()
        'funCol.Text = objXML.GetLabelName("EngLabelMsg", "BB_MASMUAG_FUNSET")
        'funCol.DataNavigateUrlFormatString = "~/PresentationLayer/useraccess/UserAccessGroupFunSet.aspx?uagID={0}"
        'funCol.HeaderImageUrl = "~/PresentationLayer/graph/edit.gif"

        ''construct the URLs for the hyperlinks ==========================================
        'Dim uagID As String = uagView.Tables.Item(0).Columns(0).ColumnName.ToString
        'Dim NavigateUrls As Array = Array.CreateInstance(GetType(String), 1)
        'NavigateUrls(0) = uagID

        'editCol.DataNavigateUrlFields = NavigateUrls
        'funCol.DataNavigateUrlFields = NavigateUrls

        Dim editCol As New CommandField
        editCol.EditText = objXML.GetLabelName("EngLabelMsg", "BB_MASMUAG_EDIT")
        editCol.HeaderImageUrl = "~/PresentationLayer/graph/edit.gif"
        editCol.ShowEditButton = True

        Dim funCol As New CommandField
        funCol.SelectText = objXML.GetLabelName("EngLabelMsg", "BB_MASMUAG_FUNSET")
        funCol.HeaderImageUrl = "~/PresentationLayer/graph/edit.gif"
        funCol.ShowSelectButton = True

        userGroupView.Columns.Add(editCol)
        userGroupView.Columns.Add(funCol)
        If (uagView.Tables.Count > 0) Then
            userGroupView.DataSource = uagView
            userGroupView.DataBind()
        End If
        If (uagView.Tables(0).Rows.Count > 0) Then
            DisplayGridViewHeader()
        Else
            objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
            uagErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_FUN_CALLLIST_RERR")
            uagErrLab.Visible = True
        End If

    End Sub


    Protected Sub searchButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles searchButton.Click
        System.Threading.Thread.CurrentThread.CurrentCulture = dataStyle
        Dim userGroupEntity As New clsUserAccessGroup()
        userGroupEntity.GroupID = Trim(userGroupIDBox.Text)
        userGroupEntity.GroupName = Trim(userGroupNameBox.Text)
        userGroupEntity.GroupStatus = userGroupStatusDrop.SelectedItem.Value.ToString()

        Dim uagView As DataSet = userGroupEntity.GetUserGroup()
        If uagView Is Nothing Then
            objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
            uagErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_FUN_CALLLIST_RERR")
            uagErrLab.Visible = True
            Return
        End If

        'Session("userAccessGroupData") = uagView

        If (uagView.Tables.Count > 0) Then
            userGroupView.DataSource = uagView
            userGroupView.DataBind()
        End If
        If (uagView.Tables(0).Rows.Count > 0) Then
            DisplayGridViewHeader()
        Else
            objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
            uagErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_FUN_CALLLIST_RERR")
            uagErrLab.Visible = True
        End If
    End Sub


    Protected Sub addButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles addButton.Click
        Dim uagStat As String = userGroupStatusDrop.SelectedValue
        Response.Redirect("~/PresentationLayer/useraccess/modifyUserGroup.aspx?uagID=" + "&uagSTAT=" + uagStat)
    End Sub

    Protected Sub userGroupView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles userGroupView.PageIndexChanging
        userGroupView.PageIndex = e.NewPageIndex
        System.Threading.Thread.CurrentThread.CurrentCulture = dataStyle

        'Dim uagView As DataSet = Session.Contents("userAccessGroupData")
        Dim userGroupEntity As New clsUserAccessGroup()
        userGroupEntity.GroupID = Trim(userGroupIDBox.Text)
        userGroupEntity.GroupName = Trim(userGroupNameBox.Text)
        userGroupEntity.GroupStatus = userGroupStatusDrop.SelectedItem.Value.ToString()

        Dim uagView As DataSet = userGroupEntity.GetUserGroup()

        If uagView Is Nothing Then
            objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
            uagErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_FUN_CALLLIST_RERR")
            uagErrLab.Visible = True
            Return
        End If
        userGroupView.DataSource = uagView
        userGroupView.DataBind()

        If (uagView.Tables.Count = 0) Then
            objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
            uagErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_FUN_CALLLIST_RERR")
            uagErrLab.Visible = True
            Return
        End If

        If (uagView.Tables(0).Rows.Count > 0) Then
            DisplayGridViewHeader()
        End If
    End Sub

    Private Sub SetUserPurview()
        Dim arrayPurview = New Boolean() {False, False, False, False, False}
        arrayPurview = clsUserAccessGroup.GetUserPurview(Session("accessgroup"), "02")
        addButton.Enabled = arrayPurview(0)
        'userGroupView.Columns(0).Visible = arrayPurview(1)
        'userGroupView.Columns(1).Visible = arrayPurview(1)
        searchButton.Enabled = arrayPurview(2)
    End Sub


    Protected Sub userGroupView_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles userGroupView.RowEditing
        'Dim ds As DataSet = Session("userAccessGroupData")
        'If ds Is Nothing Then
        '    objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
        '    uagErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_FUN_CALLLIST_RERR")
        '    uagErrLab.Visible = True
        '    Return
        'End If

        'If ds.Tables.Count = 0 Then
        '    objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
        '    uagErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_FUN_CALLLIST_RERR")
        '    uagErrLab.Visible = True
        '    Return
        'End If

        'If ds.Tables(0).Rows.Count = 0 Then
        '    objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
        '    uagErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_FUN_CALLLIST_RERR")
        '    uagErrLab.Visible = True
        '    Return
        'End If

        Dim uagStat As String
        Dim uagID As String
        'Try
        '    uagID = ds.Tables(0).Rows(userGroupView.PageIndex * userGroupView.PageSize + e.NewEditIndex).Item(0).ToString()
        '    uagID = Server.UrlEncode(uagID)
        '    'Dim uagStat As String = Server.UrlEncode(userGroupStatusDrop.SelectedValue)
        '    uagStat = ds.Tables(0).Rows(userGroupView.PageIndex * userGroupView.PageSize + e.NewEditIndex).Item(3).ToString
        '    uagStat = Server.UrlEncode(uagStat)

        'Catch ex As Exception
        '    objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
        '    uagErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_MASMUAG_RECLICK")
        '    uagErrLab.Visible = True
        '    Return
        'End Try
        uagStat = userGroupView.Rows(e.NewEditIndex).Cells(5).Text.ToString.Trim
        uagStat = Server.UrlEncode(uagStat)
        uagID = userGroupView.Rows(e.NewEditIndex).Cells(2).Text.ToString.Trim
        uagID = Server.UrlEncode(uagID)

        Dim i As Integer
        For i = 0 To Me.userGroupStatusDrop.Items.Count - 1
            If uagStat = Me.userGroupStatusDrop.Items(i).Text Then
                uagStat = Me.userGroupStatusDrop.Items(i).Value
                Exit For
            End If
        Next
        Response.Redirect("~/PresentationLayer/useraccess/modifyUserGroup.aspx?uagID=" + uagID + "&uagSTAT=" + uagStat)
    End Sub


    Protected Sub userGroupView_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles userGroupView.SelectedIndexChanging
        'Dim ds As DataSet = Session("userAccessGroupData")
        'Dim iIndex As Integer = e.NewSelectedIndex
        'If ds Is Nothing Then
        '    objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
        '    uagErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_FUN_CALLLIST_RERR")
        '    uagErrLab.Visible = True
        '    Return
        'End If

        'If (ds.Tables.Count = 0) Then
        '    objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
        '    uagErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_FUN_CALLLIST_RERR")
        '    uagErrLab.Visible = True
        '    Return
        'End If

        'If ds.Tables(0).Rows.Count = 0 Then
        '    objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
        '    uagErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_FUN_CALLLIST_RERR")
        '    uagErrLab.Visible = True
        '    Return
        'End If

        Dim uagID As String
        'Try
        '    uagID = ds.Tables(0).Rows(userGroupView.PageIndex * userGroupView.PageSize + iIndex).Item(0).ToString()
        'Catch ex As Exception
        '    objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
        '    uagErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_MASMUAG_RECLICK")
        '    uagErrLab.Visible = True
        '    Return
        'End Try
        uagID = userGroupView.Rows(e.NewSelectedIndex).Cells(2).Text.ToString.Trim
        uagID = Server.UrlEncode(uagID)
        Response.Redirect("~/PresentationLayer/useraccess/UserAccessGroupFunSet.aspx?uagID=" + uagID + "&uagSTAT=")
    End Sub
End Class
