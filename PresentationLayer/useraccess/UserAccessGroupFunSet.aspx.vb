﻿Imports System.Configuration
Imports System.Globalization
Imports BusinessEntity
Imports System.Data

Partial Class PresentationLayer_useraccess_GroupFunSet_aspx
    Inherits System.Web.UI.Page
    Dim objXML As New clsXml
    Dim dsXML As New DataSet
    Dim dsUAG As New DataSet
    Dim uagID As String = ""
    Dim userGroupEntity As New clsUserAccessGroup()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim script As String = "top.location='../logon.aspx';"
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                    'Response.Redirect("~/PresentationLayer/logon.aspx")
                End If
            Catch ex As Exception
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try
            Me.BindGrid()

            SetUserPurview()
        Else
            uagErrLab.Visible = False
        End If
    End Sub

    Protected Sub BindGrid()
        Dim dataStyle As CultureInfo = New CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = dataStyle

        'Accept parameters        
        If (Request.Params.HasKeys()) Then
            uagID = Request.Params("uagID").ToString()
            Session.Contents("UAGID") = uagID
        End If

        'Display the Label or Button Text=================================================
        objXML.XmlFile = ConfigurationManager.AppSettings("XmlFilePath")
        Me.uagTitleLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_MASMUAG_TITLE_FUNSET") + " ---- " + Session.Contents("UAGID")
        Me.uagCheckedLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_MASMUAG_CHECKEDFUN")
        Me.uagUnCheckedLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_MASMUAG_ALLFUN")
        Me.uagAddOne.Text = objXML.GetLabelName("EngLabelMsg", "BB_MASMUAG_ADDONE")
        Me.uagAddAll.Text = objXML.GetLabelName("EngLabelMsg", "BB_MASMUAG_ADDALL")
        Me.uagDelOne.Text = objXML.GetLabelName("EngLabelMsg", "BB_MASMUAG_DELONE")
        Me.uagDelAll.Text = objXML.GetLabelName("EngLabelMsg", "BB_MASMUAG_DELALL")

        Me.uagCheckBoxNew.Text = objXML.GetLabelName("EngLabelMsg", "BB_MASMUAG_FUNNEW")
        Me.uagCheckBoxEdit.Text = objXML.GetLabelName("EngLabelMsg", "BB_MASMUAG_FUNEDIT")
        Me.uagCheckBoxView.Text = objXML.GetLabelName("EngLabelMsg", "BB_MASMUAG_FUNVIEW")
        Me.uagCheckBoxPrint.Text = objXML.GetLabelName("EngLabelMsg", "BB_MASMUAG_FUNPRINT")
        Me.uagCheckBoxDel.Text = objXML.GetLabelName("EngLabelMsg", "BB_MASMUAG_FUNDEL")

        Me.saveButton.Text = objXML.GetLabelName("EngLabelMsg", "BB_MASMUAG_SAVE")
        Me.cancelLink.Text = objXML.GetLabelName("EngLabelMsg", "BB_MASMUAG_CANCEL")

        Me.SetTreeview(uagID)
    End Sub

    Protected Sub SetTreeview(ByVal groupID As String)
        dsXML.ReadXml(ConfigurationManager.AppSettings("ResouceXmlPath"))
        If dsXML Is Nothing Then
            objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
            uagErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_FUN_CALLLIST_RERR")
            uagErrLab.Visible = True
            Return
        End If

        userGroupEntity.GroupID = groupID
        dsUAG = userGroupEntity.SelectUserGroup()
        If dsUAG Is Nothing Then
            objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
            uagErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_FUN_CALLLIST_RERR")
            uagErrLab.Visible = True
            Return
        End If

        Dim i, iIndex, iChildNode As Integer
        Dim strModID, strModIDTemp, strModName, strFunID, strFunIDTemp, strFunName As String
        Dim ModuleNode As TreeNode
        strModIDTemp = ""
        iIndex = -1
        'Build all functions tree
        For i = 0 To dsUAG.Tables(0).Rows.Count - 1
            strModID = dsUAG.Tables(0).Rows(i).Item("LFNC_MODCD")
            If (strModID <> strModIDTemp) Then
                strModName = dsXML.Tables("Module").Rows(Convert.ToInt32(strModID) - 1).Item("NAME")
                ModuleNode = New TreeNode(strModName, strModID)
                uagTreeViewAll.Nodes.Add(ModuleNode)
                strModIDTemp = strModID
                iIndex = iIndex + 1
            End If
            strFunID = dsUAG.Tables(0).Rows(i).Item("LFNC_FNCID")
            strFunName = dsXML.Tables("Function").Rows(Convert.ToInt32(strFunID) - 1).Item("NAME")
            uagTreeViewAll.Nodes(iIndex).ChildNodes.Add(New TreeNode(strFunName, strFunID))
        Next

        'Build the User Access Group functions tree
        strModIDTemp = ""
        strFunIDTemp = ""
        iIndex = -1
        For i = 0 To dsUAG.Tables(1).Rows.Count - 1

            strModID = dsUAG.Tables(1).Rows(i).Item("LFNC_MODCD")
            If (strModID <> strModIDTemp) Then
                strModName = dsXML.Tables("Module").Rows(Convert.ToInt32(strModID) - 1).Item("NAME")
                ModuleNode = New TreeNode(strModName, strModID)
                uagTreeViewChecked.Nodes.Add(ModuleNode)
                strModIDTemp = strModID
                iIndex = iIndex + 1
                iChildNode = -1
            End If
            strFunID = dsUAG.Tables(1).Rows(i).Item("LFNC_FNCID")
            strFunName = dsXML.Tables("Function").Rows(Convert.ToInt32(strFunID) - 1).Item("NAME")
            uagTreeViewChecked.Nodes(iIndex).ChildNodes.Add(New TreeNode(strFunName, strFunID))

            iChildNode = iChildNode + 1

            If (dsUAG.Tables(1).Rows(i).Item("MUA2_NEWFG") = "1"c) Then
                uagTreeViewChecked.Nodes(iIndex).ChildNodes(iChildNode).ChildNodes.Add(New TreeNode(uagCheckBoxNew.Text.ToString(), "1"))
            End If
            If (dsUAG.Tables(1).Rows(i).Item("MUA2_EDTFG") = "1"c) Then
                uagTreeViewChecked.Nodes(iIndex).ChildNodes(iChildNode).ChildNodes.Add(New TreeNode(uagCheckBoxEdit.Text.ToString(), "2"))
            End If

            If (dsUAG.Tables(1).Rows(i).Item("MUA2_VIWFG") = "1"c) Then
                uagTreeViewChecked.Nodes(iIndex).ChildNodes(iChildNode).ChildNodes.Add(New TreeNode(uagCheckBoxView.Text.ToString(), "3"))
            End If

            If (dsUAG.Tables(1).Rows(i).Item("MUA2_PRTFG") = "1"c) Then
                uagTreeViewChecked.Nodes(iIndex).ChildNodes(iChildNode).ChildNodes.Add(New TreeNode(uagCheckBoxPrint.Text.ToString(), "4"))
            End If

            If (dsUAG.Tables(1).Rows(i).Item("MUA2_DELFG") = "1"c) Then
                uagTreeViewChecked.Nodes(iIndex).ChildNodes(iChildNode).ChildNodes.Add(New TreeNode(uagCheckBoxDel.Text.ToString(), "5"))
            End If
        Next
    End Sub

    Protected Sub uagTreeViewAll_TreeNodeCheckChanged(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.TreeNodeEventArgs) Handles uagTreeViewAll.TreeNodeCheckChanged
        Dim i As Integer
        If (e.Node.ChildNodes.Count = 0) Then
            Return
        End If
        For i = 0 To e.Node.ChildNodes.Count - 1
            e.Node.ChildNodes(i).Checked = e.Node.Checked
        Next

    End Sub

    Protected Sub uagTreeViewAll_SelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles uagTreeViewAll.SelectedNodeChanged
        Dim i As Integer
        For i = 0 To Me.uagTreeViewAll.SelectedNode.ChildNodes.Count - 1
            Me.uagTreeViewAll.SelectedNode.ChildNodes.Item(i).Checked = Me.uagTreeViewAll.SelectedNode.Checked
        Next
    End Sub

    Protected Sub uagAddOne_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles uagAddOne.Click
        'Check if TreeNode has been selected 
        If Not (SelectedNode(uagTreeViewAll)) Then
            Return
        End If

        'Check if Treeview and CheckBox have been checked
        If Not CheckNode(uagTreeViewAll) Then
            Return
        End If

        Dim i, j As Integer
        Dim bChecked As Boolean = False
        If uagTreeViewAll.SelectedNode.Depth = 0 Then
            For i = 0 To uagTreeViewAll.SelectedNode.ChildNodes.Count - 1
                If uagTreeViewAll.SelectedNode.ChildNodes(i).Checked Then
                    bChecked = True
                    Exit For
                End If
            Next
        Else
            bChecked = uagTreeViewAll.SelectedNode.Checked
        End If

        If Not bChecked Then
            objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
            Me.uagErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_MASMUAG_SELUNCHECKED")
            Me.uagErrLab.Visible = True
            Return
        End If

        'Add detail function to User Access Group
        Dim strModID, strModName, strFunID, strFunName As String
        Dim bParentExist, bChildExist As Boolean
        Dim addNode As TreeNode
        Dim iParentIndex, iChildIndex As Integer
        bParentExist = False
        If uagTreeViewAll.SelectedNode.Depth = 0 Then
            strModID = uagTreeViewAll.SelectedNode.Value
            strModName = uagTreeViewAll.SelectedNode.Text
        Else
            strModID = uagTreeViewAll.SelectedNode.Parent.Value
            strModName = uagTreeViewAll.SelectedNode.Parent.Text
        End If

        For i = 0 To Me.uagTreeViewChecked.Nodes.Count - 1
            If (uagTreeViewChecked.Nodes(i).Value = strModID) Then
                iParentIndex = i
                bParentExist = True
                Exit For
            End If
        Next

        If Not bParentExist Then
            addNode = New TreeNode(strModName, strModID)
            uagTreeViewChecked.Nodes.Add(addNode)
            iParentIndex = uagTreeViewChecked.Nodes.IndexOf(addNode)
        End If


        If uagTreeViewAll.SelectedNode.Depth = 0 Then
            For i = 0 To uagTreeViewAll.SelectedNode.ChildNodes.Count - 1
                If uagTreeViewAll.SelectedNode.ChildNodes(i).Checked Then
                    strFunID = uagTreeViewAll.SelectedNode.ChildNodes(i).Value
                    strFunName = uagTreeViewAll.SelectedNode.ChildNodes(i).Text
                    bChildExist = False
                    For j = 0 To uagTreeViewChecked.Nodes(iParentIndex).ChildNodes.Count - 1
                        If uagTreeViewChecked.Nodes(iParentIndex).ChildNodes(j).Value = strFunID Then
                            bChildExist = True
                            iChildIndex = j
                            Exit For
                        End If
                    Next
                    If Not bChildExist Then
                        addNode = New TreeNode(strFunName, strFunID)
                        uagTreeViewChecked.Nodes(iParentIndex).ChildNodes.Add(addNode)
                        iChildIndex = uagTreeViewChecked.Nodes(iParentIndex).ChildNodes.IndexOf(addNode)
                    End If

                    AddDetailFunctions(iParentIndex, iChildIndex)
                End If
            Next
        Else
            If uagTreeViewAll.SelectedNode.Checked Then
                strFunID = uagTreeViewAll.SelectedNode.Value
                strFunName = uagTreeViewAll.SelectedNode.Text
                bChildExist = False
                For j = 0 To uagTreeViewChecked.Nodes(iParentIndex).ChildNodes.Count - 1
                    If uagTreeViewChecked.Nodes(iParentIndex).ChildNodes(j).Value = strFunID Then
                        bChildExist = True
                        iChildIndex = j
                        Exit For
                    End If
                Next
                If Not bChildExist Then
                    addNode = New TreeNode(strFunName, strFunID)
                    uagTreeViewChecked.Nodes(iParentIndex).ChildNodes.Add(addNode)
                    iChildIndex = uagTreeViewChecked.Nodes(iParentIndex).ChildNodes.IndexOf(addNode)
                End If

                AddDetailFunctions(iParentIndex, iChildIndex)
            End If
        End If

    End Sub

    Protected Function CheckNode(ByVal sender As TreeView) As Boolean
        If (sender.CheckedNodes.Count = 0) Then
            objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
            Me.uagErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_MASMUAG_UNCHECKED")
            Me.uagErrLab.Visible = True
            Return False
        ElseIf Not (uagCheckBoxNew.Checked Or uagCheckBoxEdit.Checked Or uagCheckBoxView.Checked Or uagCheckBoxPrint.Checked Or uagCheckBoxDel.Checked) Then
            objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
            Me.uagErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_MASMUAG_UNCHECKEDBOX")
            Me.uagErrLab.Visible = True
            Return False
        Else
            Return True
        End If
    End Function

    Protected Sub uagAddAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles uagAddAll.Click
        'Check if Treeview and CheckBox have been checked
        If Not CheckNode(uagTreeViewAll) Then
            Return
        End If

        Dim i, j, k, iParentIndex, iChildIndex As Integer
        Dim bParentExist, bChildExist, bChecked As Boolean
        Dim strFunID, strFunName, strModID, strModName As String
        Dim addNode As TreeNode

        For i = 0 To uagTreeViewAll.Nodes.Count - 1
            bChecked = False
            For j = 0 To uagTreeViewAll.Nodes(i).ChildNodes.Count - 1
                If uagTreeViewAll.Nodes(i).ChildNodes(j).Checked Then
                    bChecked = True
                    Exit For
                End If
            Next

            If bChecked Then
                strModID = uagTreeViewAll.Nodes(i).Value
                strModName = uagTreeViewAll.Nodes(i).Text
                bParentExist = False
                For j = 0 To Me.uagTreeViewChecked.Nodes.Count - 1
                    If (uagTreeViewChecked.Nodes(j).Value = strModID) Then
                        iParentIndex = j
                        bParentExist = True
                        Exit For
                    End If
                Next

                If Not bParentExist Then
                    addNode = New TreeNode(strModName, strModID)
                    uagTreeViewChecked.Nodes.Add(addNode)
                    iParentIndex = uagTreeViewChecked.Nodes.IndexOf(addNode)
                End If

                For k = 0 To uagTreeViewAll.Nodes(i).ChildNodes.Count - 1
                    If uagTreeViewAll.Nodes(i).ChildNodes(k).Checked Then
                        strFunID = uagTreeViewAll.Nodes(i).ChildNodes(k).Value
                        strFunName = uagTreeViewAll.Nodes(i).ChildNodes(k).Text
                        bChildExist = False
                        For j = 0 To uagTreeViewChecked.Nodes(iParentIndex).ChildNodes.Count - 1
                            If uagTreeViewChecked.Nodes(iParentIndex).ChildNodes(j).Value = strFunID Then
                                bChildExist = True
                                iChildIndex = j
                                Exit For
                            End If
                        Next
                        If Not bChildExist Then
                            addNode = New TreeNode(strFunName, strFunID)
                            uagTreeViewChecked.Nodes(iParentIndex).ChildNodes.Add(addNode)
                            iChildIndex = uagTreeViewChecked.Nodes(iParentIndex).ChildNodes.IndexOf(addNode)
                        End If

                        AddDetailFunctions(iParentIndex, iChildIndex)

                    End If
                Next
            End If
        Next
    End Sub

    Protected Sub uagDelOne_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles uagDelOne.Click
        'Check if Treeview has been selected
        If Not (SelectedNode(uagTreeViewChecked)) Then
            Return
        End If

        Dim iDepth As Integer = uagTreeViewChecked.SelectedNode.Depth
        Dim iParent As Integer

        If iDepth = 0 Then
            Me.uagTreeViewChecked.Nodes.Remove(uagTreeViewChecked.SelectedNode)
        ElseIf iDepth = 1 Then
            iParent = Me.uagTreeViewChecked.Nodes.IndexOf(uagTreeViewChecked.SelectedNode.Parent)
            Me.uagTreeViewChecked.Nodes(iParent).ChildNodes.Remove(uagTreeViewChecked.SelectedNode)

            If uagTreeViewChecked.Nodes(iParent).ChildNodes.Count = 0 Then
                Me.uagTreeViewChecked.Nodes.RemoveAt(iParent)
            End If
        ElseIf iDepth = 2 Then
            iParent = Me.uagTreeViewChecked.Nodes.IndexOf(uagTreeViewChecked.SelectedNode.Parent.Parent)
            Dim iChild = Me.uagTreeViewChecked.Nodes(iParent).ChildNodes.IndexOf(uagTreeViewChecked.SelectedNode.Parent)
            Me.uagTreeViewChecked.Nodes(iParent).ChildNodes(iChild).ChildNodes.Remove(uagTreeViewChecked.SelectedNode)

            If Me.uagTreeViewChecked.Nodes(iParent).ChildNodes(iChild).ChildNodes.Count = 0 Then
                Me.uagTreeViewChecked.Nodes(iParent).ChildNodes.RemoveAt(iChild)
                If uagTreeViewChecked.Nodes(iParent).ChildNodes.Count = 0 Then
                    Me.uagTreeViewChecked.Nodes.RemoveAt(iParent)
                End If
            End If
        End If

    End Sub

    Protected Sub uagDelAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles uagDelAll.Click
        Me.uagTreeViewChecked.Nodes.Clear()
        'Dim strUserGroupID As String = Session("accessgroup")

        'If IsDeleteFunction(strUserGroupID) Then
        '    Me.uagTreeViewChecked.Nodes.Clear()
        'Else
        '    objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
        '    Me.uagErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_MASMUAG_ISDELETE")
        '    Me.uagErrLab.Visible = True
        'End If

    End Sub

    Protected Sub uagTreeViewChecked_SelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles uagTreeViewChecked.SelectedNodeChanged
        'Dim i As Integer
        'For i = 0 To Me.uagTreeViewChecked.SelectedNode.ChildNodes.Count - 1
        '    Me.uagTreeViewChecked.SelectedNode.ChildNodes.Item(i).Checked = Me.uagTreeViewChecked.SelectedNode.Checked
        'Next
    End Sub

    Protected Sub uagTreeViewChecked_TreeNodeCheckChanged(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.TreeNodeEventArgs) Handles uagTreeViewChecked.TreeNodeCheckChanged
        'Dim i As Integer
        'If (e.Node.ChildNodes.Count = 0) Then
        '    Return
        'End If
        'For i = 0 To e.Node.ChildNodes.Count - 1
        '    e.Node.ChildNodes(i).Checked = e.Node.Checked
        'Next
        'e.Node.Selected = True
    End Sub

    Protected Function SelectedNode(ByVal sender As TreeView) As Boolean
        If (sender.SelectedNode Is Nothing) Then
            objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
            Me.uagErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_MASMUAG_UNSELECTED")
            Me.uagErrLab.Visible = True
            Return False
        Else
            Return True
        End If
    End Function

    Protected Sub AddDetailFunctions(ByVal iParentIndex As Integer, ByVal iChildIndex As Integer)
        'Add Detail Function (New, Edit, Print, View) for Parent Node's child Node
        Dim i As Integer
        Dim bNew, bEdit, bView, bPrint, bDel As Boolean
        Dim strNodeID As String

        For i = 0 To uagTreeViewChecked.Nodes(iParentIndex).ChildNodes(iChildIndex).ChildNodes.Count - 1
            strNodeID = uagTreeViewChecked.Nodes(iParentIndex).ChildNodes(iChildIndex).ChildNodes(i).Value
            If strNodeID = "1" Then
                bNew = True
            ElseIf strNodeID = "2" Then
                bEdit = True
            ElseIf strNodeID = "3" Then
                bView = True
            ElseIf strNodeID = "4" Then
                bPrint = True
            ElseIf strNodeID = "5" Then
                bDel = True
            End If
        Next

        If (uagCheckBoxNew.Checked) And (Not bNew) Then
            uagTreeViewChecked.Nodes(iParentIndex).ChildNodes(iChildIndex).ChildNodes.Add(New TreeNode(uagCheckBoxNew.Text.ToString(), "1"))
        End If
        If (uagCheckBoxEdit.Checked) And (Not bEdit) Then
            uagTreeViewChecked.Nodes(iParentIndex).ChildNodes(iChildIndex).ChildNodes.Add(New TreeNode(uagCheckBoxEdit.Text.ToString(), "2"))
        End If
        If (uagCheckBoxView.Checked) And (Not bView) Then
            uagTreeViewChecked.Nodes(iParentIndex).ChildNodes(iChildIndex).ChildNodes.Add(New TreeNode(uagCheckBoxView.Text.ToString(), "3"))
        End If
        If (uagCheckBoxPrint.Checked) And (Not bPrint) Then
            uagTreeViewChecked.Nodes(iParentIndex).ChildNodes(iChildIndex).ChildNodes.Add(New TreeNode(uagCheckBoxPrint.Text.ToString(), "4"))
        End If
        If (uagCheckBoxDel.Checked) And (Not bDel) Then
            uagTreeViewChecked.Nodes(iParentIndex).ChildNodes(iChildIndex).ChildNodes.Add(New TreeNode(uagCheckBoxDel.Text.ToString(), "5"))
        End If
        uagErrLab.Visible = False
    End Sub

    Protected Sub saveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveButton.Click
        If (uagTreeViewChecked.Nodes.Count = 0) Then
            Dim strUserGroupID As String = Session.Contents("UAGID")

            If IsDeleteFunction(strUserGroupID) Then
                Me.uagTreeViewChecked.Nodes.Clear()
            Else
                objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
                Me.uagErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_MASMUAG_ISDELETE")
                Me.uagErrLab.Visible = True
                Return
            End If
        End If

        objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
        Dim msgInfo As String = objXML.GetLabelName("StatusMessage", "BB_MASMUAG_QUESAVE")
        MessageBoxFunSet.Confirm(msgInfo)
    End Sub

    Protected Sub MessageBoxFunSet_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBoxFunSet.GetMessageBoxResponse
        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
            userGroupEntity.GroupID = Session.Contents("UAGID")
            userGroupEntity.PreUpdateUAGFunctions()

            SaveRecord()
        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
            Return
        End If
    End Sub

    Protected Function SaveRecord() As Boolean
        Dim i, j, k, iRow As Integer
        Dim strModID, strFunID, strDetailFun As String
        Dim cNew, cEdit, cView, cPrint, cDelete As Char
        Dim bUpdate As Boolean = True

        For i = 0 To uagTreeViewChecked.Nodes.Count - 1
            strModID = uagTreeViewChecked.Nodes(i).Value
            For j = 0 To uagTreeViewChecked.Nodes(i).ChildNodes.Count - 1
                strFunID = uagTreeViewChecked.Nodes(i).ChildNodes(j).Value
                cNew = "0"c
                cEdit = "0"c
                cView = "0"c
                cPrint = "0"c
                cDelete = "0"c
                iRow = 0

                For k = 0 To uagTreeViewChecked.Nodes(i).ChildNodes(j).ChildNodes.Count - 1
                    strDetailFun = uagTreeViewChecked.Nodes(i).ChildNodes(j).ChildNodes(k).Value
                    'Select Case strDetailFun
                    '    Case "1"  cNew = "1"c
                    '    Case "2"  cEdit = "1"c
                    'End Select
                    If strDetailFun = "1" Then
                        cNew = "1"c
                    ElseIf strDetailFun = "2" Then
                        cEdit = "1"c
                    ElseIf strDetailFun = "3" Then
                        cView = "1"c
                    ElseIf strDetailFun = "4" Then
                        cPrint = "1"c
                    ElseIf strDetailFun = "5" Then
                        cDelete = "1"c
                    End If
                Next
                userGroupEntity.GroupID = Session.Contents("UAGID")
                userGroupEntity.FunctionId = strFunID
                userGroupEntity.NewFlag = cNew
                userGroupEntity.EditFlag = cEdit
                userGroupEntity.ViewFlag = cView
                userGroupEntity.PrintFlag = cPrint
                userGroupEntity.DeleteFlag = cDelete
                userGroupEntity.ModifiedBy = Session("userID").ToString()
                userGroupEntity.ServiceID = Session("login_svcID")
                userGroupEntity.IPAddress = Request.UserHostAddress
                userGroupEntity.SessionID = ""  'Session.SessionID

                iRow = userGroupEntity.UpdateUAGFunctions()

                If Not (iRow > 0) Then
                    bUpdate = False
                End If
            Next
        Next

        objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
        If bUpdate Then
            Me.uagErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_MASMUAG_UPDFUN_SUCCESS")
        Else
            Me.uagErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_MASMUAG_UPDFUN_FAIL")
        End If
        Me.uagErrLab.Visible = True

    End Function

    Private Sub SetUserPurview()
        saveButton.Enabled = clsUserAccessGroup.GetUserPurview(Session("accessgroup"), "02", "E")
    End Sub

    Private Function IsDeleteFunction(ByVal strUserGroupID As String) As Boolean
        Return userGroupEntity.IsDeleteUserGroup(strUserGroupID)
    End Function

    Protected Sub CheckBox_TreeViewCheckChanged(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.TreeNodeEventArgs) Handles uagTreeViewAll.TreeNodeCheckChanged
        e.Node.Selected = e.Node.Checked
    End Sub
End Class
