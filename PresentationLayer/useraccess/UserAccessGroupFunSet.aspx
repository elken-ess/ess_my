<%@ Page Language="VB" AutoEventWireup="false" CodeFile="UserAccessGroupFunSet.aspx.vb" Inherits="PresentationLayer_useraccess_GroupFunSet_aspx" EnableEventValidation="false"%>

<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>UserFunctionSet Page</title>
    <meta http-equiv="Content-Type" content="text/html; charset=gb2312"/>
    <link href="../css/style.css" type="text/css" rel="stylesheet"/>    
    <style type="text/css"> 
        A:link { COLOR: #336666; TEXT-DECORATION: none } BODY { FONT-SIZE: 9pt; FONT-FAMILY: "Arial", "Verdana", "Tahoma"; BACKGROUND-COLOR: #ffffff }
	    TD { FONT-SIZE: 9pt; FONT-FAMILY: Arial,Verdana,Tahoma }
	</style>
</head>
<body>
    <form id="formUserFunSet" runat="server">
    <div>
        <table id="uagTab" border="0" width = "100%">
            <tr>
                <td>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title1.gif" width="5" /></td>
                            <td background="../graph/title_bg.gif" class="style2" width="98%">
                                <asp:Label ID="uagTitleLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="right" background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title_2.gif" width="5" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" class="style2" width="98%">
                                <font color="red">
                                    <asp:Label ID="uagErrLab" runat="server" Text="Label" Visible="False"></asp:Label></font></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 280px">
                    <table id="userGroup" bgcolor="#b7e6e6" border="0" cellpadding="2" cellspacing="1"
                        style="height: 300px; width: 100%">
                        <tr bgcolor="#ffffff">
                            <td align="center" style="height: 19px; width: 25%;">
                                <asp:Label ID="uagCheckedLab" runat="server" Text="Label"></asp:Label></td>                            
                            <td align="right" width="16%" style="height: 19px">
                                </td>
                            <td align="center" style="height: 19px; width: 24%;">                                
                                <asp:Label ID="uagUnCheckedLab" runat="server" Text="Label"></asp:Label></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="left" style="height: 389px; width: 25%;">
                                &nbsp;<asp:Panel ID="Panel1" runat="server" Height="550px" Width="300px" ScrollBars="Auto">
                                <asp:TreeView ID="uagTreeViewChecked" runat="server" ShowLines="True" Height="361px" Width="277px" ExpandDepth="0">
                                    <SelectedNodeStyle Font-Bold="True" Font-Underline="True" />
                                </asp:TreeView>
                                </asp:Panel>
                                </td>                            
                            <td align="center" width="16%" style="height: 389px">
                                <asp:LinkButton ID="uagAddOne" runat="server">LinkButton</asp:LinkButton><br />
                                <br />
                                <asp:LinkButton ID="uagAddAll" runat="server">LinkButton</asp:LinkButton><br />
                                <br />
                                <asp:LinkButton ID="uagDelOne" runat="server">LinkButton</asp:LinkButton><br />
                                <br />
                                <asp:LinkButton ID="uagDelAll" runat="server">LinkButton</asp:LinkButton></td>
                            <td align="left" style="height: 389px; width: 24%;">
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:Panel ID="Panel2" runat="server" Height="550px" Width="300px" ScrollBars="Auto">
                                    &nbsp;<asp:TreeView ID="uagTreeViewAll" scroll="true" runat="server" ShowLines="True" ShowCheckBoxes="All" OnTreeNodeCheckChanged="CheckBox_TreeViewCheckChanged" Height="329px" Width="350px" ExpandDepth="0">
                                    <SelectedNodeStyle BackColor="White" Font-Bold="True" Font-Underline="True" />
                                </asp:TreeView>
                                </asp:Panel>
                            </td>
                        </tr>                        
                    </table>
                    <asp:CheckBox ID="uagCheckBoxNew" runat="server" />
                    <asp:CheckBox ID="uagCheckBoxEdit" runat="server" />
                    <asp:CheckBox ID="uagCheckBoxView" runat="server" />
                    <asp:CheckBox ID="uagCheckBoxPrint" runat="server" />
                    <asp:CheckBox ID="uagCheckBoxDel" runat="server" /></td>
            </tr>
            <tr>
                <td style="height: 37px">
                    <table id="Table1" border="0" cellpadding="1" cellspacing="1">
                        <tr>
                            <td align="center" style="width: 20%">
                                &nbsp;<asp:LinkButton ID="saveButton" runat="server"></asp:LinkButton></td>
                            <td align="center">
                                <asp:HyperLink ID="cancelLink" runat="server" NavigateUrl="~/PresentationLayer/useraccess/UserAccessGroup.aspx">[cancelLink]</asp:HyperLink></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
        <cc1:messagebox id="MessageBoxFunSet" runat="server" EnableTheming="True"></cc1:messagebox>
    </form>
</body>
</html>
