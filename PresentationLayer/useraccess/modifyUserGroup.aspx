<%@ Page Language="VB" AutoEventWireup="false" CodeFile="modifyUserGroup.aspx.vb" Inherits="PresentationLayer_useraccess_modifyUserGroup" EnableEventValidation="false"%>

<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Modify User Access Group</title>
    <meta http-equiv="Content-Type" content="text/html; charset=gb2312"/>
	<link href="../css/style.css" type="text/css" rel="stylesheet"/>
</head>
<body>
    <form id="formUAG_M" runat="server">
    <div>
        <table id="uagTab" border="0" width = "100%">
            <tr>
                <td>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title1.gif" width="5" /></td>
                            <td background="../graph/title_bg.gif" class="style2" width="98%">
                                <asp:Label ID="uagTitleLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="right" background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title_2.gif" width="5" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" class="style2" width="98%">
                                <font color="red">
                                    <asp:Label ID="uagErrLab" runat="server" Text="Label" Visible="False"></asp:Label></font></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 157px">
                    <table id="userGroup" bgcolor="#b7e6e6" border="0" cellpadding="2" cellspacing="1">
                        <tr bgcolor="#ffffff">
                            <td align="right" width="15%">
                                <asp:Label ID="uagIDLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%">
                                <asp:TextBox ID="uagIDBox" runat="server" CssClass="textborder" MaxLength="10" Width="84%"></asp:TextBox><font
                                    color="red">*</font>
                                <asp:RequiredFieldValidator ID="uagIDError" runat="server" ControlToValidate="uagIDBox">*</asp:RequiredFieldValidator></td>
                            <td align="right" width="15%">
                                <asp:Label ID="uagNameLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" width="35%">
                                <asp:TextBox ID="uagNameBox" runat="server" CssClass="textborder" Width="84%" MaxLength="50"></asp:TextBox><font
                                    color="red">*</font>
                                <asp:RequiredFieldValidator ID="uagNameError" runat="server" ControlToValidate="uagNameBox">*</asp:RequiredFieldValidator></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" width="15%">
                                <asp:Label ID="uagAlterNameLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%">
                                <asp:TextBox ID="uagAlterNameBox" runat="server" CssClass="textborder" MaxLength="50" Width= "98%"></asp:TextBox></td>
                            <td align="right" width="15%">
                                <asp:Label ID="uagStatusLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" width="35%">
                                <asp:DropDownList ID="uagStatusDrop" runat="server" CssClass="textborder" Width="100%">
                                </asp:DropDownList></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" width="15%">
                                <asp:Label ID="uagGreatedByLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%">
                                <asp:TextBox ID="uagCreatedByBox" runat="server" CssClass="textborder" ReadOnly="true" Width="98%"></asp:TextBox></td>
                            <td align="right" width="15%">
                                <asp:Label ID="uagCreatedDateLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" width="35%">
                                <asp:TextBox ID="uagCreatedDateBox" runat="server" CssClass="textborder" ReadOnly="true" Width="98%"></asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="right" width="15%">
                                <asp:Label ID="uagModifiedByLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 35%">
                                <asp:TextBox ID="uagModifiedByBox" runat="server" CssClass="textborder" ReadOnly="true" Width= "98%"></asp:TextBox></td>
                            <td align="right" width="15%">
                                <asp:Label ID="uagModifiedDateLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" width="35%">
                                <asp:TextBox ID="uagModifiedDateBox" runat="server" CssClass="textborder" ReadOnly="true" Width="98%"></asp:TextBox></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 37px">
                    <table id="Table1" border="0" cellpadding="1" cellspacing="1">
                        <tr>
                            <td align="center" style="width: 20%">
                                &nbsp;<asp:LinkButton ID="saveButton" runat="server"></asp:LinkButton></td>
                            <td align="center">
                                <asp:HyperLink ID="cancelLink" runat="server" NavigateUrl="~/PresentationLayer/UserAccess/UserAccessGroup.aspx">[cancelLink]</asp:HyperLink></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    
    </div>
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
            ShowSummary="False" />
        <cc1:MessageBox ID="MessageBox1" runat="server" EnableTheming="True" />
    </form>
</body>
</html>
