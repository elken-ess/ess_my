<%@ Page Language="VB" AutoEventWireup="false" CodeFile="UserAccessGroup.aspx.vb" Inherits="PresentationLayer_useraccess_UserAccessGroup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>User Access Group Page</title>
    <meta http-equiv="Content-Type" content="text/html; charset=gb2312"/>
    <link href="../css/style.css" type="text/css" rel="stylesheet"/>
    <style type="text/css"> 
        A:link { COLOR: #336666; TEXT-DECORATION: none } BODY { FONT-SIZE: 9pt; FONT-FAMILY: "Arial", "Verdana", "Tahoma"; BACKGROUND-COLOR: #ffffff }
	    TD { FONT-SIZE: 9pt; FONT-FAMILY: Arial,Verdana,Tahoma }
	</style>
</head>
<body>
    <form id="formUAG" runat="server">
    <div>
        <table id="uagTab" border="0" width="100%">
            <tr>
                <td>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td background="../graph/title_bg.gif" style="width: 1%">
                        <img height="24" src="../graph/title1.gif" width="5" /></td>
                    <td background="../graph/title_bg.gif" class="style2" width="98%">
                        <asp:Label ID="userGroupTitle" runat="server" Text="Label"></asp:Label></td>
                    <td align="right" background="../graph/title_bg.gif" width="1%">
                        <img height="24" src="../graph/title_2.gif" width="5" /></td>
                </tr>                        
                </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" class="style2" width="100%">
                                <font color="red">
                                    <asp:Label ID="uagErrLab" runat="server" Text="Label" Visible="False"></asp:Label></font></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>       
        <asp:Label ID="userGroupID" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="userGroupIDBox" runat="server" CssClass="textborder" Width="15%" MaxLength="10"></asp:TextBox>
        <asp:Label ID="userGroupName" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="userGroupNameBox" runat="server" CssClass="textborder" Width="15%" MaxLength="50"></asp:TextBox>
        <asp:Label ID="userGroupStatus" runat="server" Text="Label"></asp:Label>
        <asp:DropDownList ID="userGroupStatusDrop" runat="server"></asp:DropDownList>
        <asp:LinkButton ID="searchButton" runat="server"></asp:LinkButton>
        &nbsp; &nbsp;<asp:LinkButton ID="addButton" runat="server"></asp:LinkButton><br />
        <br />
    
    </div>
        <asp:GridView ID="userGroupView" runat="server" AllowPaging="True"
            Font-Size="Smaller" Width="100%">
        </asp:GridView>
    </form>
</body>
</html>
