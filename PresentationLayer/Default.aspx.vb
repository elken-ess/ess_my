﻿Imports BusinessEntity
Imports System.Data
Partial Class Default2
    Inherits System.Web.UI.Page
    Private ApptCalListEntity As clsApptCalList
    Dim CommonClassEntity As New clsCommonClass
    Private ApptCalList As DataSet

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Response.Redirect("~/PresentationLayer/logon.aspx")
            End If

            Dim UserID As String = Session("userID")
            ApptCalListEntity = New clsApptCalList()
            With ApptCalListEntity
                .UserID = UserID
                .CurrentDate = Date.Today.Day.ToString + "/" + Date.Today.Month.ToString + "/" + Date.Today.Year.ToString
            End With
            ApptCalList = ApptCalListEntity.GetApptListPop()
            Session("ApptListViewpop") = ApptCalList
            If Not ApptCalList Is Nothing Then
                If ApptCalList.Tables(0).Rows.Count > 0 Then
                    Dim Pop As String
                    Pop = "<script language='javascript'>window.open('function/OnlineCallListDuePop.aspx',null,'height=500, width=700,status= no, resizable= no, scrollbars=yes, toolbar=no,location=no,menubar=no');</script>"
                    Response.Write(Pop)
                End If
            End If

        Catch ex As Exception
            Response.Redirect("~/PresentationLayer/logon.aspx")
        End Try
    End Sub
End Class
