﻿Imports BusinessEntity
Imports System.Configuration
Partial Class PresentationLayer_masterrecord_Error
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        ErrorLabel.Text = objXmlTr.GetLabelName("StatusMessage", "BB-ERROR")
    End Sub
End Class
