/*
	checkbox control
*/
function doBoxControl(controlbox, sequencebox)
{
		var isControlSelected = controlbox.checked
		if(sequencebox==null)return;
		if(isNaN(sequencebox.length))
		{
			if (sequencebox.disabled==false)
				sequencebox.checked = isControlSelected;
		}
		else{
				for(var i=0;i<sequencebox.length;i++)
				{
					if (sequencebox[i].disabled==false)
						sequencebox[i].checked = isControlSelected;
				}
		}
}
function doBoxChange(controlbox, sequencebox)
{
		var isSelected = true;
		var isChecked = true;
		if(isNaN(sequencebox.length))
		{
				isSelected = sequencebox.checked;
				controlbox.checked = isSelected;
		} else {
				for(var i=0;i<sequencebox.length;i++)
				{
						isSelected = sequencebox[i].checked;
						isChecked = sequencebox[i].disabled;
						if (isSelected==false && isChecked==false)
						{
								controlbox.checked = false;
								break;
						}
				}
		}
		if (isSelected==true)
		{
				controlbox.checked = true;
		}
}

//用于多选CheckBox的选择,controlbox为父控件,sequencebox为子控件
//父控件和子控件均可为数组，且子控件数组根据父控件分组
//如controlbox 1 控制 sequencebox 1，sequencebox 2
//controlbox 2 控制 sequencebox 3，sequencebox 4，sequencebox 5的选择
//同组的controlbox和sequencebox设置相同的ID属性checkBoxID，如物资编码
//使用时控件必须为input对象，不能为struts的multibox标签，因为必须给定
//相同的ID属性，struts标签无该属性
function doMultiBoxControl(controlbox,sequencebox,checkBoxID)
{
	var isControlSelected;
	if(sequencebox==null)return;
	if(isNaN(controlbox.length))
	{
		isControlSelected=controlbox.checked;
		if(isNaN(sequencebox.length))
		{
			if (sequencebox.disabled==false)
				sequencebox.checked = isControlSelected;
		}
		else
		{
			for(var i=0;i<sequencebox.length;i++)
			{
				if (sequencebox[i].id==checkBoxID)
					if (sequencebox[i].disabled==false)
						sequencebox[i].checked = isControlSelected;
			}
		}
	}
	else
	{
		for(var i=0;i<controlbox.length;i++)
		{
			if (controlbox[i].id==checkBoxID)
				isControlSelected=controlbox[i].checked;
		}
		for(var i=0;i<sequencebox.length;i++)
		{
			if (sequencebox[i].id==checkBoxID)
				if (sequencebox[i].disabled==false)
					sequencebox[i].checked = isControlSelected;
		}
	}
}
//用于多选CheckBox的选择,controlbox为父控件,sequencebox为子控件
//父控件和子控件均可为数组，且子控件数组根据父控件分组
//如controlbox 1 控制 sequencebox 1，sequencebox 2
//controlbox 2 控制 sequencebox 3，sequencebox 4，sequencebox 5的选择
//同组的controlbox和sequencebox设置相同的ID属性checkBoxID，如物资编码
//使用时控件必须为input对象，不能为struts的multibox标签，因为必须给定
//相同的ID属性，struts标签无该属性
function doMultiBoxChange(controlbox,sequencebox,checkBoxID)
{
	var isSelected = true;
	var isChecked = true;
	var isReturnChecked = true;
	if(isNaN(sequencebox.length))
	{
		isSelected = sequencebox.checked;
		controlbox.checked = isSelected;
	}
	else
	{
		for(var i=0;i<sequencebox.length;i++)
		{
			isSelected = sequencebox[i].checked;
			isChecked = sequencebox[i].disabled;
			if (sequencebox[i].id==checkBoxID)
				if (isSelected==false && isChecked==false)
				{
					isReturnChecked = false;
					break;
				}
		}
	}
	if(isNaN(controlbox.length))
		controlbox = isReturnChecked;
	else
	{
		for(var i=0;i<controlbox.length;i++)
		{
			if (controlbox[i].id==checkBoxID)
			{
				controlbox[i].checked = isReturnChecked;
				break;
			}
		}
	}
}

function gourls(uri)
{
	var c_url = location.search;
	re = /definition=[\w\.]*&/;
	r = c_url.replace(re,"definition="+uri+"&");
	location.search=r;
}

/**
 * 判断radio是否有选中,或判断checkbox是否选中一个
 *
 * @param checkIdObj radio或checkbox对象
 */
function isRadioChecked(checkIdObj)
{
	//对象是否为空	
	if (checkIdObj == null) 
	{	
		return false;
	}
	//checkbox,并且列表对象有多个
	if(checkIdObj.length)
	{	
		var sum = 0;	
		for (i=0; i<checkIdObj.length; i++) 
		{	
			if(checkIdObj[i].checked  &&  checkIdObj[i].disabled==false)
			{	
				sum++;
			}	
		}	
		//checkbox中只有一个选中
		if (sum == 1)
		{
			return true;
		}		
	}	
	//radio,或checkbox列表对象只有一个	
	else if(checkIdObj.checked   &&  checkIdObj.disabled==false)
	{	
		return true;	
	}	
}

/**
 * 判断checkbox是否有选中
 *
 * @param checkIdObj checkbox对象
 */
function isBoxChecked(checkIdObj)
{
	//对象是否为空
	if(checkIdObj == null)
	{
		return false;
	}
	//checkbox列表对象有多个
	if(checkIdObj.length)
	{
		for(i = 0;i < checkIdObj.length;i++)
		{
			//checkbox有选中项
			if(checkIdObj[i].checked  &&  checkIdObj[i].disabled == false)
			{
				return true;
			}
		}
	}
	//checkbox列表对象只有一个
	else if(checkIdObj.checked  &&  checkIdObj.disabled == false)
	{
		return true;
	}	
}

/**
 * 判断checkbox是否有选中且只能选中一个
 *
 * @param checkIdObj checkbox对象
 */
function isBoxCheckedForOne(checkIdObj)
{
	var flag = 0;
	//对象是否为空
	if(checkIdObj == null)
	{
		return flag;
	}
	//checkbox列表对象有多个
	if(checkIdObj.length)
	{
		for(i = 0;i < checkIdObj.length;i++)
		{
			//checkbox有选中项
			if(checkIdObj[i].checked  &&  checkIdObj[i].disabled == false)
			{
				flag = flag + 1;
			}
		}
	}
	//checkbox列表对象只有一个
	else if(checkIdObj.checked  &&  checkIdObj.disabled == false)
	{
		flag = 1;
	}	
	return flag;
}
	 
  	 
	 /**
*将str中的第一个非空格字符前空格删除
*
*/	 
function Trim(str){
 if(str.charAt(0) == ' '){
  str = str.slice(1);
  str = Trim(str); 
 }
 return str;
}

/**
*dateBegin 第一个需要比较的页面日期输入域
*dateEnd 第二个需要比较的页面日期输入域
*isFocus 在校验不通过的情况下是否需要设点焦点
*第一个参数不是日期类型时2000-01-01,提示信息
*第二个参数不是日期类型时2000-01-01,提示信息
*第二个日期大于第一个日期的时候提示信息
*/
function CompareDate(dateBegin,dateEnd,isFocus,str1,str2,str3)
{
	var tempBegin,sBegin;
	var tempEnd,sEnd
			tempBegin=eval("dateBegin.value");
			tempEnd=eval("dateEnd.value");
			sBegin=new String("");
			sEnd=new String("");
			for(var i=0;i<=tempBegin.length-1;i++)
			{
				if(tempBegin.charAt(i)=="-" || tempBegin.charAt(i)=="/")
					sBegin=sBegin+"/";
				else
				{
					if(isNaN(Number(tempBegin.charAt(i))))
					{
						alert(str1);
						if (isFocus)
							dateBegin.focus();
						return false;
					}				
					else
						sBegin=sBegin+tempBegin.charAt(i);
				}
			}
			for(var i=0;i<=tempEnd.length-1;i++)
			{
				if(tempEnd.charAt(i)=="-" || tempEnd.charAt(i)=="/")
					sEnd=sEnd+"/";
				else
				{
					if(isNaN(Number(tempEnd.charAt(i))))
					{
						alert(str2);
						if (isFocus)
							dateEnd.focus();
						return false;
					}				
					else
						sEnd=sEnd+tempEnd.charAt(i);
				}
			}
			dtOne=new Date(sBegin);
			dtTwo=new Date(sEnd);
			if(dtOne.toString()=="NaN")
			{
				dateBegin.focus();
				alert(str1);
				return false;
			}
			if(dtTwo.toString()=="NaN")
			{
				if (isFocus)
					dateEnd.focus();
				alert(str2);
				return false;
			}
			if(dtOne.valueOf()>dtTwo.valueOf())
			{
				if (isFocus)
					dateEnd.focus();
				alert(str3);
				return false;
			}
			else
			{
				return true;
			}
}

/**
 * 反向格式化金额,去掉逗号
 * 自动设置焦点
 * @param strData 需要格式化的数据
 * @return 返回反格式化的金额
 */
 function reverseFormatAmount(thisPoint,lIsMin)
 {
		var i,strTemp;
		var strData = thisPoint.value;
		//去掉所有的","
		strData = reverseFormatAmountString(strData);
		thisPoint.value=strData;
		thisPoint.select();
 }
 /**
 *strAmount 需要反向格式化的金额字符串
 *return 反向格式化好的金额字符串
 */
 function reverseFormatAmountString(strAmount)
 {
 		//去掉所有的","
		strTemp=new String(strAmount);
		strAmount="";
		for(var i=0;i<strTemp.length;i++)
		{
			var cData;
			cData=strTemp.charAt(i);
			if (cData!=",")
			{
				strAmount=strAmount+cData;
			}
		}
		return strAmount;
 }
 
 /**
 * 格式化金额，自动设置焦点
 * @param strData 需要格式化的数据控件指针
  * @param lIsMin 是否可以为负数，1表示为正，-1表示为负，0表示可以为正可以为负
 * @return 设置数据控件的值为格式化后的金额字符串
 */
 function formatAmount(thisPoint,lScale,lIsMin)
 {
 	var strData = thisPoint.value;
	strData = reverseFormatAmountString(strData);
	strData = parseFloat(strData);
	if (strData>999999999999.99)
		strData = 0.00;
	strData = formatAmountString(strData,lScale,lIsMin);
	thisPoint.value=strData;
 }
 /*
 *
 */
 function getRoundAmount(strData,lScale)
 {
 	var lRectify = 1;
	if (lScale==1)
		lRectify = 10;
	else if (lScale==2)
		lRectify = 100;
	else if (lScale==3)
		lRectify = 1000;
	else if (lScale==4)
		lRectify = 10000;
	else if (lScale==5)
		lRectify = 100000;
	return Math.round(parseFloat(strData)*lRectify)/lRectify;
	/*var strDataCopy = strData;
	strDataCopy = Math.abs(strData);
	var add = 0;
    var s,temp;
    var s1 = strDataCopy + "";
    var start = s1.indexOf(".");
    if(start > 0 && s1.substr(start+lScale+1,1)>=5)add=1;
    var temp = Math.pow(10,lScale);
    s = Math.floor(strDataCopy * temp) + add;
	if (strData >= 0)
    	return s/temp;
	else
		return -1 * s/temp;*/
 }
 /*
 *strData 
 * * @param lIsMin 是否可以为负数，1表示为正，-1表示为负，0表示可以为正可以为负
 */
 function formatAmountString(strData,lScale,lIsMin)
 {
	if(!isNaN(parseFloat(strData)))
 	{
		if(strData!=null)
 		{
			var i,strTemp;

			//去掉所有的","
			strTemp=new String(strData);
			strData="";
			var isValidZero = true;
			var isValidComma = true;
			for(i=0;i<strTemp.length;i++)
			{
				var cData;
				cData=strTemp.charAt(i);
				if(cData=="-" && i==0 && (lIsMin==null || lIsMin=="undefined" || lIsMin == 0 || lIsMin == -1))
				{
					strData = "-";
				}
				else 
				if (cData=="0")
				{
					if (isValidZero)
						strData = strData + cData;
				}
				else
				if (cData==".")
				{
					if (strData!="" && isValidComma)
					{
						strData = strData + cData; 
						isValidComma=true;
					}
				} 
				else
				if (cData!="," && cData!=" ")
				{
					if (!isNaN(cData) || cData==".")
					{
						strData=strData+cData;
						isValidZero = true;
					}
					else
					{
						strData="";
						i=10000;
					}
				}
			}
		}
		if(strData!="")
 		{
			var strRoundAmunt ;
			strRoundAmunt = getRoundAmount(strData,lScale);
			strData = "" + strRoundAmunt;
			//将小数点前和后的数据分别取出来
	 		var nPoint;
	 		nPoint=strData.indexOf(".");
	 		var strFront=strData,strEnd="";
	 		if(nPoint!=-1)
	 		{
	 			strFront=strData.substring(0,nPoint);
	 			strEnd=strData.substring(nPoint+1,strData.length);
	 		}

			//小数点前面的数据加","
			strTemp=new String(strFront);
			var bHaveMinus=false;
			if(strFront.substring(0,1)=="-")
			{
				bHaveMinus=true;
				strTemp=strTemp.substring(1,strTemp.length);
			}
			strFront="";
			var nNum;
			nNum=0;
			for(i=strTemp.length-1;i>=0;i--)
			{
				if(nNum==3)
				{
					strFront=","+strFront ;
					nNum=0;
				}
				nNum++;
				var cData;
				cData=strTemp.charAt(i);
				strFront=cData+strFront;
			}
			if(bHaveMinus)
			{
				strFront="-" + strFront;
			}

			//补或者截小数点后面的值，保持两位
	 		if(strEnd.length>lScale)
	 		{
	 			strEnd=strEnd.substring(0,lScale);
	 		}
	 		else
	 		{
	 			for (i=strEnd.length;i<lScale;i++)
					strEnd = strEnd + "0";
	 		}
			//如果需要小数位
			if (parseInt(lScale,10)> 0)
				{
			 		strData=strFront+"." + strEnd;
				}
			else
				{
					strData=strFront;
				}

 		}
		else
		{
			strData="";
		}
	}
	else
	{
		strData = "";
	}

	if (strData=="")
	{
		if (parseInt(lScale,10)> 0)
			{
				strData="0.";
				for (i=0;i<lScale;i++)
					{
						strData=strData+"0";
					}
			}
		else
			{
				strData="0";
			}
	}
	return strData;
 }
 /*
 实现对权限的check box 的选择
 thisPoint固定为this
 formName页面表单的名字,字符串
 ctrlName页面check box 的名字,字符串
 */
 function rightBoxChange(thisPoint,formName,ctrlName)
	{
		var isChecked = thisPoint.checked;
		var lLength = thisPoint.value.length;
		var strFirst,strSecond,strThird,strFourth;
		var isFirst=false,isSecond=false,isThird=false,isFourth=false;
		var f = eval("document."+formName);

			if (lLength==2)
			{
				strFirst = thisPoint.value;
			}
			else if (lLength==4)
			{
				strFirst = thisPoint.value.substring(0,2);
				strSecond = thisPoint.value;
			}
			else if  (lLength==6)
			{
				strFirst = thisPoint.value.substring(0,2);
				strSecond = thisPoint.value.substring(0,4);
				strThird = thisPoint.value;
			}
			else if  (lLength==8)
			{
				strFirst = thisPoint.value.substring(0,2);
				strSecond = thisPoint.value.substring(0,4);
				strThird = thisPoint.value.substring(0,6);
				strFourth = thisPoint.value;
			}
			if (lLength==2)
			{
				for(c=0; c < f.elements.length; c++)
				{
					if(f.elements[c].name == ctrlName)
					{
						if (f.elements[c].value.substring(0,2)==strFirst)
						{
							f.elements[c].checked=isChecked;
						}
					}
				}
			}
			else if (lLength==4)
			{
				for(c=0; c < f.elements.length; c++)
				{
					if(f.elements[c].name == ctrlName)
					{
						if (f.elements[c].value.length==4 && f.elements[c].value.substring(0,2)==strFirst)
						{
							if (f.elements[c].checked)
							{
								isFirst = true;
								break;
							}
						}
					}
				}
				for(c=0; c < f.elements.length; c++)
				{
					if(f.elements[c].name == ctrlName)
					{
						if (f.elements[c].value==strFirst)
						{
							f.elements[c].checked=isFirst;
							break;
						}
					}
				}
				for(c=0; c < f.elements.length; c++)
				{
					if(f.elements[c].name == ctrlName)
					{
						if (f.elements[c].value.substring(0,4)==strSecond)
						{
							f.elements[c].checked=isChecked;
						}
					}
				}
			}
			else if (lLength==6)
			{
				for(c=0; c < f.elements.length; c++)
				{
					if(f.elements[c].name == ctrlName)
					{
						if (f.elements[c].value.length==6 && f.elements[c].value.substring(0,4)==strSecond)
						{
							if (f.elements[c].checked)
							{
								isSecond = true;
								break;
							}
						}
					}
				}
				for(c=0; c < f.elements.length; c++)
				{
					if(f.elements[c].name == ctrlName)
					{
						if (f.elements[c].value==strSecond)
						{
							f.elements[c].checked=isSecond;
							break;
						}
					}
				}
				for(c=0; c < f.elements.length; c++)
				{
					if(f.elements[c].name == ctrlName)
					{
						if (f.elements[c].value.length==4 && f.elements[c].value.substring(0,2)==strFirst)
						{
							if (f.elements[c].checked)
							{
								isFirst = true;
								break;
							}
						}
					}
				}
				for(c=0; c < f.elements.length; c++)
				{
					if(f.elements[c].name == ctrlName)
					{
						if (f.elements[c].value==strFirst)
						{
							f.elements[c].checked=isFirst;
							break;
						}
					}
				}
				for(c=0; c < f.elements.length; c++)
				{
					if(f.elements[c].name == ctrlName)
					{
						if (f.elements[c].value.substring(0,6)==strThird)
						{
							f.elements[c].checked=isChecked;
						}
					}
				}
			}
			else if (lLength==8)
			{
				for(c=0; c < f.elements.length; c++)
				{
					if(f.elements[c].name == ctrlName)
					{
						if (f.elements[c].value.length==8 && f.elements[c].value.substring(0,6)==strThird)
						{
							if (f.elements[c].checked)
							{
								isThird = true;
								break;
							}
						}
					}
				}
				for(c=0; c < f.elements.length; c++)
				{
					if(f.elements[c].name == ctrlName)
					{
						if (f.elements[c].value==strThird)
						{
							f.elements[c].checked=isThird;
							break;
						}
					}
				}
				for(c=0; c < f.elements.length; c++)
				{
					if(f.elements[c].name == ctrlName)
					{
						if (f.elements[c].value.length==6 && f.elements[c].value.substring(0,4)==strSecond)
						{
							if (f.elements[c].checked)
							{
								isSecond = true;
								break;
							}
						}
					}
				}
				for(c=0; c < f.elements.length; c++)
				{
					if(f.elements[c].name == ctrlName)
					{
						if (f.elements[c].value==strSecond)
						{
							f.elements[c].checked=isSecond;
							break;
						}
					}
				}
				for(c=0; c < f.elements.length; c++)
				{
					if(f.elements[c].name == ctrlName)
					{
						if (f.elements[c].value.length==4 && f.elements[c].value.substring(0,2)==strFirst)
						{
							if (f.elements[c].checked)
							{
								isFirst = true;
								break;
							}
						}
					}
				}
				for(c=0; c < f.elements.length; c++)
				{
					if(f.elements[c].name == ctrlName)
					{
						if (f.elements[c].value==strFirst)
						{
							f.elements[c].checked=isFirst;
							break;
						}
					}
				}
			}
	}
	
/**
 * 判断物资编码是否合法
 * @param strNO 物资编码
 * @return true(合法);false(不合法)
 */
function isValidProductNO(strNO)
{
	//不能为空,长度必须为16位
	if((Trim(strNO) == '') || (strNO.length != 16))	return false;
	//必须是数字
	else if(isNaN(strNO)) return false;
	else
	{
		//大类
		var strLarger = strNO.substring(0,2);
		var nLarger = parseInt(strLarger,10);
		if((nLarger <= 0) || (nLarger> 56)) return false;
		//中类
		var strMiddle = strNO.substring(2,4);
		var nMiddle = parseInt(strMiddle,10);
		if((nMiddle % 2 != 1) && (nMiddle != 0)) return false;
		//小类
		var strSmall = strNO.substring(4,6);
		var nSmall = parseInt(strSmall,10);
		if((nSmall % 2 != 0) && (nSmall != 99)) return false;
		//特征1
		var strSpec1 = strNO.substring(6,9);
		var nSpec1 = parseInt(strSpec1,10);
		if((nSpec1 % 2 != 1) && (nSpec1 != 0)) return false;
		//特征2
		var strSpec2 = strNO.substring(9,13);
		var nSpec2 = parseInt(strSpec2,10);
		if((nSpec2 % 2 != 0) && (nSpec2 != 9999)) return false;
		//特征3
		var strSpec3 = strNO.substring(13,16);
		var nSpec3 = parseInt(strSpec3,10);
		if((nSpec3 % 2 != 1) && (nSpec3 != 0)) return false;
		
		return true;
	}
}
		
	/**
	* @param procuctNO 物资编码对象
	* @param ltype 物资编码类型
	* @return true(符合物资编码规范) false(反之)
	*/	
 	function checkProductNO(procuctNO,ntype)
	{
		var strProductNO = procuctNO.value;
		//输入项可以为空
		if (Trim(strProductNO) == '') return true;
		//输入项必须是数字
		else if (isNaN(strProductNO)) return false;
		else
			{
				switch(parseInt(ntype,10))
					{
						case 1:
						{
							//大类必须是两位而且不大于56
							if (strProductNO.length == 2 && parseInt(strProductNO,10) <= 56)
							return true;
						}
						break;
						case 2:
						case 3:
						{
							//中类必须是两位,小类必须是两位
							if (strProductNO.length == 2)
							return true;
						}
						break;
						case 4:
						case 6:
						{
							//特征1必须是3位,特征3必须是3位
							if (strProductNO.length == 3)
							return true;
						}
						break;
						case 5:
						{
							//特征2必须是4位
							if (strProductNO.length == 4)
							return true;
						}
						break;
					}
			}
	}
	/**
	 *物资编码自动补0或9
	 *@param formName
	 *@param strCtrlName
	 *@param index 数组下标
	 *@param beginOrEnd 0表示补0，9表示补9
	 */
	function formatProductNO(formName,strCtrlName,index,beginOrEnd)
		{
			var strProductNO ; 
			if (eval("document."+formName+".elements[\""+strCtrlName+"\"]").length)
				{	
					eval("strProductNO = document."+formName+".elements[\""+strCtrlName+"\"]["+index+"].value");
					if (Trim(strProductNO) == "")
				 		return;
					else
						{
							while(strProductNO.length < 16)
								{
									strProductNO += beginOrEnd;
								}
						}
					eval("document."+formName+".elements[\""+strCtrlName+"\"]["+index+"].value = strProductNO");

				}
			else
				{
					eval("strProductNO = document."+formName+".elements[\""+strCtrlName+"\"].value");
					if (Trim(strProductNO) == "")
				 		return;
					else
						{
							while(strProductNO.length < 16)
								{
									strProductNO += beginOrEnd;
								}
						}
					eval("document."+formName+".elements[\""+strCtrlName+"\"].value = strProductNO");
				}
				
		}
/**
功能：校验表单金额数组
使用范围：表单中有批量录入、修改数据，并且通过选择每条数
		  据前的checkbox或者radio来进行操作的，ehckbox和radio
		  的值为数组下标
参数：1、selectedObj    checkbox 或者 raido 的对象，如nameForm.checkBoxName
	  2、checkObj  要校验的金额数组的对象   如 nameForm.amountName
	  3、focusObj  校验如果没有通过焦点的位置，如果focusObj没有定义或者为""，
	  则焦点被设置到checkObj的相应位置上
return：通过校验返回  true ;否则返回false
*/		
function checkMutiAmount(selectedObj,checkObj,focusObj)
{
	var returnCehckResult = true;
	if (selectedObj=="")
	{
		if (isNaN(checkObj.length))
		{
			if (parseFloat(reverseFormatAmountString(checkObj.value))<=0)
			{
				if (focusObj==null || focusObj=="")
				{
					checkObj.focus();
					checkObj.select();
				}
				else
				{
					focusObj.focus();
					focusObj.select();
				}
				returnCehckResult = false;
			}	
		}
		else
		{
			for (var i=0;i<checkObj.length;i++)
			{
				if (parseFloat(reverseFormatAmountString(checkObj[i].value))<=0)
				{
					if (focusObj==null || focusObj=="")
					{
						checkObj[i].focus();
						checkObj[i].select();
					}
					else
					{
						focusObj[i].focus();
						focusObj[i].select();
					}
					returnCehckResult = false;
				}
			}
		}
	}
	else if (isNaN(selectedObj.length))
	 {
	 	if (parseFloat(reverseFormatAmountString(checkObj.value))<=0)
		{
			if (focusObj==null || focusObj=="")
			{
				checkObj.focus();
				checkObj.select();
			}
			else
			{
				focusObj.focus();
				focusObj.select();
			}
			returnCehckResult = false;
		}
		else
		{
			return true;
		}
	 }
	 else
	 {
	 	for (var i=0;i<selectedObj.length;i++)
		{
			if (selectedObj[i].checked)
			{
				if (parseFloat(reverseFormatAmountString(checkObj[i].value))<=0)
				{
					if (focusObj==null || focusObj=="")
					{
						checkObj[i].focus();
						checkObj[i].select();
					}
					else
					{
						focusObj[i].focus();
						focusObj[i].select();
					}
					returnCehckResult = false;
				}
			}
		}
	 }
	 return returnCehckResult;
}

/**
校验字符串是否为日期  即满足  yyyy-MM-DD 格式
是：返回 true
否：返回 false

*/
function checkDate(strDate)
{
	var strCheckDate = "";
	for(var i=0;i<=strDate.length-1;i++)
		{
			if(strDate.charAt(i)=="-" || strDate.charAt(i)=="/")
			{
				strCheckDate=strCheckDate+"/";
			}
			else
			{
				if(isNaN(Number(strDate.charAt(i))))
				{
					return false;
				}				
				else
				{
					strCheckDate=strCheckDate+strDate.charAt(i);
				}
			}
		}
	dtOne=new Date(strCheckDate);
	if(dtOne.toString()=="NaN")
	{
		return false;
	}
	var y=dtOne.getFullYear();
    var m=dtOne.getMonth()+1;
    var d=dtOne.getDate();
	if (m<10) m="0"+m;
	if (d<10) d="0"+d;
	var myday=y + "-" + m + "-" + d;
	if (myday!=strDate)
	{
		return false;
	}
	return true;
}

/**
功能：校验表单日期数组
使用范围：表单中有批量录入、修改数据，并且通过选择每条数
		  据前的checkbox或者radio来进行操作的，ehckbox和radio
		  的值为数组下标
参数：1、selectedObj    checkbox 或者 raido 的对象，如nameForm.checkBoxName
	  2、checkObj  要校验的日期数组的对象   如 nameForm.amountName
	  3、focusObj  校验如果没有通过焦点的位置，如果focusObj没有定义或者为""，
	  则焦点被设置到checkObj的相应位置上
return：通过校验返回  true ;否则返回false
*/		
function checkMutiDate(selectedObj,checkObj,focusObj)
{
	var returnCehckResult = true;
	if (selectedObj=="")
	{
		if (isNaN(checkObj.length))
		{
			if (checkObj.value!="" && !checkDate(checkObj.value))
			{
				if (focusObj==null || focusObj=="")
				{
					checkObj.focus();
					checkObj.select();
				}
				else
				{
					focusObj.focus();
					focusObj.select();
				}
				returnCehckResult = false;
			}	
		}
		else
		{
			for (var i=0;i<checkObj.length;i++)
			{
				if (checkObj[i].value!="" && !checkDate(checkObj[i].value))
				{
					if (focusObj==null || focusObj=="")
					{
						checkObj[i].focus();
						checkObj[i].select();
					}
					else
					{
						focusObj[i].focus();
						focusObj[i].select();
					}
					returnCehckResult = false;
				}
			}
		}
	}
	else if (isNaN(selectedObj.length))
	 {
	 	if (checkObj.value!="" && !checkDate(checkObj.value))
		{
			checkObj.value="";
			if (focusObj==null || focusObj=="")
			{
				checkObj.focus();
				checkObj.select();
			}
			else
			{
				focusObj.focus();
				focusObj.select();
			}
			returnCehckResult = false;
		}
		else
		{
			return true;
		}
	 }
	 else
	 {
	 	for (var i=0;i<selectedObj.length;i++)
		{
			if (selectedObj[i].checked)
			{
				if (checkObj[i].value!="" &&  !checkDate(checkObj[i].value))
				{
					if (focusObj==null || focusObj=="")
					{
						checkObj[i].focus();
						checkObj[i].select();
					}
					else
					{
						if (isNaN(focusObj.length))
						{
							focusObj.focus();
							focusObj.select();
						}
						else
						{
							focusObj.focus();
							focusObj.select();
						}
					}
					returnCehckResult = false;
				}
			}
		}
	 }
	 return returnCehckResult;
}

/**
功能：校验表单字符串数组不能为空
使用范围：表单中有批量录入、修改数据，并且通过选择每条数
		  据前的checkbox或者radio来进行操作的，ehckbox和radio
		  的值为数组下标
参数：1、selectedObj    checkbox 或者 raido 的对象，如nameForm.checkBoxName
	  2、checkObj  要校验的字符串数组的对象   如 nameForm.amountName
	  3、focusObj  校验如果没有通过焦点的位置，如果focusObj没有定义或者为""，
	  则焦点被设置到checkObj的相应位置上
return：通过校验返回  true ;否则返回false
*/		
function checkMutiRequire(selectedObj,checkObj,focusObj)
{
	var returnCehckResult = true;
		if (selectedObj=="")
	{
		if (isNaN(checkObj.length))
		{
			if (Trim(checkObj.value)=="")
			{
				if (focusObj==null || focusObj=="")
				{
					checkObj.focus();
					checkObj.select();
				}
				else
				{
					focusObj.focus();
					focusObj.select();
				}
				returnCehckResult = false;
			}	
		}
		else
		{
			for (var i=0;i<checkObj.length;i++)
			{
				if (Trim(checkObj[i].value)=="")
				{
					if (focusObj==null || focusObj=="")
					{
						checkObj[i].focus();
						checkObj[i].select();
					}
					else
					{
						focusObj[i].focus();
						focusObj[i].select();
					}
					returnCehckResult = false;
				}
			}
		}
	}
	else if (isNaN(selectedObj.length))
	 {
	 	if (Trim(checkObj.value)=="")
		{
			if (focusObj==null || focusObj=="")
			{
				checkObj.focus();
				checkObj.select();
			}
			else
			{
						if (isNaN(focusObj.length))
						{
							focusObj.focus();
							focusObj.select();
						}
						else
						{
							focusObj.focus();
							focusObj.select();
						}
			}
			returnCehckResult = false;
		}
		else
		{
			return true;
		}
	 }
	 else
	 {
	 	for (var i=0;i<selectedObj.length;i++)
		{
			if (selectedObj[i].checked)
			{
				if (Trim(checkObj[i].value)=="")
				{
					if (focusObj==null || focusObj=="")
					{
						checkObj[i].focus();
						checkObj[i].select();
					}
					else
					{
						if (isNaN(focusObj.length))
						{
							focusObj.focus();
							focusObj.select();
						}
						else
						{
							focusObj.focus();
							focusObj.select();
						}
					}
					returnCehckResult = false;
				}
			}
		}
	 }
	 return returnCehckResult;
}

/**返回文本字符长度
  *@param checkObj 页面上的元素
  *@param lLength 能输入文本的最大长度
  *@param promptInfo 要显示的提示信息
  */
  function checkTextLength(checkObj,lLength,promptInfo)
  	{
		if (checkObj.value.length> parseInt(lLength,10))
			{
				alert(promptInfo);
				checkObj.focus();
				return false;
			}
		else
			return true;
	}
/*
*设置Form中checkBox或者radio被选中
*用于页面中明细信息发生变化时自动选中当前记录
*用法：在input 的 onChange事件中调用此方法
*@param thisObj 当前input的指针，固定传入 this
*@param thisObjs 当前 input 的名称，如：form.userName
*@param checkBoxOjb heckBox或者radio对象，如：form.selectNo
*/
function returnSelectedRowNumber(thisObj,thisObjs,checkBoxObjs)
{
	if (isNaN(thisObjs.length))
	{
        return 0;
    }
	else
	{
    	for (var i=0;i<thisObjs.length;i++)
		{
			if (thisObj==thisObjs[i])
			{
                return i;
			}
		}
	}
}
	
/*
*设置Form中checkBox或者radio被选中
*用于页面中明细信息发生变化时自动选中当前记录
*用法：在input 的 onChange事件中调用此方法
*@param thisObj 当前input的指针，固定传入 this
*@param thisObjs 当前 input 的名称，如：form.userName
*@param checkBoxOjb heckBox或者radio对象，如：form.selectNo
*/
function autoSelectChecked(thisObj,thisObjs,checkBoxObjs)
{
	if (isNaN(checkBoxObjs.length))
	{
		if (checkBoxObjs!=null && checkBoxObjs!="")
			if(checkBoxObjs.disabled != true)
				checkBoxObjs.checked = true;
	}
	else
	{
		for (var i=0;i<thisObjs.length;i++)
		{
			if (thisObj==thisObjs[i])
			{
				if (checkBoxObjs!=null && checkBoxObjs!="")
                {
					if(checkBoxObjs[i].disabled != true)
        	            checkBoxObjs[i].checked = true;
                    break;
                }
			}
		}
	}
}

function autoSelectCheckedByValue(thisObj,thisObjs,thisObjValues,checkBoxObjs)
{
	var compValue = "";
	if (isNaN(checkBoxObjs.length))
	{
		if (checkBoxObjs!=null && checkBoxObjs!="")
			checkBoxObjs.checked = true;
	}
	else
	{
		for (var i=0;i<thisObjValues.length;i++)
		{
			if (thisObj == thisObjs[i])
				compValue = thisObjValues[i].value
		}
		for (var i=0;i<thisObjValues.length;i++)
		{
			if (thisObjValues[i].value == compValue)
			{
				if (checkBoxObjs!=null && checkBoxObjs!="")
					checkBoxObjs[i].checked = true;
			}
		}
	}
}

function setButtonDisableByExecPrice(checkObj,priceObj,execPriceObj)
{
	if (priceObj==null)
	{
		return;
	}
	else if (isNaN(priceObj.length))
	{
		if (parseFloat(reverseFormatAmountString(priceObj.value)) != parseFloat(reverseFormatAmountString(execPriceObj.value)))
		{
			checkObj.disabled=true;
		}
	}
	else
	{
		for (var i =0 ;i<priceObj.length;i++)
		{
			if (parseFloat(reverseFormatAmountString(priceObj[i].value)) != parseFloat(reverseFormatAmountString(execPriceObj[i].value)))
			{
				checkObj[i].disabled=true;
			}
		}
	}
}

function filterByCompare(compareObj,compareObjs,checkObjs)
{
	if (compareObjs == null)
	{
		return;
	}
	if (isNaN(compareObjs.length)) 
	{
		if (compareObj.value != "")
		{
			if (compareObjs.value != compareObj.value)
			{
				checkObjs.disabled = true;
			}
		}
	}
	else
	{
		for (var i=0;i<compareObjs.length;i++)
		{
			if (compareObj.value != "")
			{
				if (compareObjs[i].value != compareObj.value)
				{
					checkObjs[i].disabled = true;
				}
			}
		}
	}
}
/**判断相同物资编码，批次的物资是否重复，对重复物资disable*/
	function setProdcutButtonDisableByBatchIsExist(checkBoxObj,productObj,strArgObj1)
	{
		if (productObj==null)
		{
			return;
		}
		else if (isNaN(productObj.length))
		{
		  	var strArg1;
	
		  	if (strArgObj1!=null && strArgObj1!="")
		  		strArg1=strArgObj1.value;
		  	else 
		  		strArg1="";			  	
		  	
			if (checkIsExistProductNo(productObj.value,strArg1))
			{
				checkBoxObj.disabled=true;					
			}

		}
		else
		{
			for (var i =0 ;i<productObj.length;i++)
			{
			  	var strArg1;			  	
			  	if (strArgObj1!=null && strArgObj1!="")
			  		strArg1=strArgObj1[i].value;
			  	else 
			  		strArg1="";			  				  	
				if (checkIsExistProductNo(productObj[i].value,strArg1))
				{
					checkBoxObj[i].disabled=true;							
				}
			}			
		}		
	}

/**判断相同物资编码，批次的价格是否不同，对不同价格物资disable*/
	function setProdcutButtonDisableByPriceIsSame(checkBoxObj,productObj,strArgObj1)
	{
		if (productObj==null)
		{
			return;
		}
		else if (isNaN(productObj.length))
		{
		  	var strArg1;
	
		  	if (strArgObj1!=null && strArgObj1!="")
		  		strArg1=strArgObj1.value;
		  	else 
		  		strArg1="";			  	
		  	
			if(checkIsExistProductNo(productObj.value))
			{
				if(!checkIsExistProductNo(productObj.value,strArg1,""))
				{
					checkBoxObj.disabled=true;					
				}
			} 
		}
		else
		{
			for (var i =0 ;i<productObj.length;i++)
			{
			  	var strArg1;			  	
			  	if (strArgObj1!=null && strArgObj1!="")
			  		strArg1=strArgObj1[i].value;
			  	else 
			  		strArg1="";			  				  				  		
				if(checkIsExistProductNo(productObj[i].value))
				{
				   if(!checkIsExistProductNo(productObj[i].value,strArg1,""))
					{
							checkBoxObj[i].disabled=true;							
					}
				}
			}
		}			
				
	}

/**
 * 根据给定的值设置Html<select>标签的缺省option项
 * @param selectObj <select>标签的Name
 * @param ObjValue 缺省option项的Value值
 */
function selectOption(selectObj,ObjValue)
{
	for (i=0;i<selectObj.length;i++)
	{
		if (selectObj[i].value == ObjValue)
			selectObj[i].selected = true;
	}
}

/**验证大类的范围
 * @param targetProductNo 要验证的物资编码
 * @param srcProductNo1s  IC卡的物资编码范围(split with comma)
 */
function validCatalog(targetProductNo, srcProductNo1s)
	{
		if (srcProductNo1s == null || srcProductNo1s == "")
			{
				return false;
			}
		var lTargetProductNo = parseInt(Trim(targetProductNo), 10);
		if (Trim(srcProductNo1s) == "")
			{
				return false;
			}
		var srcProductNos = srcProductNo1s.split(",");
		 for (var i = 0;i < srcProductNos.length;i++)
		 	{
				var lSrcProductNo = parseInt(Trim(srcProductNos[i]), 10);
				if (lTargetProductNo == lSrcProductNo)
					{
						return true;
					}
			}
		return false;
	}
	
/**制作领料申请刷卡验证身份
 @ param ipsGydw 供应单位
 @ param totalMoney 验证金额 is a object,need to convert to double
 @ param catalogNO 物资大类
 @ param strOutboundID 领料申请编号
 */
function brushCardForApply(ipsGydw, catalogNO, totalMoney, strOutboundID)
	{
		var ipsFile;//卡机配置文件
		ipsFile = "wsjs.ini";
		//提示用户输入密码
		if (!confirm("请确认已输入密码，并且卡已放好！"))
			return false;
		//读卡
		var nResult;
		nResult = parseInt(window.SlofIC.ReadIc(ipsFile, ipsGydw, "1", strDllVesion), 10);//标志1表示在科室刷卡
		//如果读卡失败
		if (nResult < 0)
			{
				window.SlofIC.FinishCycle();
				return false;
			}
		//取IC卡号，日志编号以及物资权限
		var strIcNo;
		var strLogNo;
		var strWzNo;
		var dMoney;
		strIcNo = window.SlofIC.Icbh();
		strLogNo = window.SlofIC.Iclog();
		strWzNo = window.SlofIC.Wzqx();
		if (strIcNo == "")
			{
				alert("读取IC卡信息失败，请重新刷卡！");
				window.SlofIC.FinishCycle();
				return false;
			}		
		strIcNoForSave = strIcNo;
		dMoney = parseFloat(reverseFormatAmountString(totalMoney));
		if (!validCatalog(catalogNO, strWzNo))
			{
				alert("发料单物资大类不在IC卡所有大类之中！");
				//写日志
				window.SlofIC.WriteIcLog(strIcNo, strLogNo, dMoney, "发料单物资大类不在IC卡所有大类之中，放弃制领料申请单", '');
				window.SlofIC.FinishCycle();
				return false;
			}
		//写日志
		//window.SlofIC.WriteIcLog(strIcNo, strLogNo, dMoney, "制发料单。编号="+strOutboundID, '');
		window.SlofIC.FinishCycle();
		return true;
	}	
	
/**科室刷卡验证身份和金额
 @ param ipsGydw 供应单位
 @ param totalMoney 验证金额 is a object,need to convert to double
 @ param catalogNO 物资大类
 @ param strOutboundID 发料单ID
 */
function brushCardForDept(ipsGydw, catalogNO, totalMoney, strOutboundID)
	{
		var ipsFile;//卡机配置文件
		ipsFile = "wsjs.ini";
		//提示用户输入密码
		if (!confirm("请确认已输入密码，并且卡已放好！"))
			return false;
		//读卡
		var nResult;
		nResult = parseInt(window.SlofIC.ReadIc(ipsFile, ipsGydw, "1", strDllVesion), 10);//标志1表示在科室刷卡
		//如果读卡失败
		if (nResult < 0)
			{
				window.SlofIC.FinishCycle();
				return false;
			}
		//取IC卡号，日志编号以及物资权限
		var strIcNo;
		var strLogNo;
		var strWzNo;
		var dMoney;
		strIcNo = window.SlofIC.Icbh();
		strLogNo = window.SlofIC.Iclog();
		strWzNo = window.SlofIC.Wzqx();
		if (strIcNo == "")
			{
				alert("读取IC卡信息失败，请重新刷卡！");
				window.SlofIC.FinishCycle();
				return false;
			}		
		strIcNoForSave = strIcNo;
		dMoney = parseFloat(reverseFormatAmountString(totalMoney));
		if (!validCatalog(catalogNO, strWzNo))
			{
				alert("发料单物资大类不在IC卡所有大类之中！");
				//写日志
				window.SlofIC.WriteIcLog(strIcNo, strLogNo, dMoney, "发料单物资大类不在IC卡所有大类之中，放弃制发料单", '');
				window.SlofIC.FinishCycle();
				return false;
			}
		if (window.SlofIC.BalanceValidate("1", strIcNo, dMoney) < 0)
			{
				alert("金额验证不通过！");
				//写日志
				window.SlofIC.WriteIcLog(strIcNo, strLogNo, dMoney, "金额验证不通过，放弃制发料单", '');
				window.SlofIC.FinishCycle();
				return false;
			}
		//写日志
		window.SlofIC.WriteIcLog(strIcNo, strLogNo, dMoney, "发料单号:"+strOutboundID, strOutboundID);
		window.SlofIC.FinishCycle();
		return true;
	}
	
/**库房刷卡验证身份和金额
 @ param ipsGydw 供应单位
 @ param totalMoney 验证金额 is a object,need to convert to double
 @ param catalogNO 物资大类
 @ param strOutboundNO 发料单编号
 @ param strReceiveDate 发料日期
 @ param items 料单明细
 @ param oldIcNo 科室刷卡制单时的IC卡号，如果是直达，则为空
 @ param oriTime 跳转到页面的时间
 */
var ifSucessSettle = -1;
var strIcNoForSave = "";
var isBrushCard = 0; //控制在一次刷卡未结束（成功或失败）时不能再次刷卡
var strDllVesion = "1.0"; //dll的最新版本号，更新dll时要更新此版本号
function brushCardForDepot(ipsGydw, catalogNO, totalMoney, stroutboundNo, strReceiveDate, items, oldIcNo, oriTime)
	{
		isBrushCard++;
		//不能在一次刷卡未结束时再次刷卡
		if (isBrushCard > 1)
			{
				return false;
			}
		var ipsFile;//卡机配置文件
		ipsFile = "wsjs.ini";
		var currentTime;
		currentTime = new Date();
		if ((currentTime - oriTime)/(1000 * 60) > 5)//如果5分钟内没有操作页面，提示重新登陆
			{
				alert("与服务器会话超时，请重新登陆刷卡！");
				isBrushCard = 0;
				return false;
			}
		
		if (ipsGydw == "")
			{
				alert("单位编号不能为空！");
				isBrushCard = 0;
				return false;
			}
		if (stroutboundNo == "")
			{	
				alert("发料单号不能为空！");
				isBrushCard = 0;
				return false;
			}
		//提示用户输入密码
		if (!confirm("请确认已输入密码，并且卡已放好！"))
			{
				isBrushCard = 0;
				return false;
			}
		//读卡
		var nResult;
		nResult = parseInt(window.SlofIC.ReadIc(ipsFile, ipsGydw, "2", strDllVesion), 10);//标志2表示在库房刷卡
		//如果读卡失败
		if (nResult < 0)
			{
				window.SlofIC.FinishCycle();
				alert("读取IC卡信息失败，请重新刷卡！");
				isBrushCard = 0;
				return false;
			}
		//取IC卡号，日志编号以及物资权限
		var strIcNo;
		var strLogNo;
		var strWzNo;
		var dMoney;
		strIcNo = window.SlofIC.Icbh();
		strLogNo = window.SlofIC.Iclog();
		strWzNo = window.SlofIC.Wzqx();
		if (strIcNo == "")
			{
				alert("读取IC卡信息失败，请重新刷卡！");
				window.SlofIC.FinishCycle();
				isBrushCard = 0;
				return false;
			}
		
		//同科室刷卡时的IC卡号比较，必须是同一张卡
		if (oldIcNo != null && oldIcNo != 'null' && oldIcNo != '' && oldIcNo != 'undefined')
			{
				if (oldIcNo != strIcNo)
					{
						alert("该单据此前刷卡使用的IC卡卡号为" + oldIcNo + "，当前刷卡的卡号为" + strIcNo + "，必须使用同一张IC卡刷卡结算");
						window.SlofIC.FinishCycle();
						isBrushCard = 0;
						return false;
					}
			}
		strIcNoForSave = strIcNo;
		dMoney = parseFloat(reverseFormatAmountString(totalMoney));
		if (!validCatalog(catalogNO, strWzNo))
			{
				alert("发料单物资大类不在IC卡所有大类之中！");
				window.SlofIC.FinishCycle();
				isBrushCard = 0;
				return false;
			}
		if (window.SlofIC.BalanceValidate("2", strIcNo, dMoney) < 0)
			{
				alert("金额验证不通过！");
				window.SlofIC.FinishCycle();
				isBrushCard = 0;
				return false;
			}
				
		//开始事务
		window.SlofIC.BeginTrans();
		//保存料单信息
		if (window.SlofIC.SaveLlxx(stroutboundNo, ipsGydw, strReceiveDate, strIcNo, dMoney) < 0)
			{
				window.SlofIC.Rollback();
				window.SlofIC.FinishCycle();
				alert("保存料单信息失败！");
				isBrushCard = 0;
				return false;
			}		
		//保存明细
		var i;
		if (items != null)
			{
				for (i = 0;i < items.length;i++)
					{
						if (window.SlofIC.SaveLlmx(items[i]) < 0)
							{
								window.SlofIC.Rollback();
								window.SlofIC.FinishCycle();
								alert("保存料单明细失败");
								isBrushCard = 0;
								return false;
							}
					}
			}		
		//提交
	 	window.SlofIC.Commit();
		window.SlofIC.FinishCycle();
		//设置结算成功标志
		ifSucessSettle = 1;
		isBrushCard = 0;
		return true;
	}	
    
/**查询物资：默认选中所有明细
 *@param checkObj  复选框name
 *@Outhor  李应洪
 */    
function setButtonChecked(checkObj)
    {                                 
        if (checkObj == null)
        {
            return;
        }    
        if (isNaN(checkObj.length) && !checkObj.disabled)
        {
            checkObj.checked = true;
        }
        else
        {
            for (var i = 0; i  < checkObj.length; i++)
            {   
                if(!checkObj[i].disabled)
                {
                    checkObj[i].checked = true;
                }
            }
        }
    }      

/**系统中所有的审核功能点，将“待审核”的所有单据直接查询出俩，并第一项默认选中，审核后返回列表页面将该页面刷新，第一项仍默认选中
 *@param checkObj  复选框name
 *@Outhor  李应洪
 */    
function setRadioChecked(checkObj)
    {                                 
        if (checkObj == null)
        {
            return;
        }    
        if (isNaN(checkObj.length) && !checkObj.disabled)
        {
            checkObj.checked = true;
        }
        else
        {
            for (var i = 0; i  < checkObj.length; i++)
            {   
                if(!checkObj[i].disabled)
                {
                    checkObj[i].checked = true;
					break;
                }
            }
        }
    }   
/*
 *当鼠标指到某条数据时，高亮显示当前数据
 */
 var  initObjectBackground;
 function changeBackground(obj)
 {
 	//initObjectBackground=obj.style.background;
	obj.className ='tableListContentOver';
 }
 //当鼠标离开某条数据时，恢复显示当前数据背景
 function reChangeBackground(obj)
 {
 	//obj.style.background=initObjectBackground;
	obj.className ='tableListContentOut';
 }
 
 /*
  *add by jianwu
  *当定义了分层以后，决定是否显示分层内容
  *@param div1 div ID
  *@param relateDiv 相应需要调整大小的div ID
  *@param hiddenHeight 相应需要调整大小的div 隐藏div1时的高度
  *@param viewHeight 相应需要调整大小的div  不隐藏div1时的高度
  */ 
 function expands(div1,relateDiv,hiddenHeight,viewHeight) 
{
		var whichEl1=eval(div1);
		var relateDiv1=eval(relateDiv);
		if (whichEl1.style.display=="none")
		{	
			whichEl1.style.display="block";
			document.images["view"].src = "/slyq/images/up.gif";
			if(relateDiv != "")
			relateDiv1.style.height=validWorkAreaHeight*hiddenHeight;
		}
		else{
			whichEl1.style.display="none";
			document.images["view"].src = "/slyq/images/down.gif";
			if(relateDiv != "")
			relateDiv1.style.height=validWorkAreaHeight*viewHeight;
		}
}	

/**
* add by jianwu 
*改变焦点所在行的背景色
*@ param obj <tr>处输入this
*@ param formName form名称 
*@ param trNum <tr>所在行数，如nColorIndex
*/
function keyboardChangeBackground(obj,fromName,trNum)
{			

	lastNum = eval('document.'+fromName).elements['lastNum'];
	if(lastNum.value == -1)
	{
		lastNum.value = trNum;
		lastObject = obj;
		obj.style.background='#ffff66';				
	}
	else
	{			
		if(lastNum.value != trNum)
		{	
			lastObject.style.background='#ffffff';
			obj.style.background='#ffff66';				
			lastNum.value = trNum;
			lastObject = obj;
		}
	}			
}	
	/**
	* add by jianwu 
	*拉动滚动条始终显示表头和栏目
	*@ param xID  表头分层id
	*@ param yID  栏目分层id	
	*@ param theParent  父分层id
	*@ param i   纵向滚动条拉动i个像素之后显示
	*@ param j   横向滚动条拉动j个像素之后显示
	*@ param tdNum 表头分层中ｔｄ得数量
	*@ param yTdNum 栏目分层总ｔｄ得数量
	*/
function setPosition(xID,yID,theParent,i,j,tdNum,yTdNum)
{		
		var whichEl1=eval(xID);
		var whichEl2=eval(yID);
		var parentLayers=eval(theParent);
		var dTemp = 0;
		//显示行
		if(parentLayers.scrollTop >= i)
		{			
			if(whichEl1.style.display=="none")
			{
				whichEl1.style.display="block";	
				for(var k = 1; k <=tdNum; k++)
				{	
					if(eval(xID+k).width ==null ||eval(xID+k).width =="")					
					{
						eval(xID+k).width = eval(theParent+k).width;
					}
				}			
			}			
		}
		else
		{
			if(whichEl1.style.display=="block")
			{
				whichEl1.style.display="none";								
			}	
		}
		//显示列
		if(parentLayers.scrollLeft >= j)
		{		
	
			if(whichEl2.style.display=="none")
			{									
				whichEl2.style.display="block";
				if(yTdNum > 0)			
				{
					for(var k = 1; k <=yTdNum; k++)
					{	
						dTemp +=  parseFloat(eval(theParent+k).width);											
						if(eval(yID+k) != null)
						{						
							if(isNaN(eval(yID+k).length))
							{
								eval(yID+k).width = eval(theParent+k).width;						
							}
							else
							{
								eval(yID+k)[0].width = eval(theParent+k).width;													
							}
						}
					}					
					eval(yID).style.width = dTemp*100/((screen.availWidth*0.96)*(parseFloat(eval(theParent+"Table").width)/100))+"%";
					eval(yID+"Table").width = "100%";
				}
			}			
		}
		else
		{
			if(whichEl2.style.display=="block")
			{
				whichEl2.style.display="none";				
			}	
		}				
		if(whichEl1.style.display=="block")
		{
			whichEl1.style.top =parentLayers.scrollTop;				
		}
		if(whichEl2.style.display=="block")
		{
			whichEl2.style.left =parentLayers.scrollLeft;	
		}
	}
	
	/**
	*add by jianwu
	*将页面规范中得单元格宽度转化为分层中的实际宽度，xID设置的情况下同时设置相应分层的宽度
	*@ param xID--   相关联分层宽度
	＊@ param the{arent -- 需设置分层的ｔａｂｌｅ所在分层ｉｄ
	＊@ param tdNum --单元各个数
	*/
	function setTheWidth(xID,theParent,tdNum)	
	{
		var dTemp = 0;
		for(var i = 1; i <= tdNum; i++)
		{
			dTemp += parseFloat(eval(theParent + i).width);	
		}		
		dTemp += "%";
		eval(theParent+"Table").width= dTemp;
		if(xID != null && xID != null)		
		{
			eval(xID+"Table").width=dTemp;		
	    }
		for(var j = 1; j <= tdNum; j++)
		{
			eval(theParent+j).width = screen.availWidth*(0.96)*(parseFloat(dTemp)/100)*(parseFloat(eval(theParent+j).width)/100);
		}		
	}