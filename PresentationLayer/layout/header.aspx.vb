Imports System.Data
Imports BusinessEntity
Partial Class PresentationLayer_layout_Default
    Inherits System.Web.UI.Page
    Dim strUserName As String
    Dim dsUser As New DataSet

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim Rcreat As New clsCommonClass
        Try
            strUserName = Session("UserID")
            Rcreat.userid() = strUserName
            dsUser = Rcreat.Getuseridname("BB_MASUSER_SelByIDName")
            strUserName = dsUser.Tables(0).Rows(0).Item(0)
            lblUserName.Text = "Welcome " & Split(strUserName, "-")(1)
        Catch ex As Exception
            lblUserName.Text = "You have logged out. Please relogin."
        End Try
    End Sub

    Protected Sub LnkLogout_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LnkLogout.Click
        Dim logoncls As New clsLogin()
        logoncls.UseID = Session("UserID")
        logoncls.LogTime = Date.Now()
        logoncls.LVTime = Date.Now()
        logoncls.IPAdress = Request.UserHostAddress.ToString()
        logoncls.PCName = System.Net.Dns.GetHostEntry(Request.UserHostName()).HostName

        logoncls.UpdateSSYA_Update()
        Session.RemoveAll()
        'Response.Redirect("~/PresentationLayer/logon.aspx")
        'Dim script As String = "top.location='/BB/logon.aspx';"  
        Dim script As String = "top.location='../logon.aspx';" 'LLY 20160922
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
        Return

    End Sub

    
End Class
