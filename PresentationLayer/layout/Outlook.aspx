﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Outlook.aspx.vb" Inherits="PresentationLayer_Outlook" %>
<html>
<head runat="server">
    <title>OutlookMenu</title>  
     <script type="text/javascript" language="javascript" src="../js/Outlookmenu.js"> </script>  
     <LINK href="../css/Outlookstyle.css" type="text/css" rel="stylesheet">
</head>
<body>
    <xml id="OutlookXML">
	<%
	    Dim outlook As New BusinessEntity.OutlookMenu
	    Response.Write(outlook.BuildOutlook(Session("accessgroup").ToString().Trim()))
    %>
    </xml>
	<script type="text/javascript" language="javascript"> 
	    initMenu();
    </script>    
</body>
</html>
