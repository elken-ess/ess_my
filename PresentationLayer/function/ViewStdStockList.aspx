﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ViewStdStockList.aspx.vb" Inherits="PresentationLayer_function_ViewStdStockList" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <link href="../css/style.css" type="text/css" rel="stylesheet"/>
     <style type="text/css">A:link { COLOR: #336666; TEXT-DECORATION: none }
	BODY { FONT-SIZE: 9pt; FONT-FAMILY: "Arial", "Verdana", "Tahoma"; BACKGROUND-COLOR: #ffffff }
	td { FONT-SIZE: 9pt; FONT-FAMILY: Arial,Verdana,Tahoma }
	</style>
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table cellSpacing="0" cellPadding="0" width="100%" border="0">
		  <tr>
		    <td width="1%" background="../graph/title_bg.gif"><IMG height="24" src="../graph/title1.gif" width="5"></td>
		    <td class="style2" width="98%" background="../graph/title_bg.gif">
              <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
		    <td align="right" width="1%" background="../graph/title_bg.gif"><IMG height="24" src="../graph/title_2.gif" width="5"></td>
	      </tr>
		  <tr><td>
		  </td>
		    <td>
		      <asp:Label ID="lblMessage" runat="server" ForeColor="red" Text=""></asp:Label>
		    </td> 
	      </tr>
	    </table>
        <table style="width: 100%" bgColor="#b7e6e6" border="0" cellpadding="0">
            <tr bgColor="#ffffff">
                <td align="center" style="width: 15%">
                    <asp:Label ID="lblTech" runat="server" Text="Label"></asp:Label></td>
                <td align="center" style="width: 18.33%">
                    <asp:TextBox ID="txtTech" CssClass="textborder" runat="server" Enabled="False"></asp:TextBox></td>
                <td align="center" style="width: 15%">
                    <asp:Label ID="lblDate" runat="server" Text="Label"></asp:Label></td>
                <td align="center" style="width: 18.33%">
                    <asp:TextBox ID="txtdate" CssClass="textborder" runat="server" Enabled="False"></asp:TextBox></td>
                <td align="center" style="width: 15%">
                    <asp:Label ID="lblctr" runat="server" Text="Label"></asp:Label></td>
                <td align="center" style="width: 18.33%">
                    <asp:TextBox ID="txtCTR" CssClass="textborder" runat="server" Enabled="False"></asp:TextBox></td>
            </tr>
            <tr bgColor="#ffffff">
                <td align="center" style="width: 15%; height: 21px">
                    <asp:Label ID="lblcom" runat="server" Text="Label"></asp:Label></td>
                <td align="center" style="width: 18.33%; height: 21px">
                    <asp:TextBox ID="txtCOM" CssClass="textborder" runat="server" Enabled="False"></asp:TextBox></td>
                <td align="center" style="width: 15%; height: 21px">
                    <asp:Label ID="lblSVC" runat="server" Text="Label"></asp:Label></td>
                <td align="center" style="width: 18.33%; height: 21px">
                    <asp:TextBox ID="txtSVC" CssClass="textborder" runat="server" Enabled="False"></asp:TextBox></td>
                <td align="center" style="height: 21px" colspan="2">
                    </td>
            </tr>
            <tr bgColor="#ffffff">
                <td align="center" colspan="6" style="height: 124px">
                    <asp:GridView ID="viewStdStklst" runat="server" Width="100%">
                    <Columns>
                    <asp:TemplateField>
                    <ItemTemplate>
                        <%#Container.DataItemIndex + 1%>
                    </ItemTemplate>
                 </asp:TemplateField>
                    </Columns>
                    </asp:GridView>
                </td>
            </tr>
        
            <tr bgcolor="#ffffff">
                <td align="center" colspan="6" style="height: 15px">
                    &nbsp;
                </td>
            </tr>
                <tr bgColor="#ffffff">
                <td align="LEFT" colspan="3">
        <asp:LinkButton ID="lbtnBack" runat="server">LinkButton</asp:LinkButton></td>
       <td align="LEFT" colspan="3">
        <asp:LinkButton ID="lnkViewReport" runat="server">View Report</asp:LinkButton></td> 
            </tr>
            <tr bgcolor="#ffffff">
                <td align="left" colspan="6">
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td align="left" colspan="6">
                </td>
            </tr>
        
        </table>
    
    </div><br/>
        &nbsp;
    </form>
</body>
</html>
