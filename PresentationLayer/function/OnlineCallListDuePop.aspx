<%@ Page Language="VB" AutoEventWireup="false" CodeFile="OnlineCallListDuePop.aspx.vb" Inherits="PresentationLayer_function_OnlineCallListDuePop" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Call List Over Due</title>
        <meta http-equiv="Content-Type" content="text/html; charset=gb2312"/>
	<link href="../css/style.css" type="text/css" rel="stylesheet"/>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table align =center >
        <tr>
        <td>
        <table border="0" cellpadding="0" cellspacing="0" width="150%">
        <tr>
        <td background="../graph/title_bg.gif" style="width: 1%; height: 24px;">
        <img height="24" src="../graph/title1.gif" width="5" /></td>
        <td background="../graph/title_bg.gif" class="style2" width="98%" style="height: 24px">
        <asp:Label ID="titleLab" runat="server" Text="Call List Over Due"></asp:Label></td>
        <td align="right" background="../graph/title_bg.gif" width="1%" style="height: 24px">
        <img height="24" src="../graph/title_2.gif" width="5" /></td>
        </tr>                        
        </table>
        </td>
        </tr>
    <tr>
    <td align =center>
             <asp:GridView ID="ApptListViewPop" runat="server" Width="90%"  PageSize =15 AutoGenerateColumns="False" AllowPaging="True">
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <%#Container.DataItemIndex + 1%>
                    </ItemTemplate>
                    <ItemStyle Width="30px" />
                </asp:TemplateField>
                
                <asp:BoundField DataField="MROU_CUSID" >
                    <ItemStyle Width="100px" />
                </asp:BoundField>
                
                <asp:BoundField DataField="MCUS_ENAME" >
                    <ItemStyle Width="250px" />
                </asp:BoundField>
                
                <asp:TemplateField>
                <ItemTemplate>
                  <asp:DropDownList Width ="100%" ID="MTEL" runat="server"></asp:DropDownList>
                </ItemTemplate>
                <ItemStyle Width="130px" />
                </asp:TemplateField>  
                
                <asp:BoundField DataField="MROU_NCADT" >
                    <ItemStyle Width="100px" />
                </asp:BoundField> 
            </Columns>
        </asp:GridView>
        </td>
        </tr>
        </table>
    </div>
    </form>
</body>
</html>
