<%@ Page Language="VB" AutoEventWireup="false" EnableEventValidation="false" CodeFile="ServiceBillUpdate2.aspx.vb"
    Inherits="PresentationLayer_function_ServiceBillUpdate2" %>

<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc1" %>
<%@ Register Assembly="JCalendar" Namespace="JCalendar" TagPrefix="cc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Update Service bill part 2</title>
    <meta http-equiv="Content-Type" content="text/html; charset=gb2312">
    <link href="../css/style.css" type="text/css" rel="stylesheet">

    <script language="JavaScript" src="../js/common.js"></script>

    <script language="javascript">
        function LoadCheckFreeService_CallBack(response) {

            //if the server-side code threw an exception
            if (response.error != null) {
                //we should probably do better than this
                alert(response.error);

                return;
            }

            var ResponseValue = response.value;

            if (ResponseValue != "") {
                alert(ResponseValue);
                document.getElementById("<%=cboFreeService.ClientID%>").selectedIndex = 1;

            }

        }


        function LoadCheckFreeService(objectClient) {

            var FreeServiceEntitle = document.getElementById("<%=cboFreeService.ClientID%>").options[document.getElementById("<%=cboFreeService.ClientID%>").selectedIndex].value;
            var RoID = document.getElementById("<%=cboRO.ClientID%>").options[document.getElementById("<%=cboRO.ClientID%>").selectedIndex].value;

            PresentationLayer_function_ServiceBillUpdate2.CheckFreeService(FreeServiceEntitle, RoID, LoadCheckFreeService_CallBack);


        }



        function LoadRoChange_CallBack(response) {

            //if the server-side code threw an exception
            if (response.error != null) {
                //we should probably do better than this
                alert(response.error);

                return;
            }

            var ResponseValue = response.value;



            document.getElementById("<%=txtContractID.ClientID%>").value = ResponseValue[0];
            document.getElementById("<%=txtInstallDate.ClientID%>").value = ResponseValue[1];
            if (ResponseValue[0] != "") {

                document.getElementById("<%=lnkContract.ClientID%>").style.display = "";



            }
            else {
                document.getElementById("<%=lnkContract.ClientID%>").style.display = "none";

            }


        }


        function LoadRoChange(objectClient) {

            var RoID = document.getElementById("<%=cboRO.ClientID%>").options[document.getElementById("<%=cboRO.ClientID%>").selectedIndex].value;
            var InvoiceDate = document.getElementById("<%=txtDate.ClientID%>").value;
            var CustomerPrefix = document.getElementById("<%=txtCustomerPrefix.ClientID%>").value;
            var CustomerID = document.getElementById("<%=txtCustomerID.ClientID%>").value;
            PresentationLayer_function_ServiceBillUpdate2.RoDropdownChange(RoID, InvoiceDate, CustomerPrefix, CustomerID, LoadRoChange_CallBack);


        }



        function LoadInstallbaseCheck_CallBack(response) {

            //if the server-side code threw an exception
            if (response.error != null) {
                //we should probably do better than this
                alert(response.error);

                return;
            }

            var ResponseValue = response.value;
            if (ResponseValue != "") {
                alert(ResponseValue);
                document.getElementById("<%=txtInstallDate.ClientID%>").focus();
            }


        }


        function LoadInstallbaseCheck(objectClient) {

            var RoID = document.getElementById("<%=cboRO.ClientID%>").options[document.getElementById("<%=cboRO.ClientID%>").selectedIndex].value;
            var InstallDate = document.getElementById("<%=txtInstallDate.ClientID%>").value;

            PresentationLayer_function_ServiceBillUpdate2.InstallDateChange(RoID, InstallDate, LoadInstallbaseCheck_CallBack);


        }





    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table bgcolor="#b7e6e6" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
            <tr>
                <td background="../graph/title_bg.gif" width="1%">
                    <img height="24" src="../graph/title1.gif" width="5" />
                </td>
                <td background="../graph/title_bg.gif" class="style2" width="98%">
                    <asp:Label ID="titleLab" runat="server" Text="Uodate Service Bill 2"></asp:Label>
                </td>
            </tr>
        </table>
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td background="../graph/title_bg.gif" class="style2" style="height: 14px" width="100%">
                    <font color="red">
                        <asp:Label ID="lblError" runat="server" Visible="False"></asp:Label></font>
                </td>
            </tr>
        </table>
        <table bgcolor="#b7e6e6" width="100%">
            <tr bgcolor="#ffffff">
                <td colspan="4">
                    <asp:Label ID="lblNoteUp" runat="server" ForeColor="Red" Text="Label"></asp:Label>
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td colspan="4">
                    &nbsp;
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td style="width: 15%; height: 24px;">
                    <asp:Label ID="lblServiceBillType" runat="server" Text="Service Bill Type" Width="104px"></asp:Label>
                </td>
                <td style="width: 30%; height: 24px;">
                    <asp:DropDownList ID="cboServiceBillType" runat="server" Width="90%" AutoPostBack="True"
                        Enabled="False">
                    </asp:DropDownList>
                    <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Size="Small" ForeColor="Red"
                        Text="*"></asp:Label>
                </td>
                <td style="width: 15%; height: 24px;">
                    <asp:Label ID="Label10" runat="server" Text="RMS No" Width="104px"></asp:Label></td>
                <td style="width: 30%; height: 24px;">
                    <asp:TextBox ID="RMStxt" runat="server" Enabled="False" MaxLength="10"></asp:TextBox></td>
            </tr>
            <tr bgcolor="#ffffff">
                <td style="width: 15%; height: 24px">
                </td>
                <td style="width: 30%; height: 24px">
                </td>
                <td style="width: 15%; height: 24px">
                    <asp:Label ID="lblInvoiceNo" runat="server" Text="Invoice #" Width="104px"></asp:Label></td>
                <td style="width: 30%; height: 24px">
                    <asp:TextBox ID="txtInvoicePf" runat="server" ReadOnly="True" Width="32px" Enabled="False"></asp:TextBox><asp:TextBox ID="txtInvoiceNo" runat="server" ReadOnly="True" Width="100px" Enabled="False"></asp:TextBox></td>
            </tr>
            <tr bgcolor="#ffffff">
                <td style="width: 15%; height: 7px;">
                    <asp:Label ID="lblDate" runat="server" Text="Date" Width="104px"></asp:Label>
                </td>
                <td style="width: 30%; height: 7px;">
                    <asp:TextBox ID="txtDate" runat="server" Style="width: 135px;" ReadOnly="True" Enabled="true"></asp:TextBox>&nbsp;<cc2:JCalendar
                        ID="JCalendar1" runat="server" ControlToAssign="JDATEBox" ImgURL="~/PresentationLayer/graph/calendar.gif"
                        Enabled="False" Visible="False"></cc2:JCalendar>
                    <asp:HyperLink ID="HypCal" Enabled="false" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                        ToolTip="Choose a Date">Choose a Date</asp:HyperLink>
                    <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Size="Small" ForeColor="Red"
                        Text="*"></asp:Label>
                </td>
                <td style="width: 39px; height: 7px;" valign="top">
                    <asp:Label ID="lblServiceBillNo" runat="server" Text="Service Bill #" Width="104px"></asp:Label>
                </td>
                <td style="width: 363px; height: 7px;" valign="top">
                    <asp:TextBox ID="txtServiceBillNo" runat="server" MaxLength="10" Enabled="False"></asp:TextBox>
                    <asp:Label ID="Label6" runat="server" Font-Bold="True" Font-Size="Small" ForeColor="Red"
                        Text="*"></asp:Label>
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td style="width: 15%;">
                    <asp:Label ID="lblTechnician" runat="server" Text="Technician" Width="104px"></asp:Label>
                </td>
                <td style="width: 30%;">
                    <asp:DropDownList ID="cboTechnician" runat="server" Width="90%" Enabled="False">
                    </asp:DropDownList>
                    <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Size="Small" ForeColor="Red"
                        Text="*"></asp:Label>
                </td>
                <td style="width: 39px;">
                    <asp:Label ID="lblServiceType" runat="server" Text="Service Type" Width="104px"></asp:Label>
                </td>
                <td style="width: 363px;">
                    <asp:DropDownList ID="cboServiceType" runat="server" Width="90%">
                    </asp:DropDownList>
                    <asp:Label ID="Label7" runat="server" Font-Bold="True" Font-Size="Small" ForeColor="Red"
                        Text="*"></asp:Label>
                    <asp:RequiredFieldValidator ID="rfvServiceType" runat="server" ControlToValidate="cboServiceType"
                        ErrorMessage="*" ForeColor="White">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td colspan="4" style="height: 3px">
                    &nbsp;
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td style="height: 30px; width: 15%;">
                    <asp:Label ID="lblAppoinmentNo" runat="server" Text="Appointment No" Width="124px"></asp:Label>
                </td>
                <td style="width: 30%; height: 30px">
                    <asp:TextBox ID="txtAppointmentPrefix" runat="server" Width="26px" ReadOnly="True"
                        Enabled="False"></asp:TextBox>&nbsp;
                    <asp:TextBox ID="txtAppointmentNo" runat="server" Width="94px" ReadOnly="True" Enabled="False"></asp:TextBox>
                    <asp:Button ID="btnSearchAppNo" runat="server" Text="..." CausesValidation="False"
                        Width="1px" Enabled="False" Visible="False" />
                    <asp:Label ID="Label4" runat="server" Font-Bold="True" Font-Size="Small" ForeColor="Red"
                        Text="*"></asp:Label>
                </td>
                <td style="height: 30px; width: 39px;">
                    <asp:Label ID="lblCustomerID" runat="server" Text="CustomerID" Width="124px"></asp:Label>
                </td>
                <td style="height: 30px; width: 363px;">
                    <asp:TextBox ID="txtCustomerPrefix" runat="server" Width="26px" ReadOnly="True"></asp:TextBox>&nbsp;
                    <asp:TextBox ID="txtCustomerID" runat="server" Width="100px" ReadOnly="True"></asp:TextBox>&nbsp;<asp:Button
                        ID="btnSearchCustomer" runat="server" Text="..." CausesValidation="False" Width="18px" />
                    <asp:Label ID="Label8" runat="server" Font-Bold="True" Font-Size="Small" ForeColor="Red"
                        Text="*"></asp:Label><asp:RequiredFieldValidator ID="rfvCustomerNo" runat="server"
                            ControlToValidate="txtCustomerID" ErrorMessage="*" ForeColor="White">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td style="height: 28px; width: 15%;">
                    <asp:Label ID="lblROSerialNo" runat="server" Text="RO Serial No" Width="124px"></asp:Label>
                </td>
                <td style="width: 30%; height: 28px">
                    <asp:DropDownList ID="cboRO" runat="server" Width="90%" Visible="True" AutoPostBack="false"
                        onchange="LoadRoChange(this)">
                    </asp:DropDownList>
                    <asp:Label ID="Label5" runat="server" Font-Bold="True" Font-Size="Small" ForeColor="Red"
                        Text="*"></asp:Label><asp:RequiredFieldValidator ID="rfvRO" runat="server" ControlToValidate="cboRO"
                            ErrorMessage="*" ForeColor="White">*</asp:RequiredFieldValidator>
                </td>
                <td style="height: 28px; width: 39px;">
                    <asp:Label ID="lblCustomer" runat="server" Text="Customer Name" Width="124px"></asp:Label>
                </td>
                <td style="height: 28px; width: 363px;">
                    <asp:TextBox ID="txtCustomer" runat="server" ReadOnly="True" Width="200px"></asp:TextBox>
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td style="width: 15%">
                    <asp:Label ID="lblCountry" runat="server" Text="Country" Width="124px"></asp:Label>
                </td>
                <td style="width: 30%">
                    <asp:DropDownList ID="cboCountry" runat="server" AutoPostBack="True" Enabled="False"
                        Width="90%">
                    </asp:DropDownList>
                </td>
                <td style="width: 39px">
                    <asp:Label ID="lblCompany" runat="server" Text="Company" Width="124px"></asp:Label>
                </td>
                <td style="width: 363px">
                    <asp:DropDownList ID="cboCompany" runat="server" AutoPostBack="True" Enabled="False"
                        Width="90%">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td style="width: 15%">
                    <asp:Label ID="lblServiceCenter" runat="server" Text="Service Center" Width="124px"></asp:Label>
                </td>
                <td style="width: 30%">
                    <asp:DropDownList ID="cboServiceCenter" runat="server" AutoPostBack="True" Enabled="False"
                        Width="90%">
                    </asp:DropDownList>
                </td>
                <td style="width: 39px">
                    <asp:Label ID="lblStatus" runat="server" Text="Status" Width="124px"></asp:Label>
                </td>
                <td style="width: 363px">
                    <asp:DropDownList ID="cboStatus" runat="server" AutoPostBack="True" Width="90%">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td style="height: 28px; width: 15%;">
                    <asp:Label ID="lblCreatedBy" runat="server" Text="Created By" Width="124px"></asp:Label>
                </td>
                <td style="width: 30%; height: 28px">
                    <asp:TextBox ID="txtCreatedBy" runat="server" ReadOnly="True"></asp:TextBox>
                </td>
                <td style="height: 28px; width: 39px;">
                    <asp:Label ID="lblCreateDate" runat="server" Text="Create Date" Width="124px"></asp:Label>
                </td>
                <td style="height: 28px; width: 363px;">
                    <asp:TextBox ID="txtCreateDate" runat="server" ReadOnly="True"></asp:TextBox>
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td style="height: 28px; width: 15%;">
                    <asp:Label ID="lblModifyBy" runat="server" Text="Modify By" Width="124px"></asp:Label>
                </td>
                <td style="width: 30%; height: 28px">
                    <asp:TextBox ID="txtModifyBy" runat="server" ReadOnly="True"></asp:TextBox>
                </td>
                <td style="height: 28px; width: 39px;">
                    <asp:Label ID="lblModifyDate" runat="server" Text="Modfy Date" Width="124px"></asp:Label>
                </td>
                <td style="height: 28px; width: 363px;">
                    <asp:TextBox ID="txtModifyDate" runat="server" ReadOnly="True"></asp:TextBox>
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <%--Modified by Ryan Estandarte 7 March 2012--%>
                <%--<td colspan=4>&nbsp;
                </td>--%>
                <td colspan="4">
                    &nbsp;
                    <asp:LinkButton ID="lnkModifyCust" runat="server" Text="Modify Customer" OnClick="lnkModifyCust_Click"
                        CausesValidation="false" />
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td colspan="4" style="height: 25px">
                    <asp:Label ID="lblUpdateStockList" runat="server" Font-Underline="True" Text="Update Stock List"
                        Width="272px"></asp:Label>&nbsp;
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td colspan="4">
                    <asp:GridView ID="grdServiceBill" runat="server" Width="100%">
                        <Columns>
                            <asp:CommandField CancelText="" EditText="" HeaderImageUrl="~/PresentationLayer/graph/edit.gif"
                                ShowDeleteButton="True" Visible="False" />
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr bgcolor="#ffffff" valign="top">
                <td style="height: 65px; width: 15%;">
                    <asp:Label ID="lblDiscountRemarks" runat="server" Text="Discount Remarks" Width="124px"></asp:Label>
                </td>
                <td style="height: 65px" colspan="2">
                    <asp:TextBox ID="txtDiscountRemarks" runat="server" Height="116px" TextMode="MultiLine"
                        Width="305px" MaxLength="800" Enabled="False"></asp:TextBox>
                </td>
                <td style="height: 65px; width: 363px;" align="right">
                    <table width="100%">
                        <tr>
                            <td style="height: 27px" align="right">
                                <asp:Label ID="lblTotalGross" runat="server" Text="Total" Width="107px"></asp:Label>
                            </td>
                            <td style="height: 27px; width: 21px;" align="right">
                                <asp:TextBox ID="txtTotalGross" runat="server" MaxLength="6" ReadOnly="True" Width="106px"
                                    Enabled="False" Style="text-align: right">0</asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 27px" align="right">
                                <asp:Label ID="lblDiscount" runat="server" Text="Discount" Width="107px"></asp:Label>
                            </td>
                            <td style="height: 27px; width: 21px;" align="right">
                                <asp:TextBox ID="txtDiscount" runat="server" MaxLength="6" Width="106px" AutoPostBack="True"
                                    Enabled="False" Style="text-align: right">0</asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 27px" align="right">
                                <asp:Label ID="lblTotalNet" runat="server" Text="Total" Width="107px"></asp:Label>
                            </td>
                            <td style="height: 27px; width: 21px;" align="right">
                                <asp:TextBox ID="txtTotalNet" runat="server" MaxLength="6" ReadOnly="True" Width="106px"
                                    Enabled="False" Style="text-align: right">0</asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 27px" align="right">
                                <asp:Label ID="lblInvoiceTax" runat="server" Text="Invoice Tax" Width="107px"></asp:Label>
                                <asp:Label ID="lblInclusive" runat="server" Font-Underline="True" Text="(Inclusive)"
                                    Width="107px"></asp:Label>
                            </td>
                            <td style="height: 27px; width: 21px;" align="right">
                                <asp:TextBox ID="txtTax" runat="server" MaxLength="6" ReadOnly="True" Width="106px"
                                    Enabled="False" Style="text-align: right">0</asp:TextBox>
                            </td>
                        </tr>
                    </table>
                    &nbsp;
                </td>
            </tr>
            <tr bgcolor="#ffffff" valign="top">
                <td style="height: 2px" colspan="2" id="#dscnt">
                    <asp:Label ID="lblPaymentDetails" runat="server" Text="Payment Details" Width="124px"></asp:Label>
                </td>
                <td style="height: 2px; width: 39px;">
                </td>
                <td style="height: 2px; width: 363px;">
                </td>
            </tr>
            <tr bgcolor="#ffffff" valign="top">
                <td style="height: 2px">
                    <asp:Label ID="lblCash" runat="server" Text="Cash" Width="124px"></asp:Label>
                </td>
                <td style="width: 30%; height: 2px">
                    <asp:TextBox ID="txtCash" runat="server" MaxLength="10" Width="122px" AutoPostBack="True"
                        Enabled="False" Style="text-align: right">0</asp:TextBox>
                </td>
                <td style="width: 39px; height: 2px">
                    <asp:Label ID="lblCheque" runat="server" Text="Cheque" Width="124px"></asp:Label>
                </td>
                <td style="width: 363px; height: 2px">
                    <asp:TextBox ID="txtCheque" runat="server" MaxLength="10" Width="122px" AutoPostBack="True"
                        Enabled="False" Style="text-align: right">0</asp:TextBox>
                </td>
            </tr>
            <tr bgcolor="#ffffff" valign="top">
                <td style="height: 2px">
                    <asp:Label ID="lblCredit" runat="server" Text="Credit" Width="124px"></asp:Label>
                </td>
                <td style="width: 30%; height: 2px">
                    <asp:TextBox ID="txtCredit" runat="server" MaxLength="10" Width="122px" AutoPostBack="True"
                        Enabled="False" Style="text-align: right">0</asp:TextBox>
                </td>
                <td style="width: 39px; height: 2px">
                    <asp:Label ID="lblCreditCard" runat="server" Text="CreditCard" Width="124px"></asp:Label>
                </td>
                <td style="width: 363px; height: 2px">
                    <asp:TextBox ID="txtCreditCard" runat="server" MaxLength="10" Width="122px" AutoPostBack="True"
                        Enabled="False" Style="text-align: right">0</asp:TextBox>
                </td>
            </tr>
            <tr bgcolor="#ffffff" valign="top">
                <td style="height: 2px">
                    <asp:Label ID="lblOther" runat="server" Text="Other" Width="124px"></asp:Label>
                </td>
                <td style="width: 30%; height: 2px">
                    <asp:TextBox ID="txtOther" runat="server" MaxLength="10" Width="122px" AutoPostBack="True"
                        Enabled="False" Style="text-align: right">0</asp:TextBox>
                </td>
                <td style="width: 39px; height: 2px">
                </td>
                <td style="width: 363px; height: 2px">
                </td>
            </tr>
            <tr bgcolor="#ffffff" valign="top">
                <td style="height: 2px">
                    <asp:Label ID="lblTotal" runat="server" Text="Total" Width="124px"></asp:Label>
                </td>
                <td style="width: 30%; height: 2px">
                    <asp:TextBox ID="txtTotal" runat="server" MaxLength="10" ReadOnly="True" Width="122px"
                        Enabled="False" Style="text-align: right">0</asp:TextBox>
                </td>
                <td style="width: 39px; height: 2px">
                </td>
                <td style="width: 363px; height: 2px">
                </td>
            </tr>
            <tr bgcolor="#ffffff" valign="top">
                <td style="height: 2px" colspan="4">
                    &nbsp;
                </td>
            </tr>
            <tr bgcolor="#ffffff" valign="top">
                <td style="width: 15%;" colspan="4">
                    <asp:Label ID="Label9" runat="server" Font-Underline="True" Text="CR Update" Width="272px"></asp:Label>
                </td>
            </tr>
            <tr bgcolor="#ffffff" valign="top">
                <td style="height: 0px; width: 15%;">
                    <asp:Label ID="Label14" runat="server" Text="Free Service ?" Width="124px"></asp:Label>
                </td>
                <td style="height: 0px; width: 15%;">
                    <asp:DropDownList ID="cboFreeService" runat="server" AutoPostBack="false" onchange="LoadCheckFreeService(this);"
                        Width="20%">
                    </asp:DropDownList>
                </td>
                <td style="height: 0px; width: 15%;">
                    <asp:Label ID="lblContractID" runat="server" Text="Contract ID" Width="124px"></asp:Label>
                </td>
                <td style="height: 0px; width: 363px;">
                    <asp:TextBox ID="txtContractID" runat="server" ReadOnly="true"></asp:TextBox>
                    <asp:LinkButton ID="lnkContract" runat="server" CausesValidation="False" Style="text-align: right"
                        Width="27px" Enabled="False">View</asp:LinkButton>
                </td>
            </tr>
            <tr bgcolor="#ffffff" valign="top">
                <td style="width: 15%; height: 0px">
                    <asp:Label ID="lblInstallDate" runat="server" Text="Install Date" Width="124px"></asp:Label><br />
                    <asp:Label ID="Label11" runat="server" Text="DD/MM/YYYY"></asp:Label>
                </td>
                <td style="width: 15%; height: 0px">
                    <asp:TextBox ID="txtInstallDate" runat="server" Width="125px" AutoPostBack="false"
                        onchange="LoadInstallbaseCheck(this)" MaxLength="10"></asp:TextBox>&nbsp;<cc2:JCalendar
                            ID="Jcalendar2" runat="server" ImgURL="~/PresentationLayer/graph/calendar.gif"
                            Visible="False" />
                    <asp:HyperLink ID="HypCal2" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                        ToolTip="Choose a Date">Choose a Date</asp:HyperLink><br />
                    <asp:CompareValidator ID="CompDateVal" runat="server" ControlToValidate="txtInstallDate"
                        Operator="DataTypeCheck" SetFocusOnError="True" Type="Date"></asp:CompareValidator>
                </td>
                <td style="width: 15%; height: 0px">
                </td>
                <td style="width: 363px; height: 0px">
                </td>
            </tr>
            <tr bgcolor="#ffffff" valign="top">
                <td style="width: 15%; height: 0px" colspan="2">
                    <asp:Label ID="lblActionTaken" runat="server" Text="Action Taken" Width="124px"></asp:Label>
                    <asp:TextBox ID="txtActionTaken" runat="server" Width="294px" Height="116px" MaxLength="200"
                        TextMode="MultiLine"></asp:TextBox>
                </td>
                <td style="width: 15%; height: 0px" colspan="2">
                    <asp:Label ID="lblRemarks" runat="server" Text="Remarks" Width="124px"></asp:Label>
                    <asp:TextBox ID="txtRemarks" runat="server" Height="116px" MaxLength="800" TextMode="MultiLine"
                        Width="335px"></asp:TextBox>
                </td>
            </tr>
            <tr bgcolor="#ffffff" valign="top">
                <td style="height: 0px" colspan="4">
                </td>
            </tr>
            <tr bgcolor="#ffffff" valign="top">
                <td align="right" colspan="4" style="height: 16px">
                    <asp:LinkButton ID="lnkSave" runat="server" Width="143px">Save</asp:LinkButton>
                    <asp:LinkButton ID="lnkCancel" runat="server" CausesValidation="False" Width="143px"
                        Style="text-align: right">Cancel</asp:LinkButton>
                    &nbsp; &nbsp; &nbsp;
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td colspan="4">
                    &nbsp;
                    <asp:ValidationSummary ID="vsError" runat="server" ShowMessageBox="True" ShowSummary="False" />
                    <cc1:MessageBox ID="MessageBox2" runat="server" />
                    <cc1:MessageBox ID="MessageBox1" runat="server" />
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td colspan="4">
                    <asp:Label ID="lblNoteDown" runat="server" ForeColor="Red" Text="Label"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>