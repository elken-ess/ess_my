<%@ Page Language="VB" AutoEventWireup="false" CodeFile="addappointment.aspx.vb"
    Inherits="PresentationLayer_function_addappointment_aspx" EnableEventValidation="false" %>

<%@ Register Assembly="JCalendar" Namespace="JCalendar" TagPrefix="cc2" %>
<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Add Appointment</title>
    <link href="../css/style.css" type="text/css" rel="stylesheet">

    <script language="JavaScript" src="../js/common.js"></script>

    <script language="javascript">
    
    
 function LoadAppointmentEndTime_CallBack(response){
 
     //if the server-side code threw an exception
     if (response.error != null)
     {
      //we should probably do better than this
      alert(response.error); 
      
      return;
     }

     var ResponseValue = response.value;    
     var al = ResponseValue.split(":");
     var htime = al[0];
     var mtime = al[1];
     document.getElementById("<%=apptEndTimeHBox.ClientID%>").value =htime;
     document.getElementById("<%=apptEndTimeMBox.ClientID%>").value =mtime;
    
     
     
     
}
 
 
 function LoadAppointmentEndTime(objectClient)
{ 

    var starttime = objectClient.value;    
    PresentationLayer_function_addappointment_aspx.GetAppointmentEndTimeChange(starttime, LoadAppointmentEndTime_CallBack);
 
}




 function LoadTechnician_CallBack(response){
 
     //if the server-side code threw an exception
     if (response.error != null)
     {
      //we should probably do better than this
      alert(response.error); 
      
      return;
     }

     var ResponseValue = response.value;
     
   
     //if the response wasn't what we expected  
     if (ResponseValue == null || typeof(ResponseValue) != "object")
     {       
      return;
     }
          
     //Get the states drop down
     var ResultList = document.getElementById("<%=assignToDrpList.ClientID%>");
     ResultList.options.length = 0; //reset the states dropdown
 
    
     //Remember, its length not Length in JavaScript
     for (var i = 0; i < ResponseValue.length; ++i)
     {
     
      //the columns of our rows are exposed like named properties
      var al = ResponseValue[i].split(":");
      var alid = al[0];
      var alname = al[1];
      ResultList.options[ResultList.options.length] =  new Option(alname, alid);
     }
     
     
}
 
 function LoadCR_CallBack(response){
 
     //if the server-side code threw an exception
     if (response.error != null)
     {
      //we should probably do better than this
      alert(response.error); 
      
      return;
     }

     var ResponseValue = response.value;
     
   
     //if the response wasn't what we expected  
     if (ResponseValue == null || typeof(ResponseValue) != "object")
     {       
      return;
     }
          
     //Get the states drop down
     var ResultList = document.getElementById("<%=cboCrID.ClientID%>");
     ResultList.options.length = 0; //reset the states dropdown
 
    
     //Remember, its length not Length in JavaScript
     for (var i = 0; i < ResponseValue.length; ++i)
     {
     
      //the columns of our rows are exposed like named properties
      var al = ResponseValue[i].split(":");
      var alid = al[0];
      var alname = al[1];
      ResultList.options[ResultList.options.length] =  new Option(alname, alid);
     }
     
     
}

function LoadZoneList_CallBack(response){
 
     //if the server-side code threw an exception
     if (response.error != null)
     {
      //we should probably do better than this
      alert(response.error); 
      
      return;
     }

     var ResponseValue = response.value;
     
   
     //if the response wasn't what we expected  
     if (ResponseValue == null || typeof(ResponseValue) != "object")
     {       
      return;
     }
          
     //Get the states drop down
     var ResultList = document.getElementById("<%=cboZoneID.ClientID%>");
     ResultList.options.length = 0; //reset the states dropdown
 
    
     //Remember, its length not Length in JavaScript
     for (var i = 0; i < ResponseValue.length; ++i)
     {
     
      //the columns of our rows are exposed like named properties
      var al = ResponseValue[i].split(":");
      var alid = al[0];
      var alname = al[1];
      ResultList.options[ResultList.options.length] =  new Option(alname, alid);
      
     }
     
     
}

 
 function LoadTechnician(objectClient)
{
 
 var trantype = document.getElementById("<%=transTypeDrpList.ClientID%>").options[document.getElementById("<%=transTypeDrpList.ClientID%>").selectedIndex].value;

 if (objectClient.selectedIndex > 0){
    var countryid ='<%=Session("login_ctry")%>';
    var companyid = '<%=Session("login_rank")%>';
    var servicecenterid = document.getElementById("<%=servCenterDrpList.ClientID%>").options[document.getElementById("<%=servCenterDrpList.ClientID%>").selectedIndex].value;
    var rank = '<%=Session("login_rank")%>';
    var areaid =document.getElementById("<%=areaBox.ClientID%>").value;
    var appointmentdate = document.getElementById("<%=apptDateBox.ClientID%>").value;
     
  if (trantype == 'AS') {
  
    PresentationLayer_function_addappointment_aspx.GetTechnicianIDList(servicecenterid,LoadTechnician_CallBack);
    PresentationLayer_function_addappointment_aspx.GetCrIDList( servicecenterid,LoadCR_CallBack);
    }    
    
    PresentationLayer_function_addappointment_aspx.GetZoneList(trantype, areaid, appointmentdate,servicecenterid, LoadZoneList_CallBack);
    
 }
 else
 {

    document.getElementById("<%=assignToDrpList.ClientID%>").options.length = 0
    document.getElementById("<%=cboCrID.ClientID%>").options.length = 0
 }
}




function LoadRoChange_CallBack(response){
 
     //if the server-side code threw an exception
     if (response.error != null)
     {
      //we should probably do better than this
      alert(response.error); 
      
      return;
     }

     var ResponseValue = response.value;    
//     var al = ResponseValue.split(":");

     var lastservDate = ResponseValue[0];
     var Warranty = ResponseValue[1];
     var installBy = ResponseValue[2];
     var lastRemDate = ResponseValue[3];
     var contractNo = ResponseValue[4];
     var contEDate = ResponseValue[5];
     var lastApptDate= ResponseValue[6];
   
    
    document.getElementById("<%=lastservDateBox.ClientID%>").value =lastservDate;
    document.getElementById("<%=WarrantyBox.ClientID%>").value =Warranty;
    document.getElementById("<%=installByBox.ClientID%>").value =installBy;
    document.getElementById("<%=lastRemDateBox.ClientID%>").value =lastRemDate;
    document.getElementById("<%=contractNoBox.ClientID%>").value =contractNo;
    document.getElementById("<%=contEDateBox.ClientID%>").value =contEDate;
    document.getElementById("<%=lastApptDateBox.ClientID%>").value =lastApptDate;
    
     
}
 
 
 function LoadRoChange(objectClient)
{     
    var RoID= objectClient.value;
    var CustomerPrefix =document.getElementById("<%=CustPrxBox.ClientID%>").value;
    var CustomerID =document.getElementById("<%=CustIDBox.ClientID%>").value;
    var AppointmentDate=document.getElementById("<%=apptDateBox.ClientID%>").value;
      
    PresentationLayer_function_addappointment_aspx.GetRoInformationChange(RoID,CustomerPrefix,CustomerID,AppointmentDate, LoadRoChange_CallBack);
 
}

function LoadAppointmentDateChange()
{

    var trantype = document.getElementById("<%=transTypeDrpList.ClientID%>").options[document.getElementById("<%=transTypeDrpList.ClientID%>").selectedIndex].value;
    var RoID= document.getElementById("<%=ROInfoDrpList.ClientID%>").options[document.getElementById("<%=ROInfoDrpList.ClientID%>").selectedIndex].value;
    var CustomerPrefix =document.getElementById("<%=CustPrxBox.ClientID%>").value;
    var CustomerID =document.getElementById("<%=CustIDBox.ClientID%>").value;
    var AppointmentDate=document.getElementById("<%=apptDateBox.ClientID%>").value;
    var countryid ='<%=Session("login_ctry")%>';
    var companyid = '<%=Session("login_rank")%>';
    var servicecenterid = document.getElementById("<%=servCenterDrpList.ClientID%>").options[document.getElementById("<%=servCenterDrpList.ClientID%>").selectedIndex].value;
    var rank = '<%=Session("login_rank")%>';
    var areaid =document.getElementById("<%=areaBox.ClientID%>").value;
   
    
    PresentationLayer_function_addappointment_aspx.GetRoInformationChange(RoID,CustomerPrefix,CustomerID,AppointmentDate, LoadRoChange_CallBack);
    PresentationLayer_function_addappointment_aspx.GetZoneList(trantype, areaid, AppointmentDate,servicecenterid, LoadZoneList_CallBack);
    
 
}




function CheckScheduleClick() {
        
        var RoID= document.getElementById("<%=ROInfoDrpList.ClientID%>").options[document.getElementById("<%=ROInfoDrpList.ClientID%>").selectedIndex].value;
        var CustomerPrefix =document.getElementById("<%=CustPrxBox.ClientID%>").value;
        var CustomerID =document.getElementById("<%=CustIDBox.ClientID%>").value;
        var AppointmentDate=document.getElementById("<%=apptDateBox.ClientID%>").value;
        var countryid ='<%=Session("login_ctry")%>';
        var companyid = '<%=Session("login_rank")%>';
        var servicecenterid = document.getElementById("<%=servCenterDrpList.ClientID%>").options[document.getElementById("<%=servCenterDrpList.ClientID%>").selectedIndex].value;
        
        var zoneid = document.getElementById("<%=cboZoneID.ClientID%>").options[document.getElementById("<%=cboZoneID.ClientID%>").selectedIndex].value;
    
        var strTempURL = "servCenter=" + servicecenterid + "&apptDate=" + AppointmentDate + "&zoneID=" + zoneid + "&timestamp="+ Date()

        strTempURL = "checkSchedulePopup.aspx?" + strTempURL
                
        showModalDialog(strTempURL,"_calPick","status=no;center=yes;dialogWidth=600pt;dialogHeight=400pt");
        
}

function DetailClick(){
        
        var RoID= document.getElementById("<%=ROInfoDrpList.ClientID%>").options[document.getElementById("<%=ROInfoDrpList.ClientID%>").selectedIndex].value;
        var CustomerPrefix =document.getElementById("<%=CustPrxBox.ClientID%>").value;
        var CustomerID =document.getElementById("<%=CustIDBox.ClientID%>").value;
        var CustomerName =document.getElementById("<%=CustNameBox.ClientID%>").value;
        var AppointmentDate=document.getElementById("<%=apptDateBox.ClientID%>").value;
        var countryid ='<%=Session("login_ctry")%>';
        var companyid = '<%=Session("login_rank")%>';
        var servicecenterid = document.getElementById("<%=servCenterDrpList.ClientID%>").options[document.getElementById("<%=servCenterDrpList.ClientID%>").selectedIndex].value;        
        var zoneid = document.getElementById("<%=cboZoneID.ClientID%>").options[document.getElementById("<%=cboZoneID.ClientID%>").selectedIndex].value;
    

        var strTempURL = "custID=" + CustomerID + "&custPf=" + CustomerPrefix + "&custName=" + CustomerName + "&ROID=" + RoID + "&timestamp="+ Date()
        
        strTempURL = "appointmenthistorydetailPopup.aspx?" + strTempURL
        showModalDialog(strTempURL,"_calPick","status=no;center=yes;dialogWidth=600pt;dialogHeight=400pt");   
}

function ContractHistoryClick() {
        
        var RoID= document.getElementById("<%=ROInfoDrpList.ClientID%>").options[document.getElementById("<%=ROInfoDrpList.ClientID%>").selectedIndex].value;
        var RoName= document.getElementById("<%=ROInfoDrpList.ClientID%>").options[document.getElementById("<%=ROInfoDrpList.ClientID%>").selectedIndex].text;
        var CustomerPrefix =document.getElementById("<%=CustPrxBox.ClientID%>").value;
        var CustomerID =document.getElementById("<%=CustIDBox.ClientID%>").value;
        var CustomerName =document.getElementById("<%=CustNameBox.ClientID%>").value;
        var AppointmentDate=document.getElementById("<%=apptDateBox.ClientID%>").value;
        var countryid ='<%=Session("login_ctry")%>';
        var companyid = '<%=Session("login_rank")%>';
        var servicecenterid = document.getElementById("<%=servCenterDrpList.ClientID%>").options[document.getElementById("<%=servCenterDrpList.ClientID%>").selectedIndex].value;        
        var zoneid = document.getElementById("<%=cboZoneID.ClientID%>").options[document.getElementById("<%=cboZoneID.ClientID%>").selectedIndex].value;
    
     
        var strTempURL  = "ROID=" + RoID + "&ROINFO=" + RoName + "&custName=" + CustomerName + "&custID=" + CustomerID+ "&custPf=" + CustomerPrefix + "&timestamp="+ Date()
                                   
        strTempURL = "contracthistoryPopup.aspx?" + strTempURL;
        showModalDialog(strTempURL,"_calPick","status=no;center=yes;dialogWidth=600pt;dialogHeight=400pt");   
        
        
}

function ServiceHistoryClick() {
        
        var RoID= document.getElementById("<%=ROInfoDrpList.ClientID%>").options[document.getElementById("<%=ROInfoDrpList.ClientID%>").selectedIndex].value;
        var RoName= document.getElementById("<%=ROInfoDrpList.ClientID%>").options[document.getElementById("<%=ROInfoDrpList.ClientID%>").selectedIndex].text;
        var CustomerPrefix =document.getElementById("<%=CustPrxBox.ClientID%>").value;
        var CustomerID =document.getElementById("<%=CustIDBox.ClientID%>").value;
        var CustomerName =document.getElementById("<%=CustNameBox.ClientID%>").value;
        var AppointmentDate=document.getElementById("<%=apptDateBox.ClientID%>").value;
        var countryid ='<%=Session("login_ctry")%>';
        var companyid = '<%=Session("login_rank")%>';
        var servicecenterid = document.getElementById("<%=servCenterDrpList.ClientID%>").options[document.getElementById("<%=servCenterDrpList.ClientID%>").selectedIndex].value;        
        var zoneid = document.getElementById("<%=cboZoneID.ClientID%>").options[document.getElementById("<%=cboZoneID.ClientID%>").selectedIndex].value;
        
        
        //var strTempURL = "ROID=" + RoID + "&ROINFO=" + RoName + "&custName=" + CustomerName + "&custID=" + CustomerID+ "&custPf=" + CustomerPrefix + "&timestamp="+ Date()
        //strTempURL = "serverHistoryPopup.aspx?" + strTempURL;
        //showModalDialog(strTempURL,"_calPick","status=no;center=yes;dialogWidth=600pt;dialogHeight=400pt");   
        
        var strTempURL = "ROID=" + RoID + "&CustomerID=" + CustomerID+ "&customerPrefix=" + CustomerPrefix + "&timestamp="+ Date()
        strTempURL = "ViewAppointmentServiceHistory.aspx?" + strTempURL;
        window.open (strTempURL);
        
}


    </script>

</head>
<body>
    <form id="form1" runat="server">
        <div style="vertical-align: top">
            <table width="100%" border="0">
                <tr>
                    <td style="width: 100%">
                        <table cellspacing="0" cellpadding="0" width="100%" border="0">
                            <tr>
                                <td width="1%" background="../graph/title_bg.gif">
                                    <img height="24" src="../graph/title1.gif" width="5" /></td>
                                <td class="style2" width="98%" background="../graph/title_bg.gif" style="width: 80%">
                                    <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="right" width="1%" background="../graph/title_bg.gif">
                                    <img height="24" src="../graph/title_2.gif" width="5" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="width: 100%">
                        <table style="width: 100%" cellspacing="0" cellpadding="0" border="0">
                            <tr>
                                <td class="style2" background="../graph/title_bg.gif" style="width: 110%;">
                                    <font color="red" style="width: 100%">
                                        <asp:Label ID="errlab" runat="server" Text="Label" Visible="False"></asp:Label></font></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="width: 100%;">
                        <table cellspacing="1" cellpadding="2" bgcolor="#b7e6e6" border="0" style="width: 100%">
                            <tr bgcolor="#ffffff">
                                <td colspan="5">
                                    <asp:Label ID="lblNoteUP" runat="server" ForeColor="Red" Text="Label"></asp:Label>
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td colspan="5">
                                    &nbsp;
                                </td>
                            </tr>
                            <%--Added by Ryan Aimel J. Estandarte 20 Sept 2012--%>
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width: 20%">
                                    <asp:Label ID="lblIsInbound" runat="server" />
                                </td>
                                <td align="left" style="width: 25%">
                                    <%--Modified by Ryan Estandarte 25 Sept 2012--%>
                                    <%--<asp:CheckBox ID="chkIsInbound" runat="server" Text="Yes" />--%>
                                    <asp:RadioButtonList ID="rdoInboundOutbound" runat="server" RepeatDirection="Horizontal" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please select either inbound or outbound"
                                        Text="*" ControlToValidate="rdoInboundOutbound" />
                                </td>
                                <td align="right" style="width: 20%">
                                    <asp:Label ID="RMSlbl" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 25%">
                                    <asp:TextBox ID="RMStxt" runat="server" CssClass="textborder" MaxLength="12" Width="80%"></asp:TextBox></td>
                                <td style="width: 10%">
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width: 20%; height: 38px;">
                                    <asp:Label ID="transtypeLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 25%; height: 38px;">
                                    <%--<asp:TextBox ID="transTypeBox" runat="server" CssClass="textborder" Width="80%" MaxLength="10"></asp:TextBox>--%>
                                    <asp:DropDownList ID="transTypeDrpList" runat="server" AutoPostBack="true" CssClass="textborder"
                                        Width="93%">
                                    </asp:DropDownList>
                                    <font color="red">*</font>
                                    <asp:RequiredFieldValidator ID="transTypeError" runat="server" ControlToValidate="transTypeDrpList"
                                        ForeColor="White">*</asp:RequiredFieldValidator></td>
                                <td align="right" style="width: 20%; height: 38px;">
                                    <asp:Label ID="transNoLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 25%; height: 38px;">
                                    <asp:TextBox ID="transNoBox" runat="server" CssClass="textborder" Width="80%" MaxLength="20"
                                        ReadOnly="True"></asp:TextBox><%--<font color="red">*</font>
                                    <asp:RequiredFieldValidator ID="transNoError" runat="server" ControlToValidate="transNoBox">*</asp:RequiredFieldValidator>--%></td>
                                <td style="width: 10%; height: 38px;">
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width: 20%;">
                                    <asp:Label ID="ROInfoLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 25%;">
                                    <asp:DropDownList ID="ROInfoDrpList" Width="93%" runat="server" CssClass="textborder"
                                        AutoPostBack="false" onchange="LoadRoChange(this)">
                                    </asp:DropDownList>
                                </td>
                                <td align="right" style="width: 20%">
                                    <asp:Label ID="apptStatusLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 25%;">
                                    <asp:TextBox ID="apptStatBox" runat="server" CssClass="textborder" Width="80%" ReadOnly="True"
                                        Visible="False"></asp:TextBox>
                                    <asp:DropDownList ID="DDApptStatus" Width="80%" runat="server" CssClass="textborder"
                                        AutoPostBack="true">
                                    </asp:DropDownList><%--<asp:DropDownList ID="apptStatDrpList" runat="server" CssClass="textborder" Width="90px">
                                    </asp:DropDownList>--%></td>
                                <td style="width: 10%">
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width: 20%">
                                    <asp:Label ID="lastservDateLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 25%">
                                    <asp:TextBox ID="lastservDateBox" runat="server" Width="89%" CssClass="textborder"
                                        ReadOnly="True"></asp:TextBox></td>
                                <td align="right" style="width: 20%;">
                                    <asp:Label ID="WarrantyLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 25%;">
                                    <asp:TextBox ID="WarrantyBox" runat="server" Width="80%" CssClass="textborder" ReadOnly="True"></asp:TextBox></td>
                                <td style="width: 10%">
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width: 20%; height: 30px;">
                                    <asp:Label ID="PICNameLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 25%; height: 30px;">
                                    <asp:TextBox ID="PICNameBox" runat="server" Width="89%" CssClass="textborder" MaxLength="50"></asp:TextBox></td>
                                <td align="right" style="width: 20%; height: 30px;">
                                    <asp:Label ID="PICContactLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 25%; height: 30px;">
                                    <asp:TextBox ID="PICContactBox" runat="server" CssClass="textborder" MaxLength="20"
                                        Width="80%"></asp:TextBox>
                                    <font color="red">*</font>
                                    <asp:RequiredFieldValidator ID="PICContactNullError" runat="server" ControlToValidate="PICContactBox"
                                        ForeColor="White">*</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="PICContactError" runat="server" ControlToValidate="PICContactBox"
                                        ValidationExpression="\d{0,20}" Width="1px">*</asp:RegularExpressionValidator></td>
                                <td style="width: 10%; height: 30px;">
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width: 20%">
                                    <asp:Label ID="CustIDLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 25%">
                                    <asp:TextBox ID="CustIDBox" runat="server" Width="89%" CssClass="textborder" ReadOnly="True"></asp:TextBox></td>
                                <td align="right" style="width: 20%;">
                                    <asp:TextBox ID="CustPrxBox" runat="server" Text="Label" style="visibility:hidden;"></asp:TextBox></td>
                                <td align="left" style="width: 25%;">
                                    &nbsp;</td>
                                <td style="width: 10%">
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width: 20%;">
                                    <asp:Label ID="contEDateLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 25%;">
                                    <asp:TextBox ID="contEDateBox" runat="server" Width="89%" CssClass="textborder" ReadOnly="True"></asp:TextBox></td>
                                <td align="right" style="width: 20%;">
                                    <asp:Label ID="CustNameLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 25%;">
                                    <asp:TextBox ID="CustNameBox" runat="server" Width="80%" CssClass="textborder" ReadOnly="True"></asp:TextBox></td>
                                <td style="width: 10%">
                                </td>
                            </tr>
                        </table>
                        <table cellspacing="1" cellpadding="2" bgcolor="#b7e6e6" border="0" style="width: 100%">
                            <tr bgcolor="#ffffff" style="width: 100%">
                                <td align="right" style="width: 20%;">
                                    <asp:Label ID="addr1Lab" runat="server" Text="Label"></asp:Label>
                                </td>
                                <td style="width: 80%;">
                                    <asp:TextBox ID="addr1Box" runat="server" CssClass="textborder" Width="80%" ReadOnly="True"></asp:TextBox>
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff" style="width: 100%">
                                <td align="right" style="width: 20%;">
                                    <asp:Label ID="addr2Lab" runat="server" Text="Label"></asp:Label>
                                </td>
                                <td style="width: 80%;">
                                    <asp:TextBox ID="addr2Box" runat="server" CssClass="textborder" Width="80%" ReadOnly="True"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                        <table cellspacing="1" cellpadding="2" bgcolor="#b7e6e6" border="0" style="width: 100%"
                            id="TABLE1">
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width: 20%;" id="#ApptLoc">
                                    <asp:Label ID="POCodeLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 25%;">
                                    <asp:TextBox ID="POCodeBox" runat="server" Width="85%" CssClass="textborder" ReadOnly="True"></asp:TextBox></td>
                                <td align="right" style="width: 20%;">
                                    <asp:Label ID="areaLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 25%;">
                                    <asp:TextBox ID="areaBox" runat="server" Width="80%" CssClass="textborder" ReadOnly="True"></asp:TextBox></td>
                                <td style="width: 10%">
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width: 20%;">
                                    <asp:Label ID="contryLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 25%;">
                                    <asp:TextBox ID="contryBox" Width="85%" runat="server" CssClass="textborder" ReadOnly="True"></asp:TextBox></td>
                                <td align="right" style="width: 20%;">
                                    <asp:Label ID="stateLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 25%;">
                                    <asp:TextBox ID="stateBox" Width="80%" runat="server" CssClass="textborder" ReadOnly="True"></asp:TextBox></td>
                                <td style="width: 10%">
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width: 20%;">
                                    <asp:Label ID="apptDateLab" runat="server" Text="Label"></asp:Label><br />
                                    <asp:Label ID="Label2" runat="server" Text="DD/MM/YYYY"></asp:Label></td>
                                <td align="left" style="width: 25%;">
                                    <asp:TextBox ID="apptDateBox" runat="server" CssClass="textborder" Width="73%" MaxLength="10"
                                        onblur="LoadAppointmentDateChange()" AutoPostBack="True"></asp:TextBox>
                                    <font color="red">*</font>
                                    <asp:RequiredFieldValidator ID="apptDateError" runat="server" ControlToValidate="apptDateBox"
                                        Width="1px" ForeColor="White">*</asp:RequiredFieldValidator>
                                    <asp:HyperLink ID="HypCal" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                                        ToolTip="Choose a Date" Enabled="true">Choose a Date</asp:HyperLink>
                                    <cc2:JCalendar ID="apptDateCalendar" runat="server" ImgURL="~/PresentationLayer/graph/calendar.gif"
                                        ControlToAssign="apptDateBox" Visible="false" />
                                    <asp:CompareValidator ID="CompDateVal" runat="server" ControlToValidate="apptDateBox"
                                        Enabled="false" Visible="false" Operator="DataTypeCheck" SetFocusOnError="True"
                                        Type="Date"></asp:CompareValidator>
                                </td>
                                <td align="right" style="width: 20%;">
                                    <asp:Label ID="apptTypeLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 25%;">
                                    <asp:DropDownList ID="apptTypeDrpList" runat="server" CssClass="textborder" Width="93%" AutoPostBack="True">
                                    </asp:DropDownList><font color="red">*</font><asp:RequiredFieldValidator ID="apptTypeError"
                                        runat="server" ControlToValidate="apptTypeDrpList" ForeColor="White">*</asp:RequiredFieldValidator></td>
                                <td style="width: 10%">
                                    <%--<asp:LinkButton ID="chkSchedulelkBtn" runat="server" CausesValidation="False">Link</asp:LinkButton>--%>
                                    <asp:Label ID="chkSchedulelkBtn" OnClick="CheckScheduleClick()" runat="server" CssClass="cursor">Link</asp:Label>
                                </td>
                            </tr>
                            <!--SMR1711-2286 Add Nature of feedback-->
                            <tr bgcolor="#ffffff" id="trNatureFeedback" runat="server">
                                <td style="width: 20%;">                                    
                                </td>
                                <td style="width: 25%;">
                                </td>
                                <td align="right" style="width: 20%;">
                                    <asp:Label ID="NatureFeedbackLab" runat="server" Text="Label"></asp:Label>
                                </td>
                                <td align="left" style="width: 25%;">
                                    <asp:DropDownList ID="ddlNatureFeedback" runat="server" CssClass="textborder" Width="93%">
                                    </asp:DropDownList>
                                    <font color="red">*</font>
                                    <asp:RequiredFieldValidator ID="NatureFeedbackError" runat="server" ControlToValidate="ddlNatureFeedback"
                                        ForeColor="White">*</asp:RequiredFieldValidator>
                                </td>
                                <td style="width: 10%">
                                </td>
                            </tr>                                
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width: 20%;">
                                    <asp:Label ID="apptStartTimeLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 25%;">
                                    <asp:DropDownList ID="apptStartTimeDrpList" runat="server" CssClass="textborder"
                                        Width="93%" AutoPostBack="false" onchange="LoadAppointmentEndTime(this)">
                                    </asp:DropDownList></td>
                                <td align="right" style="width: 20%;">
                                    <asp:Label ID="apptEndTimeLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 25%;">
                                    <asp:TextBox ID="apptEndTimeHBox" runat="server" CssClass="textborder" Width="11%"
                                        MaxLength="2"></asp:TextBox>
                                    <asp:Label ID="Label1" runat="server" Text=":" Width="1px" Font-Bold="False" Font-Size="X-Large"></asp:Label>
                                    <asp:TextBox ID="apptEndTimeMBox" runat="server" CssClass="textborder" Width="11%"
                                        MaxLength="2"></asp:TextBox>
                                    <asp:RangeValidator ID="apptEndTimeHError" runat="server" ControlToValidate="apptEndTimeHBox"
                                        MaximumValue="23" MinimumValue="0" Type="Integer" ForeColor="White">*</asp:RangeValidator>
                                    <asp:RangeValidator ID="apptEndTimeMError" runat="server" ControlToValidate="apptEndTimeMBox"
                                        MaximumValue="59" MinimumValue="0" Type="Integer" ForeColor="White">*</asp:RangeValidator></td>
                                <td style="width: 10%">
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width: 20%;">
                                    <asp:Label ID="servCenterLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 25%;">
                                    <asp:DropDownList ID="servCenterDrpList" runat="server" CssClass="textborder" Width="93%"
                                        AutoPostBack="false" onchange="LoadTechnician(this)">
                                    </asp:DropDownList><font color="red">*</font><asp:RequiredFieldValidator ID="servCIDError"
                                        runat="server" ControlToValidate="servCenterDrpList" ForeColor="White">*</asp:RequiredFieldValidator></td>
                                <td align="right" style="width: 20%;">
                                    <%--SMR1711-2286 remove "Installed By", replaced with "Last Appointment Date" --%>
                                    <asp:Label ID="installByLab" runat="server" Text="Label" style="display:none;"></asp:Label>
                                    <asp:Label ID="lastApptDateLab" runat="server" Text="Label"></asp:Label>
                                <td align="left" style="width: 25%;">
                                    <asp:TextBox ID="installByBox" Width="80%" runat="server" CssClass="textborder" ReadOnly="true" style="display:none;"></asp:TextBox>
                                    <asp:TextBox ID="lastApptDateBox" Width="60%" runat="server" CssClass="textborder"
                                        ReadOnly="true"></asp:TextBox>
                                    <%--<asp:LinkButton ID="detaillkBtn" runat="server" Height="17px" CausesValidation="False">Link</asp:LinkButton>--%>
                                    <asp:Label ID="detaillkBtn" runat="server" Height="17px" CssClass="cursor" onclick="DetailClick()">Link</asp:Label>                                    
                                <td style="width: 10%">
                                    <%--<asp:LinkButton ID="servHistorylkBtn" runat="server" CausesValidation="False">Link</asp:LinkButton>--%>
                                    <asp:Label ID="servHistorylkBtn" runat="server" CssClass="cursor" onclick="ServiceHistoryClick()">Link</asp:Label>
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width: 20%; height: 30px;">
                                    <asp:Label ID="assignToLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 25%; height: 30px;">
                                    <asp:DropDownList ID="assignToDrpList" runat="server" CssClass="textborder" Width="93%">
                                    </asp:DropDownList></td>
                                <td align="right" style="width: 20%; height: 30px;">
                                    <%--SMR1711-2286 remove "Last Reminder Date", replaced with "Contract No." --%>
                                    <asp:Label ID="lastRemDateLab" runat="server" Text="Label" style="display:none;"></asp:Label>
                                    <asp:Label ID="contractNoLab" runat="server" Text="Label"></asp:Label>                                                                      
                                    
                                <td align="left" style="width: 25%; height: 30px;">
                                    <asp:TextBox ID="lastRemDateBox" Width="80%" runat="server" CssClass="textborder"
                                        ReadOnly="True" style="display:none;"></asp:TextBox>
                                    <asp:TextBox ID="contractNoBox" Width="80%" runat="server" CssClass="textborder"
                                        ReadOnly="true"></asp:TextBox>
                                </td>
                                <td style="width: 10%;">
                                    <%--<asp:LinkButton ID="contractHistroylkBtn" runat="server" Height="17px" CausesValidation="False">Link</asp:LinkButton>--%>
                                    <asp:Label ID="contractHistroylkBtn" runat="server" Height="35px" CssClass="cursor"
                                        onclick="ContractHistoryClick()">Link</asp:Label>
                                </td>                                
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width: 20%;">
                                    <asp:Label ID="lblCrID" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 25%;">
                                    <asp:DropDownList ID="cboCrID" Width="93%" runat="server" CssClass="textborder">
                                    </asp:DropDownList>
                                    <asp:Label ID="lblAsc" runat="server" Text="*" ForeColor="red" Visible="false"></asp:Label>
                                </td>
                                <td align="right" style="width: 20%;">
                                </td>
                                <td align="left" style="width: 25%;">
                                </td>
                                <td style="width: 10%">
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff" id="trReasonObjection" runat="server">
                                <td align="right" style="width: 20%;">
                                    <asp:Label ID="ReasonObjection" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 25%;">
                                    <asp:DropDownList ID="ddlReasonObjection" Width="93%" runat="server" CssClass="textborder">
                                    </asp:DropDownList>
                                    <asp:Label ID="LabelReasonObjection" runat="server" Text="*" ForeColor="red" Visible="false"></asp:Label>
                                </td>
                                <td align="right" style="width: 20%;">
                                </td>
                                <td align="left" style="width: 25%;">
                                </td>
                                <td style="width: 10%">
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width: 20%;">
                                </td>
                                <td align="left" style="width: 25%;">
                                    <td align="right" style="width: 20%;">
                                    </td>
                                    <td align="left" style="width: 25%;">
                                    </td>
                                    <td style="width: 10%">
                                    </td>
                            </tr>
                        </table>
                        <table cellspacing="1" cellpadding="2" bgcolor="#b7e6e6" border="0" style="width: 100%">
                            <tr bgcolor="#ffffff" style="width: 100%">
                                <td align="right" style="width: 20%; height: 101px;">
                                    <asp:Label ID="notesLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 50%; height: 101px;">
                                    <asp:TextBox ID="notesBox" runat="server" CssClass="textborder" Height="85px" Width="95%"
                                        MaxLength="400" TextMode="MultiLine"></asp:TextBox></td>
                                <td style="width: 15%; height: 101px;">
                                </td>
                                <td style="width: 15%; height: 101px;">
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff" style="width: 100%">
                                <td align="right" style="width: 20%;">
                                    <asp:Label ID="custNotesLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 50%;">
                                    <asp:TextBox ID="custNotesBox" runat="server" CssClass="textborder" Height="98px"
                                        Width="95%" ReadOnly="True" TextMode="MultiLine"></asp:TextBox></td>
                                <td align="left" style="width: 15%" colspan="2">
                                    <asp:Label ID="assToZoneLab" runat="server" Text="Label"></asp:Label>
                                    <%--</td><td align="left" style="width: 30%">--%>
                                    <asp:DropDownList ID="cboZoneID" runat="server" CssClass="textborder" Width="93%"
                                        AutoPostBack="false">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                        <table cellspacing="1" cellpadding="2" bgcolor="#b7e6e6" border="0" style="width: 100%">
                                        <tr bgcolor="#ffffff">
                                            <td>
                                                <table style="width: 100%">
                                                    <tr>
                                                        <th align="left">
                                                            Send SMS
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:RadioButtonList ID="rblSendSMS" runat="server" RepeatDirection="Horizontal"
                                                                AutoPostBack="true">
                                                                <asp:ListItem Text="Yes" Value="Y" Selected="True"></asp:ListItem>
                                                                <asp:ListItem Text="No" Value="N"></asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table id="tblMobile" runat="server">
                                                                <tr>
                                                                    <td>
                                                                        <asp:RadioButtonList ID="rblMobile" runat="server" RepeatDirection="Horizontal" AutoPostBack="true">
                                                                            <asp:ListItem Text="Mobile 1" Value="M1" Selected="True"></asp:ListItem>
                                                                            <asp:ListItem Text="Mobile 2" Value="M2"></asp:ListItem>
                                                                        </asp:RadioButtonList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="txtMobile" runat="server" ReadOnly="true" />&nbsp;
                                                                        <asp:TextBox ID="txtMobileName" runat="server" ReadOnly="true" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                        <asp:LinkButton ID="saveButton" runat="server"></asp:LinkButton>&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:HyperLink ID="cancelLink" runat="server">[cancelLink]</asp:HyperLink>
                    </td>
                </tr>
                <tr bgcolor="#ffffff">
                    <td colspan="4">
                        &nbsp;
                    </td>
                </tr>
                <tr bgcolor="#ffffff">
                    <td colspan="4">
                        <asp:Label ID="lblNoteDown" runat="server" ForeColor="Red" Text="Label"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <asp:RequiredFieldValidator ID="rfvCrID" runat="server" ControlToValidate="cboCrID"
            ForeColor="WhiteSmoke">*</asp:RequiredFieldValidator>
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
            ShowSummary="False" />
        <cc1:MessageBox ID="InfoMsgBox" runat="server" />
        <cc1:MessageBox ID="saveMsgBox" runat="server"></cc1:MessageBox>
    </form>
</body>
</html>
