Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Partial Class PresentationLayer_Function_UpdateServiceBillPart2
    Inherits System.Web.UI.Page
    Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try

        If (Not Page.IsPostBack) Then
            txtStartDate.Text = System.DateTime.Today
            txtEndDate.Text = System.DateTime.Today
            BindGrid()

        End If
        HypCal1.NavigateUrl = "javascript:DoCal(document.form1.txtStartDate);"
        HypCal2.NavigateUrl = "javascript:DoCal(document.form1.txtEndDate);"



    End Sub
    Protected Sub BindGrid()
        
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        solab.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-INVOICENO")
        phnolab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-customer-02")
        CNlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-customer-03")
        snlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-customer-04")
        searchbtn.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-SEARCH")
        titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-update2")
        ctryStat.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCTRY-0004")
        Me.lblNoRecord.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-NR")
        'Me.lblStartDate.Text = objXmlTr.GetLabelName("EngLabelMsg", "INVSTARTDATE")'''''''''''''''''''''''''''''''''''removed: tomas 20120215
        'Me.lblEndDate.Text = objXmlTr.GetLabelName("EngLabelMsg", "INVENDDATE")'''''''''''''''''''''''''''''''''''removed: tomas 20120215

        Me.lblAppointmentNo.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-APPOINMENTNO")
        Me.lblCustomerID.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTMDSL-0001")
        Me.lblManualServiceBillNo.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-SERVICEBILLNO")

        lblNoRecord.Visible = False
        '--------------------------------------------------------------------
        Dim cntryStat As New clsUtil()
        Dim statParam As ArrayList = New ArrayList
        statParam = cntryStat.searchconfirminf("SerStat")
        Dim count As Integer
        Dim statid As String
        Dim statnm As String
        For count = 0 To statParam.Count - 1
            statid = statParam.Item(count)
            statnm = statParam.Item(count + 1)
            count = count + 1

            If statid = "UM" Or statid = "UA" Or statid = "BL" Or UCase(statid) = "AL" Then
                ctryStatDrop.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
            End If
        Next
        ctryStatDrop.Items(0).Selected = True
        '----------------------------------------------------------------------------------------------
        'Dim objServiceBillPart12 As New clsupdateservice2()

        ''Amended by KSLim on 18/6/2006
        'objServiceBillPart12.SOID = soText.Text
        'objServiceBillPart12.CustomerName = CNText.Text
        'objServiceBillPart12.PhoneNo = phnoText.Text
        'objServiceBillPart12.SerialNo = snText.Text
        'objServiceBillPart12.UserID = Session("userID").ToString()
        'objServiceBillPart12.STATUS = ctryStatDrop.Text     '5.16增加
        ''Amended by KSLim on 18/6/2006

        'Dim partall As DataSet = objServiceBillPart12.SelectAllServiceBillPart2()
        'partView.DataSource = partall

        '---------------------------------------------------------------------------------------------- Get Service Center List -- copied from Tomas 20120104
        Dim SVC As New DataSet
        Dim installSVC As New clsCommonClass
        installSVC.sparea = Session("login_svcID")
        installSVC.spctr = Session("login_ctryID")
        installSVC.spstat = Session("login_cmpID")
        installSVC.rank = Session("login_rank")

        SVC = installSVC.Getcomidname("BB_TOMAS_SVC")
        If SVC.Tables.Count <> 0 Then
            svcbonds(SVC, SVClist)
        End If

        partView.AllowPaging = True
        partView.AllowSorting = True
        Dim editcol As New CommandField
        editcol.EditText = objXmlTr.GetLabelName("EngLabelMsg", "BB-Edit") '"edit"
        editcol.HeaderImageUrl = "~/PresentationLayer/graph/edit.gif"
        editcol.ShowEditButton = True
        partView.Columns.Add(editcol)
        'partView.DataBind()
        DisplayGridHeader()
        lblNoRecord.Visible = False


        '''access control'''''''''''''''''''''''''''''''''''''''''''''''
        Dim accessgroup As String = Session("accessgroup").ToString
        Dim purviewArray As Array = New Boolean() {False, False, False, False}
        purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "31")

        editcol.Visible = purviewArray(2)


    End Sub

    Sub DisplayGridHeader()
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        If partView.Rows.Count <> 0 Then
            partView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-INVOICENO") '("EngLabelMsg", "BB-01") '''''''''''''''''''''''''''''added: Tomas 20120216
            partView.HeaderRow.Cells(1).Font.Size = 8
            partView.HeaderRow.Cells(2).Text = "Service Order No" 'objXmlTr.GetLabelName("EngLabelMsg", "BB-06")'("EngLabelMsg", "SBA-INVOICENO")'''''''''''''''''''''''''''''added: Tomas 20120216
            partView.HeaderRow.Cells(2).Font.Size = 8
            partView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-04") '("EngLabelMsg", "BB-05")'''''''''''''''''''''''''''''added: Tomas 20120216
            partView.HeaderRow.Cells(3).Font.Size = 8
            partView.HeaderRow.Cells(4).Text = "Service Bill No" 'objXmlTr.GetLabelName("EngLabelMsg", "BB-04")'''''''''''''''''''''''''''''added: Tomas 20120216
            partView.HeaderRow.Cells(4).Font.Size = 8
            partView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-07") '("EngLabelMsg", "BB-06")'''''''''''''''''''''''''''''added: Tomas 20120216
            partView.HeaderRow.Cells(5).Font.Size = 8
            partView.HeaderRow.Cells(6).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-05") '("EngLabelMsg", "BB-07")'''''''''''''''''''''''''''''added: Tomas 20120216
            partView.HeaderRow.Cells(6).Font.Size = 8
            partView.HeaderRow.Cells(7).Text = "Technician Name" 'objXmlTr.GetLabelName("EngLabelMsg", "BB-CR-09")'''''''''''''''''''''''''''''added: Tomas 20120216
            partView.HeaderRow.Cells(7).Font.Size = 8
            partView.HeaderRow.Cells(8).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CR-09") '''''''''''''''''''''''''''''added: Tomas 20120216
            partView.HeaderRow.Cells(8).Font.Size = 8
            partView.HeaderRow.Cells(9).Text = "Customer Name" '''''''''''''''''''''''''''''added: Tomas 20120216
            partView.HeaderRow.Cells(9).Font.Size = 8

            ' Added by Ryan Estandarte 9 April 2012
            partView.HeaderRow.Cells(10).Visible = False

            partView.Visible = True
            lblNoRecord.Visible = False
        Else
            lblNoRecord.Visible = True
        End If
    End Sub
    Protected Sub searchbtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles searchbtn.Click

        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Dim objServiceBillPart12 As New clsupdateservice2()
        Dim lstrStartDate, lstrEndDate As String

        lstrStartDate = Me.txtStartDate.Text
        lstrEndDate = Me.txtEndDate.Text

        objServiceBillPart12.SOID = soText.Text
        objServiceBillPart12.CustomerName = CNText.Text
        objServiceBillPart12.PhoneNo = phnoText.Text
        objServiceBillPart12.SerialNo = snText.Text
        'Amended by KSLim on 18/6/2006
        objServiceBillPart12.UserID = Session("userID").ToString()
        'Amended by KSLim on 18/6/2006
        If (ctryStatDrop.Text = "AL") Then
            objServiceBillPart12.STATUS = ""
        Else
            objServiceBillPart12.STATUS = ctryStatDrop.Text
        End If
        ' objServiceBillPart12.STATUS = ctryStatDrop.Text     '5.16增加
        objServiceBillPart12.StartBillDate = lstrStartDate
        objServiceBillPart12.EndBillDate = lstrEndDate

        objServiceBillPart12.CustomerID = Me.txtCustomerID.Text
        ' Modified by Ryan Estandarte 6 Mar 2012
        'objServiceBillPart12.CustomerPrefix = Session("login_ctryid")
        objServiceBillPart12.CustomerPrefix = Session("login_ctryid").ToString()
        objServiceBillPart12.AppointmentPrefix = Me.txtAppointmentPrefix.Text
        objServiceBillPart12.AppointmentNo = Me.txtAppointmentNo.Text
        objServiceBillPart12.ManualServiceBillNo = Me.txtManualServiceBillNo.Text
        objServiceBillPart12.Remark = SVClist.SelectedItem.Value.ToString() '''''''''''''''''''''''''''Added: Tomas 20120215
        objServiceBillPart12.Technician = technicianList.SelectedValue '''''''''''''''''''''''''''''added: Tomas 20120215
        objServiceBillPart12.AppointmentFix = cboServiceBillType.SelectedItem.Value.ToString()
        objServiceBillPart12.RMSNo = RMStxt.Text ''''''''''''''''''''''added by Tomas 20180620

        If Not IsDate(lstrStartDate) And lstrStartDate <> "" Then
            partView.Visible = False
            Exit Sub
        End If

        If Not IsDate(lstrEndDate) And lstrEndDate <> "" Then
            partView.Visible = False
            Exit Sub
        End If


        Dim selDS As DataSet = objServiceBillPart12.SelectAllServiceBillPart2()

        If Not selDS Is Nothing Then
            partView.DataSource = selDS

            partView.DataBind()
            ' Added by Ryan Estandarte 9 April 2012
            Session("selDS") = selDS
       
        End If

        DisplayGridHeader()
    End Sub
    Protected Sub partView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles partView.PageIndexChanging
        partView.PageIndex = e.NewPageIndex

        ' Modified by Ryan Estandarte 9 April 2012
        'System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        ''Dim partall As DataSet = Session("partView")
        'Dim objServiceBillPart12 As New clsupdateservice2()
        'Dim lstrStartDate, lstrEndDate As String

        'lstrStartDate = Me.txtStartDate.Text
        'lstrEndDate = Me.txtEndDate.Text

        'objServiceBillPart12.SOID = soText.Text
        'objServiceBillPart12.CustomerName = CNText.Text
        'objServiceBillPart12.PhoneNo = phnoText.Text
        'objServiceBillPart12.SerialNo = snText.Text
        ''Amended by KSLim on 18/6/2006
        'objServiceBillPart12.UserID = Session("userID").ToString()
        ''Amended by KSLim on 18/6/2006
        'If (ctryStatDrop.Text = "AL") Then
        '    objServiceBillPart12.STATUS = ""
        'Else
        '    objServiceBillPart12.STATUS = ctryStatDrop.Text
        'End If
        ''5.16增加
        'objServiceBillPart12.StartBillDate = lstrStartDate
        'objServiceBillPart12.EndBillDate = lstrEndDate

        'objServiceBillPart12.CustomerID = Me.txtCustomerID.Text
        'objServiceBillPart12.CustomerPrefix = Session("login_ctryid").ToString()
        'objServiceBillPart12.AppointmentPrefix = Me.txtAppointmentPrefix.Text
        'objServiceBillPart12.AppointmentNo = Me.txtAppointmentNo.Text
        'objServiceBillPart12.ManualServiceBillNo = Me.txtManualServiceBillNo.Text

        '' Added by Ryan Estandarte 9 April 2012
        'objServiceBillPart12.AppointmentFix = cboServiceBillType.SelectedValue

        'If Not IsDate(lstrStartDate) And lstrStartDate <> "" Then
        '    partView.Visible = False
        '    Exit Sub
        'End If

        'If Not IsDate(lstrEndDate) And lstrEndDate <> "" Then
        '    partView.Visible = False
        '    Exit Sub
        'End If

        'Dim selDS As DataSet = objServiceBillPart12.SelectAllServiceBillPart2()

        Dim selDS As DataSet = CType(Session("selDS"), DataSet)

        partView.DataSource = selDS
        partView.DataBind()
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        DisplayGridHeader()
    End Sub
#Region " bonds"
    '----------------------------------------------------------------------------------------------- Binding Technician to DropDown -- Tomas 20111214
    Public Function databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        Dim row As DataRow
        If ds.Tables(0).Rows.Count > 0 Then
            dropdown.Items.Add(" ")
            For Each row In ds.Tables(0).Rows
                Dim NewItem As New ListItem()
                NewItem.Text = row("name") & "-" & row("id")
                NewItem.Value = row("id")
                dropdown.Items.Add(NewItem)
            Next

        End If
    End Function
    Public Function svcbonds(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        '----------------------------------------------------------------------------------------------- Binding SVC to DropDown -- Tomas 20111214
        dropdown.Items.Clear()
        Dim row As DataRow
        If ds.Tables(0).Rows.Count > 0 Then
            dropdown.Items.Add(" ")
            For Each row In ds.Tables(0).Rows
                Dim NewItem As New ListItem()
                NewItem.Text = row("id") & "-" & row("name")
                NewItem.Value = row("id")
                dropdown.Items.Add(NewItem)
            Next

        End If
    End Function

#End Region
    
    ''' <remarks>Added by Ryan Estandarte 9 April 2012</remarks>
    Protected Sub partView_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles partView.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(10).Visible = False
        End If

    End Sub
    Protected Sub partView_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles partView.RowEditing
        'Dim ds As DataSet = Session("partView")
        'Dim type As String = ds.Tables(0).Rows(partView.PageIndex * partView.PageSize + e.NewEditIndex).Item(0).ToString()
        Dim type As String = partView.Rows(e.NewEditIndex).Cells(1).Text
        type = Server.UrlEncode(type)
        'Dim no As String = ds.Tables(0).Rows(partView.PageIndex * partView.PageSize + e.NewEditIndex).Item(1).ToString
        Dim NO As String = partView.Rows(e.NewEditIndex).Cells(2).Text
        NO = Server.UrlEncode(NO)

        'Added by Ryan Estandarte 9 April 2012
        If NO.Contains("nbsp") Then
            NO = partView.Rows(e.NewEditIndex).Cells(10).Text
        End If

        Dim strTempURL As String = "type=" + type + "&no=" + NO
        'strTempURL = "~/PresentationLayer/Function/CRUpdate.aspx?" + strTempURL
        strTempURL = "~/PresentationLayer/Function/ServiceBillUpdate2.aspx?" + strTempURL
        Response.Redirect(strTempURL)
    End Sub

    '-- Michelle 2012-01-04
    Protected Sub SVCList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'Dim svcID As Integer
        'If SVClist.SelectedValue <> "ZZZ" Then
        '    svcID = SVClist.SelectedValue
        'Else
        '    svcID = 0
        'End If

        Dim svcID As String
        svcID = SVClist.SelectedValue

        Dim tech As New DataSet
        Dim installbond As New clsCommonClass
        installbond.sparea = svcID
        installbond.spctr = Session("login_ctryID").ToString()
        installbond.spstat = Session("login_cmpID").ToString()
        installbond.rank = Session("login_rank").ToString()
        tech = installbond.Getcomidname("BB_MASTECH_IDNAME")
        If tech.Tables.Count <> 0 Then
            databonds(tech, technicianList)
        End If

        DisplayGridHeader()
    End Sub

    Protected Sub partView_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles partView.SelectedIndexChanged

    End Sub
End Class
