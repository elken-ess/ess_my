﻿Imports BusinessEntity
Imports SQLDataAccess
Imports System.Data
Imports CrystalDecisions.Web
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.Configuration
Imports System.Globalization

Partial Class PresentationLayer_Function_ApptAlloc_aspx
    Inherits System.Web.UI.Page
    Private apptalloc As clsApptAlloc
    Private objXmlTr As New clsXml()
    Private Msg As New MessageBox(Me.page)
    Private ZoneEntity As clsZone
    Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.MaintainScrollPositionOnPostBack = True
        AjaxPro.Utility.RegisterTypeForAjax(GetType(PresentationLayer_Function_ApptAlloc_aspx))

        If (Not Page.IsPostBack) Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try
            'txtApptDate.Text = Format(Now, "dd/MM/yyyy")
            HypCal.NavigateUrl = "javascript:DoCal(document.form1.txtApptDate);"
            BindPage()
        Else
            'Msg = New MessageBox(Me.Page)
            apptalloc = Session.Contents("apptalloc")
            Me.lblMessage.Text = ""
        End If
        If DDLtech.SelectedValue <> "" Then
            Session("TechID") = Me.DDLtech.SelectedValue
        Else
            Session("TechID") = ""
        End If

    End Sub

    Protected Sub BindPage()

        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        'display label message
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        Me.lblApptdate.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_D_APPTDATE")
        Me.lblCenter.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_D_CENTER")
        Me.lblFromZone.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_D_FROMZONE")
        Me.lblToZone.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_D_TOZONE")
        'Me.lblTech.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_D_TECH")
        Me.lbladdr1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_D_ADDR1")
        Me.lblAddr2.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_D_ADDR2")
        Me.lblAreaID.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_D_AREAID")
        Me.lblCustIDNM.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_D_CUSTIDNM")
        Me.lblPO.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_D_PO")
        Me.lblZoneID.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_D_ZONEID")
        Me.lblAreaCust.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_D_AREACUST")
        Me.lblAssigned.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_D_ASSIGNED")
        Me.lblShow.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_D_UNASSIGNEDSUM")
        lblNoteUP.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
        lblNoteDown.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")

        Me.lbtnRetri.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_D_RETRIEVE")
        Me.lbtnAddAPPT.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_D_ADDAPPT")
        Me.lbtnAddAll.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_D_ADDAREA")
        Me.lbtnAddZone.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_D_ADDZONE")
        Me.lbtnDelAll.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_D_DELALL")
        Me.lbtnDelAppt.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_D_DELAPPT")
        Me.lbtnEdit.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_D_EDIT")
        Me.lbtnPrint.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_D_PRINT")
        Me.lbtnSave.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_D_SAVE")
        Me.lbtnSaveTech.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_D_SAVETECH")
        Me.titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_D_TITLE")
        Me.lblCustomerID.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0001")
        Me.lblAppointmentNo.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSOSA-APPTNO")

        Me.lbtnPrint.PostBackUrl = "~/PresentationLayer/function/RptTechApptAlloc.aspx"
        'display error message
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Me.SerCenIDerr.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-D-HELP2")
        Me.RFVDate.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-D-HELP1")
        'Me.RFVcenter.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-D-HELP2")



        Me.lbxAssigned.Items.Clear()
        SetCtrlStatus(True)

        ZoneEntity = New clsZone()
        ZoneEntity.UserID = Session("userID")
        Dim SerCentInfo As DataSet = ZoneEntity.GetSerCent()
        If SerCentInfo Is Nothing Then
            Response.Redirect("~/PresentationLayer/error.aspx")
        End If
        SerCenIDDrop.Items.Add("")
        SerCenIDDrop.DataSource = SerCentInfo
        SerCenIDDrop.DataBind()
        SerCenIDDrop.Items.Insert(0, "")
        SerCenIDDrop.SelectedValue = Session("login_svcid")
        'For i = 0 To SerCentInfo.Tables(0).Rows.Count - 1
        '    SerCenIDDrop.Items.Add(SerCentInfo.Tables(0).Rows(i).Item("MSVC_SVCNAME"))
        'Next

        setPurview()
        If Request.Form("txtApptDate") = "" Then
            txtApptDate.Text = Format(Now, "dd/MM/yyyy")
        End If
    End Sub


    Sub RetrieveAppointment()
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Me.objXmlTr.XmlFile = ConfigurationManager.AppSettings("StatMsg")

        Me.ClearPage()
        '***************************'
        If (Session.Contents("apptalloc") IsNot Nothing) Then
            Session.Remove("apptalloc")
        End If

        Me.apptalloc = New clsApptAlloc(Session("userID"))
        '***************************'

        Dim strDate As String = Request.Form("txtApptDate")
        If Not IsDate(strDate) Then

            Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-MASJOBS-0008")
            Return
        End If



        Dim result As Integer = apptalloc.FillDataSet(strDate, SerCenIDDrop.SelectedItem.Value, _
                                                 Me.txtFromZone.Text, Me.txtToZone.Text, DDLtech.Text, Me.txtAppointmentNo.Text, Me.txtCustomerID.Text, Me.techorsub.SelectedValue) 'SerCenIDDrop.Text.Trim(), _
        Session.Contents("apptalloc") = apptalloc
        '***************************'

        If result = 0 Then
            Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-D-ERROR3")
            'Response.Redirect("~/PresentationLayer/error.aspx")
        ElseIf result < 0 Then
            Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-D-WARNING1")
        Else
            Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-D-SUCCESS3")
            Me.BuildTree()
            Me.BindDDL()
            ShowUnassignedSum()
        End If
        txtApptDate.Text = Request.Form("txtApptDate")
    End Sub

    Protected Sub lbtnRetri_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnRetri.Click
        RetrieveAppointment()
    End Sub
    Private Sub BuildTree()
        Try
            Me.objXmlTr.XmlFile = ConfigurationManager.AppSettings("StatMsg")
            '***************************'
            If Me.apptalloc Is Nothing Then
                Return
            End If

            Me.TVareacust.Nodes.Clear()  '无数据时应清空界面

            Dim zonestat As New DataTable
            zonestat = Me.apptalloc.GetApptStat()
            If zonestat Is Nothing Then
                Return
            End If
            If zonestat.Rows.Count = 0 Then
                Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-D-SUCCESS4")
                Return
            End If
            '***************************'
            For i As Integer = 0 To zonestat.Rows.Count - 1
                Dim strZone As String
                Dim unsgnrow As DataRow()
                Dim rwo As DataRow = zonestat.Rows(i)
                strZone = zonestat.Rows(i).ItemArray(0).ToString() + "(" + _
                         zonestat.Rows(i).ItemArray(2).ToString() + "/" + _
                         zonestat.Rows(i).ItemArray(1).ToString() + ")"
                Me.TVareacust.Nodes.Add(New TreeNode(strZone, zonestat.Rows(i).ItemArray(0).ToString()))
                '***************************'
                unsgnrow = Me.apptalloc.GetUnAssignedAppt( _
                                         zonestat.Rows(i).ItemArray(0).ToString() _
                                         )
                If unsgnrow IsNot Nothing Then
                    If unsgnrow.Length > 0 Then
                        Dim index As Index = Me.apptalloc.getIndex(0)
                        For j As Integer = 0 To unsgnrow.Length - 1
                            Dim text As String, val As String
                            text = unsgnrow(j).ItemArray(index.CustName).ToString() + "-" + _
                                     unsgnrow(j).ItemArray(index.StartTime).ToString()
                            val = unsgnrow(j).ItemArray(index.TransType).ToString() + "/" + _
                                  unsgnrow(j).ItemArray(index.TransNO).ToString()
                            Me.TVareacust.Nodes(i).ChildNodes.Add(New TreeNode(text, val))
                            'TVareacust.Nodes(i).ChildNodes.Item(j).ToolTip = "customer address " & text
                            'added by deyb 26092006

                            Dim custinfo As PersonInfo
                            custinfo = Me.apptalloc.GetCustInfo(unsgnrow(j).ItemArray(index.TransType).ToString(), unsgnrow(j).ItemArray(index.TransNO).ToString())
                            Me.TVareacust.Nodes(i).ChildNodes.Item(j).ToolTip = custinfo.addr1 + Chr(13) + custinfo.addr2


                            '-----------------------------
                            Me.TVareacust.Nodes(i).ToolTip = strZone
                        Next
                        Continue For
                    End If
                End If

                Me.TVareacust.Nodes(i).ChildNodes.Add(New TreeNode("", ""))
                Me.TVareacust.Nodes(i).ChildNodes(0).ShowCheckBox = False


            Next
        Catch ex As Exception
        End Try
    End Sub
    Private Sub BindDDL()
        If Me.apptalloc Is Nothing Then
            Return
        Else
            If Me.apptalloc.GetTechBySVC() Is Nothing Then
                Return
            End If
            Me.DDLtech.DataSource = Me.apptalloc.GetTechBySVC()
            Me.DDLtech.DataTextField = "MTCH_ENAME"
            Me.DDLtech.DataValueField = "MTCH_TCHID"
            Me.DDLtech.DataBind()
            Me.DDLtech.Items.Insert(0, New ListItem("", ""))
            Me.ddlZone.Enabled = True
            If Me.apptalloc.GetZonesBySVC() Is Nothing Then
                Return
            End If
            Me.ddlZone.DataSource = Me.apptalloc.GetZonesBySVC()
            Me.ddlZone.DataTextField = "FZN1_ZONID"
            Me.ddlZone.DataBind()
            Me.ddlZone.Items.Insert(0, "")
            Me.ddlZone.Enabled = False
            Me.ddlZone.Visible = False

            Me.ddlArea.Enabled = True
            If Me.apptalloc.GetAreasBySVC() Is Nothing Then
                Return
            End If
            Me.ddlArea.DataSource = Me.apptalloc.GetAreasBySVC()
            Me.ddlArea.DataTextField = "AREID"
            Me.ddlArea.DataBind()
            Me.ddlArea.Items.Insert(0, "")
            Me.ddlArea.Enabled = False
            Me.ddlArea.Visible = False
        End If
    End Sub
    Protected Sub TVareacust_SelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TVareacust.SelectedNodeChanged
        Try
            If Me.TVareacust.SelectedNode.Depth = 1 Then
                Me.TVareacust.SelectedNode.Checked = True
                Dim str, strtype, strno As String
                str = Me.TVareacust.SelectedNode.Value
                strtype = str.Substring(0, str.IndexOf("/"))
                strno = str.Substring(str.IndexOf("/") + 1)
                Me.ShowCustInfo(strtype, strno)
            Else
                Me.TVareacust.SelectedNode.Collapse()
                For i As Integer = 0 To Me.TVareacust.Nodes.Count - 1
                    If Me.TVareacust.Nodes(i).Equals(Me.TVareacust.SelectedNode) Then
                        For j As Integer = 0 To Me.TVareacust.Nodes(i).ChildNodes.Count - 1
                            Me.TVareacust.Nodes(i).ChildNodes(j).Checked = True
                        Next
                    Else
                        For j As Integer = 0 To Me.TVareacust.Nodes(i).ChildNodes.Count - 1
                            Me.TVareacust.Nodes(i).ChildNodes(j).Checked = False
                        Next
                    End If
                Next

                SetCtrlStatus(False)
                Me.txtCust.Text = ""
                Me.txtPO.Text = ""
                Me.txtAddr1.Text = ""
                Me.txtAddr2.Text = ""
                Me.ddlZone.Visible = False
                Me.ddlArea.Visible = False
                SetCtrlStatus(True)
            End If
            Me.lbxAssigned.ClearSelection()
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub lbtnAddAPPT_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnAddAPPT.Click
        Try
            Me.objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            If Me.DDLtech.SelectedValue = "" Then
                Me.Msg.Show(objXmlTr.GetLabelName("StatusMessage", "BB-FUN-D-HELP6"))
                Return
            End If
            If Me.TVareacust.CheckedNodes.Count < 1 Then
                Me.Msg.Show(objXmlTr.GetLabelName("StatusMessage", "BB-FUN-D-HELP4"))
                Return
            Else
                For i As Integer = 0 To Me.TVareacust.CheckedNodes.Count - 1
                    If Me.TVareacust.CheckedNodes(i).Depth = 1 Then

                        If Me.TVareacust.CheckedNodes(i).Value.Trim <> "" Then
                            Me.lbxAssigned.Items.Add( _
                                         New ListItem(Me.TVareacust.CheckedNodes(i).Text, _
                                                    Me.TVareacust.CheckedNodes(i).Value) _
                                        )
                        End If
                        Dim str, strtype, strno As String
                        str = Me.TVareacust.CheckedNodes(i).Value
                        If Not str.Equals("") Then
                            strtype = str.Substring(0, str.IndexOf("/"))
                            strno = str.Substring(str.IndexOf("/") + 1)
                            If apptalloc.AddAppt(strtype, strno, Me.DDLtech.SelectedValue) = -1 Then
                                'Response.Redirect("~/PresentationLayer/error.aspx")
                            End If
                        End If
                    End If
                Next

                BuildTree()
                'Me.TVareacust.ExpandAll()

                ShowUnassignedSum()
            End If
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub lbtnDelAppt_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnDelAppt.Click
        Try
            Dim indexs As Integer() = Me.lbxAssigned.GetSelectedIndices()
            If indexs.Length > 0 Then
                Dim delItems As New ListItemCollection
                Dim str, strtype, strno As String
                For i As Integer = 0 To indexs.Length - 1
                    str = Me.lbxAssigned.Items(indexs(i)).Value
                    strtype = str.Substring(0, str.IndexOf("/"))
                    strno = str.Substring(str.IndexOf("/") + 1)
                    If Me.apptalloc.RemoveAppt(strtype, strno) = -1 Then
                        'Response.Redirect("~/PresentationLayer/error.aspx")
                    End If
                    delItems.Add(Me.lbxAssigned.Items(indexs(i)))
                Next
                '***************************'
                For j As Integer = 0 To delItems.Count - 1
                    Me.lbxAssigned.Items.Remove(delItems(j))
                Next
                '***************************
                BuildTree()
                ShowUnassignedSum()
                'Me.TVareacust.ExpandAll()
            Else
                Me.objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                Me.Msg.Show(objXmlTr.GetLabelName("StatusMessage", "BB-FUN-D-HELP4"))
            End If
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub lbtnAddArea_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnAddAll.Click
        Try
            Dim lstrSelectedNode As String = ""
            Me.objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            If Me.DDLtech.SelectedValue = "" Then
                Me.Msg.Show(objXmlTr.GetLabelName("StatusMessage", "BB-FUN-D-HELP6"))
                Return
            End If
            If Me.TVareacust.SelectedNode Is Nothing Then
                Me.Msg.Show(objXmlTr.GetLabelName("StatusMessage", "BB-FUN-D-HELP3"))
                Return
            Else
                If Me.TVareacust.SelectedNode.Depth = 0 Then

                    For i As Integer = 0 To Me.TVareacust.SelectedNode.ChildNodes.Count - 1
                        lstrSelectedNode = TVareacust.SelectedNode.Value
                        If Me.TVareacust.SelectedNode.ChildNodes(i).Value.Trim <> "" Then
                            Me.lbxAssigned.Items.Add(New ListItem(Me.TVareacust.SelectedNode.ChildNodes(i).Text, _
                                                  Me.TVareacust.SelectedNode.ChildNodes(i).Value))
                        End If
                        Dim str, strtype, strno As String
                        str = Me.TVareacust.SelectedNode.ChildNodes(i).Value
                        If Not str.Equals("") Then

                            strtype = str.Substring(0, str.IndexOf("/"))
                            strno = str.Substring(str.IndexOf("/") + 1)
                            If Me.apptalloc.AddAppt(strtype, strno, Me.DDLtech.SelectedValue) = -1 Then
                                'Response.Redirect("~/PresentationLayer/error.aspx")
                            End If
                        End If
                    Next
                    BuildTree()
                    'Me.TVareacust.ExpandAll()
                    'TVareacust.SelectedNode. = lstrSelectedNode
                    'TVareacust.SelectedNode.Expand()

                    ShowUnassignedSum()
                Else
                    Me.Msg.Show(objXmlTr.GetLabelName("StatusMessage", "BB-FUN-D-HELP3"))
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub lbtnDelAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnDelAll.Click
        Try
            If Me.lbxAssigned.Items.Count > 0 Then
                For i As Integer = 0 To Me.lbxAssigned.Items.Count - 1
                    Dim str, strtype, strno As String
                    str = Me.lbxAssigned.Items(i).Value
                    strtype = str.Substring(0, str.IndexOf("/"))
                    strno = str.Substring(str.IndexOf("/") + 1)
                    If Me.apptalloc.RemoveAppt(strtype, strno) = -1 Then
                        'Response.Redirect("~/PresentationLayer/error.aspx")
                    End If
                Next
                BuildTree()
                ShowUnassignedSum()
                'Me.TVareacust.ExpandAll()
                Me.lbxAssigned.Items.Clear()
            End If
        Catch ex As Exception
        End Try
    End Sub
    Private Sub SetCtrlStatus(ByVal status As Boolean)
        Me.txtCust.ReadOnly = status
        Me.txtPO.ReadOnly = status
        Me.txtAddr2.ReadOnly = status
        Me.txtAddr1.ReadOnly = status
        Me.ddlArea.Enabled = Not status
        Me.ddlZone.Enabled = Not status
    End Sub

    Protected Sub lbtnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnEdit.Click
        Me.objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")

        If Me.lbxAssigned.SelectedItem IsNot Nothing Then
            Me.txtPO.ReadOnly = False
            Me.ddlArea.Enabled = True
            Me.ddlZone.Enabled = True
            Return
        End If
        If Me.TVareacust.SelectedNode IsNot Nothing Then
            If Me.TVareacust.SelectedNode.Depth = 1 Then
                Me.txtPO.ReadOnly = False
                Me.ddlArea.Enabled = True
                Me.ddlZone.Enabled = True
                Return
            End If
        End If
        Me.Msg.Show(objXmlTr.GetLabelName("StatusMessage", "BB-FUN-D-HELP5"))
    End Sub

    Protected Sub lbtnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnSave.Click
        Me.objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        'Me.MsgBox2.Confirm(objXmlTr.GetLabelName("StatusMessage", "SAVEORNOT"))
        SaveAppointmentAllocation()
    End Sub

    Protected Sub lbtnSaveTech_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnSaveTech.Click
        Me.objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        'Me.MsgBox.Confirm(objXmlTr.GetLabelName("StatusMessage", "SAVEORNOT"))
        SaveTechnician()
    End Sub

    Protected Sub DDLtech_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLtech.SelectedIndexChanged
        Try
            If Me.apptalloc Is Nothing Then
                Return
            End If

            Dim assignedrows As DataRow()
            assignedrows = apptalloc.GetAssignedAppt("FAPT_TCHID ='" + _
                                       Me.DDLtech.SelectedValue + "'")
            If assignedrows Is Nothing Then
                Return
            End If

            Me.lbxAssigned.Items.Clear()
            Dim index As Index = apptalloc.getIndex(0)
            For i As Integer = 0 To assignedrows.Length - 1
                Dim transtype As String = assignedrows(i).ItemArray(index.TransType).ToString()
                Dim transno As String = assignedrows(i).ItemArray(index.TransNO).ToString()

                Dim showtext As String = assignedrows(i).ItemArray(index.CustName).ToString()
                showtext = showtext + "-" + assignedrows(i).ItemArray(index.StartTime).ToString()

                Dim custinfo As PersonInfo
                custinfo = Me.apptalloc.GetCustInfo(transtype, transno)

                'showtext = showtext + "(" + "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZz)"


                Me.lbxAssigned.Items.Add(New ListItem(showtext, transtype + "/" + transno))




                'Me.TVareacust.Nodes(i).ChildNodes.Item(j).ToolTip = custinfo.addr1 + Chr(13) + custinfo.addr2


            Next
            Session.Contents.Remove("rptds")
            txtApptDate.Text = Request.Form("txtApptDate")
        Catch ex As Exception
        End Try
    End Sub
    Protected Sub ShowUnassignedSum()
        If Me.apptalloc Is Nothing Then
            Return
        End If

        Dim rows As DataRow() = Me.apptalloc.GetUnAssignedAppt()
        If rows Is Nothing Then
            Return
        End If
        Dim index As Integer = Me.lblShow.Text.IndexOf(" : ")
        If index > 0 Then
            Me.lblShow.Text = Me.lblShow.Text.Remove(index)
        End If
        Me.lblShow.Text = Me.lblShow.Text + " : " + rows.Length.ToString()
    End Sub

    Sub SaveTechnician()
        Me.objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        If Me.apptalloc Is Nothing Then
            Return ' 
        Else
            If Me.apptalloc.GetModifiedAppt() Is Nothing Then
                Return '
            ElseIf Me.apptalloc.GetModifiedAppt().Length > 0 Then
                Me.apptalloc.IPaddr = Request.UserHostAddress
                Me.apptalloc.SessionID = Now.Year.ToString() + _
                                     Now.Month.ToString() + _
                                     Now.Day.ToString() + _
                                     Now.Hour.ToString() + _
                                     Now.Minute.ToString() + _
                                     Now.Second.ToString() + _
                                     Now.Millisecond.ToString()
                Dim result As Integer = Me.apptalloc.SaveTechAppt()
                If result > 0 Then
                    Me.lbxAssigned.Items.Clear()
                    RetrieveAppointment()
                    Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-D-SUCCESS1")
                ElseIf result = 0 Then
                    Me.lbxAssigned.Items.Clear()
                    RetrieveAppointment()
                    Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-D-WARNING3")
                Else
                    Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-D-ERROR2")
                    'Response.Redirect("~/PresentationLayer/error.aspx")
                End If
            End If
        End If
    End Sub

    Protected Sub MsgBox_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MsgBox.GetMessageBoxResponse
        If e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok Then
            SaveTechnician()
        End If
    End Sub

    Sub SaveAppointmentAllocation()
        If Me.apptalloc Is Nothing Then
            Return
        End If

        Dim Str As String = Nothing, strtype As String = Nothing, strno As String = Nothing
        If Me.TVareacust.SelectedNode IsNot Nothing Then
            If Me.TVareacust.SelectedNode.Depth = 1 Then
                Str = Me.TVareacust.SelectedNode.Value
                strtype = Str.Substring(0, Str.IndexOf("/"))
                strno = Str.Substring(Str.IndexOf("/") + 1)
            End If

        ElseIf Me.lbxAssigned.SelectedItem IsNot Nothing Then
            Str = Me.lbxAssigned.SelectedValue
            strtype = Str.Substring(0, Str.IndexOf("/"))
            strno = Str.Substring(Str.IndexOf("/") + 1)
        End If

        If Str IsNot Nothing Then
            Me.apptalloc.IPaddr = Request.UserHostAddress
            Me.apptalloc.SessionID = Now.Year.ToString() + _
                                     Now.Month.ToString() + _
                                     Now.Day.ToString() + _
                                     Now.Hour.ToString() + _
                                     Now.Minute.ToString() + _
                                     Now.Second.ToString() + _
                                     Now.Millisecond.ToString()
            Dim result As Integer
            result = Me.apptalloc.SaveCustInfo(strtype, strno, Me.ddlArea.SelectedItem.Text, _
                                              Me.txtPO.Text, Me.ddlZone.SelectedItem.Text)
            Me.objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            Select Case result
                Case 1
                    SetCtrlStatus(False)
                    Me.txtCust.Text = ""
                    Me.txtPO.Text = ""
                    Me.txtAddr1.Text = ""
                    Me.txtAddr2.Text = ""
                    Me.ddlArea.Visible = False
                    Me.ddlZone.Visible = False
                    SetCtrlStatus(True)
                    RetrieveAppointment()
                    'Me.TVareacust.ExpandAll()
                    Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-D-SUCCESS2")
                Case 0
                    Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-D-ERROR1")
                    'Response.Redirect("~/PresentationLayer/error.aspx")
                Case -1
                    Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-D-ERROR1")
                    'Response.Redirect("~/PresentationLayer/error.aspx")
                Case -2
                    Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-D-ERROR4")
                    'Response.Redirect("~/PresentationLayer/error.aspx")
                Case -3
                    Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-D-ERROR5")
                    'Response.Redirect("~/PresentationLayer/error.aspx")
                Case -4
                    Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-D-WARNING1")
                    'Response.Redirect("~/PresentationLayer/error.aspx")
            End Select
        End If

    End Sub

    Protected Sub MsgBox2_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MsgBox2.GetMessageBoxResponse
        Try
            If e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok Then
                SaveAppointmentAllocation()
            End If
        Catch ex As Exception
        End Try
    End Sub
    Private Sub ClearPage()
        Me.TVareacust.Nodes.Clear()
        Me.lbxAssigned.Items.Clear()
        Me.txtAddr1.Text = ""
        Me.txtAddr2.Text = ""
        Me.txtCust.Text = ""
        Me.txtPO.Text = ""
        Me.ddlArea.Items.Clear()
        Me.ddlZone.Items.Clear()
        'Me.DDLtech.Items.Clear()
        Dim index As Integer = Me.lblShow.Text.IndexOf(":")
        If index > 0 Then
            Me.lblShow.Text = Me.lblShow.Text.Substring(0, index)
        End If
    End Sub

    Protected Sub lbxAssigned_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbxAssigned.SelectedIndexChanged
        Try
            Dim str, strtype, strno As String
            str = Me.lbxAssigned.SelectedValue
            strtype = str.Substring(0, str.IndexOf("/"))
            strno = str.Substring(str.IndexOf("/") + 1)
            Me.ShowCustInfo(strtype, strno)
            If Me.TVareacust.SelectedNode IsNot Nothing Then
                Me.TVareacust.SelectedNode.Selected = False
            End If
        Catch

        End Try

    End Sub

    Protected Sub ShowCustInfo(ByVal TransTY As String, ByVal TransNO As String)
        Try
            Dim custinfo As PersonInfo
            custinfo = Me.apptalloc.GetCustInfo(TransTY, TransNO)

            SetCtrlStatus(False)
            Me.txtCust.Text = custinfo.ID + "/" + custinfo.Name
            Me.txtPO.Text = custinfo.PO
            Me.txtAddr1.Text = custinfo.addr1
            Me.txtAddr2.Text = custinfo.addr2
            Me.ddlZone.ClearSelection()
            Me.ddlArea.ClearSelection()
            Me.ddlZone.Visible = True
            Me.ddlArea.Visible = True
            Dim areaitem As ListItem = Me.ddlArea.Items.FindByText(custinfo.AreaID)
            Dim zoneitem As ListItem = Me.ddlZone.Items.FindByText(custinfo.ZoneID)
            If areaitem Is Nothing Then
                Me.ddlArea.SelectedIndex = 0
            Else
                areaitem.Selected = True
            End If
            If zoneitem Is Nothing Then
                Me.ddlZone.SelectedIndex = 0
            Else
                zoneitem.Selected = True
            End If
            SetCtrlStatus(True)
        Catch ex As Exception
            SetCtrlStatus(True)
            Return
        End Try
    End Sub

    Protected Sub lbtnAddZone_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnAddZone.Click
        Dim ServCID As String = SerCenIDDrop.Text
        ServCID = Server.UrlEncode(ServCID)
        Dim ApptDate As String = Me.txtApptDate.Text
        ApptDate = Server.UrlEncode(ApptDate)
        Dim ZoneType As String = "NORMAL"
        ZoneType = Server.UrlEncode(ZoneType)
        Dim strTempURL As String = "ServCID=" + ServCID + "&ApptDate=" + ApptDate + "&ZoneType=" + ZoneType
        strTempURL = "~/PresentationLayer/function/ModifyZoneInfomation.aspx?" + strTempURL
        Response.Redirect(strTempURL)
    End Sub

    Protected Sub setPurview()
        Dim arrayPurview As Boolean() = Nothing
        arrayPurview = clsUserAccessGroup.GetUserPurview( _
                               Session.Contents("accessgroup"), "25" _
                        )
        Me.lbtnEdit.Enabled = arrayPurview(1)
        Me.lbtnPrint.Enabled = arrayPurview(3)
        Me.lbtnRetri.Enabled = arrayPurview(2)
        Me.lbtnSave.Enabled = arrayPurview(1)
        Me.lbtnSaveTech.Enabled = arrayPurview(1)
    End Sub

    Protected Sub MyCalendar_CalendarVisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyCalendar.CalendarVisibleChanged
        Me.DDLtech.Visible = Not Me.MyCalendar.CalendarVisible
        Me.lbxAssigned.Visible = Not Me.MyCalendar.CalendarVisible
    End Sub

    Protected Sub MyCalendar_SelectedDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyCalendar.SelectedDateChanged
        'Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Me.txtApptDate.Text = Me.MyCalendar.SelectedDate
    End Sub

    Protected Sub lbtnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnPrint.Click
        If DDLtech.SelectedValue <> "" Then
            Session("TechID") = Me.DDLtech.SelectedValue
        End If
    End Sub

#Region "Ajax"
    <AjaxPro.AjaxMethod()> _
    Public Function DisplayCustomerAddress(ByVal TransTypeNo As String) As String
        Dim lstrTransType As String = ""
        Dim lstrTransNo As String = ""
        Dim lstrAddress As String = ""
        Dim objApptAlloc As New clsApptAlloc

        If TransTypeNo <> "" Then
            lstrTransType = TransTypeNo.Substring(0, TransTypeNo.IndexOf("/"))
            lstrTransNo = TransTypeNo.Substring(TransTypeNo.IndexOf("/") + 1)

            Dim custinfo As PersonInfo
            custinfo = objApptAlloc.GetCustInfo(lstrTransType, lstrTransNo)
            lstrAddress = custinfo.addr1 + Chr(13) + custinfo.addr2
        End If

        DisplayCustomerAddress = lstrAddress
    End Function

#End Region

    Protected Sub techorsub_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles techorsub.SelectedIndexChanged

        If Session.Contents("apptalloc") Is Nothing Then
            Me.Msg.Show("Have to click RETRIEVE first")
            Return
        End If

        Dim strDate As String = Request.Form("txtApptDate")
        Dim result As Integer = apptalloc.FillDataSet(strDate, SerCenIDDrop.SelectedItem.Value, _
                                                         Me.txtFromZone.Text, Me.txtToZone.Text, DDLtech.Text, Me.txtAppointmentNo.Text, Me.txtCustomerID.Text, Me.techorsub.SelectedValue) 'SerCenIDDrop.Text.Trim(), _
        Session.Contents("apptalloc") = apptalloc
        BindDDL()
    End Sub
End Class
