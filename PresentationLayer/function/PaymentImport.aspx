<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PaymentImport.aspx.vb" Inherits="PresentationLayer_function_PaymentImport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>ESS Smart Payment Import</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:FileUpload ID="FileUpload1" runat="server" />
        <asp:LinkButton ID="btnUpload" runat="server">Upload File</asp:LinkButton>
        <br />
        <br />
        <asp:GridView ID="GridView1" runat="server">
        </asp:GridView>
        <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="Label" Visible="False"></asp:Label><br />
        <asp:LinkButton ID="btnSubmit" runat="server">Submit and Process</asp:LinkButton></div>
        <br />
    </form>
</body>
</html>
