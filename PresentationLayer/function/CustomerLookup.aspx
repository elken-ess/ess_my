<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CustomerLookup.aspx.vb" Inherits="PresentationLayer_function_CustomerLookup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Search Customer</title>
</head>
<body>
    <form id="form1" runat="server"> 
    <div>
        <div>
            <table id="TABLE2" border="0" width ="100%">
                <tr>
                    <td style="width: 100%; height: 13px">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td background="../graph/title_bg.gif" width="1%">
                                    <img height="24" src="../graph/title1.gif" width="5" /></td>
                                <td background="../graph/title_bg.gif" class="style2" width="98%">
                                    <asp:Label ID="lblTitle" runat="server"></asp:Label></td>
                                <td align="right" background="../graph/title_bg.gif" width="1%">
                                    <img height="24" src="../graph/title_2.gif" width="5" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="width: 90%; height: 12px">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td background="../graph/title_bg.gif" class="style2" style="height: 1px" width="98%">
                                    <font color="red">
                                        <asp:Label ID="lblError" runat="server" Visible="False"></asp:Label></font></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table id="countrytab" border="0" width ="100%">
                <tr>
                    <td style="height: 85%" width="90%">
                        <table id="country" bgcolor="#b7e6e6" border="0" cellpadding="2" cellspacing="1" width="100%">
                            <tr bgcolor="#ffffff">
                                <td align="right" style="height: 28px" width="15%">
                                    <asp:Label ID="lblCustomerID" runat="server" Text="Customer ID"></asp:Label></td>
                                <td align="left" style="width: 27%; height: 28px">
                                    <asp:TextBox ID="txtCustomerID" runat="server" CssClass="textborder" Font-Size="Small"
                                        MaxLength="20"></asp:TextBox></td>
                                <td align="right" style="height: 28px" width="15%">
                                    <asp:Label ID="lblArea" runat="server" Text="Area"></asp:Label></td>
                                <td align="left" style="width: 35%; height: 28px">
                                    <asp:DropDownList ID="cboArea" runat="server" CssClass="textborder" Width="90%">
                                    </asp:DropDownList></td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" width="15%">
                                    <asp:Label ID="lblCustomerName" runat="server" Text="Customer Name"></asp:Label></td>
                                <td align="left" style="width: 27%">
                                    <asp:TextBox ID="txtCustomerName" runat="server" CssClass="textborder" Font-Size="Small"
                                        MaxLength="50"></asp:TextBox></td>
                                <td align="right" width="15%">
                                    <asp:Label ID="txtContact" runat="server" Text="Contact"></asp:Label></td>
                                <td align="left" style="width: 35%">
                                    <asp:TextBox ID="lblContact" runat="server" CssClass="textborder" Font-Size="Small"
                                        MaxLength="30"></asp:TextBox></td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="left" colspan="4">
                                    <asp:LinkButton ID="lnkSearch" runat="server">search</asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                        <hr />
                        <asp:GridView ID="grdCustomer" runat="server" AllowPaging="True" AllowSorting="True"
                            Font-Size="Smaller" Width="100%">
                            <Columns>
                                <asp:CommandField CancelText="" DeleteText="" EditText="Select" HeaderImageUrl="~/PresentationLayer/graph/edit.gif"
                                    InsertText="" NewText="" SelectText="" ShowEditButton="True" UpdateText="" />
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td style="height: 85%" width="90%">
                        <asp:LinkButton ID="lnkback" runat="server">Back</asp:LinkButton></td>
                </tr>
            </table>
            &nbsp;<asp:Label ID="Label1" runat="server" ForeColor="Red" Text="Label" Visible="False"
                Width="100%"></asp:Label><br />
            &nbsp;&nbsp;
            <br />
        </div>
    
    </div>
    </form>
</body>
</html>
