Imports BusinessEntity
Imports System
Imports System.Data

Partial Class PresentationLayer_Function_defineMasterRS
    Inherits System.Web.UI.Page

    Dim objXML As New clsXml
    Private ZoneEntity As clsZoneRS


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try
        If (Not Page.IsPostBack) Then
            DisplayPage()
        End If

        Dim accessgroup As String = Session("accessgroup").ToString
        Dim purviewArray As Array = New Boolean() {False, False, False, False}
        purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "28")

        Me.btnSave.Enabled = purviewArray(1)

        errlab.Visible = False
    End Sub
    Protected Sub DisplayPage()
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        'display label message

        objXML.XmlFile = Configuration.ConfigurationManager.AppSettings("XmlFilePath")
        titleLab.Text = objXML.GetLabelName("EngLabelMsg", "BB-ZONGINGINF-0018")
        ZoneIDLab.Text = objXML.GetLabelName("EngLabelMsg", "BB-ZONGINGINF-0019")
        ZonTYLab.Text = objXML.GetLabelName("EngLabelMsg", "BB-ZONGINGINF-0020")
        btnCreZone.Text = objXML.GetLabelName("EngLabelMsg", "BB-ZONGINGINF-0024")
        btnDelZone.Text = objXML.GetLabelName("EngLabelMsg", "BB-ZONGINGINF-0023")
        btnAddArea.Text = objXML.GetLabelName("EngLabelMsg", "BB-ZONGINGINF-0021")
        btnDelArea.Text = objXML.GetLabelName("EngLabelMsg", "BB-ZONGINGINF-0022")
        SerCentIDLab.Text = objXML.GetLabelName("EngLabelMsg", "BB-ZONGINGINF-0001")
        ApptDateLab.Text = objXML.GetLabelName("EngLabelMsg", "BB-ZONGINGINF-0002")
        'btnSaveMas.Text = objXML.GetLabelName("EngLabelMsg", "BB-ZONGINGINF-0013")
        btnSave.Text = objXML.GetLabelName("EngLabelMsg", "BB-Save")
        cancel.Text = objXML.GetLabelName("EngLabelMsg", "BB-Cancel")
        cancel.NavigateUrl = Request.UrlReferrer.ToString()
        lblNoteUP.Text = objXML.GetLabelName("EngLabelMsg", "BB-RFV")
        lblNoteDown.Text = objXML.GetLabelName("EngLabelMsg", "BB-RFV")

        Me.SerCentBox.Text = Request.Params("ServCID")
        Me.ApptDateBox.Text = Request.Params("ApptDate")
        objXML.XmlFile = System.Configuration.ConfigurationManager.AppSettings("StatMsg")
        Me.ZonTYBox.Text = objXML.GetLabelName("StatusMessage", Request.Params("ZoneType").ToString())
        'Me.btnSaveMas.Visible = False
        DisplayDailyZoneInfo()
    End Sub
    Protected Sub DisplayDailyZoneInfo()
        ZoneEntity = New clsZoneRS()
        ZoneEntity.ServiceCenterID = SerCentBox.Text
        ZoneEntity.ApptDate = ApptDateBox.Text
        ZoneEntity.ZoneType = Request.Params("ZoneType")

        Dim ds As DataSet = ZoneEntity.GetDailyZoneInfo()
        If ds Is Nothing Then
            Response.Redirect("~/PresentationLayer/error.aspx")
        End If


        Dim i, j As Integer
        Dim areaId As String
        Dim areaName As String
        Dim ZoneId As String
        'dim ZoneIdTemp as String 
        'AreaInfo ListBox
        AreaList.Items.Clear()
        For i = 0 To ds.Tables("AreaInfo").Rows.Count - 1
            areaId = ds.Tables("AreaInfo").Rows(i).Item("MARE_AREID")
            areaName = ds.Tables("AreaInfo").Rows(i).Item("MARE_ENAME")
            AreaList.Items.Add(New ListItem(areaId + "-" + areaName, areaId))
        Next

        'edit June 27
        'ZoneInfo Tree
        ZonAreaInfoTree.Nodes.Clear()
        'ZoneId = ""
        'ZoneIdTemp = ""
        'j = 0
        'For i = 0 To ds.Tables("DailyZoneInfo").Rows.Count - 1
        '    ZoneId = ds.Tables("DailyZoneInfo").Rows(i).Item("FZN2_ZONID")
        '    If ZoneId <> ZoneIdTemp Then
        '        ZonAreaInfoTree.Nodes.Add(New TreeNode(ZoneId, ZoneId))
        '        ZoneIdTemp = ZoneId
        '        j = j + 1
        '    End If
        '    areaId = ds.Tables("DailyZoneInfo").Rows(i).Item("FZN2_AREID").ToString()
        '    areaName = ds.Tables("DailyZoneInfo").Rows(i).Item("MARE_ENAME")
        '    ZonAreaInfoTree.Nodes(j - 1).ChildNodes.Add(New TreeNode(areaId + "-" + areaName, areaId))
        'Next
        j = 0
        For i = 0 To ds.Tables("ZoneInfo").Rows.Count - 1
            ZoneId = ds.Tables("ZoneInfo").Rows(i).Item("FZN1_ZONID")
            ZonAreaInfoTree.Nodes.Add(New TreeNode(ZoneId, ZoneId))
            While j < ds.Tables("DailyZoneInfo").Rows.Count
                If ds.Tables("DailyZoneInfo").Rows(j).Item("FZN2_ZONID") <> ZoneId Then
                    Exit While
                End If
                areaId = ds.Tables("DailyZoneInfo").Rows(j).Item("FZN2_AREID").ToString()
                areaName = ds.Tables("DailyZoneInfo").Rows(j).Item("MARE_ENAME")
                ZonAreaInfoTree.Nodes(i).ChildNodes.Add(New TreeNode(areaId + "-" + areaName, areaId))
                j = j + 1
            End While
        Next
    End Sub
    Protected Sub BtnCreZone_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCreZone.Click
        objXML.XmlFile = System.Configuration.ConfigurationManager.AppSettings("StatMsg")
        Dim infoMsg As String
        If ZonIDBox.Text = "" Then
            infoMsg = objXML.GetLabelName("StatusMessage", "BB-ZONE-NEWZONEIDCHECK")
            InfoMsgBox.Alert(infoMsg)
        Else
            Dim ZoneNode As String = ZonIDBox.Text.Trim()
            If AddZone(ZonIDBox.Text) Then
                Me.errlab.Text = objXML.GetLabelName("StatusMessage", "BB-AddZone-Suc")
            Else
                Me.errlab.Text = objXML.GetLabelName("StatusMessage", "BB-AddZone-Fail")
            End If
            Me.errlab.Visible = True
        End If
        ZonIDBox.Text = ""
    End Sub
    Private Function AddZone(ByVal strZone As String) As Boolean
        Dim bExist As Boolean = False
        Dim i As Integer

        For i = 0 To ZonAreaInfoTree.Nodes.Count - 1
            If (ZonAreaInfoTree.Nodes(i).Value.ToUpper() = strZone.ToUpper()) Then
                bExist = True
                Exit For
            End If
        Next

        If Not bExist Then
            ZonAreaInfoTree.Nodes.Add(New TreeNode(strZone, strZone))
            Return True
        End If
        Return False
    End Function
    Protected Sub BtnDelZone_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelZone.Click
        objXML.XmlFile = System.Configuration.ConfigurationManager.AppSettings("StatMsg")
        If ZonAreaInfoTree.SelectedNode Is Nothing Then
            Me.errlab.Text = objXML.GetLabelName("StatusMessage", "BB-SelZone")
            errlab.Visible = True
            Return
        End If
        If ZonAreaInfoTree.SelectedNode.Depth = 0 Then
            ZonAreaInfoTree.Nodes.Remove(ZonAreaInfoTree.SelectedNode)
            Me.errlab.Text = objXML.GetLabelName("StatusMessage", "BB-DelZone-Suc")
        Else
            Me.errlab.Text = objXML.GetLabelName("StatusMessage", "BB-DelZone-Fail")
        End If
        errlab.Visible = True
    End Sub

    Protected Sub BtnAddArea_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddArea.Click
        objXML.XmlFile = System.Configuration.ConfigurationManager.AppSettings("StatMsg")
        If ZonAreaInfoTree.SelectedNode Is Nothing Then
            Me.errlab.Text = objXML.GetLabelName("StatusMessage", "BB-SelZone")
            errlab.Visible = True
            Return
        End If
        If AreaList.SelectedIndex < 0 Then
            Me.errlab.Text = objXML.GetLabelName("StatusMessage", "BB-SelArea")
            errlab.Visible = True
            Return
        End If

        Dim iParent As Integer

        If ZonAreaInfoTree.SelectedNode.Depth = 0 Then
            iParent = ZonAreaInfoTree.Nodes.IndexOf(ZonAreaInfoTree.SelectedNode)
        ElseIf ZonAreaInfoTree.SelectedNode.Depth = 1 Then
            iParent = ZonAreaInfoTree.Nodes.IndexOf(ZonAreaInfoTree.SelectedNode.Parent)
        End If
        If AddArea(AreaList.SelectedItem, iParent) Then
            Me.errlab.Text = objXML.GetLabelName("StatusMessage", "BB-AddArea-Suc")
        Else
            Me.errlab.Text = objXML.GetLabelName("StatusMessage", "BB-AddArea-Fail")
        End If
        Me.errlab.Visible = True
    End Sub
    Private Function AddArea(ByVal Area As ListItem, ByVal iParent As Integer) As Boolean
        Dim bExist As Boolean = False
        Dim i As Integer

        For i = 0 To ZonAreaInfoTree.Nodes(iParent).ChildNodes.Count - 1
            If (ZonAreaInfoTree.Nodes(iParent).ChildNodes(i).Value.ToUpper() = Area.Value.ToUpper()) Then
                bExist = True
                Exit For
            End If
        Next

        If Not bExist Then
            ZonAreaInfoTree.Nodes(iParent).ChildNodes.Add(New TreeNode(Area.Text, Area.Value))
            Return True
        End If
        Return False
    End Function
    Protected Sub BtnDelArea_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelArea.Click
        objXML.XmlFile = System.Configuration.ConfigurationManager.AppSettings("StatMsg")
        If ZonAreaInfoTree.SelectedNode Is Nothing Then
            Me.errlab.Text = objXML.GetLabelName("StatusMessage", "BB-SelArea")
            Me.errlab.Visible = True
            Return
        End If
        If ZonAreaInfoTree.SelectedNode.Depth = 0 Then
            Me.errlab.Text = objXML.GetLabelName("StatusMessage", "BB-DelArea-Fail")
        Else
            Dim iParent As Integer = ZonAreaInfoTree.Nodes.IndexOf(ZonAreaInfoTree.SelectedNode.Parent)
            ZonAreaInfoTree.Nodes(iParent).ChildNodes.Remove(ZonAreaInfoTree.SelectedNode)
            Me.errlab.Text = objXML.GetLabelName("StatusMessage", "BB-DelArea-Suc")
        End If
        Me.errlab.Visible = True
    End Sub

    Protected Sub BtnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
        Dim msgtitle As String
        Dim msgInfo As String = objXML.GetLabelName("StatusMessage", "BB-ZONGINGINF-QusSave3")
        msgInfo = objXML.GetLabelName("StatusMessage", "BB-ZONGINGINF-QusSave3")
        msgtitle = objXML.GetLabelName("StatusMessage", "BB-ZONGINGINF-QusSave3")
        saveMsgBox.Confirm(msgInfo, "save")
    End Sub
    Protected Sub saveMsgBox_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles saveMsgBox.GetMessageBoxResponse
        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
            ZoneEntity = New clsZoneRS()
            ZoneEntity.ServiceCenterID = SerCentBox.Text
            ZoneEntity.ApptDate = ApptDateBox.Text
            ZoneEntity.ZoneType = Request.Params("ZoneType")
            ZoneEntity.UserID = Session("userID")
            ZoneEntity.IpAddress = Request.UserHostAddress.ToString()
            ZoneEntity.UserServCenterID = Session("login_svcID")
            'edit june 26
            Dim EmptyZones As New DataTable 'area id ???Zone
            EmptyZones.Columns.Add("ZoneID", System.Type.GetType("System.String"))

            Dim ZoneInfo As New DataTable
            ZoneInfo.Columns.Add("ZoneID", System.Type.GetType("System.String"))
            ZoneInfo.Columns.Add("AreaID", System.Type.GetType("System.String"))
            Dim i, j As Integer
            For i = 0 To ZonAreaInfoTree.Nodes.Count - 1
                If ZonAreaInfoTree.Nodes(i).ChildNodes.Count = 0 Then
                    Dim row As DataRow
                    row = EmptyZones.NewRow()
                    row.Item("ZoneID") = ZonAreaInfoTree.Nodes(i).Value
                    EmptyZones.Rows.Add(row)
                End If
                For j = 0 To ZonAreaInfoTree.Nodes(i).ChildNodes.Count - 1
                    Dim row As DataRow
                    row = ZoneInfo.NewRow()
                    row.Item("ZoneID") = ZonAreaInfoTree.Nodes(i).Value
                    row.Item("AreaID") = ZonAreaInfoTree.Nodes(i).ChildNodes(j).Value
                    ZoneInfo.Rows.Add(row)
                Next
            Next
            Dim bsuccess As Boolean
            If (e.Data = "save") Then
                bsuccess = ZoneEntity.SaveZoneInfo(EmptyZones, ZoneInfo, "0")
            ElseIf (e.Data = "SaveMaster") Then
                bsuccess = ZoneEntity.SaveZoneInfo(EmptyZones, ZoneInfo, "1")
            End If
            objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")

            If bsuccess Then
                Me.errlab.Text = objXML.GetLabelName("StatusMessage", "BB-ZONE-SAVESUCCESS")
                'If Request.Params("ZoneType") <> "MASTER" Then
                '    Me.btnSaveMas.Visible = True
                'End If
            Else
                Me.errlab.Text = objXML.GetLabelName("StatusMessage", "BB-ZONE-SAVEFAIL")
            End If
            Me.errlab.Visible = True
        End If
    End Sub
    'Protected Sub btnSaveMas_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveMas.Click
    '    objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
    '    Dim msgtitle As String
    '    Dim msgInfo As String = objXML.GetLabelName("StatusMessage", "BB-ZONGINGINF-QusSave3")
    '    msgInfo = objXML.GetLabelName("StatusMessage", "BB-ZONGINGINF-QusSave2")
    '    msgtitle = objXML.GetLabelName("StatusMessage", "BB-ZONGINGINF-QusSave2")
    '    saveMsgBox.Confirm(msgInfo, "SaveMaster")
    'End Sub
End Class
