<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AppointmentLookUp.aspx.vb" Inherits="PresentationLayer_function_AppointmentLookUp" %>

<%@ Register Assembly="JCalendar" Namespace="JCalendar" TagPrefix="cc2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Appointment Search</title>
    <link href="../css/style.css" type="text/css" rel="stylesheet">
</head>
<body style="font-family: Verdana; text-align: left;">
    <form id="form1" runat="server">
        <div style="vertical-align: top" >
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                            <tr>
                                <td width="1%" background="../graph/title_bg.gif">
                                    <img height="24" src="../graph/title1.gif" width="5"></td>
                                <td class="style2" width="98%" background="../graph/title_bg.gif" style="width: 80%">
                                    <asp:Label ID="titleLab" runat="server" Text="Appointment Lookup"></asp:Label></td>
                                <td align="right" width="1%" background="../graph/title_bg.gif">
                                    <img height="24" src="../graph/title_2.gif" width="5"></td>
                            </tr>
                        </table>
        
        <table bgcolor="#b7e6e6" width = 100%> 
            <tr bgcolor="#ffffff" valign=top align=left  >    
                <td width="25%">
                    &nbsp;<asp:Label ID="lblAppointmentNo" runat="server" Width="60%">Appointment No</asp:Label></td>
                <td style="width: 25%;" valign=top>
                    <asp:TextBox ID="txtAppointmentPrefix" runat="server" Width="10%"  MaxLength =2></asp:TextBox>&nbsp;
                    <asp:TextBox ID="txtAppointmentNo" runat="server" Width="70%" TabIndex="1" ></asp:TextBox></td>
                <td valign=top style="width: 25%; height: 24px;">
                    <asp:Label ID="lblCustomerID" runat="server" Width="85px">Customer ID</asp:Label></td>
                <td valign=top style="width: 25%; height: 24px;">
                    <asp:TextBox ID="txtCustomerPrefix" runat="server" Width="10%" TabIndex="2"></asp:TextBox>&nbsp;
                    <asp:TextBox ID="txtCustomerID" runat="server" Width="70%" TabIndex="3"></asp:TextBox></td>
                
            </tr> 
            
            <tr bgcolor="#ffffff">
                <td valign="top" width="25%">
                    <asp:Label ID="lblSericeCenter" runat="server" Width="90%">Service Center</asp:Label></td>
                <td style="width: 25%;" valign="top">
                    <asp:DropDownList ID="cboServiceCenter" runat="server" Width="90%" TabIndex="5" AutoPostBack="True">
                    </asp:DropDownList></td>
                <td style="width: 25%; height: 19px;" valign="top">
                <asp:Label ID="lblTechnician" runat="server">Technician</asp:Label></td>
                <td style="width: 25%; height: 19px;" valign="top">
                <asp:DropDownList ID="cboTechnician" runat="server" TabIndex="6" Width ="90%">
                </asp:DropDownList></td>
                
            </tr>
            <tr bgcolor="#ffffff">
                <td style="width: 25%; height: 14px">
                    <asp:Label ID="lblStatus" runat="server">Status</asp:Label></td>
                <td style="width: 25%; height: 14px"><asp:DropDownList ID="cboStatus" runat="server" Width="90%" TabIndex="4">
                </asp:DropDownList></td>
                <td valign=top style="width: 25%; height: 24px;">
                    <asp:Label ID="lblServiceType" runat="server">Service Type</asp:Label></td>
                <td valign=top style="width: 25%; height: 24px;">
                    <asp:DropDownList ID="cboServiceType" runat="server" Width="90%" TabIndex="4">
                </asp:DropDownList></td>
                
                
            </tr>
            
            <tr bgcolor="#ffffff">
            <td style="width: 25%; height: 19px;" valign="top">
                    <asp:Label ID="lblAppointmentDate" runat="server" Height="34px">Appointment Date</asp:Label></td>
                <td style="width: 25%; height: 19px;">
                    <asp:TextBox ID="txtDate" runat="server" Width="70%" ReadOnly="false" MaxLength =10></asp:TextBox>&nbsp;
                    <cc2:jcalendar id="JCalendar1" runat="server" controltoassign="JDATEBox" imgurl="~/PresentationLayer/graph/calendar.gif"></cc2:jcalendar>
                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                        TabIndex="7" />&nbsp;<asp:Calendar ID="Calendar1" runat="server" BackColor="White" BorderColor="#3366CC"
                        BorderWidth="1px" CellPadding="1" DayNameFormat="Shortest" Font-Names="Verdana"
                        Font-Size="8pt" ForeColor="#003399" Height="86px" ShowGridLines="True" Visible="False"
                        Width="61px" Enabled="False">
                        <SelectedDayStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                        <TodayDayStyle BackColor="#99CCCC" ForeColor="White" />
                        <SelectorStyle BackColor="#99CCCC" ForeColor="#336666" />
                        <WeekendDayStyle BackColor="#CCCCFF" />
                        <OtherMonthDayStyle ForeColor="#999999" />
                        <NextPrevStyle Font-Size="8pt" ForeColor="#CCCCFF" />
                        <DayHeaderStyle BackColor="#99CCCC" ForeColor="#336666" Height="1px" />
                        <TitleStyle BackColor="#003399" BorderColor="#3366CC" BorderWidth="1px" Font-Bold="True"
                            Font-Size="10pt" ForeColor="#CCCCFF" Height="25px" />
                    </asp:Calendar>
                </td>
                
                    <td style="width: 25%; height: 14px">
                    <td style="width: 25%; height: 14px">
                    <asp:LinkButton ID="lnkSearch" runat="server" TabIndex="8">Search</asp:LinkButton></td>
            </tr>
            <tr bgcolor="#ffffff">
                <td style="height: 26px;" colspan="4">
                    &nbsp;<asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label></td>
            </tr>
            </table> 
            <table bgcolor="#b7e6e6" width = 150%> 
            <tr bgcolor="#ffffff">
                <td colspan="4" style="height: 14px">
                    <asp:GridView ID="grdAppointment" runat="server" TabIndex="8" Width ="100%">
                        <Columns>
                            <asp:CommandField CancelText="" DeleteText="" EditText="Select" HeaderImageUrl="~/PresentationLayer/graph/edit.gif"
                                InsertText="" NewText="" SelectText="" ShowCancelButton="False" ShowEditButton="True"
                                UpdateText="" />
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td colspan="4" rowspan="2">
                    <asp:LinkButton ID="lnkBack" runat="server" TabIndex="8">Back</asp:LinkButton></td>
            </tr>
            <tr bgcolor="#ffffff">
            </tr>
        </table>
        
    </form>
</body>
</html>
