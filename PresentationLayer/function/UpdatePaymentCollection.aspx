<%@ Page Language="VB" AutoEventWireup="false" CodeFile="UpdatePaymentCollection.aspx.vb" Inherits="PresentationLayer_function_UpdatePaymentCollection" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Update Payment Collection</title>
    <LINK href="../css/style.css" type="text/css" rel="stylesheet">
    <script language="JavaScript" src="../js/common.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table bgcolor="#b7e6e6" border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td background="../graph/title_bg.gif" width="1%">
                    <img height="24" src="../graph/title1.gif" width="5" /></td>
                <td background="../graph/title_bg.gif" class="style2" width="98%">
                    <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
                <td align="right" background="../graph/title_bg.gif" width="1%">
                    <img height="24" src="../graph/title_2.gif" width="5" /></td>
            </tr>
        </table>
    
    </div>
        <table bgColor="#b7e6e6" width=100%>
            <tr bgColor="#ffffff">
                <td style="width: 10% ;height: 26px;">
                    <asp:Label ID="CNlab" runat="server" Text="Label" Width="100%"></asp:Label></td>
                <td style="width: 15%; height: 26px;">
                    <asp:TextBox ID="CNText" runat="server" Width="90%"></asp:TextBox></td>
                <td style="width: 10%; height: 26px;">
        <asp:Label ID="solab" runat="server" Text="Label" Width="100%"></asp:Label></td>
                <td style="width: 15%; height: 26px;">
                    <asp:TextBox ID="soText" runat="server" Width="90%"></asp:TextBox></td>
                <td style="width: 10%; height: 26px;">
                    <asp:Label ID="ctryStat" runat="server" Text="Label"></asp:Label></td>
                <td style="width: 15%; height: 26px;">
                    <asp:DropDownList ID="ctryStatDrop" runat="server" Width="100px">
                    </asp:DropDownList></td>
            </tr>
            
            <tr bgcolor="#ffffff">
                <td style="width: 10%">
        <asp:Label ID="phnolab" runat="server" Text="Label" Width="100%"></asp:Label></td>
                <td style="width: 15%">
                    <asp:TextBox ID="phnoText" runat="server" Width="90%"></asp:TextBox></td>
                <td style="width: 10%">
                    <asp:Label ID="snlab" runat="server" Text="Label" Width="100%"></asp:Label></td>
                <td style="width: 15%">
                    <asp:TextBox ID="snText" runat="server" Width="90%"></asp:TextBox></td>
                <td style="width: 10%">
                    <asp:Label ID="lblServiceBillNo" runat="server" Text="Service Bill No"></asp:Label></td>
                <td style="width: 15%">
                   <asp:TextBox ID="txtServiceBillNo" runat="server" Width="90%" MaxLength =20></asp:TextBox>
                
            </tr>
            
            <tr bgColor="#ffffff">
                <td  style="width: 10%">
                    <asp:Label ID="lblCustomerID" runat="server" Text="Customer ID" Width="60%"></asp:Label></td>
                <td  style="width: 15%">
                    <asp:TextBox ID="txtCustomerID" runat="server" Width="90%"  MaxLength =20></asp:TextBox></td>
                <td  style="width: 10%">
                    <asp:Label ID="lblAppointmentNo" runat="server" Text="Appointment No" Width="90%"></asp:Label></td>
                <td  style="width: 15%">
                    <asp:TextBox ID="txtAppointmentPrefix" runat="server" Width="20%" MaxLength =2></asp:TextBox>
                    <asp:TextBox ID="txtAppointmentNo" runat="server" Width="60%"  MaxLength =20></asp:TextBox></td>
                <td style="width: 100px">
                    <asp:Label ID="lblSVC" runat="server" Text="Service Center ID" Width="114px"></asp:Label></td>
                <td style="width: 100px">
                    <asp:DropDownList ID="SVClist" runat="server" AutoPostBack="true" OnSelectedIndexChanged="SVCList_SelectedIndexChanged" Width="187px">
                    </asp:DropDownList></td>
            </tr>
            <tr bgColor="#ffffff">
                <td style="width: 100px">
        <asp:Label ID="lblStartDate" runat="server" Text="Service Order Date from" Width="100%"></asp:Label>
        </td>
                <td style="width: 100px">
                  <asp:TextBox ID="txtStartDate" runat="server" Width="70px" MaxLength =10></asp:TextBox>
                    <asp:HyperLink ID="HypCal1" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                        ToolTip="Choose a Date">Choose a Date</asp:HyperLink>
                        </td>
                <td style="width: 100px">
                    <asp:Label ID="lblEndDate" runat="server" Text="Service Order Date to" Width="100%"></asp:Label></td>
                <td style="width: 100px">
                    <asp:TextBox ID="txtEndDate" runat="server" Width="70px" MaxLength =10></asp:TextBox>
                    <asp:HyperLink ID="HypCal2" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                        ToolTip="Choose a Date">Choose a Date</asp:HyperLink>
                        </td>
                <td style="width: 100px">
                    <asp:Label ID="technicianLab" runat="server" Text="Technician"></asp:Label></td>
                <td style="width: 100px">
                    <asp:DropDownList ID="technicianList" runat="server" AppendDataBoundItems="false"
                        Width="196%">
                        <asp:ListItem Selected="True"></asp:ListItem>
                    </asp:DropDownList></td>
            </tr>
            <tr bgcolor="#ffffff">
                <td style="width: 100px">
                    <asp:Label ID="lblServiceBillType" runat="server" Text="Service Bill Type" Width="104px"></asp:Label></td>
                <td style="width: 100px">
                    <asp:DropDownList ID="cboServiceBillType" runat="server" Width="145px">
                        <asp:ListItem Value="WITH" Selected="True">With Appointment</asp:ListItem>
                        <asp:ListItem Value="WITO">Without Appointment</asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 100px">
                </td>
                <td style="width: 100px">
                </td>
                <td style="width: 100px">
                    <asp:LinkButton  ID="searchbtn" runat="server">Search</asp:LinkButton></td>
                <td style="width: 100px">
                    </td>
            </tr>
            
        </table>
        &nbsp;
        <asp:GridView ID="partView" runat="server" AllowPaging="True" AllowSorting="True" Width="100%">
        </asp:GridView>
        <asp:Label ID="lblNoRecord" runat="server" ForeColor="Red" Text="Label"></asp:Label>
    </form>
</body>
</html>
