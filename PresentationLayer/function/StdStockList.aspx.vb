﻿Imports BusinessEntity
Imports system.Data
Imports System.IO

Partial Class PresentationLayer_function_StdStockList
    Inherits System.Web.UI.Page
    Private objXmlTr As New clsXml()
    Private stdstocklist As clsStdStockList
    Dim dsitem As New DataSet

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.MaintainScrollPositionOnPostBack = True
        If Not IsPostBack Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try


            BindPage(sender, e)
            txtDate.Text = Format(Now(), "dd/MM/yyyy")
        Else
            Me.stdstocklist = Session.Contents("stdstocklist")
            Me.lblMessage.Text = ""
        End If

        'added by deyb 03/07/2006
        If ddlTech.Text <> "" And ddlSVC.Text <> "" Then
            lbtnViewExiList.Enabled = True
        Else
            lbtn1stdlist.Enabled = False : lbtnViewExiList.Enabled = False
        End If

        If mygridview.Visible = True Then
            lbtnViewCurList.Enabled = True
        Else
            lbtnViewCurList.Enabled = False
        End If

        If ddlTransTY.SelectedValue = "STK-ADJ" Or ddlTransTY.SelectedIndex = 0 Then
            Session("cType") = "ADJ"
        ElseIf ddlTransTY.SelectedValue = "STK-STD" Or ddlTransTY.SelectedIndex = 1 Then
            Session("cType") = "STD"
        End If
        HypCal.NavigateUrl = "javascript:DoCal(document.form1.txtDate);"
        '------------------------

        'populate list based on search if any
        Try
            PopulateStdList()
        Catch
        End Try
    End Sub

    Sub RequestFromValue()
        If Request.Form("txtDate") <> "" Then
            txtDate.Text = Request.Form("txtDate")
        End If
    End Sub
    Protected Sub BindPage(ByVal sender As Object, ByVal e As System.EventArgs)
        BindLabel()
        BindDropDownList(sender, e)
    End Sub

    Protected Sub BindLabel()
        objXmlTr.XmlFile = ConfigurationManager.AppSettings("XmlFilePath")
        Me.lblDate.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_F_DATE")
        Me.lblTransTY.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_F_TRANTY")
        Me.lblTech.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_F_TECH")
        Me.lblSVC.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_F_SVC")
        Me.lbllineTotal.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_F_LINETOTAL")
        Me.lblNewStockList.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_F_NEWSTOCK")
        Me.lblPartCode.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_F_PARTCODE")
        Me.lblQuantity.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_F_QUANTITY")
        Me.lblUpStkList.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_F_UPSTOCKLIST")
        Me.lbtn1stdlist.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_F_1STDLIST")
        Me.lbtnAdd.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Add")
        Me.lbtnAddStdlist.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_F_ADDLIST")
        Me.lbtnDelete.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_F_DEL")
        Me.lbtnGenPackList.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_F_GENPACK")
        Me.lbtnSave.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Save")
        Me.lbtnViewCurList.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_F_VIEWCURLIST")
        Me.lbtnViewExiList.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_F_VIEWEXIST")
        Me.titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_F_TITLE")
        Me.LblPriceID.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPRID-0001")
        lblNoteUP.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
        lblNoteDown.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")

        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Me.rfvTrTy.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-F-Help1")
        Me.REValidator.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-F-Help2")
        Me.lbtnViewCurList.PostBackUrl = "~/PresentationLayer/function/ViewStdStockList.aspx?id=2"
        'Me.lbtnViewExiList.PostBackUrl = "~/PresentationLayer/function/ViewStdStockList.aspx?id=1"

        setPurview()
        lbtnSave.Enabled = False
        lbtnGenPackList.Enabled = False
    End Sub

    Protected Sub BindDropDownList(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim xComClass As New clsCommonClass
        If Session.Contents("stdstocklist") Is Nothing Then
            Me.stdstocklist = New clsStdStockList(Session.Contents("userID"))
            Me.stdstocklist.IPaddr = Request.UserHostAddress
            Me.stdstocklist.SessionID = Now.Year.ToString() + _
                                        Now.Month.ToString() + _
                                        Now.Day.ToString() + _
                                        Now.Hour.ToString() + _
                                        Now.Minute.ToString() + _
                                        Now.Second.ToString() + _
                                        Now.Millisecond.ToString()
            Session.Contents("stdstocklist") = Me.stdstocklist
        Else
            Me.stdstocklist = Session.Contents("stdstocklist")
        End If

        Dim count As Integer
        Dim statid As String
        Dim statnm As String

        'Dim stocktype As ArrayList = Me.stdstocklist.getStockType()
        'Me.ddlTransTY.Enabled = True
        'Me.ddlTransTY.Items.Clear()

        'For count = 0 To stocktype.Count - 1 Step 2
        '    statid = stocktype.Item(count)
        '    statnm = stocktype.Item(count + 1)
        '    If statid.Equals("STK-STD") Or statid.Equals("STK-ADJ") Then
        '        Me.ddlTransTY.Items.Add(New ListItem(statnm, statid))
        '    End If
        'Next

        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")

        ddlTransTY.ClearSelection()

        Me.ddlTransTY.Items.Add(New ListItem(objXmlTr.GetLabelName("StatusMessage", "STD-IN"), "STK-STD"))
        Me.ddlTransTY.Items.Add(New ListItem(objXmlTr.GetLabelName("StatusMessage", "STD-OUT"), "STK-ADJ"))


        ddlTransTY.SelectedIndex = 0

        If Me.stdstocklist.getSVC_Tech() = 1 Then
            Me.ddlSVC.DataSource = Me.stdstocklist.getSVC()
            Me.ddlSVC.DataTextField = "SVCNAME" '"MSVC_SVCID"
            Me.ddlSVC.DataValueField = "SVCID" '"MSVC_SVCID"
            Me.ddlSVC.DataBind()
            Me.ddlSVC.Items.Insert(0, New ListItem("", ""))
            ddlSVC.SelectedValue = Session("login_svcid")

            loadTechnician()
            Me.DDPriceID.DataSource = Me.stdstocklist.GetPriceID(Session("Login_CtryID"))
            Me.DDPriceID.DataTextField = "MPRC_ENAME"
            Me.DDPriceID.DataValueField = "MPRC_PRCID"
            Me.DDPriceID.DataBind()
        Else
            Return  ' error
        End If

        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        If Me.ddlTransTY.Items.Count < 2 Then
            Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-F-Help5")
        End If
        If Me.ddlSVC.Items.Count < 2 Then
            Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-F-Help3")
        End If
    End Sub

    Protected Sub BindGrid()
        If Me.stdstocklist IsNot Nothing Then

            Me.mygridview.DataSource = Me.stdstocklist.getPartTable()
            Me.mygridview.DataBind()

            mygridview.Visible = True
            If mygridview.Rows.Count > 0 Then
                lbtnViewCurList.Enabled = True ': LnkSelAll.Visible = True
            Else
                lbtnViewCurList.Enabled = False ': LnkSelAll.Visible = False
            End If
        End If
    End Sub

    Protected Sub ddlSVC_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSVC.SelectedIndexChanged
        Try

            If Me.stdstocklist Is Nothing Then
                Return
            End If
            If Trim(ddlSVC.SelectedValue).Equals("") Then
                Me.ddlTransTY.Enabled = True
                Me.ddlTransTY.ClearSelection()
                ddlTech.ClearSelection()
                BtnPartSearch.Enabled = False
            End If
            loadTechnician()
            If Not stdstocklist Is Nothing Then
                stdstocklist.ClearPartTab()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Sub loadTechnician()
        Me.ddlTech.DataSource = Me.stdstocklist.getTech(Me.ddlSVC.SelectedValue)
        Me.ddlTech.DataTextField = "MTCH_ENAME"
        Me.ddlTech.DataValueField = "MTCH_TCHID"
        Me.ddlTech.DataBind()
        Me.ddlTech.Items.Insert(0, New ListItem("", ""))
        If Me.ddlTech.Items.Count < 2 Then
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-F-Help4")
        Else
            Me.lblMessage.Text = ""
        End If
        txtPartCode.Text = ""
        txtQuantity.Text = ""
        txtlinetotal.Text = ""
        lblPartName.Text = ""
        lblUnitPrice.Text = ""
        DDPriceID.Enabled = True
        Me.ddlTransTY.Enabled = True
        mygridview.Visible = False
        
        RequestFromValue()
    End Sub

    Protected Sub txtPartCode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPartCode.TextChanged
        If Me.stdstocklist Is Nothing Then
            Return
        End If

        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")

        If Me.ValidatePage() = -1 Then
            Me.msg.Alert(objXmlTr.GetLabelName("StatusMessage", "BB-FUN-F-Help1"))
            Return
        End If

        If VerifyPart() = "OK" Then
            Dim partinfo As ArrayList = Me.stdstocklist.getPartInfo(Me.txtPartCode.Text, _
                                                 Me.stdstocklist.UserCtrID, _
                                                 Me.DDPriceID.SelectedValue, _
                                                 Me.txtDate.Text) 'Request.Form("txtDate")) '
            If partinfo IsNot Nothing Then
                Me.lblPartName.Text = partinfo.Item(0)
                Me.lblUnitPrice.Text = partinfo.Item(1)
                Me.lblTaxPerc.Text = partinfo.Item(2)
                Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-F-Success2")
                Me.txtQuantity.Focus()
            Else
                Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-F-Warning1")
                Me.txtPartCode.Text = ""
                Me.lblPartName.Text = ""
                Me.lblUnitPrice.Text = ""
                Me.lblTaxPerc.Text = ""
            End If
        End If
        RequestFromValue()
    End Sub

    Function VerifyPart() As String
        Dim xResp As String = stdstocklist.VerifyPartInfo(Me.txtPartCode.Text, _
                                                          Me.stdstocklist.UserCtrID, _
                                                          Me.DDPriceID.SelectedValue, _
                                                          Me.txtDate.Text)

        Me.BtnPartSearch.Enabled = True
        Select Case UCase(xResp)
            Case Is = "PARTNOTFOUND"
                Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "PARTNOTFOUND")
                Me.txtPartCode.Text = ""
                Me.lblPartName.Text = ""
                Me.lblUnitPrice.Text = ""
                Me.lblTaxPerc.Text = ""
                txtQuantity.Text = ""
                txtlinetotal.Text = ""
            Case Is = "PARTKIT"
                Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "PARTKIT")
                Me.txtPartCode.Text = ""
                Me.lblPartName.Text = ""
                Me.lblUnitPrice.Text = ""
                Me.lblTaxPerc.Text = ""
                txtQuantity.Text = ""
                txtlinetotal.Text = ""
            Case Is = "PARTSVC"
                Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "PARTSVC")
                Me.txtPartCode.Text = ""
                Me.lblPartName.Text = ""
                Me.lblUnitPrice.Text = ""
                Me.lblTaxPerc.Text = ""
                txtQuantity.Text = ""
                txtlinetotal.Text = ""
            Case Is = "PARTOBSOLETE"
                Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "PARTOBSOLETE")
                Me.txtPartCode.Text = ""
                Me.lblPartName.Text = ""
                Me.lblUnitPrice.Text = ""
                Me.lblTaxPerc.Text = ""
                txtQuantity.Text = ""
                txtlinetotal.Text = ""
            Case Is = "PARTNOTEFF"
                Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "PARTNOTEFF")
                Me.txtPartCode.Text = ""
                Me.lblPartName.Text = ""
                Me.lblUnitPrice.Text = ""
                Me.lblTaxPerc.Text = ""
                txtQuantity.Text = ""
                txtlinetotal.Text = ""
            Case Else 'if ok
                txtQuantity.Text = ""
                txtlinetotal.Text = ""
                Return "OK"
                Exit Function
        End Select
        RequestFromValue()
        Return xResp
    End Function

    Protected Sub txtQuantity_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtQuantity.TextChanged
        Try
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            If Me.ValidatePage() = -1 Then
                Me.msg.Alert(objXmlTr.GetLabelName("StatusMessage", "BB-FUN-F-Help1"))
                Return
            End If

            If Not IsNumeric(txtQuantity.Text) Then
                lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-F-Help2")
                Exit Sub
            End If

            If String.IsNullOrEmpty(Me.txtPartCode.Text) Or String.IsNullOrEmpty(Me.lblPartName.Text) Or _
               String.IsNullOrEmpty(Me.lblUnitPrice.Text) Then
                Me.txtPartCode.Text = ""
                Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-F-Help6")
            Else
                'check qty if adjustment
                If ddlTransTY.SelectedValue = "STK-ADJ" Or ddlTransTY.SelectedValue = "ADJ" Then
                    Dim xRest As Boolean = stdstocklist.CheckAdjustmentQty(ddlTech.SelectedValue, ddlSVC.SelectedValue, _
                                            Trim(txtPartCode.Text), Session("Login_CtryID"), CInt(Trim(txtQuantity.Text)))
                    If xRest = False Then
                        Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "GPL-INVQTY")
                        Exit Sub
                    End If
                End If


                If txtPartCode.Text <> "" And txtQuantity.Text <> "" Then
                    Dim lineamt As Double = Single.Parse(Me.lblUnitPrice.Text) * Integer.Parse(Me.txtQuantity.Text)
                    Me.txtlinetotal.ReadOnly = False
                    Me.txtlinetotal.Text = Format(lineamt, "#####0.00")
                    Me.txtlinetotal.ReadOnly = True
                End If
                'CInt(txtQuantity.Text) > 0 And
                If txtPartCode.Text <> "" And txtlinetotal.Text <> "" Then
                    lbtnAdd.Enabled = True
                Else
                    lbtnAdd.Enabled = False
                End If
            End If
            RequestFromValue()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub lbtnAddStdlist_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnAddStdlist.Click
        Try

            If Me.stdstocklist Is Nothing Then
                Return
            End If
            Dim xTrNo As String

            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            If Me.ValidatePage() = -1 Then
                Me.msg.Alert(objXmlTr.GetLabelName("StatusMessage", "BB-FUN-F-Help1"))
                Return
            End If
            Me.stdstocklist.IPaddr = Request.UserHostAddress
            Me.stdstocklist.SessionID = Now.Year.ToString() + _
                                        Now.Month.ToString() + _
                                        Now.Day.ToString() + _
                                        Now.Hour.ToString() + _
                                        Now.Minute.ToString() + _
                                        Now.Second.ToString() + _
                                        Now.Millisecond.ToString()
            Dim result As Integer = Me.stdstocklist.addStdStockList( _
                                        Me.ddlTech.SelectedValue.Trim(), _
                                        Me.txtDate.Text.Trim(), _
                                        Me.ddlSVC.SelectedValue.Trim(), "STD", xTrNo)
            Select Case result
                Case 1
                    Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-F-Success1")
                    Dim stkno As ArrayList = Me.stdstocklist.getStkListNo(Me.ddlSVC.SelectedValue, _
                                                                          Me.ddlTech.SelectedValue, "STD")
                    If stkno IsNot Nothing Then
                        If stkno.Count > 1 Then
                            Me.lblSTKNO.Text = stkno.Item(1)
                        End If
                    End If
                Case 0
                    Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-F-Warning2")
                Case -1
                    Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-F-Error1")
                Case -2
                    Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-F-Warning8")
            End Select
            RequestFromValue()
        Catch ex As Exception

        End Try
    End Sub

    Protected Function AddStdList() As String
        Dim Res As String
        Dim xType As String
        Dim xTranNo As String
        Try

            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            If Me.ddlTransTY.Text = "STK-ADJ" Or Me.ddlTransTY.Text = "ADJ" Then
                xType = "ADJ"
            Else
                xType = "STD"
            End If

            Me.stdstocklist.IPaddr = Request.UserHostAddress
            Me.stdstocklist.SessionID = Now.Year.ToString() + _
                                        Now.Month.ToString() + _
                                        Now.Day.ToString() + _
                                        Now.Hour.ToString() + _
                                        Now.Minute.ToString() + _
                                        Now.Second.ToString() + _
                                        Now.Millisecond.ToString()
            Dim result As Integer = Me.stdstocklist.addStdStockList( _
                                        Me.ddlTech.SelectedValue.Trim(), _
                                        txtDate.Text, _
                                        Me.ddlSVC.SelectedValue.Trim(), xType, xTranNo)
            Select Case result
                Case 1
                    Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-F-Success1")
                    Dim stkno As ArrayList = Me.stdstocklist.getStkListNo(Me.ddlSVC.SelectedValue, _
                                                                          Me.ddlTech.SelectedValue, xType)
                    If stkno IsNot Nothing Then
                        If stkno.Count > 1 Then
                            Me.lblSTKNO.Text = stkno.Item(1)
                            Res = stkno.Item(1)
                        End If
                    End If
                Case 0
                    Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-F-Warning2")
                Case -1
                    Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-F-Error1")
                Case -2
                    Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-F-Warning8")
            End Select


            Return xTranNo
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Protected Sub lbtn1stdlist_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtn1stdlist.Click
        If Me.stdstocklist Is Nothing Then
            Return
        End If

        'Dim xRes As String = AddStdList()

        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        If Me.ValidatePage() = -1 Then
            Me.msg.Alert(objXmlTr.GetLabelName("StatusMessage", "BB-FUN-F-Help1"))
            Return
        End If
        'Me.stdstocklist.PackListNo = xRes 'Me.lblSTKNO.Text
        PnlLoad.Visible = True
        'Session("Res") = xRes
        Me.stdstocklist.TransSVC = Me.ddlSVC.SelectedValue
        Me.stdstocklist.TransDate = Me.txtDate.Text
        DisableButtons()
        RequestFromValue()
        'Response.Redirect("~/PresentationLayer/function/LoadStdStkList.aspx")
    End Sub

    Protected Sub lbtnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnAdd.Click
        Try
            If Me.stdstocklist Is Nothing Then
                Return
            End If

            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            If Me.ValidatePage() = -1 Then
                Me.msg.Alert(objXmlTr.GetLabelName("StatusMessage", "BB-FUN-F-Help1"))
                Return
            End If

            If String.IsNullOrEmpty(Me.txtPartCode.Text) Or String.IsNullOrEmpty(Me.txtQuantity.Text) Or _
               String.IsNullOrEmpty(Me.lblUnitPrice.Text) Then
                Me.msg.Alert(objXmlTr.GetLabelName("StatusMessage", "BB-FUN-F-Help7"))
            Else
                'added by deyb -verify part code
                'Dim xRes As Boolean = stdstocklist.VerifyPart(Me.txtPartCode.Text.Trim(), Me.stdstocklist.UserCtrID)
                'If xRes = False Then
                '    Me.msg.Alert(objXmlTr.GetLabelName("StatusMessage", "BB-FUN-F-Warning1"))
                '    Return
                'End If
                '-------------------------------

                Dim unitprice As Single = Single.Parse(Me.lblUnitPrice.Text)
                Dim qty As Integer = Integer.Parse(Me.txtQuantity.Text)
                Dim lineamt As Single = Format(Single.Parse(Me.txtlinetotal.Text), "####0.00")
                Dim linetax As Single = lineamt * Single.Parse(Me.lblTaxPerc.Text) / 100
                'added by deyb
                Session("ctrID") = Me.stdstocklist.UserCtrID
                '-------------
                If Me.stdstocklist.addPart(Me.txtPartCode.Text.Trim(), _
                                           Me.lblPartName.Text, _
                                           Me.stdstocklist.UserCtrID, _
                                           unitprice, qty, linetax, _
                                           lineamt, ddlTransTY.SelectedValue) = 1 Then
                    BindGrid()
                    Me.txtPartCode.Text = ""
                    Me.txtQuantity.Text = ""
                    Me.txtlinetotal.ReadOnly = False
                    Me.txtlinetotal.Text = ""
                    Me.txtlinetotal.ReadOnly = True
                    Me.lblPartName.Text = ""
                    Me.lblUnitPrice.Text = ""
                    Me.lblTaxPerc.Text = ""
                    mygridview.Visible = True
                    lbtnSave.Enabled = True

                    ddlTransTY.Enabled = False
                    DDPriceID.Enabled = False
                    lbtnDelete.Enabled = True
                    lbtnSave.Enabled = True
                Else
                    Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-F-Error2")
                End If
            End If
            RequestFromValue()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub lbtnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnSave.Click
        Me.objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")

        If Not IsDate(txtDate.Text) Or txtDate.Text = "" Then
            Me.msgSave.Alert("HI")
        End If

        If mygridview.Visible = True Then
            Me.msgSave.Confirm(objXmlTr.GetLabelName("StatusMessage", "SAVEORNOT"))
        Else
            Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-addpart1st")
        End If
        RequestFromValue()
    End Sub

    Protected Sub lbtnGenPackList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnGenPackList.Click
        Me.objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        If Me.stdstocklist Is Nothing Then
            Return
        End If

        If ddlSVC.SelectedItem.Value = "" Then
            lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "GPL-SVCCTR")
            Exit Sub
        End If

        If ddlTech.SelectedItem.Value = "" Then
            lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "GPL-TECHID")
            Exit Sub
        End If
        RequestFromValue()
        Me.MsgGenPack.Confirm(objXmlTr.GetLabelName("StatusMessage", "GPL-CONFIRM"))
    End Sub

    Protected Sub lbtnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnDelete.Click
        Try
            If Me.stdstocklist Is Nothing Then
                Return
            End If

            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            If Me.ValidatePage() = -1 Then
                Me.msg.Alert(objXmlTr.GetLabelName("StatusMessage", "BB-FUN-F-Help1"))
                Return
            End If
            If Me.mygridview.SelectedIndex > -1 Then
                Dim partid As String, ctrid As String
                partid = Me.mygridview.SelectedRow.Cells(2).Text
                'ctrid = Me.mygridview.SelectedRow.Cells(4).Text
                ctrid = Session("ctrID")
                Me.stdstocklist.delPart(partid, ctrid)
                txtDate.Text = txtDate.Text 'Request.Form("txtDate")
                BindGrid()
                If mygridview.Rows.Count < 1 Then
                    lbtnSave.Enabled = False
                    ddlTransTY.Enabled = True
                    DDPriceID.Enabled = True
                    lbtnDelete.Enabled = False
                    lbtnSave.Enabled = False
                End If
            Else
                Me.msg.Alert(objXmlTr.GetLabelName("StatusMessage", "BB-FUN-F-Help8"))
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub mygridview_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles mygridview.RowDataBound
        If e.Row.RowIndex >= 0 Then
            'modified by deyb
            If CDbl(e.Row.Cells(8).Text) < 0 Then
                e.Row.Visible = False
            End If
        End If
        e.Row.Cells(8).Visible = False
    End Sub

    Protected Sub ddlTech_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTech.SelectedIndexChanged
        Try
            If Me.stdstocklist Is Nothing Then
                If ddlTech.SelectedValue <> "" Then
                    lbtnGenPackList.Enabled = True
                    BtnPartSearch.Enabled = True
                Else
                    lbtnGenPackList.Enabled = False
                    BtnPartSearch.Enabled = False
                End If

                Return
            End If

            If ddlTech.SelectedValue <> "" Then
                lbtnGenPackList.Enabled = True
                BtnPartSearch.Enabled = True
            Else
                lbtnGenPackList.Enabled = False
                BtnPartSearch.Enabled = False
            End If

            If Trim(ddlTech.SelectedValue).Equals("") Then
                txtPartCode.Text = ""
                txtQuantity.Text = ""
                txtlinetotal.Text = ""
                lblPartName.Text = ""
                lblUnitPrice.Text = ""
                DDPriceID.Enabled = True
                Me.ddlTransTY.Enabled = True
                mygridview.Visible = False
                If Not stdstocklist Is Nothing Then
                    stdstocklist.ClearPartTab()
                End If
                Return
            End If
            Dim xType As String
            If Me.ddlTransTY.Text = "STK-ADJ" Or ddlTransTY.SelectedValue = "ADJ" Then
                xType = "ADJ"
            Else
                xType = "STD"
            End If
            Dim stktyno As ArrayList = Me.stdstocklist.getStkListNo( _
                                    Me.ddlSVC.SelectedValue, Me.ddlTech.SelectedValue, xType)

            If Me.ddlTransTY.Items.FindByValue("STK-STD").Selected Then
                lbtn1stdlist.Enabled = True
            Else
                lbtn1stdlist.Enabled = False
            End If

            txtPartCode.Text = ""
            txtQuantity.Text = ""
            txtlinetotal.Text = ""
            lblPartName.Text = ""
            lblUnitPrice.Text = ""
            DDPriceID.Enabled = True
            Me.ddlTransTY.Enabled = True
            mygridview.Visible = False
            If Not stdstocklist Is Nothing Then
                stdstocklist.ClearPartTab()
            End If
            RequestFromValue()
        Catch ex As Exception

        End Try
    End Sub

    Private Function ValidatePage() As Integer
        If String.IsNullOrEmpty(Me.ddlTransTY.SelectedValue) Or _
           String.IsNullOrEmpty(Me.ddlSVC.SelectedValue) Or _
           String.IsNullOrEmpty(Me.ddlTech.SelectedValue) Then
            Return -1
        Else
            Return 1
        End If
    End Function

    Protected Sub msgSave_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles msgSave.GetMessageBoxResponse
        If e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok Then
            If Me.stdstocklist Is Nothing Then
                txtDate.Text = txtDate.Text 'Request.Form("txtDate")
                Return
            End If

            Dim xRes As String = AddStdList()

            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            If Me.ValidatePage() = -1 Then
                Me.msg.Alert(objXmlTr.GetLabelName("StatusMessage", "BB-FUN-F-Help1"))
                Return
            End If
            Dim result As Integer = 0, errlist As New ArrayList()
            Dim xType As String
            If Me.ddlTransTY.Text = "STK-ADJ" Or ddlTransTY.SelectedValue = "ADJ" Then
                xType = "ADJ"
            Else
                xType = "STD"
            End If
            result = Me.stdstocklist.updateStdStock(xType, xRes, errlist)
            If result = 1 Then
                lbtnGenPackList.Enabled = True
                Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-F-Success3")

                Dim script As String = "window.open('StdStockListReport.aspx?trnty=" & xType & "&trnno=" & xRes & "&svcid=" & Me.ddlSVC.SelectedValue & "&tchid=" & Me.ddlTech.SelectedValue & "&usrid=" & Session("userID") & "')"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "RPTCSVHVIEW", script, True)

            ElseIf result = 0 Then
                Dim str As String = ""
                For i As Integer = 0 To errlist.Count - 1
                    str += errlist.Item(i).ToString()
                    str += ","
                Next
                Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-F-Success3") + str


            End If
            lbtnSave.Enabled = False
            mygridview.Visible = False
            stdstocklist.ClearPartTab()
            ddlTransTY.Enabled = True
            DDPriceID.Enabled = True
            RequestFromValue()
        End If
    End Sub

    Protected Sub setPurview()
        Dim arrayPurview As Boolean() = Nothing
        arrayPurview = clsUserAccessGroup.GetUserPurview( _
                               Session.Contents("accessgroup"), "27" _
                        )
        Me.lbtn1stdlist.Enabled = arrayPurview(0)
        Me.lbtnAddStdlist.Enabled = arrayPurview(0)
        Me.lbtnGenPackList.Enabled = arrayPurview(0)
        Me.lbtnSave.Enabled = arrayPurview(1)
        Me.lbtnViewCurList.Enabled = arrayPurview(2)
        Me.lbtnViewExiList.Enabled = arrayPurview(2)
    End Sub

    Protected Sub MyCalendar_SelectedDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyCalendar.SelectedDateChanged
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Me.txtDate.Text = Me.MyCalendar.SelectedDate
    End Sub

    Protected Sub lbtnViewExiList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnViewExiList.Click
        Try
            RetainValues()
            RequestFromValue()
            Response.Redirect("~/PresentationLayer/function/ViewStdStockList.aspx?id=1" & "&ddlSVC=" & ddlSVC.SelectedValue & "&ddlTech=" & ddlTech.SelectedValue)
        Catch ex As Exception
            'error
        End Try
    End Sub

    Protected Sub LnkSelAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LnkSelAll.Click
        Try
            Dim x As Integer
            For x = 0 To mygridview.Rows.Count - 1
                Dim xChk As CheckBox = CType(mygridview.Rows(x).FindControl("ChkAll"), CheckBox)
                xChk.Checked = True
            Next
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub lbtnViewCurList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnViewCurList.Click

    End Sub

    Protected Sub lbtnLoad_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnLoad.Click
        Try
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            If String.IsNullOrEmpty(Me.FileUpload1.PostedFile.FileName) Then
                Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-F-Help9")
                lbtnviewexilist.enabled = False
                Return
            End If
            If Me.FileUpload1.PostedFile.ContentType <> "text/plain" Then
                Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-F-Warning7")
                Return
            End If
            RequestFromValue()
            Dim err As Integer = 0
            Dim stdstklist As clsStdStockList = Session.Contents("stdstocklist")
            Dim params As New ArrayList(), errlist As New ArrayList()
            Dim FileStr As Stream = Me.FileUpload1.PostedFile.InputStream
            Dim reader As StreamReader = New StreamReader(FileStr)
            Dim linecount As Integer = 0
            Try

                Dim strLine As String = reader.ReadLine()
                Dim xUPrice As Double
                Dim xQty As Integer
                Dim xAmt As Double
                Dim xTax As Single
                Dim xPrtInfo As ArrayList

                'Dim xRes As String = AddStdList()

                While strLine IsNot Nothing
                    Dim strs As String() = strLine.Split(" ")
                    params.Clear()
                    For Each str1 As String In strs
                        Dim str2 As String = str1.Trim()
                        If str1.CompareTo(" ") > 0 Then
                            params.Add(str2)
                        End If
                    Next
                    'get info details
                    xPrtInfo = stdstocklist.getPartInfo(params(0), Session("Login_CtryID"), DDPriceID.SelectedValue, txtDate.Text)
                    If xPrtInfo IsNot Nothing Then
                        xUPrice = Format(xPrtInfo.Item(1), "####0.00")
                        xQty = params(2)
                        xAmt = Format(xQty * xUPrice, "####0.00")
                        xTax = 0
                        If Me.stdstocklist.addPart(params(0), xPrtInfo.Item(0), Me.stdstocklist.UserCtrID, xUPrice, xQty, xTax, xAmt, ddlTransTY.SelectedValue) = 1 Then
                            BindGrid()
                            Me.txtPartCode.Text = ""
                            Me.txtQuantity.Text = ""
                            Me.txtlinetotal.ReadOnly = False
                            Me.txtlinetotal.Text = ""
                            Me.txtlinetotal.ReadOnly = True
                            Me.lblPartName.Text = ""
                            Me.lblUnitPrice.Text = ""
                            Me.lblTaxPerc.Text = ""
                            mygridview.Visible = True
                            lbtnSave.Enabled = True
                        Else
                            Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-F-Error2")
                        End If
                    Else
                        Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-F-Warning1")
                        Exit Sub
                    End If
                    strLine = reader.ReadLine()
                End While
                'hide panel
                PnlLoad.Visible = False
                EnableButtons()
                txtDate.Text = txtDate.Text 'Request.Form("txtDate")
            Catch ex As Exception
                Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-F-Error6")
                err = 1
            Finally
                FileStr.Close()
                reader.Close()
            End Try

            If err = 1 Then
                Return
            End If

            If errlist.Count > 0 Then
                Dim errstr As String = ""
                For i As Integer = 0 To errlist.Count - 1
                    errstr += (errlist.Item(i) + ",")
                Next
                Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-F-Error5") + errstr
            Else
                Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-F-Success4")
            End If
        Catch ex As Exception

        End Try
    End Sub

    Public Sub databonds(ByRef dt As DataTable, ByRef dropdown As DropDownList)
        Dim count As Integer = 0
        dropdown.Items.Clear()
        dropdown.Items.Add(New ListItem("", ""))
        Dim row As DataRow
        For Each row In dt.Rows
            Dim NewItem As New ListItem()
            NewItem.Text = row("id") & "-" & row("name")
            NewItem.Value = row("id")
            dropdown.Items.Add(NewItem)
            count = count + 1
        Next
    End Sub

    Protected Sub LnkCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LnkCancel.Click
        PnlLoad.Visible = False
        EnableButtons()
        RequestFromValue()
    End Sub

    Sub DisableButtons()
        lbtn1stdlist.Enabled = False : lbtnViewExiList.Enabled = False : ddlTransTY.Enabled = False : ddlTech.Enabled = False : DDPriceID.Enabled = False : txtPartCode.Enabled = False : txtQuantity.Enabled = False : txtlinetotal.Enabled = False : lbtnSave.Enabled = False : lbtnGenPackList.Enabled = False : lbtnDelete.Enabled = False : lbtnAdd.Enabled = False : ddlSVC.Enabled = False
    End Sub

    Sub EnableButtons()
        lbtn1stdlist.Enabled = True : lbtnViewExiList.Enabled = True : ddlTransTY.Enabled = True : ddlTech.Enabled = True : DDPriceID.Enabled = True : txtPartCode.Enabled = True : txtQuantity.Enabled = True : txtlinetotal.Enabled = True : lbtnSave.Enabled = True : lbtnGenPackList.Enabled = True : lbtnDelete.Enabled = True : lbtnAdd.Enabled = True : ddlSVC.Enabled = True
    End Sub

    Protected Sub ddlTransTY_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTransTY.SelectedIndexChanged
        If Me.ddlTransTY.Items.FindByValue("STK-STD").Selected Then
            lbtn1stdlist.Enabled = True
        Else
            lbtn1stdlist.Enabled = False
        End If
    End Sub

    Protected Sub MsgGenPack_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MsgGenPack.GetMessageBoxResponse
        If e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok Then
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            Try
                Dim xTy As String
                If ddlTransTY.SelectedValue = "STK-ADJ" Or ddlTransTY.SelectedValue = "ADJ" Then
                    xTy = "ADJ"
                Else
                    xTy = "STD"
                End If
                Dim xMsg As String
                Dim xRet As String = stdstocklist.GeneratePackingList(Me.ddlTech.SelectedValue, ddlSVC.SelectedValue, Session("Login_CmpID"), Session("Login_CtryID"), Session("UserID"), xMsg)
                Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", xMsg) & " : " & xRet
                RequestFromValue()
            Catch ex As Exception
                Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-F-Error5")
            End Try
        End If
    End Sub

    Protected Sub DDPriceID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDPriceID.SelectedIndexChanged
        txtPartCode.Text = ""
        txtQuantity.Text = ""
        txtlinetotal.Text = ""
        Me.txtPartCode.Text = ""
        Me.lblPartName.Text = ""
        Me.lblUnitPrice.Text = ""
        Me.lblTaxPerc.Text = ""
        RequestFromValue()
    End Sub

    Protected Sub BtnPartSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnPartSearch.Click
        Try
            RetainValues()
           
            RequestFromValue()
            Response.Redirect("PartSearch.aspx")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub PopulateStdList()
        Try
            If Session("xHTSSL") Is Nothing Then
                Exit Sub
            End If
            Dim xHTSSL As New Hashtable
            xHTSSL = Session("xHTSSL")

            'ddlTransTY.Items(xHTSSL.Item("TransTypeIDX")).Selected = True
            ddlTransTY.SelectedIndex = xHTSSL.Item("TransTypeIDX")
            'ddlSVC.Items(xHTSSL.Item("SVCIDX")).Selected = True
            ddlSVC.SelectedIndex = xHTSSL.Item("SVCIDX")
            'populate technician DD by SVC
            Me.ddlTech.DataSource = Me.stdstocklist.getTech(xHTSSL.Item("SVCVal"))
            Me.ddlTech.DataTextField = "MTCH_ENAME"
            Me.ddlTech.DataValueField = "MTCH_TCHID"
            Me.ddlTech.DataBind()
            Me.ddlTech.Items.Insert(0, New ListItem("", ""))
            '----------------------------------
            DDPriceID.SelectedValue = xHTSSL.Item("PriceID")
            'ddlTech.Items(xHTSSL.Item("Technician")).Selected = True
            ddlTech.SelectedIndex = xHTSSL.Item("Technician")
            lbtnGenPackList.Enabled = xHTSSL.Item("BtnGPL")
            lbtn1stdlist.Enabled = xHTSSL.Item("BtnStdList")
            lbtnViewExiList.Enabled = xHTSSL.Item("BtnExiList")
            lbtnAdd.Enabled = xHTSSL.Item("BtnAdd")
            lbtnSave.Enabled = xHTSSL.Item("BtnSave")
            lbtnDelete.Enabled = xHTSSL.Item("BtnDelete")
            txtDate.Text = xHTSSL.Item("Date")
            LblCal.Text = xHTSSL.Item("Date")
            ddlTransTY.Enabled = xHTSSL.Item("TransTypeStat")
            DDPriceID.Enabled = xHTSSL.Item("PriceIDStat")
            txtPartCode.Text = xHTSSL.Item("PartCode")
            Me.txtDate.Text = xHTSSL.Item("CreateDate")

            Dim xDS As New DataSet
            xDS = Session("dsitem")
            If xDS.Tables("StdStockList") IsNot Nothing Then
                mygridview.DataSource = xDS
                mygridview.DataBind()
                'RefreshDataSet()
            End If
            mygridview.Visible = True

            Session.Remove("xHTSSL")

            Session.Remove("dsitem")

            'Dim xBack As Boolean = CBool(Request.Params("back"))
            If Request.Params("back") Is Nothing Then
                'verify the part  
                If VerifyPart() = "OK" Then
                    Dim partinfo As ArrayList = Me.stdstocklist.getPartInfo(Me.txtPartCode.Text, _
                                                         Me.stdstocklist.UserCtrID, _
                                                         Me.DDPriceID.SelectedValue, _
                                                         Me.txtDate.Text)
                    If partinfo IsNot Nothing Then
                        Me.lblPartName.Text = partinfo.Item(0)
                        Me.lblUnitPrice.Text = partinfo.Item(1)
                        Me.lblTaxPerc.Text = partinfo.Item(2)
                        Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-F-Success2")
                        Me.txtQuantity.Focus()
                    Else
                        Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-F-Warning1")
                        Me.txtPartCode.Text = ""
                        Me.lblPartName.Text = ""
                        Me.lblUnitPrice.Text = ""
                        Me.lblTaxPerc.Text = ""
                    End If
                End If
                'BtnPartSearch.Enabled = True
            End If
            BtnPartSearch.Enabled = True

            RequestFromValue()
        Catch
            'error
        End Try

    End Sub

    Private Sub RefreshDataSet()
        Try
            If mygridview.Visible = False Then
                'If Not Me.stdstocklist Is Nothing Then
                '    stdstocklist.ClearPartTab()
                'End If
                If mygridview.Rows.Count > 0 Then
                    dsitem.Clear()
                    'Dim x As Integer
                    'For x = 0 To mygridview.Rows.Count - 1
                    mygridview.Dispose()
                    stdstocklist.ClearPartTab()
                    'Next
                End If
            Else
                dsitem.Clear()

                If dsitem.Tables.Count = 0 Then
                    dsitem.Tables.Add("StdStockList")
                    dsitem.Tables("StdStockList").Columns.Add("No")
                    dsitem.Tables("StdStockList").Columns.Add("PartID")
                    dsitem.Tables("StdStockList").Columns.Add("PartName")
                    dsitem.Tables("StdStockList").Columns.Add("CtrID")
                    dsitem.Tables("StdStockList").Columns.Add("Sign")
                    dsitem.Tables("StdStockList").Columns.Add("Quantity")
                    dsitem.Tables("StdStockList").Columns.Add("UnitPrice")
                    dsitem.Tables("StdStockList").Columns.Add("LineAmt")
                    dsitem.Tables("StdStockList").Columns.Add("Stat")

                End If

                Dim intGrdRow As Integer = 0
                Dim drItem As DataRow
                Dim dblGrossTotal As Double = 0
                Dim dblTax As Double = 0

                While intGrdRow <= mygridview.Rows.Count - 1
                    drItem = dsitem.Tables("StdStockList").NewRow()
                    drItem.Item(0) = intGrdRow + 1
                    drItem.Item(1) = Me.mygridview.Rows(intGrdRow).Cells(2).Text
                    drItem.Item(2) = Me.mygridview.Rows(intGrdRow).Cells(3).Text
                    drItem.Item(3) = Val(Me.mygridview.Rows(intGrdRow).Cells(4).Text)
                    drItem.Item(4) = Me.mygridview.Rows(intGrdRow).Cells(5).Text
                    drItem.Item(5) = Val(Me.mygridview.Rows(intGrdRow).Cells(6).Text)
                    drItem.Item(6) = String.Format("{0:f2}", Val(Me.mygridview.Rows(intGrdRow).Cells(7).Text))
                    drItem.Item(7) = String.Format("{0:f2}", Val(Me.mygridview.Rows(intGrdRow).Cells(8).Text))
                    drItem.Item(8) = Val(Me.mygridview.Rows(intGrdRow).Cells(9).Text)

                    dsitem.Tables("StdStockList").Rows.Add(drItem)

                    intGrdRow = intGrdRow + 1
                End While
            End If
        Catch ex As Exception

            'error
        End Try

    End Sub

    Sub RetainValues()
        Try
            Dim xHTSSL As New Hashtable
            xHTSSL.Item("TransTypeIDX") = Me.ddlTransTY.SelectedIndex
            xHTSSL.Item("SVCIDX") = Me.ddlSVC.SelectedIndex
            xHTSSL.Item("SVCVal") = Me.ddlSVC.SelectedValue
            xHTSSL.Item("PartCode") = Me.txtPartCode.Text
            xHTSSL.Item("PriceID") = Me.DDPriceID.SelectedValue
            xHTSSL.Item("Technician") = Me.ddlTech.SelectedIndex
            xHTSSL.Item("BtnGPL") = Me.lbtnGenPackList.Enabled
            xHTSSL.Item("BtnStdList") = Me.lbtn1stdlist.Enabled
            xHTSSL.Item("BtnExiList") = Me.lbtnViewExiList.Enabled
            xHTSSL.Item("BtnAdd") = Me.lbtnAdd.Enabled
            xHTSSL.Item("BtnSave") = Me.lbtnSave.Enabled
            xHTSSL.Item("BtnDelete") = Me.lbtnDelete.Enabled
            xHTSSL.Item("Date") = CStr(Me.txtDate.Text) 'Request.Form("txtDate") '
            xHTSSL.Item("TransTypeStat") = Me.ddlTransTY.Enabled
            xHTSSL.Item("PriceIDStat") = Me.DDPriceID.Enabled
            xHTSSL.Item("CreateDate") = Me.txtDate.Text

            RefreshDataSet()
            Session("dsitem") = dsitem
            Session("xHTSSL") = xHTSSL
        Catch

        End Try

    End Sub
End Class
