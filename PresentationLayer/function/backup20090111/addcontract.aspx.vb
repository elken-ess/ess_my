﻿Imports BusinessEntity
Imports System.data
Partial Class PresentationLayer_function_addcontract
    Inherits System.Web.UI.Page
    Dim objXmlTr As New clsXml
    Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.MaintainScrollPositionOnPostBack = True
        AjaxPro.Utility.RegisterTypeForAjax(GetType(PresentationLayer_function_addcontract))
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        If Not Page.IsPostBack Then

            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try

            'commenrt by raymond 26082006 because of ajax
            '---------------------------------------------------
            'Session("FUN-AACNT-ROID") = ""
            'Session("FUN-AACNT-NoofYear") = 0
            'Session("FUN-AACNT-Frequence") = 0
            '----------------------------------------------------

            'validator
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            'custIDError.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "FUN-CNT-CUSTID")
            'custPfError.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "FUN-CNT-CUSTPF")
            'ROInfoError.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "FUN-CNT-ROINFO")
            cntTypeError.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "FUN-CNT-CNTTYPE")
            cntNoError.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "FUN-CNT-CNTNO")
            cntStartDateError.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "FUN-CNT-STARTDATE")
            servCError.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "FUN-CNT-SERVC")
            'ROInfoError.Visible = False
            cntTypeError.Visible = False
            cntNoError.Visible = False
            cntStartDateError.Visible = False
            servCError.Visible = False
            saveButton.Enabled = False

            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-ACNT-TITL")
            custIDLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-CUSTID")
            custPfLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-CUSTPRX")
            custNameLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-CUSTNAME")
            ROInfoLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-ROINFO")
            cntTypeLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-ACNT-CNTTYPE")
            cntNoLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CNT-CNTNO")
            cntDspLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-ACNT-CNTDSP")
            lastServDateLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-LASTSERVDATE")
            installDateLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-ACNT-INSDATE")
            cntStartDateLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CNT-CNTSDATE")
            cntEndDateLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CNT-CNTEDATE")
            custContactLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-ACNT-CUSTCONTACT")
            preCntNoLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-ACNT-PRECNTNO")
            signByLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-ACNT-SIGNBY")
            cntStatusLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CNT-STATUS")
            servCLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-SERVCENTER")
            nextServDateLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-ACNT-NSERVDATE")
            SMSRemLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-ACNT-SMSREM")
            notesLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-ACNT-NOTES")
            revCustomlkBtn.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-ACNT-REVCUSTOM")
            servHistorylkBtn.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-SERVHISTORY")
            contractHistroylkBtn.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-CONTRACTHISTORY")
            saveButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Save")
            cancelLink.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Cancel")
            CompDateVal.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "INVALID_DATE")

            lblNoteUP.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            lblNoteDown.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")


            ROInfoDrpList.Items.Add(New ListItem("", ""))
            cntTypeDrpList.Items.Add(New ListItem("", ""))
            'servHistorylkBtn.Visible = False
            'contractHistroylkBtn.Visible = False
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
            cntStartDateBox.Text = Date.Today
            cntEndDateBox.Text = cntStartDateBox.Text

            If Request.QueryString.Count <> 0 Then
                custIDBox.Text = Request.Params("custID").ToString()
                custPfBox.Text = Request.Params("custPf").ToString()
            End If
            HypCal.NavigateUrl = "javascript:DoCal(document.form1.cntStartDateBox);"
            GetData()


            '''access control'''''''''''''''''''''''''''''''''''''''''''''''
            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "23")

            Me.saveButton.Enabled = purviewArray(0)


        ElseIf cntServInfoView.Rows.Count > 0 Then
            DispGridViewHead()
        End If
        Me.errlab.Visible = False
    End Sub
    Private Sub GetData()
        'Server Center drop down list

        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        Dim apptEntity As New clsAppointment()
        Dim ds As DataSet = apptEntity.GetSVCIDByUserID(Session("userID"))
        If ds Is Nothing Then
            Return
        End If
        databonds(ds.Tables("ServerCenterID"), servCDrpList)
        'servCDrpList.Items.Insert(0, New ListItem("", ""))

        'Wey Start 12/10/2007
        '---- TECHNICIAN BASED ON SERVICE CENTER
        Dim Technician As New clsCommonClass
        Dim rank As String = Session("login_rank")
        If rank = 7 Then
            Technician.spctr = Session("login_ctryid")
            Technician.spstat = Session("login_cmpID")
            Technician.sparea = servCDrpList.SelectedValue
        End If
        If rank = 8 Then
            Technician.spctr = Session("login_ctryid")
            Technician.spstat = Session("login_cmpID")
            Technician.sparea = servCDrpList.SelectedValue
        End If
        If rank = 9 Then
            Technician.spctr = Session("login_ctryid")
            Technician.spstat = Session("login_cmpID")
            Technician.sparea = Session("login_svcID")
        End If
        If rank = 0 Then
            Technician.spctr = Session("login_ctryid")
        End If

        Technician.rank = rank

        'Technician
        Dim dsTechnician As New DataSet
        dsTechnician = Technician.Getcomidname("BB_MASTECH_NAMEID")
        If dsTechnician.Tables.Count <> 0 Then
            databondsForTechnician(dsTechnician, Me.cboTechnician)
        End If
        Me.cboTechnician.Items.Insert(0, "")
        Me.cboTechnician.SelectedIndex = 0

        'Status
        Dim count As Integer
        Dim statid As String
        Dim statnm As String
        Dim cntStat As New clsUtil()
        Dim statParam As ArrayList = New ArrayList
        statParam = cntStat.searchconfirminf("CNTStatus")
        'cntStatusDrpList.Items.Add(New ListItem(statParam.Item(1), statParam.Item(0))) 'active
        cntStatusBox.Text = statParam.Item(1) 'active

        'SMS Reminder
        statParam = cntStat.searchconfirminf("YESNO")
        For count = 0 To statParam.Count - 1
            statid = statParam.Item(count)
            statnm = statParam.Item(count + 1)
            count = count + 1
            SMSRemDrpList.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
        Next

        'Dim cntEntity As New clsContract
        'cntNoBox.Text = cntEntity.GetContractNo()
        'If cntNoBox.Text = "" Then
        '    Response.Redirect("~/PresentationLayer/error.aspx")
        'End If
        servHistorylkBtn.Enabled = False
        contractHistroylkBtn.Enabled = False

        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        If Trim(custIDBox.Text) <> "" And Trim(custPfBox.Text <> "") Then
          
            apptEntity.CustomerID = Trim(custIDBox.Text)
            apptEntity.CustomerPrifx = Trim(custPfBox.Text)
            apptEntity.ServiceType = "CONTRACT"
            ds = apptEntity.GetDropDownListData(Session("userID"))
            'If ds Is Nothing Or ds.Tables("MCUS_FIL").Rows.Count <> 1 Or ds.Tables("ROINFO").Rows.Count < 1 Then
            '    Dim msginfo As String = objXmlTr.GetLabelName("StatusMessage", "FUN-CNT-CUSTNOTEXIST")
            '    InfoMsgBox.Alert(msginfo)
            '    Return
            'End If

            'ROInfoError.Visible = True
            cntTypeError.Visible = True
            cntNoError.Visible = True
            cntStartDateError.Visible = True
            servCError.Visible = True
            saveButton.Enabled = True

            'RO Info dropdownlist
            ROInfoDrpList.Items.Clear()
            ROInfoDrpList.Items.Add(New ListItem("", ""))
            For count = 0 To ds.Tables("ROINFO").Rows.Count - 1
                Dim NewItem As New ListItem()
                NewItem.Text = ds.Tables("ROINFO").Rows(count).Item("MODEID").ToString() + "-" + _
                        ds.Tables("ROINFO").Rows(count).Item("SERIALNO").ToString()
                NewItem.Value = ds.Tables("ROINFO").Rows(count).Item("ROID").ToString()
                ROInfoDrpList.Items.Add(NewItem)
            Next
            ROInfoDrpList.SelectedIndex = 0

            'Contract Type
            databonds(ds.Tables("MSV1_FIL"), cntTypeDrpList)
            'cntTypeDrpList.Items.Insert(0, New ListItem("", ""))
            cntTypeDrpList.SelectedIndex = 0

            custNameBox.Text = ds.Tables("MCUS_FIL").Rows(0).Item("MCUS_ENAME").ToString()

            If ds.Tables("MTEL_FIL").Rows.Count > 1 Then
                custContactBox.Text = ds.Tables("MTEL_FIL").Rows(0).Item("MTEL_TELNO").ToString()
            End If

            custIDBox.ReadOnly = True
            custPfBox.ReadOnly = True

            Dim cntEntity As New clsContract
            cntEntity.CustomerID = custIDBox.Text
            cntEntity.CustomerPrifx = custPfBox.Text
            cntEntity.ROID = ROInfoDrpList.SelectedValue
            preCntNoBox.Text = cntEntity.GetPreContractNO(Date.Now)
            servHistorylkBtn.Enabled = True
            contractHistroylkBtn.Enabled = True
        End If
    End Sub

    Private Sub databondsForTechnician(ByVal ds As DataSet, ByVal dropdown As DropDownList, Optional ByVal X As Boolean = True)

        dropdown.Items.Clear()
        Dim row As DataRow

        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            If X = True Then
                NewItem.Text = Trim(row("name")) & "-" & Trim(row("id"))
            Else
                NewItem.Text = Trim(row("name"))
            End If
            NewItem.Value = Trim(row("id"))
            dropdown.Items.Add(NewItem)
        Next

    End Sub

    Public Sub databonds(ByRef dt As DataTable, ByRef dropdown As DropDownList)
        Dim count As Integer = 0
        dropdown.Items.Clear()
        dropdown.Items.Add(New ListItem("", ""))
        Dim row As DataRow
        For Each row In dt.Rows
            Dim NewItem As New ListItem()
            NewItem.Text = row("id") & "-" & row("name")
            NewItem.Value = row("id")
            dropdown.Items.Add(NewItem)
            count = count + 1
        Next
        'If dt.Rows.Count = 0 Then '处理数据表为空的情况
        '    dropdown.Items.Insert(0, New ListItem("", ""))
        'End If
    End Sub

    Protected Sub cntStartDateCalendar_SelectedDateChanged1(ByVal sender As Object, ByVal e As System.EventArgs) Handles cntStartDateCalendar.SelectedDateChanged
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Dim noy As Integer = Val(Request.Form("txtNoOfYear"))
        If noy = 0 Then
            cntEndDateBox.Text = cntStartDateBox.Text
        Else
            Dim enddate As DateTime = Convert.ToDateTime(Trim(cntStartDateBox.Text))
            enddate = enddate.AddYears(noy)
            cntEndDateBox.Text = enddate
        End If
    End Sub

    'Protected Sub servHistorylkBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles servHistorylkBtn.Click
    '    Dim strTempURL As String = "ROID=" + Me.ROInfoDrpList.SelectedValue.ToString() + "&ROINFO=" + ROInfoDrpList.SelectedItem.Text + _
    '                              "&custName=" + custNameBox.Text + "&custID=" + custIDBox.Text + "&custPf=" + custPfBox.Text
    '    strTempURL = "~/PresentationLayer/function/serverHistory.aspx?" + strTempURL
    '    Response.Redirect(strTempURL)
    'End Sub

    'Protected Sub contractHistroylkBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles contractHistroylkBtn.Click
    '    Dim strTempURL As String = "ROID=" + Me.ROInfoDrpList.SelectedValue.ToString() + "&ROINFO=" + ROInfoDrpList.SelectedItem.Text + _
    '                              "&custName=" + custNameBox.Text + "&custID=" + custIDBox.Text + "&custPf=" + custPfBox.Text
    '    strTempURL = "~/PresentationLayer/function/contracthistory.aspx?" + strTempURL
    '    Response.Redirect(strTempURL)
    'End Sub

    'Protected Sub ROInfoDrpList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ROInfoDrpList.SelectedIndexChanged
    '    If ROInfoDrpList.SelectedValue <> "" Then
    '        Dim apptEntity As New clsAppointment
    '        Dim cntEntity As New clsContract
    '        apptEntity.ModeID = ROInfoDrpList.SelectedItem.Text.Split("-")(0).ToString()
    '        apptEntity.SerialNo = ROInfoDrpList.SelectedItem.Text.Split("-")(1).ToString()
    '        apptEntity.ROID = ROInfoDrpList.SelectedItem.Value.ToString()

    '        Dim ds As DataSet = apptEntity.GetROAndContractInfo()
    '        '处理异常***未完成*******
    '        If ds Is Nothing Then
    '            Response.Redirect("~/PresentationLayer/error.aspx")
    '        End If
    '        If ds.Tables("MROU_FIL").Rows.Count = 1 Then
    '            lastServDateBox.Text = ds.Tables("MROU_FIL").Rows(0).Item("MROU_LSVDT").ToString()
    '            installDateBox.Text = ds.Tables("MROU_FIL").Rows(0).Item("MROU_INSDT").ToString()
    '            Session("FUN-AACNT-ROID") = ds.Tables("MROU_FIL").Rows(0).Item("MROU_ROUID").ToString()
    '            nextServDateBox.Text = ds.Tables("MROU_FIL").Rows(0).Item("MROU_NSVDT").ToString()
    '            cntEntity.ROID = Session("FUN-AACNT-ROID")
    '        End If
    '        'servHistorylkBtn.Visible = True
    '        'contractHistroylkBtn.Visible = True
    '    Else
    '        lastServDateBox.Text = ""
    '        installDateBox.Text = ""
    '        Session("FUN-AACNT-ROID") = ""
    '        nextServDateBox.Text = ""
    '        'preCntNoBox.Text = ""
    '        'servHistorylkBtn.Visible = False
    '        'contractHistroylkBtn.Visible = False
    '    End If
    '    cntStartDateBox.Text = Request.Form("cntStartDateBox")
    'End Sub

    'Protected Sub cntTypeDrpList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cntTypeDrpList.SelectedIndexChanged
    '    If cntTypeDrpList.SelectedValue <> "" Then
    '        Dim cntEntity As New clsContract()
    '        cntEntity.ContractType = cntTypeDrpList.SelectedValue
    '        cntEntity.COUNTRY = Session("login_ctryID")
    '        Dim ds As DataSet = cntEntity.GetContractTypeInfo()
    '        If ds Is Nothing Or ds.Tables("MSV1_FIL").Rows.Count <> 1 Then
    '            Response.Redirect("~/PresentationLayer/error.aspx")
    '        End If
    '        Session("FUN-AACNT-NoofYear") = ds.Tables("MSV1_FIL").Rows(0).Item("MSV1_NOYR")
    '        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
    '        Dim enddate As DateTime = Convert.ToDateTime(Trim(Request.Form("cntStartDateBox")))
    '        enddate = enddate.AddYears(Convert.ToInt32(Session("FUN-AACNT-NoofYear")))
    '        enddate = DateAdd(DateInterval.Day, -1, enddate)
    '        cntEndDateBox.Text = enddate
    '        cntDspBox.Text = ds.Tables("MSV1_FIL").Rows(0).Item("MSV1_ENAME")
    '        Session("FUN-AACNT-Frequence") = ds.Tables("MSV1_FIL").Rows(0).Item("MSV1_FREQ")
    '    Else
    '        Session("FUN-AACNT-Frequence") = 0
    '        Session("FUN-AACNT-NoofYear") = 0
    '        cntEndDateBox.Text = Request.Form("cntStartDateBox") 'cntStartDateBox.Text
    '        cntDspBox.Text = ""
    '    End If
    '    cntStartDateBox.Text = Request.Form("cntStartDateBox")
    'End Sub

    Sub SaveContract()
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        Dim frq As Integer = Val(Request.Form("txtFrequency"))
        Dim noy As Integer = Val(Request.Form("txtNoOfYear"))
        'RequestFromValue()

        Dim cntEntity As New clsContract()
        cntEntity.ContractNO = Trim(cntNoBox.Text)
        cntEntity.ROID = Me.ROInfoDrpList.SelectedValue
        cntEntity.CustomerPrifx = Trim(custPfBox.Text)
        cntEntity.CustomerID = Trim(custIDBox.Text)
        cntEntity.ContractType = Trim(cntTypeDrpList.SelectedValue)
        cntEntity.ContractStartDate = Trim(Request.Form("cntStartDateBox")) 'Trim(cntStartDateBox.Text)
        cntEntity.ContractEndDate = Trim(Request.Form("cntEndDateBox")) 'Trim(cntEndDateBox.Text)
        cntEntity.ServerCenterID = Trim(servCDrpList.SelectedValue)
        cntEntity.ContractStatus = Trim("ACTIVE")
        cntEntity.SignedBy = Trim(cboTechnician.SelectedValue)
        cntEntity.SMSReminder = Trim(SMSRemDrpList.SelectedValue)
        cntEntity.ContractNotes = Trim(notesBox.Text)
        cntEntity.SessionID = Session("login_session")
        cntEntity.IpAddress = Request.UserHostAddress.ToString()

        Me.errlab.Visible = True
        If frq > 0 And noy > 0 Then
            If (cntEntity.AddContract(Session("userID"), frq, Convert.ToInt32(noy * 12 / frq))) Then
                BindGrid(Trim(cntNoBox.Text))
                objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                Me.errlab.Text = objXmlTr.GetLabelName("StatusMessage", "FUN-APPTCNT-SUCCESS")
                cntNoBox.ReadOnly = True
                InfoMsgBox.Alert(errlab.Text)
            Else
                objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                Me.errlab.Text = objXmlTr.GetLabelName("StatusMessage", "FUN-APPTCNT-FAILED")
                InfoMsgBox.Alert(errlab.Text)
            End If
        End If
    End Sub
    Protected Sub saveMsgBox_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles saveMsgBox.GetMessageBoxResponse
        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
            SaveContract()
        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
        End If
        cntStartDateBox.Text = Request.Form("cntStartDateBox")
    End Sub
    Private Sub BindGrid(ByVal ContractNo As String)
        Dim cntEntity As New clsContract()
        cntEntity.ContractNO = ContractNo
        Dim ds As DataSet = cntEntity.GetContractServInfo()
        cntServInfoView.DataSource = ds
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        Dim col As CommandField = cntServInfoView.Columns(0)
        col.EditText = objXmlTr.GetLabelName("EngLabelMsg", "BB-Edit") '"edit"
        col.CancelText = objXmlTr.GetLabelName("EngLabelMsg", "BB-Cancel") ' "Cancel"
        col.UpdateText = objXmlTr.GetLabelName("EngLabelMsg", "BB-UPDATE")
        col.HeaderImageUrl = "~/PresentationLayer/graph/edit.gif"
        cntServInfoView.DataBind()
        If cntServInfoView.Rows.Count > 0 Then
            DispGridViewHead()
        End If
    End Sub
    Private Sub DispGridViewHead()
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        cntServInfoView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CHKS-NO") 'NO
        cntServInfoView.HeaderRow.Cells(1).Font.Size = 8
        cntServInfoView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-ACNT-CNTSERVMNTH") 'Service Month
        cntServInfoView.HeaderRow.Cells(2).Font.Size = 8
        cntServInfoView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-ACNT-ACLSERVDATE") 'Actual ServiceDate
        cntServInfoView.HeaderRow.Cells(3).Font.Size = 8
        cntServInfoView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-APPT-TRANSTYPE") 'Transaction Type
        cntServInfoView.HeaderRow.Cells(4).Font.Size = 8
        cntServInfoView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-APPT-TRANSNO") 'Transaction No
        cntServInfoView.HeaderRow.Cells(5).Font.Size = 8
        cntServInfoView.HeaderRow.Cells(6).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CNT-STATUS") 'Status
        cntServInfoView.HeaderRow.Cells(6).Font.Size = 8
    End Sub

    Sub RequestFromValue()
        Dim enddate As DateTime = Convert.ToDateTime(Trim(Request.Form("cntStartDateBox")))
        Dim noy As Integer = Val(Request.Form("txtNoOfYear"))
        enddate = enddate.AddYears(noy)
        enddate = DateAdd(DateInterval.Day, -1, enddate)
        cntEndDateBox.Text = enddate

        installDateBox.Text = Request.Form("installDateBox")
        preCntNoBox.Text = Request.Form("preCntNoBox")

        lastServDateBox.Text = Request.Form("lastServDateBox")
        nextServDateBox.Text = Request.Form("nextServDateBox")

    End Sub

    Protected Sub saveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveButton.Click
        RequestFromValue()
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        Dim ROID As String = Me.ROInfoDrpList.SelectedValue
        Dim PreCNTNo As String = preCntNoBox.Text.Trim
        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim msginfo As String
        Dim msgtitle As String
        Dim cntEntity As New clsContract()
        cntEntity.ContractNO = cntNoBox.Text
        cntEntity.CustomerID = custIDBox.Text
        cntEntity.CustomerPrifx = custPfBox.Text
        cntEntity.ContractType = cntTypeDrpList.SelectedValue
        cntEntity.ContractStartDate = Request.Form("cntStartDateBox") ' cntStartDateBox.Text
        cntEntity.ROID = ROID
        cntEntity.PreContractNo = PreCNTNo
        Dim ValidatorArray As Array = cntEntity.Validator()
        If ValidatorArray Is Nothing Then
            Response.Redirect("~/PresentationLayer/error.aspx")
        End If

        If ValidatorArray(0) = True Then 'Contract No Exists
            msginfo = objXm.GetLabelName("StatusMessage", "FUN-CNT-CNTNOTEXIST")
            InfoMsgBox.Alert(msginfo)
            Return
        End If

        If ValidatorArray(1) = True Then 'Customer can't have different Contract Type
            msginfo = objXm.GetLabelName("StatusMessage", "FUN-CNT-CUSTCNTTYPE")
            InfoMsgBox.Alert(msginfo)
            Return
        End If

        If (ROID <> "" Or ROID <> "0") And ValidatorArray(3) = True Then
            msginfo = objXm.GetLabelName("StatusMessage", "FUN-CNT-STDROINSTALL")
            InfoMsgBox.Alert(msginfo)
            Return
        End If
        If PreCNTNo <> "" And ValidatorArray(4) = True Then
            msginfo = objXm.GetLabelName("StatusMessage", "BB-CNT-STDPRECONTRACT")
            InfoMsgBox.Alert(msginfo)
            Return
        End If
        msginfo = objXm.GetLabelName("StatusMessage", "SAVEORNOT")
        msgtitle = objXm.GetLabelName("StatusMessage", "SAVETITLE")
        'saveMsgBox.Confirm(msginfo)
        SaveContract()
    End Sub

    Protected Sub cntServInfoView_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles cntServInfoView.RowEditing
        cntServInfoView.EditIndex = e.NewEditIndex
        BindGrid(cntNoBox.Text)
        'Dim ActualServDateImg As New ImageButton
        'ActualServDateImg = Me.cntServInfoView.Rows(e.NewEditIndex).FindControl("ActualServDateImg")
        'ActualServDateImg.Enabled = True
        Dim ActualServDateDateCalendar As JCalendar.JCalendar
        ActualServDateDateCalendar = Me.cntServInfoView.Rows(e.NewEditIndex).FindControl("ActualServDateDateCalendar")
        ActualServDateDateCalendar.Enabled = True
        Dim tempBox As New TextBox
        tempBox = Me.cntServInfoView.Rows(e.NewEditIndex).FindControl("tansTypeBox")
        tempBox.ReadOnly = False
        tempBox = Me.cntServInfoView.Rows(e.NewEditIndex).FindControl("tansNoBox")
        tempBox.ReadOnly = False
        Dim cnt2StatusDrpList As New DropDownList()
        cnt2StatusDrpList = Me.cntServInfoView.Rows(e.NewEditIndex).FindControl("cnt2StatusDrpList")
        cnt2StatusDrpList.Enabled = True
    End Sub

    Protected Sub cntServInfoView_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles cntServInfoView.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim current, count As Integer
            Dim cnt2StatusDrpList As DropDownList
            cnt2StatusDrpList = e.Row.FindControl("cnt2StatusDrpList")
            cnt2StatusDrpList.Items.Clear()
            Dim cntStat As New clsUtil()
            Dim statParam As ArrayList = New ArrayList
            statParam = cntStat.searchconfirminf("CNT2Status")
            Dim statid As String
            Dim statnm As String
            current = 0
            For count = 0 To statParam.Count - 1
                statid = statParam.Item(count)
                statnm = statParam.Item(count + 1)
                count = count + 1
                cnt2StatusDrpList.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
            Next
            cnt2StatusDrpList.Enabled = False
            objXmlTr.XmlFile = System.Configuration.ConfigurationManager.AppSettings("StatMsg")
            Dim xCV As CompareValidator = e.Row.FindControl("CompDateVal")
            xCV.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "INVALID_DATE")
        End If
    End Sub
    Protected Sub cntServInfoView_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles cntServInfoView.RowCancelingEdit
        cntServInfoView.EditIndex = -1
        BindGrid(cntNoBox.Text)
    End Sub

    'Protected Sub cntServInfoView_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles cntServInfoView.RowCommand
    '    If e.CommandName = "ImagBtn" Then
    '        Dim ActualServDateDateCalendar As Calendar = cntServInfoView.Rows(cntServInfoView.EditIndex).FindControl("ActualServDateDateCalendar")
    '        ActualServDateDateCalendar.Visible = Not ActualServDateDateCalendar.Visible
    '        ActualServDateDateCalendar.Style.Item("position") = "absolute"
    '        Dim ActualServDateBox As TextBox = cntServInfoView.Rows(cntServInfoView.EditIndex).FindControl("ActualServDateBox")
    '    End If
    'End Sub
    Protected Sub ActualServDateDateCalendar_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        'Dim ActualServDateDateCalendar As Calendar = sender
        Dim ActualServDateDateCalendar As JCalendar.JCalendar = sender
        Dim ActualServDateBox As TextBox = cntServInfoView.Rows(cntServInfoView.EditIndex).FindControl("ActualServDateBox")
        ActualServDateBox.Text = ActualServDateDateCalendar.SelectedDate
        'ActualServDateDateCalendar.Visible = False
    End Sub
    Protected Sub UpdateMsgBox_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles UpdateMsgBox.GetMessageBoxResponse
        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
            Dim cntEntity As New clsContract
            cntEntity.ContractNO = cntNoBox.Text
            Dim MTHYRLab As Label = cntServInfoView.Rows(cntServInfoView.EditIndex).FindControl("MTHYRLab")
            cntEntity.ServiceDate = MTHYRLab.Text
            Dim ActualServDateBox As TextBox = cntServInfoView.Rows(cntServInfoView.EditIndex).FindControl("ActualServDateBox")
            cntEntity.ActualServiceDate = ActualServDateBox.Text
            Dim tansTypeBox As TextBox = cntServInfoView.Rows(cntServInfoView.EditIndex).FindControl("tansTypeBox")
            cntEntity.TransactionType = tansTypeBox.Text
            Dim tansNoBox As TextBox = cntServInfoView.Rows(cntServInfoView.EditIndex).FindControl("tansNoBox")
            cntEntity.TransactionNo = tansNoBox.Text
            Dim cnt2StatusDrpList As New DropDownList()
            cnt2StatusDrpList = cntServInfoView.Rows(cntServInfoView.EditIndex).FindControl("cnt2StatusDrpList")
            cntEntity.CNT2Status = cnt2StatusDrpList.SelectedValue

            cntEntity.IpAddress = Request.UserHostAddress.ToString()
            cntEntity.UserServCenterID = Session("login_svcID")
            cntEntity.SessionID = Session("login_session")

            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            Me.errlab.Visible = True
            If cntEntity.UpdateContractService(Session("userID")) = True Then
                Me.errlab.Text = objXmlTr.GetLabelName("StatusMessage", "FUN-APPTCNT-SUCCESS")
                InfoMsgBox.Alert(errlab.Text)
            Else
                Me.errlab.Text = objXmlTr.GetLabelName("StatusMessage", "FUN-APPTCNT-FAILED")
                InfoMsgBox.Alert(errlab.Text)
            End If
            cntServInfoView.EditIndex = -1
            BindGrid(cntNoBox.Text)
        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
        End If
    End Sub

    Protected Sub cntServInfoView_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles cntServInfoView.RowUpdating
        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim msginfo As String
        Dim msgtitle As String
        msginfo = objXm.GetLabelName("StatusMessage", "SAVEORNOT")
        msgtitle = objXm.GetLabelName("StatusMessage", "SAVETITLE")
        UpdateMsgBox.Confirm(msginfo)
    End Sub


#Region "AJAX"


    <AjaxPro.AjaxMethod()> _
    Function AjaxChangeRO(ByVal ROUID As String, ByVal CustomerPrefix As String, ByVal CustomerID As String, ByVal CreateDate As String) As ArrayList
        Dim arrlst As New ArrayList
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        If ROUID <> "" Then
            Dim apptEntity As New clsAppointment


            apptEntity.ROID = ROUID

            Dim ds As DataSet = apptEntity.GetROAndContractInfo()
            '处理异常***未完成*******
            If ds Is Nothing Then
                Response.Redirect("~/PresentationLayer/error.aspx")
            End If
            If ds.Tables("MROU_FIL").Rows.Count = 1 Then
                arrlst.Add(ds.Tables("MROU_FIL").Rows(0).Item("MROU_LSVDT").ToString())
                arrlst.Add(ds.Tables("MROU_FIL").Rows(0).Item("MROU_INSDT").ToString())
                arrlst.Add(ds.Tables("MROU_FIL").Rows(0).Item("MROU_ROUID").ToString())
                arrlst.Add(ds.Tables("MROU_FIL").Rows(0).Item("MROU_NSVDT").ToString())
            Else
                arrlst.Add("")
                arrlst.Add("")
                arrlst.Add("")
                arrlst.Add("")
            End If
        Else
            arrlst.Add("")
            arrlst.Add("")
            arrlst.Add("")
            arrlst.Add("")
        End If

        If CreateDate = "" Then CreateDate = Date.Today

        Dim cntEntity As New clsContract
        cntEntity.CustomerID = CustomerID
        cntEntity.CustomerPrifx = CustomerPrefix
        cntEntity.ROID = ROUID
        arrlst.Add(cntEntity.GetPreContractNO(CreateDate))

        Return arrlst
    End Function

    <AjaxPro.AjaxMethod()> _
    Function ContractChange(ByVal Country As String, ByVal ContractType As String, ByVal StartDate As String) As ArrayList
        Dim arrlist As New ArrayList
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        If ContractType <> "" Then
            Dim cntEntity As New clsContract()
            Dim NOY As Integer

            cntEntity.ContractType = ContractType
            cntEntity.COUNTRY = Country

            Dim ds As DataSet = cntEntity.GetContractTypeInfo()

            If ds Is Nothing Or ds.Tables("MSV1_FIL").Rows.Count <> 1 Then
                Response.Redirect("~/PresentationLayer/error.aspx")
            End If

            NOY = ds.Tables("MSV1_FIL").Rows(0).Item("MSV1_NOYR")
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

            If StartDate = "" Then StartDate = CStr(Now())

            Dim enddate As DateTime = CDate(StartDate)
            enddate = DateAdd(DateInterval.Year, NOY, CDate(StartDate))
            enddate = DateAdd(DateInterval.Day, -1, enddate)

            arrlist.Add(ds.Tables("MSV1_FIL").Rows(0).Item("MSV1_FREQ"))
            arrlist.Add(NOY)
            arrlist.Add(Format(enddate, "dd/MM/yyyy"))
            arrlist.Add(ds.Tables("MSV1_FIL").Rows(0).Item("MSV1_ENAME"))

        Else

            arrlist.Add("0")  'FRQ
            arrlist.Add("0") 'NOY
            arrlist.Add(StartDate) 'ENDDT
            arrlist.Add("") 'CONNM

        End If

        Return arrlist
    End Function

    <AjaxPro.AjaxMethod()> _
    Function AjaxComputeEndDate(ByVal StartDate As String, ByVal NOY As String) As String
        Dim EndDate As Date
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        If IsDate(StartDate) = True Then
            EndDate = DateAdd(DateInterval.Year, Val(NOY), CDate(StartDate))
            EndDate = DateAdd(DateInterval.Day, -1, EndDate)
        Else
            EndDate = Now()
        End If
        Return Format(EndDate, "dd/MM/yyyy")
    End Function


#End Region


End Class
