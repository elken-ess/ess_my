<%@ Page Language="VB" AutoEventWireup="false" EnableEventValidation="false" CodeFile="reminder.aspx.vb" Inherits="PresentationLayer_function_reminder" %>
<%@ Register Assembly="JCalendar" Namespace="JCalendar" TagPrefix="cc1" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Reminder Slip Generation</title>
    <link href="../css/style.css" type="text/css" rel="stylesheet">
    <meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
    <link href="../css/style.css" type="text/css" rel="stylesheet" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <script language="JavaScript" src="../js/common.js"></script>   
       
     <!--LW Added--> 
     <script language="javascript">  
    
        function LoadRaces_CallBack(response){
 

         //if the server-side code threw an exception
         if (response.error != null)
         {
          //we should probably do better than this
          alert(response.error); 
          
          return;
         }

         var ResponseValue = response.value;
         
         //if the response wasn't what we expected  
         if (ResponseValue == null || typeof(ResponseValue) != "object")
         {       
          return;
         }
              
         //Get the states drop down
         var ResultList = document.getElementById("<%=RacesDLL.ClientID%>");
         ResultList.options.length = 0; //reset the states dropdown
         
         //Remember, its length not Length in JavaScript
         for (var i = 0; i < ResponseValue.length; ++i)
         {
         
          //the columns of our rows are exposed like named properties
          var al = ResponseValue[i].split(":");
          var alid = al[0];
          var alname = al[1];
          ResultList.options[ResultList.options.length] =  new Option(alname, alid);
         }
     
        }
     
        function LoadRaces(ObjectClient)
        {
             var ObjectId = ObjectClient.options[ObjectClient.selectedIndex].value;
              
             PresentationLayer_function_reminder.GetRaces(ObjectId, LoadRaces_CallBack); 
        }
    </script>
         
</head>
<body>
    <form id="form1" method="post" enctype="multipart/form-data" runat="server">
        <div style="vertical-align: top">
            <table width="100%" border="0">
                <tr>
                    <td style="height: 40px">
                        <table cellspacing="0" cellpadding="0" width="100%" border="0">
                            <tr>
                                <td width="1%" background="../graph/title_bg.gif">
                                    <img height="24" src="../graph/title1.gif" width="5"></td>
                                <td class="style2" width="98%" background="../graph/title_bg.gif" style="width: 80%">
                                    <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="right" width="1%" background="../graph/title_bg.gif">
                                    <img height="24" src="../graph/title_2.gif" width="5"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table style="width: 100%" cellspacing="0" cellpadding="0" width="100%" border="0">
                            <tr>
                                <td class="style2" background="../graph/title_bg.gif" style="width: 110%">
                                    <font color="red" style="width: 100%">
                                    <asp:Label ID="errlab" runat="server" Text="Label" Visible="false"></asp:Label></font>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="1" cellpadding="2" bgcolor="#b7e6e6" border="0" style="width: 100%">
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width: 20%">
                                    <asp:Label ID="fModeTypeLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 30%;">
                                    <asp:TextBox ID="fModeTypeBox" Width="80%" runat="server" CssClass="textborder" MaxLength="10"></asp:TextBox></td>
                                <td align="right" style="width: 20%">
                                    <asp:Label ID="tModeTypeLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 30%;">
                                    <asp:TextBox ID="tModeTypeBox" Width="80%" runat="server" CssClass="textborder" MaxLength="10"></asp:TextBox></td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width: 20%">
                                    <asp:Label ID="fStatIDLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 30%;">
                                    <asp:TextBox ID="fStatIDBox" Width="80%" runat="server" CssClass="textborder" MaxLength="10"></asp:TextBox></td>
                                <td align="right" style="width: 20%">
                                    <asp:Label ID="tStatIDLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 30%;">
                                    <asp:TextBox ID="tStatIDBox" Width="80%" runat="server" CssClass="textborder" MaxLength="10"></asp:TextBox></td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width: 20%">
                                    <asp:Label ID="fAreaIDLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 30%;">
                                    <asp:TextBox ID="fAreaIDBox" Width="80%" runat="server" CssClass="textborder" MaxLength="10"></asp:TextBox></td>
                                <td align="right" style="width: 20%">
                                    <asp:Label ID="tAreaIDLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 30%;">
                                    <asp:TextBox ID="tAreaIDBox" Width="80%" runat="server" CssClass="textborder" MaxLength="10"></asp:TextBox></td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width: 20%">
                                    <asp:Label ID="fSvcIDLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 30%;">
                                    <asp:TextBox ID="fSvcIDBox" Width="80%" runat="server" CssClass="textborder" MaxLength="10"></asp:TextBox></td>
                                <td align="right" style="width: 20%">
                                    <asp:Label ID="tSvcIDLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 30%;">
                                    <asp:TextBox ID="tSvcIDBox" Width="80%" runat="server" CssClass="textborder" MaxLength="10"></asp:TextBox></td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width: 20%;">
                                    <asp:Label ID="fDueDateLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 30%;">
                                    <asp:TextBox ID="fDueDateBox" Width="60%" runat="server" CssClass="textborder" AutoPostBack="False"
                                        ReadOnly="false" MaxLength =10></asp:TextBox>
                                   
                                    <asp:HyperLink ID="HypCalFrom" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif" ToolTip="Choose a Date">Choose a Date</asp:HyperLink>

                                    <cc1:JCalendar ID="fDueDateCalendar" runat="server" ImgURL="~/PresentationLayer/graph/calendar.gif" ControlToAssign="fDueDateBox" Visible =false />
                                </td>
                                <td align="right" style="width: 20%;">
                                    <asp:Label ID="tDueDateLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 30%;">
                                    <asp:TextBox ID="tDueDateBox" Width="60%" runat="server" CssClass="textborder" AutoPostBack="False"
                                        ReadOnly="false" MaxLength =10></asp:TextBox>
                                    
                                    <asp:HyperLink ID="HypCalTo" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif" ToolTip="Choose a Date">Choose a Date</asp:HyperLink>
                                    <cc1:JCalendar ID="tDueDateCalendar" runat="server" ImgURL="~/PresentationLayer/graph/calendar.gif" ControlToAssign="tDueDateBox" Visible =false />
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width: 20%;">
                                    <asp:Label ID="CustomerTypeLab" runat="server" Text="Label"></asp:Label>
                                </td>
                                <td align="left" style="width: 30%;">
                                    <asp:DropDownList ID="CustomerTypeDLL" runat="server" CssClass="textborder" Width="81%" AutoPostBack="false" onchange="LoadRaces(this)">
                                    </asp:DropDownList>
                                </td>
                                 <td align="right" style="width: 20%;">
                                    <asp:Label ID="prefLangLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 30%;">
                                 <asp:DropDownList ID="prefLangDLL" runat="server" AutoPostBack="False" CssClass="textborder" Width="81%">
                                        </asp:DropDownList></td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width: 20%;">
                                    <asp:Label ID="RacesLab" runat="server" Text="Label"></asp:Label>
                                </td>
                                <td align="left" colspan = "3">
                                    <asp:DropDownList ID="RacesDLL" runat="server" CssClass="textborder" Width="30%">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            
                        </table>
                        <asp:CheckBox ID="IncludeCHKBOX" runat="server" />
                        <asp:LinkButton ID="ViewReportBtn" runat="server"></asp:LinkButton>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="1" cellpadding="2" bgcolor="#b7e6e6" border="0" style="width: 100%">
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width: 20%">
                                    <asp:Label ID="totalCusLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 30%">
                                    <asp:TextBox ID="totalCusBox" Width="80%" runat="server" CssClass="textborder" ReadOnly="True"></asp:TextBox></td>
                                <td style="width: 20%">
                                    <asp:LinkButton ID="OutPutTextBtn" runat="server"></asp:LinkButton></td>
                                <td style="width: 30%">
                                    <asp:LinkButton ID="GeneratePdfBtn" runat="server"></asp:LinkButton></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="1" cellpadding="2" bgcolor="#b7e6e6" border="0" style="width: 100%">
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width: 20%">
                                    <asp:Label ID="UploadLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 60%">
                                    <input id="UploadFile" type="file" style="width: 95%" runat="server" /></td>
                                <td style="width: 20%">
                                    <asp:LinkButton ID="UploadBtn" runat="server"></asp:LinkButton></td>
                            </tr>
                        </table>                        
                    </td>
                </tr>
             
            </table>
            &nbsp;<CR:CrystalReportViewer ID="ReminderReportViewer" runat="server" AutoDataBind="true"
                DisplayGroupTree="False" />
        </div>
    </form>
</body>
</html>
