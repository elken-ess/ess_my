<%@ Page Language="VB" AutoEventWireup="false" CodeFile="modifyPaymentExport.aspx.vb" Inherits="PresentationLayer_function_modifyPaymentExport_aspx" EnableEventValidation="false"%>

<%@ Register Assembly="JCalendar" Namespace="JCalendar" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Modify Payment Export</title>
    <link href="../css/style.css" type="text/css" rel="stylesheet">
    <script language="JavaScript" src="../js/common.js"></script>
</head>
<body>
    <form id="form2" runat="server">
        <div style="vertical-align: top">
            <table style="width: 100%" border="0">
                <tr>
                    <td style="width: 1068px; height: 7px;">
                        <table cellspacing="0" cellpadding="0" width="100%" border="0">
                            <tr>
                                <td width="1%" background="../graph/title_bg.gif">
                                    <img height="24" src="../graph/title1.gif" width="5" /></td>
                                <td class="style2" width="98%" background="../graph/title_bg.gif" style="width: 80%">
                                    <asp:Label ID="titleLab" runat="server" Text="Payment Export - Modify"></asp:Label></td>
                                <td align="LEFT" width="1%" background="../graph/title_bg.gif">
                                    <img height="24" src="../graph/title_2.gif" width="5" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="width: 1068px; height: 300px;">
                        <table cellspacing="1" cellpadding="2" bgcolor="#b7e6e6" border="0" style="width: 100%">
                            <tr bgcolor="#ffffff">
                                <td colspan="5">
                                    <asp:Label ID="lblNoteUP" runat="server" ForeColor="Red" Text="Label" Visible="False"></asp:Label>
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="LEFT" style="width: 20%">
                                    <asp:Label ID="lblContractID" runat="server" Text="Contract ID"></asp:Label></td>
                                <td align="left" style="width: 25%;">
                                    <asp:TextBox ID="txtContractID" runat="server" CssClass="textborder" Width="80%" ReadOnly="True" Enabled="False" EnableTheming="False"></asp:TextBox></td>
                                <td align="LEFT" style="width: 20%">
                                    <asp:Label ID="lblInstalledBasedID" runat="server" Text="InstalledBased ID"></asp:Label></td>
                                <td align="left" style="width: 25%;">
                                    <asp:TextBox ID="txtInstalledBasedID" runat="server" CssClass="textborder" Width="80%" ReadOnly="True" Enabled="False"></asp:TextBox>
                                </td>
                                <td style="width: 10%">
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="LEFT" style="width: 20%;">
                                    <asp:Label ID="lblCountry" runat="server" Text="Country"></asp:Label></td>
                                <td align="left" style="width: 25%;">
                                    <asp:TextBox ID="txtCountry" runat="server" CssClass="textborder" Enabled="False" EnableTheming="False"
                                        ReadOnly="True" Width="80%"></asp:TextBox></td>
                                <td align="LEFT" style="width: 20%">
                                    <asp:Label ID="lblSerialNumber" runat="server" Text="Serial Number"></asp:Label></td>
                                <td align="left" style="width: 25%;">
                                    <asp:TextBox ID="txtSerialNumber" runat="server" CssClass="textborder" Enabled="False"
                                        ReadOnly="True" Width="80%"></asp:TextBox></td>
                                <td style="width: 10%">
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="LEFT" style="width: 20%">
                                    <asp:Label ID="lblCustomerID" runat="server" Text="Customer ID"></asp:Label></td>
                                <td align="left" style="width: 25%">
                                    <asp:TextBox ID="txtCustomerID" Width="80%" runat="server" CssClass="textborder"
                                        ReadOnly="True" Enabled="False"></asp:TextBox></td>
                                <td align="LEFT" style="width: 20%;">
                                    <asp:Label ID="lblCustomerName" runat="server" Text="Customer Name"></asp:Label></td>
                                <td align="left" style="width: 25%;">
                                    <asp:TextBox ID="txtCustomerName" runat="server" Width="80%" CssClass="textborder" ReadOnly="True" Enabled="False"></asp:TextBox></td>
                                <td style="width: 10%">
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="LEFT" style="width: 20%;">
                                    <asp:Label ID="lblCCName" runat="server" Text="Card Holder's Name"></asp:Label></td>
                                <td align="left" style="width: 25%;">
                                    <asp:TextBox ID="txtCCName" runat="server" Width="80%" CssClass="textborder" MaxLength="50"></asp:TextBox></td>
                                <td align="LEFT" style="width: 20%">
                                    <asp:Label ID="lblCCNumber" runat="server" Text="Credit Card No"></asp:Label></td>
                                <td align="left" style="width: 25%;">
                                    <asp:TextBox ID="txtCCNumber" runat="server" Width="80%" CssClass="textborder"
                                        MaxLength="20"></asp:TextBox>
                                    &nbsp;
                                </td>
                                <td style="width: 10%">
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="LEFT" style="width: 20%">
                                    </td>
                                <td align="left" style="width: 25%">
                                    </td>
                                <td align="LEFT" style="width: 20%;">
                                    <asp:Label ID="lblCCExpiry" runat="server" Text="Credit Card Expiry" style="visibility:hidden;"></asp:Label></td>
                                <td align="left" style="width: 25%;">
                                    <asp:TextBox ID="txtCCExpiry" runat="server" Width="80%" CssClass="textborder" ReadOnly="True" Enabled="False"></asp:TextBox>
                                </td>
                                <td style="width: 10%;">
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="LEFT" style="width: 20%;">
                                    <asp:Label ID="lblPaymentMonth" runat="server" Text="Payment Month"></asp:Label></td>
                                <td align="left" style="width: 25%;">
                                    <asp:TextBox ID="txtPaymentMonth" runat="server" Width="80%" CssClass="textborder" ReadOnly="True" Enabled="False"></asp:TextBox></td>
                                <td align="LEFT" style="width: 20%;">
                                    <asp:Label ID="lblPaymentDueDate" runat="server" Text="Payment Due Date"></asp:Label></td>
                                <td align="left" style="width: 25%;">
                                    <asp:TextBox ID="txtPaymentDueDate" runat="server" Width="80%" CssClass="textborder" ReadOnly="True" Enabled="False"></asp:TextBox></td>
                                <td style="width: 10%">
                                </td>
                            </tr>
                        </table>
                        <table cellspacing="1" cellpadding="2" bgcolor="#b7e6e6" border="0" style="width: 100%">
                            <tr bgcolor="#ffffff" style="width: 100%">
                                <td align="LEFT" style="width: 20%;">
                                    <asp:Label ID="lblStatus" runat="server" Text="Status"></asp:Label>
                                </td>
                                <td style="width: 80%;">
                                    <asp:DropDownList ID="ddlStatus" runat="server">
                                        <asp:ListItem>OUTSTANDING</asp:ListItem>
                                        <asp:ListItem>PAID</asp:ListItem>
                                        <asp:ListItem>REJECTED</asp:ListItem>
                                    </asp:DropDownList></td>
                            </tr>
                            <tr bgcolor="#ffffff" style="width: 100%">
                                <td align="LEFT" style="width: 20%; height: 30px;">
                                    <asp:Label ID="lblRemarks" runat="server" Text="Remarks"></asp:Label>
                                </td>
                                <td style="width: 80%;">
                                    <asp:TextBox ID="txtRemarks" runat="server" CssClass="textborder" Height="85px" Width="95%"
                                        MaxLength="400" TextMode="MultiLine"></asp:TextBox></td>
                            </tr>
                        </table>
                        <table cellspacing="1" cellpadding="2" bgcolor="#b7e6e6" border="0" style="width: 100%">
                            <tr bgcolor="#ffffff">
                                <td align="LEFT" style="width: 20%;" id="#ApptLoc">
                                    <asp:Label ID="lblActualPaymentDate" runat="server" Text="Actual Payment Date"></asp:Label></td>
                                <td align="left" style="width: 25%;">
                                    <asp:TextBox ID="txtActualPaymentDate" runat="server" CssClass="textborder" Height="23px"
                                        MaxLength="10" ReadOnly="false" Width="70%"></asp:TextBox>
                                        <asp:HyperLink ID="HypCal" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif" ToolTip="Choose a Date" EnableTheming="True" Visible="False">Choose a Date</asp:HyperLink>
                                    
                                    <cc1:JCalendar ID="apptStartDateJCalendar" runat="server" ImgURL="~/PresentationLayer/graph/calendar.gif" ControlToAssign="txtActualPaymentDate"/>
                                <td align="LEFT" style="width: 20%;">
                                    <asp:Label ID="lblInvoiceNumber" runat="server" Text="Invoice No"></asp:Label></td>
                                <td align="left" style="width: 25%;">
                                    <asp:TextBox ID="txtInvoiceNumber" Width="80%" runat="server" CssClass="textborder" ReadOnly="true" Enabled="False"></asp:TextBox></td>
                                <td style="width: 10%">
                                    <asp:LinkButton ID="LinkButton1" runat="server">Print Invoice</asp:LinkButton></td>
                            </tr>
                            <!--SMR1711-2286 Add Nature of feedback-->
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="width: 1068px">
                    </td>
                </tr>
                <tr>
                    <td style="width: 1068px">
                        <asp:LinkButton ID="saveButton" runat="server" OnClientClick="return confirm('Are you sure?');">Save</asp:LinkButton>&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:HyperLink ID="cancelLink" runat="server" NavigateUrl="~/PresentationLayer/function/PaymentExport.aspx">Cancel</asp:HyperLink>
                    </td>
                </tr>
                <tr bgcolor="#ffffff">
                    <td colspan="4">
                        &nbsp;
                    </td>
                </tr>
                <tr bgcolor="#ffffff">
                    <td colspan="4">
                        <asp:Label ID="lblNoteDown" runat="server" ForeColor="Red" Text="Label" Visible="False"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
            ShowSummary="False" />
        &nbsp;&nbsp;
    </form>
</body>
</html>
