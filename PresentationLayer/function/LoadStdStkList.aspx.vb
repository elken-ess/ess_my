﻿Imports System.IO
Imports System.Data
Imports BusinessEntity

Partial Class PresentationLayer_function_test
    Inherits System.Web.UI.Page
    Dim objXmlTr As New clsXml()
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try
        End If
        objXmlTr.XmlFile = ConfigurationManager.AppSettings("XmlFilePath")
        Me.lbtnLoad.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_F_LOAD")
        Me.lbtnback.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-BACK")
        Me.titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_F_TITLE")
        Me.lbtnback.PostBackUrl = "~/PresentationLayer/function/StdStockList.aspx"
    End Sub

    Protected Sub lbtnLoad_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnLoad.Click

        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        If String.IsNullOrEmpty(Me.FileUpload1.PostedFile.FileName) Then
            Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-F-Help9")
            Return
        End If
        If Me.FileUpload1.PostedFile.ContentType <> "text/plain" Then
            Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-F-Warning7")
            Return
        End If

        Dim err As Integer = 0
        Dim stdstklist As clsStdStockList = Session.Contents("stdstocklist")
        Dim params As New ArrayList(), errlist As New ArrayList()
        Dim FileStr As Stream = Me.FileUpload1.PostedFile.InputStream
        Dim reader As StreamReader = New StreamReader(FileStr)
        Dim linecount As Integer = 0
        Try

            Dim strLine As String = reader.ReadLine()
            While strLine IsNot Nothing
                Dim strs As String() = strLine.Split(" ")
                params.Clear()
                For Each str1 As String In strs
                    Dim str2 As String = str1.Trim()
                    If str1.CompareTo(" ") > 0 Then
                        params.Add(str2)
                    End If
                Next
                If strLine IsNot Nothing Then
                    If stdstklist.InsertIntoDB(params, Session("Res")) <> 1 Then
                        errlist.Add(params(0))
                    End If
                End If
                strLine = reader.ReadLine()
                'linecount = linecount + 1
            End While
        Catch ex As Exception
            Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-F-Error6")
            err = 1
        Finally
            FileStr.Close()
            reader.Close()
        End Try

        If err = 1 Then
            Return
        End If

        If errlist.Count > 0 Then
            Dim errstr As String = ""
            For i As Integer = 0 To errlist.Count - 1
                errstr += (errlist.Item(i) + ",")
            Next
            Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-F-Error5") + errstr
        Else
            Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-F-Success4")
        End If
    End Sub

End Class
