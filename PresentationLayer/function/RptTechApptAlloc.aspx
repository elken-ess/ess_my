﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="RptTechApptAlloc.aspx.vb" Inherits="PresentationLayer_function_RptTechApptAlloc" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Appointment Allocation</title>
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table cellspacing="0" cellpadding="0" width="100%" border="0">
		<tr>
		  <td width="1%" background="../graph/title_bg.gif"><img height="24" src="../graph/title1.gif" width="5"></td>
		  <td class="style2" width="98%" background="../graph/title_bg.gif">
             <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
		  <td align="right" width="1%" background="../graph/title_bg.gif"><img height="24" src="../graph/title_2.gif" width="5"></td>
		</tr>
		<tr><td></td> 
		  <td>
		   <asp:Label ID="lblMessage" runat="server" ForeColor="red" Text=""></asp:Label>
		   </td> 
	   </tr>
	 </table>
     <table style="width: 100%">
            <tr align="center">
                <td style="width: 100%; text-align: left;">
                    <CR:CrystalReportViewer ID="rptTechApptAllocViewer" displaygrid="True" runat="server" AutoDataBind="true" HasDrillUpButton="False" HasExportButton="False" HasToggleGroupTreeButton="False" Width="100%" EnableDrillDown="False" HasSearchButton="False" HasViewList="False" HasZoomFactorList="False" style="position: static" />
                </td>
            </tr>
        </table>
    
    </div>
        <br/>
        <asp:LinkButton ID="backLink" runat="server">LinkButton</asp:LinkButton>
    </form>
</body>
</html>
