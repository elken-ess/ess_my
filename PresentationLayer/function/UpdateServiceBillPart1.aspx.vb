Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Partial Class PresentationLayer_Function_UpdateServiceBillPart1
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim script As String = "top.location='../logon.aspx';"
        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
                'Response.Redirect("~/PresentationLayer/logon.aspx")
            End If
        Catch ex As Exception
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try

        '''access control'''''''''''''''''''''''''''''''''''''''''''''''
        Dim accessgroup As String = Session("accessgroup").ToString
        Dim purviewArray As Array = New Boolean() {False, False, False, False}
        purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "30")

        Me.LnkAdd.Enabled = purviewArray(0)

        If (Not Page.IsPostBack) Then
            txtStartDate.Text = DateAdd(DateInterval.Day, -1, System.DateTime.Today).ToString("dd/MM/yyyy")
            txtEndDate.Text = System.DateTime.Today.ToString("dd/MM/yyyy")
            BindGrid()
        End If

        HypCal1.NavigateUrl = "javascript:DoCal(document.form1.txtStartDate);"
        HypCal2.NavigateUrl = "javascript:DoCal(document.form1.txtEndDate);"
    End Sub
    Protected Sub BindGrid()
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        solab.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-INVOICENO")
        phnolab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-customer-02")
        technicianLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-05")
        CNlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-customer-03")
        snlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-customer-04")
        searchbtn.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-SEARCH")
        titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-update1")
        ctryStat.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCTRY-0004")
        lnkAdd.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Add")

        Me.lblAppointmentNo.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-APPOINMENTNO")
        Me.lblCustomerID.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTMDSL-0001")
        Me.lblManualServiceBillNo.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-SERVICEBILLNO")



        '--------------------------------------------------------------------
        Dim cntryStat As New clsUtil()
        Dim statParam As ArrayList = New ArrayList
        statParam = cntryStat.searchconfirminf("SerStat")
        Dim count As Integer
        Dim statid As String
        Dim statnm As String
        For count = 0 To statParam.Count - 1
            statid = statParam.Item(count)
            statnm = statParam.Item(count + 1)
            count = count + 1
            If UCase(statid) = "BL" Or UCase(statid) = "UA" Or UCase(statid) = "AL" Or UCase(statid) = "UM" Then
                ctryStatDrop.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
            End If

        Next
        ctryStatDrop.Items(0).Selected = True
        '---------------------------------------------------------------------------
        'Dim modelty As New clsUpdateService1()
        ''---读取传参数传给类中的方法
        'modelty.SOID = soText.Text
        'modelty.CustomerName = CNText.Text
        'modelty.PhoneNo = phnoText.Text
        'modelty.SerialNo = snText.Text
        'modelty.Technician = technicianList.Text
        'modelty.STATUS = ctryStatDrop.Text

        'modelty.Country = Session("login_ctryID")
        'modelty.ModifyBy = Session("userID")

        ''以下显示technician
        'Dim ds As New DataSet
        'ds = modelty.chaxun()
        'Dim i As Integer
        'For i = 0 To ds.Tables(1).Rows.Count - 1
        '    technicianList.Items.Add(ds.Tables(1).Rows(i).Item(0).ToString)
        'Next
        'technicianList.Items.Insert(0, "")

        'technicianList.Items(0).Selected = True
        'Dim tech As New DataSet
        'Dim installbond As New clsCommonClass
        'installbond.sparea = Session("login_svcID")
        'installbond.spctr = Session("login_ctryID")
        'installbond.spstat = Session("login_cmpID")
        'installbond.rank = Session("login_rank")

        'tech = installbond.Getcomidname("BB_MASTECH_IDNAME")
        'If tech.Tables.Count <> 0 Then
        '    databonds(tech, technicianList)
        'End If

        '---------------------------------------------------------------------------------------------- Get Service Center List -- Tomas 20111214
        Dim SVC As New DataSet
        Dim installSVC As New clsCommonClass
        installSVC.sparea = Session("login_svcID")
        installSVC.spctr = Session("login_ctryID")
        installSVC.spstat = Session("login_cmpID")
        installSVC.rank = Session("login_rank")

        SVC = installSVC.Getcomidname("BB_TOMAS_SVC")
        If SVC.Tables.Count <> 0 Then
            svcbonds(SVC, SVClist)
        End If

        '----------------------------------------------------------------------------------------------
        'Dim objServiceBillPart1 As New clsUpdateService1()
        'Dim partall As DataSet = objServiceBillPart1.chaxun()
        'partView.DataSource = partall
        'Session("partView") = partall
        partView.AllowPaging = True
        partView.AllowSorting = True

        Dim editcol As New CommandField
        editcol.EditText = objXmlTr.GetLabelName("EngLabelMsg", "BB-Edit") '"edit"
        editcol.HeaderImageUrl = "~/PresentationLayer/graph/edit.gif"
        editcol.ShowEditButton = True
        partView.Columns.Add(editcol)

        Dim accessgroup As String = Session("accessgroup").ToString
        Dim purviewArray As Array = New Boolean() {False, False, False, False}
        purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "30")

        editcol.Visible = purviewArray(2)


        'partView.DataBind()
        DisplayGridHeader()
        'populategrid()


    End Sub

    Protected Sub searchbtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles searchbtn.Click
        populategrid()
    End Sub

    Private Sub populategrid()
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Dim objServiceBillPart1 As New clsUpdateService1()
        Dim telepass As New clsCommonClass
        Dim lstrStartDate, lstrEndDate As String

        lstrStartDate = Me.txtStartDate.Text
        lstrEndDate = Me.txtEndDate.Text


        ''---------------------
        'Dim part1Entity As New clsUpdateService1()
        'If (Trim(soText.Text) <> "") Then
        objServiceBillPart1.SOID = soText.Text
        'End If
        'If (Trim(CNText.Text) <> "") Then
        objServiceBillPart1.CustomerName = CNText.Text
        'End If
        If (Trim(phnoText.Text) <> "") Then
            objServiceBillPart1.PhoneNo = telepass.telconvertpass(phnoText.Text)
        Else
            objServiceBillPart1.PhoneNo = ""
        End If
        'If (Trim(snText.Text) <> "") Then
        objServiceBillPart1.SerialNo = snText.Text
        'End If
        'If (technicianList.SelectedValue().ToString() <> "") Then
        ' Modified by Ryan Estandarte 15 Feb 2012
        'objServiceBillPart1.Technician = technicianList.SelectedItem.Value.ToString()
        objServiceBillPart1.Technician = technicianList.SelectedValue '''''''''''''''''''''''''''' added: tomas 20120215

        objServiceBillPart1.Remark = SVClist.SelectedItem.Value.ToString() '''''''''''''''''''''''''''' added: tomas 20120215

        objServiceBillPart1.AppointmentFix = cboServiceBillType.SelectedItem.Value.ToString() '''''''''''''''''''''''''''' added: tomas 20120215


        'End If
        'If (ctryStatDrop.SelectedValue().ToString() <> "") Then
        If (ctryStatDrop.Text = "AL") Then
            objServiceBillPart1.STATUS = ""
        Else
            objServiceBillPart1.STATUS = ctryStatDrop.SelectedItem.Value.ToString()
        End If
        'objServiceBillPart1.STATUS = ctryStatDrop.SelectedItem.Value.ToString()
        'End If

        objServiceBillPart1.Country = Session("login_ctryID")
        objServiceBillPart1.ModifyBy = Session("userID")

        objServiceBillPart1.CustomerID = Me.txtCustomerID.Text
        objServiceBillPart1.CustomerPrefix = Session("login_ctryid")
        objServiceBillPart1.AppointmentPrefix = Me.txtAppointmentPrefix.Text
        objServiceBillPart1.AppointmentNo = Me.txtAppointmentNo.Text
        objServiceBillPart1.ManualServiceBillNo = Me.txtManualServiceBillNo.Text

        If Not IsDate(lstrStartDate) And lstrStartDate <> "" Then
            partView.Visible = False
            Exit Sub
        End If

        If Not IsDate(lstrEndDate) And lstrEndDate <> "" Then
            partView.Visible = False
            Exit Sub
        End If

        If lstrStartDate <> "" Then
            lstrStartDate = convertDbDate(lstrStartDate)
        Else
            lstrStartDate = ""
        End If

        If lstrEndDate <> "" Then
            lstrEndDate = convertDbDate(lstrEndDate)
        Else
            lstrEndDate = ""
        End If

        objServiceBillPart1.StartBillDate = lstrStartDate
        objServiceBillPart1.EndBillDate = lstrEndDate
        objServiceBillPart1.RMSNo = RMStxt.Text

        Dim selDS As DataSet = objServiceBillPart1.chaxun()
        partView.DataSource = selDS
        partView.DataBind()
        'Session("partView") = selDS
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        If selDS.Tables(0).Rows.Count <> 0 Then
            DisplayGridHeader()
            Me.lblError.Text = ""
        Else
            'BB-View-Fail
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            Me.lblError.Text = objXmlTr.GetLabelName("StatusMessage", "BB-View-Fail")
        End If
    End Sub

    Private Function convertDbDate(ByVal strDate As String)
        Dim strDay As String
        Dim strMonth As String
        Dim strYear As String

        strDay = strDate.Substring(0, 2)
        strMonth = strDate.Substring(3, 2)
        strYear = strDate.Substring(6, 4)

        Return (strYear + "-" + strMonth + "-" + strDay) 'yyyy-MM-dd
    End Function

    Sub DisplayGridHeader()
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        If partView.Rows.Count <> 0 Then
            partView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-08")
            partView.HeaderRow.Cells(1).Font.Size = 8
            partView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-INVOICENO")
            partView.HeaderRow.Cells(2).Font.Size = 8
            partView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-05")
            partView.HeaderRow.Cells(3).Font.Size = 8
            partView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-03")
            partView.HeaderRow.Cells(4).Font.Size = 8
            partView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCTRY-0004")
            partView.HeaderRow.Cells(5).Font.Size = 8
            partView.HeaderRow.Cells(6).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-customer-03")
            partView.HeaderRow.Cells(6).Font.Size = 8
            partView.HeaderRow.Cells(7).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CR-09")
            partView.HeaderRow.Cells(7).Font.Size = 8
            partView.HeaderRow.Cells(8).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-09")
            partView.HeaderRow.Cells(8).Font.Size = 8
            partView.HeaderRow.Cells(9).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-09")
            partView.HeaderRow.Cells(9).Font.Size = 8
        End If
        'partView.PageSize = 3
    End Sub

    Protected Sub partView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles partView.PageIndexChanging
        partView.PageIndex = e.NewPageIndex
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        'Dim partall As DataSet = Session("partView")
        'partView.DataSource = partall
        'partView.DataBind()
        populategrid()

        DisplayGridHeader()
    End Sub

    Protected Sub partView_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles partView.RowEditing
        'Dim ds As DataSet = Session("partView")
        'partView.DataSource = ds
        'partView.DataBind()
        'Dim type As String = ds.Tables(0).Rows(partView.PageIndex * partView.PageSize + e.NewEditIndex).Item(0).ToString()
        Dim type As String = partView.Rows(e.NewEditIndex).Cells(1).Text
        type = Server.UrlEncode(type)
        'Dim no As String = ds.Tables(0).Rows(partView.PageIndex * partView.PageSize + e.NewEditIndex).Item(1).ToString
        Dim no As String = partView.Rows(e.NewEditIndex).Cells(2).Text
        no = Server.UrlEncode(no)
        Dim strTempURL As String = "type=" + type + "&no=" + no
        strTempURL = "~/PresentationLayer/Function/ServiceBillModify.aspx?" + strTempURL
        Response.Redirect(strTempURL)

    End Sub

    Protected Sub lnkAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAdd.Click
        Response.Redirect("ServiceBillAdd.aspx")
    End Sub

#Region " bonds"
    '----------------------------------------------------------------------------------------------- Binding Technician to DropDown -- Tomas 20111214
    Public Function databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        Dim row As DataRow
        If ds.Tables(0).Rows.Count > 0 Then
            dropdown.Items.Add(" ")
            For Each row In ds.Tables(0).Rows
                Dim NewItem As New ListItem()
                NewItem.Text = row("name") & "-" & row("id")
                NewItem.Value = row("id")
                dropdown.Items.Add(NewItem)
            Next

        End If
    End Function
    Public Function svcbonds(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        '----------------------------------------------------------------------------------------------- Binding SVC to DropDown -- Tomas 20111214
        dropdown.Items.Clear()
        Dim row As DataRow
        If ds.Tables(0).Rows.Count > 0 Then
            dropdown.Items.Add(" ")
            For Each row In ds.Tables(0).Rows
                Dim NewItem As New ListItem()
                NewItem.Text = row("id") & "-" & row("name")
                NewItem.Value = row("id")
                dropdown.Items.Add(NewItem)
            Next

        End If
    End Function
#End Region

    Protected Sub partView_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles partView.RowDataBound
        If (e.Row.RowIndex <> -1) Then
            If e.Row.Cells(9).Text <> "" Then
                e.Row.Cells(9).Text = "<a href=../report/RPTReprintSBCSearch.aspx?FIV1_SBCNO=" + e.Row.Cells(9).Text + " target='_blank' style='text-decoration : none'>" + e.Row.Cells(9).Text + "</a>"
            End If
        End If

    End Sub

    '-- Michelle 2011-12-29 
    Protected Sub SVCList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles SVClist.SelectedIndexChanged
        ' Modified by Ryan Estandarte 14 Feb 2012
        'Dim svcID As Integer
        'If SVClist.SelectedValue <> "ZZZ" Then
        '    svcID = SVClist.SelectedValue
        'Else
        '    svcID = 0
        'End If

        Dim svcID As String
        svcID = SVClist.SelectedValue

        Dim tech As New DataSet
        Dim installbond As New clsCommonClass
        installbond.sparea = svcID
        installbond.spctr = Session("login_ctryID")
        installbond.spstat = Session("login_cmpID")
        installbond.rank = Session("login_rank")
        tech = installbond.Getcomidname("BB_MASTECH_IDNAME")
        If tech.Tables.Count <> 0 Then
            databonds(tech, technicianList)
        End If

        DisplayGridHeader()

    End Sub

    Protected Sub technicianList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles technicianList.SelectedIndexChanged

    End Sub

    Protected Sub ctryStatDrop_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ctryStatDrop.SelectedIndexChanged

    End Sub
End Class
