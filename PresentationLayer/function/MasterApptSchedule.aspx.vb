﻿Imports BusinessEntity
Imports SQLDataAccess
Imports System.Configuration
Imports System.Data
Partial Class PresentationLayer_function_MasterApptSchedule_aspx
    Inherits System.Web.UI.Page
    Private masterapptschdule As clsMasterApptSchedule
    Private objXmlTr As clsXml
    Private ZoneEntity As clsZone
    Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try

        If (Not Page.IsPostBack) Then

            BindPage()
            'ADDED BY DEYB 30-06-2006
            txtApptDate.Text = Format(Now(), "dd/MM/yyyy")
            '------------------------
        End If
        GridViewPreProcess()
        Me.masterapptschdule = New clsMasterApptSchedule()
        HypCal.NavigateUrl = "javascript:DoCal(document.form1.txtApptDate);"
    End Sub

    Protected Sub BindPage()

        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        'display label message
        objXmlTr = New clsXml()
        objXmlTr.XmlFile = ConfigurationManager.AppSettings("XmlFilePath")
        Me.titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_E_TITLE")
        Me.lblApptDate.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_D_APPTDATE")
        Me.lblSVC.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_D_CENTER")

        Me.lblTotalASApt.Text = objXmlTr.GetLabelName("EngLabelMsg", "TotalASApt")
        Me.lblTotalSSApt.Text = objXmlTr.GetLabelName("EngLabelMsg", "TotalSSApt")
        Me.lblJobServiceType.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-TRANSTYPE")



        Me.lbtnRetrieve.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_D_RETRIEVE")
        lblNoteUP.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
        lblNoteDown.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")


        objXmlTr.XmlFile = ConfigurationManager.AppSettings("StatMsg")
        Me.SerCenIDerr.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-D-HELP2")

        ZoneEntity = New clsZone()
        ZoneEntity.UserID = Session("userID")
        Dim SerCentInfo As DataSet = ZoneEntity.GetSerCent()
        If SerCentInfo Is Nothing Then
            Response.Redirect("~/PresentationLayer/error.aspx")
        End If
        SerCenIDDrop.Items.Add("")
        Dim i As Integer
        For i = 0 To SerCentInfo.Tables(0).Rows.Count - 1
            Dim list As New ListItem

            list.Text = SerCentInfo.Tables(0).Rows(i).Item("MSVC_SVCID") & "-" & SerCentInfo.Tables(0).Rows(i).Item("MSVC_ENAME")
            list.Value = SerCentInfo.Tables(0).Rows(i).Item("MSVC_SVCID")

            SerCenIDDrop.Items.Add(list)
        Next
        SerCenIDDrop.SelectedValue = Session("LOGIN_SVCID")

        setPurview()

        Me.cboJobServiceType.Items.Clear()
        cboJobServiceType.Items.Add("ALL")
        cboJobServiceType.Items.Add("SS")
        cboJobServiceType.Items.Add("AS")
        cboJobServiceType.SelectedIndex = 1



    End Sub

    Protected Sub lbtnRetrieve_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnRetrieve.Click
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        objXmlTr = New clsXml()

        If Me.masterapptschdule Is Nothing Then
            txtApptDate.Text = Request.Form("txtApptDate")
            Return
        End If

        If Not IsDate(txtApptDate.Text) Then
            objXmlTr.XmlFile = ConfigurationManager.AppSettings("StatMsg")
            Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-MASJOBS-0008")
            Return
        End If

        ClearPage()
        Dim result As Integer
        masterapptschdule.JobType = Me.cboJobServiceType.SelectedValue
        Dim dataSource As DataTable = Me.masterapptschdule.getApptStatView(Session("userID"), _
                                        Request.Form("txtApptDate"), Me.SerCenIDDrop.Text, result)


        Select Case result
            Case 1
                Me.ApptView.DataSource = dataSource
                Me.ApptView.DataBind()
                If Me.ApptView.Rows.Count < 1 Then
                    objXmlTr.XmlFile = ConfigurationManager.AppSettings("StatMsg")
                    Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-D-SUCCESS4")
                    Return
                End If
                objXmlTr.XmlFile = ConfigurationManager.AppSettings("XmlFilePath")
                For i As Integer = 0 To Me.ApptView.HeaderRow.Cells.Count - 1
                    Me.ApptView.HeaderRow.Cells(i).Text = objXmlTr.GetLabelName( _
                         "EngLabelMsg", "BB_FUN_E_HEADER" + i.ToString())
                Next

                If Me.ApptView.Rows.Count > 0 Then
                    Me.ApptView.FooterRow.Cells(0).ColumnSpan = Me.ApptView.FooterRow.Cells.Count - 1
                    For j As Integer = 2 To Me.ApptView.FooterRow.Cells.Count - 1
                        Me.ApptView.FooterRow.Cells.RemoveAt(2)
                    Next
                    Me.ApptView.FooterRow.Cells(0).Text = objXmlTr.GetLabelName( _
                             "EngLabelMsg", "BB_FUN_E_GrandTol")
                    Me.ApptView.FooterRow.Cells(1).Text = masterapptschdule.getTotalAppt().Item(0).ToString()

                    'Me.txtTotalSSApt.Text = masterapptschdule.getTotalAppt().Item(0).ToString() + "/" + _
                    'masterapptschdule.getTotalAppt().Item(1).ToString()
                    Me.txtTotalSSApt.Text = masterapptschdule.TotalSS.ToString() + "/" + _
                                       masterapptschdule.getTotalAppt().Item(1).ToString()

                    Me.txtTotalASApt.Text = masterapptschdule.TotalAS
                End If

                objXmlTr.XmlFile = ConfigurationManager.AppSettings("StatMsg")
                Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-D-SUCCESS3")

                Session.Contents("MasterApptSchedule") = Me.masterapptschdule
            Case 0
                objXmlTr.XmlFile = ConfigurationManager.AppSettings("StatMsg")
                Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-D-WARNING1")
            Case -1
                objXmlTr.XmlFile = ConfigurationManager.AppSettings("StatMsg")
                Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-D-ERROR3")
            Case -2
                objXmlTr.XmlFile = ConfigurationManager.AppSettings("StatMsg")
                Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-E-ERROR1")
        End Select
        txtApptDate.Text = Request.Form("txtApptDate")
    End Sub
    Public Sub ViewButtonClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles ApptView.RowCommand
        Dim ZoneID As String, argument As String
        argument = Server.UrlEncode(e.CommandArgument)
        ZoneID = Server.UrlEncode(e.CommandName)
        Response.Redirect("~/PresentationLayer/function/ViewApptSchedule.aspx?zone=" + ZoneID + "&arg=" + argument + "&JobType=" + Me.cboJobServiceType.SelectedItem.Text + "&TotalSS=" + Me.txtTotalSSApt.Text + "&TotalAS=" + Me.txtTotalASApt.Text)
    End Sub

    Public Sub ClearPage()
        Me.txtTotalSSApt.Text = ""
        'Me.txtTotalASApt.Text = ""

        If Me.ApptView.Rows.Count > 0 Then
            For i As Integer = 0 To Me.ApptView.Rows.Count - 1
                Me.ApptView.Rows(i).Cells.Clear()
            Next
            Me.ApptView.FooterRow.Cells.Clear()
        End If
    End Sub

    Protected Sub GridViewPreProcess()
        'Gridview还原后处理
        If Me.ApptView.Rows.Count > 0 Then
            Me.ApptView.FooterRow.Cells(0).ColumnSpan = Me.ApptView.FooterRow.Cells.Count - 1
            For j As Integer = 2 To Me.ApptView.FooterRow.Cells.Count - 1
                Me.ApptView.FooterRow.Cells.RemoveAt(2)
            Next
        End If
    End Sub

    Protected Sub setPurview()
        Dim arrayPurview As Boolean() = Nothing
        arrayPurview = clsUserAccessGroup.GetUserPurview( _
                               Session.Contents("accessgroup"), "26" _
                       )
        Me.lbtnRetrieve.Enabled = arrayPurview(2)
    End Sub

    Protected Sub MyCalendar_SelectedDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyCalendar.SelectedDateChanged

        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Me.txtApptDate.Text = Me.MyCalendar.SelectedDate
    End Sub

End Class
