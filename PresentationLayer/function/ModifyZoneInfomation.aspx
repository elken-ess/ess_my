<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ModifyZoneInfomation.aspx.vb"
    Inherits="PresentationLayer_Function_defineMaster" EnableEventValidation="false" %>

<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
    <link href="../css/style.css" type="text/css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <table id="uagTab" border="0" width="100%">
            <tr>
                <td style="width: 100%">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" style="width: 1%">
                                <img height="24" src="../graph/title1.gif" width="5" /></td>
                            <td background="../graph/title_bg.gif" class="style2" width="98%">
                                <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="right" background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title_2.gif" width="5" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 30px">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" class="style2" width="98%">
                                <font color="red">
                                    <asp:Label ID="errlab" runat="server" Text="Label" Visible="False"></asp:Label></font></td>
                        </tr>
                    </table>
                    <asp:Label ID="lblNoteUP" runat="server" ForeColor="Red" Text="Label"></asp:Label></td>
            </tr>
            <tr>
                <td style="height: 476px">
                    <table cellspacing="1" cellpadding="2" bgcolor="#b7e6e6" border="0" width="100%">
                        <tr bgcolor="white">
                            <td align="right" style="width: 30%">
                                <asp:Label ID="ZoneIDLab" runat="server"></asp:Label></td>
                            <td style="width: 30%">
                                <asp:TextBox ID="ZonIDBox" Width="80%" MaxLength="10" runat="server"></asp:TextBox>
                                <font
                                    color="red">*</font></td>
                            <td style="width: 30%">
                                <asp:LinkButton ID="btnCreZone" runat="server"></asp:LinkButton>
                                <asp:LinkButton ID="btnDelZone" runat="server"></asp:LinkButton></td>
                        </tr>
                        <tr bgcolor="white">
                            <td align="center" style="width: 30%;">
                                <asp:ListBox ID="AreaList" runat="server" Height="300px" Width="95%"></asp:ListBox></td>
                            <td style="width: 30%;" bgcolor="white">
                                <asp:LinkButton ID="btnAddArea" runat="server"></asp:LinkButton><br />
                                <br />
                                <br />
                                <asp:LinkButton ID="btnDelArea" runat="server"></asp:LinkButton></td>
                            <td align="left"  style="width: 30%;">
                                &nbsp;
                            
                                <asp:Panel ID="Panel1" runat="server" Height="320px" ScrollBars="Auto" Width="300px">
                                
                                    <asp:TreeView ID="ZonAreaInfoTree"  runat="server" Height="320px" Width="198%" Font-Underline="False"
                                    ShowLines="True">
                                    <SelectedNodeStyle Font-Underline="True" />
                                    </asp:TreeView>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr bgcolor="white">
                            <td align="right" style="width: 30%">
                                <asp:Label ID="ZonTYLab" runat="server"></asp:Label></td>
                            <td style="width: 30%">
                                <asp:TextBox ID="ZonTYBox" ReadOnly="true" runat="server"></asp:TextBox></td>
                            <td style="width: 30%">
                                <%--<asp:LinkButton ID="btnSaveMas" runat="server"></asp:LinkButton>--%></td>
                        </tr>
                        <tr bgcolor="white">
                            <td align="right" style="width: 30%;">
                                <asp:Label ID="SerCentIDLab" runat="server"></asp:Label></td>
                            <td style="width: 30%;">
                                <asp:TextBox ID="SerCentBox" runat="server" ReadOnly="True"></asp:TextBox></td>
                            <td style="width: 30%;">
                                <asp:LinkButton ID="btnSave" runat="server"></asp:LinkButton>
                            </td>
                        </tr>
                        <tr bgcolor="white">
                            <td align="right" style="width: 30%;">
                                <asp:Label ID="ApptDateLab" runat="server"></asp:Label></td>
                            <td style="width: 30%;">
                                <asp:TextBox ID="ApptDateBox" runat="server" ReadOnly="True"></asp:TextBox></td>
                            <td style="width: 30%;">
                                <asp:HyperLink ID="cancel" runat="server"></asp:HyperLink></td>
                        </tr>
                    </table>
                    <asp:Label ID="lblNoteDown" runat="server" ForeColor="Red" Text="Label"></asp:Label></td>
            </tr>
        </table>
        <cc1:MessageBox ID="saveMsgBox" runat="server" />
        <cc1:MessageBox ID="InfoMsgBox" runat="server" />
    </form>
</body>
</html>
