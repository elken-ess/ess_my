﻿Imports BusinessEntity
Imports System
Imports System.Data

Partial Class PresentationLayer_function_AppointmentCallingListing
    Inherits System.Web.UI.Page
    Private objXmlTr As New clsXml
    Private ApptCalListEntity As clsApptCalList
    Dim CommonClassEntity As New clsCommonClass
    Dim callList As New clsCallListAssignment
    Private ApptCalList As DataSet
    Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.MaintainScrollPositionOnPostBack = True
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                'Response.Redirect("~/PresentationLayer/logon.aspx")
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
        Catch ex As Exception
            'Response.Redirect("~/PresentationLayer/logon.aspx")
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try

        errlab.Visible = False

        If (Not Page.IsPostBack) Then
            DisplayInfo()
            Dim CommonClass As New clsCommonClass
            CommonClass.spctr = Session("login_ctryID").ToString().ToUpper
            Dim areads As New DataSet
            Dim stateds As New DataSet
            areads = CommonClass.GetAreaidname()
            stateds = CommonClass.GetStateidname()
            If areads.Tables.Count <> 0 Then
                databonds(areads, Me.cboAreaID)
            End If
            If areads.Tables.Count <> 0 Then
                databonds(stateds, Me.cboStateID)
            End If

            Me.cboAreaID.Items.Insert(0, "")
            Me.cboStateID.Items.Insert(0, "")
        ElseIf ApptListView.Rows.Count > 0 Then
            DispGridViewHead()
        End If

    End Sub

    Private Sub databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList, Optional ByVal X As Boolean = True)

        dropdown.Items.Clear()
        Dim row As DataRow

        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            If X = True Then
                NewItem.Text = Trim(row("id")) & "-" & Trim(row("name"))
            Else
                NewItem.Text = Trim(row("name"))
            End If
            NewItem.Value = Trim(row("id"))
            dropdown.Items.Add(NewItem)
        Next

    End Sub

    Protected Sub DisplayInfo()
        
        'Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        'display label message

        objXmlTr.XmlFile = Configuration.ConfigurationManager.AppSettings("XmlFilePath")
        titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CALLINGLIST-0001")
        State.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CALLINGLIST-0002")
        SucCallNum.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CALLINGLIST-0003")
        BtnPrePage.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CALLINGLIST-0005")
        BtnNexPage.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CALLINGLIST-0006")
        BtnRefresh.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CALLINGLIST-0007")
        CusName.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CALLINGLIST-0008")
        CusID.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CALLINGLIST-0014")
        Status.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CALLINGLIST-0009")
        BtnSeach.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Search")
        lblAreaID.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0009")
        lblStateID.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0011")
        lblContractEntitled.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0006")

        Dim objYesNo As New clsrlconfirminf()
        Dim count As Integer
        Dim statParam As ArrayList = New ArrayList '待验证

        Dim strItemID As String
        Dim strItemName As String
        Dim contractpar As ArrayList = New ArrayList
        contractpar = objYesNo.searchconfirminf("YESNO")
        count = 0
        cboContractEntitled.Items.Add("")
        For count = 0 To contractpar.Count - 1
            strItemID = contractpar.Item(count + 1)
            strItemName = contractpar.Item(count)
            count = count + 1
            Me.cboContractEntitled.Items.Add(New ListItem(strItemID.ToString(), strItemName.ToString()))

        Next
        objYesNo = Nothing

        'create the dropdownlist
        'Add by BAIYUNPENG ================================
        objXmlTr.XmlFile = Configuration.ConfigurationManager.AppSettings("StatMsg")
        StatusDrop.Items.Add(New ListItem(objXmlTr.GetLabelName("StatusMessage", "RPT_A"), "All"))
        'End Add 060615    ================================
        Dim callStat As New clsUtil()  '待验证


        statParam = callStat.searchconfirminf("callStatus")        


        Dim statid As String
        Dim statnm As String
        For count = 0 To statParam.Count - 1
            statid = statParam.Item(count)
            statnm = statParam.Item(count + 1)
            count = count + 1
            Dim list As New ListItem

            list.Value = statid
            list.Text = objXmlTr.GetLabelName("StatusMessage", statid)

            StatusDrop.Items.Add(list)
        Next
        StatusDrop.Items(0).Selected = True '待验证

        BindGrid()

        objXmlTr.XmlFile = System.Configuration.ConfigurationManager.AppSettings("StatMsg")
        If ApptCalList Is Nothing Then

            errlab.Text = objXmlTr.GetLabelName("StatusMessage", "BB-View-Fail")
            errlab.Visible = True
            MsgErr.Alert(errlab.Text)
            Return
        End If
        If ApptCalList.Tables.Count <= 0 Then
            
            errlab.Text = objXmlTr.GetLabelName("StatusMessage", "BB-View-Fail")
            errlab.Visible = True
            MsgErr.Alert(errlab.Text)
            Return
        End If
        If ApptCalList.Tables(0).Rows.Count >= 1 Then

            DispGridViewHead()

        End If
    End Sub

    Protected Sub BindGrid()
        Dim UserID As String = Session("userID")


        ApptCalListEntity = New clsApptCalList()

        With ApptCalListEntity
            .UserID = UserID
            .CusName = ""
            .Status = ""
            .GetStateID = ""
            .GetAreaID = ""
            .ContractEntitled = ""
            .CustomerID = ""
        End With


        ApptCalList = ApptCalListEntity.GetApptList()
        If ApptCalList Is Nothing Then
            objXmlTr.XmlFile = System.Configuration.ConfigurationManager.AppSettings("StatMsg")
            errlab.Text = objXmlTr.GetLabelName("StatusMessage", "BB-View-Fail")
            errlab.Visible = True
            MsgErr.Alert(errlab.Text)
            Return
        End If
        If ApptCalList.Tables.Count <= 0 Then
            objXmlTr.XmlFile = System.Configuration.ConfigurationManager.AppSettings("StatMsg")
            errlab.Text = objXmlTr.GetLabelName("StatusMessage", "BB-View-Fail")
            errlab.Visible = True
            MsgErr.Alert(errlab.Text)
            Return
        End If
        If ApptCalList.Tables(0).Rows.Count < 1 Then
            objXmlTr.XmlFile = System.Configuration.ConfigurationManager.AppSettings("StatMsg")
            StateID.Text = ""
            errlab.Text = objXmlTr.GetLabelName("StatusMessage", "BB-View-Fail")
            errlab.Visible = True
            MsgErr.Alert(errlab.Text)
        Else
            StateID.Text = ApptCalList.Tables(0).Rows(0).Item(0).ToString
            ApptListView.AllowPaging = True
            ApptListView.AllowSorting = True
            ApptListView.DataSource = ApptCalList.Tables(0)
            Session("ApptListView") = ApptCalList
            ApptListView.Columns(10).HeaderImageUrl = "~/PresentationLayer/graph/edit.gif"
            ApptListView.Columns(11).HeaderImageUrl = "~/PresentationLayer/graph/edit.gif"
            ApptListView.DataBind()
            ApptListView.Columns(1).Visible = False
            DispGridViewHead()
            'Add by BAIYUNPENG ================================
            Number.Text = SumConfirmCount()
            'End Add 060615    ================================
        End If
        'Number.Text = "0"
    End Sub

    Private Sub DispGridViewHead()

        objXmlTr.XmlFile = Configuration.ConfigurationManager.AppSettings("XmlFilePath")

        ApptListView.HeaderRow.Cells(0).Text = _
                        objXmlTr.GetLabelName("EngLabelMsg", "BB-ZONGINGINF-0025")
        ApptListView.HeaderRow.Cells(0).Font.Size = 8

        ApptListView.HeaderRow.Cells(1).Text = _
                objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_D_RPT003")
        ApptListView.HeaderRow.Cells(1).Font.Size = 8
        ApptListView.HeaderRow.Cells(1).Width = 0

        ApptListView.HeaderRow.Cells(2).Text = _
                         objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-REMINDER-RPTH5")
        ApptListView.HeaderRow.Cells(2).Font.Size = 8

        ApptListView.HeaderRow.Cells(3).Text = _
                 objXmlTr.GetLabelName("EngLabelMsg", "BB-CALLINGLIST-0017")
        ApptListView.HeaderRow.Cells(3).Font.Size = 8

        ApptListView.HeaderRow.Cells(4).Text = _
                 objXmlTr.GetLabelName("EngLabelMsg", "BB-CALLINGLIST-0018")
        ApptListView.HeaderRow.Cells(4).Font.Size = 8

        ApptListView.HeaderRow.Cells(5).Text = _
                 objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0009")
        ApptListView.HeaderRow.Cells(5).Font.Size = 8

        ApptListView.HeaderRow.Cells(6).Text = _
                        objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0011")
        ApptListView.HeaderRow.Cells(6).Font.Size = 8

        ApptListView.HeaderRow.Cells(7).Text = _
                         objXmlTr.GetLabelName("EngLabelMsg", "BB-CALLINGLIST-0009")
        ApptListView.HeaderRow.Cells(7).Font.Size = 8


        ApptListView.HeaderRow.Cells(8).Text = _
                         objXmlTr.GetLabelName("EngLabelMsg", "BB-CALLINGLIST-0012")
        ApptListView.HeaderRow.Cells(8).Font.Size = 8

        ApptListView.HeaderRow.Cells(9).Text = _
                         objXmlTr.GetLabelName("EngLabelMsg", "BB-CALLINGLIST-0013")
        ApptListView.HeaderRow.Cells(9).Font.Size = 8

        'ApptListView.HeaderRow.Cells(5).Text = _
        '                 objXmlTr.GetLabelName("EngLabelMsg", "BB-INSTALLHD0")
        'ApptListView.HeaderRow.Cells(5).Font.Size = 8

        'ApptListView.HeaderRow.Cells(7).Text = _
        '                 objXmlTr.GetLabelName("EngLabelMsg", "BB-CALLINGLIST-0015")
        'ApptListView.HeaderRow.Cells(7).Font.Size = 8


    End Sub

    Protected Sub ApptListView_RowCreated(ByVal sender As Object, ByVal e As _
                    System.Web.UI.WebControls.GridViewRowEventArgs) Handles ApptListView.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim ds As DataSet = Session("ApptListView")
            Dim flag As Integer
            Dim Contactarray As New ArrayList()
            Dim dr As DataRow
            flag = 0
            For Each dr In ds.Tables(1).Rows '待验证
                If dr.Item("MTEL_CUSID").ToString().Trim() = ds.Tables(0).Rows(e.Row.DataItemIndex).Item("MROU_CUSID").ToString().Trim() And _
                    dr.Item("MTEL_CUSPF").ToString().Trim() = ds.Tables(0).Rows(e.Row.DataItemIndex).Item("MROU_CUSPF").ToString().Trim() Then
                    Dim Tel As String = ""
                    Tel = CommonClassEntity.passconverttel(dr.Item("MTEL_TELNO"))
                    Contactarray.Add(Tel)
                    flag = 1
                ElseIf flag = 1 Then
                    flag = 2
                End If
                If flag = 2 Then
                    Exit For
                End If
            Next

            'jerry test chg remove contact column
            Dim Contactddl As New DropDownList()
            'Contactddl = e.Row.FindControl("ddltel")
            'Contactddl.DataSource = Contactarray '待验证
            'Contactddl.DataBind()

            '*******************
            objXmlTr.XmlFile = Configuration.ConfigurationManager.AppSettings("XmlFilePath")
            Dim update As New LinkButton()
            update = e.Row.FindControl("Update") '待验证
            update.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Save")

            Dim create As New LinkButton()
            create = e.Row.FindControl("CreateAppt") '待验证
            create.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CALLINGLIST-0004")
            '*******************

            Dim callStat As New clsUtil()

            Dim xAppt As New clsApptCalList
            Contactddl = e.Row.FindControl("ddlstatus") '待验证Contactddl
            Contactddl.DataSource = xAppt.GetStatus("callStatus")
            Contactddl.DataValueField = "LCOD_CODID"
            Contactddl.DataTextField = "LCOD_REM"
            Contactddl.DataBind()

            Dim statParam As ArrayList = New ArrayList
            statParam = callStat.searchconfirminf("callStatus")
            Dim statarray As ArrayList = New ArrayList
            Dim current, i As Integer
            current = 0
            For i = 0 To statParam.Count - 1 '待验证ds
                If ds.Tables(0).Rows(e.Row.DataItemIndex).Item("MROU_CSTAT").ToString().Trim() = statParam(i) Then
                    If i Mod 2 = 0 Then
                        current = i / 2
                    End If
                End If
            Next
            Contactddl.SelectedIndex = current

            'Remarks
            Dim temTextBox As New TextBox
            temTextBox = e.Row.FindControl("ddlremark") '待验证temTextBox
            temTextBox.Text = ds.Tables(0).Rows(e.Row.DataItemIndex).Item("MROU_CAREM").ToString().Trim()

            objXmlTr.XmlFile = System.Configuration.ConfigurationManager.AppSettings("StatMsg")
            Dim xCV As CompareValidator = e.Row.FindControl("CompDateVal")
            xCV.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "INVALID_DATE")

        End If
    End Sub

    Protected Sub BtnSeach_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSeach.Click
        Dim UserID As String = Session("userID")
        ApptCalListEntity = New clsApptCalList()


        ApptCalListEntity.UserID = UserID
        ApptCalListEntity.CusName = Me.CusNameBox.Text
        ApptCalListEntity.CustomerID = Me.CusIDBox.Text()

        If StatusDrop.SelectedIndex = 0 Then
            ApptCalListEntity.Status = ""
        Else
            ApptCalListEntity.Status = Me.StatusDrop.SelectedItem.Value
        End If

        ApptCalListEntity.GetStateID = Me.cboStateID.SelectedValue
        ApptCalListEntity.GetAreaID = Me.cboAreaID.SelectedValue
        ApptCalListEntity.ContractEntitled = Me.cboContractEntitled.SelectedItem.Value

        ApptCalList = ApptCalListEntity.GetApptList()
        If ApptCalList Is Nothing Then
            objXmlTr.XmlFile = System.Configuration.ConfigurationManager.AppSettings("StatMsg")
            errlab.Text = objXmlTr.GetLabelName("StatusMessage", "BB-View-Fail")
            errlab.Visible = True
            MsgErr.Alert(errlab.Text)
            Return
        End If
        If ApptCalList.Tables.Count <= 0 Then
            objXmlTr.XmlFile = System.Configuration.ConfigurationManager.AppSettings("StatMsg")
            errlab.Text = objXmlTr.GetLabelName("StatusMessage", "BB-View-Fail")
            errlab.Visible = True
            MsgErr.Alert(errlab.Text)
            Return
        End If
        If ApptCalList.Tables(0) Is Nothing Then
            objXmlTr.XmlFile = System.Configuration.ConfigurationManager.AppSettings("StatMsg")
            errlab.Text = objXmlTr.GetLabelName("StatusMessage", "BB-View-Fail")
            errlab.Visible = True
            MsgErr.Alert(errlab.Text)
            Return
        End If
        ApptListView.DataSource = ApptCalList.Tables(0)
        Session("ApptListView") = ApptCalList
        ApptListView.DataBind()
        If ApptCalList.Tables(0).Rows.Count >= 1 Then
            DispGridViewHead()
        End If

        'Add by BAIYUNPENG ================================
        Number.Text = SumConfirmCount()
        'End Add 060615    ================================
    End Sub

    Protected Sub ApptListView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles ApptListView.PageIndexChanging
        ApptListView.PageIndex = e.NewPageIndex
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Dim ApptCalList As DataSet = Session("ApptListView")
        ApptListView.DataSource = ApptCalList
        ApptListView.DataBind()
        If ApptCalList.Tables(0).Rows.Count > 0 Then '待验证ApptCalList

            DispGridViewHead()
        End If
    End Sub

    Protected Sub BtnNexPage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnNexPage.Click
        If ApptListView.PageIndex < ApptListView.PageCount Then
            ApptListView.PageIndex += 1
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
            Dim ApptCalList As DataSet = Session("ApptListView")
            ApptListView.DataSource = ApptCalList
            ApptListView.DataBind()
            If ApptCalList.Tables(0).Rows.Count > 0 Then '待验证ApptCalList
                DispGridViewHead()
            End If
        End If
    End Sub

    Protected Sub BtnPrePage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnPrePage.Click
        If ApptListView.PageIndex > 0 Then
            ApptListView.PageIndex -= 1
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
            Dim ApptCalList As DataSet = Session("ApptListView")
            ApptListView.DataSource = ApptCalList
            ApptListView.DataBind()
            If ApptCalList.Tables(0).Rows.Count > 0 Then '待验证ApptCalList
                DispGridViewHead()
            End If
        End If
    End Sub

    Protected Sub ApptListView_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles ApptListView.RowCommand
        Dim index As Integer
        Dim objCommonClass As New clsCommonClass
        Dim lstrDayReminder As String = objCommonClass.GetReminderDay
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        Dim blnErrorFree As Boolean
        Dim objXm As New clsXml
        objXm.XmlFile = System.Configuration.ConfigurationManager.AppSettings("StatMsg")



        If e.CommandName = "Update" Then
            blnErrorFree = True

            Dim UserID As String = Session("userID")
            ApptCalListEntity = New clsApptCalList()
            ApptCalListEntity.UserID = UserID
            'modfied by deyb 06-25-2006
            'Dim index As Integer = Integer.Parse(e.CommandArgument)
            index = e.CommandArgument - (ApptListView.PageIndex * 10)
            '-----------------------------------------
            Dim custid As LinkButton = Me.ApptListView.Rows(index).FindControl("CustomID")
            ApptCalListEntity.CustomerID = custid.Text  '待验证custid
            ApptCalListEntity.CustomerPrefix = Session("login_ctryID")


            'Dim custid As String = Me.ApptListView.Rows(index).Cells(2).Text
            'ApptCalListEntity.CustomerID = custid

            Dim stat As DropDownList = Me.ApptListView.Rows(index).FindControl("ddlstatus")
            ApptCalListEntity.Status = stat.SelectedItem.Value  '待验证stat
            Dim Remark As TextBox = Me.ApptListView.Rows(index).FindControl("ddlremark")
            ApptCalListEntity.Remarks = Remark.Text '待验证Remark
            ApptCalListEntity.IPaddress = Request.UserHostAddress

            Dim nextCallDT As TextBox = Me.ApptListView.Rows(index).FindControl("ActualServDateBox")
            ApptCalListEntity.NextCallDate = nextCallDT.Text '待验证nextCallDT

            ApptCalListEntity.GetRoId = getRouId(Me.ApptListView.Rows(index).Cells(2).Text.Trim, custid.Text)
            'ApptCalListEntity.GetRoId = Me.ApptListView.Rows(index).Cells(1).Text.Trim

            If nextCallDT.Text = "" Then
                MessageBox.Alert(objXm.GetLabelName("StatusMessage", "NEXTCALLDATE_ERROR4"))
                Me.ApptListView.Rows(index).FindControl("ddlremark").Focus()
                Exit Sub
            End If

            If CType(nextCallDT.Text, Date) <= System.DateTime.Today Then
                blnErrorFree = False
                MessageBox.Alert(objXm.GetLabelName("StatusMessage", "NEXTCALLDATE_ERROR2"))
                Me.ApptListView.Rows(index).FindControl("ddlremark").Focus()
            End If

            ' next call date can not more than the reminder day from today date
            If CType(nextCallDT.Text, Date) > DateAdd(DateInterval.Day, CInt(lstrDayReminder), System.DateTime.Today) Then
                blnErrorFree = False
                MessageBox.Alert(objXm.GetLabelName("StatusMessage", "NEXTCALLDATE_ERROR1") + ":" + lstrDayReminder)
                Me.ApptListView.Rows(index).FindControl("ddlremark").Focus()
            End If

            Dim ds As DataSet = ApptCalListEntity.CheckContractOn13Month
            If Not ds Is Nothing Then
                Dim strContracted As String = ds.Tables(0).Rows(0).Item(0).ToString.Trim.ToUpper
                Dim strContractExpiryDate As String = IIf(IsDBNull(ds.Tables(0).Rows(0).Item(1)), "", ds.Tables(0).Rows(0).Item(1).ToString)
                Dim strConID As String = IIf(IsDBNull(ds.Tables(0).Rows(0).Item(2)), "", ds.Tables(0).Rows(0).Item(2).ToString)

                If strContracted = "Y" And strContractExpiryDate <> "" And strConID <> "" Then
                    If CType(nextCallDT.Text, Date) > CType(strContractExpiryDate, Date) Then
                        blnErrorFree = False
                        MessageBox.Alert(objXm.GetLabelName("StatusMessage", "NEXTCALLDATE_ERROR3") + ":" + strConID + "(" + Left(strContractExpiryDate, 10) + ")")
                        Me.ApptListView.Rows(index).FindControl("ddlremark").Focus()
                    End If
                End If
            End If


            If blnErrorFree Then
                Dim UpdateApptInfo As Integer = 0
                UpdateApptInfo = ApptCalListEntity.UpdApptInfo()


                If UpdateApptInfo > 0 Then
                    errlab.Text = objXm.GetLabelName("StatusMessage", "BB-UpdApptInfo-Suc")
                    errlab.Visible = True
                    MsgErr.Alert(errlab.Text)
                End If
                BtnSeach_Click(sender, e)
            End If

        ElseIf e.CommandName = "CreateAppt" Then
            Dim ds As DataSet = Session("ApptListView") '待验证ds
            Dim tempURL As String
            index = e.CommandArgument - (ApptListView.PageIndex * 10)
            'Dim CustomID As String = ds.Tables(0).Rows(index).Item("MROU_CUSID").ToString().Trim()

            '-----------------------------------------
            Dim custid As LinkButton = Me.ApptListView.Rows(index).FindControl("CustomID")
            Dim CustomID As String = custid.Text  '待验证custid


            CustomID = Server.UrlEncode(CustomID)
            Dim CustomPf As String = ds.Tables(0).Rows(index).Item("MROU_CUSPF").ToString().Trim()
            Dim ROID As String = getRouId(Me.ApptListView.Rows(index).Cells(2).Text.Trim, custid.Text)
            ' Dim ROID As String = Me.ApptListView.Rows(index).Cells(1).Text.Trim

            'Dim cboTel As DropDownList = Me.ApptListView.Rows(index).FindControl("ddltel")
            'Dim lstrTelno As String = cboTel.SelectedValue

            CustomPf = Server.UrlEncode(CustomPf)
            'tempURL = "custID=" + CustomID + "&custPf=" + CustomPf + "&telno=" + lstrTelno + "&ROID=" + ROID
            tempURL = "custID=" + CustomID + "&custPf=" + CustomPf + "&ROID=" + ROID
            Response.Redirect("~/PresentationLayer/function/addappointment.aspx?" + tempURL)
        ElseIf e.CommandName = "CustomID" Then
            Dim ds As DataSet = Session("ApptListView") '待验证ds
            Dim tempURL As String
            Dim modisy As String = -1
            index = e.CommandArgument - (ApptListView.PageIndex * 10)
            'Dim CustomID As String = ds.Tables(0).Rows(index).Item("MROU_CUSID").ToString().Trim()
            Dim custid As LinkButton = Me.ApptListView.Rows(index).FindControl("CustomID")
            Dim CustomID As String = custid.Text  '待验证custid
            CustomID = Server.UrlEncode(CustomID)
            Dim ROID As String = getRouId(Me.ApptListView.Rows(index).Cells(2).Text.Trim, custid.Text)
            'Dim ROID As String = Me.ApptListView.Rows(index).Cells(1).Text.Trim
            'Dim CustomPf As String = ds.Tables(0).Rows(e.CommandArgument).Item("MROU_CUSPF").ToString().Trim()
            'CustomPf = Server.UrlEncode(CustomPf)
            'Dim custid As String = CustomPf + CustomID
            tempURL = "custid=" + CustomID + "&modisy=" + modisy + "&ROID=" + ROID
            Response.Redirect("~/PresentationLayer/masterrecord/modifycustomer.aspx?" + tempURL)
            'ElseIf e.CommandName = "ImagBtn" Then
            '    Dim ActualServDateDateCalendar As Calendar = ApptListView.Rows(ApptListView.SelectedIndex).FindControl("ActualServDateDateCalendar")
            '    ActualServDateDateCalendar.Visible = Not ActualServDateDateCalendar.Visible
            '    ActualServDateDateCalendar.Style.Item("position") = "absolute"
            '    Dim ActualServDateBox As TextBox = ApptListView.Rows(ApptListView.SelectedIndex).FindControl("ActualServDateBox")
        End If
    End Sub

    Private Function getRouId(ByVal serialNo As String, ByVal cusid As String) As String
        Dim ROUID As String = ""
        Dim ds As DataSet
        Dim dt As DataTable
        Dim dr As DataRow
        ds = Session("ApptListView")
        dt = ds.Tables(0)
        For i As Integer = 0 To dt.Rows.Count - 1
            dr = dt.Rows(i)
            If (dr("ROINFO").ToString.Trim = serialNo And dr("MROU_CUSID").ToString.Trim = cusid) Then
                ROUID = dr("MROU_ROUID").ToString.Trim
                Exit For
            End If
        Next
        Return ROUID
    End Function
    

    Private Function SumConfirmCount() As Integer
        Dim ds As DataSet = Session("ApptListView")
        Dim i, iCount As Integer

        If ds.Tables.Count = 0 Then '待验证ds
            Return 0
        End If

        If ds.Tables(0).Rows.Count = 0 Then
            Return 0
        End If

        iCount = 0
        For i = 0 To ds.Tables(0).Rows.Count - 1
            If ds.Tables(0).Rows(i).Item("MROU_CSTAT").ToString.ToUpper = "SA" Then
                iCount = iCount + 1
            End If
        Next
        Return iCount
    End Function

    Protected Sub ActualServDateDateCalendar_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        'Dim ActualServDateDateCalendar As Calendar = sender
        Dim ActualServDateDateCalendar As JCalendar.JCalendar = sender
        'added by deyb 06-25-2006
        'Dim xCnt As Integer = ApptListView.Rows.Count - 1
        Dim yTab As Integer = ActualServDateDateCalendar.TabIndex - (ApptListView.PageIndex * 10)
        '-------------------------------------
        Dim ActualServDateBox As TextBox = ApptListView.Rows(yTab).FindControl("ActualServDateBox")
        ActualServDateBox.Text = ActualServDateDateCalendar.SelectedDate
        'ActualServDateDateCalendar.Visible = False
    End Sub

    Protected Sub BtnRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnRefresh.Click
        Response.Redirect(Request.RawUrl.ToString())
    End Sub

    Protected Sub ApptListView_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles ApptListView.RowUpdating

    End Sub

    Protected Sub cboStateID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboStateID.SelectedIndexChanged
        cboAreaID.Items.Clear()
        If Not cboStateID.SelectedValue = "" Then
            callList.StateFrom = cboStateID.SelectedValue
            callList.ServiceCenterFrom = "%"
        Else
            callList.StateFrom = "%"
            callList.ServiceCenterFrom = "%"
            cboStateID.Items.Add(New ListItem(objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), ""))
        End If

        Dim ds As DataSet = callList.getAreafromState()
        Dim dt As DataTable = ds.Tables(0)
        'Dim dr As DataRow
        Dim areanm As String
        Me.cboAreaID.Items.Insert(0, "")
        For i As Integer = 0 To dt.Rows.Count - 1
            areanm = dt.Rows(i).Item(0) + "-" + dt.Rows(i).Item(1)
            cboAreaID.Items.Add(New ListItem(areanm, dt.Rows(i).Item(0)))
        Next
    End Sub
End Class
