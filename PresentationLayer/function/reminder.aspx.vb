﻿Imports BusinessEntity
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared
Imports TransFile
Imports System.io
Imports System.Data
Partial Class PresentationLayer_function_reminder
    Inherits System.Web.UI.Page
    Dim objXmlTr As New clsXml
    Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
    Dim ReminderReportDoc As New ReportDocument()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        'LW Added part: To add for ajax
        AjaxPro.Utility.RegisterTypeForAjax(GetType(PresentationLayer_function_reminder))

        If Not Page.IsPostBack Then
            Session("ReminderReportDoc") = Nothing
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-REMINDER-TITL")
            fModeTypeLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-REMINDER-FMODETYPE")
            tModeTypeLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-REMINDER-TMODETYPE")
            fStatIDLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-REMINDER-FSTATID")
            tStatIDLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-REMINDER-TSTATID")
            fAreaIDLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-REMINDER-FAREAID")
            tAreaIDLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-REMINDER-TAREAID")
            fSvcIDLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-REMINDER-FSVCID")
            tSvcIDLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-REMINDER-TSVCID")
            fDueDateLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-REMINDER-FDUEDATE")
            tDueDateLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-REMINDER-TDUEDATE")
            IncludeCHKBOX.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-REMINDER-INCLUDECUS")
            ViewReportBtn.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-VIEWREPORT")
            totalCusLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-REMINDER-TOTALCUS")
            OutPutTextBtn.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-REMINDER-OUTPUTTEXT")
            GeneratePdfBtn.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-REMINDER-GENERATEPDF")
            prefLangLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-REMINDER-PREFLANG") 'lly 201702
            fDueDateBox.Text = Date.Today
            tDueDateBox.Text = Date.Today.AddMonths(1)
            OutPutTextBtn.Visible = False
            GeneratePdfBtn.Visible = False

            ReminderReportViewer.HasCrystalLogo = False
          
            UploadLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-REMINDER-UPLOADLAB")
            UploadBtn.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-REMINDER-UPLOADBTN")

        ElseIf Not (Session("ReminderReportDoc") Is Nothing) Then 'CrystalReport 已经显示
            Dim ReminderReportDoc As ReportDocument = Session("ReminderReportDoc")
            ReminderReportViewer.ReportSource = ReminderReportDoc
        End If

        HypCalFrom.NavigateUrl = "javascript:DoCal(document.form1.fDueDateBox);"
        HypCalTo.NavigateUrl = "javascript:DoCal(document.form1.tDueDateBox);"
        Me.errlab.Visible = False

        BindGrid() 'LW Added

    End Sub

    'LW Added part: to bind the drop down list
    Protected Sub BindGrid()

        If Not Page.IsPostBack Then

            objXmlTr.XmlFile = ConfigurationManager.AppSettings("XmlFilePath")
            'Label
            RacesLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0011")
            CustomerTypeLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0012")

            'List Customer type in drop down 
            Dim custtype As New clsrlconfirminf()
            Dim custParam As ArrayList = New ArrayList
            custParam = custtype.searchconfirminf("CUSTTYPE")
            Dim custcount As Integer
            Dim custid As String
            Dim custnm As String
            Me.CustomerTypeDLL.Items.Add(New ListItem("ALL", "ALL"))
            For custcount = 0 To custParam.Count - 1
                custid = custParam.Item(custcount)
                custnm = custParam.Item(custcount + 1)
                custcount = custcount + 1
                Me.CustomerTypeDLL.Items.Add(New ListItem(custnm.ToString(), custid.ToString()))

            Next

            'List Preferred Language in drop down - Added by LLY 201702
            Me.prefLangDLL.Items.Clear()
            Dim prefLang As New clsrlconfirminf()
            Dim langParam As ArrayList = New ArrayList
            langParam = prefLang.searchconfirminf("PREFLANG")
            Dim preflangcount As Integer
            Dim langid As String
            Dim langnm As String
            Me.prefLangDLL.Items.Add(New ListItem("ALL", "PREFLANG-ALL"))
            For preflangcount = 0 To langParam.Count - 1
                langid = langParam.Item(preflangcount)
                langnm = langParam.Item(preflangcount + 1)
                preflangcount = preflangcount + 1
                Me.prefLangDLL.Items.Add(New ListItem(langnm.ToString(), langid.ToString()))
            Next


            'List Race in drop down 
            Dim race As New clsrlconfirminf()
            Dim raceParam As ArrayList = New ArrayList
            raceParam = race.searchconfirminf("PERSONRACE")
            Dim racecount As Integer
            Dim raceid As String
            Dim racenm As String
            Me.RacesDLL.Items.Add(New ListItem("ALL", "ALL"))
            For racecount = 0 To raceParam.Count - 1
                raceid = raceParam.Item(racecount)
                racenm = raceParam.Item(racecount + 1)
                racecount = racecount + 1
                'If Not raceid.Equals("ZNONAPPL") Then
                Me.RacesDLL.Items.Add(New ListItem(racenm.ToString(), raceid.ToString()))
                'End If
            Next

        End If

    End Sub
  
    Sub RequestFormValue()
        If Request.Form("fDueDateBox") IsNot Nothing Then
            fDueDateBox.Text = Request.Form("fDueDateBox")
        End If

        If Request.Form("tDueDateBox") IsNot Nothing Then
            tDueDateBox.Text = Request.Form("tDueDateBox")
        End If
    End Sub

    Protected Sub ViewReportBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ViewReportBtn.Click
        RequestFormValue()

        GetRemiderDataset()

        'Dim ReminderReportDoc As ReportDocument
        'ReminderReportDoc = GetReminderReportDoc()
        'ReminderReportViewer.ReportSource = ReminderReportDoc '显示 CrystalReport 
        'ReminderReportViewer.Visible =False 
        'Session("ReminderReportDoc") = ReminderReportDoc
        'GeneratePDF()

       

    End Sub
    Private Function FormatMinData(ByVal MinData As String)
        Dim strTemp As String
        strTemp = MinData.ToUpper().Trim("A")
        If strTemp = "" Then 'MinDate 为空或全是A
            Return "A"
        Else
            Return MinData
        End If
    End Function
    Private Function FormatMaxData(ByVal MaxData As String)
        Dim strTemp As String
        strTemp = MaxData.ToUpper().Trim("Z")
        If strTemp = "" Then
            Return "Z"
        Else
            Return MaxData
        End If
    End Function

    Private Function GetReminderDataSet() As DataSet
        Dim Reminder As New clsreminder
        Reminder.FromModeType = FormatMinData(fModeTypeBox.Text)
        Reminder.ToModeType = FormatMaxData(tModeTypeBox.Text)
        Reminder.FromStatID = FormatMinData(fStatIDBox.Text)
        Reminder.ToStatID = FormatMaxData(tStatIDBox.Text)
        Reminder.StartingAreaID = FormatMinData(fAreaIDBox.Text)
        Reminder.EndingAreaID = FormatMaxData(tAreaIDBox.Text)
        Reminder.FromServerCenterID = FormatMinData(fSvcIDBox.Text)
        Reminder.ToServerCenterID = FormatMaxData(tSvcIDBox.Text)
        Reminder.StartingDueDate = fDueDateBox.Text
        Reminder.EndingDueDate = tDueDateBox.Text
        Reminder.CustomerType = CustomerTypeDLL.SelectedValue
        Reminder.CustomerRace = RacesDLL.SelectedValue
        Reminder.PrefLang = prefLangDLL.SelectedValue.Substring(9, prefLangDLL.SelectedValue.ToString.Length - 9) 'PREFLANG-MALAY

        Dim ds As DataSet
        If IncludeCHKBOX.Checked = True Then
            ds = Reminder.GetReminder(Session("userID"), "1")
        Else
            ds = Reminder.GetReminder(Session("userID"), "0")
        End If
        Return ds
    End Function

    Sub GetRemiderDataset()
        Dim ds As DataSet = GetReminderDataSet()

        Try
            totalCusBox.Text = ds.Tables("reminder").Rows.Count
            If ds.Tables("reminder").Rows.Count <> 0 Then
                GeneratePdfBtn.Visible = True

                Session("ReminderDs") = ds
            Else
                GeneratePdfBtn.Visible = False
            End If
            If ds.Tables("Mobileno").Rows.Count <> 0 Then
                OutPutTextBtn.Visible = True
            Else
                OutPutTextBtn.Visible = False
            End If

        Catch ex As Exception

        End Try
    End Sub


    Private Function GetReminderReportDoc() As ReportDocument
        Dim ds As DataSet = GetReminderDataSet()
        Try
            totalCusBox.Text = ds.Tables("reminder").Rows.Count
            If ds.Tables("reminder").Rows.Count <> 0 Then
                GeneratePdfBtn.Visible = True

                Session("ReminderDs") = ds
            Else
                GeneratePdfBtn.Visible = False
            End If
            If ds.Tables("Mobileno").Rows.Count <> 0 Then
                OutPutTextBtn.Visible = True
            Else
                OutPutTextBtn.Visible = False
            End If

            ReminderReportDoc.Load(MapPath("reminder.rpt"))
            ReminderReportDoc.SetDataSource(ds.Tables("reminder"))

            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            Dim i As Integer
            For i = 1 To ReminderReportDoc.ParameterFields.Count
                ReminderReportDoc.SetParameterValue("id" + i.ToString(), objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-REMINDER-RPTH" + i.ToString()))
            Next i

            Return ReminderReportDoc
        Catch ex As Exception
            Response.Redirect("~/PresentationLayer/error.aspx")
            Return Nothing
        End Try
    End Function

    Sub GeneratePDF()
        Dim ReminderReportDoc As ReportDocument
        If Session("ReminderReportDoc") Is Nothing Then
            ReminderReportDoc = GetReminderReportDoc()
        Else 'CrystalReport 已经显示
            ReminderReportDoc = Session("ReminderReportDoc")
        End If
        '向客户端传文件
        Dim sr As Stream = ReminderReportDoc.ExportToStream(ExportFormatType.PortableDocFormat)
        Response.Clear()
        Dim bSuccess As Boolean = dowload.ResponseFile(Request, Response, "Reminder.pdf", sr, 1024000)
        If bSuccess = False Then

        End If
        sr.Close()
        Response.End()
    End Sub
    Protected Sub GeneratePdfBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles GeneratePdfBtn.Click

        Dim script As String = "window.open('ReminderReport.aspx')"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ReminderReport", script, True)
        Return
    End Sub

    Protected Sub OutPutTextBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles OutPutTextBtn.Click
        Dim ds As DataSet = GetReminderDataSet()
        If ds Is Nothing Then
            Response.Redirect("~/PresentationLayer/error.aspx")
        End If

        If ds.Tables("Mobileno").Rows.Count = 0 Then
            Return
        End If

        Dim Mobileno As New StringBuilder
        Dim i As Integer
        For i = 0 To ds.Tables("Mobileno").Rows.Count - 1
            Mobileno.Append(ds.Tables("Mobileno").Rows(i).Item(0).ToString() + ";")
            'Mobileno.AppendLine()
        Next

        Dim sr As New MemoryStream
        Dim bw As New BinaryWriter(sr)
        bw.Write(Mobileno.ToString().ToCharArray())


        '向客户端传文件
        Response.Clear()
        Dim bSuccess As Boolean = dowload.ResponseFile(Request, Response, "mobileno.txt", sr, 1024000)
        If bSuccess = False Then
            'Response.Redirect("~/PresentationLayer/error.aspx")
        End If
        sr.Close()
        Response.End()
    End Sub

    Protected Sub UploadBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles UploadBtn.Click
       
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Try
            If Not (UploadFile.PostedFile Is Nothing) Then
                Dim lstrFileFolder As String = ConfigurationSettings.AppSettings("FTPPath")
                If (Not Directory.Exists(lstrFileFolder)) Then
                    Directory.CreateDirectory(lstrFileFolder)
                End If

                Dim strFilePath As String = UploadFile.PostedFile.FileName
                If strFilePath = "" Then
                    Me.errlab.Visible = True
                    Me.errlab.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-REMINDER-FPATHEMPTY")
                    Return
                End If
                Dim strFileName As String = Path.GetFileName(strFilePath)
                UploadFile.PostedFile.SaveAs(lstrFileFolder + strFileName) '向服务器端传文件
                Me.errlab.Visible = True
                Me.errlab.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-REMINDER-UPSUCCESS")
            End If
        Catch ex As Exception
            Me.errlab.Visible = True
            Me.errlab.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-REMINDER-UPFAIL")
        End Try
    End Sub

    Protected Sub fDueDateCalendar_SelectedDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles fDueDateCalendar.SelectedDateChanged
        tDueDateBox.Text = CDate(fDueDateBox.Text).AddMonths(1)
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        ReminderReportDoc.Dispose()

    End Sub

    'LW Added part : Ajax 
    <AjaxPro.AjaxMethod()> _
        Public Function GetRaces(ByVal strCustomerType As String) As ArrayList
        Dim race As New clsrlconfirminf()
        Dim raceParam As ArrayList = New ArrayList
        raceParam = race.searchconfirminf("PERSONRACE")
        Dim racecount As Integer
        Dim raceid As String
        Dim racenm As String
        Dim Listas As New ArrayList(raceParam.Count / 2)
        Dim icnt As Integer = 0
        Dim strTemp As String

        If strCustomerType = "CORPORATE" Then

            Listas.Add("ALL:ALL")
            For racecount = 0 To raceParam.Count - 1
                raceid = raceParam.Item(racecount)
                racenm = raceParam.Item(racecount + 1)
                racecount = racecount + 1
                If raceid.Equals("ZNONAPPL") Then
                    strTemp = raceid & ":" & racenm
                    Listas.Add(strTemp)
                End If

            Next
        ElseIf strCustomerType = "INDIVIDUAL" Then

            Listas.Add("ALL:ALL")
            For racecount = 0 To raceParam.Count - 1
                raceid = raceParam.Item(racecount)
                racenm = raceParam.Item(racecount + 1)
                racecount = racecount + 1
                If Not raceid.Equals("ZNONAPPL") Then
                    strTemp = raceid & ":" & racenm
                    Listas.Add(strTemp)
                End If

            Next
        ELSE
            Listas.Add("ALL:ALL")
            For racecount = 0 To raceParam.Count - 1
                raceid = raceParam.Item(racecount)
                racenm = raceParam.Item(racecount + 1)
                racecount = racecount + 1

                strTemp = raceid & ":" & racenm
                Listas.Add(strTemp)

            Next

        End If

        Return Listas
    End Function
End Class
