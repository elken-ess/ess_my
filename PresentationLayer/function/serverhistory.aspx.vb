﻿Imports BusinessEntity
Imports System.Data
Partial Class PresentationLayer_function_ServerHistory
    Inherits System.Web.UI.Page
    Dim objXmlTr As New clsXml
    Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
    Dim apptEntity As New clsAppointment()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            Me.titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-SERVHISTORY")
            Me.ROInfoLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-ROINFO")
            Me.custNameLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-CUSTNAME")
            Me.backLink.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-BACK")
            backLink.Attributes("onclick") = "javascript:history.back();"
            Me.ROInfoBox.Text = Request.Params("ROINFO").ToString()
            Me.custNameBox.Text = Request.Params("custName").ToString()
            BindGrid()
        ElseIf ServerHistoryView.Rows.Count > 0 Then
            DispGridViewHead()
        End If
    End Sub
    Protected Sub BindGrid()
        apptEntity.ROID = Request.Params("ROID").ToString()
        apptEntity.CustomerID = Request.Params("custID").ToString()
        apptEntity.CustomerPrifx = Request.Params("custPf").ToString()
        Dim ds As DataSet = apptEntity.GetServerHistory()
        If ds Is Nothing Then
            Response.Redirect("~/PresentationLayer/error.aspx")
        End If
        Session("ServerHistoryView") = ds
        Me.ServerHistoryView.DataSource = ds
        ServerHistoryView.AllowPaging = True
        ServerHistoryView.AllowSorting = True
        Me.ServerHistoryView.DataBind()
        If ds.Tables(0).Rows.Count > 0 Then
            DispGridViewHead()
        End If
    End Sub
    Private Sub DispGridViewHead()
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        ServerHistoryView.HeaderRow.Cells(0).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-TRANSTYPE") 'transaction Type
        ServerHistoryView.HeaderRow.Cells(0).Font.Size = 8
        ServerHistoryView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-TRANSNO") 'transactionNo
        ServerHistoryView.HeaderRow.Cells(1).Font.Size = 8
        ServerHistoryView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "RPT_APT_SERDATE")
        ServerHistoryView.HeaderRow.Cells(2).Font.Size = 8
        'ServerHistoryView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CHKS-STIME")
        'ServerHistoryView.HeaderRow.Cells(3).Font.Size = 8
        'ServerHistoryView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CHKS-ETIME")
        'ServerHistoryView.HeaderRow.Cells(4).Font.Size = 8
        ServerHistoryView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-DETAIL-TCHID")
        ServerHistoryView.HeaderRow.Cells(3).Font.Size = 8
    End Sub

    Protected Sub ServerHistoryView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles ServerHistoryView.PageIndexChanging
        ServerHistoryView.PageIndex = e.NewPageIndex
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Dim ds As DataSet = Session("ServerHistoryView")
        ServerHistoryView.DataSource = ds
        ServerHistoryView.DataBind()
        If ds.Tables(0).Rows.Count > 0 Then
            DispGridViewHead()
        End If
    End Sub
End Class
