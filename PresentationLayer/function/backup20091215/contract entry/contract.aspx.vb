﻿Imports BusinessEntity
Imports System.Data
Partial Class PresentationLayer_function_contract
    Inherits System.Web.UI.Page
    Dim objXmlTr As New clsXml
    Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
    Dim ContactEntity As New clsContract
    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try

        Me.lblNoRecord.Visible = False
        If Not Page.IsPostBack Then

            'label message
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            Me.titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CNT-TITL")
            Me.cntNoLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CNT-CNTNO")
            Me.servCIDLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CNT-SERVC")
            Me.custNameLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CNT-CUSTNAME")
            Me.telNoLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CNT-TELNO")
            Me.cntStatusLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CNT-STATUS")
            Me.addButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Add")
            Me.searchButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Search")
            Dim strall As String = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_CLA_RELEASED3")
            Me.lblNoRecord.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-NR")
            Me.lblCustomerID.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0001")


            'dropdownlist apointment status
            Dim apptStat As New clsUtil()
            Dim statParam As ArrayList = New ArrayList
            statParam = apptStat.searchconfirminf("CNTStatus")
            Dim count As Integer
            Dim statid As String
            Dim statnm As String
            For count = 0 To statParam.Count - 1
                statid = statParam.Item(count)
                statnm = statParam.Item(count + 1)
                count = count + 1
                cntStatusDrpList.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
            Next
            cntStatusDrpList.Items.Add(New ListItem(strall, strall))
            cntStatusDrpList.Items(0).Selected = True
            BindGrid(Session("userID"))


            '''access control'''''''''''''''''''''''''''''''''''''''''''''''
            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "23")

            Me.addButton.Enabled = purviewArray(0)



        ElseIf cntView.Rows.Count > 0 Then
            DispGridViewHead()
        End If
    End Sub
    Protected Sub BindGrid(ByVal strUserID As String)
        'objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        'Me.lblNoRecord.Visible = False
        'With ContactEntity
        '    .ContractNO = Me.cntNoBox.Text.ToString()
        '    .ServerCenterID = Me.servCIDBox.Text.ToString()
        '    .ContractStatus = Me.cntStatusDrpList.SelectedValue.ToString()
        '    .CustomName = Me.custNameBox.Text
        '    .TelphoneNo = Me.telNoBox.Text
        '    .SerialNo = txtSerialNo.Text
        'End With

        'Dim ds As DataSet = ContactEntity.GetContracts(strUserID)

        'If ds Is Nothing Then
        '    Return
        'End If
        'Me.cntView.DataSource = ds
        cntView.AllowPaging = True
        cntView.AllowSorting = True
        Dim editcol As New CommandField
        editcol.EditText = objXmlTr.GetLabelName("EngLabelMsg", "BB-Edit") '"edit"
        editcol.HeaderImageUrl = "~/PresentationLayer/graph/edit.gif"
        editcol.ShowEditButton = True
        cntView.Columns.Add(editcol)



        '''access control'''''''''''''''''''''''''''''''''''''''''''''''
        Dim accessgroup As String = Session("accessgroup").ToString
        Dim purviewArray As Array = New Boolean() {False, False, False, False}
        purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "23")

        editcol.Visible = purviewArray(2)


        'Me.cntView.DataBind()
        'Me.lblNoRecord.Visible = True
        'If ds.Tables(0).Rows.Count > 0 Then


        '    Me.lblNoRecord.Visible = False
        '    DispGridViewHead()
        'End If
    End Sub
    Private Sub DispGridViewHead()
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        cntView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CNT-CNTNO")
        cntView.HeaderRow.Cells(1).Font.Size = 8
        cntView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CNT-CUSTID")
        cntView.HeaderRow.Cells(2).Font.Size = 8

        cntView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0002")
        cntView.HeaderRow.Cells(3).Font.Size = 8

        cntView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CNT-SVCTY")
        cntView.HeaderRow.Cells(4).Font.Size = 8
        cntView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CNT-CNTSDATE")
        cntView.HeaderRow.Cells(5).Font.Size = 8
        cntView.HeaderRow.Cells(6).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CNT-CNTEDATE")
        cntView.HeaderRow.Cells(6).Font.Size = 8
        cntView.HeaderRow.Cells(7).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CNT-SERVC")
        cntView.HeaderRow.Cells(7).Font.Size = 8
        cntView.HeaderRow.Cells(8).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CNT-STATUS")
        cntView.HeaderRow.Cells(8).Font.Size = 8
    End Sub

    Protected Sub cntView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles cntView.PageIndexChanging
        cntView.PageIndex = e.NewPageIndex
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        With ContactEntity
            .ContractNO = Me.cntNoBox.Text.ToString()
            .ServerCenterID = Me.servCIDBox.Text.ToString()
            .ContractStatus = Me.cntStatusDrpList.SelectedValue.ToString()
            .CustomName = Me.custNameBox.Text
            .CustomerID = Me.txtCustomerID.Text
            .TelphoneNo = Me.telNoBox.Text
            .SerialNo = txtSerialNo.Text
        End With

        Dim ds As DataSet = ContactEntity.GetContracts(Session("userID"))

        cntView.DataSource = ds
        cntView.DataBind()
        If ds.Tables(0).Rows.Count > 0 Then
            DispGridViewHead()
        End If
    End Sub

    Protected Sub searchButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles searchButton.Click
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        'search condition

        With ContactEntity
            .ContractNO = Me.cntNoBox.Text.ToString()
            .ServerCenterID = Me.servCIDBox.Text.ToString()
            .ContractStatus = Me.cntStatusDrpList.SelectedValue.ToString()
            .CustomName = Me.custNameBox.Text
            .CustomerID = Me.txtCustomerID.Text
            .TelphoneNo = Me.telNoBox.Text
            .SerialNo = txtSerialNo.Text
        End With

        Dim selds As DataSet = ContactEntity.GetContracts(Session("userID"))
        If selds Is Nothing Then
            cntView.Visible = False
            Return
        End If
        cntView.Visible = True
        Me.cntView.DataSource = selds

        Me.cntView.DataBind()
        Me.lblNoRecord.Visible = True
        If selds.Tables(0).Rows.Count > 0 Then
            Me.lblNoRecord.Visible = False
            DispGridViewHead()
        End If
    End Sub

    Protected Sub cntView_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles cntView.RowEditing

        Dim cntNO As String = cntView.Rows(e.NewEditIndex).Cells(1).Text.Trim() 'ds.Tables(0).Rows(cntView.PageIndex * cntView.PageSize + e.NewEditIndex).Item(0).ToString()
        cntNO = Server.UrlEncode(cntNO)
        Dim strTempURL As String = "contractNo=" + cntNO
        strTempURL = "~/PresentationLayer/function/modifycontract.aspx?" + strTempURL
        Response.Redirect(strTempURL)
    End Sub
End Class
