<%@ Page Language="VB" AutoEventWireup="false" EnableEventValidation="false" CodeFile="ServiceBillAdd.aspx.vb"
    Inherits="PresentationLayer_function_ServiceBillAdd" %>

<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc1" %>
<%@ Register Assembly="JCalendar" Namespace="JCalendar" TagPrefix="cc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Update Service Bill Part 1</title>
    <meta http-equiv="Content-Type" content="text/html; charset=gb2312">
    <link href="../css/style.css" type="text/css" rel="stylesheet">
</head>

<script language="JavaScript" src="../js/common.js"></script>

<body>
    <script language="JavaScript" type="text/javascript">
        function LoadDiscount_CallBack(response) {
            //if the server-side code threw an exception
            if (response.error != null) {
                //we should probably do better than this
                alert(response.error);
                return;
            }

            var ResponseValue = response.value;
            document.getElementById("<%=txtTotalNet.ClientID%>").value = ResponseValue;
            document.getElementById("<%=txtCash.ClientID%>").focus();
        }

        function FormatDiscount_CallBack(response) {

            //if the server-side code threw an exception
            if (response.error != null) {
                //we should probably do better than this
                alert(response.error);
                return;
            }

            var ResponseValue = response.value;
            document.getElementById("<%=txtDiscount.ClientID%>").value = ResponseValue;
        }

        function LoadDiscount(objectClient) {
            var GrossTotal = document.getElementById("<%=txtTotalGross.ClientID%>").value;
            var Discount = objectClient.value;

            PresentationLayer_function_ServiceBillAdd.FormatNumber(Discount, FormatDiscount_CallBack);
            PresentationLayer_function_ServiceBillAdd.DisplayNetAfterDiscount(Discount, GrossTotal, LoadDiscount_CallBack);
        }

        function LoadTotalPayment_CallBack(response) {
            //if the server-side code threw an exception
            if (response.error != null) {
                //we should probably do better than this
                alert(response.error);
                return;
            }

            var ResponseValue = response.value;
            document.getElementById("<%=txtTotal.ClientID%>").value = ResponseValue;
        }

        function FormatCash_CallBack(response) {
            //if the server-side code threw an exception
            if (response.error != null) {
                //we should probably do better than this
                alert(response.error);
                return;
            }

            var ResponseValue = response.value;
            document.getElementById("<%=txtCash.ClientID%>").value = ResponseValue;
        }

        function FormatCheque_CallBack(response) {
            //if the server-side code threw an exception
            if (response.error != null) {
                //we should probably do better than this
                alert(response.error);
                return;
            }

            var ResponseValue = response.value;
            document.getElementById("<%=txtCheque.ClientID%>").value = ResponseValue;
        }

        function FormatCredit_CallBack(response) {
            //if the server-side code threw an exception
            if (response.error != null) {
                //we should probably do better than this
                alert(response.error);
                return;
            }

            var ResponseValue = response.value;
            document.getElementById("<%=txtCredit.ClientID%>").value = ResponseValue;
        }

        function FormatCreditcard_CallBack(response) {
            //if the server-side code threw an exception
            if (response.error != null) {
                //we should probably do better than this
                alert(response.error);
                return;
            }

            var ResponseValue = response.value;
            document.getElementById("<%=txtCreditCard.ClientID%>").value = ResponseValue;
        }

        function FormatOther_CallBack(response) {
            //if the server-side code threw an exception
            if (response.error != null) {
                //we should probably do better than this
                alert(response.error);
                return;
            }

            var ResponseValue = response.value;
            document.getElementById("<%=txtOther.ClientID%>").value = ResponseValue;
        }

        function LoadTotalPayment() {
            var cash = document.getElementById("<%=txtCash.ClientID%>").value;
            var cheque = document.getElementById("<%=txtCheque.ClientID%>").value;
            var credit = document.getElementById("<%=txtCredit.ClientID%>").value;
            var creditcard = document.getElementById("<%=txtCreditCard.ClientID%>").value;
            var other = document.getElementById("<%=txtOther.ClientID%>").value;

            PresentationLayer_function_ServiceBillAdd.FormatNumber(cash, FormatCash_CallBack);
            PresentationLayer_function_ServiceBillAdd.FormatNumber(cheque, FormatCheque_CallBack);
            PresentationLayer_function_ServiceBillAdd.FormatNumber(credit, FormatCredit_CallBack);
            PresentationLayer_function_ServiceBillAdd.FormatNumber(creditcard, FormatCreditcard_CallBack);
            PresentationLayer_function_ServiceBillAdd.FormatNumber(other, FormatOther_CallBack);
            PresentationLayer_function_ServiceBillAdd.DisplayPayment(cash, cheque, credit, creditcard, other, LoadTotalPayment_CallBack);
        }

        function LoadTechnician_CallBack(response) {
            //if the server-side code threw an exception
            if (response.error != null) {
                //we should probably do better than this
                alert(response.error);
                return;
            }

            var ResponseValue = response.value;

            //if the response wasn't what we expected  
            if (ResponseValue == null || typeof (ResponseValue) != "object") {
                return;
            }

            //Get the states drop down
            var ResultList = document.getElementById("<%=cboTechnician.ClientID%>");
            ResultList.options.length = 0; //reset the states dropdown

            //Remember, its length not Length in JavaScript
            for (var i = 0; i < ResponseValue.length; ++i) {

                //the columns of our rows are exposed like named properties
                var al = ResponseValue[i].split(":");
                var alid = al[0];
                var alname = al[1];
                ResultList.options[ResultList.options.length] = new Option(alname, alid);
            }
        }

        function LoadTechnician(objectClient) {
            if (objectClient.selectedIndex > 0) {
                var countryid = document.getElementById("<%=cboCountry.ClientID%>").options[document.getElementById("<%=cboCountry.ClientID%>").selectedIndex].value;
                var companyid = document.getElementById("<%=cboCompany.ClientID%>").options[document.getElementById("<%=cboCompany.ClientID%>").selectedIndex].value;
                var servicecenterid = document.getElementById("<%=cboServiceCenter.ClientID%>").options[document.getElementById("<%=cboServiceCenter.ClientID%>").selectedIndex].value;
                var rank = '<%=Session("login_rank")%>';

                PresentationLayer_function_ServiceBillAdd.GetTechnicianList(countryid, companyid, servicecenterid, rank, LoadTechnician_CallBack);
                PresentationLayer_function_ServiceBillAdd.GetTaxRate(countryid, servicecenterid, rank, LoadTax_CallBack);
            }
            else {
                document.getElementById("<%=cboTechnician.ClientID%>").options.length = 0;
            }
        }

        function LoadServiceCenter_CallBack(response) {
            //if the server-side code threw an exception
            if (response.error != null) {
                //we should probably do better than this
                alert(response.error);
                return;
            }

            var ResponseValue = response.value;

            //if the response wasn't what we expected  
            if (ResponseValue == null || typeof (ResponseValue) != "object") {
                return;
            }

            //Get the states drop down
            var ResultList = document.getElementById("<%=cboServiceCenter.ClientID%>");
            ResultList.options.length = 0; //reset the states dropdown

            //Remember, its length not Length in JavaScript
            for (var i = 0; i < ResponseValue.length; ++i) {

                //the columns of our rows are exposed like named properties
                var al = ResponseValue[i].split(":");
                var alid = al[0];
                var alname = al[1];
                ResultList.options[ResultList.options.length] = new Option(alname, alid);
            }

            if (ResponseValue.length = 0) {
                document.getElementById("<%=cboTechnician.ClientID%>").options.length = 0;
            }
        }

        function LoadTax_CallBack(response) {
            //if the server-side code threw an exception
            if (response.error != null) {
                //we should probably do better than this
                alert(response.error);
                return;
            }

            var ResponseValue = response.value;

            var al = ResponseValue.split(":");
            var alid = al[0];
            var alname = al[1];
            document.getElementById("<%=txtTaxID.ClientID%>").value = alid;
            document.getElementById("<%=txtTaxRate.ClientID%>").value = alname;
        }

        function LoadServiceCenter(objectClient) {
            document.getElementById("<%=cboTechnician.ClientID%>").options.length = 0;
            if (objectClient.selectedIndex > 0) {
                var countryid = document.getElementById("<%=cboCountry.ClientID%>").options[document.getElementById("<%=cboCountry.ClientID%>").selectedIndex].value;
                var companyid = document.getElementById("<%=cboCompany.ClientID%>").options[document.getElementById("<%=cboCompany.ClientID%>").selectedIndex].value;

                var servicecenterid
                if (document.getElementById("<%=cboServiceCenter.ClientID%>").options.length != 0) {
                    servicecenterid = document.getElementById("<%=cboServiceCenter.ClientID%>").options[document.getElementById("<%=cboServiceCenter.ClientID%>").selectedIndex].value;
                }
                else {
                    servicecenterid = "";
                }
                var rank = '<%=Session("login_rank")%>';
                document.getElementById("<%=txtTaxID.ClientID%>").value = "";
                document.getElementById("<%=txtTaxRate.ClientID%>").value = "";
                PresentationLayer_function_ServiceBillAdd.GetServiceCenterList(countryid, companyid, servicecenterid, rank, LoadServiceCenter_CallBack);
            }
            else {
                document.getElementById("<%=cboServiceCenter.ClientID%>").options.length = 0
                document.getElementById("<%=cboTechnician.ClientID%>").options.length = 0
            }
        }

        function LoadPart_CallBack(response) {
            //if the server-side code threw an exception
            if (response.error != null) {
                //we should probably do better than this
                alert(response.error);
                return;
            }

            var ResponseValue = response.value;
            var al = ResponseValue.split(":");
            var errormsg = al[0];
            var partname = al[1];

            if (errormsg == "") {
                document.getElementById("<%=txtPartName.ClientID%>").value = partname;
            }
            else {
                document.getElementById("<%=txtPartCode.ClientID%>").value = "";
                document.getElementById("<%=txtPartName.ClientID%>").value = errormsg;
            }
        }

        function LoadPart() {
            var countryid = document.getElementById("<%=cboCountry.ClientID%>").options[document.getElementById("<%=cboCountry.ClientID%>").selectedIndex].value;
            var partid = document.getElementById("<%=txtPartCode.ClientID%>").value;
            var priceid;

            var priceIDDropdown = document.getElementById("cboPriceID");
            if (priceIDDropdown.selectedIndex > 0) {
                priceid = priceIDDropdown.options[priceIDDropdown.selectedIndex].value;
            }
            else {
                priceid = priceIDDropdown.options.length = 0;
            }

            var rank = '<%= Session("login_rank")%>';

            window.PresentationLayer_function_ServiceBillAdd.GetPartCodeAndName(countryid, partid, priceid, LoadPart_CallBack);
            window.PresentationLayer_function_ServiceBillAdd.PopulatePriceList(countryid, partid, rank, PopulatePrice_Callback);
        }

        // Added by Ryan Estandarte 5 March 2012
        function PopulatePrice_Callback(response) {
            if (response.error != null) {
                alert(response.error);
                return;
            }

            var value = response.value;

            if (value == null || typeof (value) != "object") {
                return;
            }

            var priceDropdown = document.getElementById("<%=cboPriceID.ClientID%>");
            priceDropdown.options.length = 0;

            for (var i = 0; i < value.length; i++) {
                var item = value[i].split(":");
                var id = item[0];
                var name = item[1];

                priceDropdown.options[priceDropdown.options.length] = new Option(name, id);
            }
        }

        function LoadPrice_CallBack(response) {
            //if the server-side code threw an exception
            if (response.error != null) {
                //we should probably do better than this
                alert(response.error);
                return;
            }

            var ResponseValue = response.value;

            var al = ResponseValue.split(":");
            var errormsg = al[0];
            var price = al[1];

            if (errormsg == "") {

                document.getElementById("<%=txtUnitPrice.ClientID%>").value = price;
                document.getElementById("<%=txtQty.ClientID%>").value = 1;
                //      document.getElementById("<%=txtQty.ClientID%>").focus();
            }
            else {
                //        alert(errormsg);
                //document.getElementById("txtPriceID").value = "";

                document.getElementById("<%=txtQty.ClientID%>").value = 1;
                document.getElementById("<%=txtUnitPrice.ClientID%>").value = errormsg;
                document.getElementById("cboPriceID").selectedIndex = 0;
            }
        }

        function LoadPrice(objectClient) {
            var countryid = document.getElementById("<%=cboCountry.ClientID%>").options[document.getElementById("<%=cboCountry.ClientID%>").selectedIndex].value;
            var partid = document.getElementById("<%=txtPartCode.ClientID%>").value;
            var priceid = document.getElementById("cboPriceID").options[document.getElementById("cboPriceID").selectedIndex].value;
            var rank = '<%= Session("login_rank")%>';

            if (objectClient.selectedIndex > 0) {
                window.PresentationLayer_function_ServiceBillAdd.GetPriceIDAndPrice(countryid, partid, priceid, LoadPrice_CallBack);

                var qty = document.getElementById("<%=txtQty.ClientID%>").value;
                var unitprice = document.getElementById("txtUnitPrice").value;

                window.PresentationLayer_function_ServiceBillAdd.GetLineAmount(qty, unitprice, LoadLineAmount_CallBack);
            }
            else {
                window.PresentationLayer_function_ServiceBillAdd.PopulatePriceList(countryid, partid, rank, PopulatePrice_Callback);
                document.getElementById("txtUnitPrice").value = "";
            }
        }

        function LoadLineAmount_CallBack(response) {
            //if the server-side code threw an exception
            if (response.error != null) {
                //we should probably do better than this
                alert(response.error);
                return;
            }

            var ResponseValue = response.value;
            var al = ResponseValue.split(":");
            var errormsg = al[0];
            var qty = al[1];
            var unitprice = al[2];
            var lineamount = al[3];

            if (errormsg == "") {
            }
            else {
                alert(errormsg);
                document.getElementById("<%=txtQty.ClientID%>").focus();
            }

            document.getElementById("<%=txtLineTotal.ClientID%>").value = lineamount;
        }

        function LoadLineAmount() {
            var qty = document.getElementById("<%=txtQty.ClientID%>").value;
            var unitprice = document.getElementById("<%=txtUnitPrice.ClientID%>").value;

            PresentationLayer_function_ServiceBillAdd.GetLineAmount(qty, unitprice, LoadLineAmount_CallBack);
        }
    </script>

    <form id="form1" runat="server">
    <div>
        <table bgcolor="#b7e6e6" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
            <tr>
                <td background="../graph/title_bg.gif" width="1%">
                    <img height="24" src="../graph/title1.gif" width="5" />
                </td>
                <td background="../graph/title_bg.gif" class="style2" width="100%">
                    <asp:Label ID="titleLab" runat="server" Text="Add Service Bill"></asp:Label>
                </td>
            </tr>
        </table>
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td background="../graph/title_bg.gif" class="style2" style="height: 14px" width="100%">
                    <font color="red">
                        <asp:Label ID="lblError" runat="server" Visible="False"></asp:Label></font>
                </td>
            </tr>
        </table>
        <table bgcolor="#b7e6e6" width="100%">
            <tr bgcolor="#ffffff">
                <td colspan="4">
                    <asp:Label ID="lblNoteUp" runat="server" ForeColor="Red" Text="Label"></asp:Label>
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td colspan="4">
                    &nbsp;
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td style="width: 15%; height: 24px;">
                    <asp:Label ID="lblServiceBillType" runat="server" Text="Service Bill Type" Width="104px"></asp:Label>
                </td>
                <td style="width: 35%; height: 24px;">
                    <asp:DropDownList ID="cboServiceBillType" runat="server" Width="145px" AutoPostBack="True"
                                      ValidationGroup="ServBillGrp"/>
                    <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Size="Small" ForeColor="Red"
                        Text="*"></asp:Label>
                    <asp:RequiredFieldValidator ID="rfvServiceBillType" 
                                                runat="server"
                                                ControlToValidate="cboServiceBillType" 
                                                ErrorMessage="*" 
                                                ForeColor="White" 
                                                ValidationGroup="ServBillGrp"
                                                Text="*"/>
                </td>
                <td style="width: 15%; height: 24px;">
                    <asp:Label ID="lblInvoiceNo" runat="server" Text="Invoice #" Width="104px"></asp:Label>
                </td>
                <td style="width: 36%; height: 24px;">
                    <asp:TextBox ID="txtInvoicePf" runat="server" ReadOnly="True" Width="20%"></asp:TextBox>&nbsp;
                    <asp:TextBox ID="txtInvoiceNo" runat="server" ReadOnly="True" Width="70%"></asp:TextBox>
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td style="width: 15%; height: 31px;">
                    <asp:Label ID="lblDate" runat="server" Text="Service Order Date" Width="133px"></asp:Label><br />
                    <asp:Label ID="Label11" runat="server" Text="DD/MM/YYYY"></asp:Label>
                </td>
                <td style="width: 35%; height: 31px;">
                    <asp:TextBox ID="txtDate" runat="server" Style="width: 135px;" MaxLength="10" ValidationGroup="ServBillGrp"/>
                    &nbsp;
                    <cc2:JCalendar
                        ID="JCalendar1" runat="server" ControlToAssign="JDATEBox" ImgURL="~/PresentationLayer/graph/calendar.gif"
                        Visible="False"></cc2:JCalendar>
                    <asp:HyperLink ID="HypCal" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                        ToolTip="Choose a Date">Choose a Date</asp:HyperLink>
                    &nbsp; &nbsp; &nbsp;&nbsp;
                    <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Size="Small" ForeColor="Red"
                        Text="     *"></asp:Label>
                    <asp:RequiredFieldValidator ID="rfvDate" runat="server"
                                                ControlToValidate="txtDate" 
                                                ErrorMessage="*" 
                                                ForeColor="White" 
                                                Text="*"
                                                ValidationGroup="ServBillGrp"/>
                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                        CausesValidation="False" Enabled="False" Visible="False" />
                    <asp:Calendar ID="Calendar1" runat="server" BackColor="White" BorderColor="#3366CC"
                        BorderWidth="1px" CellPadding="1" DayNameFormat="Shortest" Font-Names="Verdana"
                        Font-Size="8pt" ForeColor="#003399" Height="200px" ShowGridLines="True" Visible="False"
                        Width="220px" Enabled="False">
                        <SelectedDayStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                        <TodayDayStyle BackColor="#99CCCC" ForeColor="White" />
                        <SelectorStyle BackColor="#99CCCC" ForeColor="#336666" />
                        <WeekendDayStyle BackColor="#CCCCFF" />
                        <OtherMonthDayStyle ForeColor="#999999" />
                        <NextPrevStyle Font-Size="8pt" ForeColor="#CCCCFF" />
                        <DayHeaderStyle BackColor="#99CCCC" ForeColor="#336666" Height="1px" />
                        <TitleStyle BackColor="#003399" BorderColor="#3366CC" BorderWidth="1px" Font-Bold="True"
                            Font-Size="10pt" ForeColor="#CCCCFF" Height="25px" />
                    </asp:Calendar>
                    <asp:CompareValidator ID="CompDateVal" runat="server" ControlToValidate="txtDate"
                                          Operator="DataTypeCheck" SetFocusOnError="True" Type="Date"
                                          ValidationGroup="ServBillGrp"/>
                </td>
                <td style="width: 15%; height: 31px;" valign="top">
                    <asp:Label ID="lblServiceBillNo" runat="server" Text="Service Bill #" Width="100%"></asp:Label>
                </td>
                <td style="width: 36%; height: 31px;" valign="top">
                    <asp:TextBox ID="txtServiceBillNo" runat="server" MaxLength="20" Width="50%" ValidationGroup="ServBillGrp"/>
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td style="width: 15%; height: 23px;">
                    <asp:Label ID="lblTechnician" runat="server" Text="Technician" Width="104px"></asp:Label>
                </td>
                <td style="width: 35%; height: 23px;">
                    <asp:DropDownList ID="cboTechnician" runat="server" Width="90%" ValidationGroup="ServBillGrp"/>
                    <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Size="Small" ForeColor="Red"
                        Text="*"></asp:Label>
                    <asp:RequiredFieldValidator ID="rfvTechnician" runat="server"
                                                ControlToValidate="cboTechnician" ErrorMessage="*" ForeColor="White"
                                                Text="*" ValidationGroup="ServBillGrp"/>
                </td>
                <td style="width: 15%; height: 23px;">
                    <asp:Label ID="lblServiceType" runat="server" Text="Service Type" Width="104px"></asp:Label>
                </td>
                <td style="width: 36%; height: 23px;">
                    <asp:DropDownList ID="cboServiceType" runat="server" Width="90%" AutoPostBack="True" ValidationGroup="ServBillGrp"/>
                    <asp:Label ID="Label7" runat="server" Font-Bold="True" Font-Size="Small" ForeColor="Red"
                        Text="*"></asp:Label>
                    <asp:RequiredFieldValidator ID="rfvServiceType" runat="server"
                                                ControlToValidate="cboServiceType" ErrorMessage="*" ForeColor="White"
                                                Text="*" ValidationGroup="ServBillGrp"/>
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td style="width: 15%; height: 23px">
                    <asp:Label ID="Label6" runat="server" Text="RMS No" Width="104px"></asp:Label></td>
                <td style="width: 35%; height: 23px">
                    <asp:TextBox ID="RMStxt" runat="server" MaxLength="12" Width="50%" ValidationGroup="ServBillGrp" Enabled="False"/></td>
                <td style="width: 15%; height: 23px">
                </td>
                <td style="width: 36%; height: 23px">
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td style="height: 30px; width: 15%;">
                    <asp:Label ID="lblAppoinmentNo" runat="server" Text="Appointment No" Width="124px"></asp:Label>
                </td>
                <td style="width: 35%; height: 30px">
                    <asp:TextBox ID="txtAppointmentPrefix" runat="server" Width="5%" />&nbsp;                    
                    <asp:TextBox ID="txtAppointmentNo" runat="server" Width="60%" ValidationGroup="ServBillGrp"/>
                    <asp:Button ID="btnSearchAppNo" runat="server" Text="..." CausesValidation="False"
                        Width="12px" />
                    <asp:Button runat="server" ID="btnRetrieve" Text="Retrieve" CausesValidation="False"
                        OnClick="BtnRetrieveClick" />
                    <asp:Label ID="Label4" runat="server" Font-Bold="True" Font-Size="Small" ForeColor="Red"
                               Text="*"/>
                    <asp:RequiredFieldValidator ID="rfvAppointmentNo" runat="server" ControlToValidate="txtAppointmentNo"
                                                ErrorMessage="*" ForeColor="White" Text="*" ValidationGroup="ServBillGrp"/>
                </td>
                <td style="height: 30px; width: 15%;">
                    <asp:Label ID="lblCustomerID" runat="server" Text="CustomerID" Width="124px"></asp:Label>
                </td>
                <td style="height: 30px; width: 36%;">
                    <asp:TextBox ID="txtCustomerPrefix" runat="server" Width="26px" ReadOnly="True"></asp:TextBox>&nbsp;
                    <asp:TextBox ID="txtCustomerID" runat="server" Width="100px" ReadOnly="True" ValidationGroup="ServBillGrp"/>
                    &nbsp;
                    <asp:Button ID="btnSearchCustomer" runat="server" Text="..." CausesValidation="False" Enabled="False"
                        Width="12px" />
                    <asp:Label ID="Label8" runat="server" Font-Bold="True" Font-Size="Small" ForeColor="Red"
                        Text="*"></asp:Label>
                    <asp:RequiredFieldValidator ID="rfvCustomerNo" runat="server"
                                                ControlToValidate="txtCustomerID" ErrorMessage="*" ForeColor="White"
                                                Text="*" ValidationGroup="ServBillGrp"/>
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td style="height: 28px; width: 15%;">
                    <asp:Label ID="lblROSerialNo" runat="server" Text="RO Serial No" Width="124px"></asp:Label>
                </td>
                <td style="width: 35%; height: 28px">
                    <asp:TextBox ID="txtROSerialNo" runat="server" ReadOnly="True" Visible="False"></asp:TextBox>
                    <asp:TextBox ID="txtROUID" runat="server" Enabled="False" Visible="False" ReadOnly="True"></asp:TextBox>
                    <asp:DropDownList ID="cboRO" runat="server" Width="95%">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="rfvRO" runat="server" ControlToValidate="cboServiceBillType"
                                                ErrorMessage="*" ForeColor="White" Text="*" ValidationGroup="ServBillGrp"/>
                </td>
                <td style="height: 28px; width: 15%;">
                    <asp:Label ID="lblCustomer" runat="server" Text="Customer Name" Width="124px"></asp:Label>
                </td>
                <td style="height: 28px; width: 36%;">
                    <asp:TextBox ID="txtCustomer" runat="server" ReadOnly="True" Width="200px"></asp:TextBox>
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td style="width: 15%">
                    <asp:Label ID="lblCountry" runat="server" Text="Country" Width="124px"></asp:Label>
                </td>
                <td style="width: 35%">
                    <asp:DropDownList ID="cboCountry" runat="server" AutoPostBack="True" Width="90%" ValidationGroup="ServBillGrp"/>
                    <asp:Label ID="Label5" runat="server" Font-Bold="True" Font-Size="Small" ForeColor="Red"
                        Text="*"></asp:Label>
                    <asp:RequiredFieldValidator ID="rfvCountry" runat="server" ControlToValidate="cboCountry"
                                                ErrorMessage="*" ForeColor="White" Text="*" ValidationGroup="ServBillGrp"/>
                </td>
                <td style="width: 15%">
                    <asp:Label ID="lblCompany" runat="server" Text="Company" Width="124px"></asp:Label>
                </td>
                <td style="width: 36%">
                    <asp:DropDownList ID="cboCompany" runat="server" AutoPostBack="false" Width="90%"
                                      onchange="LoadServiceCenter(this)" ValidationGroup="ServBillGrp"/>
                    <asp:Label ID="Label10" runat="server" Font-Bold="True" Font-Size="Small" ForeColor="Red"
                        Text="*"></asp:Label>
                    <asp:RequiredFieldValidator ID="rfvCompany" runat="server" ControlToValidate="cboCompany"
                                                ErrorMessage="*" ForeColor="White" Text="*" ValidationGroup="ServBillGrp"/>
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td style="width: 15%">
                    <asp:Label ID="lblServiceCenter" runat="server" Text="Service Center" Width="124px"></asp:Label>
                </td>
                <td style="width: 35%">
                    <asp:DropDownList ID="cboServiceCenter" runat="server" AutoPostBack="false" Width="90%"
                                      onchange="LoadTechnician(this)" ValidationGroup="ServBillGrp"/>
                    <asp:Label ID="Label9" runat="server" Font-Bold="True" Font-Size="Small" ForeColor="Red"
                        Text="*"></asp:Label>
                    <asp:RequiredFieldValidator ID="rfvServiceCenter" runat="server"
                                                ControlToValidate="cboServiceCenter" ErrorMessage="*" ForeColor="White"
                                                Text="*" ValidationGroup="ServBillGrp"/>
                </td>
                <td style="width: 15%">
                    <asp:Label ID="lblStatus" runat="server" Text="Status" Width="124px"></asp:Label>
                </td>
                <td style="width: 36%">
                    <asp:DropDownList ID="cboStatus" runat="server" AutoPostBack="True" Width="90%">
                    </asp:DropDownList>
                    &nbsp;
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td style="height: 28px; width: 15%;">
                    <asp:Label ID="lblCreatedBy" runat="server" Text="Created By" Width="124px"></asp:Label>
                </td>
                <td style="width: 35%; height: 28px">
                    <asp:TextBox ID="txtCreatedBy" runat="server" ReadOnly="True"></asp:TextBox>
                </td>
                <td style="height: 28px; width: 15%;">
                    <asp:Label ID="lblCreateDate" runat="server" Text="Create Date" Width="124px"></asp:Label>
                </td>
                <td style="height: 28px; width: 36%;">
                    <asp:TextBox ID="txtCreateDate" runat="server" ReadOnly="True"></asp:TextBox>
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td style="height: 28px; width: 15%;">
                    <asp:Label ID="lblModifyBy" runat="server" Text="Modify By" Width="124px"></asp:Label>
                </td>
                <td style="width: 35%; height: 28px">
                    <asp:TextBox ID="txtModifyBy" runat="server" ReadOnly="True"></asp:TextBox>
                </td>
                <td style="height: 28px; width: 15%;">
                    <asp:Label ID="lblModifyDate" runat="server" Text="Modfy Date" Width="124px"></asp:Label>
                </td>
                <td style="height: 28px; width: 36%;">
                    <asp:TextBox ID="txtModifyDate" runat="server" ReadOnly="True"></asp:TextBox>
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td colspan="4">
                    &nbsp;
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td colspan="4" id="#partcode">
                    <asp:Label ID="lblUpdateStockList" runat="server" Font-Underline="True" Text="Update Stock List"
                        Width="272px"></asp:Label>
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td colspan="4">
                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td style="width: 227px; height: 13px">
                                <asp:Label ID="lblPartCode" runat="server" Text="Part Code" Width="124px"></asp:Label>
                            </td>
                            <td style="height: 13px">
                                <asp:TextBox ID="txtPartCode" runat="server" MaxLength="20" AutoPostBack="false"
                                    onblur="LoadPart()"></asp:TextBox>
                                &nbsp;&nbsp;
                                <asp:TextBox ID="txtPartName" runat="server" ReadOnly="True" Width="209px" TabIndex="100"></asp:TextBox>
                                <font color="red" size="1">
                                    <asp:Label ID="lblErrorPart" runat="server" Text="" Width="150"></asp:Label>
                                </font>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 227px; height: 15px">
                                <asp:Label ID="lblPriceID" runat="server" Text="Price ID" Width="124px"></asp:Label>
                            </td>
                            <td style="height: 15px">
                                <asp:TextBox ID="txtPriceID" runat="server" MaxLength="20" AutoPostBack="false" onblur="LoadPrice()"
                                             Visible="false"/>
                                <asp:DropDownList runat="server" ID="cboPriceID" onChange="LoadPrice(this)" Width="150px" />
                                &nbsp;&nbsp;&nbsp;
                                <asp:TextBox ID="txtUnitPrice" runat="server" ReadOnly="True" Width="209px" Style="text-align: right"
                                    TabIndex="100"></asp:TextBox>
                            </td>
                            <asp:Label ID="lblErrorPrice" runat="server" Text="" Width="124px"></asp:Label></tr>
                        <tr>
                            <td style="width: 227px; height: 16px">
                                <asp:Label ID="lblQuantity" runat="server" Text="Quantity" Width="124px"></asp:Label>
                            </td>
                            <td style="height: 16px">
                                <asp:TextBox ID="txtQty" runat="server" MaxLength="5" AutoPostBack="false" Style="text-align: right"
                                    onblur="LoadLineAmount()" Text="1" />
                                <asp:Label ID="lblErrorQty" runat="server" Text="" Width="124px"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 227px; height: 14px">
                                <asp:Label ID="lblLineTotal" runat="server" Text="Line Total" Width="124px"></asp:Label>
                            </td>
                            <td style="height: 14px">
                                <asp:TextBox ID="txtLineTotal" runat="server" ReadOnly="True" Style="text-align: right">0</asp:TextBox>
                                &nbsp; &nbsp;&nbsp;
                                <asp:LinkButton ID="lnkAddItem" runat="server" CausesValidation="False">Add Item</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 227px; height: 14px">
                                <asp:Label ID="lblTax" runat="server" Text="Tax" Width="124px"></asp:Label>
                            </td>
                            <td style="height: 14px">
                                <asp:TextBox ID="txtTaxID" runat="server" Enabled="False" Visible="true" Width="10%"></asp:TextBox>
                                <asp:TextBox ID="txtTaxRate" runat="server" Enabled="False" Visible="true" Width="10%">0</asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 227px; height: 14px">
                                &nbsp;
                            </td>
                            <td style="height: 14px">
                            </td>
                        </tr>
                    </table>
                    <asp:GridView ID="grdServiceBill" runat="server" Width="100%">
                        <Columns>
                            <asp:CommandField CancelText="" EditText="" HeaderImageUrl="~/PresentationLayer/graph/edit.gif"
                                ShowDeleteButton="True" />
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr bgcolor="#ffffff" valign="top">
                <td style="height: 65px; width: 15%;" id="#dscnt">
                    <asp:Label ID="lblRemarks" runat="server" Text="Discount Remarks" Width="124px"></asp:Label>
                </td>
                <td style="height: 65px" colspan="2">
                    <asp:TextBox ID="txtRemarks" runat="server" Height="116px" TextMode="MultiLine" Width="305px"
                        MaxLength="800"></asp:TextBox>
                </td>
                <td style="height: 65px; width: 36%;" align="right">
                    <table width="100%">
                        <tr>
                            <td style="height: 27px" align="right">
                                <asp:Label ID="lblTotalGross" runat="server" Text="Total" Width="107px"></asp:Label>
                            </td>
                            <td style="height: 27px; width: 21px;" align="right">
                                <asp:TextBox ID="txtTotalGross" runat="server" MaxLength="8" Width="106px"
                                             Style="text-align: right" Text="0" ValidationGroup="ServBillGrp"
                                             OnTextChanged="ComputeTotal"/>
                                <asp:RequiredFieldValidator ID="reqVal_Total" 
                                                            runat="server" 
                                                            ErrorMessage="ErrorMessage"
                                                            ControlToValidate="txtTotalGross"
                                                            ValidationGroup="ServBillGrp"
                                                            Text="*"
                                                            Display="Dynamic"/>
                                <asp:RegularExpressionValidator ID="regExVal_Total" 
                                                                runat="server" 
                                                                ErrorMessage="RegularExpressionValidator"
                                                                ControlToValidate="txtTotalGross"
                                                                ValidationGroup="ServBillGrp"
                                                                ValidationExpression="^(\d|-)?(\d|,)*\.?\d*$"
                                                                Text="*"
                                                                Display="Dynamic"/>
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 27px" align="right">
                                <asp:Label ID="lblDiscount" runat="server" Text="Discount" Width="107px"></asp:Label>
                            </td>
                            <td style="height: 27px; width: 21px;" align="right">
                                <asp:TextBox ID="txtDiscount" runat="server" MaxLength="6" Width="106px" AutoPostBack="false"
                                             Style="text-align: right" onchange="LoadDiscount(this);" Text="0"
                                             ValidationGroup="ServBillGrp" OnTextChanged="ComputeTotal"/>
                                <asp:RequiredFieldValidator ID="reqVal_Discount" 
                                             runat="server" 
                                             ErrorMessage="RequiredFieldValidator"
                                             ControlToValidate="txtDiscount"
                                             ValidationGroup="ServBillGrp"
                                             Text="*"
                                             Display="Dynamic"/>
                                <asp:RegularExpressionValidator ID="regExVal_Discount" 
                                                                runat="server" 
                                                                ErrorMessage="RegularExpressionValidator"
                                                                ControlToValidate="txtDiscount"
                                                                ValidationGroup="ServBillGrp"
                                                                ValidationExpression="^(\d|-)?(\d|,)*\.?\d*$"
                                                                Text="*"
                                                                Display="Dynamic"/>
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 27px" align="right">
                                <asp:Label ID="lblTotalNet" runat="server" Text="Total" Width="107px"></asp:Label>
                            </td>
                            <td style="height: 27px; width: 21px;" align="right">
                                <asp:TextBox ID="txtTotalNet" runat="server" MaxLength="6" ReadOnly="True" Width="106px"
                                    Style="text-align: right">0</asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 27px" align="right">
                                <asp:Label ID="lblInvoiceTax" runat="server" Text="Invoice Tax" Width="107px"></asp:Label>
                                <asp:Label ID="lblInclusive" runat="server" Font-Underline="True" Text="(Inclusive)"
                                    Width="107px"></asp:Label>
                            </td>
                            <td style="height: 27px; width: 21px;" align="right">
                                <asp:TextBox ID="txtTax" runat="server" MaxLength="6" ReadOnly="True" Width="106px"
                                    Style="text-align: right">0</asp:TextBox>
                            </td>
                        </tr>
                    </table>
                    &nbsp;
                </td>
            </tr>
            <tr bgcolor="#ffffff" valign="top">
                <td style="height: 2px" colspan="2">
                    <asp:Label ID="lblPaymentDetails" runat="server" Text="Payment Details" Width="124px"></asp:Label>
                </td>
                <td style="height: 2px; width: 15%;">
                </td>
                <td style="height: 2px; width: 36%;">
                </td>
            </tr>
            <tr bgcolor="#ffffff" valign="top">
                <td colspan="3" style="height: 65px" align="right">
                    <table width="80%">
                        <tr align="right">
                            <td style="width: 37px; height: 16px" align="right">
                            </td>
                            <td style="width: 319px; height: 16px" align="right">
                                <asp:Label ID="lblCash" runat="server" Text="Cash" Width="124px"></asp:Label>
                            </td>
                            <td style="width: 2px; height: 16px" align="right">
                                <asp:TextBox ID="txtCash" runat="server" MaxLength="10" Width="122px" AutoPostBack="false"
                                    Style="text-align: right" onchange="LoadTotalPayment()">0</asp:TextBox>
                            </td>
                        </tr>
                        <tr align="right">
                            <td style="width: 37px" align="right">
                            </td>
                            <td style="width: 319px" align="right">
                                <asp:Label ID="lblCheque" runat="server" Text="Cheque" Width="124px"></asp:Label>
                            </td>
                            <td style="width: 2px" align="right">
                                <asp:TextBox ID="txtCheque" runat="server" MaxLength="10" Width="122px" AutoPostBack="false"
                                    Style="text-align: right" onchange="LoadTotalPayment()">0</asp:TextBox>
                            </td>
                        </tr>
                        <tr align="right">
                            <td style="width: 37px" align="right">
                            </td>
                            <td style="width: 319px" align="right">
                                <asp:Label ID="lblCredit" runat="server" Text="Credit" Width="124px"></asp:Label>
                            </td>
                            <td style="width: 2px" align="right">
                                <asp:TextBox ID="txtCredit" runat="server" MaxLength="10" Width="122px" AutoPostBack="false"
                                    Style="text-align: right" onchange="LoadTotalPayment()">0</asp:TextBox>
                            </td>
                        </tr>
                        <tr align="right">
                            <td style="width: 37px" align="right">
                            </td>
                            <td style="width: 319px" align="right">
                                <asp:Label ID="lblCreditCard" runat="server" Text="CreditCard" Width="124px"></asp:Label>
                            </td>
                            <td style="width: 2px" align="right">
                                <asp:TextBox ID="txtCreditCard" runat="server" MaxLength="10" Width="122px" AutoPostBack="false"
                                    Style="text-align: right" onchange="LoadTotalPayment()">0</asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 37px" align="right">
                            </td>
                            <td style="width: 319px" align="right">
                                <asp:Label ID="lblOther" runat="server" Text="Other" Width="124px"></asp:Label>
                            </td>
                            <td style="width: 2px" align="right">
                                <asp:TextBox ID="txtOther" runat="server" MaxLength="10" Width="122px" AutoPostBack="false"
                                    Style="text-align: right" onchange="LoadTotalPayment()">0</asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 37px">
                            </td>
                            <td align="right" style="width: 319px">
                                <asp:Label ID="lblTotal" runat="server" Text="Total" Width="124px"></asp:Label>
                            </td>
                            <td align="right" style="width: 2px">
                                <asp:TextBox ID="txtTotal" runat="server" MaxLength="10" ReadOnly="True" Width="122px"
                                    Style="text-align: right">0</asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="height: 65px; width: 36%;" align="center" valign="middle">
                    &nbsp;
                    <asp:LinkButton ID="lnkSave" runat="server" Width="143px" Text="Save" ValidationGroup="ServBillGrp"/>
                    <br />
                    <br />
                    <asp:LinkButton ID="lnkGeneratePackingList" runat="server" Enabled="False">Generate Packing List</asp:LinkButton><br />
                    <br />
                    <asp:LinkButton ID="lnkCancel" runat="server" CausesValidation="False" Width="143px">Cancel</asp:LinkButton><br />
                    <br />
                    <asp:LinkButton ID="lnkNew" runat="server" Enabled="False" Width="143px">New</asp:LinkButton><br />
                    <br />
                    <asp:LinkButton ID="lnkGenerateSBC" runat="server" Width="143px" Enabled="False">Generate SBC</asp:LinkButton>
                    <br />
                    <br />
                    <asp:LinkButton ID="lnkGenerateTaxInvoice" runat="server" Width="143px" Enabled="False">Generate Tax Invoice</asp:LinkButton><br />
                </td>
            </tr>
            <tr bgcolor="#ffffff" valign="top">
                <td align="left" colspan="3" style="height: 65px">
                    &nbsp;
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
                        ShowSummary="true" ValidationGroup="ServBillGrp"/>
                    <cc1:MessageBox ID="MessageBox1" runat="server" />
                    <cc1:MessageBox ID="MessageBox2" runat="server" />
                    &nbsp;
                </td>
                <td align="center" style="width: 36%; height: 65px" valign="middle">
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td colspan="4">
                    &nbsp;
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td colspan="4">
                    <asp:Label ID="lblNoteDown" runat="server" ForeColor="Red" Text="Label"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>