﻿Imports BusinessEntity
Imports System
Imports System.Data

Partial Class PresentationLayer_Function_dailyAreaZoningInfo
    Inherits System.Web.UI.Page
    Private ZoneEntity As clsZone
    Private MasterZoneInfo As DataSet
    Private DailyZoneInfo As DataSet
    Private TempAreas As DataSet
    Dim objXML As New clsXml
    Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try
        If (Not Page.IsPostBack) Then
            DisplayPage()
        ElseIf ZoneInfoView.Rows.Count > 0 Then
            DispGridViewHead()
        End If


        '''access control'''''''''''''''''''''''''''''''''''''''''''''''
        Dim accessgroup As String = Session("accessgroup").ToString
        Dim purviewArray As Array = New Boolean() {False, False, False, False}
        purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "28")

        Me.btnModify.Enabled = purviewArray(2)

        Me.btnCopy.Enabled = purviewArray(0)
        Me.bthCopyAll.Enabled = purviewArray(0)



        Me.errlab.Visible = False
        HypCal.NavigateUrl = "javascript:DoCal(document.form1.ApptDateBox);"
    End Sub
    Protected Sub DisplayPage()

        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        'display label message
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = Configuration.ConfigurationManager.AppSettings("XmlFilePath")
        titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-ZONGINGINF-0012")
        SerCenterID.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-ZONGINGINF-0001")
        ApptDate.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-ZONGINGINF-0002")
        ToNoofTech.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-ZONGINGINF-0003")
        ZoneTypeLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-ZONGINGINF-0004")
        btnLoad.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-ZONGINGINF-0005")
        MasZoneCodeLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-ZONGINGINF-0006")
        NorZoneCodeLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-ZONGINGINF-0007")
        btnCopy.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-ZONGINGINF-0008")
        btnModify.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-ZONGINGINF-0009")
        lblNoteUP.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
        lblNoteDown.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")

        Me.bthCopyAll.Text = objXmlTr.GetLabelName("EngLabelMsg", "CopyAllLink")

        objXmlTr.XmlFile = Configuration.ConfigurationManager.AppSettings("StatMsg")
        SerCenIDerr.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "BB-SERCENIDCHECK")

        'display dropdown list

        ZoneEntity = New clsZone()
        ZoneEntity.UserID = Session("userID")
        Dim SerCentInfo As DataSet = ZoneEntity.GetSerCent()
        If SerCentInfo Is Nothing Then
            Response.Redirect("~/PresentationLayer/error.aspx")
        End If
        SerCenIDDrop.Items.Add("")
        Dim i As Integer
        For i = 0 To SerCentInfo.Tables(0).Rows.Count - 1
            Dim list As New ListItem

            list.Text = SerCentInfo.Tables(0).Rows(i).Item("MSVC_SVCID") & "-" & SerCentInfo.Tables(0).Rows(i).Item("MSVC_ENAME")
            list.Value = SerCentInfo.Tables(0).Rows(i).Item("MSVC_SVCID")
            SerCenIDDrop.Items.Add(list)
        Next
        SerCenIDDrop.SelectedValue = Session("LOGIN_SVCID")
        ApptDateBox.Text = System.DateTime.Today

        Dim count As Integer
        Dim apptStat As New clsUtil()
        Dim statParam As ArrayList = New ArrayList
        statParam = apptStat.searchconfirminf("ZONTY")
        Dim statid As String
        Dim statnm As String
        For count = 0 To statParam.Count - 1
            statid = statParam.Item(count)
            statnm = statParam.Item(count + 1)
            count = count + 1
            Me.ZoneTypeDrp.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
        Next
        ZoneTypeDrp.SelectedIndex = 0
    End Sub
    'Protected Sub DisplayCal_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles DisplayCal.Click
    '    CalendaAppt.Visible = Not CalendaAppt.Visible
    'End Sub

    'Protected Sub CalendaAppt_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CalendaAppt.SelectionChanged

    '    System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
    '    ApptDateBox.Text = CalendaAppt.SelectedDate
    '    CalendaAppt.Visible = False
    '    ModifyStat(False)
    'End Sub
    Protected Sub CalendaAppt_SelectedDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CalendaAppt.SelectedDateChanged
        ModifyStat(False)
    End Sub
    Protected Sub btnLoad_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLoad.Click
        RequestFormValue()
        DispyZoneInfo()

    End Sub
    Private Sub DispyZoneInfo()

        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        RequestFormValue()

        Dim ZoneEntity As New clsZone()
        ZoneEntity.ServiceCenterID = SerCenIDDrop.Text
        ZoneEntity.ApptDate = ApptDateBox.Text
        ZoneEntity.ZoneType = ZoneTypeDrp.SelectedValue

        Dim ds As DataSet = ZoneEntity.GetZoneInfo()

        If ds Is Nothing Then
            Response.Redirect("~/PresentationLayer/error.aspx")
        End If
        If ds.Tables("TOTCH").Rows.Count = 1 Then
            Me.ToNoTechBox.Text = ds.Tables("TOTCH").Rows(0).Item(0).ToString()
        Else
            Me.ToNoTechBox.Text = "0"
        End If
        BindGrid(ds)
    End Sub
    Protected Sub DispGridViewHead()
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = Configuration.ConfigurationManager.AppSettings("XmlFilePath")
        ZoneInfoView.HeaderRow.Cells(0).Text = _
                        objXmlTr.GetLabelName("EngLabelMsg", "BB-ZONGINGINF-0014")
        ZoneInfoView.HeaderRow.Cells(0).Font.Size = 8
        ZoneInfoView.HeaderRow.Cells(1).Text = _
                        objXmlTr.GetLabelName("EngLabelMsg", "BB-ZONGINGINF-0015")
        ZoneInfoView.HeaderRow.Cells(1).Font.Size = 8
        ZoneInfoView.HeaderRow.Cells(2).Text = _
                         objXmlTr.GetLabelName("EngLabelMsg", "BB-ZONGINGINF-0016")
        ZoneInfoView.HeaderRow.Cells(2).Font.Size = 8
        ZoneInfoView.HeaderRow.Cells(3).Text = _
                         objXmlTr.GetLabelName("EngLabelMsg", "BB-ZONGINGINF-0017")
        ZoneInfoView.HeaderRow.Cells(2).Font.Size = 8
    End Sub
    Protected Sub BindGrid(ByVal ds As DataSet)
        Dim objXmlTr As New clsXml

        Session("ZoneInfoView") = ds.Tables("ZoneInfo")
        ZoneInfoView.DataSource = ds.Tables("ZoneInfo")
        Me.MasZoneCodeDrop.Items.Clear()
        Me.MasZoneCodeDrop.Items.Add("")
        Dim i As Integer
        If ZoneTypeDrp.SelectedValue = "MASTER" Then
            For i = 0 To ds.Tables(1).Rows.Count - 1
                Me.MasZoneCodeDrop.Items.Add(ds.Tables(1).Rows(i).Item("FZN1_ZONID").ToString().Trim())
            Next
            ZoneInfoView.Columns(3).Visible = False
            ZoneInfoView.Columns(4).Visible = False
        Else
            For i = 0 To ds.Tables(3).Rows.Count - 1
                Me.MasZoneCodeDrop.Items.Add(ds.Tables(3).Rows(i).Item("FZN1_ZONID").ToString().Trim())
            Next
            ZoneInfoView.Columns(3).Visible = True
            ZoneInfoView.Columns(4).Visible = True
        End If
        Me.NorZoneCodeBox.Text = ""

        ZoneInfoView.DataBind()
        If ZoneInfoView.Rows.Count > 0 Then
            DispGridViewHead()
        End If
        If ZoneTypeDrp.SelectedValue = "MASTER" And _
                    ds.Tables("ZoneInfo").Rows.Count = 0 Then
            Me.errlab.Visible = True
            objXmlTr.XmlFile = Configuration.ConfigurationManager.AppSettings("StatMsg")
            Me.errlab.Text = objXmlTr.GetLabelName("StatusMessage", "BB-ZONE-NoMasterInfo")
            objXmlTr.XmlFile = Configuration.ConfigurationManager.AppSettings("XmlFilePath")
            Me.btnModify.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-ZONGINGINF-0010")
            Me.btnModify.Visible = True
        ElseIf ZoneTypeDrp.SelectedValue = "NORMAL" And _
                    ds.Tables("ZoneInfo").Rows.Count = 0 Then
            Me.errlab.Visible = True
            objXmlTr.XmlFile = Configuration.ConfigurationManager.AppSettings("StatMsg")
            Me.errlab.Text = objXmlTr.GetLabelName("StatusMessage", "BB-ZONE-NoNormalInfo")
            objXmlTr.XmlFile = Configuration.ConfigurationManager.AppSettings("XmlFilePath")
            Me.btnModify.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-ZONGINGINF-0011")
            ModifyStat(True)
        Else
            objXmlTr.XmlFile = Configuration.ConfigurationManager.AppSettings("XmlFilePath")
            Me.btnModify.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-ZONGINGINF-0009")
            ModifyStat(True)
        End If

        If Me.MasZoneCodeDrop.Items.Count = 1 Then
            Me.MasZoneCodeLab.Visible = False
            Me.MasZoneCodeDrop.Visible = False
            Me.NorZoneCodeLab.Visible = False
            Me.NorZoneCodeBox.Visible = False
            Me.btnCopy.Visible = False
            Me.bthCopyAll.Visible = False
        Else
            Me.MasZoneCodeLab.Visible = True
            Me.MasZoneCodeDrop.Visible = True
            Me.NorZoneCodeLab.Visible = True
            Me.NorZoneCodeBox.Visible = True
            Me.btnCopy.Visible = True
            Me.bthCopyAll.Visible = True
        End If
    End Sub

    Protected Sub SerCenIDDrop_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles SerCenIDDrop.SelectedIndexChanged
        ModifyStat(False)
        RequestFormValue()
    End Sub
    Private Sub ModifyStat(ByVal Status As Boolean)
        Me.MasZoneCodeLab.Visible = Status
        Me.MasZoneCodeDrop.Visible = Status
        Me.NorZoneCodeLab.Visible = Status
        Me.NorZoneCodeBox.Visible = Status
        Me.btnCopy.Visible = Status
        Me.ZoneInfoView.Visible = Status
        Me.btnModify.Visible = Status
        Me.bthCopyAll.Visible = Status
    End Sub

    Protected Sub ZoneTypeDrp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ZoneTypeDrp.SelectedIndexChanged
        ModifyStat(False)
        RequestFormValue()
    End Sub

    Protected Sub bthCopyAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles bthCopyAll.Click
        RequestFormValue()
        objXML.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim msginfo As String
        Dim msgtitle As String

        msginfo = objXML.GetLabelName("StatusMessage", "CopyAllZoneMessage")

        Me.CopyAllMsgBox.Confirm(msginfo, "Copy")
    End Sub

    Protected Sub btnCopy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCopy.Click
        RequestFormValue()
        objXML.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim msginfo As String
        Dim msgtitle As String
        If Me.MasZoneCodeDrop.SelectedIndex = 0 Then
            msginfo = objXML.GetLabelName("StatusMessage", "BB-ZONE-MASZONEIDCHECK")
            InfoMsgBox.Alert(msginfo)
            Return
        End If
        If Me.NorZoneCodeBox.Text = "" Then
            msginfo = objXML.GetLabelName("StatusMessage", "BB-ZONE-NORZONEIDCHECK")
            InfoMsgBox.Alert(msginfo)
            Return
        End If

        msginfo = objXML.GetLabelName("StatusMessage", "BB-ZONE-COPY")
        msgtitle = objXML.GetLabelName("StatusMessage", "BB-ZONE-COPY")
        saveMsgBox.Confirm(msginfo, "Copy")
    End Sub

    Protected Sub CopyAllMsgBox_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles CopyAllMsgBox.GetMessageBoxResponse
        RequestFormValue()
        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then

            Dim lstrZoneCode As String
            Dim lintIndex As Integer
            Dim ret As Integer = 0

            Dim ZoneEntity As New clsZone()
            ZoneEntity.UserID = Session("userID")
            ZoneEntity.ServiceCenterID = Me.SerCenIDDrop.SelectedValue
            ZoneEntity.ApptDate = ApptDateBox.Text  'Me.ApptDateBox.Text


            For lintIndex = 0 To MasZoneCodeDrop.Items.Count - 1

                lstrZoneCode = MasZoneCodeDrop.Items(lintIndex).Value
                ZoneEntity.MasterZoneCode = lstrZoneCode
                ZoneEntity.NormalZoneCode = lstrZoneCode

                If lstrZoneCode.Trim <> "" Then
                    ret = ZoneEntity.CopyMasZoneToNormal()
                End If
            Next

            objXML.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            If ret > 0 Then
                Me.errlab.Text = objXML.GetLabelName("StatusMessage", "BB-ZONE-DUPNORZNCODE")
            ElseIf ret < 0 Then
                Me.errlab.Text = objXML.GetLabelName("StatusMessage", "BB-ZONE-COPYFAIL")
            Else
                Me.errlab.Text = objXML.GetLabelName("StatusMessage", "BB-ZONE-COPYSUC")
                DispyZoneInfo()
            End If
            Me.errlab.Visible = True
        End If
    End Sub

    Protected Sub saveMsgBox_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles saveMsgBox.GetMessageBoxResponse
        RequestFormValue()

        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
            Dim ZoneEntity As New clsZone()
            ZoneEntity.UserID = Session("userID")
            ZoneEntity.ServiceCenterID = Me.SerCenIDDrop.SelectedValue
            ZoneEntity.ApptDate = ApptDateBox.Text  'Me.ApptDateBox.Text
            If e.Data = "Copy" Then
                ZoneEntity.MasterZoneCode = Me.MasZoneCodeDrop.SelectedValue
                ZoneEntity.NormalZoneCode = Me.NorZoneCodeBox.Text
                Dim ret As Integer = ZoneEntity.CopyMasZoneToNormal()
                objXML.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                If ret > 0 Then
                    Me.errlab.Text = objXML.GetLabelName("StatusMessage", "BB-ZONE-DUPNORZNCODE")
                ElseIf ret < 0 Then
                    Me.errlab.Text = objXML.GetLabelName("StatusMessage", "BB-ZONE-COPYFAIL")
                Else
                    Me.errlab.Text = objXML.GetLabelName("StatusMessage", "BB-ZONE-COPYSUC")
                    DispyZoneInfo()
                End If
                Me.errlab.Visible = True
            Else 'Save As New Master
                Dim ZoneInfo As DataTable = Session("ZoneInfoView")
                ZoneEntity.NormalZoneCode = ZoneInfo.Rows(e.Data).Item("FZN1_ZONID").ToString()
                Dim ret As Integer = ZoneEntity.SaveAsNewMas()
                objXML.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                If ret > 0 Then
                    Me.errlab.Text = objXML.GetLabelName("StatusMessage", "BB-ZONE-DUPMASZNCODE")
                ElseIf ret < 0 Then
                    Me.errlab.Text = objXML.GetLabelName("StatusMessage", "BB-ZONE-SAVEMASFAIL")
                Else
                    Me.errlab.Text = objXML.GetLabelName("StatusMessage", "BB-ZONE-SAVEMASSUC")
                    Dim btnSaveMas As LinkButton = ZoneInfoView.Rows(e.Data).FindControl("btnSaveMas")
                    btnSaveMas.Enabled = False
                End If
                Me.errlab.Visible = True
            End If
        Else

        End If
    End Sub

    Protected Sub ZoneInfoView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles ZoneInfoView.PageIndexChanging
        ZoneInfoView.PageIndex = e.NewPageIndex
        Dim ZoneInfo As DataTable = Session("ZoneInfoView")
        ZoneInfoView.DataSource = ZoneInfo
        ZoneInfoView.DataBind()
        If ZoneInfo.Rows.Count > 0 Then
            DispGridViewHead()
        End If
        RequestFormValue()
    End Sub

    Protected Sub btnModify_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModify.Click
        RequestFormValue()
        Dim ServCID As String = Me.SerCenIDDrop.SelectedValue
        ServCID = Server.UrlEncode(ServCID)
        Dim ApptDate As String = ApptDateBox.Text 'Me.ApptDateBox.Text
        ApptDate = Server.UrlEncode(ApptDate)
        Dim ZoneType As String = Me.ZoneTypeDrp.SelectedValue
        ZoneType = Server.UrlEncode(ZoneType)
        Dim strTempURL As String = "ServCID=" + ServCID + "&ApptDate=" + ApptDate + "&ZoneType=" + ZoneType
        strTempURL = "~/PresentationLayer/function/ModifyZoneInfomation.aspx?" + strTempURL
        Response.Redirect(strTempURL)
    End Sub

    Protected Sub ZoneInfoView_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles ZoneInfoView.RowCreated
        RequestFormValue()
        If e.Row.RowType = DataControlRowType.DataRow And Me.ZoneTypeDrp.SelectedValue <> "MASTER" Then
            objXML.XmlFile = Configuration.ConfigurationManager.AppSettings("XmlFilePath")
            Dim btnSaveMas As LinkButton = e.Row.FindControl("btnSaveMas")
            btnSaveMas.Text = objXML.GetLabelName("EngLabelMsg", "BB-ZONGINGINF-0013")
            btnSaveMas.Enabled = True
        End If

    End Sub

    Protected Sub ZoneInfoView_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles ZoneInfoView.RowCommand
        If e.CommandName = "btnSaveMas" Then
            objXML.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            RequestFormValue()
            Dim msginfo As String
            msginfo = objXML.GetLabelName("StatusMessage", "BB-ZONGINGINF-QusSave2")
            saveMsgBox.Confirm(msginfo, e.CommandArgument)
        End If
    End Sub

    Sub RequestFormValue()
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        ApptDateBox.Text = Request.Form("ApptDateBox")
        If Not IsDate(ApptDateBox.Text) Then
            ApptDateBox.Text = System.DateTime.Today
        End If

    End Sub
End Class
