Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data

Partial Class PresentationLayer_function_UpdatePaymentCollection
    Inherits System.Web.UI.Page
    Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try

        If (Not Page.IsPostBack) Then
            txtStartDate.Text = System.DateTime.Today
            txtEndDate.Text = System.DateTime.Today
            BindGrid()

        End If
        HypCal1.NavigateUrl = "javascript:DoCal(document.form1.txtStartDate);"
        HypCal2.NavigateUrl = "javascript:DoCal(document.form1.txtEndDate);"



    End Sub
    Protected Sub BindGrid()

        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        solab.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-INVOICENO")
        phnolab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-customer-02")
        CNlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-customer-03")
        snlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-customer-04")
        searchbtn.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-SEARCH")
        titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-payment-collection")
        ctryStat.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCTRY-0004")
        Me.lblNoRecord.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-NR")
        Me.lblAppointmentNo.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-APPOINMENTNO")
        Me.lblCustomerID.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTMDSL-0001")
        Me.lblServiceBillNo.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-SERVICEBILLNO")

        lblNoRecord.Visible = False
        '--------------------------------------------------------------------
        Dim cntryStat As New clsUtil()
        Dim statParam As ArrayList = New ArrayList
        statParam = cntryStat.searchconfirminf("SerStat")
        Dim count As Integer
        Dim statid As String
        Dim statnm As String
        For count = 0 To statParam.Count - 1
            statid = statParam.Item(count)
            statnm = statParam.Item(count + 1)
            count = count + 1

            If statid = "UM" Or statid = "UA" Or statid = "BL" Or UCase(statid) = "AL" Then
                ctryStatDrop.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
            End If
        Next
        ctryStatDrop.Items(0).Selected = True
        
        Dim SVC As New DataSet
        Dim installSVC As New clsCommonClass
        installSVC.sparea = Session("login_svcID")
        installSVC.spctr = Session("login_ctryID")
        installSVC.spstat = Session("login_cmpID")
        installSVC.rank = Session("login_rank")

        SVC = installSVC.Getcomidname("BB_TOMAS_SVC")
        If SVC.Tables.Count <> 0 Then
            svcbonds(SVC, SVClist)
        End If

        partView.AllowPaging = True
        partView.AllowSorting = True
        Dim editcol As New CommandField
        editcol.EditText = objXmlTr.GetLabelName("EngLabelMsg", "BB-Edit") '"edit"
        editcol.HeaderImageUrl = "~/PresentationLayer/graph/edit.gif"
        editcol.ShowEditButton = True
        partView.Columns.Add(editcol)
        'partView.DataBind()
        DisplayGridHeader()
        lblNoRecord.Visible = False

        Dim accessgroup As String = Session("accessgroup").ToString
        Dim purviewArray As Array = New Boolean() {False, False, False, False}
        purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "31")

        editcol.Visible = purviewArray(2)


    End Sub

    Protected Sub SVCList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim svcID As String
        svcID = SVClist.SelectedValue

        Dim tech As New DataSet
        Dim installbond As New clsCommonClass
        installbond.sparea = svcID
        installbond.spctr = Session("login_ctryID").ToString()
        installbond.spstat = Session("login_cmpID").ToString()
        installbond.rank = Session("login_rank").ToString()
        tech = installbond.Getcomidname("BB_MASTECH_IDNAME")
        If tech.Tables.Count <> 0 Then
            databonds(tech, technicianList)
        End If

        DisplayGridHeader()
    End Sub

    Sub DisplayGridHeader()
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        If partView.Rows.Count <> 0 Then
            partView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-INVOICENO")
            partView.HeaderRow.Cells(1).Font.Size = 8
            partView.HeaderRow.Cells(2).Text = "Service Order No"
            partView.HeaderRow.Cells(2).Font.Size = 8
            partView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-04")
            partView.HeaderRow.Cells(3).Font.Size = 8
            partView.HeaderRow.Cells(4).Text = "Service Bill No"
            partView.HeaderRow.Cells(4).Font.Size = 8
            partView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-07")
            partView.HeaderRow.Cells(5).Font.Size = 8
            partView.HeaderRow.Cells(6).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-05")
            partView.HeaderRow.Cells(6).Font.Size = 8
            partView.HeaderRow.Cells(7).Text = "Technician Name"
            partView.HeaderRow.Cells(7).Font.Size = 8
            partView.HeaderRow.Cells(8).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CR-09")
            partView.HeaderRow.Cells(8).Font.Size = 8
            partView.HeaderRow.Cells(9).Text = "Customer Name"
            partView.HeaderRow.Cells(9).Font.Size = 8

           
            partView.HeaderRow.Cells(10).Text = "Balance"
            partView.HeaderRow.Cells(10).Font.Size = 8

            partView.Visible = True
            lblNoRecord.Visible = False
        Else
            lblNoRecord.Visible = True
        End If
    End Sub

#Region " bonds"
    '----------------------------------------------------------------------------------------------- Binding Technician to DropDown -- Tomas 20111214
    Public Function databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        Dim row As DataRow
        If ds.Tables(0).Rows.Count > 0 Then
            dropdown.Items.Add(" ")
            For Each row In ds.Tables(0).Rows
                Dim NewItem As New ListItem()
                NewItem.Text = row("name") & "-" & row("id")
                NewItem.Value = row("id")
                dropdown.Items.Add(NewItem)
            Next

        End If
    End Function
    Public Function svcbonds(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        '----------------------------------------------------------------------------------------------- Binding SVC to DropDown -- Tomas 20111214
        dropdown.Items.Clear()
        Dim row As DataRow
        If ds.Tables(0).Rows.Count > 0 Then
            dropdown.Items.Add(" ")
            For Each row In ds.Tables(0).Rows
                Dim NewItem As New ListItem()
                NewItem.Text = row("id") & "-" & row("name")
                NewItem.Value = row("id")
                dropdown.Items.Add(NewItem)
            Next

        End If
    End Function

#End Region

    Protected Sub searchbtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles searchbtn.Click

        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Dim objUpdateCollectPayment As New clsupdatepaymentcollection()
        Dim lstrStartDate, lstrEndDate As String

        lstrStartDate = Me.txtStartDate.Text
        lstrEndDate = Me.txtEndDate.Text

        objUpdateCollectPayment.SOID = soText.Text
        objUpdateCollectPayment.CustomerName = CNText.Text
        objUpdateCollectPayment.PhoneNo = phnoText.Text
        objUpdateCollectPayment.SerialNo = snText.Text
        objUpdateCollectPayment.UserID = Session("userID").ToString()
        If (ctryStatDrop.Text = "AL") Then
            objUpdateCollectPayment.STATUS = ""
        Else
            objUpdateCollectPayment.STATUS = ctryStatDrop.Text
        End If
        objUpdateCollectPayment.StartBillDate = lstrStartDate
        objUpdateCollectPayment.EndBillDate = lstrEndDate

        objUpdateCollectPayment.CustomerID = Me.txtCustomerID.Text
        objUpdateCollectPayment.CustomerPrefix = Session("login_ctryid").ToString()
        objUpdateCollectPayment.AppointmentPrefix = Me.txtAppointmentPrefix.Text
        objUpdateCollectPayment.AppointmentNo = Me.txtAppointmentNo.Text
        objUpdateCollectPayment.ServiceBillNo = Me.txtServiceBillNo.Text
        'objUpdateCollectPayment.Remark = SVClist.SelectedItem.Value.ToString()
        objUpdateCollectPayment.Technician = technicianList.SelectedValue
        objUpdateCollectPayment.AppointmentFix = cboServiceBillType.SelectedItem.Value.ToString()

        If Not IsDate(lstrStartDate) And lstrStartDate <> "" Then
            partView.Visible = False
            Exit Sub
        End If

        If Not IsDate(lstrEndDate) And lstrEndDate <> "" Then
            partView.Visible = False
            Exit Sub
        End If


        Dim selDS As DataSet = objUpdateCollectPayment.SelectAllPaymentCollection()

        If Not selDS Is Nothing Then
            partView.DataSource = selDS
            partView.DataBind()
            Session("selDS") = selDS

        End If

        DisplayGridHeader()
    End Sub

    Protected Sub partView_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles partView.RowEditing
        'trnno 
        Dim type As String = partView.Rows(e.NewEditIndex).Cells(1).Text
        type = Server.UrlEncode(type)
        Dim no As String = partView.Rows(e.NewEditIndex).Cells(2).Text
        no = Server.UrlEncode(no)
        Dim debt As String = partView.Rows(e.NewEditIndex).Cells(10).Text
        Dim strTempURL As String = "type=" + type + "&no=" + no + "&debt=" + debt
        'strTempURL = "~/PresentationLayer/Function/ServiceBillModify.aspx?" + strTempURL
        strTempURL = "~/PresentationLayer/Function/CollectPaymentModify.aspx?" + strTempURL
        Response.Redirect(strTempURL)

    End Sub
End Class
