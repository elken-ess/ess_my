﻿Imports BusinessEntity
Imports System.data
Imports System.IO
Imports System.Collections.Generic

Partial Class PresentationLayer_function_UploadFile
    Inherits System.Web.UI.Page
    Dim objXmlTr As New clsXml
    Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.MaintainScrollPositionOnPostBack = True
        AjaxPro.Utility.RegisterTypeForAjax(GetType(PresentationLayer_function_UploadFile))
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        If Not Page.IsPostBack Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try

            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-ACNT-UPLOAD")

            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "74")
        End If

        If (Not Page.IsPostBack) Then
            Try
                'Me.ddlUploadTo.Items.Insert(0, "Payment Collection") 'removed lly 
                Me.ddlUploadTo.Items.Insert(0, "Incentive Calculation - Deduction")
                Me.ddlUploadTo.Items.Insert(0, "Incentive Calculation - OT/Demerit")
                Me.ddlUploadTo.SelectedIndex = 0
            Catch
            End Try
        End If

        Me.errlab.Visible = False
        Me.TextBox_SheetName.Text = "sheet1"
    End Sub

    Protected Sub Button_Upload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button_Upload.Click
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")

        Try
            If Not (FileUpload_CSV.PostedFile Is Nothing) Then
                Dim lstrFileFolder As String = ConfigurationSettings.AppSettings("FTPPath")

                If (Not Directory.Exists(lstrFileFolder)) Then
                    Directory.CreateDirectory(lstrFileFolder)
                End If

                Dim strExtension As String = Path.GetExtension(FileUpload_CSV.PostedFile.FileName)

                If strExtension <> ".csv" Then
                    Me.errlab.Visible = True
                    Me.errlab.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-UPLOAD-UPEXTENSION")
                    Return
                Else
                    If Me.TextBox_SheetName.Text.Trim = "" Then
                        Me.errlab.Visible = True
                        Me.errlab.Text = "Please enter Sheet Name to proceed."
                        Return
                    Else

                        Dim strFileName As String = Path.GetFileName(FileUpload_CSV.PostedFile.FileName)
                        Dim strFile As String = Left(strFileName, strFileName.Length - strExtension.Length)
                        Dim strPath As String = Server.MapPath("~/FTP/" & strFile & "_" & Date.Now.ToString("yyyyMMdd_HHmmss") & strExtension)

                        FileUpload_CSV.PostedFile.SaveAs(strPath) '向服务器端传文件

                        'added by LLY cater for diff purpose of uploading
                        Dim strMsg As String = String.Empty
                        Select Case Me.ddlUploadTo.SelectedIndex
                            Case 0
                                'check file name format
                                If FilenameIsOK(strFileName) Then
                                    strMsg = ImportFromExcel_Incentive_OTDemerit(strPath, strFileName, Me.TextBox_SheetName.Text.Trim)
                                Else
                                    Me.errlab.Visible = True
                                    Me.errlab.Text = Me.FileUpload_CSV.FileName.ToString() + Environment.NewLine() + objXmlTr.GetLabelName("StatusMessage", "BB-FUN-UPLOAD-INVALIDFNDEMERIT")
                                    Return
                                End If
                            Case 1
                                'check file name format
                                If FilenameIsOK(strFileName) Then
                                    strMsg = ImportFromExcel_Incentive_Deduction(strPath, strFileName, Me.TextBox_SheetName.Text.Trim)
                                Else
                                    Me.errlab.Visible = True
                                    Me.errlab.Text = Me.FileUpload_CSV.FileName.ToString() + Environment.NewLine() + objXmlTr.GetLabelName("StatusMessage", "BB-FUN-UPLOAD-INVALIDFNDEDUCTION")
                                    Return
                                End If
                                'Case 2 - removed lly
                                'strMsg = ImportFromExcel(strPath, Me.TextBox_SheetName.Text.Trim)

                        End Select
                        'Dim strMsg As String = ImportFromExcel(strPath, Me.TextBox_SheetName.Text.Trim)

                        If strMsg = "" Then
                            Me.errlab.Visible = True
                            Me.errlab.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-UPLOAD-UPSUCCESS")
                        Else
                            Me.errlab.Visible = True
                            Me.errlab.Text = strMsg
                        End If
                    End If
                End If
            Else
                Me.errlab.Visible = True
                Me.errlab.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-UPLOAD-FPATHEMPTY")
                Return
            End If
        Catch ex As Exception
            Me.errlab.Visible = True
            'Me.errlab.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-UPLOAD-UPFAIL")
            Me.errlab.Text = ex.Message.ToString()
        End Try
    End Sub

    Private Function ImportFromExcel_Incentive_OTDemerit(ByVal filePath As String, ByVal FileName As String, ByVal SheetName As String) As String
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")

        Try
            Dim Folder As New DirectoryInfo(ConfigurationSettings.AppSettings("FTPPath"))

            If Folder.Exists Then
                'checking only
                Dim objStreamReader As StreamReader = New StreamReader(filePath)
                Dim UploadClass As New clsUploadIncentiveDemerit()
                Dim strLine() As String
                Dim cnt As Integer = 0
                Dim separator As Char = ","
                Dim bSuccess As Boolean = True
                Dim strErrorMsg As String = ""

                Do While objStreamReader.Peek() >= 0

                    ReDim Preserve strLine(cnt)
                    strLine(cnt) = objStreamReader.ReadLine()
                    Dim fieldValues As String() = strLine(cnt).Split(separator)

                    If cnt >= 1 Then
                        If fieldValues.Length = 7 Then

                            Dim dsFINCUPLOAD As DataSet = UploadClass.GetExistFINCUPLOAD_FIL(FileName, fieldValues(0), Session("userID").ToString())

                            'Technician Id cannot be empty
                            If fieldValues(0) Is Nothing OrElse fieldValues(0).ToString = "" Then
                                strErrorMsg = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-UPLOAD-INVALIDTCHID")
                                bSuccess = False
                                Exit Do
                            End If

                            'Technician Name cannot be empty
                            If fieldValues(1) Is Nothing OrElse fieldValues(1) = "" Then
                                strErrorMsg = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-UPLOAD-INVALIDTCHNAME")
                                bSuccess = False
                                Exit Do
                            End If

                            'Demerit_MS cannot be empty
                            If fieldValues(2) Is Nothing OrElse fieldValues(2) = "" Then
                                strErrorMsg = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-UPLOAD-INVALIDDEMERITMS")
                                bSuccess = False
                                Exit Do
                                'Demerit_MS is integer type
                            ElseIf IsNumeric(fieldValues(2)) = False Then
                                strErrorMsg = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-UPLOAD-INVALIDDEMERITMS")
                                bSuccess = False
                                Exit Do
                            End If

                            'Demerit_RptJob cannot be empty
                            If fieldValues(3) Is Nothing OrElse fieldValues(3) = "" Then
                                strErrorMsg = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-UPLOAD-INVALIDDEMERITRPTJOB")
                                bSuccess = False
                                Exit Do
                            ElseIf IsNumeric(fieldValues(3)) = False Then
                                strErrorMsg = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-UPLOAD-INVALIDDEMERITRPTJOB")
                                bSuccess = False
                                Exit Do
                            End If

                            'Premium_ML cannot be empty
                            If fieldValues(4) Is Nothing OrElse fieldValues(4) = "" Then
                                strErrorMsg = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-UPLOAD-INVALIDPREMIUMML")
                                bSuccess = False
                                Exit Do
                            ElseIf IsNumeric(fieldValues(4)) = False Then
                                strErrorMsg = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-UPLOAD-INVALIDPREMIUMML")
                                bSuccess = False
                                Exit Do
                            End If
                            'Premium_PL cannot be empty
                            If fieldValues(5) Is Nothing OrElse fieldValues(5) = "" Then
                                strErrorMsg = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-UPLOAD-INVALIDPREMIUMPL")
                                bSuccess = False
                                Exit Do
                            ElseIf IsNumeric(fieldValues(5)) = False Then
                                strErrorMsg = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-UPLOAD-INVALIDPREMIUMPL")
                                bSuccess = False
                                Exit Do
                            End If
                            'OT_Sunday cannot be empty
                            If fieldValues(6) Is Nothing OrElse fieldValues(6) = "" Then
                                strErrorMsg = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-UPLOAD-INVALIDOTSUNDAY")
                                bSuccess = False
                                Exit Do
                            ElseIf IsNumeric(fieldValues(6)) = False Then
                                strErrorMsg = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-UPLOAD-INVALIDOTSUNDAY")
                                bSuccess = False
                                Exit Do
                            End If

                            'checking validity here
                            If dsFINCUPLOAD Is Nothing Or dsFINCUPLOAD.Tables(0).Rows.Count = 0 Then
                                'do insert if nothing
                                UploadClass.IncentiveList.Add(New clsUploadIncentiveDemerit(FileName, Session("userID").ToString().ToUpper, fieldValues(0).ToString.ToUpper, fieldValues(1), fieldValues(2), fieldValues(3), fieldValues(4), fieldValues(5), fieldValues(6), "I"))
                            Else
                                'do update
                                UploadClass.IncentiveList.Add(New clsUploadIncentiveDemerit(FileName, Session("userID").ToString().ToUpper, fieldValues(0).ToString.ToUpper, fieldValues(1), fieldValues(2), fieldValues(3), fieldValues(4), fieldValues(5), fieldValues(6), "U"))
                            End If


                        Else
                            strErrorMsg = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-UPLOAD-INVALIDFIELD")
                            bSuccess = False
                            Exit Do
                        End If
                        
                       
                    End If
                    cnt += 1
                Loop
                objStreamReader.Close()

                If bSuccess Then
                    Dim iSuccess As Integer = UploadClass.Insert(UploadClass, Session("userID").ToString().ToUpper)

                    If iSuccess <> 0 Then
                        Return objXmlTr.GetLabelName("StatusMessage", "BB-FUN-UPLOAD-UPFAIL")
                    Else
                        Return objXmlTr.GetLabelName("StatusMessage", "BB-FUN-UPLOAD-UPSUCCESS")
                    End If
                Else
                    Return strErrorMsg
                End If
            Else
                Return objXmlTr.GetLabelName("StatusMessage", "BB-FUN-UPLOAD-UPFAIL")

            End If

        Catch ex As Exception
            Return ex.Message
        End Try

        Return ""
    End Function

    Private Function ImportFromExcel_Incentive_Deduction(ByVal filePath As String, ByVal FileName As String, ByVal SheetName As String) As String
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")

        Try
            Dim Folder As New DirectoryInfo(ConfigurationSettings.AppSettings("FTPPath"))

            If Folder.Exists Then
                'checking only
                Dim objStreamReader As StreamReader = New StreamReader(filePath)
                Dim UploadClass As New clsUploadIncentiveDeduction()
                Dim strLine() As String
                Dim cnt As Integer = 0
                Dim separator As Char = ","
                Dim bSuccess As Boolean = True
                Dim strErrorMsg As String = ""
                Dim result As Double = 0.0
                Do While objStreamReader.Peek() >= 0

                    ReDim Preserve strLine(cnt)
                    strLine(cnt) = objStreamReader.ReadLine()
                    Dim fieldValues As String() = strLine(cnt).Split(separator)

                    If cnt >= 1 Then

                        Dim dsFINCUPLOAD As DataSet = UploadClass.GetExistFINCUPLOAD_FIL(FileName, fieldValues(0), Session("userID").ToString())

                        If fieldValues.Length = 6 Then
                            'Technician Id cannot be empty
                            If fieldValues(0) Is Nothing OrElse fieldValues(0).ToString = "" Then
                                strErrorMsg = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-UPLOAD-INVALIDTCHID")
                                bSuccess = False
                                Exit Do
                            End If

                            'Technician Name cannot be empty
                            If fieldValues(1) Is Nothing OrElse fieldValues(1) = "" Then
                                strErrorMsg = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-UPLOAD-INVALIDTCHNAME")
                                bSuccess = False
                                Exit Do
                            End If

                            'Deduction_LateSub cannot be empty
                            If fieldValues(2) Is Nothing OrElse fieldValues(2) = "" Then
                                strErrorMsg = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-UPLOAD-INVALIDDEDUCTIONLATESUB")
                                bSuccess = False
                                Exit Do
                                'Deduction_LateSub is 2 decimal places 
                            ElseIf Double.TryParse(fieldValues(2), result) = False Then
                                strErrorMsg = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-UPLOAD-INVALIDDEDUCTIONLATESUB")
                                bSuccess = False
                                Exit Do
                            End If

                            'Deduction_Late cannot be empty
                            If fieldValues(3) Is Nothing OrElse fieldValues(3) = "" Then
                                strErrorMsg = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-UPLOAD-INVALIDDEDUCTIONLATE")
                                bSuccess = False
                                Exit Do
                                'Deduction_Late is 2 decimal places 
                            ElseIf Double.TryParse(fieldValues(3), result) = False Then
                                strErrorMsg = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-UPLOAD-INVALIDDEDUCTIONLATE")
                                bSuccess = False
                                Exit Do
                            End If

                            'Deduction_Others cannot be empty
                            If fieldValues(4) Is Nothing OrElse fieldValues(4) = "" Then
                                strErrorMsg = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-UPLOAD-INVALIDDEDUCTIONOTHERS")
                                bSuccess = False
                                Exit Do
                                'Deduction_Others is 2 decimal places 
                            ElseIf Double.TryParse(fieldValues(4), result) = False Then
                                strErrorMsg = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-UPLOAD-INVALIDDEDUCTIONOTHERS")
                                bSuccess = False
                                Exit Do
                            End If
                            'Deduction_Sales cannot be empty
                            If fieldValues(5) Is Nothing OrElse fieldValues(5) = "" Then
                                strErrorMsg = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-UPLOAD-INVALIDDEMERITSALES")
                                bSuccess = False
                                Exit Do
                                'Deduction_Sales is 2 decimal places 
                            ElseIf Double.TryParse(fieldValues(5), result) = False Then
                                strErrorMsg = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-UPLOAD-INVALIDDEMERITSALES")
                                bSuccess = False
                                Exit Do
                            End If

                            'checking validity here
                            If dsFINCUPLOAD Is Nothing Or dsFINCUPLOAD.Tables(0).Rows.Count = 0 Then
                                'do insert if nothing
                                UploadClass.IncentiveList.Add(New clsUploadIncentiveDeduction(FileName, Session("userID").ToString().ToUpper, fieldValues(0).ToString.ToUpper, fieldValues(1), fieldValues(2), fieldValues(3), fieldValues(4), fieldValues(5), "I"))
                            Else
                                'do update
                                UploadClass.IncentiveList.Add(New clsUploadIncentiveDeduction(FileName, Session("userID").ToString().ToUpper, fieldValues(0).ToString.ToUpper, fieldValues(1), fieldValues(2), fieldValues(3), fieldValues(4), fieldValues(5), "U"))
                            End If

                        Else
                            strErrorMsg = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-UPLOAD-INVALIDFIELD")
                            bSuccess = False
                            Exit Do

                        End If
                    End If
                    cnt += 1
                Loop
                objStreamReader.Close()

                If bSuccess Then
                    Dim iSuccess As Integer = UploadClass.Insert(UploadClass, Session("userID").ToString().ToUpper)

                    If iSuccess <> 0 Then
                        Return objXmlTr.GetLabelName("StatusMessage", "BB-FUN-UPLOAD-UPFAIL")
                    Else
                        Return objXmlTr.GetLabelName("StatusMessage", "BB-FUN-UPLOAD-UPSUCCESS")
                    End If
                Else
                    Return strErrorMsg
                End If
            Else
                Return objXmlTr.GetLabelName("StatusMessage", "BB-FUN-UPLOAD-UPFAIL")

            End If

        Catch ex As Exception
            Return ex.Message
        End Try

        Return ""

    End Function
    Private Function ImportFromExcel(ByVal filePath As String, ByVal SheetName As String) As String
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")

        Try
            Dim Folder As New DirectoryInfo(ConfigurationSettings.AppSettings("FTPPath"))

            If Folder.Exists Then
                'checking only
                Dim objStreamReader As StreamReader = New StreamReader(filePath)
                Dim UploadClass As New clsUpload()
                Dim strLine() As String
                Dim cnt As Integer = 0
                Dim separator As Char = ","
                Dim bSuccess As Boolean = True
                Dim strErrorMsg As String = ""

                Do While objStreamReader.Peek() >= 0
                    ReDim Preserve strLine(cnt)
                    strLine(cnt) = objStreamReader.ReadLine()
                    Dim fieldValues As String() = strLine(cnt).Split(separator)

                    If cnt >= 1 Then
                        Dim dsFIV1_CR As DataSet = UploadClass.GetExistFIV1_CR(fieldValues(0), Session("userID").ToString())

                        'Service Bill No cannot be empty
                        If fieldValues(0) Is Nothing OrElse fieldValues(0).ToString = "" Then
                            strErrorMsg = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-UPLOAD-INVALIDSVTBIL")
                            bSuccess = False
                            Exit Do
                        Else
                            'checking validity here
                            If dsFIV1_CR Is Nothing Then
                                strErrorMsg = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-UPLOAD-INVALIDSVTBIL")
                                bSuccess = False
                                Exit Do
                            End If
                        End If

                        'Amount cannot be empty
                        If fieldValues(1) Is Nothing OrElse fieldValues(1) = "" Then
                            strErrorMsg = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-UPLOAD-INVALIDAMT")
                            bSuccess = False
                            Exit Do
                        Else
                            'Amount must be double type
                            If IsNumeric(fieldValues(1)) = False Then
                                strErrorMsg = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-UPLOAD-INVALIDAMT")
                                bSuccess = False
                                Exit Do
                            Else
                                'Amount must be greater than balance
                                If dsFIV1_CR.Tables(0).Rows.Count <> 0 Then
                                    If Convert.ToDouble(fieldValues(1)) > Convert.ToDouble(dsFIV1_CR.Tables(0).Rows(0).Item("FIV1_CR_BAL")) Then
                                        strErrorMsg = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-UPLOAD-INVALIDAMT")
                                        bSuccess = False
                                        Exit Do
                                    End If
                                End If
                            End If
                        End If

                        'Payment Collection Date cannot be empty
                        If fieldValues(2) Is Nothing OrElse fieldValues(2) = "" Then
                            strErrorMsg = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-UPLOAD-INVALIDDATE")
                            bSuccess = False
                            Exit Do
                        Else
                            'Payment Collection Date must be date type
                            If IsDate(fieldValues(2)) = False Then
                                strErrorMsg = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-UPLOAD-INVALIDDATE")
                                bSuccess = False
                                Exit Do
                            Else
                                'Payment Collection Date cannot greater than today
                                If Date.Compare(Convert.ToDateTime(fieldValues(2)), Today) > 0 Then
                                    strErrorMsg = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-UPLOAD-INVALIDDATE")
                                    bSuccess = False
                                    Exit Do
                                End If
                            End If
                        End If

                        'Payment Type cannot be empty
                        If fieldValues(3) Is Nothing OrElse fieldValues(3) = "" Then
                            strErrorMsg = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-UPLOAD-INVALIDTYPE")
                            bSuccess = False
                            Exit Do
                        Else
                            'Payment Type must be P or F
                            If fieldValues(3).ToString.ToUpper <> "P" AndAlso fieldValues(3).ToString.ToUpper <> "F" Then
                                strErrorMsg = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-UPLOAD-INVALIDTYPE")
                                bSuccess = False
                                Exit Do
                            Else
                                'F type cannot duplicate
                                Dim dsFIV1_CR_UPLOAD As DataSet = UploadClass.GetExistFIV1_CR_UPLOAD(fieldValues(0), Session("userID").ToString())

                                If dsFIV1_CR_UPLOAD Is Nothing Then
                                    strErrorMsg = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-UPLOAD-INVALIDSVTBIL")
                                    bSuccess = False
                                    Exit Do
                                Else
                                    If dsFIV1_CR_UPLOAD.Tables(0).Rows.Count <> 0 Then
                                        strErrorMsg = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-UPLOAD-INVALIDSVTBIL")
                                        bSuccess = False
                                        Exit Do
                                    Else
                                        If fieldValues(3).ToUpper = "F" Then
                                            If Convert.ToDouble(fieldValues(1)) <> Convert.ToDouble(dsFIV1_CR.Tables(0).Rows(0).Item("FIV1_CR_BAL")) Then
                                                strErrorMsg = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-UPLOAD-INVALIDAMT")
                                                bSuccess = False
                                                Exit Do
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                        End If

                        UploadClass.ProductsList.Add(New clsUpload(fieldValues(0).ToString.ToUpper, fieldValues(1), fieldValues(2), fieldValues(3).ToString.ToUpper, filePath, Session("userID").ToString()))
                    End If

                    cnt += 1
                Loop

                objStreamReader.Close()

                If bSuccess Then
                    Dim iSuccess As Integer = UploadClass.Insert(UploadClass, Session("userID").ToString().ToUpper)

                    If iSuccess <> 0 Then
                        Return objXmlTr.GetLabelName("StatusMessage", "BB-FUN-UPLOAD-UPFAIL")
                    Else
                        Return objXmlTr.GetLabelName("StatusMessage", "BB-FUN-UPLOAD-UPSUCCESS")
                    End If
                Else
                    Return strErrorMsg
                End If
            Else
                Return objXmlTr.GetLabelName("StatusMessage", "BB-FUN-UPLOAD-UPFAIL")
            End If
        Catch ex As Exception
            Return ex.Message
        End Try

        Return ""

    End Function

    Public Function FilenameIsOK(ByVal fileName As String) As Boolean

        'ess_technician_cal_inc_demerit
        'ess_technician_cal_inc_deduction
        Dim splitPath() As String
        splitPath = fileName.Split("_")
        If splitPath.Length = 6 Then
            If (fileName.Substring(0, fileName.Length - 10) = "ess_technician_cal_inc_demerit_") Then
                If Me.ddlUploadTo.SelectedIndex = 0 Then
                    Dim culture As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-US")
                    Try
                        Dim dtdate As DateTime = DateTime.ParseExact(splitPath(5).Substring(0, 6), "MMyyyy", System.Globalization.CultureInfo.InvariantCulture)
                    Catch ex As Exception
                        Return False
                    End Try
                Else
                    Return False
                End If
            ElseIf (fileName.Substring(0, fileName.Length - 10) = "ess_technician_cal_inc_deduction_") Then
                If Me.ddlUploadTo.SelectedIndex = 0 Then
                    Return False
                Else

                    If Me.ddlUploadTo.SelectedIndex = 1 Then
                        Dim culture As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-US")
                        Try
                            Dim dtdate As DateTime = DateTime.ParseExact(splitPath(5).Substring(0, 6), "MMyyyy", System.Globalization.CultureInfo.InvariantCulture)
                        Catch ex As Exception
                            Return False
                        End Try
                    Else
                        Return False
                    End If

                End If
            Else


            End If
            If (fileName.Substring(0, fileName.Length - 10) = "ess_technician_cal_inc_demerit_" Or fileName.Substring(0, fileName.Length - 10) = "ess_technician_cal_inc_deduction_") Then
                Dim culture As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-US")
                Try
                    Dim dtdate As DateTime = DateTime.ParseExact(splitPath(5).Substring(0, 6), "MMyyyy", System.Globalization.CultureInfo.InvariantCulture)
                Catch ex As Exception
                    Return False
                End Try
            End If
        Else
            Return False
        End If



        Return True
    End Function

End Class
