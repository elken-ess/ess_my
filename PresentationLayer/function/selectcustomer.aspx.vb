Imports BusinessEntity
Imports System.Configuration
Imports System.Windows.Forms
Imports System.Data
Imports System.Data.SqlClient

Partial Class PresentationLayer_masterrecord_CustomerRecord
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If (Not Page.IsPostBack) Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try

            Try
                BindGrid()
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try
        End If
    End Sub
    Protected Sub BindGrid()
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        customerIDlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0001")
        customernamelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0002")
        areaIDlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0003")
        contactlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0004")
        roserialnolab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0005")
        'addButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Add")
        searchButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Search")
        titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BROCUSTOM")
        ctryStat.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Status")

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Me.addinfo.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-AR")
        Me.addinfo.Visible = False
        Me.Label1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-NR")
        Me.Label1.Visible = False
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Dim userIDNamestr As String = Session("userID").ToString().ToUpper + "-" + Session("username").ToString.ToUpper
        Dim rank As String = Session("login_rank")
        Dim customer As New clsCustomerRecord()
        customer.username = userIDNamestr


        Dim cntryStat As New clsrlconfirminf()

        Dim statParam As ArrayList = New ArrayList
        statParam = cntryStat.searchconfirminf("STATUS")
        Dim count As Integer
        Dim statid As String
        Dim statnm As String
        For count = 0 To statParam.Count - 1
            statid = statParam.Item(count)
            statnm = statParam.Item(count + 1)
            count = count + 1
            If statid.Equals("ACTIVE") Or statid.Equals("OBSOLETE") Then
                ctryStatDrop.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
            End If
        Next

        Dim area As New clsCommonClass


        If rank <> 0 Then
            area.spctr = Session("login_ctryID").ToString().ToUpper

        End If
        area.rank = rank

        'create  area the dropdownlist
        'Dim area As New clsCommonClass
        Dim areads As New DataSet

        areads = area.Getcomidname("BB_MASAREA_CUSTIDNAME")
        If areads.Tables.Count <> 0 Then
            databonds(areads, Me.areaID)
        End If
        Me.areaID.Items.Insert(0, "")

        'Dim cntryStat As New clsrlconfirminf()

        Dim telepass As New clsCommonClass()


        Dim CustomerEntity As New clsCustomerRecord()
        CustomerEntity.ModBy = userIDNamestr
        If (Trim(Me.contactbox.Text) <> "") Then

            CustomerEntity.Contact = telepass.telconvertpass(contactbox.Text.ToUpper())
            CustomerEntity.Contacttype = "Y"
        Else
            CustomerEntity.Contacttype = "N"
        End If
        'Dim rank As String = Session("login_rank")
        If rank <> 0 Then
            CustomerEntity.ctryid = Session("login_ctryID")
            CustomerEntity.compid = Session("login_cmpID")
            CustomerEntity.usvcid = Session("login_svcID")

        End If
        CustomerEntity.rank = rank
        CustomerEntity.Status = "ACTIVE"
        'Dim ctryall As DataSet = CustomerEntity.GetAllCustomer()
        'ctryView.DataSource = ctryall
        'Session("Customer") = ctryall
        'checknorecord(ctryall)
        ctryView.AllowPaging = True
        ctryView.AllowSorting = True


        Dim selectcol As New CommandField
        selectcol.SelectText = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-ACNT-SELECT")
        selectcol.HeaderImageUrl = "~/PresentationLayer/graph/edit.gif"
        selectcol.ShowSelectButton = True
        ctryView.Columns.Add(selectcol)

        ctryView.DataBind()
        DisplayGridHeader()

    End Sub

    Sub DisplayGridHeader()
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")

        If ctryView.Rows.Count >= 1 Then
            ctryView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0001")
            ctryView.HeaderRow.Cells(1).Font.Size = 8
            ctryView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0002")
            ctryView.HeaderRow.Cells(2).Font.Size = 8
            ctryView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASADD1Z")
            ctryView.HeaderRow.Cells(3).Font.Size = 8
            ctryView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASADD2Z")
            ctryView.HeaderRow.Cells(4).Font.Size = 8
            ctryView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPOCODEZ")
            ctryView.HeaderRow.Cells(5).Font.Size = 8
            ctryView.HeaderRow.Cells(6).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0009")
            ctryView.HeaderRow.Cells(6).Font.Size = 8
            ctryView.HeaderRow.Cells(7).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0005")
            ctryView.HeaderRow.Cells(7).Font.Size = 8
            ctryView.HeaderRow.Cells(8).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-ModelID")
            ctryView.HeaderRow.Cells(8).Font.Size = 8
            ctryView.HeaderRow.Cells(9).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-Country")
            ctryView.HeaderRow.Cells(9).Font.Size = 8
            ctryView.HeaderRow.Cells(10).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0008")
            ctryView.HeaderRow.Cells(10).Font.Size = 8
            ctryView.HeaderRow.Cells(11).Text = objXmlTr.GetLabelName("EngLabelMsg", "TELEPHONE")
            ctryView.HeaderRow.Cells(11).Font.Size = 8
        End If
    End Sub


    Protected Sub search_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles searchButton.Click
        checkinfo()
        Dim CustomerEntity As New clsCustomerRecord()
        Dim telepass As New clsCommonClass()

        If (Trim(Me.customerIDbox.Text) <> "") Then

            CustomerEntity.CustomerID = customerIDbox.Text.ToUpper()

        End If

        If (Trim(Me.customernamebox.Text) <> "") Then

            CustomerEntity.CustomerName = customernamebox.Text.ToUpper()

        End If

        If (Me.areaID.SelectedValue().ToString() <> "") Then
            CustomerEntity.AreaID = Me.areaID.SelectedValue().ToString().ToUpper()
        End If

        If (Trim(Me.roserialnobox.Text) <> "") Then

            CustomerEntity.RoSerialNo = roserialnobox.Text.ToUpper()

        End If
        If (Trim(Me.contactbox.Text) <> "") Then

            CustomerEntity.Contact = telepass.telconvertpass(contactbox.Text.ToUpper())
            CustomerEntity.Contacttype = "Y"
        Else
            CustomerEntity.Contacttype = "N"
        End If
        Dim rank As String = Session("login_rank")
        If rank <> 0 Then
            CustomerEntity.ctryid = Session("login_ctryID")
            CustomerEntity.compid = Session("login_cmpID")
            CustomerEntity.usvcid = Session("login_svcID")

        End If
        CustomerEntity.rank = rank

        If (Me.ctryStatDrop.SelectedValue().ToString() <> "") Then
            CustomerEntity.Status = ctryStatDrop.SelectedItem.Value.ToString()
        End If

        CustomerEntity.ModBy = Session("userID").ToString().ToUpper

        Dim ctryall As DataSet = CustomerEntity.GetAllCustomer()
        ctryView.DataSource = ctryall
        Session("Customer") = ctryall
        checknorecord(ctryall)

        ctryView.DataBind()
        DisplayGridHeader()

        Dim selectVisible As Boolean = ctryStatDrop.SelectedValue = "ACTIVE"
        ctryView.Columns(0).Visible = selectVisible
    End Sub


    Protected Sub ctryView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles ctryView.PageIndexChanging
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        Dim CustomerEntity As New clsCustomerRecord()
        ctryView.PageIndex = e.NewPageIndex
        'Dim priceidall As DataSet = PriceIDEntity.GetAllService()
        Dim Customerall As DataSet = Session("Customer")
        ctryView.DataSource = Customerall
        ctryView.DataBind()
        If Customerall.Tables(0).Rows.Count >= 1 Then
            ctryView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0001")
            ctryView.HeaderRow.Cells(1).Font.Size = 8
            ctryView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0002")
            ctryView.HeaderRow.Cells(2).Font.Size = 8
            ctryView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASADD1Z")
            ctryView.HeaderRow.Cells(3).Font.Size = 8
            ctryView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASADD2Z")
            ctryView.HeaderRow.Cells(4).Font.Size = 8
            ctryView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPOCODEZ")
            ctryView.HeaderRow.Cells(5).Font.Size = 8
            ctryView.HeaderRow.Cells(6).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0009")
            ctryView.HeaderRow.Cells(6).Font.Size = 8
            ctryView.HeaderRow.Cells(7).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0005")
            ctryView.HeaderRow.Cells(7).Font.Size = 8
            ctryView.HeaderRow.Cells(8).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-ModelID")
            ctryView.HeaderRow.Cells(8).Font.Size = 8
            ctryView.HeaderRow.Cells(9).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-Country")
            ctryView.HeaderRow.Cells(9).Font.Size = 8
            ctryView.HeaderRow.Cells(10).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0008")
            ctryView.HeaderRow.Cells(10).Font.Size = 8
            ctryView.HeaderRow.Cells(11).Text = objXmlTr.GetLabelName("EngLabelMsg", "TELEPHONE")
            ctryView.HeaderRow.Cells(11).Font.Size = 8
        End If
    End Sub
#Region " id bond"
    Public Function databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList) As ArrayList
        dropdown.Items.Clear()
        Dim row As DataRow
        Dim infon As ArrayList = New ArrayList()
        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            NewItem.Text = row("id") & "-" & row("name")
            NewItem.Value = row("id")
            dropdown.Items.Add(NewItem)
            infon.Add(NewItem.Value)
        Next
        Return infon

    End Function
#End Region
  
#Region "��ʾ��Ϣ"
    Public Function checkinfo() As Integer
        If Me.customerIDbox.Text = "" And Me.customernamebox.Text = "" And Me.roserialnobox.Text = "" And Me.contactbox.Text = "" And Me.areaID.Text = "" Then
            Me.addinfo.Visible = True
        Else
            Me.addinfo.Visible = False
        End If
    End Function
#End Region
#Region "if have no record "
    Public Function checknorecord(ByVal ds As DataSet) As Integer

        If Not ds Is Nothing Then
            If ds.HasErrors Then
                If ds.Tables(0).Rows.Count = 0 Then
                    Me.Label1.Visible = True
                    'Me.addinfo.Visible = False
                Else
                    Me.Label1.Visible = False

                End If

            End If

            If ds.Tables(0).Rows.Count = 0 Then
                Me.Label1.Visible = True
                'Me.addinfo.Visible = False
            Else
                Me.Label1.Visible = False

            End If
        Else
            Me.Label1.Visible = True
        End If


        

    End Function
#End Region
    

    Protected Sub ctryView_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles ctryView.SelectedIndexChanging
        Dim strTempURL As String
        Dim custID As String = ctryView.Rows(e.NewSelectedIndex).Cells(1).Text
        custID = Server.UrlEncode(custID)
        Dim custPf As String = Session("login_ctryID")
        custPf = Server.UrlEncode(custPf)
        strTempURL = "custID=" + custID + "&custPf=" + custPf
        strTempURL = "~/PresentationLayer/function/addcontract.aspx?" + strTempURL
        Response.Redirect(strTempURL)
    End Sub
End Class
