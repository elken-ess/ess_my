<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ViewApptScheduleRS.aspx.vb" Inherits="PresentationLayer_function_ViewApptScheduleRS_aspx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Master Appointment Schedule - Rental</title>
    <link href="../css/style.css" type="text/css" rel="stylesheet"/>
     <style type="text/css">A:link { COLOR: #336666; TEXT-DECORATION: none }
	BODY { FONT-SIZE: 9pt; FONT-FAMILY: "Arial", "Verdana", "Tahoma"; BACKGROUND-COLOR: #ffffff }
	td { FONT-SIZE: 9pt; FONT-FAMILY: Arial,Verdana,Tahoma }
	</style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td width="1%" background="../graph/title_bg.gif">
                        <img height="24" src="../graph/title1.gif" width="5"></td>
                    <td class="style2" width="98%" background="../graph/title_bg.gif">
                        <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
                    <td align="right" background="../graph/title_bg.gif" style="width: 1%">
                        <img height="24" src="../graph/title_2.gif" width="5"></td>
                </tr>
            </table>
            <table style="width: 100%;" bgColor="#b7e6e6">
                <tr bgColor="#ffffff" align="left">
                    <td valign="top">
                        <asp:Label ID="servCenterLab" runat="server" Text="Label" Width="80%"></asp:Label></td>
                        <td valign="top">
                        <asp:TextBox ID="servCenterBox" runat="server" Width="80%" CssClass="textborder"
                            ReadOnly="True"></asp:TextBox>
                    </td>
                    <td valign="top">
                        <asp:Label ID="apptDateLab" runat="server" Text="Label" Width="80%"></asp:Label></td>
                        <td valign="top">
                        <asp:TextBox ID="apptDateBox" runat="server" Width="80%" CssClass="textborder" ReadOnly="True"></asp:TextBox>
                    </td>
                    <td valign="top">
                        <asp:Label ID="zoneIDLab" runat="server" Text="Label" Width="80%"></asp:Label></td>
                        <td valign="top">
                        <asp:TextBox ID="zoneIDBox" runat="server" Width="80%" CssClass="textborder" ReadOnly="True"></asp:TextBox>
                    </td>              
                </tr>
                
                <tr bgColor="#ffffff" align="left">
                    <td valign="top">
                        <asp:Label ID="lblJobServiceType" runat="server" Text="JobServiceType" Width="80%"></asp:Label>
                         </td>
                        <td valign="top">
                        <asp:TextBox ID="txtJobServiceType" runat="server" Width="80%" CssClass="textborder"
                            ReadOnly="True"></asp:TextBox>
                    </td>
                    <td valign="top">
                        <asp:Label ID="lblTotalSSApt" runat="server" Text="lblTotalSSApt" Width="80%"></asp:Label> </td>
                        <td valign="top">
                        <asp:TextBox ID="txtTotalSSApt" runat="server" Width="80%" CssClass="textborder" ReadOnly="True"></asp:TextBox>
                    </td>
                    <td valign="top">
                        <asp:Label ID="lblTotalASApt" runat="server" Text="lblTotalASApt" Width="80%" Visible="False"></asp:Label>
                         </td>
                        <td valign="top">
                        <asp:TextBox ID="txtTotalASApt" runat="server" Width="80%" CssClass="textborder" ReadOnly="True" Visible="False"></asp:TextBox>
                    </td>              
                </tr>
                
                <tr bgColor="#ffffff">
                    <td colspan="6">
                        <asp:Label ID="lblShow" runat="server" Text="lblShow"></asp:Label>
                    </td>
                </tr>
                <tr bgColor="#ffffff" align="center">
                  <td colspan="6">
                       <asp:GridView ID="ApptView" Width="100%" runat="server" AutoGenerateColumns="False" >
                     <Columns>
                    <asp:TemplateField>
                      <ItemTemplate>
                         <%#Container.DataItemIndex + 1%>
                      </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    </asp:TemplateField>
                    <asp:BoundField  DataField="TRNTY">
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    </asp:BoundField>
                    <asp:BoundField  DataField="TRNNO">
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    </asp:BoundField>
                    <asp:BoundField  DataField="StartTime">
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    </asp:BoundField>
                    <asp:BoundField DataField="EndTime">
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    </asp:BoundField>
                    <asp:BoundField DataField="CUSTID">
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    </asp:BoundField>
                    <asp:BoundField DataField="CustName" >
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    </asp:BoundField>
                    <asp:BoundField DataField="CRID" >
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    </asp:BoundField>
                    <asp:BoundField DataField="TCHID" >
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    </asp:BoundField>
                     </Columns> 
                       </asp:GridView>
                  </td>
                </tr>
            </table>   <br />       
            <asp:HyperLink ID="backlink" NavigateUrl="javascript:history.back();" runat="server">HyperLink</asp:HyperLink></div>
    </form>
</body>
</html>
