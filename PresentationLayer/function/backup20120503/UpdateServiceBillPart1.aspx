<%@ Page Language="VB" AutoEventWireup="false" CodeFile="UpdateServiceBillPart1.aspx.vb" Inherits="PresentationLayer_Function_UpdateServiceBillPart1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
     <title>Update service bill part1</title>
      <meta http-equiv="Content-Type" content="text/html; charset=gb2312">
    <LINK href="../css/style.css" type="text/css" rel="stylesheet">
    <script language="JavaScript" src="../js/common.js"></script>
     <STYLE type="text/css">A:link { COLOR: #336666; TEXT-DECORATION: none }
	BODY { FONT-SIZE: 9pt; FONT-FAMILY: "Arial", "Verdana", "Tahoma"; BACKGROUND-COLOR: #ffffff }
	TD { FONT-SIZE: 9pt; FONT-FAMILY: Arial,Verdana,Tahoma }
	</STYLE>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table bgcolor="#b7e6e6" border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td background="../graph/title_bg.gif" width="1%">
                        <img height="24" src="../graph/title1.gif" width="5" /></td>
                    <td background="../graph/title_bg.gif" class="style2" width="100%">
                        <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
                </tr>
            </table>
        </div>
        <table bgcolor="#b7e6e6" width ="100%" style="height: 22px">
            <tr bgcolor="#ffffff">
                <td style="width: 10%">
                    <asp:Label ID="solab" runat="server" Text="Label" Width="90%"></asp:Label></td>
                <td style="width: 15%">
                    <asp:TextBox ID="soText" runat="server" Width="90%"></asp:TextBox></td>
                <td style="width: 10%">
                    <asp:Label ID="CNlab" runat="server" Text="Label" Width="90%"></asp:Label></td>
                <td style="width: 15%">
                    <asp:TextBox ID="CNText" runat="server" Width="90%"></asp:TextBox></td>
                <td style="width: 10%">
                    <asp:Label ID="ctryStat" runat="server" Text="Label"></asp:Label></td>
                <td style="width: 15%">
                    <asp:DropDownList ID="ctryStatDrop" runat="server" Width="98%">
                    </asp:DropDownList></td>
                
            </tr>
            <tr bgcolor="#ffffff">
                <td style="width: 10%">
                    <asp:Label ID="phnolab" runat="server" Text="Label" Width="90%"></asp:Label></td>
                <td style="width: 15%">
                    <asp:TextBox ID="phnoText" runat="server" Width="90%"></asp:TextBox></td>
                <td style="width: 10%">
                    <asp:Label ID="snlab" runat="server" Text="Label" Width="90%"></asp:Label></td>
                <td style="width: 15%">
                    <asp:TextBox ID="snText" runat="server" Width="90%"></asp:TextBox></td>
                <td style="width: 10%">
                    <asp:Label ID="technicianLab" runat="server" Text="Label"></asp:Label></td>
                <td style="width: 15%">
                    <asp:DropDownList ID="technicianList" runat="server" Width="98%">
                    </asp:DropDownList></td>
                
            </tr>
             <tr bgcolor="#ffffff">
                <td style="width: 10%">
                    <asp:Label ID="lblCustomerID" runat="server" Text="Customer ID" Width="90%"></asp:Label></td>
                <td style="width: 15%">
                    <asp:TextBox ID="txtCustomerID" runat="server" Width="90%"  MaxLength =20></asp:TextBox></td>
                <td style="width: 10%">
                    <asp:Label ID="lblAppointmentNo" runat="server" Text="Appointment No" Width="90%"></asp:Label></td>
                <td style="width: 15%">
                    <asp:TextBox ID="txtAppointmentPrefix" runat="server" Width="20%" MaxLength =2></asp:TextBox>
                    <asp:TextBox ID="txtAppointmentNo" runat="server" Width="70%"  MaxLength =20></asp:TextBox>
                    </td>
                <td style="width: 10%">
                    <asp:Label ID="lblManualServiceBillNo" runat="server" Text="Service Bill No"></asp:Label></td>
                <td style="width: 15%">
                   <asp:TextBox ID="txtManualServiceBillNo" runat="server" Width="90%" MaxLength =20></asp:TextBox>
                
            </tr>
            
            <tr bgcolor="#ffffff">
                <td style="width: 10%; height: 8px;">
        <asp:Label ID="lblStartDate" runat="server" Text="Label" Width="100%">Start Date</asp:Label></td>
                <td style="width: 15%; height: 8px;">
                    <asp:TextBox ID="txtStartDate" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
                    <asp:HyperLink ID="HypCal1" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                        ToolTip="Choose a Date">Choose a Date</asp:HyperLink></td>
                <td style="width: 10%; height: 8px;">
                    <asp:Label ID="lblEndDate" runat="server" Text="Label" Width="100%">End Date</asp:Label></td>
                <td style="width: 15%; height: 8px;">
                    <asp:TextBox ID="txtEndDate" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
                    <asp:HyperLink ID="HypCal2" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                        ToolTip="Choose a Date">Choose a Date</asp:HyperLink></td>
                <td style="width: 10%; height: 8px;">
                    <asp:LinkButton ID="searchbtn" runat="server">LinkButton</asp:LinkButton></td>
                <td style="width: 15%; height: 8px;">
                    <asp:LinkButton ID="LnkAdd" runat="server">LinkButton</asp:LinkButton></tr>
            
        </table>
      
        <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
        <br />
        <asp:GridView ID="partView" runat="server" AllowPaging="True" AllowSorting="True"
            Width="100%">
        </asp:GridView>
    </form>
</body>
</html>
