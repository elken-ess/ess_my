Imports BusinessEntity
Imports System.Configuration
Imports System.Xml
Imports SQLDataAccess
Imports System.Data.SqlClient
Imports System.Web
Imports System.Data
Imports System.Windows.Forms

Partial Class PresentationLayer_function_ServiceBillUpdate2
    Inherits System.Web.UI.Page

    Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
    Dim ConType As String



#Region "PRIVATE FUNCTIONS"

    Private Sub ComputePaymentTotal()
        Try
            lblError.Visible = False
            lblError.Text = ""
            If IsNumeric(txtCash.Text) Then
                If Val(txtCash.Text) < 0 Then
                    txtCash.Text = "0"
                End If
            Else
                txtCash.Text = "0"
            End If

            If IsNumeric(txtCheque.Text) Then
                If Val(txtCheque.Text) < 0 Then
                    txtCheque.Text = "0"
                End If
            Else
                txtCheque.Text = "0"
            End If

            If IsNumeric(txtCredit.Text) Then
                If Val(txtCredit.Text) < 0 Then
                    txtCredit.Text = "0"
                End If
            Else
                txtCredit.Text = "0"
            End If

            If IsNumeric(txtCreditCard.Text) Then
                If Val(txtCreditCard.Text) < 0 Then
                    txtCreditCard.Text = "0"
                End If
            Else
                txtCreditCard.Text = "0"
            End If

            If IsNumeric(txtOther.Text) Then
                If Val(txtOther.Text) < 0 Then
                    txtOther.Text = "0"
                End If
            Else
                txtOther.Text = "0"
            End If

            Dim PaymentTotal As Double
            PaymentTotal = Val(txtCash.Text) + Val(txtCheque.Text) + _
                    Val(txtCredit.Text) + Val(txtCreditCard.Text) + _
                    Val(txtOther.Text)

            Me.txtTotal.Text = String.Format("{0:f2}", PaymentTotal)
            Me.txtCash.Text = String.Format("{0:f2}", Val(Me.txtCash.Text))
            Me.txtCheque.Text = String.Format("{0:f2}", Val(Me.txtCheque.Text))
            Me.txtCredit.Text = String.Format("{0:f2}", Val(Me.txtCredit.Text))
            Me.txtCreditCard.Text = String.Format("{0:f2}", Val(Me.txtCreditCard.Text))
            Me.txtOther.Text = String.Format("{0:f2}", Val(Me.txtOther.Text))
        Catch

        End Try

    End Sub

    Private Sub databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList, Optional ByVal X As Boolean = True)
        dropdown.Items.Clear()
        Dim row As DataRow

        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            If X = True Then
                NewItem.Text = Trim(row("id") & "-" & row("name"))
            Else
                NewItem.Text = Trim(row("name"))
            End If
            NewItem.Value = Trim(row("id"))
            dropdown.Items.Add(NewItem)
        Next

    End Sub


    Private Sub GetCustomerRO()
        Try
            Dim clsCustomer As New clsServiceBillUpdate
            clsCustomer.Country = Me.cboCountry.SelectedValue
            clsCustomer.CustomerPrefix = txtCustomerPrefix.Text
            clsCustomer.CustomerNo = txtCustomerID.Text

            Dim dstCustomer As New DataSet
            dstCustomer = clsCustomer.GetCustomerRO

            If dstCustomer.Tables.Count <> 0 Then
                databonds(dstCustomer, cboRO, False)
            End If
            cboRO.Items.Insert(0, "")
            'cboRO.SelectedIndex = 0
        Catch

        End Try

    End Sub


    Private Sub InitializeGridColumns()
        Try
            'get column labels
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            Me.grdServiceBill.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-NO")
            Me.grdServiceBill.HeaderRow.Cells(1).Font.Size = 8

            Me.grdServiceBill.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-PARTCODE")
            Me.grdServiceBill.HeaderRow.Cells(2).Font.Size = 8

            Me.grdServiceBill.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-PARTDESC")
            Me.grdServiceBill.HeaderRow.Cells(3).Font.Size = 8

            Me.grdServiceBill.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-QUANTITY")
            Me.grdServiceBill.HeaderRow.Cells(4).Font.Size = 8

            Me.grdServiceBill.HeaderRow.Cells(6).Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-UNITPRICE")
            Me.grdServiceBill.HeaderRow.Cells(6).Font.Size = 8

            Me.grdServiceBill.HeaderRow.Cells(7).Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-TAX")
            Me.grdServiceBill.HeaderRow.Cells(7).Font.Size = 8

            Me.grdServiceBill.HeaderRow.Cells(8).Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-LINETOTAL")
            Me.grdServiceBill.HeaderRow.Cells(8).Font.Size = 8

            Me.grdServiceBill.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-PRICEID")
            Me.grdServiceBill.HeaderRow.Cells(5).Font.Size = 8
        Catch

        End Try

    End Sub

    Private Sub InitializePage()
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")

        '*******************************************************************
        ' BEGIN INITIALIZE LABELS AND VALIDATORS
        '*******************************************************************
        '************
        ' LABELS    
        '************
        Me.lblAppoinmentNo.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-APPOINMENTNO")
        Me.lblCash.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-CASH")
        Me.lblCheque.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-CHEQUE")
        Me.lblCompany.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-COMPANY")
        Me.lblCountry.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-COUNTRY")
        Me.lblCreateDate.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-CREATEDATE")
        Me.lblCreatedBy.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-CREATEBY")
        Me.lblCredit.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-CREDIT")
        Me.lblCreditCard.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-CREDITCARD")
        Me.lblCustomer.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-CUSTOMER")
        Me.lblCustomerID.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-CUSTOMERID")
        Me.lblDate.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-DATE")
        Me.lblDiscount.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-DISCOUNT")
        Me.lblInclusive.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-INCLUSIVE")
        Me.lblInvoiceTax.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-INVOICETAX")
        Me.lblInvoiceNo.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-INVOICENO")
        Me.lblModifyBy.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-MODIFYBY")
        Me.lblModifyDate.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-MODIFYDATE")
        Me.lblOther.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-OTHER")
        Me.lblPaymentDetails.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-PAYMENTDETAILS")
        Me.lblDiscountRemarks.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-DISCOUNTREMARKS")
        Me.lblRemarks.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-REMARKS")
        Me.lblROSerialNo.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-ROSERIALNO")
        Me.lblServiceBillNo.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-SERVICEBILLNO")
        Me.lblServiceBillType.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-SERVICEBILLTYPE")
        Me.lblServiceCenter.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-SERVICECENTER")
        Me.lblServiceType.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-SERVICETYPE")
        Me.lblStatus.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-STATUS")
        Me.lblTechnician.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-TECHNICIAN")
        Me.lblTotal.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-TOTAL")
        Me.lblTotalGross.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-TOTAL")
        Me.lblTotalNet.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-TOTAL")
        Me.lblUpdateStockList.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-UPDATESTOCKLIST")
        Me.lblContractID.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-CONTRACTNO")

        titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-update2")
        CompDateVal.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "INVALID_DATE")

        Me.lnkSave.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-SAVE")
        lblNoteUp.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
        lblNoteDown.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")

        '************
        ' VALIDATORS   
        '************

        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Me.rfvServiceType.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRSERVICETYPE")
        Me.rfvCustomerNo.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRCUSTOMERNO")
        Me.rfvRO.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRRO")


        '*******************************************************************
        ' END INITIALIZE LABELS AND VALIDATORS
        '*******************************************************************
       

        Dim CommonClass As New clsCommonClass()
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        Dim userIDNamestr As String = Session("userID").ToString().ToUpper + "-" + Session("username").ToString.ToUpper
        Dim rank As String = Session("login_rank")
        CommonClass.spstat = Session("login_cmpID")
        CommonClass.sparea = Session("login_svcID")

        If rank <> 0 Then
            CommonClass.spctr = Session("login_ctryID").ToString().ToUpper
        End If
        CommonClass.rank = rank
        CommonClass.spstat = "ACTIVE"


        objXmlTr.XmlFile = System.Configuration.ConfigurationSettings.AppSettings("XmlFilePath")

        Dim Util As New clsUtil()
        Dim statParam As ArrayList = New ArrayList

        statParam = Util.searchconfirminf("SERBILLTY")
        Dim count As Integer
        Dim statid As String
        Dim statnm As String
        For count = 0 To statParam.Count - 1
            statid = statParam.Item(count)
            statnm = statParam.Item(count + 1)
            count = count + 1
            Me.cboServiceBillType.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
        Next
        'Me.cboServiceBillType.Items.Insert(0, "")

        Dim yesnopar As ArrayList = New ArrayList
        yesnopar = Util.searchconfirminf("YESNO")
        
        For count = 0 To yesnopar.Count - 1
            statid = yesnopar.Item(count)
            statnm = yesnopar.Item(count + 1)
            Me.cboFreeService.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
            count += 1
        Next
        cboFreeService.SelectedIndex = 1

        statParam = Util.searchconfirminf("SerStat")
        For count = 0 To statParam.Count - 1
            statid = statParam.Item(count)
            statnm = statParam.Item(count + 1)
            count = count + 1
            If UCase(statid) = "UM" Or UCase(statid) = "UA" Then
                cboStatus.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
            End If
        Next
        cboStatus.Items(0).Selected = True


        Dim dsCountry As New DataSet
        Me.cboCountry.DataTextField = "name"
        Me.cboCountry.DataValueField = "id"
        dsCountry = CommonClass.Getcomidname("BB_MASCTRY_IDNAME")
        If dsCountry.Tables.Count <> 0 Then
            databonds(dsCountry, Me.cboCountry)
        End If
        Me.cboCountry.Items.Insert(0, "")
        Me.cboCountry.SelectedIndex = 0
        Me.cboCountry.SelectedValue = Session("login_ctryID").ToString().ToUpper
        Me.cboCountry.Enabled = False


        Dim dsSerType As New DataSet
        dsSerType = CommonClass.Getcomidname("BB_MASSRCT_IDNAMEALL")
        If dsSerType.Tables.Count <> 0 Then
            databonds(dsSerType, Me.cboServiceType)
        End If
        cboServiceType.Items.Insert(0, "")

        Dim dscompany As New DataSet
        CommonClass.spstat = Session("login_cmpID")

        dscompany = CommonClass.Getcomidname("BB_MASCOMP_IDNAME")
        If dscompany.Tables.Count <> 0 Then
            databonds(dscompany, Me.cboCompany)
        End If
        Me.cboCompany.Items.Insert(0, "")
        Me.cboCompany.SelectedIndex = 0
        Me.cboCompany.SelectedValue = Session("login_cmpID")
        Me.cboCompany.Enabled = False

        Dim dsServiceCentere As New DataSet
        CommonClass.spstat = Session("login_cmpID")

        dsServiceCentere = CommonClass.Getcomidname("BB_MASSVRC_IDNAME")

        If dsServiceCentere.Tables.Count <> 0 Then
            databonds(dsServiceCentere, Me.cboServiceCenter)
        End If
        Me.cboServiceCenter.Items.Insert(0, "")
        Me.cboServiceCenter.SelectedIndex = 0
        Me.cboServiceCenter.SelectedValue = Session("login_svcID")
        Me.cboServiceCenter.Enabled = False

        Dim dsTechnician As New DataSet
        dsTechnician = CommonClass.Getcomidname("BB_MASTECH_IDNAME")
        If dsTechnician.Tables.Count <> 0 Then
            databonds(dsTechnician, Me.cboTechnician)
        End If
        Me.cboTechnician.Items.Insert(0, "")
        Me.cboTechnician.SelectedIndex = 0


        txtCreatedBy.Text = userIDNamestr
        txtModifyBy.Text = userIDNamestr
        txtCreateDate.Text = Now().ToString
        txtModifyDate.Text = Now().ToString

        PopulateFromSearch()

    End Sub

    Private Sub PopulateFromSearch()
        '*******************************************************************
        ' BEGIN POPULATE VALUES FROM SEARCH
        '*******************************************************************
        Dim CommonClass As New clsCommonClass()
        Dim clsServiceBill As New clsupdateservice2
        Dim dsServiceBill As New DataSet
        Dim drServiceBill As DataRow
        Dim dsServiceCentere As New DataSet
        Dim dsTechnician As New DataSet
        Dim rank As String = Session("login_rank")
        txtInvoicePf.Text = Request.QueryString("type")
        txtInvoiceNo.Text = Request.QueryString("no")

        Dim htSvcBill As New Hashtable


        If txtInvoicePf.Text <> "" Then
            'If Session("htSvcBill") Is Nothing Then


            dsServiceBill = clsServiceBill.selds(txtInvoicePf.Text, txtInvoiceNo.Text)
        Else
            htSvcBill = Session("htSvcBill")
            txtInvoicePf.Text = htSvcBill.Item("InvoicePf")
            txtInvoiceNo.Text = htSvcBill.Item("InvoiceNo")
            dsServiceBill = clsServiceBill.selds(txtInvoicePf.Text, txtInvoiceNo.Text)


        End If
        'End If
        dsServiceCentere = CommonClass.Getcomidname("BB_MASSVRC_IDNAME")
        If dsServiceCentere.Tables.Count <> 0 Then
            databonds(dsServiceCentere, Me.cboServiceCenter)
        End If
        Me.cboServiceCenter.Items.Insert(0, "")
        Me.cboServiceCenter.SelectedIndex = 0

        Me.cboServiceCenter.Enabled = False

        If dsServiceBill.Tables.Count <> 0 Then
            If dsServiceBill.Tables(6).Rows.Count > 0 Then

                grdServiceBill.DataSource = dsServiceBill.Tables(6)
                grdServiceBill.DataBind()
                InitializeGridColumns()
            End If


            If dsServiceBill.Tables(5).Rows.Count > 0 Then
                drServiceBill = dsServiceBill.Tables(5).Rows(0)
                CommonClass.rank = rank
                CommonClass.spstat = drServiceBill.Item(3)
                CommonClass.sparea = drServiceBill.Item(2)
                CommonClass.spctr = drServiceBill.Item(4)

                Me.cboServiceCenter.SelectedValue = drServiceBill.Item(2)

                dsTechnician = CommonClass.Getcomidname("BB_MASTECH_IDNAME")
                If dsTechnician.Tables.Count <> 0 Then
                    databonds(dsTechnician, Me.cboTechnician)
                End If
                Me.cboTechnician.Items.Insert(0, "")
                Me.cboTechnician.SelectedIndex = 0

                Me.cboServiceCenter.SelectedValue = drServiceBill.Item(2)
                Me.cboCompany.SelectedValue = drServiceBill.Item(3)
                Me.cboCountry.SelectedValue = drServiceBill.Item(4)
                Me.txtServiceBillNo.Text = drServiceBill.Item(5)
                Me.cboServiceBillType.SelectedValue = drServiceBill.Item(6)

                Me.txtDate.Text = Format(drServiceBill.Item(7), "dd/MM/yyyy")
                Me.cboTechnician.SelectedValue = drServiceBill.Item(8)
                Me.cboServiceType.SelectedValue = Trim(drServiceBill.Item(9))
                Me.txtCustomerPrefix.Text = drServiceBill.Item(13)
                Me.txtCustomerID.Text = drServiceBill.Item(14)
                Me.txtCustomer.Text = drServiceBill.Item(15)
                Me.txtTotalGross.Text = String.Format("{0:f2}", drServiceBill.Item(16))
                Me.txtDiscount.Text = String.Format("{0:f2}", drServiceBill.Item(17))
                Me.txtTotalNet.Text = String.Format("{0:f2}", Val(Me.txtTotalGross.Text) - Val(Me.txtDiscount.Text))
                Me.txtTax.Text = String.Format("{0:f2}", drServiceBill.Item(18))
                Me.txtDiscountRemarks.Text = drServiceBill.Item(19)
                Me.cboStatus.SelectedValue = drServiceBill.Item(20)
                If UCase(cboStatus.SelectedValue) = "UA" Then
                    cboServiceType.Enabled = False
                    cboRO.Enabled = False
                    btnSearchCustomer.Enabled = False
                    Jcalendar2.Enabled = False
                    cboFreeService.Enabled = False
                    lnkSave.Enabled = False
                    cboStatus.Enabled = False
                    txtActionTaken.Enabled = False
                    txtRemarks.Enabled = False
                End If
                If UCase(Me.cboFreeService.SelectedValue) = "Y" And Trim(UCase(drServiceBill.Item(20))) <> "BL" Then
                    Me.cboFreeService.Enabled = False
                End If

                Me.txtAppointmentPrefix.Text = drServiceBill.Item(21)
                Me.txtAppointmentNo.Text = drServiceBill.Item(22)
                Me.txtCreatedBy.Text = drServiceBill.Item(23) & " - " & drServiceBill.Item(24)
                Me.txtCreateDate.Text = CDate(drServiceBill.Item(25)).ToString

                Me.cboFreeService.SelectedValue = Trim(drServiceBill.Item(33))
                Me.txtInstallDate.Text = Trim(drServiceBill.Item(29))
                Me.txtRemarks.Text = Trim(drServiceBill.Item(31))
                Me.txtContractID.Text = Trim(drServiceBill.Item(30))
                Me.txtActionTaken.Text = Trim(drServiceBill.Item(28))
                If drServiceBill.Item(34).ToString <> "" Then
                    Session("ContractType") = Trim(drServiceBill.Item(34))
                Else
                    Session("ContractType") = ""
                End If

                GetCustomerRO()
                cboRO.SelectedValue = drServiceBill.Item(10).ToString.Trim

            End If



                If Trim(txtContractID.Text) = "" Then
                    Me.lnkContract.Visible = True
                Else
                    Me.lnkContract.Visible = True
                End If

                Dim i As Integer = 0
                If dsServiceBill.Tables(5).Rows.Count > 0 Then
                    While i <= dsServiceBill.Tables(5).Rows.Count - 1
                        Dim lstrPaymentType As String = IIf(IsDBNull(dsServiceBill.Tables(5).Rows(i).Item(26)), "", dsServiceBill.Tables(5).Rows(i).Item(26))
                        Dim lstrPaymentAmount As String = IIf(IsDBNull(dsServiceBill.Tables(5).Rows(i).Item(27)), "0", dsServiceBill.Tables(5).Rows(i).Item(27))


                        Select Case UCase(lstrPaymentType)
                            Case "CS"
                                txtCash.Text = String.Format("{0:f2}", lstrPaymentAmount)
                            Case "CQ"
                                txtCheque.Text = String.Format("{0:f2}", lstrPaymentAmount)
                            Case "CR"
                                txtCredit.Text = String.Format("{0:f2}", lstrPaymentAmount)
                            Case "CC"
                                txtCreditCard.Text = String.Format("{0:f2}", lstrPaymentAmount)
                            Case "OP"
                                txtOther.Text = String.Format("{0:f2}", lstrPaymentAmount)
                        End Select
                        i = i + 1
                    End While
                End If

                ComputePaymentTotal()

                If Session("htSvcBill") Is Nothing Then
                    Exit Sub
                End If


                htSvcBill = Session("htSvcBill")

                If Trim(UCase(Me.txtCustomerID.Text)) <> Trim(UCase(htSvcBill.Item("CustomerNo"))) Then
                    Me.txtCustomerPrefix.Text = UCase(htSvcBill.Item("CustomerPrefix"))
                    Me.txtCustomerID.Text = htSvcBill.Item("CustomerNo")
                    Me.txtCustomer.Text = UCase(htSvcBill.Item("Customer"))
                    GetCustomerRO()
                    cboRO.SelectedValue = Trim(htSvcBill.Item("ROUID"))
                End If
                Me.cboServiceType.SelectedValue = Trim(htSvcBill.Item("ServiceType"))


                Me.txtActionTaken.Text = UCase(Trim(htSvcBill.Item("ActionTaken")))
                txtRemarks.Text = UCase(Trim(htSvcBill.Item("Remarks")))
                cboFreeService.SelectedValue = UCase(Trim(htSvcBill.Item("FreeService")))
                txtInstallDate.Text = UCase(Trim(htSvcBill.Item("InstallDate")))

            End If
            Session("htSvcBill") = Nothing
            '*******************************************************************
            ' END POPULATE VALUES FROM SEARCH
            '*******************************************************************

    End Sub


    Private Sub RetainValuesBeforeSearch()
        Try
            Dim htSvcBill As New Hashtable

            htSvcBill.Item("InvoicePf") = Me.txtInvoicePf.Text
            htSvcBill.Item("InvoiceNo") = Me.txtInvoiceNo.Text
            htSvcBill.Item("ServiceType") = Me.cboServiceType.SelectedValue
            htSvcBill.Item("CustomerPrefix") = Me.txtCustomerPrefix.Text
            htSvcBill.Item("CustomerNo") = Me.txtCustomerID.Text
            htSvcBill.Item("CustomerName") = Me.txtCustomer.Text

            htSvcBill.Item("ActionTaken") = Me.txtActionTaken.Text
            htSvcBill.Item("Remarks") = txtRemarks.Text
            htSvcBill.Item("FreeService") = cboFreeService.SelectedValue
            htSvcBill.Item("InstallDate") = txtInstallDate.Text
            htSvcBill.Item("ROUID") = cboRO.SelectedValue

            htSvcBill.Item("SBUNO") = 2
            htSvcBill.Item("Company") = Me.cboCompany.SelectedValue
            htSvcBill.Item("ServiceCenter") = Me.cboServiceCenter.SelectedValue

            Session("htSvcBill") = htSvcBill
        Catch

        End Try

    End Sub

    Private Sub SaveServiceBill2()
        Dim chkInsDate As Integer
        Dim clsSBU As New clsServiceBillUpdate
        clsSBU.ROInstallDate = txtInstallDate.Text
        clsSBU.ROSerialNo = cboRO.SelectedItem.Value

        chkInsDate = clsSBU.CheckROProductionDate()

        If chkInsDate >= 1 Then
            Dim objXm As New clsXml
            objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            lblError.Text = objXm.GetLabelName("StatusMessage", "BB-HintMsg-COMPAREPROINSDATE")
            lblError.Visible = True
            MessageBox1.Alert(lblError.Text)
            Exit Sub
        End If

        If validatefreeservice() = False Then Exit Sub

        'begin save

        Dim CREntity As New clsupdateservice2()

        Dim transType As String = ""
        Dim transNo As String = ""

        If Not Request.QueryString("type") Is Nothing Then
            transType = Request.QueryString("type").ToString()
            transNo = Request.QueryString("no").ToString()
        Else
            transType = Me.txtInvoicePf.Text
            transNo = Me.txtInvoiceNo.Text
        End If
        CREntity.ServiceType = cboServiceType.SelectedValue
        CREntity.ActionTaken = UCase(txtActionTaken.Text)
        CREntity.InstallDate = txtInstallDate.Text

        CREntity.ContractID = UCase(txtContractID.Text)
        CREntity.ROUnitID = cboRO.SelectedValue
        CREntity.FreeService = cboFreeService.Text
        CREntity.STATUS = cboStatus.SelectedValue
        CREntity.Remark = UCase(txtRemarks.Text)
        CREntity.IPAddress = Request.UserHostAddress
        CREntity.ModifyBy = UCase(Session("userID"))
        CREntity.CustomerID = Me.txtCustomerID.Text
        CREntity.CustomerPrefix = Me.txtCustomerPrefix.Text


        Dim updCtryCnt As Integer = CREntity.Update1(transType, transNo)
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        If updCtryCnt = 0 Then
            Dim objXm As New clsXml
            objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            lblError.Text = objXm.GetLabelName("StatusMessage", "BB-HintMsg-Suc")
            lblError.Visible = True
            MessageBox1.Alert(lblError.Text)

            If Me.cboStatus.SelectedValue = "UA" Then
                cboServiceType.Enabled = False
                cboRO.Enabled = False
                btnSearchCustomer.Enabled = False
                Jcalendar2.Enabled = False
                cboFreeService.Enabled = False
                lnkSave.Enabled = False
                cboStatus.Enabled = False
                txtActionTaken.Enabled = False
                txtRemarks.Enabled = False
            End If

            If UCase(Me.cboFreeService.SelectedValue) = "Y" Then
                Me.cboFreeService.Enabled = False
            End If

        Else
            Response.Redirect("~/PresentationLayer/Error.aspx")
        End If

        'disable controls

    End Sub

    Private Function validatefreeservice() As Boolean
        validatefreeservice = True

        If cboFreeService.Enabled = True Then
            If cboFreeService.SelectedValue = "Y" Then
                If Trim(cboRO.SelectedItem.Value) = "" Then
                    Dim objXm As New clsXml
                    objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    'Me.lblError.Text = objXm.GetLabelName("StatusMessage", "SBA-ERRRO")
                    'lblError.Visible = True
                    MessageBox1.Alert(objXm.GetLabelName("StatusMessage", "SBA-ERRRO"))
                    Return False
                Else
                    Dim clsInstallBase As New ClsInstalledBase
                    Dim dr As SqlDataReader
                    clsInstallBase.RoID = cboRO.SelectedItem.Value
                    dr = clsInstallBase.GetinstallbaseDetailsByRoID

                    If dr.Read() Then
                        If UCase(IIf(IsDBNull(dr.Item("MROU_FRSVE")), "", dr.Item("MROU_FRSVE"))) = "N" Then
                            Dim objXm As New clsXml
                            objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                            'lblError.Text = objXm.GetLabelName("StatusMessage", "SBU2-ERRFRSVENO") & " " & Me.cboRO.SelectedItem.Text
                            'lblError.Visible = True
                            MessageBox1.Alert(objXm.GetLabelName("StatusMessage", "SBU2-ERRFRSVENO") & " " & Me.cboRO.SelectedItem.Text)
                            Return False
                        End If
                    End If
                End If
            End If
        End If
    End Function


#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        AjaxPro.Utility.RegisterTypeForAjax(GetType(PresentationLayer_function_ServiceBillUpdate2))
        Page.MaintainScrollPositionOnPostBack = True

        Try
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try

        If Not IsPostBack Then
            InitializePage()
        End If
       



        '''access control'''''''''''''''''''''''''''''''''''''''''''''''
        Dim accessgroup As String = Session("accessgroup").ToString
        Dim purviewArray As Array = New Boolean() {False, False, False, False}
        purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "31")

        Me.lnkSave.Enabled = purviewArray(1)


        HypCal.NavigateUrl = "javascript:DoCal(document.form1.txtDate);"
        HypCal2.NavigateUrl = "javascript:DoCal(document.form1.txtInstallDate);"

    End Sub


    Sub FreeServiceChange()
        Dim valid As Boolean
        valid = validatefreeservice()
        'txtDate.Text = Request.Form("txtDate")
        'txtInstallDate.Text = Request.Form("txtInstallDate")
        ''Dim script As String = "self.location='#dscnt';"
        ''Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "dscnt", script, True)
    End Sub
    

    Protected Sub cboFreeService_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboFreeService.SelectedIndexChanged
        'FreeServiceChange()
    End Sub

    Protected Sub Jcalendar2_SelectedDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Jcalendar2.SelectedDateChanged
        lblError.Visible = False
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Dim installdate As String
        installdate = txtInstallDate.Text
        txtInstallDate.Text = Jcalendar2.SelectedDate
        Dim clsSBU As New clsServiceBillUpdate
        clsSBU.ROInstallDate = txtInstallDate.Text
        'clsSBU.ROSerialNo = rouid.Text
        clsSBU.ROSerialNo = Me.cboRO.SelectedItem.Value

        Dim chkInsDate As Integer = 0
        chkInsDate = clsSBU.CheckROProductionDate()

        If chkInsDate >= 1 Then
            Dim objXm As New clsXml
            objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            lblError.Text = objXm.GetLabelName("StatusMessage", "BB-HintMsg-COMPAREPROINSDATE")
            lblError.Visible = True
            MessageBox1.Alert(lblError.Text)
            txtInstallDate.Text = installdate
            Exit Sub
        End If
    End Sub

    Function RoChange(ByVal RoID As String, ByVal InvoiceDate As String, ByVal CustomerPrefix As String, ByVal CustomerID As String) As String
        'GET RO CONTRACT ID
        Dim clsro As New clsServiceBillUpdate
        Dim result As Integer
        Dim lstrConID As String

        lstrConID = ""
        clsro.CustomerPrefix = CustomerPrefix
        clsro.CustomerNo = CustomerID
        clsro.ROSerialNo = RoID
        clsro.InvoiceDate = InvoiceDate
        clsro.ContractID = ""

        result = clsro.GetContractID()
        If clsro.ContractID <> "" Then
            lstrConID = clsro.ContractID
            '  Session("ContractType") = clsro.ContractType
            ConType = clsro.ContractType
        End If

        If Trim(lstrConID) = "" Then
            'Me.lnkContract.Enabled = False
        Else
            'Me.lnkContract.Enabled = True
        End If

        'Me.txtContractID.Text = lstrConID
        'txtDate.Text = Request.Form("txtDate")
        'txtInstallDate.Text = Request.Form("txtInstallDate")

        RoChange = lstrConID
    End Function

    Protected Sub cboRO_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboRO.SelectedIndexChanged
        'Dim lstrConID As String
        'lstrConID = RoChange()

    End Sub

    Protected Sub btnSearchCustomer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearchCustomer.Click
        txtDate.Text = Request.Form("txtDate")
        txtInstallDate.Text = Request.Form("txtInstallDate")
        RetainValuesBeforeSearch()
        Response.Redirect("CustomerLookup.aspx")
    End Sub

    Protected Sub lnkContract_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkContract.Click
        'Dim cntno As String = Me.txtContractID.Text
        Dim cntno As String = Request.Form("txtContractID")
        Dim cntType As String
        Dim invty As String = Me.txtInvoicePf.Text
        Dim invno As String = Me.txtInvoiceNo.Text
        If Session("ContractType") <> "" Then
            cntType = Session("ContractType")
        Else
            Dim clsro As New clsServiceBillUpdate
            Dim result As Integer
            clsro.CustomerPrefix = txtCustomerPrefix.Text
            clsro.CustomerNo = txtCustomerID.Text
            clsro.ROSerialNo = cboRO.SelectedItem.Value
            clsro.InvoiceDate = txtDate.Text
            clsro.ContractID = ""
            result = clsro.GetContractID()
            If clsro.ContractID <> "" Then
                '  Session("ContractType") = clsro.ContractType
                cntType = clsro.ContractType
            Else
                cntType = "--"
            End If


            '  
        End If



        If cntno <> "" Then
            cntno = Server.UrlEncode(cntno)

            Dim strTempURL As String = "contractNo=" + cntno + "&retpage=ServiceBillUpdate2.aspx&type=" + invty + "&no=" + invno + "&contractType=" + cntType
            strTempURL = "~/PresentationLayer/function/modifycontract.aspx?" + strTempURL
            Response.Redirect(strTempURL)
        Else
            'Dim objXm As New clsXml
            'objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            'MessageBox1.Alert(objXm.GetLabelName("StatusMessage", "CONTRACT_BLANK"))
        End If
    End Sub

    Protected Sub txtInstallDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtInstallDate.TextChanged
        'CheckInstallDate()

        
    End Sub

    Function CheckInstallDate() As Boolean
        'ByVal InstallDate As String, ByVal txtDate As String
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        txtDate.Text = Request.Form("txtDate")
        txtInstallDate.Text = Request.Form("txtInstallDate")
        Dim clsSBU As New clsServiceBillUpdate
        Dim chkInsDate As Integer = 0

        clsSBU.ROInstallDate = txtInstallDate.Text
        clsSBU.ROSerialNo = Me.cboRO.SelectedItem.Value

        chkInsDate = clsSBU.CheckROProductionDate()

        If chkInsDate >= 1 Then
            Dim objXm As New clsXml
            objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            lblError.Text = objXm.GetLabelName("StatusMessage", "BB-HintMsg-COMPAREPROINSDATE")
            lblError.Visible = True
            MessageBox1.Alert(lblError.Text)
            txtInstallDate.Text = Request.Form("txtInstallDate")
            Exit Function
        End If
        CheckInstallDate = True
    End Function


    Protected Sub MessageBox1_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox1.GetMessageBoxResponse

        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
            'Dim x As String
            'x = Me.ctryStatDrop.SelectedItem.Text
            SaveServiceBill2()
        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
            Exit Sub
        End If
        txtDate.Text = Request.Form("txtDate")
        txtInstallDate.Text = Request.Form("txtInstallDate")

    End Sub

    Protected Sub lnkSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSave.Click
        Dim objXm As New clsXml
        Dim lstrError As String = ""

        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        txtDate.Text = Request.Form("txtDate")
        txtInstallDate.Text = Request.Form("txtInstallDate")
        txtContractID.Text = Request.Form("txtContractID")
        cboRO.SelectedValue = Request.Form("cboRO")

        If Not IsDate(txtInstallDate.Text) Then
            lstrError = objXm.GetLabelName("StatusMessage", "BB-CHECKINSTALLDATE")
            MessageBox1.Alert(lstrError)
            Exit Sub
        End If

        If Not CheckInstallDate() Then
            Exit Sub
        End If

        Dim msginfo As String = objXm.GetLabelName("StatusMessage", "SAVEORNOT")
        Dim msgtitle As String = objXm.GetLabelName("StatusMessage", "SAVETITLE")
        'MessageBox1.Confirm(msginfo)
        SaveServiceBill2()
    End Sub

    Protected Sub lnkCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCancel.Click
        'If lnkSave.Enabled = True Then
        '    Dim objXm As New clsXml
        '    objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        '    Dim msginfo As String = objXm.GetLabelName("StatusMessage", "SBA-CANCELMSG")
        '    Dim msgtitle As String = objXm.GetLabelName("StatusMessage", "SBA-CANCEL")
        '    MessageBox2.Confirm(msginfo)
        'Else
        Response.Redirect("UpdateServiceBillPart2.Aspx")
        'End If
    End Sub


#Region "Ajax"
    <AjaxPro.AjaxMethod()> _
    Public Function CheckFreeService(ByVal FreeServiceYesNo As String, ByVal RoID As String) As String
        Dim lstrErrorMessage As String
        lstrErrorMessage = ""
        If FreeServiceYesNo.Trim.ToUpper = "Y" Then
            'If Trim(cboRO.SelectedItem.Value) = "" Then
            If RoID.Trim = "" Then
                Dim objXm As New clsXml
                objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                'Me.lblError.Text = objXm.GetLabelName("StatusMessage", "SBA-ERRRO")
                'lblError.Visible = True
                lstrErrorMessage = objXm.GetLabelName("StatusMessage", "SBA-ERRRO")

            Else
                Dim clsInstallBase As New ClsInstalledBase
                Dim dr As SqlDataReader
                clsInstallBase.RoID = RoID.Trim
                dr = clsInstallBase.GetinstallbaseDetailsByRoID

                If dr.Read() Then
                    If UCase(IIf(IsDBNull(dr.Item("MROU_FRSVE")), "", dr.Item("MROU_FRSVE"))) = "N" Then
                        Dim objXm As New clsXml
                        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                        'lblError.Text = objXm.GetLabelName("StatusMessage", "SBU2-ERRFRSVENO") & " " & Me.cboRO.SelectedItem.Text
                        'lblError.Visible = True
                        lstrErrorMessage = objXm.GetLabelName("StatusMessage", "SBU2-ERRFRSVENO") & " " & dr.Item("MROU_MODID") & "-" & dr.Item("MROU_SERNO")

                    End If
                End If
            End If
        End If

        CheckFreeService = lstrErrorMessage
    End Function

    <AjaxPro.AjaxMethod()> _
       Public Function RoDropdownChange(ByVal RoID As String, ByVal InvoiceDate As String, ByVal CustomerPrefix As String, ByVal CustomerID As String) As ArrayList
        Dim lstrConID, lstrInstallDate As String

        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        lstrConID = RoChange(RoID, InvoiceDate, CustomerPrefix, CustomerID)

        Dim objInstallBased As New ClsInstalledBase
        lstrInstallDate = ""

        If RoID <> "" Then
            objInstallBased.RoID = RoID
            Dim edtReader As SqlDataReader = objInstallBased.GetinstallbaseDetailsByRoID()
            If edtReader.Read() Then
                lstrInstallDate = edtReader.GetValue(7).ToString().Substring(0, 10)
            End If
        End If
        Dim Listas As New ArrayList(2)

        Listas.Add(lstrConID)
        Listas.Add(lstrInstallDate)
        RoDropdownChange = Listas
    End Function

    <AjaxPro.AjaxMethod()> _
       Public Function InstallDateChange(ByVal RoID As String, ByVal InstallDate As String) As String
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")

        Dim clsSBU As New clsServiceBillUpdate
        Dim chkInsDate As Integer = 0
        Dim lstrOutput As String = ""

        If IsDate(InstallDate) Then
            clsSBU.ROInstallDate = InstallDate
            clsSBU.ROSerialNo = RoID

            chkInsDate = clsSBU.CheckROProductionDate()

            If chkInsDate >= 1 Then
                lstrOutput = objXm.GetLabelName("StatusMessage", "BB-HintMsg-COMPAREPROINSDATE")
            End If
        Else
            lstrOutput = objXm.GetLabelName("StatusMessage", "BB-CHECKINSTALLDATE")
        End If
        InstallDateChange = lstrOutput
    End Function

#End Region
End Class
