<%@ Page Language="VB" AutoEventWireup="false" EnableEventValidation ="false" CodeFile="ServiceBillModify.aspx.vb" Inherits="PresentationLayer_function_ServiceBillModify" %>

<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc1" %>

<%@ Register Assembly="JCalendar" Namespace="JCalendar" TagPrefix="cc2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" > 
<head runat="server">
    <title>Modify Service Bill</title>
        <meta http-equiv="Content-Type" content="text/html; charset=gb2312">
	<LINK href="../css/style.css" type="text/css" rel="stylesheet">
</head>
<body>

<SCRIPT language="JavaScript" type="text/javascript"> 

//function CalculatePayment()
//{
//         document.form1.item('txtTotal').value = parseFloat(document.form1.item('txtCash').value) + 
//                parseFloat(document.form1.item('txtCheque').value) +
//                parseFloat(document.form1.item('txtCredit').value) +
//                parseFloat(document.form1.item('txtCreditCard').value) +
//                parseFloat(document.form1.item('txtOther').value);
//                
//         document.form1.item('txtTotalNet').value = parseFloat(document.form1.item('txtTotalGross').value) - 
//            parseFloat(document.form1.item('txtDiscount').value)
//                
//            
//}       

//function validateFloat(obj, nLength, nPrecision)
//{
//  	var strVal = new String(obj.value);
//   	var nIndexOfDot = strVal.indexOf('.');
//   	var nValidLength = nIndexOfDot==-1?strVal.length:strVal.length+1
//   	if(nValidLength<strVal.length)
//   	{
//   		alert('Maximum length can be '+nLength);
//   		strVal = strVal.substring(0,nValidLength);
//   	}
//       	if(strVal.charAt(strVal.length-1)!='.' || nIndexOfDot!=(strVal.length-1))
//           	{
//           		if(isNaN(parseFloat(strVal)))
//           		{
//           			strVal='0';
//           		}
//           		obj.value=parseFloat(strVal);
//           	}
//       	if(-1!=nIndexOfDot && strVal.substring(nIndexOfDot+1).length>nPrecision)
//        {
//            	strVal=strVal.substring(0, strVal.length-1);
//                obj.value=strVal;
//        }
//        CalculatePayment()
//}

//function validateInt(obj, nLength, nPrecision)
//{
//  	var strVal = new String(obj.value);
//   	var nIndexOfDot = strVal.indexOf('.');
//   	var nValidLength = nIndexOfDot==-1?strVal.length:strVal.length+1
//   	nPrecision = 0 
//   	if(nValidLength<strVal.length)
//   	{
//   		alert('Maximum length can be '+nLength);
//   		strVal = strVal.substring(0,nValidLength);
//   	}
//       	if(strVal.charAt(strVal.length-1)!='.' || nIndexOfDot!=(strVal.length-1))
//           	{
//           		if(isNaN(parseFloat(strVal)))
//           		{
//           			strVal='0';
//           		}
//           		obj.value=parseFloat(strVal);
//           	}
//       	if(-1!=nIndexOfDot && strVal.substring(nIndexOfDot+1).length>nPrecision)
//        {
//            	strVal=strVal.substring(0, strVal.length-1);
//                obj.value=strVal;
//        }
//       document.form1.item('txtLineTotal').value = parseFloat(document.form1.item('txtQty').value) * parseFloat(document.form1.item('txtUnitPrice').value)
//}



function LoadDiscount_CallBack(response){
 
     //if the server-side code threw an exception
     if (response.error != null)
     {
      //we should probably do better than this
      alert(response.error); 
      
      return;
     }

     var ResponseValue = response.value;
     document.getElementById("<%=txtTotalNet.ClientID%>").value = ResponseValue;
     document.getElementById("<%=txtCash.ClientID%>").focus();
     
                
     
}
 
 function FormatDiscount_CallBack(response){
 
     //if the server-side code threw an exception
     if (response.error != null)
     {
      //we should probably do better than this
      alert(response.error); 
      
      return;
     }

     var ResponseValue = response.value;
     document.getElementById("<%=txtDiscount.ClientID%>").value = ResponseValue;
     
                
     
}

 function LoadDiscount(objectClient)
{    
    
    var GrossTotal = document.getElementById("<%=txtTotalGross.ClientID%>").value;
//    var Discount = document.getElementById("<%=txtDiscount.ClientID%>").value;
     var Discount =objectClient.value;
    PresentationLayer_function_ServiceBillModify.FormatNumber(Discount, FormatDiscount_CallBack);
    PresentationLayer_function_ServiceBillModify.DisplayNetAfterDiscount(Discount,GrossTotal, LoadDiscount_CallBack);
    
}

function LoadTotalPayment_CallBack(response){
 
     //if the server-side code threw an exception
     if (response.error != null)
     {
      //we should probably do better than this
      alert(response.error); 
      
      return;
     }

     var ResponseValue = response.value;
     document.getElementById("<%=txtTotal.ClientID%>").value = ResponseValue;

     
                
     
}

 function FormatCash_CallBack(response){
 
     //if the server-side code threw an exception
     if (response.error != null)
     {
      //we should probably do better than this
      alert(response.error); 
      
      return;
     }

     var ResponseValue = response.value;
     document.getElementById("<%=txtCash.ClientID%>").value = ResponseValue;

}     
  function FormatCheque_CallBack(response){
 
     //if the server-side code threw an exception
     if (response.error != null)
     {
      //we should probably do better than this
      alert(response.error); 
      
      return;
     }

     var ResponseValue = response.value;
     document.getElementById("<%=txtCheque.ClientID%>").value = ResponseValue;

}     
                   
 function FormatCredit_CallBack(response){
 
     //if the server-side code threw an exception
     if (response.error != null)
     {
      //we should probably do better than this
      alert(response.error); 
      
      return;
     }

     var ResponseValue = response.value;
     document.getElementById("<%=txtCredit.ClientID%>").value = ResponseValue;

}   

 function FormatCreditcard_CallBack(response){
 
     //if the server-side code threw an exception
     if (response.error != null)
     {
      //we should probably do better than this
      alert(response.error); 
      
      return;
     }

     var ResponseValue = response.value;
     document.getElementById("<%=txtCreditCard.ClientID%>").value = ResponseValue;

}   

 
 function FormatOther_CallBack(response){
 
     //if the server-side code threw an exception
     if (response.error != null)
     {
      //we should probably do better than this
      alert(response.error); 
      
      return;
     }

     var ResponseValue = response.value;
     document.getElementById("<%=txtOther.ClientID%>").value = ResponseValue;
}     


function LoadTotalPayment (){

var cash = document.getElementById("<%=txtCash.ClientID%>").value;
var cheque = document.getElementById("<%=txtCheque.ClientID%>").value;
var credit = document.getElementById("<%=txtCredit.ClientID%>").value;
var creditcard = document.getElementById("<%=txtCreditCard.ClientID%>").value;
var other = document.getElementById("<%=txtOther.ClientID%>").value;


PresentationLayer_function_ServiceBillModify.FormatNumber(cash, FormatCash_CallBack);
PresentationLayer_function_ServiceBillModify.FormatNumber(cheque, FormatCheque_CallBack);
PresentationLayer_function_ServiceBillModify.FormatNumber(credit, FormatCredit_CallBack);
PresentationLayer_function_ServiceBillModify.FormatNumber(creditcard, FormatCreditcard_CallBack);
PresentationLayer_function_ServiceBillModify.FormatNumber(other, FormatOther_CallBack);

PresentationLayer_function_ServiceBillModify.DisplayPayment(cash,cheque,credit,creditcard,other, LoadTotalPayment_CallBack);
    
}



</Script>

    <form id="form1" runat="server">
    <div>
      <table bgcolor="#b7e6e6" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
            <tr>
                <td background="../graph/title_bg.gif" width="1%">
                    <img height="24" src="../graph/title1.gif" width="5" /></td>
                <td background="../graph/title_bg.gif" class="style2" width="100%">
                    <asp:Label ID="titleLab" runat="server" Text="Modify Service Bill"></asp:Label></td>
            </tr>
        </table>
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td background="../graph/title_bg.gif" class="style2" style="height: 14px" width="100%">
                    <font color="red">
                        <asp:Label ID="lblError" runat="server" Visible="False"></asp:Label></font></td>
            </tr>
        </table>
        <table bgcolor="#b7e6e6" width = 100%> 
                                  <tr bgcolor="#ffffff">
<td colspan = 4>
                            <asp:Label ID="lblNoteUP" runat="server" ForeColor="Red" Text="Label"></asp:Label>
</td>
             </tr>
             <tr bgcolor="#ffffff">
<td colspan = 4>
                        	&nbsp;
                     	</td>
               </tr> 
            <tr bgcolor="#ffffff">    
                <td style="width: 15%;" height="1">
                    <asp:Label ID="lblServiceBillType" runat="server" Text="Service Bill Type" Width="104px"></asp:Label></td>
                <td style="width: 35%;" height="1">
                    <asp:DropDownList ID="cboServiceBillType" runat="server" Width="90%" Enabled="False">
                    </asp:DropDownList>
                    <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Size="Small" ForeColor="Red"
                        Text="*"></asp:Label>
                    <asp:RequiredFieldValidator ID="rfvServiceBillType" runat="server" ControlToValidate="cboServiceBillType"
                        ErrorMessage="*" ForeColor="White"></asp:RequiredFieldValidator>
                    </td>
                <td style="width: 15%;" height="1">
                    <asp:Label ID="lblInvoiceNo" runat="server" Text="Invoice #" Width="104px"></asp:Label></td>
                <td style="width: 35%;" height="1">
                    <asp:TextBox ID="txtInvoicePf" runat="server" ReadOnly="True" Width="32px"></asp:TextBox>&nbsp;
                    <asp:TextBox ID="txtInvoiceNo" runat="server" ReadOnly="True" Width="150px"></asp:TextBox></td>
            
            </tr>
                        <tr bgcolor="#ffffff">    
                <td style="width: 15%;" height="1">
                    <asp:Label ID="lblDate" runat="server" Text="Date" Width="104px"></asp:Label></td>
                <td style="width: 35%;" height="1">
                    <asp:TextBox ID="txtDate" runat="server" style="width: 135px;" ReadOnly="True"></asp:TextBox>
                    <cc2:jcalendar id="JCalendar1" runat="server" controltoassign="JDATEBox" imgurl="~/PresentationLayer/graph/calendar.gif" Enabled="False" Visible="False"></cc2:jcalendar>
                    <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Size="Small" ForeColor="Red"
                        Text="*"></asp:Label>
                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif" CausesValidation="False" Enabled="False" Visible="False" /><asp:RequiredFieldValidator ID="rfvDate" runat="server" ControlToValidate="txtDate"
                        ErrorMessage="*" ForeColor="White"></asp:RequiredFieldValidator><asp:Calendar ID="Calendar1" runat="server" BackColor="White" BorderColor="#3366CC"
                        BorderWidth="1px" CellPadding="1" DayNameFormat="Shortest" Font-Names="Verdana"
                        Font-Size="8pt" ForeColor="#003399" Height="200px" ShowGridLines="True" Visible="False"
                        Width="220px" Enabled="False">
                        <SelectedDayStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                        <TodayDayStyle BackColor="#99CCCC" ForeColor="White" />
                        <SelectorStyle BackColor="#99CCCC" ForeColor="#336666" />
                        <WeekendDayStyle BackColor="#CCCCFF" />
                        <OtherMonthDayStyle ForeColor="#999999" />
                        <NextPrevStyle Font-Size="8pt" ForeColor="#CCCCFF" />
                        <DayHeaderStyle BackColor="#99CCCC" ForeColor="#336666" Height="1px" />
                        <TitleStyle BackColor="#003399" BorderColor="#3366CC" BorderWidth="1px" Font-Bold="True"
                            Font-Size="10pt" ForeColor="#CCCCFF" Height="25px" />
                    </asp:Calendar>
                </td>
                <td style="width: 15%;" valign="top" height="1">
                    <asp:Label ID="lblServiceBillNo" runat="server" Text="Service Bill #" Width="104px"></asp:Label></td>
                <td style="width: 35%;" valign="top" height="1">
                    <asp:TextBox ID="txtServiceBillNo" runat="server" MaxLength="10"></asp:TextBox>
                    <asp:Label ID="Label4" runat="server" Font-Bold="True" Font-Size="Small" ForeColor="Red"
                        Text="*"></asp:Label>
                    <asp:RequiredFieldValidator ID="rfvServiceBill" runat="server" ControlToValidate="txtServiceBillNo" ForeColor="White">*</asp:RequiredFieldValidator>
                    </td>
            
            </tr>
            <tr bgcolor="#ffffff">    
                <td style="width: 15%; height: 19px;">
                    <asp:Label ID="Label7" runat="server" Text="PRF No" Width="104px"></asp:Label></td>
                <td style="width: 35%; height: 19px;">
                    <asp:TextBox ID="txtPRFNo" runat="server" ReadOnly="True" Width="200px"></asp:TextBox></td>
                <td style="width: 15%; height: 19px;">
                    </td>
                <td style="width: 35%; height: 19px;">
                    &nbsp;&nbsp;
                    </td>
            
            </tr>
                        <tr bgcolor="#ffffff">    
                <td style="width: 15%; height: 19px;">
                    <asp:Label ID="lblTechnician" runat="server" Text="Technician" Width="104px"></asp:Label></td>
                <td style="width: 35%; height: 19px;">
                    <asp:DropDownList ID="cboTechnician" runat="server" Width="90%" Enabled="False">
                    </asp:DropDownList>
                    <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Size="Small" ForeColor="Red"
                        Text="*"></asp:Label>
                    <asp:RequiredFieldValidator ID="rfvTechnician" runat="server" ControlToValidate="cboTechnician"
                        ErrorMessage="*" Display="None" ForeColor="White">*</asp:RequiredFieldValidator>
                    </td>
                <td style="width: 15%; height: 19px;">
                    <asp:Label ID="lblServiceType" runat="server" Text="Service Type" Width="104px"></asp:Label></td>
                <td style="width: 35%; height: 19px;"><asp:DropDownList ID="cboServiceType" runat="server" Width="90%">
                </asp:DropDownList>
                    <asp:Label ID="Label5" runat="server" Font-Bold="True" Font-Size="Small" ForeColor="Red"
                        Text="*"></asp:Label>
                    <asp:RequiredFieldValidator ID="rfvServiceType" runat="server" ControlToValidate="cboServiceType"
                        ErrorMessage="*" Display="Dynamic" ForeColor="White"></asp:RequiredFieldValidator>
                    </td>
            
            </tr>
            <tr bgcolor="#ffffff">
                <td colspan=4>&nbsp;
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td style="width: 15%;">
                    <asp:Label ID="lblAppoinmentNo" runat="server" Text="Appointment No" Width="124px"></asp:Label></td>
                <td style="width: 35%;">
                    <asp:TextBox ID="txtAppointmentPrefix" runat="server" Width="10%" ReadOnly="True"></asp:TextBox>&nbsp;
                    <asp:TextBox ID="txtAppointmentNo" runat="server" Width="70%" ReadOnly="True"></asp:TextBox>
                    <asp:Button ID="btnSearchAppNo" runat="server" Text="..." CausesValidation="False" Enabled="False" Visible="False" Width="1px" />
                    <asp:Label ID="Label6" runat="server" Font-Bold="True" Font-Size="Small" ForeColor="Red"
                        Text="*"></asp:Label>
                        <asp:RequiredFieldValidator ID="rfvAppointmentPrefix"  Enabled =False  runat="server" ControlToValidate="txtAppointmentPrefix"
                        ErrorMessage="*" ForeColor="White">*</asp:RequiredFieldValidator><asp:RequiredFieldValidator ID="rfvAppointmentNo" runat="server" ControlToValidate="txtAppointmentNo"  Enabled =False
                        ErrorMessage="*" ForeColor="White">*</asp:RequiredFieldValidator></td>
                <td style="width: 15%;">
                    <asp:Label ID="lblCustomerID" runat="server" Text="CustomerID" Width="124px"></asp:Label></td>
                <td style="width: 35%;">
                    <asp:TextBox ID="txtCustomerPrefix" runat="server" Width="26px" ReadOnly="True"></asp:TextBox>&nbsp;
                    <asp:TextBox ID="txtCustomerID" runat="server" Width="100px" ReadOnly="True"></asp:TextBox>&nbsp;</td>
            </tr>
            <tr bgcolor="#ffffff">
                <td style="height: 15%; width: 15%;">
                    <asp:Label ID="lblROSerialNo" runat="server" Text="RO Serial No" Width="124px"></asp:Label></td>
                <td style="width: 35%; height: 28px">
                    <asp:TextBox ID="txtROSerialNo" runat="server" ReadOnly="True" Visible="False"></asp:TextBox>
                    <asp:TextBox ID="txtROUID" runat="server" Enabled="False" Visible="False" ReadOnly="True"></asp:TextBox><br />
                    <asp:DropDownList ID="cboRO" runat="server" Width="90%">
                    </asp:DropDownList></td>
                <td style="height: 15%; width: 15%;">
                    <asp:Label ID="lblCustomer" runat="server" Text="Customer Name" Width="124px"></asp:Label></td>
                <td style="height: 35%; width: 35%;">
                    <asp:TextBox ID="txtCustomer" runat="server" ReadOnly="True" Width="200px"></asp:TextBox></td>
            </tr>
            <tr bgcolor="#ffffff">
                <td style="width: 15%">
                    <asp:Label ID="lblCountry" runat="server" Text="Country" Width="124px"></asp:Label></td>
                <td style="width: 35%">
                    <asp:DropDownList ID="cboCountry" runat="server" AutoPostBack="True" Width="90%" Enabled="False">
                    </asp:DropDownList>
                </td>
                <td style="width: 15%">
                    <asp:Label ID="lblCompany" runat="server" Text="Company" Width="124px"></asp:Label></td>
                <td style="width: 35%">
                    <asp:DropDownList ID="cboCompany" runat="server" AutoPostBack="True" Width="90%" Enabled="False">
                    </asp:DropDownList></td>
            </tr>
            <tr bgcolor="#ffffff">
                <td style="width: 15%; height: 31px;">
                    <asp:Label ID="lblServiceCenter" runat="server" Text="Service Center" Width="124px"></asp:Label></td>
                <td style="width: 35%; height: 31px;">
                    <asp:DropDownList ID="cboServiceCenter" runat="server" AutoPostBack="True" Width="90%" Enabled="False">
                    </asp:DropDownList></td>
                <td style="width: 15%; height: 31px;">
                    <asp:Label ID="lblStatus" runat="server" Text="Status" Width="124px"></asp:Label></td>
                <td style="width: 35%; height: 31px;">
                    <asp:DropDownList ID="cboStatus" runat="server" AutoPostBack="False" Width="90%">
                    </asp:DropDownList></td>
            </tr>
            <tr bgcolor="#ffffff">
                <td style="height: 28px; width: 15%;">
                    <asp:Label ID="lblCreatedBy" runat="server" Text="Created By" Width="124px"></asp:Label></td>
                <td style="width: 35%; height: 28px">
                    <asp:TextBox ID="txtCreatedBy" runat="server" ReadOnly="True"></asp:TextBox></td>
                <td style="height: 28px; width: 15%;">
                    <asp:Label ID="lblCreateDate" runat="server" Text="Create Date" Width="124px"></asp:Label></td>
                <td style="height: 28px; width: 35%;">
                    <asp:TextBox ID="txtCreateDate" runat="server" ReadOnly="True"></asp:TextBox></td>
            </tr>
            <tr bgcolor="#ffffff">
                <td style="width: 15%;">
                    <asp:Label ID="lblModifyBy" runat="server" Text="Modify By" Width="124px"></asp:Label></td>
                <td style="width: 35%;">
                    <asp:TextBox ID="txtModifyBy" runat="server" ReadOnly="True"></asp:TextBox></td>
                <td style="width: 15%;">
                    <asp:Label ID="lblModifyDate" runat="server" Text="Modfy Date" Width="124px"></asp:Label></td>
                <td style="width: 35%;">
                    <asp:TextBox ID="txtModifyDate" runat="server" ReadOnly="True"></asp:TextBox></td>
            </tr>
            <tr bgcolor="#ffffff">
                <td style="width: 15%;">
                    <asp:Label ID="Label8" runat="server" Text="PRF Create By" Width="124px"></asp:Label></td>
                <td style="width: 35%;">
                    <asp:TextBox ID="txtPRFCreateBy" runat="server" ReadOnly="True"></asp:TextBox></td>
                <td style="width: 15%;">
                    <asp:Label ID="Label9" runat="server" Text="PRF Create Date" Width="124px"></asp:Label></td>
                <td style="width: 35%;">
                    <asp:TextBox ID="txtPRFCreateDate" runat="server" ReadOnly="True"></asp:TextBox></td>
            </tr>
            <tr bgcolor="#ffffff">
                <td colspan=4>&nbsp;
                </td>
            </tr>
             <tr bgcolor="#ffffff">
                <td colspan=4 id="#partcode">
                    <asp:Label ID="lblUpdateStockList" runat="server" Font-Underline="True" Text="Update Stock List"
                        Width="272px"></asp:Label>
                    <asp:TextBox ID="txtTaxRate" runat="server" Enabled="False" Visible="False">.10</asp:TextBox>
                    <asp:TextBox ID="txtTaxID" runat="server" Enabled="False" Visible="False"></asp:TextBox></td>
            </tr>
              <tr bgcolor="#ffffff">
                <td colspan=4>
                        <table cellpadding=0 cellspacing=0 border=0 width = 100%>
                            <tr>   
                                <td style="width: 227px; height: 13px">
                                    <asp:Label ID="lblPartCode" runat="server" Text="Part Code" Width="124px"></asp:Label></td>
                                <td style="height: 13px">
                                    <asp:TextBox ID="txtPartCode" runat="server" AutoPostBack="True" Enabled="False"></asp:TextBox>
                                    &nbsp;&nbsp;
                                    <asp:TextBox ID="txtPartName" runat="server" ReadOnly="True" Width="209px"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td style="width: 227px; height: 15px">
                                    <asp:Label ID="lblPriceID" runat="server" Text="Price ID" Width="124px"></asp:Label></td>
                                <td style="height: 15px">
                                    <asp:TextBox ID="txtPriceID" runat="server" AutoPostBack="True" Enabled="False"></asp:TextBox>&nbsp;&nbsp;&nbsp;
                                    <asp:TextBox ID="txtUnitPrice" runat="server" ReadOnly="True" Width="209px"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td style="width: 227px; height: 16px">
                                    <asp:Label ID="lblQuantity" runat="server" Text="Quantity" Width="124px"></asp:Label></td>
                                <td style="height: 16px">
                                    <asp:TextBox ID="txtQty" runat="server" onkeyup="validateInt(this, 10, 0)" Enabled="False">0</asp:TextBox>
                                    &nbsp;
                                    &nbsp;&nbsp;
                                    <asp:LinkButton ID="lnkAddItem" runat="server" CausesValidation="False" Enabled="False" Visible="False">Add Item</asp:LinkButton></td>
                            </tr>
                            <tr>
                                <td style="width: 227px; height: 14px">
                                    <asp:Label ID="lblLineTotal" runat="server" Text="Line Total" Width="124px"></asp:Label></td>
                                <td style="height: 14px">
                                    <asp:TextBox ID="txtLineTotal" runat="server" ReadOnly="True" Enabled="False">0</asp:TextBox>
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td style="width: 227px; height: 14px">
                                    &nbsp;</td>
                                <td style="height: 14px">
                                </td>
                            </tr>
                                                   
                        </table>
                    <asp:GridView ID="grdServiceBill" runat="server" Width="100%">
                        <Columns>
                            <asp:CommandField CancelText="" EditText="" HeaderImageUrl="~/PresentationLayer/graph/edit.gif"
                                ShowDeleteButton="True" Visible="False" />
                        </Columns>
                    </asp:GridView>
                     
                </td>
            </tr>
            
                <tr bgcolor="#ffffff" valign = top >
                <td style="height: 65px; width: 15%;" id="#dscnt">
                    <asp:Label ID="lblRemarks" runat="server" Text="Discount Remarks" Width="124px"></asp:Label></td>
                <td style="height: 65px" colspan="2">
                    <asp:TextBox ID="txtRemarks" runat="server" Height="116px" TextMode="MultiLine" Width="90%" MaxLength="800"></asp:TextBox></td>
                <td style="height: 65px; width: 35%;" align=right >
                    <table width=100%>
                        <tr>
                            <td style="height: 27px" align = right>
                                <asp:Label ID="lblTotalGross" runat="server" Text="Total" Width="107px"></asp:Label></td>
                            <td style="height: 27px; width: 21px;" align = right>
                                <asp:TextBox ID="txtTotalGross" runat="server" MaxLength="6" ReadOnly="True" Width="106px">0</asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="height: 27px" align = right>
                                <asp:Label ID="lblDiscount" runat="server" Text="Discount" Width="107px"></asp:Label></td>
                            <td style="height: 27px; width: 21px;" align = right>
                                <asp:TextBox ID="txtDiscount" runat="server" MaxLength="6" Width="106px" AutoPostBack="false" onblur="LoadDiscount(this)">0</asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="height: 27px" align = right>
                                <asp:Label ID="lblTotalNet" runat="server" Text="Total" Width="107px"></asp:Label></td>
                            <td style="height: 27px; width: 21px;" align = right>
                                <asp:TextBox ID="txtTotalNet" runat="server" MaxLength="6" ReadOnly="True" Width="106px">0</asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="height: 27px" align = right>
                                <asp:Label ID="lblInvoiceTax" runat="server" Text="Invoice Tax" Width="107px"></asp:Label>
                                <asp:Label ID="lblInclusive" runat="server" Font-Underline="True" Text="(Inclusive)"
                                    Width="107px"></asp:Label></td>
                            <td style="height: 27px; width: 21px;" align = right>
                                <asp:TextBox ID="txtTax" runat="server" MaxLength="6" ReadOnly="True" Width="106px">0</asp:TextBox></td>
                        </tr>
                       
                    </table>
                    &nbsp;</td>
            </tr>
            <tr bgcolor="#ffffff" valign="top">
                <td style="height: 2px" colspan="2">
                    <asp:Label ID="lblPaymentDetails" runat="server" Text="Payment Details" Width="124px"></asp:Label>
                    </td>
                <td style="height: 2px; width: 15%;">
                </td>
                <td style="height: 2px; width: 35%;">
                </td>
            </tr>
            <tr bgcolor="#ffffff" valign="top">
                <td colspan="3" style="height: 65px" align=right>
                    <table width = 80%>
                          <tr align = right>
                            <td style="width: 37px; height: 16px" align = right>
                            </td>
                            <td style="width: 319px; height: 16px" align = right>
                                <asp:Label ID="lblCash" runat="server" Text="Cash" Width="124px"></asp:Label></td>
                            <td style="width: 2px; height: 16px" align = right>
                                <asp:TextBox ID="txtCash" runat="server" MaxLength="10" Width="122px" AutoPostBack="false" onblur="LoadTotalPayment()">0</asp:TextBox></td>
                          </tr>  
                          <tr align = right>
                            <td style="width: 37px" align = right>
                            </td>
                            <td style="width: 319px" align = right>
                                <asp:Label ID="lblCheque" runat="server" Text="Cheque" Width="124px"></asp:Label></td>
                            <td style="width: 2px" align = right>
                                <asp:TextBox ID="txtCheque" runat="server" MaxLength="10" Width="122px" AutoPostBack="false" onblur="LoadTotalPayment()">0</asp:TextBox></td>
                        </tr>
                        
                        <tr align = right>
                            <td style="width: 37px" align = right>
                            </td>
                            <td style="width: 319px" align = right>
                                <asp:Label ID="lblCredit" runat="server" Text="Credit" Width="124px"></asp:Label></td>
                            <td style="width: 2px" align = right>
                                <asp:TextBox ID="txtCredit" runat="server" MaxLength="10" Width="122px" AutoPostBack="false" onblur="LoadTotalPayment()">0</asp:TextBox></td>
                        </tr>
                        <tr align = right>
                            <td style="width: 37px" align = right>
                            </td>
                            <td style="width: 319px" align = right>
                                <asp:Label ID="lblCreditCard" runat="server" Text="CreditCard" Width="124px" ></asp:Label></td>
                            <td style="width: 2px" align = right>
                                <asp:TextBox ID="txtCreditCard" runat="server" MaxLength="10" Width="122px" AutoPostBack="false" onblur="LoadTotalPayment()">0</asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 37px" align = right>
                            </td>
                            <td style="width: 319px" align = right>
                                <asp:Label ID="lblOther" runat="server" Text="Other" Width="124px"></asp:Label></td>
                            <td style="width: 2px" align = right>
                                <asp:TextBox ID="txtOther" runat="server" MaxLength="10" Width="122px"  AutoPostBack="false" onblur="LoadTotalPayment()">0</asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 37px">
                            </td>
                            <td align="right" style="width: 319px">
                                <asp:Label ID="lblTotal" runat="server" Text="Total" Width="124px"></asp:Label></td>
                            <td align="right" style="width: 2px">
                                                              <asp:TextBox ID="txtTotal" runat="server" MaxLength="10" ReadOnly="True" Width="122px">0</asp:TextBox></td>
                                
                                
                        </tr>
                    </table>
                </td>
                <td style="height: 65px; width: 35%;" align=center valign=middle >
                    &nbsp;<asp:LinkButton ID="lnkSave" runat="server" Width="143px">Save</asp:LinkButton><br />
                    <br />
                    <asp:LinkButton ID="lnkGeneratePackingList" runat="server">Generate Packing List</asp:LinkButton><br />
                    <br />
                    <asp:LinkButton ID="lnkCancel" runat="server" CausesValidation="False" Width="143px">Cancel</asp:LinkButton><br />
                    <br />
                    <asp:LinkButton ID="lnkEdit" runat="server" CausesValidation="False" Width="143px" Enabled="False" Visible="False">Edit</asp:LinkButton><br />
                    <br />
                    <asp:LinkButton ID="lnkGenerateSBC" runat="server"  Width="143px" >Generate SBC</asp:LinkButton></td>
            </tr>
            <tr bgcolor="#ffffff" valign="top">
                <td align="right" colspan="3" style="height: 65px">
                    &nbsp;<asp:ValidationSummary ID="vsError" runat="server" ShowMessageBox="True" Visible="False" />
                <cc1:MessageBox ID="MessageBox1" runat="server" /><cc1:MessageBox ID="MessageBox2" runat="server" />
                </td>
                <td align="center" style="width: 35%; height: 65px" valign="middle">
                </td>
            </tr>
                     <tr bgcolor="#ffffff">
<td colspan = 4>
                        	&nbsp;
                     	</td>
               </tr> 
                               <tr bgcolor="#ffffff">
<td colspan = 4>
                            <asp:Label ID="lblNoteDown" runat="server" ForeColor="Red" Text="Label"></asp:Label>
</td>
             </tr>
    
          
          
        </table>
        
        
    </div>
    </form>
</body>
</html>


