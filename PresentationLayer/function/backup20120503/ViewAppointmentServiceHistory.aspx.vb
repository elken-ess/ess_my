Imports SQLDataAccess
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
Imports BusinessEntity
Imports CrystalDecisions.Web
Imports CrystalDecisions.CrystalReports.Engine

Partial Class PresentationLayer_function_ViewAppointmentServiceHistory_aspx
    Inherits System.Web.UI.Page
    Dim fstrMonth As String = ""
    Dim fstrDefaultStartDate As String = ""
    Dim fstrDefaultEndDate As String = ""
    Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
    Dim clsReport As New ClsCommonReport

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        fstrMonth = Month(System.DateTime.Today)
        fstrDefaultStartDate = "01/" & IIf(Len(fstrMonth) = 1, "0" & fstrMonth, fstrMonth) & "/" & Year(System.DateTime.Today)
        fstrDefaultEndDate = DateAdd(DateInterval.Month, 1, CDate(fstrDefaultStartDate))


        If Not Me.IsPostBack Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If

            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try

            PrintReport()
        End If
    End Sub


    Public Sub PrintReport()
        Dim rptcsvh As New clsRPTCSVH
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        rptcsvh.CustomerPrefix = Request.Params("CustomerPrefix")
        rptcsvh.CustomerID = Request.Params("CustomerID")
        rptcsvh.UserID = Session("userID").ToString()
        rptcsvh.RoID = Request.Params("RoID")

        Dim ctryIDPara As String = "%"
        Dim cmpIDPara As String = "%"
        Dim svcIDPara As String = "%"
        Dim login_rank As Integer = Session("login_rank")
        ctryIDPara = Session("login_ctryID").ToString.ToUpper


        If login_rank <> 0 Then
            Select Case (login_rank)
                Case 9
                    ctryIDPara = Session("login_ctryID").ToString.ToUpper
                    cmpIDPara = Session("login_cmpID").ToString.ToUpper
                    svcIDPara = Session("login_svcID").ToString.ToUpper
                Case 8
                    ctryIDPara = Session("login_ctryID").ToString.ToUpper
                    cmpIDPara = Session("login_cmpID").ToString.ToUpper

            End Select

        End If

        rptcsvh.Rank = login_rank.ToString
        rptcsvh.CountryID = ctryIDPara
        rptcsvh.CompID = cmpIDPara
        rptcsvh.SVCID = svcIDPara

        Dim rptds As DataSet = rptcsvh.GetAppointmentCustomerServiceHistory()

        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")

        If rptds Is Nothing Then
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            Dim lstrMessage = objXmlTr.GetLabelName("StatusMessage", "NoData")
            Dim script As String = "alert('" & lstrMessage & "');window.close()"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)

        ElseIf rptds.Tables(0).Rows.Count <= 0 Then

            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            Dim lstrMessage = objXmlTr.GetLabelName("StatusMessage", "NoData")
            Dim script As String = "alert('" & lstrMessage & "');window.close()"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
        Else

            Dim count As Integer
            Dim prdctclname As String
            Dim clscomm As New clsCommonClass()

            If Not rptds Is Nothing Then

                If rptds.Tables(0).Rows.Count > 0 Then
                    For count = 0 To rptds.Tables(0).Rows.Count - 1
                        Dim prdctclsid As String = rptds.Tables(0).Rows(count).Item(2).ToString 'item(8) is product class 
                        prdctclname = clscomm.passconverttel(prdctclsid)
                        If prdctclname <> "" Then
                            rptds.Tables(0).Rows(count).Item(2) = "HP--" & prdctclname
                        End If
                        'rptds.Tables(0).Rows(count).Item(2) = ""
                    Next
                End If
            End If

            Dim strHead As Array = Array.CreateInstance(GetType(String), 50)

            strHead(0) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCHL-0023")
            strHead(1) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCHL-0001")
            strHead(2) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCHL-0002")
            strHead(3) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCHL-0003")
            strHead(4) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCHL-0026")
            strHead(5) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCHL-0004")

            strHead(6) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCHL-0013")
            strHead(7) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCHL-0005")

            strHead(8) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCHL-0006")
            strHead(9) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCHL-0007")
            strHead(10) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCHL-0008")
            '
            strHead(11) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCHL-0009")
            strHead(12) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCHL-0010")

            strHead(13) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCHL-0011")
            strHead(14) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCHL-0012")
            strHead(15) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCHL-0024")
            strHead(16) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCHL-0022")

            strHead(17) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCHL-0014")
            If Session("userID") = "" Then
                strHead(18) = " "
            Else
                strHead(18) = "/" & Session("userID").ToString().ToUpper + "/" + Session("username").ToString.ToUpper
            End If
            strHead(19) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCHL-0027")
            strHead(20) = "/"


            strHead(21) = "" ' objXmlTr.GetLabelName("EngLabelMsg", "BB-TO")
            strHead(22) = "" 'objXmlTr.GetLabelName("EngLabelMsg", "BB-FROM")

            strHead(23) = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDSL-0010")
            strHead(24) = "" 'objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDSL-0011")
            strHead(25) = "" 'objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDSL-0012")

            strHead(26) = "" ' objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDSL-0013")
            strHead(27) = "" 'objXmlTr.GetLabelName("EngLabelMsg", "RPT_SERBITECH_INVDATE")

            strHead(28) = "" 'objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDSL-0015")
            strHead(29) = ""
            strHead(30) = ""

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            strHead(31) = ""
            strHead(32) = ""
            strHead(33) = ""
            strHead(34) = ""
            strHead(35) = ""
            strHead(36) = ""
            strHead(37) = ""
            strHead(38) = ""
            strHead(39) = ""
            strHead(40) = ""
            strHead(41) = ""
            strHead(42) = ""
            strHead(43) = ""
            strHead(44) = ""


            If Not rptds Is Nothing Then
                If (rptds.Tables(0).Rows.Count <> 0) Then
                    Dim strtotal As String = objXmlTr.GetLabelName("EngLabelMsg", "RPT_APT_RECORD")
                    strHead(45) = strtotal & ": " & rptds.Tables(0).Rows(0).Item("totalrecord")
                End If

                strHead(46) = ""
                strHead(47) = ""

                strHead(18) = Session("userID")

                ' Modified by Ryan Estandarte 21 Mar 2012
                'Dim lstrPdfFileName As String = "CustomerServiceHistory_" & Session("login_session") & ".pdf"
                Dim lstrPdfFileName As String = "CustomerServiceHistory_" & Session("login_session").ToString() & "_" & TimeOfDay.TimeOfDay.Hours & TimeOfDay.TimeOfDay.Minutes & TimeOfDay.TimeOfDay.Seconds & ".pdf"

                Try
                    With clsReport
                        .ReportFileName = "RPTCSVH.rpt"
                        .SetReportDocument(rptds, strHead)
                        .PdfFileName = lstrPdfFileName
                        .ExportPdf()
                    End With

                    Response.Redirect(clsReport.PdfUrl)

                Catch err As Exception
                    Response.Write("<BR>")
                    Response.Write(err.Message.ToString)

                End Try

            End If
        End If

    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        clsReport.UnloadReport()

    End Sub

End Class
