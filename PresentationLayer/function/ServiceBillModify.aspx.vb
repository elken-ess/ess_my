Imports BusinessEntity
Imports System.Configuration
Imports System.Xml
Imports SQLDataAccess
Imports System.Data.SqlClient
Imports System.Web
Imports System.Data
Imports System.Windows.Forms

Partial Class PresentationLayer_function_ServiceBillModify
    Inherits System.Web.UI.Page
    Dim clsReport As New ClsCommonReport
    Private dsItem As New DataSet
    Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.MaintainScrollPositionOnPostBack = True
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        AjaxPro.Utility.RegisterTypeForAjax(GetType(PresentationLayer_function_ServiceBillModify))

        Dim script As String = "top.location='../logon.aspx';"
        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
                'Response.Redirect("~/PresentationLayer/logon.aspx")
            End If
        Catch ex As Exception
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try
        If (Not Page.IsPostBack) Then
            Try
                InitializePage()
            Catch

            End Try
        End If

        Dim accessgroup As String = Session("accessgroup").ToString
        Dim purviewArray As Array = New Boolean() {False, False, False, False}
        purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "30")

        Me.lnkSave.Enabled = purviewArray(1)

    End Sub

    Protected Sub cboCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCountry.SelectedIndexChanged
        Try
            Dim company As New clsCommonClass
            Dim rank As String = Session("login_rank")
            If rank = 7 Then
                company.spctr = Me.cboCountry.SelectedValue().ToString().ToUpper()
            End If
            If rank = 8 Then
                company.spctr = Me.cboCountry.SelectedValue().ToString().ToUpper()
                company.spstat = Session("login_cmpID")
            End If
            If rank = 9 Then
                company.spctr = Me.cboCountry.SelectedValue().ToString().ToUpper()
                company.spstat = Session("login_cmpID")
                company.sparea = Session("login_svcID")
            End If
            If rank = 0 Then
                company.spctr = Me.cboCountry.SelectedValue().ToString().ToUpper()
            End If

            company.rank = rank
            Dim dscompany As New DataSet

            dscompany = company.Getcomidname("BB_MASCOMP_IDNAME")
            If dscompany.Tables.Count <> 0 Then
                databonds(dscompany, Me.cboCompany)
            End If
            Me.cboCompany.Items.Insert(0, "")
            Me.cboCompany.SelectedIndex = 0
            Me.cboCompany.SelectedValue = Session("login_cmpID")
            Me.cboCompany.Enabled = False
        Catch 
        End Try

    End Sub

    Protected Sub cboCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCompany.SelectedIndexChanged
        Try
            Dim ServiceCenter As New clsCommonClass
            Dim rank As String = Session("login_rank")
            If rank = 7 Then
                ServiceCenter.spctr = Me.cboCountry.SelectedValue().ToString().ToUpper()
                ServiceCenter.spstat = Me.cboCompany.SelectedValue()
            End If
            If rank = 8 Then
                ServiceCenter.spctr = Me.cboCountry.SelectedValue().ToString().ToUpper()
                ServiceCenter.spstat = Session("login_cmpID")
            End If
            If rank = 9 Then
                ServiceCenter.spctr = Me.cboCountry.SelectedValue().ToString().ToUpper()
                ServiceCenter.spstat = Session("login_cmpID")
                ServiceCenter.sparea = Session("login_svcID")
            End If
            If rank = 0 Then
                ServiceCenter.spctr = Me.cboCountry.SelectedValue().ToString().ToUpper()
            End If

            ServiceCenter.rank = rank

            Dim dsServiceCentere As New DataSet
            dsServiceCentere = ServiceCenter.Getcomidname("BB_MASSVRC_IDNAME")
            If dsServiceCentere.Tables.Count <> 0 Then
                databonds(dsServiceCentere, Me.cboServiceCenter)
            End If
            Me.cboServiceCenter.Items.Insert(0, "")
            Me.cboServiceCenter.SelectedIndex = 0
            Me.cboServiceCenter.SelectedValue = Session("login_svcID")
            Me.cboServiceCenter.Enabled = False
        Catch
        End Try
    End Sub

    Protected Sub cboServiceCenter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboServiceCenter.SelectedIndexChanged
        Dim Technician As New clsCommonClass
        Dim rank As String = Session("login_rank")
        If rank = 7 Then
            Technician.spctr = Me.cboCountry.SelectedValue().ToString().ToUpper()
            Technician.spstat = Me.cboCompany.SelectedValue()
        End If
        If rank = 8 Then
            Technician.spctr = Me.cboCountry.SelectedValue().ToString().ToUpper()
            Technician.spstat = Session("login_cmpID")
        End If
        If rank = 9 Then
            Technician.spctr = Me.cboCountry.SelectedValue().ToString().ToUpper()
            Technician.spstat = Session("login_cmpID")
            Technician.sparea = Session("login_svcID")
        End If
        If rank = 0 Then
            Technician.spctr = Me.cboCountry.SelectedValue().ToString().ToUpper()
        End If

        Technician.rank = rank

        Dim dsTechnician As New DataSet
        dsTechnician = Technician.Getcomidname("BB_MASTECH_NAMEID")
        If dsTechnician.Tables.Count <> 0 Then
            databondsForTechnician(dsTechnician, Me.cboTechnician)
        End If
        Me.cboTechnician.Items.Insert(0, "")
        Me.cboTechnician.SelectedIndex = 0

        Technician.sparea = Me.cboServiceCenter.SelectedValue

        Dim dsTax As New DataSet
        dsTax = Technician.Getcomidname("BB_FNCSALESTAX")
        If dsTax.Tables(0).Rows.Count <> 0 Then
            txtTaxRate.Text = dsTax.Tables(0).Rows(0).Item("NAME")
            txtTaxID.Text = dsTax.Tables(0).Rows(0).Item("ID")
        Else
            txtTaxRate.Text = 0
            txtTaxID.Text = ""
        End If

    End Sub

    Protected Sub grdServiceBill_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdServiceBill.RowDeleting
        RefreshDataSet()
        dsItem.Tables("StockList").Rows(e.RowIndex).Delete()
        Me.grdServiceBill.DataSource = dsItem
        Me.grdServiceBill.DataBind()
        InitializeGridColumns()
        RefreshDataSet()
    End Sub

    Protected Sub txtPartCode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPartCode.TextChanged
        Try
            Dim objXm As New clsXml
            objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            Dim clsPart As New clsServiceBillUpdate
            clsPart.Country = Session("login_ctryID")
            clsPart.PartID = txtPartCode.Text
            txtPartName.Text = clsPart.GetPartName
            If txtPartName.Text <> "" Then
                lblError.Text = ""
                lblError.Visible = False
                txtPriceID.Focus()
            Else
                lblError.Visible = True
                lblError.Text = objXm.GetLabelName("StatusMessage", "SBA-ERRPARCODE")
                txtPartCode.Focus()
                MessageBox1.Alert(lblError.Text)
            End If

            clsPart.Country = Session("login_ctryID")
            clsPart.PartID = txtPartCode.Text
            clsPart.PriceID = txtPriceID.Text
            txtUnitPrice.Text = clsPart.GetPriceID

            If Val(txtUnitPrice.Text) <> 0 Then
                Me.txtQty.Focus()
            Else
                txtPriceID.Focus()
            End If
        Catch
        End Try
        'Dim script As String = "self.location='#partcode';"
        'Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "partcode", script, True)
    End Sub

    Protected Sub txtPriceID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPriceID.TextChanged
        Try
            Dim objXm As New clsXml
            objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            Dim clsPrice As New clsServiceBillUpdate
            clsPrice.Country = Session("login_ctryID")
            clsPrice.PartID = txtPartCode.Text
            clsPrice.PriceID = txtPriceID.Text
            txtUnitPrice.Text = clsPrice.GetPriceID

            If Val(txtUnitPrice.Text) <> 0 Then
                lblError.Visible = False
                lblError.Text = ""
                Me.txtQty.Focus()
            Else
                lblError.Visible = True
                lblError.Text = objXm.GetLabelName("StatusMessage", "SBA-ERRPRICE")
                txtPriceID.Focus()
                MessageBox1.Alert(lblError.Text)
            End If
        Catch
        End Try
        'Dim script As String = "self.location='#partcode';"
        'Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "partcode", script, True)
    End Sub

#Region "Private Subs"
    Private Sub ADDITEMS()
        Try
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = System.Configuration.ConfigurationSettings.AppSettings("XmlFilePath")

            RefreshDataSet()

            Dim drItem As DataRow
            Dim RowNo As Integer = dsItem.Tables("StockList").Rows.Count
            Dim TaxAmt As Double
            txtLineTotal.Text = Val(Val(Me.txtQty.Text) * Val(Me.txtUnitPrice.Text))
            TaxAmt = CDbl(Val(txtTaxRate.Text) / 100.0) * txtLineTotal.Text
            drItem = dsItem.Tables("StockList").NewRow()

            drItem(0) = RowNo + 1
            drItem.Item(1) = UCase(Me.txtPartCode.Text)
            drItem.Item(2) = UCase(Trim(Me.txtPartName.Text))
            drItem.Item(3) = String.Format("{0:f0}", Val(Me.txtQty.Text))
            drItem.Item(4) = UCase(Me.txtPriceID.Text)
            drItem.Item(5) = String.Format("{0:f2}", Val(Me.txtUnitPrice.Text))
            drItem.Item(6) = String.Format("{0:f2}", Val(TaxAmt))
            drItem.Item(7) = String.Format("{0:f2}", Val(Me.txtQty.Text) * Val(Me.txtUnitPrice.Text))

            dsItem.Tables("StockList").Rows.Add(drItem)
            Me.grdServiceBill.DataSource = dsItem
            Me.grdServiceBill.DataBind()
            InitializeGridColumns()

            txtTotalGross.Text = String.Format("{0:f2}", Val(txtTotalGross.Text) + Val(Me.txtLineTotal.Text))
            txtTotalNet.Text = String.Format("{0:f2}", Val(txtTotalGross.Text) - Val(txtDiscount.Text))
            txtTax.Text = String.Format("{0:f2}", Val(txtTax.Text) + TaxAmt)

            txtPartCode.Text = ""
            'txtPriceID.Text = ""
            txtPartName.Text = ""
            txtUnitPrice.Text = ""
            txtQty.Text = "0"
            txtLineTotal.Text = "0"
        Catch

        End Try

    End Sub

    Private Sub databondsForTechnician(ByVal ds As DataSet, ByVal dropdown As DropDownList, Optional ByVal X As Boolean = True)
        dropdown.Items.Clear()
        Dim row As DataRow

        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            If X = True Then
                NewItem.Text = row("name") & "-" & Trim(row("id"))
            Else
                NewItem.Text = Trim(row("name"))
            End If
            NewItem.Value = Trim(row("id"))
            dropdown.Items.Add(NewItem)
        Next

    End Sub

    Private Sub databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList, Optional ByVal X As Boolean = True)
        dropdown.Items.Clear()
        Dim row As DataRow

        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            If X = True Then
                NewItem.Text = Trim(row("id") & "-" & row("name"))
            Else
                NewItem.Text = Trim(row("name"))
            End If
            NewItem.Value = Trim(row("id"))
            dropdown.Items.Add(NewItem)
        Next

    End Sub

    Private Sub GetAppointmentdetails()
        Try
            Dim clsAppointment As New clsServiceBillUpdate
            clsAppointment.Country = Me.cboCountry.SelectedValue
            clsAppointment.AppointmentNo = txtAppointmentNo.Text
            clsAppointment.AppointmentPrefix = txtAppointmentPrefix.Text

            Dim dstappointment As New DataSet
            dstappointment = clsAppointment.GetApptDetails

            If dstappointment.Tables(0).Rows.Count <> 0 Then
                If dstappointment.Tables(0).Rows(0)(9) <> "BL" Then
                    txtCustomerPrefix.Text = dstappointment.Tables(0).Rows(0).Item("CUSPF").ToString()
                    txtCustomerID.Text = dstappointment.Tables(0).Rows(0).Item("CUSID").ToString()
                    txtCustomer.Text = dstappointment.Tables(0).Rows(0).Item("CUSNM").ToString()
                    txtROSerialNo.Text = Trim(dstappointment.Tables(0).Rows(0).Item("MODID").ToString()) & "-" & _
                                           Trim(dstappointment.Tables(0).Rows(0).Item("SERNO").ToString())
                    txtROUID.Text = Trim(dstappointment.Tables(0).Rows(0).Item("ROUID").ToString())
                Else
                    txtCustomerPrefix.Text = String.Empty
                    txtCustomerID.Text = String.Empty
                    txtCustomer.Text = String.Empty
                    txtROSerialNo.Text = String.Empty
                    txtROUID.Text = String.Empty
                End If
            Else
                txtCustomerPrefix.Text = String.Empty
                txtCustomerID.Text = String.Empty
                txtCustomer.Text = String.Empty
                txtROSerialNo.Text = String.Empty
                txtROUID.Text = String.Empty
            End If
        Catch
        End Try
    End Sub

    Private Sub InitializeGridColumns()
        Try
            'get column labels
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            Me.grdServiceBill.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-NO")
            Me.grdServiceBill.HeaderRow.Cells(1).Font.Size = 8

            Me.grdServiceBill.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-PARTCODE")
            Me.grdServiceBill.HeaderRow.Cells(2).Font.Size = 8

            Me.grdServiceBill.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-PARTDESC")
            Me.grdServiceBill.HeaderRow.Cells(3).Font.Size = 8

            Me.grdServiceBill.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-QUANTITY")
            Me.grdServiceBill.HeaderRow.Cells(4).Font.Size = 8

            Me.grdServiceBill.HeaderRow.Cells(6).Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-UNITPRICE")
            Me.grdServiceBill.HeaderRow.Cells(6).Font.Size = 8

            Me.grdServiceBill.HeaderRow.Cells(7).Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-TAX")
            Me.grdServiceBill.HeaderRow.Cells(7).Font.Size = 8

            Me.grdServiceBill.HeaderRow.Cells(8).Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-LINETOTAL")
            Me.grdServiceBill.HeaderRow.Cells(8).Font.Size = 8

            Me.grdServiceBill.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-PRICEID")
            Me.grdServiceBill.HeaderRow.Cells(5).Font.Size = 8
        Catch
        End Try
    End Sub

    Private Sub InitializePage()

        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")

        '*******************************************************************
        ' BEGIN INITIALIZE LABELS AND VALIDATORS
        '*******************************************************************
        '************
        ' LABELS    
        '************
        Me.lblAppoinmentNo.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-APPOINMENTNO")
        Me.lblCash.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-CASH")
        Me.lblCheque.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-CHEQUE")
        Me.lblCompany.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-COMPANY")
        Me.lblCountry.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-COUNTRY")
        Me.lblCreateDate.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-CREATEDATE")
        Me.lblCreatedBy.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-CREATEBY")
        Me.lblCredit.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-CREDIT")
        Me.lblCreditCard.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-CREDITCARD")
        Me.lblCustomer.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-CUSTOMER")
        Me.lblCustomerID.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-CUSTOMERID")
        Me.lblDate.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-DATE")
        Me.lblDiscount.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-DISCOUNT")
        Me.lblInclusive.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-INCLUSIVE")
        Me.lblInvoiceTax.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-INVOICETAX")
        Me.lblInvoiceNo.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-INVOICENO")
        Me.lblLineTotal.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-LINETOTAL")
        Me.lblModifyBy.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-MODIFYBY")
        Me.lblModifyDate.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-MODIFYDATE")
        Me.lblOther.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-OTHER")
        Me.lblPartCode.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-PARTCODE")
        Me.lblPaymentDetails.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-PAYMENTDETAILS")
        Me.lblPriceID.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-PRICEID")
        Me.lblQuantity.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-QUANTITY")
        Me.lblRemarks.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-REMARKS")
        Me.lblROSerialNo.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-ROSERIALNO")
        Me.lblServiceBillNo.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-SERVICEBILLNO")
        Me.lblServiceBillType.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-SERVICEBILLTYPE")
        Me.lblServiceCenter.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-SERVICECENTER")
        Me.lblServiceType.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-SERVICETYPE")
        Me.lblStatus.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-STATUS")
        Me.lblTechnician.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-TECHNICIAN")
        Me.lblTotal.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-TOTAL")
        Me.lblTotalGross.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-TOTAL")
        Me.lblTotalNet.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-TOTAL")
        Me.lblUpdateStockList.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-UPDATESTOCKLIST")

        Me.lnkAddItem.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-ADDITEM")
        Me.lnkGeneratePackingList.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-GENPACKLIST")
        Me.lnkSave.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-SAVE")
        Me.lnkCancel.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-BACK")
        lblNoteUP.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
        lblNoteDown.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
        titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-update1")
        '************
        ' VALIDATORS   
        '************

        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Me.rfvDate.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRDATE")
        'Me.rfvServiceBill.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRSERVICEBILL")
        Me.rfvServiceBillType.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRSERVICEBILLTYPE")
        Me.rfvServiceType.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRSERVICETYPE")
        Me.rfvTechnician.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRTECHNICIAN")
        Me.rfvAppointmentNo.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRAPPOINTMENTNO")
        Me.rfvAppointmentPrefix.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRAPPOINTMENTPF")

        '*******************************************************************
        ' END INITIALIZE LABELS AND VALIDATORS
        '*******************************************************************
        Dim CommonClass As New clsCommonClass()
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        Dim userIDNamestr As String = Session("userID").ToString().ToUpper + "-" + Session("username").ToString.ToUpper
        Dim rank As String = Session("login_rank")
        CommonClass.spstat = Session("login_cmpID")
        CommonClass.sparea = Session("login_svcID")

        If rank <> 0 Then
            CommonClass.spctr = Session("login_ctryID").ToString().ToUpper
        End If
        CommonClass.rank = rank
        CommonClass.spstat = "ACTIVE"

        objXmlTr.XmlFile = System.Configuration.ConfigurationSettings.AppSettings("XmlFilePath")

        Dim Util As New clsUtil()
        Dim statParam As ArrayList = New ArrayList

        statParam = Util.searchconfirminf("SERBILLTY")
        Dim count As Integer
        Dim statid As String
        Dim statnm As String
        For count = 0 To statParam.Count - 1
            statid = statParam.Item(count)
            statnm = statParam.Item(count + 1)
            count = count + 1
            Me.cboServiceBillType.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
        Next
        'Me.cboServiceBillType.Items.Insert(0, "")

        statParam = Util.searchconfirminf("SerStat")
        For count = 0 To statParam.Count - 1
            statid = statParam.Item(count)
            statnm = statParam.Item(count + 1)
            count = count + 1
            If UCase(statid) = "BL" Or UCase(statid) = "UA" Or UCase(statid) = "UM" Then
                cboStatus.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
            End If
        Next
        cboStatus.Items(0).Selected = True

        Dim dsCountry As New DataSet
        Me.cboCountry.DataTextField = "name"
        Me.cboCountry.DataValueField = "id"
        dsCountry = CommonClass.Getcomidname("BB_MASCTRY_IDNAME")
        If dsCountry.Tables.Count <> 0 Then
            databonds(dsCountry, Me.cboCountry)
        End If
        Me.cboCountry.Items.Insert(0, "")
        Me.cboCountry.SelectedIndex = 0
        Me.cboCountry.SelectedValue = Session("login_ctryID").ToString().ToUpper
        Me.cboCountry.Enabled = False

        Dim dsSerType As New DataSet
        dsSerType = CommonClass.Getcomidname("BB_MASSRCT_IDNAMEALL")
        If dsSerType.Tables.Count <> 0 Then
            databonds(dsSerType, Me.cboServiceType)
        End If
        cboServiceType.Items.Insert(0, "")

        Dim dscompany As New DataSet
        CommonClass.spstat = Session("login_cmpID")

        dscompany = CommonClass.Getcomidname("BB_MASCOMP_IDNAME")
        If dscompany.Tables.Count <> 0 Then
            databonds(dscompany, Me.cboCompany)
        End If
        Me.cboCompany.Items.Insert(0, "")
        Me.cboCompany.SelectedIndex = 0
        Me.cboCompany.SelectedValue = Session("login_cmpID")
        Me.cboCompany.Enabled = False

        Dim dsServiceCentere As New DataSet
        CommonClass.spstat = Session("login_cmpID")

        dsServiceCentere = CommonClass.Getcomidname("BB_MASSVRC_IDNAME")

        If dsServiceCentere.Tables.Count <> 0 Then
            databonds(dsServiceCentere, Me.cboServiceCenter)
        End If
        Me.cboServiceCenter.Items.Insert(0, "")
        Me.cboServiceCenter.SelectedIndex = 0
        Me.cboServiceCenter.SelectedValue = Session("login_svcID")
        Me.cboServiceCenter.Enabled = False

        Dim dsTechnician As New DataSet
        dsTechnician = CommonClass.Getcomidname("BB_MASTECH_NAMEID")
        If dsTechnician.Tables.Count <> 0 Then
            databondsForTechnician(dsTechnician, Me.cboTechnician)
        End If
        Me.cboTechnician.Items.Insert(0, "")
        Me.cboTechnician.SelectedIndex = 0

        txtTaxRate.Text = 0
        Dim dsTax As New DataSet
        dsTax = CommonClass.Getcomidname("BB_FNCSALESTAX")
        If dsTax.Tables.Count <> 0 Then
            If dsTax.Tables(0).Rows.Count <> 0 Then
                txtTaxRate.Text = dsTax.Tables(0).Rows(0).Item("NAME")
                txtTaxID.Text = dsTax.Tables(0).Rows(0).Item("ID")
            End If
        End If

        txtCreatedBy.Text = userIDNamestr
        txtModifyBy.Text = userIDNamestr
        txtCreateDate.Text = Now().ToString
        txtModifyDate.Text = Now().ToString

        '*******************************************************************
        ' BEGIN POPULATE VALUES FROM SEARCH
        '*******************************************************************
        Dim clsServiceBill As New clsServiceBillUpdate
        Dim dsServiceBill As New DataSet
        Dim drServiceBill As DataRow

        txtInvoicePf.Text = Request.QueryString("type")
        txtInvoiceNo.Text = Request.QueryString("no")

        clsServiceBill.TransactionPrefix = txtInvoicePf.Text
        clsServiceBill.TransactionNo = txtInvoiceNo.Text

        dsServiceBill = clsServiceBill.GetServiceBill
        If dsServiceBill.Tables.Count <> 0 Then
            grdServiceBill.DataSource = dsServiceBill
            grdServiceBill.DataBind()
            RefreshDataSet()
            InitializeGridColumns()

            drServiceBill = dsServiceBill.Tables(1).Rows(0)

            'FIV1_TRNTY, FIV1_TRNNO, FIV1_SVCID, FIV1_COMID, 
            'FIV1_CTRID, FIV1_SVBIL, FIV1_SVBTY, FIV1_INVDT, 
            'FIV1_TCHID, FIV1_SVTID, FIV1_ROUID, MROU_MODID,
            'MROU_SERNO, FIV1_CUSPF, FIV1_CUSID, MCUS_ENAME,
            'FIV1_TOTAM, FIV1_DISAM, FIV1_TAXAM, FIV1_DSREM,  
            'FIV1_STAT,  FIV1_REFTY, FIV1_REFNO, FIV1_CUSER,
            'MUSR_ENAME, FIV1_CREDT, FIV2_TAXID, FIV2_TPERC

            CommonClass.spstat = drServiceBill.Item(3)
            CommonClass.sparea = drServiceBill.Item(2)
            CommonClass.spctr = drServiceBill.Item(4)

            dsServiceCentere = CommonClass.Getcomidname("BB_MASSVRC_IDNAME")
            If dsServiceCentere.Tables.Count <> 0 Then
                databonds(dsServiceCentere, Me.cboServiceCenter)
            End If
            Me.cboServiceCenter.Items.Insert(0, "")
            Me.cboServiceCenter.SelectedIndex = 0
            Me.cboServiceCenter.SelectedValue = drServiceBill.Item(2)
            Me.cboServiceCenter.Enabled = False

            dsTechnician = CommonClass.Getcomidname("BB_MASTECH_NAMEID")
            If dsTechnician.Tables.Count <> 0 Then
                databondsForTechnician(dsTechnician, Me.cboTechnician)
            End If
            Me.cboTechnician.Items.Insert(0, "")
            Me.cboTechnician.SelectedIndex = 0

            Me.cboServiceCenter.SelectedValue = drServiceBill.Item(2)
            Me.cboCompany.SelectedValue = drServiceBill.Item(3)
            Me.cboCountry.SelectedValue = drServiceBill.Item(4)
            Me.txtServiceBillNo.Text = drServiceBill.Item(5)
            Me.cboServiceBillType.SelectedValue = drServiceBill.Item(6)
            Me.txtDate.Text = Format(drServiceBill.Item(7), "dd/MM/yyyy")
            Me.cboTechnician.SelectedValue = drServiceBill.Item(8)
            Me.cboServiceType.SelectedValue = drServiceBill.Item(9)
            Me.txtROUID.Text = drServiceBill.Item(10)
            Me.txtROSerialNo.Text = drServiceBill.Item(11) & " - " & drServiceBill.Item(12)
            Me.txtCustomerPrefix.Text = drServiceBill.Item(13)
            Me.txtCustomerID.Text = drServiceBill.Item(14)
            Me.txtCustomer.Text = drServiceBill.Item(15)
            Me.txtTotalGross.Text = String.Format("{0:f2}", drServiceBill.Item(16))
            Me.txtDiscount.Text = String.Format("{0:f2}", drServiceBill.Item(17))
            Me.txtTotalNet.Text = String.Format("{0:f2}", Val(Me.txtTotalGross.Text) - Val(Me.txtDiscount.Text))
            Me.txtTax.Text = String.Format("{0:f2}", drServiceBill.Item(18))
            Me.txtRemarks.Text = drServiceBill.Item(19)
            Me.cboStatus.SelectedValue = drServiceBill.Item(20)
            If UCase(cboStatus.SelectedValue) = "UA" Then
                lnkSave.Enabled = False
                cboStatus.Enabled = False
            End If
            Me.txtAppointmentPrefix.Text = drServiceBill.Item(21)
            Me.txtAppointmentNo.Text = drServiceBill.Item(22)
            Me.txtCreatedBy.Text = drServiceBill.Item(23) & "-" & drServiceBill.Item(24)
            Me.txtCreateDate.Text = CDate(drServiceBill.Item(25)).ToString

            Session("currStatus") = drServiceBill.Item(20).ToString

            Dim Rcreatby As String = drServiceBill.Item("fiv1_luser")
            Dim Rcreat As New clsCommonClass
            Dim Rcreatds As New DataSet()

            Rcreat.userid() = Rcreatby

            Rcreatds = Rcreat.Getuseridname("BB_MASUSER_SelByIDName")

            Me.txtModifyBy.Text = Rcreatds.Tables(0).Rows(0).Item(0)

            Me.txtModifyDate.Text = CDate(drServiceBill.Item("fiv1_lstdt")).ToString

            Me.txtTaxID.Text = drServiceBill.Item(26)
            Me.txtTaxRate.Text = drServiceBill.Item(27)

            Me.txtPRFNo.Text = IIf(IsDBNull(drServiceBill.Item(30)), "", drServiceBill.Item(30))

            Dim PRFcreatby As String = IIf(IsDBNull(drServiceBill.Item(31)), "", drServiceBill.Item(31))
            Dim PRFcreat As New clsCommonClass
            Dim PRFcreatds As New DataSet()

            PRFcreat.userid() = PRFcreatby
            PRFcreatds = PRFcreat.Getuseridname("BB_MASUSER_SelByIDName")
            Me.txtPRFCreateBy.Text = PRFcreatds.Tables(0).Rows(0).Item(0)

            Me.txtPRFCreateDate.Text = IIf(IsDBNull(drServiceBill.Item(32)), "", drServiceBill.Item(32))

            GetCustomerRO()
            cboRO.SelectedValue = drServiceBill.Item(10)

            Dim i As Integer = 0
            While i <= dsServiceBill.Tables(2).Rows.Count - 1
                Select Case UCase(dsServiceBill.Tables(2).Rows(i).Item(2))
                    Case "CS"
                        txtCash.Text = String.Format("{0:f2}", dsServiceBill.Tables(2).Rows(i).Item(3))
                    Case "CQ"
                        txtCheque.Text = String.Format("{0:f2}", dsServiceBill.Tables(2).Rows(i).Item(3))
                    Case "CR"
                        txtCredit.Text = String.Format("{0:f2}", dsServiceBill.Tables(2).Rows(i).Item(3))
                    Case "CC"
                        txtCreditCard.Text = String.Format("{0:f2}", dsServiceBill.Tables(2).Rows(i).Item(3))
                    Case "OP"
                        txtOther.Text = String.Format("{0:f2}", dsServiceBill.Tables(2).Rows(i).Item(3))
                End Select
                i = i + 1
            End While

            cboTechnician.Enabled = False

            ComputePaymentTotal()

        End If
        '*******************************************************************
        ' END POPULATE VALUES FROM SEARCH
        '*******************************************************************
    End Sub

    Private Sub RefreshDataSet()
        dsItem.Clear()

        If dsItem.Tables.Count = 0 Then
            dsItem.Tables.Add("StockList")
            dsItem.Tables("StockList").Columns.Add("ItemNo")
            dsItem.Tables("StockList").Columns.Add("Part Code")
            dsItem.Tables("StockList").Columns.Add("Part Description")
            dsItem.Tables("StockList").Columns.Add("Quantity", GetType(Integer))
            dsItem.Tables("StockList").Columns.Add("PriceID")
            dsItem.Tables("StockList").Columns.Add("UnitPrice", GetType(Double))
            dsItem.Tables("StockList").Columns.Add("Tax", GetType(Double))
            dsItem.Tables("StockList").Columns.Add("Amount", GetType(Double))

        End If

        Dim intGrdRow As Integer = 0
        Dim drItem As DataRow
        Dim dblGrossTotal As Double = 0
        Dim dblTax As Double = 0

        While intGrdRow <= grdServiceBill.Rows.Count - 1
            drItem = dsItem.Tables("StockList").NewRow()
            drItem.Item(0) = intGrdRow + 1
            drItem.Item(1) = Me.grdServiceBill.Rows(intGrdRow).Cells(2).Text
            drItem.Item(2) = Me.grdServiceBill.Rows(intGrdRow).Cells(3).Text
            drItem.Item(3) = Val(Me.grdServiceBill.Rows(intGrdRow).Cells(4).Text)
            drItem.Item(4) = Me.grdServiceBill.Rows(intGrdRow).Cells(5).Text
            drItem.Item(5) = String.Format("{0:f2}", Val(Me.grdServiceBill.Rows(intGrdRow).Cells(6).Text))
            drItem.Item(6) = String.Format("{0:f2}", Val(Me.grdServiceBill.Rows(intGrdRow).Cells(7).Text))
            drItem.Item(7) = String.Format("{0:f2}", Val(Me.grdServiceBill.Rows(intGrdRow).Cells(8).Text))

            dsItem.Tables("StockList").Rows.Add(drItem)

            dblGrossTotal = dblGrossTotal + Val(Me.grdServiceBill.Rows(intGrdRow).Cells(8).Text)
            dblTax = dblTax + Val(Me.grdServiceBill.Rows(intGrdRow).Cells(7).Text)
            intGrdRow = intGrdRow + 1
        End While

        txtTotalGross.Text = String.Format("{0:f2}", dblGrossTotal)
        txtTotalNet.Text = String.Format("{0:f2}", dblGrossTotal - Val(txtDiscount.Text))
        txtTax.Text = String.Format("{0:f2}", dblTax)

    End Sub

    Private Sub RetainValuesBeforeSearch()
        Dim htSvcBill As New Hashtable
        htSvcBill.Item("ServiceBillType") = Me.cboServiceBillType.SelectedValue
        htSvcBill.Item("SeviceBillType") = Me.cboServiceType.SelectedValue
        htSvcBill.Item("Technician") = Me.cboTechnician.SelectedValue
        htSvcBill.Item("AppointmentNo") = Me.txtAppointmentNo.Text
        htSvcBill.Item("AppointmentPrefix") = Me.txtAppointmentPrefix.Text
        htSvcBill.Item("Cash") = Me.txtCash.Text
        htSvcBill.Item("Cheque") = Me.txtCheque.Text
        htSvcBill.Item("Credit") = Me.txtCredit.Text
        htSvcBill.Item("CreditCard") = Me.txtCreditCard.Text
        htSvcBill.Item("Date") = CStr(Me.txtDate.Text)
        htSvcBill.Item("Discount") = Me.txtDiscount.Text
        htSvcBill.Item("Other") = Me.txtOther.Text
        htSvcBill.Item("Remarks") = Me.txtRemarks.Text
        htSvcBill.Item("ServiceBillNo") = Me.txtServiceBillNo.Text
        htSvcBill.Item("Tax") = Me.txtTax.Text

        Session("dsitem") = dsItem
        Session("htSvcBill") = htSvcBill
    End Sub

    Private Function ValidateDetails() As Boolean
        Try
            ValidateDetails = False

            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            lblError.Text = ""
            lblError.Visible = True

            ComputeLineTotal()

            If Trim(Me.txtPartCode.Text) = "" Then
                lblError.Text = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRPARTID")
                MessageBox1.Alert(lblError.Text)
                Exit Function
            End If
            If Trim(Me.txtPartName.Text) = "" Then
                lblError.Text = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRPARTNAME")
                MessageBox1.Alert(lblError.Text)
                Exit Function
            End If
            If Trim(Me.txtPriceID.Text) = "" Then
                lblError.Text = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRPRICEID")
                MessageBox1.Alert(lblError.Text)
                Exit Function
            End If
            If Trim(Me.txtUnitPrice.Text) = "" Then
                lblError.Text = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRPRICE")
                MessageBox1.Alert(lblError.Text)
                Exit Function
            End If
            If Val(Me.txtQty.Text) <= 0 Then
                lblError.Text = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRQTY")
                MessageBox1.Alert(lblError.Text)
                Exit Function
            End If

            Me.txtLineTotal.Text = String.Format("{0:f2}", Val(Me.txtLineTotal.Text))
            Me.txtQty.Text = String.Format("{0:f0}", Val(Me.txtQty.Text))
            Me.txtUnitPrice.Text = String.Format("{0:f2}", Val(Me.txtUnitPrice.Text))
            lblError.Visible = False
            Return True

        Catch

        End Try

    End Function

    Private Function ValidateHeader() As Boolean
        Try
            ValidateHeader = False
            Dim PaymentTotal As Double
            RefreshDataSet()
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            lblError.Text = ""
            lblError.Visible = True

            ComputeNetTotal()
            lblError.Visible = True

            'If dsItem.Tables.Count = 0 Then
            '    lblError.Text = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRDETAILS")
            '    Exit Function
            'End If

            'If dsItem.Tables(0).Rows.Count = 0 Then
            '    lblError.Text = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRDETAILS")
            '    Exit Function
            'End If

            PaymentTotal = Val(txtCash.Text) + Val(txtCheque.Text) + _
                    Val(txtCredit.Text) + Val(txtCreditCard.Text) + _
                    Val(txtOther.Text)

            'If PaymentTotal <= 0 Then
            '    lblError.Text = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRPAYMENT")
            '    Exit Function
            'End If

            If Val(txtTotalNet.Text) < 0 Then
                lblError.Text = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRDISCMORE")
                If lblError.Text = "" Then lblError.Text = "Discount Must be Less Than Total"
                MessageBox1.Alert(lblError.Text)
                Exit Function
            End If

            If String.Format("{0:f2}", PaymentTotal) <> Val(txtTotalNet.Text) Then
                lblError.Text = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRPAYTOTAL")
                If lblError.Text = "" Then lblError.Text = "Payment Must be Equal the Net Total"
                MessageBox1.Alert(lblError.Text)
                Exit Function
            End If

            If Val(txtDiscount.Text) <> 0 And Trim(Me.txtRemarks.Text) = "" Then
                lblError.Text = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRDISCREM")
                If lblError.Text = "" Then lblError.Text = "Discount Remark is Required when you Enter a discount"
                MessageBox1.Alert(lblError.Text)
                Exit Function
            End If

            Me.txtTotal.Text = String.Format("{0:f2}", PaymentTotal)
            lblError.Visible = False
            Return True
        Catch

        End Try

    End Function

    Private Sub SaveServiceBill()
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")

        Dim invoice As String
        Dim clsSvcBill As New clsServiceBillUpdate

        clsSvcBill.TransactionPrefix = Me.txtInvoicePf.Text
        clsSvcBill.TransactionNo = Me.txtInvoiceNo.Text
        clsSvcBill.ServiceBill = Me.txtServiceBillNo.Text

        Dim ResultCtr As Integer = 0
        ResultCtr = clsSvcBill.CheckDuplicateServiceBillNo

        If ResultCtr > 0 Then
            lblError.Text = objXmlTr.GetLabelName("StatusMessage", "SBA-DUPSBN")
            lblError.Visible = True
            MessageBox1.Alert(lblError.Text)
            Exit Sub
        End If

        clsSvcBill.ServiceBillType = Me.cboServiceBillType.SelectedValue
        clsSvcBill.InvoiceDate = CDate(Me.txtDate.Text)
        clsSvcBill.Technician = Me.cboTechnician.SelectedValue
        clsSvcBill.ServiceType = Me.cboServiceType.SelectedValue
        clsSvcBill.AppointmentPrefix = Me.txtAppointmentPrefix.Text
        clsSvcBill.AppointmentNo = Me.txtAppointmentNo.Text
        clsSvcBill.CustomerPrefix = Me.txtCustomerPrefix.Text
        clsSvcBill.CustomerNo = Me.txtCustomerID.Text
        'clsSvcBill.ROSerialNo = Me.txtROUID.Text
        clsSvcBill.ROSerialNo = Me.cboRO.SelectedValue

        clsSvcBill.CustomerName = Me.txtCustomer.Text
        clsSvcBill.Country = Me.cboCountry.SelectedValue
        clsSvcBill.Company = Me.cboCompany.SelectedValue
        clsSvcBill.ServiceCenter = Me.cboServiceCenter.SelectedValue
        clsSvcBill.CreateBy = Session("userID").ToString().ToUpper
        clsSvcBill.CreateDate = CDate(Me.txtCreateDate.Text)
        clsSvcBill.ModifyBy = Session("userID").ToString().ToUpper
        clsSvcBill.ModifyDate = CDate(Me.txtModifyDate.Text)
        clsSvcBill.TotalGross = Me.txtTotalGross.Text
        clsSvcBill.Discount = Me.txtDiscount.Text
        clsSvcBill.TotalNet = Me.txtTotalNet.Text
        clsSvcBill.Tax = Me.txtTax.Text
        clsSvcBill.DiscRem = Me.txtRemarks.Text

        clsSvcBill.TaxPercentage = Val(Me.txtTaxRate.Text)
        clsSvcBill.TaxID = Me.txtTaxID.Text
        clsSvcBill.CashAmount = Val(Me.txtCash.Text)
        clsSvcBill.ChequeAmount = Val(Me.txtCheque.Text)
        clsSvcBill.CreditAmount = Val(Me.txtCredit.Text)
        clsSvcBill.CreditCardAmount = Val(Me.txtCreditCard.Text)
        clsSvcBill.OtherPaymentAmount = Val(Me.txtOther.Text)
        clsSvcBill.Status = Me.cboStatus.SelectedValue
        clsSvcBill.SessionID = Session("login_session")
        clsSvcBill.IPAddress = Request.UserHostAddress.ToString()
        RefreshDataSet()
        clsSvcBill.InvoiceDetails = dsItem

        invoice = clsSvcBill.UpdateServiceBill

        Session("currStatus") = Me.cboStatus.SelectedValue.ToString
 
        If invoice <> "-1" Then
            lnkAddItem.Enabled = False
            lnkSave.Enabled = False
            lnkGeneratePackingList.Enabled = True
            Me.txtInvoiceNo.Text = clsSvcBill.TransactionNo
            Me.txtInvoicePf.Text = clsSvcBill.TransactionPrefix

            lblError.Text = objXmlTr.GetLabelName("StatusMessage", "SBA-SUCSAVING")
            lblError.Visible = True
            MessageBox1.Alert(lblError.Text)

            ImageButton1.Enabled = False
            cboServiceBillType.Enabled = False
            txtServiceBillNo.Enabled = False
            cboTechnician.Enabled = True
            cboServiceType.Enabled = False
            btnSearchAppNo.Enabled = False
            lnkAddItem.Enabled = False
            txtDiscount.Enabled = False
            txtCash.Enabled = False
            txtCheque.Enabled = False
            txtCredit.Enabled = False
            txtCreditCard.Enabled = False
            txtOther.Enabled = False
            grdServiceBill.Enabled = False

            'CHECK IF TECHNICIAN GOT PACKING LIST TO GENERATE 
            'Comment this old code out - Wey 23032009
            Dim clsgenprf As New clsGenPRF
            ResultCtr = 0
            ResultCtr = clsSvcBill.CheckGenPackList

            clsgenprf.InvoiceDate = Me.txtDate.Text
            clsgenprf.Technician = Me.cboTechnician.SelectedValue
            clsgenprf.ServiceCenter = Me.cboServiceCenter.SelectedValue

            clsgenprf.logctrid = Session("login_ctryID").ToString.ToUpper
            clsgenprf.logcompanyid = Session("login_cmpID").ToString.ToUpper
            clsgenprf.logserviceid = Session("login_svcID").ToString.ToUpper
            clsgenprf.loguserid = Session("userID").ToString().ToUpper()

            ResultCtr = clsgenprf.CheckGenPackList

            If ResultCtr > 0 Then
                Me.lnkGeneratePackingList.Enabled = True
            Else
                Me.lnkGeneratePackingList.Enabled = False
            End If

        Else
            'prompt error

            lblError.Text = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRSAVING")
            lblError.Visible = True
            MessageBox1.Alert(lblError.Text)
        End If


    End Sub

#End Region

    Protected Sub btnSearchAppNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearchAppNo.Click
        'GetAppointmentdetails()
        RetainValuesBeforeSearch()
        Response.Redirect("AppointmentLookup.aspx?stat=SM")
    End Sub

    Protected Sub MessageBox1_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox1.GetMessageBoxResponse

        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
            SaveServiceBill()
        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
            Exit Sub
        End If

    End Sub

    Protected Sub Calendar1_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendar1.SelectionChanged
        Me.txtDate.Text = Me.Calendar1.SelectedDate
        Calendar1.Visible = False
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Calendar1.Visible = True
    End Sub

    Protected Sub lnkAddItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddItem.Click
        Dim valid As Boolean = ValidateDetails()
        If valid = True Then AddItems()
    End Sub

    Protected Sub lnkSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSave.Click

        Dim valid As Boolean = ValidateHeader()
        If valid = False Then Exit Sub
        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim msginfo As String = objXm.GetLabelName("StatusMessage", "SAVEORNOT")
        Dim msgtitle As String = objXm.GetLabelName("StatusMessage", "SAVETITLE")
        'MessageBox1.Confirm(msginfo)

        Me.txtDiscount.Text = Request.Form("txtDiscount")
        Me.txtTotalNet.Text = Request.Form("txtTotalNet")
        Me.txtCash.Text = Request.Form("txtCash")
        Me.txtCheque.Text = Request.Form("txtCheque")
        Me.txtCredit.Text = Request.Form("txtCredit")
        Me.txtCreditCard.Text = Request.Form("txtCreditCard")
        Me.txtOther.Text = Request.Form("txtOther")

        SaveServiceBill()

    End Sub

    Protected Sub MessageBox2_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox2.GetMessageBoxResponse
        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
            Response.Redirect("UpdateServiceBillPart1.Aspx")
        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
            Exit Sub
        End If

    End Sub

    Protected Sub lnkCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCancel.Click
        If lnkSave.Enabled = True Then
            Dim objXm As New clsXml
            objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            Dim msginfo As String = objXm.GetLabelName("StatusMessage", "SBA-CANCELMSG")
            Dim msgtitle As String = objXm.GetLabelName("StatusMessage", "SBA-CANCEL")
            'MessageBox2.Confirm(msginfo)
            Response.Redirect("UpdateServiceBillPart1.Aspx")
        Else
            Response.Redirect("UpdateServiceBillPart1.Aspx")
        End If
    End Sub

    Protected Sub lnkGeneratePackingList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkGeneratePackingList.Click
        Try
            Dim clsGPL As New clsServiceBillUpdate

            Dim clsgenprf As New clsGenPRF
            Dim ResultCtr As Integer = 0

            clsgenprf.InvoiceDate = Me.txtDate.Text
            clsgenprf.Technician = Me.cboTechnician.SelectedValue
            clsgenprf.ServiceCenter = Me.cboServiceCenter.SelectedValue

            clsgenprf.logctrid = Session("login_ctryID").ToString.ToUpper
            clsgenprf.logcompanyid = Session("login_cmpID").ToString.ToUpper
            clsgenprf.logserviceid = Session("login_svcID").ToString.ToUpper
            clsgenprf.loguserid = Session("userID").ToString().ToUpper()

            ResultCtr = 0

            If Session("currStatus").ToString() = "BL" Or Session("currStatus").ToString() = "UM" Then
                ResultCtr = clsgenprf.CheckGenPackList
                Dim prfNo As String

                If ResultCtr > 0 Then
                    prfNo = clsgenprf.GeneratePackingList()

                    If prfNo <> "" Then
                        ShowPRFReport(prfNo)
                    Else
                        lblError.Text = "Error Showing Report"
                        MessageBox1.Alert(lblError.Text)
                    End If
                Else
                    lblError.Text = "No Packing List to be generate."
                    MessageBox1.Alert(lblError.Text)
                End If
            Else
                lblError.Text = "Billed & Completed status only able to generate Packing List"
                MessageBox1.Alert(lblError.Text)
            End If

            lnkSave.Enabled = False

        Catch ex As Exception
            lblError.Text = ex.Message
        End Try

    End Sub

    Private Sub ShowPRFReport(ByVal prfNo As String)
        Dim packEntity As New ClsRptReprintPRF

        packEntity.servcenterid = Me.cboServiceCenter.SelectedValue
        packEntity.datemin = Me.txtDate.Text
        packEntity.tecnicianidbegin = Me.cboTechnician.SelectedValue
        packEntity.prfno = prfNo.Trim.ToUpper

        packEntity.logctrid = Session("login_ctryID").ToString.ToUpper
        packEntity.logcompanyid = Session("login_cmpID").ToString.ToUpper
        packEntity.logserviceid = Session("login_svcID").ToString.ToUpper
        packEntity.logrank = Session("login_rank").ToString().ToUpper()

        Session("rptreprintprf_search_condition") = packEntity

        Dim script As String = "window.open('../report/RptReprintPRFSearch.aspx')"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "RptReprintPRFSearch", script, True)

    End Sub
    Private Sub ShowSBCReport(ByVal SBCNo As String)
        Dim script As String = "window.open('../report/RptReprintSBCSearch.aspx?FIV1_SBCNO=" + SBCNo + "')"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "RptReprintSBCSearch", script, True)

    End Sub

    Protected Sub lnkEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEdit.Click
        ImageButton1.Enabled = True
        cboServiceType.Enabled = True
        lnkAddItem.Enabled = True
        txtDiscount.Enabled = True
        txtCash.Enabled = True
        txtCheque.Enabled = True
        txtCredit.Enabled = True
        txtCreditCard.Enabled = True
        txtOther.Enabled = True
        grdServiceBill.Enabled = True
        lnkSave.Enabled = True
        lnkEdit.Enabled = False
        lnkGeneratePackingList.Enabled = False
    End Sub

    Protected Sub txtQty_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtQty.TextChanged
        ComputeLineTotal()
        'Dim script As String = "self.location='#partcode';"
        'Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "partcode", script, True)
    End Sub

    Private Sub ComputeLineTotal()
        Try
            lblError.Visible = False
            lblError.Text = ""
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            If IsNumeric(txtQty.Text) Then
                If Val(Me.txtQty.Text) <= 0 Then
                    txtQty.Text = "0"
                    lblError.Text = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRQTY")
                    MessageBox1.Alert(lblError.Text)
                    Exit Sub
                End If
                If IsNumeric(txtUnitPrice.Text) Then
                    txtQty.Text = String.Format("{0:f0}", Val(txtQty.Text))
                    txtLineTotal.Text = String.Format("{0:f2}", Val(txtUnitPrice.Text) * (txtQty.Text))
                Else
                    txtQty.Text = "0"
                    txtUnitPrice.Text = ""
                    txtLineTotal.Text = "0"
                End If
            Else
                txtQty.Text = "0"
                txtLineTotal.Text = "0"
            End If
        Catch

        End Try

    End Sub

    Private Sub ComputeNetTotal()
        Try
            lblError.Visible = False
            lblError.Text = ""
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            If IsNumeric(txtDiscount.Text) Then
                If Val(Me.txtDiscount.Text) < 0 Then
                    lblError.Text = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRDISCLESS")
                    If lblError.Text = "" Then lblError.Text = "Discount Must be Greater Than or Equal to 0"
                    lblError.Visible = True
                    MessageBox1.Alert(lblError.Text)
                    txtDiscount.Text = "0"
                    txtTotalNet.Text = txtTotalGross.Text
                    Exit Sub
                End If
                If Val(Me.txtTotalGross.Text) - Val(Me.txtDiscount.Text) < 0 Then
                    lblError.Text = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRDISCMORE")
                    If lblError.Text = "" Then lblError.Text = "Discount Must be Less Than the Grand Total"
                    lblError.Visible = True
                    MessageBox1.Alert(lblError.Text)
                    txtDiscount.Text = "0"
                    txtTotalNet.Text = txtTotalGross.Text
                    Exit Sub
                End If
                txtDiscount.Text = String.Format("{0:f2}", Val(txtDiscount.Text))
                txtTotalNet.Text = String.Format("{0:f2}", Val(txtTotalGross.Text) - Val(txtDiscount.Text))
            Else
                txtDiscount.Text = "0"
                txtTotalNet.Text = txtTotalGross.Text
            End If
        Catch

        End Try

    End Sub

    Private Sub ComputePaymentTotal()
        Try
            lblError.Visible = False
            lblError.Text = ""
            If IsNumeric(txtCash.Text) Then
                If Val(txtCash.Text) < 0 Then
                    txtCash.Text = "0"
                End If
            Else
                txtCash.Text = "0"
            End If

            If IsNumeric(txtCheque.Text) Then
                If Val(txtCheque.Text) < 0 Then
                    txtCheque.Text = "0"
                End If
            Else
                txtCheque.Text = "0"
            End If

            If IsNumeric(txtCredit.Text) Then
                If Val(txtCredit.Text) < 0 Then
                    txtCredit.Text = "0"
                End If
            Else
                txtCredit.Text = "0"
            End If

            If IsNumeric(txtCreditCard.Text) Then
                If Val(txtCreditCard.Text) < 0 Then
                    txtCreditCard.Text = "0"
                End If
            Else
                txtCreditCard.Text = "0"
            End If

            If IsNumeric(txtOther.Text) Then
                If Val(txtOther.Text) < 0 Then
                    txtOther.Text = "0"
                End If
            Else
                txtOther.Text = "0"
            End If

            Dim PaymentTotal As Double
            PaymentTotal = Val(txtCash.Text) + Val(txtCheque.Text) + _
                    Val(txtCredit.Text) + Val(txtCreditCard.Text) + _
                    Val(txtOther.Text)

            Me.txtTotal.Text = String.Format("{0:f2}", PaymentTotal)
            Me.txtCash.Text = String.Format("{0:f2}", Val(Me.txtCash.Text))
            Me.txtCheque.Text = String.Format("{0:f2}", Val(Me.txtCheque.Text))
            Me.txtCredit.Text = String.Format("{0:f2}", Val(Me.txtCredit.Text))
            Me.txtCreditCard.Text = String.Format("{0:f2}", Val(Me.txtCreditCard.Text))
            Me.txtOther.Text = String.Format("{0:f2}", Val(Me.txtOther.Text))
        Catch

        End Try

    End Sub

    Protected Sub txtCash_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCash.TextChanged
        ComputePaymentTotal()
        'Dim script As String = "self.location='#dscnt';"
        'Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "dscnt", script, True)
    End Sub

    Protected Sub txtCheque_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCheque.TextChanged
        ComputePaymentTotal()
        'Dim script As String = "self.location='#dscnt';"
        'Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "dscnt", script, True)
    End Sub

    Protected Sub txtCredit_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCredit.TextChanged
        ComputePaymentTotal()
        'Dim script As String = "self.location='#dscnt';"
        'Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "dscnt", script, True)
    End Sub

    Protected Sub txtCreditCard_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCreditCard.TextChanged
        ComputePaymentTotal()
        'Dim script As String = "self.location='#dscnt';"
        'Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "dscnt", script, True)
    End Sub

    Protected Sub txtOther_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtOther.TextChanged
        ComputePaymentTotal()
        'Dim script As String = "self.location='#dscnt';"
        'Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "dscnt", script, True)
    End Sub

    Protected Sub txtDiscount_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDiscount.TextChanged
        ComputeNetTotal()
        'Dim script As String = "self.location='#dscnt';"
        'Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "dscnt", script, True)
    End Sub

    Protected Sub JCalendar1_SelectedDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles JCalendar1.SelectedDateChanged
        If JCalendar1.SelectedDate > Now() Then
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            lblError.Text = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRFUTUREDATE")
            MessageBox1.Alert(lblError.Text)
            Me.txtDate.Text = Date.Now().ToShortDateString
            lblError.Visible = True
        Else
            txtDate.Text = JCalendar1.SelectedDate
            lblError.Visible = False
        End If
    End Sub

    Private Sub GetCustomerRO()
        Try
            Dim clsCustomer As New clsServiceBillUpdate
            clsCustomer.Country = Me.cboCountry.SelectedValue
            clsCustomer.CustomerPrefix = txtCustomerPrefix.Text
            clsCustomer.CustomerNo = txtCustomerID.Text

            Dim dstCustomer As New DataSet
            dstCustomer = clsCustomer.GetCustomerRO

            If dstCustomer.Tables.Count <> 0 Then
                databonds(dstCustomer, cboRO, False)
            End If
            cboRO.Items.Insert(0, "")
            cboRO.SelectedIndex = 0
        Catch

        End Try

    End Sub

#Region "Ajax"
    <AjaxPro.AjaxMethod()> _
    Public Function DisplayPayment(ByVal Cash As String, ByVal Cheque As String, ByVal Credit As String, ByVal CreditCard As String, ByVal OtherPayment As String) As String
        Dim ldblTotalPayment As Double
        ldblTotalPayment = Val(Cash) + Val(Cheque) + Val(Credit) + Val(CreditCard) + Val(OtherPayment)
        DisplayPayment = Format(ldblTotalPayment, "0.00")
    End Function

    <AjaxPro.AjaxMethod()> _
    Public Function DisplayNetAfterDiscount(ByVal DiscountAmount As String, ByVal GrossTotal As String) As String
        Dim ldlNetAfterDiscount As Double
        ldlNetAfterDiscount = Val(GrossTotal) - Val(DiscountAmount)

        DisplayNetAfterDiscount = Format(ldlNetAfterDiscount, "0.00")
    End Function

    <AjaxPro.AjaxMethod()> _
    Public Function FormatNumber(ByVal Amount As String) As String
        FormatNumber = Format(Val(Amount), "0.00")
    End Function

#End Region

    Protected Sub lnkGenerateSBC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkGenerateSBC.Click
        Try
            Dim clsGPL As New clsServiceBillUpdate

            Dim clsgenSBC As New clsGenSBC
            Dim ResultCtr As Integer = 0

            clsgenSBC.InvoiceDate = Me.txtDate.Text
            clsgenSBC.Technician = Me.cboTechnician.SelectedValue
            clsgenSBC.ServiceCenter = Me.cboServiceCenter.SelectedValue

            clsgenSBC.logctrid = Session("login_ctryID").ToString.ToUpper
            'clsgenprf.logcompanyid = Session("login_cmpID").ToString.ToUpper
            clsgenSBC.logserviceid = Session("login_svcID").ToString.ToUpper
            clsgenSBC.loguserid = Session("userID").ToString().ToUpper()

            ResultCtr = 0

            If Session("currStatus").ToString() = "BL" Or Session("currStatus").ToString() = "UM" Or Session("currStatus").ToString() = "UA" Then
                ResultCtr = clsgenSBC.CheckSBC
                Dim SBCNo As String

                If ResultCtr > 0 Then
                    SBCNo = clsgenSBC.GenerateSBC()

                    If SBCNo <> "" Then
                        ShowSBCReport(SBCNo)
                    Else
                        lblError.Text = "Error Showing Report"
                        MessageBox1.Alert(lblError.Text)
                    End If
                Else
                    lblError.Text = "No SBC to be generate."
                    MessageBox1.Alert(lblError.Text)
                End If
            Else
                lblError.Text = "Billed, Completed & Cancelled status only able to generate SBC"
                MessageBox1.Alert(lblError.Text)
            End If

            lnkSave.Enabled = False

        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnRetrieve_Click(ByVal sender As Object, ByVal e As EventArgs)
        GetAppointmentdetails()
    End Sub

    Protected Sub lnkGenerateTaxInvoice_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkGenerateTaxInvoice.Click
        Session("InvoiceNo") = Me.txtInvoiceNo.Text
        Session("InvoicePf") = Me.txtInvoicePf.Text

        Dim script As String = "window.open('../function/PrintTaxInvoice.aspx')"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "PrintTaxInvoice", script, True)
    End Sub
End Class



