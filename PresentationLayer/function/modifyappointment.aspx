<%@ Page Language="VB" AutoEventWireup="false" CodeFile="modifyappointment.aspx.vb"
    Inherits="PresentationLayer_function_modifyappointment_aspx" EnableEventValidation="false" %>

<%@ Register Assembly="JCalendar" Namespace="JCalendar" TagPrefix="cc2" %>
<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Modify Appointment</title>
    <link href="../css/style.css" type="text/css" rel="stylesheet" />

    <script language="JavaScript" src="../js/common.js"></script>

    <script language="javascript">
    
    
 function LoadAppointmentEndTime_CallBack(response){
 
     //if the server-side code threw an exception
     if (response.error != null)
     {
      //we should probably do better than this
      alert(response.error); 
      
      return;
     }

     var ResponseValue = response.value;    
     var al = ResponseValue.split(":");
     var htime = al[0];
     var mtime = al[1];
     document.getElementById("<%=apptEndTimeHBox.ClientID%>").value =htime;
     document.getElementById("<%=apptEndTimeMBox.ClientID%>").value =mtime;
    
     
     
     
}
 
 
 function LoadAppointmentEndTime(objectClient)
{ 

    var starttime = objectClient.value;    
    PresentationLayer_function_modifyappointment_aspx.GetAppointmentEndTimeChange(starttime, LoadAppointmentEndTime_CallBack);
 
}




 function LoadTechnician_CallBack(response){
 
     //if the server-side code threw an exception
     if (response.error != null)
     {
      //we should probably do better than this
      alert(response.error); 
      
      return;
     }

     var ResponseValue = response.value;
     
   
     //if the response wasn't what we expected  
     if (ResponseValue == null || typeof(ResponseValue) != "object")
     {       
      return;
     }
          
     //Get the states drop down
     var ResultList = document.getElementById("<%=assignToDrpList.ClientID%>");
     ResultList.options.length = 0; //reset the states dropdown
 
    
     //Remember, its length not Length in JavaScript
     for (var i = 0; i < ResponseValue.length; ++i)
     {
     
      //the columns of our rows are exposed like named properties
      var al = ResponseValue[i].split(":");
      var alid = al[0];
      var alname = al[1];
      ResultList.options[ResultList.options.length] =  new Option(alname, alid);
     }
     
     
}
 
 function LoadCR_CallBack(response){
 
     //if the server-side code threw an exception
     if (response.error != null)
     {
      //we should probably do better than this
      alert(response.error); 
      
      return;
     }

     var ResponseValue = response.value;
     
   
     //if the response wasn't what we expected  
     if (ResponseValue == null || typeof(ResponseValue) != "object")
     {       
      return;
     }
          
     //Get the states drop down
     var ResultList = document.getElementById("<%=cboCrID.ClientID%>");
     ResultList.options.length = 0; //reset the states dropdown
 
    
     //Remember, its length not Length in JavaScript
     for (var i = 0; i < ResponseValue.length; ++i)
     {
     
      //the columns of our rows are exposed like named properties
      var al = ResponseValue[i].split(":");
      var alid = al[0];
      var alname = al[1];
      ResultList.options[ResultList.options.length] =  new Option(alname, alid);
     }
     
     
}

function LoadZoneList_CallBack(response){
 
     //if the server-side code threw an exception
     if (response.error != null)
     {
      //we should probably do better than this
      alert(response.error); 
      
      return;
     }

     var ResponseValue = response.value;
     
   
     //if the response wasn't what we expected  
     if (ResponseValue == null || typeof(ResponseValue) != "object")
     {       
      return;
     }
          
     //Get the states drop down
     var ResultList = document.getElementById("<%=cboZoneID.ClientID%>");
     ResultList.options.length = 0; //reset the states dropdown
 
    
     //Remember, its length not Length in JavaScript
     for (var i = 0; i < ResponseValue.length; ++i)
     {
     
      //the columns of our rows are exposed like named properties
      var al = ResponseValue[i].split(":");
      var alid = al[0];
      var alname = al[1];
      ResultList.options[ResultList.options.length] =  new Option(alname, alid);
      
     }
     
     
}

 
 function LoadTechnician(objectClient)
{
 
 var trantype = document.getElementById("<%=transTypeBox.ClientID%>").value;

 if (objectClient.selectedIndex > 0){
    var countryid ='<%=Session("login_ctry")%>';
    var companyid = '<%=Session("login_rank")%>';
    var servicecenterid = document.getElementById("<%=servCenterDrpList.ClientID%>").options[document.getElementById("<%=servCenterDrpList.ClientID%>").selectedIndex].value;
    var rank = '<%=Session("login_rank")%>';
    var areaid =document.getElementById("<%=areaBox.ClientID%>").value;
    var appointmentdate = document.getElementById("<%=apptDateBox.ClientID%>").value;
    var tranNo = document.getElementById("<%=transNoBox.ClientID%>").value;
    
  if (trantype == 'AS') {
  
    PresentationLayer_function_modifyappointment_aspx.GetTechnicianIDList(servicecenterid,LoadTechnician_CallBack);
    PresentationLayer_function_modifyappointment_aspx.GetCrIDList( servicecenterid,LoadCR_CallBack);
    }    
    
    PresentationLayer_function_modifyappointment_aspx.GetZoneList(trantype, areaid, appointmentdate,servicecenterid, tranNo, LoadZoneList_CallBack);
    
 }
 else
 {

    document.getElementById("<%=assignToDrpList.ClientID%>").options.length = 0
    document.getElementById("<%=cboCrID.ClientID%>").options.length = 0
 }
}




function LoadRoChange_CallBack(response){
 
     //if the server-side code threw an exception
     if (response.error != null)
     {
      //we should probably do better than this
      alert(response.error); 
      
      return;
     }

     
     var ResponseValue = response.value;    
//     var al = ResponseValue.split(":");

//if the response wasn't what we expected  
     if (ResponseValue == null || typeof(ResponseValue) != "object")
     {       
      return;
     }
     
     var lastservDate = ResponseValue[0];
     var Warranty = ResponseValue[1];
     var installBy = ResponseValue[2];
     var lastRemDate = ResponseValue[3];
     var contractNo = ResponseValue[4];
     var contEDate = ResponseValue[5];
     var lastApptDate= ResponseValue[6];
   
    
    document.getElementById("<%=lastservDateBox.ClientID%>").value =lastservDate;
    document.getElementById("<%=WarrantyBox.ClientID%>").value =Warranty;
    document.getElementById("<%=installByBox.ClientID%>").value =installBy;
    document.getElementById("<%=lastRemDateBox.ClientID%>").value =lastRemDate;
    document.getElementById("<%=contractNoBox.ClientID%>").value =contractNo;
    document.getElementById("<%=contEDateBox.ClientID%>").value =contEDate;
    document.getElementById("<%=lastApptDateBox.ClientID%>").value =lastApptDate;
    
     
}
 
 
 function LoadRoChange(objectClient)
{     
    var RoID= objectClient.value;
    var CustomerPrefix =document.getElementById("<%=CustPrxBox.ClientID%>").value;
    var CustomerID =document.getElementById("<%=CustIDBox.ClientID%>").value;
    var AppointmentDate=document.getElementById("<%=apptDateBox.ClientID%>").value;
      
    PresentationLayer_function_modifyappointment_aspx.GetRoInformationChange(RoID,CustomerPrefix,CustomerID,AppointmentDate, LoadRoChange_CallBack);
 
}

function LoadAppointmentDateChange()
{
    var trantype = document.getElementById("<%=transTypeBox.ClientID%>").value;
    var tranNo = '';//document.getElementById("<%=transNoBox.ClientID%>").value;
    var RoID= document.getElementById("<%=ROInfoDrpList.ClientID%>").options[document.getElementById("<%=ROInfoDrpList.ClientID%>").selectedIndex].value;
    var CustomerPrefix =document.getElementById("<%=CustPrxBox.ClientID%>").value;
    var CustomerID =document.getElementById("<%=CustIDBox.ClientID%>").value;
    var AppointmentDate=document.getElementById("<%=apptDateBox.ClientID%>").value;
    var countryid ='<%=Session("login_ctry")%>';
    var companyid = '<%=Session("login_rank")%>';
    var servicecenterid = document.getElementById("<%=servCenterDrpList.ClientID%>").options[document.getElementById("<%=servCenterDrpList.ClientID%>").selectedIndex].value;
    var rank = '<%=Session("login_rank")%>';
    var areaid =document.getElementById("<%=areaBox.ClientID%>").value;
   
    
    PresentationLayer_function_modifyappointment_aspx.GetRoInformationChange(RoID,CustomerPrefix,CustomerID,AppointmentDate, LoadRoChange_CallBack);
    PresentationLayer_function_modifyappointment_aspx.GetZoneList(trantype, areaid, AppointmentDate,servicecenterid,tranNo, LoadZoneList_CallBack);
    
 
}


function CheckScheduleClick() {
        var trantype = document.getElementById("<%=transTypeBox.ClientID%>").value;
        var RoID= document.getElementById("<%=ROInfoDrpList.ClientID%>").options[document.getElementById("<%=ROInfoDrpList.ClientID%>").selectedIndex].value;
        var CustomerPrefix =document.getElementById("<%=CustPrxBox.ClientID%>").value;
        var CustomerID =document.getElementById("<%=CustIDBox.ClientID%>").value;
        var AppointmentDate=document.getElementById("<%=apptDateBox.ClientID%>").value;
        var countryid ='<%=Session("login_ctry")%>';
        var companyid = '<%=Session("login_rank")%>';
        var servicecenterid = document.getElementById("<%=servCenterDrpList.ClientID%>").options[document.getElementById("<%=servCenterDrpList.ClientID%>").selectedIndex].value;
        
        var zoneid = document.getElementById("<%=cboZoneID.ClientID%>").options[document.getElementById("<%=cboZoneID.ClientID%>").selectedIndex].value;
    
        var strTempURL = "servCenter=" + servicecenterid + "&apptDate=" + AppointmentDate + "&zoneID=" + zoneid + "&timestamp="+ Date()
        strTempURL = "checkSchedulePopup.aspx?" + strTempURL
                
        showModalDialog(strTempURL,"_calPick","status=no;center=yes;dialogWidth=600pt;dialogHeight=400pt");
        
}

function DetailClick(){
        var trantype = document.getElementById("<%=transTypeBox.ClientID%>").value;
        var RoID= document.getElementById("<%=ROInfoDrpList.ClientID%>").options[document.getElementById("<%=ROInfoDrpList.ClientID%>").selectedIndex].value;
        var CustomerPrefix =document.getElementById("<%=CustPrxBox.ClientID%>").value;
        var CustomerID =document.getElementById("<%=CustIDBox.ClientID%>").value;
        var CustomerName =document.getElementById("<%=CustNameBox.ClientID%>").value;
        var AppointmentDate=document.getElementById("<%=apptDateBox.ClientID%>").value;
        var countryid ='<%=Session("login_ctry")%>';
        var companyid = '<%=Session("login_rank")%>';
        var servicecenterid = document.getElementById("<%=servCenterDrpList.ClientID%>").options[document.getElementById("<%=servCenterDrpList.ClientID%>").selectedIndex].value;        
        var zoneid = document.getElementById("<%=cboZoneID.ClientID%>").options[document.getElementById("<%=cboZoneID.ClientID%>").selectedIndex].value;
    

        var strTempURL = "custID=" + CustomerID + "&custPf=" + CustomerPrefix + "&custName=" + CustomerName + "&ROID=" + RoID + "&timestamp="+ Date()
        
        strTempURL = "appointmenthistorydetailPopup.aspx?" + strTempURL
        showModalDialog(strTempURL,"_calPick","status=no;center=yes;dialogWidth=600pt;dialogHeight=400pt");   
}

function ContractHistoryClick() {
        var trantype = document.getElementById("<%=transTypeBox.ClientID%>").value;
        var RoID= document.getElementById("<%=ROInfoDrpList.ClientID%>").options[document.getElementById("<%=ROInfoDrpList.ClientID%>").selectedIndex].value;
        var RoName= document.getElementById("<%=ROInfoDrpList.ClientID%>").options[document.getElementById("<%=ROInfoDrpList.ClientID%>").selectedIndex].text;
        var CustomerPrefix =document.getElementById("<%=CustPrxBox.ClientID%>").value;
        var CustomerID =document.getElementById("<%=CustIDBox.ClientID%>").value;
        var CustomerName =document.getElementById("<%=CustNameBox.ClientID%>").value;
        var AppointmentDate=document.getElementById("<%=apptDateBox.ClientID%>").value;
        var countryid ='<%=Session("login_ctry")%>';
        var companyid = '<%=Session("login_rank")%>';
        var servicecenterid = document.getElementById("<%=servCenterDrpList.ClientID%>").options[document.getElementById("<%=servCenterDrpList.ClientID%>").selectedIndex].value;        
        var zoneid = document.getElementById("<%=cboZoneID.ClientID%>").options[document.getElementById("<%=cboZoneID.ClientID%>").selectedIndex].value;
    
     
        var strTempURL  = "ROID=" + RoID + "&ROINFO=" + RoName + "&custName=" + CustomerName + "&custID=" + CustomerID+ "&custPf=" + CustomerPrefix + "&timestamp="+ Date()
                                   
        strTempURL = "contracthistoryPopup.aspx?" + strTempURL;
        showModalDialog(strTempURL,"_calPick","status=no;center=yes;dialogWidth=600pt;dialogHeight=400pt");   
        
        
}

function ServiceHistoryClick() {
        var trantype = document.getElementById("<%=transTypeBox.ClientID%>").value;
        var RoID= document.getElementById("<%=ROInfoDrpList.ClientID%>").options[document.getElementById("<%=ROInfoDrpList.ClientID%>").selectedIndex].value;
        var RoName= document.getElementById("<%=ROInfoDrpList.ClientID%>").options[document.getElementById("<%=ROInfoDrpList.ClientID%>").selectedIndex].text;
        var CustomerPrefix =document.getElementById("<%=CustPrxBox.ClientID%>").value;
        var CustomerID =document.getElementById("<%=CustIDBox.ClientID%>").value;
        var CustomerName =document.getElementById("<%=CustNameBox.ClientID%>").value;
        var AppointmentDate=document.getElementById("<%=apptDateBox.ClientID%>").value;
        var countryid ='<%=Session("login_ctry")%>';
        var companyid = '<%=Session("login_rank")%>';
        var servicecenterid = document.getElementById("<%=servCenterDrpList.ClientID%>").options[document.getElementById("<%=servCenterDrpList.ClientID%>").selectedIndex].value;        
        //var zoneid = document.getElementById("<%=cboZoneID.ClientID%>").options[document.getElementById("<%=cboZoneID.ClientID%>").selectedIndex].value;
        
        //var strTempURL = "ROID=" + RoID + "&ROINFO=" + RoName + "&custName=" + CustomerName + "&custID=" + CustomerID+ "&custPf=" + CustomerPrefix + "&timestamp="+ Date()       
        //strTempURL = "serverHistoryPopup.aspx?" + strTempURL;
        //showModalDialog(strTempURL,"_calPick","status=no;center=yes;dialogWidth=600pt;dialogHeight=400pt");   
        
        var strTempURL = "ROID=" + RoID + "&CustomerID=" + CustomerID+ "&customerPrefix=" + CustomerPrefix + "&timestamp="+ Date()
        strTempURL = "ViewAppointmentServiceHistory.aspx?" + strTempURL;
        window.open (strTempURL);
        
}


    </script>

</head>
<body>
    <form id="form1" runat="server">
        <div style="vertical-align: top">
            <table style="width: 100%" border="0">
                <tr>
                    <td style="width: 1068px">
                        <table cellspacing="0" cellpadding="0" width="100%" border="0">
                            <tr>
                                <td width="1%" background="../graph/title_bg.gif">
                                    <img height="24" src="../graph/title1.gif" width="5" /></td>
                                <td class="style2" width="98%" background="../graph/title_bg.gif" style="width: 80%">
                                    <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="LEFT" width="1%" background="../graph/title_bg.gif">
                                    <img height="24" src="../graph/title_2.gif" width="5" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="height: 30px; width: 1068px;">
                        <table style="width: 100%" cellspacing="0" cellpadding="0" border="0">
                            <tr>
                                <td class="style2" background="../graph/title_bg.gif" style="width: 110%">
                                    <font color="red" style="width: 100%">
                                        <asp:Label ID="errlab" runat="server" Text="Label" Visible="false"></asp:Label></font></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="width: 1068px">
                        <table cellspacing="1" cellpadding="2" bgcolor="#b7e6e6" border="0" style="width: 100%">
                            <tr bgcolor="#ffffff">
                                <td colspan="5">
                                    <asp:Label ID="lblNoteUP" runat="server" ForeColor="Red" Text="Label"></asp:Label>
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td colspan="5">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="left" style="width: 20%">
                                    <asp:Label ID="lblIsInbound" runat="server" />
                                </td>
                                <td align="left" style="width: 25%">
                                    <%--Modified by Ryan Estandarte 25 Sept 2012--%>
                                    <%--<asp:CheckBox ID="chkIsInbound" runat="server" Text="Yes?" />--%>
                                    <asp:RadioButtonList ID="rdoInboundOutbound" runat="server" RepeatDirection="Horizontal" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please select either inbound or outbound"
                                        Text="*" ControlToValidate="rdoInboundOutbound" />
                                </td>
                                <td align="left" style="width: 20%">
                                    <asp:Label ID="Label3" runat="server" Text="RMS No"></asp:Label></td>
                                <td align="left" style="width: 25%">
                                    <asp:TextBox ID="RMStxt" runat="server" CssClass="textborder" MaxLength="12" ReadOnly="True"
                                        Width="80%" Enabled="False"></asp:TextBox></td>
                                <td style="width: 10%">
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="LEFT" style="width: 20%">
                                    <asp:Label ID="transtypeLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 25%;">
                                    <asp:TextBox ID="transTypeBox" runat="server" CssClass="textborder" Width="80%" ReadOnly="True"></asp:TextBox></td>
                                <td align="LEFT" style="width: 20%">
                                    <asp:Label ID="transNoLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 25%;">
                                    <asp:TextBox ID="transNoBox" runat="server" CssClass="textborder" Width="80%" ReadOnly="True"></asp:TextBox>
                                </td>
                                <td style="width: 10%">
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="LEFT" style="width: 20%;">
                                    <asp:Label ID="ROInfoLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 25%;">
                                    <asp:DropDownList ID="ROInfoDrpList" runat="server" CssClass="textborder" Width="93%"
                                        AutoPostBack="false" onchange="LoadRoChange(this)">
                                    </asp:DropDownList>
                                </td>
                                <td align="LEFT" style="width: 20%">
                                    <asp:Label ID="apptStatusLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 25%;">
                                    <asp:DropDownList ID="apptStatDrpList" Width="93%" runat="server" CssClass="textborder"
                                        AutoPostBack="true">
                                    </asp:DropDownList>
                                </td>
                                <td style="width: 10%">
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="LEFT" style="width: 20%">
                                    <asp:Label ID="lastservDateLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 25%">
                                    <asp:TextBox ID="lastservDateBox" Width="80%" runat="server" CssClass="textborder"
                                        ReadOnly="True"></asp:TextBox></td>
                                <td align="LEFT" style="width: 20%;">
                                    <asp:Label ID="WarrantyLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 25%;">
                                    <asp:TextBox ID="WarrantyBox" runat="server" Width="80%" CssClass="textborder" ReadOnly="True"></asp:TextBox></td>
                                <td style="width: 10%">
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="LEFT" style="width: 20%;">
                                    <asp:Label ID="PICNameLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 25%;">
                                    <asp:TextBox ID="PICNameBox" runat="server" Width="80%" CssClass="textborder" MaxLength="50"></asp:TextBox></td>
                                <td align="LEFT" style="width: 20%">
                                    <asp:Label ID="PICContactLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 25%;">
                                    <asp:TextBox ID="PICContactBox" runat="server" Width="80%" CssClass="textborder"
                                        MaxLength="20"></asp:TextBox>
                                    <font color="red">*</font>
                                    <asp:RequiredFieldValidator ID="PICContactNullError" runat="server" ControlToValidate="PICContactBox"
                                        ForeColor="White">*</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="PICContactError" runat="server" ControlToValidate="PICContactBox"
                                        ValidationExpression="\d{0,20}" Width="1px" ForeColor="White">*</asp:RegularExpressionValidator></td>
                                <td style="width: 10%">
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="LEFT" style="width: 20%">
                                    <asp:Label ID="CustIDLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 25%">
                                    <asp:TextBox ID="CustIDBox" runat="server" Width="80%" CssClass="textborder" ReadOnly="True"></asp:TextBox></td>
                                <td align="LEFT" style="width: 20%;">
                                    <asp:Label ID="CustPrxLab" runat="server" Text="Label" style="visibility:hidden;"></asp:Label></td>
                                <td align="left" style="width: 25%;">
                                    <asp:TextBox ID="CustPrxBox" runat="server" Width="80%" CssClass="textborder" ReadOnly="True" style="visibility:hidden;"></asp:TextBox></td>
                                <td style="width: 10%;">
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="LEFT" style="width: 20%;">
                                    <asp:Label ID="contEDateLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 25%;">
                                    <asp:TextBox ID="contEDateBox" runat="server" Width="80%" CssClass="textborder" ReadOnly="True"></asp:TextBox></td>
                                <td align="LEFT" style="width: 20%;">
                                    <asp:Label ID="CustNameLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 25%;">
                                    <asp:TextBox ID="CustNameBox" runat="server" Width="80%" CssClass="textborder" ReadOnly="True"></asp:TextBox></td>
                                <td style="width: 10%">
                                </td>
                            </tr>
                        </table>
                        <table cellspacing="1" cellpadding="2" bgcolor="#b7e6e6" border="0" style="width: 100%">
                            <tr bgcolor="#ffffff" style="width: 100%">
                                <td align="LEFT" style="width: 20%;">
                                    <asp:Label ID="addr1Lab" runat="server" Text="Label"></asp:Label>
                                </td>
                                <td style="width: 80%;">
                                    <asp:TextBox ID="addr1Box" runat="server" CssClass="textborder" Width="80%" ReadOnly="True"></asp:TextBox></td>
                            </tr>
                            <tr bgcolor="#ffffff" style="width: 100%">
                                <td align="LEFT" style="width: 20%; height: 30px;">
                                    <asp:Label ID="addr2Lab" runat="server" Text="Label"></asp:Label>
                                </td>
                                <td style="width: 80%;">
                                    <asp:TextBox ID="addr2Box" runat="server" CssClass="textborder" Width="80%" ReadOnly="True"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                        <table cellspacing="1" cellpadding="2" bgcolor="#b7e6e6" border="0" style="width: 100%">
                            <tr bgcolor="#ffffff">
                                <td align="LEFT" style="width: 20%;" id="#ApptLoc">
                                    <asp:Label ID="POCodeLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 25%;">
                                    <asp:TextBox ID="POCodeBox" Width="80%" runat="server" CssClass="textborder" ReadOnly="true"></asp:TextBox></td>
                                <td align="LEFT" style="width: 20%;">
                                    <asp:Label ID="areaLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 25%;">
                                    <asp:TextBox ID="areaBox" Width="80%" runat="server" CssClass="textborder" ReadOnly="true"></asp:TextBox></td>
                                <td style="width: 10%">
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="LEFT" style="width: 20%;">
                                    <asp:Label ID="contryLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 25%;">
                                    <asp:TextBox ID="contryBox" Width="80%" runat="server" CssClass="textborder" ReadOnly="True"></asp:TextBox></td>
                                <td align="LEFT" style="width: 20%;">
                                    <asp:Label ID="stateLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 25%;">
                                    <asp:TextBox ID="stateBox" Width="80%" runat="server" CssClass="textborder" ReadOnly="True"></asp:TextBox></td>
                                <td style="width: 10%">
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="LEFT" style="width: 20%;">
                                    <asp:Label ID="apptDateLab" runat="server" Text="Label"></asp:Label><br />
                                    <asp:Label ID="Label2" runat="server" Text="DD/MM/YYYY"></asp:Label>
                                </td>
                                <td align="left" style="width: 25%;">
                                    <asp:TextBox ID="apptDateBox" runat="server" CssClass="textborder" Width="73%" MaxLength="10"
                                        onblur="LoadAppointmentDateChange()"></asp:TextBox>
                                    <font color="red">*</font>
                                    <asp:RequiredFieldValidator ID="apptDateError" runat="server" ControlToValidate="apptDateBox"
                                        Width="1px" ForeColor="White">*</asp:RequiredFieldValidator>
                                    <asp:HyperLink ID="HypCal" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                                        ToolTip="Choose a Date" Enabled="true">Choose a Date</asp:HyperLink>
                                    <cc2:JCalendar ID="apptDateCalendar" runat="server" ImgURL="~/PresentationLayer/graph/calendar.gif"
                                        ControlToAssign="apptDateBox" Visible="false" />
                                    <asp:CompareValidator ID="CompDateVal" runat="server" ControlToValidate="apptDateBox"
                                        Enabled="false" Visible="false" Operator="DataTypeCheck" SetFocusOnError="True"
                                        Type="Date"></asp:CompareValidator>
                                </td>
                                <td align="LEFT" style="width: 20%;">
                                    <asp:Label ID="apptTypeLab" runat="server" Text="Label"></asp:Label>
                                </td>
                                <td align="left" style="width: 25%;">
                                    <asp:DropDownList ID="apptTypeDrpList" runat="server" CssClass="textborder" Width="93%" AutoPostBack="True">
                                    </asp:DropDownList>
                                    <font color="red">*</font>
                                    <asp:RequiredFieldValidator ID="apptTypeError" runat="server" ControlToValidate="apptTypeDrpList"
                                        ForeColor="White">*</asp:RequiredFieldValidator>
                                </td>
                                <td style="width: 10%">
                                    <%--<asp:LinkButton ID="chkSchedulelkBtn" OnClientClick="CheckSchedule()" runat="server" CausesValidation="False" >Link</asp:LinkButton>--%>
                                    <asp:Label ID="chkSchedulelkBtn" OnClick="CheckScheduleClick()" runat="server" CssClass="cursor">Link</asp:Label>
                                </td>
                            </tr>
                            <!--SMR1711-2286 Add Nature of feedback-->
                            <tr bgcolor="#ffffff" id="trNatureFeedback" runat="server">
                                <td style="width: 20%;">                                    
                                </td>
                                <td style="width: 25%;">
                                </td>
                                <td align="LEFT" style="width: 20%;">
                                    <asp:Label ID="NatureFeedbackLab" runat="server" Text="Label"></asp:Label>
                                </td>
                                <td align="left" style="width: 25%;">
                                    <asp:DropDownList ID="ddlNatureFeedback" runat="server" CssClass="textborder" Width="93%">
                                    </asp:DropDownList>
                                    <font color="red">*</font>
                                    <asp:RequiredFieldValidator ID="NatureFeedbackError" runat="server" ControlToValidate="ddlNatureFeedback"
                                        ForeColor="White">*</asp:RequiredFieldValidator>
                                </td>
                                <td style="width: 10%">
                                </td>
                            </tr>                            
                            <tr bgcolor="#ffffff">
                                <td align="LEFT" style="width: 20%;">
                                    <asp:Label ID="apptStartTimeLab" runat="server" Text="Label"></asp:Label>
                                </td>
                                <td align="left" style="width: 25%;">
                                    <asp:DropDownList ID="apptStartTimeDrpList" Width="93%" runat="server" CssClass="textborder"
                                        AutoPostBack="false" onchange="LoadAppointmentEndTime(this)">
                                    </asp:DropDownList>
                                </td>
                                <td align="LEFT" style="width: 20%;">
                                    <asp:Label ID="apptEndTimeLab" runat="server" Text="Label"></asp:Label>
                                </td>
                                <td align="left" style="width: 25%;">
                                    <asp:TextBox ID="apptEndTimeHBox" runat="server" CssClass="textborder" Width="11%"
                                        MaxLength="2"></asp:TextBox>
                                    <asp:Label ID="Label1" runat="server" Text=":" Width="1px" Font-Bold="False" Font-Size="X-Large"></asp:Label>
                                    <asp:TextBox ID="apptEndTimeMBox" runat="server" CssClass="textborder" Width="11%"
                                        MaxLength="2"></asp:TextBox>
                                    <asp:RangeValidator ID="apptEndTimeHError" runat="server" ControlToValidate="apptEndTimeHBox"
                                        MaximumValue="23" MinimumValue="0" Type="Integer">*</asp:RangeValidator>
                                    <asp:RangeValidator ID="apptEndTimeMError" runat="server" ControlToValidate="apptEndTimeMBox"
                                        MaximumValue="59" MinimumValue="0" Type="Integer">*</asp:RangeValidator>
                                </td>
                                <td style="width: 10%">
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="LEFT" style="width: 20%;">
                                    <asp:Label ID="servCenterLab" runat="server" Text="Label"></asp:Label>
                                </td>
                                <td align="left" style="width: 25%;">
                                    <asp:DropDownList ID="servCenterDrpList" Width="93%" runat="server" CssClass="textborder"
                                        AutoPostBack="false" onchange="LoadTechnician(this)">
                                    </asp:DropDownList><font color="red">*</font>
                                    <asp:RequiredFieldValidator ID="servCIDError" runat="server" ControlToValidate="servCenterDrpList"
                                        ForeColor="White">*</asp:RequiredFieldValidator>
                                </td>
                                <td align="LEFT" style="width: 20%;">
                                    <%--SMR1711-2286 remove "Installed By", replaced with "Last Appointment Date" --%>
                                    <asp:Label ID="installByLab" runat="server" Text="Label" style="display:none;"></asp:Label>
                                    <asp:Label ID="lastApptDateLab" runat="server" Text="Label"></asp:Label>
                                </td>
                                <td align="left" style="width: 25%;">
                                    <asp:TextBox ID="installByBox" Width="80%" runat="server" CssClass="textborder" ReadOnly="true" style="display:none;"></asp:TextBox>
                                    <asp:TextBox ID="lastApptDateBox" Width="60%" runat="server" CssClass="textborder"
                                        ReadOnly="true"></asp:TextBox>
                                    <%--<asp:LinkButton ID="detaillkBtn" runat="server" Height="17px" CausesValidation="False">Link</asp:LinkButton>--%>
                                    <asp:Label ID="detaillkBtn" runat="server" Height="17px" CssClass="cursor" onclick="DetailClick()">Link</asp:Label>                                    
                                </td>
                                <td style="width: 10%">
                                    <%--<asp:LinkButton ID="servHistorylkBtn" runat="server" CausesValidation="False">Link</asp:LinkButton>--%>
                                    <asp:Label ID="servHistorylkBtn" runat="server" CssClass="cursor" onclick="ServiceHistoryClick()">Link</asp:Label>
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="LEFT" style="width: 20%;">
                                    <asp:Label ID="assignToLab" runat="server" Text="Label"></asp:Label>
                                </td>
                                <td align="left" style="width: 25%;">
                                    <asp:DropDownList ID="assignToDrpList" Width="93%" runat="server" CssClass="textborder">
                                    </asp:DropDownList><%--<font color="red">*</font>
                                    <asp:RequiredFieldValidator ID="technicanError" runat="server" ControlToValidate="assignToDrpList">*</asp:RequiredFieldValidator></td>--%>
                                </td>                                
                                <td align="LEFT" style="width: 20%;">
                                    <%--SMR1711-2286 remove "Last Reminder Date", replaced with "Contract No." --%>
                                    <asp:Label ID="lastRemDateLab" runat="server" Text="Label" style="display:none;"></asp:Label>
                                    <asp:Label ID="contractNoLab" runat="server" Text="Label"></asp:Label>
                                </td>
                                <td align="left" style="width: 25%;">
                                    <asp:TextBox ID="lastRemDateBox" Width="80%" runat="server" CssClass="textborder"
                                        ReadOnly="True" style="display:none;"></asp:TextBox>
                                    <asp:TextBox ID="contractNoBox" Width="80%" runat="server" CssClass="textborder"
                                        ReadOnly="true"></asp:TextBox>
                                </td>
                                <td style="width: 10%">
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="LEFT" style="width: 20%;">
                                    <asp:Label ID="lblCrID" runat="server" Text="Label"></asp:Label>
                                </td>
                                <td align="left" style="width: 25%;">
                                    <asp:DropDownList ID="cboCrID" Width="93%" runat="server" CssClass="textborder">
                                    </asp:DropDownList>
                                </td>
                                <td align="LEFT" style="width: 20%;">
                                </td>
                                <td align="left" style="width: 25%;">
                                </td>
                                <td style="width: 10%">
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="LEFT" style="width: 20%;">
                                    <asp:Label ID="ResReasonLab" runat="server" Text="Label"></asp:Label>
                                </td>
                                <td align="left" style="width: 25%;" colspan="3">
                                    <asp:DropDownList ID="ResReasonDrpList" Width="93%" runat="server" CssClass="textborder">
                                    </asp:DropDownList></td>
                                <td style="width: 10%;">
                                    <%--<asp:LinkButton ID="contractHistroylkBtn" runat="server" Height="17px" CausesValidation="False">Link</asp:LinkButton>--%>
                                    <asp:Label ID="contractHistroylkBtn" runat="server" Height="17px" CssClass="cursor"
                                        onclick="ContractHistoryClick()">Link</asp:Label>
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff" id="trReasonObjection" runat="server">
                                <td align="LEFT" style="width: 20%;">
                                    <asp:Label ID="ReasonObjection" runat="server" Text="Label"></asp:Label>
                                </td>
                                <td align="left" style="width: 25%;" colspan="3">
                                    <asp:DropDownList ID="ddlReasonObjection" Width="93%" runat="server" CssClass="textborder">
                                    </asp:DropDownList></td>
                                <td style="width: 10%;">
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td>
                                    <asp:Label ID="lblCreateBy" runat="server" Text="Create By"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCreateBy" Width="80%" runat="server" CssClass="textborder" ReadOnly="true"></asp:TextBox>
                                </td>
                                <td align="LEFT" style="width: 20%;">
                                    <asp:Label ID="lblCreateDate" runat="server" Text="Create Date"></asp:Label>
                                </td>
                                <td align="left" style="width: 25%;">
                                    <asp:TextBox ID="txtCreateDate" Width="80%" runat="server" CssClass="textborder"
                                        ReadOnly="true"></asp:TextBox>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td>
                                    <asp:Label ID="lblLastModifyBy" runat="server" Text="Last Modify By"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtLastModify" Width="80%" runat="server" CssClass="textborder"
                                        ReadOnly="true"></asp:TextBox>
                                </td>
                                <td align="LEFT" style="width: 20%;">
                                    <asp:Label ID="lastEditDateLab" runat="server" Text="Label"></asp:Label>
                                </td>
                                <td align="left" style="width: 25%;">
                                    <asp:TextBox ID="lastEditDateBox" Width="80%" runat="server" CssClass="textborder"
                                        ReadOnly="true"></asp:TextBox></td>
                                <td>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="width: 1068px">
                        <table cellspacing="1" cellpadding="2" bgcolor="#b7e6e6" border="0" style="width: 100%">
                            <tr bgcolor="#ffffff">
                                <td align="left" style="width: 20%;">
                                    <asp:Label ID="notesLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 50%;">
                                    <asp:TextBox ID="notesBox" runat="server" CssClass="textborder" Height="85px" Width="95%"
                                        MaxLength="400" TextMode="MultiLine"></asp:TextBox></td>
                                <td style="width: 15%">
                                </td>
                                <td style="width: 17%">
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="left" style="width: 20%;">
                                    <asp:Label ID="custNotesLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 50%;">
                                    <asp:TextBox ID="custNotesBox" runat="server" CssClass="textborder" Height="98px"
                                        Width="95%" ReadOnly="true" TextMode="MultiLine"></asp:TextBox></td>
                                <td align="left" style="width: 15%" colspan="2">
                                    <asp:Label ID="assToZoneLab" runat="server" Text="Label"></asp:Label>
                                    <%--</td><td align="left" style="width: 30%">                                    --%>
                                    <asp:DropDownList ID="cboZoneID" runat="server" CssClass="textborder" Width="93%"
                                        AutoPostBack="false">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="width: 1068px">
                        <table cellspacing="1" cellpadding="2" bgcolor="#b7e6e6" border="0" style="width: 100%">
                            <tr bgcolor="#ffffff">
                                <td>
                                    <table style="width: 100%">
                                        <tr>
                                            <th align="left">
                                                Send SMS
                                            </th>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RadioButtonList ID="rblSendSMS" runat="server" RepeatDirection="Horizontal"
                                                    AutoPostBack="true">
                                                    <asp:ListItem Text="Yes" Value="Y" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Text="No" Value="N"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table id="tblMobile" runat="server">
                                                    <tr>
                                                        <td>
                                                            <asp:RadioButtonList ID="rblMobile" runat="server" RepeatDirection="Horizontal" AutoPostBack="true">
                                                                <asp:ListItem Text="Mobile 1" Value="M1" Selected="True"></asp:ListItem>
                                                                <asp:ListItem Text="Mobile 2" Value="M2"></asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:TextBox ID="txtMobile" runat="server" ReadOnly="true" />&nbsp;
                                                            <asp:TextBox ID="txtMobileName" runat="server" ReadOnly="true" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="width: 1068px">
                        <asp:LinkButton ID="saveButton" runat="server"></asp:LinkButton>&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:HyperLink ID="cancelLink" runat="server" NavigateUrl="~/PresentationLayer/function/appointment.aspx">[cancelLink]</asp:HyperLink>
                    </td>
                </tr>
                <tr bgcolor="#ffffff">
                    <td colspan="4">
                        &nbsp;
                    </td>
                </tr>
                <tr bgcolor="#ffffff">
                    <td colspan="4">
                        <asp:Label ID="lblNoteDown" runat="server" ForeColor="Red" Text="Label"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
            ShowSummary="False" />
        <cc1:MessageBox ID="InfoMsgBox" runat="server" />
        <cc1:MessageBox ID="saveMsgBox" runat="server" />
    </form>
</body>
</html>
