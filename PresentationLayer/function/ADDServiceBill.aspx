<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ADDServiceBill.aspx.vb" Inherits="PresentationLayer_Function_ADDUpdateServiceBillPart1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>modify updata service bill part1</title>
    <meta http-equiv="Content-Type" content="text/html; charset=gb2312">
	<LINK href="../css/style.css" type="text/css" rel="stylesheet">
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table bgcolor="#b7e6e6" border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td background="../graph/title_bg.gif" width="1%">
                    <img height="24" src="../graph/title1.gif" width="5" /></td>
                <td background="../graph/title_bg.gif" class="style2" width="98%">
                    <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
            </tr>
        </table>
        <table border="0" cellpadding="0" cellspacing="0" width="50%">
            <tr>
                <td background="../graph/title_bg.gif" class="style2" style="height: 14px" width="98%">
                    <font color="red">
                        <asp:Label ID="errlab" runat="server" Text="Label" Visible="false"></asp:Label></font></td>
            </tr>
        </table>
        <table width=100%>
            <tr>
                <td>
                    <asp:Label ID="lblNoteUP" runat="server" ForeColor="Red" Text="Label"></asp:Label></td>
            </tr>
        </table>
        <table bgcolor="#b7e6e6" style="width: 520px">
            <tr bgcolor="#ffffff">
                <td style="width: 202px">
                    <asp:Label ID="sbtlab" runat="server" Text="Label" Width="95px"></asp:Label></td>
                <td style="width: 187px">
                    <asp:DropDownList ID="sbtyDropDownList" runat="server" Width="151px">
                    </asp:DropDownList></td>
                <td style="width: 114px">
                    <asp:Label ID="invoicelab" runat="server" Text="Label" Width="95px"></asp:Label></td>
                <td style="width: 179px">
                    <asp:TextBox ID="invoicelabtext" runat="server" Width="160px"></asp:TextBox></td>
            </tr>
            <tr bgcolor="#ffffff">
                <td style="width: 202px; height: 21px">
                    <asp:Label ID="scenterlab" runat="server" Text="Label" Width="95px"></asp:Label></td>
                <td style="width: 187px; height: 21px">
                    <asp:TextBox ID="scenterbox" runat="server"></asp:TextBox></td>
                <td style="width: 114px; height: 21px">
                    <asp:Label ID="stlab" runat="server" Text="Label" Width="95px"></asp:Label></td>
                <td style="width: 179px; height: 21px">
                    <asp:DropDownList ID="stDropDownList" runat="server" Width="160px">
                    </asp:DropDownList></td>
            </tr>
            <tr bgcolor="#ffffff">
                <td style="width: 202px">
                    <asp:Label ID="sblab" runat="server" Text="Label" Width="95px"></asp:Label></td>
                <td style="width: 187px">
                    <asp:TextBox ID="sbBox" runat="server"></asp:TextBox></td>
                <td colspan="1">
                    <asp:Label ID="technicianLab" runat="server" Text="Label"></asp:Label></td>
                <td colspan="2">
                    <asp:DropDownList ID="technicianList" runat="server" Width="98px">
                    </asp:DropDownList></td>
            </tr>
        </table>
    
    </div>
        <br />
        <asp:GridView ID="partGridView"  runat="server" Width="529px" AutoGenerateColumns="False">
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <%#Container.DataItemIndex + 1%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="FIV2_PARID" ReadOnly="True"/>
                <asp:BoundField DataField="MPAR_ENAME" ReadOnly="True" />
                <asp:BoundField DataField="FIV2_INVQT" />
                <asp:BoundField DataField="FIV2_PRICE"  ReadOnly="True" />
                <asp:BoundField DataField="FIV2_TAXID" ReadOnly="True" />
                <asp:BoundField DataField="FIV2_LINE" ReadOnly="True"  />
            </Columns>
        </asp:GridView>
        <br />
        <table bgcolor="#b7e6e6">
            <tr bgcolor="#ffffff">
                <td>
                    <asp:Label ID="totalLab" runat="server" Text="Label"></asp:Label></td>
                <td style="width: 50px">
                    <asp:TextBox ID="totalBox1" runat="server" Width="50px"></asp:TextBox></td>
                <td>
                    <asp:Label ID="discountLab" runat="server" Text="Label"></asp:Label></td>
                <td style="width: 50px">
                    <asp:TextBox ID="discountBox" runat="server" Width="50px" AutoPostBack="True"></asp:TextBox></td>
                <td>
                    <asp:Label ID="totalLab2" runat="server" Text="Label"></asp:Label></td>
                <td style="width: 50px">
                    <asp:TextBox ID="totaBox2" runat="server" Width="50px"></asp:TextBox></td>
                <td>
                    <asp:Label ID="InvoiceTaxLab" runat="server" Text="Label"></asp:Label></td>
                <td style="width: 50px">
                    <asp:TextBox ID="InvoiceTaxBox" runat="server" Width="50px"></asp:TextBox></td>
            </tr>
        </table>
        <br />
        &nbsp; &nbsp;&nbsp;
        <asp:Label ID="disRemark" runat="server" Text="Label"></asp:Label><br />
        <asp:TextBox ID="disRemarkbox" runat="server" TextMode="MultiLine" Height="100px" Width="350px"></asp:TextBox><br />
        <asp:Label ID="paylab" runat="server" Text="Label"></asp:Label><table bgcolor="#b7e6e6">
            <tr bgcolor="#ffffff">
                <td style="width: 100px; height: 25px;">
                    <asp:Label ID="cashlab" runat="server" Text="Label"></asp:Label></td>
                <td style="width: 79px; height: 25px;">
                    <asp:TextBox ID="cashtext" runat="server" Width="105px"></asp:TextBox>
                    <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="cashtext"
                        MinimumValue="0" Type="Double" MaximumValue="99999999999999999">*</asp:RangeValidator>&nbsp;
                    </td>
                <td style="width: 95px; height: 25px;">
                    <asp:Label ID="creditlab" runat="server" Text="Label"></asp:Label></td>
                <td style="width: 150px; height: 25px;">
                    <asp:TextBox ID="creditText" runat="server" Width="100px"></asp:TextBox>
                    <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="creditText"
                        MinimumValue="0" Type="Double" MaximumValue="999999999999999">*</asp:RangeValidator></td>
            </tr>
            <tr bgcolor="#ffffff">
                <td style="width: 100px; height: 12px">
                    <asp:Label ID="chequelab" runat="server" Text="Label"></asp:Label></td>
                <td style="width: 79px; height: 12px">
                    <asp:TextBox ID="chequeText" runat="server" Width="107px"></asp:TextBox>
                    &nbsp;
                    <asp:RangeValidator ID="RangeValidator3" runat="server" ControlToValidate="chequeText"
                        MinimumValue="0" Type="Double" MaximumValue="9999999999999999999">*</asp:RangeValidator></td>
                <td style="width: 95px; height: 12px">
                    <asp:Label ID="other1lab" runat="server" Text="Label"></asp:Label></td>
                <td style="width: 150px; height: 12px">
                    <asp:TextBox ID="optext" runat="server" Width="97px"></asp:TextBox>
                    &nbsp;&nbsp;
                    <asp:RangeValidator ID="RangeValidator4" runat="server" ControlToValidate="optext"
                        MinimumValue="0" MaximumValue="99999999999999999">*</asp:RangeValidator></td>
            </tr>
            <tr bgcolor="#ffffff">
                <td style="width: 100px; height: 25px">
                    <asp:Label ID="other2lab" runat="server" Text="Label"></asp:Label></td>
                <td style="width: 79px; height: 25px">
                    <asp:TextBox ID="creditcard" runat="server" Width="106px"></asp:TextBox>
                    <asp:RangeValidator ID="RangeValidator5" runat="server" ControlToValidate="creditcard"
                        MinimumValue="0" Type="Double" MaximumValue="99999999999999999999">*</asp:RangeValidator></td>
                <td style="width: 95px; height: 25px">
                </td>
                <td style="width: 150px; height: 25px">
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td style="width: 100px; height: 34px">
                    <asp:Label ID="total1lab" runat="server" Text="Label"></asp:Label></td>
                <td style="width: 79px; height: 34px">
                    <asp:TextBox ID="paytotalBox" runat="server" Width="106px"></asp:TextBox>
                    <asp:RangeValidator ID="RangeValidator6" runat="server" ControlToValidate="paytotalBox"
                        MinimumValue="0" Type="Double" MaximumValue="9999999999999999">*</asp:RangeValidator></td>
                <td style="width: 95px; height: 34px">
                </td>
                <td style="width: 150px; height: 34px">
                </td>
            </tr>
        </table>
        <table width=100%>
            <tr>
                <td>
                    <asp:Label ID="lblNoteDown" runat="server" ForeColor="Red" Text="Label"></asp:Label></td>
            </tr>
        </table>
        &nbsp;&nbsp;
        <asp:LinkButton ID="generateslib" runat="server">LinkButton</asp:LinkButton>
        &nbsp;
        <asp:LinkButton ID="savebtn" runat="server">LinkButton</asp:LinkButton>
        &nbsp;
        <asp:LinkButton ID="cancelbtn" runat="server" PostBackUrl="~/PresentationLayer/Function/UpdateServiceBillPart1.aspx">LinkButton</asp:LinkButton>
        <br />
        <br />
        <br />
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
            ShowSummary="False" />
    </form>
</body>
</html>
