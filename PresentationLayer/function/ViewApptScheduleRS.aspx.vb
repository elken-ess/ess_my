Imports BusinessEntity
Imports System.Data

Partial Class PresentationLayer_function_ViewApptScheduleRS_aspx
    Inherits System.Web.UI.Page
    Private objXmlTr As New clsXml
    Private datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
    Private masterApptSchedule As New clsMasterApptScheduleRS()
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try

            BindPage()
        End If
    End Sub
    Protected Sub BindPage()
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        Me.titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CHKS-TITL")
        Me.servCenterLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CHKS-SERVCENTER")
        Me.apptDateLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CHKS-APPTDATE")
        Me.zoneIDLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CHKS-ZONEID")
        Me.backlink.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-BACK")
        Me.lblShow.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_E_TOTALSHOW")

        Me.lblTotalASApt.Text = objXmlTr.GetLabelName("EngLabelMsg", "TotalASApt")
        Me.lblTotalSSApt.Text = "Total RS Appt/Max" 'objXmlTr.GetLabelName("EngLabelMsg", "TotalSSApt")
        Me.lblJobServiceType.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-TRANSTYPE")


        Me.masterApptSchedule = Session.Contents("MasterApptSchedule")
        If Me.masterApptSchedule IsNot Nothing Then
            Me.servCenterBox.Text = Me.masterApptSchedule.ServiceCenter
            Me.apptDateBox.Text = Me.masterApptSchedule.AppointDate
            Me.zoneIDBox.Text = Server.UrlDecode(Request.Params("zone"))
            Me.lblShow.Text += (" : " + Me.masterApptSchedule.getTotalAppt(0).ToString() + _
                             "/" + Me.masterApptSchedule.getTotalAppt(1).ToString())


            Me.txtJobServiceType.Text = Server.UrlDecode(Request.Params("JobType"))
            Me.txtTotalASApt.Text = Server.UrlDecode(Request.Params("TotalAS"))
            Me.txtTotalSSApt.Text = Server.UrlDecode(Request.Params("TotalSS"))
            BindGrid()
        End If
    End Sub
    Protected Sub BindGrid()
        Dim zoneid As String = Me.zoneIDBox.Text
        Dim argument As String = Server.UrlDecode(Request.Params("arg"))
        Dim datatable As DataTable = Nothing
        If argument.Equals("ZoneID") Then
            datatable = Me.masterApptSchedule.getApptByStrTm(zoneid)
        ElseIf argument.Equals("Others") Then
            datatable = Me.masterApptSchedule.getApptByStrTm(zoneid, 1)
        Else
            datatable = Me.masterApptSchedule.getApptByStrTm(zoneid, argument, 0)
        End If

        Me.ApptView.DataSource = datatable
        Me.ApptView.DataBind()

        If datatable.Rows.Count > 0 Then
            DispGridViewHead()
        End If
    End Sub

    Private Sub DispGridViewHead()
        Dim len As Integer = ApptView.HeaderRow.Cells.Count
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")



        ApptView.HeaderRow.Cells(0).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CHKS-NO")
        ApptView.HeaderRow.Cells(0).Font.Size = 8

        ApptView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-APPOINMENTNO")
        ApptView.HeaderRow.Cells(2).Font.Size = 8


        ApptView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CHKS-STIME")
        ApptView.HeaderRow.Cells(3).Font.Size = 8
        ApptView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CHKS-ETIME")
        ApptView.HeaderRow.Cells(4).Font.Size = 8
        ApptView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-CUSTOMERID")
        ApptView.HeaderRow.Cells(5).Font.Size = 8
        ApptView.HeaderRow.Cells(6).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CHKS-CUSNAME")
        ApptView.HeaderRow.Cells(6).Font.Size = 8
        ApptView.HeaderRow.Cells(7).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRID") & "-" & objXmlTr.GetLabelName("EngLabelMsg", "BB-MASUSER-0004")
        ApptView.HeaderRow.Cells(7).Font.Size = 8
        ApptView.HeaderRow.Cells(8).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-TECHID") & "-" & objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMTCH-0002")
        ApptView.HeaderRow.Cells(8).Font.Size = 8

    End Sub

End Class
