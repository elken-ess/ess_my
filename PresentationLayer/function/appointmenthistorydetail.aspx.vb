﻿Imports BusinessEntity
Imports System.Data
Partial Class PresentationLayer_function_appointmenthistorydetail
    Inherits System.Web.UI.Page
    Dim objXmlTr As New clsXml
    Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
    Dim apptEntity As New clsAppointment()
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            Me.titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-DETAIL-TITL")
            Me.custIDLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-CUSTID")
            Me.custPfLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-CUSTPRX")
            Me.custNameLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-CUSTNAME")
            Me.backLink.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-BACK")
            backLink.Attributes("onclick") = "javascript:history.back();"
            Me.custIDBox.Text = Request.Params("custID").ToString()
            Me.custPfBox.Text = Request.Params("custPf").ToString()
            Me.custNameBox.Text = Request.Params("custName").ToString()
            BindGrid()
        ElseIf DetailView.Rows.Count > 0 Then
            DispGridViewHead()
        End If
    End Sub

    Protected Sub BindGrid()
        apptEntity.ROID = Request.Params("ROID").ToString()
        apptEntity.CustomerID = Me.custIDBox.Text
        apptEntity.CustomerPrifx = Me.custPfBox.Text

        Dim ds As DataSet = apptEntity.GetApptHSTDetail()
        If ds Is Nothing Then
            Response.Redirect("~/PresentationLayer/error.aspx")
        End If


        Dim count As Integer
        Dim statXmlTr As New clsXml
        Dim strPanm As String
        statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        For count = 0 To ds.Tables(0).Rows.Count - 1
            Dim statusid As String = ds.Tables(0).Rows(count).Item(8).ToString
            strPanm = statXmlTr.GetLabelName("StatusMessage", statusid)
            'ds.Tables(0).Rows(count).Item(4) = strPanm

            Dim lstrReason As String = ds.Tables(0).Rows(count).Item(8).ToString
            If lstrReason <> "" Then
                strPanm = statXmlTr.GetLabelName("StatusMessage", lstrReason)
                ds.Tables(0).Rows(count).Item(8) = strPanm
            End If
        Next


        Session("DetailView") = ds
        Me.DetailView.DataSource = ds
        DetailView.AllowPaging = True
        DetailView.AllowSorting = True
        Me.DetailView.DataBind()
        If ds.Tables(0).Rows.Count > 0 Then
            DispGridViewHead()
        End If
    End Sub
    Private Sub DispGridViewHead()
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        DetailView.HeaderRow.Cells(0).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-TRANSTYPE") 'transaction Type
        DetailView.HeaderRow.Cells(0).Font.Size = 8
        DetailView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-TRANSNO") 'transactionNo
        DetailView.HeaderRow.Cells(1).Font.Size = 8

        DetailView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTSOSA-APPTTYPE") 'transactionNo
        DetailView.HeaderRow.Cells(2).Font.Size = 8

        DetailView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-APPTDATE")
        DetailView.HeaderRow.Cells(3).Font.Size = 8
        DetailView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CHKS-STIME")
        DetailView.HeaderRow.Cells(4).Font.Size = 8
        DetailView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CHKS-ETIME")
        DetailView.HeaderRow.Cells(5).Font.Size = 8
        DetailView.HeaderRow.Cells(6).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-DETAIL-TCHID") & "-" & objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMTCH-0002")
        DetailView.HeaderRow.Cells(6).Font.Size = 8

        DetailView.HeaderRow.Cells(7).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CRID") & "-" & objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTCDAR-USERNM") 'transactionNo
        DetailView.HeaderRow.Cells(7).Font.Size = 8


        DetailView.HeaderRow.Cells(8).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Status")
        DetailView.HeaderRow.Cells(8).Font.Size = 8

        DetailView.HeaderRow.Cells(9).Text = objXmlTr.GetLabelName("EngLabelMsg", "APPOINTMENT_CREATEDATE")
        DetailView.HeaderRow.Cells(9).Font.Size = 8


    End Sub

    Protected Sub DetailView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles DetailView.PageIndexChanging
        DetailView.PageIndex = e.NewPageIndex
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Dim ds As DataSet = Session("DetailView")
        DetailView.DataSource = ds
        DetailView.DataBind()
        If ds.Tables(0).Rows.Count > 0 Then
            DispGridViewHead()
        End If
    End Sub
End Class