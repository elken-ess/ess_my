﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="addcontract.aspx.vb" Inherits="PresentationLayer_function_addcontract"    EnableEventValidation="false" %>

<%@ Register Assembly="JCalendar" Namespace="JCalendar" TagPrefix="cc2" %>

<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Add Contract</title>
     <script language="JavaScript" src="../js/common.js"></script>
   <link href="../css/style.css" type="text/css" rel="stylesheet">
  
  </head>
<body>

<SCRIPT language="JavaScript" type="text/javascript">  
 
 function LoadROUID_CallBack(response){
 
     //if the server-side code threw an exception
     if (response.error != null)
     {
      //we should probably do better than this
      alert(response.error);     
      return;
     }
     var ResponseValue = response.value; 
        //if the response wasn't what we expected  
     if (ResponseValue == null || typeof(ResponseValue) != "object")
     {       
      return;
     }
       document.getElementById("<%=lastServDateBox.ClientID%>").value = ResponseValue[0];
       document.getElementById("<%=installDateBox.ClientID%>").value = ResponseValue[1];
       document.getElementById("<%=nextServDateBox.ClientID%>").value = ResponseValue[3];
       document.getElementById("<%=preCntNoBox.ClientID%>").value = ResponseValue[4];
 }

 function LoadROUID(objectClient)
{
 
//    if ( objectClient.selectedIndex> 0)
//    {
        var CustomerPrefix =document.getElementById("<%=custPfBox.ClientID%>").value;
        var CustomerID =document.getElementById("<%=CustIDBox.ClientID%>").value;
        var rouid =  document.getElementById("<%=ROInfoDrpList.ClientID%>").options[document.getElementById("<%=ROInfoDrpList.ClientID%>").selectedIndex].value;
        var CreatedDate = "";
        PresentationLayer_function_addcontract.AjaxChangeRO(rouid,CustomerPrefix,CustomerID,CreatedDate, LoadROUID_CallBack);

//    }
//    else
//    {
//     document.getElementById("<%=lastServDateBox.ClientID%>").value = '';
//     document.getElementById("<%=installDateBox.ClientID%>").value = '';
//     document.getElementById("<%=nextServDateBox.ClientID%>").value = '';
//    
//    }
}


function LoadContract_CallBack(response){
 
     //if the server-side code threw an exception
     if (response.error != null)
     {
      //we should probably do better than this
      alert(response.error); 
      
      return;
     }

     var ResponseValue = response.value;
     
   
     //if the response wasn't what we expected  
     if (ResponseValue == null || typeof(ResponseValue) != "object")
     {       
      return;
     }
          
        document.getElementById("<%=txtFrequency.ClientID%>").value = ResponseValue[0];
       document.getElementById("<%=txtNoOfYear.ClientID%>").value = ResponseValue[1];
       document.getElementById("<%=cntEndDateBox.ClientID%>").value = ResponseValue[2];
       document.getElementById("<%=cntDspBox.ClientID%>").value = ResponseValue[3];
        
     
     
}

 function LoadContract(objectClient)
{
 if (objectClient.selectedIndex > 0)
 {
  var ctry =  '<%=Session("login_ctryID")%>';
   var conty =document.getElementById("<%=cntTypeDrpList.ClientID%>").options[document.getElementById("<%=cntTypeDrpList.ClientID%>").selectedIndex].value;
    var strdt = document.getElementById("<%=cntStartDateBox.ClientID%>").value;
   PresentationLayer_function_addcontract.ContractChange( ctry, conty, strdt,LoadContract_CallBack);
    
 }
 else
 {
                   
       document.getElementById("<%=txtFrequency.ClientID%>").value = '0';
       document.getElementById("<%=txtNoOfYear.ClientID%>").value = '0';
       document.getElementById("<%=cntEndDateBox.ClientID%>").value =  document.getElementById("<%=cntStartDateBox.ClientID%>").value;
       document.getElementById("<%=cntDspBox.ClientID%>").value = '';
   }
}



function ComputeEndDate_CallBack(response)
{
 document.getElementById("<%=cntEndDateBox.ClientID%>").value = response.value;
 }


function ComputeEndDate()
{
 var NOY = document.getElementById("<%=txtNoOfYear.ClientID%>").value;
 var StartDate = document.getElementById("<%=cntStartDateBox.ClientID%>").value; 
  PresentationLayer_function_addcontract.AjaxComputeEndDate(StartDate,  NOY, ComputeEndDate_CallBack);
 }


function ContractHistoryClick() {
        
        var RoID= document.getElementById("<%=ROInfoDrpList.ClientID%>").options[document.getElementById("<%=ROInfoDrpList.ClientID%>").selectedIndex].value;
        var RoName= document.getElementById("<%=ROInfoDrpList.ClientID%>").options[document.getElementById("<%=ROInfoDrpList.ClientID%>").selectedIndex].text;
        var CustomerPrefix =document.getElementById("<%=custPfBox.ClientID%>").value;
        var CustomerID =document.getElementById("<%=CustIDBox.ClientID%>").value;
        var CustomerName =document.getElementById("<%=custNameBox.ClientID%>").value;
        
     
        var strTempURL  = "ROID=" + RoID + "&ROINFO=" + RoName + "&custName=" + CustomerName + "&custID=" + CustomerID+ "&custPf=" + CustomerPrefix
                                   
        strTempURL = "contracthistoryPopup.aspx?" + strTempURL;
        showModalDialog(strTempURL,"_calPick","status=no;center=yes;dialogWidth=600pt;dialogHeight=400pt");   
        
        
}

function ServiceHistoryClick() {
        
        var RoID= document.getElementById("<%=ROInfoDrpList.ClientID%>").options[document.getElementById("<%=ROInfoDrpList.ClientID%>").selectedIndex].value;
        var RoName= document.getElementById("<%=ROInfoDrpList.ClientID%>").options[document.getElementById("<%=ROInfoDrpList.ClientID%>").selectedIndex].text;
        var CustomerPrefix =document.getElementById("<%=custPfBox.ClientID%>").value;
        var CustomerID =document.getElementById("<%=CustIDBox.ClientID%>").value;
        var CustomerName =document.getElementById("<%=custNameBox.ClientID%>").value;
        
        var strTempURL = "ROID=" + RoID + "&ROINFO=" + RoName + "&custName=" + CustomerName + "&custID=" + CustomerID+ "&custPf=" + CustomerPrefix
        strTempURL = "serverHistoryPopup.aspx?" + strTempURL;
        showModalDialog(strTempURL,"_calPick","status=no;center=yes;dialogWidth=600pt;dialogHeight=400pt");   
        
}

</script> 

    <form id="form1" runat="server">
        <div style="vertical-align: top">
            <table style="width: 100%;" border="0">
                <tr>
                    <td style="width: 729px; height: 40px">
                        <table cellspacing="0" cellpadding="0" width="100%" border="0">
                            <tr>
                                <td width="1%" background="../graph/title_bg.gif">
                                    <img height="24" src="../graph/title1.gif" width="5"></td>
                                <td class="style2" width="98%" background="../graph/title_bg.gif" style="width: 80%">
                                    <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="right" width="1%" background="../graph/title_bg.gif">
                                    <img height="24" src="../graph/title_2.gif" width="5"></td>
                            </tr>
                        </table>
                                        <asp:Label ID="errlab" runat="server" Text="Label" Visible="false"></asp:Label></td>
                </tr>
                <tr>
                    <td style="height: 30px; width: 729px;">
                    </td>
                </tr>
                <tr>
                    <td style="font-family: Verdana; width: 729px;">
                        <table cellspacing="1" cellpadding="2" bgcolor="#b7e6e6" border="0" style="width: 100%">
                       <tr bgcolor="#ffffff">
	<td colspan = 5>
                            <asp:Label ID="lblNoteUP" runat="server" ForeColor="Red" Text="Label"></asp:Label>
	</td>
</tr>
<tr bgcolor="#ffffff">
<td colspan = 5 style="height: 18px">
                        	&nbsp;
       	</td>
</tr>
 
                        
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width:15%; height: 30px;">
                                    <asp:Label ID="custIDLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 30%; height: 30px;">
                                    <asp:TextBox ID="custIDBox" runat="server" CssClass="textborder" ReadOnly="true" Width="80%" MaxLength="10"></asp:TextBox><%--<font
                                        color="red">*</font>
                                    <asp:RequiredFieldValidator ID="custIDError" runat="server" ControlToValidate="custIDBox"
                                        Style="width: 1%">*</asp:RequiredFieldValidator>--%></td>
                                <td align="right" style="width:15%; height: 30px;">
                                    <asp:Label ID="custPfLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 30%; height: 30px;">
                                    <asp:TextBox ID="custPfBox" runat="server" CssClass="textborder" ReadOnly="true"  Width="80%" MaxLength="2"></asp:TextBox><%--<font
                                        color="red">*</font>
                                    <asp:RequiredFieldValidator ID="custPfError" runat="server" ControlToValidate="custPfBox">*</asp:RequiredFieldValidator>--%></td>
                                <td style="width: 10%; height: 30px;">
                                    <asp:LinkButton ID="revCustomlkBtn" runat="server" Height="17px" PostBackUrl="~/PresentationLayer/function/selectcustomer.aspx" CausesValidation="False">Link</asp:LinkButton></td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width:15%">
                                    <asp:Label ID="ROInfoLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 30%">
                                    <asp:DropDownList ID="ROInfoDrpList" runat="server" CssClass="textborder" Width="91%" AutoPostBack="false"  onchange="LoadROUID(this)">
                                    </asp:DropDownList><%--<font color="red">*</font>
                                    <asp:RequiredFieldValidator ID="ROInfoError" runat="server" ControlToValidate="ROInfoDrpList">*</asp:RequiredFieldValidator>--%></td>
                                <td align="right" style="width:15%">
                                    <asp:Label ID="custNameLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 30%">
                                    <asp:TextBox ID="custNameBox" runat="server" Width="80%" CssClass="textborder" ReadOnly="True"></asp:TextBox></td>
                                <td style="width: 10%">
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width:15%">
                                    <asp:Label ID="custContactLab" runat="server" Text="Label"></asp:Label></td>
                                <td style="width: 30%">
                                    <asp:TextBox ID="custContactBox" runat="server" CssClass="textborder" ReadOnly="True" Width="80%"></asp:TextBox></td>
                                <td align="right" style="width:15%">
                                    <asp:Label ID="preCntNoLab" runat="server" Text="Label"></asp:Label></td>
                                <td style="width: 30%">
                                    <asp:TextBox ID="preCntNoBox" runat="server" CssClass="textborder"  Width="80%" ReadOnly="True"></asp:TextBox></td>
                                <td style="width: 10%">
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width:15%">
                                    <asp:Label ID="cntTypeLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 30%">
                                    <asp:DropDownList ID="cntTypeDrpList" runat="server" CssClass="textborder" Width="91%" AutoPostBack="false" onchange="LoadContract(this)">
                                    </asp:DropDownList><font color="red">*</font><asp:RequiredFieldValidator ID="cntTypeError" runat="server" ControlToValidate="cntTypeDrpList" ForeColor="White">*</asp:RequiredFieldValidator>
                                    <asp:TextBox ID="txtFrequency" runat="server" Width="32px" Visible="true" Height =0 BorderStyle =None BorderColor =white ForeColor=white  ></asp:TextBox>
                                    <asp:TextBox ID="txtNoOfYear" runat="server" Width="32px" Visible="true"  Height =0 BorderStyle =None BorderColor =white ForeColor=white></asp:TextBox></td>
                                <td align="right" style="width:15%">
                                    <asp:Label ID="cntNoLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 30%">
                                    <asp:TextBox ID="cntNoBox" runat="server" CssClass="textborder" Width="80%" MaxLength="10"></asp:TextBox>
                                    <font
                                        color="red">*</font>
                                    <asp:RequiredFieldValidator ID="cntNoError" runat="server" ControlToValidate="cntNoBox">*</asp:RequiredFieldValidator></td>
                                <td style="width: 10%">
                                </td>
                            </tr>
                        </table>
                        <table cellspacing="1" cellpadding="2" bgcolor="#b7e6e6" border="0" style="width: 100%">
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width:14%">
                                    <asp:Label ID="cntDspLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 80%">
                                    <asp:TextBox ID="cntDspBox" runat="server" CssClass="textborder" Width="80%" ReadOnly="True"></asp:TextBox></td>
                            </tr>
                        </table>
                        <table cellspacing="1" cellpadding="2" bgcolor="#b7e6e6" border="0" style="width: 100%">
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width:15%">
                                    <asp:Label ID="lastServDateLab" runat="server" Text="Label"></asp:Label></td>
                                <td style="width: 30%">
                                    <asp:TextBox ID="lastServDateBox"  Width="80%" runat="server" CssClass="textborder" ReadOnly="True"></asp:TextBox></td>
                                <td align="right" style="width:15%">
                                    <asp:Label ID="installDateLab" runat="server" Text="Label"></asp:Label></td>
                                <td style="width: 30%">
                                    <asp:TextBox ID="installDateBox"  Width="80%" runat="server" CssClass="textborder" ReadOnly="True"></asp:TextBox></td>
                                <td style="width: 10%">
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width:15%">
                                    <asp:Label ID="cntStartDateLab" runat="server" Text="Label"></asp:Label><br />
                                    <asp:Label ID="Label1" runat="server" Text="DD/MM/YYYY"></asp:Label></td>
                                <td style="width: 30%">
                                    <asp:TextBox ID="cntStartDateBox"  Width="60%" runat="server" CssClass="textborder" MaxLength="10" onblur="ComputeEndDate()" ></asp:TextBox>
                                    <font color="red">*</font>
                                    <asp:RequiredFieldValidator ID="cntStartDateError" runat="server" ControlToValidate="cntStartDateBox" ForeColor="White">*</asp:RequiredFieldValidator>
                                    <asp:HyperLink ID="HypCal" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                                        ToolTip="Choose a Date">Choose a Date</asp:HyperLink>
                                        <cc2:JCalendar ID="cntStartDateCalendar" runat="server" ImgURL="~/PresentationLayer/graph/calendar.gif" ControlToAssign="cntStartDateBox" Visible="False" />
                                    <asp:CompareValidator ID="CompDateVal" runat="server" ControlToValidate="cntStartDateBox"
                                        Operator="DataTypeCheck" SetFocusOnError="True" Type="Date"></asp:CompareValidator></td>
                                <td align="right" style="width:15%">
                                    <asp:Label ID="cntEndDateLab" runat="server" Text="Label"></asp:Label></td>
                                <td style="width: 30%">
                                    <asp:TextBox ID="cntEndDateBox"  Width="80%" runat="server" CssClass="textborder" ReadOnly="True"></asp:TextBox></td>
                                <td style="width: 10%">
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width:15%">
                                    <asp:Label ID="signByLab" runat="server" Text="Label"></asp:Label></td>
                                <td style="width: 30%">
                                    <asp:DropDownList ID="cboTechnician" Width="98%" Font-Size="9pt" runat="server">
                                    </asp:DropDownList></td>
                                <td align="right" style="width:15%">
                                    <asp:Label ID="cntStatusLab" runat="server" Text="Label"></asp:Label></td>
                                <td style="width: 30%">
                                    <asp:TextBox ID="cntStatusBox"  Width="80%" runat="server" CssClass="textborder" ReadOnly="True"></asp:TextBox>
                                    <%--<asp:DropDownList ID="cntStatusDrpList" runat="server" CssClass="textborder">
                                    </asp:DropDownList>--%>
                                </td>
                                <td style="width: 10%">
                                    <%--<asp:LinkButton ID="servHistorylkBtn" runat="server" CausesValidation="False">Link</asp:LinkButton>--%>
                                    
                                    <asp:label ID="servHistorylkBtn" runat="server" CssClass ="cursor" onclick="ServiceHistoryClick()">Link</asp:label>
                                    
                                    
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width:15%">
                                    <asp:Label ID="servCLab" runat="server" Text="Label"></asp:Label></td>
                                <td style="width: 30%">
                                    <asp:DropDownList ID="servCDrpList" runat="server" CssClass="textborder"  Width="91%">
                                    </asp:DropDownList><font color="red">*</font><asp:RequiredFieldValidator ID="servCError" runat="server" ControlToValidate="servCDrpList" ForeColor="White">*</asp:RequiredFieldValidator></td>
                                <td align="right" style="width:15%">
                                    <asp:Label ID="nextServDateLab" runat="server" Text="Label"></asp:Label></td>
                                <td style="width: 30%">
                                    <asp:TextBox ID="nextServDateBox"  Width="80%" runat="server" CssClass="textborder" ReadOnly="True"></asp:TextBox></td>
                                <td style="width: 10%">
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td style="width:15%">
                                </td>
                                <td style="width: 30%">
                                </td>
                                <td align="right" style="width:15%">
                                    <asp:Label ID="SMSRemLab" runat="server" Text="Label"></asp:Label></td>
                                <td style="width: 30%">
                                    <asp:DropDownList ID="SMSRemDrpList"  Width="80%" runat="server" CssClass="textborder">
                                    </asp:DropDownList></td>
                                <td style="width: 10%;">
                                    <%--<asp:LinkButton ID="contractHistroylkBtn" runat="server" Height="17px" CausesValidation="False">Link</asp:LinkButton>
                                    --%>
                                    <asp:label ID="contractHistroylkBtn" runat="server" Height="17px" CssClass ="cursor" onclick="ContractHistoryClick()">Link</asp:label>
                                    
                                    </td>
                                    
                                    
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width:15%">
                                    <asp:Label ID="Label2" runat="server" Text="Cardholder's Name"></asp:Label></td>
                                <td style="width: 30%">
                                    <asp:TextBox ID="txtCCName" runat="server" CssClass="textborder"
                                        Width="80%" style="text-transform:uppercase;"></asp:TextBox>
                                    <%--<asp:DropDownList ID="cntStatusDrpList" runat="server" CssClass="textborder">
                                    </asp:DropDownList>--%>
                                </td>
                                <td align="right" style="width:15%">
                                    <asp:Label ID="Label3" runat="server" Text="Card Number"></asp:Label></td>
                                <td style="width: 30%">
                                    <asp:TextBox ID="txtCCNumber" runat="server" CssClass="textborder"
                                        Width="80%" MaxLength="16"></asp:TextBox>&nbsp;
                                    <%--<asp:DropDownList ID="cntStatusDrpList" runat="server" CssClass="textborder">
                                    </asp:DropDownList>--%></td>
                                <td style="width: 10%">
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width:15%">
                                    <asp:Label ID="Label4" runat="server" Text="Initial Amount"></asp:Label></td>
                                <td style="width: 30%">
                                    <asp:TextBox ID="txtInitialAmt" runat="server" CssClass="textborder"
                                        Width="80%">0.00</asp:TextBox>
                                    <%--<asp:DropDownList ID="cntStatusDrpList" runat="server" CssClass="textborder">
                                    </asp:DropDownList>--%>
                                </td>
                                <td align="right" style="width:15%">
                                    <asp:Label ID="Label5" runat="server" Text="Card Expiry"></asp:Label></td>
                                <td style="width: 30%">
                                    <asp:TextBox ID="txtCCExpiry" runat="server" CssClass="textborder" MaxLength="4" Width="80%">MMYY</asp:TextBox>
                                    <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="txtCCExpiry"
                                        Display="Dynamic" ErrorMessage="must be number only! format MMYY" Operator="DataTypeCheck"
                                        Type="Integer"></asp:CompareValidator>
                                    <%--<asp:DropDownList ID="cntStatusDrpList" runat="server" CssClass="textborder">
                                    </asp:DropDownList>--%>
                                </td>
                                <td style="width: 10%">
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width:15%">
                                    <asp:Label ID="Label6" runat="server" Text="Monthly Amount"></asp:Label></td>
                                <td style="width: 30%">
                                    <asp:TextBox ID="txtMonthlyAmt" runat="server" CssClass="textborder"
                                        Width="80%">0.00</asp:TextBox>
                                    <%--<asp:DropDownList ID="cntStatusDrpList" runat="server" CssClass="textborder">
                                    </asp:DropDownList>--%>
                                </td>
                                <td align="right" style="width: 15%">
                                    <asp:Label ID="Label7" runat="server" Text="Card Type"></asp:Label></td>
                                <td style="width: 30%"><asp:DropDownList ID="ddlcardtype"  Width="80%" runat="server" CssClass="textborder">
                                    <asp:ListItem>Credit</asp:ListItem>
                                    <asp:ListItem>Debit</asp:ListItem>
                                </asp:DropDownList></td>
                                <td style="width: 10%">
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width: 15%">
                                </td>
                                <td style="width: 30%">
                                </td>
                                <td align="right" style="width: 15%">
                                    <asp:Label ID="Label8" runat="server" Text="Bank Name"></asp:Label></td>
                                <td style="width: 30%">
                                    <asp:DropDownList ID="ddlbankname"  Width="80%" runat="server" CssClass="textborder">
                                    </asp:DropDownList></td>
                                <td style="width: 10%">
                                </td>
                            </tr>
                        </table>
                        <table cellspacing="1" cellpadding="2" bgcolor="#b7e6e6" border="0" style="width: 100%">
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width:15%;">
                                    <asp:Label ID="notesLab" runat="server" Text="Label"></asp:Label></td>
                                <td style="width: 85%; height: 95px;">
                                    <asp:TextBox ID="notesBox" runat="server" CssClass="textborder" Width="80%" Height="82px"
                                        MaxLength="400"></asp:TextBox></td>
                            </tr>
                        </table>
                        <asp:GridView ID="cntServInfoView" runat="server" Width="100%" AllowSorting="True"
                            Font-Size="Smaller" AutoGenerateColumns="False">
                            <Columns>
                                <asp:CommandField ShowEditButton="True" />
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Label ID="NoLab" runat="server" Width="60%" ReadOnly="true" Text='<%# Bind("No") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Label ID="MTHYRLab" runat="server" Width="60%" ReadOnly="true" Text='<%# Bind("FCU2_MTHYR") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:TextBox ID="ActualServDateBox" runat="server" Width="65%" Text='<%# Bind("FCU2_ASVDT") %>' MaxLength="10"></asp:TextBox>
                                        <%--<asp:ImageButton ID="ActualServDateImg" Width="18%" CommandName="ImagBtn" Enabled="false"
                                            runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif" />
                                        <asp:Calendar ID="ActualServDateDateCalendar" OnSelectionChanged="ActualServDateDateCalendar_SelectionChanged"
                                            runat="server" Visible="false" Style="z-index: 1;" BackColor="White" BorderColor="#3366CC"
                                            BorderWidth="1px" CellPadding="1" DayNameFormat="Shortest" Font-Names="Verdana"
                                            Font-Size="8pt" ForeColor="#003399" Height="56px" Width="120px">
                                            <SelectedDayStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                                            <TodayDayStyle BackColor="#99CCCC" ForeColor="White" />
                                            <SelectorStyle BackColor="#99CCCC" ForeColor="#336666" />
                                            <WeekendDayStyle BackColor="#CCCCFF" />
                                            <OtherMonthDayStyle ForeColor="#999999" />
                                            <NextPrevStyle Font-Size="8pt" ForeColor="#CCCCFF" />
                                            <DayHeaderStyle BackColor="#99CCCC" ForeColor="#336666" Height="1px" />
                                            <TitleStyle BackColor="#003399" BorderColor="#3366CC" BorderWidth="1px" Font-Bold="True"
                                                Font-Size="10pt" ForeColor="#CCCCFF" Height="25px" />
                                        </asp:Calendar>--%>
                                        <cc2:JCalendar ID="ActualServDateDateCalendar" runat="server" Enabled="false" ImgURL="~/PresentationLayer/graph/calendar.gif" OnSelectedDateChanged="ActualServDateDateCalendar_SelectionChanged" />
                                        <asp:CompareValidator ID="CompDateVal" runat="server" ControlToValidate="ActualServDateBox"
                                            Operator="DataTypeCheck" SetFocusOnError="True" Type="Date"></asp:CompareValidator>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:TextBox ID="tansTypeBox" runat="server" Width="75%" ReadOnly="true" MaxLength="10"
                                            Text='<%# Bind("FCU2_TRNTY") %>'></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:TextBox ID="tansNoBox" runat="server" Width="75%" ReadOnly="true" MaxLength="10"
                                            Text='<%# Bind("FCU2_TRNNO") %>'></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:DropDownList ID="cnt2StatusDrpList" Width="60px" SelectedValue='<%# Bind("FCU2_STAT") %>'
                                            runat="server" CssClass="textborder">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <asp:LinkButton ID="saveButton" runat="server"></asp:LinkButton>&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:HyperLink ID="cancelLink" runat="server" NavigateUrl="~/PresentationLayer/function/contract.aspx">[cancelLink]</asp:HyperLink>
                    </td>
                </tr>
                <tr>
                    <td style="font-family: Verdana; width: 729px;">
                    </td>
                </tr>
               <tr bgcolor="#ffffff">
<td colspan = 5>
                        	&nbsp;
       	</td>
</tr>  
               <tr bgcolor="#ffffff">
	<td colspan = 5>
                            <asp:Label ID="lblNoteDown" runat="server" ForeColor="Red" Text="Label"></asp:Label>
	</td>
</tr>

                
            </table>
        </div>
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
            ShowSummary="False" />
        <cc1:MessageBox ID="saveMsgBox" runat="server"></cc1:MessageBox>       
        <cc1:MessageBox ID="InfoMsgBox" runat="server" />
        <cc1:MessageBox ID="UpdateMsgBox" runat="server" />
    </form>
</body>
</html>
