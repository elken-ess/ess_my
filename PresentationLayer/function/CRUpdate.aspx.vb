Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.data
Partial Class PresentationLayer_Function_CRUpdate
    Inherits System.Web.UI.Page
    Dim strUser As String = "Test"
    Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        If Not Page.IsPostBack Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try
            Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CR-00")
            CRlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CR-01")
            aclab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CR-02")
            inslab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CR-04")
            contractlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-CONTRACTNO")
            'Amended by KSLim on 18/06/2006
            custPfLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-CUSTPRX")
            custIDLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CR-09")
            'Amended by KSLim on 18/06/2006

            'Amended by KSLim on 17/06/2006
            'modletypelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CR-05")
            modletypelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0006")
            'Amended by KSLim on 17/06/2006
            remarkslab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CR-10")
            freelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CR-07")
            savebtn.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CR-11")
            deletebtn.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CR-12")
            sbtlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-01")
            invoicelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-02")
            datelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-03")
            scenterlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-04")
            technicianlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-05")
            sblab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-06")
            stlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-07")
            paylab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-payment-00")
            cashlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "CS")
            creditlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "CR")
            chequelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "CQ")
            other1lab.Text = objXmlTr.GetLabelName("EngLabelMsg", "OP")
            other2lab.Text = objXmlTr.GetLabelName("EngLabelMsg", "CC")
            total1lab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-payment-06")
            staLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCTRY-0004")
            Me.lblStatus.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-STATUS")
            totalLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-part-10")
            discountLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-part-09")
            totalLab2.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-part-10")
            InvoiceTaxLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-part-11")

            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            rfvRO.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRRO")

            lblNoteUP.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            lblNoteDown.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            '--------------------------------------------------------------------------------
            Dim transType As String = Request.QueryString("type").ToString()
            Dim transNo As String = Request.QueryString("no").ToString()

            '--------------------------------------------------------------------------------
            '绑定本页的六个dropdownlist,第一个为状态downlist
            '--------------------------------------------------------------
            Dim cntryStat As New clsUtil()
            Dim statParam As ArrayList = New ArrayList
            statParam = cntryStat.searchconfirminf("SerStat")
            Dim count As Integer
            Dim statid As String
            Dim statnm As String
            For count = 0 To statParam.Count - 1
                statid = statParam.Item(count)
                statnm = statParam.Item(count + 1)
                count = count + 1
                If UCase(statid) = "UA" Or UCase(statid) = "UM" Then
                    ctryStatDrop.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                End If
            Next
            'ctryStatDrop.Items(0).Selected = True
            '-------------------------------------

            Dim modelty As New clsupdateservice2()
            Dim ds As New DataSet
            ds = modelty.selds(transType, transNo)

            Dim i As Integer
            'Amended by KSLim on 17/06/2006
            'For i = 0 To ds.Tables(0).Rows.Count - 1
            '    modelTypedrop.Items.Add(ds.Tables(0).Rows(i).Item(0).ToString)
            'Next
            'modelTypedrop.Items(0).Selected = True

            If ds.Tables.Count <> 0 Then
                databondros(ds, Me.modelTypedrop)
            End If

            Me.modelTypedrop.Items.Insert(0, "")
            Dim rotypestr As String
            rotypestr = Me.modelTypedrop.Text

            Me.rouid.Text = modelTypedrop.SelectedItem.Value
            'Dim ro As Array
            'ro = rotypestr.Split("-")
            'If ro.Length > 1 Then
            '    Dim roid As New clsAddress()
            '    roid.ModelType = ro(0)
            '    roid.SerialNo = ro(1)
            '    Dim rods As New DataSet
            '    rods = roid.GetROID()
            '    Me.rouid.Text = rods.Tables(0).Rows(0).Item(0).ToString()

            'End If
            'Amended by KSLim on 17/06/2006

            'Amended by KSLim on 18/06/2006
            'For i = 0 To ds.Tables(1).Rows.Count - 1
            '    freelist.Items.Add(ds.Tables(1).Rows(i).Item(0).ToString)
            'Next
            'freelist.Items(0).Selected = True

            statParam = cntryStat.searchconfirminf("YESNO")
            For count = 0 To statParam.Count - 1
                statid = statParam.Item(count)
                statnm = statParam.Item(count + 1)
                count = count + 1
                freelist.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
            Next
            'Amended by KSLim on 18/06/2006

            For i = 0 To ds.Tables(2).Rows.Count - 1
                sbtyDropDownList.Items.Add(ds.Tables(2).Rows(i).Item(0).ToString)
            Next
            sbtyDropDownList.Items(0).Selected = True
            For i = 0 To ds.Tables(3).Rows.Count - 1
                technicianDropDownList.Items.Add(ds.Tables(3).Rows(i).Item(0).ToString)
            Next
            technicianDropDownList.Items(0).Selected = True
            For i = 0 To ds.Tables(4).Rows.Count - 1
                stDropDownList.Items.Add(ds.Tables(4).Rows(i).Item(0).ToString)
            Next
            stDropDownList.Items(0).Selected = True
            ''--------------------------------------文本框显示
            If ds.Tables(5).Rows.Count > 0 Then
                actionText.Text = ds.Tables(5).Rows(0).Item(0).ToString()
                installbox.Text = ds.Tables(5).Rows(0).Item(1).ToString()
                contractbox.Text = ds.Tables(5).Rows(0).Item(2).ToString()
                customerBox.Text = ds.Tables(5).Rows(0).Item(3).ToString()
                'serialNobox.Text = ds.Tables(5).Rows(0).Item(4).ToString()
                remarkbox.Text = ds.Tables(5).Rows(0).Item(5).ToString()
                invoicebox.Text = ds.Tables(5).Rows(0).Item(6).ToString()
                scenterbox.Text = ds.Tables(5).Rows(0).Item(7).ToString()
                sbBox.Text = ds.Tables(5).Rows(0).Item(8).ToString()
                'Amended by KSLim on 18/6/2006
                totalBox1.Text = ds.Tables(5).Rows(0).Item(10).ToString()
                discountBox.Text = ds.Tables(5).Rows(0).Item(11).ToString()
                'totaBox2.Text = ds.Tables(5).Rows(0).Item(12).ToString()
                InvoiceTaxBox.Text = ds.Tables(5).Rows(0).Item(12).ToString()
                totaBox2.Text = (Convert.ToDouble(totalBox1.Text) - Convert.ToDouble(discountBox.Text)).ToString()
                totaBox2.Text = String.Format("{0:#0.00}", CType(totaBox2.Text, Decimal))

                'InvoiceTaxBox.Text = ds.Tables(5).Rows(0).Item(13).ToString()
                sbtyDropDownList.Text = ds.Tables(5).Rows(0).Item(14).ToString()
                invoicelabtext.Text = ds.Tables(5).Rows(0).Item(15).ToString()
                technicianDropDownList.Text = ds.Tables(5).Rows(0).Item(16).ToString()
                stDropDownList.Text = ds.Tables(5).Rows(0).Item(17).ToString()
                rouid.Text = ds.Tables(5).Rows(0).Item(18).ToString()

                Me.ctryStatDrop.SelectedValue = ds.Tables(5).Rows(0).Item("FIV1_STAT")
                If ds.Tables(5).Rows(0).Item("FIV1_STAT") = "UA" Then
                    ctryStatDrop.Enabled = False
                    savebtn.Enabled = False
                Else
                    ctryStatDrop.SelectedIndex = 0
                    ctryStatDrop.Enabled = True
                    savebtn.Enabled = True
                End If


                If Val(rouid.Text) <> 0 Then
                    modelTypedrop.Text = ds.Tables(5).Rows(0).Item(19).ToString() & "-" & ds.Tables(5).Rows(0).Item(20).ToString()
                End If

                If modelTypedrop.Text <> "" Then
                    modelTypedrop.Enabled = False
                End If

                Me.custPfbox.Text = ds.Tables(5).Rows(0).Item(21).ToString()

                Dim lstrFreeServiceFlag As String = ds.Tables(5).Rows(0).Item(22).ToString()
                If lstrFreeServiceFlag = "" Then
                    freelist.Enabled = True
                    Me.freelist.SelectedValue = "N"
                Else
                    freelist.Enabled = False
                    Me.freelist.SelectedValue = lstrFreeServiceFlag
                End If
                'Amended by KSLim on 18/6/2006
            Else : Dim objXm As New clsXml
                objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                errlab.Text = objXm.GetLabelName("StatusMessage", "NoData")
                errlab.Visible = True
            End If

            ''----------------------显示支付类型
            Dim money As New clsupdateservice2()
            Dim tab As DataTable = money.searchmoney(transType, transNo)
            For j As Integer = 0 To tab.Rows.Count - 1
                If tab.Rows(j).ItemArray(0).Equals("CS") Then
                    cashtext.Text = tab.Rows(j).ItemArray(1).ToString()
                End If
                If tab.Rows(j).ItemArray(0).Equals("CQ") Then
                    chequeText.Text = tab.Rows(j).ItemArray(1).ToString()
                End If
                If tab.Rows(j).ItemArray(0).Equals("CR") Then
                    creditText.Text = tab.Rows(j).ItemArray(1).ToString()
                End If
                If tab.Rows(j).ItemArray(0).Equals("CC") Then
                    creditcard.Text = tab.Rows(j).ItemArray(1).ToString()
                End If
                If tab.Rows(j).ItemArray(0).Equals("OP") Then
                    optext.Text = tab.Rows(j).ItemArray(1).ToString()
                End If
            Next
            '-----------------------显示计算付账总和，为五个支付方式相加
            Dim dTotal As Double

            If (cashtext.Text <> "") Then
                dTotal = dTotal + Convert.ToDouble(cashtext.Text)
            End If

            If (creditText.Text <> "") Then
                dTotal = dTotal + Convert.ToDouble(creditText.Text)
            End If

            If (chequeText.Text <> "") Then
                dTotal = dTotal + Convert.ToDouble(chequeText.Text)
            End If
            If (optext.Text <> "") Then
                dTotal = dTotal + Convert.ToDouble(optext.Text)
            End If
            If (creditcard.Text <> "") Then
                dTotal = dTotal + Convert.ToDouble(creditcard.Text)
            End If
            paytotalBox.Text = dTotal.ToString
            paytotalBox.Text = String.Format("{0:#0.00}", CType(paytotalBox.Text, Decimal))

            '--------------------------------------------------------
            'DATAGRIDVIEW显示编辑零件
            '--------------------------------------------------------
            Dim partEntity As New clsupdateservice2()
            Dim partall As DataSet = partEntity.GetPartaCR(transType, transNo)
            partGridView.DataSource = partall.Tables(6)
            Session("partView") = partall
            partGridView.AllowPaging = True
            partGridView.DataBind()
            If partall.Tables(6).Rows.Count >= 1 Then
                partGridView.HeaderRow.Cells(0).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-part-01")
                partGridView.HeaderRow.Cells(0).Font.Size = 8
                partGridView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-part-02")
                partGridView.HeaderRow.Cells(1).Font.Size = 8
                partGridView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-part-03")
                partGridView.HeaderRow.Cells(2).Font.Size = 8
                partGridView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-part-04")
                partGridView.HeaderRow.Cells(3).Font.Size = 8
                partGridView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-part-05")
                partGridView.HeaderRow.Cells(4).Font.Size = 8
                partGridView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-part-06")
                partGridView.HeaderRow.Cells(5).Font.Size = 8
                partGridView.HeaderRow.Cells(6).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-part-07")
                partGridView.HeaderRow.Cells(6).Font.Size = 8

            End If
        End If
    End Sub
    Protected Sub ImageButton2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton2.Click
        Calendar2.Visible = True
        Calendar2.Attributes.Add("style", " POSITION: absolute")
    End Sub
    Protected Sub Calendar2_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendar2.SelectionChanged
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        installbox.Text = Calendar2.SelectedDate.Date
        Calendar2.Visible = False
    End Sub
    Protected Sub savebtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles savebtn.Click
       
        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim msginfo As String = objXm.GetLabelName("StatusMessage", "SAVEORNOT")
        Dim msgtitle As String = objXm.GetLabelName("StatusMessage", "SAVETITLE")
        txtSTAT.Text = Me.ctryStatDrop.SelectedValue
        ' msginfo = Me.ctryStatDrop.SelectedItem.Text
        MessageBox1.Confirm(msginfo)

    End Sub

    'Amended by KSLim on 18/06/2006
    'Protected Sub customerbtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles customerbtn.Click
    'Panel1.Visible = Not Panel1.Visible
    'End Sub
    'Amended by KSLim on 18/06/2006

    'Amended by KSLim on 17/06/2006
    Public Function databondros(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        Dim row As DataRow
        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            NewItem.Text = row("id") & "-" & row("name")
            'NewItem.Value = row("id") & "-" & row("name")
            NewItem.Value = row("ROUID")
            dropdown.Items.Add(NewItem)
        Next
    End Function

    Protected Sub modelTypedrop_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles modelTypedrop.SelectedIndexChanged
        'If Me.rotype.SelectedIndex <> 0 Then
        Dim rotypestr As String
        rotypestr = Me.modelTypedrop.Text
        'Dim ro As Array
        'ro = rotypestr.Split("-")
        'Dim roid As New clsAddress()
        'roid.ModelType = ro(0)
        'roid.SerialNo = ro(1)
        'SerialNo.Text = roid.SerialNo
        'Dim rods As New DataSet
        'rods = roid.GetROID()
        'Me.rouid.Text = rods.Tables(0).Rows(0).Item(0).ToString()

        Dim valid As Boolean
        valid = validatefreeservice()
    End Sub
    'Amended by KSLim on 17/06/2006

    Protected Sub JCalendar1_SelectedDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles JCalendar1.SelectedDateChanged
        errlab.Visible = False
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        installbox.Text = JCalendar1.SelectedDate
        Dim clsSBU As New clsServiceBillUpdate
        clsSBU.ROInstallDate = installbox.Text
        'clsSBU.ROSerialNo = rouid.Text
        clsSBU.ROSerialNo = Me.modelTypedrop.SelectedItem.Value

        Dim chkInsDate As Integer = 0
        chkInsDate = clsSBU.CheckROProductionDate()

        If chkInsDate >= 1 Then
            Dim objXm As New clsXml
            objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            errlab.Text = objXm.GetLabelName("StatusMessage", "BB-HintMsg-COMPAREPROINSDATE")
            errlab.Visible = True
            Exit Sub
        End If
    End Sub

    Protected Sub freelist_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles freelist.SelectedIndexChanged
        Dim valid As Boolean
        valid = validatefreeservice()
    End Sub

    Private Function validatefreeservice() As Boolean
        validatefreeservice = True
        If freelist.Enabled = True Then
            If freelist.SelectedValue = "Y" Then
                If Trim(modelTypedrop.SelectedItem.Value) = "" Then
                    Dim objXm As New clsXml
                    objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    errlab.Text = objXm.GetLabelName("StatusMessage", "SBA-ERRRO")
                    errlab.Visible = True
                    Return False
                Else
                    Dim clsInstallBase As New ClsInstalledBase
                    Dim dr As SqlDataReader
                    'clsInstallBase.RoID = Me.rouid.Text
                    clsInstallBase.RoID = modelTypedrop.SelectedItem.Value
                    dr = clsInstallBase.GetinstallbaseDetailsByRoID

                    If dr.Read() Then
                        If UCase(IIf(IsDBNull(dr.Item("MROU_FRSVE")), "", dr.Item("MROU_FRSVE"))) = "N" Then
                            Dim objXm As New clsXml
                            objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                            errlab.Text = objXm.GetLabelName("StatusMessage", "SBU2-ERRFRSVENO") & " " & modelTypedrop.SelectedItem.Text
                            errlab.Visible = True
                            Return False
                        End If
                    End If
                End If
            End If
        End If
    End Function


    Protected Sub MessageBox1_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox1.GetMessageBoxResponse

        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
            'Dim x As String
            'x = Me.ctryStatDrop.SelectedItem.Text
            SaveServiceBill2()
        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
            Exit Sub
        End If

    End Sub

    Private Sub SaveServiceBill2()
        Dim chkInsDate As Integer
        Dim clsSBU As New clsServiceBillUpdate
        clsSBU.ROInstallDate = installbox.Text
        clsSBU.ROSerialNo = modelTypedrop.SelectedItem.Value

        chkInsDate = clsSBU.CheckROProductionDate()

        If chkInsDate >= 1 Then
            Dim objXm As New clsXml
            objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            errlab.Text = objXm.GetLabelName("StatusMessage", "BB-HintMsg-COMPAREPROINSDATE")
            errlab.Visible = True
            Exit Sub
        End If

        If validatefreeservice() = False Then Exit Sub

        Dim CREntity As New clsupdateservice2()
        Dim transType As String = Request.QueryString("type").ToString()
        Dim transNo As String = Request.QueryString("no").ToString()
        If (stDropDownList.SelectedItem.Value.ToString() <> "") Then
            CREntity.ServiceType = stDropDownList.SelectedItem.Value.ToString()
        End If
        If (Trim(actionText.Text) <> "") Then
            CREntity.ActionTaken = actionText.Text.ToUpper()
        End If
        If (Trim(installbox.Text) <> "") Then
            CREntity.InstallDate = installbox.Text.ToUpper()
        End If
        If (Trim(contractbox.Text) <> "") Then
            CREntity.ContractID = contractbox.Text.ToUpper()
        End If
        If (modelTypedrop.SelectedItem.Value.ToString() <> "") Then
            'Amended by KSLim on 18/6/2006
            'CREntity.Modlty = modelTypedrop.SelectedItem.Value.ToString()
            Dim ROModelStr As String = modelTypedrop.SelectedItem.Value.ToString()
            CREntity.ROUnitID = ROModelStr
            'Dim ro As Array
            'ro = Split(ROModelStr, "-")
            'If ro.Length > 1 Then
            '    CREntity.Modlty = Trim(ro(0))
            '    CREntity.SerialNo = Trim(ro(1))
            '    'Amended by KSLim on 18/6/2006
            'End If

        End If
        If (freelist.SelectedItem.Value.ToString() <> "") Then
            CREntity.FreeService = freelist.SelectedItem.Value.ToString()
        End If
        'Amended by KSLim on 18/6/2006
        'If (Trim(custPfbox.Text) <> "") Then
        'CREntity.SerialNo = custPfbox.Text.ToUpper()
        'End If
        'Amended by KSLim on 18/6/2006
        If (ctryStatDrop.SelectedItem.Value.ToString() <> "") Then
            'CREntity.STATUS = ctryStatDrop.SelectedValue
            CREntity.STATUS = txtSTAT.Text

        End If
        If (Trim(remarkbox.Text) <> "") Then
            CREntity.Remark = remarkbox.Text.ToUpper()
        End If
        'Amended by KSLim on 17/06/2006
        'If (Trim(rouid.Text) <> "") Then
        '    CREntity.ROUnitID = rouid.Text.ToUpper()
        'End If
        CREntity.IPAddress = Request.UserHostAddress
        'Amended by KSLim on 17/06/2006

        'Amended by KSLim on 18/6/2006
        CREntity.ModifyBy = Session("userID").ToString().ToUpper
        'Amended by KSLim on 18/6/2006

        Dim updCtryCnt As Integer = CREntity.Update1(transType, transNo)
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        If updCtryCnt = 0 Then
            Dim objXm As New clsXml
            objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            errlab.Text = objXm.GetLabelName("StatusMessage", "BB-HintMsg-Suc")
            errlab.Visible = True

            Me.ctryStatDrop.SelectedValue = txtSTAT.Text
            If Me.txtSTAT.Text = "UA" Then
                Me.ctryStatDrop.Enabled = False
                Me.savebtn.Enabled = False
            End If

        Else
            Response.Redirect("~/PresentationLayer/Error.aspx")
        End If
    End Sub
End Class
