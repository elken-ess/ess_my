﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="appointmenthistorydetail.aspx.vb"
    Inherits="PresentationLayer_function_appointmenthistorydetail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Appointment history</title>
    <link href="../css/style.css" type="text/css" rel="stylesheet">
</head>
<body>
    <form id="form1" runat="server">
        <div style="vertical-align: top">
            <table style="width: 100%" border="0">
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0" width="100%" border="0">
                            <tr>
                                <td width="1%" background="../graph/title_bg.gif">
                                    <img height="24" src="../graph/title1.gif" width="5"></td>
                                <td class="style2" width="98%" background="../graph/title_bg.gif">
                                    <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="right" background="../graph/title_bg.gif" style="width: 1%">
                                    <img height="24" src="../graph/title_2.gif" width="5"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0" bgcolor="#b7e6e6" border="0" style="width: 100%">
                            <tr bgcolor="#ffffff" style="width: 80%">
                                <td style="width: 85px">
                                    <asp:Label ID="custIDLab" runat="server" Text="Label"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="custIDBox" runat="server" Style="width: 90%" CssClass="textborder"
                                        ReadOnly="True"></asp:TextBox>
                                </td>
                                <td style="width: 71px">
                                    <asp:Label ID="custPfLab" runat="server" Text="Label"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="custPfBox" runat="server" Style="width: 50%" CssClass="textborder"
                                        ReadOnly="True"></asp:TextBox>
                                </td>
                                <td style="width: 59px">
                                    <asp:Label ID="custNameLab" runat="server" Text="Label"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="custNameBox" runat="server" Style="width: 90%" CssClass="textborder"
                                        ReadOnly="True"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                        <asp:GridView ID="DetailView" runat="server" Width="100%" AllowPaging="True" AllowSorting="True"
                            Font-Size="Smaller" Style="text-align: right">
                        </asp:GridView>
                    </td>
                </tr>
            </table>
            <asp:Label ID="backLink" runat="server" Text="Label" Style="color: blue; text-decoration: underline; cursor : hand"></asp:Label>
        </div>
    </form>
</body>
</html>
