<%@ Page Language="VB" AutoEventWireup="false" CodeFile="selectcustomer.aspx.vb"
    Inherits="PresentationLayer_masterrecord_CustomerRecord" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <meta http-equiv="Content-Type" content="text/html; charset=gb2312">
    <link href="../css/style.css" type="text/css" rel="stylesheet">
    <style type="text/css">A:link { COLOR: #336666; TEXT-DECORATION: none }
	BODY { FONT-SIZE: 9pt; FONT-FAMILY: "Arial", "Verdana", "Tahoma"; BACKGROUND-COLOR: #ffffff }
	TD { FONT-SIZE: 9pt; FONT-FAMILY: Arial,Verdana,Tahoma }
	</style>
</head>
<body style="width: 100%">
    <form id="form1" runat="server">
        <div>
            <table border="0" cellpadding="0" cellspacing="0" style="width: 178%">
                <tr>
                    <td background="../graph/title_bg.gif" width="1%">
                        <img height="24" src="../graph/title1.gif" width="5" /></td>
                    <td background="../graph/title_bg.gif" class="style2" style="width: 100%">
                        <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
                    <td align="right" background="../graph/title_bg.gif" style="width: 1%">
                        <img height="24" src="../graph/title_2.gif" width="5" /></td>
                </tr>
            </table>
            <table border="0" cellpadding="0" cellspacing="0" style="width: 178%">
                <tr>
                    <td background="../graph/title_bg.gif" style="width: 1%">
                    </td>
                    <td background="../graph/title_bg.gif" class="style2" width="98%">
                        <asp:Label ID="addinfo" runat="server" ForeColor="Red" Text="Label" Visible="False"></asp:Label></td>
                </tr>
            </table>
            <table id="countrytab" border="0" style="width: 100%">
                <tr>
                    <td width=" 90%" style="width: 100%;">
                        <table id="country" bgcolor="#b7e6e6" border="0" cellpadding="2" cellspacing="1"
                            style="width: 100%">
                            <tr bgcolor="#ffffff">
                                <td align="right" width="15%" style="width: 15%;">
                                    <asp:Label ID="customerIDlab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 35%;">
                                    <asp:TextBox ID="customerIDbox" runat="server" Font-Size="Small" CssClass="textborder"
                                        MaxLength="10"></asp:TextBox></td>
                                <td align="right" width="15%" style="width: 15%;">
                                    <asp:Label ID="areaIDlab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 35%;">
                                    <asp:DropDownList ID="areaID" runat="server" Width="152px" CssClass="textborder">
                                    </asp:DropDownList></td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" width="15%" style="width: 15%">
                                    <asp:Label ID="customernamelab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 35%">
                                    <asp:TextBox ID="customernamebox" runat="server" Font-Size="Small" CssClass="textborder"
                                        MaxLength="50"></asp:TextBox></td>
                                <td align="right" width="15%" style="width: 15%">
                                    <asp:Label ID="contactlab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 35%">
                                    <asp:TextBox ID="contactbox" runat="server" Font-Size="Small" CssClass="textborder"
                                        MaxLength="30"></asp:TextBox></td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" width="15%" style="width: 15%">
                                    <asp:Label ID="roserialnolab" runat="server" Text="Label"></asp:Label></td>
                                <td align="left" style="width: 35%">
                                    <asp:TextBox ID="roserialnobox" runat="server" Font-Size="Small" CssClass="textborder"
                                        MaxLength="10"></asp:TextBox></td>
                                <td align="right" width="15%" style="width: 15%">
                                    <asp:Label ID="ctryStat" runat="server" Text="Label"></asp:Label>&nbsp;
                                </td>
                                <td align="left" style="width: 35%">
                                    <asp:DropDownList ID="ctryStatDrop" runat="server" Width="152px">
                                    </asp:DropDownList>
                                    <asp:LinkButton ID="searchButton" runat="server">search</asp:LinkButton>
                            </tr>
                        </table>
                        <hr />
                        <asp:GridView ID="ctryView" runat="server" AllowPaging="True" AllowSorting="True"
                            Font-Size="Smaller" Width="100%">
                        </asp:GridView>
                    </td>
                </tr>
            </table>
            <asp:Label ID="Label1" runat="server" ForeColor="Red" Height="62px" Text="Label"
                Width="638px"></asp:Label>&nbsp;<br />
            &nbsp;&nbsp;
            <br />
        </div>
        &nbsp;&nbsp; &nbsp;
    </form>
</body>
</html>
