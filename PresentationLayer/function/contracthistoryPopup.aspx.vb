﻿Imports BusinessEntity
Imports System.Data
Partial Class PresentationLayer_function_ContractHistoryPopup
    Inherits System.Web.UI.Page
    Dim objXmlTr As New clsXml
    Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
    Dim apptEntity As New clsAppointment()
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            Me.titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-CONTRACTHISTORY")
            Me.ROInfoLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-ROINFO")
            Me.custNameLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-CUSTNAME")
            Me.backLink.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-BACK")
            'backLink.Attributes("onclick") = "javascript:history.back();"
            Me.ROInfoBox.Text = Request.Params("ROINFO").ToString()
            Me.custNameBox.Text = Request.Params("custName").ToString()
            BindGrid()
        ElseIf ContractHistoryView.Rows.Count > 0 Then
            DispGridViewHead()
        End If
    End Sub
    Protected Sub BindGrid()
        apptEntity.ROID = Request.Params("ROID").ToString()
        apptEntity.CustomerID = Request.Params("custID").ToString()
        apptEntity.CustomerPrifx = Request.Params("custPf").ToString()
        Dim ds As DataSet = apptEntity.GetContractHistory()


        If ds Is Nothing Then
            Response.Redirect("~/PresentationLayer/error.aspx")
        End If
        Session("ContractHistoryView") = ds
        Me.ContractHistoryView.DataSource = ds
        ContractHistoryView.AllowPaging = True
        ContractHistoryView.AllowSorting = True
        Me.ContractHistoryView.DataBind()
        If ds.Tables(0).Rows.Count > 0 Then
            DispGridViewHead()
        End If
    End Sub
    Private Sub DispGridViewHead()
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        ContractHistoryView.HeaderRow.Cells(0).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CNT-CNTNO")
        ContractHistoryView.HeaderRow.Cells(0).Font.Size = 8
        ContractHistoryView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CNT-CUSTID")
        ContractHistoryView.HeaderRow.Cells(1).Font.Size = 8
        ContractHistoryView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CNT-SVCTY")
        ContractHistoryView.HeaderRow.Cells(2).Font.Size = 8
        ContractHistoryView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CNT-CNTSDATE")
        ContractHistoryView.HeaderRow.Cells(3).Font.Size = 8
        ContractHistoryView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CNT-CNTEDATE")
        ContractHistoryView.HeaderRow.Cells(4).Font.Size = 8
        ContractHistoryView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CNT-SERVC")
        ContractHistoryView.HeaderRow.Cells(5).Font.Size = 8
        ContractHistoryView.HeaderRow.Cells(6).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CNT-STATUS")
        ContractHistoryView.HeaderRow.Cells(6).Font.Size = 8
    End Sub

    Protected Sub ContractHistoryView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles ContractHistoryView.PageIndexChanging
        ContractHistoryView.PageIndex = e.NewPageIndex
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Dim ds As DataSet = Session("ContractHistoryView")
        ContractHistoryView.DataSource = ds
        ContractHistoryView.DataBind()
        If ds.Tables(0).Rows.Count > 0 Then
            DispGridViewHead()
        End If
    End Sub
End Class
