Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.data
Partial Class PresentationLayer_Function_ADDUpdateServiceBillPart1
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Response.Redirect("~/PresentationLayer/logon.aspx")
                End If
            Catch ex As Exception
                Response.Redirect("~/PresentationLayer/logon.aspx")
            End Try
            Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-modifybill1")
            sbtlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-01")
            invoicelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-02")
            scenterlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-04")
            technicianLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-05")
            sblab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-06")
            stlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-07")
            paylab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-payment-00")
            cashlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "CS")
            creditlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "CR")
            chequelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "CQ")
            other1lab.Text = objXmlTr.GetLabelName("EngLabelMsg", "OP")
            other2lab.Text = objXmlTr.GetLabelName("EngLabelMsg", "CC")
            total1lab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-payment-06")
            totalLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-part-10")
            discountLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-part-09")
            totalLab2.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-part-10")
            InvoiceTaxLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-part-11")
            disRemark.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-part-12")
            generateslib.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-generate")
            savebtn.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CR-11")
            cancelbtn.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CR-12")
            lblNoteUP.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            lblNoteDown.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")

            '--------------------------------
            '绑定datalist
            '----------------------------------
            Dim transType As String = Request.QueryString("type").ToString()
            Dim transNo As String = Request.QueryString("no").ToString()

            Dim xianshi As New clsUpdateService1()
            Dim ds As New DataSet
            ds = xianshi.SelByID(transType, transNo)

            Dim i As Integer

            For i = 0 To ds.Tables(0).Rows.Count - 1
                sbtyDropDownList.Items.Add(ds.Tables(0).Rows(i).Item(0).ToString)
            Next
            sbtyDropDownList.Items(0).Selected = True

            For i = 0 To ds.Tables(1).Rows.Count - 1
                stDropDownList.Items.Add(ds.Tables(1).Rows(i).Item(0).ToString)
            Next
            stDropDownList.Items(0).Selected = True
            Dim dt As New DataSet
            dt = xianshi.chaxun()
            Dim k As Integer
            For k = 0 To dt.Tables(1).Rows.Count - 1
                technicianList.Items.Add(dt.Tables(1).Rows(k).Item(0).ToString)
            Next
            technicianList.Items(0).Selected = True
            '-----------------------------------------------------------
            '显示文本框
            '-----------------------------------------------------------
            If ds.Tables(2).Rows.Count > 0 Then
                scenterbox.Text = ds.Tables(2).Rows(0).Item(0).ToString()
                sbBox.Text = ds.Tables(2).Rows(0).Item(1).ToString()
                invoicelabtext.Text = ds.Tables(2).Rows(0).Item(2).ToString()
            Else : Dim objXm As New clsXml
                objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                errlab.Text = objXm.GetLabelName("StatusMessage", "NoData")
                errlab.Visible = True
            End If

            '----------------------------------------------------------------
            '显示GRID VIEW
            '----------------------------------------------------------------
            Dim partEntity As New clsUpdateService1()
            Dim partall As DataSet = partEntity.SelByID(transType, transNo)
            Dim cmdField As New CommandField()
            cmdField.EditText = objXmlTr.GetLabelName("EngLabelMsg", "BB-Edit")
            cmdField.UpdateText = objXmlTr.GetLabelName("EngLabelMsg", "BB-update1")
            cmdField.CancelText = objXmlTr.GetLabelName("EngLabelMsg", "BB-Cancel")
            cmdField.ShowEditButton = True
            partGridView.Columns.Insert(0, cmdField)
            partGridView.DataSource = partall.Tables(3)
            Session("addView") = partall
            partGridView.AllowPaging = True
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            partGridView.DataBind()
            If partall.Tables(3).Rows.Count >= 1 Then
                gridview()

                '-----------------
                '计算折扣和total总和()
                '-------------------
                Dim dtoal As Double
                'Dim strTax As String
                'For j As Integer = 0 To Me.partGridView.Rows.Count - 1
                '    strTax = Trim(Me.partGridView.Rows(j).Cells(6).Text)
                '    If (Not System.String.IsNullOrEmpty(strTax)) And (strTax <> Nothing) Then
                '        dtoal = dtoal + Double.Parse(Me.partGridView.Rows(j).Cells(6).Text)
                '    End If
                'Next
                For j As Integer = 0 To partall.Tables(3).Rows.Count - 1
                    dtoal = dtoal + Double.Parse(partall.Tables(3).Rows(j).Item(5))
                Next

                Me.totalBox1.Text = dtoal.ToString()
            End If
            If totalBox1.Text <> "" And discountBox.Text <> "" Then
                totaBox2.Text = Convert.ToString(Convert.ToDouble(totalBox1.Text.ToString) - Convert.ToDouble(discountBox.Text.ToString))
            Else : totaBox2.Text = totalBox1.Text
            End If
            '-------------------------------
            '显示支付类型及数额()
            '-------------------------------
            Dim money As New clsUpdateService1()
            Dim tab As DataTable = money.searchmoney(transType, transNo)
            For j As Integer = 0 To tab.Rows.Count - 1
                If tab.Rows(j).ItemArray(0).Equals("CS") Then
                    cashtext.Text = tab.Rows(j).ItemArray(1).ToString()
                End If
                If tab.Rows(j).ItemArray(0).Equals("CQ") Then
                    chequeText.Text = tab.Rows(j).ItemArray(1).ToString()
                End If
                If tab.Rows(j).ItemArray(0).Equals("CR") Then
                    creditText.Text = tab.Rows(j).ItemArray(1).ToString()
                End If
                If tab.Rows(j).ItemArray(0).Equals("CC") Then
                    creditcard.Text = tab.Rows(j).ItemArray(1).ToString()
                End If
                If tab.Rows(j).ItemArray(0).Equals("OP") Then
                    optext.Text = tab.Rows(j).ItemArray(1).ToString()
                End If
            Next
            '-----------------------显示计算付账总和，为五个支付方式相加
            Dim dTotal As Double

            If (cashtext.Text <> "") Then
                dTotal = dTotal + Convert.ToDouble(cashtext.Text)
            End If

            If (creditText.Text <> "") Then
                dTotal = dTotal + Convert.ToDouble(creditText.Text)
            End If

            If (chequeText.Text <> "") Then
                dTotal = dTotal + Convert.ToDouble(chequeText.Text)
            End If
            If (optext.Text <> "") Then
                dTotal = dTotal + Convert.ToDouble(optext.Text)
            End If
            If (creditcard.Text <> "") Then
                dTotal = dTotal + Convert.ToDouble(creditcard.Text)
            End If
            paytotalBox.Text = dTotal.ToString

            '--------------------------

        End If
    End Sub
    Protected Sub gridview()
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        partGridView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-part-01")
        partGridView.HeaderRow.Cells(1).Font.Size = 8
        partGridView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-part-02")
        partGridView.HeaderRow.Cells(2).Font.Size = 8
        partGridView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-part-03")
        partGridView.HeaderRow.Cells(3).Font.Size = 8
        partGridView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-part-04")
        partGridView.HeaderRow.Cells(4).Font.Size = 8
        partGridView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-part-05")
        partGridView.HeaderRow.Cells(5).Font.Size = 8
        partGridView.HeaderRow.Cells(6).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-part-06")
        partGridView.HeaderRow.Cells(6).Font.Size = 8
        partGridView.HeaderRow.Cells(7).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-part-07")
        partGridView.HeaderRow.Cells(7).Font.Size = 8

    End Sub
    Protected Sub partGridView_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles partGridView.SelectedIndexChanged

    End Sub

    Protected Sub savebtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles savebtn.Click

        Dim partEntity As New clsUpdateService1()

        If (Trim(sbBox.Text) <> "") Then
            partEntity.SERVICEBIL = sbBox.Text()
        End If
        If (Trim(stDropDownList.SelectedItem.Value.ToString()) <> "") Then
            partEntity.ServiceType = stDropDownList.SelectedItem.Value.ToString()
        End If
        If (Trim(technicianList.SelectedItem.Value.ToString()) <> "") Then
            partEntity.Technician = technicianList.SelectedItem.Value.ToString()
        End If

        If (Trim(discountBox.Text) <> "") Then
            partEntity.discount = discountBox.Text()
        End If

        If (Trim(totaBox2.Text) <> "") Then
            partEntity.total = totaBox2.Text()
        End If
        If (Trim(InvoiceTaxBox.Text) <> "") Then
            partEntity.InvoiceTax = InvoiceTaxBox.Text()
        End If
        If (Trim(disRemarkbox.Text) <> "") Then
            partEntity.DiscountRemark = disRemarkbox.Text()
        End If
        If (Trim(cashtext.Text) <> "") Then
            partEntity.Cash = Decimal.Parse(cashtext.Text)
        Else : partEntity.Cash = 0
        End If
        If (Trim(creditText.Text) <> "") Then
            partEntity.Credit = Decimal.Parse(creditText.Text)
        Else : partEntity.Credit = 0
        End If
        If (Trim(chequeText.Text) <> "") Then
            partEntity.cheque = Decimal.Parse(chequeText.Text)
        Else : partEntity.cheque = 0
        End If
        If (Trim(creditcard.Text) <> "") Then
            partEntity.CreditCard = Decimal.Parse(creditcard.Text)
        Else : partEntity.CreditCard = 0
        End If
        If (Trim(optext.Text) <> "") Then
            partEntity.other = Decimal.Parse(optext.Text)
        Else : partEntity.other = 0
        End If

        Dim transType As String = Request.QueryString("type").ToString()
        Dim transNo As String = Request.QueryString("no").ToString()
        '--------------------------------更新
        Dim gengxin As Integer = partEntity.update1(transType, transNo)
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        If gengxin = 0 Then
            Dim objXm As New clsXml
            objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            errlab.Text = objXm.GetLabelName("StatusMessage", "BB-HintMsg-Suc")
            errlab.Visible = True
        Else
            Response.Redirect("~/PresentationLayer/Error.aspx")
        End If
        '---------------------------------------------插入

        

        Dim insCtryCnt As Integer = partEntity.Insert(transType, transNo)
        'Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        If insCtryCnt = 0 Then
            Dim objXm As New clsXml
            objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            errlab.Text = objXm.GetLabelName("StatusMessage", "BB-HintMsg-Suc")
            errlab.Visible = True
        Else
            Dim msg As String = objXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-Fail")
            Dim msgBox As New MessageBox(Me)
            msgBox.Show(msg)
            Response.Redirect("~/PresentationLayer/Error.aspx")
        End If
        '----------------------------------------------
    End Sub

    Protected Sub partGridView_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles partGridView.RowEditing

        Dim index As Integer = partGridView.PageIndex * partGridView.PageSize + e.NewEditIndex
        'Dim TempButton As LinkButton = CType(partGridView.Rows(e.NewEditIndex).Cells(0).Controls(0), LinkButton)
        Me.partGridView.EditIndex = e.NewEditIndex
        Dim transType As String = Request.QueryString("type").ToString()
        Dim transNo As String = Request.QueryString("no").ToString()
        Dim partEntity As New clsUpdateService1()
        Dim partall As DataSet = partEntity.SelByID(transType, transNo)
        partGridView.DataSource = partall.Tables(3)
        Session("addView") = partall
        partGridView.AllowPaging = True
        partGridView.DataBind()
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        If partall.Tables(3).Rows.Count >= 1 Then
            gridview()
            Dim txt As TextBox
            txt = Me.partGridView.Rows(e.NewEditIndex).Cells(4).Controls(0)
            txt.Width = Unit.Pixel(50)
        End If
    End Sub

    Protected Sub partGridView_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles partGridView.RowUpdating
        Me.partGridView.EditIndex = -1
        Dim transType As String = Request.QueryString("type").ToString()
        Dim transNo As String = Request.QueryString("no").ToString()
        Dim partEntity As New clsUpdateService1()
        Dim quantity As TextBox, lineAmt As TextBox
        quantity = Me.partGridView.Rows(e.RowIndex).Cells(4).Controls(0)
        'lineAmt = Me.partGridView.Rows(e.RowIndex).Cells(7).Controls(0)

        partEntity.Quantity = Convert.ToInt32(quantity.Text)
        'partEntity.LineAmt = Convert.ToInt32(lineAmt.Text)
        partEntity.ModifyBy = Session.Contents("userID")

        partEntity.update(transType, transNo)
        Dim partall As DataSet = partEntity.SelByID(transType, transNo)
        partGridView.AllowPaging = True
        partGridView.DataSource = partall.Tables(3)
        partGridView.DataBind()
        gridview()
        total()
    End Sub

    Protected Sub partGridView_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles partGridView.RowCancelingEdit
        Me.partGridView.EditIndex = -1
        Dim transType As String = Request.QueryString("type").ToString()
        Dim transNo As String = Request.QueryString("no").ToString()
        Dim partEntity As New clsUpdateService1()
        Dim partall As DataSet = partEntity.SelByID(transType, transNo)
        partGridView.DataSource = partall.Tables(3)
        Session("addView") = partall
        partGridView.AllowPaging = True
        partGridView.DataBind()
        gridview()
    End Sub

 
    Protected Sub discountBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles discountBox.TextChanged
        'Dim x As Integer
        If totalBox1.Text <> "" And discountBox.Text <> "" Then
            totaBox2.Text = Convert.ToString(Convert.ToDouble(totalBox1.Text.ToString) - Convert.ToDouble(discountBox.Text.ToString))
        Else : totaBox2.Text = totalBox1.Text
        End If
        'InvoiceTaxBox = Convert.ToDouble(totaBox2.Text.ToString) * x
    End Sub


    Protected Sub total()
        Dim dtoal As Double = 0
        Dim strTax As String
        For j As Integer = 0 To Me.partGridView.Rows.Count - 1
            strTax = Trim(Me.partGridView.Rows(j).Cells(7).Text)
            If (Not System.String.IsNullOrEmpty(strTax)) And (strTax <> Nothing) Then
                dtoal = dtoal + Double.Parse(Me.partGridView.Rows(j).Cells(7).Text)
            End If
        Next
        Me.totalBox1.Text = dtoal.ToString()
    End Sub
End Class
