﻿Imports BusinessEntity
Imports System.data
Imports JCalendar
Imports System.Data.SqlClient

Partial Class PresentationLayer_function_modifycontract_aspx
    Inherits System.Web.UI.Page
    Dim objXmlTr As New clsXml
    Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
    'Dim ContractTypes As String


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.MaintainScrollPositionOnPostBack = True
        AjaxPro.Utility.RegisterTypeForAjax(GetType(PresentationLayer_function_modifycontract_aspx))
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle


        If Not Page.IsPostBack Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            servCError.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "FUN-CNT-SERVC")

            'Session("FUN-MCNT-ROID") = ""
            Session("FUN-MCNT-CNTSTAT") = ""
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-ACNT-TITL")
            custIDLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-CUSTID")
            custPfLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-CUSTPRX")
            custNameLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-CUSTNAME")
            ROInfoLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-ROINFO")
            cntTypeLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-ACNT-CNTTYPE")
            cntNoLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CNT-CNTNO")
            cntDspLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-ACNT-CNTDSP")
            lastServDateLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-LASTSERVDATE")
            installDateLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-ACNT-INSDATE")
            cntStartDateLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CNT-CNTSDATE")
            cntEndDateLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CNT-CNTEDATE")
            custContactLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-ACNT-CUSTCONTACT")
            preCntNoLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-ACNT-PRECNTNO")
            signByLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-ACNT-SIGNBY")
            cntStatusLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CNT-STATUS")
            servCLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-SERVCENTER")
            nextServDateLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-ACNT-NSERVDATE")
            SMSRemLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-ACNT-SMSREM")
            notesLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-ACNT-NOTES")
            servHistorylkBtn.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-SERVHISTORY")
            contractHistroylkBtn.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-CONTRACTHISTORY")
            saveButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Save")
            cancelLink.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Cancel")
            cancelLink2.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Cancel")
            CreatedBylab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATBY")
            CreatedDatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
            ModifiedBylab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFBY")
            ModifiedDatelab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFDATE")

            If Request.QueryString("retpage") Is Nothing Then
                cancelLink2.Enabled = False
                cancelLink2.Visible = False

            Else
                cancelLink.Enabled = False
                cancelLink.Visible = False
                cancelLink2.NavigateUrl = "ServiceBillUpdate2.aspx?type=" + Request.Params("type").ToString() + "&no=" + Request.Params("no").ToString()
            End If
            lblNoteUP.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            lblNoteDown.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")

            HypCal.NavigateUrl = "javascript:DoCal(document.form1.cntStartDateBox);"

            GetData(Trim(Request.Params("contractNo").ToString()), Trim(Request.Params("contractType").ToString()))
            Session("ContractTypes") = Trim(Request.Params("contractType").ToString())




        ElseIf cntServInfoView.Rows.Count > 0 Then
            DispGridViewHead()
        End If
        Me.errlab.Visible = False
        Me.custIDBox.ReadOnly = False
        Me.custIDBox.AutoPostBack = True

    End Sub

    Private Sub GetData(ByVal ContractNo As String, ByVal contractType As String)

        Dim cntEntity As New clsContract()
        cntEntity.ContractNO = ContractNo
        cntEntity.ContractType = contractType
        cntNoBox.Text = ContractNo
        BindGrid(Trim(cntNoBox.Text), contractType)

        Dim ds As DataSet
        If contractType <> "--" Then
            ds = cntEntity.GetContractByNO()
        Else
            ds = cntEntity.GetContractByNO2()
        End If

        If ds Is Nothing Or ds.Tables("FCU1_FIL").Rows.Count <> 1 Then
            Response.Redirect("~/PresentationLayer/error.aspx")
        End If
        custIDBox.Text = ds.Tables("FCU1_FIL").Rows(0).Item("FCU1_CUSID")
        custPfBox.Text = ds.Tables("FCU1_FIL").Rows(0).Item("FCU1_CUSPF")
        cntStartDateBox.Text = ds.Tables("FCU1_FIL").Rows(0).Item("FCU1_EFFDT")
        cntEndDateBox.Text = ds.Tables("FCU1_FIL").Rows(0).Item("FCU1_OBSDT")

        'Wey
        'signByBox.Text = ds.Tables("FCU1_FIL").Rows(0).Item("FCU1_SGNBY")
        Dim test As String = ds.Tables("FCU1_FIL").Rows(0).Item("FCU1_SGNBY")
        'cboTechnician.SelectedValue = ds.Tables("FCU1_FIL").Rows(0).Item("FCU1_SGNBY")

        notesBox.Text = ds.Tables("FCU1_FIL").Rows(0).Item("FCU1_REM")
        CreatedBy.Text = ds.Tables("FCU1_FIL").Rows(0).Item("FCU1_CUSER")
        CreatedDate.Text = ds.Tables("FCU1_FIL").Rows(0).Item("FCU1_CREDT")
        ModifiedBy.Text = ds.Tables("FCU1_FIL").Rows(0).Item("FCU1_LUSER")
        ModifiedDate.Text = ds.Tables("FCU1_FIL").Rows(0).Item("FCU1_LSTDT")




        'Contract Type
        cntTypeBox.Text = ds.Tables("FCU1_FIL").Rows(0).Item("FCU1_SVTID").ToString()
        cntEntity.ContractType = Trim(cntTypeBox.Text)
        cntEntity.COUNTRY = Session("login_ctryID")
        Dim dsContractType As DataSet = cntEntity.GetContractTypeInfo()
        If dsContractType Is Nothing Then
            'Response.Redirect("~/PresentationLayer/error.aspx")
        End If
        If dsContractType.Tables("MSV1_FIL").Rows.Count = 1 Then
            cntDspBox.Text = dsContractType.Tables("MSV1_FIL").Rows(0).Item("MSV1_ENAME")
        End If

        Dim current As Integer
        Dim count As Integer
        'drop down list
        'Contract Status
        Dim cntStatus As String = ds.Tables("FCU1_FIL").Rows(0).Item("FCU1_STAT").ToString().Trim()
        Session("FUN-MCNT-CNTSTAT") = cntStatus
        Dim cntStat As New clsUtil()
        Dim statParam As ArrayList = New ArrayList
        statParam = cntStat.searchconfirminf("CNTStatus")
        Dim statid As String
        Dim statnm As String
        If cntStatus <> "ACTIVE" Then '不是active
            For count = 0 To statParam.Count - 1
                statid = statParam.Item(count)
                statnm = statParam.Item(count + 1)
                count = count + 1
                If statid = cntStatus Then
                    cntStatusDrpList.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                End If
            Next
            Me.saveButton.Enabled = False
            Dim row As GridViewRow
            For Each row In cntServInfoView.Rows
                If row.RowType = DataControlRowType.DataRow Then
                    Dim edit As LinkButton = CType(row.Controls(0).Controls(0), LinkButton)
                    edit.Enabled = False
                End If
            Next
        Else
            For count = 0 To statParam.Count - 1
                statid = statParam.Item(count)
                statnm = statParam.Item(count + 1)
                count = count + 1
                cntStatusDrpList.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
            Next
        End If
        cntStatusDrpList.SelectedIndex = 0

        'SMSReminder
        Dim cntRem As String = ds.Tables("FCU1_FIL").Rows(0).Item("FCU1_SMSRM").ToString()
        statParam = cntStat.searchconfirminf("YESNO")
        current = 0
        For count = 0 To statParam.Count - 1
            statid = statParam.Item(count)
            statnm = statParam.Item(count + 1)
            If statid.ToString().Trim() = cntRem Then
                current = count / 2
            End If
            count = count + 1
            SMSRemDrpList.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
        Next
        SMSRemDrpList.SelectedIndex = current

        'ROInfo
        Dim ROID As String = ds.Tables("FCU1_FIL").Rows(0).Item("FCU1_ROUID").ToString()


        cntEntity.CustomerPrifx = custPfBox.Text
        cntEntity.CustomerID = custIDBox.Text
        cntEntity.ROID = ROID
        preCntNoBox.Text = cntEntity.GetPreContractNO(ds.Tables("FCU1_FIL").Rows(0).Item("FCU1_CREDT"))


        'Session("FUN-MCNT-ROID") = ROID
        Dim apptEntity As New clsAppointment
        apptEntity.CustomerID = Trim(custIDBox.Text.ToString())
        apptEntity.CustomerPrifx = Trim(custPfBox.Text.ToString())
        apptEntity.ServiceType = "CONTRACT"
        Dim dsDrpListData As DataSet = apptEntity.GetDropDownListData(Session("userID"))
        If dsDrpListData Is Nothing Then
            Response.Redirect("~/PresentationLayer/error.aspx")
        End If
        ROInfoDrpList.Items.Add(New ListItem("", ""))
        If ROID <> "" And ROID <> "0" Then
            current = -1
            For count = 0 To dsDrpListData.Tables("ROINFO").Rows.Count - 1
                Dim NewItem As New ListItem()
                NewItem.Text = dsDrpListData.Tables("ROINFO").Rows(count).Item("MODEID").ToString() + "-" + _
                        dsDrpListData.Tables("ROINFO").Rows(count).Item("SERIALNO").ToString()
                NewItem.Value = dsDrpListData.Tables("ROINFO").Rows(count).Item("ROID").ToString().Trim()
                ROInfoDrpList.Items.Add(NewItem)
                If ROID = dsDrpListData.Tables("ROINFO").Rows(count).Item("ROID").ToString().Trim() Then
                    current = count
                End If
            Next
            If current <> -1 Then
                ROInfoDrpList.SelectedIndex = current + 1
            Else
                Response.Redirect("~/PresentationLayer/error.aspx") '该水机号在数据库中不存在，
                '或该客户没有买这个水机()
            End If

            apptEntity.ModeID = ROInfoDrpList.SelectedItem.Text.Split("-")(0).ToString()
            apptEntity.SerialNo = ROInfoDrpList.SelectedItem.Text.Split("-")(1).ToString()
            apptEntity.ROID = ROInfoDrpList.SelectedItem.Value.ToString()
            Dim dsRoInfo As New DataSet
            dsRoInfo = apptEntity.GetROAndContractInfo()
            '处理异常***未完成*******
            If dsRoInfo Is Nothing Then
                Response.Redirect("~/PresentationLayer/error.aspx")
            End If
            If dsRoInfo.Tables("MROU_FIL").Rows.Count = 1 Then
                lastServDateBox.Text = dsRoInfo.Tables("MROU_FIL").Rows(0).Item("MROU_LSVDT").ToString()
                installDateBox.Text = dsRoInfo.Tables("MROU_FIL").Rows(0).Item("MROU_INSDT").ToString()
                nextServDateBox.Text = dsRoInfo.Tables("MROU_FIL").Rows(0).Item("MROU_NSVDT").ToString()
                cntEntity.ROID = ROInfoDrpList.SelectedIndex
            End If
        Else
            For count = 0 To dsDrpListData.Tables("ROINFO").Rows.Count - 1
                Dim NewItem As New ListItem()
                NewItem.Text = dsDrpListData.Tables("ROINFO").Rows(count).Item("MODEID").ToString() + "-" + _
                        dsDrpListData.Tables("ROINFO").Rows(count).Item("SERIALNO").ToString()
                NewItem.Value = dsDrpListData.Tables("ROINFO").Rows(count).Item("ROID").ToString()
                ROInfoDrpList.Items.Add(NewItem)
            Next
            ROInfoDrpList.SelectedIndex = 0
            lastServDateBox.Text = ""
            installDateBox.Text = ""
            Session("FUN-AACNT-ROID") = ""
            nextServDateBox.Text = ""
            'preCntNoBox.Text = ""
        End If

        custNameBox.Text = dsDrpListData.Tables("MCUS_FIL").Rows(0).Item("MCUS_ENAME").ToString()

        If dsDrpListData.Tables("MTEL_FIL").Rows.Count > 1 Then
            custContactBox.Text = dsDrpListData.Tables("MTEL_FIL").Rows(0).Item("MTEL_TELNO").ToString()
        End If
        'custContactBox.Text = dsDrpListData.Tables("MCUS_FIL").Rows(0).Item("MCUS_NOTES").ToString()

        'Server Center dropdownlist
        Dim crtServCenterID As String = ds.Tables("FCU1_FIL").Rows(0).Item("FCU1_SVCID").ToString()
        Dim dsServerCenter As DataSet = apptEntity.GetSVCIDByUserID(Session("userID"))
        databonds(dsServerCenter.Tables("ServerCenterID"), servCDrpList, crtServCenterID)

        'Wey Start 4/10/2007
        '---- TECHNICIAN BASED ON SERVICE CENTER
        Dim Technician As New clsCommonClass
        Dim rank As String = Session("login_rank")
        If rank = 7 Then
            Technician.spctr = Session("login_ctryid")
            Technician.spstat = Session("login_cmpID")
            Technician.sparea = servCDrpList.SelectedValue
        End If
        If rank = 8 Then
            Technician.spctr = Session("login_ctryid")
            Technician.spstat = Session("login_cmpID")
            Technician.sparea = servCDrpList.SelectedValue
        End If
        If rank = 9 Then
            Technician.spctr = Session("login_ctryid")
            Technician.spstat = Session("login_cmpID")
            Technician.sparea = Session("login_svcID")
        End If
        If rank = 0 Then
            Technician.spctr = Session("login_ctryid")
        End If

        Technician.rank = rank

        Dim dsTechnician As New DataSet
        dsTechnician = Technician.Getcomidname("BB_MASTECH_NAMEID")
        If dsTechnician.Tables.Count <> 0 Then
            databondsForTechnician(dsTechnician, Me.cboTechnician)
        End If
        Me.cboTechnician.Items.Insert(0, "")
        'Me.cboTechnician.SelectedIndex = 0
        cboTechnician.SelectedValue = ds.Tables("FCU1_FIL").Rows(0).Item("FCU1_SGNBY").ToString
        'Wey End 4/10/2007

        '''access control'''''''''''''''''''''''''''''''''''''''''''''''
        Dim accessgroup As String = Session("accessgroup").ToString
        Dim purviewArray As Array = New Boolean() {False, False, False, False}
        purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "23")

        Me.saveButton.Enabled = purviewArray(1)


        ContractChange(Session("login_ctryid"), cntTypeBox.Text, Me.cntStartDateBox.Text)
    End Sub

    Private Sub databondsForTechnician(ByVal ds As DataSet, ByVal dropdown As DropDownList, Optional ByVal X As Boolean = True)

        dropdown.Items.Clear()
        Dim row As DataRow

        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            If X = True Then
                NewItem.Text = Trim(row("name")) & "-" & Trim(row("id"))
            Else
                NewItem.Text = Trim(row("name"))
            End If
            NewItem.Value = Trim(row("id"))
            dropdown.Items.Add(NewItem)
        Next

    End Sub

    Public Sub databonds(ByRef dt As DataTable, ByRef dropdown As DropDownList, ByVal crtId As String)
        Dim current As Integer = -1
        Dim count As Integer = 0
        dropdown.Items.Clear()
        dropdown.Items.Add(New ListItem("", ""))
        Dim row As DataRow
        For Each row In dt.Rows
            If crtId = row("id").ToString().Trim() Then
                current = count
            End If
            Dim NewItem As New ListItem()
            NewItem.Text = row("id") & "-" & row("name")
            NewItem.Value = row("id")
            dropdown.Items.Add(NewItem)
            count = count + 1
        Next
        'If dt.Rows.Count = 0 Then '处理数据表为空的情况
        '    dropdown.Items.Insert(0, New ListItem("", ""))
        'End If
        dropdown.SelectedIndex = current + 1


        'If crtId = "" Then '处理NULL
        '    dropdown.Items.Insert(0, New ListItem("", ""))
        '    dropdown.SelectedIndex = 0
        'End If
    End Sub
    Protected Sub ROInfoDrpList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ROInfoDrpList.SelectedIndexChanged
        'ChangeRO()

    End Sub

    Sub ChangeRO()
        If ROInfoDrpList.SelectedValue <> "" Then
            Dim apptEntity As New clsAppointment
            'Dim cntEntity As New clsContract
            apptEntity.ModeID = ROInfoDrpList.SelectedItem.Text.Split("-")(0).ToString()
            apptEntity.SerialNo = ROInfoDrpList.SelectedItem.Text.Split("-")(1).ToString()
            apptEntity.ROID = ROInfoDrpList.SelectedItem.Value.ToString()

            Dim ds As DataSet = apptEntity.GetROAndContractInfo()

            If ds Is Nothing Then
                'Response.Redirect("~/PresentationLayer/error.aspx")
            End If
            If ds.Tables("MROU_FIL").Rows.Count = 1 Then
                lastServDateBox.Text = ds.Tables("MROU_FIL").Rows(0).Item("MROU_LSVDT").ToString()
                installDateBox.Text = ds.Tables("MROU_FIL").Rows(0).Item("MROU_INSDT").ToString()
                'Session("FUN-MCNT-ROID") = ds.Tables("MROU_FIL").Rows(0).Item("MROU_ROUID").ToString()
                nextServDateBox.Text = ds.Tables("MROU_FIL").Rows(0).Item("MROU_NSVDT").ToString()
                'cntEntity.ROID = Session("FUN-MCNT-ROID")
            End If
        Else
            lastServDateBox.Text = ""
            installDateBox.Text = ""
            Session("FUN-AACNT-ROID") = ""
            nextServDateBox.Text = ""
            'preCntNoBox.Text = ""
        End If
    End Sub


    'Protected Sub servHistorylkBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles servHistorylkBtn.Click



    '    'Dim strTempURL As String = "ROID=" + Session("FUN-MCNT-ROID").ToString() + "&ROINFO=" + ROInfoDrpList.SelectedItem.Text + _
    '    Dim strTempURL As String = "ROID=" + Me.ROInfoDrpList.SelectedValue + "&ROINFO=" + ROInfoDrpList.SelectedItem.Text + _
    '                              "&custName=" + custNameBox.Text + "&custID=" + custIDBox.Text + "&custPf=" + custPfBox.Text
    '    strTempURL = "~/PresentationLayer/function/serverHistory.aspx?" + strTempURL
    '    Response.Redirect(strTempURL)
    'End Sub

    'Protected Sub contractHistroylkBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles contractHistroylkBtn.Click
    '    Dim strTempURL As String = "ROID=" + Me.ROInfoDrpList.SelectedValue + "&ROINFO=" + ROInfoDrpList.SelectedItem.Text + _
    '                              "&custName=" + custNameBox.Text + "&custID=" + custIDBox.Text + "&custPf=" + custPfBox.Text
    '    strTempURL = "~/PresentationLayer/function/contracthistory.aspx?" + strTempURL
    '    Response.Redirect(strTempURL)
    'End Sub

    Private Sub BindGrid(ByVal ContractNo As String, ByVal ContractType As String)
        Dim cntEntity As New clsContract()
        cntEntity.ContractNO = ContractNo
        cntEntity.ContractType = ContractType
        Dim ds As DataSet = cntEntity.GetContractServInfo()
        Session("cntServInfoView") = ds
        cntServInfoView.DataSource = ds.Tables(0)
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        Dim col As CommandField = cntServInfoView.Columns(0)
        col.EditText = objXmlTr.GetLabelName("EngLabelMsg", "BB-Edit") '"edit"
        col.CancelText = objXmlTr.GetLabelName("EngLabelMsg", "BB-Cancel") ' "Cancel"
        col.UpdateText = objXmlTr.GetLabelName("EngLabelMsg", "BB-Save")
        col.HeaderImageUrl = "~/PresentationLayer/graph/edit.gif"
        cntServInfoView.DataBind()
        If cntServInfoView.Rows.Count > 0 Then
            DispGridViewHead()
        End If

        If cntEntity.ResetFlag = 1 Then
            HypCal.Enabled = False
            Me.cntStartDateBox.ReadOnly = True
        Else
            HypCal.Enabled = True
            Me.cntStartDateBox.ReadOnly = False
        End If
    End Sub
    Private Sub DispGridViewHead()
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        cntServInfoView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CHKS-NO") 'NO
        cntServInfoView.HeaderRow.Cells(1).Font.Size = 8
        cntServInfoView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-ACNT-CNTSERVMNTH") 'Service Month
        cntServInfoView.HeaderRow.Cells(2).Font.Size = 8
        cntServInfoView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-ACNT-ACLSERVDATE") 'Actual ServiceDate
        cntServInfoView.HeaderRow.Cells(3).Font.Size = 8
        cntServInfoView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-APPT-TRANSTYPE") 'Transaction Type
        cntServInfoView.HeaderRow.Cells(4).Font.Size = 8
        cntServInfoView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-APPT-TRANSNO") 'Transaction No
        cntServInfoView.HeaderRow.Cells(5).Font.Size = 8
        cntServInfoView.HeaderRow.Cells(6).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CNT-STATUS") 'Status
        cntServInfoView.HeaderRow.Cells(6).Font.Size = 8
    End Sub

    Protected Sub saveMsgBox_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles saveMsgBox.GetMessageBoxResponse
        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
            Dim cntEntity As New clsContract()
            cntEntity.ContractNO = Trim(cntNoBox.Text)
            cntEntity.ROID = Me.ROInfoDrpList.SelectedValue
            cntEntity.CustomerPrifx = Trim(custPfBox.Text)
            cntEntity.CustomerID = Trim(custIDBox.Text)
            cntEntity.ContractType = Trim(cntTypeBox.Text)
            cntEntity.ContractStartDate = Trim(cntStartDateBox.Text)
            cntEntity.ContractEndDate = Trim(cntEndDateBox.Text)
            cntEntity.ServerCenterID = Trim(servCDrpList.SelectedValue)
            cntEntity.ContractStatus = Trim(cntStatusDrpList.SelectedValue)
            cntEntity.SignedBy = Trim(cboTechnician.SelectedValue)
            cntEntity.SMSReminder = Trim(SMSRemDrpList.SelectedValue)
            cntEntity.ContractNotes = Trim(notesBox.Text)

            cntEntity.Frequency = Val(txtFrequency.Text)
            cntEntity.NoOfYear = Val(txtNoOfYear.Text)
            cntEntity.MonthDistance = Convert.ToInt32(Val(txtNoOfYear.Text) * 12 / Val(txtFrequency.Text))

            cntEntity.SessionID = Session("login_session")

            cntEntity.IpAddress = Request.UserHostAddress.ToString()
            cntEntity.UserServCenterID = Session("login_svcID")

            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            Me.errlab.Visible = True

            If cntEntity.UpdateContract(Session("userID")) = True Then
                Me.errlab.Text = objXmlTr.GetLabelName("StatusMessage", "FUN-APPTCNT-SUCCESS")
                InfoMsgBox.Alert(errlab.Text)
                If Me.cntStatusDrpList.SelectedValue <> Session("FUN-MCNT-CNTSTAT") Then
                    Me.saveButton.Enabled = False
                    Me.cntStatusDrpList.Enabled = False
                    BindGrid(Me.cntNoBox.Text.Trim(), Session("ContractTypes"))
                End If
            Else
                Me.errlab.Text = objXmlTr.GetLabelName("StatusMessage", "FUN-APPTCNT-FAILED")
                InfoMsgBox.Alert(errlab.Text)
            End If
        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
        End If
    End Sub

    Protected Sub saveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveButton.Click
        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim msginfo As String
        Dim msgtitle As String

        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        RequestFormValue()


        If Not IsDate(cntStartDateBox.Text) Then
            msginfo = objXm.GetLabelName("StatusMessage", "FUN-CNT-STARTDATE")
            InfoMsgBox.Alert(msginfo)
            Return
        End If



        If Me.cntStatusDrpList.SelectedValue = "EXPIRED" Then 'expired
            If System.DateTime.Today < CType(Me.cntEndDateBox.Text, Date) Then
                msginfo = objXm.GetLabelName("StatusMessage", "FUN-CNT-CNTSTATD")
                InfoMsgBox.Alert(msginfo)
                Return
            End If
        ElseIf Me.cntStatusDrpList.SelectedValue = "COMPLETE" Then 'complete
            Dim cntEntity As New clsContract()
            cntEntity.ContractNO = cntNoBox.Text
            cntEntity.CustomerID = custIDBox.Text
            cntEntity.CustomerPrifx = custPfBox.Text
            cntEntity.ContractType = cntTypeBox.Text
            cntEntity.ContractStartDate = cntStartDateBox.Text
            cntEntity.Frequency = Val(txtFrequency.Text)
            cntEntity.NoOfYear = Val(txtNoOfYear.Text)
            cntEntity.MonthDistance = Convert.ToInt32(Val(txtNoOfYear.Text) * 12 / Val(txtFrequency.Text))

            Dim ValidatorArray As Array = cntEntity.Validator()
            If ValidatorArray Is Nothing Then
                Response.Redirect("~/PresentationLayer/error.aspx")
            End If

            If ValidatorArray(2) = True Then
                msginfo = objXm.GetLabelName("StatusMessage", "FUN-CNT-CNTSTATC")
                InfoMsgBox.Alert(msginfo)
                Return
            End If
        End If
        msginfo = objXm.GetLabelName("StatusMessage", "SAVEORNOT")
        msgtitle = objXm.GetLabelName("StatusMessage", "SAVETITLE")
        saveMsgBox.Confirm(msginfo)
    End Sub

    Protected Sub cntServInfoView_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles cntServInfoView.RowEditing
        cntServInfoView.EditIndex = e.NewEditIndex
        BindGrid(cntNoBox.Text, Session("ContractTypes"))
        'Dim ActualServDateImg As New ImageButton
        'ActualServDateImg = Me.cntServInfoView.Rows(e.NewEditIndex).FindControl("ActualServDateImg")
        'ActualServDateImg.Enabled = True
        Dim ActualServDateDateCalendar As JCalendar.JCalendar
        ActualServDateDateCalendar = Me.cntServInfoView.Rows(e.NewEditIndex).FindControl("ActualServDateDateCalendar")
        ActualServDateDateCalendar.Enabled = True
        Dim tempBox As New TextBox
        tempBox = Me.cntServInfoView.Rows(e.NewEditIndex).FindControl("ActualServDateBox")
        tempBox.ReadOnly = False
        tempBox.Enabled = True
        tempBox = Me.cntServInfoView.Rows(e.NewEditIndex).FindControl("tansTypeBox")
        tempBox.ReadOnly = False
        tempBox = Me.cntServInfoView.Rows(e.NewEditIndex).FindControl("tansNoBox")
        tempBox.ReadOnly = False
        Dim cnt2StatusDrpList As New DropDownList()
        cnt2StatusDrpList = Me.cntServInfoView.Rows(e.NewEditIndex).FindControl("cnt2StatusDrpList")
        cnt2StatusDrpList.Enabled = True

        Me.saveButton.Enabled = False
    End Sub

    Protected Sub cntServInfoView_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles cntServInfoView.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim count As Integer
            Dim cnt2StatusDrpList As New DropDownList()
            cnt2StatusDrpList = e.Row.FindControl("cnt2StatusDrpList")
            cnt2StatusDrpList.Items.Clear()
            Dim cntStat As New clsUtil()
            Dim statParam As ArrayList = New ArrayList
            statParam = cntStat.searchconfirminf("CNT2Status")
            Dim statid As String
            Dim statnm As String
            For count = 0 To statParam.Count - 1
                statid = statParam.Item(count)
                statnm = statParam.Item(count + 1)
                count = count + 1
                cnt2StatusDrpList.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
            Next
            cnt2StatusDrpList.Enabled = False

            Dim ds As DataSet = Session("cntServInfoView")
            If ds Is Nothing Then
                Response.Redirect("~/PresentationLayer/error.aspx")
            End If
            If ds.Tables(0).Rows(e.Row.RowIndex).Item("FCU2_STAT").ToString().Trim() <> "NEW" Then '不是new的不能更改
                Dim edit As LinkButton = CType(e.Row.Controls(0).Controls(0), LinkButton)
                edit.Enabled = False
            End If
            objXmlTr.XmlFile = System.Configuration.ConfigurationManager.AppSettings("StatMsg")
            Dim xCV As CompareValidator = e.Row.FindControl("CompDateVal")
            xCV.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "INVALID_DATE")
        End If
    End Sub
    Protected Sub cntServInfoView_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles cntServInfoView.RowCancelingEdit
        cntServInfoView.EditIndex = -1
        BindGrid(cntNoBox.Text, Session("ContractTypes"))
        Me.saveButton.Enabled = True
    End Sub

    Protected Sub ActualServDateDateCalendar_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        'Dim ActualServDateDateCalendar As Calendar = sender
        Dim ActualServDateDateCalendar As JCalendar.JCalendar = sender
        Dim ActualServDateBox As TextBox = cntServInfoView.Rows(cntServInfoView.EditIndex).FindControl("ActualServDateBox")
        ActualServDateBox.Text = ActualServDateDateCalendar.SelectedDate
        'ActualServDateDateCalendar.Visible = False
    End Sub
    Protected Sub UpdateMsgBox_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles UpdateMsgBox.GetMessageBoxResponse
        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
            Dim cntEntity As New clsContract
            cntEntity.ContractNO = cntNoBox.Text
            Dim MTHYRLab As Label = cntServInfoView.Rows(cntServInfoView.EditIndex).FindControl("MTHYRLab")
            cntEntity.ServiceDate = MTHYRLab.Text
            Dim ActualServDateBox As TextBox = cntServInfoView.Rows(cntServInfoView.EditIndex).FindControl("ActualServDateBox")
            cntEntity.ActualServiceDate = ActualServDateBox.Text
            Dim tansTypeBox As TextBox = cntServInfoView.Rows(cntServInfoView.EditIndex).FindControl("tansTypeBox")
            cntEntity.TransactionType = tansTypeBox.Text
            Dim tansNoBox As TextBox = cntServInfoView.Rows(cntServInfoView.EditIndex).FindControl("tansNoBox")
            cntEntity.TransactionNo = tansNoBox.Text
            Dim cnt2StatusDrpList As New DropDownList()
            cnt2StatusDrpList = cntServInfoView.Rows(cntServInfoView.EditIndex).FindControl("cnt2StatusDrpList")
            cntEntity.CNT2Status = cnt2StatusDrpList.SelectedValue

            cntEntity.IpAddress = Request.UserHostAddress.ToString()
            cntEntity.UserServCenterID = Session("login_svcID")
            cntEntity.SessionID = Session("login_session")

            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            Me.errlab.Visible = True
            If cntEntity.UpdateContractService(Session("userID")) = True Then
                Me.errlab.Text = objXmlTr.GetLabelName("StatusMessage", "CONTRACT-DETAILSAVE")
                InfoMsgBox.Alert(errlab.Text)
            Else
                Me.errlab.Text = objXmlTr.GetLabelName("StatusMessage", "FUN-APPTCNT-FAILED")
                InfoMsgBox.Alert(errlab.Text)
            End If
            cntServInfoView.EditIndex = -1
            BindGrid(cntNoBox.Text, Session("ContractTypes"))
            Me.saveButton.Enabled = True
        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
        End If
    End Sub

    Protected Sub cntServInfoView_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles cntServInfoView.RowUpdating
        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim msginfo As String
        Dim msgtitle As String
        msginfo = objXm.GetLabelName("StatusMessage", "CONTRACT-SAVEDETAIL")
        msgtitle = objXm.GetLabelName("StatusMessage", "SAVETITLE")
        UpdateMsgBox.Confirm(msginfo)
    End Sub

    Protected Sub custIDBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles custIDBox.TextChanged
        Dim lstrCustomerID As String = Me.custIDBox.Text
        Dim lstrCustomerPrefix As String = Me.custPfBox.Text
        Dim msginfo As String
        Dim rsDataReader As SqlDataReader
        Dim count As Integer

        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        msginfo = objXmlTr.GetLabelName("StatusMessage", "FUN-CNT-CUSTID")

        If lstrCustomerID.Trim = "" Then
            InfoMsgBox.Alert(msginfo)
        Else
            Dim objCustomer As New clsCustomerRecord

            objCustomer.CustomerID = lstrCustomerID
            objCustomer.Customerpf = lstrCustomerPrefix

            rsDataReader = objCustomer.GetCustomerDetailsByID()
            If rsDataReader.Read = False Then
                InfoMsgBox.Alert(msginfo)
            Else
                Me.custNameBox.Text = rsDataReader.GetValue(3).ToString()

                Dim ds As DataSet = objCustomer.GetROidname()
                ROInfoDrpList.Items.Clear()
                ROInfoDrpList.Items.Add(New ListItem("", ""))
                For count = 0 To ds.Tables(0).Rows.Count - 1
                    Dim NewItem As New ListItem()
                    NewItem.Text = ds.Tables(0).Rows(count).Item("MROU_MODID").ToString() + "-" + _
                            ds.Tables(0).Rows(count).Item("MROU_SERNO").ToString()
                    NewItem.Value = ds.Tables(0).Rows(count).Item("MROU_ROUID").ToString()
                    ROInfoDrpList.Items.Add(NewItem)
                Next
                ROInfoDrpList.SelectedIndex = 0

            End If
            objCustomer = Nothing

        End If
    End Sub

    Sub RequestFormValue()
        lastServDateBox.Text = Request.Form("lastServDateBox")
        installDateBox.Text = Request.Form("installDateBox")
        nextServDateBox.Text = Request.Form("nextServDateBox")
        preCntNoBox.Text = Request.Form("preCntNoBox")

        If Request.Form("cntStartDateBox") IsNot Nothing Then
            cntStartDateBox.Text = Request.Form("cntStartDateBox")
        End If

        If Request.Form("cntEndDateBox") IsNot Nothing Then
            Me.cntEndDateBox.Text = Request.Form("cntEndDateBox")
        End If



    End Sub


    Sub ContractChange(ByVal Country As String, ByVal ContractType As String, ByVal StartDate As String)
        Dim arrlist As New ArrayList
        Dim lintNoy As Integer
        Dim lintFreq As String
        Dim lstrStartDate As String = StartDate

        lintNoy = 1
        lintFreq = 1

        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        If ContractType <> "" Then
            Dim cntEntity As New clsContract()

            cntEntity.ContractType = ContractType
            cntEntity.COUNTRY = Country

            Dim ds As DataSet = cntEntity.GetContractTypeInfo()

            If ds Is Nothing Or ds.Tables("MSV1_FIL").Rows.Count <> 1 Then
         
                'Response.Redirect("~/PresentationLayer/error.aspx")
            Else
                lintNoy = Val(ds.Tables("MSV1_FIL").Rows(0).Item("MSV1_NOYR"))
                lintFreq = Val(ds.Tables("MSV1_FIL").Rows(0).Item("MSV1_FREQ"))
            End If

            If lstrStartDate = "" Then
                lstrStartDate = CStr(Now())
            End If

            'Dim enddate As DateTime = CDate(lstrStartDate)
            'enddate = DateAdd(DateInterval.Year, lintNoy, CDate(lstrStartDate))
            'enddate = DateAdd(DateInterval.Day, -1, enddate)

        End If

        Me.txtFrequency.Text = lintFreq
        Me.txtNoOfYear.Text = lintNoy

    End Sub


#Region "AJAX"

    <AjaxPro.AjaxMethod()> _
   Function AjaxChangeRO(ByVal ROUID As String, ByVal CustomerPrefix As String, ByVal CustomerID As String, ByVal CreateDate As String) As ArrayList
        Dim arrlst As New ArrayList
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle


        If ROUID <> "" Then
            Dim apptEntity As New clsAppointment


            apptEntity.ROID = ROUID

            Dim ds As DataSet = apptEntity.GetROAndContractInfo()
            '处理异常***未完成*******
            If ds Is Nothing Then
                Response.Redirect("~/PresentationLayer/error.aspx")
            End If
            If ds.Tables("MROU_FIL").Rows.Count = 1 Then
                arrlst.Add(ds.Tables("MROU_FIL").Rows(0).Item("MROU_LSVDT").ToString())
                arrlst.Add(ds.Tables("MROU_FIL").Rows(0).Item("MROU_INSDT").ToString())
                arrlst.Add(ds.Tables("MROU_FIL").Rows(0).Item("MROU_ROUID").ToString())
                arrlst.Add(ds.Tables("MROU_FIL").Rows(0).Item("MROU_NSVDT").ToString())
            Else
                arrlst.Add("")
                arrlst.Add("")
                arrlst.Add("")
                arrlst.Add("")
            End If
        Else
            arrlst.Add("")
            arrlst.Add("")
            arrlst.Add("")
            arrlst.Add("")
        End If


        Dim cntEntity As New clsContract
        cntEntity.CustomerID = CustomerID
        cntEntity.CustomerPrifx = CustomerPrefix
        cntEntity.ROID = ROUID
        arrlst.Add(cntEntity.GetPreContractNO(CreateDate))

        Return arrlst
    End Function


    <AjaxPro.AjaxMethod()> _
       Function AjaxComputeEndDate(ByVal StartDate As String, ByVal NOY As String) As String
        Dim EndDate As Date
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        If IsDate(StartDate) = True Then
            EndDate = DateAdd(DateInterval.Year, Val(NOY), CDate(StartDate))
            EndDate = DateAdd(DateInterval.Day, -1, EndDate)
        Else
            EndDate = Now()
        End If
        Return Format(EndDate, "dd/MM/yyyy")
    End Function



#End Region

End Class
