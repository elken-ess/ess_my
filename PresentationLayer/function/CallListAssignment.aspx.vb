﻿Imports System.Configuration
Imports System.Globalization
Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared
Imports BusinessEntity

Partial Class PresentationLayer_function_CallListAssignment_aspx
    Inherits System.Web.UI.Page

    Dim objXML As New clsXml
    Dim callList As New clsCallListAssignment
    Dim dataStyle As CultureInfo = New System.Globalization.CultureInfo("en-CA")
    Dim strUserID As String
    Dim callListReportDoc As New ReportDocument()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.MaintainScrollPositionOnPostBack = True
        If Not Page.IsPostBack Then
            Dim script As String = "top.location='../logon.aspx';"
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                    'Response.Redirect("~/PresentationLayer/logon.aspx")
                End If
            Catch ex As Exception
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try

            Session.Contents("PrintDataFlag") = ""
            strUserID = Session("userID").ToString

            '设置用户控制权限
            SetUserPurview()

            LoadInfo()
        ElseIf Session.Contents("PrintDataFlag") = "1" Then
            PrintData()
        Else
            ErrLab.Visible = False
            SetUserPurview()
        End If
    End Sub
#Region "Display GridView"
    Protected Sub DisplayGridViewHeader()
        objXML.XmlFile = ConfigurationManager.AppSettings("XmlFilePath")

        Me.callListGridView.HeaderRow.Cells(0).Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_CHECK")
        Me.callListGridView.HeaderRow.Cells(0).Font.Size = 8
        Me.callListGridView.HeaderRow.Cells(1).Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_CUSTOMER")
        Me.callListGridView.HeaderRow.Cells(1).Font.Size = 8
        Me.callListGridView.HeaderRow.Cells(2).Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_MODELRONO")
        Me.callListGridView.HeaderRow.Cells(2).Font.Size = 8
        Me.callListGridView.HeaderRow.Cells(3).Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_AREAID")
        Me.callListGridView.HeaderRow.Cells(3).Font.Size = 8
        Me.callListGridView.HeaderRow.Cells(4).Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_LSDATE")
        Me.callListGridView.HeaderRow.Cells(4).Font.Size = 8
        Me.callListGridView.HeaderRow.Cells(5).Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_DUEDATE")
        Me.callListGridView.HeaderRow.Cells(5).Font.Size = 8
        Me.callListGridView.HeaderRow.Cells(6).Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_TRELEASED")
        Me.callListGridView.HeaderRow.Cells(6).Font.Size = 8
        Me.callListGridView.HeaderRow.Cells(7).Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ASSIGN")
        Me.callListGridView.HeaderRow.Cells(7).Font.Size = 8
        Me.callListGridView.HeaderRow.Cells(8).Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_STYPE")
        Me.callListGridView.HeaderRow.Cells(8).Font.Size = 8
        Me.callListGridView.HeaderRow.Cells(9).Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_STATUS")
        Me.callListGridView.HeaderRow.Cells(9).Font.Size = 8
        'Me.callListGridView.HeaderRow.Cells(10).Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_MROUID")
        'Me.callListGridView.HeaderRow.Cells(10).Font.Size = 8

    End Sub
#End Region

    Protected Sub LoadInfo()
        'Display the Label or Button Text=================================================
        objXML.XmlFile = ConfigurationManager.AppSettings("XmlFilePath")
        Me.callListTitleLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_TITLE")
        Me.stateFLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_FROMSTATE")
        Me.stateTLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_TOSTATE")
        Me.scFLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_FROMSERVICE")
        Me.scTLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_TOSERVICE")
        Me.areaFLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_FROMAREA")
        Me.assignTLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ASSIGN")

        Me.reminderLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_REMINDER")
        Me.stLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_STYPE")
        Me.releasedLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_RELEASED")
        Me.sortLab1.Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_SORT1")
        Me.sortLab2.Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_SORT2")
        Me.retriveButton.Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_RETRIEVE")
        Me.printButton.Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_PRI")
        Me.releaseLimitLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_RELLIMIT")
        Me.totalReleaseLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_TOTALREL")
        Me.markAllButton.Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_MARKALL")
        Me.saveButton.Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_SAVE")
        Me.stateLab.Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_STATE")
        Me.lblAreaIDTo.Text = objXML.GetLabelName("EngLabelMsg", "TO_AREAID")
        Me.lblTotalRecord.Text = objXML.GetLabelName("EngLabelMsg", "RPT_APT_RECORD")
        Me.lblFrmDueDate.Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_FROM_DUEDATE")
        Me.lblToDueDate.Text = objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_TO_DUEDATE")
        Me.lblProductClass.Text = objXML.GetLabelName("EngLabelMsg", "BB-MASROUM-0010")

        'Fill DropDownList Items ===========================================================
        'Dim callList As New clsCallListAssignment
        callList.ModiBy = strUserID
        Dim dsCLA As DataSet = callList.PreQuery()
        If dsCLA Is Nothing Then
            objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
            ErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_FUN_CALLLIST_RERR")
            ErrLab.Visible = True
            MessageBox1.Alert(ErrLab.Text)
            retriveButton.Enabled = False
            printButton.Enabled = False
            markAllButton.Enabled = False
            'saveButton.Enabled = False
            Return
        End If

        Dim cntryStat As New clsrlconfirminf()
        Dim count As Integer
        Dim statid As String
        Dim statnm As String
        Dim productcl As ArrayList = New ArrayList
        productcl = cntryStat.searchconfirminf("PRODUCTCL")
        count = 0
        drpProdClass.Items.Add(New ListItem("", "%"))
        For count = 0 To productcl.Count - 1
            statid = productcl.Item(count)
            statnm = productcl.Item(count + 1)
            count = count + 1
            drpProdClass.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
        Next
        drpProdClass.Items(0).Selected = True
        txtFrmDueDate.Text = Date.Today.Day.ToString + "/" + Date.Today.Month.ToString + "/" + Date.Today.Year.ToString
        txtToDueDate.Text = Date.Today.Day.ToString + "/" + Date.Today.Month.ToString + "/" + Date.Today.Year.ToString

        Dim i As Integer

        If dsCLA.Tables.Count = 0 Then
            Return
        End If

        'stateFDropDownList.Items.Add("")
        'stateTDropDownList.Items.Add("")
        stateFDropDownList.Items.Add(New ListItem("", ""))
        stateTDropDownList.Items.Add(New ListItem("", ""))
        Dim stateid As String
        Dim statenm As String
        For i = 0 To dsCLA.Tables(0).Rows.Count - 1
            statenm = dsCLA.Tables(0).Rows(i).Item(0) + "-" + dsCLA.Tables(0).Rows(i).Item(1)
            stateid = dsCLA.Tables(0).Rows(i).Item(0)
            stateFDropDownList.Items.Add(New ListItem(statenm.ToString(), stateid.ToString()))
            stateTDropDownList.Items.Add(New ListItem(statenm.ToString(), stateid.ToString()))
            'stateFDropDownList.Items.Add(dsCLA.Tables(0).Rows(i).Item(0))
            'stateTDropDownList.Items.Add(dsCLA.Tables(0).Rows(i).Item(0))
        Next
        If stateFDropDownList.Items.Count > 0 Then
            stateFDropDownList.SelectedIndex = 0
            stateTDropDownList.SelectedIndex = 0
        Else
            retriveButton.Enabled = False
        End If

        scFDropDownList.Items.Add(New ListItem("", "%"))
        scTDropDownList.Items.Add(New ListItem("", "%"))
        Dim scid As String
        Dim scnm As String
        For i = 0 To dsCLA.Tables(1).Rows.Count - 1
            scnm = dsCLA.Tables(1).Rows(i).Item(0) + "-" + dsCLA.Tables(1).Rows(i).Item(1)
            scid = dsCLA.Tables(1).Rows(i).Item(0)
            scFDropDownList.Items.Add(New ListItem(scnm.ToString(), scid.ToString()))
            scTDropDownList.Items.Add(New ListItem(scnm.ToString(), scid.ToString()))
        Next
        If scFDropDownList.Items.Count > 0 Then
            scFDropDownList.SelectedIndex = 0
            scTDropDownList.SelectedIndex = 0
        Else
            retriveButton.Enabled = False
        End If

        'stDropDownList.Items.Add(objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"))
        ' For i = 0 To dsCLA.Tables(3).Rows.Count - 1
        'stDropDownList.Items.Add(dsCLA.Tables(3).Rows(i).Item(0))
        ' Next
        stDropDownList.SelectedIndex = 0

        '---------------------------------------------------------------------------------------

        'areaFDropDownList.Items.Add(objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"))
        areaFDropDownList.Items.Add(New ListItem(objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), ""))
        cboAreaIdTo.Items.Add(New ListItem(objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), ""))
        Dim areaid As String
        Dim areanm As String
        For i = 0 To dsCLA.Tables(2).Rows.Count - 1
            areanm = dsCLA.Tables(2).Rows(i).Item(0) + "-" + dsCLA.Tables(2).Rows(i).Item(1)
            areaid = dsCLA.Tables(2).Rows(i).Item(0)
            areaFDropDownList.Items.Add(New ListItem(areanm.ToString(), areaid.ToString()))
            cboAreaIdTo.Items.Add(New ListItem(areanm.ToString(), areaid.ToString()))
        Next

        If areaFDropDownList.Items.Count > 0 Then
            areaFDropDownList.SelectedIndex = 0
        End If

        '---------------------------------------------------------------------------------------

        'cboAreaIdTo.Items.Add(objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"))
        'For i = 0 To dsCLA.Tables(2).Rows.Count - 1
        '    cboAreaIdTo.Items.Add(dsCLA.Tables(2).Rows(i).Item(0))
        'Next

        If Me.cboAreaIdTo.Items.Count > 0 Then
            cboAreaIdTo.SelectedIndex = 0
        End If

        '---------------------------------------------------------------------------------------


        'Retrieve User ID and Fill in userDropDownList
        userDropDownList.Items.Clear()
        userDropDownList.Items.Add("")
        For i = 0 To dsCLA.Tables(4).Rows.Count - 1
            userDropDownList.Items.Add(New ListItem(dsCLA.Tables(4).Rows(i).Item(0), dsCLA.Tables(4).Rows(i).Item(1)))
        Next
        userDropDownList.SelectedIndex = 0

        releasedDropDownList.Items.Add(objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_RELEASED1"))
        releasedDropDownList.Items.Add(objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_RELEASED2"))
        releasedDropDownList.Items.Add(objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_RELEASED3"))
        releasedDropDownList.SelectedIndex = 0

        sortDropDownList1.Items.Add(objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_CUSTOMER"))
        sortDropDownList2.Items.Add(objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_CUSTOMER"))
        sortDropDownList1.Items.Add(objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_MODELRONO"))
        sortDropDownList2.Items.Add(objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_MODELRONO"))
        sortDropDownList1.Items.Add(objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_AREAID"))
        sortDropDownList2.Items.Add(objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_AREAID"))
        sortDropDownList1.Items.Add(objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_LSDATE"))
        sortDropDownList2.Items.Add(objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_LSDATE"))
        sortDropDownList1.Items.Add(objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_NCDATE"))
        sortDropDownList2.Items.Add(objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_NCDATE"))
        sortDropDownList1.Items.Add(objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_DUEDATE"))
        sortDropDownList2.Items.Add(objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_DUEDATE"))
        sortDropDownList1.Items.Add(objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_PRI_REFG"))
        sortDropDownList2.Items.Add(objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_PRI_REFG"))
        sortDropDownList1.Items.Add(objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ASSIGN"))
        sortDropDownList2.Items.Add(objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ASSIGN"))
        sortDropDownList1.Items.Add(objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_STATUS"))
        sortDropDownList2.Items.Add(objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_STATUS"))
        sortDropDownList1.SelectedIndex = 0
        sortDropDownList2.SelectedIndex = 0

        Dim dt As DateTime = DateTime.Now

        If dt.Month < 12 Then
            reminderMonth.Text = dt.Month + 1
            reminderYear.Text = dt.Year
        Else
            reminderMonth.Text = "1"
            reminderYear.Text = dt.Year + 1
        End If

        'default = 50
        releaseLimitBox.Text = "50"
        callListGridView.PageSize = 50
        releasedDropDownList.SelectedIndex = 1

        Me.cboOperation.SelectedIndex = 3

        objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
        Me.RangeValidatorMonth.ErrorMessage = objXML.GetLabelName("StatusMessage", "BB_FUN_CALLLIST_MONTHERROR")
        Me.RangeValidatorYear.ErrorMessage = objXML.GetLabelName("StatusMessage", "BB_FUN_CALLLIST_YEARERROR")
        Me.RangeValidatorLimit.ErrorMessage = objXML.GetLabelName("StatusMessage", "BB_FUN_CALLLIST_RLIMITERROR")
        Me.rfvAssignedTo.ErrorMessage = objXML.GetLabelName("StatusMessage", "AAHOD-REQUSER")
    End Sub

    Protected Sub retriveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles retriveButton.Click
        'Retrive Call List Assignment data from Server
        Try
            RetriveAssignInfo()
        Catch ex As Exception
            objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
            ErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_FUN_CALLLIST_ERROR")
            ErrLab.Visible = True
            MessageBox1.Alert(ErrLab.Text)
        End Try
        Me.lblRelease.Text = UCase(releasedDropDownList.SelectedValue)
        'If UCase(releasedDropDownList.SelectedValue) = "Y" Then
        '    rfvAssignedTo.Visible = False
        'Else
        '    rfvAssignedTo.Visible = True
        'End If
        CrystalReportViewer.Visible = False
    End Sub

    Protected Sub MessageBox1_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox1.GetMessageBoxResponse
        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
            Try
                saveRecord()
            Catch ex As Exception
                objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
                ErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_MASMUAG_FAILED")
                ErrLab.Visible = True
                MessageBox1.Alert(ErrLab.Text)
            End Try

        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
            Return
        End If
    End Sub

    Protected Function saveRecord() As Boolean
        Dim dsCLA As DataSet = Session.Contents("CallListAssignment")
        Dim i As Integer
        If dsCLA Is Nothing Then
            RetriveAssignInfo()
            Return False
        End If

        Dim intsavedreccnt As Integer = 0
        '----------------------------------------------------------------------------------------------------------------------------   
        '   CHECK VALUE OF RELEASE DROP DOWN   
        '---------------------------------------------------------------------------------------------------------------------------- 
        ' IF RELEASE = ALL DO NOTHING 
        If UCase(releasedDropDownList.SelectedItem.Text) = "ALL" Then
            RetriveAssignInfo()
            Return False
        End If

        For i = 0 To dsCLA.Tables(0).Rows.Count - 1
            If dsCLA.Tables(0).Rows(i).Item("RELFG") Then
                If UCase(dsCLA.Tables(0).Rows(i).Item("MROU_CSTAT")) <> "CONFIRMED" Then
                    If UCase(releasedDropDownList.SelectedValue) = "Y" Then
                        '----------------------------------------------------------------------------------------------------------------------------   
                        '   IF RELEASE = Y AND ASSIGNED TO = "" -- SET TO UNRELEASE 
                        '---------------------------------------------------------------------------------------------------------------------------- 
                        If Me.userDropDownList.SelectedIndex = 0 Then
                            callList.Released = "N"
                            callList.AssignTo = ""
                            callList.MRouID = dsCLA.Tables(0).Rows(i).Item("MROU_ROUID").ToString()
                            callList.ModiBy = Session("userID")
                            intsavedreccnt = intsavedreccnt + callList.SaveAppointmentInfo()
                        Else
                            '----------------------------------------------------------------------------------------------------------------------------   
                            '   IF RELEASE = Y AND ASSIGNED TO <> "" -- REASSIGN TO USER
                            '---------------------------------------------------------------------------------------------------------------------------- 
                            callList.Released = "Y"
                            callList.AssignTo = userDropDownList.SelectedItem.Value
                            callList.MRouID = dsCLA.Tables(0).Rows(i).Item("MROU_ROUID").ToString()
                            callList.ModiBy = Session("userID")
                            intsavedreccnt = intsavedreccnt + callList.SaveAppointmentInfo()
                        End If

                    Else
                        '----------------------------------------------------------------------------------------------------------------------------   
                        '   IF RELEASE = N -- SET TO RELEASE 
                        '---------------------------------------------------------------------------------------------------------------------------- 
                        callList.Released = "Y"
                        callList.AssignTo = userDropDownList.SelectedItem.Value
                        callList.MRouID = dsCLA.Tables(0).Rows(i).Item("MROU_ROUID").ToString()
                        callList.ModiBy = Session("userID")
                        intsavedreccnt = intsavedreccnt + callList.SaveAppointmentInfo()
                    End If
                End If

            End If
            'checkBoxView = callListGridView.Rows(i).FindControl("GridViewCheck")
            'If dsCLA.Tables(0).Rows(i).Item("RELFG") Then
            '    callList.Released = "Y"
            'Else
            '    callList.Released = "N"
            'End If
            'If (releasedDropDownList.SelectedItem.ToString = "N" And dsCLA.Tables(0).Rows(i).Item("RELFG")) _
            '    Or (releasedDropDownList.SelectedItem.ToString = "Y") Then
            'If dsCLA.Tables(0).Rows(i).Item("RELFG") Then
            '    'callList.AssignTo = userDropDownList.SelectedItem.Value
            '    If (userDropDownList.SelectedItem.Value = dsCLA.Tables(0).Rows(i).Item("MROU_BATCH")) And _
            '        (dsCLA.Tables(0).Rows(i).Item("MROU_RELFG") = "Y") Then
            '        callList.AssignTo = ""
            '        callList.Released = "N"
            '    Else
            '        callList.Released = "Y"
            '        callList.AssignTo = userDropDownList.SelectedItem.Value
            '    End If

            '    callList.MRouID = dsCLA.Tables(0).Rows(i).Item("MROU_ROUID").ToString()
            '    callList.ModiBy = Session("userID")
            '    callList.SaveAppointmentInfo()
            'End If
            'If UCase(dsCLA.Tables(0).Rows(i).Item("MROU_CSTAT")) <> "CONFIRMED" Then
            '    If IIf(dsCLA.Tables(0).Rows(i).Item(0), "Y", "N") <> dsCLA.Tables(0).Rows(i).Item("MROU_RELFG") Then
            '        If dsCLA.Tables(0).Rows(i).Item(0) Then
            '            callList.Released = "Y"
            '            callList.AssignTo = userDropDownList.SelectedItem.Value
            '        Else
            '            callList.Released = "N"
            '            callList.AssignTo = ""
            '        End If
            '        callList.MRouID = dsCLA.Tables(0).Rows(i).Item("MROU_ROUID").ToString()
            '        callList.ModiBy = Session("userID")
            '        intsavedreccnt = intsavedreccnt + callList.SaveAppointmentInfo()
            '    Else
            '        If IIf(dsCLA.Tables(0).Rows(i).Item(0), "Y", "N") = "Y" And _
            '            userDropDownList.SelectedItem.Value <> dsCLA.Tables(0).Rows(i).Item("MROU_BATCH").ToString() Then
            '            callList.Released = "Y"
            '            callList.AssignTo = userDropDownList.SelectedItem.Value

            '            callList.MRouID = dsCLA.Tables(0).Rows(i).Item("MROU_ROUID").ToString()
            '            callList.ModiBy = Session("userID")
            '            intsavedreccnt = intsavedreccnt + callList.SaveAppointmentInfo()
            '        End If
            '    End If
            'End If

        Next
        If intsavedreccnt > 0 Then
            objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
            ErrLab.Text = objXML.GetLabelName("StatusMessage", "SBA-SUCSAVING")
            ErrLab.Visible = True
            MessageBox1.Alert(ErrLab.Text)
        End If
        'Retrive Call List Assignment data from Server
        RetriveAssignInfo()
    End Function



    Protected Sub markAllButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles markAllButton.Click
        Dim i As Integer
        'max check count is Release Limit
        Dim iMax As Integer = Integer.Parse(releaseLimitBox.Text)
        Dim checkBoxFlag As CheckBox
        Dim dsCLA As DataSet = Session("CallListAssignment")

        Dim chkCount As Integer = 0


        For i = 0 To dsCLA.Tables(0).Rows.Count - 1
            If dsCLA.Tables(0).Rows(i).Item(0) = True Then
                chkCount += 1
            End If
        Next


        If dsCLA Is Nothing Then
            objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
            ErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_FUN_CALLLIST_RERR")
            ErrLab.Visible = True
            Return
        End If

        If dsCLA.Tables.Count = 0 Then
            objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
            ErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_FUN_CALLLIST_RERR")
            ErrLab.Visible = True
            Return
        End If

        If dsCLA.Tables(0).Rows.Count = 0 Then
            objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
            ErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_FUN_CALLLIST_RERR")
            ErrLab.Visible = True
            Return
        End If

        'If callListGridView.Rows.Count > iMax Then
        If dsCLA.Tables(0).Rows.Count > iMax Then
            objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
            Dim msgInfo As String = objXML.GetLabelName("StatusMessage", "BB_FUN_CALLLIST_MARKCOUNT") + iMax.ToString
            MessageBox2.Confirm(msgInfo)
        Else
            iMax = dsCLA.Tables(0).Rows.Count         'callListGridView.Rows.Count
        End If
        For i = 0 To iMax - 1
            'checkBox = callListGridView.Rows(i).FindControl("GridViewCheck")
            'checkBox.Checked = True
            If dsCLA.Tables(0).Rows(i).Item("MROU_CSTAT").ToString.ToUpper <> "CONFIRMED" Then
                dsCLA.Tables(0).Rows(i).Item(0) = True
            End If
        Next

        callListGridView.DataSource = dsCLA.Tables(0)
        'callListGridView.DataBind()
        If iMax > callListGridView.PageSize Then
            iMax = callListGridView.PageSize - 1
        Else
            iMax = iMax - 1
        End If
        chkCount = 0
        For i = 0 To iMax
            checkBoxFlag = callListGridView.Rows(i).FindControl("GridViewCheck")
            If dsCLA.Tables(0).Rows(i).Item("MROU_CSTAT").ToString.ToUpper <> "CONFIRMED" Then
                checkBoxFlag.Checked = True
                chkCount += 1
            End If
        Next
        If UCase(Me.releasedDropDownList.SelectedValue) = "N" Then
            Me.totalReleaseBox.Text = String.Format("{0:f0} ", chkCount)
        End If

    End Sub

    Protected Sub callListGridView_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles callListGridView.DataBound
        objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")

        Dim i As Integer
        For i = 0 To callListGridView.Rows.Count - 1
            callListGridView.Rows(i).Cells(4).Text = Left(callListGridView.Rows(i).Cells(4).Text, 10)
            callListGridView.Rows(i).Cells(5).Text = Left(callListGridView.Rows(i).Cells(5).Text, 10)
            callListGridView.Rows(i).Cells(6).Text = Left(callListGridView.Rows(i).Cells(6).Text, 10)

            callListGridView.Rows(i).Cells(9).Text = objXML.GetLabelName("StatusMessage", callListGridView.Rows(i).Cells(9).Text)
        Next
    End Sub

    Protected Sub callListGridView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles callListGridView.PageIndexChanging
        callListGridView.PageIndex = e.NewPageIndex
        System.Threading.Thread.CurrentThread.CurrentCulture = dataStyle

        Dim dsCLA As DataSet = Session.Contents("CallListAssignment")

        If dsCLA Is Nothing Then
            objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
            ErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_FUN_CALLLIST_RERR")
            ErrLab.Visible = True
            Return
        End If

        If dsCLA.Tables.Count = 0 Then
            objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
            ErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_FUN_CALLLIST_RERR")
            ErrLab.Visible = True
            Return
        End If

        callListGridView.DataSource = dsCLA
        callListGridView.DataBind()
        callListGridView.Columns(10).Visible = False
        If (dsCLA.Tables(0).Rows.Count > 0) Then
            DisplayGridViewHeader()
        End If
    End Sub

    Protected Sub printButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles printButton.Click
        PrintData()
        CrystalReportViewer.Visible = True
    End Sub

    Private Sub RetriveAssignInfo()
        'Retrive data from Database Server

        saveButton.Enabled = False
        markAllButton.Enabled = False
        printButton.Enabled = False

        If stateFDropDownList.SelectedIndex > -1 Then
            callList.StateFrom = stateFDropDownList.SelectedItem.Value
        Else
            callList.StateFrom = ""
        End If

        If stateTDropDownList.SelectedIndex > -1 Then
            callList.StateTo = stateTDropDownList.SelectedItem.Value
        Else
            callList.StateTo = ""
        End If

        If scFDropDownList.SelectedIndex > -1 Then
            callList.ServiceCenterFrom = scFDropDownList.SelectedItem.Value
        Else
            callList.ServiceCenterFrom = ""
        End If

        If scTDropDownList.SelectedIndex > -1 Then
            callList.ServiceCenterTo = scTDropDownList.SelectedItem.Value
        Else
            callList.ServiceCenterTo = ""
        End If

        If areaFDropDownList.SelectedIndex > -1 Then
            callList.AreaID = areaFDropDownList.SelectedItem.Value
        Else
            callList.AreaID = ""
        End If

        If cboAreaIdTo.SelectedIndex > -1 Then
            callList.AreaIDTo = cboAreaIdTo.SelectedItem.Value
        Else
            callList.AreaIDTo = ""
        End If



        If stDropDownList.SelectedIndex > 0 Then
            callList.ServiceType = stDropDownList.SelectedItem.Value
        Else
            callList.ServiceType = "ALL"
        End If

        If releasedDropDownList.SelectedIndex < 2 Then
            callList.Released = releasedDropDownList.SelectedItem.Value
        Else
            callList.Released = "%ALL%"
        End If

        callList.frmDueDate = txtFrmDueDate.Text.ToString()
        callList.toDueDate = txtToDueDate.Text.ToString()

        'If reminderYear.Text.Length > 0 Then
        '    Try
        '        callList.ReminderYear = Integer.Parse(reminderYear.Text).ToString
        '    Catch ex As Exception
        '        callList.ReminderYear = "0000"
        '    End Try
        'Else
        '    callList.ReminderYear = "0000"
        'End If


        'callList.Operand = Me.cboOperation.SelectedItem.Text

        'If reminderMonth.Text.Length > 0 Then
        '    Try
        '        callList.ReminderMonth = Integer.Parse(reminderMonth.Text).ToString
        '    Catch ex As Exception
        '        callList.ReminderMonth = "00"
        '    End Try
        'Else
        '    callList.ReminderMonth = "00"
        'End If

        'set Query order condition
        callList.SortByFirst = ""
        callList.SortBySecond = ""

        Select Case sortDropDownList1.SelectedIndex
            Case 0
                callList.SortByFirst = "CustomerIDandName"        '"mf.MCUS_CUSPF, mf.MCUS_CUSID, mf.MCUS_ENAME"
            Case 1
                callList.SortByFirst = "MROU_MODID"
            Case 2
                callList.SortByFirst = "MADR_AREID"
            Case 3
                'callList.SortByFirst = "CONVERT(DATETIME, MROU_LSVDT, 103)" '"mr.MROU_LSVDT"
                callList.SortByFirst = "MROU_LSVDT"
            Case 4
                'callList.SortByFirst = "CONVERT(DATETIME, MROU_NCADT, 103)" '"mr.MROU_NCADT"
                callList.SortByFirst = "MROU_NCADT"
            Case 5
                'callList.SortByFirst = "CONVERT(DATETIME, MROU_NSVDT, 103)" '"mr.MROU_NSVDT"
                callList.SortByFirst = " MROU_NSVDT"
            Case 6
                callList.SortByFirst = "MROU_RELFG"
            Case 7
                callList.SortByFirst = "MROU_BATCH"
            Case 8
                callList.SortByFirst = "MROU_CSTAT"
        End Select

        Select Case sortDropDownList2.SelectedIndex
            Case 0
                callList.SortBySecond = "CustomerIDandName"
            Case 1
                callList.SortBySecond = "MROU_MODID"
            Case 2
                callList.SortBySecond = "MADR_AREID"
            Case 3
                'callList.SortBySecond = "CONVERT(DATETIME, MROU_LSVDT, 103) as MROU_LSVDT " '"mr.MROU_LSVDT"
                callList.SortBySecond = "MROU_LSVDT"
            Case 4
                'callList.SortBySecond = "CONVERT(DATETIME, MROU_NCADT, 103)" '"mr.MROU_NCADT"
                callList.SortBySecond = "MROU_NCADT"
            Case 5
                'callList.SortBySecond = "CONVERT(DATETIME, MROU_NSVDT, 103)" '"mr.MROU_NSVDT"
                callList.SortBySecond = "MROU_NSVDT"
            Case 6
                callList.SortBySecond = "MROU_RELFG"
            Case 7
                callList.SortBySecond = "MROU_BATCH"
            Case 8
                callList.SortBySecond = "MROU_CSTAT"
        End Select

        strUserID = Session("userID").ToString
        If strUserID Is Nothing Then
            objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
            ErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_FUN_CALLLIST_LOGIN")
            ErrLab.Visible = True
            Return
        End If

        callList.ModiBy = strUserID
        callList.AssignTo = userDropDownList.SelectedItem.Value
        callList.ClassID = drpProdClass.SelectedItem.Value


        Dim dsCLA As DataSet = callList.RetriveAppointmentInfo()

        If dsCLA Is Nothing Then
            objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
            ErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_FUN_CALLLIST_RERR")
            ErrLab.Visible = True
            Return
        End If

        If dsCLA.Tables.Count = 0 Then
            objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
            ErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_FUN_CALLLIST_RERR")
            ErrLab.Visible = True
            Return
        End If

        Session("CallListAssignment") = dsCLA
        'Dim dbTemp As New DataTable
        'dbTemp = dsCLA.Tables(0).Copy
        'dbTemp.Columns.Remove("MROU_ROUID")
        If dsCLA.Tables.Count > 0 Then
            callListGridView.DataSource = dsCLA
            callListGridView.DataBind()
            callListGridView.Columns(10).Visible = False
        End If

        If dsCLA.Tables(0).Rows.Count > 0 Then
            DisplayGridViewHeader()
        End If

        If dsCLA.Tables(0).Rows.Count = 0 Then
            Me.txtTotalRecord.Text = 0
            objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
            ErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_FUN_CALLLIST_RERR")
            ErrLab.Visible = True
            Return
        End If

        If dsCLA.Tables(1).Rows.Count > 0 Then
            Me.txtTotalRecord.Text = dsCLA.Tables(1).Rows(0).Item(0)
        End If

        Dim i As Integer
        Dim iTotalRelease As Integer = 0
        'Display checkbox 
        For i = 0 To dsCLA.Tables(0).Rows.Count - 1

            If dsCLA.Tables(0).Rows(i).Item("MROU_RELFG") = "Y" Then
                iTotalRelease = iTotalRelease + 1
            End If
        Next



        'dsCLA.Tables(0).Rows(i).Item("MROU_LSVDT") = Left(dsCLA.Tables(0).Rows(i).Item("MROU_LSVDT"), 10)
        'dsCLA.Tables(0).Rows(i).Item("MROU_NCADT") = Left(dsCLA.Tables(0).Rows(i).Item("MROU_NCADT"), 10)
        'dsCLA.Tables(0).Rows(i).Item("MROU_NSVDT") = Left(dsCLA.Tables(0).Rows(i).Item("MROU_NSVDT"), 10)


        'Display Total Release
        totalReleaseBox.Text = iTotalRelease

        'saveButton.Enabled = ((dsCLA.Tables(0).Rows.Count > 0) And (releasedDropDownList.SelectedIndex <> 2) And _
        '                (userDropDownList.SelectedIndex <> 0))
        'markAllButton.Enabled = ((dsCLA.Tables(0).Rows.Count > 0) And (releasedDropDownList.SelectedIndex <> 2))
        'printButton.Enabled = (dsCLA.Tables(0).Rows.Count > 0)

        Dim arrayPurview = New Boolean() {False, False, False, False}
        arrayPurview = clsUserAccessGroup.GetUserPurview(Session("accessgroup"), "34")

        If UCase(Me.releasedDropDownList.SelectedValue) <> "ALL" Then
            If arrayPurview(1) = True Then saveButton.Enabled = True
            Me.markAllButton.Enabled = True
        End If
    End Sub

    Private Sub PrintData()
        Dim dsReport As DataSet = Session.Contents("CallListAssignment")
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle


        If dsReport Is Nothing Then
            objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
            ErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_FUN_CALLLIST_RERR")
            ErrLab.Visible = True
            Return
        End If

        If dsReport.Tables.Count = 0 Then
            objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
            ErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_FUN_CALLLIST_RERR")
            ErrLab.Visible = True
            Return
        End If

        If dsReport.Tables(0).Rows.Count = 0 Then
            objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
            ErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_FUN_CALLLIST_RERR")
            ErrLab.Visible = True
            Return
        End If



        Try
            callListReportDoc.Load(MapPath("CLACrystalReport.rpt"))
            ErrLab.Visible = False
        Catch ex As Exception
            objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
            'Dim strTitle As String = objXML.GetLabelName("StatusMessage", "BB_FUN_CALLLIST_INFO")
            'MsgBox(msgInfo, MsgBoxStyle.Information, strTitle)
            ErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_FUN_CALLLIST_LOADFAILED")
            ErrLab.Visible = True
            Return
        End Try
        callListReportDoc.SetDataSource(dsReport.Tables(0))

        objXML.XmlFile = ConfigurationManager.AppSettings("XmlFilePath")
        callListReportDoc.SetParameterValue("CustomerIDName", objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_PRI_IDNAME"))
        callListReportDoc.SetParameterValue("MODID", objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_PRI_MODELRO"))
        callListReportDoc.SetParameterValue("AREID", objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_PRI_AREAID"))
        callListReportDoc.SetParameterValue("LSVDT", objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_PRI_LSDT"))
        callListReportDoc.SetParameterValue("NCADT", objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_PRI_NCDT"))
        callListReportDoc.SetParameterValue("NSVDT", objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_PRI_DUDT"))
        callListReportDoc.SetParameterValue("RELEASED", objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_PRI_REFG"))
        callListReportDoc.SetParameterValue("BATCH", objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_PRI_ASSI"))
        callListReportDoc.SetParameterValue("STATUS", objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_PRI_STAT"))
        callListReportDoc.SetParameterValue("NUMBER", objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_PRI_NO"))


        CrystalReportViewer.ReportSource = callListReportDoc

        Session.Contents("PrintDataFlag") = "1"
    End Sub

    Private Sub SetUserPurview()
        Dim arrayPurview = New Boolean() {False, False, False, False}
        arrayPurview = clsUserAccessGroup.GetUserPurview(Session("accessgroup"), "34")

        If lblRelease.Text <> "ALL" Then saveButton.Enabled = arrayPurview(1)
        retriveButton.Enabled = arrayPurview(2)
        printButton.Enabled = arrayPurview(3)
    End Sub

    Protected Sub CheckBox_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim checkboxView As CheckBox = CType(sender, CheckBox)
        Dim i As Integer = CType(checkboxView.ToolTip, Integer)

        Dim dsCLA As DataSet = Session("CallListAssignment")
        Dim ctr As Integer = 0

        Dim iMax As Integer = Integer.Parse(releaseLimitBox.Text)
        Dim chkCount As Integer = 0

        For ctr = 0 To dsCLA.Tables(0).Rows.Count - 1
            If dsCLA.Tables(0).Rows(ctr).Item(0) = True Then
                chkCount += 1
            End If
        Next

        If UCase(Me.releasedDropDownList.SelectedValue) = "N" Then
            Me.totalReleaseBox.Text = String.Format("{0:f0} ", chkCount)
            If checkboxView.Checked = True Then
                chkCount += 1
                If chkCount > iMax Then
                    objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
                    Dim msgInfo As String = objXML.GetLabelName("StatusMessage", "BB_FUN_CALLLIST_MARKCOUNT") + iMax.ToString
                    MessageBox2.Confirm(msgInfo)
                    checkboxView.Checked = False
                    Exit Sub
                End If
            Else
                chkCount -= 1
            End If

        End If

        dsCLA.Tables(0).Rows(i).Item(0) = checkboxView.Checked

        If UCase(Me.releasedDropDownList.SelectedValue) = "N" Then
            Me.totalReleaseBox.Text = String.Format("{0:f0} ", chkCount)
        End If
        'saveButton.Enabled = ((dsCLA.Tables(0).Rows.Count > 0) And (releasedDropDownList.SelectedIndex <> 2) And _
        '(userDropDownList.SelectedIndex <> 0) And saveButton.Enabled)
    End Sub

    Protected Sub userDropDownList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles userDropDownList.SelectedIndexChanged
        Dim dsCLA As DataSet = Session("CallListAssignment")
        If dsCLA Is Nothing Then
            objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
            ErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_FUN_CALLLIST_RERR")
            ErrLab.Visible = True
            'saveButton.Enabled = False
            Return
        End If

        If dsCLA.Tables.Count = 0 Then
            objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
            ErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_FUN_CALLLIST_RERR")
            ErrLab.Visible = True
            'saveButton.Enabled = False
            Return
        End If

        'saveButton.Enabled = ((dsCLA.Tables(0).Rows.Count > 0) And (releasedDropDownList.SelectedIndex <> 2) And _
        '(userDropDownList.SelectedIndex <> 0) And saveButton.Enabled)
    End Sub

    Protected Sub callListGridView_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles callListGridView.RowDataBound
        System.Threading.Thread.CurrentThread.CurrentCulture = dataStyle
        Dim checkboxFlag As CheckBox
        If e.Row.RowType = DataControlRowType.DataRow Then


            checkboxFlag = e.Row.FindControl("GridViewCheck")
            If e.Row.Cells(9).Text.ToUpper <> "CONFIRMED" Then
                checkboxFlag.Enabled = True
            Else
                checkboxFlag.Enabled = False
            End If
        End If
    End Sub

    Protected Sub releasedDropDownList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles releasedDropDownList.SelectedIndexChanged
        If callListGridView.Rows.Count = 0 Then Exit Sub

        Dim arrayPurview = New Boolean() {False, False, False, False}
        arrayPurview = clsUserAccessGroup.GetUserPurview(Session("accessgroup"), "34")

        If lblRelease.Text = "ALL" Then
            saveButton.Enabled = False
            Me.markAllButton.Enabled = False
        Else
            If arrayPurview(1) = True Then saveButton.Enabled = True
            Me.markAllButton.Enabled = True
        End If


    End Sub

    Protected Sub cboOperation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboOperation.SelectedIndexChanged
        'If Me.cboOperation.SelectedIndex = 0 Then
        '    reminderMonth.Text = ""
        '    reminderYear.Text = ""
        'End If
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        callListReportDoc.Dispose()
    End Sub

    Protected Sub saveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveButton.Click
        If UCase(Me.releasedDropDownList.SelectedValue) = "N" Then
            If userDropDownList.SelectedIndex = 0 Then
                objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
                ErrLab.Text = objXML.GetLabelName("StatusMessage", "BB_FUN_CALLLIST_USER")
                ErrLab.Visible = True
                Exit Sub
            Else
                ErrLab.Visible = False
            End If
        End If

        objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
        Dim msgInfo As String = objXML.GetLabelName("StatusMessage", "BB_MASMUAG_QUESAVE")
        MessageBox1.Confirm(msgInfo)
    End Sub

    Protected Sub scFDropDownList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles scFDropDownList.SelectedIndexChanged
        If Not scFDropDownList.SelectedValue = "%" Then
            callList.ServiceCenterFrom = scFDropDownList.SelectedValue
            callList.CtryID = Session("login_ctryID").ToString.ToUpper()
            Dim ds As DataSet = callList.getStateAndArea()

            stateFDropDownList.Items.Clear()
            areaFDropDownList.Items.Clear()
            Dim dt As DataTable
            Dim dt1 As DataTable
            'Dim dr As DataRow
            dt = ds.Tables(0)
            dt1 = ds.Tables(1)
            stateFDropDownList.Items.Add(New ListItem("", ""))
            For i As Integer = 0 To dt.Rows.Count - 1
                Dim statenm As String = dt.Rows(i).Item(0) + "-" + dt.Rows(i).Item(1)
                stateFDropDownList.Items.Add(New ListItem(statenm, dt.Rows(i).Item(0)))
            Next
            areaFDropDownList.Items.Add(New ListItem(objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), "%"))
            For i As Integer = 0 To dt1.Rows.Count - 1
                Dim areanm As String = dt1.Rows(i).Item(0) + "-" + dt1.Rows(i).Item(1)
                areaFDropDownList.Items.Add(New ListItem(areanm, dt1.Rows(i).Item(0)))
            Next

            'stateid = dsCLA.Tables(0).Rows(i).Item(0)

        Else
            strUserID = Session("userID").ToString
            callList.ModiBy = strUserID
            Dim dsCLA As DataSet = callList.PreQuery()
            stateFDropDownList.Items.Clear()
            areaFDropDownList.Items.Clear()
            stateFDropDownList.Items.Add(New ListItem("", ""))
            'stateTDropDownList.Items.Add(New ListItem("", ""))
            Dim stateid As String
            Dim statenm As String
            For i As Integer = 0 To dsCLA.Tables(0).Rows.Count - 1
                statenm = dsCLA.Tables(0).Rows(i).Item(0) + "-" + dsCLA.Tables(0).Rows(i).Item(1)
                stateid = dsCLA.Tables(0).Rows(i).Item(0)
                stateFDropDownList.Items.Add(New ListItem(statenm.ToString(), stateid.ToString()))
                'stateTDropDownList.Items.Add(New ListItem(statenm.ToString(), stateid.ToString()))
            Next
            areaFDropDownList.Items.Add(New ListItem(objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), ""))
            'cboAreaIdTo.Items.Add(New ListItem(objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL")))
            Dim areaid As String
            Dim areanm As String
            For ii As Integer = 0 To dsCLA.Tables(2).Rows.Count - 1
                areanm = dsCLA.Tables(2).Rows(ii).Item(0) + "-" + dsCLA.Tables(2).Rows(ii).Item(1)
                areaid = dsCLA.Tables(2).Rows(ii).Item(0)
                areaFDropDownList.Items.Add(New ListItem(areanm.ToString(), areaid.ToString()))
                'cboAreaIdTo.Items.Add(New ListItem(areanm.ToString(), areaid.ToString()))
            Next
        End If


    End Sub

    Protected Sub scTDropDownList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles scTDropDownList.SelectedIndexChanged
        If Not scTDropDownList.SelectedValue = "%" Then
            callList.ServiceCenterFrom = scTDropDownList.SelectedValue
            callList.CtryID = Session("login_ctryID").ToString.ToUpper()
            Dim ds As DataSet = callList.getStateAndArea()
            stateTDropDownList.Items.Clear()
            cboAreaIdTo.Items.Clear()
            Dim dt As DataTable
            Dim dt1 As DataTable
            'Dim dr As DataRow
            dt = ds.Tables(0)
            dt1 = ds.Tables(1)
            stateTDropDownList.Items.Add(New ListItem("", ""))
            For i As Integer = 0 To dt.Rows.Count - 1
                Dim statenm As String = dt.Rows(i).Item(0) + "-" + dt.Rows(i).Item(1)
                stateTDropDownList.Items.Add(New ListItem(statenm, dt.Rows(i).Item(0)))
            Next
            cboAreaIdTo.Items.Add(New ListItem(objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), "%"))
            For i As Integer = 0 To dt1.Rows.Count - 1
                Dim areanm As String = dt1.Rows(i).Item(0) + "-" + dt1.Rows(i).Item(1)
                cboAreaIdTo.Items.Add(New ListItem(areanm, dt1.Rows(i).Item(0)))
            Next

        Else
            strUserID = Session("userID").ToString
            callList.ModiBy = strUserID
            Dim dsCLA As DataSet = callList.PreQuery()
            stateTDropDownList.Items.Clear()
            cboAreaIdTo.Items.Clear()
            'stateFDropDownList.Items.Add(New ListItem("", ""))
            stateTDropDownList.Items.Add(New ListItem("", ""))
            Dim stateid As String
            Dim statenm As String
            For i As Integer = 0 To dsCLA.Tables(0).Rows.Count - 1
                statenm = dsCLA.Tables(0).Rows(i).Item(0) + "-" + dsCLA.Tables(0).Rows(i).Item(1)
                stateid = dsCLA.Tables(0).Rows(i).Item(0)
                'stateFDropDownList.Items.Add(New ListItem(statenm.ToString(), stateid.ToString()))
                stateTDropDownList.Items.Add(New ListItem(statenm.ToString(), stateid.ToString()))
            Next
            'areaFDropDownList.Items.Add(New ListItem(objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL")))
            cboAreaIdTo.Items.Add(New ListItem(objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), ""))
            Dim areaid As String
            Dim areanm As String
            For ii As Integer = 0 To dsCLA.Tables(2).Rows.Count - 1
                areanm = dsCLA.Tables(2).Rows(ii).Item(0) + "-" + dsCLA.Tables(2).Rows(ii).Item(1)
                areaid = dsCLA.Tables(2).Rows(ii).Item(0)
                'areaFDropDownList.Items.Add(New ListItem(areanm.ToString(), areaid.ToString()))
                cboAreaIdTo.Items.Add(New ListItem(areanm.ToString(), areaid.ToString()))
            Next
        End If

    End Sub

    Protected Sub stateFDropDownList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles stateFDropDownList.SelectedIndexChanged
        areaFDropDownList.Items.Clear()
        If Not stateFDropDownList.SelectedValue = "" Then
            callList.StateFrom = stateFDropDownList.SelectedValue
            callList.ServiceCenterFrom = scFDropDownList.SelectedValue
        Else
            callList.StateFrom = "%"
            callList.ServiceCenterFrom = scFDropDownList.SelectedValue
            areaFDropDownList.Items.Add(New ListItem(objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), ""))
        End If

        Dim ds As DataSet = callList.getAreafromState()
        Dim dt As DataTable = ds.Tables(0)
        'Dim dr As DataRow
        Dim areanm As String
        areaFDropDownList.Items.Add(New ListItem(objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), "%"))
        For i As Integer = 0 To dt.Rows.Count - 1
            areanm = dt.Rows(i).Item(0) + "-" + dt.Rows(i).Item(1)
            areaFDropDownList.Items.Add(New ListItem(areanm, dt.Rows(i).Item(0)))
        Next

        'Dim areanm As String = ds.Tables(0).Rows(0).Item(0) + "-" + ds.Tables(0).Rows(0).Item(1)
        'areaFDropDownList.Items.Add(New ListItem(areanm, ds.Tables(0).Rows(0).Item(1)))
    End Sub

    Protected Sub stateTDropDownList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles stateTDropDownList.SelectedIndexChanged
        cboAreaIdTo.Items.Clear()
        If Not stateTDropDownList.SelectedValue = "" Then
            callList.StateFrom = stateTDropDownList.SelectedValue
            callList.ServiceCenterFrom = scTDropDownList.SelectedValue
        Else
            callList.StateFrom = "%"
            callList.ServiceCenterFrom = scTDropDownList.SelectedValue
            cboAreaIdTo.Items.Add(New ListItem(objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), ""))
        End If
        Dim ds As DataSet = callList.getAreafromState()

        Dim dt As DataTable = ds.Tables(0)
        'Dim dr As DataRow
        Dim areanm As String
        cboAreaIdTo.Items.Add(New ListItem(objXML.GetLabelName("EngLabelMsg", "BB_FUN_CLA_ALL"), "%"))
        For i As Integer = 0 To dt.Rows.Count - 1
            areanm = dt.Rows(i).Item(0) + "-" + dt.Rows(i).Item(1)
            cboAreaIdTo.Items.Add(New ListItem(areanm, dt.Rows(i).Item(0)))
        Next
    End Sub
End Class

