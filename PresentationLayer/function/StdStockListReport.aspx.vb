Imports System.Data
Imports BusinessEntity
Partial Class PresentationLayer_function_StdStocklistReport
    Inherits System.Web.UI.Page
    Dim clsreport As New ClsCommonReport

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim DSSSL As New DataSet
        Dim CLSSTOCKLIST As New clsServiceBillUpdate


        Dim trnno As String = Request.QueryString("trnno")
        Dim trnty As String = Request.QueryString("trnty")
        Dim svcid As String = Request.QueryString("svcid")
        Dim tchid As String = Request.QueryString("tchid")
        Dim usrid As String = Request.QueryString("usrid")


        DSSSL = CLSSTOCKLIST.getStdStockListByTechIDRPT(trnty, trnno, svcid, tchid, usrid)

        If DSSSL.Tables.Count = 0 Then
            Me.lblMsg.Text = "There's no record available!"
            Me.lblMsg.Visible = True
        ElseIf DSSSL.Tables(0).Rows.Count = 0 Then
            Me.lblMsg.Text = "There's no record available!"
            Me.lblMsg.Visible = True
        Else
            Me.lblMsg.Visible = False
            DSSSL.Tables(0).TableName = "SSLREPORT"
        End If

        Dim strHead As Array = Array.CreateInstance(GetType(String), 46)
        strHead(0) = "View Standard Stock List"

        clsreport.ReportFileName = "rptStandardStockList.rpt"
        clsreport.SetReport(Me.crvStdStockList, DSSSL, strHead)


    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        clsreport.UnloadReport()
        crvStdStockList.Dispose()
    End Sub
End Class
