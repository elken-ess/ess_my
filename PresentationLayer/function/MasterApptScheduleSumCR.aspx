<%@ Page Language="VB" AutoEventWireup="false" CodeFile="MasterApptScheduleSumCR.aspx.vb" Inherits="PresentationLayer.function.PresentationLayer_function_MasterApptScheduleSumCR" %>
<%@ Register TagPrefix="cc1" Namespace="JCalendar" Assembly="JCalendar" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <link href="../css/style.css" type="text/css" rel="stylesheet" />
    <link href="../css/default.css" rel="stylesheet" type="text/css" />
    <script language="JavaScript" src="../js/common.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
       <table id="Table1" style="width: 100%; border: 0;">
            <tr>
                <td style="width: 100%">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title1.gif" width="5" />
                            </td>
                            <td background="../graph/title_bg.gif" class="style2" width="98%" style="width: 100%">
                                <asp:Label ID="titleLabCR" runat="server" Text="Label"/>
                            </td>
                            <td align="LEFT" background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title_2.gif" width="5" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100%">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" class="style2" width="98%">
                                <font color="red" style="width: 100%">
                                    <asp:Label ID="errlab" runat="server" Text="Label" Visible="false"></asp:Label></font>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100%">
                    <table id="Table2" bgcolor="#b7e6e6" border="0" cellpadding="2" cellspacing="1"
                        style="width: 100%">
                        <tr bgcolor="#ffffff">
                            <td style="text-align:left; width: 24%; height: 28px;">
                                <asp:Label ID="lblApptDateCR" runat="server" Text="Label"/>
                            </td>
                            <td align="LEFT" style="width: 45px; height: 28px;">
                                <asp:Label ID="lblFromCR" runat="server" Text="Label"></asp:Label>
                            </td>
                            <td align="left" style="width: 34%; height: 28px;">
                                <asp:TextBox ID="txtFromAptDateCR" runat="server" CssClass="textborder" ReadOnly="false"
                                             MaxLength="10"/>
                                <asp:HyperLink ID="HypCalFromCR" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                                    ToolTip="Choose a Date">Choose a Date</asp:HyperLink>
                                <cc1:JCalendar ID="JCalendar3" runat="server" ControlToAssign="txtFromAptDateCR" ImgURL="~/PresentationLayer/graph/calendar.gif"
                                    Visible="false" />
                                &nbsp;
                            </td>
                            <td align="LEFT" style="width: 9%; height: 28px;">
                                <asp:Label ID="lblToCR" runat="server" Text="Label"></asp:Label>
                            </td>
                            <td align="left" style="width: 40%; height: 28px">
                                <asp:TextBox ID="txtToAptDateCR" runat="server" CssClass="textborder" ReadOnly="false"
                                    MaxLength="10"></asp:TextBox>
                                <asp:HyperLink ID="HypCalToCR" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                                    ToolTip="Choose a Date">Choose a Date</asp:HyperLink>
                                <cc1:JCalendar ID="JCalendar4" runat="server" ControlToAssign="txtToAptDateCR" ImgURL="~/PresentationLayer/graph/calendar.gif"
                                    Visible="false" />
                                &nbsp;
                            </td>
                        </tr>
                        <tr style="background-color: #ffffff;">
                            <td style="width: 24%; height: 28px; text-align: left">
                                <asp:Label ID="lblSVCCenter" runat="server" Text="Label"/></td>
                            <td style="width: 45px; height: 28px; text-align: left;">
                                <asp:Label runat="server" ID="lblFromSVC"/>
                            </td>
                            <td style="width: 34%; height: 28px; text-align: left;">
                                <asp:TextBox runat="server" ID="txtFromSVCCenter" CssClass="textborder"/>
                            </td>
                            <td style="width: 9%; height: 28px; text-align: left;">
                                <asp:Label runat="server" ID="lblToSVC" Text="Label"/>
                            </td>
                            <td style="width: 40%; height: 28px; text-align: left;">
                                <asp:TextBox runat="server" ID="txtToSVCCenter" CssClass="textborder"/>
                            </td>
                        </tr>
                        <tr style="background-color: #ffffff">
                            <td colspan="5" style="text-align: left">
                                <asp:LinkButton ID="lnkViewReportCR" runat="server" Text="ViewReport" OnClick="lnkViewReport_Click"/>
                            </td>
                        </tr>
                        
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 37px; width: 100%;">
                    <table style="border: 0; padding: 1; border-spacing: 1">
                        <tr>
                            <td style="width: 20%; height: 4px; text-align: left">
                                <asp:Repeater runat="server" ID="rptCR">
                                    <ItemTemplate>
                                        <table>
                                            <tr>
                                                <td>
                                                    <b>Appointment Date: <%#Container.DataItem%>
                                                    </b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="GridView_CR" runat="server">
                                                        <EmptyDataTemplate>
                                                            No record found
                                                        </EmptyDataTemplate>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                        
                    </table>
                </td>
            </tr>
        </table>
    </div>
    
    </form>
</body>
</html>
