Imports System.Collections.Generic
Imports System.Data
Imports BusinessEntity

Namespace PresentationLayer.function
    Partial Class PresentationLayer_function_MasterApptScheduleSumSVC
        Inherits Page

#Region "Declaration"
        Private _mastAppt As New ClsMastAppt()
        Private ReadOnly _datestyle As Globalization.CultureInfo = New Globalization.CultureInfo("en-CA")
        Private _xml As New clsXml()
#End Region

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load

            Threading.Thread.CurrentThread.CurrentCulture = _datestyle

            If Not IsPostBack Then
                PopulateLabels()

                txtFromAptDate.Text = DateTime.Now.ToShortDateString()
                txtToAptDate.Text = DateTime.Now.AddDays(7).ToShortDateString()
            End If

            HypCalFrom.NavigateUrl = "javascript:DoCal(document.form1.txtFromAptDate);"
            HypCalTo.NavigateUrl = "javascript:DoCal(document.form1.txtToAptDate);"
        End Sub

        Private Sub PopulateLabels()
            Const strFrom As String = "From"
            Const strTo As String = "To"
            lblFrom.Text = strFrom
            lblTo.Text = strTo

            titleLab.Text = _xml.GetLabelName("EngLabelMsg", "BB-MASTERAPPOINTMENTSUMMARY-SVTITLE")
            lblApptDateFrom.Text = _xml.GetLabelName("EngLabelMsg", "BB-RPTSVOS-0020")
        End Sub

        Private Sub GenerateReport(ByVal startDate As DateTime, ByVal endDate As DateTime)
            Dim ts As TimeSpan = endDate.Subtract(startDate)
            Dim listDate As New List(Of String)

            For i As Integer = 0 To ts.Days
                listDate.Add(startDate.AddDays(i).ToLongDateString() + " " + startDate.AddDays(i).DayOfWeek.ToString())
            Next

            rptSVC.DataSource = listDate
            rptSVC.DataBind()
        End Sub

        Protected Sub rptSVC_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs) Handles rptSVC.ItemDataBound
            Dim selectedDate As DateTime
            Dim ds As New DataSet()
            DateTime.TryParse(e.Item.DataItem.ToString(), selectedDate)

            _mastAppt.SelectedDate = selectedDate
            _mastAppt.ReportName = "BB_RPTRetrieveJobApptPerDay"
            ds = _mastAppt.RetrieveMasterAppointmentBySVC()

            If ds.Tables.Count > 0 Then
                Dim grd As GridView = CType(e.Item.FindControl("GridView_SVC"), GridView)
                AddHandler grd.RowDataBound, AddressOf GridView_SVC_RowDataBound
                grd.DataSource = PivotDataTable(ds.Tables(0), "SVC_Name", "SS_AS", "Value", False, "SERVICE CENTER")
                grd.DataBind()
            End If

        End Sub

        Private Sub GridView_SVC_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs)

            If e.Row.RowType = DataControlRowType.DataRow Then
                For i As Integer = 1 To e.Row.Cells.Count - 1
                    Dim appt As String() = e.Row.Cells(i).Text.Split(CType("/", Char))

                    If appt.Length = 2 Then
                        If appt(0) <> appt(1) Then
                            e.Row.Cells(i).ForeColor = Drawing.Color.Red
                        End If
                    End If

                Next
            End If

        End Sub

        Private Function PivotDataTable(ByVal origTable As DataTable, ByVal colHeader As String, ByVal rowHeader As String, _
                                                ByVal value As String, ByVal hasTotal As Boolean, ByVal firstColHeader As String) As DataTable

            Dim newTable As New DataTable("PivotTable")
            Dim dr As DataRow

            Dim listCols As New List(Of String)
            Dim listRows As New List(Of String)
            'Add Columns to new Table
            For i As Integer = 0 To origTable.Rows.Count - 1
                If Not listCols.Contains(origTable.Rows(i)(colHeader).ToString()) Then
                    listCols.Add(origTable.Rows(i)(colHeader).ToString())
                End If

                If Not listRows.Contains(origTable.Rows(i)(rowHeader).ToString()) Then
                    listRows.Add(origTable.Rows(i)(rowHeader).ToString())
                End If
            Next
            listCols.Sort()
            listCols.Insert(0, firstColHeader)

            If hasTotal Then
                listCols.Add("Total")
            End If

            listRows.Sort()

            ' Fix table
            For i As Integer = 1 To listCols.Count
                newTable.Columns.Add(listCols(i - 1))
            Next

            For rows As Integer = 0 To listRows.Count - 1
                dr = newTable.NewRow()
                Dim total As Integer = 0
                For cols As Integer = 0 To listCols.Count - 1

                    If cols = 0 Then
                        dr(cols) = listRows(rows)
                    Else
                        ' find records in datatable
                        Dim foundRows() As DataRow
                        foundRows = origTable.Select(colHeader + " = '" + newTable.Columns(cols).ColumnName + "' AND " + rowHeader + " = '" + listRows(rows) + "'")

                        ' with total
                        If hasTotal Then
                            ' retrieve values from datarows then add
                            If foundRows.Length > 0 Then
                                Dim sum As Integer = 0
                                For i As Integer = 0 To foundRows.Length - 1
                                    sum += CType(foundRows(i).Item(value), Integer)
                                Next
                                dr(cols) = sum.ToString()
                                total += sum
                            Else
                                dr(cols) = "0"
                            End If

                            'compute for total
                            If cols = listCols.Count - 1 Then
                                dr(cols) = total.ToString()
                            End If

                        Else
                            dr(cols) = foundRows(0).Item(value).ToString()
                        End If

                    End If

                Next
                newTable.Rows.Add(dr)
            Next

            Return newTable
        End Function

        Protected Sub lnkViewReport_Click(ByVal sender As Object, ByVal e As EventArgs)
            Dim startDate As DateTime
            DateTime.TryParse(txtFromAptDate.Text, startDate)

            Dim endDate As DateTime
            DateTime.TryParse(txtToAptDate.Text, endDate)
            GenerateReport(startDate, endDate)
        End Sub
    End Class
End Namespace