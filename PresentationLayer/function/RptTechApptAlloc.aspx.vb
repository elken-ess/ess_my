﻿Imports BusinessEntity
Imports System.Data

Partial Class PresentationLayer_function_RptTechApptAlloc
    Inherits System.Web.UI.Page
    Dim clsReport As New ClsCommonReport

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try
        End If
        viewReport()
    End Sub
    Protected Sub viewReport()
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        'display label message
        Dim objXmlTr As New clsXml()
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        Me.titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_D_TITLE")
        Me.backLink.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-BACK")
        Me.backLink.PostBackUrl = "~/PresentationLayer/function/ApptAlloc.aspx"
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim apptalloc As clsApptAlloc = Session.Contents("apptalloc")
        If apptalloc Is Nothing Then
            Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-D-WARNING4")
            Return
        End If
        Dim result As Integer = 0
        Dim strDate As String = Request.Form("txtApptDate") 'Me.txtApptDate.Text
        Dim strSVC As String = Request.Form("SerCenIDDrop")
        Dim strFromZone As String = Request.Form("txtFromZone")
        Dim strToZone As String = Request.Form("txtFromZone")
        Dim strAppointmentNo As String = Request.Form("txtAppointmentNo")
        Dim strCustomerID As String = Request.Form("txtCustomerID")
        Dim strTechID As String = Session("TechID")
        Dim rptds As DataSet
        If Session.Contents("rptds") Is Nothing Then
            rptds = apptalloc.getPrintTechAppt(strDate, strSVC, _
                                           strFromZone, strToZone, strTechID, result, strAppointmentNo, strCustomerID)
        Else
            rptds = Session.Contents("rptds")
            result = 1
        End If

        Select Case result
            Case -3
                Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-D-WARNING1")
                'Response.Redirect("~/PresentationLayer/error.aspx")
            Case -2
                Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-D-WARNING1")
                'Response.Redirect("~/PresentationLayer/error.aspx")
            Case -1
                Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-D-ERROR3")
                'Response.Redirect("~/PresentationLayer/error.aspx")
            Case 0
                Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-D-WARNING1")
                'Response.Redirect("~/PresentationLayer/error.aspx")
            Case 1
                Dim telDepra As New clsCommonClass()
                If rptds Is Nothing Then
                    Return
                End If
                If rptds.Tables.Count < 1 Then
                    Return
                End If
                For i As Integer = 0 To rptds.Tables(0).Rows.Count - 1
                    rptds.Tables(0).Rows(i).Item(7) = telDepra.passconverttel( _
                                           rptds.Tables(0).Rows(i).ItemArray(7))
                Next
                Session.Contents("rptds") = rptds
                Me.lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-D-SUCCESS3")

                Dim strHead As Array = Array.CreateInstance(GetType(String), 18)
                objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
                strHead(0) = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_D_RPT001")
                'strHead(1) = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_D_RPT002")
                'strHead(2) = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_D_RPT003")
                strHead(1) = strDate
                strHead(2) = strDate

                strHead(3) = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_D_RPT004")
                strHead(4) = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_D_RPT005")
                strHead(5) = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_D_RPT006")
                strHead(6) = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_D_RPT007")
                strHead(7) = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_D_RPT008")
                strHead(8) = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_D_RPT009")
                strHead(9) = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_D_RPT010")
                strHead(10) = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_D_RPT011")
                strHead(11) = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_D_RPT012")
                strHead(12) = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_D_RPT013")
                strHead(13) = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_D_RPT014")
                strHead(14) = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_D_RPT015")
                strHead(15) = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_D_RPT016")
                strHead(16) = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_D_RPT017")
                strHead(17) = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_D_RPT018")

                With clsReport
                    .ReportFileName = "rptTechApptAlloc.rpt"
                    .SetReport(Me.rptTechApptAllocViewer, rptds, strHead)
                End With
        End Select
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        clsReport.UnloadReport()
        rptTechApptAllocViewer.Dispose()
    End Sub
End Class
