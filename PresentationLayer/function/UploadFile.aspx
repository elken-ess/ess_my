﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="UploadFile.aspx.vb" Inherits="PresentationLayer_function_UploadFile"
    EnableEventValidation="false" %>

<%@ Register TagPrefix="cc2" Assembly="JCalendar" Namespace="JCalendar" %>
<%@ Register TagPrefix="cc1" Assembly="MessageBox" Namespace="Utilities" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="head_UploadFile" runat="server">
    <title>Upload File</title>

    <script language="JavaScript" src="../js/common.js"></script>

    <link href="../css/style.css" type="text/css" rel="stylesheet">
</head>
<body>
    <form id="form_UploadFile" runat="server">
        <div style="vertical-align: top">
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td width="1%" background="../graph/title_bg.gif">
                        <img height="24" src="../graph/title1.gif" width="5"></td>
                    <td class="style2" width="98%" background="../graph/title_bg.gif" style="width: 80%">
                        <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
                    <td align="right" width="1%" background="../graph/title_bg.gif">
                        <img height="24" src="../graph/title_2.gif" width="5"></td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td class="style2" width="98%" background="../graph/title_bg.gif" style="height: 14px">
                        <font color="red">
                            <asp:Label ID="errlab" runat="server" Text="Label" Visible="false"></asp:Label>
                        </font>
                    </td>
                </tr>
            </table>
            <fieldset>
                <legend>User Guides </legend>
                <table>
                    <tr>
                        <td>
                            <asp:Label ID="Label_Remark1" runat="server" Text="1) Click Browse button to select excel file with extension .csv only."></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label_Remark2" runat="server" Text="2) Once the file is selected, click on the Upload Button to start upload process."></asp:Label>
                        </td>
                    </tr>
                     <tr>
                        <td>
                            <asp:Label ID="Label_Remark3" runat="server" Text="3) Incentive Calculation file name must be in this format "></asp:Label><asp:Label ID="Label_Remark3a" runat="server" Text="ess_technician_cal_inc_deduction_MMYYYY.csv" Font-Bold="True" Font-Italic="True"></asp:Label><asp:Label ID="Label_Remark3b" runat="server" Text=" if you select Incentive Calculation - Deduction ."></asp:Label>
                        </td>
                    </tr>
                     <tr>
                        <td>
                            <asp:Label ID="Label_Remark4" runat="server" Text="4) Incentive Calculation file name must be in this format "></asp:Label><asp:Label ID="Label_Remark4a" runat="server" Text="ess_technician_cal_inc_demerit_MMYYYY.csv" Font-Bold="True" Font-Italic="True"></asp:Label><asp:Label ID="Label_Remark4b" runat="server" Text=" if you select Incentive Calculation - Demerit ."></asp:Label>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <table>
                 <tr>
                    <td>
                        <asp:Label ID="Label1" runat="server" Text="Upload To"></asp:Label>
                    </td>
                    <td>
                        :
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlUploadTo" runat="server" Width="288px" AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label_SourceFile" runat="server" Text="Source File"></asp:Label>
                    </td>
                    <td>
                        :
                    </td>
                    <td>
                        <asp:FileUpload ID="FileUpload_CSV" runat="server" Width="1000px" TabIndex="1" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label_SheetName" runat="server" Text="Sheet Name" Visible="False"></asp:Label>
                    </td>
                    <td>
                        <!--:-->
                    </td>
                    <td>
                        <asp:TextBox ID="TextBox_SheetName" runat="server" CssClass="textbox_medium" TabIndex="2" Visible="False"></asp:TextBox>
                        <asp:Button ID="Button_Upload" runat="server" Text="Upload" TabIndex="3" />
                    </td>
                </tr>
            </table>
        </div>
        <asp:ValidationSummary ID="ValidationSummary_UploadFIle" runat="server" ShowMessageBox="True"
            ShowSummary="False" />
        <cc1:MessageBox ID="saveMsgBox" runat="server"></cc1:MessageBox>
        <cc1:MessageBox ID="InfoMsgBox" runat="server" />
        <cc1:MessageBox ID="UpdateMsgBox" runat="server" />
    </form>
</body>
</html>
