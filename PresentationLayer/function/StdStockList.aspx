﻿<%@ Page Language="VB" AutoEventWireup="false" EnableEventValidation="false" CodeFile="StdStockList.aspx.vb" Inherits="PresentationLayer_function_StdStockList" %>

<%@ Register Assembly="JCalendar" Namespace="JCalendar" TagPrefix="cc2" %>
<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Standard Stock List</title>
    <link href="../css/style.css" type="text/css" rel="stylesheet"/>
     <style type="text/css">A:link { COLOR: #336666; TEXT-DECORATION: none }
	BODY { FONT-SIZE: 9pt; FONT-FAMILY: "Arial", "Verdana", "Tahoma"; BACKGROUND-COLOR: #ffffff }
	td { FONT-SIZE: 9pt; FONT-FAMILY: Arial,Verdana,Tahoma }
	</style>
	<script language="JavaScript" src="../js/common.js"></script> 
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div style="text-align: left">
        <table cellSpacing="0" cellPadding="0" width="100%" border="0">
		<tr>
		  <td width="1%" background="../graph/title_bg.gif" style="height: 24px"><IMG height="24" src="../graph/title1.gif" width="5"></td>
		  <td class="style2" width="98%" background="../graph/title_bg.gif" style="height: 24px">
             <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
		  <td align="right" width="1%" background="../graph/title_bg.gif" style="height: 24px"><IMG height="24" src="../graph/title_2.gif" width="5"></td>
		</tr>
		<tr>
		  <td></td>
		  <td>
		   <asp:Label ID="lblMessage" runat="server" ForeColor="red" Text=""></asp:Label>
		   </td> 
	   </tr>
	   <tr>
	    <td></td>
	    <td>
            <asp:Label ID="lblNoteUP" runat="server" ForeColor="Red" Text="Label"></asp:Label></td>
	   </tr>
	 </table>
            <table style="width: 100%" bgColor="#b7e6e6" border="0" cellpadding="0">
                <tr bgColor="#ffffff">
                    <td colspan="4" align="right">
                        <asp:Label ID="LblCal" runat="server" Visible="False"></asp:Label>
                        <asp:LinkButton ID="lbtnGenPackList" runat="server" Enabled="False">LinkButton</asp:LinkButton>
                       <asp:LinkButton ID="lbtnAddStdlist" runat="server" Visible="False">LinkButton</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp; 
                       <asp:LinkButton ID="lbtn1stdlist" runat="server">LinkButton</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;
                       <asp:LinkButton ID="lbtnViewExiList" runat="server">LinkButton</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;</td> 
                </tr>
                <tr bgColor="#ffffff">
                    <td style="width: 20%" align="center">
                        <asp:Label ID="lblTransTY" runat="server" Text="Label"></asp:Label>
                        <font color="red">*</font></td>
                    <td style="width: 30%" align="left">
                        <asp:DropDownList ID="ddlTransTY" Width="80%" runat="server" AutoPostBack="True">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="rfvTrTy" runat="server" ControlToValidate="ddlTransTY"
                            ErrorMessage="*" ForeColor="White">*</asp:RequiredFieldValidator></td>
                    <td style="width: 20%" align="center">
                        <asp:Label ID="lblDate" runat="server" Text="Label"></asp:Label><font color="red"></font></td>
                    <td style="width: 30%" align="left">
                        <asp:TextBox ID="txtDate" CssClass="textborder" runat="server" Width="77%" ReadOnly="FALSE" MaxLength =10></asp:TextBox>
                        <cc2:JCalendar ID="MyCalendar" ImgURL="~/PresentationLayer/graph/calendar.gif" ControlToAssign="txtDate" runat="server" Visible="false" />
                        <asp:HyperLink ID="HypCal" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                            ToolTip="Choose a Date">Choose a Date</asp:HyperLink>
                    </td>
                </tr>
                <tr bgColor="#ffffff">
                    <td style="width: 20%; height: 22px;" align="center">
                        <asp:Label ID="lblTech" runat="server" Text="Label"></asp:Label></td>
                    <td style="width: 30%; height: 22px;" align="left">
                        <asp:DropDownList ID="ddlTech" Width="98%" runat="server" AutoPostBack="True">
                        </asp:DropDownList></td>
                    <td style="width: 20%; height: 22px;" align="center">
                        <asp:Label ID="lblSVC" runat="server" Text="Label"></asp:Label>
                    </td>
                    <td style="width: 30%; height: 22px;" align="left">
                        <asp:DropDownList ID="ddlSVC" Width="96%" runat="server" AutoPostBack="True">
                        </asp:DropDownList></td>
                </tr>
                <tr bgColor="#ffffff">
                    <td style="width: 20%; height: 15px;" align="left">
                        <asp:Label ID="lblUpStkList" runat="server" Text="Label"></asp:Label></td>
                    <td colspan="3" align="right" style="height: 15px">
                        <asp:Label ID="lblSTKNO" runat="server" Visible="False"></asp:Label></td>
                </tr>
                 <tr bgColor="#ffffff">
                    <td style="width: 20%; height: 15px;" align="center">
                        <asp:Label ID="LblPriceID" runat="server"></asp:Label></td>
                    <td style="height: 15px">
                        <asp:DropDownList ID="DDPriceID" runat="server" Width="98%" AutoPostBack="True">
                        </asp:DropDownList></td>
                    <td colspan="2" align="left" style="height: 15px" valign="middle">
                        <asp:Panel ID="PnlLoad" runat="server" Visible="False" Width="100%">
                            <asp:FileUpload ID="FileUpload1" runat="server" Width="78%" />&nbsp;
                            <asp:LinkButton ID="lbtnLoad" runat="server">Load</asp:LinkButton>&nbsp;
                            <asp:LinkButton ID="LnkCancel" runat="server">Cancel</asp:LinkButton></asp:Panel>
                        </td>
                </tr>
               <tr bgColor="#ffffff">
                    <td style="width: 20%; height: 26px;" align="center">
                        <asp:Label ID="lblPartCode" runat="server" Text="Label"></asp:Label></td>
                    <td style="width: 30%; height: 26px;" align="left">
                        <asp:TextBox ID="txtPartCode" CssClass="textborder" runat="server" Width="50%" AutoPostBack="True" MaxLength="10"></asp:TextBox>&nbsp;<asp:Button
                            ID="BtnPartSearch" runat="server" Text="..." ToolTip="Search for Part" Enabled="False" /></td>
                    <td colspan="2" align="left" style="height: 26px">
                        <asp:Label ID="lblPartName" runat="server" Width="60%"></asp:Label></td>
                </tr>
                <tr bgColor="#ffffff" >
                    <td style="width: 20%; height: 26px;" align="center">
                        <asp:Label ID="lblQuantity" runat="server" Text="Label"></asp:Label></td>
                    <td style="width: 30%; height: 26px;" align="left" valign="middle" >
                        <asp:TextBox ID="txtQuantity" CssClass="textborder" runat="server" Width="50%" AutoPostBack="True" MaxLength="5"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="REValidator" runat="server" ControlToValidate="txtQuantity"
                             ValidationExpression="\d+" Display="Dynamic">*</asp:RegularExpressionValidator>
                    </td>
                    <td colspan="2" align="left" style="height: 26px">
                        <asp:Label ID="lblUnitPrice" runat="server" Width="60%"></asp:Label></td>
                </tr>
                <tr bgColor="#ffffff">
                    <td style="width: 20%" align="center">
                        <asp:Label ID="lbllineTotal" runat="server" Text="Label"></asp:Label></td>
                    <td style="width: 30%" align="left">
                        <asp:TextBox ID="txtlinetotal" CssClass="textborder" runat="server" Width="50%" MaxLength="10" ReadOnly="True"></asp:TextBox></td>
                    <td colspan="2" align="right">
                        <asp:Label ID="lblTaxPerc" runat="server" Visible="False"></asp:Label>
                        <asp:LinkButton ID="lbtnAdd" runat="server" Enabled="False">LinkButton</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>
                <tr bgColor="#ffffff">
                    <td colspan="4" align="right" style="height: 15px">
                        <asp:LinkButton ID="lbtnViewCurList" runat="server" Visible="False">LinkButton</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp; 
                        <asp:LinkButton ID="lbtnSave" runat="server" Enabled="False">LinkButton</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;<asp:LinkButton ID="lbtnDelete" runat="server" Enabled="False">LinkButton</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>
                 <tr bgColor="#ffffff">
                    <td colspan="4" align="right" style="height: 15px">
                        <table style="width: 100%" border="0">
                            <tr>
                                <td align="left" style="width: 100%; height: 17px;">
                                    <asp:Label ID="lblNewStockList" runat="server" Text="Label"></asp:Label>&nbsp;
                                    <asp:LinkButton ID="LnkSelAll" runat="server" Visible="False">Select All</asp:LinkButton></td>
                            </tr>
                            <tr>
                                <td style="width: 100%">
                                    <asp:GridView ID="mygridview" Width="100%" runat="server" AutoGenerateColumns="False">
                                        <Columns>
                                          <asp:CommandField ShowSelectButton="True" />
                                          <asp:TemplateField HeaderText="NO">
                                            <ItemTemplate>
                                              <%#Container.DataItemIndex + 1%>
                                            </ItemTemplate>
                                          </asp:TemplateField>
                                            <asp:BoundField DataField="PartID" HeaderText="PART CODE" />
                                            <asp:BoundField DataField="PartName" HeaderText="PART DESCRIPTION" />
                                            <asp:BoundField DataField ="CtrID" HeaderText="CTRID" Visible="False" />
                                            <asp:BoundField DataField="Sign" HeaderText="SIGN" />
                                            <asp:BoundField DataField="Quantity" HeaderText="QUANTITY" />
                                            <asp:BoundField DataField="UnitPrice" HeaderText="UNIT PRICE" />
                                            <asp:BoundField DataField="LineAmt" HeaderText="LINE AMT" />
                                            <asp:BoundField DataField="Stat" HeaderText="STAT" Visible="False" />
                                        </Columns>
                                        <SelectedRowStyle BackColor="#C0C0FF" />
                                        <RowStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr bgColor="#ffffff">
                    <td colspan=4>
                        <asp:Label ID="lblNoteDown" runat="server" ForeColor="Red" Text="Label"></asp:Label></td>
                </tr>
           </table>
        </div>
    </div>
        <cc1:messagebox id="msg" runat="server"></cc1:messagebox>
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True" ShowSummary="False" />
        <cc1:MessageBox ID="msgSave" runat="server" /><cc1:MessageBox ID="MsgGenPack" runat="server" />
    </form>
</body>
</html>
