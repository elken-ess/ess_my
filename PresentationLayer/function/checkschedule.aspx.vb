﻿Imports BusinessEntity
Imports System.Data

Partial Class PresentationLayer_function_checkSchedule
    Inherits System.Web.UI.Page
    Dim objXmlTr As New clsXml
    Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
    Dim apptEntity As New clsAppointment()
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try

        If Not Page.IsPostBack Then
            
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            Me.titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CHKS-TITL")
            Me.servCenterLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CHKS-SERVCENTER")
            Me.apptDateLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CHKS-APPTDATE")
            Me.zoneIDLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CHKS-ZONEID")
            Me.backLink.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-BACK")
            backLink.Attributes("onclick") = "javascript:history.back();"
            Me.servCenterBox.Text = Request.Params("servCenter").ToString()
            Me.apptDateBox.Text = Request.Params("apptDate").ToString()
            Me.zoneIDBox.Text = Request.Params("zoneID").ToString()
            BindGrid()
        ElseIf CHKScheduleView.Rows.Count > 0 Then
            DispGridViewHead()
        End If
    End Sub
    Protected Sub BindGrid()
        'apptEntity.ServerCenterID = Me.servCenterBox.Text.Split("-")(0).ToString()
        apptEntity.ServerCenterID = Me.servCenterBox.Text
        apptEntity.AppointmentDate = Me.apptDateBox.Text
        apptEntity.ZoneID = Me.zoneIDBox.Text
        Dim ds As DataSet = apptEntity.GetCHKSchedule()
        If ds Is Nothing Then
            Response.Redirect("~/PresentationLayer/error.aspx")
        End If
        Session("CHKScheduleView") = ds
        Me.lblShow.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB_FUN_E_TOTALSHOW") + ":" + ds.Tables(1).Rows(0).Item(0)
        Me.CHKScheduleView.DataSource = ds
        CHKScheduleView.AllowPaging = True
        CHKScheduleView.AllowSorting = True
        Me.CHKScheduleView.DataBind()
        If ds.Tables(0).Rows.Count > 0 Then
            DispGridViewHead()
        End If
    End Sub
    Private Sub DispGridViewHead()
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        CHKScheduleView.HeaderRow.Cells(0).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CHKS-NO")
        CHKScheduleView.HeaderRow.Cells(0).Font.Size = 8
        CHKScheduleView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CHKS-STIME")
        CHKScheduleView.HeaderRow.Cells(1).Font.Size = 8
        CHKScheduleView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CHKS-ETIME")
        CHKScheduleView.HeaderRow.Cells(2).Font.Size = 8
        CHKScheduleView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-CHKS-CUSNAME")
        CHKScheduleView.HeaderRow.Cells(3).Font.Size = 8
    End Sub

    Protected Sub CHKScheduleView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles CHKScheduleView.PageIndexChanging
        CHKScheduleView.PageIndex = e.NewPageIndex
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Dim ds As DataSet = Session("CHKScheduleView")
        CHKScheduleView.DataSource = ds
        CHKScheduleView.DataBind()
        If ds.Tables(0).Rows.Count > 0 Then
            DispGridViewHead()
        End If
    End Sub

End Class
