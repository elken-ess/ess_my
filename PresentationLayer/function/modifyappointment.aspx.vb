﻿Imports BusinessEntity
Imports System.Data
' Added by Ryan Estandarte 25 Sept 2012
Imports System.Collections.Generic
Imports System.Data.SqlClient

Partial Class PresentationLayer_function_modifyappointment_aspx
    Inherits System.Web.UI.Page
    Dim objXmlTr As New clsXml
    Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
    Dim apptEntity As New clsAppointment()
    Dim ROID As String = ""
    Dim apptStatus As String = ""
    Dim fstrDelimiter As String = "-"
    Dim fstrjobType_AS As String = "AS"
    Dim fstrjobType_SS As String = "SS"
    Dim fstrjobType_RS As String = "RS"
    Dim prevZoneID As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Page.MaintainScrollPositionOnPostBack = True
        AjaxPro.Utility.RegisterTypeForAjax(GetType(PresentationLayer_function_modifyappointment_aspx))
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        If Not Page.IsPostBack Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try
            'Session("FUN-MAPPT-ROID") = ""
            Session("FUN-MAPPT-apptStatus") = ""
            Session("FUN-MAPPT-CREATIME") = Date.Now
            Dim transType As String = Request.Params("transtype").ToString()
            Dim transNo As String = Request.Params("transno").ToString()

            'validator
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            apptDateError.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "FUN-APPT-APPTDATE")
            apptTypeError.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "FUN-APPT-APPTTYPE")
            servCIDError.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "FUN-APPT-APPTSEVCID")
            'technicanError.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "FUN-APPT-APPTTCHID")
            apptEndTimeHError.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "FUN-APPT-APPTENDTIME")
            apptEndTimeMError.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "FUN-APPT-APPTENDTIME")
            PICContactError.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "FUN-APPT-PICCONTACT")
            PICContactNullError.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "FUN-APPT-PICCONTACTNULL")
            CompDateVal.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "INVALID_DATE")
            NatureFeedbackError.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "FUN-APPT-NATFEEDBACK")

            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-TITL")
            'modetypeLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-MODETYPE")
            transtypeLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-TRANSTYPE")
            'serialNoLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-SERIALNO")
            transNoLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-TRANSNO")
            ROInfoLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-ROINFO")
            apptStatusLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-STAUTS")
            lastservDateLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-LASTSERVDATE")
            WarrantyLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-WARRANTY")
            PICNameLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-PICNAME")
            PICContactLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-PICCONTACT")
            CustIDLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-CUSTID")
            CustPrxLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-CUSTPRX")
            contEDateLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-CONTEDATE")
            CustNameLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-CUSTNAME")
            addr1Lab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-ADDR1")
            addr2Lab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-ADDR2")
            POCodeLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-POCODE")
            contryLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-CONTRY")
            areaLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-AREA")
            stateLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-STATE")
            apptDateLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-APPTDATE")
            apptTypeLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-APPTTYPE")
            NatureFeedbackLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-NATUREFEEDBACK")
            apptStartTimeLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-APPTSTARTTIME")
            apptEndTimeLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-APPTENDTIME")
            servCenterLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-SERVCENTER")
            installByLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-INSTALLBY")
            assignToLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-ASSIGNTO")
            lastRemDateLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-LASTREMDATE")
            lastApptDateLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-LASTAPPTDATE")
            contractNoLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-CONTRACTNO")
            ResReasonLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-RESREASON")
            ReasonObjection.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-REASONOBJECTION")
            lastEditDateLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-LASTEDITDATE")
            notesLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-NOTES")
            custNotesLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-CUSTNOTES")
            assToZoneLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-ASSTOZONE")
            detaillkBtn.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-DETAIL")
            chkSchedulelkBtn.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-CHKSCHEDULE")
            servHistorylkBtn.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-SERVHISTORY")
            contractHistroylkBtn.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-CONTRACTHISTORY")
            saveButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Save")
            cancelLink.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Cancel")
            lblCrID.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CrUserID")

            ' Added by Ryan Estandarte 20 Sept 2012
            lblIsInbound.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-INBOUND")

            ' Added by Ryan Estandarte 25 Sept 2012
            Dim optionDic As New Dictionary(Of String, String)
            optionDic.Add("Inbound", "True")
            optionDic.Add("Outbound", "False")
            rdoInboundOutbound.DataSource = optionDic
            rdoInboundOutbound.DataTextField = "Key"
            rdoInboundOutbound.DataValueField = "Value"
            rdoInboundOutbound.DataBind()

            Me.lblCreateBy.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATBY")
            Me.lblCreateDate.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
            Me.lblLastModifyBy.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFBY")


            lblNoteUP.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            lblNoteDown.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")



            If transType.Trim() <> "" And transNo.Trim() <> "" Then
                GetData(transType, transNo)
                'Session.Remove("ZoneSelectedIndex")
            End If


            '''access control'''''''''''''''''''''''''''''''''''''''''''''''
            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "22")

            Me.saveButton.Enabled = purviewArray(1)

        End If
        HypCal.NavigateUrl = "javascript:DoCal(document.form1.apptDateBox);"
        Me.errlab.Visible = False
    End Sub
    '从数据库中读取信息
    Private Sub GetData(ByVal transType As String, ByVal transNo As String)

        apptEntity.TransactionType = transType
        apptEntity.TransactionNo = transNo
        Dim xDtSet As DataSet = apptEntity.GetAppointmentDetailsByID()
        If xDtSet Is Nothing Then
            Response.Redirect("~/PresentationLayer/error.aspx")
        End If
        '处理异常情况 ***未完成*******
        If xDtSet.Tables("FAPT_FIL").Rows.Count <> 1 Or xDtSet.Tables("MCUS_FIL").Rows.Count <> 1 Then
            Response.Redirect("~/PresentationLayer/error.aspx")
        End If
        If xDtSet.Tables("MADR_FIL").Rows.Count = 1 Then
            addr1Box.Text = xDtSet.Tables("MADR_FIL").Rows(0).Item("MADR_ADDR1").ToString()
            addr2Box.Text = xDtSet.Tables("MADR_FIL").Rows(0).Item("MADR_ADDR2").ToString()
            POCodeBox.Text = xDtSet.Tables("MADR_FIL").Rows(0).Item("MADR_PCODE").ToString()
            contryBox.Text = xDtSet.Tables("MADR_FIL").Rows(0).Item("MADR_CTRID").ToString()
            stateBox.Text = xDtSet.Tables("MADR_FIL").Rows(0).Item("MADR_STAID").ToString()
            areaBox.Text = xDtSet.Tables("MADR_FIL").Rows(0).Item("MADR_AREID").ToString()
        End If
        'text Box
        transTypeBox.Text = transType
        transNoBox.Text = transNo
        PICNameBox.Text = xDtSet.Tables("FAPT_FIL").Rows(0).Item("FAPT_PICNM").ToString()
        PICContactBox.Text = xDtSet.Tables("FAPT_FIL").Rows(0).Item("FAPT_TELNO").ToString()
        CustIDBox.Text = xDtSet.Tables("FAPT_FIL").Rows(0).Item("FAPT_CUSID").ToString()
        CustPrxBox.Text = xDtSet.Tables("FAPT_FIL").Rows(0).Item("FAPT_CUSPF").ToString()
        CustNameBox.Text = xDtSet.Tables("MCUS_FIL").Rows(0).Item("MCUS_ENAME").ToString()
        apptDateBox.Text = xDtSet.Tables("FAPT_FIL").Rows(0).Item("FAPT_APTDT").ToString()
        ' Modified by Ryan Estandarte 25 Sept 2012
        ' Added by Ryan Estandarte 20 Sept 2012
        'chkIsInbound.Checked = CType(xDtSet.Tables("FAPT_FIL").Rows(0)("FAPT_IsInbound").ToString(), Boolean)
        rdoInboundOutbound.SelectedValue = CType(xDtSet.Tables("FAPT_FIL").Rows(0)("FAPT_IsInbound").ToString(), Boolean).ToString()

        Session("FUN-MAPPT-CREATIME") = xDtSet.Tables("FAPT_FIL").Rows(0).Item("FAPT_CREDT")
        If xDtSet.Tables("FAPT_FIL").Rows(0).Item("FAPT_ENDTM").ToString().Length <> 4 Then
            'Response.Redirect("~/PresentationLayer/error.aspx")
            apptEndTimeHBox.Text = "00"
            apptEndTimeMBox.Text = "00"
        Else
            apptEndTimeHBox.Text = xDtSet.Tables("FAPT_FIL").Rows(0).Item("FAPT_ENDTM").ToString().Substring(0, 2)
            apptEndTimeMBox.Text = xDtSet.Tables("FAPT_FIL").Rows(0).Item("FAPT_ENDTM").ToString().Substring(2, 2)
        End If

        lastEditDateBox.Text = xDtSet.Tables("FAPT_FIL").Rows(0).Item("FAPT_LSTDT").ToString()

        notesBox.Text = xDtSet.Tables("FAPT_FIL").Rows(0).Item("FAPT_REM").ToString()
        custNotesBox.Text = xDtSet.Tables("MCUS_FIL").Rows(0).Item("MCUS_NOTES").ToString()

        Dim Rcreat As New clsCommonClass
        Dim Rcreatds As DataSet
        Rcreat.userid = xDtSet.Tables("FAPT_FIL").Rows(0).Item("FAPT_LUSER").ToString()
        Rcreatds = Rcreat.Getuseridname("BB_MASUSER_SelByIDName")
        Me.txtLastModify.Text = Rcreatds.Tables(0).Rows(0).Item(0)

        Rcreat.userid = xDtSet.Tables("FAPT_FIL").Rows(0).Item("FAPT_CUSER").ToString()
        Rcreatds = Rcreat.Getuseridname("BB_MASUSER_SelByIDName")
        Me.txtCreateBy.Text = Rcreatds.Tables(0).Rows(0).Item(0)

        Me.txtCreateDate.Text = xDtSet.Tables("FAPT_FIL").Rows(0).Item("FAPT_CREDT").ToString()


        'RMStxt.Text = xDtSet.Tables("RMS_FIL").Rows(0).Item("RMS_NO").ToString()
        Dim Conn As New SqlConnection
        Conn.ConnectionString = ConfigurationManager.AppSettings("ConnectionString").ToString

        Using Comm As New SqlClient.SqlCommand("Select [RMS_NO] = isnull(RMS_NO,'') from dbo.RMS_FIL where APPT_TRNNO = '" & transNo & "' and APPT_TRNTY = '" + transType + "'", Conn)
            Conn.Open()
            Using readerObj As SqlClient.SqlDataReader = Comm.ExecuteReader
                While readerObj.Read
                    RMStxt.Text = readerObj("RMS_NO").ToString
                End While
            End Using
            Conn.Close()
        End Using

        'drop down list
        Dim count As Integer
        Dim current As Integer = 0
        'appointment status dropdownlsit
        apptStatus = xDtSet.Tables("FAPT_FIL").Rows(0).Item("FAPT_STAT").ToString().Trim()
        Session("FUN-MAPPT-apptStatus") = apptStatus
        Dim apptStat As New clsUtil()
        Dim statParam As ArrayList = New ArrayList
        statParam = apptStat.searchconfirminf("APPTStatus")
        Dim statid As String
        Dim statnm As String
        If apptStatus = "SM" Then 'apptStatus <> "SA" And apptStatus <> "TG" Then 'appointment is not new or reschedule Then
            For count = 0 To statParam.Count - 1
                statid = statParam.Item(count)
                statnm = statParam.Item(count + 1)
                count = count + 1
                If statid = "SM" Or statid = "UA" Or statid = "TG" Or statid = "X" Then '只显示rechedule,cancle,X 
                    apptStatDrpList.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                End If
            Next
            apptStatDrpList.SelectedIndex = 2
        ElseIf apptStatus = "TG" Then 'reschedule
            For count = 0 To statParam.Count - 1
                statid = statParam.Item(count)
                statnm = statParam.Item(count + 1)
                count = count + 1
                If statid = "UA" Or statid = "TG" Or statid = "X" Then '只显示rechedule,cancle,X 
                    apptStatDrpList.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                End If
            Next
            apptStatDrpList.SelectedIndex = 1
        ElseIf apptStatus = "SA" Then 'new
            For count = 0 To statParam.Count - 1
                statid = statParam.Item(count)
                statnm = statParam.Item(count + 1)
                count = count + 1
                If statid = "SA" Or statid = "UA" Or statid = "TG" Or statid = "X" Then '只显示new,rechedule,cancel,X 
                    apptStatDrpList.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                End If
            Next
            apptStatDrpList.SelectedIndex = 0
        ElseIf apptStatus = "UA" Then 'cancel
            For count = 0 To statParam.Count - 1
                statid = statParam.Item(count)
                statnm = statParam.Item(count + 1)
                count = count + 1
                If statid.ToString().Trim() = apptStatus Then
                    apptStatDrpList.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                End If
            Next
            apptStatDrpList.SelectedIndex = 0
        ElseIf apptStatus = "X" Then 'X
            For count = 0 To statParam.Count - 1
                statid = statParam.Item(count)
                statnm = statParam.Item(count + 1)
                count = count + 1
                If statid.ToString().Trim() = apptStatus Then
                    apptStatDrpList.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                End If
            Next
            apptStatDrpList.SelectedIndex = 0
        ElseIf apptStatus = "UM" Or apptStatus = "BL" Then 'Completed
            For count = 0 To statParam.Count - 1
                statid = statParam.Item(count)
                statnm = statParam.Item(count + 1)
                count = count + 1
                If statid.ToString().Trim() = apptStatus Then
                    apptStatDrpList.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                End If
            Next
            apptStatDrpList.SelectedIndex = 0
        End If

        'RO Info dropdownlist
        apptEntity.CustomerID = CustIDBox.Text.ToString()
        apptEntity.CustomerPrifx = CustPrxBox.Text.ToString()
        apptEntity.ServiceType = "NORMAL,FREE"
        Dim dsDrpListData As DataSet = apptEntity.GetDropDownListData(Session("userID"))
        If dsDrpListData Is Nothing Then
            Response.Redirect("~/PresentationLayer/error.aspx")
        End If


        '处理关于ROID的信息
        ROID = xDtSet.Tables("FAPT_FIL").Rows(0).Item("FAPT_ROUID").ToString()
        'Session("FUN-MAPPT-ROID") = ROID
        ROInfoDrpList.Items.Add(New ListItem("", ""))
        If ROID <> "" And ROID <> "0" Then
            '处理异常情况 ***未完成*******
            If xDtSet.Tables("MROU_FIL").Rows.Count <> 0 Then
                lastservDateBox.Text = xDtSet.Tables("MROU_FIL").Rows(0).Item("MROU_LSVDT").ToString()
                WarrantyBox.Text = xDtSet.Tables("MROU_FIL").Rows(0).Item("MROU_WRRDT").ToString()
                installByBox.Text = xDtSet.Tables("MROU_FIL").Rows(0).Item("MROU_TCHID").ToString()
                'lastApptDateBox.Text = ds.Tables("MROU_FIL").Rows(0).Item("MROU_APTDT").ToString()
                lastRemDateBox.Text = xDtSet.Tables("MROU_FIL").Rows(0).Item("MROU_LRMDT").ToString()

                current = -1
                For count = 0 To dsDrpListData.Tables("ROINFO").Rows.Count - 1
                    Dim NewItem As New ListItem()
                    NewItem.Text = dsDrpListData.Tables("ROINFO").Rows(count).Item("MODEID").ToString() + "-" + _
                            dsDrpListData.Tables("ROINFO").Rows(count).Item("SERIALNO").ToString()
                    'NewItem.Value = NewItem.Text
                    NewItem.Value = dsDrpListData.Tables("ROINFO").Rows(count).Item("ROID").ToString()
                    ROInfoDrpList.Items.Add(NewItem)
                    If ROID = dsDrpListData.Tables("ROINFO").Rows(count).Item("ROID").ToString().Trim() Then
                        current = count
                    End If
                Next
                If current <> -1 Then
                    ROInfoDrpList.SelectedIndex = current + 1 '第一行为空
                Else
                    'commented by deyb
                    'Response.Redirect("~/PresentationLayer/error.aspx") '该水机号在数据库中不存在，
                    '=============
                    '或该客户没有买这个水机()
                End If
            End If
        Else 'ROID=""
            lastservDateBox.Text = ""
            WarrantyBox.Text = ""
            installByBox.Text = ""
            lastRemDateBox.Text = ""
            For count = 0 To dsDrpListData.Tables("ROINFO").Rows.Count - 1
                Dim NewItem As New ListItem()
                NewItem.Text = dsDrpListData.Tables("ROINFO").Rows(count).Item("MODEID").ToString() + "-" + _
                        dsDrpListData.Tables("ROINFO").Rows(count).Item("SERIALNO").ToString()
                'NewItem.Value = NewItem.Text
                NewItem.Value = dsDrpListData.Tables("ROINFO").Rows(count).Item("ROID").ToString()
                ROInfoDrpList.Items.Add(NewItem)
            Next
            ROInfoDrpList.SelectedIndex = 0
        End If

        'appointment Type
        Dim crtApptType As String = xDtSet.Tables("FAPT_FIL").Rows(0).Item("FAPT_APTTY").ToString()
        databonds(dsDrpListData.Tables("MSV1_FIL"), apptTypeDrpList, crtApptType)

        'nature of feedback Dropdownlist.. added in SMR1711/2286
        If crtApptType = "MR" Or crtApptType = "MB" Or crtApptType = "PB" Then
            trNatureFeedback.Visible = True

            Dim dtNatureFeedback As DataTable
            dtNatureFeedback = apptEntity.GetNatureOfFeedback(crtApptType).Tables(0)
            ddlNatureFeedback.DataSource = dtNatureFeedback
            ddlNatureFeedback.DataValueField = "DFeedback_ID"
            ddlNatureFeedback.DataTextField = "Description"
            ddlNatureFeedback.DataBind()
            Dim list1 As New ListItem("Please select one", "")
            ddlNatureFeedback.Items.Insert(0, list1)

            If ddlNatureFeedback.Items.FindByValue(xDtSet.Tables("FAPT_FIL").Rows(0).Item("NatureFeedback").ToString()) Is Nothing Then
            ElseIf xDtSet.Tables("FAPT_FIL").Rows(0).Item("NatureFeedback").ToString() = "" Then
            Else
                ddlNatureFeedback.SelectedValue = xDtSet.Tables("FAPT_FIL").Rows(0).Item("NatureFeedback").ToString()
            End If
        Else
            trNatureFeedback.Visible = False
        End If

        'appointment start time dropdownlist
        Dim crtApptSTime As String = xDtSet.Tables("FAPT_FIL").Rows(0).Item("FAPT_STRTM").ToString()
        Dim apptSTime As New clsUtil()
        Dim STimeParam As ArrayList = New ArrayList
        STimeParam = apptSTime.searchconfirminf("APPTSTIME")
        Dim STimeid As String
        Dim STimenm As String
        current = 0
        For count = 0 To STimeParam.Count - 1
            STimeid = STimeParam.Item(count)
            STimenm = STimeParam.Item(count + 1)
            If crtApptSTime = STimeid Then
                current = count / 2
            End If
            count = count + 1
            apptStartTimeDrpList.Items.Add(New ListItem(STimenm.ToString(), STimeid.ToString()))
        Next
        apptStartTimeDrpList.SelectedIndex = current

        'Populate ReasonObjection
        If apptStatDrpList.SelectedValue = "X" Then
            Dim dt As DataTable
            dt = apptEntity.GetObjectionReason().Tables(0)
            trReasonObjection.Visible = True
            ddlReasonObjection.DataSource = dt
            ddlReasonObjection.DataValueField = "ReasonObj_ID"
            ddlReasonObjection.DataTextField = "ReasonObj_Desc"
            ddlReasonObjection.DataBind()
            ResReasonDrpList.Enabled = False
            Dim list As New ListItem("Please select one", "")
            ddlReasonObjection.Items.Insert(0, list)
            If ddlReasonObjection.Items.FindByValue(xDtSet.Tables("FAPT_FIL").Rows(0).Item("reasonobjection").ToString()) Is Nothing Then
            ElseIf xDtSet.Tables("FAPT_FIL").Rows(0).Item("reasonobjection").ToString() = "" Then
            Else
                ddlReasonObjection.SelectedValue = xDtSet.Tables("FAPT_FIL").Rows(0).Item("reasonobjection").ToString()
            End If
        Else
            ResReasonDrpList.Enabled = True
            trReasonObjection.Visible = False
        End If

        apptStatDrpList_SelectedIndexChanged(Nothing, Nothing)

        'appointment Reschedule Reason dropdownlist
        ResReasonDrpList.Items.Add(New ListItem("", ""))
        Dim crtResReason As String = xDtSet.Tables("FAPT_FIL").Rows(0).Item("FAPT_REASN").ToString().Trim()
        statParam = apptStat.searchconfirminf("RESREASON")
        current = -1
        For count = 0 To statParam.Count - 1
            statid = statParam.Item(count)
            If statid = crtResReason Then
                current = count / 2
            End If
            statnm = statParam.Item(count + 1)
            count = count + 1
            ResReasonDrpList.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
        Next
        ResReasonDrpList.SelectedIndex = current + 1

        'Server Center dropdownlist
        Dim crtServCenterID As String = xDtSet.Tables("FAPT_FIL").Rows(0).Item("FAPT_SVCID").ToString()
        Dim dsServerCenter As DataSet = apptEntity.GetSVCIDByUserID(Session("userID"))
        databonds(dsServerCenter.Tables("ServerCenterID"), servCenterDrpList, crtServCenterID)

        JobTypeProperties()
        cboZoneID.SelectedValue = xDtSet.Tables("FAPT_FIL").Rows(0).Item("FAPT_ZONID").ToString()
        prevZoneID = cboZoneID.SelectedValue
        Session("prevZoneID") = cboZoneID.SelectedValue
        'txtZoneID.Text = cboZoneID.SelectedIndex


        Dim crtCrID As String = xDtSet.Tables("FAPT_FIL").Rows(0).Item("FAPT_CRID").ToString()
        Dim dsCrUser As DataSet = apptEntity.GetCrIDBySvcID(crtServCenterID)
        databonds(dsCrUser.Tables(0), cboCrID, crtCrID)






        'edit June 25
        'assign to Technican dropdownlist
        If Me.transTypeBox.Text <> "SS" Then
            apptEntity.ServerCenterID = crtServCenterID
            Dim dsTCHINFO As DataSet = apptEntity.GetTCHINFODrpListData()
            Dim crtTCHID As String = xDtSet.Tables("FAPT_FIL").Rows(0).Item("FAPT_TCHID").ToString()
            databondsNameID(dsTCHINFO.Tables("TCHINFO"), assignToDrpList, crtTCHID)

        Else
            Me.assignToDrpList.Enabled = False
            apptEntity.ServerCenterID = crtServCenterID
            Dim dsTCHINFO As DataSet = apptEntity.GetTCHINFODrpListData()
            Dim crtTCHID As String = xDtSet.Tables("FAPT_FIL").Rows(0).Item("FAPT_TCHID").ToString()
            databonds(dsTCHINFO.Tables("TCHINFO"), assignToDrpList, crtTCHID)
        End If

        apptEntity.CustomerID = CustIDBox.Text
        apptEntity.CustomerPrifx = CustPrxBox.Text
        apptEntity.ROID = ROID
        xDtSet = New DataSet
        xDtSet = apptEntity.GetContractInfo()
        If xDtSet.Tables("FCU1_FIL").Rows.Count = 1 Then
            contractNoBox.Text = xDtSet.Tables("FCU1_FIL").Rows(0).Item("FCU1_CONID").ToString()
            contEDateBox.Text = xDtSet.Tables("FCU1_FIL").Rows(0).Item("FCU1_OBSDT").ToString()
        End If

        apptEntity.ROID = ROID
        apptEntity.CustomerID = CustIDBox.Text
        apptEntity.CustomerPrifx = CustPrxBox.Text

        lastApptDateBox.Text = apptEntity.GetLastApptDate(apptDateBox.Text)



        ' by appointment status to lock certain fields
        EnableByAppointmentStatus(apptStatus)

    End Sub

    Sub EnableByAppointmentStatus(ByVal Status As String)
        Me.HypCal.Enabled = False
        Me.apptDateBox.ReadOnly = True
        Me.apptTypeDrpList.Enabled = True
        Me.ddlNatureFeedback.Enabled = True
        Me.servCenterDrpList.Enabled = True

        Select Case Status
            Case "TG"
                Me.HypCal.Enabled = True
                Me.apptDateBox.ReadOnly = False

            Case "BL", "UM"

                Me.apptTypeDrpList.Enabled = False
                Me.ddlNatureFeedback.Enabled = False
                Me.servCenterDrpList.Enabled = False
                Me.assignToDrpList.Enabled = False

            Case Else

                ' get original appointment date
                apptEntity.TransactionType = Me.transTypeBox.Text
                apptEntity.TransactionNo = Me.transNoBox.Text
                Dim xDtSet As DataSet = apptEntity.GetAppointmentDetailsByID()

                If xDtSet.Tables("FAPT_FIL").Rows.Count >= 1 Then
                    apptDateBox.Text = xDtSet.Tables("FAPT_FIL").Rows(0).Item("FAPT_APTDT").ToString()
                End If

                xDtSet = Nothing
        End Select


    End Sub


    Public Sub databondsNameID(ByRef dt As DataTable, ByRef dropdown As DropDownList, ByVal crtId As String)
        Dim current As Integer = -1
        Dim count As Integer = 0
        dropdown.Items.Clear()
        dropdown.Items.Add(New ListItem("", ""))
        Dim row As DataRow
        For Each row In dt.Rows
            If crtId.Trim = row("id").ToString().Trim() Then
                current = count
            End If
            Dim NewItem As New ListItem()
            NewItem.Text = row("name") & "-" & row("id")
            NewItem.Value = row("id")
            dropdown.Items.Add(NewItem)
            count = count + 1
        Next
        dropdown.SelectedIndex = current + 1

    End Sub


    Public Sub databonds(ByRef dt As DataTable, ByRef dropdown As DropDownList, ByVal crtId As String)
        Dim current As Integer = -1
        Dim count As Integer = 0
        dropdown.Items.Clear()
        dropdown.Items.Add(New ListItem("", ""))
        Dim row As DataRow
        For Each row In dt.Rows
            If crtId.Trim = row("id").ToString().Trim() Then
                current = count
            End If
            Dim NewItem As New ListItem()
            NewItem.Text = row("id") & "-" & row("name")
            NewItem.Value = row("id")
            dropdown.Items.Add(NewItem)
            count = count + 1
        Next
        dropdown.SelectedIndex = current + 1

    End Sub

    Protected Sub apptDateCalendar_CalendarVisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles apptDateCalendar.CalendarVisibleChanged
        'RequestFromValue
        'Dim script As String = "self.location='#ApptLoc';"
        'Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ApptLoc", script, True)
    End Sub

    Protected Sub apptDateCalendar_SelectedDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles apptDateCalendar.SelectedDateChanged
        'assToZoneBox.Text = 
        'RequestFromValue()
        'GetZoneID()
        'Call DisplayRoInfoForAppointment()
        'Dim script As String = "self.location='#ApptLoc';"
        'Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ApptLoc", script, True)
    End Sub
    '获得ZoneID
    'Private Function GetZoneID() As String
    '    If areaBox.Text = "" Or apptDateBox.Text = "" Or Me.servCenterDrpList.SelectedValue = "" Then
    '        Return ""
    '    End If
    '    apptEntity.CustomAreaID = areaBox.Text
    '    apptEntity.AppointmentDate = apptDateBox.Text
    '    apptEntity.ServerCenterID = Me.servCenterDrpList.SelectedValue
    '    Dim ds As DataSet = apptEntity.GetZoneID()
    '    If ds.Tables("FZN2_FIL").Rows.Count > 0 Then '取第一个，可能需要改动
    '        Return ds.Tables("FZN2_FIL").Rows(0).Item("FZN2_ZONID").ToString()
    '    Else
    '        Return ""
    '    End If
    'End Function

    Sub DisplayRoInfoForAppointment()
        Dim ds As DataSet
        If ROInfoDrpList.SelectedValue <> "" Then
            apptEntity.ModeID = ROInfoDrpList.SelectedItem.Text.Split(fstrDelimiter)(0).ToString()
            apptEntity.SerialNo = ROInfoDrpList.SelectedItem.Text.Split(fstrDelimiter)(1).ToString()
            apptEntity.ROID = ROInfoDrpList.SelectedItem.Value.ToString()

            ds = apptEntity.GetROAndContractInfo()
            If ds Is Nothing Then
                Response.Redirect("~/PresentationLayer/error.aspx")
            End If
            '处理异常***未完成*******
            If ds.Tables("MROU_FIL").Rows.Count <> 1 Then '该水机不存在
                Response.Redirect("~/PresentationLayer/error.aspx")
            End If

            lastservDateBox.Text = ds.Tables("MROU_FIL").Rows(0).Item("MROU_LSVDT").ToString()
            WarrantyBox.Text = ds.Tables("MROU_FIL").Rows(0).Item("MROU_WRRDT").ToString()
            installByBox.Text = ds.Tables("MROU_FIL").Rows(0).Item("MROU_TCHID").ToString()
            lastRemDateBox.Text = ds.Tables("MROU_FIL").Rows(0).Item("MROU_LRMDT").ToString()
            ROID = ds.Tables("MROU_FIL").Rows(0).Item("MROU_ROUID").ToString()
        Else
            lastservDateBox.Text = ""
            WarrantyBox.Text = ""
            installByBox.Text = ""
            lastRemDateBox.Text = ""
            ROID = ""
        End If

        ROID = ROInfoDrpList.SelectedItem.Value.ToString()
        'Session("FUN-MAPPT-ROID") = ROID
        apptEntity.CustomerID = CustIDBox.Text
        apptEntity.CustomerPrifx = CustPrxBox.Text
        apptEntity.ROID = ROID
        apptEntity.AppointmentDate = Me.apptDateBox.Text
        ds = apptEntity.GetContractInfo()
        If ds.Tables("FCU1_FIL").Rows.Count = 1 Then
            contractNoBox.Text = ds.Tables("FCU1_FIL").Rows(0).Item("FCU1_CONID").ToString()
            contEDateBox.Text = ds.Tables("FCU1_FIL").Rows(0).Item("FCU1_OBSDT").ToString()
        End If
        lastApptDateBox.Text = apptEntity.GetLastApptDate(Session("FUN-MAPPT-CREATIME"))
    End Sub

    Protected Sub ROInfoDrpList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ROInfoDrpList.SelectedIndexChanged
        Call DisplayRoInfoForAppointment()
    End Sub

    Protected Sub apptStartTimeDrpList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles apptStartTimeDrpList.SelectedIndexChanged
        CalculateAppointmentEndTime()
    End Sub



    Protected Sub servCenterDrpList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles servCenterDrpList.SelectedIndexChanged
        'If Me.transTypeBox.Text <> "SS" Then
        '    apptEntity.ServerCenterID = servCenterDrpList.SelectedItem.Value
        '    Dim dsTCHINFO As DataSet = apptEntity.GetTCHINFODrpListData()
        '    databonds(dsTCHINFO.Tables("TCHINFO"), assignToDrpList, "0")
        '    assignToDrpList.SelectedIndex = 0
        'Else
        '    assignToDrpList.Items.Clear()
        '    assignToDrpList.Items.Add(New ListItem("", ""))
        '    assignToDrpList.SelectedIndex = 0
        'End If

        'Dim crtCrID As String = ""
        'Dim dsCrUser As DataSet = apptEntity.GetCrIDBySvcID(servCenterDrpList.SelectedItem.Value)
        'databonds(dsCrUser.Tables(0), cboCrID, crtCrID)

        'GetZoneID()

    End Sub

    'Protected Sub chkSchedulelkBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSchedulelkBtn.Click
    'RequestFromValue()



    'Dim objXm As New clsXml

    'objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
    'Dim msginfo As String
    'If servCenterDrpList.SelectedValue = "" Then
    '    msginfo = objXm.GetLabelName("StatusMessage", "FUN-APPT-APPTSEVCID")
    '    InfoMsgBox.Alert(msginfo)
    '    Return
    'End If
    'Dim strTempURL As String = "servCenter=" + servCenterDrpList.SelectedItem.Value + "&apptDate=" + apptDateBox.Text + _
    '                             "&zoneID=" + cboZoneID.SelectedValue
    'strTempURL = "~/PresentationLayer/function/checkSchedule.aspx?" + strTempURL
    ''strTempURL = "checkSchedule.aspx?" + strTempURL
    'Response.Redirect(strTempURL)

    ''Dim script As String
    ''script = "showModalDialog('" & strTempURL & "','_calPick','status=no;center=yes;');"
    ''Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)

    'End Sub


    'Protected Sub detaillkBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles detaillkBtn.Click
    '    'RequestFromValue()
    '    Dim strTempURL As String = "custID=" + CustIDBox.Text + "&custPf=" + CustPrxBox.Text + _
    '                               "&custName=" + CustNameBox.Text + "&ROID=" + ROInfoDrpList.SelectedItem.Value.ToString()
    '    strTempURL = "~/PresentationLayer/function/appointmenthistorydetail.aspx?" + strTempURL
    '    Response.Redirect(strTempURL)
    'End Sub

    'Protected Sub servHistorylkBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles servHistorylkBtn.Click
    '    'RequestFromValue()
    '    Dim strTempURL As String = "ROID=" + ROInfoDrpList.SelectedItem.Value.ToString() + "&ROINFO=" + ROInfoDrpList.SelectedItem.Text + _
    '                               "&custName=" + CustNameBox.Text + "&custID=" + CustIDBox.Text + "&custPf=" + CustPrxBox.Text
    '    strTempURL = "~/PresentationLayer/function/serverHistory.aspx?" + strTempURL
    '    Response.Redirect(strTempURL)
    'End Sub

    'Protected Sub contractHistroylkBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles contractHistroylkBtn.Click
    '    'RequestFromValue()
    '    Dim strTempURL As String = "ROID=" + ROInfoDrpList.SelectedItem.Value.ToString() + "&ROINFO=" + ROInfoDrpList.SelectedItem.Text + _
    '                               "&custName=" + CustNameBox.Text + "&custID=" + CustIDBox.Text + "&custPf=" + CustPrxBox.Text
    '    strTempURL = "~/PresentationLayer/function/contracthistory.aspx?" + strTempURL
    '    Response.Redirect(strTempURL)
    'End Sub

    Protected Sub saveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveButton.Click
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        RequestFromValue()

        If Not IsDate(apptDateBox.Text) Then
            InfoMsgBox.Alert(objXmlTr.GetLabelName("StatusMessage", "INVALID_APPOINTMENT_DATE"))
            Return
        End If

        '处理时间格式把apptStartTime和apptEndTime格式换成####,如：0900
        If Convert.ToInt32(apptEndTimeHBox.Text) < 0 Or Convert.ToInt32(apptEndTimeHBox.Text) > 23 Then
            apptEndTimeHBox.Text = "00"
        ElseIf apptEndTimeHBox.Text.Length = 1 Then
            apptEndTimeHBox.Text = "0" + apptEndTimeHBox.Text
        End If

        If Convert.ToInt32(apptEndTimeMBox.Text) < 0 Or Convert.ToInt32(apptEndTimeMBox.Text) > 59 Then
            apptEndTimeMBox.Text = "00"
        ElseIf apptEndTimeMBox.Text.Length = 1 Then
            apptEndTimeMBox.Text = "0" + apptEndTimeMBox.Text
        End If
        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim msginfo As String
        Dim msgtitle As String
        If Convert.ToInt32(apptEndTimeHBox.Text + apptEndTimeMBox.Text) <= Convert.ToInt32(apptStartTimeDrpList.SelectedValue) Then
            msginfo = objXm.GetLabelName("StatusMessage", "FUN-APPT-APPTTIME")
            InfoMsgBox.Alert(msginfo)
            Return
        End If

        Dim lstrRoID As String
        lstrRoID = Me.ROInfoDrpList.SelectedValue
        If lstrRoID = "" Then
            lstrRoID = "0"
        End If

        apptEntity.TransactionType = Trim(transTypeBox.Text)
        apptEntity.TransactionNo = Trim(transNoBox.Text)
        apptEntity.AppointmentDate = Trim(apptDateBox.Text)
        apptEntity.ZoneID = Trim(cboZoneID.SelectedValue)
        apptEntity.AppointmentStartTime = apptStartTimeDrpList.SelectedValue
        'apptEntity.TechnicanID = Me.assignToDrpList.SelectedValue
        apptEntity.ServerCenterID = Me.servCenterDrpList.SelectedValue
        apptEntity.CustomerID = Me.CustIDBox.Text
        apptEntity.CustomerPrifx = Me.CustPrxBox.Text
        apptEntity.ROID = lstrRoID


        Dim ValidatorArray As Array = apptEntity.Validator("1") '是修改

        If ValidatorArray Is Nothing Then
            Response.Redirect("~/PresentationLayer/error.aspx")
        End If
        'If ValidatorArray(0) = True Then 'appointment no exist
        '    msginfo = objXm.GetLabelName("StatusMessage", "FUN-APPT-APPTEXIST")
        '    InfoMsgBox.Alert(msginfo)
        '    Return
        'End If

        ' Added by JC ReasonObjection 31 May 2016
        If apptStatDrpList.SelectedValue = "X" Then
            If ddlReasonObjection.SelectedValue = "" Then
                InfoMsgBox.Alert("Reason Objection cannot be blank!")
                Return
            End If
        End If



        If Me.apptStatDrpList.SelectedValue.Trim = "X" Or apptStatDrpList.SelectedValue.Trim = "UA" Then
            ' no checking on the zone and control
        Else
            If transTypeBox.Text = fstrjobType_SS Or transTypeBox.Text = fstrjobType_RS Then

                'If CDate(apptDateBox.Text) < System.DateTime.Today Then

                '    msginfo = objXm.GetLabelName("StatusMessage", "SS_APPOINTMENTDATE_PAST")
                '    InfoMsgBox.Alert(msginfo)
                '    Return
                'End If

                If cboZoneID.SelectedValue.Trim = "" And Not (apptEndTimeHBox.Text = "23" And apptEndTimeMBox.Text = "59") Then
                    msginfo = objXm.GetLabelName("StatusMessage", "FUN-APPT-OTHERTIMESLOT")
                    InfoMsgBox.Alert(msginfo)
                    Return

                End If

                If ValidatorArray(5) = True Then  'the maximum prefix no of appointment for the service center that day is full
                    msginfo = objXm.GetLabelName("StatusMessage", "FUN-APPT-MAXPFNOPDJOBS")
                    InfoMsgBox.Alert(msginfo)
                    Return
                End If

                'Added checking only when change other zone. - 07082008
                If Not (Session("prevZoneID").ToString = cboZoneID.SelectedValue) Then
                    If ValidatorArray(0) = True Then 'the maximum prefix no of appointment for the area is full.
                        msginfo = objXm.GetLabelName("StatusMessage", "FUN-APPT-ZONEFULL")
                        InfoMsgBox.Alert(msginfo)
                        Return
                    End If
                End If

                If ValidatorArray(1) = True Then 'another customers already occupy a particular timeslot.
                    msginfo = objXm.GetLabelName("StatusMessage", "FUN-APPT-OCCUPYTIME")
                    InfoMsgBox.Alert(msginfo)
                    Return
                End If

                If apptStartTimeDrpList.SelectedValue <> "0000" And ValidatorArray(2) = True And cboZoneID.SelectedValue <> "" Then '5 appointments for a same technician with different timeslot.

                    If apptEntity.FixedAppointmentCount + apptEntity.OtherAppointmentCount > 7 Then

                    End If

                    msginfo = objXm.GetLabelName("StatusMessage", "FUN-APPT-TCHBUSY1")
                    InfoMsgBox.Alert(msginfo)
                    Return
                End If

                'If apptStartTimeDrpList.SelectedValue = "0000" And ValidatorArray(3) = True And cboZoneID.SelectedValue <> "" Then '3 appointments for a same technician with timeslot is 'other'.


                '    msginfo = objXm.GetLabelName("StatusMessage", "FUN-APPT-TCHBUSY2")
                '    InfoMsgBox.Alert(msginfo)
                '    Return
                'End If

                If ValidatorArray(4) = True Then 'the customer already have an appointment at that day!
                    msginfo = objXm.GetLabelName("StatusMessage", "FUN-APPT-CUSTOMINFO")
                    InfoMsgBox.Alert(msginfo)
                    Return
                End If
            Else
                'If CDate(apptDateBox.Text) > CDate(System.DateTime.Today) Then

                '    msginfo = objXm.GetLabelName("StatusMessage", "AS_APPOINTMENTDATE_PAST")
                '    InfoMsgBox.Alert(msginfo)
                '    Return
                'End If
            End If

            If ValidatorArray(6) = True Then
                msginfo = objXm.GetLabelName("StatusMessage", "FUN-APPTCNT-UNCOMAPPT")
                InfoMsgBox.Alert(msginfo)
            End If

            Dim ROID As String = ROInfoDrpList.SelectedItem.Value.ToString()
            If ROID <> "" And ROID <> "0" Then
                apptEntity.ROID = ROID
                apptEntity.AppointmentType = apptTypeDrpList.SelectedValue.ToString()
                If apptEntity.SVCTValidator() = True Then
                    msginfo = objXm.GetLabelName("StatusMessage", "FUN-APPT-SVCTVALIDATOR")
                    InfoMsgBox.Alert(msginfo)
                    Return
                End If
            End If

            'Added checking for missing CR ID in FAPT_FIL - 09072008
            If Session("userID") = "" Then
                InfoMsgBox.Alert("Error : User Session id missing, Please try again.")
                Return
            End If
        End If

        '' Added by Tomas 20180613
        'If RMStxt.Text = "" Then
        '    InfoMsgBox.Alert("RMS No cannot be blank!")
        '    Return
        'Else
        '    Dim Conn As New SqlConnection
        '    Conn.ConnectionString = ConfigurationManager.AppSettings("ConnectionString").ToString

        '    Dim RMSNoExist = 0

        '    Using Comm As New SqlClient.SqlCommand("Select [Count] = count(*) from dbo.RMS_FIL where RMS_NO = '" & RMStxt.Text & "'", Conn)
        '        Conn.Open()
        '        Using readerObj As SqlClient.SqlDataReader = Comm.ExecuteReader
        '            While readerObj.Read
        '                RMSNoExist = readerObj("Count").ToString
        '            End While
        '        End Using
        '        Conn.Close()
        '    End Using
        '    If RMSNoExist > 0 Then
        '        InfoMsgBox.Alert("RMS No duplicate!")
        '        Return
        '    End If
        'End If
		
		Dim ApptType() As String = {"M1","M2","M3","M4","M5","M6","S1","S2","S3","W1","W2.","W3.","RM1","RM2","RM3","RM4","RM5","RM6","RM7"}

        For Each value As String In ApptType
            If apptTypeDrpList.SelectedValue.Trim = value Then

                Dim Flag As Integer

                Dim Conn As New SqlConnection
                Conn.ConnectionString = ConfigurationManager.AppSettings("ConnectionString").ToString

                Using Comm As New SqlClient.SqlCommand("select (case when " _
                    & "(select MSVC_OTJOB from MSVC_FIL where msvc_svcid = '" & Me.servCenterDrpList.SelectedValue & "') > " _
                    & "(select count(*) from FAPT_FIL " _
                    & "where FAPT_TRNTY in ('AS','SS') " _
                    & "and FAPT_APTDT >= CONVERT(date,'" & apptDateBox.Text & "',103) " _
                    & "and FAPT_APTDT < dateadd(day,1,CONVERT(date,'" & apptDateBox.Text & "',103)) " _
                    & "and FAPT_STAT not in ('UA','X') " _
                    & "and TRIM(FAPT_APTTY) in ('M1','M2','M3','M4','M5','M6','S1','S2','S3','W1','W2.','W3.','RM1','RM2','RM3','RM4','RM5','RM6','RM7') " _
                    & "and FAPT_SVCID = '" & Me.servCenterDrpList.SelectedValue & "') then 1 else 0 end) as Flag " _
                    & "", Conn)
                    Conn.Open()
                    Using readerObj As SqlClient.SqlDataReader = Comm.ExecuteReader
                        While readerObj.Read
                            Flag = readerObj("Flag")
                        End While
                    End Using
                    Conn.Close()
                End Using

                If Flag = "0" Then
                    InfoMsgBox.Alert("Maximum Number of Contract Appointments detected for current appointment date!")
                    Return
                End If
            End If
        Next

        msginfo = objXm.GetLabelName("StatusMessage", "SAVEORNOT")
        msgtitle = objXm.GetLabelName("StatusMessage", "SAVETITLE")
        CompDateVal.ErrorMessage = ""
        saveMsgBox.Confirm(msginfo)

    End Sub

    Protected Sub saveMsgBox_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles saveMsgBox.GetMessageBoxResponse
        Dim UserID As String = Session("userID")
        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
            If transTypeBox.Text <> "" Then
                apptEntity.TransactionType = Trim(transTypeBox.Text)
            End If
            If transNoBox.Text <> "" Then
                apptEntity.TransactionNo = Trim(transNoBox.Text)
            End If
            If apptDateBox.Text <> "" Then
                apptEntity.AppointmentDate = Trim(apptDateBox.Text)
            End If
            If CustIDBox.Text <> "" Then
                apptEntity.CustomerID = Trim(CustIDBox.Text)
            End If
            If CustPrxBox.Text <> "" Then
                apptEntity.CustomerPrifx = Trim(CustPrxBox.Text)
            End If
            apptEntity.ROID = ROInfoDrpList.SelectedItem.Value.ToString()
            apptEntity.PICName = Trim(PICNameBox.Text)
            apptEntity.PICContact = Trim(PICContactBox.Text)
            apptEntity.AppointmentType = Trim(apptTypeDrpList.SelectedValue.ToString())
            apptEntity.AppointmentStartTime = Trim(apptStartTimeDrpList.SelectedValue.ToString())
            apptEntity.AppointmentEndTime = Trim(apptEndTimeHBox.Text) + Trim(apptEndTimeMBox.Text)
            apptEntity.TechnicanID = Trim(assignToDrpList.SelectedValue.ToString())
            apptEntity.ReschduleReason = Trim(ResReasonDrpList.SelectedValue.ToString())
            apptEntity.AppointmentNotes = Trim(notesBox.Text)
            apptEntity.AppointmentStatus = Trim(apptStatDrpList.SelectedValue.ToString())
            apptEntity.ServerCenterID = Trim(servCenterDrpList.SelectedValue.ToString())
            apptEntity.ZoneID = Trim(cboZoneID.SelectedValue)
            Session("prevZoneID") = Trim(cboZoneID.SelectedValue)

            apptEntity.IpAddress = Request.UserHostAddress.ToString()
            apptEntity.UserServCenterID = Session("login_svcID")
            apptEntity.CrID = cboCrID.SelectedValue
            ' Modified by Ryan Estandarte 25 Sept 2012
            ' Added by Ryan Estandarte 20 Sept 2012
            'apptEntity.IsInbound = chkIsInbound.Checked
            apptEntity.IsInbound = CType(rdoInboundOutbound.SelectedValue, Boolean)

            ' Added by JC ReasonObjection 31 May 2016
            If apptStatDrpList.SelectedValue = "X" Then
                apptEntity.ReasonObjection = ddlReasonObjection.SelectedValue
            Else
                apptEntity.ReasonObjection = ""
            End If

            ' Added by lyann NatureOfFeedback 27 Dec 2017
            If apptTypeDrpList.SelectedValue.Trim = "MR" Or apptTypeDrpList.SelectedValue.Trim = "MB" Or apptTypeDrpList.SelectedValue.Trim = "PB" Then
                apptEntity.NatureOfFeedback = ddlNatureFeedback.SelectedValue
            Else
                apptEntity.NatureOfFeedback = ""
            End If

            apptEntity.RMSNo = RMStxt.Text 'Added by Tomas 20180613

            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            Me.errlab.Visible = True
            Dim bSuccess As Boolean = False
            If apptStatDrpList.SelectedValue.ToString().Trim() <> Session("FUN-MAPPT-apptStatus") Then
                bSuccess = apptEntity.UpdateAppointment(UserID, "1")
            Else
                bSuccess = apptEntity.UpdateAppointment(UserID, "0")
            End If
            If bSuccess Then

                If Trim(apptStatDrpList.SelectedValue.ToString()) = "SA" Or Trim(apptStatDrpList.SelectedValue.ToString()) = "TG" Or Trim(apptStatDrpList.SelectedValue.ToString()) = "SM" Then

                    If sendSMS() = True Then
                        Me.errlab.Text = objXmlTr.GetLabelName("StatusMessage", "FUN-APPTCNT-SUCCESS")
                        Me.errlab.Text = Me.errlab.Text + ". Service order number sent!"
                    Else
                        Me.errlab.Text = objXmlTr.GetLabelName("StatusMessage", "FUN-APPTCNT-SUCCESS")
                    End If
                    'ElseIf Trim(apptStatDrpList.SelectedValue.ToString()) = "UA" or Trim(apptStatDrpList.SelectedValue.ToString()) = "X" Then

                    'If sendSMS() = True Then
                    'Me.errlab.Text = objXmlTr.GetLabelName("StatusMessage", "FUN-APPTCNT-SUCCESS")

                    'Else
                    'Me.errlab.Text = objXmlTr.GetLabelName("StatusMessage", "FUN-APPTCNT-SUCCESS")
                    'End If
                Else
                    Me.errlab.Text = objXmlTr.GetLabelName("StatusMessage", "FUN-APPTCNT-SUCCESS")
                    Me.errlab.Text = Me.errlab.Text + ". Appointment saved without send Service Order Number"

                End If

                InfoMsgBox.Alert(errlab.Text)
                If apptStatDrpList.SelectedValue.ToString().Trim() <> Session("FUN-MAPPT-apptStatus") Then
                    Me.saveButton.Enabled = False
                    Me.apptStatDrpList.Enabled = False
                End If
            Else
                Dim lstrErrorMessage As String = ""

                lstrErrorMessage = objXmlTr.GetLabelName("StatusMessage", "FUN-APPTCNT-FAILED") & ".\n Please pass this Message to your Administrator (" & apptEntity.ErrorMessage & ")"
                Me.errlab.Text = objXmlTr.GetLabelName("StatusMessage", "FUN-APPTCNT-FAILED")
                InfoMsgBox.Alert(lstrErrorMessage)
            End If
            'Response.Redirect(Request.Url.ToString()) '刷新页面
        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
        End If
    End Sub
    Private Function sendSMS() As Boolean
        Dim flag As Boolean = False

        Dim hp As String = String.Empty

        If rblSendSMS.SelectedValue = "Y" Then
            hp = txtMobile.Text
            'hp = "0124985125"
			'hp ="0122952910"

            If Trim(hp) <> "" Then

                If hp <> "" Then
                    If Left(hp, 1) <> "+" Then
                        If Left(hp, 1) = "0" Then
                            hp = "+6" + hp
                        Else
                            hp = "+" + hp
                        End If
                    End If
                End If

                Dim objAppointment As New clsAppointment()
                Dim strBranchContactNo As String = String.Empty
                Dim ds As DataSet = objAppointment.GetBranchContactNo(servCenterDrpList.SelectedValue)
                If ds.Tables(0).Rows.Count > 0 Then
                    strBranchContactNo = ds.Tables(0).Rows(0)("MSVC_ContactNo").ToString
                End If

                Dim strSMSText As String = String.Empty

                strSMSText = "Elken Service appointment: " + apptDateBox.Text
                strSMSText = strSMSText + ". Confirmation number: " + transNoBox.Text
                strSMSText = strSMSText + ". Any doubt pls call: " + strBranchContactNo
                strSMSText = strSMSText + ". From Elken Service(Do not reply)"
                apptEntity.SendSMSAppointment(Trim(hp), strSMSText, "N", "74")
                apptEntity.SendSMSAppointmentLog(transTypeBox.Text, transNoBox.Text, rblMobile.SelectedValue, txtMobile.Text, txtMobileName.Text)

                flag = True
            End If
        End If

        Return flag

    End Function
    Private Sub JobTypeProperties()
        Dim lstrJobType As String = Me.transTypeBox.Text

        If lstrJobType = fstrjobType_SS Or lstrJobType = fstrjobType_RS Then
            cboZoneID.Enabled = True
            cboZoneID.SelectedIndex = 0

            Me.assignToDrpList.Enabled = False
            cboCrID.Enabled = False
            apptStartTimeDrpList.Enabled = True

        Else
            cboZoneID.Enabled = False
            cboZoneID.SelectedIndex = cboZoneID.Items.Count - 1
            Me.assignToDrpList.Enabled = True
            cboCrID.Enabled = False

            apptStartTimeDrpList.Enabled = False
            apptStartTimeDrpList.SelectedIndex = apptStartTimeDrpList.Items.Count - 1
        End If
        'cboCrID.SelectedIndex = 0
        Me.assignToDrpList.SelectedIndex = 0
        Call CalculateAppointmentEndTime()
        Call GetZoneID()
    End Sub


    Private Sub CalculateAppointmentEndTime()
        'If apptStartTimeDrpList.SelectedValue <> "0000" Then
        '    apptEndTimeHBox.Text = (Convert.ToInt32(apptStartTimeDrpList.SelectedValue.Substring(0, 2)) + 1).ToString()
        '    apptEndTimeMBox.Text = (Convert.ToInt32(apptStartTimeDrpList.SelectedValue.Substring(2, 2)) + 30).ToString()
        '    Dim apptEndTimeM As Integer = Convert.ToInt32(apptEndTimeMBox.Text)
        '    If apptEndTimeM >= 60 Then
        '        apptEndTimeHBox.Text = (Convert.ToInt32(apptEndTimeHBox.Text) + 1).ToString()
        '        apptEndTimeM = apptEndTimeM - 60
        '        apptEndTimeMBox.Text = apptEndTimeM.ToString()
        '        If apptEndTimeM > 0 Or apptEndTimeM < 9 Then
        '            apptEndTimeMBox.Text = "0" + apptEndTimeMBox.Text
        '        End If
        '    End If
        '    apptEndTimeHBox.ReadOnly = False
        '    apptEndTimeMBox.ReadOnly = False
        'Else
        '    apptEndTimeHBox.Text = "23"
        '    apptEndTimeMBox.Text = "59"
        '    apptEndTimeHBox.ReadOnly = True
        '    apptEndTimeMBox.ReadOnly = True
        'End If

        Dim lstrApptEndTime As String
        lstrApptEndTime = GetAppointmentEndTime(apptStartTimeDrpList.SelectedValue)


        apptEndTimeHBox.Text = lstrApptEndTime.Substring(0, 2)
        apptEndTimeMBox.Text = lstrApptEndTime.Substring(3, 2)

    End Sub

    Function GetAppointmentEndTime(ByVal AppointmentStartTime) As String
        Dim EndTimeH, EndTimeM As String

        If AppointmentStartTime <> "0000" Then
            EndTimeH = (Convert.ToInt32(AppointmentStartTime.Substring(0, 2)) + 1).ToString()
            EndTimeM = (Convert.ToInt32(AppointmentStartTime.Substring(2, 2)) + 30).ToString()
            Dim apptEndTimeM As Integer = Convert.ToInt32(EndTimeM)
            If apptEndTimeM >= 60 Then
                EndTimeH = (Convert.ToInt32(EndTimeH) + 1).ToString()
                apptEndTimeM = apptEndTimeM - 60
                EndTimeM = apptEndTimeM.ToString()
                If apptEndTimeM > 0 Or apptEndTimeM < 9 Then
                    EndTimeM = "0" + EndTimeM
                End If
            End If

        Else
            EndTimeH = "23"
            EndTimeM = "59"
        End If

        GetAppointmentEndTime = EndTimeH & ":" & EndTimeM
    End Function

    Sub RequestFromValue()
        apptEndTimeHBox.Text = Request.Form("apptEndTimeHBox")
        apptEndTimeMBox.Text = Request.Form("apptEndTimeMBox")
        apptDateBox.Text = Request.Form("apptDateBox")
        Dim ZoneID As String = Request.Form("cboZoneID")
        'Dim txtZoneID As String = Request.Form("txtZoneID")
        Dim CrID As String = ""
        Dim ThID As String = ""



        CrID = Request.Form("cboCrID")
        ThID = Request.Form("assignToDrpList")

        lastservDateBox.Text = Request.Form("lastservDateBox")
        WarrantyBox.Text = Request.Form("WarrantyBox")
        installByBox.Text = Request.Form("installByBox")
        lastRemDateBox.Text = Request.Form("lastRemDateBox")

        contractNoBox.Text = Request.Form("contractNoBox")
        contEDateBox.Text = Request.Form("contEDateBox")

        lastApptDateBox.Text = Request.Form("lastApptDateBox")

        'Dim RoID As String
        'RoID = Request.Form("ROInfoDrpList")

        Dim ServiceCenterID As String
        ServiceCenterID = servCenterDrpList.SelectedValue


        GetZoneID()
        cboZoneID.SelectedValue = ZoneID

        'Session("ZoneSelectedIndex") = cboZoneID.SelectedIndex

        If CrID Is Nothing Then CrID = ""
        If ThID Is Nothing Then ThID = ""

        Dim dsCrUser As DataSet = apptEntity.GetCrIDBySvcID(ServiceCenterID)
        databonds(dsCrUser.Tables(0), cboCrID, CrID)

        apptEntity.ServerCenterID = ServiceCenterID
        Dim dsThUser As DataSet = apptEntity.GetTCHINFODrpListData()
        databonds(dsThUser.Tables(0), assignToDrpList, ThID)

    End Sub

#Region "Ajax"

    <AjaxPro.AjaxMethod()> _
    Function GetCrIDList(ByVal lstrServiceCenterID As String) As ArrayList
        Dim objAppointment As New clsAppointment()

        Dim dsCrUser As DataSet = objAppointment.GetCrIDBySvcID(lstrServiceCenterID)

        Dim strTemp As String
        Dim Listas As New ArrayList(dsCrUser.Tables(0).Rows.Count)
        Dim i As Integer = 0
        Listas.Add("")
        Try
            For i = 0 To dsCrUser.Tables(0).Rows.Count - 1
                strTemp = dsCrUser.Tables(0).Rows(i).Item(0).ToString & ":" & dsCrUser.Tables(0).Rows(i).Item(0).ToString & "-" & dsCrUser.Tables(0).Rows(i).Item(1).ToString
                Listas.Add(strTemp)
            Next
        Catch
        End Try

        Return Listas
    End Function

    <AjaxPro.AjaxMethod()> _
    Function GetTechnicianIDList(ByVal lstrServiceCenterID As String) As ArrayList
        Dim objAppointment As New clsAppointment()
        objAppointment.ServerCenterID = lstrServiceCenterID
        Dim dsTechnician As DataSet = objAppointment.GetTCHINFODrpListData()

        Dim strTemp As String
        Dim Listas As New ArrayList(dsTechnician.Tables(0).Rows.Count)
        Dim i As Integer = 0
        Listas.Add("")
        Try
            For i = 0 To dsTechnician.Tables(0).Rows.Count - 1
                strTemp = dsTechnician.Tables(0).Rows(i).Item(0).ToString & ":" & dsTechnician.Tables(0).Rows(i).Item(1).ToString & "-" & dsTechnician.Tables(0).Rows(i).Item(0).ToString
                Listas.Add(strTemp)
            Next
        Catch
        End Try

        Return Listas
    End Function

    <AjaxPro.AjaxMethod()> _
    Function GetZoneList(ByVal strTransType As String, ByVal strAreaID As String, ByVal strAppoinmentDate As String, ByVal strServiceCenterID As String, ByVal strTransNo As String) As ArrayList
        Dim objAppointment As New clsAppointment()
        Dim strMaxApt, strMaxZoneApt, lstrTotalApt As String
        Dim strZoneID As String
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        If strAreaID = "" Or strAppoinmentDate = "" Or strServiceCenterID = "" Or Not IsDate(strAppoinmentDate) Then
        Else



            objAppointment.CustomAreaID = strAreaID
            objAppointment.AppointmentDate = strAppoinmentDate
            objAppointment.ServerCenterID = strServiceCenterID
            objAppointment.TransactionType = strTransType
            objAppointment.TransactionNo = strTransNo

            Dim ds As DataSet = objAppointment.GetZoneID()

            Dim strTemp As String
            Dim Listas As New ArrayList(ds.Tables(0).Rows.Count)
            Dim i As Integer = 0

            For i = 0 To ds.Tables(0).Rows.Count - 1
                strZoneID = ds.Tables(0).Rows(i).Item(0).ToString


                lstrTotalApt = ds.Tables(0).Rows(i).Item(1).ToString
                strMaxZoneApt = ds.Tables(0).Rows(i).Item(2).ToString

                If strTransType = fstrjobType_SS Or strTransType = fstrjobType_RS Then
                    strMaxApt = strMaxZoneApt
                Else
                    strMaxApt = lstrTotalApt
                End If

                If (strTransType = "SS") Or (strZoneID = "" And strTransType = "AS") Then
                    strTemp = strZoneID & ":" & IIf(strZoneID = "", "Other ", strZoneID) & " (" & lstrTotalApt & "/" & strMaxApt & ")"
                    Listas.Add(strTemp)
                End If

            Next
            Return Listas

        End If

    End Function


    <AjaxPro.AjaxMethod()> _
    Function GetAppointmentEndTimeChange(ByVal AppointmentStartTimea As String) As String
        GetAppointmentEndTimeChange = GetAppointmentEndTime(AppointmentStartTimea)
    End Function

    <AjaxPro.AjaxMethod()> _
    Function GetRoInformationChange(ByVal RoID As String, ByVal CustomerPrefix As String, ByVal CustomerID As String, ByVal AppointmentDate As String) As ArrayList
        Dim ds As DataSet
        Dim lastservDate As String = ""
        Dim Warranty As String = ""
        Dim installBy As String = ""
        Dim lastRemDate As String = ""
        Dim contractNo As String = ""
        Dim contEDate As String = ""
        Dim lastApptDate As String = ""
        Dim Listas As New ArrayList(7)


        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        If IsDate(AppointmentDate) Then

            If RoID <> "" Then
                apptEntity.ModeID = ""
                apptEntity.SerialNo = ""
                apptEntity.ROID = RoID

                ds = apptEntity.GetROAndContractInfo()
                If Not ds Is Nothing Then
                    'lastservDateBox.Text = ds.Tables("MROU_FIL").Rows(0).Item("MROU_LSVDT").ToString()
                    'WarrantyBox.Text = ds.Tables("MROU_FIL").Rows(0).Item("MROU_WRRDT").ToString()
                    'installByBox.Text = ds.Tables("MROU_FIL").Rows(0).Item("MROU_TCHID").ToString()
                    'lastRemDateBox.Text = ds.Tables("MROU_FIL").Rows(0).Item("MROU_LRMDT").ToString()
                    lastservDate = ds.Tables("MROU_FIL").Rows(0).Item("MROU_LSVDT").ToString()
                    Warranty = ds.Tables("MROU_FIL").Rows(0).Item("MROU_WRRDT").ToString()
                    installBy = ds.Tables("MROU_FIL").Rows(0).Item("MROU_TCHID").ToString()
                    lastRemDate = ds.Tables("MROU_FIL").Rows(0).Item("MROU_LRMDT").ToString()

                End If
            End If

            apptEntity.CustomerID = CustomerID
            apptEntity.CustomerPrifx = CustomerPrefix
            apptEntity.ROID = RoID
            apptEntity.AppointmentDate = AppointmentDate
            ds = apptEntity.GetContractInfo()
            If ds.Tables("FCU1_FIL").Rows.Count = 1 Then
                'contractNoBox.Text = ds.Tables("FCU1_FIL").Rows(0).Item("FCU1_CONID").ToString()
                'contEDateBox.Text = ds.Tables("FCU1_FIL").Rows(0).Item("FCU1_OBSDT").ToString()
                contractNo = ds.Tables("FCU1_FIL").Rows(0).Item("FCU1_CONID").ToString()
                contEDate = ds.Tables("FCU1_FIL").Rows(0).Item("FCU1_OBSDT").ToString()
            End If
            'lastApptDateBox.Text = apptEntity.GetLastApptDate(Session("FUN-MAPPT-CREATIME"))

            If IsDate(AppointmentDate) Then
                lastApptDate = apptEntity.GetLastApptDate(AppointmentDate)
            End If



            'lastservDate & ":" & Warranty & ":" & installBy & ":" & lastRemDate & ":" & contractNo & ":" & contEDate & ":" & lastApptDate

            Listas.Add(lastservDate)
            Listas.Add(Warranty)
            Listas.Add(installBy)
            Listas.Add(lastRemDate)
            Listas.Add(contractNo)
            Listas.Add(contEDate)
            Listas.Add(lastApptDate)
            Return Listas
        Else
            Listas.Add("")
            Listas.Add("")
            Listas.Add("")
            Listas.Add("")
            Listas.Add("")
            Listas.Add("")
            Listas.Add("")
            Return Listas
        End If



    End Function

#End Region


    Private Sub GetZoneID()
        Dim fstrMaxApt, strZoneID As String
        Dim strTransType As String = Me.transTypeBox.Text
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        If areaBox.Text = "" Or Not IsDate(apptDateBox.Text) Or Me.servCenterDrpList.SelectedValue = "" Then
        Else

            apptEntity.CustomAreaID = areaBox.Text
            apptEntity.AppointmentDate = apptDateBox.Text
            apptEntity.ServerCenterID = Me.servCenterDrpList.SelectedValue
            apptEntity.TransactionType = Me.transTypeBox.Text
            apptEntity.TransactionNo = Me.transNoBox.Text
            Dim ds As DataSet = apptEntity.GetZoneID()
            If ds Is Nothing Then
                Response.Redirect("~/PresentationLayer/error.aspx")
            End If

            Dim count As Integer = 0
            cboZoneID.Items.Clear()
            Dim row As DataRow
            For Each row In ds.Tables(0).Rows
                Dim NewItem As New ListItem()

                If Me.transTypeBox.Text = fstrjobType_SS Or Me.transTypeBox.Text = fstrjobType_RS Then
                    fstrMaxApt = row("maxzoneapt")
                Else
                    fstrMaxApt = row("totalapt")
                End If
                strZoneID = row("FZN2_ZONID")
                If (strTransType = "SS") Or (strZoneID = "" And strTransType = "AS") Then
                    NewItem.Text = IIf(strZoneID = "", "Other ", strZoneID) & " (" & row("totalapt") & "/" & fstrMaxApt & ")"
                    NewItem.Value = row("FZN2_ZONID")
                    cboZoneID.Items.Add(NewItem)
                End If
                count = count + 1
            Next
        End If

    End Sub

    Protected Sub apptStatDrpList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles apptStatDrpList.SelectedIndexChanged
        If apptStatDrpList.SelectedValue = "X" Then
            Dim dt As DataTable
            dt = apptEntity.GetObjectionReason().Tables(0)
            trReasonObjection.Visible = True
            ddlReasonObjection.DataSource = dt
            ddlReasonObjection.DataValueField = "ReasonObj_ID"
            ddlReasonObjection.DataTextField = "ReasonObj_Desc"
            ddlReasonObjection.DataBind()
            Dim list As New ListItem("Please select one", "")
            ddlReasonObjection.Items.Insert(0, list)
            ResReasonDrpList.Enabled = False
        Else
            trReasonObjection.Visible = False
            ResReasonDrpList.Enabled = True
        End If

        'Added By JC,SMR1604/1714/Auto SMS Service Order Number
        If Trim(apptStatDrpList.SelectedValue.ToString()) = "SA" Or Trim(apptStatDrpList.SelectedValue.ToString()) = "TG" Or Trim(apptStatDrpList.SelectedValue.ToString()) = "SM" Then
            'rblSendSMS.SelectedValue = "Y"
            'rblSendSMS.Enabled = True
            'rblSendSMS_SelectedIndexChanged(Nothing, Nothing)
            Dim CustomerEntity As New clsCustomerRecord()
            Dim telepass As New clsCommonClass()
            Dim intCtr As Integer
            CustomerEntity.CustomerID = CustIDBox.Text.ToString()
            CustomerEntity.Customerpf = contryBox.Text.ToString()
            Dim edtReaderTele As DataSet = CustomerEntity.GetCustomerTele()
            Dim mobFlag1 As Boolean = False
            Dim mobFlag2 As Boolean = False

            For intCtr = 0 To edtReaderTele.Tables(0).Rows.Count - 1
                Select Case edtReaderTele.Tables(0).Rows(intCtr).Item(0).ToString
                    Case "MOB1" 'o tele
                        If telepass.passconverttel(edtReaderTele.Tables(0).Rows(intCtr).Item(2).ToString) <> String.Empty Then
                            mobFlag1 = True
                            txtMobile.Text = telepass.passconverttel(edtReaderTele.Tables(0).Rows(intCtr).Item(2).ToString)
                            txtMobileName.Text = edtReaderTele.Tables(0).Rows(intCtr).Item(1).ToString()
                        End If
                    Case "MOB2" 'o tele
                        If telepass.passconverttel(edtReaderTele.Tables(0).Rows(intCtr).Item(2).ToString) <> String.Empty Then
                            mobFlag2 = True
                            txtMobile.Text = telepass.passconverttel(edtReaderTele.Tables(0).Rows(intCtr).Item(2).ToString)
                            txtMobileName.Text = edtReaderTele.Tables(0).Rows(intCtr).Item(1).ToString()
                        End If
                End Select
            Next

            If mobFlag1 = False And mobFlag2 = False Then
                rblSendSMS.SelectedValue = "N"
                rblSendSMS.Enabled = False
                rblSendSMS_SelectedIndexChanged(Nothing, Nothing)
            ElseIf mobFlag1 = True And mobFlag2 = False Then
                rblSendSMS.Enabled = True
                tblMobile.Visible = True
                rblMobile.SelectedValue = "M1"
                rblMobile.Enabled = False
            ElseIf mobFlag1 = False And mobFlag2 = True Then
                rblSendSMS.Enabled = True
                tblMobile.Visible = True
                rblMobile.SelectedValue = "M2"
                rblMobile.Enabled = False
            Else
                rblSendSMS.Enabled = True
                rblSendSMS.SelectedValue = "Y"
                rblSendSMS_SelectedIndexChanged(Nothing, Nothing)
            End If
        Else
            rblSendSMS.SelectedValue = "N"
            rblSendSMS.Enabled = False
            rblSendSMS_SelectedIndexChanged(Nothing, Nothing)
        End If

    End Sub

    Protected Sub apptStatDrpList_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles apptStatDrpList.TextChanged
        EnableByAppointmentStatus(apptStatDrpList.SelectedValue)
    End Sub

    Protected Sub rblSendSMS_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblSendSMS.SelectedIndexChanged
        If rblSendSMS.SelectedValue = "Y" Then
            tblMobile.Visible = True
            rblMobile.SelectedValue = "M1"
            rblMobile_TextChanged(Nothing, Nothing)
        Else
            tblMobile.Visible = False
        End If

    End Sub

    Protected Sub rblMobile_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblMobile.TextChanged
        Dim CustomerEntity As New clsCustomerRecord()
        Dim telepass As New clsCommonClass()
        Dim intCtr As Integer
        CustomerEntity.CustomerID = CustIDBox.Text.ToString()
        CustomerEntity.Customerpf = contryBox.Text.ToString()
        Dim edtReaderTele As DataSet = CustomerEntity.GetCustomerTele()
        txtMobile.Text = ""
        txtMobileName.Text = ""
        txtMobile.Enabled = False
        txtMobileName.Enabled = False

        If rblMobile.SelectedValue = "M1" Then
            For intCtr = 0 To edtReaderTele.Tables(0).Rows.Count - 1
                Select Case edtReaderTele.Tables(0).Rows(intCtr).Item(0).ToString
                    Case "MOB1" 'o tele
                        txtMobile.Text = telepass.passconverttel(edtReaderTele.Tables(0).Rows(intCtr).Item(2).ToString)
                        txtMobileName.Text = edtReaderTele.Tables(0).Rows(intCtr).Item(1).ToString()
                End Select
            Next
        Else
            For intCtr = 0 To edtReaderTele.Tables(0).Rows.Count - 1
                Select Case edtReaderTele.Tables(0).Rows(intCtr).Item(0).ToString
                    Case "MOB2" 'mob 2
                        txtMobile.Text = telepass.passconverttel(edtReaderTele.Tables(0).Rows(intCtr).Item(2).ToString)
                        txtMobileName.Text = edtReaderTele.Tables(0).Rows(intCtr).Item(1).ToString()
                End Select
            Next
        End If
    End Sub

    Protected Sub apptTypeDrpList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles apptTypeDrpList.SelectedIndexChanged
        If apptTypeDrpList.SelectedValue.Trim = "MR" Or apptTypeDrpList.SelectedValue.Trim = "MB" Or apptTypeDrpList.SelectedValue.Trim = "PB" Then
            trNatureFeedback.Visible = True

            Dim dtNatureFeedback As DataTable
            dtNatureFeedback = apptEntity.GetNatureOfFeedback(apptTypeDrpList.SelectedValue.Trim).Tables(0)
            ddlNatureFeedback.DataSource = dtNatureFeedback
            ddlNatureFeedback.DataValueField = "DFeedback_ID"
            ddlNatureFeedback.DataTextField = "Description"
            ddlNatureFeedback.DataBind()
            Dim list As New ListItem("Please select one", "")
            ddlNatureFeedback.Items.Insert(0, list)
        Else
            trNatureFeedback.Visible = False
        End If
    End Sub

End Class
