﻿Imports BusinessEntity
Imports System.Data
' Added by Ryan Estandarte 25 Sept 2012
Imports System.Collections.Generic
Imports System.Data.SqlClient


Partial Class PresentationLayer_function_addappointment_aspx
    Inherits System.Web.UI.Page
    Dim objXmlTr As New clsXml
    Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
    Dim apptEntity As New clsAppointment()
    Dim ROID As String = ""
    Dim fstrDelimiter As String = "-"
    Dim fstrjobType_AS As String = "AS"
    Dim fstrjobType_SS As String = "SS"
    Dim fstrjobType_RS As String = "RS"



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Page.MaintainScrollPositionOnPostBack = True
        AjaxPro.Utility.RegisterTypeForAjax(GetType(PresentationLayer_function_addappointment_aspx))

        If Not Page.IsPostBack Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try
            'Session("FUN-AAPPT-ROID") = ""
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
            'validator
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            transTypeError.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "FUN-APPT-TRANSTY")
            'transNoError.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "FUN-APPT-TRANSNO")
            apptDateError.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "FUN-APPT-APPTDATE")
            apptTypeError.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "FUN-APPT-APPTTYPE")
            servCIDError.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "FUN-APPT-APPTSEVCID")
            apptEndTimeHError.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "FUN-APPT-APPTENDTIME")
            apptEndTimeMError.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "FUN-APPT-APPTENDTIME")
            PICContactError.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "FUN-APPT-PICCONTACT")
            PICContactNullError.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "FUN-APPT-PICCONTACTNULL")
            CompDateVal.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "INVALID_DATE")
            ReasonObjection.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-REASONOBJECTION")
            NatureFeedbackError.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "FUN-APPT-NATFEEDBACK")

            Me.rfvCrID.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "ENTER_CR_ID")

            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            Dim CustomID As String = Request.Params("custID").ToString()
            Dim CustomPf As String = Request.Params("custPf").ToString()
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-AAPPT-TITL")
            transtypeLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-TRANSTYPE")
            transNoLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-TRANSNO")
            ROInfoLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-ROINFO")
            apptStatusLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-STAUTS")
            lastservDateLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-LASTSERVDATE")
            WarrantyLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-WARRANTY")
            PICNameLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-PICNAME")
            PICContactLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-PICCONTACT")
            CustIDLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-CUSTID")
            CustPrxBox.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-CUSTPRX")
            contEDateLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-CONTEDATE")
            CustNameLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-CUSTNAME")
            addr1Lab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-ADDR1")
            addr2Lab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-ADDR2")
            POCodeLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-POCODE")
            contryLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-CONTRY")
            areaLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-AREA")
            stateLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-STATE")
            apptDateLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-APPTDATE")
            apptTypeLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-APPTTYPE")
            NatureFeedbackLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-NATUREFEEDBACK")
            apptStartTimeLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-APPTSTARTTIME")
            apptEndTimeLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-APPTENDTIME")
            servCenterLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-SERVCENTER")
            installByLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-INSTALLBY")
            assignToLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-ASSIGNTO")
            lastRemDateLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-LASTREMDATE")
            lastApptDateLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-LASTAPPTDATE")
            contractNoLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-CONTRACTNO")
            notesLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-NOTES")
            custNotesLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-CUSTNOTES")
            assToZoneLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-ASSTOZONE")
            detaillkBtn.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-DETAIL")
            chkSchedulelkBtn.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-CHKSCHEDULE")
            servHistorylkBtn.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-SERVHISTORY")
            contractHistroylkBtn.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-CONTRACTHISTORY")
            saveButton.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Save")
            cancelLink.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Cancel")
            lblCrID.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CrUserID")
            ' Added by Ryan Estandarte 20 Sept 2012
            lblIsInbound.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-MAPPT-INBOUND")
            RMSlbl.Text = "RMS No"

            ' Added by Ryan Estandarte 25 Sept 2012
            Dim optionDic As New Dictionary(Of String, String)
            optionDic.Add("Inbound", "True")
            optionDic.Add("Outbound", "False")
            rdoInboundOutbound.DataSource = optionDic
            rdoInboundOutbound.DataTextField = "Key"
            rdoInboundOutbound.DataValueField = "Value"
            rdoInboundOutbound.DataBind()

            trNatureFeedback.Visible = False

            cancelLink.NavigateUrl = Request.UrlReferrer.ToString()

            lblNoteUP.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            lblNoteDown.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")

            If CustomID.Trim() <> "" And CustomPf.Trim() <> "" Then
                'dropdownlist apointment status
                Dim apptStat As New clsUtil()
                Dim statParam As ArrayList = New ArrayList

                'DDApptStatus.Items.Add(New ListItem("", ""))
                statParam = apptStat.searchconfirminf("APPTStatus")
                Dim count As Integer
                Dim statid As String
                Dim statnm As String
                For count = 0 To statParam.Count - 1
                    statid = statParam.Item(count)
                    statnm = statParam.Item(count + 1)
                    count = count + 1
                    If Trim(statid) = "SA" Or Trim(statid) = "X" Then
                        DDApptStatus.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                    End If
                Next
                GetData(CustomID, CustomPf)
            Else
                Response.Redirect("~/PresentationLayer/error.aspx")
            End If
        Else
            Me.errlab.Visible = False
        End If
        HypCal.NavigateUrl = "javascript:DoCal(document.form1.apptDateBox);"
        'rblSendSMS_SelectedIndexChanged(Nothing, Nothing)
    End Sub
    Private Sub GetData(ByVal CustomID As String, ByVal CustomPf As String)
        'edit June 25
        'transTypeDrpList.Items.Add(New ListItem("", ""))
        transTypeDrpList.Items.Add(New ListItem(fstrjobType_AS, fstrjobType_AS))
        transTypeDrpList.Items.Add(New ListItem(fstrjobType_SS, fstrjobType_SS))
        transTypeDrpList.Items.Add(New ListItem(fstrjobType_RS, fstrjobType_RS))
        transTypeDrpList.SelectedIndex = 1


        'end edit June 25 

        CustIDBox.Text = CustomID
        CustPrxBox.Text = CustomPf
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        apptDateBox.Text = Date.Today
        apptEntity.CustomerID = CustomID
        apptEntity.CustomerPrifx = CustomPf
        apptEntity.ServiceType = "'NORMAL','FREE'"

        'appointment status dropdownlsit
        Dim count As Integer
        Dim apptStat As New clsUtil()
        Dim statParam As ArrayList = New ArrayList
        statParam = apptStat.searchconfirminf("APPTStatus")
        'apptStatDrpList.Items.Add(New ListItem(statParam.Item(1), statParam.Item(0))) 'active
        apptStatBox.Text = statParam.Item(1) 'active

        Dim ds As DataSet = apptEntity.GetDropDownListData(Session("userID"))
        If ds Is Nothing Or ds.Tables("MCUS_FIL").Rows.Count <> 1 Then
            Response.Redirect("~/PresentationLayer/error.aspx")
        End If
        If ds.Tables("MADR_FIL").Rows.Count = 1 Then
            addr1Box.Text = ds.Tables("MADR_FIL").Rows(0).Item("MADR_ADDR1").ToString()
            addr2Box.Text = ds.Tables("MADR_FIL").Rows(0).Item("MADR_ADDR2").ToString()
            POCodeBox.Text = ds.Tables("MADR_FIL").Rows(0).Item("MADR_PCODE").ToString()
            contryBox.Text = ds.Tables("MADR_FIL").Rows(0).Item("MADR_CTRID").ToString()
            stateBox.Text = ds.Tables("MADR_FIL").Rows(0).Item("MADR_STAID").ToString()
            areaBox.Text = ds.Tables("MADR_FIL").Rows(0).Item("MADR_AREID").ToString()
        End If
        CustNameBox.Text = ds.Tables("MCUS_FIL").Rows(0).Item("MCUS_ENAME").ToString()
        custNotesBox.Text = ds.Tables("MCUS_FIL").Rows(0).Item("MCUS_NOTES").ToString()
        'edit June 25
        PICNameBox.Text = CustNameBox.Text

        Dim lstrTelNo As String = Request.Params("telno")

        If lstrTelNo Is Nothing Or lstrTelNo = "" Then
            If ds.Tables("MTEL_FIL").Rows.Count > 0 Then
                PICContactBox.Text = ds.Tables("MTEL_FIL").Rows(0).Item("MTEL_TELNO").ToString()
            End If
        Else
            PICContactBox.Text = lstrTelNo
        End If
        'end edit June 25

        'RO Info dropdownlist
        ROInfoDrpList.Items.Add(New ListItem("", ""))
        For count = 0 To ds.Tables("ROINFO").Rows.Count - 1
            Dim NewItem As New ListItem()
            NewItem.Text = ds.Tables("ROINFO").Rows(count).Item("MODEID").ToString() + fstrDelimiter + _
                    ds.Tables("ROINFO").Rows(count).Item("SERIALNO").ToString()
            NewItem.Value = ds.Tables("ROINFO").Rows(count).Item("ROID").ToString()
            ROInfoDrpList.Items.Add(NewItem)
        Next
        'If ROInfoDrpList.Items.Count = 1 Then
        '    Response.Redirect("~/PresentationLayer/error.aspx") '客户没有买水机()
        'End If
        ROInfoDrpList.SelectedIndex = 0

        'appointment Type
        databonds(ds.Tables("MSV1_FIL"), apptTypeDrpList)
        'apptTypeDrpList.Items.Insert(0, New ListItem("", ""))
        apptTypeDrpList.SelectedIndex = 0

        'appointment start time dropdownlist
        Dim apptSTime As New clsUtil()
        Dim STimeParam As ArrayList = New ArrayList
        STimeParam = apptSTime.searchconfirminf("APPTSTIME")
        Dim STimeid As String
        Dim STimenm As String
        For count = 0 To STimeParam.Count - 1
            STimeid = STimeParam.Item(count)
            STimenm = STimeParam.Item(count + 1)
            count = count + 1
            apptStartTimeDrpList.Items.Add(New ListItem(STimenm.ToString(), STimeid.ToString()))
        Next
        apptStartTimeDrpList.SelectedIndex = STimeParam.Count / 2 - 1
        apptEndTimeHBox.Text = "23"
        apptEndTimeMBox.Text = "59"
        apptEndTimeHBox.ReadOnly = True
        apptEndTimeMBox.ReadOnly = True

        'Server Center dropdownlist
        Dim dsServerCenter As DataSet = apptEntity.GetSVCIDByUserID(Session("userID"))
        databonds(dsServerCenter.Tables("ServerCenterID"), servCenterDrpList)

        servCenterDrpList.SelectedValue = Session("login_svcID")

        'assign to Technican dropdownlist
        assignToDrpList.Items.Add(New ListItem("", ""))
        assignToDrpList.SelectedIndex = 0

        If servCenterDrpList.SelectedItem.Value <> "" Then
            apptEntity.ServerCenterID = servCenterDrpList.SelectedItem.Value
            Dim dsTCHINFO As DataSet = apptEntity.GetTCHINFODrpListData()
            If dsTCHINFO Is Nothing Then
                Response.Redirect("~/PresentationLayer/error.aspx")
            End If
            databondsNameID(dsTCHINFO.Tables("TCHINFO"), assignToDrpList)
            'assignToDrpList.Items.Insert(0, New ListItem("", ""))
            assignToDrpList.SelectedIndex = 0
        Else
            assignToDrpList.Items.Clear()
            assignToDrpList.Items.Add(New ListItem("", ""))
            assignToDrpList.SelectedIndex = 0
        End If

        If addr1Box.Text = "" Or addr2Box.Text = "" Then
            Me.saveButton.Enabled = False
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            Me.errlab.Text = objXmlTr.GetLabelName("StatusMessage", "FUN-APPTCNT-NOADDR")
            Me.errlab.Visible = True
            InfoMsgBox.Alert(errlab.Text)
        End If

        'apptEntity.CustomerID = CustIDBox.Text
        'apptEntity.CustomerPrifx = CustPrxBox.Text
        'apptEntity.ROID = ROID
        'ds = apptEntity.GetContractInfo()
        'If ds.Tables("FCU1_FIL").Rows.Count = 1 Then
        '    contractNoBox.Text = ds.Tables("FCU1_FIL").Rows(0).Item("FCU1_CONID").ToString()
        '    contEDateBox.Text = ds.Tables("FCU1_FIL").Rows(0).Item("FCU1_OBSDT").ToString()
        'End If

        If Request.Params("ROID") IsNot Nothing Then
            ROInfoDrpList.SelectedValue = Request.Params("ROID")
            DisplayRoInfoForAppointment()
        End If
        lastApptDateBox.Text = apptEntity.GetLastApptDate(Date.Now)

        JobTypeProperties()
        Me.GetZoneID()

        Dim crtCrID As String = Session("userID")
        Dim dsCrUser As DataSet = apptEntity.GetCrIDBySvcID(Session("login_svcID"))
        databonds(dsCrUser.Tables(0), cboCrID)
        cboCrID.SelectedValue = crtCrID

        'Added By JC,SMR1604/1714/Auto SMS Service Order Number

        Dim CustomerEntity As New clsCustomerRecord()
        Dim telepass As New clsCommonClass()
        Dim intCtr As Integer
        CustomerEntity.CustomerID = CustIDBox.Text.ToString()
        CustomerEntity.Customerpf = contryBox.Text.ToString()
        Dim edtReaderTele As DataSet = CustomerEntity.GetCustomerTele()
        Dim mobFlag1 As Boolean = False
        Dim mobFlag2 As Boolean = False

        For intCtr = 0 To edtReaderTele.Tables(0).Rows.Count - 1
            Select Case edtReaderTele.Tables(0).Rows(intCtr).Item(0).ToString
                Case "MOB1" 'o tele
                    If telepass.passconverttel(edtReaderTele.Tables(0).Rows(intCtr).Item(2).ToString) <> String.Empty Then
                        mobFlag1 = True
                        txtMobile.Text = telepass.passconverttel(edtReaderTele.Tables(0).Rows(intCtr).Item(2).ToString)
                        txtMobileName.Text = edtReaderTele.Tables(0).Rows(intCtr).Item(1).ToString()
                    End If
                Case "MOB2" 'o tele
                    If telepass.passconverttel(edtReaderTele.Tables(0).Rows(intCtr).Item(2).ToString) <> String.Empty Then
                        mobFlag2 = True
                        txtMobile.Text = telepass.passconverttel(edtReaderTele.Tables(0).Rows(intCtr).Item(2).ToString)
                        txtMobileName.Text = edtReaderTele.Tables(0).Rows(intCtr).Item(1).ToString()
                    End If
            End Select
        Next

        apptEntity.TransactionType = Trim(transTypeDrpList.SelectedValue)
        apptEntity.TransactionNo = Trim(transNoBox.Text)
        Dim xDtSet As DataSet = apptEntity.GetAppointmentDetailsByID()
        'Populate ReasonObjection
        If DDApptStatus.SelectedValue = "X" Then
            Dim dt As DataTable
            dt = apptEntity.GetObjectionReason().Tables(0)
            trReasonObjection.Visible = True
            ddlReasonObjection.DataSource = dt
            ddlReasonObjection.DataValueField = "ReasonObj_ID"
            ddlReasonObjection.DataTextField = "ReasonObj_Desc"
            ddlReasonObjection.DataBind()
            Dim list As New ListItem("Please select one", "")
            ddlReasonObjection.Items.Insert(0, list)
            If ddlReasonObjection.Items.FindByValue(xDtSet.Tables("FAPT_FIL").Rows(0).Item("reasonobjection").ToString()) Is Nothing Then
            ElseIf xDtSet.Tables("FAPT_FIL").Rows(0).Item("reasonobjection").ToString() = "" Then
            Else
                ddlReasonObjection.SelectedValue = xDtSet.Tables("FAPT_FIL").Rows(0).Item("reasonobjection").ToString()
            End If
        Else
            trReasonObjection.Visible = False
        End If

        '10/01/2019 SMR1901/2587 Enable selection to send SMS by user choice
        If mobFlag1 = False And mobFlag2 = False Then
            rblSendSMS.SelectedValue = "N"
            rblSendSMS.Enabled = False
            rblSendSMS_SelectedIndexChanged(Nothing, Nothing)
        ElseIf mobFlag1 = True And mobFlag2 = False Then
            rblSendSMS.Enabled = True
            tblMobile.Visible = True
            rblMobile.SelectedValue = "M1"
            'rblMobile.Enabled = False
        ElseIf mobFlag1 = False And mobFlag2 = True Then
            rblSendSMS.Enabled = True
            tblMobile.Visible = True
            rblMobile.SelectedValue = "M2"
            'rblMobile.Enabled = False
        Else
            'rblSendSMS.Enabled = False
            rblSendSMS.SelectedValue = "Y"
            rblSendSMS_SelectedIndexChanged(Nothing, Nothing)
        End If

    End Sub

    Public Sub databondsNameID(ByRef dt As DataTable, ByRef dropdown As DropDownList)
        Dim count As Integer = 0
        dropdown.Items.Clear()
        dropdown.Items.Add(New ListItem("", ""))
        Dim row As DataRow
        For Each row In dt.Rows
            Dim NewItem As New ListItem()
            NewItem.Text = row("name") & "-" & row("id")
            NewItem.Value = row("id")
            dropdown.Items.Add(NewItem)
            count = count + 1
        Next
        'If dt.Rows.Count = 0 Then '处理数据表为空的情况
        '    dropdown.Items.Insert(0, New ListItem("", ""))
        'End If
    End Sub


    Public Sub databonds(ByRef dt As DataTable, ByRef dropdown As DropDownList)
        Dim count As Integer = 0
        dropdown.Items.Clear()
        dropdown.Items.Add(New ListItem("", ""))
        Dim row As DataRow
        For Each row In dt.Rows
            Dim NewItem As New ListItem()
            NewItem.Text = row("id") & "-" & row("name")
            NewItem.Value = row("id")
            dropdown.Items.Add(NewItem)
            count = count + 1
        Next
        'If dt.Rows.Count = 0 Then '处理数据表为空的情况
        '    dropdown.Items.Insert(0, New ListItem("", ""))
        'End If
    End Sub

    Protected Sub apptDateCalendar_CalendarVisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles apptDateCalendar.CalendarVisibleChanged
        'Dim script As String = "self.location='#ApptLoc';"
        'Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ApptLoc", script, True)
    End Sub

    'Protected Sub apptDateCalendar_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles apptDateCalendar.Load
    '    Dim script As String = "self.location='#ApptLoc';"
    '    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ApptLoc", script, True)
    'End Sub

    'Protected Sub apptDateImg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles apptDateImg.Click
    '    apptDateCalendar.Visible = Not apptDateCalendar.Visible
    '    apptDateCalendar.Style.Item("position") = "absolute"
    'End Sub
    'Protected Sub apptDateCalendar_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles apptDateCalendar.SelectionChanged
    '    System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
    '    apptDateBox.Text = apptDateCalendar.SelectedDate
    '    apptDateCalendar.Visible = False
    '    Me.assToZoneBox.Text = GetZoneID()
    'End Sub
    Protected Sub apptDateCalendar_SelectedDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles apptDateCalendar.SelectedDateChanged
        'assToZoneBox.Text = 
        GetZoneID()
        Call DisplayRoInfoForAppointment()
        'Dim script As String = "self.location='#ApptLoc';"
        'Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ApptLoc", script, True)
    End Sub
    Private Sub GetZoneID()
        Dim fstrMaxApt As String
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        If areaBox.Text = "" Or Not IsDate(apptDateBox.Text) Or Me.servCenterDrpList.SelectedValue = "" Then
        Else

            apptEntity.CustomAreaID = areaBox.Text
            apptEntity.AppointmentDate = apptDateBox.Text
            apptEntity.ServerCenterID = Me.servCenterDrpList.SelectedValue
            apptEntity.TransactionNo = ""
            apptEntity.TransactionType = ""

            If transTypeDrpList.SelectedValue = fstrjobType_RS Then
                Dim ds As DataSet = apptEntity.GetZoneIDRS()
                If ds Is Nothing Then
                    'Response.Redirect("~/PresentationLayer/error.aspx")
                End If

                Dim count As Integer = 0
                cboZoneID.Items.Clear()
                Dim row As DataRow
                For Each row In ds.Tables(0).Rows
                    Dim NewItem As New ListItem()

                    If transTypeDrpList.SelectedValue = fstrjobType_RS Then
                        fstrMaxApt = row("maxzoneapt")
                    Else
                        fstrMaxApt = row("totalapt")
                    End If

                    NewItem.Text = IIf(row("FZN2_ZONID") = "", "Other ", row("FZN2_ZONID")) & " (" & row("totalapt") & "/" & fstrMaxApt & ")"
                    NewItem.Value = row("FZN2_ZONID")
                    cboZoneID.Items.Add(NewItem)
                    count = count + 1
                Next
            Else
                Dim ds As DataSet = apptEntity.GetZoneID()
                If ds Is Nothing Then
                    'Response.Redirect("~/PresentationLayer/error.aspx")
                End If

                Dim count As Integer = 0
                cboZoneID.Items.Clear()
                Dim row As DataRow
                For Each row In ds.Tables(0).Rows
                    Dim NewItem As New ListItem()

                    If transTypeDrpList.SelectedValue = fstrjobType_SS Then
                        fstrMaxApt = row("maxzoneapt")
                    Else
                        fstrMaxApt = row("totalapt")
                    End If

                    NewItem.Text = IIf(row("FZN2_ZONID") = "", "Other ", row("FZN2_ZONID")) & " (" & row("totalapt") & "/" & fstrMaxApt & ")"
                    NewItem.Value = row("FZN2_ZONID")
                    cboZoneID.Items.Add(NewItem)
                    count = count + 1
                Next
            End If

            
        End If

    End Sub

    Sub DisplayRoInfoForAppointment()
        Dim ds As DataSet
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        If ROInfoDrpList.SelectedValue <> "" Then
            apptEntity.ModeID = ROInfoDrpList.SelectedItem.Text.Split(fstrDelimiter)(0).ToString()
            apptEntity.SerialNo = ROInfoDrpList.SelectedItem.Text.Split(fstrDelimiter)(1).ToString()
            apptEntity.ROID = ROInfoDrpList.SelectedItem.Value.ToString.Trim
            ds = apptEntity.GetROAndContractInfo()
            If ds Is Nothing Then
                Response.Redirect("~/PresentationLayer/error.aspx")
            End If
            '处理异常***未完成*******
            If ds.Tables("MROU_FIL").Rows.Count = 1 Then
                lastservDateBox.Text = ds.Tables("MROU_FIL").Rows(0).Item("MROU_LSVDT").ToString()
                WarrantyBox.Text = ds.Tables("MROU_FIL").Rows(0).Item("MROU_WRRDT").ToString()
                installByBox.Text = ds.Tables("MROU_FIL").Rows(0).Item("MROU_TCHID").ToString()
                'lastApptDateBox.Text = ds.Tables("MROU_FIL").Rows(0).Item("MROU_APTDT").ToString()
                lastRemDateBox.Text = ds.Tables("MROU_FIL").Rows(0).Item("MROU_LRMDT").ToString()
                ROID = ds.Tables("MROU_FIL").Rows(0).Item("MROU_ROUID").ToString()
            End If
            'servHistorylkBtn.Visible = True
            'contractHistroylkBtn.Visible = True
        Else
            lastservDateBox.Text = ""
            WarrantyBox.Text = ""
            'contEDateBox.Text = ""
            installByBox.Text = ""
            'lastApptDateBox.Text = ""
            lastRemDateBox.Text = ""
            'contractNoBox.Text = ""
            ROID = ""
            'servHistorylkBtn.Visible = False
            'contractHistroylkBtn.Visible = False
        End If
        'ROInfoDrpList.SelectedItem.Value.ToString.Trim
        'Session("FUN-AAPPT-ROID") = ROID
        'Session("FUN-AAPPT-ROID") = ROInfoDrpList.SelectedItem.Value.ToString.Trim

        apptEntity.CustomerID = CustIDBox.Text
        apptEntity.CustomerPrifx = CustPrxBox.Text
        'apptEntity.ROID = ROID
        apptEntity.ROID = ROInfoDrpList.SelectedItem.Value.ToString.Trim
        apptEntity.AppointmentDate = Me.apptDateBox.Text
        ds = apptEntity.GetContractInfo()
        If ds.Tables("FCU1_FIL").Rows.Count = 1 Then
            contractNoBox.Text = ds.Tables("FCU1_FIL").Rows(0).Item("FCU1_CONID").ToString()
            contEDateBox.Text = ds.Tables("FCU1_FIL").Rows(0).Item("FCU1_OBSDT").ToString()
        End If

        lastApptDateBox.Text = apptEntity.GetLastApptDate(Date.Now)
    End Sub
    Protected Sub ROInfoDrpList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ROInfoDrpList.SelectedIndexChanged
        Call DisplayRoInfoForAppointment()
    End Sub
    Protected Sub servCenterDrpList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles servCenterDrpList.SelectedIndexChanged
        If servCenterDrpList.SelectedItem.Value <> "" Then
            apptEntity.ServerCenterID = servCenterDrpList.SelectedItem.Value
            Dim dsTCHINFO As DataSet = apptEntity.GetTCHINFODrpListData()
            If dsTCHINFO Is Nothing Then
                Response.Redirect("~/PresentationLayer/error.aspx")
            End If
            databonds(dsTCHINFO.Tables("TCHINFO"), assignToDrpList)
            'assignToDrpList.Items.Insert(0, New ListItem("", ""))
            assignToDrpList.SelectedIndex = 0
        Else
            assignToDrpList.Items.Clear()
            assignToDrpList.Items.Add(New ListItem("", ""))
            assignToDrpList.SelectedIndex = 0
        End If

        Dim crtCrID As String = Session("userID")
        Dim dsCrUser As DataSet = apptEntity.GetCrIDBySvcID(servCenterDrpList.SelectedItem.Value)
        databonds(dsCrUser.Tables(0), cboCrID)

        'Me.assToZoneBox.Text = 
        GetZoneID()
        'Dim script As String = "self.location='#ApptLoc';"
        'Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ApptLoc", script, True)
    End Sub

    Protected Sub apptStartTimeDrpList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles apptStartTimeDrpList.SelectedIndexChanged

        CalculateAppointmentEndTime()
    End Sub


    'Protected Sub servHistorylkBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles servHistorylkBtn.Click
    '    Dim strTempURL As String = "ROID=" + Session("FUN-AAPPT-ROID").ToString() + "&ROINFO=" + ROInfoDrpList.SelectedItem.Text + _
    '                              "&custName=" + CustNameBox.Text + "&custID=" + CustIDBox.Text + "&custPf=" + CustPrxBox.Text
    '    strTempURL = "~/PresentationLayer/function/serverHistory.aspx?" + strTempURL
    '    Response.Redirect(strTempURL)
    'End Sub
    'Protected Sub contractHistroylkBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles contractHistroylkBtn.Click
    '    Dim strTempURL As String = "ROID=" + Session("FUN-AAPPT-ROID").ToString() + "&ROINFO=" + ROInfoDrpList.SelectedItem.Text + _
    '                               "&custName=" + CustNameBox.Text + "&custID=" + CustIDBox.Text + "&custPf=" + CustPrxBox.Text
    '    strTempURL = "~/PresentationLayer/function/contracthistory.aspx?" + strTempURL
    '    Response.Redirect(strTempURL)
    'End Sub
    'Protected Sub detaillkBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles detaillkBtn.Click
    '    Dim strTempURL As String = "custID=" + CustIDBox.Text + "&custPf=" + CustPrxBox.Text + _
    '                               "&custName=" + CustNameBox.Text + "&ROID=" + Session("FUN-AAPPT-ROID").ToString()
    '    strTempURL = "~/PresentationLayer/function/appointmenthistorydetail.aspx?" + strTempURL
    '    Response.Redirect(strTempURL)
    'End Sub
    'Protected Sub chkSchedulelkBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSchedulelkBtn.Click
    '    Dim objXm As New clsXml
    '    objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
    '    Dim msginfo As String
    '    If servCenterDrpList.SelectedValue = "" Then
    '        msginfo = objXm.GetLabelName("StatusMessage", "FUN-APPT-APPTSEVCID")
    '        InfoMsgBox.Alert(msginfo)
    '        Return
    '    End If
    '    Dim strTempURL As String = "servCenter=" + servCenterDrpList.SelectedItem.Value + "&apptDate=" + apptDateBox.Text + _
    '                                 "&zoneID=" + cboZoneID.SelectedValue
    '    'strTempURL = "~/PresentationLayer/function/checkSchedule.aspx?" + strTempURL
    '    'Response.Redirect(strTempURL)

    '    strTempURL = "checkSchedulepopup.aspx?" + strTempURL
    '    Dim script As String
    '    script = "showModalDialog('" & strTempURL & "','_calPick','status=no;center=yes;dialogWidth=600pt;dialogHeight=400pt;');"
    '    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)

    'End Sub

    Protected Sub saveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveButton.Click
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Dim msginfo As String
        Dim msgtitle As String

        RequestFromValue()

        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")

        If Not IsDate(apptDateBox.Text) Then
            InfoMsgBox.Alert(objXmlTr.GetLabelName("StatusMessage", "INVALID_APPOINTMENT_DATE"))
            Return
        End If

        Dim objUser As New clsUser
        Dim drUser As SqlClient.SqlDataReader

        Dim lstrUserID As String = ""
        objUser.UserID = Session("userid")
        objUser.StaffStatus = "ACTIVE"
        drUser = objUser.GetUserDetailsByID

        If Me.transTypeDrpList.SelectedValue = "SS" Then
            If (drUser.Read()) Then
                If drUser("MUSR_DEPT").ToString.ToUpper <> "CR" Then

                    msginfo = objXmlTr.GetLabelName("StatusMessage", "FUN-APPT-CR_SS_APPT_ALLOW") '"Only CR dept users are allowed SS appointment type"
                    InfoMsgBox.Alert(msginfo)
                    Return
                End If
            End If
        End If
        objUser = Nothing
        drUser = Nothing




        '处理时间格式
        If Convert.ToInt32(apptEndTimeHBox.Text) < 0 Or Convert.ToInt32(apptEndTimeHBox.Text) > 23 Then
            apptEndTimeHBox.Text = "00"
        ElseIf apptEndTimeHBox.Text.Length = 1 Then
            apptEndTimeHBox.Text = "0" + apptEndTimeHBox.Text
        End If

        If Convert.ToInt32(apptEndTimeMBox.Text) < 0 Or Convert.ToInt32(apptEndTimeMBox.Text) > 59 Then
            apptEndTimeMBox.Text = "00"
        ElseIf apptEndTimeMBox.Text.Length = 1 Then
            apptEndTimeMBox.Text = "0" + apptEndTimeMBox.Text
        End If


        If Convert.ToInt32(apptEndTimeHBox.Text + apptEndTimeMBox.Text) <= Convert.ToInt32(apptStartTimeDrpList.SelectedValue) Then
            msginfo = objXm.GetLabelName("StatusMessage", "FUN-APPT-APPTTIME")
            InfoMsgBox.Alert(msginfo)
            Return
        End If

        Dim lstrRoID As String
        lstrRoID = Me.ROInfoDrpList.SelectedValue
        If lstrRoID = "" Then
            lstrRoID = "0"
        End If



        'apptEntity.TransactionType = Trim(transTypeBox.Text)
        apptEntity.TransactionType = Trim(transTypeDrpList.SelectedValue)
        apptEntity.TransactionNo = Trim(transNoBox.Text)
        apptEntity.AppointmentDate = Trim(apptDateBox.Text)
        apptEntity.ZoneID = cboZoneID.SelectedValue
        apptEntity.AppointmentStartTime = apptStartTimeDrpList.SelectedValue
        'apptEntity.TechnicanID = Me.assignToDrpList.SelectedValue
        apptEntity.ServerCenterID = Me.servCenterDrpList.SelectedValue
        apptEntity.CustomerID = Me.CustIDBox.Text
        apptEntity.CustomerPrifx = Me.CustPrxBox.Text
        apptEntity.ROID = lstrRoID

        Dim ValidatorArray As Array = apptEntity.Validator("0") '不是修改，是增加
        If ValidatorArray Is Nothing Then
            Return
        End If
        'If ValidatorArray(0) = True Then 'appointment no exist
        '    msginfo = objXm.GetLabelName("StatusMessage", "FUN-APPT-APPTEXIST")
        '    InfoMsgBox.Alert(msginfo)
        '    Return
        'End If

        If DDApptStatus.SelectedItem.Value = "X" Then
            ' no checking on the zone and control
        Else
            If transTypeDrpList.SelectedValue = fstrjobType_SS Then
                If CDate(apptDateBox.Text) < CDate(System.DateTime.Today) And DDApptStatus.SelectedItem.Value = "SA" Then
                    msginfo = objXm.GetLabelName("StatusMessage", "SS_APPOINTMENTDATE_PAST")
                    InfoMsgBox.Alert(msginfo)
                    Return
                End If

                If cboZoneID.SelectedValue.Trim = "" And Not (apptEndTimeHBox.Text = "23" And apptEndTimeMBox.Text = "59") Then
                    msginfo = objXm.GetLabelName("StatusMessage", "FUN-APPT-OTHERTIMESLOT")
                    InfoMsgBox.Alert(msginfo)
                    Return
                End If

                If ValidatorArray(5) = True Then  'the maximum prefix no of appointment for the service center that day is full
                    msginfo = objXm.GetLabelName("StatusMessage", "FUN-APPT-MAXPFNOPDJOBS")
                    InfoMsgBox.Alert(msginfo)
                    Return
                End If

                If ValidatorArray(0) = True Then 'the maximum prefix no of appointment for the area is full.
                    msginfo = objXm.GetLabelName("StatusMessage", "FUN-APPT-ZONEFULL")
                    InfoMsgBox.Alert(msginfo)
                    Return
                End If

                If ValidatorArray(1) = True Then 'another customers already occupy a particular timeslot.
                    msginfo = objXm.GetLabelName("StatusMessage", "FUN-APPT-OCCUPYTIME")
                    InfoMsgBox.Alert(msginfo)
                    Return
                End If

                'If apptEntity.FixedAppointmentCount + apptEntity.OtherAppointmentCount > 7 Then

                'End If

                If apptStartTimeDrpList.SelectedValue <> "0000" And ValidatorArray(2) = True And cboZoneID.SelectedValue <> "" Then '5 appointments for a same technician with different timeslot.

                    If apptEntity.FixedAppointmentCount + apptEntity.OtherAppointmentCount > 7 Then

                    End If

                    msginfo = objXm.GetLabelName("StatusMessage", "FUN-APPT-TCHBUSY1")
                    InfoMsgBox.Alert(msginfo)
                    Return
                End If

                'If apptStartTimeDrpList.SelectedValue = "0000" And ValidatorArray(3) = True And cboZoneID.SelectedValue <> "" Then '3 appointments for a same technician with timeslot is 'other'.

                '    If apptEntity.FixedAppointmentCount + apptEntity.OtherAppointmentCount > 7 Then

                '    End If


                '    msginfo = objXm.GetLabelName("StatusMessage", "FUN-APPT-TCHBUSY2")
                '    InfoMsgBox.Alert(msginfo)
                '    Return
                'End If

                If ValidatorArray(4) = True Then 'the customer already have an appointment at that day!
                    msginfo = objXm.GetLabelName("StatusMessage", "FUN-APPT-CUSTOMINFO")
                    InfoMsgBox.Alert(msginfo)
                    Return
                End If
            Else
                'If CDate(apptDateBox.Text) > CDate(System.DateTime.Today) and DDApptStatus.SelectedItem.Value = "SA" Then

                '    msginfo = objXm.GetLabelName("StatusMessage", "AS_APPOINTMENTDATE_PAST")
                '    InfoMsgBox.Alert(msginfo)
                '    Return
                'End If
            End If

            'If ValidatorArray(6) = True Then '''''''''''''''''''''''''''''''''''''''''''''''''''''''commented by Tomas temporarily for UAT 20180607
            '    msginfo = objXm.GetLabelName("StatusMessage", "FUN-APPTCNT-UNCOMAPPT2")
            '    InfoMsgBox.Alert(msginfo)
            'End If

            Dim ROID As String = ROInfoDrpList.SelectedItem.Value.ToString.Trim 'Session("FUN-AAPPT-ROID")
            If ROID <> "" And ROID <> "0" Then
                apptEntity.ROID = ROID
                apptEntity.AppointmentType = apptTypeDrpList.SelectedValue.ToString()
                If apptEntity.SVCTValidator() = True Then
                    msginfo = objXm.GetLabelName("StatusMessage", "FUN-APPT-SVCTVALIDATOR")
                    InfoMsgBox.Alert(msginfo)
                    Return
                End If
            End If

            'Added checking for missing CR ID in FAPT_FIL - 09072008
            If Session("userID") = "" Then
                InfoMsgBox.Alert("Error : User Session id missing, Please try again.")
                Return
            End If

        End If

        ' Added by JC ReasonObjection 31 May 2016
        If DDApptStatus.SelectedItem.Value = "X" Then
            If ddlReasonObjection.SelectedValue = "" Then
                InfoMsgBox.Alert("Reason Objection cannot be blank!")
                Return
            End If
        End If

        ' Added by Tomas 20180613



        If Me.transTypeDrpList.SelectedValue = "RS" Then
            If RMStxt.Text = "" Then
                InfoMsgBox.Alert("RMS No cannot be blank!")
                Return
            Else
                Dim RMSNoExist As String = 0

                Dim Conn As New SqlConnection
                Conn.ConnectionString = ConfigurationManager.AppSettings("ConnectionString").ToString

                Using Comm As New SqlClient.SqlCommand("Select [Count] = count(*) from dbo.RMS_FIL left join FAPT_FIL on APPT_TRNTY = FAPT_TRNTY and APPT_TRNNO = FAPT_TRNNO where RMS_NO = '" & RMStxt.Text & "' and FAPT_STAT not in ('TG','UA','X')", Conn)
                    Conn.Open()
                    Using readerObj As SqlClient.SqlDataReader = Comm.ExecuteReader
                        While readerObj.Read
                            RMSNoExist = readerObj("Count").ToString
                        End While
                    End Using
                    Conn.Close()
                End Using

                If RMSNoExist <> "0" Then
                    InfoMsgBox.Alert("RMS No duplicate!")
                    Return
                End If
            End If
        Else
            Dim ApptType() As String = {"M1","M2","M3","M4","M5","M6","S1","S2","S3","W1","W2.","W3.","RM1","RM2","RM3","RM4","RM5","RM6","RM7"}

            For Each value As String In ApptType
                If apptTypeDrpList.SelectedValue.Trim = value Then

                    Dim Flag As Integer

                    Dim Conn As New SqlConnection
                    Conn.ConnectionString = ConfigurationManager.AppSettings("ConnectionString").ToString

                    Using Comm As New SqlClient.SqlCommand("select (case when " _
                        & "(select MSVC_OTJOB from MSVC_FIL where msvc_svcid = '" & Me.servCenterDrpList.SelectedValue & "') > " _
                        & "(select count(*) from FAPT_FIL " _
                        & "where FAPT_TRNTY in ('AS','SS') " _
                        & "and FAPT_APTDT >= CONVERT(date,'" & apptDateBox.Text & "',103) " _
                        & "and FAPT_APTDT < dateadd(day,1,CONVERT(date,'" & apptDateBox.Text & "',103)) " _
                        & "and FAPT_STAT not in ('UA','X') " _
                        & "and TRIM(FAPT_APTTY) in ('M1','M2','M3','M4','M5','M6','S1','S2','S3','W1','W2.','W3.','RM1','RM2','RM3','RM4','RM5','RM6','RM7') " _
                        & "and FAPT_SVCID = '" & Me.servCenterDrpList.SelectedValue & "') then 1 else 0 end) as Flag " _
                        & "", Conn)
                        Conn.Open()
                        Using readerObj As SqlClient.SqlDataReader = Comm.ExecuteReader
                            While readerObj.Read
                                Flag = readerObj("Flag")
                            End While
                        End Using
                        Conn.Close()
                    End Using

                    If Flag = "0" Then
                        InfoMsgBox.Alert("Maximum Number of Contract Appointments detected for current appointment date!")
                        Return
                    End If
                End If
            Next
        End If
        


        msginfo = objXm.GetLabelName("StatusMessage", "SAVEORNOT")
        msgtitle = objXm.GetLabelName("StatusMessage", "SAVETITLE")
        'saveMsgBox.Confirm(msginfo)
        SaveAppointment()
        CompDateVal.ErrorMessage = ""
        GetZoneID()

    End Sub

    Sub SaveAppointment()
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        Dim UserID As String = Session("userID")
        apptEntity.TransactionType = Trim(transTypeDrpList.SelectedValue)
        apptEntity.ServerCenterID = Trim(servCenterDrpList.SelectedValue.ToString())

        If apptDateBox.Text <> "" Then
            apptEntity.AppointmentDate = Trim(apptDateBox.Text)
        End If
        If CustIDBox.Text <> "" Then
            apptEntity.CustomerID = Trim(CustIDBox.Text)
        End If
        If CustPrxBox.Text <> "" Then
            apptEntity.CustomerPrifx = Trim(CustPrxBox.Text)
        End If
        apptEntity.ROID = ROInfoDrpList.SelectedItem.Value.ToString.Trim 'Trim(Session("FUN-AAPPT-ROID"))
        apptEntity.PICName = Trim(PICNameBox.Text)
        apptEntity.PICContact = Trim(PICContactBox.Text)
        apptEntity.AppointmentType = Trim(apptTypeDrpList.SelectedValue.ToString())
        apptEntity.AppointmentStartTime = Trim(apptStartTimeDrpList.SelectedValue.ToString())

        apptEntity.AppointmentEndTime = Trim(apptEndTimeHBox.Text) + Trim(apptEndTimeMBox.Text)
        apptEntity.TechnicanID = Trim(assignToDrpList.SelectedValue.ToString())
        apptEntity.AppointmentNotes = Trim(notesBox.Text)
        apptEntity.AppointmentStatus = Trim(DDApptStatus.SelectedItem.Value) 'Trim("SA")
        apptEntity.ZoneID = cboZoneID.SelectedValue

        ' Modified by Ryan Estandarte 25 Sept 2012
        ' Added by Ryan Estandarte 20 Sept 2012
        'apptEntity.IsInbound = chkIsInbound.Checked
        apptEntity.IsInbound = CType(rdoInboundOutbound.SelectedValue, Boolean)
        If Trim(DDApptStatus.SelectedItem.Value) = "X" Then
            apptEntity.ReasonObjection = ddlReasonObjection.SelectedValue
        Else
            apptEntity.ReasonObjection = ""
        End If

        If Trim(transTypeDrpList.SelectedValue) = fstrjobType_SS Or Trim(transTypeDrpList.SelectedValue) = fstrjobType_RS Then
            apptEntity.CrID = Session("userID")
        Else
            apptEntity.CrID = cboCrID.SelectedValue
        End If

        ' Added by lyann NatureOfFeedback 27 Dec 2017
        If apptTypeDrpList.SelectedValue.Trim = "MR" Or apptTypeDrpList.SelectedValue.Trim = "MB" Or apptTypeDrpList.SelectedValue.Trim = "PB" Then
            apptEntity.NatureOfFeedback = ddlNatureFeedback.SelectedValue
        Else
            apptEntity.NatureOfFeedback = ""
        End If

        apptEntity.RMSNo = RMStxt.Text 'Added by Tomas 20180613

        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Me.errlab.Visible = True
        Dim transNo As String = apptEntity.AddAppointment(UserID)
        If transNo <> "" Then
            If Trim(DDApptStatus.SelectedItem.Value) = "SA" Then

                If sendSMS(transNo) = True Then
                    Me.errlab.Text = objXmlTr.GetLabelName("StatusMessage", "FUN-APPTCNT-SUCCESS")
                    Me.errlab.Text = Me.errlab.Text + ". Service order number sent!"
                Else
                    Me.errlab.Text = objXmlTr.GetLabelName("StatusMessage", "FUN-APPTCNT-SUCCESS")
                End If
                'ElseIf Trim(DDApptStatus.SelectedItem.Value) = "UA" Or Trim(DDApptStatus.SelectedItem.Value) = "X" Then

                'If sendSMS(transNo) = True Then
                ' Me.errlab.Text = objXmlTr.GetLabelName("StatusMessage", "FUN-APPTCNT-SUCCESS")

                'Else
                'Me.errlab.Text = objXmlTr.GetLabelName("StatusMessage", "FUN-APPTCNT-SUCCESS")
                'End If
            Else
                Me.errlab.Text = objXmlTr.GetLabelName("StatusMessage", "FUN-APPTCNT-SUCCESS")
                'Me.errlab.Text = Me.errlab.Text + ". Appointment saved without send Service Order Number"
            End If

            saveMsgBox.Alert(errlab.Text)
            transNoBox.Text = transNo
            saveButton.Enabled = False
        Else
            Me.errlab.Text = objXmlTr.GetLabelName("StatusMessage", "FUN-APPTCNT-FAILED")
            saveMsgBox.Alert(errlab.Text)
        End If
    End Sub

    Private Function sendSMS(ByVal transNo As String) As Boolean
        Dim flag As Boolean = False

        Dim hp As String = String.Empty

        If rblSendSMS.SelectedValue = "Y" Then
            hp = txtMobile.Text
            'hp = "0124985125"
			'hp ="0122952910"

            If Trim(hp) <> "" Then

                If hp <> "" Then
                    If Left(hp, 1) <> "+" Then
                        If Left(hp, 1) = "0" Then
                            hp = "+6" + hp
                        Else
                            hp = "+" + hp
                        End If
                    End If
                End If

                Dim objAppointment As New clsAppointment()
                Dim strBranchContactNo As String = String.Empty
                Dim ds As DataSet = objAppointment.GetBranchContactNo(servCenterDrpList.SelectedValue)
                If ds.Tables(0).Rows.Count > 0 Then
                    strBranchContactNo = ds.Tables(0).Rows(0)("MSVC_ContactNo").ToString
                End If

                Dim strSMSText As String = String.Empty

                strSMSText = "Elken Service appointment: " + apptDateBox.Text
                strSMSText = strSMSText + ". Confirmation number: " + transNo
                strSMSText = strSMSText + ". Any doubt pls call: " + strBranchContactNo
                strSMSText = strSMSText + ". From Elken Service(Do not reply)"
                apptEntity.SendSMSAppointment(Trim(hp), strSMSText, "N", "74")
                apptEntity.SendSMSAppointmentLog(Trim(transTypeDrpList.SelectedValue), transNo, rblMobile.SelectedValue, txtMobile.Text, txtMobileName.Text)

                flag = True
            End If
        End If

        Return flag

    End Function

    Protected Sub saveMsgBox_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles saveMsgBox.GetMessageBoxResponse

        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then

            SaveAppointment()
        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
        End If
    End Sub

    Protected Sub transTypeDrpList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles transTypeDrpList.SelectedIndexChanged

        Call JobTypeProperties()
    End Sub

    Private Sub JobTypeProperties()
        Dim lstrJobType As String = transTypeDrpList.SelectedValue



        If lstrJobType = fstrjobType_SS Or lstrJobType = fstrjobType_RS Then
            cboZoneID.Enabled = True
            cboZoneID.SelectedIndex = 0

            Me.assignToDrpList.Enabled = False
            cboCrID.Enabled = False
            'cboCrID.Enabled = True
            apptStartTimeDrpList.Enabled = True

            rfvCrID.Enabled = False
            lblAsc.Visible = False
        Else
            cboZoneID.Enabled = False
            cboZoneID.SelectedIndex = cboZoneID.Items.Count - 1
            Me.assignToDrpList.Enabled = True
            cboCrID.Enabled = True

            apptStartTimeDrpList.Enabled = False
            apptStartTimeDrpList.SelectedIndex = apptStartTimeDrpList.Items.Count - 1

            rfvCrID.Enabled = True
            lblAsc.Visible = True

        End If

        If lstrJobType = fstrjobType_SS Or lstrJobType = fstrjobType_AS Then
            Me.RMStxt.Enabled = False
        Else
            Me.RMStxt.Enabled = True
        End If


        Dim objUser As New clsUser
        Dim drUser As SqlClient.SqlDataReader

        objUser.UserID = Session("userid")
        objUser.StaffStatus = "ACTIVE"
        drUser = objUser.GetUserDetailsByID
        If (drUser.Read()) Then
            If drUser("MUSR_DEPT").ToString.ToUpper = "CR" Then
                cboCrID.SelectedValue = Session("userid")
            Else
                cboCrID.SelectedIndex = 0
            End If
        End If

        objUser = Nothing
        drUser = Nothing

        'cboCrID.SelectedIndex = 0
        Me.assignToDrpList.SelectedIndex = 0
        Call CalculateAppointmentEndTime()
        Call GetZoneID()
    End Sub


    Private Sub CalculateAppointmentEndTime()

        If apptStartTimeDrpList.SelectedValue <> "0000" Then
            apptEndTimeHBox.Text = (Convert.ToInt32(apptStartTimeDrpList.SelectedValue.Substring(0, 2)) + 1).ToString()
            apptEndTimeMBox.Text = (Convert.ToInt32(apptStartTimeDrpList.SelectedValue.Substring(2, 2)) + 30).ToString()
            Dim apptEndTimeM As Integer = Convert.ToInt32(apptEndTimeMBox.Text)
            If apptEndTimeM >= 60 Then
                apptEndTimeHBox.Text = (Convert.ToInt32(apptEndTimeHBox.Text) + 1).ToString()
                apptEndTimeM = apptEndTimeM - 60
                apptEndTimeMBox.Text = apptEndTimeM.ToString()
                If apptEndTimeM > 0 Or apptEndTimeM < 9 Then
                    apptEndTimeMBox.Text = "0" + apptEndTimeMBox.Text
                End If
            End If
            apptEndTimeHBox.ReadOnly = False
            apptEndTimeMBox.ReadOnly = False
        Else
            apptEndTimeHBox.Text = "23"
            apptEndTimeMBox.Text = "59"
            apptEndTimeHBox.ReadOnly = True
            apptEndTimeMBox.ReadOnly = True
        End If
    End Sub

    Sub RequestFromValue()
        apptEndTimeHBox.Text = Request.Form("apptEndTimeHBox")
        apptEndTimeMBox.Text = Request.Form("apptEndTimeMBox")
        apptDateBox.Text = Request.Form("apptDateBox")
        Dim ZoneID As String = Request.Form("cboZoneID")
        'Dim txtZoneID As String = Request.Form("txtZoneID")
        Dim CrID As String = ""
        Dim ThID As String = ""



        CrID = Request.Form("cboCrID")
        ThID = Request.Form("assignToDrpList")

        lastservDateBox.Text = Request.Form("lastservDateBox")
        WarrantyBox.Text = Request.Form("WarrantyBox")
        installByBox.Text = Request.Form("installByBox")
        lastRemDateBox.Text = Request.Form("lastRemDateBox")

        contractNoBox.Text = Request.Form("contractNoBox")
        contEDateBox.Text = Request.Form("contEDateBox")

        lastApptDateBox.Text = Request.Form("lastApptDateBox")

        'Dim RoID As String
        'RoID = Request.Form("ROInfoDrpList")

        Dim ServiceCenterID As String
        ServiceCenterID = servCenterDrpList.SelectedValue


        GetZoneID()
        cboZoneID.SelectedValue = ZoneID

        'Session("ZoneSelectedIndex") = cboZoneID.SelectedIndex

        If CrID Is Nothing Then CrID = ""
        If ThID Is Nothing Then ThID = ""

        Dim dsCrUser As DataSet = apptEntity.GetCrIDBySvcID(ServiceCenterID)
        databonds(dsCrUser.Tables(0), cboCrID)
        cboCrID.SelectedValue = CrID

        apptEntity.ServerCenterID = ServiceCenterID
        Dim dsThUser As DataSet = apptEntity.GetTCHINFODrpListData()
        databonds(dsThUser.Tables(0), assignToDrpList)
        assignToDrpList.SelectedValue = ThID

    End Sub

    Function GetAppointmentEndTime(ByVal AppointmentStartTime) As String
        Dim EndTimeH, EndTimeM As String

        If AppointmentStartTime <> "0000" Then
            EndTimeH = (Convert.ToInt32(AppointmentStartTime.Substring(0, 2)) + 1).ToString()
            EndTimeM = (Convert.ToInt32(AppointmentStartTime.Substring(2, 2)) + 30).ToString()
            Dim apptEndTimeM As Integer = Convert.ToInt32(EndTimeM)
            If apptEndTimeM >= 60 Then
                EndTimeH = (Convert.ToInt32(EndTimeH) + 1).ToString()
                apptEndTimeM = apptEndTimeM - 60
                EndTimeM = apptEndTimeM.ToString()
                If apptEndTimeM > 0 Or apptEndTimeM < 9 Then
                    EndTimeM = "0" + EndTimeM
                End If
            End If

        Else
            EndTimeH = "23"
            EndTimeM = "59"
        End If

        GetAppointmentEndTime = EndTimeH & ":" & EndTimeM
    End Function

#Region "Ajax"

    <AjaxPro.AjaxMethod()> _
    Function GetCrIDList(ByVal lstrServiceCenterID As String) As ArrayList
        Dim objAppointment As New clsAppointment()

        Dim dsCrUser As DataSet = objAppointment.GetCrIDBySvcID(lstrServiceCenterID)

        Dim strTemp As String
        Dim Listas As New ArrayList(dsCrUser.Tables(0).Rows.Count)
        Dim i As Integer = 0
        Listas.Add("")
        Try
            For i = 0 To dsCrUser.Tables(0).Rows.Count - 1
                strTemp = dsCrUser.Tables(0).Rows(i).Item(0).ToString & ":" & dsCrUser.Tables(0).Rows(i).Item(0).ToString & "-" & dsCrUser.Tables(0).Rows(i).Item(1).ToString
                Listas.Add(strTemp)
            Next
        Catch
        End Try

        Return Listas
    End Function

    <AjaxPro.AjaxMethod()> _
    Function GetTechnicianIDList(ByVal lstrServiceCenterID As String) As ArrayList
        Dim objAppointment As New clsAppointment()
        objAppointment.ServerCenterID = lstrServiceCenterID
        Dim dsTechnician As DataSet = objAppointment.GetTCHINFODrpListData()

        Dim strTemp As String
        Dim Listas As New ArrayList(dsTechnician.Tables(0).Rows.Count)
        Dim i As Integer = 0
        Listas.Add("")
        Try
            For i = 0 To dsTechnician.Tables(0).Rows.Count - 1
                strTemp = dsTechnician.Tables(0).Rows(i).Item(0).ToString & ":" & dsTechnician.Tables(0).Rows(i).Item(1).ToString & "-" & dsTechnician.Tables(0).Rows(i).Item(0).ToString
                Listas.Add(strTemp)
            Next
        Catch
        End Try

        Return Listas
    End Function

    <AjaxPro.AjaxMethod()> _
    Function GetZoneList(ByVal strTransType As String, ByVal strAreaID As String, ByVal strAppoinmentDate As String, ByVal strServiceCenterID As String) As ArrayList
        Dim objAppointment As New clsAppointment()
        Dim strMaxApt, strMaxZoneApt, lstrTotalApt As String
        Dim strZoneID As String

        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        If strAreaID = "" Or strAppoinmentDate = "" Or strServiceCenterID = "" Or Not IsDate(strAppoinmentDate) Then
        Else

            objAppointment.CustomAreaID = strAreaID
            objAppointment.AppointmentDate = strAppoinmentDate
            objAppointment.ServerCenterID = strServiceCenterID
            objAppointment.TransactionType = ""
            objAppointment.TransactionNo = ""
            Dim ds As DataSet = objAppointment.GetZoneID()

            Dim strTemp As String
            Dim Listas As New ArrayList(ds.Tables(0).Rows.Count)
            Dim i As Integer = 0

            For i = 0 To ds.Tables(0).Rows.Count - 1
                strZoneID = ds.Tables(0).Rows(i).Item(0).ToString


                lstrTotalApt = ds.Tables(0).Rows(i).Item(1).ToString
                strMaxZoneApt = ds.Tables(0).Rows(i).Item(2).ToString

                If strTransType = fstrjobType_SS Then
                    strMaxApt = strMaxZoneApt
                Else
                    strMaxApt = lstrTotalApt
                End If

                If (strTransType = "SS") Or (strZoneID = "" And strTransType = "AS") Then
                    strTemp = strZoneID & ":" & IIf(strZoneID = "", "Other ", strZoneID) & " (" & lstrTotalApt & "/" & strMaxApt & ")"
                    Listas.Add(strTemp)
                End If

            Next
            Return Listas

        End If

    End Function


    <AjaxPro.AjaxMethod()> _
    Function GetAppointmentEndTimeChange(ByVal AppointmentStartTimea As String) As String
        GetAppointmentEndTimeChange = GetAppointmentEndTime(AppointmentStartTimea)
    End Function

    <AjaxPro.AjaxMethod()> _
    Function GetRoInformationChange(ByVal RoID As String, ByVal CustomerPrefix As String, ByVal CustomerID As String, ByVal AppointmentDate As String) As ArrayList
        Dim ds As DataSet
        Dim lastservDate As String = ""
        Dim Warranty As String = ""
        Dim installBy As String = ""
        Dim lastRemDate As String = ""
        Dim contractNo As String = ""
        Dim contEDate As String = ""
        Dim lastApptDate As String = ""
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        If RoID <> "" And IsDate(AppointmentDate) Then
            apptEntity.ModeID = ""
            apptEntity.SerialNo = ""
            apptEntity.ROID = RoID

            ds = apptEntity.GetROAndContractInfo()
            If Not ds Is Nothing Then
                'lastservDateBox.Text = ds.Tables("MROU_FIL").Rows(0).Item("MROU_LSVDT").ToString()
                'WarrantyBox.Text = ds.Tables("MROU_FIL").Rows(0).Item("MROU_WRRDT").ToString()
                'installByBox.Text = ds.Tables("MROU_FIL").Rows(0).Item("MROU_TCHID").ToString()
                'lastRemDateBox.Text = ds.Tables("MROU_FIL").Rows(0).Item("MROU_LRMDT").ToString()
                lastservDate = ds.Tables("MROU_FIL").Rows(0).Item("MROU_LSVDT").ToString()
                Warranty = ds.Tables("MROU_FIL").Rows(0).Item("MROU_WRRDT").ToString()
                installBy = ds.Tables("MROU_FIL").Rows(0).Item("MROU_TCHID").ToString()
                lastRemDate = ds.Tables("MROU_FIL").Rows(0).Item("MROU_LRMDT").ToString()

            End If


            apptEntity.CustomerID = CustomerID
            apptEntity.CustomerPrifx = CustomerPrefix
            apptEntity.ROID = RoID
            apptEntity.AppointmentDate = AppointmentDate
            ds = apptEntity.GetContractInfo()
            If ds.Tables("FCU1_FIL").Rows.Count = 1 Then
                'contractNoBox.Text = ds.Tables("FCU1_FIL").Rows(0).Item("FCU1_CONID").ToString()
                'contEDateBox.Text = ds.Tables("FCU1_FIL").Rows(0).Item("FCU1_OBSDT").ToString()
                contractNo = ds.Tables("FCU1_FIL").Rows(0).Item("FCU1_CONID").ToString()
                contEDate = ds.Tables("FCU1_FIL").Rows(0).Item("FCU1_OBSDT").ToString()
            End If
            'lastApptDateBox.Text = apptEntity.GetLastApptDate(Session("FUN-MAPPT-CREATIME"))
            lastApptDate = apptEntity.GetLastApptDate(AppointmentDate)


        End If
        Dim Listas As New ArrayList(7)


        'lastservDate & ":" & Warranty & ":" & installBy & ":" & lastRemDate & ":" & contractNo & ":" & contEDate & ":" & lastApptDate

        Listas.Add(lastservDate)
        Listas.Add(Warranty)
        Listas.Add(installBy)
        Listas.Add(lastRemDate)
        Listas.Add(contractNo)
        Listas.Add(contEDate)
        Listas.Add(lastApptDate)



        GetRoInformationChange = Listas
    End Function

#End Region

    'Protected Sub cboZoneID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboZoneID.SelectedIndexChanged
    '    RequestFromValue()
    'End Sub

    Protected Sub apptDateBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles apptDateBox.TextChanged
        Dim ZoneID As String = Request.Form("cboZoneID")
        GetZoneID()
        cboZoneID.SelectedValue = ZoneID
    End Sub

    Protected Sub rblSendSMS_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblSendSMS.SelectedIndexChanged
        If rblSendSMS.SelectedValue = "Y" Then
            tblMobile.Visible = True
            rblMobile.SelectedValue = "M1"
            rblMobile_TextChanged(Nothing, Nothing)
        Else
            tblMobile.Visible = False
        End If

    End Sub

    Protected Sub rblMobile_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblMobile.TextChanged
        Dim CustomerEntity As New clsCustomerRecord()
        Dim telepass As New clsCommonClass()
        Dim intCtr As Integer
        CustomerEntity.CustomerID = CustIDBox.Text.ToString()
        CustomerEntity.Customerpf = contryBox.Text.ToString()
        Dim edtReaderTele As DataSet = CustomerEntity.GetCustomerTele()
        txtMobile.Text = ""
        txtMobileName.Text = ""
        txtMobile.Enabled = False
        txtMobileName.Enabled = False

        If rblMobile.SelectedValue = "M1" Then
            For intCtr = 0 To edtReaderTele.Tables(0).Rows.Count - 1
                Select Case edtReaderTele.Tables(0).Rows(intCtr).Item(0).ToString
                    Case "MOB1" 'o tele
                        txtMobile.Text = telepass.passconverttel(edtReaderTele.Tables(0).Rows(intCtr).Item(2).ToString)
                        txtMobileName.Text = edtReaderTele.Tables(0).Rows(intCtr).Item(1).ToString()
                End Select
            Next
        Else
            For intCtr = 0 To edtReaderTele.Tables(0).Rows.Count - 1
                Select Case edtReaderTele.Tables(0).Rows(intCtr).Item(0).ToString
                    Case "MOB2" 'mob 2
                        txtMobile.Text = telepass.passconverttel(edtReaderTele.Tables(0).Rows(intCtr).Item(2).ToString)
                        txtMobileName.Text = edtReaderTele.Tables(0).Rows(intCtr).Item(1).ToString()
                End Select
            Next
        End If
			'Added by Lyann 30/08/2017 to remain the zoneID list after page load. 
			GetZoneID()		
    End Sub

    Protected Sub DDApptStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDApptStatus.SelectedIndexChanged
        If Trim(DDApptStatus.SelectedItem.Value) = "SA" Then
            rblSendSMS.SelectedValue = "Y"
            rblSendSMS_SelectedIndexChanged(Nothing, Nothing)
        Else
            rblSendSMS.SelectedValue = "N"
            rblSendSMS_SelectedIndexChanged(Nothing, Nothing)
        End If

        If Trim(DDApptStatus.SelectedItem.Value) = "X" Then
            Dim dt As DataTable
            dt = apptEntity.GetObjectionReason().Tables(0)
            trReasonObjection.Visible = True
            ddlReasonObjection.DataSource = dt
            ddlReasonObjection.DataValueField = "ReasonObj_ID"
            ddlReasonObjection.DataTextField = "ReasonObj_Desc"
            ddlReasonObjection.DataBind()
            Dim list As New ListItem("Please select one", "")
            ddlReasonObjection.Items.Insert(0, list)
        Else
            trReasonObjection.Visible = False
        End If
    End Sub

    'SMR1711-2286 Add Nature of feedback
    Protected Sub apptTypeDrpList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles apptTypeDrpList.SelectedIndexChanged
        If apptTypeDrpList.SelectedValue.Trim = "MR" Or apptTypeDrpList.SelectedValue.Trim = "MB" Or apptTypeDrpList.SelectedValue.Trim = "PB" Then
            trNatureFeedback.Visible = True

            Dim dtNatureFeedback As DataTable
            dtNatureFeedback = apptEntity.GetNatureOfFeedback(apptTypeDrpList.SelectedValue.Trim).Tables(0)
            ddlNatureFeedback.DataSource = dtNatureFeedback
            ddlNatureFeedback.DataValueField = "DFeedback_ID"
            ddlNatureFeedback.DataTextField = "Description"
            ddlNatureFeedback.DataBind()
            Dim list As New ListItem("Please select one", "")
            ddlNatureFeedback.Items.Insert(0, list)
        Else
            trNatureFeedback.Visible = False
        End If
    End Sub
End Class
