<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CollectPaymentModify.aspx.vb" Inherits="PresentationLayer_function_CollectPaymentModify" %>

<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Update Payment Collection</title>
    
    <meta http-equiv="Content-Type" content="text/html; charset=gb2312"><meta/>
    <link href="../css/style.css" type="text/css" rel="stylesheet"><link/>

    <script language="JavaScript" src="../js/common.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">
         function isNumberKey(evt) {
              var charCode = (evt.which) ? evt.which : evt.keyCode;
              if (charCode != 46 && charCode > 31 
                && (charCode < 48 || charCode > 57))
                 return false;

              return true;
           }
         function deleteConfirm(pubid) {
            var result = confirm('Do you want to delete ' + pubid + ' ?');
            if (result) {
                return true;
            }
            else {
                return false;
            }
        }
        
        
    </script>


</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table bgcolor="#b7e6e6" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
            <tr>
                <td background="../graph/title_bg.gif" width="1%">
                    <img height="24" src="../graph/title1.gif" width="5" alt="Text alternative when image is not available"/>
                </td>
                <td background="../graph/title_bg.gif" class="style2" width="98%">
                    <asp:Label ID="titleLab" runat="server" Text="Payment Collection"></asp:Label>
                </td>
            </tr>
        </table>
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td background="../graph/title_bg.gif" class="style2" style="height: 14px" width="100%">
                    <font color="red">
                        <asp:Label ID="lblError" runat="server" Visible="False"></asp:Label></font>
                </td>
            </tr>
        </table>
        <table bgcolor="#b7e6e6" width="100%">
            <tr bgcolor="#ffffff">
                <td colspan="4">
                    <asp:Label ID="lblNoteUp" runat="server" ForeColor="Red" Text="Label"></asp:Label>
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td colspan="4">
                    &nbsp;
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td style="width: 15%; height: 24px;">
                    <asp:Label ID="lblServiceBillType" runat="server" Text="Service Bill Type" Width="104px"></asp:Label>
                </td>
                <td style="width: 30%; height: 24px;">
                    <asp:DropDownList ID="cboServiceBillType" runat="server" Width="90%" AutoPostBack="True" ReadOnly="True"
                        Enabled="False">
                    </asp:DropDownList>
                </td>
                <td style="width: 15%; height: 24px;">
                    <asp:Label ID="lblInvoiceNo" runat="server" Text="Invoice #" Width="104px"></asp:Label>
                </td>
                <td style="width: 30%; height: 24px;">
                    <asp:TextBox ID="txtInvoicePf" runat="server" ReadOnly="True" Width="32px" Enabled="False" ></asp:TextBox>&nbsp;
                    <asp:TextBox ID="txtInvoiceNo" runat="server" ReadOnly="True" Width="100px" Enabled="False"></asp:TextBox>
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td style="width: 15%; height: 7px;">
                    <asp:Label ID="lblDate" runat="server" Text="Date" Width="104px"></asp:Label>
                </td>
                <td style="width: 30%; height: 7px;">
                    <asp:TextBox ID="txtDate" runat="server" Style="width: 135px;" ReadOnly="True" Enabled="false"></asp:TextBox>&nbsp;
                </td>
                <td style="width: 39px; height: 7px;" valign="top">
                    <asp:Label ID="lblServiceBillNo" runat="server" Text="Service Bill #" Width="104px"></asp:Label>
                </td>
                <td style="width: 363px; height: 7px;" valign="top">
                    <asp:TextBox ID="txtServiceBillNo" runat="server" MaxLength="10" Enabled="False"></asp:TextBox>
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td style="width: 15%;">
                    <asp:Label ID="lblTechnician" runat="server" Text="Technician" Width="104px"></asp:Label>
                </td>
                <td style="width: 30%;">
                    <asp:DropDownList ID="cboTechnician" runat="server" Width="90%" Enabled="False">
                    </asp:DropDownList>&nbsp;
                </td>
                <td style="width: 39px;">
                    <asp:Label ID="lblServiceType" runat="server" Text="Service Type" Width="104px"></asp:Label>
                </td>
                <td style="width: 363px;">
                    <asp:DropDownList ID="cboServiceType" runat="server" Width="90%" Enabled="False">
                    </asp:DropDownList>
                    &nbsp;
                </td>
            </tr>
 
            <tr bgcolor="#ffffff">
                <td style="width: 15%;">
                    <asp:Label ID="lblAppoinmentNo" runat="server" Text="Appointment No" Width="124px"></asp:Label>
                </td>
                <td style="width: 30%;">
                    <asp:TextBox ID="txtAppointmentPrefix" runat="server" Width="26px" ReadOnly="True"
                        Enabled="False"></asp:TextBox>&nbsp;
                    <asp:TextBox ID="txtAppointmentNo" runat="server" Width="94px" ReadOnly="True" Enabled="False"></asp:TextBox>
                    <asp:Button ID="btnSearchAppNo" runat="server" Text="..." CausesValidation="False"
                        Width="1px" Enabled="False" Visible="False" />
                   
                </td>
                <td style="width: 39px;">
                    <asp:Label ID="lblCustomerID" runat="server" Text="CustomerID" Width="124px"></asp:Label>
                </td>
                <td style="width: 363px;">
                    <asp:TextBox ID="txtCustomerPrefix" runat="server" Width="26px" ReadOnly="True" Enabled="false"></asp:TextBox>&nbsp;
                    <asp:TextBox ID="txtCustomerID" runat="server" Width="100px" ReadOnly="True" Enabled = "false"></asp:TextBox>&nbsp;&nbsp;
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td style="width: 15%;">
                    <asp:Label ID="lblROSerialNo" runat="server" Text="RO Serial No" Width="124px"></asp:Label>
                </td>
                <td style="width: 30%;">
                    <asp:DropDownList ID="cboRO" runat="server" Width="90%" Visible="True" AutoPostBack="false">
                    </asp:DropDownList>&nbsp;
                </td>
                <td style="width: 39px;">
                    <asp:Label ID="lblCustomer" runat="server" Text="Customer Name" Width="124px"></asp:Label>
                </td>
                <td style="width: 363px;">
                    <asp:TextBox ID="txtCustomer" runat="server" ReadOnly="True" Width="200px"></asp:TextBox>
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td style="width: 15%">
                    <asp:Label ID="lblCountry" runat="server" Text="Country" Width="124px"></asp:Label>
                </td>
                <td style="width: 30%">
                    <asp:DropDownList ID="cboCountry" runat="server" AutoPostBack="True" Enabled="False"
                        Width="90%">
                    </asp:DropDownList>
                </td>
                <td style="width: 39px">
                    <asp:Label ID="lblCompany" runat="server" Text="Company" Width="124px"></asp:Label>
                </td>
                <td style="width: 363px">
                    <asp:DropDownList ID="cboCompany" runat="server" AutoPostBack="True" Enabled="False"
                        Width="90%">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td style="width: 15%">
                    <asp:Label ID="lblServiceCenter" runat="server" Text="Service Center" Width="124px"></asp:Label>
                </td>
                <td style="width: 30%">
                    <asp:DropDownList ID="cboServiceCenter" runat="server" AutoPostBack="True" Enabled="False"
                        Width="90%">
                    </asp:DropDownList>
                </td>
                <td style="width: 39px">
                    <asp:Label ID="lblStatus" runat="server" Text="Status" Width="124px"></asp:Label>
                </td>
                <td style="width: 363px">
                    <asp:DropDownList ID="cboStatus" runat="server" AutoPostBack="True" Width="90%">
                    </asp:DropDownList>
                </td>
            </tr>
           
            <tr bgcolor="#ffffff">
                <%--Modified by Ryan Estandarte 7 March 2012--%>
                <%--<td colspan=4>&nbsp;
                </td>--%>
                <td colspan="4">
                    &nbsp;&nbsp;
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td colspan="4" style="height: 25px">
                    <asp:Label ID="lblDebt" runat="server" Font-Underline="True"
                        Width="272px">Debt Details</asp:Label>&nbsp;
                </td>
            </tr>
              <tr bgcolor="#ffffff">
                <td style="height: 28px; width: 15%;">
                    <asp:Label ID="Label1" runat="server" Text="Current Debt" Width="124px"></asp:Label>
                </td>
                <td style="width: 30%; height: 28px">
                    <asp:TextBox ID="tbCurrentDebt" runat="server" ReadOnly="True" Width="200px" Font-Bold="True"></asp:TextBox>
                </td>
                <td style="height: 28px; width: 39px;">
                    <asp:Label ID="Label8" runat="server" Text="Total Credit" Width="124px"></asp:Label>
                </td>
                <td style="height: 28px; width: 363px;">
                    <asp:TextBox ID="tbTotalCredit" runat="server" ReadOnly="True" Width="200px" Font-Bold="True"></asp:TextBox>
                </td>
            </tr>
             <tr bgcolor="#ffffff">
                <td colspan="4" style="height: 25px">
                    <asp:Label ID="Label2" runat="server" Font-Underline="True"
                        Width="272px">Payment Details</asp:Label>&nbsp;
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td colspan="4">
                    <asp:GridView ID="grdPaymentCollectionDetails" runat="server" Width="100%" ShowFooter="True">
                        <Columns>
                            <asp:CommandField CancelText="" EditText="" HeaderImageUrl="~/PresentationLayer/graph/edit.gif"
                                ShowDeleteButton="True" />
                        </Columns>
                        <RowStyle HorizontalAlign="Center" />
                    </asp:GridView>
                </td>
            </tr>
                   
             <tr bgcolor="#ffffff">
                <td colspan="4">
                    &nbsp;</td>
            </tr>  
            <tr bgcolor="#ffffff">
                <td colspan="4" style="height: 25px">
                    <asp:Label ID="Label3" runat="server" Font-Underline="True"
                        Width="272px">Add Payment</asp:Label>&nbsp;
                </td>
            </tr>
           
            <tr bgcolor="#ffffff">
                <td style="width: 15%; height: 31px;">
                    <asp:Label ID="Label6" runat="server" Text="Payment Date" Width="133px"></asp:Label><br />
                    <asp:Label ID="Label11" runat="server" Text="DD/MM/YYYY"></asp:Label>
                </td>
                <td style="width: 35%; height: 31px;">
                    <asp:TextBox ID="txtPaidDate" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
                    <asp:HyperLink ID="HypCal1" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                        ToolTip="Choose a Date">Choose a Date</asp:HyperLink></td>
                <td style="width: 15%; height: 31px;" valign="top">
                    &nbsp;</td>
                <td style="width: 35%; height: 31px;" valign="top">
                    &nbsp;</td>
            </tr>
            
              <tr bgcolor="#ffffff">
                <td style="width: 15%;">
                    <asp:Label ID="Label9" runat="server" Text="Payment Mode" Width="104px"></asp:Label>
                </td>
                <td style="width: 30%;">
                    <asp:DropDownList ID="cboPaymentMode" runat="server" Width="56%" AutoPostBack="True">
                        <asp:ListItem Selected="True" Value="CS">Cash</asp:ListItem>
                        <asp:ListItem Value="CC">Credit Card </asp:ListItem>
                        <asp:ListItem Value="CQ">Cheque</asp:ListItem>
                    </asp:DropDownList>
                    <asp:Label ID="Label10" runat="server" Font-Bold="True" Font-Size="Small" ForeColor="Red"
                        Text="*"></asp:Label>
                </td>
                <td style="width: 39px;">
                    &nbsp;</td>
                <td style="width: 363px;">
                    &nbsp;&nbsp;
                </td>
            </tr>
            
              <tr bgcolor="#ffffff">
                <td style="width: 15%;">
                    <asp:Label ID="Label12" runat="server" Text="Paid Amount" Width="104px"></asp:Label>
                </td>
                <td style="width: 30%;">
                     <asp:TextBox ID="tbPaidAmount" runat="server" Width="200px" Font-Bold="True" onkeypress="return isNumberKey(event)"></asp:TextBox>
                     <asp:Label ID="Label4" runat="server" Font-Bold="True" Font-Size="Small" ForeColor="Red"
                        Text="*"></asp:Label>
                </td>
                <td style="width: 90px;">
                     
                </td>
                <td style="width: 363px;">
                    &nbsp;&nbsp;
                </td>
                
            </tr>
            
               <tr bgcolor="#ffffff">
                <td style="width: 15%;">
                    <asp:Label ID="Label5" runat="server" Text="Ref No" Width="104px"></asp:Label>
                </td>
                <td style="width: 30%;">
                     <asp:TextBox ID="tbRefNo" runat="server" Width="200px"></asp:TextBox>
                     <asp:Label ID="Label7" runat="server" Font-Bold="True" Font-Size="Small" ForeColor="Red"
                        Text="*"></asp:Label>
                </td>
                <td style="width: 90px;">
                     <asp:LinkButton ID="lnkAddItem" runat="server" CausesValidation="False" Width="80px">Add Item</asp:LinkButton>
                </td>
                <td style="width: 363px;">
                    &nbsp;&nbsp;
                </td>
                
            </tr>
         
            <tr bgcolor="#ffffff" valign="top">
                <td style="height: 2px">
                    &nbsp;</td>
                <td style="width: 30%; height: 2px">
                    &nbsp;</td>
                <td style="width: 39px; height: 2px">
                    &nbsp;</td>
                <td style="width: 363px; height: 2px">
                    &nbsp;</td>
            </tr>
             <tr bgcolor="#ffffff">
                <td colspan="4">
                     <asp:GridView ID="grdNewPayment" runat="server" Width="100%">
                            <Columns>
                                <asp:CommandField CancelText="" EditText="" HeaderImageUrl="~/PresentationLayer/graph/edit.gif"
                                    ShowDeleteButton="True" ButtonType="Button" />
                            </Columns>
                         <RowStyle HorizontalAlign="Center" />
                     </asp:GridView>
                </td>
           </tr>
          
            <tr bgcolor="#ffffff" valign="top">
                <td align="right" colspan="4">
                    <asp:LinkButton ID="lnkSave" runat="server" Width="143px">Save</asp:LinkButton>
                    <asp:LinkButton ID="lnkCancel" runat="server" CausesValidation="False" Width="143px"
                        Style="text-align: right">Cancel</asp:LinkButton>
                    &nbsp; &nbsp; &nbsp;
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td colspan="4">
                    &nbsp;
                    <asp:ValidationSummary ID="vsError" runat="server" ShowMessageBox="True" ShowSummary="False" />
                    <cc1:MessageBox ID="MessageBox2" runat="server" />
                    <cc1:MessageBox ID="MessageBox1" runat="server" />
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td colspan="4">
                    <asp:Label ID="lblNoteDown" runat="server" ForeColor="Red" Text="Label"></asp:Label>
                </td>
            </tr>
        </table>
                    <asp:GridView ID="grdPaymentDetails" runat="server" Width="100%" ShowFooter="True" AutoGenerateColumns="False" DataKeyNames="refno" Visible="False">
                        <Columns>
                             <asp:TemplateField HeaderText="Payment Date">
                                <ItemTemplate>
                                    <asp:TextBox ID="tbPaymentDate" Width="100px" runat="server" Text='<%#Eval("paymentDate") %>'></asp:TextBox>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox id="EditPaymentDateTextBox" runat="server" Width="100px"></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Payment Mode">
                                <ItemTemplate>
                                    <asp:TextBox ID="tbPaymentMode" Width="100px" runat="server" Text='<%#Eval("paymentMode") %>'></asp:TextBox>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:DropDownList ID="ddlPaymentMode" runat="server" Width="100px"></asp:DropDownList>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Credit Amount">
                                <ItemTemplate>
                                    <asp:TextBox ID="tbCreditAmt" Width="100px" runat="server" Text='<%#Eval("creditAmt") %>'></asp:TextBox>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox id="EditCreditAmtTextBox" runat="server" Width="100px"></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Debit Amount">
                                <ItemTemplate>
                                    <asp:TextBox ID="tbDebitAmt" Width="100px" runat="server" Text='<%#Eval("debitAmt") %>'></asp:TextBox>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox id="EditDebitAmtTextBox" runat="server" Width="100px"></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Ref No">
                                <ItemTemplate>
                                    <asp:TextBox ID="tbRefNo" Width="100px" runat="server" Text='<%#Eval("refno") %>'></asp:TextBox>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox id="EditRefNoTextBox" runat="server" Width="100px"></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:Button ID="ButtonDelete" runat="server" CommandName="Delete"  Text="Delete"  />
                                </ItemTemplate>
                             </asp:TemplateField>
                        </Columns>
                        <RowStyle HorizontalAlign="Center" />
                    </asp:GridView>
    </div>
    </form>
</body>
</html>
