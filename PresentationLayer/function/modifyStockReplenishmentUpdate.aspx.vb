Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared
Imports TransFile
Imports System.io


Partial Class PresentationLayer_function_modifyStockReplenishmentUpdate
    Inherits System.Web.UI.Page

    Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
    Dim objXML As New clsXml
    Dim StockReportDoc As New ReportDocument()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Page.MaintainScrollPositionOnPostBack = True
        If (Not Page.IsPostBack) Then
            Dim script As String = "top.location='../logon.aspx';"
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    ' Response.Redirect("~/PresentationLayer/logon.aspx")
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
                Session("StockUpdatePrintView") = "0"
                'txtCollDate.Text = Format(Now, "dd/MM/yyyy")
                HypCal.NavigateUrl = "javascript:DoCal(document.form1.txtCollDate);"
            Catch ex As Exception
                'Response.Redirect("~/PresentationLayer/logon.aspx")
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try
            quanxian()
            BindGrid()
            showpartgridview()

            '''access control'''''''''''''''''''''''''''''''''''''''''''''''
            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "32")

            Me.saveBtn.Enabled = purviewArray(1)


        ElseIf Session("StockUpdatePrintView") = "1" Then
            'Me.PrintData()
        End If
    End Sub
    Protected Sub BindGrid()
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        '-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        '     INITIALIZE THE LABELS
        '-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        titleLab1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-STOCK-01")
        PKLTyLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-STOCK-06")
        PKLDocNLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-STOCK-07")
        PKLDateLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-STOCK-05")
        PRFDateLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "PRF_COLLECTIONDATE")
        countryIDLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-STOCK-17")
        companyIDLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-STOCK-18")
        SerCenterID.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-STOCK-19")
        statusLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCTRY-0004")
        lbltechnicianID.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMTCH-0001")
        TotalAmount.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-STOCK-20")
        TaxAmount.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-STOCK-21")
        Remark.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-STOCK-22")
        CreatBy.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATBY")
        CreatDate.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CREATDATE")
        ModifyBy.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFBY")
        ModifyDate.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MODIFDATE")
        titleLab2.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-STOCK-02")
        saveBtn.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CR-11")
        ReprintPRFBtn.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-STOCK-16")
        cancelBtn.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CR-12")
        CompDateVal.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "INVALID_DATE")
        lblCollDate.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB_FNCSTKR_COLLECTDATE")
        'ModifyByBox.Text = Session("userid").ToString.ToUpper()

        'get the userid and name
        

        '--------------------------------------------------------------
        Dim PKLTY As String = Request.QueryString("pklty").ToString()
        Dim PKLNO As String = Request.QueryString("pklno").ToString()

        Dim clsPKL As New clsStockupdate()
        clsPKL.IPAddress = Request.UserHostAddress
        clsPKL.PKLType = PKLTY
        clsPKL.PKLNO = PKLNO
        clsPKL.userid = Session("userid").ToString.ToUpper()

        Dim ds As New DataSet
        ds = clsPKL.SelByID()

        '-----------------------------------------------------------
        '显示文本框
        '-----------------------------------------------------------
        If ds.Tables(0).Rows.Count > 0 Then
            txtPKLTY.Text = ds.Tables(0).Rows(0).Item("FSR1_PKLTY").ToString()
            txtPKLNO.Text = ds.Tables(0).Rows(0).Item("FSR1_PKLNO").ToString()
            txtPRFTY.Text = ds.Tables(0).Rows(0).Item("FSR1_PRFTY").ToString()
            txtPRFNO.Text = ds.Tables(0).Rows(0).Item("FSR1_PRFNO").ToString()
            txtPKLDate.Text = ds.Tables(0).Rows(0).Item("FSR1_PKLDT").ToString()
            txtPRFDate.Text = IIf(ds.Tables(0).Rows(0).Item("FSR1_PRFDT").ToString() = "", String.Format("{0:d}", Now()), ds.Tables(0).Rows(0).Item("FSR1_PRFDT").ToString())
            txtCountry.Text = ds.Tables(0).Rows(0).Item("FSR1_CTRID").ToString() & " - " & IIf(IsDBNull(ds.Tables(0).Rows(0).Item("MCTR_ENAME")), "", ds.Tables(0).Rows(0).Item("MCTR_ENAME").ToString)
            txtCountryID.Text = ds.Tables(0).Rows(0).Item("FSR1_CTRID").ToString()
            txtCompany.Text = ds.Tables(0).Rows(0).Item("FSR1_COMID").ToString() & " - " & IIf(IsDBNull(ds.Tables(0).Rows(0).Item("MCOM_ENAME")), "", ds.Tables(0).Rows(0).Item("MCOM_ENAME").ToString)
            txtCompanyID.Text = ds.Tables(0).Rows(0).Item("FSR1_COMID").ToString()
            txtServiceCenter.Text = ds.Tables(0).Rows(0).Item("FSR1_SVCID").ToString() & " - " & IIf(IsDBNull(ds.Tables(0).Rows(0).Item("MSVC_ENAME")), "", ds.Tables(0).Rows(0).Item("MSVC_ENAME").ToString)
            txtServiceCenterID.Text = ds.Tables(0).Rows(0).Item("FSR1_SVCID").ToString()
            txtTechnician.Text = ds.Tables(0).Rows(0).Item("FSR1_TCHID").ToString() & " - " & IIf(IsDBNull(ds.Tables(0).Rows(0).Item("MTCH_ENAME")), "", ds.Tables(0).Rows(0).Item("MTCH_ENAME").ToString)
            txtTechnicianID.Text = ds.Tables(0).Rows(0).Item("FSR1_TCHID").ToString()
            txtTotalAmount.Text = ds.Tables(0).Rows(0).Item("FSR1_TOTAM").ToString()
            txtTaxAmount.Text = ds.Tables(0).Rows(0).Item("FSR1_TAXAM").ToString()
            RemarkBox.Text = ds.Tables(0).Rows(0).Item("FSR1_REM").ToString()
            txtCreateBy.Text = ds.Tables(0).Rows(0).Item("CUSER").ToString()
            txtCreateDate.Text = ds.Tables(0).Rows(0).Item("FSR1_CREDT").ToString()
            If ds.Tables(0).Rows(0).Item("FSR1_COLDT").ToString() = "" Then
                txtCollDate.Text = Format(Now, "dd/MM/yyyy")
            Else
                txtCollDate.Text = ds.Tables(0).Rows(0).Item("FSR1_COLDT").ToString()
            End If

            Dim Rcreat As New clsCommonClass
            Dim strUserName As String
            Dim dsUser As New DataSet
            strUserName = ds.Tables(0).Rows(0).Item("FSR1_LUSER").ToString()
            Rcreat.userid() = strUserName
            dsUser = Rcreat.Getuseridname("BB_MASUSER_SelByIDName")
            txtModifyBy.Text = dsUser.Tables(0).Rows(0).Item(0)

            If txtPRFNO.Text <> "" Then
                txtModifyDate.Text = ds.Tables(0).Rows(0).Item("FSR1_LSTDT").ToString()
                Me.txtPRFDate.Text = ds.Tables(0).Rows(0).Item("FSR1_PRFDT").ToString()
            Else
                txtModifyDate.Text = Now
                Me.txtPRFDate.Text = System.DateTime.Today
            End If



        Else
            Dim objXm As New clsXml
            objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            errlab.Text = objXm.GetLabelName("StatusMessage", "NoData")
            errlab.Visible = True
        End If

        If Trim(Replace(txtPRFNO.Text, "-", "")) <> "" Then
            'Me.saveBtn.Enabled = False
            Me.dgrdPackList.Enabled = False
        Else
            Me.saveBtn.Enabled = True
            Me.dgrdPackList.Enabled = True
        End If
        '-----------------------------------------------------------

        '状态显示
        '-----------------------------------------------------------
        Dim cntryStat As New clsUtil()
        Dim statParam As ArrayList = New ArrayList
        statParam = cntryStat.searchconfirminf("STATUS")
        Dim count As Integer
        Dim statid As String
        Dim statnm As String
        cboStatus.Items.Clear()
        For count = 0 To statParam.Count - 1
            statid = statParam.Item(count)
            statnm = statParam.Item(count + 1)
            count = count + 1
            If (Not statid.Equals("DELETE")) And (Not statid.Equals("OBSOLETE")) Then
                cboStatus.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
            End If
        Next
        Try
            cboStatus.SelectedValue = ds.Tables(0).Rows(0).Item("FSR1_STAT").ToString()
        Catch
        End Try
        'commented by deyb
        'statusList.Items.FindByValue(ds.Tables(0).Rows(0).Item(7).ToString()).Selected = True

        If txtPRFNO.Text = "" And cboStatus.SelectedValue = "ACTIVE" Then
        Else
            Me.saveBtn.Enabled = False
            Me.cboStatus.Enabled = False
            HypCal.Enabled = False
        End If


        Me.txtPRFDate.Visible = False
        Me.lblCollDate.Visible = False
    End Sub
    Protected Sub showpartgridview()

        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")

        Dim PKLTY As String = Request.QueryString("pklty").ToString()
        Dim PKLNO As String = Request.QueryString("pklno").ToString()

        Dim clsPKL As New clsStockupdate()
        clsPKL.IPAddress = Request.UserHostAddress
        clsPKL.PKLType = PKLTY
        clsPKL.PKLNO = PKLNO
        clsPKL.userid = Session("userid").ToString.ToUpper()

        Dim ds As New DataSet
        ds = clsPKL.SelByID()

        If ds.Tables(1).Rows.Count <= 0 Then
            Dim objXm As New clsXml
            objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            errlab1.Text = objXm.GetLabelName("StatusMessage", "NoData")
            errlab1.Visible = True
        End If

        Session("addView") = ds
        dgrdPackList.AllowPaging = False
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        dgrdPackList.DataSource = ds.Tables(1)
        dgrdPackList.DataBind()

        If ds.Tables(1).Rows.Count >= 1 Then
            PartGridView1()
            errlab1.Visible = False
        End If
    End Sub
    Protected Sub PartGridView1()
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        dgrdPackList.Columns(0).HeaderText = objXmlTr.GetLabelName("EngLabelMsg", "BB-part-01")
        dgrdPackList.Columns(0).HeaderStyle.Font.Size = 8
        dgrdPackList.Columns(1).HeaderText = objXmlTr.GetLabelName("EngLabelMsg", "BB-part-02")
        dgrdPackList.Columns(1).HeaderStyle.Font.Size = 8
        dgrdPackList.Columns(2).HeaderText = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-STOCK-25")
        dgrdPackList.Columns(2).HeaderStyle.Font.Size = 8
        dgrdPackList.Columns(3).HeaderText = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-STOCK-14")
        dgrdPackList.Columns(3).HeaderStyle.Font.Size = 8
        dgrdPackList.Columns(4).HeaderText = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-STOCK-15")
        dgrdPackList.Columns(4).HeaderStyle.Font.Size = 8
        dgrdPackList.Columns(5).HeaderText = objXmlTr.GetLabelName("EngLabelMsg", "BB-part-05")
        dgrdPackList.Columns(5).HeaderStyle.Font.Size = 8
        dgrdPackList.Columns(6).HeaderText = objXmlTr.GetLabelName("EngLabelMsg", "BB-part-06")
        dgrdPackList.Columns(6).HeaderStyle.Font.Size = 8
        dgrdPackList.Columns(7).HeaderText = objXmlTr.GetLabelName("EngLabelMsg", "BB-part-07")
        dgrdPackList.Columns(7).HeaderStyle.Font.Size = 8
    End Sub
    Protected Sub saveBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveBtn.Click
        If ValidateDetails() = 0 Then
            Dim objXm As New clsXml
            objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            Dim msginfo As String = objXm.GetLabelName("StatusMessage", "SAVEORNOT")
            Dim msgtitle As String = objXm.GetLabelName("StatusMessage", "SAVETITLE")
            MessageBox1.Confirm(msginfo)

            Me.CrystalReportViewer1.Visible = False
        End If
    End Sub
    Protected Sub MessageBox1_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox1.GetMessageBoxResponse
        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then

            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            ' check pkl record have been generated
            Dim lblnGenerated As Boolean = False
            Dim PKLTY As String = Me.txtPKLTY.Text
            Dim PKLNO As String = Me.txtPKLNO.Text

            Dim clsPKL As New clsStockupdate()
            clsPKL.PKLType = PKLTY
            clsPKL.PKLNO = PKLNO

            Dim ds1 As New DataSet
            ds1 = clsPKL.SelByID()

            '-----------------------------------------------------------
            '显示文本框
            '-----------------------------------------------------------
            If ds1.Tables(0).Rows.Count > 0 Then
                txtPRFTY.Text = ds1.Tables(0).Rows(0).Item("FSR1_PRFTY").ToString()
                txtPRFNO.Text = ds1.Tables(0).Rows(0).Item("FSR1_PRFNO").ToString()

                If txtPRFNO.Text <> "" Then
                    lblnGenerated = True
                End If

                If lblnGenerated Then
                    Dim msginfo As String = objXmlTr.GetLabelName("StatusMessage", "DUPLICATED_SAVED")
                    Messagebox2.Alert(msginfo)
                    Exit Sub

                End If
            Else
                Dim msginfo As String = objXmlTr.GetLabelName("StatusMessage", "NoData")
                Messagebox2.Alert(msginfo)
                Exit Sub
            End If



            Dim pklEntity As New clsStockupdate()

            Dim DS As New DataSet
            DS = Me.GridToDataset

            pklEntity.PKLType = txtPKLTY.Text.ToString
            pklEntity.PKLDocNO = txtPKLNO.Text.ToString
            pklEntity.PRFType = txtPRFTY.Text.ToString
            pklEntity.PRFDocNO = txtPRFNO.Text.ToString
            pklEntity.PKLDate = txtPKLDate.Text.ToString
            pklEntity.PRFDate = txtPRFDate.Text.ToString
            pklEntity.IPAddress = Request.UserHostAddress
            pklEntity.ModifyBy = Session("USERID")
            pklEntity.userid = Session("USERID")
            pklEntity.CollectDate = Request.Form("TxtCollDate")
            If (cboStatus.SelectedItem.Value.ToString() <> "") Then
                pklEntity.STATUS = cboStatus.SelectedItem.Value.ToString()
            End If
            pklEntity.PRFDetails = DS

            Dim updCtryCnt As Integer = pklEntity.UPDATE()


            If updCtryCnt = 0 Then
                Me.txtPRFTY.Text = pklEntity.PRFType
                Me.txtPRFNO.Text = pklEntity.PRFDocNO
                Dim objXm As New clsXml
                objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                errlab.Text = objXm.GetLabelName("StatusMessage", "BB-HintMsg-Suc")
                errlab.Visible = True
                'BindGrid()
                Me.dgrdPackList.Enabled = False
            Else
                Response.Redirect("~/PresentationLayer/Error.aspx")
            End If
        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
        End If

    End Sub


   

    Private Sub PrintData()
        'Dim lstrReportFileName As String = "stockRepUp.rpt"
        Dim lstrReportFileName As String = "PRFReport.rpt"


        Dim clsPKL As New clsStockupdate()
        clsPKL.IPAddress = Request.UserHostAddress
        clsPKL.PKLType = Me.txtPKLTY.Text
        clsPKL.PKLNO = Me.txtPKLNO.Text
        clsPKL.userid = Session("userid").ToString.ToUpper()

        'Dim dsReport As DataSet = Session("addView")
        Dim dsReport As DataSet = clsPKL.SelByIDForReport()

        Try
            StockReportDoc.Load(MapPath(lstrReportFileName))
            errlab.Visible = False
        Catch ex As Exception

            objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
            errlab.Text = objXML.GetLabelName("StatusMessage", "BB_FUN_CALLLIST_LOADFAILED")
            errlab.Visible = True
            Return
        End Try
        StockReportDoc.SetDataSource(dsReport.Tables(1))

        Try
            StockReportDoc.Load(MapPath(lstrReportFileName))
            errlab.Visible = False
        Catch ex As Exception

            objXML.XmlFile = ConfigurationManager.AppSettings("StatMsg")
            errlab.Text = objXML.GetLabelName("StatusMessage", "BB_FUN_CALLLIST_LOADFAILED")
            errlab.Visible = True
            Return
        End Try

        CrystalReportViewer1.HasCrystalLogo = False
        objXML.XmlFile = ConfigurationManager.AppSettings("XmlFilePath")

        StockReportDoc.SetParameterValue("NO", objXML.GetLabelName("EngLabelMsg", "BB-FUN-CRYSTALREP-00"))
        StockReportDoc.SetParameterValue("Description", objXML.GetLabelName("EngLabelMsg", "BB-FUN-CRYSTALREP-01"))

        StockReportDoc.SetParameterValue("Stock Code", objXML.GetLabelName("EngLabelMsg", "BB-FUN-CRYSTALREP-02"))
        StockReportDoc.SetParameterValue("Qty Requested", objXML.GetLabelName("EngLabelMsg", "BB-FUN-CRYSTALREP-03"))
        StockReportDoc.SetParameterValue("Tick", objXML.GetLabelName("EngLabelMsg", "BB-FUN-CRYSTALREP-04"))
        StockReportDoc.SetParameterValue("Remarks", objXML.GetLabelName("EngLabelMsg", "BB-FUN-CRYSTALREP-05"))
        StockReportDoc.SetParameterValue("Requested by:", objXML.GetLabelName("EngLabelMsg", "BB-FUN-CRYSTALREP-11"))
        StockReportDoc.SetParameterValue("Cheked by:", objXML.GetLabelName("EngLabelMsg", "BB-FUN-CRYSTALREP-12"))
        StockReportDoc.SetParameterValue("Approved by:", objXML.GetLabelName("EngLabelMsg", "BB-FUN-CRYSTALREP-13"))
        StockReportDoc.SetParameterValue("Store Keeper:", objXML.GetLabelName("EngLabelMsg", "BB-FUN-CRYSTALREP-14"))
        StockReportDoc.SetParameterValue("Received by:", objXML.GetLabelName("EngLabelMsg", "BB-FUN-CRYSTALREP-15"))
        StockReportDoc.SetParameterValue("Name:", objXML.GetLabelName("EngLabelMsg", "BB-FUN-CRYSTALREP-06"))
        StockReportDoc.SetParameterValue("Name1:", objXML.GetLabelName("EngLabelMsg", "BB-FUN-CRYSTALREP-06"))
        StockReportDoc.SetParameterValue("name2", objXML.GetLabelName("EngLabelMsg", "BB-FUN-CRYSTALREP-06"))
        StockReportDoc.SetParameterValue("name3", objXML.GetLabelName("EngLabelMsg", "BB-FUN-CRYSTALREP-06"))
        StockReportDoc.SetParameterValue("name4", objXML.GetLabelName("EngLabelMsg", "BB-FUN-CRYSTALREP-06"))
        StockReportDoc.SetParameterValue("title", objXML.GetLabelName("EngLabelMsg", "BB-FUN-CRYSTALREP-10"))
        StockReportDoc.SetParameterValue("additional", objXML.GetLabelName("EngLabelMsg", "BB-FUN-CRYSTALREP-07"))
        StockReportDoc.SetParameterValue("replenish", objXML.GetLabelName("EngLabelMsg", "BB-FUN-CRYSTALREP-08"))
        StockReportDoc.SetParameterValue("non standard", objXML.GetLabelName("EngLabelMsg", "BB-FUN-CRYSTALREP-09"))
        StockReportDoc.SetParameterValue("date", objXML.GetLabelName("EngLabelMsg", "BB-FUN-CRYSTALREP-17"))
        StockReportDoc.SetParameterValue("ReqNo", "Req.No.")

        'CrystalReportViewer1.ReportSource = StockReportDoc

        Dim sr As Stream = StockReportDoc.ExportToStream(ExportFormatType.PortableDocFormat)
        Response.Clear()
        Dim bSuccess As Boolean = dowload.ResponseFile(Request, Response, "PrfReport.pdf", sr, 1024000)
        If bSuccess = False Then
            'fail
        End If
        sr.Close()
        Response.End()
        Session("StockUpdatePrintView") = "1"
    End Sub

    Protected Sub ReprintPRFBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ReprintPRFBtn.Click
        PrintData()
        CrystalReportViewer1.Visible = True
    End Sub
    Protected Sub quanxian()
        Dim arrayPurview = New Boolean() {False, False, False, False}
        arrayPurview = clsUserAccessGroup.GetUserPurview(Session("accessgroup"), "32")
        saveBtn.Enabled = arrayPurview(1)
        'dgrdPackList.Columns(0).Visible = arrayPurview(1)
        ReprintPRFBtn.Enabled = arrayPurview(3)
    End Sub

    Protected Sub JCalendar1_SelectedDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles jcalPRFDate.SelectedDateChanged
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        txtPRFDate.Text = jcalPRFDate.SelectedDate
    End Sub

    Function ValidateDetails() As Integer
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        ValidateDetails = 0 'OK

        Dim diffqty As Integer = 0
        Dim QTY As Integer = 0

        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")

        Dim i As Integer
        For i = 0 To dgrdPackList.Items.Count - 1
            Dim txtbox As TextBox
            txtbox = Me.dgrdPackList.Items(i).FindControl("PRFQT")

            If Not IsNumeric(txtbox.Text) Then
                'GIVE AN ERROR MESSAGE
                errlab.Text = objXm.GetLabelName("StatusMessage", "BB-PRF-QTYERROR") & " " & dgrdPackList.Items(i).Cells(2).Text
                errlab.Visible = True
                txtbox.Focus()
                Return 1 'Error
            End If

            If Val(dgrdPackList.Items(i).Cells(3).Text) <> Val(txtbox.Text) Then
                diffqty = diffqty + 1
            End If
            QTY = QTY + Val(txtbox.Text)
        Next


        ' check pkl record have been generated
        Dim lblnGenerated As Boolean = False
        Dim PKLTY As String = Me.txtPKLTY.Text
        Dim PKLNO As String = Me.txtPKLNO.Text

        Dim clsPKL As New clsStockupdate()
        clsPKL.PKLType = PKLTY
        clsPKL.PKLNO = PKLNO
       
        Dim ds As New DataSet
        ds = clsPKL.SelByID()

        '-----------------------------------------------------------
        '显示文本框
        '-----------------------------------------------------------
        If ds.Tables(0).Rows.Count > 0 Then
            txtPRFTY.Text = ds.Tables(0).Rows(0).Item("FSR1_PRFTY").ToString()
            txtPRFNO.Text = ds.Tables(0).Rows(0).Item("FSR1_PRFNO").ToString()

            If txtPRFNO.Text <> "" Then
                lblnGenerated = True
            End If

            If lblnGenerated Then
                Dim msginfo As String = objXm.GetLabelName("StatusMessage", "DUPLICATED_SAVED")
                Messagebox2.Alert(msginfo)
                Return 1

            End If
        Else
            Dim msginfo As String = objXm.GetLabelName("StatusMessage", "NoData")
            Messagebox2.Alert(msginfo)
            Return 1
        End If





        If CDate(txtPKLDate.Text) > CDate(txtCollDate.Text) Then
            Dim msginfo As String = objXm.GetLabelName("StatusMessage", "STKREP-COLLDATEERR1")
            Messagebox2.Alert(msginfo)
            Return 1
        End If

        If CDate(txtCollDate.Text) > Now Then
            Dim msginfo As String = objXm.GetLabelName("StatusMessage", "STKREP-COLLDATEERR2")
            Messagebox2.Alert(msginfo)
            Return 1
        End If

        If diffqty > 0 Then
            'WARNING 
            Dim msginfo As String = objXm.GetLabelName("StatusMessage", "BB-PRF-QTYNOTMATCH")
            Dim msgtitle As String = objXm.GetLabelName("StatusMessage", "SAVETITLE")
            MessageBox1.Confirm(msginfo)
            Return 2 'Warning
        End If



    End Function

    Private Function GridToDataset() As DataSet
        Dim ds As New DataSet
        ds.Clear()

        If ds.Tables.Count = 0 Then
            ds.Tables.Add("PRF")
            ds.Tables("PRF").Columns.Add("PKLTY")
            ds.Tables("PRF").Columns.Add("PKLNO")
            ds.Tables("PRF").Columns.Add("LINE")
            ds.Tables("PRF").Columns.Add("PARID")
            ds.Tables("PRF").Columns.Add("PRFQT")
        End If

        Dim intGrdRow As Integer = 0
        Dim drItem As DataRow
        Dim dblGrossTotal As Double = 0
        Dim dblTax As Double = 0

        While intGrdRow <= Me.dgrdPackList.Items.Count - 1
            Dim txtbox As TextBox
            txtbox = Me.dgrdPackList.Items(intGrdRow).FindControl("PRFQT")

            drItem = ds.Tables("PRF").NewRow()
            drItem.Item(0) = Me.txtPKLTY.Text
            drItem.Item(1) = Me.txtPKLNO.Text
            drItem.Item(2) = Me.dgrdPackList.Items(intGrdRow).Cells(0).Text
            drItem.Item(3) = Me.dgrdPackList.Items(intGrdRow).Cells(1).Text
            drItem.Item(4) = txtbox.Text
            ds.Tables("PRF").Rows.Add(drItem)
            intGrdRow = intGrdRow + 1
        End While

        Return ds


    End Function

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        StockReportDoc.Dispose()
        CrystalReportViewer1.Dispose()
    End Sub
End Class
