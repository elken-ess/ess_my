<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ApptAlloc.aspx.vb" EnableEventValidation="false" Inherits="PresentationLayer_Function_ApptAlloc_aspx" %>

<%@ Register Assembly="JCalendar" Namespace="JCalendar" TagPrefix="cc2" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Appointment Allocation</title>
    <script language="JavaScript" src="../js/common.js"></script>
 <meta http-equiv="Content-Type" content="text/html; charset=gb2312"/>
    <link href="../css/style.css" type="text/css" rel="stylesheet"/>
     <style type="text/css">A:link { COLOR: #336666; TEXT-DECORATION: none }
	BODY { FONT-SIZE: 9pt; FONT-FAMILY: "Arial", "Verdana", "Tahoma"; BACKGROUND-COLOR: #ffffff }
	td { FONT-SIZE: 9pt; FONT-FAMILY: Arial,Verdana,Tahoma }
	</style>
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
        
        <script language =javascript >
        
        function changeToolTips () {
//            document.getElementById ("lbxAssigned").title = document.getElementById ("lbxAssigned").value
//            alert(document.getElementById ("lbxAssigned").title)
            //document.getElementById ("lbxAssigned").tooltip =  document.getElementById ("lbxAssigned").value
            
            PresentationLayer_Function_ApptAlloc_aspx.DisplayCustomerAddress(document.getElementById ("lbxAssigned").value, changeToolTips_CallBack);
            
            
            
        }
     function changeToolTips_CallBack(response){
 
     //if the server-side code threw an exception
     if (response.error != null)
     {
      //we should probably do better than this
      alert(response.error); 
      
      return;
     }

     var ResponseValue = response.value;
     document.getElementById ("txtToolTip").value = ResponseValue;     
}     

        </script>
</head>
<body>
    <form id="form1" runat="server" method="post">
    <div>
    <table cellspacing="0" cellpadding="0" width="100%" border="0">
		<tr>
		  <td width="1%" background="../graph/title_bg.gif"><img height="24" src="../graph/title1.gif" width="5"></td>
		  <td class="style2" width="98%" background="../graph/title_bg.gif">
             <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
		  <td align="right" width="1%" background="../graph/title_bg.gif"><img height="24" src="../graph/title_2.gif" width="5"></td>
		</tr>
		<tr><td></td> 
		  <td>
		   <asp:Label ID="lblMessage" runat="server" ForeColor="red" Text=""></asp:Label>
		   </td> 
	   </tr>
	   <tr>
	        <td></td>
	        <td>
                <asp:Label ID="lblNoteUP" runat="server" ForeColor="Red" Text="Label"></asp:Label></td>
	   </tr>
	 </table>
        <table style="width: 100%;" bgColor="#b7e6e6" border="0" cellpadding="0">
            <tr bgColor="#ffffff" style="height:20%">
                <td align="LEFT" colspan="2" style="width: 100%">
                    <table style="width: 100%; height: 100%;" bgColor="#b7e6e6" cellpadding="0">
                        <tr bgColor="#ffffff">
                            <td style="width: 15%" align="LEFT">
                               <asp:Label ID="lblApptdate" runat="server" Text="Label" Width="100%"></asp:Label>
                                <font color="red" >*</font></td>
                            <td style="width: 30%" align="left" valign="middle">
                                <asp:TextBox ID="txtApptDate" Width="85%" runat="server" CssClass="textborder" Readonly="false" MaxLength=10></asp:TextBox>
                                <cc2:JCalendar ID="MyCalendar" ImgUrl="~/PresentationLayer/graph/calendar.gif" ControlToAssign="txtApptDate" runat="server" Visible="False" />
                                <asp:RequiredFieldValidator ID="RFVDate" runat="server" ControlToValidate="txtApptDate"
                                    Display="None" Width="2px" ForeColor="White">*</asp:RequiredFieldValidator>
                                <asp:HyperLink ID="HypCal" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif" ToolTip="Choose a Date">Choose a Date</asp:HyperLink></td>
                            <td style="width: 15%" align="LEFT">
                               <asp:Label ID="lblCenter" runat="server" Text="Label" Width="100%"></asp:Label>
                                <font color="red" >*</font></td>
                            <td style="width: 30%" align="left" valign="middle">
                                <asp:DropDownList ID="SerCenIDDrop" runat="server" AutoPostBack="False" Width="96%" DataTextField="MSVC_SVCNAME" DataValueField="MSVC_SVCID">
                        </asp:DropDownList><asp:RequiredFieldValidator ID="SerCenIDerr" runat="server" ControlToValidate="SerCenIDDrop"
                                    Width="2px" Display="None" ForeColor="White">*</asp:RequiredFieldValidator></td>
                        </tr>
                        <tr bgColor="#ffffff">
                            <td style="width: 10%;" align="LEFT">
                               <asp:Label ID="lblFromZone" runat="server" Text="Label" Width="100%"></asp:Label></td>
                            <td style="width: 40%;" align="left">
                                <asp:TextBox ID="txtFromZone" runat="server" Width="96%" CssClass="textborder" MaxLength="10"></asp:TextBox></td>
                            <td style="width: 15%;" align="LEFT">
                                <asp:Label ID="lblToZone" runat="server" Text="Label" Width="100%"></asp:Label>
                            </td>
                            <td style="width: 35%;" align="left">
                                <asp:TextBox ID="txtToZone" runat="server" Width="96%" CssClass="textborder" MaxLength="10"></asp:TextBox></td>
                        </tr>
                        
                         <tr bgColor="#ffffff">
                            <td style="width: 10%;" align="LEFT">
                               <asp:Label ID="lblAppointmentNo" runat="server" Text="Appointment No" Width="100%"></asp:Label></td>
                            <td style="width: 40%;" align="left">
                                <asp:TextBox ID="txtAppointmentNo" runat="server" Width="96%" CssClass="textborder" MaxLength="10"></asp:TextBox></td>
                            <td style="width: 15%;" align="LEFT">
                                <asp:Label ID="lblCustomerID" runat="server" Text="Customer ID" Width="100%"></asp:Label>
                            </td>
                            <td style="width: 35%;" align="left">
                                <asp:TextBox ID="txtCustomerID" runat="server" Width="96%" CssClass="textborder" MaxLength="10"></asp:TextBox></td>
                        </tr>
                        
                        <tr bgColor="#ffffff">
                            <td style="width: 10%" align="LEFT">
                                <asp:DropDownList ID="techorsub" runat="server" AutoPostBack="true" Font-Size="9pt"
                                    Width="98%">
                                    <asp:ListItem Selected="True">Sub-con</asp:ListItem>
                                    <asp:ListItem>Technician</asp:ListItem>
                                </asp:DropDownList></td>
                            <td style="width: 40%" align="left">
                                <asp:DropDownList ID="DDLtech" Width="98%" Font-Size="9pt" runat="server" AutoPostBack="true">
                                </asp:DropDownList>
                                </td>
                            <td colspan="2" align="CENTER">
                                <asp:LinkButton ID="lbtnRetri" runat="server">Retrieve</asp:LinkButton></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr bgColor="#ffffff" style="width:100%;height:60%">
                <td align="LEFT" colspan="2" style="width: 100%">
                    <table style="width: 100%; height: 100%" bgColor="#b7e6e6" cellpadding="0"  cellspacing="0">
                        <tr bgColor="#ffffff" style="height: 10%">
                            <td align="CENTER" style="width: 40%">
                             <p><br />  <asp:Label ID="lblAreaCust" Font-Bold="true"  runat="server" Text="Label"></asp:Label></p></td>
                            <td align="LEFT" style="width: 20%" valign="middle">
                            <br />
                            </td>
                            <td align="CENTER" style="width: 40%">
                              <p><br /> <asp:Label ID="lblAssigned" Font-Bold="true"  runat="server" Text="Label"></asp:Label></p></td>
                        </tr>
                        <tr bgColor="#ffffff" style="height: 80%;" align="CENTER" valign="bottom"  >
                            <td style="width: 40%;border-style:groove" align="left" valign="top" >
                                <asp:treeView ID="TVareacust"  runat="server" Width="70%" Height="100%" AutoGenerateDataBindings="False" ImageSet="Contacts" NodeIndent="0" BorderStyle="None" MaxDataBindDepth="2" ShowLines="true" ShowCheckBoxes="Leaf" Font-Names="Verdana">
                                    <ParentNodeStyle Font-Bold="True" ForeColor="#5555DD" />
                                    <HoverNodeStyle Font-Underline="False" />
                                    <SelectedNodeStyle Font-Underline="True" HorizontalPadding="0px" VerticalPadding="0px" BorderColor="#FFC0C0" Font-Bold="True" Font-Italic="True" />
                                    <NodeStyle Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" HorizontalPadding="0px" />
                                </asp:treeView>
                                &nbsp;
                            </td>
                            <td style="width: 20%;" align="center" valign="middle">
                                <asp:LinkButton ID="lbtnAddAPPT" runat="server" Width="100%">LinkButton</asp:LinkButton><br /> 
                                <asp:LinkButton ID="lbtnDelAppt" Width="100%" runat="server">LinkButton</asp:LinkButton><br />
                                <asp:LinkButton ID="lbtnAddAll" Width="100%" runat="server">LinkButton</asp:LinkButton><br />
                                <br />
                                <asp:LinkButton ID="lbtnDelAll" Width="100%" runat="server">LinkButton</asp:LinkButton><br />
                                <asp:LinkButton ID="lbtnAddZone" Width="100%" runat="server">LinkButton</asp:LinkButton><br />
                                <asp:LinkButton ID="lbtnEdit" Width="100%" runat="server">LinkButton</asp:LinkButton><br />
                            </td>  
                            <td style="width: 40%;" align="center" valign="middle">
                                <asp:TextBox ID="txtToolTip"  runat="server" Text="" width="90%" Height ="50px" ReadOnly =true TextMode ="MultiLine"  ></asp:TextBox>
                                <asp:ListBox ID="lbxAssigned" style="border-style:groove" runat="server" Width="90%" Height="200px" AutoPostBack="False" onclick="changeToolTips()"></asp:ListBox>
                                
                                
                            </td>
                        </tr>
                        <tr bgColor="#ffffff" style="height: 10%">
                            <td style="width: 40%; height: 10%;" align="center">
                                <asp:Label ID="lblShow" runat="server" Text="Label"></asp:Label></td>
                            <td align="center" style="width: 20%; height: 10%;" valign="middle">
                                <asp:LinkButton ID="lbtnPrint" runat="server">LinkButton</asp:LinkButton> 
                            </td> 
                            <td align="center" style="width: 40%; height: 10%;">
                               <asp:LinkButton ID="lbtnSaveTech" style="width: 40%" runat="server">LinkButton</asp:LinkButton>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr bgColor="#ffffff" style="height:20%">
                <td align="center" colspan="2" style="height: 46%; width: 100%;">
                    <table style="width: 100%; height: 100%" bgColor="#b7e6e6" cellpadding="0">
                        <tr bgColor="#ffffff">
                            <td align="center" style="width: 10%">
                                <asp:Label ID="lblCustIDNM" runat="server" Text="Label" Width="32%"></asp:Label></td>
                            <td style="width: 23.33%" align="center">
                                <asp:TextBox ID="txtCust" runat="server" Width="80%" CssClass="textborder" ReadOnly="true"></asp:TextBox></td>
                            <td align="center" style="width: 10%">
                                <asp:Label ID="lblPO" runat="server" Text="Label" Width="32%"></asp:Label></td>
                            <td style="width: 23.33%" align="center">
                                <asp:TextBox ID="txtPO" runat="server" Width="80%" CssClass="textborder" ReadOnly="true" MaxLength="10"></asp:TextBox></td>
                            <td style="width: 10%" align="center">
                                <asp:Label ID="lblAreaID" runat="server" Text="Label" Width="32%"></asp:Label></td>
                            <td style="width: 23.33%" align="center"> 
                                <asp:DropDownList ID="ddlArea" Width="80%" runat="server" Enabled="False">
                                </asp:DropDownList></td>
                        </tr>
                        <tr bgColor="#ffffff">
                            <td align="center" style="width: 12%">
                                <asp:Label ID="lbladdr1" runat="server" Text="Label" Width="35%"></asp:Label></td>
                            <td align="center" colspan="3">
                                <asp:TextBox ID="txtAddr1" runat="server" Width="80%" CssClass="textborder" ReadOnly="true"></asp:TextBox></td>
                            <td style="width: 10%" align="center">
                                <asp:Label ID="lblZoneID" runat="server" Text="Label" Width="32%"></asp:Label></td>
                            <td style="width: 21.33%" align="center">
                                <asp:DropDownList ID="ddlZone" Width="80%" runat="server" Enabled="False">
                                </asp:DropDownList></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="center" style="width: 12%">
                                <asp:Label ID="lblAddr2" runat="server" Text="Label" Width="35%"></asp:Label></td>
                            <td align="center" colspan="3">
                                <asp:TextBox ID="txtAddr2" runat="server" Width="80%" CssClass="textborder" ReadOnly="true"></asp:TextBox></td>
                            <td align="center" colspan="2">
                    <asp:LinkButton ID="lbtnSave" runat="server">LinkButton</asp:LinkButton></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr bgColor="#ffffff">
                <td>
                    <asp:Label ID="lblNoteDown" runat="server" ForeColor="Red" Text="Label"></asp:Label></td>
            </tr>
        </table>
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
            ShowSummary="False" />
        <cc1:messagebox id="MsgBox" OnGetMessageBoxResponse="MsgBox_GetMessageBoxResponse" runat="server"></cc1:messagebox>
        <cc1:MessageBox id="MsgBox2" OnGetMessageBoxResponse="MsgBox2_GetMessageBoxResponse" runat="server" />
        &nbsp;&nbsp;
    
    </div>
    </form>
</body>
</html>
