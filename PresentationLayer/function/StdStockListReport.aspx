<%@ Page Language="VB" AutoEventWireup="false" CodeFile="StdStockListReport.aspx.vb" Inherits="PresentationLayer_function_StdStocklistReport" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Technician Stock Report</title>
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td background="../graph/title_bg.gif" width="1%">
                    <img height="24" src="../graph/title1.gif" width="5" /></td>
                <td background="../graph/title_bg.gif" class="style2" width="98%">
                    <asp:Label ID="titleLab" runat="server" Text="View Standard Stock List"></asp:Label></td>
                <td align="right" background="../graph/title_bg.gif" width="1%">
                    <img height="24" src="../graph/title_2.gif" width="5" /></td>
            </tr>
        </table>
    
         <table width = 700 border =0 cellpadding =0 cellspacing =0 >
                <tr>
                    <td width = 5%>
                    
                    </td>
                        <td>
                    
                    </td>
                
                </tr>
             <tr>
                 <td width="5%">
                 </td>
                 <td><asp:Label ID="lblMsg" runat="server" Text="" visible=false forecolor=red></asp:Label> 
                     <CR:CrystalReportViewer ID="crvStdStockList" runat="server" AutoDataBind="true" />
                 </td>
             </tr>
              </table>   
  
      
    </div>
    </form>
</body>
</html>
