<%@ Page Language="VB" AutoEventWireup="false" CodeFile="StockReplenishmentUpdate.aspx.vb" Inherits="PresentationLayer_function_StockReplenishmentUpdate" %>

<%@ Register Assembly="JCalendar" Namespace="JCalendar" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Stock Replenishment Update</title>
    <meta http-equiv="Content-Type" content="text/html; charset=gb2312"> 
	<LINK href="../css/style.css" type="text/css" rel="stylesheet">
<script language="JavaScript" src="../js/common.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div style="text-align: left">
        <table bgcolor="#b7e6e6" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
            <tr>
                <td background="../graph/title_bg.gif" style="width: 1%">
                    <img height="24" src="../graph/title1.gif" width="5" /></td>
                <td background="../graph/title_bg.gif" class="style2" width="98%">
                    <asp:Label ID="titleLab1" runat="server" Text="Label"></asp:Label></td>
            </tr>
        </table>   

        <table bgcolor ="#b7e6e6" width ="100%">
            <tr  bgcolor="#ffffff">
                <td style="width: 15%; height: 26px;">
                    <asp:Label ID="ServiceCntLab" runat="server" Text="Label"></asp:Label></td>
                <td style="width: 20%; height: 26px;">
                    <asp:DropDownList ID="cboServiceCenter" runat="server" AutoPostBack="True" Width="99%">
                    </asp:DropDownList></td>
                <td style="width: 15%; height: 26px;">
                    <asp:Label ID="ShTechLab" runat="server" Text="Label"></asp:Label></td>
                <td style="width: 20%; height: 26px;">
                    <asp:DropDownList ID="cboTechnician" runat="server" Width="100%">
                    </asp:DropDownList></td>
                <td style="width: 5%; height: 26px;">
                    <asp:Label ID="ShowCPLLab" runat="server" Text="Label" Visible="False"></asp:Label></td>
                    <asp:DropDownList ID="ShowCPLList" runat="server" Width="100%" Visible="False">
                    </asp:DropDownList></tr>
            <tr  bgcolor="#ffffff">
                <td style="width: 15%; height: 11px;">
                    <asp:Label ID="PKLDocLab" runat="server" Text="Label"></asp:Label></td>
                <td style="width: 15%; height: 11px;">
                    <asp:TextBox ID="txtPKLNo" runat="server" MaxLength="30"></asp:TextBox></td>
               
                <td style="width: 15%; height: 11px;">
                <asp:Label ID="statuslab" runat="server" Text="Label"></asp:Label>
                    </td>
                <td style="width: 5%; height: 11px;">
                <asp:DropDownList ID="cboStatus" runat="server" Width="100%">
                    </asp:DropDownList>
                </td><td ></td>
               <%-- <td style="width: 15%; height: 11px;" visible ="false" >
                </td>--%>
            </tr>
            <tr  bgcolor="#ffffff">
                <td style="width: 100px">
                    <asp:Label ID="PKLDateLab" runat="server" Text="Label"></asp:Label></td>
                <td style="width: 15%">
                    <asp:TextBox ID="txtPKLDate" runat="server" Width="124px" MaxLength="10" Enabled="TRUE" ReadOnly=false></asp:TextBox>
                    <cc1:JCalendar ID="JCalendar1" runat="server" ControlToAssign="PKLDateBox" ImgURL="~/PresentationLayer/graph/calendar.gif" Visible="False" />
                    <asp:HyperLink ID="HypCal" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                        ToolTip="Choose a Date">Choose a Date</asp:HyperLink>
                </td>
                <td style="width: 15%">
                    </td>
                <td style="width: 15%"></td> 
                    
                <td style="width: 5%">
                    <asp:LinkButton ID="lnkSearch" runat="server">Search</asp:LinkButton></td>
                <%--<td style="width: 15%" >
                </td>--%>
            </tr>
        </table>
                    <asp:GridView ID="PPTGrid" runat="server" style="width: 100%" AutoGenerateColumns="False">
                        <Columns>
                            <asp:CommandField ShowSelectButton="True" HeaderImageUrl="~/PresentationLayer/graph/edit.gif" />
                            <asp:BoundField DataField="PKLTY" HeaderText="PKLTY" />
                            <asp:BoundField DataField="PKLNO" HeaderText="PKLNO" />
                            <asp:BoundField DataField="PKLDT" DataFormatString="{0:d}" HeaderText="PKL Date" />
                            <asp:BoundField DataField="PRFTY" HeaderText="PRFTY" />
                            <asp:BoundField DataField="PRFNO" HeaderText="PRFNO" />
                            <asp:BoundField DataField="PRFDT" DataFormatString="{0:d}" HeaderText="PRF Date" />
                            <asp:BoundField DataField="COLDT" DataFormatString="{0:d}" HeaderText="COL Date" />
                            <asp:BoundField DataField="SVCNM" HeaderText="Service Center" />
                            <asp:BoundField DataField="TCHNM" HeaderText="Technician" />                            
                            <asp:BoundField DataField="STAT" HeaderText="Status" />
                        </Columns>
                    </asp:GridView>
        <br />
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
            <tr>
                <td background="../graph/title_bg.gif" class="style2" style="width: 110%">
                    <font color="red" style="width: 100%">
                        <asp:Label ID="errlab" runat="server" Text="Label" Visible="false"></asp:Label></font></td>
            </tr>
        </table>
        </div>
        <asp:Label ID="ShowOTDLab" runat="server" Text="Label" Visible =false></asp:Label>
        <asp:DropDownList ID="ShowOTDList" runat="server" Width="100%"  Visible =false >
                    </asp:DropDownList></td>
    </form>
</body>
</html>
