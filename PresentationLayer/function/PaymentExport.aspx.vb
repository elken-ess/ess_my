Imports BusinessEntity
Imports System.Data
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Windows
Imports Microsoft.Win32


Partial Class PresentationLayer_function_PaymentExport
    Inherits System.Web.UI.Page
    Dim objXmlTr As New clsXml
    Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)
        Return connection
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.MaintainScrollPositionOnPostBack = True
        If Not Page.IsPostBack Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try

            Dim UserID As String = Session("userID")
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle


            Dim CommonClass As New clsCommonClass()
            If Session("login_rank") <> 0 Then
                CommonClass.spctr = Session("login_ctryID").ToString().ToUpper
            End If
            CommonClass.rank = Session("login_rank")

            Dim userIDNamestr As String = Session("userID").ToString().ToUpper + "-" + Session("username").ToString.ToUpper
            Dim rank As String = Session("login_rank")
            CommonClass.spstat = Session("login_cmpID")
            CommonClass.sparea = Session("login_svcID")

            If rank <> 0 Then
                CommonClass.spctr = Session("login_ctryID").ToString().ToUpper
            End If
            CommonClass.rank = rank
            CommonClass.spstat = "ACTIVE"

            lblTotalDescription.Visible = False
            BindData()
            BindGrid(UserID)

        ElseIf appsView.Rows.Count > 0 Then
            DispGridViewHead()
        End If


        HypCal.NavigateUrl = "javascript:DoCal(document.form1.txtDueDateFr);"
        HypCal2.NavigateUrl = "javascript:DoCal(document.form1.txtDueDateTo);"

        searchButton.Enabled = True
        searchButton.Visible = True
    End Sub
    Protected Sub BindGrid(ByVal strUserID As String)
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")

        appsView.AllowPaging = True
        appsView.AllowSorting = True
        Dim editcol As New CommandField
        editcol.EditText = objXmlTr.GetLabelName("EngLabelMsg", "BB-Edit") '"edit"
        editcol.HeaderImageUrl = "~/PresentationLayer/graph/edit.gif"
        editcol.ShowEditButton = True
        appsView.Columns.Add(editcol)

        lblNoRecord.Visible = False

        'access control'''''''''''''''''''''''''''''''''''''''''''''''
        Dim accessgroup As String = Session("accessgroup").ToString
        Dim purviewArray As Array = New Boolean() {False, False, False, False}
        purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "22")

        'editcol.Visible = purviewArray(2)


        If txtDueDateFr.Text = "" Then
            txtDueDateFr.Text = System.DateTime.Today
        End If

        If txtDueDateTo.Text = "" Then
            txtDueDateTo.Text = System.DateTime.Today
        End If
    End Sub
    Private Sub DispGridViewHead()
        appsView.HeaderRow.Cells(0).Text = ""
        appsView.HeaderRow.Cells(0).Font.Size = 8
        appsView.HeaderRow.Cells(1).Text = "CardHolderName"
        appsView.HeaderRow.Cells(1).Font.Size = 8
        appsView.HeaderRow.Cells(2).Text = "CreditCardNo"
        appsView.HeaderRow.Cells(2).Font.Size = 8
        appsView.HeaderRow.Cells(3).Text = "Expiry"
        appsView.HeaderRow.Cells(3).Font.Size = 8
        appsView.HeaderRow.Cells(4).Text = "Amt"
        appsView.HeaderRow.Cells(4).Font.Size = 8
        appsView.HeaderRow.Cells(5).Text = "ContractNo"
        appsView.HeaderRow.Cells(5).Font.Size = 8
        appsView.HeaderRow.Cells(6).Text = "Month"
        appsView.HeaderRow.Cells(6).Font.Size = 8
        appsView.HeaderRow.Cells(7).Text = "Date"
        appsView.HeaderRow.Cells(7).Font.Size = 8
        appsView.HeaderRow.Cells(8).Text = "Tenure"
        appsView.HeaderRow.Cells(8).Font.Size = 8
        appsView.HeaderRow.Cells(9).Text = "Status"
        appsView.HeaderRow.Cells(9).Font.Size = 8
        appsView.HeaderRow.Cells(10).Text = "SerialNo"
        appsView.HeaderRow.Cells(10).Font.Size = 8
        appsView.HeaderRow.Cells(11).Text = "CustomerID"
        appsView.HeaderRow.Cells(11).Font.Size = 8
        appsView.HeaderRow.Cells(12).Text = "Remarks"
        appsView.HeaderRow.Cells(12).Font.Size = 8
        appsView.HeaderRow.Cells(13).Text = "InvoiceNo"
        appsView.HeaderRow.Cells(13).Font.Size = 8
        appsView.HeaderRow.Cells(14).Text = "InitialAmt"
        appsView.HeaderRow.Cells(14).Font.Size = 8
        appsView.HeaderRow.Cells(15).Text = "Technician"
        appsView.HeaderRow.Cells(15).Font.Size = 8
    End Sub

    Protected Sub searchButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles searchButton.Click
        Dim UserID As String = Session("userID")
        Dim objCommonTel As New clsCommonClass
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        txtDueDateFr.Text = Request.Form("txtDueDateFr")
        txtDueDateTo.Text = Request.Form("txtDueDateTo")
        'search condition

        Dim Conn As New SqlConnection
        Conn.ConnectionString = ConfigurationManager.AppSettings("ConnectionString").ToString

        Using Comm As New SqlClient.SqlCommand("exec dbo.BB_EXPORTPAYMENT '" + txtCustomerID.Text + "','" + txtDueDateFr.Text + "','" + txtDueDateTo.Text + "','" _
        & "" + ddlcontracttypefr.SelectedValue + "','" + ddlcontracttypeto.SelectedValue + "','" + ddlStatus.SelectedValue + "','" + txtContractID.Text + "','" + txtInvoiceNo.Text + "'", Conn)
            Conn.Open()

            Try
                Using sda As New SqlDataAdapter
                    sda.SelectCommand = Comm

                    If sda Is Nothing Then
                        appsView.Visible = False
                        lblNoRecord.Visible = True
                        Return
                    Else
                        Using selds As New DataSet
                            sda.Fill(selds)
                            appsView.Visible = True
                            appsView.DataSource = selds
                            appsView.DataBind()
                            DispGridViewHead()
                            If selds.Tables(0).Rows.Count > 0 Then
                                lblNoRecord.Visible = False
                                DispGridViewHead()
                                LblTotRecNo.Text = selds.Tables(0).Rows.Count
                            Else
                                LblTotRecNo.Text = "0"
                            End If
                            Session("selds") = selds
                        End Using
                    End If
                End Using
            Catch ex As Exception
                Return
            End Try
        End Using
        Conn.Close()
    End Sub
    Protected Sub appsView_PageIndexChanging(ByVal sender As Object, ByVal e As GridViewPageEventArgs) Handles appsView.PageIndexChanging
        appsView.PageIndex = e.NewPageIndex
        Threading.Thread.CurrentThread.CurrentCulture = datestyle

        If Session("selds") IsNot Nothing Then
            Dim ds As DataSet = CType(Session("selds"), DataSet)
            'Dim ds As DataSet = Session("appsView")
            appsView.DataSource = ds
            appsView.DataBind()
            If ds.Tables(0).Rows.Count > 0 Then
                DispGridViewHead()
            End If
        End If

    End Sub

    Protected Sub appsView_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles appsView.RowEditing

        Dim ContractID As String = appsView.Rows(e.NewEditIndex).Cells(5).Text.Trim()
        ContractID = Server.UrlEncode(ContractID)
        Dim Month As String = appsView.Rows(e.NewEditIndex).Cells(6).Text.Trim()
        Month = Server.UrlEncode(Month)
        Dim strTempURL As String = "ContractID=" + ContractID + "&Month=" + Month
        strTempURL = "~/PresentationLayer/function/modifyPaymentExport.aspx?" + strTempURL
        Response.Redirect(strTempURL)
    End Sub

    Protected Sub appsView_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles appsView.SelectedIndexChanged

    End Sub

    Private Sub BindData()
        Dim Conn As New SqlConnection
        Conn.ConnectionString = ConfigurationManager.AppSettings("ConnectionString").ToString

        ddlcontracttypefr.Items.Clear()
        ddlcontracttypeto.Items.Clear()

        If ddlcontracttypefr.SelectedValue = "" Then
            Dim Comm2 As SqlCommand
            Comm2 = Conn.CreateCommand
            Comm2.CommandText = "select msv1_svtid from msv1_fil where msv1_stat = 'active' and msv1_svty = 'contract' and msv1_ctrid = '" + Session("login_ctryID").ToString() + "' order by msv1_svtid"
            Conn.Open()

            Dim dr2 As SqlDataReader = Comm2.ExecuteReader(CommandBehavior.CloseConnection)

            Dim dt2 As DataTable = New DataTable()
            dt2.Load(dr2)

            Dim counter2 As Integer

            For counter2 = 0 To dt2.Rows.Count - 1
                ddlcontracttypefr.Items.Add(dt2.Rows(counter2).Item("msv1_svtid").ToString)
                ddlcontracttypeto.Items.Add(dt2.Rows(counter2).Item("msv1_svtid").ToString)
            Next
            Conn.Close()

            ddlcontracttypefr.SelectedIndex = 0
            ddlcontracttypeto.SelectedIndex = ddlcontracttypeto.Items.Count - 1
        End If
    End Sub
    Protected Sub ddlStateFr_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlcontracttypefr.SelectedIndexChanged

    End Sub

    Protected Sub ddlStateTo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlcontracttypeto.SelectedIndexChanged

    End Sub
    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub
    Protected Sub BtnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnExport.Click
        If Session("selds") Is Nothing Then
            lblNoRecord.Text = "Please search report for results first before exporting."
            lblNoRecord.Visible = True
            Return
        End If

        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=PaymentExport" + DateTime.Now.ToString + ".xls")
        Response.Charset = ""
        Response.ContentType = "application/vnd.xls"
        Dim StringWriter As IO.StringWriter = New System.IO.StringWriter()
        Dim HtmlTextWriter As New HtmlTextWriter(StringWriter)
        Dim style As String = "<style> td { mso-number-format:\@; } </style> "

        appsView.AllowPaging = False
        appsView.DataSource = Session("selds")
        appsView.DataBind()

        appsView.RenderControl(HtmlTextWriter)
        Response.Write(style) 'style is added dynamically
        Response.Write(StringWriter.ToString())
        Response.[End]()


    End Sub

    Protected Sub btnImport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnImport.Click
        Dim script As String = "window.open('../function/PaymentImport.aspx')"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "PaymentImport", script, True)
    End Sub
End Class
