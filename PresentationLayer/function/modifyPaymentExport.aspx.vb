Imports BusinessEntity
Imports System.Data
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Windows
Imports Microsoft.Win32

Partial Class PresentationLayer_function_modifypaymentexport_aspx
    Inherits System.Web.UI.Page
    Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        AjaxPro.Utility.RegisterTypeForAjax(GetType(PresentationLayer_function_modifypaymentexport_aspx))
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        If Not Page.IsPostBack Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try
            Dim ContractID As String = Request.Params("ContractID").ToString()
            Dim Month As String = Request.Params("Month").ToString()


            If ContractID.Trim() <> "" And Month.Trim() <> "" Then
                GetData(ContractID, Month)
            End If

            'access control
            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "22")

            Me.saveButton.Enabled = purviewArray(1)

        End If
        HypCal.NavigateUrl = "javascript:DoCal(document.form1.txtActualPaymentDate);"

        lblNoteDown.Visible = False
        lblNoteUP.Visible = False

    End Sub
    Private Sub GetData(ByVal ContractID As String, ByVal Month As String)
        Dim Conn As New SqlConnection
        Conn.ConnectionString = ConfigurationManager.AppSettings("ConnectionString").ToString


        Using Comm As New SqlClient.SqlCommand("select [ContractID] = FCU1_CONID, " _
            & "[InstalledBasedID] = MROU_ROUID, " _
            & "[Country] = FCU1_CUSPF, " _
            & "[SerialNum] = MROU_MODID+'-'+MROU_SERNO, " _
            & "[CustomerID] = MCUS_CUSID, " _
            & "[CustomerName] = MCUS_ENAME, " _
            & "[CardHolderName] = FCU1_CCNAME, " _
            & "[CCNo] = FCU1_CCNO, " _
            & "[CCExpiry] = FCU1_CCEX, " _
            & "[PaymentMth] = FCU3_INMTH, " _
            & "[PaymentDueDt] = FCU3_NXPYM, " _
            & "[Status] = FCU3_STAT, " _
            & "[Remarks] = FCU3_REM, " _
            & "[ActualPaymentDt] = isnull(FCU3_PYMDT,''), " _
            & "[InvoiceNo] = FCU3_INVTY + FCU3_INVNO " _
            & "from FCU1_FIL left join FCU2_FIL on FCU1_CONID = FCU2_CONID " _
            & "left join FCU3_FIL on FCU2_CONID = FCU3_CONID " _
            & "left join MROU_FIL on MROU_ROUID = FCU1_ROUID and MROU_CUSPF = FCU1_CUSPF and MROU_CUSID = FCU1_CUSID " _
            & "left join MCUS_FIL on MCUS_CUSID = MROU_CUSID and MCUS_CUSPF = MROU_CUSPF " _
            & "where FCU1_CONID = '" & ContractID & "'" _
            & "and FCU3_INMTH = '" & Month & "'", Conn)
            Conn.Open()
            Using readerObj As SqlClient.SqlDataReader = Comm.ExecuteReader
                While readerObj.Read
                    txtContractID.Text = readerObj("ContractID").ToString
                    txtInstalledBasedID.Text = readerObj("InstalledBasedID").ToString
                    txtCountry.Text = readerObj("Country").ToString
                    txtSerialNumber.Text = readerObj("SerialNum").ToString
                    txtCustomerID.Text = readerObj("CustomerID").ToString
                    txtCustomerName.Text = readerObj("CustomerName").ToString
                    txtCCName.Text = readerObj("CardHolderName").ToString
                    txtCCNumber.Text = readerObj("CCNo").ToString
                    txtCCExpiry.Text = readerObj("CCExpiry").ToString
                    txtPaymentMonth.Text = readerObj("PaymentMth").ToString
                    txtPaymentDueDate.Text = Left(readerObj("PaymentDueDt"), 10).ToString
                    ddlStatus.SelectedValue = readerObj("Status").ToString
                    txtRemarks.Text = readerObj("Remarks").ToString
                    txtActualPaymentDate.Text = Left(readerObj("ActualPaymentDt"), 10).ToString
                    txtInvoiceNumber.Text = readerObj("InvoiceNo").ToString
                End While
            End Using
            Conn.Close()
        End Using
    End Sub

    Protected Sub saveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveButton.Click

        txtActualPaymentDate.Text = Request.Form("txtActualPaymentDate")

        Try

            Dim Conn As New SqlConnection
            Conn.ConnectionString = ConfigurationManager.AppSettings("ConnectionString").ToString

            Using command As New SqlCommand("exec dbo.BB_EXPORTPAYMENT_SAVE @ContractID, @Month, @Status, @CustomerID, @CustomerPF, @Remarks, @PaymentDate, @UserID", Conn)
                command.Parameters.AddWithValue("@ContractID", txtContractID.Text)
                command.Parameters.AddWithValue("@Month", txtPaymentMonth.Text)
                command.Parameters.AddWithValue("@Status", ddlStatus.SelectedValue)
                command.Parameters.AddWithValue("@CustomerID", txtCustomerID.Text)
                command.Parameters.AddWithValue("@CustomerPF", txtCountry.Text)
                command.Parameters.AddWithValue("@Remarks", txtRemarks.Text)
                command.Parameters.AddWithValue("@PaymentDate", txtActualPaymentDate.Text)
                command.Parameters.AddWithValue("@UserID", (Session("userID").ToString()))
                Conn.Open()

                command.ExecuteNonQuery()
                Conn.Close()
            End Using

            Using Comm As New SqlClient.SqlCommand("select distinct [InvoicePf] = FCU3_INVTY, " _
                        & "[InvoiceNo] = FCU3_INVNO  " _
                        & "from FCU1_FIL left join FCU2_FIL on FCU1_CONID = FCU2_CONID " _
                        & "left join FCU3_FIL on FCU2_CONID = FCU3_CONID " _
                        & "where FCU1_CONID = '" & txtContractID.Text & "'" _
                        & "and FCU3_INMTH = '" & txtPaymentMonth.Text & "'", Conn)
                Conn.Open()
                Using readerObj As SqlClient.SqlDataReader = Comm.ExecuteReader
                    While readerObj.Read
                        txtInvoiceNumber.Text = readerObj("InvoicePf").ToString & readerObj("InvoiceNo").ToString
                    End While
                End Using
                Conn.Close()
            End Using

            lblNoteDown.Text = "Record saved successfully."
            lblNoteUP.Text = "Record saved successfully."
            lblNoteDown.Visible = True
            lblNoteDown.Visible = True

        Catch ex As Exception
            lblNoteDown.Text = "There's an exception during record update. Pls contact GIT to check."
            lblNoteUP.Text = "There's an exception during record update. Pls contact GIT to check."
            lblNoteDown.Visible = True
            lblNoteDown.Visible = True
        End Try
        
    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton1.Click
        Dim Conn As New SqlConnection
        Conn.ConnectionString = ConfigurationManager.AppSettings("ConnectionString").ToString

        Using Comm As New SqlClient.SqlCommand("select distinct [InvoicePf] = FCU3_INVTY, " _
            & "[InvoiceNo] = FCU3_INVNO  " _
            & "from FCU1_FIL left join FCU2_FIL on FCU1_CONID = FCU2_CONID " _
            & "left join FCU3_FIL on FCU2_CONID = FCU3_CONID " _
            & "where FCU1_CONID = '" & txtContractID.Text & "'" _
            & "and FCU3_INMTH = '" & txtPaymentMonth.Text & "'", Conn)
            Conn.Open()
            Using readerObj As SqlClient.SqlDataReader = Comm.ExecuteReader
                While readerObj.Read
                    Session("InvoicePf") = readerObj("InvoicePf").ToString
                    Session("InvoiceNo") = readerObj("InvoiceNo").ToString
                End While
            End Using
            Conn.Close()
        End Using

        Dim script As String = "window.open('../function/PrintTaxInvoice.aspx')"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "PrintTaxInvoice", script, True)
    End Sub
End Class
