<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AppointmentCallingListing.aspx.vb" Inherits="PresentationLayer_function_AppointmentCallingListing" %>

<%@ Register Assembly="JCalendar" Namespace="JCalendar" TagPrefix="cc2" %>
<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Online Calling List</title>
    <meta http-equiv="Content-Type" content="text/html; charset=gb2312"/>
	<link href="../css/style.css" type="text/css" rel="stylesheet"/>
</head>
<body>
    <form id="frmOnlineCallList" runat="server">
<table id="uagTab" border="0" width="150%">
            <tr>
                <td>
                <table border="0" cellpadding="0" cellspacing="0" width="150%">
                <tr>
                    <td background="../graph/title_bg.gif" style="width: 1%; height: 24px;">
                        <img height="24" src="../graph/title1.gif" width="5" /></td>
                    <td background="../graph/title_bg.gif" class="style2" width="98%" style="height: 24px">
                        <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
                    <td align="right" background="../graph/title_bg.gif" width="1%" style="height: 24px">
                        <img height="24" src="../graph/title_2.gif" width="5" /></td>
                </tr>                        
                </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table border="0" cellpadding="0" cellspacing="0" width="150%">
                        <tr>
                            <td background="../graph/title_bg.gif" class="style2" width="98%">
                                <font color="red">
                                    <asp:Label ID="errlab" runat="server" Text="Label" Visible="False"></asp:Label></font></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <asp:Label ID="State" runat="server" Width="96px" Visible="False"></asp:Label>
        <asp:Label ID="StateID" runat="server" Width="96px" Visible="False"></asp:Label>
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
        <td ><asp:Label ID="CusID" runat="server"  Width="100px"></asp:Label></td>
        <td ><asp:TextBox ID="CusIDBox" runat="server" MaxLength="50"></asp:TextBox></td>
        <td ><asp:Label ID="CusName" runat="server"  Width="100px"></asp:Label></td>
        <td ><asp:TextBox ID="CusNameBox" runat="server" MaxLength="50"></asp:TextBox></td>
         </tr>
         <tr>
        <td ><asp:Label ID="lblStateID" runat="server" Text="State ID"  Width="80px"></asp:Label></td>
        <td ><asp:dropdownlist ID="cboStateID" runat="server" width=60% AutoPostBack =true></asp:dropdownlist></td>   
        <td ><asp:Label ID="lblAreaID" runat="server"  Text="Area ID" Width="80px"></asp:Label></td>
        <td ><asp:dropdownlist ID="cboAreaID" runat="server" width=60%  ></asp:dropdownlist></td>  
        </tr>
        <tr>
        <td ><asp:Label ID="lblContractEntitled" runat="server" Text="Contract Entitled" Width="80px"></asp:Label></td>
        <td ><asp:DropDownList ID="cboContractEntitled" runat="server" Width="100px"></asp:DropDownList></td>
        <td ><asp:Label ID="Status" runat="server" Width="80px"></asp:Label></td>
        <td ><asp:DropDownList ID="StatusDrop" runat="server" Width="144px"></asp:DropDownList></td>
        </tr>
        <tr>
        <td></td>
        <td></td>
        <td></td>
        <td ><asp:LinkButton ID="BtnSeach" runat="server" CausesValidation="False"></asp:LinkButton></td>
        </tr>
        </table>
       
        <asp:GridView ID="ApptListView" runat="server" Width="100%" AutoGenerateColumns="False" AllowPaging="True">
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <%#Container.DataItemIndex + 1%>
                    </ItemTemplate>
                    <ItemStyle Width="3%" />
                </asp:TemplateField>
                
                 <asp:BoundField DataField="MROU_ROUID">
                </asp:BoundField>
                
                <asp:BoundField DataField="ROINFO" >
                    <ItemStyle Width="7%" />
                </asp:BoundField>
                
                <asp:TemplateField>                 
                    <ItemTemplate>
                        <asp:LinkButton ID="CustomID" CommandName ="CustomID" Width="10%" CommandArgument ='<%#Container.DataItemIndex %>' Text='<%# Bind("MROU_CUSID") %>'  runat="server" ></asp:LinkButton>
                    </ItemTemplate>
                    <ItemStyle Width="6%" />
                </asp:TemplateField>
                
                
                <asp:BoundField DataField="MCUS_ENAME" >
                    <ItemStyle Width="10%" />
                </asp:BoundField>
                
                <asp:BoundField DataField="MADR_AREID">
                    <ItemStyle Width="10%" />
                </asp:BoundField>
                
                <asp:BoundField DataField="MADR_STAID">
                    <ItemStyle Width="3%" />
                </asp:BoundField>
                
                <asp:TemplateField>
                    <ItemTemplate>
                     <asp:DropDownList ID="ddlstatus"  runat="server" Width="100%"></asp:DropDownList>
                    </ItemTemplate>
                    <ItemStyle Width="10%" />
                </asp:TemplateField>
                
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:TextBox ID="ActualServDateBox" runat="server" width="70%" Text='<%# Bind("MROU_NCADT") %>' MaxLength="10"></asp:TextBox>

                        <cc2:JCalendar ID="ActualServDateDateCalendar" runat="server"  TabIndex ='<%#Container.DataItemIndex%>' Enabled="true" ImgURL="~/PresentationLayer/graph/calendar.gif" OnSelectedDateChanged="ActualServDateDateCalendar_SelectionChanged" Visible="True" />
                        <asp:CompareValidator ID="CompDateVal" runat="server" ControlToValidate="ActualServDateBox" Operator="DataTypeCheck" SetFocusOnError="True"
                        Type="Date"></asp:CompareValidator>&nbsp;
                    </ItemTemplate>
                    <ItemStyle Width="16%" />
                </asp:TemplateField>
                
                 <asp:TemplateField>
                <ItemTemplate>
                  <asp:TextBox ID="ddlremark"   runat="server" MaxLength="70"></asp:TextBox>
                </ItemTemplate>
                    <ItemStyle Width="6%" />
                </asp:TemplateField>
                            
                <asp:TemplateField>
                <ItemTemplate>
                  <asp:LinkButton id="update" runat="server" CommandName="Update" CommandArgument='<%#Container.DataItemIndex %>'></asp:LinkButton>
                </ItemTemplate>
                    <ItemStyle Width="5%" />
                </asp:TemplateField>
                
                <asp:TemplateField>
                <ItemTemplate>
                  <asp:LinkButton ID="CreateAppt" runat="server" CommandName="CreateAppt" CommandArgument='<%#Container.DataItemIndex %>'></asp:LinkButton>
                </ItemTemplate>
                    <ItemStyle Width="5%" />
                </asp:TemplateField>
                
            </Columns>
        </asp:GridView>
        <hr/>
        <asp:Label ID="SucCallNum" runat="server" Width="200px"></asp:Label>
        <asp:Label ID="Number" runat="server"></asp:Label>
        &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;
        &nbsp;<asp:LinkButton ID="BtnPrePage" runat="server"></asp:LinkButton>
        &nbsp; &nbsp; &nbsp;<asp:LinkButton ID="BtnNexPage" runat="server"></asp:LinkButton>
        &nbsp; &nbsp; &nbsp;<asp:LinkButton ID="BtnRefresh" runat="server" CausesValidation="False"></asp:LinkButton>
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True" DisplayMode="List" ShowSummary="False" />
        <hr/>
        <cc1:messagebox id="MessageBox" runat="server"></cc1:messagebox><cc1:messagebox id="MsgErr" runat="server"></cc1:messagebox>
    </form>
</body>
</html>
