Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Partial Class PresentationLayer_Function_UpdateServiceBillPart2
    Inherits System.Web.UI.Page
    Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try

        If (Not Page.IsPostBack) Then
            txtStartDate.Text = System.DateTime.Today
            txtEndDate.Text = System.DateTime.Today
            BindGrid()

        End If
        HypCal1.NavigateUrl = "javascript:DoCal(document.form1.txtStartDate);"
        HypCal2.NavigateUrl = "javascript:DoCal(document.form1.txtEndDate);"



    End Sub
    Protected Sub BindGrid()
        
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        solab.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-INVOICENO")
        phnolab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-customer-02")
        CNlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-customer-03")
        snlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-customer-04")
        searchbtn.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-SEARCH")
        titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-update2")
        ctryStat.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCTRY-0004")
        Me.lblNoRecord.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-NR")
        Me.lblStartDate.Text = objXmlTr.GetLabelName("EngLabelMsg", "INVSTARTDATE")
        Me.lblEndDate.Text = objXmlTr.GetLabelName("EngLabelMsg", "INVENDDATE")

        Me.lblAppointmentNo.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-APPOINMENTNO")
        Me.lblCustomerID.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RPTMDSL-0001")
        Me.lblManualServiceBillNo.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-SERVICEBILLNO")

        lblNoRecord.Visible = False
        '--------------------------------------------------------------------
        Dim cntryStat As New clsUtil()
        Dim statParam As ArrayList = New ArrayList
        statParam = cntryStat.searchconfirminf("SerStat")
        Dim count As Integer
        Dim statid As String
        Dim statnm As String
        For count = 0 To statParam.Count - 1
            statid = statParam.Item(count)
            statnm = statParam.Item(count + 1)
            count = count + 1

            If statid = "UM" Or statid = "UA" Or statid = "BL" Or UCase(statid) = "AL" Then
                ctryStatDrop.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
            End If
        Next
        ctryStatDrop.Items(0).Selected = True
        '----------------------------------------------------------------------------------------------
        'Dim objServiceBillPart12 As New clsupdateservice2()

        ''Amended by KSLim on 18/6/2006
        'objServiceBillPart12.SOID = soText.Text
        'objServiceBillPart12.CustomerName = CNText.Text
        'objServiceBillPart12.PhoneNo = phnoText.Text
        'objServiceBillPart12.SerialNo = snText.Text
        'objServiceBillPart12.UserID = Session("userID").ToString()
        'objServiceBillPart12.STATUS = ctryStatDrop.Text     '5.16增加
        ''Amended by KSLim on 18/6/2006

        'Dim partall As DataSet = objServiceBillPart12.SelectAllServiceBillPart2()
        'partView.DataSource = partall

        partView.AllowPaging = True
        partView.AllowSorting = True
        Dim editcol As New CommandField
        editcol.EditText = objXmlTr.GetLabelName("EngLabelMsg", "BB-Edit") '"edit"
        editcol.HeaderImageUrl = "~/PresentationLayer/graph/edit.gif"
        editcol.ShowEditButton = True
        partView.Columns.Add(editcol)
        'partView.DataBind()
        DisplayGridHeader()
        lblNoRecord.Visible = False


        '''access control'''''''''''''''''''''''''''''''''''''''''''''''
        Dim accessgroup As String = Session("accessgroup").ToString
        Dim purviewArray As Array = New Boolean() {False, False, False, False}
        purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "31")

        editcol.Visible = purviewArray(2)


    End Sub

    Sub DisplayGridHeader()
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        If partView.Rows.Count <> 0 Then
            partView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-01")
            partView.HeaderRow.Cells(1).Font.Size = 8
            partView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-INVOICENO")
            partView.HeaderRow.Cells(2).Font.Size = 8
            partView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-05")
            partView.HeaderRow.Cells(3).Font.Size = 8
            partView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-04")
            partView.HeaderRow.Cells(4).Font.Size = 8
            partView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-06")
            partView.HeaderRow.Cells(5).Font.Size = 8
            partView.HeaderRow.Cells(6).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-07")
            partView.HeaderRow.Cells(6).Font.Size = 8
            partView.HeaderRow.Cells(7).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-CR-09")
            partView.HeaderRow.Cells(7).Font.Size = 8

            partView.Visible = True
            lblNoRecord.Visible = False
        Else
            lblNoRecord.Visible = True
        End If
    End Sub
    Protected Sub searchbtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles searchbtn.Click

        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Dim objServiceBillPart12 As New clsupdateservice2()
        Dim lstrStartDate, lstrEndDate As String

        lstrStartDate = Me.txtStartDate.Text
        lstrEndDate = Me.txtEndDate.Text

        objServiceBillPart12.SOID = soText.Text
        objServiceBillPart12.CustomerName = CNText.Text
        objServiceBillPart12.PhoneNo = phnoText.Text
        objServiceBillPart12.SerialNo = snText.Text
        'Amended by KSLim on 18/6/2006
        objServiceBillPart12.UserID = Session("userID").ToString()
        'Amended by KSLim on 18/6/2006
        If (ctryStatDrop.Text = "AL") Then
            objServiceBillPart12.STATUS = ""
        Else
            objServiceBillPart12.STATUS = ctryStatDrop.Text
        End If
        ' objServiceBillPart12.STATUS = ctryStatDrop.Text     '5.16增加
        objServiceBillPart12.StartBillDate = lstrStartDate
        objServiceBillPart12.EndBillDate = lstrEndDate

        objServiceBillPart12.CustomerID = Me.txtCustomerID.Text
        objServiceBillPart12.CustomerPrefix = Session("login_ctryid")
        objServiceBillPart12.AppointmentPrefix = Me.txtAppointmentPrefix.Text
        objServiceBillPart12.AppointmentNo = Me.txtAppointmentNo.Text
        objServiceBillPart12.ManualServiceBillNo = Me.txtManualServiceBillNo.Text

        If Not IsDate(lstrStartDate) And lstrStartDate <> "" Then
            partView.Visible = False
            Exit Sub
        End If

        If Not IsDate(lstrEndDate) And lstrEndDate <> "" Then
            partView.Visible = False
            Exit Sub
        End If


        Dim selDS As DataSet = objServiceBillPart12.SelectAllServiceBillPart2()

        If Not selDS Is Nothing Then
            partView.DataSource = selDS
            partView.DataBind()
       
        End If

        DisplayGridHeader()
    End Sub
    Protected Sub partView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles partView.PageIndexChanging
        partView.PageIndex = e.NewPageIndex

        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        'Dim partall As DataSet = Session("partView")
        Dim objServiceBillPart12 As New clsupdateservice2()
        Dim lstrStartDate, lstrEndDate As String

        lstrStartDate = Me.txtStartDate.Text
        lstrEndDate = Me.txtEndDate.Text

        objServiceBillPart12.SOID = soText.Text
        objServiceBillPart12.CustomerName = CNText.Text
        objServiceBillPart12.PhoneNo = phnoText.Text
        objServiceBillPart12.SerialNo = snText.Text
        'Amended by KSLim on 18/6/2006
        objServiceBillPart12.UserID = Session("userID").ToString()
        'Amended by KSLim on 18/6/2006
        If (ctryStatDrop.Text = "AL") Then
            objServiceBillPart12.STATUS = ""
        Else
            objServiceBillPart12.STATUS = ctryStatDrop.Text
        End If
        '5.16增加
        objServiceBillPart12.StartBillDate = lstrStartDate
        objServiceBillPart12.EndBillDate = lstrEndDate

        objServiceBillPart12.CustomerID = Me.txtCustomerID.Text
        objServiceBillPart12.CustomerPrefix = Session("login_ctryid")
        objServiceBillPart12.AppointmentPrefix = Me.txtAppointmentPrefix.Text
        objServiceBillPart12.AppointmentNo = Me.txtAppointmentNo.Text
        objServiceBillPart12.ManualServiceBillNo = Me.txtManualServiceBillNo.Text


        If Not IsDate(lstrStartDate) And lstrStartDate <> "" Then
            partView.Visible = False
            Exit Sub
        End If

        If Not IsDate(lstrEndDate) And lstrEndDate <> "" Then
            partView.Visible = False
            Exit Sub
        End If

        Dim selDS As DataSet = objServiceBillPart12.SelectAllServiceBillPart2()

        partView.DataSource = selDS
        partView.DataBind()
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        DisplayGridHeader()
    End Sub

    Protected Sub partView_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles partView.RowEditing
        'Dim ds As DataSet = Session("partView")
        'Dim type As String = ds.Tables(0).Rows(partView.PageIndex * partView.PageSize + e.NewEditIndex).Item(0).ToString()
        Dim type As String = partView.Rows(e.NewEditIndex).Cells(1).Text
        type = Server.UrlEncode(type)
        'Dim no As String = ds.Tables(0).Rows(partView.PageIndex * partView.PageSize + e.NewEditIndex).Item(1).ToString
        Dim NO As String = partView.Rows(e.NewEditIndex).Cells(2).Text
        no = Server.UrlEncode(no)
        Dim strTempURL As String = "type=" + type + "&no=" + no
        'strTempURL = "~/PresentationLayer/Function/CRUpdate.aspx?" + strTempURL
        strTempURL = "~/PresentationLayer/Function/ServiceBillUpdate2.aspx?" + strTempURL
        Response.Redirect(strTempURL)
    End Sub

End Class
