Imports BusinessEntity
Imports System.Data
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Windows
Imports Microsoft.Win32

Partial Class PresentationLayer_function_appointment_rms_aspx
    Inherits System.Web.UI.Page
    Dim objXmlTr As New clsXml
    Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)
        Return connection
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.MaintainScrollPositionOnPostBack = True
        If Not Page.IsPostBack Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try

            Dim UserID As String = Session("userID")
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle


            Dim CommonClass As New clsCommonClass()
            If Session("login_rank") <> 0 Then
                CommonClass.spctr = Session("login_ctryID").ToString().ToUpper
            End If
            CommonClass.rank = Session("login_rank")

            Dim userIDNamestr As String = Session("userID").ToString().ToUpper + "-" + Session("username").ToString.ToUpper
            Dim rank As String = Session("login_rank")
            CommonClass.spstat = Session("login_cmpID")
            CommonClass.sparea = Session("login_svcID")

            If rank <> 0 Then
                CommonClass.spctr = Session("login_ctryID").ToString().ToUpper
            End If
            CommonClass.rank = rank
            CommonClass.spstat = "ACTIVE"

            lblTotalDescription.Visible = False
            BindData()
            BindGrid(UserID)

        ElseIf appsView.Rows.Count > 0 Then
            DispGridViewHead()
        End If
        HypCal.NavigateUrl = "javascript:DoCal(document.form1.txtPreInsDateFr);"
        HypCal2.NavigateUrl = "javascript:DoCal(document.form1.txtPreInsDateTo);"

        searchButton.Enabled = True
        searchButton.Visible = True
    End Sub
    Protected Sub BindGrid(ByVal strUserID As String)
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")

        appsView.AllowPaging = True
        appsView.AllowSorting = True
        'Dim editcol As New CommandField
        'editcol.EditText = objXmlTr.GetLabelName("EngLabelMsg", "BB-Edit") '"edit"
        'editcol.HeaderImageUrl = "~/PresentationLayer/graph/edit.gif"
        'editcol.ShowEditButton = True
        'appsView.Columns.Add(editcol)

        lblNoRecord.Visible = False

        'access control'''''''''''''''''''''''''''''''''''''''''''''''
        Dim accessgroup As String = Session("accessgroup").ToString
        Dim purviewArray As Array = New Boolean() {False, False, False, False}
        purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "22")

        'editcol.Visible = purviewArray(2)


        If txtPreInsDateFr.Text = "" Then
            txtPreInsDateFr.Text = System.DateTime.Today
        End If

        If txtPreInsDateTo.Text = "" Then
            txtPreInsDateTo.Text = System.DateTime.Today
        End If
    End Sub
    Private Sub DispGridViewHead()
        appsView.HeaderRow.Cells(0).Text = "Status"
        appsView.HeaderRow.Cells(0).Font.Size = 8
        appsView.HeaderRow.Cells(1).Text = "Order"
        appsView.HeaderRow.Cells(1).Font.Size = 8
        appsView.HeaderRow.Cells(2).Text = "EKInvDate"
        appsView.HeaderRow.Cells(2).Font.Size = 8
        appsView.HeaderRow.Cells(3).Text = "EKInvNo"
        appsView.HeaderRow.Cells(3).Font.Size = 8
        appsView.HeaderRow.Cells(4).Text = "ProdCode"
        appsView.HeaderRow.Cells(4).Font.Size = 8
        appsView.HeaderRow.Cells(5).Text = "ProdName"
        appsView.HeaderRow.Cells(5).Font.Size = 8
        appsView.HeaderRow.Cells(6).Text = "SerialNo"
        appsView.HeaderRow.Cells(6).Font.Size = 8
        appsView.HeaderRow.Cells(7).Text = "Receipient"
        appsView.HeaderRow.Cells(7).Font.Size = 8
        appsView.HeaderRow.Cells(8).Text = "ContactNo"
        appsView.HeaderRow.Cells(8).Font.Size = 8
        appsView.HeaderRow.Cells(9).Text = "Address"
        appsView.HeaderRow.Cells(9).Font.Size = 8
        appsView.HeaderRow.Cells(10).Text = "PostCode"
        appsView.HeaderRow.Cells(10).Font.Size = 8
        appsView.HeaderRow.Cells(11).Text = "Area"
        appsView.HeaderRow.Cells(11).Font.Size = 8
        appsView.HeaderRow.Cells(12).Text = "State"
        appsView.HeaderRow.Cells(12).Font.Size = 8
        appsView.HeaderRow.Cells(13).Text = "ServiceCenter"
        appsView.HeaderRow.Cells(13).Font.Size = 8
    End Sub

    Protected Sub searchButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles searchButton.Click
        Dim UserID As String = Session("userID")
        Dim objCommonTel As New clsCommonClass
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        txtPreInsDateFr.Text = Request.Form("txtPreInsDateFr")
        txtPreInsDateTo.Text = Request.Form("txtPreInsDateTo")
        'search condition

        Dim Conn As New SqlConnection
        Conn.ConnectionString = ConfigurationManager.AppSettings("ConnectionString").ToString

        Using Comm As New SqlClient.SqlCommand("" _
            & "exec dbo.RMStoBB '" + txtContactNumber.Text + "','" + txtOrderNumFr.Text + "','" + txtOrderNumTo.Text + "','" + txtPreInsDateFr.Text + "','" + txtPreInsDateTo.Text + "','" + ddlSVCFr.SelectedValue + "','" + ddlSVCTo.SelectedValue + "','" + ddlStateFr.SelectedValue + "','" + ddlStateTo.SelectedValue + "','" + ddlAreaFr.SelectedValue + "','" + ddlAreaTo.SelectedValue + "'", Conn)
            Conn.Open()

            Try
                Using sda As New SqlDataAdapter
                    sda.SelectCommand = Comm

                    If sda Is Nothing Then
                        appsView.Visible = False
                        lblNoRecord.Visible = True
                        Return
                    Else
                        Using selds As New DataSet
                            sda.Fill(selds)
                            appsView.Visible = True
                            appsView.DataSource = selds
                            appsView.DataBind()
                            DispGridViewHead()
                            If selds.Tables(0).Rows.Count > 0 Then
                                lblNoRecord.Visible = False
                                DispGridViewHead()
                                LblTotRecNo.Text = selds.Tables(0).Rows.Count
                            Else
                                LblTotRecNo.Text = "0"
                            End If
                            Session("selds") = selds
                        End Using
                    End If
                End Using
            Catch ex As Exception
                Return
            End Try
        End Using
        Conn.Close()
    End Sub
    Protected Sub appsView_PageIndexChanging(ByVal sender As Object, ByVal e As GridViewPageEventArgs) Handles appsView.PageIndexChanging
        appsView.PageIndex = e.NewPageIndex
        Threading.Thread.CurrentThread.CurrentCulture = datestyle

        If Session("selds") IsNot Nothing Then
            Dim ds As DataSet = CType(Session("selds"), DataSet)
            'Dim ds As DataSet = Session("appsView")
            appsView.DataSource = ds
            appsView.DataBind()
            If ds.Tables(0).Rows.Count > 0 Then
                DispGridViewHead()
            End If
            txtPreInsDateFr.Text = Request.Form("apptStartDateBox")
            txtPreInsDateTo.Text = Request.Form("apptEndDateBox")
        End If

    End Sub

    Protected Sub appsView_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles appsView.RowEditing

        'Dim transtype As String = appsView.Rows(e.NewEditIndex).Cells(1).Text.Trim()
        'transtype = Server.UrlEncode(transtype)
        'Dim transno As String = appsView.Rows(e.NewEditIndex).Cells(2).Text.Trim()
        'transno = Server.UrlEncode(transno)
        'Dim strTempURL As String = "transtype=" + transtype + "&transno=" + transno
        'strTempURL = "~/PresentationLayer/function/modifyappointment.aspx?" + strTempURL
        'Response.Redirect(strTempURL)
    End Sub

    Protected Sub appsView_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles appsView.SelectedIndexChanged

    End Sub

    Private Sub BindData()
        Dim Conn As New SqlConnection
        Conn.ConnectionString = ConfigurationManager.AppSettings("ConnectionString").ToString

        ddlStateFr.Items.Clear()
        ddlStateTo.Items.Clear()
        ddlAreaFr.Items.Clear()
        ddlAreaTo.Items.Clear()

        If ddlSVCFr.SelectedValue = "" Then
            Dim Comm2 As SqlCommand
            Comm2 = Conn.CreateCommand
            Comm2.CommandText = "Select distinct isnull(ServiceCenter,'') [ServiceCenter] from dbo.RMS_Appt order by ServiceCenter"
            Conn.Open()

            Dim dr2 As SqlDataReader = Comm2.ExecuteReader(CommandBehavior.CloseConnection)

            Dim dt2 As DataTable = New DataTable()
            dt2.Load(dr2)

            Dim counter2 As Integer

            For counter2 = 0 To dt2.Rows.Count - 1
                ddlSVCFr.Items.Add(dt2.Rows(counter2).Item("ServiceCenter").ToString)
                ddlSVCTo.Items.Add(dt2.Rows(counter2).Item("ServiceCenter").ToString)
            Next
            Conn.Close()

            ddlSVCFr.SelectedIndex = 0
            ddlSVCTo.SelectedIndex = ddlSVCTo.Items.Count - 1
        End If

        If ddlStateFr.SelectedValue = "" Then
            Dim Comm2 As SqlCommand
            Comm2 = Conn.CreateCommand
            Comm2.CommandText = "Select distinct [State] from dbo.RMS_Appt where ServiceCenter >= '" + ddlSVCFr.SelectedValue + "' and ServiceCenter <= '" + ddlSVCTo.SelectedValue + "' order by State "
            Conn.Open()

            Dim dr2 As SqlDataReader = Comm2.ExecuteReader(CommandBehavior.CloseConnection)

            Dim dt2 As DataTable = New DataTable()
            dt2.Load(dr2)

            Dim counter2 As Integer

            For counter2 = 0 To dt2.Rows.Count - 1
                ddlStateFr.Items.Add(dt2.Rows(counter2).Item("State").ToString)
                ddlStateTo.Items.Add(dt2.Rows(counter2).Item("State").ToString)
            Next
            Conn.Close()

            ddlStateFr.SelectedIndex = 0
            ddlStateTo.SelectedIndex = ddlStateTo.Items.Count - 1
        End If

        If ddlAreaFr.SelectedValue = "" Then
            Dim Comm2 As SqlCommand
            Comm2 = Conn.CreateCommand
            Comm2.CommandText = "Select distinct Area from dbo.RMS_Appt where [State] >= '" + ddlStateFr.SelectedValue + "' and [State] <= '" + ddlStateTo.SelectedValue + "' order by Area "
            Conn.Open()

            Dim dr2 As SqlDataReader = Comm2.ExecuteReader(CommandBehavior.CloseConnection)

            Dim dt2 As DataTable = New DataTable()
            dt2.Load(dr2)

            Dim counter2 As Integer

            For counter2 = 0 To dt2.Rows.Count - 1
                ddlAreaFr.Items.Add(dt2.Rows(counter2).Item("Area").ToString)
                ddlAreaTo.Items.Add(dt2.Rows(counter2).Item("Area").ToString)
            Next
            Conn.Close()

            ddlAreaFr.SelectedIndex = 0
            ddlAreaTo.SelectedIndex = ddlAreaTo.Items.Count - 1
        End If

        'If ddlPreSess.SelectedValue = "" Then
        '    Dim Comm2 As SqlCommand
        '    Comm2 = Conn.CreateCommand
        '    Comm2.CommandText = "Select distinct isnull(PreferredSession,'') [PreferredSession] from dbo.RMS_Appt " _
        '    & "where ServiceCenter >= '" + ddlSVCFr.SelectedValue + "' and ServiceCenter <= '" + ddlSVCTo.SelectedValue + "'" _
        '    & "and [State] >= '" + ddlStateFr.SelectedValue + "' and [State] <= '" + ddlStateTo.SelectedValue + "'" _
        '    & "and [Area] >= '" + ddlAreaFr.SelectedValue + "' and [Area] <= '" + ddlAreaTo.SelectedValue + "'"
        '    Conn.Open()

        '    Dim dr2 As SqlDataReader = Comm2.ExecuteReader(CommandBehavior.CloseConnection)

        '    Dim dt2 As DataTable = New DataTable()
        '    dt2.Load(dr2)

        '    Dim counter2 As Integer

        '    For counter2 = 0 To dt2.Rows.Count - 1
        '        ddlPreSess.Items.Add(dt2.Rows(counter2).Item("PreferredSession").ToString)
        '    Next
        '    Conn.Close()

        'End If
    End Sub

    Protected Sub ddlSVCFr_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSVCFr.SelectedIndexChanged
        BindData()
    End Sub

    Protected Sub ddlSVCTo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSVCTo.SelectedIndexChanged
        BindData()
    End Sub

    Protected Sub ddlStateFr_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStateFr.SelectedIndexChanged
        Dim Conn As New SqlConnection
        Conn.ConnectionString = ConfigurationManager.AppSettings("ConnectionString").ToString

        ddlAreaFr.Items.Clear()
        ddlAreaTo.Items.Clear()


        If ddlAreaFr.SelectedValue = "" Then
            Dim Comm2 As SqlCommand
            Comm2 = Conn.CreateCommand
            Comm2.CommandText = "Select distinct Area from dbo.RMS_Appt where [State] >= '" + ddlStateFr.SelectedValue + "' and [State] <= '" + ddlStateTo.SelectedValue + "' order by Area "
            Conn.Open()

            Dim dr2 As SqlDataReader = Comm2.ExecuteReader(CommandBehavior.CloseConnection)

            Dim dt2 As DataTable = New DataTable()
            dt2.Load(dr2)

            Dim counter2 As Integer

            For counter2 = 0 To dt2.Rows.Count - 1
                ddlAreaFr.Items.Add(dt2.Rows(counter2).Item("Area").ToString)
                ddlAreaTo.Items.Add(dt2.Rows(counter2).Item("Area").ToString)
            Next
            Conn.Close()

            ddlAreaFr.SelectedIndex = 0
            ddlAreaTo.SelectedIndex = ddlAreaTo.Items.Count - 1

        End If
    End Sub

    Protected Sub ddlStateTo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStateTo.SelectedIndexChanged
        Dim Conn As New SqlConnection
        Conn.ConnectionString = ConfigurationManager.AppSettings("ConnectionString").ToString

        ddlAreaFr.Items.Clear()
        ddlAreaTo.Items.Clear()


        If ddlAreaFr.SelectedValue = "" Then
            Dim Comm2 As SqlCommand
            Comm2 = Conn.CreateCommand
            Comm2.CommandText = "Select distinct Area from dbo.RMS_Appt where [State] >= '" + ddlStateFr.SelectedValue + "' and [State] <= '" + ddlStateTo.SelectedValue + "' order by Area "
            Conn.Open()

            Dim dr2 As SqlDataReader = Comm2.ExecuteReader(CommandBehavior.CloseConnection)

            Dim dt2 As DataTable = New DataTable()
            dt2.Load(dr2)

            Dim counter2 As Integer

            For counter2 = 0 To dt2.Rows.Count - 1
                ddlAreaFr.Items.Add(dt2.Rows(counter2).Item("Area").ToString)
                ddlAreaTo.Items.Add(dt2.Rows(counter2).Item("Area").ToString)
            Next
            Conn.Close()

            ddlAreaFr.SelectedIndex = 0
            ddlAreaTo.SelectedIndex = ddlAreaTo.Items.Count - 1


        End If
    End Sub
    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub
    Protected Sub BtnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnExport.Click
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=GridView1.xls")
        Response.Charset = ""
        Response.ContentType = "application/vnd.xls"
        Dim StringWriter As IO.StringWriter = New System.IO.StringWriter()
        Dim HtmlTextWriter As New HtmlTextWriter(StringWriter)
        Dim style As String = "<style> td { mso-number-format:\@; } </style> "

        appsView.AllowPaging = False
        appsView.DataSource = Session("selds")
        appsView.DataBind()

        appsView.RenderControl(HtmlTextWriter)
        Response.Write(style) 'style is added dynamically
        Response.Write(StringWriter.ToString())
        Response.[End]()
    End Sub

    Protected Sub ddlAreaFr_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAreaFr.SelectedIndexChanged
        'Dim Conn As New SqlConnection
        'Conn.ConnectionString = ConfigurationManager.AppSettings("ConnectionString").ToString

        'If ddlPreSess.SelectedValue = "" Then
        '    Dim Comm2 As SqlCommand
        '    Comm2 = Conn.CreateCommand
        '    Comm2.CommandText = "Select distinct isnull(PreferredSession,'') [PreferredSession] from dbo.RMS_Appt " _
        '    & "where ServiceCenter >= '" + ddlSVCFr.SelectedValue + "' and ServiceCenter <= '" + ddlSVCTo.SelectedValue + "'" _
        '    & "and [State] >= '" + ddlStateFr.SelectedValue + "' and [State] <= '" + ddlStateTo.SelectedValue + "'" _
        '    & "and [Area] >= '" + ddlAreaFr.SelectedValue + "' and [Area] <= '" + ddlAreaTo.SelectedValue + "'"
        '    Conn.Open()

        '    Dim dr2 As SqlDataReader = Comm2.ExecuteReader(CommandBehavior.CloseConnection)

        '    Dim dt2 As DataTable = New DataTable()
        '    dt2.Load(dr2)

        '    Dim counter2 As Integer

        '    For counter2 = 0 To dt2.Rows.Count - 1
        '        ddlPreSess.Items.Add(dt2.Rows(counter2).Item("PreferredSession").ToString)
        '    Next
        '    Conn.Close()

        'End If
    End Sub

    Protected Sub ddlAreaTo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAreaTo.SelectedIndexChanged
        'Dim Conn As New SqlConnection
        'Conn.ConnectionString = ConfigurationManager.AppSettings("ConnectionString").ToString

        'ddlPreSess.Items.Clear()

        'If ddlPreSess.SelectedValue = "" Then
        '    Dim Comm2 As SqlCommand
        '    Comm2 = Conn.CreateCommand
        '    Comm2.CommandText = "Select distinct isnull(PreferredSession,'') [PreferredSession] from dbo.RMS_Appt " _
        '    & "where ServiceCenter >= '" + ddlSVCFr.SelectedValue + "' and ServiceCenter <= '" + ddlSVCTo.SelectedValue + "'" _
        '    & "and [State] >= '" + ddlStateFr.SelectedValue + "' and [State] <= '" + ddlStateTo.SelectedValue + "'" _
        '    & "and [Area] >= '" + ddlAreaFr.SelectedValue + "' and [Area] <= '" + ddlAreaTo.SelectedValue + "'"
        '    Conn.Open()

        '    Dim dr2 As SqlDataReader = Comm2.ExecuteReader(CommandBehavior.CloseConnection)

        '    Dim dt2 As DataTable = New DataTable()
        '    dt2.Load(dr2)

        '    Dim counter2 As Integer

        '    For counter2 = 0 To dt2.Rows.Count - 1
        '        ddlPreSess.Items.Add(dt2.Rows(counter2).Item("PreferredSession").ToString)
        '    Next
        '    Conn.Close()

        'End If
    End Sub
End Class
