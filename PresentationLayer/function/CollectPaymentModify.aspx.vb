Imports BusinessEntity
Imports System.Configuration
Imports System.Xml
Imports System.Globalization
Imports SQLDataAccess
Imports System.Data.SqlClient
Imports System.Web
Imports System.Data

Partial Class PresentationLayer_function_CollectPaymentModify
    Inherits System.Web.UI.Page

    Private dsItem As New DataSet
    Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
    Dim ConType As String
    Dim _totalDebit As Decimal = 0
    Dim _totalCredit As Decimal = 0


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        AjaxPro.Utility.RegisterTypeForAjax(GetType(PresentationLayer_function_CollectPaymentModify))
        Page.MaintainScrollPositionOnPostBack = True

        Try
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try

        If Not IsPostBack Then
            InitializePage()
        End If

        '''access control'''''''''''''''''''''''''''''''''''''''''''''''
        Dim accessgroup As String = Session("accessgroup").ToString
        Dim purviewArray As Array = New Boolean() {False, False, False, False}
        purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "31")

        Me.lnkSave.Enabled = purviewArray(1)


        'HypCal.NavigateUrl = "javascript:DoCal(document.form1.txtDate);"
        HypCal1.NavigateUrl = "javascript:DoCal(document.form1.txtPaidDate);"
        'HypCal2.NavigateUrl = "javascript:DoCal(document.form1.txtStartDate);"

    End Sub

    Protected Sub lnkSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSave.Click
        lblError.Visible = False
        RequestFromValue()
        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")

        Dim msginfo As String = objXm.GetLabelName("StatusMessage", "SAVEORNOT")
        Dim msgtitle As String = objXm.GetLabelName("StatusMessage", "SAVETITLE")
        MessageBox1.Confirm(msginfo)

        ViewState("ServiceOrderDate") = txtDate.Text
        ViewState("TechnicianValue") = cboTechnician.SelectedValue
        ViewState("ServiceCenterIndex") = cboServiceCenter.SelectedValue

        SaveCollectPayment()

    End Sub

    Protected Sub lnkCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCancel.Click
        Response.Redirect("UpdatePaymentCollection.Aspx")

    End Sub

    Protected Sub lnkAddItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddItem.Click
        lblError.Visible = False
        RequestFromValue()
        Dim valid As Boolean = False
        valid = ValidateDetails()
        If valid = True Then AddItems()
        InitializeGridColumns()
    End Sub

    Protected Sub RequestFromValue()

        Dim rcvPaymentDate As String = String.Empty
        Dim rcvPaymentAmt As String = String.Empty
        Dim rcvPaymentMode As String = String.Empty
        Dim rcvPaymentRefNo As String = String.Empty

        If txtPaidDate.Text = "" Then

        Else

        End If

        If tbPaidAmount.Text = "" Then

        End If



    End Sub

    Public Sub AddItems()
        Try
            RefreshDataSet()

            Dim drItem As DataRow
            Dim RowNo As Integer = dsItem.Tables("PaymentList").Rows.Count
            drItem = dsItem.Tables("PaymentList").NewRow()

            drItem(0) = RowNo + 1
            drItem.Item(0) = Me.txtPaidDate.Text
            drItem.Item(1) = Me.cboPaymentMode.SelectedItem.Text()
            drItem.Item(2) = String.Format("{0:f2}", Val(Me.tbPaidAmount.Text))
            drItem.Item(3) = UCase(Me.tbRefNo.Text)
            drItem.Item(4) = Me.cboPaymentMode.SelectedItem.Value

            dsItem.Tables("PaymentList").Rows.Add(drItem)
            Me.grdNewPayment.DataSource = dsItem
            Me.grdNewPayment.DataBind()
            InitializeNewPaymentGridColumns()
        Catch

        End Try

    End Sub

    Protected Sub MessageBox1_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox1.GetMessageBoxResponse
        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
            SaveCollectPayment()
        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
            Exit Sub
        End If
        txtDate.Text = Request.Form("txtDate")

    End Sub


    Protected Sub grdNewPayment_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdNewPayment.RowDataBound
        e.Row.Cells(5).Visible = False
    End Sub

    Protected Sub grdNewPayment_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdNewPayment.RowDeleting

        Try
            RefreshDataSet()
            dsItem.Tables("PaymentList").Rows(e.RowIndex).Delete()
            Me.grdNewPayment.DataSource = dsItem
            Me.grdNewPayment.DataBind()
            InitializeNewPaymentGridColumns()
            InitializeGridColumns()
            RefreshDataSet()
            txtDate.Text = Request.Form("txtDate")
        Catch

        End Try
    End Sub

    Protected Sub grdPaymentCollectionDetails_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdPaymentCollectionDetails.RowDeleting

        lblError.Visible = False
        Dim paymentRefNo As String = grdPaymentDetails.DataKeys(e.RowIndex).Values("refno").ToString()
        If paymentRefNo = "" Then ' cannot proceed to delete
            Me.lblError.Text = " Failed to delete."
            MessageBox1.Alert(lblError.Text)
            lblError.Visible = True
        Else

            Dim CR As String
            Dim clsColPayment As New clsupdatepaymentcollection

            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")

            clsColPayment.TransactionNo = Me.txtInvoiceNo.Text
            clsColPayment.TransactionPrefix = Me.txtInvoicePf.Text
            clsColPayment.ServiceBillNo = txtServiceBillNo.Text
            clsColPayment.ServiceType = Me.cboServiceBillType.SelectedValue
            clsColPayment.Technician = Me.cboTechnician.SelectedValue
            clsColPayment.ServiceType = Me.cboServiceType.SelectedValue
            clsColPayment.AppointmentPrefix = Me.txtAppointmentPrefix.Text
            clsColPayment.AppointmentNo = Me.txtAppointmentNo.Text
            clsColPayment.CustomerPrefix = Me.txtCustomerPrefix.Text
            clsColPayment.CustomerID = Me.txtCustomerID.Text
            clsColPayment.CustomerName = Me.txtCustomer.Text
            clsColPayment.STATUS = Me.cboStatus.SelectedValue
            clsColPayment.ROUnitID = Me.cboRO.SelectedValue
            clsColPayment.RefNo = paymentRefNo

            RefreshDataSet()
            clsColPayment.CRDetails = dsItem

            CR = clsColPayment.DeleteColPaymentDetails()  ' delete payment detail

            If CR <> "-1" Then
                Me.lnkAddItem.Enabled = True
                Me.lnkSave.Enabled = True

                Me.txtInvoiceNo.Text = clsColPayment.TransactionNo
                Me.txtInvoicePf.Text = clsColPayment.TransactionPrefix

                'Me.lblError.Text = objXmlTr.GetLabelName("StatusMessage", "SBA-SUCSAVING")  'delete successfully
                Me.lblError.Text = "Collection record has been deleted successfully!"
                Me.MessageBox1.Alert(lblError.Text)
                Me.lblError.Visible = True

                'Me.cboServiceBillType.Enabled = False
                'Me.txtServiceBillNo.Enabled = False
                'Me.cboTechnician.Enabled = False
                'Me.cboServiceType.Enabled = False
                'Me.btnSearchAppNo.Enabled = False
                'Me.lnkAddItem.Enabled = False

                'Me.cboRO.Enabled = False
                'Me.cboCompany.Enabled = False
                'Me.cboServiceCenter.Enabled = False
                'Me.grdNewPayment.Enabled = False

                PopulateAfterSave()
            Else
                Me.lblError.Text = " Failed to delete."
                MessageBox1.Alert(lblError.Text)
                lblError.Visible = True
            End If

        End If

    End Sub

    Protected Sub grdPaymentCollectionDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdPaymentCollectionDetails.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then

            _totalDebit += (DataBinder.Eval(e.Row.DataItem, "debitAmt"))
            _totalCredit += (DataBinder.Eval(e.Row.DataItem, "creditAmt"))
        ElseIf e.Row.RowType = DataControlRowType.Footer Then

            ' for the Footer, display the running totals
            e.Row.Cells(6).Text = "Totals:" + _totalDebit.ToString("c")
            e.Row.Cells(5).Text = "Totals:" + _totalCredit.ToString("c")

            e.Row.Cells(6).HorizontalAlign = HorizontalAlign.Center
            e.Row.Cells(5).HorizontalAlign = HorizontalAlign.Center
            e.Row.Font.Bold = True
        End If

    End Sub

    Protected Sub grdPaymentCollectionDetails_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdPaymentCollectionDetails.RowEditing

        'Set the edit index.
        grdPaymentCollectionDetails.EditIndex = e.NewEditIndex
        'Bind data to the GridView control.
        PopulateAfterSave()
    End Sub

    Protected Sub grdPaymentCollectionDetails_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles grdPaymentCollectionDetails.RowCancelingEdit
        'Reset the edit index.
        grdPaymentCollectionDetails.EditIndex = -1
        PopulateAfterSave()
    End Sub

    Protected Sub grdPaymentDetails_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles grdPaymentDetails.RowCancelingEdit

    End Sub

    Protected Sub grdPaymentDetails_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdPaymentDetails.RowDeleting

        lblError.Visible = False
        Dim paymentRefNo As String = grdPaymentDetails.DataKeys(e.RowIndex).Values("refno").ToString()
        If paymentRefNo = "" Then ' cannot proceed to delete
            Me.lblError.Text = " Failed to delete."
            MessageBox1.Alert(lblError.Text)
            lblError.Visible = True
        Else

            Dim CR As String
            Dim clsColPayment As New clsupdatepaymentcollection

            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")

            clsColPayment.TransactionNo = Me.txtInvoiceNo.Text
            clsColPayment.TransactionPrefix = Me.txtInvoicePf.Text
            clsColPayment.ServiceBillNo = txtServiceBillNo.Text
            clsColPayment.ServiceType = Me.cboServiceBillType.SelectedValue
            clsColPayment.Technician = Me.cboTechnician.SelectedValue
            clsColPayment.ServiceType = Me.cboServiceType.SelectedValue
            clsColPayment.AppointmentPrefix = Me.txtAppointmentPrefix.Text
            clsColPayment.AppointmentNo = Me.txtAppointmentNo.Text
            clsColPayment.CustomerPrefix = Me.txtCustomerPrefix.Text
            clsColPayment.CustomerID = Me.txtCustomerID.Text
            clsColPayment.CustomerName = Me.txtCustomer.Text
            clsColPayment.STATUS = Me.cboStatus.SelectedValue
            clsColPayment.ROUnitID = Me.cboRO.SelectedValue
            clsColPayment.RefNo = paymentRefNo

            RefreshDataSet()
            clsColPayment.CRDetails = dsItem

            CR = clsColPayment.DeleteColPaymentDetails()  ' delete payment detail

            If CR <> "-1" Then
                Me.lnkAddItem.Enabled = True
                Me.lnkSave.Enabled = True

                Me.txtInvoiceNo.Text = clsColPayment.TransactionNo
                Me.txtInvoicePf.Text = clsColPayment.TransactionPrefix

                'Me.lblError.Text = objXmlTr.GetLabelName("StatusMessage", "SBA-SUCSAVING")  'delete successfully
                Me.lblError.Text = "Collection record has been deleted successfully!"
                Me.MessageBox1.Alert(lblError.Text)
                Me.lblError.Visible = True

                'Me.cboServiceBillType.Enabled = False
                'Me.txtServiceBillNo.Enabled = False
                'Me.cboTechnician.Enabled = False
                'Me.cboServiceType.Enabled = False
                'Me.btnSearchAppNo.Enabled = False
                'Me.lnkAddItem.Enabled = False

                'Me.cboRO.Enabled = False
                'Me.cboCompany.Enabled = False
                'Me.cboServiceCenter.Enabled = False
                'Me.grdNewPayment.Enabled = False

                PopulateAfterSave()
            Else
                Me.lblError.Text = " Failed to delete."
                MessageBox1.Alert(lblError.Text)
                lblError.Visible = True
            End If

        End If
       
    End Sub

    Protected Sub grdPaymentDetails_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdPaymentDetails.RowEditing

        grdPaymentDetails.EditIndex = e.NewEditIndex
        PopulateAfterSave()

    End Sub

    Protected Sub grdPaymentDetails_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles grdPaymentDetails.RowUpdating

    End Sub

    Protected Sub grdPaymentDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdPaymentDetails.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim refno As String = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "refno"))
            
            Dim rowIndex As Int16 = e.Row.RowIndex
            If rowIndex = 0 And refno = "" Then

            Else
                Dim lnkbtnresult As Button = DirectCast(e.Row.FindControl("ButtonDelete"), Button)
                If lnkbtnresult IsNot Nothing Then
                    lnkbtnresult.Attributes.Add("onclick", (Convert.ToString("javascript:return deleteConfirm('") & refno) + "')")
                End If
            End If
        End If


    End Sub

    Protected Sub grdPaymentDetails_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdPaymentDetails.RowCommand

    End Sub

#Region "PRIVATE FUNCTIONS"

    Private Sub databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList, Optional ByVal X As Boolean = True)
        dropdown.Items.Clear()
        Dim row As DataRow

        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            If X = True Then
                NewItem.Text = Trim(row("id") & "-" & row("name"))
            Else
                NewItem.Text = Trim(row("name"))
            End If
            NewItem.Value = Trim(row("id"))
            dropdown.Items.Add(NewItem)
        Next

    End Sub

    Private Sub InitializeGridColumns()
        Try
            'get column labels
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")


            Me.grdPaymentCollectionDetails.AllowPaging = True
            Me.grdPaymentCollectionDetails.AllowSorting = True

            Me.grdPaymentCollectionDetails.HeaderRow.Cells(1).Text = "Payment Date"
            Me.grdPaymentCollectionDetails.HeaderRow.Cells(1).Font.Size = 8

            Me.grdPaymentCollectionDetails.HeaderRow.Cells(2).Text = "Service Bill No"
            Me.grdPaymentCollectionDetails.HeaderRow.Cells(2).Font.Size = 8

            Me.grdPaymentCollectionDetails.HeaderRow.Cells(3).Text = "Ref No"
            Me.grdPaymentCollectionDetails.HeaderRow.Cells(3).Font.Size = 8

            Me.grdPaymentCollectionDetails.HeaderRow.Cells(4).Text = "Payment Mode"
            Me.grdPaymentCollectionDetails.HeaderRow.Cells(4).Font.Size = 8

            Me.grdPaymentCollectionDetails.HeaderRow.Cells(5).Text = "Debit"
            Me.grdPaymentCollectionDetails.HeaderRow.Cells(5).Font.Size = 8

            Me.grdPaymentCollectionDetails.HeaderRow.Cells(6).Text = "Credit"
            Me.grdPaymentCollectionDetails.HeaderRow.Cells(6).Font.Size = 8

        Catch

        End Try

    End Sub

    Private Sub InitializeNewPaymentGridColumns()
        Try
            'get column labels

            Me.grdNewPayment.HeaderRow.Cells(0).Text = "Delete"
            Me.grdNewPayment.HeaderRow.Cells(0).Font.Size = 8

            Me.grdNewPayment.HeaderRow.Cells(1).Text = "Payment Date"
            Me.grdNewPayment.HeaderRow.Cells(1).Font.Size = 8

            Me.grdNewPayment.HeaderRow.Cells(2).Text = "Payment Mode"
            Me.grdNewPayment.HeaderRow.Cells(2).Font.Size = 8

            Me.grdNewPayment.HeaderRow.Cells(3).Text = "Payment Amount"
            Me.grdNewPayment.HeaderRow.Cells(3).Font.Size = 8

            Me.grdNewPayment.HeaderRow.Cells(4).Text = "Ref No"
            Me.grdNewPayment.HeaderRow.Cells(4).Font.Size = 8

        Catch

        End Try

    End Sub

    Private Sub InitializePage()
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")

        '*******************************************************************
        ' BEGIN INITIALIZE LABELS AND VALIDATORS
        '*******************************************************************
        '************
        ' LABELS    
        '************
        Me.lblAppoinmentNo.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-APPOINMENTNO")
        Me.lblCompany.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-COMPANY")
        Me.lblCountry.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-COUNTRY")
        Me.lblCustomer.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-CUSTOMER")
        Me.lblCustomerID.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-CUSTOMERID")
        Me.lblDate.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-DATE")
        Me.lblInvoiceNo.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-INVOICENO")
        Me.lblROSerialNo.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-ROSERIALNO")
        Me.lblServiceBillNo.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-SERVICEBILLNO")
        Me.lblServiceBillType.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-SERVICEBILLTYPE")
        Me.lblServiceCenter.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-SERVICECENTER")
        Me.lblServiceType.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-SERVICETYPE")
        Me.lblStatus.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-STATUS")
        Me.lblTechnician.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-TECHNICIAN")

        titleLab.Text = "Update Payment Collection"

        Me.lnkSave.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-SAVE")
        lblNoteUp.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
        lblNoteDown.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")

        '************
        ' VALIDATORS   
        '************

        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        'Me.rfvServiceType.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRSERVICETYPE")
        '*******************************************************************
        ' END INITIALIZE LABELS AND VALIDATORS
        '*******************************************************************


        Dim CommonClass As New clsCommonClass()
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")

        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        Dim userIDNamestr As String = Session("userID").ToString().ToUpper + "-" + Session("username").ToString.ToUpper
        Dim rank As String = Session("login_rank").ToString()
        CommonClass.spstat = Session("login_cmpID").ToString()
        CommonClass.sparea = Session("login_svcID").ToString()

        If rank <> 0 Then
            CommonClass.spctr = Session("login_ctryID").ToString().ToUpper
        End If
        CommonClass.rank = rank
        CommonClass.spstat = "ACTIVE"


        objXmlTr.XmlFile = System.Configuration.ConfigurationSettings.AppSettings("XmlFilePath")

        Dim Util As New clsUtil()
        Dim statParam As ArrayList = New ArrayList

        statParam = Util.searchconfirminf("SERBILLTY")
        Dim count As Integer
        Dim statid As String
        Dim statnm As String
        For count = 0 To statParam.Count - 1
            statid = statParam.Item(count).ToString()
            statnm = statParam.Item(count + 1).ToString()
            count = count + 1
            Me.cboServiceBillType.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
        Next
        'Me.cboServiceBillType.Items.Insert(0, "")

        Dim yesnopar As ArrayList = New ArrayList
        yesnopar = Util.searchconfirminf("YESNO")

        For count = 0 To yesnopar.Count - 1
            statid = yesnopar.Item(count).ToString()
            statnm = yesnopar.Item(count + 1).ToString()
            count += 1
        Next

        statParam = Util.searchconfirminf("SerStat")
        For count = 0 To statParam.Count - 1
            statid = statParam.Item(count).ToString()
            statnm = statParam.Item(count + 1).ToString()
            count = count + 1
            If UCase(statid) = "UM" Or UCase(statid) = "UA" Or UCase(statid) = "BL" Then
                cboStatus.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
            End If
        Next
        cboStatus.Items(0).Selected = True


        Dim dsCountry As New DataSet
        Me.cboCountry.DataTextField = "name"
        Me.cboCountry.DataValueField = "id"
        dsCountry = CommonClass.Getcomidname("BB_MASCTRY_IDNAME")
        If dsCountry.Tables.Count <> 0 Then
            databonds(dsCountry, Me.cboCountry)
        End If
        Me.cboCountry.Items.Insert(0, "")
        Me.cboCountry.SelectedIndex = 0
        Me.cboCountry.SelectedValue = Session("login_ctryID").ToString().ToUpper
        Me.cboCountry.Enabled = False


        Dim dsSerType As New DataSet
        dsSerType = CommonClass.Getcomidname("BB_MASSRCT_IDNAMEALL")
        If dsSerType.Tables.Count <> 0 Then
            databonds(dsSerType, Me.cboServiceType)
        End If
        cboServiceType.Items.Insert(0, "")

        Dim dscompany As New DataSet
        CommonClass.spstat = Session("login_cmpID").ToString()

        dscompany = CommonClass.Getcomidname("BB_MASCOMP_IDNAME")
        If dscompany.Tables.Count <> 0 Then
            databonds(dscompany, Me.cboCompany)
        End If
        Me.cboCompany.Items.Insert(0, "")
        Me.cboCompany.SelectedIndex = 0
        Me.cboCompany.SelectedValue = Session("login_cmpID").ToString()
        Me.cboCompany.Enabled = False

        Dim dsServiceCentere As New DataSet
        CommonClass.spstat = Session("login_cmpID").ToString()

        dsServiceCentere = CommonClass.Getcomidname("BB_MASSVRC_IDNAME")

        If dsServiceCentere.Tables.Count <> 0 Then
            databonds(dsServiceCentere, Me.cboServiceCenter)
        End If
        Me.cboServiceCenter.Items.Insert(0, "")
        Me.cboServiceCenter.SelectedIndex = 0
        Me.cboServiceCenter.SelectedValue = Session("login_svcID").ToString()
        Me.cboServiceCenter.Enabled = False

        Dim dsTechnician As New DataSet
        dsTechnician = CommonClass.Getcomidname("BB_MASTECH_IDNAME")
        If dsTechnician.Tables.Count <> 0 Then
            databonds(dsTechnician, Me.cboTechnician)
        End If
        Me.cboTechnician.Items.Insert(0, "")
        Me.cboTechnician.SelectedIndex = 0
        Me.tbCurrentDebt.Text = Request.QueryString("debt")
        PopulateFromSearch()

    End Sub

    Private Sub PopulateFromSearch()
        '*******************************************************************
        ' BEGIN POPULATE VALUES FROM SEARCH
        '*******************************************************************
        Dim CommonClass As New clsCommonClass()
        Dim clsPaymentCollection As New clsupdatepaymentcollection
        Dim dsPaymentCollection As New DataSet
        Dim drPaymentCollection As DataRow
        Dim dsServiceCentere As New DataSet
        Dim dsTechnician As New DataSet

        Dim rank As String = Session("login_rank").ToString()
        txtInvoiceNo.Text = Request.QueryString("type")
        Dim apptNo = Request.QueryString("no")

        Dim htSvcBill As New Hashtable

        If String.IsNullOrEmpty(apptNo) Then
            htSvcBill = CType(Session("htSvcBill"), Hashtable)
            apptNo = htSvcBill.Item("ApptNo").ToString()
            txtInvoiceNo.Text = htSvcBill.Item("InvoiceNo").ToString()
        End If
        dsPaymentCollection = clsPaymentCollection.selds(txtInvoiceNo.Text, "INV")

        Me.cboServiceCenter.Enabled = False
        Me.cboStatus.Enabled = False
        Me.cboRO.Enabled = False

        If dsPaymentCollection.Tables.Count <> 0 Then
            If dsPaymentCollection.Tables(1).Rows.Count > 0 Then

                grdPaymentCollectionDetails.DataSource = dsPaymentCollection.Tables(1)
                grdPaymentCollectionDetails.DataBind()
                grdPaymentDetails.DataSource = dsPaymentCollection.Tables(1)
                grdPaymentDetails.DataBind()

                InitializeGridColumns()
            End If


            If dsPaymentCollection.Tables(0).Rows.Count > 0 Then
                drPaymentCollection = dsPaymentCollection.Tables(0).Rows(0)
                CommonClass.rank = rank
                CommonClass.sparea = drPaymentCollection.Item(2).ToString()
                CommonClass.spstat = drPaymentCollection.Item(3).ToString()
                CommonClass.spctr = drPaymentCollection.Item(4).ToString()

                Me.txtInvoicePf.Text = drPaymentCollection.Item(0).ToString()
                Me.cboServiceCenter.SelectedValue = drPaymentCollection.Item(2).ToString()
                Me.cboCompany.SelectedValue = drPaymentCollection.Item(3).ToString()
                Me.cboCountry.SelectedValue = drPaymentCollection.Item(4).ToString()
                Me.txtServiceBillNo.Text = drPaymentCollection.Item(5).ToString()
                Me.cboServiceBillType.SelectedValue = drPaymentCollection.Item(6).ToString()
                Me.txtDate.Text = Format(drPaymentCollection.Item(7), "dd/MM/yyyy")
                Me.cboTechnician.SelectedValue = drPaymentCollection.Item(8).ToString()
                Me.cboServiceType.SelectedValue = Trim(drPaymentCollection.Item(9).ToString()).ToString()
                Me.cboRO.SelectedValue = drPaymentCollection.Item(10).ToString.Trim
                Me.txtCustomerPrefix.Text = drPaymentCollection.Item(11).ToString()
                Me.txtCustomerID.Text = drPaymentCollection.Item(12).ToString()
                Me.cboStatus.SelectedValue = drPaymentCollection.Item(14).ToString()
                If UCase(cboStatus.SelectedValue) = "UA" Then
                    cboServiceType.Enabled = False
                    cboRO.Enabled = False
                    lnkSave.Enabled = False
                    cboStatus.Enabled = False
                End If

                txtAppointmentPrefix.Text = drPaymentCollection.Item(15).ToString()
                txtAppointmentNo.Text = drPaymentCollection.Item(16).ToString()
                Me.txtCustomer.Text = drPaymentCollection.Item(19).ToString()
                Me.tbTotalCredit.Text = drPaymentCollection.Item(20).ToString()
                Me.tbCurrentDebt.Text = drPaymentCollection.Item(21).ToString()

            End If

            If Session("htSvcBill") Is Nothing Then
                Exit Sub
            End If
            htSvcBill = CType(Session("htSvcBill"), Hashtable)

            If Trim(UCase(txtCustomerID.Text)) <> Trim(UCase(htSvcBill.Item("CustomerNo")).ToString()) Then
                txtCustomerPrefix.Text = UCase(htSvcBill.Item("CustomerPrefix")).ToString()
                txtCustomerID.Text = htSvcBill.Item("CustomerNo").ToString()
                txtCustomer.Text = UCase(htSvcBill.Item("Customer")).ToString()
                cboRO.SelectedValue = Trim(htSvcBill.Item("ROUID").ToString())
            End If
            cboServiceType.SelectedValue = Trim(htSvcBill.Item("ServiceType").ToString())

        End If
        Session("htSvcBill") = Nothing
        '*******************************************************************
        ' END POPULATE VALUES FROM SEARCH
        '*******************************************************************

    End Sub

    Private Sub PopulateAfterSave()

        Dim CommonClass As New clsCommonClass()
        Dim clsPaymentCollection As New clsupdatepaymentcollection
        Dim dsPaymentCollection As New DataSet
        Dim drPaymentCollection As DataRow
        Dim dsServiceCentere As New DataSet
        Dim dsTechnician As New DataSet

        Dim rank As String = Session("login_rank").ToString()
        txtInvoiceNo.Text = Request.QueryString("type")
        Dim apptNo = Request.QueryString("no")

        Dim htSvcBill As New Hashtable

        If String.IsNullOrEmpty(apptNo) Then
            htSvcBill = CType(Session("htSvcBill"), Hashtable)
            apptNo = htSvcBill.Item("ApptNo").ToString()
            txtInvoiceNo.Text = htSvcBill.Item("InvoiceNo").ToString()
        End If
        dsPaymentCollection = clsPaymentCollection.selds(txtInvoiceNo.Text, "INV")

        Me.cboServiceCenter.Enabled = False
        Me.cboStatus.Enabled = False
        Me.cboRO.Enabled = False

        If dsPaymentCollection.Tables.Count <> 0 Then
            If dsPaymentCollection.Tables(1).Rows.Count > 0 Then
                grdPaymentCollectionDetails.DataSource = dsPaymentCollection.Tables(1)
                grdPaymentCollectionDetails.DataBind()
                grdPaymentDetails.DataSource = dsPaymentCollection.Tables(1)
                grdPaymentDetails.DataBind()
                InitializeGridColumns()
            End If


            If dsPaymentCollection.Tables(0).Rows.Count > 0 Then
                drPaymentCollection = dsPaymentCollection.Tables(0).Rows(0)
                CommonClass.rank = rank
                CommonClass.sparea = drPaymentCollection.Item(2).ToString()
                CommonClass.spstat = drPaymentCollection.Item(3).ToString()
                CommonClass.spctr = drPaymentCollection.Item(4).ToString()

                Me.txtInvoicePf.Text = drPaymentCollection.Item(0).ToString()
                Me.cboServiceCenter.SelectedValue = drPaymentCollection.Item(2).ToString()
                Me.cboCompany.SelectedValue = drPaymentCollection.Item(3).ToString()
                Me.cboCountry.SelectedValue = drPaymentCollection.Item(4).ToString()
                Me.txtServiceBillNo.Text = drPaymentCollection.Item(5).ToString()
                Me.cboServiceBillType.SelectedValue = drPaymentCollection.Item(6).ToString()
                Me.txtDate.Text = Format(drPaymentCollection.Item(7), "dd/MM/yyyy")
                Me.cboTechnician.SelectedValue = drPaymentCollection.Item(8).ToString()
                Me.cboServiceType.SelectedValue = Trim(drPaymentCollection.Item(9).ToString()).ToString()
                Me.cboRO.SelectedValue = drPaymentCollection.Item(10).ToString.Trim
                Me.txtCustomerPrefix.Text = drPaymentCollection.Item(11).ToString()
                Me.txtCustomerID.Text = drPaymentCollection.Item(12).ToString()
                Me.cboStatus.SelectedValue = drPaymentCollection.Item(14).ToString()
                If UCase(cboStatus.SelectedValue) = "UA" Then
                    cboServiceType.Enabled = False
                    cboRO.Enabled = False
                    lnkSave.Enabled = False
                    cboStatus.Enabled = False
                End If

                txtAppointmentPrefix.Text = drPaymentCollection.Item(15).ToString()
                txtAppointmentNo.Text = drPaymentCollection.Item(16).ToString()
                Me.txtCustomer.Text = drPaymentCollection.Item(19).ToString()
                Me.tbTotalCredit.Text = drPaymentCollection.Item(20).ToString()
                Me.tbCurrentDebt.Text = drPaymentCollection.Item(21).ToString()

            End If

            If Session("htSvcBill") Is Nothing Then
                Exit Sub
            End If
            htSvcBill = CType(Session("htSvcBill"), Hashtable)

            If Trim(UCase(txtCustomerID.Text)) <> Trim(UCase(htSvcBill.Item("CustomerNo")).ToString()) Then
                txtCustomerPrefix.Text = UCase(htSvcBill.Item("CustomerPrefix")).ToString()
                txtCustomerID.Text = htSvcBill.Item("CustomerNo").ToString()
                txtCustomer.Text = UCase(htSvcBill.Item("Customer")).ToString()
                cboRO.SelectedValue = Trim(htSvcBill.Item("ROUID").ToString())
            End If
            cboServiceType.SelectedValue = Trim(htSvcBill.Item("ServiceType").ToString())

        End If
        Session("htSvcBill") = Nothing

    End Sub

    Private Sub RetainValuesBeforeSearch()
        Try
            Dim htSvcBill As New Hashtable
            htSvcBill.Item("ApptNo") = txtAppointmentNo.Text
            htSvcBill.Item("InvoiceNo") = Me.txtInvoiceNo.Text
            htSvcBill.Item("ServiceType") = Me.cboServiceType.SelectedValue
            htSvcBill.Item("CustomerPrefix") = Me.txtCustomerPrefix.Text
            htSvcBill.Item("CustomerNo") = Me.txtCustomerID.Text
            htSvcBill.Item("CustomerName") = Me.txtCustomer.Text
            htSvcBill.Item("ROUID") = cboRO.SelectedValue

            htSvcBill.Item("SBUNO") = 2
            htSvcBill.Item("Company") = Me.cboCompany.SelectedValue
            htSvcBill.Item("ServiceCenter") = Me.cboServiceCenter.SelectedValue

            Session("htSvcBill") = htSvcBill
        Catch

        End Try

    End Sub

    Private Sub SaveCollectPayment()
        Try
            Dim CR As String
            Dim clsColPayment As New clsupdatepaymentcollection

            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")

            clsColPayment.TransactionNo = Me.txtInvoiceNo.Text
            clsColPayment.TransactionPrefix = Me.txtInvoicePf.Text
            clsColPayment.ServiceBillNo = txtServiceBillNo.Text
            clsColPayment.ServiceType = Me.cboServiceBillType.SelectedValue
            clsColPayment.Technician = Me.cboTechnician.SelectedValue
            clsColPayment.ServiceType = Me.cboServiceType.SelectedValue
            clsColPayment.AppointmentPrefix = Me.txtAppointmentPrefix.Text
            clsColPayment.AppointmentNo = Me.txtAppointmentNo.Text
            clsColPayment.CustomerPrefix = Me.txtCustomerPrefix.Text
            clsColPayment.CustomerID = Me.txtCustomerID.Text
            clsColPayment.CustomerName = Me.txtCustomer.Text
            clsColPayment.STATUS = Me.cboStatus.SelectedValue
            clsColPayment.ROUnitID = Me.cboRO.SelectedValue

            RefreshDataSet()
            clsColPayment.CRDetails = dsItem

            CR = clsColPayment.SaveColPaymentDetails()

            If CR <> "-1" Then
                Me.lnkAddItem.Enabled = True
                Me.lnkSave.Enabled = True
                dsItem.Clear()
                Me.grdNewPayment.DataSource = dsItem
                Me.grdNewPayment.DataBind()

                Me.txtInvoiceNo.Text = clsColPayment.TransactionNo
                Me.txtInvoicePf.Text = clsColPayment.TransactionPrefix

                Me.lblError.Text = objXmlTr.GetLabelName("StatusMessage", "SBA-SUCSAVING")
                Me.MessageBox1.Alert(lblError.Text)
                Me.lblError.Visible = True

                Me.cboPaymentMode.Text = ""
                Me.tbPaidAmount.Text = ""
                Me.tbRefNo.Text = ""

                'Me.cboServiceBillType.Enabled = False
                'Me.txtServiceBillNo.Enabled = False
                'Me.cboTechnician.Enabled = False
                'Me.cboServiceType.Enabled = False
                'Me.btnSearchAppNo.Enabled = False
                'Me.lnkAddItem.Enabled = False

                'Me.cboRO.Enabled = False
                'Me.cboCompany.Enabled = False
                'Me.cboServiceCenter.Enabled = False

                PopulateAfterSave()

            Else
                Me.lblError.Text = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRSAVING")
                Dim lstrError As String = Me.lblError.Text
                Me.MessageBox1.Alert(lstrError)
                Me.lblError.Visible = True
                InitializeGridColumns()
            End If
        Catch

        End Try

    End Sub

    Private Sub RefreshDataSet()
        Try
            dsItem.Clear()

            If dsItem.Tables.Count = 0 Then
                dsItem.Tables.Add("PaymentList")
                dsItem.Tables("PaymentList").Columns.Add("Payment Date")
                dsItem.Tables("PaymentList").Columns.Add("Payment Mode")
                dsItem.Tables("PaymentList").Columns.Add("Payment Amount")
                dsItem.Tables("PaymentList").Columns.Add("Ref No")
                dsItem.Tables("PaymentList").Columns.Add("Payment Mode Value")
            End If


            Dim intGrdRow As Integer = 0
            Dim drItem As DataRow

            While intGrdRow <= Me.grdNewPayment.Rows.Count - 1
                drItem = dsItem.Tables("PaymentList").NewRow()
                drItem.Item(0) = Me.grdNewPayment.Rows(intGrdRow).Cells(1).Text
                drItem.Item(1) = Me.grdNewPayment.Rows(intGrdRow).Cells(2).Text
                drItem.Item(2) = String.Format("{0:f2}", Val(Me.grdNewPayment.Rows(intGrdRow).Cells(3).Text))
                drItem.Item(3) = Me.grdNewPayment.Rows(intGrdRow).Cells(4).Text
                drItem.Item(4) = Me.grdNewPayment.Rows(intGrdRow).Cells(5).Text

                dsItem.Tables("PaymentList").Rows.Add(drItem)
                intGrdRow = intGrdRow + 1
            End While
        Catch

        End Try

    End Sub

    Private Function ValidateDetails() As Boolean
        Try
            ValidateDetails = False

            If Trim(Me.txtPaidDate.Text) = "" Then
                lblError.Text = "Payment Date is required."
                MessageBox1.Alert(lblError.Text)
                txtPaidDate.Focus()
                Exit Function
            End If
            If Trim(Me.tbPaidAmount.Text) = "" Then
                lblError.Text = "Payment Amount is required."
                MessageBox1.Alert(lblError.Text)
                tbPaidAmount.Focus()
                Exit Function
            End If

            If Trim(Me.tbRefNo.Text) = "" Then
                lblError.Text = "Ref No is required."
                MessageBox1.Alert(lblError.Text)
                tbRefNo.Focus()
                Exit Function
            End If


            Me.tbPaidAmount.Text = String.Format("{0:f2}", Val(Me.tbPaidAmount.Text))
            lblError.Visible = False
            Return True

        Catch

        End Try

    End Function

#End Region

    
   
End Class
