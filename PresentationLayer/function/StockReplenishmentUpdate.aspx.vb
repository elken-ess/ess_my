Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Partial Class PresentationLayer_function_StockReplenishmentUpdate
    Inherits System.Web.UI.Page
    Private ZoneEntity As clsZone
    Private stdstocklist As clsStdStockList

    Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'If txtPKLDate.Text.Trim = "" Then
            'txtPKLDate.Text = Request.Form("txtPKLDate")
            'End If

            Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
            If (Not Page.IsPostBack) Then
                Dim script As String = "top.location='../logon.aspx';"
                Try
                    If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                        Return
                        ' Response.Redirect("~/PresentationLayer/logon.aspx")
                    End If
                Catch ex As Exception
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                    ' Response.Redirect("~/PresentationLayer/logon.aspx")
                End Try
                HypCal.NavigateUrl = "javascript:DoCal(document.form1.txtPKLDate);"
                BindGrid()
            End If
        Catch

        End Try
        If txtPKLDate.Text.Trim = "" Then
            'txtPKLDate.Text = System.DateTime.Today
        Else
            txtPKLDate.Text = Request.Form("txtPKLDate")
        End If
    End Sub
    Protected Sub BindGrid()

        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        titleLab1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-STOCK-01")
        ServiceCntLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-04")
        PKLDocLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-STOCK-06")
        PKLDateLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-STOCK-05")
        ShowCPLLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-STOCK-09")
        ShowOTDLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-STOCK-10")
        statuslab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-STOCK-12")
        ShTechLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-STOCK-11")
        lnkSearch.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Search")
        '--------------------------------------------------------------------
        '��ʾ״̬
        '----------------------------------------------------------------------
        Dim cntryStat As New clsUtil()
        Dim statParam As ArrayList = New ArrayList
        statParam = cntryStat.searchconfirminf("STATUS")
        Dim count As Integer
        Dim statid As String
        Dim statnm As String
        cboStatus.Items.Add(New ListItem("All", ""))
        For count = 0 To statParam.Count - 1
            statid = statParam.Item(count)
            statnm = statParam.Item(count + 1)
            count = count + 1
            If (Not statid.Equals("DELETE")) And (Not statid.Equals("OBSOLETE")) Then
                cboStatus.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
            End If
        Next

        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        cboStatus.Items.Add(New ListItem(objXmlTr.GetLabelName("StatusMessage", "COMPLETE"), "COMPLETE"))
        cboStatus.SelectedIndex = 1
        
        '---------------------------------------------------------------------------
        'show cancel packinglist  and  show only today packinglist 
        '-------------------------------------------------------------------------
        
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        ShowCPLList.Items.Add("")
        ShowCPLList.Items.Add(objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-STOCK-23"))
        ShowCPLList.Items.Add(objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-STOCK-24"))
        'ShowOTDList.Items.Add("")
        ShowOTDList.Items.Add(objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-STOCK-23"))
        ShowOTDList.Items.Add(objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-STOCK-24"))
        ShowOTDList.SelectedIndex = 1


        'If txtPKLDate.Text.Trim = "" Then
        '    txtPKLDate.Text = System.DateTime.Today
        'End If

        '---------------------------------------------
        'show technician dropdown list 
        '---------------------------------------------
        'Dim technic As New clsStockupdate()
        'technic.userid = Session("userID")
        'Dim ds As New DataSet
        'ds = technic.technician1()
        'Dim i As Integer
        'ShTechList.Items.Add("")
        'For i = 0 To ds.Tables(0).Rows.Count - 1
        ' ShTechList.Items.Add(ds.Tables(0).Rows(i).Item(0).ToString)
        'Next
        'ShTechList.Items(0).Selected = True

        '----------------------------------------------
        'show service center dropdown list
        '----------------------------------------------
        ZoneEntity = New clsZone()
        ZoneEntity.UserID = Session("userID")
        Dim SerCentInfo As DataSet = ZoneEntity.GetSerCent()
        If SerCentInfo Is Nothing Then
            Response.Redirect("~/PresentationLayer/error.aspx")
        End If
        cboServiceCenter.Items.Add("")
        Dim intCtr As Integer
        For intCtr = 0 To SerCentInfo.Tables(0).Rows.Count - 1
            Dim NewItem As New ListItem()
            NewItem.Text = Trim(SerCentInfo.Tables(0).Rows(intCtr).Item("MSVC_SVCID")) & "-" & Trim(SerCentInfo.Tables(0).Rows(intCtr).Item("MSVC_ENAME"))
            NewItem.Value = Trim(SerCentInfo.Tables(0).Rows(intCtr).Item("MSVC_SVCID"))
            cboServiceCenter.Items.Add(NewItem)
        Next
        cboServiceCenter.SelectedValue = Session("LOGIN_SVCID")

        Dim technic As New clsStockupdate()
        Me.cboTechnician.DataSource = technic.technician1(Me.cboServiceCenter.SelectedValue, Session("UserID"))
        Me.cboTechnician.DataTextField = "MTCH_ENAME"
        Me.cboTechnician.DataValueField = "MTCH_TCHID"
        Me.cboTechnician.DataBind()
        Me.cboTechnician.Items.Insert(0, New ListItem("", ""))
        If Me.cboTechnician.Items.Count < 2 Then
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            Me.errlab.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-F-Help4")
            Me.errlab.Visible = True
        Else
            Me.errlab.Text = ""
            Me.errlab.Visible = False
        End If

        '---------------------------------------------------------------------------
        '��ʾgrid view 
        '--------------------------------------
        Dim PPTEntity As New clsStockupdate()
        PPTEntity.ServiceCenterID = ""
        PPTEntity.PKLDocNO = ""
        PPTEntity.PKLDate = ""
        PPTEntity.cancelPl = ""
        PPTEntity.OnlyToday = ""
        PPTEntity.STATUS = cboStatus.SelectedValue
        PPTEntity.technician = ""
        PPTEntity._fstrUserID = Session("UserID")
        Dim partall As DataSet = PPTEntity.view()
        PPTGrid.DataSource = partall
        'Session("partView") = partall
        Me.PPTGrid.PageSize = 10
        PPTGrid.AllowPaging = True
        PPTGrid.DataBind()
        PRTgridview()


       


    End Sub
    Protected Sub PRTgridview()
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")

        If PPTGrid.Rows.Count > 0 Then
            PPTGrid.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-STOCK-03")
            PPTGrid.HeaderRow.Cells(1).Font.Size = 8
            PPTGrid.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-STOCK-06")
            PPTGrid.HeaderRow.Cells(2).Font.Size = 8
            PPTGrid.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-STOCK-05")
            PPTGrid.HeaderRow.Cells(3).Font.Size = 8
            PPTGrid.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-STOCK-04")
            PPTGrid.HeaderRow.Cells(4).Font.Size = 8
            PPTGrid.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-STOCK-07")
            PPTGrid.HeaderRow.Cells(5).Font.Size = 8
            PPTGrid.HeaderRow.Cells(6).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-STOCK-08")
            PPTGrid.HeaderRow.Cells(6).Font.Size = 8
            PPTGrid.HeaderRow.Cells(7).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB_FNCSTKR_COLLECTDATE")
            PPTGrid.HeaderRow.Cells(7).Font.Size = 8

            PPTGrid.HeaderRow.Cells(8).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-STOCK-27")
            PPTGrid.HeaderRow.Cells(8).Font.Size = 8
            PPTGrid.HeaderRow.Cells(9).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-STOCK-26")
            PPTGrid.HeaderRow.Cells(9).Font.Size = 8
            PPTGrid.HeaderRow.Cells(10).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-STOCK-12")
            PPTGrid.HeaderRow.Cells(10).Font.Size = 8
            
        End If
    End Sub

    Protected Sub SearchBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSearch.Click
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Dim partEntity As New clsStockupdate()
        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")

        'If txtPKLDate.Text.Trim = "" Then
        txtPKLDate.Text = Request.Form("txtPKLDate")
        'End If
        'If (Trim(ServiceCntBox.Text) <> "") Then
        partEntity.ServiceCenterID = Trim(cboServiceCenter.Text)
        'End If
        'If (Trim(PKLDocBox.Text) <> "") Then
        partEntity.PKLDocNO = Trim(txtPKLNo.Text)
        'End If
        'If (Trim(PKLDateBox.Text) <> "") Then
        partEntity.PKLDate = Trim(txtPKLDate.Text)
        'End If

        'If (ShowCPLList.Text.ToString() <> "") Then
        partEntity.cancelPl = ShowCPLList.SelectedItem.ToString()
        'End If
        'If (ShowOTDList.Text.ToString() <> "") Then
        partEntity.OnlyToday = ShowOTDList.SelectedItem.ToString()
        'End If
        'If (ShTechList.Text.ToString() <> "") Then
        partEntity.technician = cboTechnician.SelectedValue.ToString()

        'End If
        'If (statuslist.SelectedValue().ToString() <> "") Then
        partEntity.STATUS = cboStatus.SelectedValue.ToString()
        'End If
        partEntity._fstrUserID = Session("UserID")
        Dim selDS As New DataSet
        selDS = partEntity.view()
        If selDS Is Nothing Then

            errlab.Text = objXm.GetLabelName("StatusMessage", "NoData")
            errlab.Visible = True
            Return
        End If

        PPTGrid.DataSource = selDS
        PPTGrid.DataBind()
        'Session("partView") = selDS
        errlab.Visible = False
        If selDS.Tables.Count <= 0 Then

            errlab.Text = objXm.GetLabelName("StatusMessage", "NoData")
            errlab.Visible = True
            Return
        Else
            If selDS.Tables(0).Rows.Count > 0 Then
                PRTgridview()
            Else

                errlab.Text = objXm.GetLabelName("StatusMessage", "NoData")
                errlab.Visible = True
                Return
            End If

        End If


        


    End Sub

    Protected Sub PPTGrid_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles PPTGrid.DataBound
        Dim objXml As New clsXml

        objXml.XmlFile = ConfigurationSettings.AppSettings("StatMsg")



        If PPTGrid.Rows.Count >= 1 Then


            Dim i As Integer
            For i = 0 To PPTGrid.Rows.Count - 1

                Dim accessgroup As String = Session("accessgroup").ToString
                Dim purviewArray As Array = New Boolean() {False, False, False, False}
                purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "32")

                If purviewArray(2) = False Then
                    Me.PPTGrid.Rows(i).Cells(0).Text = ""
                End If


                PPTGrid.Rows(i).Cells(10).Text = IIf(PPTGrid.Rows(i).Cells(10).Text = "ACTIVE", objXml.GetLabelName("StatusMessage", "ACTIVE"), objXml.GetLabelName("StatusMessage", "CANCEL"))
            Next
        End If
    End Sub


    Protected Sub PPTGrid_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles PPTGrid.PageIndexChanging
        'Dim ds As DataSet = Session("partView")
        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim partEntity As New clsStockupdate()

        txtPKLDate.Text = Request.Form("txtPKLDate")

        partEntity.ServiceCenterID = Trim(cboServiceCenter.Text)
        'End If
        'If (Trim(PKLDocBox.Text) <> "") Then
        partEntity.PKLDocNO = Trim(txtPKLNo.Text)
        'End If
        'If (Trim(PKLDateBox.Text) <> "") Then
        partEntity.PKLDate = Trim(txtPKLDate.Text)
        'End If

        'If (ShowCPLList.Text.ToString() <> "") Then
        partEntity.cancelPl = ShowCPLList.SelectedItem.ToString()
        'End If
        'If (ShowOTDList.Text.ToString() <> "") Then
        partEntity.OnlyToday = ShowOTDList.SelectedItem.ToString()
        'End If
        'If (ShTechList.Text.ToString() <> "") Then
        partEntity.technician = cboTechnician.SelectedValue.ToString()

        'End If
        'If (statuslist.SelectedValue().ToString() <> "") Then
        partEntity.STATUS = cboStatus.SelectedValue.ToString()
        'End If
        partEntity._fstrUserID = Session("UserID")
        Dim selDS As New DataSet
        selDS = partEntity.view()
        If selDS Is Nothing Then

            errlab.Text = objXm.GetLabelName("StatusMessage", "NoData")
            errlab.Visible = True
            Return
        End If

        PPTGrid.PageIndex = e.NewPageIndex
        PPTGrid.DataSource = selDS
        PPTGrid.DataBind()
        PRTgridview()

    End Sub

    Protected Sub JCalendar1_SelectedDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles JCalendar1.SelectedDateChanged
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        txtPKLDate.Text = JCalendar1.SelectedDate
    End Sub


    Protected Sub ServiceCntBox_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboServiceCenter.SelectedIndexChanged
        Dim objXmlTr As New clsXml()
        Dim technic As New clsStockupdate()
        Try

            If Session.Contents("stdstocklist") Is Nothing Then
                Me.stdstocklist = New clsStdStockList(Session.Contents("userID"))
                Me.stdstocklist.IPaddr = Request.UserHostAddress
                Me.stdstocklist.SessionID = Now.Year.ToString() + _
                                            Now.Month.ToString() + _
                                            Now.Day.ToString() + _
                                            Now.Hour.ToString() + _
                                            Now.Minute.ToString() + _
                                            Now.Second.ToString() + _
                                            Now.Millisecond.ToString()
                Session.Contents("stdstocklist") = Me.stdstocklist
            Else
                Me.stdstocklist = Session.Contents("stdstocklist")
            End If

            If Me.stdstocklist Is Nothing Then
                Return
            End If

            Me.cboTechnician.DataSource = technic.technician1(Me.cboServiceCenter.SelectedValue, Session("UserID"))
            Me.cboTechnician.DataTextField = "MTCH_ENAME"
            Me.cboTechnician.DataValueField = "MTCH_TCHID"
            Me.cboTechnician.DataBind()
            Me.cboTechnician.Items.Insert(0, New ListItem("", ""))
            If Me.cboTechnician.Items.Count < 2 Then
                objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                Me.errlab.Text = objXmlTr.GetLabelName("StatusMessage", "BB-FUN-F-Help4")
                Me.errlab.Visible = True
            Else
                Me.errlab.Text = ""
                Me.errlab.Visible = False
            End If
        Catch ex As Exception

        End Try
        txtPKLDate.Text = Request.Form("txtPKLDate")
    End Sub

    Protected Sub PPTGrid_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles PPTGrid.SelectedIndexChanging
        Dim PKLTY As String = PPTGrid.Rows(e.NewSelectedIndex).Cells(1).Text
        Dim PKLNO As String = PPTGrid.Rows(e.NewSelectedIndex).Cells(2).Text

        Dim strTempURL As String = "&pklty=" & PKLTY & "&pklno=" & PKLNO
        strTempURL = "~/PresentationLayer/Function/modifyStockReplenishmentUpdate.aspx?" + strTempURL
        Response.Redirect(strTempURL)
    End Sub
End Class
