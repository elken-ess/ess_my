<%@ Page Language="VB" AutoEventWireup="false" CodeFile="UpdateServiceBillPart1.aspx.vb" Inherits="PresentationLayer_Function_UpdateServiceBillPart1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Update service bill part1</title>
    <meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
    <link href="../css/style.css" type="text/css" rel="stylesheet" />
    <script type="text/javascript" language="JavaScript" src="../js/common.js"></script>
    <style type="text/css">
        a:link { color: #336666; text-decoration: none }
	    body { font-size: 9pt; font-family: "Arial", "Verdana", "Tahoma"; background-color: #ffffff }
	    td { font-size: 9pt; font-family: Arial,Verdana,Tahoma }
	</style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table bgcolor="#b7e6e6" border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td style="background-image: url('../graph/title_bg.gif'); width: 1%; height: 24px;">
                        <img height="24" src="../graph/title1.gif" width="5" alt="title1" /></td>
                    <td style="background-image: url('../graph/title_bg.gif'); width: 100%; height: 24px;" class="style2" >
                        <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
                </tr>
            </table>
        </div>
        <table style="background-color: #b7e6e6; height: 22px" width ="100%">
            <tr style="background-color: #ffffff">
                <td style="width: 10%">
                    <asp:Label ID="CNlab" runat="server" Text="Label" Width="90%"></asp:Label></td>
                <td style="width: 15%">
                    <asp:TextBox ID="CNText" runat="server" Width="90%"></asp:TextBox></td>
                <td style="width: 10%">
                    <asp:Label ID="solab" runat="server" Text="Label" Width="90%"></asp:Label></td>
                <td style="width: 15%">
                    <asp:TextBox ID="soText" runat="server" Width="90%"></asp:TextBox></td>
                <td style="width: 10%">
                    <asp:Label ID="ctryStat" runat="server" Text="Label"></asp:Label></td>
                <td style="width: 15%">
                    <asp:DropDownList ID="ctryStatDrop" runat="server" Width="98%">
                    </asp:DropDownList></td>
                
            </tr>
            <tr bgcolor="#ffffff">
                <td style="width: 10%">
                    <asp:Label ID="phnolab" runat="server" Text="Label" Width="90%"></asp:Label></td>
                <td style="width: 15%">
                    <asp:TextBox ID="phnoText" runat="server" Width="90%"></asp:TextBox></td>
                <td style="width: 10%">
                    <asp:Label ID="snlab" runat="server" Text="Label" Width="90%"></asp:Label></td>
                <td style="width: 15%">
                    <asp:TextBox ID="snText" runat="server" Width="90%"></asp:TextBox></td>
                <td style="width: 10%">
                    <asp:Label ID="lblManualServiceBillNo" runat="server" Text="Service Bill No"></asp:Label></td>
                <td style="width: 15%">
                   <asp:TextBox ID="txtManualServiceBillNo" runat="server" Width="90%" MaxLength ="20"></asp:TextBox></td>
                
            </tr>
            <tr bgcolor="#ffffff">
                <td style="width: 10%; height: 30px;">
                    <asp:Label ID="lblCustomerID" runat="server" Text="Customer ID" Width="90%"></asp:Label></td>
                <td style="width: 15%; height: 30px;">
                    <asp:TextBox ID="txtCustomerID" runat="server" Width="90%"  MaxLength ="20"></asp:TextBox></td>
                <td style="width: 10%; height: 30px;">
                    <asp:Label ID="lblAppointmentNo" runat="server" Text="Appointment No" Width="90%"></asp:Label></td>
                <td style="width: 15%; height: 30px;">
                    <asp:TextBox ID="txtAppointmentPrefix" runat="server" Width="20%" MaxLength ="2"></asp:TextBox>
                    <asp:TextBox ID="txtAppointmentNo" runat="server" Width="70%"  MaxLength ="20"></asp:TextBox>
                    </td>
                <td style="width: 10%; height: 30px;">
                    <asp:Label ID="lblSVC" runat="server" Text="Service Center ID"></asp:Label></td>
                <td style="width: 15%; height: 30px;">
                    <asp:DropDownList ID="SVClist" runat="server" AutoPostBack="True" OnSelectedIndexChanged="SVCList_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td style="width: 10%; height: 8px;">
        <asp:Label ID="lblStartDate" runat="server" Text="Service Order Date from" Width="100%"></asp:Label></td>
                <td style="width: 15%; height: 8px;">
                    <asp:TextBox ID="txtStartDate" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
                    <asp:HyperLink ID="HypCal1" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                        ToolTip="Choose a Date">Choose a Date</asp:HyperLink></td>
                <td style="width: 10%; height: 8px;">
                    <asp:Label ID="lblEndDate" runat="server" Text="Service Order Date to" Width="100%"></asp:Label></td>
                <td style="width: 15%; height: 8px;">
                    <asp:TextBox ID="txtEndDate" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
                    <asp:HyperLink ID="HypCal2" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                        ToolTip="Choose a Date">Choose a Date</asp:HyperLink></td>
                 <td style="width: 10%; height: 30px;">
                    <asp:Label ID="technicianLab" runat="server" Text="Technician"></asp:Label></td>
                <td style="width: 15%; height: 30px;">
                    <asp:DropDownList ID="technicianList" runat="server" Width="98%" AppendDataBoundItems="false">
                    <asp:ListItem Selected="True"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td style="width: 10%; height: 8px">
                    <asp:Label ID="lblServiceBillType" runat="server" Text="Service Bill Type" Width="104px"></asp:Label></td>
                <td style="width: 15%; height: 8px">
                    <asp:DropDownList ID="cboServiceBillType" runat="server" Width="145px">
                        <asp:ListItem Value="WITH" Selected="True">With Appointment</asp:ListItem>
                        <asp:ListItem Value="WITO">Without Appointment</asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 10%; height: 8px">
                    <asp:Label ID="Label1" runat="server" Text="RMS No"></asp:Label></td>
                <td style="width: 15%; height: 8px">
                    <asp:TextBox ID="RMStxt" runat="server" MaxLength="20" Width="90%"></asp:TextBox></td>
                <td style="width: 10%; height: 30px">
                    <asp:LinkButton ID="searchbtn" runat="server">LinkButton</asp:LinkButton></td>
                <td style="width: 15%; height: 30px">
                    <asp:LinkButton ID="LnkAdd" runat="server">LinkButton</asp:LinkButton></td>
            </tr>
            
        </table>
      
        <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
        <br />
        <asp:GridView ID="partView" runat="server" AllowPaging="True" AllowSorting="True"
            Width="100%">
        </asp:GridView>
    </form>
</body>
</html>
