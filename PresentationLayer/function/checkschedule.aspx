﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="checkschedule.aspx.vb" Inherits="PresentationLayer_function_checkSchedule" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Check Schedule</title>
    <link href="../css/style.css" type="text/css" rel="stylesheet">
</head>
<body>
    <form id="form1" runat="server">
        <div style="vertical-align: top">
            <table style="width: 100%" border="0">
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0" width="100%" border="0">
                            <tr>
                                <td width="1%" background="../graph/title_bg.gif">
                                    <img height="24" src="../graph/title1.gif" width="5"></td>
                                <td class="style2" width="98%" background="../graph/title_bg.gif">
                                    <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="right" background="../graph/title_bg.gif" style="width: 1%">
                                    <img height="24" src="../graph/title_2.gif" width="5"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="1" cellpadding="2" bgcolor="#b7e6e6" border="0" style="width: 100%">
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width: 20%">
                                    <asp:Label ID="servCenterLab" runat="server" Text="Label" ></asp:Label>
                                 </td>
                                 <td style="width: 30%">
                                    <asp:TextBox ID="servCenterBox" runat="server" Width="80%" CssClass="textborder"
                                        ReadOnly="True"></asp:TextBox>
                                </td>
                                <td align="right" style="width: 20%">
                                    <asp:Label ID="apptDateLab" runat="server" Text="Label"></asp:Label>
                                 </td>
                                 <td style="width: 30%">
                                    <asp:TextBox ID="apptDateBox" runat="server" Width="80%" CssClass="textborder" ReadOnly="True"></asp:TextBox>
                                </td>
                             </tr>
                             <tr bgcolor="#ffffff">
                                <td align="right" style="width: 20%" >
                                    <asp:Label ID="zoneIDLab" runat="server" Text="Label"></asp:Label>
                                 </td>
                                 <td style="width: 30%">
                                    <asp:TextBox ID="zoneIDBox" runat="server" Width="80%" CssClass="textborder" ReadOnly="True"></asp:TextBox>
                                </td>
                                <td colspan ="2">
                                     <asp:Label ID="lblShow" runat="server" Text="Label"></asp:Label>
                                </td>
                                
                            </tr>
                        </table>
                        <asp:GridView ID="CHKScheduleView" runat="server" Width="100%" AllowPaging="True"
                            AllowSorting="True" Font-Size="Smaller" Style="text-align: right">
                        </asp:GridView>
                    </td>
                </tr>
            </table>
            <asp:Label ID="backLink" runat="server" Text="Label" Style="color: blue; text-decoration: underline" Visible="False"></asp:Label>
            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="javascript:history.back();">Back</asp:HyperLink></div>
    </form>
</body>
</html>
