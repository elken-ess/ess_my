Imports BusinessEntity
Imports System.Configuration
Imports System.Xml
Imports System.Data.SqlClient
Imports System.Data

Partial Class PresentationLayer_function_PartSearch
    Inherits System.Web.UI.Page
    Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then
            Try
                If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End If
            Catch ex As Exception
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End Try
            Session("UrlFrom") = Request.UrlReferrer.AbsoluteUri
            PopulateGrid()
        End If

    End Sub

    Sub PopulateGrid()
        Try

            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            'linkbutton
            LBsearch.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Search")
            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-parttl")

            PartIDlab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCAT-0001")
            Partname.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCAT-0002")
            statusLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPCAT-0016")
            Label1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Maslbleinfo")
            Label1.Visible = False
            Me.addinfo.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-AR")
            Me.addinfo.Visible = False

            'create the dropdownlist
            Dim stat As New clsrlconfirminf()
            Dim param As ArrayList = New ArrayList
            param = stat.searchconfirminf("STATUS")
            Dim count As Integer
            Dim statid As String
            Dim statnm As String
            count = 0
            For count = 0 To param.Count - 1
                statid = param.Item(count)
                statnm = param.Item(count + 1)
                count = count + 1
                ctryStatDrop.ClearSelection()
                If statid.Equals("ACTIVE") Or statid.Equals("OBSOLETE") Then
                    ctryStatDrop.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                End If
            Next
            ctryStatDrop.Items(0).Selected = True

            Dim partEntity As New ClsPartRequirement()
            If (Trim(PartIDBox.Text) <> "") Then
                partEntity.partid = PartIDBox.Text
            End If
            If (Trim(PartnmBox.Text) <> "") Then
                partEntity.partname = PartnmBox.Text
            End If
            If (ctryStatDrop.SelectedItem.Value.ToString() <> "") Then
                partEntity.Status = ctryStatDrop.SelectedItem.Value.ToString()
            End If
            If Trim(PartIDBox.Text) = "" And Trim(PartnmBox.Text) = "" Then
                addinfo.Visible = True
            Else
                addinfo.Visible = False
            End If

            partEntity.ModifiedBy = Session("username")
            partEntity.logcountryid = Session("login_ctryID")
            partEntity.logrank = Session("login_rank")

            Dim accessgroup As String = Session("accessgroup").ToString
            Dim purviewArray As Array = New Boolean() {False, False, False, False}
            purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "06")
            partEntity.Status = "ACTIVE"
            partEntity.obsoleteDate = System.DateTime.Today

            Dim partall As DataSet = partEntity.GetStandardPart()
            If partall.Tables(0).Rows.Count = 0 Then
                Label1.Visible = True
            Else
                Label1.Visible = False
            End If
            If partall.Tables(0).Rows.Count <> 0 Then
                partView.DataSource = partall
                Session("partView") = partall
                partView.AllowPaging = True
                partView.AllowSorting = False
                Dim SelCol As New CommandField
                SelCol.EditText = "Select" 'objXmlTr.GetLabelName("EngLabelMsg", "BB-Edit") '"edit"
                SelCol.HeaderImageUrl = "~/PresentationLayer/graph/edit.gif"
                SelCol.ShowEditButton = True
                partView.Columns.Add(SelCol)
                partView.DataBind()


                DisplayGridHeader()




            End If
        Catch ex As Exception

        End Try

    End Sub

    Sub DisplayGridHeader()
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")

        If partView.Rows.Count <> 0 Then
            partView.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PARTHD0")
            partView.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PARTHD1")
            partView.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PARTHD2")
            partView.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PARTHD3")
            partView.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PARTHD4")
            partView.HeaderRow.Cells(6).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PARTHD5")
            partView.HeaderRow.Cells(7).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PARTHD6")
            partView.HeaderRow.Cells(1).Font.Size = 8
            partView.HeaderRow.Cells(2).Font.Size = 8
            partView.HeaderRow.Cells(3).Font.Size = 8
            partView.HeaderRow.Cells(4).Font.Size = 8
            partView.HeaderRow.Cells(5).Font.Size = 8
            partView.HeaderRow.Cells(6).Font.Size = 8
            partView.HeaderRow.Cells(7).Font.Size = 8
        End If
    End Sub

    Protected Sub LBsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LBsearch.Click
        Try
            Dim partEntity As New ClsPartRequirement()
            Dim objXmlTr As New clsXml

            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
            partView.PageIndex = 0
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            If (Trim(PartIDBox.Text) <> "") Then
                partEntity.partid = PartIDBox.Text
            End If
            If (Trim(PartnmBox.Text) <> "") Then
                partEntity.partname = PartnmBox.Text
            End If
            If (ctryStatDrop.SelectedItem.Value.ToString() <> "") Then
                partEntity.Status = ctryStatDrop.SelectedItem.Value.ToString()
            End If
            If Trim(PartIDBox.Text) = "" And Trim(PartnmBox.Text) = "" Then
                addinfo.Visible = True
            Else
                addinfo.Visible = False
            End If
            partEntity.ModifiedBy = Session("username")
            partEntity.logcountryid = Session("login_ctryID")
            partEntity.logrank = Session("login_rank")
            partEntity.obsoleteDate = System.DateTime.Today

            Dim selDS As DataSet = partEntity.GetStandardPart()
            partView.DataSource = selDS
            partView.DataBind()
            Session("partView") = selDS
            If selDS.Tables(0).Rows.Count = 0 Then
                Label1.Visible = True
            Else
                Label1.Visible = False

            End If

            DisplayGridHeader()
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub partView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles partView.PageIndexChanging
        Try
            partView.PageIndex = e.NewPageIndex
            PopulateGrid()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub partView_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles partView.RowEditing
        'Dim partid As String = Me.partView.Rows(e.NewEditIndex).Cells(1).Text
        'partid = Server.UrlEncode(partid)
        Try
            Dim xHT As New Hashtable
            xHT = Session("xHTSSL")
            xHT.Item("PartCode") = partView.Rows(e.NewEditIndex).Cells(1).Text
            Session("xHTSSL") = xHT
            'Response.Redirect("StdStockList.aspx")
            Dim lstrUrlFrom As String = Session("UrlFrom")
            Session.Remove("UrlFrom")
            Response.Redirect(lstrUrlFrom)
        Catch ex As Exception

        End Try

    End Sub
End Class
