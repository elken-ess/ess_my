Imports BusinessEntity
Imports System.Configuration
Imports System.Windows.Forms
Imports System.Data
Imports System.Data.SqlClient

Partial Class PresentationLayer_function_CustomerLookup
    Inherits System.Web.UI.Page
    Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try

        If (Not Page.IsPostBack) Then
            Try
                BindGrid()
            Catch

            End Try
        End If
    End Sub

    Protected Sub BindGrid()
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        lblCustomerID.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0001")
        lblCustomerName.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0002")
        lblArea.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0003")
        txtContact.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0004")
        Me.lnkBack.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-LOGIN-04")

        lnkSearch.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Search")
        lblTitle.Text = objXmlTr.GetLabelName("EngLabelMsg", "BROCUSTOM")
        Label1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-Maslbleinfo")
        Label1.Visible = False
        Me.lblError.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASMODTY-AR")
        Me.lblError.Visible = False


        Dim userIDNamestr As String = Session("userID").ToString().ToUpper + "-" + Session("username").ToString.ToUpper
        Dim rank As String = Session("login_rank")
        Dim customer As New clsCustomerRecord()
        customer.username = userIDNamestr
        Dim area As New clsCommonClass
        If rank <> "0" Then
            area.spctr = Session("login_ctryID")
            area.spstat = ""
            area.sparea = ""
        End If
        area.rank = rank

        'create  area the dropdownlist
        'Dim area As New clsCommonClass
        Dim areads As New DataSet

        areads = area.Getcomidname("BB_MASAREA_IDNAME")
        If areads.Tables.Count <> 0 Then
            databonds(areads, Me.cboArea)
        End If
        Me.cboArea.Items.Insert(0, "")

        Dim cntryStat As New clsrlconfirminf()
        Dim telepass As New clsCommonClass()

        customer.ModBy = userIDNamestr
        If (Trim(Me.lblContact.Text) <> "") Then
            customer.Contact = telepass.telconvertpass(lblContact.Text.ToUpper())
            customer.Contacttype = "Y"
        Else
            customer.Contacttype = "N"
        End If

        customer.ctryid = Session("login_ctryID")
        customer.compid = Session("Login_cmpid")
        customer.usvcid = Session("login_svcid")
        customer.rank = Session("login_rank")

        'Dim ctryall As DataSet = customer.GetSearchCustomer()
        'grdCustomer.DataSource = ctryall
        'Session("Customer") = ctryall
        grdCustomer.AllowPaging = True
        grdCustomer.AllowSorting = True

        'grdCustomer.DataBind()

        If grdCustomer.Rows.Count = 0 Then
            Label1.Visible = True
        Else
            Label1.Visible = False
        End If
        DisplayGridHeader()

    End Sub

    Sub DisplayGridHeader()
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")

        If grdCustomer.Rows.Count >= 1 Then
            grdCustomer.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0001")
            grdCustomer.HeaderRow.Cells(1).Font.Size = 8
            grdCustomer.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0002")
            grdCustomer.HeaderRow.Cells(2).Font.Size = 8
            grdCustomer.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASADD1Z")
            grdCustomer.HeaderRow.Cells(3).Font.Size = 8
            grdCustomer.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASADD2Z")
            grdCustomer.HeaderRow.Cells(4).Font.Size = 8
            grdCustomer.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASPOCODEZ")
            grdCustomer.HeaderRow.Cells(5).Font.Size = 8
            grdCustomer.HeaderRow.Cells(6).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASSERVICECENTER-0009")
            grdCustomer.HeaderRow.Cells(6).Font.Size = 8
            grdCustomer.HeaderRow.Cells(7).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0005")
            grdCustomer.HeaderRow.Cells(7).Font.Size = 8
            grdCustomer.HeaderRow.Cells(8).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-ModelID")
            grdCustomer.HeaderRow.Cells(8).Font.Size = 8
            grdCustomer.HeaderRow.Cells(9).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-Country")
            grdCustomer.HeaderRow.Cells(9).Font.Size = 8
            grdCustomer.HeaderRow.Cells(10).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASCUSTOMER-0008")
            grdCustomer.HeaderRow.Cells(10).Font.Size = 8
            grdCustomer.HeaderRow.Cells(11).Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-MASROUM-0004")
            grdCustomer.HeaderRow.Cells(11).Font.Size = 8

        End If
    End Sub


    Protected Sub search_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSearch.Click
        Dim CustomerEntity As New clsCustomerRecord()
        Dim telepass As New clsCommonClass()

        If (Trim(Me.txtCustomerID.Text) <> "") Then

            CustomerEntity.CustomerID = txtCustomerID.Text.ToUpper()

        End If

        If (Trim(Me.txtCustomerName.Text) <> "") Then

            CustomerEntity.CustomerName = txtCustomerName.Text.ToUpper()

        End If

        If (Me.cboArea.SelectedValue().ToString() <> "") Then
            CustomerEntity.AreaID = Me.cboArea.SelectedValue().ToString().ToUpper()
        End If



        CustomerEntity.RoSerialNo = "%"

        If (Trim(Me.lblContact.Text) <> "") Then

            CustomerEntity.Contact = telepass.telconvertpass(lblContact.Text.ToUpper())
            CustomerEntity.Contacttype = "Y"
        Else
            CustomerEntity.Contacttype = "N"
        End If
        CustomerEntity.ctryid = Session("login_ctryID")
        CustomerEntity.compid = Session("Login_cmpid")
        CustomerEntity.usvcid = Session("login_svcID")
        CustomerEntity.rank = Session("login_rank")

        CustomerEntity.ModBy = Session("userID").ToString().ToUpper

        Dim ctryall As DataSet = CustomerEntity.GetSearchCustomer()
        grdCustomer.DataSource = ctryall
        Session("Customer") = ctryall
        'CustomerEntity.TORS = ctryall.Tables(0).Rows.Count()
        grdCustomer.DataBind()
        If Me.txtCustomerID.Text = "" And Me.txtCustomerName.Text = "" And Me.lblContact.Text = "" And Me.cboArea.Text = "" Then
            lblError.Visible = True
        Else
            lblError.Visible = False
        End If
        If ctryall.Tables(0).Rows.Count = 0 Then
            Label1.Visible = True
        Else
            Label1.Visible = False
        End If
        
        DisplayGridHeader
    End Sub
    Protected Sub ctryView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdCustomer.PageIndexChanging
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        Dim CustomerEntity As New clsCustomerRecord()
        grdCustomer.PageIndex = e.NewPageIndex

        Dim Customerall As DataSet = Session("Customer")
        grdCustomer.DataSource = Customerall
        grdCustomer.DataBind()
        DisplayGridHeader
    End Sub
#Region " id bond"
    Public Function databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList) As ArrayList
        dropdown.Items.Clear()
        Dim row As DataRow
        Dim infon As ArrayList = New ArrayList()
        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            NewItem.Text = row("id") & "-" & row("name")
            NewItem.Value = row("id")
            dropdown.Items.Add(NewItem)
            infon.Add(NewItem.Value)
        Next
        Return infon

    End Function
#End Region


    Protected Sub ctryView_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdCustomer.RowEditing
        Dim hst As New Hashtable
        Try
            hst = Session("htSvcBill")

            hst.Item("CustomerNo") = grdCustomer.Rows(e.NewEditIndex).Cells(1).Text
            hst.Item("CustomerPrefix") = Session("login_ctryID")
            hst.Item("Customer") = grdCustomer.Rows(e.NewEditIndex).Cells(2).Text

            Session("htSvcBill") = hst

            If hst.Item("SBUNO") = 1 Then
                Response.Redirect("ServiceBillAdd.aspx")
            Else
                Response.Redirect("ServiceBillUpdate2.aspx")
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub lnkback_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkback.Click
        Dim hst As New Hashtable
        Try
            hst = Session("htSvcBill")
            Session("htSvcBill") = hst
            If hst.Item("SBUNO") = 1 Then
                Response.Redirect("ServiceBillAdd.aspx")
            Else
                Response.Redirect("ServiceBillUpdate2.aspx")
            End If
        Catch

        End Try
    End Sub

    Protected Sub grdCustomer_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdCustomer.SelectedIndexChanged

    End Sub
End Class
