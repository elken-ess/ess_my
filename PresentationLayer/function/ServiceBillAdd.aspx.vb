Imports BusinessEntity
Imports System.Configuration
Imports System.Xml
Imports SQLDataAccess
Imports System.Data.SqlClient
Imports System.Web
Imports System.Data
Imports System.Windows.Forms


Partial Class PresentationLayer_function_ServiceBillAdd

    Inherits System.Web.UI.Page

    Private dsItem As New DataSet
    Private fstrCountryID As String
    Private fstrCompanyID As String
    Private fstrServiceCenterID As String
    Private fstrUserID As String
    Private fstrRank As String

    Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Page.MaintainScrollPositionOnPostBack = True
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        AjaxPro.Utility.RegisterTypeForAjax(GetType(PresentationLayer_function_ServiceBillAdd))

        fstrCountryID = Session("login_ctryID").ToString()
        fstrCompanyID = Session("login_cmpID").ToString()
        fstrServiceCenterID = Session("login_svcID").ToString()
        fstrUserID = Session("userID").ToString()
        fstrRank = Session("login_rank").ToString()

        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try
        If (Not Page.IsPostBack) Then
            Try
                InitializePage()
            Catch
            End Try
        End If
        HypCal.NavigateUrl = "javascript:DoCal(document.form1.txtDate);"

        EnableDisableControlsIfCollectPayment(cboServiceType.SelectedValue = "CP")

        Dim accessgroup As String = Session("accessgroup").ToString
        Dim purviewArray As Array = New Boolean() {False, False, False, False}
        purviewArray = clsUserAccessGroup.GetUserPurview(accessgroup, "30")

        Me.lnkSave.Enabled = purviewArray(0)

    End Sub

    Protected Sub cboCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCountry.SelectedIndexChanged
        Try
            Dim company As New clsCommonClass
            Dim rank As String = Session("login_rank")
            If rank = 7 Then
                company.spctr = Me.cboCountry.SelectedValue().ToString().ToUpper()
            End If
            If rank = 8 Then
                company.spctr = Me.cboCountry.SelectedValue().ToString().ToUpper()
                company.spstat = Session("login_cmpID")
            End If
            If rank = 9 Then
                company.spctr = Me.cboCountry.SelectedValue().ToString().ToUpper()
                company.spstat = Session("login_cmpID")
                company.sparea = Session("login_svcID")
            End If
            If rank = 0 Then
                company.spctr = Me.cboCountry.SelectedValue().ToString().ToUpper()
            End If

            company.rank = rank
            Dim dscompany As New DataSet

            dscompany = company.Getcomidname("BB_MASCOMP_IDNAME")
            If dscompany.Tables.Count <> 0 Then
                databonds(dscompany, Me.cboCompany)
            End If
            Me.cboCompany.Items.Insert(0, "")
            Me.cboCompany.SelectedIndex = 0
            Me.cboCompany.SelectedValue = Session("login_cmpID")
            Me.cboCompany.Enabled = False
        Catch

        End Try
        txtDate.Text = Request.Form("txtDate")
    End Sub

    Sub GetServiceCenterDropdown()
        Try
            Dim ServiceCenter As New clsCommonClass
            Dim rank As String = Session("login_rank")
            If rank = 7 Then
                ServiceCenter.spctr = Me.cboCountry.SelectedValue().ToString().ToUpper()
                ServiceCenter.spstat = Me.cboCompany.SelectedValue()
            End If
            If rank = 8 Then
                ServiceCenter.spctr = Me.cboCountry.SelectedValue().ToString().ToUpper()
                ServiceCenter.spstat = Session("login_cmpID")
            End If
            If rank = 9 Then
                ServiceCenter.spctr = Me.cboCountry.SelectedValue().ToString().ToUpper()
                ServiceCenter.spstat = Session("login_cmpID")
                ServiceCenter.sparea = Session("login_svcID")
            End If
            If rank = 0 Then
                ServiceCenter.spctr = Me.cboCountry.SelectedValue().ToString().ToUpper()
            End If

            ServiceCenter.rank = rank

            Dim dsServiceCentere As New DataSet
            dsServiceCentere = ServiceCenter.Getcomidname("BB_MASSVRC_IDNAME")
            If dsServiceCentere.Tables.Count <> 0 Then
                databonds(dsServiceCentere, Me.cboServiceCenter)
            End If
            Me.cboServiceCenter.Items.Insert(0, "")
            Me.cboServiceCenter.SelectedIndex = 0
        Catch

        End Try
    End Sub

    Sub GetTechnicianDropdown()
        Try
            Dim Technician As New clsCommonClass
            Dim rank As String = Session("login_rank")
            If rank = 7 Then
                Technician.spctr = Me.cboCountry.SelectedValue().ToString().ToUpper()
                Technician.spstat = Me.cboCompany.SelectedValue()
                Technician.sparea = cboServiceCenter.SelectedValue
            End If
            If rank = 8 Then
                Technician.spctr = Me.cboCountry.SelectedValue().ToString().ToUpper()
                Technician.spstat = Session("login_cmpID")
                Technician.sparea = cboServiceCenter.SelectedValue
            End If
            If rank = 9 Then
                Technician.spctr = Me.cboCountry.SelectedValue().ToString().ToUpper()
                Technician.spstat = Session("login_cmpID")
                Technician.sparea = Session("login_svcID")
            End If
            If rank = 0 Then
                Technician.spctr = Me.cboCountry.SelectedValue().ToString().ToUpper()
            End If

            Technician.rank = rank

            Dim dsTechnician As New DataSet
            dsTechnician = Technician.Getcomidname("BB_MASTECH_NAMEID")
            If dsTechnician.Tables.Count <> 0 Then
                databondsForTechnician(dsTechnician, Me.cboTechnician)
            End If
            Me.cboTechnician.Items.Insert(0, "")
            Me.cboTechnician.SelectedIndex = 0

            Technician.sparea = Me.cboServiceCenter.SelectedValue

            Dim dsTax As New DataSet
            dsTax = Technician.Getcomidname("BB_FNCSALESTAX")
            If dsTax.Tables(0).Rows.Count <> 0 Then
                txtTaxRate.Text = dsTax.Tables(0).Rows(0).Item("NAME")
                txtTaxID.Text = dsTax.Tables(0).Rows(0).Item("ID")
            Else
                txtTaxRate.Text = 0
                txtTaxID.Text = ""
            End If
        Catch

        End Try
        txtDate.Text = Request.Form("txtDate")
    End Sub

    Protected Sub cboServiceCenter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboServiceCenter.SelectedIndexChanged
        GetTechnicianDropdown()
    End Sub

    Protected Sub grdServiceBill_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdServiceBill.RowDeleting
        Try
            RefreshDataSet()
            dsItem.Tables("StockList").Rows(e.RowIndex).Delete()
            Me.grdServiceBill.DataSource = dsItem
            Me.grdServiceBill.DataBind()
            InitializeGridColumns()
            RefreshDataSet()
            txtDate.Text = Request.Form("txtDate")
        Catch

        End Try
    End Sub

    Protected Sub btnSearchAppNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearchAppNo.Click
        RetainValuesBeforeSearch()
        Response.Redirect("AppointmentLookup.aspx?stat=SM")
    End Sub

    Protected Sub MessageBox1_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox1.GetMessageBoxResponse

        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
            SaveServiceBill()
        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
            Exit Sub
        End If
        txtDate.Text = Request.Form("txtDate")

    End Sub

    Protected Sub Calendar1_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendar1.SelectionChanged
        'Me.txtDate.Text = Format(Me.Calendar1.SelectedDate, "dd-MM-yyyy")
        Me.txtDate.Text = Me.Calendar1.SelectedDate
        Calendar1.Visible = False
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Calendar1.Visible = True
    End Sub

    Protected Sub lnkAddItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddItem.Click
        RequestFromValue()
        Dim valid As Boolean = False
        valid = ValidateDetails()
        If valid = True Then AddItems()

    End Sub

    Sub RequestFromValue()
        Dim commonClass As New clsCommonClass
        commonClass.spctr = cboCountry.SelectedValue.ToUpper()
        commonClass.spstat = "ACTIVE"
        commonClass.sparea = txtPartCode.Text
        commonClass.rank = Session("login_rank").ToString()

        Dim dsPriceID As DataSet = commonClass.Getcomidname("dbo.BB_FNCUSB1_SelPriceList")
        databonds(dsPriceID, cboPriceID)

        Me.txtDiscount.Text = Request.Form("txtDiscount")
        Me.txtTotalNet.Text = Request.Form("txtTotalNet")
        Me.txtCash.Text = Request.Form("txtCash")
        Me.txtCheque.Text = Request.Form("txtCheque")
        Me.txtCredit.Text = Request.Form("txtCredit")
        Me.txtCreditCard.Text = Request.Form("txtCreditCard")
        Me.txtOther.Text = Request.Form("txtOther")

        txtDate.Text = Request.Form("txtDate")
        txtTaxRate.Text = Request.Form("txtTaxRate")
        txtTaxID.Text = Request.Form("txtTaxID")

        txtPartName.Text = Request.Form("txtPartName")

        Dim priceID As String = Request.Form("cboPriceID")
        cboPriceID.SelectedValue = priceID

        txtPartCode.Text = Request.Form("txtPartCode")
        txtLineTotal.Text = Request.Form("txtLineTotal")
        txtQty.Text = Request.Form("txtQty")
        txtUnitPrice.Text = Request.Form("txtUnitPrice")

        Dim lstrServiceCenterID As String = ""
        Dim lstrTechnicianID As String = ""

        If Not Request.Form("cboServiceCenter") Is Nothing Then
            lstrServiceCenterID = Request.Form("cboServiceCenter")
            lstrTechnicianID = Request.Form("cboTechnician")
        Else
            lstrServiceCenterID = cboServiceCenter.SelectedValue
            lstrTechnicianID = cboTechnician.SelectedValue
        End If

        GetServiceCenterDropdown()

        Me.cboServiceCenter.SelectedValue = lstrServiceCenterID

        GetTechnicianDropdown()
        Me.cboTechnician.SelectedValue = lstrTechnicianID

    End Sub

    Protected Sub lnkSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSave.Click
        RequestFromValue()

        Dim valid As Boolean = ValidateHeader()
        If valid = False Then Exit Sub
        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        txtDate.Text = Request.Form("txtDate")

        Dim msginfo As String = objXm.GetLabelName("StatusMessage", "SAVEORNOT")
        Dim msgtitle As String = objXm.GetLabelName("StatusMessage", "SAVETITLE")

        ViewState("ServiceOrderDate") = txtDate.Text
        ViewState("TechnicianValue") = cboTechnician.SelectedValue
        ViewState("ServiceCenterIndex") = cboServiceCenter.SelectedValue

        'LLY 20170517
        If (txtServiceBillNo.Text = "") Then
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            MessageBox1.Confirm(objXmlTr.GetLabelName("StatusMessage", "SBA-EMPTY-REMINDER-P1") + "\n" + objXmlTr.GetLabelName("StatusMessage", "SBA-EMPTY-REMINDER-P2"))
            Exit Sub
        End If

        SaveServiceBill()
    End Sub

    Protected Sub MessageBox2_GetMessageBoxResponse(ByVal sender As Object, ByVal e As Utilities.MessageBox.MessageBoxEventHandler) Handles MessageBox2.GetMessageBoxResponse
        If (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Ok) Then
            Response.Redirect("UpdateServiceBillPart1.Aspx")
        ElseIf (e.ButtonPressed = Utilities.MessageBox.MessageBoxEventHandler.Button.Cancel) Then
            Exit Sub
        End If
        txtDate.Text = Request.Form("txtDate")

    End Sub

    Protected Sub lnkCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCancel.Click
        If lnkSave.Enabled = True Then
            Dim objXm As New clsXml
            objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            txtDate.Text = Request.Form("txtDate")
            Dim msginfo As String = objXm.GetLabelName("StatusMessage", "SBA-CANCELMSG")
            Dim msgtitle As String = objXm.GetLabelName("StatusMessage", "SBA-CANCEL")

            Response.Redirect("UpdateServiceBillPart1.Aspx")
        Else
            Response.Redirect("UpdateServiceBillPart1.Aspx")
        End If
    End Sub

    Protected Sub lnkNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkNew.Click
        Try
            txtDate.Text = ViewState("ServiceOrderDate").ToString()

            lnkSave.Enabled = True
            lnkNew.Enabled = False
            lnkGeneratePackingList.Enabled = False
            lnkGenerateSBC.Enabled = False
            'ImageButton1.Enabled = True
            txtServiceBillNo.Enabled = True
            cboServiceBillType.Enabled = True
            cboTechnician.Enabled = True
            cboServiceType.Enabled = True
            btnSearchAppNo.Enabled = True
            lnkAddItem.Enabled = True
            txtDiscount.Enabled = True
            txtCash.Enabled = True
            txtCheque.Enabled = True
            txtCredit.Enabled = True
            txtCreditCard.Enabled = True
            txtOther.Enabled = True
            grdServiceBill.Enabled = True
            JCalendar1.Enabled = True

            dsItem.Clear()

            If dsItem.Tables.Count = 0 Then
                dsItem.Tables.Add("StockList")
                dsItem.Tables("StockList").Columns.Add("ItemNo")
                dsItem.Tables("StockList").Columns.Add("Part Code")
                dsItem.Tables("StockList").Columns.Add("Part Description")
                dsItem.Tables("StockList").Columns.Add("Quantity")
                dsItem.Tables("StockList").Columns.Add("PriceID")
                dsItem.Tables("StockList").Columns.Add("UnitPrice")
                dsItem.Tables("StockList").Columns.Add("Tax")
                dsItem.Tables("StockList").Columns.Add("Amount")
            End If

            grdServiceBill.DataSource = dsItem
            grdServiceBill.DataBind()

            lblError.Text = ""

            cboServiceBillType.SelectedIndex = 0
            txtServiceBillNo.Text = ""
            cboServiceType.SelectedIndex = 0

            txtDiscount.Text = "0"
            txtCash.Text = "0"
            txtCheque.Text = "0"
            txtCredit.Text = "0"
            txtCreditCard.Text = "0"
            txtOther.Text = "0"
            txtTotal.Text = "0"
            txtTotalGross.Text = "0"
            txtTotalNet.Text = "0"
            txtTax.Text = "0"

            txtLineTotal.Text = "0"
            txtQty.Text = "1"
            txtPartCode.Text = ""
            txtPartName.Text = ""
            txtPriceID.Text = ""
            txtUnitPrice.Text = ""

            txtInvoicePf.Text = ""
            txtInvoiceNo.Text = ""
            Me.txtRemarks.Text = ""
            Me.txtAppointmentPrefix.Text = ""
            Me.txtAppointmentNo.Text = ""
            Me.txtROSerialNo.Text = ""
            Me.txtCustomerPrefix.Text = ""
            Me.txtCustomerID.Text = ""
            'Me.txtDate.Text = ""
            Me.txtCustomer.Text = ""

            Dim cls As New clsCommonClass
            cls.spctr = Me.cboCountry.SelectedValue().ToString().ToUpper()
            cls.spstat = Session("login_cmpID").ToString()
            cls.sparea = Session("Login_SVCID").ToString()
            cls.rank = Session("login_rank").ToString()

            Dim dscompany As New DataSet
            dscompany = cls.Getcomidname("BB_MASCOMP_IDNAME")
            If dscompany.Tables.Count <> 0 Then
                databonds(dscompany, Me.cboCompany)
            End If
            Me.cboCompany.Items.Insert(0, "")
            Me.cboCompany.SelectedValue = Session("login_cmpID").ToString()
            If Session("Login_Rank") >= 8 Then
                Me.cboCompany.Enabled = False
            Else
                Me.cboCompany.Enabled = True
            End If

            Dim dsServiceCentere As New DataSet

            dsServiceCentere = cls.Getcomidname("BB_MASSVRC_IDNAME")
            If dsServiceCentere.Tables.Count <> 0 Then
                databonds(dsServiceCentere, Me.cboServiceCenter)
            End If
            Me.cboServiceCenter.Items.Insert(0, "")

            cboServiceCenter.SelectedValue = ViewState("ServiceCenterIndex").ToString()

            If Session("Login_Rank") >= 9 Then
                Me.cboServiceCenter.Enabled = False
            Else
                Me.cboServiceCenter.Enabled = True
            End If

            Dim dsTax As New DataSet
            dsTax = cls.Getcomidname("BB_FNCSALESTAX")
            If dsTax.Tables(0).Rows.Count <> 0 Then
                txtTaxRate.Text = dsTax.Tables(0).Rows(0).Item("NAME").ToString()
                txtTaxID.Text = dsTax.Tables(0).Rows(0).Item("ID").ToString()
            Else
                txtTaxRate.Text = "0"
                txtTaxID.Text = ""
            End If

            Dim dsTechnician As New DataSet
            Dim lstrTechnicianID As String = cboTechnician.SelectedValue
            dsTechnician = cls.Getcomidname("BB_MASTECH_NAMEID")
            If dsTechnician.Tables.Count <> 0 Then
                databondsForTechnician(dsTechnician, Me.cboTechnician)
            End If
            Me.cboTechnician.Items.Insert(0, "")
            
            cboTechnician.SelectedValue = ViewState("TechnicianValue").ToString()

        Catch

        End Try

    End Sub

    Protected Sub lnkGeneratePackingList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkGeneratePackingList.Click

        Try
            Dim clsGPL As New clsServiceBillUpdate

            Dim clsgenprf As New clsGenPRF
            Dim ResultCtr As Integer = 0

            clsgenprf.InvoiceDate = Me.txtDate.Text
            clsgenprf.Technician = Me.cboTechnician.SelectedValue
            clsgenprf.ServiceCenter = Me.cboServiceCenter.SelectedValue

            clsgenprf.logctrid = Session("login_ctryID").ToString.ToUpper
            clsgenprf.logcompanyid = Session("login_cmpID").ToString.ToUpper
            clsgenprf.logserviceid = Session("login_svcID").ToString.ToUpper
            clsgenprf.loguserid = Session("userID").ToString().ToUpper()

            ResultCtr = 0

            'Add new Update Service Bill P1 not required check, because new always will Billed status.
            'If Session("currStatus").ToString() = "BL" Then
            ResultCtr = clsgenprf.CheckGenPackList
            Dim prfNo As String

            If ResultCtr > 0 Then
                prfNo = clsgenprf.GeneratePackingList()

                If prfNo <> "" Then
                    ShowPRFReport(prfNo)
                Else
                    lblError.Text = "Error Showing Report"
                    MessageBox1.Alert(lblError.Text)
                End If
            Else
                lblError.Text = "No Packing List to be generate."
                MessageBox1.Alert(lblError.Text)
            End If
            'Else
            'lblError.Text = "Billed status only able to generate Packing List"
            'MessageBox1.Alert(lblError.Text)
            'End If

            lnkSave.Enabled = False

        Catch ex As Exception
            lblError.Text = ex.Message
        End Try

    End Sub

    Private Sub ShowPRFReport(ByVal prfNo As String)
        Dim packEntity As New ClsRptReprintPRF

        packEntity.servcenterid = Me.cboServiceCenter.SelectedValue
        packEntity.datemin = Me.txtDate.Text
        packEntity.tecnicianidbegin = Me.cboTechnician.SelectedValue
        packEntity.prfno = prfNo.Trim.ToUpper

        packEntity.logctrid = Session("login_ctryID").ToString.ToUpper
        packEntity.logcompanyid = Session("login_cmpID").ToString.ToUpper
        packEntity.logserviceid = Session("login_svcID").ToString.ToUpper
        packEntity.logrank = Session("login_rank").ToString().ToUpper()

        Session("rptreprintprf_search_condition") = packEntity

        Dim script As String = "window.open('../report/RptReprintPRFSearch.aspx')"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "RptReprintPRFSearch", script, True)

    End Sub

    Protected Sub JCalendar1_SelectedDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles JCalendar1.SelectedDateChanged
        If JCalendar1.SelectedDate > Now() Then
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            lblError.Text = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRFUTUREDATE")
            MessageBox1.Alert(lblError.Text)
            Me.txtDate.Text = Date.Now().ToShortDateString
            lblError.Visible = True
        Else
            txtDate.Text = JCalendar1.SelectedDate
            lblError.Visible = False
        End If
    End Sub

    Protected Sub cboServiceBillType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboServiceBillType.SelectedIndexChanged
        ServiceBillType()
        txtDate.Text = Request.Form("txtDate")
    End Sub

    Protected Sub btnSearchCustomer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearchCustomer.Click
        txtDate.Text = Request.Form("txtDate")
        RetainValuesBeforeSearch()
        Response.Redirect("CustomerLookup.aspx")
    End Sub

    Protected Sub txtCash_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCash.TextChanged
        txtDate.Text = Request.Form("txtDate")
    End Sub

    Protected Sub txtCheque_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCheque.TextChanged
        txtDate.Text = Request.Form("txtDate")
    End Sub

    Protected Sub txtCredit_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCredit.TextChanged
        txtDate.Text = Request.Form("txtDate")
    End Sub

    Protected Sub txtCreditCard_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCreditCard.TextChanged
        txtDate.Text = Request.Form("txtDate")
    End Sub

    Protected Sub txtOther_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtOther.TextChanged
        txtDate.Text = Request.Form("txtDate")
    End Sub

    Protected Sub txtDiscount_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDiscount.TextChanged
        txtDate.Text = Request.Form("txtDate")
    End Sub

#Region "Private Subs"
    Public Sub AddItems()
        Try
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = System.Configuration.ConfigurationSettings.AppSettings("XmlFilePath")

            RefreshDataSet()

            Dim drItem As DataRow
            Dim RowNo As Integer = dsItem.Tables("StockList").Rows.Count
            Dim TaxAmt As Double
            txtLineTotal.Text = Val(Val(Me.txtQty.Text) * Val(Me.txtUnitPrice.Text))
            TaxAmt = CDbl(Val(txtTaxRate.Text) / 100.0) * txtLineTotal.Text
            drItem = dsItem.Tables("StockList").NewRow()

            drItem(0) = RowNo + 1
            drItem.Item(1) = UCase(Me.txtPartCode.Text)
            drItem.Item(2) = UCase(Trim(Me.txtPartName.Text))
            drItem.Item(3) = String.Format("{0:f0}", Val(Me.txtQty.Text))
            drItem.Item(4) = UCase(cboPriceID.SelectedValue)
            drItem.Item(5) = String.Format("{0:f2}", Val(Me.txtUnitPrice.Text))
            drItem.Item(6) = String.Format("{0:f2}", Val(TaxAmt))
            drItem.Item(7) = String.Format("{0:f2}", Val(Me.txtQty.Text) * Val(Me.txtUnitPrice.Text))

            dsItem.Tables("StockList").Rows.Add(drItem)
            Me.grdServiceBill.DataSource = dsItem
            Me.grdServiceBill.DataBind()
            InitializeGridColumns()

            txtTotalGross.Text = String.Format("{0:f2}", Val(txtTotalGross.Text) + Val(Me.txtLineTotal.Text))
            txtTotalNet.Text = String.Format("{0:f2}", Val(txtTotalGross.Text) - Val(txtDiscount.Text))
            txtTax.Text = String.Format("{0:f2}", Val(txtTax.Text) + TaxAmt)

            txtPartCode.Text = ""
            cboPriceID.Items.Clear()

            txtPartName.Text = ""
            txtUnitPrice.Text = ""
            txtQty.Text = "1"
            txtLineTotal.Text = "0"
        Catch

        End Try

    End Sub

    Private Sub ComputeLineTotal()
        Try
            lblError.Visible = False
            lblError.Text = ""
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            If IsNumeric(txtQty.Text) Then
                If Val(Me.txtQty.Text) <= 0 Then
                    txtQty.Text = "1"
                    lblError.Text = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRQTY")
                    MessageBox1.Alert(lblError.Text)
                    lblError.Visible = True
                    Exit Sub
                End If
                If IsNumeric(txtUnitPrice.Text) Then
                    txtQty.Text = String.Format("{0:f0}", Val(txtQty.Text))
                    txtLineTotal.Text = String.Format("{0:f2}", Val(txtUnitPrice.Text) * (txtQty.Text))
                Else
                    txtQty.Text = "1"
                    txtUnitPrice.Text = ""
                    txtLineTotal.Text = "0"
                End If
            Else
                txtQty.Text = "1"
                txtLineTotal.Text = "0"
            End If
        Catch

        End Try

    End Sub

    Private Sub ComputeNetTotal()
        Try
            lblError.Visible = False
            lblError.Text = ""
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            If IsNumeric(txtDiscount.Text) Then
                If Val(Me.txtDiscount.Text) < 0 Then
                    lblError.Text = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRDISCLESS")
                    If lblError.Text = "" Then lblError.Text = "Discount Must be Greater Than or Equal to 0"
                    MessageBox1.Alert(lblError.Text)
                    lblError.Visible = True
                    txtDiscount.Text = "0"
                    txtTotalNet.Text = txtTotalGross.Text
                    Exit Sub
                End If
                If Val(Me.txtTotalGross.Text) - Val(Me.txtDiscount.Text) < 0 Then
                    lblError.Text = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRDISCMORE")
                    If lblError.Text = "" Then lblError.Text = "Discount Must be Less Than the Grand Total"
                    MessageBox1.Alert(lblError.Text)
                    lblError.Visible = True
                    txtDiscount.Text = "0"
                    txtTotalNet.Text = txtTotalGross.Text
                    Exit Sub
                End If
                txtDiscount.Text = String.Format("{0:f2}", Val(txtDiscount.Text))
                txtTotalNet.Text = String.Format("{0:f2}", Val(txtTotalGross.Text) - Val(txtDiscount.Text))
            Else
                txtDiscount.Text = "0"
                txtTotalNet.Text = txtTotalGross.Text
            End If
        Catch

        End Try

    End Sub

    Private Sub databondsForTechnician(ByVal ds As DataSet, ByVal dropdown As DropDownList, Optional ByVal X As Boolean = True)

        dropdown.Items.Clear()
        Dim row As DataRow

        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            If X = True Then
                NewItem.Text = Trim(row("name")) & "-" & Trim(row("id"))
            Else
                NewItem.Text = Trim(row("name"))
            End If
            NewItem.Value = Trim(row("id"))
            dropdown.Items.Add(NewItem)
        Next

    End Sub

    Private Sub databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList, Optional ByVal X As Boolean = True)

        dropdown.Items.Clear()
        Dim row As DataRow

        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            If X = True Then
                NewItem.Text = Trim(row("id")) & "-" & Trim(row("name"))
            Else
                NewItem.Text = Trim(row("name"))
            End If
            NewItem.Value = Trim(row("id"))
            dropdown.Items.Add(NewItem)
        Next

    End Sub

    ''' <summary>
    ''' Checks if the service order number entered is billed or not
    ''' </summary>
    ''' <param name="apptNo">Service order number</param>
    ''' <param name="apptPrefix">Service order prefix</param>
    ''' <param name="country">country</param>
    ''' <returns>
    ''' Returns true if the service order number is billed, done, etc.
    ''' Returns false if the service order number is assigned
    ''' </returns>
    ''' <remarks>Added by Ryan Estandarte 3 May 2012</remarks>
    Private Function ServiceOrderNoIsBilled(ByVal apptNo As String, ByVal apptPrefix As String, ByVal country As String) As Boolean
        Dim clsAppointment As New clsServiceBillUpdate
        clsAppointment.Country = country
        clsAppointment.AppointmentNo = apptNo
        clsAppointment.AppointmentPrefix = apptPrefix

        Dim dsAppt As DataSet = clsAppointment.GetApptDetails

        If dsAppt.Tables(0).Rows.Count > 0 Then
            If dsAppt.Tables(0).Rows(0)("FAPT_STAT") = "BL" Then
                Return True
            Else
                Return False
            End If
        Else
            Return False
        End If

    End Function

    Private Sub GetAppointmentdetails()
        Try
            Dim cls As New clsCommonClass
            Dim clsAppointment As New clsServiceBillUpdate
            clsAppointment.Country = cboCountry.SelectedValue
            clsAppointment.AppointmentNo = txtAppointmentNo.Text
            clsAppointment.AppointmentPrefix = txtAppointmentPrefix.Text

            Dim dstappointment As New DataSet
            dstappointment = clsAppointment.GetApptDetails

            If dstappointment.Tables(0).Rows.Count <> 0 Then
                If dstappointment.Tables(0).Rows(0)(9) <> "BL" Then
                    txtCustomerPrefix.Text = dstappointment.Tables(0).Rows(0).Item("CUSPF").ToString()
                    txtCustomerID.Text = dstappointment.Tables(0).Rows(0).Item("CUSID").ToString()
                    txtCustomer.Text = dstappointment.Tables(0).Rows(0).Item("CUSNM").ToString()
                    txtROSerialNo.Text = Trim(dstappointment.Tables(0).Rows(0).Item("MODID").ToString()) & "-" & _
                                           Trim(dstappointment.Tables(0).Rows(0).Item("SERNO").ToString())
                    txtROUID.Text = Trim(dstappointment.Tables(0).Rows(0).Item("ROUID").ToString())
                    RMStxt.Text = Trim(dstappointment.Tables(0).Rows(0).Item("RMSNO").ToString())
                    
                    Dim servDate As DateTime
                    DateTime.TryParse(dstappointment.Tables(0).Rows(0).Item("INVDT").ToString(), servDate)
                    txtDate.Text = servDate.ToShortDateString()

                    GetCustomerRO()
                    cboRO.SelectedValue = Trim(dstappointment.Tables(0).Rows(0).Item("ROUID").ToString())

                    cls.spctr = cboCountry.SelectedValue().ToString().ToUpper()
                    cls.spstat = Trim(dstappointment.Tables(0).Rows(0).Item("COMID").ToString())
                    cls.sparea = Trim(dstappointment.Tables(0).Rows(0).Item("SVCID").ToString())

                    cls.rank = Session("login_rank").ToString()

                    Dim dscompany As DataSet
                    dscompany = cls.Getcomidname("BB_MASCOMP_IDNAME")
                    If dscompany.Tables.Count <> 0 Then
                        databonds(dscompany, cboCompany)
                    End If
                    cboCompany.Items.Insert(0, "")
                    cboCompany.Enabled = False

                    Dim dsServiceCentere As DataSet

                    dsServiceCentere = cls.Getcomidname("BB_MASSVRC_IDNAME")
                    If dsServiceCentere.Tables.Count <> 0 Then
                        databonds(dsServiceCentere, cboServiceCenter)
                    End If
                    cboServiceCenter.Items.Insert(0, "")
                    cboServiceCenter.Enabled = True

                    Dim dsTechnician As DataSet
                    dsTechnician = cls.Getcomidname("BB_MASTECH_NAMEID")
                    If dsTechnician.Tables.Count <> 0 Then
                        databondsForTechnician(dsTechnician, cboTechnician)
                    End If
                    cboTechnician.Items.Insert(0, "")

                    Dim dsTax As DataSet
                    dsTax = cls.Getcomidname("BB_FNCSALESTAX")
                    If dsTax.Tables(0).Rows.Count <> 0 Then
                        txtTaxRate.Text = dsTax.Tables(0).Rows(0).Item("NAME").ToString()
                        txtTaxID.Text = dsTax.Tables(0).Rows(0).Item("ID").ToString()
                    Else
                        txtTaxRate.Text = "0"
                        txtTaxID.Text = ""
                    End If

                    cboCompany.SelectedValue = Trim(dstappointment.Tables(0).Rows(0).Item("COMID").ToString())
                    cboServiceCenter.SelectedValue = Trim(dstappointment.Tables(0).Rows(0).Item("SVCID").ToString())
                    cboTechnician.SelectedValue = Trim(dstappointment.Tables(0).Rows(0).Item("TCHID").ToString())
                    cboServiceType.SelectedValue = Trim(dstappointment.Tables(0).Rows(0).Item("SVTID").ToString())

                    cboServiceType.Enabled = True
                    cboTechnician.Enabled = True
                    JCalendar1.Enabled = False
                    cboCompany.Enabled = False
                    cboServiceCenter.Enabled = False
                Else
                    ServiceBillError()
                    RMStxt.Text = Trim(dstappointment.Tables(0).Rows(0).Item("RMSNO").ToString())
                End If
            Else
                ServiceBillError()
            End If
        Catch

        End Try

    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks>Added by Ryan Estandarte 3 May 2012</remarks>
    Private Sub ServiceBillError()

        JCalendar1.Enabled = True
        cboServiceType.Enabled = True
        cboTechnician.Enabled = True
        If Session("Login_Rank") <= 7 Then
            cboCompany.Enabled = True
        End If

        If Session("Login_Rank") <= 8 Then
            cboServiceCenter.Enabled = True
        End If

        txtCustomerPrefix.Text = ""
        txtCustomerID.Text = ""
        txtCustomer.Text = ""
        txtROSerialNo.Text = ""
        txtROUID.Text = ""
        txtDate.Text = ""

        Dim objXmlTr As New clsXml

        lblError.Text = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRNOSERVICEORDNO")
        MessageBox1.Alert(lblError.Text)
    End Sub

    Private Sub GetCustomerRO()
        Try
            Dim clsCustomer As New clsServiceBillUpdate
            clsCustomer.Country = Me.cboCountry.SelectedValue
            clsCustomer.CustomerPrefix = txtCustomerPrefix.Text
            clsCustomer.CustomerNo = txtCustomerID.Text

            Dim dstCustomer As New DataSet
            dstCustomer = clsCustomer.GetCustomerRO

            If dstCustomer.Tables.Count <> 0 Then
                databonds(dstCustomer, cboRO, False)
            End If
            cboRO.Items.Insert(0, "")
            cboRO.SelectedIndex = 0
        Catch

        End Try

    End Sub

    Private Sub InitializeGridColumns()
        Try
            'get column labels
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            Me.grdServiceBill.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-NO")
            Me.grdServiceBill.HeaderRow.Cells(1).Font.Size = 8

            Me.grdServiceBill.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-PARTCODE")
            Me.grdServiceBill.HeaderRow.Cells(2).Font.Size = 8

            Me.grdServiceBill.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-PARTDESC")
            Me.grdServiceBill.HeaderRow.Cells(3).Font.Size = 8

            Me.grdServiceBill.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-QUANTITY")
            Me.grdServiceBill.HeaderRow.Cells(4).Font.Size = 8

            Me.grdServiceBill.HeaderRow.Cells(6).Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-UNITPRICE")
            Me.grdServiceBill.HeaderRow.Cells(6).Font.Size = 8

            Me.grdServiceBill.HeaderRow.Cells(7).Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-TAX")
            Me.grdServiceBill.HeaderRow.Cells(7).Font.Size = 8

            Me.grdServiceBill.HeaderRow.Cells(8).Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-LINETOTAL")
            Me.grdServiceBill.HeaderRow.Cells(8).Font.Size = 8

            Me.grdServiceBill.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-PRICEID")
            Me.grdServiceBill.HeaderRow.Cells(5).Font.Size = 8
        Catch

        End Try

    End Sub

    Private Sub InitializePage()
        Try
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")

            '*******************************************************************
            ' BEGIN INITIALIZE LABELS AND VALIDATORS
            '*******************************************************************
            '************
            ' LABELS    
            '************
            Me.lblAppoinmentNo.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-APPOINMENTNO")
            Me.lblCash.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-CASH")
            Me.lblCheque.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-CHEQUE")
            Me.lblCompany.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-COMPANY")
            Me.lblCountry.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-COUNTRY")
            Me.lblCreateDate.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-CREATEDATE")
            Me.lblCreatedBy.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-CREATEBY")
            Me.lblCredit.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-CREDIT")
            Me.lblCreditCard.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-CREDITCARD")
            Me.lblCustomer.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-CUSTOMER")
            Me.lblCustomerID.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-CUSTOMERID")
            Me.lblDate.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-DATE")
            Me.lblDiscount.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-DISCOUNT")
            Me.lblInclusive.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-INCLUSIVE")
            Me.lblInvoiceTax.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-INVOICETAX")
            Me.lblInvoiceNo.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-INVOICENO")
            Me.lblLineTotal.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-LINETOTAL")
            Me.lblModifyBy.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-MODIFYBY")
            Me.lblModifyDate.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-MODIFYDATE")
            Me.lblOther.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-OTHER")
            Me.lblPartCode.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-PARTCODE")
            Me.lblPaymentDetails.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-PAYMENTDETAILS")
            Me.lblPriceID.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-PRICEID")
            Me.lblQuantity.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-QUANTITY")
            Me.lblRemarks.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-REMARKS")
            Me.lblROSerialNo.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-ROSERIALNO")
            Me.lblServiceBillNo.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-SERVICEBILLNO")
            Me.lblServiceBillType.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-SERVICEBILLTYPE")
            Me.lblServiceCenter.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-SERVICECENTER")
            Me.lblServiceType.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-SERVICETYPE")
            Me.lblStatus.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-STATUS")
            Me.lblTechnician.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-TECHNICIAN")
            Me.lblTotal.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-TOTAL")
            Me.lblTotalGross.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-TOTAL")
            Me.lblTotalNet.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-TOTAL")
            Me.lblUpdateStockList.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-UPDATESTOCKLIST")
            Me.lnkAddItem.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-ADDITEM")
            Me.lnkGeneratePackingList.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-GENPACKLIST")
            Me.lnkSave.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-SAVE")
            lblNoteUp.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            lblNoteDown.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-RFV")
            CompDateVal.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "INVALID_DATE")
            Me.lblTax.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-PPL-0015")

            Label4.Visible = True
            Label8.Visible = False

            titleLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-update1")
            '************
            ' VALIDATORS   
            '************

            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            Me.rfvDate.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRDATE")
            'Me.rfvServiceBill.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRSERVICEBILL")
            Me.rfvServiceBillType.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRSERVICEBILLTYPE")
            Me.rfvServiceType.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRSERVICETYPE")
            Me.rfvTechnician.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRTECHNICIAN")
            Me.rfvAppointmentNo.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRAPPOINTMENTNO")
            Me.rfvCustomerNo.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRCUSTOMERNO")
            Me.rfvRO.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRRO")

            Me.rfvCountry.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-COUNTRYID")
            Me.rfvCompany.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-CompID")
            Me.rfvServiceCenter.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "BB-HintMsg-sercenteriderr")

            reqVal_Total.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "AMOUNTERROR")
            regExVal_Total.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "AMOUNTERROR")

            reqVal_Discount.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "AMOUNTERROR")
            regExVal_Discount.ErrorMessage = objXmlTr.GetLabelName("StatusMessage", "AMOUNTERROR")

            '*******************************************************************
            ' END INITIALIZE LABELS AND VALIDATORS
            '*******************************************************************

            '*******************************************************************
            ' BEGIN POPULATE DROP DOWN LISTS
            '*******************************************************************

            Dim CommonClass As New clsCommonClass()

            Dim userIDNamestr As String = Session("userID").ToString().ToUpper + "-" + Session("username").ToString.ToUpper
            Dim rank As String = Session("login_rank")
            CommonClass.spstat = Session("login_cmpID")
            CommonClass.sparea = Session("login_svcID")

            If rank <> 0 Then
                CommonClass.spctr = Session("login_ctryID").ToString().ToUpper
            End If
            CommonClass.rank = rank
            CommonClass.spstat = "ACTIVE"

            objXmlTr.XmlFile = System.Configuration.ConfigurationSettings.AppSettings("XmlFilePath")

            Dim Util As New clsUtil()
            Dim statParam As ArrayList = New ArrayList

            '---- SERVICE BILL TYPE
            statParam = Util.searchconfirminf("SERBILLTY")
            Dim count As Integer
            Dim statid As String
            Dim statnm As String
            For count = 0 To statParam.Count - 1
                statid = statParam.Item(count)
                statnm = statParam.Item(count + 1)
                count = count + 1
                Me.cboServiceBillType.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
            Next

            '---- STATUS
            statParam = Util.searchconfirminf("SerStat")
            For count = 0 To statParam.Count - 1
                statid = statParam.Item(count)
                statnm = statParam.Item(count + 1)
                count = count + 1
                If UCase(statid) = "BL" Then
                    Me.cboStatus.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
                End If
            Next
            cboStatus.Items(0).Selected = True

            '---- COUNTRY (ALWAYS DISABLED AND SELECT FROM LOGIN)
            Dim dsCountry As New DataSet
            Me.cboCountry.DataTextField = "name"
            Me.cboCountry.DataValueField = "id"
            dsCountry = CommonClass.Getcomidname("BB_MASCTRY_IDNAME")
            If dsCountry.Tables.Count <> 0 Then
                databonds(dsCountry, Me.cboCountry)
            End If
            Me.cboCountry.Items.Insert(0, "")
            Me.cboCountry.SelectedIndex = 0
            Me.cboCountry.SelectedValue = Session("login_ctryID").ToString().ToUpper
            Me.cboCountry.Enabled = False

            '---- SERVICE TYPE
            Dim dsSerType As New DataSet
            dsSerType = CommonClass.Getcomidname("BB_MASSRCT_IDNAMEALL")
            If dsSerType.Tables.Count <> 0 Then
                databonds(dsSerType, Me.cboServiceType)
            End If
            Me.cboServiceType.Items.Insert(0, "")

            CommonClass.spstat = Session("login_cmpID")

            '---- COMPANY (DISABLE IF RANK >= 8)
            Dim dscompany As New DataSet
            dscompany = CommonClass.Getcomidname("BB_MASCOMP_IDNAME")
            If dscompany.Tables.Count <> 0 Then
                databonds(dscompany, Me.cboCompany)
            End If
            Me.cboCompany.Items.Insert(0, "")
            Me.cboCompany.SelectedIndex = 0
            Me.cboCompany.SelectedValue = Session("login_cmpID")
            If Session("Login_Rank") >= 8 Then
                Me.cboCompany.Enabled = False
            End If

            '---- SERVICE CENTER (DISABLE IF RANK >= 9)
            Dim dsServiceCentere As New DataSet
            dsServiceCentere = CommonClass.Getcomidname("BB_MASSVRC_IDNAME")
            If dsServiceCentere.Tables.Count <> 0 Then
                databonds(dsServiceCentere, Me.cboServiceCenter)
            End If
            Me.cboServiceCenter.Items.Insert(0, "")
            Me.cboServiceCenter.SelectedIndex = 0
            Me.cboServiceCenter.SelectedValue = Session("login_svcID")
            If Session("Login_Rank") >= 9 Then
                Me.cboServiceCenter.Enabled = False
            End If

            '---- TECHNICIAN BASED ON SERVICE CENTER
            Dim dsTechnician As New DataSet
            dsTechnician = CommonClass.Getcomidname("BB_MASTECH_NAMEID")
            If dsTechnician.Tables.Count <> 0 Then
                databondsForTechnician(dsTechnician, Me.cboTechnician)
            End If
            Me.cboTechnician.Items.Insert(0, "")
            Me.cboTechnician.SelectedIndex = 0

            '*******************************************************************
            ' END POPULATE DROP DOWN LISTS
            '*******************************************************************

            '---- TAX RATE BASED ON SERVICE CENTER
            txtTaxRate.Text = 0
            Dim dsTax As New DataSet
            dsTax = CommonClass.Getcomidname("BB_FNCSALESTAX")
            If dsTax.Tables.Count <> 0 Then
                If dsTax.Tables(0).Rows.Count <> 0 Then
                    txtTaxRate.Text = dsTax.Tables(0).Rows(0).Item("NAME")
                    txtTaxID.Text = dsTax.Tables(0).Rows(0).Item("ID")
                End If
            End If

            txtCreatedBy.Text = userIDNamestr
            txtModifyBy.Text = userIDNamestr
            txtCreateDate.Text = Now().ToString
            txtModifyDate.Text = Now().ToString

            '---- POPULATE SERACH DATA IF ANY
            PopulateSearchData()
        Catch

        End Try

    End Sub

    Private Sub PopulateSearchData()
        '*******************************************************************
        ' BEGIN POPULATE VALUES FROM SEARCH
        '*******************************************************************
        If Session("htSvcBill") Is Nothing Then
            Exit Sub
        End If
        Dim htSvcBill As New Hashtable

        Me.RMStxt.Text = Session("RMSNo")
        htSvcBill = Session("htSvcBill")

        Me.cboServiceBillType.SelectedValue = htSvcBill.Item("ServiceBillType")
        ServiceBillType()

        Me.cboServiceType.SelectedValue = htSvcBill.Item("ServiceType")
        Me.cboTechnician.SelectedValue = htSvcBill.Item("Technician")
        Me.txtServiceBillNo.Text = htSvcBill.Item("ServiceBillNo")

        If UCase(Me.cboServiceBillType.SelectedValue) = "WITH" Then
            Me.txtAppointmentNo.Text = htSvcBill.Item("AppointmentNo")
            Me.txtAppointmentPrefix.Text = htSvcBill.Item("AppointmentPrefix")
            Me.txtCustomer.Text = ""
            Me.txtCustomerPrefix.Text = ""
            Me.txtCustomerID.Text = ""
            GetAppointmentdetails()
        Else
            Me.txtAppointmentNo.Text = ""
            Me.txtAppointmentPrefix.Text = ""
            Me.txtCustomer.Text = htSvcBill.Item("Customer")
            Me.txtCustomerPrefix.Text = htSvcBill.Item("CustomerPrefix")
            Me.txtCustomerID.Text = htSvcBill.Item("CustomerNo")
            Me.txtDate.Text = CStr(htSvcBill.Item("Date"))
            GetCustomerRO()
        End If

        'PAYMENT DETAILS
        Me.txtCash.Text = htSvcBill.Item("Cash")
        Me.txtCheque.Text = htSvcBill.Item("Cheque")
        Me.txtCredit.Text = htSvcBill.Item("Credit")
        Me.txtCreditCard.Text = htSvcBill.Item("CreditCard")
        Me.txtOther.Text = htSvcBill.Item("Other")

        Me.txtDiscount.Text = htSvcBill.Item("Discount")
        Me.txtRemarks.Text = htSvcBill.Item("Remarks")

        Me.txtTax.Text = htSvcBill.Item("Tax")
        Dim PaymentTotal As Double = Val(txtCash.Text) + Val(txtCheque.Text) + _
                        Val(txtCredit.Text) + Val(txtCreditCard.Text) + _
                        Val(txtOther.Text)
        Me.txtTotal.Text = String.Format("{0:f2}", PaymentTotal)

        Session.Remove("htSvcBill")

        Dim ds As New DataSet
        ds = Session("dsitem")
        If ds.Tables.Count = 0 Then
            txtTotalGross.Text = "0"
            txtTotalNet.Text = "0"
            txtDiscount.Text = "0"
            txtTax.Text = "0"
            Exit Sub
        End If
        grdServiceBill.DataSource = ds
        grdServiceBill.DataBind()
        InitializeGridColumns()
        RefreshDataSet()
        ds.Clear()
        ds.Dispose()
        Session.Remove("dsitem")
    End Sub

    Private Sub RefreshDataSet()
        Try
            dsItem.Clear()

            If dsItem.Tables.Count = 0 Then
                dsItem.Tables.Add("StockList")
                dsItem.Tables("StockList").Columns.Add("ItemNo")
                dsItem.Tables("StockList").Columns.Add("Part Code")
                dsItem.Tables("StockList").Columns.Add("Part Description")
                dsItem.Tables("StockList").Columns.Add("Quantity")
                dsItem.Tables("StockList").Columns.Add("PriceID")
                dsItem.Tables("StockList").Columns.Add("UnitPrice")
                dsItem.Tables("StockList").Columns.Add("Tax")
                dsItem.Tables("StockList").Columns.Add("Amount")
            End If

            Dim intGrdRow As Integer = 0
            Dim drItem As DataRow
            Dim dblGrossTotal As Double = 0
            Dim dblTax As Double = 0

            While intGrdRow <= grdServiceBill.Rows.Count - 1
                drItem = dsItem.Tables("StockList").NewRow()
                drItem.Item(0) = intGrdRow + 1
                drItem.Item(1) = Me.grdServiceBill.Rows(intGrdRow).Cells(2).Text
                drItem.Item(2) = Me.grdServiceBill.Rows(intGrdRow).Cells(3).Text
                drItem.Item(3) = Val(Me.grdServiceBill.Rows(intGrdRow).Cells(4).Text)
                drItem.Item(4) = Me.grdServiceBill.Rows(intGrdRow).Cells(5).Text
                drItem.Item(5) = String.Format("{0:f2}", Val(Me.grdServiceBill.Rows(intGrdRow).Cells(6).Text))
                drItem.Item(6) = String.Format("{0:f2}", Val(Me.grdServiceBill.Rows(intGrdRow).Cells(7).Text))
                drItem.Item(7) = String.Format("{0:f2}", Val(Me.grdServiceBill.Rows(intGrdRow).Cells(8).Text))

                dsItem.Tables("StockList").Rows.Add(drItem)

                dblGrossTotal = dblGrossTotal + Val(Me.grdServiceBill.Rows(intGrdRow).Cells(8).Text)
                dblTax = dblTax + Val(Me.grdServiceBill.Rows(intGrdRow).Cells(7).Text)
                intGrdRow = intGrdRow + 1
            End While

            If cboServiceType.SelectedValue <> "CP" Then
                txtTotalGross.Text = String.Format("{0:f2}", dblGrossTotal)
                txtTotalNet.Text = String.Format("{0:f2}", dblGrossTotal - Val(txtDiscount.Text))
                txtTax.Text = String.Format("{0:f2}", dblTax)
            End If
        Catch

        End Try

    End Sub

    Private Sub RetainValuesBeforeSearch()
        Try
            Dim htSvcBill As New Hashtable
            htSvcBill.Item("ServiceBillType") = Me.cboServiceBillType.SelectedValue
            htSvcBill.Item("ServiceType") = Me.cboServiceType.SelectedValue
            htSvcBill.Item("Technician") = Me.cboTechnician.SelectedValue
            htSvcBill.Item("AppointmentNo") = Me.txtAppointmentNo.Text
            htSvcBill.Item("AppointmentPrefix") = Me.txtAppointmentPrefix.Text
            htSvcBill.Item("Cash") = Me.txtCash.Text
            htSvcBill.Item("Cheque") = Me.txtCheque.Text
            htSvcBill.Item("Credit") = Me.txtCredit.Text
            htSvcBill.Item("CreditCard") = Me.txtCreditCard.Text
            htSvcBill.Item("Date") = Request.Form("txtDate") 'CStr(Me.txtDate.Text)
            htSvcBill.Item("Discount") = Me.txtDiscount.Text
            htSvcBill.Item("Other") = Me.txtOther.Text
            htSvcBill.Item("Remarks") = Me.txtRemarks.Text
            htSvcBill.Item("ServiceBillNo") = Me.txtServiceBillNo.Text
            htSvcBill.Item("Tax") = Me.txtTax.Text
            htSvcBill.Item("Company") = Me.cboCompany.SelectedValue
            htSvcBill.Item("ServiceCenter") = Me.cboServiceCenter.SelectedValue
            htSvcBill.Item("SBUNO") = 1
            Session("dsitem") = dsItem
            Session("htSvcBill") = htSvcBill
        Catch

        End Try

    End Sub

    Private Sub SaveServiceBill()
        Try
            Dim invoice As String
            Dim clsSvcBill As New clsServiceBillUpdate

            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")

            Dim ResultCtr As Integer = 0
            Dim ResultSBC As Integer = 0

            clsSvcBill.TransactionNo = Me.txtInvoiceNo.Text
            clsSvcBill.TransactionPrefix = Me.txtInvoicePf.Text
            clsSvcBill.ServiceBill = txtServiceBillNo.Text

            If (txtServiceBillNo.Text <> "") Then
                Dim dup As Integer = clsSvcBill.CheckDuplicateServiceBillNo()

                If (dup > 0) Then
                    lblError.Text = objXmlTr.GetLabelName("StatusMessage", "SBA-DUPSBN")
                    lblError.Visible = True
                    MessageBox1.Alert(lblError.Text)
                    Exit Sub
                End If
            End If


            clsSvcBill.ServiceBillType = Me.cboServiceBillType.SelectedValue
            clsSvcBill.InvoiceDate = CDate(Request.Form("txtDate")) 'CDate(Me.txtDate.Text)
            clsSvcBill.Technician = Me.cboTechnician.SelectedValue
            clsSvcBill.ServiceType = Me.cboServiceType.SelectedValue
            clsSvcBill.AppointmentPrefix = Me.txtAppointmentPrefix.Text
            clsSvcBill.AppointmentNo = Me.txtAppointmentNo.Text
            clsSvcBill.CustomerPrefix = Me.txtCustomerPrefix.Text
            clsSvcBill.CustomerNo = Me.txtCustomerID.Text
            clsSvcBill.CustomerName = Me.txtCustomer.Text
            clsSvcBill.Country = Me.cboCountry.SelectedValue
            clsSvcBill.Company = Me.cboCompany.SelectedValue
            clsSvcBill.ServiceCenter = Me.cboServiceCenter.SelectedValue
            clsSvcBill.CreateBy = Session("userID").ToString().ToUpper
            clsSvcBill.CreateDate = CDate(Me.txtCreateDate.Text)
            clsSvcBill.ModifyBy = Session("userID").ToString().ToUpper
            clsSvcBill.ModifyDate = CDate(Me.txtModifyDate.Text)
            clsSvcBill.TotalGross = Me.txtTotalGross.Text
            clsSvcBill.Discount = Me.txtDiscount.Text
            clsSvcBill.TotalNet = Me.txtTotalNet.Text
            clsSvcBill.Tax = Me.txtTax.Text
            clsSvcBill.DiscRem = Me.txtRemarks.Text
            clsSvcBill.TaxPercentage = Val(Me.txtTaxRate.Text)
            clsSvcBill.TaxID = Me.txtTaxID.Text
            clsSvcBill.CashAmount = Val(Me.txtCash.Text)
            clsSvcBill.ChequeAmount = Val(Me.txtCheque.Text)
            clsSvcBill.CreditAmount = Val(Me.txtCredit.Text)
            clsSvcBill.CreditCardAmount = Val(Me.txtCreditCard.Text)
            clsSvcBill.OtherPaymentAmount = Val(Me.txtOther.Text)
            clsSvcBill.Status = Me.cboStatus.SelectedValue
            clsSvcBill.IPAddress = Request.UserHostAddress.ToString()
            clsSvcBill.SessionID = Session("login_session")
            clsSvcBill.AppointmentStatus = "BL"
            clsSvcBill.ROSerialNo = Me.cboRO.SelectedValue

            If UCase(Me.cboServiceBillType.SelectedValue) = "WITO" Then
                Dim Appno As String
                'Appno = clsSvcBill.SaveDummyAppointment
                Appno = ""
                If Appno = "-1" Then
                    lblError.Text = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRSAVING")
                    MessageBox1.Alert(lblError.Text)
                    lblError.Visible = True
                    Exit Sub
                End If
                'txtAppointmentPrefix.Text = clsSvcBill.AppointmentPrefix
                'txtAppointmentNo.Text = clsSvcBill.AppointmentNo
                txtAppointmentPrefix.Text = ""
                txtAppointmentNo.Text = ""

            End If

            RefreshDataSet()
            clsSvcBill.InvoiceDetails = dsItem

            invoice = clsSvcBill.SaveServiceBill

            lnkGeneratePackingList.Enabled = True
            lnkGenerateTaxInvoice.Enabled = True

            If invoice <> "-1" Then
                lnkAddItem.Enabled = False
                lnkSave.Enabled = False
                lnkNew.Enabled = True

                Me.txtInvoiceNo.Text = clsSvcBill.TransactionNo
                Me.txtInvoicePf.Text = clsSvcBill.TransactionPrefix
                Me.txtServiceBillNo.Text = clsSvcBill.ServiceBill

                lblError.Text = objXmlTr.GetLabelName("StatusMessage", "SBA-SUCSAVING")
                MessageBox1.Alert(lblError.Text)
                lblError.Visible = True

                ImageButton1.Enabled = False
                cboServiceBillType.Enabled = False
                txtServiceBillNo.Enabled = False
                cboTechnician.Enabled = False
                cboServiceType.Enabled = False
                btnSearchAppNo.Enabled = False
                lnkAddItem.Enabled = False
                txtDiscount.Enabled = False
                txtCash.Enabled = False
                txtCheque.Enabled = False
                txtCredit.Enabled = False
                txtCreditCard.Enabled = False
                txtOther.Enabled = False
                grdServiceBill.Enabled = False
                btnSearchCustomer.Enabled = False
                cboRO.Enabled = False
                cboCompany.Enabled = False
                cboServiceCenter.Enabled = False

                'CHECK IF TECHNICIAN GOT PACKING LIST TO GENERATE 
                'ResultCtr = 0
                'ResultCtr = clsSvcBill.CheckGenPackList
                'If ResultCtr > 0 Then
                '    Me.lnkGeneratePackingList.Enabled = True
                'Else
                '    Me.lnkGeneratePackingList.Enabled = False
                'End If

                Dim clsgenprf As New clsGenPRF
                Dim clsgenSBC As New clsGenSBC
                ResultCtr = 0
                ResultSBC = 0
                ResultCtr = clsSvcBill.CheckGenPackList

                clsgenprf.InvoiceDate = Me.txtDate.Text
                clsgenprf.Technician = Me.cboTechnician.SelectedValue
                clsgenprf.ServiceCenter = Me.cboServiceCenter.SelectedValue

                clsgenprf.logctrid = Session("login_ctryID").ToString.ToUpper
                clsgenprf.logcompanyid = Session("login_cmpID").ToString.ToUpper
                clsgenprf.logserviceid = Session("login_svcID").ToString.ToUpper
                clsgenprf.loguserid = Session("userID").ToString().ToUpper()

                ResultCtr = clsgenprf.CheckGenPackList
                clsgenSBC.Technician = Me.cboTechnician.SelectedValue
                ResultSBC = clsgenSBC.CheckSBC
                If ResultSBC > 0 Then
                    Me.lnkGenerateSBC.Enabled = True
                Else
                    Me.lnkGenerateSBC.Enabled = False
                End If

                If ResultCtr > 0 Then
                    Me.lnkGeneratePackingList.Enabled = True
                Else
                    Me.lnkGeneratePackingList.Enabled = False
                End If

            Else
                Dim lstrError As String = ""

                lstrError = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRSAVING") & ".\n Please pass this Message to your Administrator (" & clsSvcBill.ErrorMessage & ")"
                lblError.Text = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRSAVING")
                MessageBox1.Alert(lstrError)
                lblError.Visible = True
            End If
        Catch

        End Try
    End Sub

    Private Sub ServiceBillType()
        Try
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")

            If UCase(Me.cboServiceBillType.SelectedValue) = "WITH" Then
                Label4.Visible = True
                Label8.Visible = False
                'REQUIRED FIELD VALIDATORS
                Me.rfvAppointmentNo.Visible = True
                Me.rfvCustomerNo.Visible = False
                Me.rfvRO.Visible = False

                'BUTTONS
                Me.btnSearchAppNo.Enabled = True
                Me.btnSearchCustomer.Enabled = False

                ' Added by Ryan Estandarte 28 Mar 2012
                btnRetrieve.Enabled = True
                txtAppointmentPrefix.Enabled = True
                txtAppointmentNo.Enabled = True

                'RO
                Me.txtROSerialNo.Visible = False
                Me.cboRO.Enabled = True
                Me.cboRO.Visible = True

                If Session("login_rank") >= 8 Then
                    Me.cboCompany.Enabled = False
                End If

                If Session("login_rank") >= 9 Then
                    Me.cboServiceCenter.Enabled = False
                End If

                Me.txtCustomerPrefix.Text = ""
                Me.txtCustomerID.Text = ""
                Me.txtCustomer.Text = ""
                Me.txtAppointmentPrefix.Text = ""
                Me.txtAppointmentNo.Text = ""
                Me.txtCustomerPrefix.Text = ""
                Me.txtCustomerID.Text = ""
                Me.txtCustomer.Text = ""

            Else
                Label4.Visible = False
                Label8.Visible = True
                Dim cls As New clsCommonClass

                cls.spctr = Me.cboCountry.SelectedValue().ToString().ToUpper()
                cls.spstat = Session("login_cmpID").ToString()
                cls.sparea = Session("Login_SVCID").ToString()
                cls.rank = Session("login_rank").ToString()

                Dim dscompany As New DataSet
                dscompany = cls.Getcomidname("BB_MASCOMP_IDNAME")
                If dscompany.Tables.Count <> 0 Then
                    databonds(dscompany, Me.cboCompany)
                End If
                Me.cboCompany.Items.Insert(0, "")
                Me.cboCompany.SelectedValue = Session("login_cmpID").ToString()
                Me.cboCompany.Enabled = False
                If Session("Login_rank") >= 8 Then
                    Me.cboCompany.Enabled = False
                End If

                Dim dsServiceCentere As New DataSet

                dsServiceCentere = cls.Getcomidname("BB_MASSVRC_IDNAME")
                If dsServiceCentere.Tables.Count <> 0 Then
                    databonds(dsServiceCentere, Me.cboServiceCenter)
                End If
                Me.cboServiceCenter.Items.Insert(0, "")
                Me.cboServiceCenter.SelectedValue = Session("Login_SVCID").ToString()
                If Session("Login_rank") >= 9 Then
                    Me.cboServiceCenter.Enabled = False
                End If

                Dim dsTechnician As New DataSet
                dsTechnician = cls.Getcomidname("BB_MASTECH_NAMEID")
                If dsTechnician.Tables.Count <> 0 Then
                    databondsForTechnician(dsTechnician, Me.cboTechnician)
                End If
                Me.cboTechnician.Items.Insert(0, "")

                Dim dsTax As New DataSet
                dsTax = cls.Getcomidname("BB_FNCSALESTAX")
                If dsTax.Tables(0).Rows.Count <> 0 Then
                    txtTaxRate.Text = dsTax.Tables(0).Rows(0).Item("NAME").ToString()
                    txtTaxID.Text = dsTax.Tables(0).Rows(0).Item("ID").ToString()
                Else
                    txtTaxRate.Text = 0
                    txtTaxID.Text = ""
                End If

                Me.rfvRO.Visible = False
                Me.rfvAppointmentNo.Visible = False
                Me.rfvCustomerNo.Visible = True

                'Me.rfvRO.Visible = True
                Me.btnSearchAppNo.Enabled = False
                Me.btnSearchCustomer.Enabled = True
                Me.txtROSerialNo.Visible = False
                Me.cboRO.Enabled = True
                Me.cboRO.Visible = True
                Me.cboRO.Items.Clear()
                ' Added by Ryan Estandarte 28 Mar 2012
                btnRetrieve.Enabled = False
                txtAppointmentPrefix.Enabled = False
                txtAppointmentNo.Enabled = False

                If Session("login_rank") < 8 Then
                    Me.cboCompany.Enabled = True
                End If

                If Session("login_rank") < 9 Then
                    Me.cboServiceCenter.Enabled = True
                End If

                Me.cboServiceType.Enabled = True
                Me.cboTechnician.Enabled = True
                Me.JCalendar1.Enabled = True

                Me.txtAppointmentPrefix.Text = ""
                Me.txtAppointmentNo.Text = ""
                Me.txtCustomerPrefix.Text = ""
                Me.txtCustomerID.Text = ""
                Me.txtCustomer.Text = ""
            End If
        Catch

        End Try
    End Sub

    Private Function ValidateDetails() As Boolean
        Try
            ValidateDetails = False

            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            lblError.Text = ""
            lblError.Visible = True

            ComputeLineTotal()

            If Trim(Me.txtPartCode.Text) = "" Then
                lblError.Text = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRPARTID")
                MessageBox1.Alert(lblError.Text)
                txtPartCode.Focus()
                Exit Function
            End If
            If Trim(Me.txtPartName.Text) = "" Then
                lblError.Text = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRPARTNAME")
                MessageBox1.Alert(lblError.Text)
                Exit Function
            End If
            
            If Trim(cboPriceID.SelectedValue) = "" Then
                lblError.Text = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRPRICEID")
                MessageBox1.Alert(lblError.Text)
                cboPriceID.Focus()
                Exit Function
            End If

            If Trim(Me.txtUnitPrice.Text) = "" Then
                lblError.Text = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRPRICE")
                MessageBox1.Alert(lblError.Text)
                Exit Function
            End If
            If Val(Me.txtQty.Text) <= 0 Then
                lblError.Text = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRQTY")
                MessageBox1.Alert(lblError.Text)
                txtQty.Focus()
                Exit Function
            End If

            Me.txtLineTotal.Text = String.Format("{0:f2}", Val(Me.txtLineTotal.Text))
            Me.txtQty.Text = String.Format("{0:f0}", Val(Me.txtQty.Text))
            Me.txtUnitPrice.Text = String.Format("{0:f2}", Val(Me.txtUnitPrice.Text))
            lblError.Visible = False
            Return True

        Catch

        End Try

    End Function

    Private Function ValidateHeader() As Boolean
        Try
            ValidateHeader = False
            Dim PaymentTotal As Double
            RefreshDataSet()
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            lblError.Text = ""
            lblError.Visible = True

            ComputeNetTotal()
            lblError.Visible = True

            If Me.cboServiceBillType.SelectedIndex = 0 Then
                If Me.txtAppointmentNo.Text = "" Then
                    lblError.Text = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRAPPOINTMENTNO")
                    MessageBox1.Alert(lblError.Text)
                    Exit Function
                End If
            End If

            If Not IsDate(Me.txtDate.Text) Then
                lblError.Text = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRDATE")
                MessageBox1.Alert(lblError.Text)
                Exit Function
            End If

            If txtServiceBillNo.Text <> "" Then
                Dim varAllowed As New RegularExpressions.Regex("^[0-9a-zA-Z()-]*$")
                If Not (varAllowed.IsMatch(txtServiceBillNo.Text)) Then
                    lblError.Text = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRSERVICEBILL")
                    MessageBox1.Alert(lblError.Text)
                    Exit Function
                End If
            End If

            If ServiceOrderNoIsBilled(txtAppointmentNo.Text, txtAppointmentPrefix.Text, cboCountry.SelectedValue) Then
                lblError.Text = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRSERVICEBILL")
                MessageBox1.Alert(lblError.Text)
                Exit Function
            End If

            If Me.cboServiceType.SelectedValue = "" Then
                lblError.Text = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRSERVICETYPE")
                MessageBox1.Alert(lblError.Text)
                Exit Function
            End If

            If Me.cboTechnician.SelectedValue = "" Then
                lblError.Text = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRTECHNICIAN")
                MessageBox1.Alert(lblError.Text)
                Exit Function
            End If

            If Me.txtCustomerID.Text = "" Then
                lblError.Text = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRCUSTOMERNO")
                MessageBox1.Alert(lblError.Text)
                Exit Function
            End If

            PaymentTotal = Val(txtCash.Text) + Val(txtCheque.Text) + _
                    Val(txtCredit.Text) + Val(txtCreditCard.Text) + _
                    Val(txtOther.Text)

            If Val(txtTotalNet.Text) < 0 Then
                lblError.Text = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRDISCMORE")
                If lblError.Text = "" Then
                    lblError.Text = "Discount Must be Less Than Total"
                End If

                MessageBox1.Alert(lblError.Text)

                Exit Function
            End If

            If PaymentTotal <> Val(txtTotalNet.Text) Then
                lblError.Text = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRPAYTOTAL")
                If lblError.Text = "" Then
                    lblError.Text = "Payment Must be Equal the Net Total"
                End If
                MessageBox1.Alert(lblError.Text)

                Exit Function
            End If

            If Val(txtDiscount.Text) <> 0 And Trim(Me.txtRemarks.Text) = "" Then
                lblError.Text = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRDISCREM")
                If lblError.Text = "" Then
                    lblError.Text = "Discount Remark is Required when you Enter a discount"
                End If

                MessageBox1.Alert(lblError.Text)

                Exit Function
            End If

            Me.txtTotal.Text = String.Format("{0:f2}", PaymentTotal)
            lblError.Visible = False
            Return True
        Catch

        End Try

    End Function

#End Region

    Protected Sub form1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles form1.Load

    End Sub


#Region "Ajax"
    <AjaxPro.AjaxMethod()> _
    Public Function DisplayPayment(ByVal Cash As String, ByVal Cheque As String, ByVal Credit As String, ByVal CreditCard As String, ByVal OtherPayment As String) As String
        Dim ldblTotalPayment As Double
        ldblTotalPayment = Val(Cash) + Val(Cheque) + Val(Credit) + Val(CreditCard) + Val(OtherPayment)
        DisplayPayment = Format(ldblTotalPayment, "0.00")
    End Function

    <AjaxPro.AjaxMethod()> _
   Public Function DisplayNetAfterDiscount(ByVal DiscountAmount As String, ByVal GrossTotal As String) As String
        Dim ldlNetAfterDiscount As Double
        ldlNetAfterDiscount = Val(GrossTotal) - Val(DiscountAmount)
        DisplayNetAfterDiscount = Format(ldlNetAfterDiscount, "0.00")
    End Function

    <AjaxPro.AjaxMethod()> _
       Public Function FormatNumber(ByVal Amount As String) As String
        FormatNumber = Format(Val(Amount), "0.00")
    End Function

    <AjaxPro.AjaxMethod()> _
    Function GetServiceCenterList(ByVal CountryID As String, ByVal CompanyID As String, ByVal ServiceCenterID As String, ByVal Rank As String) As ArrayList
        Dim ServiceCenter As New clsCommonClass

        ServiceCenter.spctr = CountryID
        ServiceCenter.spstat = CompanyID
        ServiceCenter.sparea = ServiceCenterID
        ServiceCenter.rank = Rank

        Dim dsServiceCentere As New DataSet
        dsServiceCentere = ServiceCenter.Getcomidname("BB_MASSVRC_IDNAME")

        Dim strTemp As String
        Dim Listas As New ArrayList(dsServiceCentere.Tables(0).Rows.Count)
        Dim i As Integer = 0
        Listas.Add("")
        Try
            For i = 0 To dsServiceCentere.Tables(0).Rows.Count - 1
                strTemp = dsServiceCentere.Tables(0).Rows(i).Item(0).ToString & ":" & dsServiceCentere.Tables(0).Rows(i).Item(0).ToString & "-" & dsServiceCentere.Tables(0).Rows(i).Item(1).ToString
                Listas.Add(strTemp)
            Next
        Catch
        End Try

        Return Listas
    End Function

    <AjaxPro.AjaxMethod()> _
    Function GetTechnicianList(ByVal CountryID As String, ByVal CompanyID As String, ByVal ServiceCenterID As String, ByVal Rank As String) As ArrayList
        Dim Technician As New clsCommonClass

        Technician.spctr = CountryID
        Technician.spstat = CompanyID
        Technician.sparea = ServiceCenterID
        Technician.rank = Rank

        Dim dsTechnician As New DataSet
        dsTechnician = Technician.Getcomidname("BB_MASTECH_IDNAME")

        Dim strTemp As String
        Dim Listas As New ArrayList(dsTechnician.Tables(0).Rows.Count)
        Dim i As Integer = 0
        Listas.Add("")
        Try
            For i = 0 To dsTechnician.Tables(0).Rows.Count - 1
                strTemp = dsTechnician.Tables(0).Rows(i).Item(0).ToString & ":" & dsTechnician.Tables(0).Rows(i).Item(1).ToString & "-" & dsTechnician.Tables(0).Rows(i).Item(0).ToString
                Listas.Add(strTemp)
            Next
        Catch
        End Try

        Return Listas
    End Function

    ''' <summary>
    ''' Populates the Price List dropdown based on the part id
    ''' </summary>
    ''' <param name="countryID"></param>
    ''' <param name="partID"></param>
    ''' <returns>List of prices</returns>
    ''' <remarks>Added by Ryan Aimel J. Estandarte 5 March 2012</remarks>
    <AjaxPro.AjaxMethod()> _
    Function PopulatePriceList(ByVal countryID As String, ByVal partID As String, ByVal rank As String) As ArrayList
        Dim commonClass As New clsCommonClass

        commonClass.spctr = countryID
        commonClass.spstat = "ACTIVE"
        commonClass.sparea = partID
        commonClass.rank = rank

        Dim dsPriceList As DataSet = commonClass.Getcomidname("dbo.BB_FNCUSB1_SelPriceList")

        Dim dr As DataRow = dsPriceList.Tables(0).NewRow()

        Dim priceList As New ArrayList(dsPriceList.Tables(0).Rows.Count)

        priceList.Add(":")

        For i As Integer = 0 To dsPriceList.Tables(0).Rows.Count - 1
            Dim list As String = dsPriceList.Tables(0).Rows(i)("ID").ToString() + ":" + dsPriceList.Tables(0).Rows(i)(0).ToString() + "-" + dsPriceList.Tables(0).Rows(i)(1).ToString()
            priceList.Add(list)
        Next

        Return priceList

    End Function

    <AjaxPro.AjaxMethod()> _
    Function GetTaxRate(ByVal CountryID As String, ByVal ServiceCenterID As String, ByVal Rank As String) As String
        Dim lstrValueID As String
        Dim lstrValueTax As String
        Dim lstrValueString As String
        Dim Technician As New clsCommonClass

        Technician.spctr = CountryID
        Technician.sparea = ServiceCenterID
        Technician.rank = Rank

        Dim dsTax As New DataSet
        dsTax = Technician.Getcomidname("BB_FNCSALESTAX")
        If dsTax.Tables(0).Rows.Count <> 0 Then
            lstrValueTax = dsTax.Tables(0).Rows(0).Item("NAME")
            lstrValueID = dsTax.Tables(0).Rows(0).Item("ID")
            lstrValueString = lstrValueID & ":" & lstrValueTax
        Else
            lstrValueString = ""
        End If

        GetTaxRate = lstrValueString
    End Function

    <AjaxPro.AjaxMethod()> _
    Function GetPartCodeAndName(ByVal CountryID As String, ByVal PartCode As String, ByVal PriceID As String) As String
        Dim lstrOutput As String
        Dim lstrError As String

        lstrError = ""

        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")

        Dim clsPart As New clsServiceBillUpdate
        clsPart.Country = CountryID
        clsPart.PartID = PartCode

        lstrOutput = clsPart.GetPartName

        If lstrOutput <> "" Then
        Else
            lstrError = objXm.GetLabelName("StatusMessage", "SBA-ERRPARCODE")
        End If

        GetPartCodeAndName = lstrError & ":" & lstrOutput
    End Function

    <AjaxPro.AjaxMethod()> _
    Function GetPriceIDAndPrice(ByVal CountryID As String, ByVal PartCode As String, ByVal PriceID As String) As String
        Dim lstrOutput As String
        Dim lstrError As String

        lstrError = ""

        Dim objXm As New clsXml
        objXm.XmlFile = ConfigurationSettings.AppSettings("StatMsg")

        Dim clsPart As New clsServiceBillUpdate
        clsPart.Country = CountryID
        clsPart.PartID = PartCode
        clsPart.PriceID = PriceID
        'txtUnitPrice.Text = clsPart.GetPriceID
        lstrOutput = clsPart.GetPriceID

        If Len(lstrOutput) <> 0 Then
        Else
            lstrError = objXm.GetLabelName("StatusMessage", "SBA-ERRPRICE")
            'txtPriceID.Focus()
        End If

        GetPriceIDAndPrice = lstrError & ":" & lstrOutput
    End Function

    <AjaxPro.AjaxMethod()> _
     Function GetLineAmount(ByVal Qty As String, ByVal UnitPrice As String) As String
        Dim lstrError As String
        Dim lstrLineAmount As String
        Dim lstrUnitPrice As String
        Dim lstrQty As String

        lstrError = ""
        lstrUnitPrice = UnitPrice
        lstrQty = Qty

        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")

        If IsNumeric(Qty) Then
            If Val(Qty) <= 0 Then
                lstrQty = "1"
                lstrError = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRQTY")
            End If
            If IsNumeric(UnitPrice) Then
                lstrLineAmount = String.Format("{0:f2}", Val(UnitPrice) * (Qty))
            Else
                lstrQty = "1"
                lstrUnitPrice = ""
                lstrLineAmount = "0"
            End If
        Else
            lstrQty = "1"
            lstrLineAmount = "0"
        End If

        GetLineAmount = lstrError & ":" & lstrQty & ":" & lstrUnitPrice & ":" & lstrLineAmount
    End Function

#End Region

    Protected Sub lnkGenerateSBC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkGenerateSBC.Click
        Try
            Dim clsGPL As New clsServiceBillUpdate

            Dim clsgenSBC As New clsGenSBC
            Dim ResultCtr As Integer = 0

            clsgenSBC.InvoiceDate = Me.txtDate.Text
            clsgenSBC.Technician = Me.cboTechnician.SelectedValue
            clsgenSBC.ServiceCenter = Me.cboServiceCenter.SelectedValue

            clsgenSBC.logctrid = Session("login_ctryID").ToString.ToUpper
            'clsgenprf.logcompanyid = Session("login_cmpID").ToString.ToUpper
            clsgenSBC.logserviceid = Session("login_svcID").ToString.ToUpper
            clsgenSBC.loguserid = Session("userID").ToString().ToUpper()

            ResultCtr = 0

            ResultCtr = clsgenSBC.CheckSBC
            Dim SBCNo As String

            If ResultCtr > 0 Then
                SBCNo = clsgenSBC.GenerateSBC()

                If SBCNo <> "" Then
                    ShowSBCReport(SBCNo)
                Else
                    lblError.Text = "Error Showing Report"
                    MessageBox1.Alert(lblError.Text)
                End If
            Else
                lblError.Text = "No SBC to be generate."
                MessageBox1.Alert(lblError.Text)
            End If

            lnkSave.Enabled = False

        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub
    Private Sub ShowSBCReport(ByVal SBCNo As String)
        Dim script As String = "window.open('../report/RptReprintSBCSearch.aspx?FIV1_SBCNO=" + SBCNo + "')"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "RptReprintSBCSearch", script, True)
    End Sub

    ''' <summary>
    ''' Enables/disables controls if the 
    ''' </summary>
    ''' <param name="isCP"></param>
    ''' <remarks></remarks>
    Private Sub EnableDisableControlsIfCollectPayment(ByVal isCP As Boolean)
        txtTotalGross.ReadOnly = Not isCP

        'parts section
        txtPartCode.Enabled = Not isCP
        txtPartName.Enabled = Not isCP
        cboPriceID.Enabled = Not isCP
        txtUnitPrice.Enabled = Not isCP
        txtQty.Enabled = Not isCP
        txtLineTotal.Enabled = Not isCP
        lnkAddItem.Enabled = Not isCP

        txtTotalGross.AutoPostBack = isCP
        txtDiscount.AutoPostBack = isCP
    End Sub

    ''' <summary>
    ''' Retrieves the appointment details based on the service order no.
    ''' </summary>
    ''' <remarks>Added by Ryan Estandarte 14 Mar 2012</remarks>
    Protected Sub BtnRetrieveClick(ByVal sender As Object, ByVal e As EventArgs) Handles btnRetrieve.Click
        GetAppointmentdetails()
        cboRO.Enabled = True
        cboServiceType.Enabled = True

        'Dim Conn As New SqlConnection
        'Conn.ConnectionString = ConfigurationManager.ConnectionStrings("ConnectionString").ToString

        'Using Comm As New SqlClient.SqlCommand("Select isnull(RMS_NO,'') from dbo.RMS_FIL where APPT_TRNNO = '" & txtAppointmentNo.Text & "'", Conn)
        '    Conn.Open()
        '    Using readerObj As SqlClient.SqlDataReader = Comm.ExecuteReader
        '        While readerObj.Read
        '            RMStxt.Text = readerObj("RMS_NO").ToString
        '        End While
        '    End Using
        '    Conn.Close()
        'End Using
    End Sub

    ''' <summary>
    ''' Computes for the total
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Added by Ryan Estandarte 22 Aug 2012</remarks>
    Protected Sub ComputeTotal(ByVal sender As Object, ByVal e As EventArgs)
        txtTotalNet.Text = String.Format("{0:f2}", Val(txtTotalGross.Text) - Val(txtDiscount.Text))
    End Sub

    Protected Sub lnkGenerateTaxInvoice_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkGenerateTaxInvoice.Click
        Session("InvoiceNo") = Me.txtInvoiceNo.Text
        Session("InvoicePf") = Me.txtInvoicePf.Text

        Dim script As String = "window.open('../function/PrintTaxInvoice.aspx')"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "PrintTaxInvoice", script, True)
    End Sub
End Class