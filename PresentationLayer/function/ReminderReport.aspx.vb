Imports BusinessEntity
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared
Imports TransFile
Imports System.io
Imports System.Data

Partial Class PresentationLayer_function_ReminderReport
    Inherits System.Web.UI.Page
    Dim objXmlTr As New clsXml
    Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
    Dim ReminderReportDoc As New ReportDocument()
    Dim clsReport As New ClsCommonReport


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        GeneratePDF()
    End Sub

    Sub GeneratePDF()
        'Dim ReminderReportDoc As ReportDocument = Session("ReminderReportDoc")


        ''向客户端传文件
        'Dim sr As Stream = ReminderReportDoc.ExportToStream(ExportFormatType.PortableDocFormat)
        'Response.Clear()
        'Dim bSuccess As Boolean = dowload.ResponseFile(Request, Response, "Reminder.pdf", sr, 1024000)
        'If bSuccess = False Then

        'End If
        'sr.Close()
        'Response.End()

        Dim strHead As Array = Array.CreateInstance(GetType(String), 6)
        Dim ds As DataSet = Session("ReminderDs")

        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        strHead(0) = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-REMINDER-RPTH1")
        strHead(1) = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-REMINDER-RPTH2")
        strHead(2) = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-REMINDER-RPTH3")
        strHead(3) = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-REMINDER-RPTH4")
        strHead(4) = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-REMINDER-RPTH5")
        strHead(5) = objXmlTr.GetLabelName("EngLabelMsg", "BB-FUN-REMINDER-RPTH6")

        Dim lstrPdfFileName As String = "ReminderSlip_" & Session("login_session") & ".pdf"

        Try
            With clsReport
                .ReportFileName = "reminder.rpt"
                .SetReportDocument(ds, strHead)
                .PdfFileName = lstrPdfFileName
                .ExportPdf()
            End With

            Response.Redirect(clsReport.PdfUrl)

        Catch err As Exception
            'System.IO.File.Delete(lstrPhysicalFile)
            Response.Write("<BR>")
            Response.Write(err.Message.ToString)

        End Try

    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        ReminderReportDoc.Dispose()

    End Sub
End Class
