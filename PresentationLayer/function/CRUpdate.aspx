<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CRUpdate.aspx.vb" Inherits="PresentationLayer_Function_CRUpdate"%>

<%@ Register Assembly="JCalendar" Namespace="JCalendar" TagPrefix="cc2" %>

<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>CR Update</title>
    <meta http-equiv="Content-Type" content="text/html; charset=gb2312">
	<LINK href="../css/style.css" type="text/css" rel="stylesheet">
</head>
<body style="text-align: left">
    <form id="form1" runat="server">
    <div>
        <table bgcolor="#b7e6e6" border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td background="../graph/title_bg.gif" width="1%">
                    <img height="24" src="../graph/title1.gif" width="5" /></td>
                <td background="../graph/title_bg.gif" class="style2" width="98%">
                    <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
                <td align="right" background="../graph/title_bg.gif" width="1%">
                    <img height="24" src="../graph/title_2.gif" width="5" /></td>
            </tr>
        </table>
         <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
             <tr>
							<TD class="style2" width="98%" background="../graph/title_bg.gif" style="height: 12px">
                                <font color=red><asp:Label ID="errlab" runat="server" Text="Label" Visible=false></asp:Label></font></TD>
             </tr>
			 </TABLE>
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
            ShowSummary="False" />
        <cc1:MessageBox ID="MessageBox1" runat="server" />
        <br />
        &nbsp; &nbsp;&nbsp;
        <asp:Panel ID="Panel1" runat="server" Height="355px" Width="100%" Enabled="False">
        <table bgcolor="#b7e6e6" width="100%">
       <tr bgcolor="#ffffff">
	<td colspan = 4>
                            <asp:Label ID="lblNoteUP" runat="server" ForeColor="Red" Text="Label"></asp:Label>
	</td>
</tr>
<tr bgcolor="#ffffff">
<td colspan = 4>
                        	&nbsp;
       	</td>
</tr>
 
            <tr bgcolor="#ffffff">
                <td style="width: 202px">
                    <asp:Label ID="sbtlab" runat="server" Text="Label" Width="95px"></asp:Label></td>
                <td style="width: 183px">
                    <asp:DropDownList ID="sbtyDropDownList" runat="server" Width="151px">
                    </asp:DropDownList></td>
                <td style="width: 114px">
                    <asp:Label ID="invoicelab" runat="server" Text="Label" Width="95px"></asp:Label></td>
                <td style="width: 179px">
                    <asp:TextBox ID="invoicelabtext" runat="server" Width="160px" ReadOnly="True"></asp:TextBox></td>
            </tr>
            <tr bgcolor="#ffffff">
                <td style="width: 202px; height: 21px">
                    <asp:Label ID="datelab" runat="server" Text="Label" Width="95px"></asp:Label></td>
                <td style="width: 183px; height: 21px">
                    <asp:TextBox ID="invoicebox" runat="server" Width="144px" ReadOnly="True"></asp:TextBox></td>
                <td style="width: 114px; height: 21px">
                    <asp:Label ID="technicianlab" runat="server" Text="Label" Width="95px"></asp:Label></td>
                <td style="width: 179px; height: 21px">
                    <asp:DropDownList ID="technicianDropDownList" runat="server" Width="160px">
                    </asp:DropDownList></td>
            </tr>
            <tr bgcolor="#ffffff">
                <td style="width: 202px">
                    <asp:Label ID="scenterlab" runat="server" Text="Label" Width="95px"></asp:Label></td>
                <td style="width: 183px">
                    <asp:TextBox ID="scenterbox" runat="server" ReadOnly="True" Width="143px"></asp:TextBox></td>
                <td style="width: 114px">
                    <asp:Label ID="stlab" runat="server" Text="Label" Width="95px"></asp:Label></td>
                <td style="width: 179px">
                    <asp:DropDownList ID="stDropDownList" runat="server" Width="160px">
                    </asp:DropDownList></td>
            </tr>
            <tr bgcolor="#ffffff">
                <td style="width: 202px; height: 14px;">
                    <asp:Label ID="sblab" runat="server" Text="Label" Width="95px"></asp:Label></td>
                <td style="width: 183px; height: 14px;">
                    <asp:TextBox ID="sbBox" runat="server" ReadOnly="True" Width="142px"></asp:TextBox></td>
                <td style="width: 114px; height: 14px;">
                    </td>
                <td style="width: 179px; height: 14px;">
                    <asp:TextBox ID="txtSTAT" runat="server" Visible="False" Width="24px"></asp:TextBox></td>
            </tr>
        </table>
            <br />
        <asp:GridView ID="partGridView" runat="server" Width="100%">
            <Columns>
                <asp:TemplateField>                    
                   <ItemTemplate>
                     <%#Container.DataItemIndex + 1%>
                  </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <table bgcolor="#b7e6e6"  width=100%>
            <tr bgcolor="#ffffff">
                <td style="height: 25px">
                    <asp:Label ID="totalLab" runat="server" Text="Label"></asp:Label></td>
                <td style="width: 50px; height: 25px;">
                    <asp:TextBox ID="totalBox1" runat="server" Width="50px" ReadOnly="True"></asp:TextBox></td>
                <td style="height: 25px">
                    <asp:Label ID="discountLab" runat="server" Text="Label"></asp:Label></td>
                <td style="width: 50px; height: 25px;">
                    <asp:TextBox ID="discountBox" runat="server" Width="50px" ReadOnly="True"></asp:TextBox></td>
                <td style="height: 25px">
                    <asp:Label ID="totalLab2" runat="server" Text="Label"></asp:Label></td>
                <td style="width: 50px; height: 25px;">
                    <asp:TextBox ID="totaBox2" runat="server" Width="50px" ReadOnly="True"></asp:TextBox></td>
                <td style="height: 25px">
                    <asp:Label ID="InvoiceTaxLab" runat="server" Text="Label"></asp:Label></td>
                <td style="width: 50px; height: 25px;">
                    <asp:TextBox ID="InvoiceTaxBox" runat="server" Width="50px" ReadOnly="True"></asp:TextBox></td>
            </tr>
        </table>
            <br />
            <asp:Label ID="paylab" runat="server" Text="Label"></asp:Label><br />
        <table bgcolor="#b7e6e6" width=100%>
            <tr bgcolor="#ffffff">
                <td style="width: 100px">
                    <asp:Label ID="cashlab" runat="server" Text="Label"></asp:Label></td>
                <td style="width: 100px">
                    <asp:TextBox ID="cashtext" runat="server" ReadOnly="True"></asp:TextBox></td>
                <td style="width: 95px">
                    <asp:Label ID="creditlab" runat="server" Text="Label"></asp:Label></td>
                <td style="width: 100px">
                    <asp:TextBox ID="creditText" runat="server" ReadOnly="True"></asp:TextBox></td>
            </tr>
            <tr bgcolor="#ffffff">
                <td style="width: 100px; height: 12px">
                    <asp:Label ID="chequelab" runat="server" Text="Label"></asp:Label></td>
                <td style="width: 100px; height: 12px">
                    <asp:TextBox ID="chequeText" runat="server" ReadOnly="True"></asp:TextBox></td>
                <td style="width: 95px; height: 12px">
                    <asp:Label ID="other1lab" runat="server" Text="Label"></asp:Label></td>
                <td style="width: 100px; height: 12px">
                    <asp:TextBox ID="optext" runat="server" ReadOnly="True"></asp:TextBox></td>
            </tr>
            <tr bgcolor="#ffffff">
                <td style="width: 100px; height: 15px;">
                    <asp:Label ID="other2lab" runat="server" Text="Label"></asp:Label></td>
                <td style="width: 100px; height: 15px;">
                    <asp:TextBox ID="creditcard" runat="server"></asp:TextBox></td>
                <td style="width: 95px; height: 15px;">
                </td>
                <td style="width: 100px; height: 15px;">
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td style="width: 100px; height: 17px;">
                    <asp:Label ID="total1lab"
            runat="server" Text="Label"></asp:Label></td>
                <td style="width: 100px; height: 17px;">
        <asp:TextBox ID="paytotalBox" runat="server" ReadOnly="True"></asp:TextBox></td>
                <td style="width: 95px; height: 17px;">
                </td>
                <td style="width: 100px; height: 17px;">
                </td>
            </tr>
        </table>
        </asp:Panel>
        <br />
        &nbsp;</div>
        <asp:Label ID="CRlab" runat="server" Text="Label"></asp:Label><br />
        <table id="TABLE1" bgcolor="#b7e6e6" frame="void">
            <tr bgcolor="#ffffff">
                    <td colspan = 4>
                    
                    </td>
            </tr> 
            <tr bgcolor="#ffffff">
                    <td >
                    <asp:Label ID="lblStatus" runat="server" Text="Label" Width="95px"></asp:Label></td>
                    <td colspan = 3>
                    <asp:DropDownList ID="ctryStatDrop" runat="server" Width="141px" Enabled = true >
                    </asp:DropDownList></td>
            </tr> 
            <tr bgcolor="#ffffff">
                <td style="width: 78px">
                    <asp:Label ID="aclab" runat="server" Text="Label"></asp:Label></td>
                <td style="width: 174px">
                    <asp:TextBox ID="actionText" runat="server" Width="134px"></asp:TextBox></td>
                <td rowspan="8" style="width: 80px">
                    <asp:Label ID="remarkslab" runat="server" Text="Label"></asp:Label></td>
                <td rowspan="8" style="width: 50px">
                    <asp:TextBox ID="remarkbox" runat="server" Height="200px" TextMode="MultiLine"></asp:TextBox></td>
            </tr>
            
                       
            <tr bgcolor="#ffffff">
                <td style="width: 78px; height: 153px;">
                    <asp:Label ID="inslab" runat="server" Text="Label"></asp:Label></td>
                <td style="width: 174px; height: 153px;">
                    <asp:TextBox ID="installbox" runat="server" Width="135px" ReadOnly="True"></asp:TextBox>
                    <cc2:jcalendar id="JCalendar1" runat="server" controltoassign="JDATEBox" imgurl="~/PresentationLayer/graph/calendar.gif"></cc2:jcalendar>
                    <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif" Enabled="False" Visible="False" /><asp:Calendar
                        ID="Calendar2" runat="server" BackColor="White" BorderColor="#3366CC" BorderWidth="1px"
                        CellPadding="1" DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="8pt"
                        ForeColor="#003399" Height="160px" Visible="False" Width="180px" Enabled="False">
                        <SelectedDayStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                        <TodayDayStyle BackColor="#99CCCC" ForeColor="White" />
                        <SelectorStyle BackColor="#99CCCC" ForeColor="#336666" />
                        <WeekendDayStyle BackColor="#CCCCFF" />
                        <OtherMonthDayStyle ForeColor="#999999" />
                        <NextPrevStyle Font-Size="8pt" ForeColor="#CCCCFF" />
                        <DayHeaderStyle BackColor="#99CCCC" ForeColor="#336666" Height="1px" />
                        <TitleStyle BackColor="#003399" BorderColor="#3366CC" BorderWidth="1px" Font-Bold="True"
                            Font-Size="10pt" ForeColor="#CCCCFF" Height="25px" />
                    </asp:Calendar>
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td style="width: 78px; height: 5px">
                    <asp:Label ID="contractlab" runat="server" Text="Label"></asp:Label></td>
                <td style="width: 174px; height: 5px">
                    <asp:TextBox ID="contractbox" runat="server" Width="137px" Enabled="False"></asp:TextBox></td>
            </tr>
            <tr bgcolor="#ffffff">
                <td style="width: 78px">
                    <asp:Label ID="custPfLab" runat="server" Text="Label"></asp:Label></td>
                <td style="width: 174px">
                    <asp:TextBox ID="custPfbox" runat="server" Enabled="False" Width="137px"></asp:TextBox></td>
            </tr>
            <tr bgcolor="#ffffff">
                <td style="width: 78px">
                    <asp:Label ID="custIDLab" runat="server" Text="Label"></asp:Label>
                    <asp:Label ID="staLab" runat="server" Text="Label" Visible="False" Width="1px"></asp:Label></td>
                <td style="width: 174px">
                    <asp:TextBox ID="customerBox" runat="server" ReadOnly="True" Width="136px" Enabled="False"></asp:TextBox>
                    </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td style="width: 78px">
                    <asp:Label ID="modletypelab" runat="server" Text="Label"></asp:Label></td>
                <td style="width: 174px">
                    <asp:DropDownList ID="modelTypedrop" runat="server" Width="144px" AutoPostBack ="True"  >
                    </asp:DropDownList><asp:TextBox ID="rouid" runat="server" CssClass="textborder" Height="16px" Visible="False"
                        Width="1%"></asp:TextBox>
                    <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="Red" Text="*"></asp:Label>
                    <asp:RequiredFieldValidator ID="rfvRO" runat="server" ControlToValidate="modelTypedrop"
                        ErrorMessage="*" ForeColor="White">*</asp:RequiredFieldValidator></td>
            </tr>
            <tr bgcolor="#ffffff">
                <td style="width: 78px; height: 12px">
                    <asp:Label ID="freelab" runat="server" Text="Label"></asp:Label></td>
                <td style="width: 174px; height: 12px">
                    <asp:DropDownList ID="freelist" runat="server" Width="145px" AutoPostBack="true" >
                    </asp:DropDownList></td>
            </tr>
            <tr bgcolor="#ffffff">
<td colspan = 4>
                        	&nbsp;
       	</td>
</tr>

            <tr bgcolor="#ffffff">
	<td colspan = 4>
                            <asp:Label ID="lblNoteDown" runat="server" ForeColor="Red" Text="Label"></asp:Label>
	</td>
</tr>

        </table>
        <br />
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;
        <asp:LinkButton ID="savebtn" runat="server">LinkButton</asp:LinkButton>
        &nbsp; &nbsp; 
        <asp:LinkButton ID="deletebtn" runat="server" PostBackUrl="~/PresentationLayer/Function/UpdateServiceBillPart2.aspx" CausesValidation="False">LinkButton</asp:LinkButton>&nbsp;<br />
        <br />
        &nbsp; &nbsp; &nbsp;&nbsp;
        <br />
        <br />
    </form>
</body>
</html>
