﻿Imports BusinessEntity
Imports System
Imports System.IO
Imports System.Collections.Generic
Imports TransFile
Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine

Partial Class PresentationLayer_function_CalIncenforTech1
    Inherits System.Web.UI.Page
    Private objXmlTr As New clsXml
    Private CalIncentEntity As clsCalIncenforTech
    Private ReadOnly _incentive As New clsCalIncenforTechNew_Summary

    Private IncentiveInfo As DataSet
    Private JobDetailInfo As DataSet
    Dim IncetiveReportDoc As New ReportDocument()

    Private fromDt As Date
    Private toDt As Date

    Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                Try
                    If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                        'Response.Redirect("~/PresentationLayer/logon.aspx")
                        Dim script As String = "top.location='../logon.aspx';"
                        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                        Return
                    End If
                Catch ex As Exception
                    'Response.Redirect("~/PresentationLayer/logon.aspx")
                    Dim script As String = "top.location='../logon.aspx';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                    Return
                End Try
                Session.Contents("AuditLogPrintDataFlag") = ""

                DisplayInfo()
                btnPrint.Enabled = False
                Session.Contents("AuditLogPrintDataFlag") = "0"

                ' Added by Ryan Estandarte 12 Sept 2012
                PopulateStaffList(StaffTyDrop.SelectedValue)

            ElseIf Session.Contents("AuditLogPrintDataFlag") = "1" Then
                PrintData()
            End If

            HypCal.NavigateUrl = "javascript:DoCal(document.form1.FrDateBox);"
            HypCal2.NavigateUrl = "javascript:DoCal(document.form1.ToDateBox);"
            IncentiveInfo = Session("IncentInfoView")

            If IncentiveInfo Is Nothing Then
                Return
            End If
            If IncentiveInfo.Tables.Count > 0 Then
                If IncentiveInfo.Tables(0).Rows.Count > 0 Then
                    DispGridViewHead()
                End If
            End If

            errlab.Visible = False
        Catch ex As Exception

        End Try
    End Sub



    Protected Sub DisplayInfo()

        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

     
        titleLab.Text = "Calculate Incentive For Technician"
        FrDate.Text = "From Date"
        ToDate.Text = "To Date"
        StaffTy.Text = "Staff Type"
        lblAllStaffs.Text = "Staff ID/Name"
        lblSelectedStaffs.Text = "Selected Staff/s"

        lnkSearchStaff.Text = "Search"

        ServiceTy.Text = "Service Type"
        FrSerCent.Text = "From Service Center"
        ToSerCent.Text = "To Service Center"
        SerStats.Text = "Service Status"
        BtnRetrieve.Text = "Retrieve"
        TableHead.Text = "Technician/Customer Relation Officer Performance Report:"
        Formula.Text = "MF=Free Service/MM=Maintenance Service+MD=Reinstall&Service/MB=Warranty Job/MR=Repeated Contract=M1-M4+S1-S3+W1-W3/ML=Installation/ML2=New Installation/PL=POE installation PB-POE Breakdown/PM=POE Maintenance/SP=Service Unit Plan CV=Courtesy Visit/MS=Sundry Job/CP=Collect Payment" ''objXmlTr.GetLabelName("EngLabelMsg", "BB-CALCULATEINCENT-0016")
        btnPrint.Text = "View Report"
        btnExport.Text = "Export Job Details"

        Dim lstrMonth As String = System.DateTime.Today.Month
        If Len(lstrMonth) = 1 Then
            lstrMonth = "0" & lstrMonth
        End If

        FrDateBox.Text = "01/" & lstrMonth & "/" & System.DateTime.Today.Year
        ToDateBox.Text = DateAdd(DateInterval.Day, -DateAdd(DateInterval.Month, 1, System.DateTime.Today).Day, DateAdd(DateInterval.Month, 1, System.DateTime.Today))

        'create the dropdownlist
        Dim callStat As New clsUtil()

        Dim TyParam As ArrayList = New ArrayList
        TyParam = callStat.searchconfirminf("StaffTy")
        Dim i As Integer
        Dim Tyid As String
        Dim Tynm As String
        StaffTyDrop.ClearSelection()

        'StaffTyDrop.Items.Add("")
        '--------------------------
        For i = 0 To TyParam.Count - 1
            Tyid = TyParam.Item(i)
            Tynm = TyParam.Item(i + 1)
            i = i + 1

            If Tyid = "TYPE_CR" Or Tyid = "TYPE_TS" Then
                StaffTyDrop.Items.Add(New ListItem(Tynm.ToString(), Tyid.ToString()))
            End If
        Next
        'added by deyb 27-june-2006
        'StaffTyDrop.Items.Insert(0, "")
        '--------------------------
        If StaffTyDrop.Items.Count > 0 Then
            StaffTyDrop.Items(0).Selected = True
        Else
            objXmlTr.XmlFile = System.Configuration.ConfigurationManager.AppSettings("StatMsg")
            errlab.Text = objXmlTr.GetLabelName("StatusMessage", "BB-StaffTy-Fail")
            errlab.Visible = True
            BtnRetrieve.Enabled = False
            btnExport.Enabled = False
            Return
        End If

        Dim SerTyParam As ArrayList = New ArrayList
        SerTyParam = callStat.searchconfirminf("SerTy")
        Dim j As Integer
        Dim SerTyid As String
        Dim SerTynm As String
        'added by deyb 27-june-2006
        ServiceTyDrop.ClearSelection()
        '--------------------------
        For j = 0 To SerTyParam.Count - 1
            SerTyid = SerTyParam.Item(j)
            SerTynm = SerTyParam.Item(j + 1)
            j = j + 1
            ServiceTyDrop.Items.Add(New ListItem(SerTynm.ToString(), SerTyid.ToString()))
        Next
        'added by deyb 27-june-2006
        ServiceTyDrop.Items.Insert(0, "")
        '--------------------------
        If ServiceTyDrop.Items.Count > 0 Then
            ServiceTyDrop.Items(0).Selected = True
        Else
            objXmlTr.XmlFile = System.Configuration.ConfigurationManager.AppSettings("StatMsg")
            errlab.Text = objXmlTr.GetLabelName("StatusMessage", "BB-SerTy-Fail")
            errlab.Visible = True
            BtnRetrieve.Enabled = False
            btnExport.Enabled = False
            Return
        End If


        Dim statParam As ArrayList = New ArrayList
        statParam = callStat.searchconfirminf("SerStat")
        Dim n As Integer
        Dim statid As String
        Dim statnm As String
        SerStatsDrop.Items.Add(New ListItem("All", "ALL"))
        For n = 0 To statParam.Count - 1
            statid = statParam.Item(n)
            statnm = statParam.Item(n + 1)
            n = n + 1
            If statid = "BL" Or statid = "UM" Then
                SerStatsDrop.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
            End If
        Next
        If SerStatsDrop.Items.Count > 0 Then
            SerStatsDrop.Items(0).Selected = True
        Else
            objXmlTr.XmlFile = System.Configuration.ConfigurationManager.AppSettings("StatMsg")
            errlab.Text = objXmlTr.GetLabelName("StatusMessage", "BB-SerBLail")
            errlab.Visible = True
            BtnRetrieve.Enabled = False
            btnExport.Enabled = False
            Return
        End If
        BtnRetrieve.Enabled = True

    End Sub

    Protected Sub BindGrid()
        Dim UserID As String = Session("userID")

        _incentive.StrUserID = UserID
        _incentive.StrFrDt = fromDt
        _incentive.StrToDt = toDt

        Dim listStaff As String
        Dim count As Integer

        count = lstSelectedStaffs.Items.Count - 1
        listStaff = ""

        For i As Integer = 0 To count
            listStaff = listStaff + "," + lstSelectedStaffs.Items(i).Value.ToString()
            'listStaff = listStaff + "','" + lstSelectedStaffs.Items(i).Value.ToString()
        Next

        _incentive.StaffList = listStaff
        _incentive.StrFrSerCent = FrSerCentBox.Text
        _incentive.StrToSerCent = ToSerCentBox.Text
        _incentive.StrStaffTy = StaffTyDrop.SelectedValue
        _incentive.StrSerStats = SerStatsDrop.SelectedValue

        IncentiveInfo = _incentive.GetIncentiveInfo()

        If IncentiveInfo Is Nothing Then
            errlab.Text = "There's no record available!"
            errlab.Visible = True
            IncentInfoView.Visible = False
            Return

        End If
        If IncentiveInfo.Tables.Count = 0 Then
            errlab.Text = "There's no record available!"
            errlab.Visible = True
            Return
        End If
        If IncentiveInfo.Tables(0).Rows.Count > 0 Then
            errlab.Visible = False

            IncentInfoView.AllowPaging = True
            IncentInfoView.AllowSorting = True
            IncentInfoView.PageSize = IncentiveInfo.Tables(0).Rows.Count
            IncentInfoView.DataSource = IncentiveInfo.Tables(0)
            Session("IncentInfoView") = IncentiveInfo
            IncentInfoView.DataBind()

            For Each row As GridViewRow In IncentInfoView.Rows
                row.Cells(22).Visible = False
                row.Cells(23).Visible = False
                row.Cells(24).Visible = False
                row.Cells(25).Visible = False
                row.Cells(26).Visible = False
                row.Cells(27).Visible = False
                row.Cells(28).Visible = False
                row.Cells(29).Visible = False
                row.Cells(30).Visible = False
                row.Cells(31).Visible = False
                row.Cells(32).Visible = False
                row.Cells(33).Visible = False
                row.Cells(34).Visible = False
                row.Cells(35).Visible = False
                row.Cells(36).Visible = False
                row.Cells(37).Visible = False

            Next
            DisplayViewFooter()

            DispGridViewHead()
            DateRange.Text = Request.Form("FrDateBox") + "-" + Request.Form("ToDateBox") 'FrDateBox.Text + "-" + ToDateBox.Text
            TableHead.Visible = True
            DateRange.Visible = True
            IncentInfoView.Visible = True
            Formula.Visible = True
            btnPrint.Enabled = True
            'IncentiveReportViewer.Visible = True
        Else
            errlab.Text = "There's no record available!"
            errlab.Visible = True
            IncentInfoView.Visible = False
            'Formula.Visible = False
            TableHead.Visible = False
            DateRange.Visible = False
            btnPrint.Enabled = False
            Session.Contents("AuditLogPrintDataFlag") = "0"
        End If
        IncentiveReportViewer.Visible = True
        btnExport.Enabled = True
    End Sub

    Private Sub DispGridViewHead()

        Try

            IncentInfoView.HeaderRow.Cells(0).Text = "Service Center" 
            IncentInfoView.HeaderRow.Cells(1).Text = "HR ID"
            IncentInfoView.HeaderRow.Cells(2).Text = "Staff ID/Name"
            IncentInfoView.HeaderRow.Cells(3).Text = "MF"
            IncentInfoView.HeaderRow.Cells(4).Text = "MM"
            IncentInfoView.HeaderRow.Cells(5).Text = "MD"
            IncentInfoView.HeaderRow.Cells(6).Text = "MR"
            IncentInfoView.HeaderRow.Cells(7).Text = "MC"
            IncentInfoView.HeaderRow.Cells(8).Text = "ML"
            IncentInfoView.HeaderRow.Cells(9).Text = "ML2"
            IncentInfoView.HeaderRow.Cells(10).Text = "MS"
            IncentInfoView.HeaderRow.Cells(11).Text = "MB"
            IncentInfoView.HeaderRow.Cells(12).Text = "CP"
            IncentInfoView.HeaderRow.Cells(13).Text = "PL"
            IncentInfoView.HeaderRow.Cells(14).Text = "PB"
            IncentInfoView.HeaderRow.Cells(15).Text = "PM"
            'IncentInfoView.HeaderRow.Cells(16).Text = "Demerit Jobs"
            IncentInfoView.HeaderRow.Cells(16).Text = "Total Jobs"
            IncentInfoView.HeaderRow.Cells(17).Text = "Total Sales"
            IncentInfoView.HeaderRow.Cells(18).Text = "SP"
            IncentInfoView.HeaderRow.Cells(19).Text = "SP Sales Revenue"
            IncentInfoView.HeaderRow.Cells(20).Text = "Corporate Revenue"
            IncentInfoView.HeaderRow.Cells(21).Text = ">8 Days Debtor"
            IncentInfoView.HeaderRow.Cells(22).Text = "K200 $500"
            IncentInfoView.HeaderRow.Cells(23).Text = "K200 $400"
            IncentInfoView.HeaderRow.Cells(24).Text = "K200 $100"
            IncentInfoView.HeaderRow.Cells(25).Text = "K100 $300"
            IncentInfoView.HeaderRow.Cells(26).Text = "K100 $100"
            IncentInfoView.HeaderRow.Cells(22).Visible = False
            IncentInfoView.HeaderRow.Cells(23).Visible = False
            IncentInfoView.HeaderRow.Cells(24).Visible = False
            IncentInfoView.HeaderRow.Cells(25).Visible = False
            IncentInfoView.HeaderRow.Cells(26).Visible = False
            IncentInfoView.HeaderRow.Cells(27).Visible = False
            IncentInfoView.HeaderRow.Cells(28).Visible = False
            IncentInfoView.HeaderRow.Cells(29).Visible = False
            IncentInfoView.HeaderRow.Cells(30).Visible = False
            IncentInfoView.HeaderRow.Cells(31).Visible = False
            IncentInfoView.HeaderRow.Cells(32).Visible = False
            IncentInfoView.HeaderRow.Cells(33).Visible = False
            IncentInfoView.HeaderRow.Cells(34).Visible = False
            IncentInfoView.HeaderRow.Cells(35).Visible = False
            IncentInfoView.HeaderRow.Cells(36).Visible = False
            IncentInfoView.HeaderRow.Cells(37).Visible = False


        Catch ex As Exception

        End Try


    End Sub

    Protected Sub BtnRetrieve_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnRetrieve.Click
        FrDateBox.Text = Request.Form("FrDateBox")  '01/06/2016
        ToDateBox.Text = Request.Form("ToDateBox")

        'change the date format to 2010-01-01 00:00:00.000
        Dim FrDateArr() As String
        If Not (FrDateBox.Text = "") Then
            FrDateArr = FrDateBox.Text.Split("/")
            If FrDateArr.Length = 3 Then
                fromDt = FrDateArr(2) + "-" + FrDateArr(1) + "-" + FrDateArr(0)
            End If
        End If

        Dim ToDateArr() As String
        If Not (ToDateBox.Text = "") Then
            ToDateArr = ToDateBox.Text.Split("/")
            If ToDateArr.Length = 3 Then
                toDt = ToDateArr(2) + "-" + ToDateArr(1) + "-" + ToDateArr(0)
            End If
        End If
        

        BindGrid()
        Session.Contents("AuditLogPrintDataFlag") = "0"
        'added by deyb 27-june-2006
        IncentiveReportViewer.Visible = False
        '--------------------------
    End Sub

    Protected Sub DisplayViewFooter()
        IncentInfoView.FooterRow.Cells(0).Text = "Total:"
        IncentInfoView.FooterRow.Cells(1).Text = IncentiveInfo.Tables(0).Rows.Count
        Dim totalMF As Double = 0, totalMM As Double = 0, totalMD As Double = 0, totalMR As Double = 0
        Dim totalCtrt As Double = 0, totalML1 As Double = 0, totalML2 As Double = 0, totalMS As Double = 0, totalMB As Double = 0
        Dim totalJobAmt As Double = 0, totalSalesAmt As Double = 0, totalPointsAmt As Double = 0
        Dim totalPL As Double = 0, totalPB As Double = 0, totalPM As Double = 0, totalCP As Double = 0
        Dim totalSP As Double = 0, totalSPSalesAmt As Double = 0
        Dim totalCorSalesAmt As Double = 0, totalOver8DebtorAmt As Double = 0
        Dim totalK200_1 As Double = 0, totalK200_2 As Double = 0, totalK200_3 As Double = 0
        Dim totalK100_1 As Double = 0, totalK100_2 As Double = 0
        Dim totalDemeritMS As Double = 0, totalDemeritRPTJOB As Double = 0

        For i As Integer = 0 To IncentiveInfo.Tables(0).Rows.Count - 1
            'totalMF += Integer.Parse(IncentInfoView.Rows(i).Cells(2).Text)
            'totalMM += Integer.Parse(IncentInfoView.Rows(i).Cells(3).Text)
            'totalMD += Integer.Parse(IncentInfoView.Rows(i).Cells(4).Text)
            'totalMR += Integer.Parse(IncentInfoView.Rows(i).Cells(5).Text)
            'totalCtrt += Integer.Parse(IncentInfoView.Rows(i).Cells(6).Text)
            'totalML1 += Integer.Parse(IncentInfoView.Rows(i).Cells(7).Text)
            'totalML2 += Integer.Parse(IncentInfoView.Rows(i).Cells(8).Text)
            'totalMS += Integer.Parse(IncentInfoView.Rows(i).Cells(9).Text)
            'totalJobAmt += Integer.Parse(IncentInfoView.Rows(i).Cells(10).Text)
            'totolSalesAmt += Single.Parse(IncentInfoView.Rows(i).Cells(11).Text)
            'totalPointsAmt += Single.Parse(IncentInfoView.Rows(i).Cells(12).Text)
            totalMF += CDbl(IncentInfoView.Rows(i).Cells(3).Text)
            totalMM += CDbl(IncentInfoView.Rows(i).Cells(4).Text)
            totalMD += CDbl(IncentInfoView.Rows(i).Cells(5).Text)
            totalMR += CDbl(IncentInfoView.Rows(i).Cells(6).Text)
            totalCtrt += CDbl(IncentInfoView.Rows(i).Cells(7).Text)

            totalML1 += CDbl(IncentInfoView.Rows(i).Cells(8).Text)
            totalML2 += CDbl(IncentInfoView.Rows(i).Cells(9).Text)
            totalMS += CDbl(IncentInfoView.Rows(i).Cells(10).Text)
            totalMB += CDbl(IncentInfoView.Rows(i).Cells(11).Text)
            totalCP += CDbl(IncentInfoView.Rows(i).Cells(12).Text)
            totalPL += CDbl(IncentInfoView.Rows(i).Cells(13).Text)
            totalPB += CDbl(IncentInfoView.Rows(i).Cells(14).Text)
            totalPM += CDbl(IncentInfoView.Rows(i).Cells(15).Text)
            'totalDemerit += CDbl(IncentInfoView.Rows(i).Cells(16).Text)
            totalJobAmt += CDbl(IncentInfoView.Rows(i).Cells(16).Text)
            totalSalesAmt += CDbl(IncentInfoView.Rows(i).Cells(17).Text)
            totalSP += CDbl(IncentInfoView.Rows(i).Cells(18).Text)
            totalSPSalesAmt += CDbl(IncentInfoView.Rows(i).Cells(19).Text)
            totalCorSalesAmt += CDbl(IncentInfoView.Rows(i).Cells(20).Text)
            totalOver8DebtorAmt += CDbl(IncentInfoView.Rows(i).Cells(21).Text)
            'totalK200_1 += CDbl(IncentInfoView.Rows(i).Cells(22).Text)
            'totalK200_2 += CDbl(IncentInfoView.Rows(i).Cells(23).Text)
            'totalK200_3 += CDbl(IncentInfoView.Rows(i).Cells(24).Text)
            'totalK100_1 += CDbl(IncentInfoView.Rows(i).Cells(25).Text)
            'totalK100_2 += CDbl(IncentInfoView.Rows(i).Cells(26).Text)
            'totalDemeritMS += CDbl(IncentInfoView.Rows(i).Cells(27).Text)
        Next
        IncentInfoView.FooterRow.Cells(3).Text = totalMF
        IncentInfoView.FooterRow.Cells(4).Text = totalMM
        IncentInfoView.FooterRow.Cells(5).Text = totalMD
        IncentInfoView.FooterRow.Cells(6).Text = totalMR
        IncentInfoView.FooterRow.Cells(7).Text = totalCtrt

        IncentInfoView.FooterRow.Cells(8).Text = totalML1
        IncentInfoView.FooterRow.Cells(9).Text = totalML2
        IncentInfoView.FooterRow.Cells(10).Text = totalMS
        IncentInfoView.FooterRow.Cells(11).Text = totalMB
        IncentInfoView.FooterRow.Cells(12).Text = totalCP
        IncentInfoView.FooterRow.Cells(13).Text = totalPL
        IncentInfoView.FooterRow.Cells(14).Text = totalPB
        IncentInfoView.FooterRow.Cells(15).Text = totalPM
        IncentInfoView.FooterRow.Cells(16).Text = totalJobAmt
        IncentInfoView.FooterRow.Cells(17).Text = Format(totalSalesAmt, "#0.00")
        IncentInfoView.FooterRow.Cells(18).Text = totalSP

        IncentInfoView.FooterRow.Cells(19).Text = Format(totalSPSalesAmt, "#0.00")
        IncentInfoView.FooterRow.Cells(20).Text = Format(totalCorSalesAmt, "#0.00")
        IncentInfoView.FooterRow.Cells(21).Text = Format(totalOver8DebtorAmt, "#0.00")
        'IncentInfoView.FooterRow.Cells(22).Text = totalK200_1
        'IncentInfoView.FooterRow.Cells(23).Text = totalK200_2
        'IncentInfoView.FooterRow.Cells(24).Text = totalK200_3
        'IncentInfoView.FooterRow.Cells(25).Text = totalK100_1
        'IncentInfoView.FooterRow.Cells(26).Text = totalK100_2
        'IncentInfoView.FooterRow.Cells(27).Text = totalDemeritMS
        'IncentInfoView.FooterRow.Cells(28).Text = totalDemeritRPTJOB
        'IncentInfoView.FooterRow.Cells(29).Text = totalDemeritMS

        IncentInfoView.FooterRow.Visible = True
        IncentInfoView.FooterRow.Cells(22).Visible = False
        IncentInfoView.FooterRow.Cells(23).Visible = False
        IncentInfoView.FooterRow.Cells(24).Visible = False
        IncentInfoView.FooterRow.Cells(25).Visible = False
        IncentInfoView.FooterRow.Cells(26).Visible = False
        IncentInfoView.FooterRow.Cells(27).Visible = False
        IncentInfoView.FooterRow.Cells(28).Visible = False
        IncentInfoView.FooterRow.Cells(29).Visible = False
        IncentInfoView.FooterRow.Cells(30).Visible = False
        IncentInfoView.FooterRow.Cells(31).Visible = False
        IncentInfoView.FooterRow.Cells(32).Visible = False
        IncentInfoView.FooterRow.Cells(33).Visible = False
        IncentInfoView.FooterRow.Cells(34).Visible = False
        IncentInfoView.FooterRow.Cells(35).Visible = False
        IncentInfoView.FooterRow.Cells(36).Visible = False
        IncentInfoView.FooterRow.Cells(37).Visible = False


    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        FrDateBox.Text = Request.Form("FrDateBox")
        ToDateBox.Text = Request.Form("ToDateBox")

        PrintData()
        IncentiveReportViewer.Visible = True

    End Sub

    Private Sub PrintData()
        Dim dsReport As DataSet = Session.Contents("IncentInfoView")


        Try
            IncetiveReportDoc.Load(MapPath("IncentiveReport1.rpt"))
            errlab.Visible = False
        Catch ex As Exception
            errlab.Text = "Failed to load"
            errlab.Visible = True
            Return
        End Try
        IncetiveReportDoc.SetDataSource(dsReport.Tables(0))
        'LLY
        Dim listStaff As String = String.Empty
        Dim count As Integer
        count = lstSelectedStaffs.Items.Count - 1

        For i As Integer = 0 To count
            listStaff = listStaff + "," + lstSelectedStaffs.Items(i).Value.ToString()
            'listStaff = listStaff + "','" + lstSelectedStaffs.Items(i).Value.ToString()
        Next

        IncetiveReportDoc.SetParameterValue("@UserID", Session("userID"))
        IncetiveReportDoc.SetParameterValue("@FromDateTemp", FrDateBox.Text)
        IncetiveReportDoc.SetParameterValue("@ToDateTemp", ToDateBox.Text)
        IncetiveReportDoc.SetParameterValue("@StaffCo", listStaff)
        IncetiveReportDoc.SetParameterValue("@FromServCenter", FrSerCentBox.Text)
        IncetiveReportDoc.SetParameterValue("@ToServCenter", ToSerCentBox.Text)
        IncetiveReportDoc.SetParameterValue("@StaffTy", StaffTyDrop.SelectedValue)
        IncetiveReportDoc.SetParameterValue("@Status", SerStatsDrop.SelectedValue)

        IncetiveReportDoc.SetParameterValue("OT_Price1", "12.00")
        IncetiveReportDoc.SetParameterValue("OT_Price2", "15.00")
        IncetiveReportDoc.SetParameterValue("OT_Price3", "18.00")
        IncetiveReportDoc.SetParameterValue("OT_Price4", "21.00")
        IncetiveReportDoc.SetParameterValue("OT_Price5", "25.00")
        IncetiveReportDoc.SetParameterValue("OT_Job_R1", "100-110")
        IncetiveReportDoc.SetParameterValue("OT_Job_R2", "111-120")
        IncetiveReportDoc.SetParameterValue("OT_Job_R3", "121-130")
        IncetiveReportDoc.SetParameterValue("OT_Job_R4", "131-140")
        IncetiveReportDoc.SetParameterValue("OT_Job_R5", ">=141")
        IncetiveReportDoc.SetParameterValue("Job_L1", "Jobs")
        IncetiveReportDoc.SetParameterValue("Job_L2", "Extra")
        IncetiveReportDoc.SetParameterValue("Job_L3", "Demerit Job/not count")
        IncetiveReportDoc.SetParameterValue("Job_L4", "Total")
        IncetiveReportDoc.SetParameterValue("Job_L5", "No of Job entitled for OT")
        IncetiveReportDoc.SetParameterValue("Extra_L1", "MD")
        IncetiveReportDoc.SetParameterValue("Extra_L2", "Sunday Job")
        IncetiveReportDoc.SetParameterValue("Extra_L3", "Others")
        IncetiveReportDoc.SetParameterValue("Extra_L4", "Total")
        IncetiveReportDoc.SetParameterValue("Demerit_L1", "MS")
        IncetiveReportDoc.SetParameterValue("Demerit_L2", "CP")
        IncetiveReportDoc.SetParameterValue("Demerit_L3", "Repeated Job")
        IncetiveReportDoc.SetParameterValue("Demerit_L4", "POE Installation")
        IncetiveReportDoc.SetParameterValue("Demerit_L5", "RO Installation")
        IncetiveReportDoc.SetParameterValue("Demerit_L6", "Total")
        IncetiveReportDoc.SetParameterValue("Rev_L1", "Service Revenue")
        IncetiveReportDoc.SetParameterValue("Rev_L2", "POE Install Rev")
        IncetiveReportDoc.SetParameterValue("Rev_L3", "Corporate")
        IncetiveReportDoc.SetParameterValue("Rev_L4", "Over 8 Days Debtor")
        IncetiveReportDoc.SetParameterValue("Rev_L5", "Total Sales")
        IncetiveReportDoc.SetParameterValue("Rev_L6", "GST 6%")
        IncetiveReportDoc.SetParameterValue("Rev_L7", "Net Sales")
        IncetiveReportDoc.SetParameterValue("Rev_L8", "Incentive")
        IncetiveReportDoc.SetParameterValue("Rev_L9", "Demerit")
        IncetiveReportDoc.SetParameterValue("Rev_L10", "Total Incentive")
        IncetiveReportDoc.SetParameterValue("Rev_L11", "SP Sales Rev")
        IncetiveReportDoc.SetParameterValue("Rev_L12", "WP Installment * 120")
        IncetiveReportDoc.SetParameterValue("SP", "SP")
        IncetiveReportDoc.SetParameterValue("SP_Sales_Rev", "SP Sales Revenue")
        IncetiveReportDoc.SetParameterValue("Corporate", "Corporate")
        IncetiveReportDoc.SetParameterValue("over8", ">8 Days Debtor")
        IncetiveReportDoc.SetParameterValue("K200_1", "K200 $500")
        IncetiveReportDoc.SetParameterValue("K200_2", "K200 $400")
        IncetiveReportDoc.SetParameterValue("K200_3", "K200 $100")
        IncetiveReportDoc.SetParameterValue("K100_1", "K100 $300")
        IncetiveReportDoc.SetParameterValue("K100_2", "K100 $100")
        IncetiveReportDoc.SetParameterValue("Demerit_Job", "Demerit Job")
        IncetiveReportDoc.SetParameterValue("POE", 0)
        IncetiveReportDoc.SetParameterValue("Sunday_Job", 0)
        IncetiveReportDoc.SetParameterValue("OT_PREMIUM_OTHERS", 0)
        IncetiveReportDoc.SetParameterValue("over8", ">8 Days Debtor")

        'LLY

        IncetiveReportDoc.SetParameterValue("ReportHead", "Technician/Customer Relation Officer Performance Report:")
        IncetiveReportDoc.SetParameterValue("daterange", FrDateBox.Text + " - " + ToDateBox.Text)
        IncetiveReportDoc.SetParameterValue("FIV1_SVCID", "Service Center")
        IncetiveReportDoc.SetParameterValue("FIV1_HRID", "HR ID")
        IncetiveReportDoc.SetParameterValue("StaffIDandName", "Staff ID/Name")
        IncetiveReportDoc.SetParameterValue("MF", "MF")
        IncetiveReportDoc.SetParameterValue("MM", "MM")
        IncetiveReportDoc.SetParameterValue("MD", "MD")
        IncetiveReportDoc.SetParameterValue("MR", "MR")
        IncetiveReportDoc.SetParameterValue("MC", "MC")
        'LLY
        'IncetiveReportDoc.SetParameterValue("CONTRACT", "CONTRACT")
        'LLY

        IncetiveReportDoc.SetParameterValue("ML", "ML")
        IncetiveReportDoc.SetParameterValue("ML2", "ML2")
        IncetiveReportDoc.SetParameterValue("MS", "MS")
        IncetiveReportDoc.SetParameterValue("MB", "MB")
        IncetiveReportDoc.SetParameterValue("CP", "CP")
        IncetiveReportDoc.SetParameterValue("PL", "PL")
        IncetiveReportDoc.SetParameterValue("PB", "PB")
        IncetiveReportDoc.SetParameterValue("PM", "PM")
        IncetiveReportDoc.SetParameterValue("TotalJobs", "Total Jobs")
        IncetiveReportDoc.SetParameterValue("TotalSales", "Total Sales")
        IncetiveReportDoc.SetParameterValue("TotalPoints", "Total Points")
        IncetiveReportDoc.SetParameterValue("incentiveDate", FrDateBox.Text)

        IncentiveReportViewer.HasCrystalLogo = False
        IncentiveReportViewer.ReportSource = IncetiveReportDoc
        Session.Contents("AuditLogPrintDataFlag") = "1"



    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        FrDateBox.Text = Request.Form("FrDateBox")
        ToDateBox.Text = Request.Form("ToDateBox")
        ExportJobDetail()
    End Sub

    Private Sub ExportJobDetail()
        Dim UserID As String = Session("userID")
        _incentive.StrUserID = UserID
        _incentive.StrFrDate = Request.Form("FrDateBox")
        _incentive.StrToDate = Request.Form("ToDateBox")

        Dim listStaff As String
        Dim count As Integer

        count = lstSelectedStaffs.Items.Count - 1
        listStaff = ""

        For i As Integer = 0 To count
            listStaff = listStaff + "','" + lstSelectedStaffs.Items(i).Value.ToString()
        Next

        _incentive.StaffList = listStaff
        _incentive.StrFrSerCent = FrSerCentBox.Text
        _incentive.StrToSerCent = ToSerCentBox.Text
        _incentive.StrStaffTy = StaffTyDrop.SelectedValue
        _incentive.StrSerStats = SerStatsDrop.SelectedValue

        JobDetailInfo = _incentive.GetExportInfo()

        If JobDetailInfo Is Nothing Then
            errlab.Text = "There's no record available!" ''.GetLabelName("StatusMessage", "BB-View-Fail")
            errlab.Visible = True
            Return
        End If
        If JobDetailInfo.Tables.Count <= 0 Then
            errlab.Text = "There's no record available!" ''objXmlTr.GetLabelName("StatusMessage", "BB-View-Fail")
            errlab.Visible = True
            Return
        End If
        If JobDetailInfo.Tables(0).Rows.Count = 0 Then
            errlab.Text = "There's no record available!" ''objXmlTr.GetLabelName("StatusMessage", "BB-View-Fail")
            errlab.Visible = True
            Return
        End If
        Dim JobDetail As New StringBuilder
        objXmlTr.XmlFile = Configuration.ConfigurationManager.AppSettings("XmlFilePath")
        Dim ExFileHead As String = ""
        ExFileHead = objXmlTr.GetLabelName("EngLabelMsg", "BB-CALCULATEINCENT-0031") + " ; " + _
                     objXmlTr.GetLabelName("EngLabelMsg", "BB-CALCULATEINCENT-0032") + " ; " + _
                     objXmlTr.GetLabelName("EngLabelMsg", "BB-CALCULATEINCENT-0033") + " ; " + _
                     "Customer Id" + " ; " + _
                     "Serial No" + " ; " + _
                     "Appointment Create Date" + " ; " + _
                     objXmlTr.GetLabelName("EngLabelMsg", "BB-CALCULATEINCENT-0034") + " ; " + _
                     objXmlTr.GetLabelName("EngLabelMsg", "BB-CALCULATEINCENT-0035") + " ; " + _
                     objXmlTr.GetLabelName("EngLabelMsg", "BB-CALCULATEINCENT-0036") + " ; " + _
                     objXmlTr.GetLabelName("EngLabelMsg", "BB-CALCULATEINCENT-0037") + " ; " + _
                     objXmlTr.GetLabelName("EngLabelMsg", "BB-CALCULATEINCENT-0038") + " ; " + _
                     objXmlTr.GetLabelName("EngLabelMsg", "BB-CALCULATEINCENT-0039") + " ; " + _
                     objXmlTr.GetLabelName("EngLabelMsg", "BB-CALCULATEINCENT-0040") + " ; " + _
                     objXmlTr.GetLabelName("EngLabelMsg", "BB-CALCULATEINCENT-0041") + " ; " + _
                     "Action Taken" + " ; "


        JobDetail.Append(ExFileHead)
        JobDetail.AppendLine()

        For i As Integer = 0 To JobDetailInfo.Tables(0).Rows.Count - 1
            For j As Integer = 0 To JobDetailInfo.Tables(0).Columns.Count - 1
                JobDetail.Append(JobDetailInfo.Tables(0).Rows(i).Item(j).ToString() + "    ;   ")
            Next
            JobDetail.AppendLine()
        Next

        Dim sr As New MemoryStream
        Dim bw As New BinaryWriter(sr)
        bw.Write(JobDetail.ToString())

        'Transfer File to the Client
        Response.Clear()
        Dim bSuccess As Boolean = dowload.ResponseFile(Request, Response, "JobDetail.txt", sr, 1024000)
        If bSuccess = False Then
            ''objXmlTr.XmlFile = System.Configuration.ConfigurationManager.AppSettings("StatMsg")
            errlab.Text = "There's no record available!" ''objXmlTr.GetLabelName("StatusMessage", "BB-Export-Fail")
            errlab.Visible = True
            Return
        End If
        sr.Close()
        Response.End()

    End Sub

    Protected Sub FrDateCalendar_SelectedDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) _
    Handles FrDateCalendar.SelectedDateChanged
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        FrDateBox.Text = FrDateCalendar.SelectedDate
        Me.FrDateBox.ReadOnly = True
    End Sub

    Protected Sub ToDateCalendar_SelectedDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) _
    Handles ToDateCalendar.SelectedDateChanged
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        ToDateBox.Text = ToDateCalendar.SelectedDate
        Me.ToDateBox.ReadOnly = True
    End Sub

    Protected Sub ServiceTyDrop_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ServiceTyDrop.SelectedIndexChanged

    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        IncetiveReportDoc.Dispose()
        IncentiveReportViewer.Dispose()
    End Sub

    Protected Sub IncentiveReportViewer_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles IncentiveReportViewer.Init

    End Sub

#Region "Added by Ryan Estandarte 13 Sept 2012"
    Protected Sub btnAddOne_Click(ByVal sender As Object, ByVal e As EventArgs)
        errlab.Visible = False
        Dim array As New ArrayList()

        If lstAllStaffs.SelectedIndex > -1 Then
            For i As Integer = 0 To lstAllStaffs.Items.Count - 1
                If lstAllStaffs.Items(i).Selected Then
                    If Not array.Contains(lstAllStaffs.Items(i)) Then
                        array.Add(lstAllStaffs.Items(i))
                    End If
                End If
            Next

            For i As Integer = 0 To array.Count - 1

                If Not lstSelectedStaffs.Items.Contains(CType(array(i), ListItem)) Then
                    lstSelectedStaffs.Items.Add(array(i))
                End If
                lstAllStaffs.Items.Remove(array(i))
            Next
            lstSelectedStaffs.SelectedIndex = -1

            Dim dtSelected As DataTable = CType(Session("Staffs"), DataTable).Clone()
            If lstAllStaffs.Items.Count < 1 Then
                For Each item As ListItem In lstSelectedStaffs.Items
                    Dim dr As DataRow = dtSelected.NewRow()
                    dr(0) = item.Value
                    dr(1) = item.Text
                    dtSelected.Rows.Add(dr)
                Next

                PopulateStaffList(dtSelected)
            End If

        Else
            errlab.Visible = True
            errlab.Text = "Please select staff ID to move"
        End If


    End Sub

    Protected Sub btnAddAll_Click(ByVal sender As Object, ByVal e As EventArgs)
        errlab.Visible = False
        Dim listItem As Integer = lstAllStaffs.Items.Count

        While lstAllStaffs.Items.Count > 0
            For i As Integer = 0 To lstAllStaffs.Items.Count - 1
                lstSelectedStaffs.Items.Add(lstAllStaffs.Items(i))
            Next
            lstAllStaffs.Items.Clear()
        End While

        Dim dtSelected As DataTable = CType(Session("Staffs"), DataTable).Clone()

        If lstSelectedStaffs.Items.Count = listItem Then
            For Each item As ListItem In lstSelectedStaffs.Items
                Dim dr As DataRow = dtSelected.NewRow()
                dr(0) = item.Value
                dr(1) = item.Text
                dtSelected.Rows.Add(dr)
            Next

            PopulateStaffList(dtSelected)
        End If

    End Sub


    Protected Sub btnRemoveOne_Click(ByVal sender As Object, ByVal e As EventArgs)
        errlab.Visible = False
        Dim array As New ArrayList()
        If lstSelectedStaffs.SelectedIndex > -1 Then
            For i As Integer = 0 To lstSelectedStaffs.Items.Count - 1
                If lstSelectedStaffs.Items(i).Selected Then
                    If Not array.Contains(lstSelectedStaffs.Items(i)) Then
                        array.Add(lstSelectedStaffs.Items(i))
                    End If
                End If
            Next
            For i As Integer = 0 To array.Count - 1
                If Not lstAllStaffs.Items.Contains(DirectCast(array(i), ListItem)) Then
                    lstAllStaffs.Items.Add(DirectCast(array(i), ListItem))
                End If
                lstSelectedStaffs.Items.Remove(DirectCast(array(i), ListItem))
            Next
            lstAllStaffs.SelectedIndex = -1
        Else
            errlab.Visible = True
            errlab.Text = "Please select staff ID to move"
        End If
    End Sub

    Protected Sub btnRemoveAll_Click(ByVal sender As Object, ByVal e As EventArgs)
        errlab.Visible = False

        While lstSelectedStaffs.Items.Count > 0
            For i As Integer = 0 To lstSelectedStaffs.Items.Count - 1
                lstAllStaffs.Items.Add(lstSelectedStaffs.Items(i))
            Next
            lstSelectedStaffs.Items.Clear()
        End While

    End Sub

    ''' <summary>
    ''' Populates the staff list box based on the selected staff type
    ''' </summary>
    ''' <param name="staffType"></param>
    ''' <remarks></remarks>
    '''  Modified by PF, 17/10/2016 - SMR/1610/1890 (Filter staff list according to staff Rank)
    Private Sub PopulateStaffList(ByVal staffType As String)
        lstAllStaffs.Items.Clear()
        'Dim dt As DataTable = _incentive.RetrieveStaffs(staffType)
        Dim strUserId As String = Session("userID")
        Dim dt As DataTable = _incentive.RetrieveStaffs(staffType, strUserId)

        ' Added by Ryan Estandarte 30 Oct 2012
        Session("Staffs") = dt

        lstAllStaffs.DataSource = dt
        lstAllStaffs.DataTextField = "Name"
        lstAllStaffs.DataValueField = "ID"
        lstAllStaffs.DataBind()

    End Sub

    Private Sub PopulateStaffList(ByVal dtSelected As DataTable)

        If Session("Staffs") IsNot Nothing Then
            Dim dtAll As DataTable = CType(Session("Staffs"), DataTable)

            For i As Integer = 0 To dtSelected.Rows.Count - 1
                Dim id As String = dtSelected.Rows(i)(0).ToString()
                For Each row As DataRow In dtAll.Rows
                    If row(0).ToString() = id Then
                        row.Delete()
                    End If
                Next
                dtAll.AcceptChanges()
            Next

            lstAllStaffs.DataSource = dtAll
            lstAllStaffs.DataTextField = "Name"
            lstAllStaffs.DataValueField = "ID"
            lstAllStaffs.DataBind()
        End If

    End Sub

    Protected Sub StaffTyDrop_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        ' Added by Ryan Estandarte 12 Sept 2012
        lstSelectedStaffs.Items.Clear()
        PopulateStaffList(StaffTyDrop.SelectedValue)
    End Sub
#End Region

    ''' <remarks>Added by Ryan Estandarte 30 Oct 2012</remarks>
    Protected Sub lnkSearchStaff_Click(ByVal sender As Object, ByVal e As EventArgs)

        If Session("Staffs") IsNot Nothing Then
            Dim dt As DataTable = CType(Session("Staffs"), DataTable)
            Dim dtFilter As New DataTable("Filter")
            Dim rows As DataRow()

            dtFilter = dt.Clone()
            rows = dt.Select("ID LIKE '%" + txtSearchStaff.Text + "%'")
            Dim newRow As DataRow
            For Each dataRow As DataRow In rows
                newRow = dtFilter.NewRow()
                newRow("ID") = dataRow("ID")
                newRow("Name") = dataRow("Name")
                dtFilter.Rows.Add(newRow)
            Next

            lstAllStaffs.DataSource = dtFilter
            lstAllStaffs.DataTextField = "Name"
            lstAllStaffs.DataValueField = "ID"
            lstAllStaffs.DataBind()
        End If

    End Sub
End Class
