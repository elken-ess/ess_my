﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="contracthistory.aspx.vb"
    Inherits="PresentationLayer_function_ContractHistory" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Contract History</title>
    <link href="../css/style.css" type="text/css" rel="stylesheet">
</head>
<body>
    <form id="form1" runat="server">
        <div style="vertical-align: top">
            <table style="width: 100%" border="0">
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0" width="100%" border="0">
                            <tr>
                                <td width="1%" background="../graph/title_bg.gif">
                                    <img height="24" src="../graph/title1.gif" width="5" /></td>
                                <td class="style2" width="98%" background="../graph/title_bg.gif">
                                    <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="right" background="../graph/title_bg.gif" style="width: 1%">
                                    <img height="24" src="../graph/title_2.gif" width="5" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="1" cellpadding="2" bgcolor="#b7e6e6" border="0" style="width: 100%">
                            <tr bgcolor="#ffffff" style="width: 80%">
                                <td align="right" style="width: 20%">
                                    <asp:Label ID="ROInfoLab" runat="server" Text="Label"></asp:Label>
                                </td>
                                <td style="width: 30%">
                                    <asp:TextBox ID="ROInfoBox" runat="server" Style="width: 80%" CssClass="textborder"
                                        ReadOnly="True"></asp:TextBox>
                                </td>
                                <td align="right" style="width: 20%">
                                    <asp:Label ID="custNameLab" runat="server" Text="Label"></asp:Label>
                                </td>
                                <td style="width: 30%">
                                    <asp:TextBox ID="custNameBox" runat="server" Style="width: 80%" CssClass="textborder" ReadOnly="True"
                                        Width="88px"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                        <asp:GridView ID="ContractHistoryView" runat="server" Width="100%" AllowPaging="True"
                            AllowSorting="True" Font-Size="Smaller" Style="text-align: right">
                        </asp:GridView>
                    </td>
                </tr>
            </table>
            <asp:Label ID="backLink" runat="server" Text="Label" Style="color: blue; text-decoration: underline" Visible="False"></asp:Label>
            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="javascript:history.back();">Back</asp:HyperLink>
        </div>
    </form>
</body>
</html>
