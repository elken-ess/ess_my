<%@ Page Language="VB" AutoEventWireup="false" CodeFile="MasterApptScheduleRS.aspx.vb" Inherits="PresentationLayer_function_MasterApptScheduleRS_aspx" %>

<%@ Register Assembly="JCalendar" Namespace="JCalendar" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Master Appointment Schedule - Rental</title>
    <meta http-equiv="Content-Type" content="text/html; charset=gb2312"/>
    <link href="../css/style.css" type="text/css" rel="stylesheet"/>
     <style type="text/css">A:link { COLOR: #336666; TEXT-DECORATION: none }
	BODY { FONT-SIZE: 9pt; FONT-FAMILY: "Arial", "Verdana", "Tahoma"; BACKGROUND-COLOR: #ffffff }
	td { FONT-SIZE: 9pt; FONT-FAMILY: Arial,Verdana,Tahoma }
	</style>
<script language="JavaScript" src="../js/common.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    <table cellSpacing="0" cellPadding="0" width="100%" border="0">
		<tr>
		  <td width="1%" background="../graph/title_bg.gif"><IMG height="24" src="../graph/title1.gif" width="5"></td>
		  <td class="style2" width="98%" background="../graph/title_bg.gif">
             <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
		  <td align="right" width="1%" background="../graph/title_bg.gif"><IMG height="24" src="../graph/title_2.gif" width="5"></td>
		</tr>
		<tr><td></td> 
		  <td>
		   <asp:Label ID="lblMessage" runat="server" ForeColor="red" Text=""></asp:Label>
		   </td> 
	   </tr>
	   <tr>
	    <td></td>
	    <td>
            <asp:Label ID="lblNoteUP" runat="server" ForeColor="Red" Text="Label"></asp:Label></td>
	   </tr>
	 </table>
            <table style="width: 100%" bgColor="#b7e6e6" border="0" cellpadding="0">
                <tr bgColor="#ffffff">
                    <td align="left" style="width: 15%" valign="middle">
                        <asp:Label ID="lblApptDate" runat="server" Text="Label"></asp:Label>
                        <font color="red">*</font></td>
                    <td align="left" style="width: 20%;" valign="middle">
                        <asp:TextBox ID="txtApptDate" runat="server" CssClass="textborder" Width="50%" Enabled="true" ReadOnly=false MaxLength =10></asp:TextBox>
                        <cc1:JCalendar ID="MyCalendar" ImgUrl="~/PresentationLayer/graph/calendar.gif" ControlToAssign="txtApptDate" runat="server" Visible="False" />
                        <asp:HyperLink ID="HypCal" runat="server" ImageUrl="~/PresentationLayer/graph/calendar.gif"
                            ToolTip="Choose a Date">Choose a Date</asp:HyperLink>
                    </td>
                    <td align="left" style="width: 15%" valign="middle">
                        <asp:Label ID="lblSVC" runat="server" Text="Label"></asp:Label>
                        <font color="red">*</font></td>
                    <td align="left" style="width: 100%" valign="middle">
                        &nbsp;<asp:DropDownList ID="SerCenIDDrop" runat="server" AutoPostBack="false" Width="95%">
                        </asp:DropDownList><asp:RequiredFieldValidator ID="SerCenIDerr" runat="server" ControlToValidate="SerCenIDDrop"
                                    Width="2px" Display="None" ForeColor="White">*</asp:RequiredFieldValidator></td>
                </tr>
                <tr bgColor="#ffffff">
                    <td align="left" style="width: 15%" valign="middle">
                        <asp:Label ID="lblTotalSSApt" runat="server" Text="Total RS Apt"></asp:Label>
                        </td>
                    <td align="left" style="width: 20%" valign="middle">
                        <asp:TextBox ID="txtTotalSSApt" runat="server" CssClass="textborder" Width="50%" Enabled="False"></asp:TextBox>
                    </td>
                    <td align="left" style="width: 15%" valign="middle">
                        <asp:Label ID="lblJobServiceType" runat="server" Text="Job Service Type" Width="96px"></asp:Label>
                        </td>
                    <td align="left" style="width: 50%" valign="middle">
                        <asp:DropDownList ID="cboJobServiceType" runat="server" AutoPostBack="false" Width="20%">
                        </asp:DropDownList>
                        </td>
                </tr>
                
                <tr bgColor="#ffffff">
                    <td align="left" style="width: 15%" valign="middle">
                        <asp:Label ID="lblTotalASApt" runat="server" Text="Total AS Apt" Visible="False"></asp:Label>
                        </td>
                    <td align="left" style="width: 20%" valign="middle">
                        <asp:TextBox ID="txtTotalASApt" runat="server" CssClass="textborder" Width="50%" Enabled="False" Visible="False"></asp:TextBox>
                    </td>
                    <td align="middle" style="width: 50%" valign="middle" colspan="2">
                        <asp:LinkButton ID="lbtnRetrieve" runat="server">Retrieve</asp:LinkButton></td>
                </tr>
                <tr bgColor="#ffffff">
                    <td align="center" colspan="4" valign="middle">
                        <asp:GridView ID="ApptView" runat="server" BorderStyle="Solid" HorizontalAlign="Center" ShowFooter="True" Width="100%" AutoGenerateColumns="False">
                            <Columns>
                             <asp:TemplateField>
                               <ItemTemplate>
                                 <asp:linkbutton ID="viewbutton1" CommandName='<%# Bind("ZoneID") %>' CommandArgument="ZoneID" runat="server" Text='<%# Bind("ZoneID") %>'></asp:linkbutton>
                               </ItemTemplate>
                             </asp:TemplateField>
                             <asp:TemplateField>
                               <ItemTemplate>
                                 <asp:linkbutton ID="viewbutton2" CommandName='<%# Bind("ZoneID") %>' CommandArgument="0900" runat="server" Text='<%# Bind("0900") %>'></asp:linkbutton>
                               </ItemTemplate>
                             </asp:TemplateField> 
                            <asp:TemplateField>
                               <ItemTemplate>
                                 <asp:linkbutton ID="viewbutton3" CommandName='<%# Bind("ZoneID") %>' CommandArgument="1030" runat="server" Text='<%# Bind("1030") %>'></asp:linkbutton>
                               </ItemTemplate>
                             </asp:TemplateField> 
                            <asp:TemplateField>
                               <ItemTemplate>
                                 <asp:linkbutton ID="viewbutton4" CommandName='<%# Bind("ZoneID") %>' CommandArgument="1200" runat="server" Text='<%# Bind("1200") %>'></asp:linkbutton>
                               </ItemTemplate>
                             </asp:TemplateField> 
                            <asp:TemplateField>
                               <ItemTemplate>
                                 <asp:linkbutton ID="viewbutton5" CommandName='<%# Bind("ZoneID") %>' CommandArgument="1330" runat="server" Text='<%# Bind("1330") %>'></asp:linkbutton>
                               </ItemTemplate>
                             </asp:TemplateField> 
                            <asp:TemplateField>
                               <ItemTemplate>
                                 <asp:linkbutton ID="viewbutton6" CommandName='<%# Bind("ZoneID") %>' CommandArgument="1500" runat="server" Text='<%# Bind("1500") %>'></asp:linkbutton>
                               </ItemTemplate>
                             </asp:TemplateField> 
                            <asp:TemplateField>
                               <ItemTemplate>
                                 <asp:linkbutton ID="viewbutton7" CommandName='<%# Bind("ZoneID") %>' CommandArgument="1630" runat="server" Text='<%# Bind("1630") %>'></asp:linkbutton>
                               </ItemTemplate>
                             </asp:TemplateField>      
                            <asp:TemplateField>
                               <ItemTemplate>
                                 <asp:linkbutton ID="viewbutton8" CommandName='<%# Bind("ZoneID") %>' CommandArgument="1800" runat="server" Text='<%# Bind("1800") %>'></asp:linkbutton>
                               </ItemTemplate>
                             </asp:TemplateField>     
                            <asp:TemplateField>
                               <ItemTemplate>
                                 <asp:linkbutton ID="viewbutton9" CommandName='<%# Bind("ZoneID") %>' CommandArgument="1930" runat="server" Text='<%# Bind("1930") %>'></asp:linkbutton>
                               </ItemTemplate>
                             </asp:TemplateField>     
                            <asp:TemplateField>
                               <ItemTemplate>
                                 <asp:linkbutton ID="viewbutton10" CommandName='<%# Bind("ZoneID") %>' CommandArgument="2100" runat="server" Text='<%# Bind("2100") %>'></asp:linkbutton>
                               </ItemTemplate>
                             </asp:TemplateField>     
                            <asp:TemplateField>
                               <ItemTemplate>
                                 <asp:linkbutton ID="viewbutton11" CommandName='<%# Bind("ZoneID") %>' CommandArgument="Others" runat="server" Text='<%# Bind("Others") %>'></asp:linkbutton>
                               </ItemTemplate>
                             </asp:TemplateField>         
                             <asp:BoundField  DataField="Total"/>
                                
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
            <table width=100%>
                <tr>
                    <td>
                        <asp:Label ID="lblNoteDown" runat="server" ForeColor="Red" Text="Label"></asp:Label></td>
                </tr>
            </table>
    </div>
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
            ShowSummary="False" />
    </form>
</body>
</html>
