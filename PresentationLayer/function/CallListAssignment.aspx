<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CallListAssignment.aspx.vb" Inherits="PresentationLayer_function_CallListAssignment_aspx" EnableEventValidation="false"%>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<%@ Register Assembly="MessageBox" Namespace="Utilities" TagPrefix="cc1" %>
<%@ Register Assembly="JCalendar" Namespace="JCalendar" TagPrefix="cc2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Call List Assignment</title>
    <meta http-equiv="Content-Type" content="text/html; charset=gb2312"/>
	<link href="../css/style.css" type="text/css" rel="stylesheet"/>
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
</head>
<body>
<!--
<script language = "javascript">


   function sstchur_SmartScroller_GetCoords()
   {
      var scrollX, scrollY;
      
      if (document.all)
      {
         if (!document.documentElement.scrollLeft)
            scrollX = document.body.scrollLeft;
         else
            scrollX = document.documentElement.scrollLeft;
               
         if (!document.documentElement.scrollTop)
            scrollY = document.body.scrollTop;
         else
            scrollY = document.documentElement.scrollTop;
      }   
      else
      {
         scrollX = window.pageXOffset;
         scrollY = window.pageYOffset;
      }
   
      document.forms['form1'].xbox.value = scrollX;
      document.forms['form1'].ybox.value = scrollY;
   }
   
   function sstchur_SmartScroller_Scroll()
   {
      var x = document.forms['form1'].xbox.value;
      var y = document.forms['form1'].ybox.value;
      window.scrollTo(x, y);
   }
   
   window.onload = sstchur_SmartScroller_Scroll;
   window.onscroll = sstchur_SmartScroller_GetCoords;
   window.onkeypress = sstchur_SmartScroller_GetCoords;
   window.onclick = sstchur_SmartScroller_GetCoords;

</script>
// -->


    <form id="form1" runat="server">
    <div>
        <table id="uagTab" border="0" style="width: 100%">
            <tr>
                <td style="width: 80%">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title1.gif" width="5" /></td>
                            <td background="../graph/title_bg.gif" class="style2" width="98%">
                                <asp:Label ID="callListTitleLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="right" background="../graph/title_bg.gif" width="1%">
                                <img height="24" src="../graph/title_2.gif" width="5" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 80%">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../graph/title_bg.gif" class="style2" width="98%">
                                <font color="red">
                                    <asp:Label ID="ErrLab" runat="server" Text="Label" Visible="False"></asp:Label></font></td>
                        </tr>
                    </table>
                    </td>
            </tr>
            <tr>
                <td style="height: 200px; width: 80%;">
                    <table id="userGroup" bgcolor="#b7e6e6" border="0" cellpadding="1" cellspacing="1" style="border-bottom: black 1px solid;" width="100%">
                        <tr bgcolor="#ffffff">
                            <td align="left" style="width: 10%; height: 22px;">
                                <asp:Label ID="scFLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 20%; height: 22px;"><asp:DropDownList ID="scFDropDownList" runat="server" CssClass="textborder" Width="100%" AutoPostBack =true  >
                            </asp:DropDownList></td>
                            <td align="left" style="width: 10%; height: 22px;">
                                <asp:Label ID="scTLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 25%; height: 22px;"><asp:DropDownList ID="scTDropDownList" runat="server" CssClass="textborder" Width="100%"  AutoPostBack =true>
                            </asp:DropDownList></td>
                            <td align="left" style="height: 42px; width: 10%;">
                            <asp:Label ID="assignTLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 20%; height: 42px;"><asp:DropDownList ID="userDropDownList" runat="server" AutoPostBack="true" CssClass="textborder" Width="100%">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvAssignedTo" runat="server" ControlToValidate="userDropDownList"
                            ErrorMessage="*" Visible="False">*</asp:RequiredFieldValidator></td>
                           <!-- 
                            <td align="left" style="width: 15%">
                            <asp:Label ID="reminderLab" runat="server" Text="Label"></asp:Label></td>
                           <td align="left" style="width: 20%">
                            <asp:DropDownList ID="cboOperation" runat="server" AutoPostBack="false" Width="56px">
                                <asp:ListItem></asp:ListItem>
                                <asp:ListItem Value="greater">&gt;=</asp:ListItem>
                                <asp:ListItem Value="egual">=</asp:ListItem>
                                <asp:ListItem Value="less">&lt;=</asp:ListItem>
                                </asp:DropDownList>
                                <asp:TextBox ID="reminderMonth" runat="server" CssClass="textborder" MaxLength="2" Width="18px"></asp:TextBox>
                                /
                                <asp:TextBox ID="reminderYear" runat="server" CssClass="textborder" MaxLength="4" Width="36px"></asp:TextBox>
                                </td>-->
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="left" style="width: 10%">
                                <asp:Label ID="stateFLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 20%">

                               <asp:DropDownList ID="stateFDropDownList" runat="server" CssClass="textborder" Width="100%" AutoPostBack =true >
                                             </asp:DropDownList>
                            </td>
                            <td align="left" style="width: 10%">
                                <asp:Label ID="stateTLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 25%"><asp:DropDownList ID="stateTDropDownList" runat="server" CssClass="textborder" Width="100%"  AutoPostBack =true>
                            </asp:DropDownList></td>
                            <td align="left" style="width: 10%; height: 22px;">
                                <asp:Label ID="stLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 20%; height: 22px;">
                                <asp:DropDownList ID="stDropDownList" runat="server" CssClass="textborder" Width="100%">
                                    <asp:ListItem Value ="ALL">ALL</asp:ListItem>
                                    <asp:ListItem Value ="MF">MF</asp:ListItem>
                                    <asp:ListItem Value ="MM">MM</asp:ListItem>
                                    <asp:ListItem Value ="M1">M1</asp:ListItem>
                                    <asp:ListItem Value ="M2">M2</asp:ListItem>
                                    <asp:ListItem Value ="M3">M3</asp:ListItem>
                                    <asp:ListItem Value ="M4">M4</asp:ListItem>
                                </asp:DropDownList></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="left" style="width: 10%; height: 22px;">
                                <asp:Label ID="areaFLab" runat="server" Text="Area ID From"></asp:Label>
                                </td>
                            <td align="left" style="width: 20%; height: 22px;">
                            <asp:DropDownList ID="areaFDropDownList" runat="server" CssClass="textborder" Width="100%">
                            </asp:DropDownList>
                            </td>
                            <td align="left" style="width: 10%; height: 22px;">
                            <asp:Label ID="lblAreaIDTo" runat="server" Text="Area ID To"></asp:Label>
                                </td>
                            <td align="left" style="width: 20%; height: 22px;">
                            <asp:DropDownList ID="cboAreaIdTo" runat="server" CssClass="textborder" Width="100%">
                            </asp:DropDownList>
                            </td>
                            <td align="left" style="width: 10%; height: 22px;">
                                <asp:Label ID="releasedLab" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" style="width: 20%; height: 22px;">
                                <asp:DropDownList ID="releasedDropDownList" runat="server" CssClass="textborder" Width="100%">
                                </asp:DropDownList></td>
                        </tr>
                        <tr bgcolor="#ffffff">
                        <td><asp:Label ID="lblFrmDueDate" runat="server" Text="Label"></asp:Label></td>
                         <td><asp:TextBox ID="txtFrmDueDate" runat="server" width="70%"  MaxLength="10"></asp:TextBox>
                        <cc2:JCalendar ID="JCFrmDueDate" runat="server" Enabled="true" ImgURL="~/PresentationLayer/graph/calendar.gif" ControlToAssign="txtFrmDueDate" Visible="True" /></td>
                        <td><asp:Label ID="lblToDueDate" runat="server" Text="Label"></asp:Label></td>
                        <td><asp:TextBox ID="txtToDueDate" runat="server" width="70%"  MaxLength="10"></asp:TextBox>
                        <cc2:JCalendar ID="JCToDueDate" runat="server" Enabled="true" ImgURL="~/PresentationLayer/graph/calendar.gif" ControlToAssign="txtToDueDate" Visible="True" /></td>
                        <td>
                        <asp:Label ID="lblProductClass" runat="server" Text="Label"></asp:Label></td>
                        <td><asp:DropDownList ID="drpProdClass" runat="server" CssClass="textborder" Width="100%">
                        </asp:DropDownList>
                        </tr>
                        <tr style ="background-color: #ffffff">
                        <td align="left" style="width: 15%"> 
                                <asp:Label ID="lblTotalRecord" runat="server" Visible="true"></asp:Label>
                            </td>
                            <td align="left"> 
                                <asp:TextBox ID="txtTotalRecord" runat="server" CssClass="textborder" readonly=true  Width="100px"></asp:TextBox>
                            </td>
                            <td><asp:Label ID="sortLab1" runat="server" Text="Label"></asp:Label></td>
                            <td align="left" ><asp:DropDownList ID="sortDropDownList1" runat="server" CssClass="textborder" Width="100%">
                            </asp:DropDownList></td>
                            <td><asp:Label ID="sortLab2" runat="server" Text="Label"></asp:Label></td>
                            <td><asp:DropDownList ID="sortDropDownList2" runat="server" CssClass="textborder" Width="100%">
                            </asp:DropDownList></td>
                        </tr>
                        <tr style ="background-color: #ffffff">                         
                            <td align="right" style="width: 15%; height: 42px;" colspan ="5">
                            <asp:Label ID="lblRelease" runat="server" Visible="False"></asp:Label></td>
                            <td align="left" style="width: 20%; height: 42px;">
                            <asp:LinkButton ID="retriveButton" runat="server" CausesValidation="False">retriveButton</asp:LinkButton></td></tr>
                    </table>
                    <table id ="View" border = "0" cellpadding="1" cellspacing="1">
                        <tr>
                            <td align ="left" style=" width: 100%; border-top-width: 1px; border-top-color: #000000;" >
                                &nbsp;
                                <asp:Label ID="stateLab" runat="server" Text="Label" Visible="False"></asp:Label></td>
                        </tr>
                    </table>
                    <input id="xbox" type="hidden" value="0" />
                    <input id="ybox" type="hidden" value="500" />
                    <div  >
                    <table id ="GridView" border = "0" cellpadding ="1" cellspacing = "1" width="100%">
                        <tr>
                            <td align = "left" style=" width : 100%; height: 100%;">
                            <asp:GridView ID="callListGridView" runat="server" Width="100%" Height="100%" AllowPaging="True" AutoGenerateColumns="False" ShowFooter="True" >
                            <Columns> 
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="GridViewCheck"  OnCheckedChanged="CheckBox_CheckedChanged"  runat="Server" ToolTip="<%#Container.DataItemIndex%>" CommandArgument= "<%#Container.DataItemIndex%>" Checked ='<%# Bind("RELFG") %>' AutoPostBack="True"/>
                                        
                                    </ItemTemplate>
                                </asp:TemplateField>                                
                                <asp:BoundField DataField="CustomerIDandName" ReadOnly="True" />
                                <asp:BoundField DataField="MROU_MODID" ReadOnly="True" />
                                <asp:BoundField DataField="MADR_AREID" ReadOnly="True" />
                                <asp:BoundField DataField="MROU_LSVDT" ReadOnly="True" />
                                <asp:BoundField DataField="MROU_NSVDT" ReadOnly="True" />
                                <asp:BoundField DataField="MROU_RELFG" ReadOnly="True" />
                                <asp:BoundField DataField="MROU_BATCH" ReadOnly="True" />
                                <asp:BoundField DataField="MROU_FSRVTY" ReadOnly="True" />
                                <asp:BoundField DataField="MROU_CSTAT" ReadOnly="True" />
                                <asp:BoundField DataField="MROU_ROUID" ReadOnly="True" />
                            </Columns>
                            </asp:GridView>
                            </td>
                        </tr>         
                        <tr >
                        <td align = "center" style=" width : 100%;">
                        <br /><br /><br /><br />
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                    <asp:LinkButton ID="printButton" runat="server" CausesValidation="False">Print</asp:LinkButton>
                    &nbsp;&nbsp; &nbsp; &nbsp;
                    <asp:Label ID="releaseLimitLab" runat="server" Text="release limit"></asp:Label>
                    &nbsp;
                    <asp:TextBox ID="releaseLimitBox" runat="server" Width="40px" MaxLength="5"></asp:TextBox>
                    &nbsp; &nbsp;
                    <asp:Label ID="totalReleaseLab" runat="server" Text="Total Release"></asp:Label>&nbsp;
                    <asp:TextBox ID="totalReleaseBox" runat="server" Width="40px" ReadOnly="True"></asp:TextBox>
                    &nbsp;&nbsp;
                    <asp:LinkButton ID="markAllButton" runat="server" CausesValidation="False">Mark All 'x'</asp:LinkButton>
                    &nbsp;&nbsp;
                    <asp:LinkButton ID="saveButton" runat="server">Save</asp:LinkButton>
                    &nbsp;&nbsp;
                        </td>
                        </tr>           
                    </table>                    
                    </div>
                    
                </td>
            </tr>
            <%--<tr>
                <td style="height: 35px; width: 100%;">
                    <table id="Table1" border="0" cellpadding="1" cellspacing="1">
                        <tr>
                            <td align="center" style="width: 20%; height: 16px;">
                                &nbsp;</td>
                            <td align="center" style="height: 16px; width: 356px;">
                                </td>
                        </tr>
                    </table>
                </td>
            </tr>--%>
        </table>
    
    </div>
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
            ShowSummary="False" />
        &nbsp;&nbsp;<br />
       <asp:RangeValidator ID="RangeValidatorLimit" runat="server" ControlToValidate="releaseLimitBox" MinimumValue="1" Type="Integer" MaximumValue="99999" Display="None"></asp:RangeValidator>
        <asp:RangeValidator ID="RangeValidatorYear" runat="server" ControlToValidate="reminderYear" Type="Integer" MaximumValue="9999" MinimumValue="2000" Display="None"></asp:RangeValidator>
        <asp:RangeValidator ID="RangeValidatorMonth" runat="server" ControlToValidate="reminderMonth" MaximumValue="12" MinimumValue="1" SetFocusOnError="True" Type="Integer" Display="None"></asp:RangeValidator>
        <cc1:MessageBox ID="MessageBox1" runat="server" />
        <cc1:MessageBox ID="MessageBox2" runat="server" />
        <CR:CrystalReportViewer ID="CrystalReportViewer" runat="server" AutoDataBind="true" BestFitPage="False" DisplayGroupTree="False" />
        &nbsp;&nbsp;
    </form>
</body>
</html>
