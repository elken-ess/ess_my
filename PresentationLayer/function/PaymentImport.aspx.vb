Imports BusinessEntity
Imports System.Data
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Windows
Imports Microsoft.Win32
Imports System.IO


Partial Class PresentationLayer_function_PaymentImport
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Label1.Visible = False
    End Sub

    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        ' Upload and save file
        Dim csvPath As String = Server.MapPath("~\\App_Data\CIMB.csv")
        FileUpload1.SaveAs(csvPath)

        Dim dt As New DataTable()
        dt.Columns.AddRange(New DataColumn(8) {New DataColumn("CCName", GetType(String)), New DataColumn("CCNo", GetType(String)), New DataColumn("CCExpiry", GetType(String)), New DataColumn("Amt", GetType(String)), New DataColumn("NetAmt", GetType(String)), New DataColumn("ServiceCode", GetType(String)), New DataColumn("Remarks", GetType(String)), New DataColumn("Response", GetType(String)), New DataColumn("ApprovalCode", GetType(String))})

        Dim csvData As String = File.ReadAllText(csvPath)

        For Each row As String In csvData.Split(ControlChars.Lf)
            If Not String.IsNullOrEmpty(row) Then
                dt.Rows.Add()
                Dim i As Integer = 0

                For Each cell As String In row.Split(","c)
                    dt.Rows(dt.Rows.Count - 1)(i) = cell
                    i += 1
                Next
            End If
        Next



        'declaration of connection string
        Dim Conn As New SqlConnection
        Conn.ConnectionString = ConfigurationManager.AppSettings("ConnectionString").ToString

        'selecting row counts from dbo.Master to check if to Truncate table
        Dim Count As String = ""

        Using Comm As New SqlClient.SqlCommand("Select Count(*) [Count] from dbo.CIMB_View ", Conn)
            Conn.Open()
            Using readerObj As SqlClient.SqlDataReader = Comm.ExecuteReader
                While readerObj.Read
                    Count = readerObj("Count").ToString
                End While
            End Using
            Conn.Close()
        End Using

        'saving data from csv into the database
        If Count = 0 Then
            Using bulk As New SqlBulkCopy(Conn)
                ' Set database table name
                bulk.DestinationTableName = "dbo.CIMB_View"
                Conn.Open()
                bulk.WriteToServer(dt)
                Conn.Close()
            End Using
        Else
            Using command As New SqlCommand("Truncate Table dbo.CIMB_View", Conn)
                Conn.Open()
                command.ExecuteNonQuery()
                Conn.Close()
            End Using

            Using command As New SqlCommand("Truncate Table dbo.CIMB_View", Conn)
                Conn.Open()
                command.ExecuteNonQuery()
                Conn.Close()
            End Using

            Using bulk As New SqlBulkCopy(Conn)
                ' Set database table name
                bulk.DestinationTableName = "dbo.CIMB_View"
                Conn.Open()
                bulk.WriteToServer(dt)
                Conn.Close()
            End Using
        End If
        Try
            Using command As New SqlCommand("Insert into dbo.CIMB select " _
                                                    + "CCName, " _
                                                    + "CCNo, " _
                                                    + "CCEx, " _
                                                    + "convert(decimal(11,2),replace(Amt,'$','')), " _
                                                    + "convert(decimal(11,2),replace(NetAmt,'$','')), " _
                                                    + "ServiceCode, " _
                                                    + "Remarks, " _
                                                    + "Response, " _
                                                    + "ApprovalCode, " _
                                                    + "getdate(), NULL from dbo.CIMB_View ", Conn)
                Conn.Open()
                command.ExecuteNonQuery()
                Conn.Close()
            End Using

            GridView1.DataSource = dt
            GridView1.DataBind()
            GridView1.Visible = True
        Catch ex As Exception
            Label1.Text = "There's an exception during record update. Pls contact GIT to check."
            Label1.Visible = True
        End Try
        
    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim Conn As New SqlConnection
        Conn.ConnectionString = ConfigurationManager.AppSettings("ConnectionString").ToString

        Try
            Using command As New SqlCommand("exec dbo.BB_IMPORTPAYMENT_SAVE '" + Session("userID") + "'", Conn)
                Conn.Open()
                Dim dr1 As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection)
                Dim dt1 As DataTable = New DataTable()
                dt1.Load(dr1)

                GridView1.DataSource = dt1
                GridView1.DataBind()

                GridView1.Visible = True

                Conn.Close()
            End Using

            Label1.Text = "Record saved successfully. Please check for failed records at FailReason column if there are any."
            Label1.Visible = True
        Catch ex As Exception
            Label1.Text = "There's an exception during record update. Pls contact GIT to check."
            Label1.Visible = True
        End Try
        
    End Sub
End Class
