﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="contract.aspx.vb" Inherits="PresentationLayer_function_contract" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Untitled Page</title>
    <link href="../css/style.css" type="text/css" rel="stylesheet">
</head>
<body>
    <form id="form1" runat="server">
        <div style="vertical-align: top">
            <table style="width: 100%;" border="0">
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0" width="100%" border="0">
                            <tr>
                                <td width="1%" background="../graph/title_bg.gif">
                                    <img height="24" src="../graph/title1.gif" width="5"></td>
                                <td class="style2" width="98%" background="../graph/title_bg.gif">
                                    <asp:Label ID="titleLab" runat="server" Text="Label"></asp:Label></td>
                                <td align="right" background="../graph/title_bg.gif" style="width: 1%">
                                    <img height="24" src="../graph/title_2.gif" width="5"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="1" cellpadding="2" bgcolor="#b7e6e6" border="0" style="width: 100%">
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width: 20%">
                                    <asp:Label ID="cntNoLab" runat="server" Text="Label"></asp:Label>
                                </td>
                                <td style="width: 30%">
                                    <asp:TextBox ID="cntNoBox" runat="server" Width="80%" CssClass="textborder" MaxLength="10"></asp:TextBox>
                                </td>
                                <td align="right" style="width: 20%">
                                    <asp:Label ID="servCIDLab" runat="server" Text="Label"></asp:Label>
                                </td>
                                <td style="width: 30%">
                                    <asp:TextBox ID="servCIDBox" runat="server" Width="80%" CssClass="textborder" MaxLength="10"></asp:TextBox>
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="right" style="width: 20%">
                                    <asp:Label ID="custNameLab" runat="server" Text="Label"></asp:Label>
                                </td>
                                <td style="width: 30%">
                                    <asp:TextBox ID="custNameBox" runat="server" Width="80%" CssClass="textborder" MaxLength="50"></asp:TextBox>
                                </td>
                                <td align="right" style="width: 20%">
                                    <asp:Label ID="lblContractType" runat="server" Text="Contract Type"></asp:Label>
                                </td>
                                <td style="width: 30%">
                                    <asp:TextBox ID="txtContractType" runat="server" Width="80%" CssClass="textborder" MaxLength="30"></asp:TextBox>
                                </td>
                            </tr>
                            
                            <tr bgcolor="#ffffff">
                                
                                <td align="right" style="width: 20%">
                                     <asp:Label ID="lblCustomerID" runat="server" Text="Customer ID"></asp:Label>
                                </td>
                                <td style="width: 30%">
                                    <asp:TextBox ID="txtCustomerID" runat="server" Width="80%" CssClass="textborder" MaxLength="50"></asp:TextBox>
                                </td>
                                <td align="right" style="width: 20%">
                                    <asp:Label ID="lblSerialNo" runat="server" Text="Serial No"></asp:Label>
                                </td>
                                <td style="width: 30%">
                                    <asp:TextBox ID="txtSerialNo" runat="server" Width="80%" CssClass="textborder" MaxLength="20"></asp:TextBox>
                                </td>
                            </tr>
                            
                            <tr bgcolor="#ffffff">
                                <td  align="right" style="width: 20%">
                                    <asp:Label ID="cntStatusLab" runat="server" Text="Label"></asp:Label>
                                </td>
                                <td style="width: 30%">
                                    <asp:DropDownList ID="cntStatusDrpList" Width="80%" runat="server">
                                    </asp:DropDownList>
                                </td>
                                <td style="width: 20%">
                                    <asp:LinkButton ID="searchButton" runat="server"></asp:LinkButton>
                                </td>
                                <td style="width: 30%">
                                    <asp:LinkButton ID="addButton" runat="server" PostBackUrl="~/PresentationLayer/function/addcontract.aspx"></asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                        <asp:GridView ID="cntView" runat="server" Width="100%" AllowPaging="True" AllowSorting="True"
                            Font-Size="Smaller">
                        </asp:GridView>
                        <asp:Label ID="lblNoRecord" runat="server" ForeColor="Red" Text="Label"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
