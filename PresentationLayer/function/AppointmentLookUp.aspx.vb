Imports BusinessEntity
Imports System.Configuration
Imports System.Xml
Imports SQLDataAccess
Imports System.Data.SqlClient
Imports System.Web
Imports System.Data
Imports System.Windows.Forms


Partial Class PresentationLayer_function_AppointmentLookUp
    Inherits System.Web.UI.Page
    Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        Try
            If (Session("userID").ToString() = Nothing Or Session("userID").ToString() = "") Then
                Dim script As String = "top.location='../logon.aspx';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
                Return
            End If
        Catch ex As Exception
            Dim script As String = "top.location='../logon.aspx';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "logon", script, True)
            Return
        End Try

        If (Not Page.IsPostBack) Then
            Try
                InitializePage()
            Catch

            End Try
        End If
    End Sub

    Private Sub InitializePage()
        Dim CommonClass As New clsCommonClass()
        Dim hst As New Hashtable
        Dim objXmlTr As New clsXml
        Dim strCompany As String = ""
        Dim strServiceCenter As String = ""
        Dim strServiceType As String = ""
        Dim strApptDate As String = ""
        Dim strTechnician As String = ""

        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        If Not Session("htSvcBill") Is Nothing Then
            hst = Session("htSvcBill")
            strServiceType = hst.Item("ServiceType")
            strCompany = hst.Item("Company")
            strServiceCenter = hst.Item("ServiceCenter")
            strApptDate = hst.Item("Date")
            strTechnician = hst.Item("Technician")
        End If

        CommonClass.spstat = IIf(strCompany = "", Session("login_cmpID"), strCompany)
        CommonClass.sparea = IIf(strServiceCenter = "", Session("login_svcID"), strServiceCenter)
        txtDate.Text = strApptDate

        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")

        Me.lblAppointmentDate.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-APPOINTMENTDATE")
        Me.lblAppointmentNo.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-APPOINMENTNO")
        Me.lblCustomerID.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-CUSTOMERID")
        Me.lblStatus.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-STATUS")
        Me.lblSericeCenter.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-SERVICECENTER")
        Me.lblTechnician.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-TECHNICIAN")
        Me.lnkSearch.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-SEARCH")
        Me.lblServiceType.Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-SERVICETYPE")
        Me.lnkBack.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-LOGIN-04")

        Dim userIDNamestr As String = Session("userID").ToString().ToUpper + "-" + Session("username").ToString.ToUpper
        Dim rank As String = Session("login_rank")

        If rank <> 0 Then
            CommonClass.spctr = Session("login_ctryID").ToString().ToUpper
        End If
        CommonClass.rank = rank
        CommonClass.spstat = "ACTIVE"

        Dim util As New clsUtil()
        Dim statParam As ArrayList = New ArrayList

        Dim count As Integer
        Dim statid As String
        Dim statnm As String
        Dim stat As String = Request.QueryString("stat")

        statParam = util.searchconfirminf("APPTStatus")
        Me.cboStatus.Items.Insert(0, "")
        Dim i As Integer = 1
        For count = 0 To statParam.Count - 1
            statid = statParam.Item(count)
            statnm = statParam.Item(count + 1)
            count = count + 1
            Me.cboStatus.Items.Add(New ListItem(statnm.ToString(), statid.ToString()))
            If UCase(statid) = UCase(stat) Then
                cboStatus.SelectedIndex = i
                cboStatus.Items(i).Selected = True
            End If
            i = i + 1
        Next

        If stat <> "" Then
            cboStatus.Enabled = False
        End If

        '---- SERVICE TYPE
        Dim dsSerType As New DataSet
        dsSerType = CommonClass.Getcomidname("BB_MASSRCT_IDNAMEALL")
        If dsSerType.Tables.Count <> 0 Then
            databonds(dsSerType, Me.cboServiceType)
        End If
        Me.cboServiceType.Items.Insert(0, "")
        Me.cboServiceType.SelectedValue = Trim(strServiceType)

        Dim dsServiceCentere As New DataSet
        CommonClass.spstat = IIf(strCompany = "", Session("login_cmpID"), strCompany)

        dsServiceCentere = CommonClass.Getcomidname("BB_MASSVRC_IDNAME")

        If dsServiceCentere.Tables.Count <> 0 Then
            databonds(dsServiceCentere, Me.cboServiceCenter)
        End If
        Me.cboServiceCenter.Items.Insert(0, "")
        Me.cboServiceCenter.SelectedIndex = 0
        Me.cboServiceCenter.SelectedValue = IIf(strServiceCenter = "", Session("login_svcID"), strServiceCenter)
        If rank = 9 Then
            Me.cboServiceCenter.Enabled = False
        End If

        Dim dsTechnician As New DataSet
        dsTechnician = CommonClass.Getcomidname("BB_MASTECH_IDNAME")
        If dsTechnician.Tables.Count <> 0 Then
            databonds(dsTechnician, Me.cboTechnician)
        End If
        Me.cboTechnician.Items.Insert(0, "")
        Me.cboTechnician.SelectedIndex = 0
        Me.cboTechnician.SelectedValue = strTechnician


        If Session("htSvcBill") Is Nothing Then
            Exit Sub
        End If

        searchappointment()

    End Sub

    Private Sub databonds(ByVal ds As DataSet, ByVal dropdown As DropDownList)
        dropdown.Items.Clear()
        Dim row As DataRow
        For Each row In ds.Tables(0).Rows
            Dim NewItem As New ListItem()
            NewItem.Text = Trim(row("id") & "-" & row("name"))
            NewItem.Value = Trim(row("id"))
            dropdown.Items.Add(NewItem)
        Next

    End Sub

    Protected Sub cboServiceCenter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboServiceCenter.SelectedIndexChanged
        Dim Technician As New clsCommonClass
        Dim rank As String = Session("login_rank")
        If rank = 7 Then
            Technician.spctr = Session("login_ctryID").ToString().ToUpper
            Technician.spstat = Session("login_cmpID")
            Technician.sparea = cboServiceCenter.SelectedValue
        End If
        If rank = 8 Then
            Technician.spctr = Session("login_ctryID").ToString().ToUpper
            Technician.spstat = Session("login_cmpID")
            Technician.sparea = cboServiceCenter.SelectedValue
        End If
        If rank = 9 Then
            Technician.spctr = Session("login_ctryID").ToString().ToUpper
            Technician.spstat = Session("login_cmpID")
            Technician.sparea = Session("login_svcID")
        End If
        If rank = 0 Then
            Technician.spctr = Session("login_ctryID").ToString().ToUpper
        End If

        Technician.rank = rank


        Dim dsTechnician As New DataSet
        dsTechnician = Technician.Getcomidname("BB_MASTECH_IDNAME")
        If dsTechnician.Tables.Count <> 0 Then
            databonds(dsTechnician, Me.cboTechnician)
        End If
        Me.cboTechnician.Items.Insert(0, "")
        Me.cboTechnician.SelectedIndex = 0

    End Sub


    Protected Sub Calendar1_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendar1.SelectionChanged
        Me.txtDate.Text = Me.Calendar1.SelectedDate
        Calendar1.Visible = False
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Calendar1.Visible = True
    End Sub

    Private Sub InitializeGridColumns()
        'get column labels
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        Me.grdAppointment.HeaderRow.Cells(1).Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-APPOINMENTPF")
        Me.grdAppointment.HeaderRow.Cells(1).Font.Size = 8

        Me.grdAppointment.HeaderRow.Cells(2).Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-APPOINMENTNO")
        Me.grdAppointment.HeaderRow.Cells(2).Font.Size = 8

        Me.grdAppointment.HeaderRow.Cells(3).Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-APPOINTMENTDATE")
        Me.grdAppointment.HeaderRow.Cells(3).Font.Size = 8

        Me.grdAppointment.HeaderRow.Cells(4).Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-CUSTOMERPF")
        Me.grdAppointment.HeaderRow.Cells(4).Font.Size = 8

        Me.grdAppointment.HeaderRow.Cells(5).Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-CUSTOMERID")
        Me.grdAppointment.HeaderRow.Cells(5).Font.Size = 8

        Me.grdAppointment.HeaderRow.Cells(6).Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-CUSTOMER")
        Me.grdAppointment.HeaderRow.Cells(6).Font.Size = 8

        Me.grdAppointment.HeaderRow.Cells(7).Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-SERVICECENTERID")
        Me.grdAppointment.HeaderRow.Cells(7).Font.Size = 8

        Me.grdAppointment.HeaderRow.Cells(8).Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-SERVICECENTER")
        Me.grdAppointment.HeaderRow.Cells(8).Font.Size = 8

        Me.grdAppointment.HeaderRow.Cells(9).Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-TECHNICIANID")
        Me.grdAppointment.HeaderRow.Cells(9).Font.Size = 8

        Me.grdAppointment.HeaderRow.Cells(10).Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-TECHNICIAN")
        Me.grdAppointment.HeaderRow.Cells(10).Font.Size = 8

        Me.grdAppointment.HeaderRow.Cells(11).Text = objXmlTr.GetLabelName("EngLabelMsg", "SBA-SERVICETYPE")
        Me.grdAppointment.HeaderRow.Cells(11).Font.Size = 8

    End Sub

    Protected Sub lnkSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSearch.Click
        searchappointment()
    End Sub

    Protected Sub grdAppointment_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdAppointment.DataBound

    End Sub

    Protected Sub grdAppointment_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdAppointment.RowEditing
        Dim hst As New Hashtable
        Try
            hst = Session("htSvcBill")
            hst.Item("AppointmentNo") = grdAppointment.Rows(e.NewEditIndex).Cells(2).Text
            hst.Item("AppointmentPrefix") = grdAppointment.Rows(e.NewEditIndex).Cells(1).Text
            Session("htSvcBill") = hst

            ' Added by Tomas 20180613
            'Dim Conn As New SqlConnection
            'Conn.ConnectionString = ConfigurationManager.ConnectionStrings("ConnectionString").ToString

            'Using Comm As New SqlClient.SqlCommand("Select isnull(RMS_NO,'') from dbo.RMS_FIL where APPT_TRNTY = '" & hst.Item("AppointmentPrefix") & "' and APPT_TRNNO = '" & hst.Item("AppointmentNo") & "'", Conn)
            '    Conn.Open()
            '    Using readerObj As SqlClient.SqlDataReader = Comm.ExecuteReader
            '        While readerObj.Read
            '            Session("RMSNo") = readerObj("RMS_NO").ToString
            '        End While
            '    End Using
            '    Conn.Close()
            'End Using

            Response.Redirect("ServiceBillAdd.aspx")
        Catch

        End Try
    End Sub

    Protected Sub grdAppointment_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdAppointment.SelectedIndexChanged
        'Dim hst As New Hashtable
        'hst = Session("htSvcBill")
        'hst.Item("AppointmentNo") = grdAppointment.Rows(grdAppointment.SelectedIndex).Cells(2)
        'hst.Item("AppointmentPrefix") = grdAppointment.Rows(grdAppointment.SelectedIndex).Cells(1)
        'Session("htSvcBill") = hst
        'Response.Redirect("ServiceBillAdd.aspx")
    End Sub

    Private Sub searchappointment()
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

        Dim clsApptSearch As New clsServiceBillUpdate
        Dim ds As New DataSet
        Dim objXmlTr As New clsXml
        objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")

        If IsDate(txtDate.Text) Or txtDate.Text = "" Then

            ds = clsApptSearch.GetAppointments(Me.txtAppointmentPrefix.Text, _
                       Me.txtAppointmentNo.Text, _
                       Me.txtCustomerPrefix.Text, _
                       Me.txtCustomerID.Text, _
                       Me.cboServiceCenter.SelectedValue, _
                       Me.cboTechnician.SelectedValue, _
                       IIf(Me.txtDate.Text = "", "01/01/1900", Me.txtDate.Text), _
                       Me.cboStatus.SelectedValue, _
                       Me.cboServiceType.SelectedValue, _
                       Session("login_ctryID").ToString().ToUpper(), _
                       Session("userID").ToString().ToUpper)


            grdAppointment.DataSource = ds
            grdAppointment.DataBind()
            If ds.Tables(0).Rows.Count <> 0 Then
                Me.grdAppointment.Visible = True
                InitializeGridColumns()
                lblMessage.Text = ""
            Else

                lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-View-Fail")
                Me.grdAppointment.Visible = False
            End If
        Else
            lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "BB-MASJOBS-0008")
            Me.grdAppointment.Visible = False
        End If

    End Sub

    Protected Sub lnkBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkBack.Click
        Dim hst As New Hashtable
        Try
            hst = Session("htSvcBill")
            Session("htSvcBill") = hst
            Response.Redirect("ServiceBillAdd.aspx")
        Catch

        End Try
    End Sub

    Protected Sub JCalendar1_SelectedDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles JCalendar1.SelectedDateChanged
        If JCalendar1.SelectedDate > Now Then
            Dim objXmlTr As New clsXml
            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            lblMessage.Text = objXmlTr.GetLabelName("StatusMessage", "SBA-ERRFUTUREDATE")
            Me.txtDate.Text = Date.Now().ToShortDateString
        Else
            Me.txtDate.Text = JCalendar1.SelectedDate
        End If
    End Sub
End Class


