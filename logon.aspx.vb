﻿Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Imports System.Xml
Partial Class PresentationLayer_masterrecord_logon
    Inherits System.Web.UI.Page
    Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.MaintainScrollPositionOnPostBack = True
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        If Not Page.IsPostBack Then
            Session("login_count") = 3
            Dim objXmlTr As New clsXml
            Dim logoncls As New clsLogin()
            Dim lstrMessage, lstrDefaultMessage As String

            lstrDefaultMessage = "The connection between this application with the server had broken, please contract your administrator."

            'objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            'Me.UserNameLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-LOGIN-01") 'add new
            'Me.PSWBoxLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-LOGIN-02") 'add new
            'Me.mdfy.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-LOGIN-06") 'add new
            ''Me.LinkButton1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-LOGIN-08")
            'Me.UserNameBox.Focus()
            'Dim statXmlTr As New clsXml
            'statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            'Session("nameid") = "  "
            Try
                lstrMessage = logoncls.CheckConnection(ConfigurationSettings.AppSettings("ConnectionString"))
                If lstrMessage = "" Then
                    objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
                    Me.UserNameLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-LOGIN-01") 'add new
                    Me.PSWBoxLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-LOGIN-02") 'add new
                    Me.mdfy.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-LOGIN-06") 'add new
                    'Me.LinkButton1.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-LOGIN-08")
                    Me.UserNameBox.Focus()
                    Dim statXmlTr As New clsXml
                    statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
                    Session("nameid") = "  "
                Else
                    'msgLogin.Alert(lstrMessage)
                    msgLogin.Alert(lstrDefaultMessage)
                End If
            Catch ex As Exception
                'msgLogin.Alert(lstrMessage)
                msgLogin.Alert(lstrDefaultMessage)
            End Try

        End If


        Me.UserNameBox.Focus()
    End Sub
    

    Protected Sub mdfy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mdfy.Click
        Response.Redirect("~/PresentationLayer/masterrecord/mdyPSW.aspx?name=" + Me.UserNameBox.Text.ToString().ToUpper)
        ' redirectmdfy()
        'Response.Redirect("~/PresentationLayer/masterrecord/mdyPSW.aspx")
    End Sub
#Region "redirect to mdfy password page"
    Public Sub redirectmdfy()
        Dim statXmlTr As New clsXml
        statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        ' Me.UserNameLab.Text = objXmlTr.GetLabelName("EngLabelMsg", "BB-LOGIN-01") 'add new
        Dim logoncls As New clsLogin
        'If (Me.UserNameBox.Text = "admin") Then
        '    addinfo.Text = statXmlTr.GetLabelName("StatusMessage", "LOGSUPUS")
        '    addinfo.Visible = True
        'Else
        If (Me.UserNameBox.Text <> "") Then
            logoncls.UseID = Me.UserNameBox.Text.ToUpper
        End If
        If (Me.PSWBox.Text <> "") Then
            logoncls.PassWord = FormsAuthentication.HashPasswordForStoringInConfigFile(Me.PSWBox.Text, "md5")
        End If
        Dim drstat As SqlDataReader = Nothing
        Dim drpsw As SqlDataReader = Nothing
        drpsw = logoncls.checkNamePSW()

        If (drpsw.Read()) Then
            If drpsw("MUSR_USRID").ToString.ToUpper = "ADMIN" Then
                Session("username") = drpsw("MUSR_ENAME").ToString().ToUpper
                Session("userID") = drpsw("MUSR_USRID").ToString().ToUpper
                Session("accessgroup") = drpsw("MUSR_GRPID").ToString()
                Session("login_rank") = "0"

            End If
        End If
        
        drpsw = logoncls.checkNamePSW()
        drstat = logoncls.checkNameStat()

        If (drstat.Read()) Then
            If (drpsw.Read()) Then
                If (drpsw("MUSR_STAT").ToString() = "ACTIVE") Then
                    Response.Redirect("~/PresentationLayer/masterrecord/mdyPSW.aspx?name=" + Me.UserNameBox.Text.ToString().ToUpper)
                Else
                    addinfo.Text = statXmlTr.GetLabelName("StatusMessage", "MDYCHNMPSW")
                    addinfo.Visible = True
                End If
            End If
        Else
            Response.Redirect("~/logon.aspx")
            addinfo.Text = statXmlTr.GetLabelName("StatusMessage", "MDYCHNMPSW")
            addinfo.Visible = True
        End If

    End Sub
#End Region



    Protected Sub logon_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles logon.Click
        Dim statXmlTr As New clsXml
        statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
        Dim logoncls As New clsLogin()
        Dim changeornot As Boolean = False
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle

            'Else
        If (Me.UserNameBox.Text <> "") Then
            logoncls.UseID = Me.UserNameBox.Text.ToUpper
        End If

        If (Me.PSWBox.Text <> "") Then
            logoncls.PassWord = FormsAuthentication.HashPasswordForStoringInConfigFile(Me.PSWBox.Text, "md5")
        End If

        Dim drstat As SqlDataReader = Nothing
        Dim drpsw As SqlDataReader = Nothing
        drpsw = logoncls.checkNamePSW()

        If (drpsw.Read()) Then

            If drpsw("MUSR_USRID").ToString.ToUpper = "ADMIN" Then

                Session("username") = drpsw("MUSR_ENAME").ToString().ToUpper
                Session("userID") = drpsw("MUSR_USRID").ToString().ToUpper
                Session("accessgroup") = drpsw("MUSR_GRPID").ToString()
                Session("login_rank") = drpsw("MUSR_RANK").ToString()
                Session("login_svcID") = drpsw("MUSR_SVCID").ToString()
                Session("login_session") = logoncls.GenerateSessionRandomNumber
                ' Added by Ryan Estandarte 2 Oct 2012
                Dim lastPWChange As DateTime = CType(drpsw("MUSR_LSTPWCHNGE"), Date)

                '          convert( int,RAND(DATEPART(mm, GETDATE()) * 100000 + DATEPART(ss, GETDATE()) 
                '* 1000 + DATEPART(ms, GETDATE()))*1000000000) 

                logoncls.UseID = drpsw("MUSR_USRID").ToString()
                logoncls.LogTime = Date.Now()
                logoncls.LVTime = Date.Now()
                logoncls.IPAdress = Request.UserHostAddress.ToString()
                'logoncls.PCName = System.Net.Dns.GetHostByAddress(Request.UserHostName()).HostName
                logoncls.PCName = Request.UserHostName()

                logoncls.UpdateSSYA_Add()

                ' Added by Ryan Estandarte 2 October 2012
                ' Response.Redirect("~/PresentationLayer/Default.aspx")

                ' Modified by Ryan Estandarte 28 November 2012
                ' CheckIfPasswordHasExpired(lastPWChange)
                If Session("accessgroup").ToString().ToUpper().Contains("ADMIN") Then
                    Response.Redirect("~/PresentationLayer/Default.aspx")
                End If
                CheckIfPasswordHasExpired(lastPWChange)
            End If
        End If

        '////////////////////////////////////////////////////////////////////////////


        drpsw = logoncls.checkNamePSW()
        drstat = logoncls.checkNameStat()
        If (drstat.Read()) Then
             
            If (drpsw.Read()) Then

                If ((drpsw("MUSR_STAT").ToString() = "ACTIVE") And (drpsw("MUSR_CHGPW").ToString().ToUpper() = "N")) Then

                    Session("username") = drpsw("MUSR_ENAME").ToString().ToUpper
                    Session("userID") = drpsw("MUSR_USRID").ToString().ToUpper
                    Session("accessgroup") = drpsw("MUSR_GRPID").ToString()

                    Session("login_rank") = drpsw("MUSR_RANK").ToString()
                    Session("login_ctryID") = drpsw("MUSR_CTRCD").ToString()
                    Session("login_cmpID") = drpsw("MUSR_COMCD").ToString
                    Session("login_svcID") = drpsw("MUSR_SVCID").ToString()
                    Session("login_session") = logoncls.GenerateSessionRandomNumber
                    ' Added by Ryan Estandarte 2 Oct 2012
                    Dim lastPWChange As DateTime = CType(drpsw("MUSR_LSTPWCHNGE"), Date)

                    Session("login_datetime") = Date.Now()
                    'modify failed logon value 
                    logoncls.UseID = drpsw("MUSR_USRID").ToString()
                    logoncls.UseFLT = 0
                    logoncls.ChangePSW = "N"
                    ' Added by Ryan Estandarte 03 Oct 2012
                    logoncls.PwStatus = "FLAG"

                    logoncls.Update()
                    'update ssya table
                    logoncls.LogTime = Date.Now()
                    logoncls.LVTime = Date.Now()

                    logoncls.IPAdress = Request.UserHostAddress.ToString()

                    'logoncls.PCName = System.Net.Dns.GetHostByAddress(Request.UserHostName()).HostName
                    logoncls.PCName = Request.UserHostName()

                    logoncls.UpdateSSYA_Add()

                    ' Modified by Ryan Estandarte 2 October 2012
                    ' Response.Redirect("~/PresentationLayer/Default.aspx")

                    ' Modified by Ryan Estandarte 28 November 2012
                    ' CheckIfPasswordHasExpired(lastPWChange)
                    If Session("accessgroup").ToString().ToUpper().Contains("ADMIN") Then
                        Response.Redirect("~/PresentationLayer/Default.aspx")
                    End If
                    CheckIfPasswordHasExpired(lastPWChange)

                ElseIf (drpsw("MUSR_CHGPW").ToString().ToUpper() = "Y") Then
                    'CHANGE PASSWORD IS Y CHANGE PASSWORD FIRST
                    Me.addinfo.Text = statXmlTr.GetLabelName("StatusMessage", "LOGPSWCHG")
                    Me.addinfo.Visible = True

                    'STAFF STATUS IS FAILED
                ElseIf ((drpsw("MUSR_STAT").ToString().ToUpper() = "FAILED") Or (drpsw("MUSR_FLCNT").ToString().ToUpper() = "3")) Then
                    Me.addinfo.Text = statXmlTr.GetLabelName("StatusMessage", "LOGUSELOCKED")
                    Me.addinfo.Visible = True
                End If
                'password is wrong 
            Else
                Try
                    If (Session("nameid") = Nothing) Then
                        Response.Redirect("~/PresentationLayer/logon.aspx")
                    End If
                Catch ex As Exception
                    Response.Redirect("~/PresentationLayer/logon.aspx")
                End Try

                Dim nameidstr As String = Session("nameid")
                If (nameidstr.ToString.ToUpper = Me.UserNameBox.Text.ToUpper) Then
                    changeornot = False
                Else
                    Session("nameid") = Me.UserNameBox.Text
                    changeornot = True
                End If
                'userid change or not
                If (changeornot) Then
                    Session("login_count") = 3
                End If
                Session("login_count") = Session("login_count") - 1
                'login fail time is 3 times
                If (Convert.ToInt32(Session("login_count").ToString()) <= 0) Then
                    'modify failed logon value
                    Me.UserNameBox.Enabled = False
                    Me.PSWBox.Enabled = False
                    logoncls.UseID = Me.UserNameBox.Text.ToString()
                    logoncls.UseFLT = 3
                    logoncls.StafStat = "FAILED"
                    logoncls.UpdateStat()
                    'tile info      
                    Me.addinfo.Text = statXmlTr.GetLabelName("StatusMessage", "LOGUSELOCKED")
                    Me.addinfo.Visible = True
                Else
                    'modify failed logon value
                    logoncls.UseID = Me.UserNameBox.Text.ToString()
                    logoncls.UseFLT = 3 - Session("login_count")
                    logoncls.updateflog()
                    Dim msg As New MessageBox(Me)
                    Dim str As String
                    Dim str1 As String = statXmlTr.GetLabelName("StatusMessage", "LOGTIME1")
                    Dim str2 As String = statXmlTr.GetLabelName("StatusMessage", "LOGTIME2")
                    str = str1 & "  " & Session("login_count").ToString & "  " & str2
                    Me.addinfo.Text = str
                    Me.addinfo.Visible = True
                End If
            End If
        Else
            Me.addinfo.Text = statXmlTr.GetLabelName("StatusMessage", "LOGUSRSTAT")
            Me.addinfo.Visible = True
            Me.UserNameBox.Text = ""
            Me.PSWBox.Text = ""
        End If



    End Sub

    ''' <summary>
    ''' Checks if the user needs to change the password
    ''' </summary>
    ''' <param name="lastPwChange"></param>
    ''' <remarks>Added by Ryan Estandarte 3 October 2012</remarks>
    Private Sub CheckIfPasswordHasExpired(ByVal lastPwChange As DateTime)
        Dim changeAlert As Double = CType(ConfigurationManager.AppSettings("PasswordChangeAlert"), Double)
        Dim changeInfo As Double = CType(ConfigurationManager.AppSettings("PasswordChangeInfo"), Double)
        Dim dayDiff As Double = (DateTime.Today - lastPwChange).TotalDays
        Dim objXm As New clsXml

        Dim cs As ClientScriptManager = Page.ClientScript
        Dim jScript As New StringBuilder
        ' objXm.GetLabelName("StatusMessage", "LOGPWEXP")
        ' objXm.GetLabelName("StatusMessage", "LOGPWEXP-10DAYS")

        If dayDiff > changeAlert Then
            jScript.Append("<script>alert('" + objXm.GetLabelName("StatusMessage", "LOGPWEXP") + "'); ")
            jScript.Append("window.location.href='PresentationLayer/masterrecord/mdyPSW.aspx?name=" + UserNameBox.Text.ToString().ToUpper + "';</script>")
        ElseIf dayDiff >= changeInfo And dayDiff <= changeAlert Then
            jScript.Append("<script>if(confirm('" + objXm.GetLabelName("StatusMessage", "LOGPWEXP-10DAYS") + "')){")
            jScript.AppendLine("window.location.href='PresentationLayer/masterrecord/mdyPSW.aspx?name=" + UserNameBox.Text.ToString().ToUpper + "';")
            jScript.AppendLine("} else {")
            jScript.AppendLine("window.location.href='PresentationLayer/Default.aspx'; } </script>")
        Else
            jScript.Append("<script>window.location.href='PresentationLayer/Default.aspx';</script>")
        End If

        'cs.RegisterClientScriptBlock(Me.GetType(), "PasswordScript", jScript.ToString(), True)
        Response.Write(jScript.ToString())


    End Sub

    'Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton1.Click
    '    'Response.Redirect("~/PresentationLayer/masterrecord/mdyPSW.aspx?name=" + Me.UserNameBox.Text.ToString().ToUpper)
    '    Response.Redirect("~/PresentationLayer/masterrecord/mdyPSW.aspx")
    'End Sub
End Class
