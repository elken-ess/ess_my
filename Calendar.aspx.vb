
Partial Class Calendar
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Hide the title of the calendar control
        myCalendar.ShowTitle = False

        'Populate month and year dropdown list boxes which
        'replace the original calendar title
        If Not Page.IsPostBack Then

            Call Populate_MonthList()

            Call Populate_YearList()
        End If
    End Sub
    Sub Set_Calendar(ByVal Sender As Object, ByVal E As EventArgs) Handles drpCalMonth.SelectedIndexChanged, drpCalYear.SelectedIndexChanged

        'Whenever month or year selection changes display the calendar for that month/year        
        myCalendar.TodaysDate = CDate(drpCalMonth.SelectedItem.Value & " 1, " & drpCalYear.SelectedItem.Value)

    End Sub


    Sub Populate_MonthList()

        drpCalMonth.Items.Add("January")
        drpCalMonth.Items.Add("February")
        drpCalMonth.Items.Add("March")
        drpCalMonth.Items.Add("April")
        drpCalMonth.Items.Add("May")
        drpCalMonth.Items.Add("June")
        drpCalMonth.Items.Add("July")
        drpCalMonth.Items.Add("August")
        drpCalMonth.Items.Add("September")
        drpCalMonth.Items.Add("October")
        drpCalMonth.Items.Add("November")
        drpCalMonth.Items.Add("December")
        Dim datestyle As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-CA")
        System.Threading.Thread.CurrentThread.CurrentCulture = datestyle
        Dim str As String = MonthName(DateTime.Now.Month)

        drpCalMonth.Items.FindByValue(MonthName(DateTime.Now.Month)).Selected = True

    End Sub


    Sub Populate_YearList()

        'Year list can be extended
        Dim intYear As Integer

        For intYear = DateTime.Now.Year - 20 To DateTime.Now.Year + 20

            drpCalYear.Items.Add(intYear)
        Next

        drpCalYear.Items.FindByValue(DateTime.Now.Year).Selected = True

    End Sub

End Class
