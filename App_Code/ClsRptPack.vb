Imports System.Configuration
Imports CrystalDecisions.Shared
Imports SQLDataAccess
Imports BusinessEntity
Imports System.Data.SqlClient
Imports System.Web
Imports CrystalDecisions.Web
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data



Public Class ClsRptPack
#Region "Declaration"

    Private fstrReportName As String
    Private fstrReportUrl As String

    Private fcrvViewer As New CrystalReportViewer
    Private fcrvReportSource As New CrystalReportSource
    Private fstrSpName As String
    Private fstrModifiedBy As String

    Private fstrstatebegin As String
    Private fstrsercidbegin As String
    Private fddatebegin As System.DateTime
    Private fstrtecnicianidbegin As String
    
   
    Private fstrstateend As String
    Private fstrsercidend As String
    Private fddateend As System.DateTime
    Private fstrtecnicianidend As String

    Private fstrlogctrid As String
    Private fstrlogcompanyid As String
    Private fstrlogserviceid As String
    Private fstrlogrank As String

    Private fstrPartCodeFrom As String
    Private fstrPartCodeTo As String

#End Region

#Region "Properties"

    Public Property PartCodeFrom() As String
        Get
            Return fstrPartCodeFrom
        End Get
        Set(ByVal Value As String)
            fstrPartCodeFrom = Value
        End Set
    End Property

    Public Property PartCodeTo() As String
        Get
            Return fstrPartCodeTo
        End Get
        Set(ByVal Value As String)
            fstrPartCodeTo = Value
        End Set
    End Property


    Public Property ReportFileName() As String
        Get
            Return fstrReportName
        End Get
        Set(ByVal Value As String)
            fstrReportName = Value
        End Set
    End Property
    Public Property statemin() As String
        Get
            Return fstrsercidbegin
        End Get
        Set(ByVal Value As String)
            fstrsercidbegin = Value
        End Set
    End Property
    Public Property datemin() As String
        Get
            Return fddatebegin
        End Get
        Set(ByVal Value As String)
            fddatebegin = Value
        End Set
    End Property
    Public Property tecnicianidmin() As String
        Get
            Return fstrtecnicianidbegin
        End Get
        Set(ByVal Value As String)
            fstrtecnicianidbegin = Value
        End Set
    End Property
    Public Property sercidmin() As String
        Get
            Return fstrsercidbegin
        End Get
        Set(ByVal Value As String)
            fstrsercidbegin = Value
        End Set
    End Property
    Public Property stateend() As String
        Get
            Return fstrstateend
        End Get
        Set(ByVal Value As String)
            fstrstateend = Value
        End Set
    End Property
    Public Property sercidend() As String
        Get
            Return fstrsercidend
        End Get
        Set(ByVal Value As String)
            fstrsercidend = Value
        End Set
    End Property
    Public Property dateend() As String
        Get
            Return fddateend
        End Get
        Set(ByVal Value As String)
            fddateend = Value
        End Set
    End Property
    Public Property tecnicianidend() As String
        Get
            Return fstrtecnicianidend
        End Get
        Set(ByVal Value As String)
            fstrtecnicianidend = Value
        End Set
    End Property
    Public Property ModifiedBy() As String
        Get
            Return fstrModifiedBy
        End Get
        Set(ByVal Value As String)
            fstrModifiedBy = Value
        End Set
    End Property
    Public Property logctrid() As String
        Get
            Return fstrlogctrid
        End Get
        Set(ByVal Value As String)
            fstrlogctrid = Value
        End Set
    End Property
    Public Property logcompanyid() As String
        Get
            Return fstrlogcompanyid
        End Get
        Set(ByVal Value As String)
            fstrlogcompanyid = Value
        End Set
    End Property
    Public Property logserviceid() As String
        Get
            Return fstrlogserviceid
        End Get
        Set(ByVal Value As String)
            fstrlogserviceid = Value
        End Set
    End Property
    Public Property logrank() As String
        Get
            Return fstrlogrank
        End Get
        Set(ByVal Value As String)
            fstrlogrank = Value
        End Set
    End Property
#End Region

#Region "Methods"
#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)

        connection.Open()

        Return connection
    End Function
#End Region
#Region "search Pack list printing"

    Public Function GetPack() As DataSet

        Dim selConnection As SqlConnection = Nothing
        Dim selConn As SqlConnection = Nothing
        selConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_RPTPACK_VIEW"
        Try
            trans = selConnection.BeginTransaction()

            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(14) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrstatebegin)
            Param(1).Value = Trim(fstrstateend)
            Param(2).Value = Trim(fstrsercidbegin)
            Param(3).Value = Trim(fstrsercidend)
            Param(4).Value = Trim(fddatebegin)
            Param(5).Value = Trim(fddateend)
            Param(6).Value = Trim(fstrtecnicianidbegin)
            Param(7).Value = Trim(fstrtecnicianidend)

            Param(8).Value = Trim(fstrlogctrid)
            Param(9).Value = Trim(fstrlogcompanyid)
            Param(10).Value = Trim(fstrlogserviceid)
            Param(11).Value = Trim(fstrlogrank)

            Param(12).Value = fstrPartCodeFrom
            Param(13).Value = fstrPartCodeTo

            Dim ds As DataSet = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)

            Return ds

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "ClsRptPack.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "ClsRptPack.vb")
        Finally
            selConnection.Close()
            selConn.Close()
        End Try

    End Function
#End Region
#End Region
End Class
