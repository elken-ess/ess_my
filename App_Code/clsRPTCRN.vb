Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web
Imports BusinessEntity
Imports SQLDataAccess

Public Class clsRPTCRN
#Region "Declaration"
    Private fstrCustmFromID As String
    Private fstrCustmToID As String
    Private fstrSvcFromID As String
    Private fstrSvcToID As String
    Private fstrModelFromID As String
    Private fstrModelToID As String
    Private fstrSerialNoFrom As String
    Private fstrSerialNoTo As String
    Private fstrStateFrom As String
    Private fstrStateTo As String
    Private fdtStartDate As System.DateTime
    Private fdtEndDate As System.DateTime
    Private fstrUserName As String
    Private fstrrank As String ' store procedure name
    Private fstrctrid As String
    Private fstrcomid As String
    Private fstrsvcid As String


    Private fstrReportName As String
    Private fstrReportPath As String
    Private fstrReportID As String
    Private fstrReportUrl As String  'fstrReportPath & fstrReprotName = fstrReportUrl
    Private fcrvViewer As New CrystalReportViewer   'Report viewer
    Private fcrvReportSource As New CrystalReportSource  'Report Source
    Private fstrSpName As String ' store procedure name
#End Region
#Region "Properties"
    Public Property CustmIDFrom() As String
        Get
            Return fstrCustmFromID
        End Get
        Set(ByVal value As String)
            fstrCustmFromID = value

        End Set
    End Property
    Public Property CustmIDTo() As String
        Get
            Return fstrCustmToID
        End Get
        Set(ByVal value As String)
            fstrCustmToID = value

        End Set
    End Property
    Public Property SvcIDFrom() As String
        Get
            Return fstrSvcFromID
        End Get
        Set(ByVal value As String)
            fstrSvcFromID = value
        End Set
    End Property
    Public Property SvcIDTo() As String
        Get
            Return fstrSvcToID
        End Get
        Set(ByVal value As String)
            fstrSvcToID = value
        End Set
    End Property
    Public Property ModelIDFrom() As String
        Get
            Return fstrModelFromID

        End Get
        Set(ByVal value As String)
            fstrModelFromID = value

        End Set
    End Property
    Public Property ModelIDTo() As String
        Get
            Return fstrModelToID

        End Get
        Set(ByVal value As String)
            fstrModelToID = value

        End Set
    End Property
    Public Property SerialNoFrom() As String
        Get
            Return fstrSerialNoFrom
        End Get
        Set(ByVal value As String)
            fstrSerialNoFrom = value
        End Set
    End Property
    Public Property SerialNoTo() As String
        Get
            Return fstrSerialNoTo
        End Get
        Set(ByVal value As String)
            fstrSerialNoTo = value
        End Set
    End Property
    Public Property StateFrom() As String
        Get
            Return fstrStateFrom
        End Get
        Set(ByVal value As String)
            fstrStateFrom = value
        End Set
    End Property
    Public Property StateTo() As String
        Get
            Return fstrStateTo
        End Get
        Set(ByVal value As String)
            fstrStateTo = value
        End Set
    End Property

    Public Property StartDate() As String
        Get
            Return fdtStartDate
        End Get
        Set(ByVal value As String)
            fdtStartDate = value
        End Set
    End Property
    Public Property EndDate() As String
        Get
            Return fdtEndDate
        End Get
        Set(ByVal value As String)
            fdtEndDate = value
        End Set
    End Property
    Public Property UserName()
        Get
            Return fstrUserName
        End Get
        Set(ByVal value)
            fstrUserName = value
        End Set
    End Property
    'set fstrReportName
    Public Property ReportFileName() As String
        Get
            Return fstrReportName
        End Get
        Set(ByVal Value As String)
            fstrReportName = Value
        End Set
    End Property
    Public Property rank() As String
        Get
            Return fstrrank
        End Get
        Set(ByVal Value As String)
            fstrrank = Value
        End Set
    End Property
    Public Property ctrid() As String
        Get
            Return fstrctrid
        End Get
        Set(ByVal Value As String)
            fstrctrid = Value
        End Set
    End Property
    Public Property comid() As String
        Get
            Return fstrcomid
        End Get
        Set(ByVal Value As String)
            fstrcomid = Value
        End Set
    End Property
    Public Property svcid() As String
        Get
            Return fstrsvcid
        End Get
        Set(ByVal Value As String)
            fstrsvcid = Value
        End Set
    End Property
    'set fstrReportPath
    Public Property ReportPath() As String
        Get
            Return fstrReportPath
        End Get
        Set(ByVal Value As String)
            fstrReportPath = Value
        End Set
    End Property
    'set ReportID
    Public Property ReportID() As String
        Get
            Return fstrReportID
        End Get
        Set(ByVal Value As String)
            fstrReportID = Value
        End Set
    End Property
#End Region
#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)

        connection.Open()

        Return connection
    End Function
#End Region


    Private Function CheckReportExist() As Boolean
        Dim lblnfileExists As Boolean = False
        Try
            If IO.File.Exists(fstrReportUrl) Then
                Dim lcrReport As New ReportDocument()
                lcrReport.Load(fstrReportUrl)
                lcrReport.Close()
                lblnfileExists = True
            End If
        Catch e As Exception
            lblnfileExists = False
        End Try

        Return lblnfileExists

    End Function



#Region "Contract Reminde Notice date soure"
    Public Function GetContractRemindeNotice() As DataSet
        Dim selConnection As SqlConnection = Nothing
        Dim selConn As SqlConnection = Nothing
        selConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_RPTCRMN_VIEW"
        Try
            trans = selConnection.BeginTransaction()

            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(17) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrCustmFromID)
            Param(1).Value = Trim(fstrCustmToID)
            Param(2).Value = Trim(fstrModelFromID)
            Param(3).Value = Trim(fstrModelToID)
            Param(4).Value = Trim(fstrSerialNoFrom)
            Param(5).Value = Trim(fstrSerialNoTo)
            Param(6).Value = Trim(fstrStateFrom)
            Param(7).Value = Trim(fstrStateTo)
            Param(8).Value = Trim(fdtStartDate)
            Param(9).Value = Trim(fdtEndDate)
            Param(10).Value = Trim(fstrSvcFromID)
            Param(11).Value = Trim(fstrSvcToID)
            Param(12).Value = Trim(fstrUserName)
            Param(13).Value = Trim(fstrctrid)
            Param(14).Value = Trim(fstrcomid)
            Param(15).Value = Trim(fstrsvcid)
            Param(16).Value = Trim(fstrrank)
            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            ' switch status id to status name for display
            'Dim count As Integer
            'Dim statXmlTr As New clsXml
            'Dim strPanm As String
            'statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            'For count = 0 To ds.Tables(0).Rows.Count - 1
            '    Dim statusid As String = ds.Tables(0).Rows(count).Item(3).ToString 'item(3) is status
            '    strPanm = statXmlTr.GetLabelName("StatusMessage", statusid)
            '    ds.Tables(0).Rows(count).Item(3) = strPanm
            'Next
            Return ds

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrUserName, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsRPTCRN.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrUserName, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsRPTCRN.vb")
        Finally
            selConnection.Close()
            selConn.Close()
        End Try
    End Function
#End Region

End Class



























































































































































































































































































































































































































































































































































































































































































































































































































































































































































































