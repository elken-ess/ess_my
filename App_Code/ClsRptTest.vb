Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web
Imports BusinessEntity


Public Class ClsRptTest
#Region "Declaration"

    Private fstrReportName As String
    Private fstrReportPath As String

    Private fstrReportID As String
    Private fstrSpName As String ' store procedure name
    Private fstrReportUrl As String


    Private fcrvViewer As New CrystalReportViewer
    Private fcrvReportSource As New CrystalReportSource

#End Region

#Region "Properties"

    Public Property ReportFileName() As String
        Get
            Return fstrReportName
        End Get
        Set(ByVal Value As String)
            fstrReportName = Value
        End Set
    End Property

    Public Property ReportPath() As String
        Get
            Return fstrReportPath
        End Get
        Set(ByVal Value As String)
            fstrReportPath = Value
        End Set
    End Property

    Public Property ReportID() As String
        Get
            Return fstrReportID
        End Get
        Set(ByVal Value As String)
            fstrReportID = Value
        End Set
    End Property

#End Region

#Region "Methods"
    'SetReport(CrystalReportViewer1, CrystalReportSource1)
    Public Sub SetReport(ByRef inpViewer As CrystalReportViewer)
        fcrvViewer = inpViewer
        'fcrvReportSource = inpReportSource 'fcrvReportSource
        Dim objRptProperties As New ClsCommonReport
        'fstrReportPath = objRptProperties.ReportPath   '得到报表的路径
        Call objRptProperties.SetViewProperties(fcrvViewer) '设置fcrvViewer的属性
        fstrReportUrl = fstrReportPath & fstrReportName '得到报表的完整URL
        ' fcrvViewer.ReportSourceID = fcrvReportSource.ID  '将reportSource和reportViewer关联
        'fcrvReportSource.Report.FileName = fstrReportUrl '将.rpt和reportSource关联。
        '最终将.rpt和reportViewer关联，所以reportViewer很关键，它的数据就是要显示在报表上的数据。
        If CheckReportExist() Then
            Call SetReportParameter()
        Else
            ' write log file for file not exist
        End If

    End Sub

    Private Function CheckReportExist() As Boolean
        Dim lblnfileExists As Boolean = False
        Try
            If IO.File.Exists(fstrReportUrl) Then
                Dim lcrReport As New ReportDocument()
                lcrReport.Load(fstrReportUrl)
                lcrReport.Close()
                lblnfileExists = True
            End If
        Catch e As Exception
            lblnfileExists = False
        End Try

        Return lblnfileExists

    End Function
#End Region

#Region "Report Parameter and properties"
    Private Sub SetReportParameter()
        Try

            'Dim dbConn As TableLogOnInfo = New TableLogOnInfo()
            Dim lcrReport As New ReportDocument()
            lcrReport.Load(fstrReportUrl) '将物理路径下存在的crystalreport1.rpt和reportDocument关联。          
            Dim countryEntity As New clsCountry()
            Dim ctryall As DataSet = countryEntity.GetCountry()
            Dim count As Integer = ctryall.Tables(0).Rows.Count
            Dim tbcnt As Integer = lcrReport.Database.Tables.Count
            lcrReport.Database.Tables(0).SetDataSource(ctryall.Tables(0))

            Dim c As Integer = lcrReport.Database.Tables(0).Fields.Count
            Dim s As String = lcrReport.Database.Tables(0).Fields(0).ToString()
            Dim objXmlTr As New clsXml

            objXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
            lcrReport.SetParameterValue("CountryID", objXmlTr.GetLabelName("EngLabelMsg", "BROCTRYTL"))
            'Dim str As String = objXmlTr.GetLabelName("EngLabelMsg", "CTRYHD1")
            lcrReport.SetParameterValue("EName", objXmlTr.GetLabelName("EngLabelMsg", "ADDCTRYTL"))
            lcrReport.SetParameterValue("AName", objXmlTr.GetLabelName("EngLabelMsg", "ADDCTRYTL"))
            lcrReport.SetParameterValue("AName", objXmlTr.GetLabelName("EngLabelMsg", "ADDCTRYTL"))

            lcrReport.SetDataSource(ctryall.Tables(0))
            fcrvViewer.ReportSource = lcrReport
        Catch ex As Exception
        End Try
    End Sub


#End Region
    Private Sub SetRptDetailPara()

    End Sub

End Class
