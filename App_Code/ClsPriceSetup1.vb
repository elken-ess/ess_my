Imports SQLDataAccess
Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Web
Imports System.Data

Public Class ClsPriceSetup1
    Private strConn As String

#Region "Declaration"
    Private fstrPriceSetUpID As Integer   ' PriceSetUpID 
    Private fstrPriceID As String ' PriceID
    Private fstrPartID As String ' PartID
    Private fstrCountryID As String ' country currency
    Private fdoubleAmount As Decimal    'EffectiveDate 
    Private fdoubleAmount1 As Decimal    'EffectiveDate 
    Private fdoubleAmount2 As Decimal    'EffectiveDate 
    Private fdoubleAmount3 As Decimal    'EffectiveDate 
    Private fdateEffectiveDate As System.DateTime 'EffectiveDate 
    Private fstrCreatedBy As String ' CreatedBy
    Private fdateCreatedDate As System.DateTime ' CreatedDate 
    Private fstrModifiedBy As String 'ModifiedBy
    Private fdateModifiedDate As System.DateTime 'ModifiedDate
    Private fstrPriceSetUpStatus As String 'PriceSetUp status
    Private fstrSpName As String ' store  procedure name
    Private fdcrdate As System.DateTime  ' store  procedure name
    Private fstrrank As String ' store procedure name
    Private fstrsvcid As String
    Private fstrip As String
#End Region

#Region "Properties"
    Public Property crdate() As System.DateTime
        Get
            Return fdcrdate
        End Get
        Set(ByVal Value As System.DateTime)
            fdcrdate = Value
        End Set
    End Property
    Public Property PriceSetUpID() As Integer
        Get
            Return fstrPriceSetUpID
        End Get
        Set(ByVal Value As Integer)
            fstrPriceSetUpID = Value
        End Set
    End Property
    Public Property Amount() As Decimal
        Get
            Return fdoubleAmount
        End Get
        Set(ByVal Value As Decimal)
            fdoubleAmount = Value
        End Set
    End Property
    Public Property AmountOri() As Decimal
        Get
            Return fdoubleAmount1
        End Get
        Set(ByVal Value As Decimal)
            fdoubleAmount1 = Value
        End Set
    End Property
    Public Property AmountGst() As Decimal
        Get
            Return fdoubleAmount2
        End Get
        Set(ByVal Value As Decimal)
            fdoubleAmount2 = Value
        End Set
    End Property
    Public Property AmountExc() As Decimal
        Get
            Return fdoubleAmount3
        End Get
        Set(ByVal Value As Decimal)
            fdoubleAmount3 = Value
        End Set
    End Property

    Public Property PriceID() As String
        Get
            Return fstrPriceID
        End Get
        Set(ByVal Value As String)
            fstrPriceID = Value
        End Set
    End Property

    Public Property PartID() As String
        Get
            Return fstrPartID
        End Get
        Set(ByVal Value As String)
            fstrPartID = Value
        End Set
    End Property

    Public Property PriceSetUpStatus() As String
        Get
            Return fstrPriceSetUpStatus
        End Get
        Set(ByVal Value As String)
            fstrPriceSetUpStatus = Value
        End Set
    End Property
    Public Property CountryID() As String
        Get
            Return fstrCountryID
        End Get
        Set(ByVal Value As String)
            fstrCountryID = Value
        End Set
    End Property


    Public Property EffectiveDate() As String
        Get
            Return fdateEffectiveDate
        End Get
        Set(ByVal Value As String)
            fdateEffectiveDate = Value
        End Set
    End Property



    Public Property CreatedBy() As String
        Get
            Return fstrCreatedBy
        End Get
        Set(ByVal Value As String)
            fstrCreatedBy = Value
        End Set
    End Property
    Public Property CreatedDate() As String
        Get
            Return fdateCreatedDate
        End Get
        Set(ByVal Value As String)
            fdateCreatedDate = Value
        End Set
    End Property
    Public Property ModifiedBy() As String
        Get
            Return fstrModifiedBy
        End Get
        Set(ByVal Value As String)
            fstrModifiedBy = Value
        End Set
    End Property
    Public Property ModifiedDate() As String
        Get
            Return fdateModifiedDate
        End Get
        Set(ByVal Value As String)
            fdateModifiedDate = Value
        End Set
    End Property

    Public Property rank() As String
        Get
            Return fstrrank
        End Get
        Set(ByVal Value As String)
            fstrrank = Value
        End Set
    End Property
    Public Property svcid() As String
        Get
            Return fstrsvcid
        End Get
        Set(ByVal Value As String)
            fstrsvcid = Value
        End Set
    End Property
    Public Property ip() As String
        Get
            Return fstrip
        End Get
        Set(ByVal Value As String)
            fstrip = Value
        End Set
    End Property


#End Region

#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)

        connection.Open()

        Return connection
    End Function
#End Region

#Region "Select PriceSetUp By ID1"

    Public Function GetPriceSetUpIDDetailsByID1() As ArrayList
        Dim retnArray As ArrayList = New ArrayList()
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASPRCE_SELBYID1"
        Try
            trans = sidConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstrPriceSetUpID)
            Param(1).Value = Trim(fdcrdate)

            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            'Return dr
            If dr.Read() Then
                Dim count As Integer
                For count = 0 To dr.FieldCount - 1
                    retnArray.Add(dr.GetValue(count).ToString())
                Next
            End If
            dr.Close()
            Return retnArray

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsPriceSetUp.vb")


        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsPriceSetUp.vb")

        End Try

    End Function
#End Region

#Region "Insert1"

    Public Function Insert1() As Integer

        Dim insConnection As SqlConnection = Nothing
        Dim insConn As SqlConnection = Nothing
        insConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        insConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASPRCE_Add1"
        Try
            trans = insConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(9) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(insConn, fstrSpName)
            Param(0).Value = Trim(fstrPriceSetUpID)
            Param(1).Value = Trim(fdoubleAmount)
            Param(2).Value = Trim(fstrPriceSetUpStatus)
            Param(3).Value = Trim(fdateEffectiveDate)
            Param(4).Value = Trim(fstrCreatedBy)
            Param(5).Value = Trim(fstrModifiedBy)
            Param(6).Value = Trim(fdoubleAmount1)
            Param(7).Value = Trim(fdoubleAmount2)
            Param(8).Value = Trim(fdoubleAmount3)



            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(insConn, CommandType.StoredProcedure, fstrSpName, Param)


            trans.Commit()
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsPriceSetUp.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsPriceSetUp.vb")
            Return -1
        End Try

    End Function

#End Region
#Region "Update1"

    Public Function Update1() As Integer

        Dim UpdConnection As SqlConnection = Nothing
        Dim UpdConn As SqlConnection = Nothing
        UpdConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        UpdConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASPRCE_Upd1"
        Try
            trans = UpdConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(11) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(UpdConn, fstrSpName)
            Param(0).Value = Trim(fstrPriceSetUpID)
            Param(1).Value = Trim(fdoubleAmount)
            Param(2).Value = Trim(fstrPriceSetUpStatus)
            Param(3).Value = Trim(fdateEffectiveDate)
            Param(4).Value = Trim(fstrModifiedBy)
            Param(5).Value = Trim(fdcrdate)
            Param(6).Value = Trim(fstrsvcid)
            Param(7).Value = Trim(fstrip)
            Param(8).Value = Trim(fdoubleAmount1)
            Param(9).Value = Trim(fdoubleAmount2)
            Param(10).Value = Trim(fdoubleAmount3)



            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(UpdConn, CommandType.StoredProcedure, fstrSpName, Param)

            trans.Commit()
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsPriceSetUp.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsPriceSetUp.vb")
            Return -1
        End Try

    End Function

#End Region
#Region "confirm no duplicated AMOUNT"
    Public Function GetDuplicatedAmount() As Integer
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASPRCE_BYAMOUNT"
        Try
            trans = sidConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstrPriceSetUpID)
            Param(1).Value = Trim(fdateEffectiveDate)
            'Param(2).Value = Trim(fdoubleAmount)

            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            If dr.Read() Then
                dr.Close()
                Return -1
            End If
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsPriceSetUp.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsPriceSetUp.vb")
            Return -1
        End Try

    End Function
#End Region
End Class
