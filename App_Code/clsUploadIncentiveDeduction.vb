Imports System.Data.SqlClient
Imports SQLDataAccess
Imports System.Configuration
Imports BusinessEntity
Imports System.data
Imports System.Collections.Generic

Public Class clsUploadIncentiveDeduction

    Private strFileName As String
    Private strUserId As String
    Private strTchId As String
    Private strTchName As String
    Private strLateSub As String
    Private strLate As String
    Private strOthers As String
    Private strSales As String
    Private bInsertUpdate As String
    Public IncentiveList As New List(Of clsUploadIncentiveDeduction)

#Region "Properties"

    Public Property FileName() As String
        Get
            Return strFileName
        End Get
        Set(ByVal value As String)
            strFileName = value
        End Set
    End Property

    Public Property UserID() As String
        Get
            Return strUserId
        End Get
        Set(ByVal value As String)
            strUserId = value
        End Set
    End Property

    Public Property TchID() As String
        Get
            Return strTchId
        End Get
        Set(ByVal value As String)
            strTchId = value
        End Set
    End Property

    Public Property TchName() As String
        Get
            Return strTchName
        End Get
        Set(ByVal value As String)
            strTchName = value
        End Set
    End Property

    Public Property LateSub() As String
        Get
            Return strLateSub
        End Get
        Set(ByVal value As String)
            strLateSub = value
        End Set
    End Property

    Public Property Late() As String
        Get
            Return strLate
        End Get
        Set(ByVal value As String)
            strLate = value
        End Set
    End Property

    Public Property Others() As String
        Get
            Return strOthers
        End Get
        Set(ByVal value As String)
            strOthers = value
        End Set
    End Property

    Public Property Sales() As String
        Get
            Return strSales
        End Get
        Set(ByVal value As String)
            strSales = value
        End Set
    End Property

    Public Property InsertUpdate() As String
        Get
            Return bInsertUpdate
        End Get
        Set(ByVal value As String)
            bInsertUpdate = value
        End Set
    End Property

    Public Sub New()

    End Sub

    Public Sub New(ByVal strFileName As String, ByVal strUserID As String, ByVal strTchId As String, _
                   ByVal strTchName As String, ByVal strLateSub As String, ByVal strLate As String, _
                   ByVal strOthers As String, ByVal strSales As String, ByVal bInsertUpdate As String)

        Me.FileName = strFileName
        Me.UserID = strUserID
        Me.TchID = strTchId
        Me.TchName = strTchName
        Me.LateSub = strLateSub
        Me.Late = strLate
        Me.Others = strOthers
        Me.Sales = strSales
        Me.bInsertUpdate = bInsertUpdate
    End Sub
#End Region

#Region "Checking"

    Public Function GetExistFINCUPLOAD_FIL(ByVal FileName As String, ByVal TechnicianID As String, ByVal UserID As String) As DataSet
        Dim checkConnection As SqlConnection = Nothing
        Dim checkConn As SqlConnection = Nothing
        checkConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        checkConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        Dim trans As SqlTransaction = Nothing
        Dim fstrSpName As String = "BB_FINCUPLOAD_DEDUCTION_FIL_Sel"

        Try
            trans = checkConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(3) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(checkConn, fstrSpName)
            Param(0).Value = FileName
            Param(1).Value = TechnicianID
            Param(2).Value = UserID

            Dim ds As DataSet = SqlHelper.ExecuteDataset(checkConn, CommandType.StoredProcedure, fstrSpName, Param)
            GetExistFINCUPLOAD_FIL = ds

            checkConnection.Dispose()
            checkConn.Dispose()
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(UserID, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsUpload.vb")
        Catch ex As SqlException
            trans.Rollback()
            checkConnection.Close()
            checkConnection.Dispose()
            checkConn.Close()
            checkConn.Dispose()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(UserID, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsUpload.vb")
        End Try
    End Function
#End Region

#Region "Insert"
    Public Function Insert(ByVal ExcelList As clsUploadIncentiveDeduction, ByVal ModBy As String) As Integer

        Dim insConnection As SqlConnection = Nothing
        insConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        Dim myCommand As SqlCommand = insConnection.CreateCommand()
        Dim transaction As SqlTransaction

        transaction = insConnection.BeginTransaction(IsolationLevel.ReadCommitted)
        myCommand.Connection = insConnection
        myCommand.Transaction = transaction
        myCommand.CommandText = "BB_FINCUPLOAD_DEDUCTION_ADD"
        myCommand.CommandType = CommandType.StoredProcedure
        Dim iInsert As Integer = 0
        Try

            For i As Integer = 0 To ExcelList.IncentiveList.Count - 1
                myCommand.Parameters.Clear()
                myCommand.Parameters.AddWithValue("@FINCUPLOAD_FILENAME", ExcelList.IncentiveList.Item(i).strFileName)
                myCommand.Parameters.AddWithValue("@FINCUPLOAD_USERID", ExcelList.IncentiveList.Item(i).strUserId)
                myCommand.Parameters.AddWithValue("@FINCUPLOAD_TCHID", ExcelList.IncentiveList.Item(i).TchID)
                myCommand.Parameters.AddWithValue("@FINCUPLOAD_TCHNAME", ExcelList.IncentiveList.Item(i).TchName)
                myCommand.Parameters.AddWithValue("@FINCUPLOAD_DEDUCTION_LATESUB", ExcelList.IncentiveList.Item(i).strLateSub)
                myCommand.Parameters.AddWithValue("@FINCUPLOAD_DEDUCTION_LATE", ExcelList.IncentiveList.Item(i).strLate)
                myCommand.Parameters.AddWithValue("@FINCUPLOAD_DEDUCTION_OTHERS", ExcelList.IncentiveList.Item(i).strOthers)
                myCommand.Parameters.AddWithValue("@FINCUPLOAD_DEMERIT_SALES", ExcelList.IncentiveList.Item(i).strSales)
                iInsert = myCommand.ExecuteNonQuery()
            Next

            transaction.Commit()
        Catch e As Exception
            Try
                transaction.Rollback()
                insConnection.Close()
                insConnection.Dispose()
            Catch ex As SqlException
                insConnection.Close()
                insConnection.Dispose()
                Dim ErrorLog As ArrayList = New ArrayList
                ErrorLog.Add("Error").ToString()
                ErrorLog.Add(ex.Message).ToString()
                Dim WriteErrLog As New clsLogFile()
                WriteErrLog.ErrorLog(ModBy, ErrorLog.Item(1).ToString, "", WriteErrLog.ADD, "clsUpload.vb")
                Return -1
            End Try
            Throw New Exception(e.Message)
        Finally
            insConnection.Close()
            insConnection.Dispose()
        End Try
        Return 0

    End Function
    Public Function Insert_New(ByVal ExcelList As clsUploadIncentiveDemerit, ByVal ModBy As String) As Integer

        Dim insConn As SqlConnection = Nothing
        insConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        Dim fstrSpName As String = "BB_FINCUPLOAD_ADD"
        Dim iInsert As Integer = 0

        Dim conn As SqlConnection
        Dim cmd As SqlCommand
        Dim tran As SqlTransaction
        ' Create a New Connection
        conn = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        ' Open the Connection
        conn.Open()
        tran = conn.BeginTransaction
        ' Set the Transaction within which the Commands execute
        Try
            For i As Integer = 0 To ExcelList.IncentiveList.Count - 1
                Dim Param() As SqlParameter = New SqlParameter(4) {}
                Param = SqlHelperParameterCache.GetSpParameterSet(insConn, fstrSpName)
                Param(0).Value = ExcelList.IncentiveList.Item(i).FileName
                Param(1).Value = ExcelList.IncentiveList.Item(i).UserID
                Param(2).Value = ExcelList.IncentiveList.Item(i).TchID
                'Param(3).Value = ExcelList.IncentiveList.Item(i).AdjJob
                'Param(4).Value = ExcelList.IncentiveList.Item(i).Remark
                'Param(5).Value = ExcelList.IncentiveList.Item(i).Demerit
                'Param(6).Value = ExcelList.IncentiveList.Item(i).DemeritCP
                Param(7).Value = ExcelList.IncentiveList.Item(i).PremiumML
                Param(8).Value = ExcelList.IncentiveList.Item(i).PremiumPL

                iInsert = SqlHelper.ExecuteNonQuery(insConn, CommandType.StoredProcedure, fstrSpName, Param)
            Next

            ' Commit the Transaction
            tran.Commit()
        Catch
            ' Rollback the Transaction
            tran.Rollback()
            conn.Close()
            conn.Dispose()
        Finally
            ' Cleanup Code
            ' Close the Connection
            conn.Close()
        End Try

    End Function


#End Region

#Region "Methods"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)
        connection.Open()
        Return connection
    End Function
#End Region
End Class