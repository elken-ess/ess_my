Imports System.Data.SqlClient
Imports System.Data
Imports BusinessEntity
Imports SQLDataAccess

Public Class ClsMastAppt
#Region "Declarations"
    Private _selectedDate As DateTime
    Private _userName As String
    Private _reportName As String
    Private _fromSvcID As String
    Private _toSvcID As String
#End Region

#Region "Properties"
    Public Property SelectedDate() As Date
        Get
            Return _selectedDate
        End Get
        Set(ByVal value As Date)
            _selectedDate = value
        End Set
    End Property

    Public Property UserName() As String
        Get
            Return _userName
        End Get
        Set(ByVal value As String)
            _userName = value
        End Set
    End Property

    Public Property ReportName() As String
        Get
            Return _reportName
        End Get
        Set(ByVal value As String)
            _reportName = value
        End Set
    End Property

    Public Property FromSvcID() As String
        Get
            Return _fromSvcID
        End Get
        Set(ByVal value As String)
            _fromSvcID = value
        End Set
    End Property

    Public Property ToSvcID() As String
        Get
            Return _toSvcID
        End Get
        Set(ByVal value As String)
            _toSvcID = value
        End Set
    End Property

#End Region

#Region "Methods"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)

        connection.Open()

        Return connection
    End Function
    ''' <summary>
    ''' Retrieves the master appointment report by SVC
    ''' </summary>
    ''' <returns>Dataset of master appointments by SVC</returns>
    ''' <remarks></remarks>
    Public Function RetrieveMasterAppointmentBySVC() As DataSet
        Dim con As New SqlConnection()
        con = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        Dim ds As New DataSet

        Try
            Dim param() As SqlParameter

            param = SqlHelperParameterCache.GetSpParameterSet(con, _reportName)
            param(0).Value = _selectedDate

            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, _reportName, param)

        Catch ex As Exception
            Dim errorLog As New ArrayList
            errorLog.Add("Error").ToString()
            errorLog.Add(ex.Message)

            Dim writeError As New clsLogFile()
            writeError.ErrorLog(_userName, errorLog.Item(1).ToString(), _reportName, writeError.SELE, "clsMastAppt")
        Finally
            con.Close()
            con.Dispose()
        End Try

        Return ds
    End Function

    ''' <summary>
    ''' Retrieves the master appointment report by CR
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function RetrieveMasterAppointmentByCRTest(ByVal [date] As DateTime) As DataSet
        Dim con As New SqlConnection()

        con = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim ds As New DataSet

        Try
            Dim param() As SqlParameter

            param = SqlHelperParameterCache.GetSpParameterSet(con, "BB_RPTRetrieveJobApptPerDay")
            param(0).Value = [date]
            'param(1).Value = String.Empty
            'param(2).Value = String.Empty

            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "BB_RPTRetrieveJobApptPerDay", param)

        Catch ex As Exception
            Dim errorLog As New ArrayList
            errorLog.Add("Error").ToString()
            errorLog.Add(ex.Message)

            Dim writeError As New clsLogFile()
            writeError.ErrorLog(_userName, errorLog.Item(1).ToString(), _reportName, writeError.SELE, "clsMastAppt")
        Finally
            con.Close()
            con.Dispose()
        End Try

        Return ds
    End Function

    ''' <summary>
    ''' Retrieves the master appointment report by CR
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function RetrieveMasterAppointmentByCR() As DataSet
        Dim con As New SqlConnection()

        con = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim ds As New DataSet

        Try
            Dim param() As SqlParameter

            param = SqlHelperParameterCache.GetSpParameterSet(con, _reportName)
            param(0).Value = _selectedDate

            If _fromSvcID = "0" Then
                param(1).Value = String.Empty
            Else
                param(1).Value = _fromSvcID
            End If

            If _toSvcID = "ZZZZZZZZZZZZZZZZZZ" Then
                param(2).Value = String.Empty
            Else
                param(2).Value = _toSvcID
            End If
            
            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, _reportName, param)

        Catch ex As Exception
            Dim errorLog As New ArrayList
            errorLog.Add("Error").ToString()
            errorLog.Add(ex.Message)

            Dim writeError As New clsLogFile()
            writeError.ErrorLog(_userName, errorLog.Item(1).ToString(), _reportName, writeError.SELE, "clsMastAppt")
        Finally
            con.Close()
            con.Dispose()
        End Try

        Return ds
    End Function
#End Region
End Class
