Imports System
Imports System.Data.SqlClient
Imports System.Data

Imports BusinessEntity
Imports SQLDataAccess

Public Class ClsServBillReport
#Region "Declarations"
    Private _fromSvcID As String
    Private _toSvcID As String
    Private _fromTechID As String
    Private _toTechID As String
    Private _fromCustID As String
    Private _toCustID As String
    Private _startInvDate As String
    Private _endInvDate As String
    Private _fromServBillNo As String
    Private _toServBillNo As String
    Private _ctryID As String
    Private _comID As String
    'Private _rank As String
    Private _reportType As String

    Private _userName As String

    Private _errorLog As ArrayList = New ArrayList
    Private _writeErrLog As New clsLogFile()

#End Region

#Region "Properties"
    Public Property FromSvcID() As String
        Get
            Return _fromSvcID
        End Get
        Set(ByVal value As String)
            _fromSvcID = value
        End Set
    End Property

    Public Property ToSvcID() As String
        Get
            Return _toSvcID
        End Get
        Set(ByVal value As String)
            _toSvcID = value
        End Set
    End Property

    Public Property FromTechID() As String
        Get
            Return _fromTechID
        End Get
        Set(ByVal value As String)
            _fromTechID = value
        End Set
    End Property

    Public Property ToTechID() As String
        Get
            Return _toTechID
        End Get
        Set(ByVal value As String)
            _toTechID = value
        End Set
    End Property

    Public Property FromCustID() As String
        Get
            Return _fromCustID
        End Get
        Set(ByVal value As String)
            _fromCustID = value
        End Set
    End Property

    Public Property ToCustID() As String
        Get
            Return _toCustID
        End Get
        Set(ByVal value As String)
            _toCustID = value
        End Set
    End Property

    Public Property StartInvDate() As String
        Get
            Return _startInvDate
        End Get
        Set(ByVal value As String)
            _startInvDate = value
        End Set
    End Property

    Public Property EndInvDate() As String
        Get
            Return _endInvDate
        End Get
        Set(ByVal value As String)
            _endInvDate = value
        End Set
    End Property

    Public Property FromServBillNo() As String
        Get
            Return _fromServBillNo
        End Get
        Set(ByVal value As String)
            _fromServBillNo = value
        End Set
    End Property

    Public Property ToServBillNo() As String
        Get
            Return _toServBillNo
        End Get
        Set(ByVal value As String)
            _toServBillNo = value
        End Set
    End Property

    Public Property CtryID() As String
        Get
            Return _ctryID
        End Get
        Set(ByVal value As String)
            _ctryID = value
        End Set
    End Property

    Public Property ComID() As String
        Get
            Return _comID
        End Get
        Set(ByVal value As String)
            _comID = value
        End Set
    End Property

    Public Property UserName() As String
        Get
            Return _userName
        End Get
        Set(ByVal value As String)
            _userName = value
        End Set
    End Property

    Public Property ReportType() As String
        Get
            Return _reportType
        End Get
        Set(ByVal value As String)
            _reportType = value
        End Set
    End Property


    'Public Property Rank() As String
    '    Get
    '        Return _rank
    '    End Get
    '    Set(ByVal value As String)
    '        _rank = value
    '    End Set
    'End Property
#End Region

#Region "Methods"
    Public Function GetServiceBillReport() As DataSet
        Dim ds As New DataSet
        Dim storedProc As String

        Select Case _reportType
            Case "Details Report"
                storedProc = "BB_RPTSERVBILL_VIEWDETAILS"
            Case "Payment Report"
                storedProc = "BB_RPTSERVBILL_VIEWPAYMENT"
			' Added by Ryan Estandarte 19 Sept 2012
            Case "Credit Report"
                storedProc = "BB_RPTSERVBILL_VIEWCREDIT"
            Case Else
                Throw New ArgumentException("Report Type is not valid")
        End Select
        
        Try
            Dim param() As SqlParameter
            param = SqlHelperParameterCache.GetSpParameterSet(ConfigurationManager.AppSettings("ConnectionString"), storedProc)
            param(0).Value = _fromSvcID
            param(1).Value = _toSvcID
            param(2).Value = _fromTechID
            param(3).Value = _toTechID
            param(4).Value = _fromCustID
            param(5).Value = _toCustID
            param(6).Value = _startInvDate
            param(7).Value = _endInvDate
            param(8).Value = _fromServBillNo
            param(9).Value = _toServBillNo
            param(10).Value = _ctryID
            param(11).Value = _comID

            ds = SqlHelper.ExecuteDataset(ConfigurationManager.AppSettings("ConnectionString"), CommandType.StoredProcedure, storedProc, param)
        Catch ex As Exception
            _errorLog.Add("Error").ToString()
            _errorLog.Add(ex.Message).ToString()
            _writeErrLog.ErrorLog(UserName, _errorLog.Item(1).ToString, storedProc, _writeErrLog.SELE, "ClsServBillReport.vb")
            Return New DataSet()
        End Try

        Return ds

    End Function

    Public Sub ExportToExcel(ByVal dt As DataTable, ByVal response As HttpResponse)
        Dim attachment As String = "attachment; filename=" & ConfigurationManager.AppSettings("ReportFilename") & _reportType & "_" & DateTime.Now.ToString("MMyyyy") & ".xls"
        Const style As String = "<style> td { mso-number-format:\@; } </style> "

        response.Clear()
        response.Charset = ""
        response.AddHeader("content-disposition", attachment)
        response.ContentType = "application/vnd.ms-excel"
        Dim stringWrite As IO.StringWriter = New IO.StringWriter()
        Dim htmlWrite As HtmlTextWriter = New HtmlTextWriter(stringWrite)
        Dim dg As DataGrid = New DataGrid()
        dg.DataSource = dt
        dg.DataBind()
        dg.RenderControl(htmlWrite)
        response.Write(style) 'style is added dynamically
        response.Write(stringWrite.ToString())
        response.End()
    End Sub
#End Region
End Class
