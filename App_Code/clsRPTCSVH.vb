﻿Imports Microsoft.VisualBasic

Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web
Imports BusinessEntity
Imports SQLDataAccess

Public Class clsRPTCSVH
#Region "Declaration"

    Private fstrminSvcID As String
    Private fstrmaxSvcID As String
    Private fstrminCusrid As String
    Private fstrmaxCusrid As String
    Private fstrminPrdctMd As String
    Private fstrmaxPrdctMd As String
    Private fstrminState As String
    Private fstrmaxState As String


    Private fstrminRONo As String
    Private fstrmaxRONo As String
 
    Private fstrmindate As System.DateTime
    Private fstrmaxdate As System.DateTime
    Private fstrServiceStat As String
    Private fstrContractTy As String
   
    Private fstrSortBy As String
    Private fstrUserName As String
    Private fstrCountryID As String ' country id
    Private fstrSVCID As String
    Private fstrCompID As String
    Private fstrRank As String
    Private fstrSortByText As String
    Private fstrSpName As String ' store procedure name

    Private fstrServiceBillType As String
    Private fstrServiceBillTypeText As String
    Private fstrUserID As String


    ' use for view appointment service bill history
    Private fstrCustomerPrefix As String
    Private fstrCustomerID As String
    Private fstrRoID As String


#End Region

#Region "Properties"
    Public Property CustomerPrefix() As String
        Get
            Return fstrCustomerPrefix
        End Get
        Set(ByVal Value As String)
            fstrCustomerPrefix = Value
        End Set
    End Property

    Public Property CustomerID() As String
        Get
            Return fstrCustomerID
        End Get
        Set(ByVal Value As String)
            fstrCustomerID = Value
        End Set
    End Property

    Public Property RoID() As String
        Get
            Return fstrRoID
        End Get
        Set(ByVal Value As String)
            fstrRoID = Value
        End Set
    End Property


    Public Property UserID() As String
        Get
            Return fstrUserID
        End Get
        Set(ByVal Value As String)
            fstrUserID = Value
        End Set
    End Property

    Public Property ServiceBillTypeText() As String
        Get
            Return fstrServiceBillTypeText
        End Get
        Set(ByVal Value As String)
            fstrServiceBillTypeText = Value
        End Set
    End Property

    Public Property ServiceBillType() As String
        Get
            Return fstrServiceBillType
        End Get
        Set(ByVal Value As String)
            fstrServiceBillType = Value
        End Set
    End Property

    Public Property SortByText() As String
        Get
            Return fstrSortByText
        End Get
        Set(ByVal Value As String)
            fstrSortByText = Value
        End Set
    End Property
    Public Property Rank() As String
        Get
            Return fstrRank
        End Get
        Set(ByVal Value As String)
            fstrRank = Value
        End Set
    End Property
    Public Property CountryID() As String
        Get
            Return fstrCountryID
        End Get
        Set(ByVal Value As String)
            fstrCountryID = Value
        End Set
    End Property
    'property
    Public Property CompID() As String
        Get
            Return fstrCompID
        End Get
        Set(ByVal Value As String)
            fstrCompID = Value
        End Set
    End Property
    Public Property SVCID() As String
        Get
            Return fstrSVCID
        End Get
        Set(ByVal Value As String)
            fstrSVCID = Value
        End Set
    End Property
    Public Property minCustmID() As String
        Get
            Return fstrminCusrid
        End Get
        Set(ByVal value As String)
            fstrminCusrid = value

        End Set
    End Property
    Public Property maxCustmID() As String
        Get
            Return fstrmaxCusrid
        End Get
        Set(ByVal value As String)
            fstrmaxCusrid = value
        End Set
    End Property
    Public Property minSvcID() As String
        Get
            Return fstrminSvcID
        End Get
        Set(ByVal value As String)
            fstrminSvcID = value
        End Set
    End Property
    Public Property maxSvcID() As String
        Get
            Return fstrmaxSvcID
        End Get
        Set(ByVal value As String)
            fstrmaxSvcID = value
        End Set
    End Property
    Public Property minPrdctMd() As String
        Get
            Return fstrminPrdctMd
        End Get
        Set(ByVal value As String)
            fstrminPrdctMd = value
        End Set
    End Property
    Public Property maxPrdctMd() As String
        Get
            Return fstrmaxPrdctMd
        End Get
        Set(ByVal value As String)
            fstrmaxPrdctMd = value
        End Set
    End Property
    Public Property minState() As String
        Get
            Return fstrminState
        End Get
        Set(ByVal value As String)
            fstrminState = value
        End Set
    End Property
    Public Property maxState() As String
        Get
            Return fstrmaxState
        End Get
        Set(ByVal value As String)
            fstrmaxState = value
        End Set
    End Property
    Public Property minRONo() As String
        Get
            Return fstrminRONo
        End Get
        Set(ByVal value As String)
            fstrminRONo = value
        End Set
    End Property
    Public Property maxRONo() As String
        Get
            Return fstrmaxRONo
        End Get
        Set(ByVal value As String)
            fstrmaxRONo = value
        End Set
    End Property
    Public Property mindate() As System.DateTime
        Get
            Return fstrmindate
        End Get
        Set(ByVal value As System.DateTime)
            fstrmindate = value
        End Set
    End Property  'service date 
    Public Property maxdate() As System.DateTime
        Get
            Return fstrmaxdate
        End Get
        Set(ByVal value As System.DateTime)
            fstrmaxdate = value
        End Set
    End Property

    Public Property ServiceStat() As String
        Get
            Return fstrServiceStat
        End Get
        Set(ByVal Value As String)
            fstrServiceStat = Value
        End Set
    End Property

    Public Property ContractTy() As String
        Get
            Return fstrContractTy
        End Get
        Set(ByVal Value As String)
            fstrContractTy = Value
        End Set
    End Property

    Public Property SortBy() As String
        Get
            Return fstrSortBy
        End Get
        Set(ByVal Value As String)
            fstrSortBy = Value
        End Set
    End Property
    Public Property UserName() As String
        Get
            Return fstrUserName
        End Get
        Set(ByVal Value As String)
            fstrUserName = Value
        End Set
    End Property

#End Region

#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)

        connection.Open()

        Return connection
    End Function
#End Region


#Region "Custom Service History List "
    Public Function GetServiceOrderSum() As DataSet
        Dim selConnection As SqlConnection = Nothing
        Dim selConn As SqlConnection = Nothing
        selConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_RPTCSVH_VIEW"
        Try
            trans = selConnection.BeginTransaction()
            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(20) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrminSvcID)
            Param(1).Value = Trim(fstrmaxSvcID)
            Param(2).Value = Trim(fstrminCusrid)
            Param(3).Value = Trim(fstrmaxCusrid)
            Param(4).Value = Trim(fstrminPrdctMd)
            Param(5).Value = Trim(fstrmaxPrdctMd)
            Param(6).Value = Trim(fstrminState)
            Param(7).Value = Trim(fstrmaxState)

            Param(8).Value = Trim(fstrminRONo)
            Param(9).Value = Trim(fstrmaxRONo)

            Param(10).Value = Trim(fstrmindate)
            Param(11).Value = Trim(fstrmaxdate)
            Param(12).Value = Trim(fstrContractTy)
            Param(13).Value = Trim(fstrSortBy)

            Param(14).Value = Trim(fstrCountryID)
            Param(15).Value = Trim(fstrCompID)
            Param(16).Value = Trim(fstrSVCID)
            Param(17).Value = Trim(fstrRank)
            Param(18).Value = Trim(fstrUserID)
            Param(19).Value = Trim(fstrServiceBillType)




            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            Return ds
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrUserName, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsRPTCSVH.vb")
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrUserName, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsRPTCSVH.vb")


        Finally
            selConnection.Close()
            selConn.Close()
        End Try
    End Function
#End Region

#Region "view appointment customer service history "
    Public Function GetAppointmentCustomerServiceHistory() As DataSet
        Dim selConn As SqlConnection = Nothing

        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        
        fstrSpName = "BB_FNCAPPS_VIEWSERVICEHISTORY"
        Try

            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(5) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrSVCID)
            Param(1).Value = Trim(fstrCustomerPrefix)
            Param(2).Value = Trim(fstrCustomerID)
            Param(3).Value = Trim(fstrRoID)
            Param(4).Value = Trim(fstrUserID)

            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            Return ds
        Catch ex As SqlException

            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrUserName, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsRPTCSVH.vb")
        Catch ex As Exception

            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrUserName, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsRPTCSVH.vb")


        Finally

            selConn.Close()
        End Try
    End Function
#End Region
End Class
