Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web
Imports BusinessEntity
Imports SQLDataAccess

Public Class clsRPTFOCParts
#Region "Declaration"
    Private ServBillDateFr As Date
    Private ServBillDateTo As Date
    Private ServCtrFr As String
    Private ServCtrTo As String
    Private TechIDFr As String
    Private TechIDTo As String
    Private PartCodeFr As String
    Private PartCodeTo As String
    'Private PriceIdFr As String
    'Private PriceIdTo As String
    Private PriceId As String 'lly 20170512
    Private ServType As String
    Private UserId As String

    Private fstrUserName As String
    Private fstrSpName As String
#End Region

#Region "Properties"
    Public Property FOC_ServBillDateFr() As Date
        Get
            Return ServBillDateFr
        End Get
        Set(ByVal value As Date)
            ServBillDateFr = value
        End Set
    End Property

    Public Property FOC_ServBillDateTo() As Date
        Get
            Return ServBillDateTo
        End Get
        Set(ByVal value As Date)
            ServBillDateTo = value
        End Set
    End Property

    Public Property FOC_ServCtrFr() As String
        Get
            Return ServCtrFr
        End Get
        Set(ByVal value As String)
            ServCtrFr = value
        End Set
    End Property

    Public Property FOC_ServCtrTo() As String
        Get
            Return ServCtrTo
        End Get
        Set(ByVal value As String)
            ServCtrTo = value
        End Set
    End Property

    Public Property FOC_TechIDFr() As String
        Get
            Return TechIDFr
        End Get
        Set(ByVal value As String)
            TechIDFr = value
        End Set
    End Property

    Public Property FOC_TechIDTo() As String
        Get
            Return TechIDTo
        End Get
        Set(ByVal value As String)
            TechIDTo = value
        End Set
    End Property

    Public Property FOC_PartCodeFr() As String
        Get
            Return PartCodeFr
        End Get
        Set(ByVal value As String)
            PartCodeFr = value
        End Set
    End Property

    Public Property FOC_PartCodeTo() As String
        Get
            Return PartCodeTo
        End Get
        Set(ByVal value As String)
            PartCodeTo = value
        End Set
    End Property

    'Public Property FOC_PriceIdFr() As String
    '    Get
    '        Return PriceIdFr
    '    End Get
    '    Set(ByVal value As String)
    '        PriceIdFr = value
    '    End Set
    'End Property

    'Public Property FOC_PriceIdTo() As String
    '    Get
    '        Return PriceIdTo
    '    End Get
    '    Set(ByVal value As String)
    '        PriceIdTo = value
    '    End Set
    'End Property

    'lly 20170512
    Public Property FOC_PriceId() As String
        Get
            Return PriceId
        End Get
        Set(ByVal value As String)
            PriceId = value
        End Set
    End Property

    Public Property FOC_ServType() As String
        Get
            Return ServType
        End Get
        Set(ByVal value As String)
            ServType = value
        End Set
    End Property

    Public Property FOC_Userid() As String
        Get
            Return UserId
        End Get
        Set(ByVal value As String)
            UserId = value
        End Set
    End Property
#End Region

#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)

        connection.Open()

        Return connection
    End Function
#End Region

#Region "Retrive Info"
    Public Function RetriveFOCPartsListing() As DataSet
        Dim sqlConn As SqlConnection = Nothing
        sqlConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        fstrSpName = "BB_FOCPartsListing"
        Dim ds As DataSet = New DataSet
        Try
            Dim Param() As SqlParameter = New SqlParameter(10) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sqlConn, fstrSpName)

            Param(0).Value = ServBillDateFr
            Param(1).Value = ServBillDateTo
            Param(2).Value = Trim(ServCtrFr)
            Param(3).Value = Trim(ServCtrTo)
            Param(4).Value = Trim(TechIDFr)
            Param(5).Value = Trim(TechIDTo)
            Param(6).Value = Trim(PartCodeFr)
            Param(7).Value = Trim(PartCodeTo)
            Param(8).Value = Trim(PriceId)
            'Param(8).Value = Trim(PriceIdFr)
            'Param(9).Value = Trim(PriceIdTo)
            Param(9).Value = Trim(ServType)

            ds = SqlHelper.ExecuteDataset(sqlConn, CommandType.StoredProcedure, fstrSpName, Param)

        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrUserName, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsFOCParts.vb")
            Return Nothing
        Finally
            sqlConn.Close()
        End Try
        Return ds
    End Function

    Public Function GetFilterData() As DataSet
        Dim sqlConn As SqlConnection = Nothing
        sqlConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        fstrSpName = "BB_RPTFOC_GetFilterData"
        Dim ds As DataSet = New DataSet
        Try
            Dim Param() As SqlParameter = New SqlParameter(1) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sqlConn, fstrSpName)
            Param(0).Value = Trim(UserId)

            ds = SqlHelper.ExecuteDataset(sqlConn, CommandType.StoredProcedure, fstrSpName, Param)

        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrUserName, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "ClsRptFOCParts.vb")
            Return Nothing
        Finally
            sqlConn.Close()
        End Try
        Return ds
    End Function

#End Region
End Class
