﻿
Imports System.Data.SqlClient
Imports BusinessEntity
Imports System.Data
Imports SQLDataAccess

Public Class ClsRptServiceDue
#Region "Declaration"
    Private _fromSVCCenter As String
    Private _toSVCCenter As String
    Private _fromState As String
    Private _toState As String
    Private _fromArea As String
    Private _toArea As String
    Private _fromProdClass As String
    Private _toProdClass As String
    Private _fromModelID As String
    Private _toModelID As String
    Private _fromServiceDueDate As DateTime
    Private _toServiceDueDate As DateTime

    Private _countryID As String
    Private _username As String
    Private _productID As String

    Private _con As New SqlConnection
    Private _spName As String

    Private _CustID As String
    Private _No As String
    Private _Name As String
    Private _Contact As String
    Private _Serial As String
    Private _SVC As String
    Private _Area As String
    Private _ModName As String
    Private _LastX As String
    Private _DueType As String
    Private _custtype As String


#End Region

#Region "Properties"
    Public Property ModName() As String
        Get
            Return _ModName
        End Get
        Set(ByVal value As String)
            _ModName = value
        End Set
    End Property
    Public Property CustID() As String
        Get
            Return _CustID
        End Get
        Set(ByVal value As String)
            _CustID = value
        End Set
    End Property
    Public Property DueType() As String
        Get
            Return _DueType
        End Get
        Set(ByVal value As String)
            _DueType = value
        End Set
    End Property
    Public Property LastX() As String
        Get
            Return _LastX
        End Get
        Set(ByVal value As String)
            _LastX = value
        End Set
    End Property
    Public Property Area() As String
        Get
            Return _Area
        End Get
        Set(ByVal value As String)
            _Area = value
        End Set
    End Property
    Public Property SVC() As String
        Get
            Return _SVC
        End Get
        Set(ByVal value As String)
            _SVC = value
        End Set
    End Property
    Public Property Serial() As String
        Get
            Return _Serial
        End Get
        Set(ByVal value As String)
            _Serial = value
        End Set
    End Property
    Public Property Contact() As String
        Get
            Return _Contact
        End Get
        Set(ByVal value As String)
            _Contact = value
        End Set
    End Property
    Public Property Name() As String
        Get
            Return _Name
        End Get
        Set(ByVal value As String)
            _Name = value
        End Set
    End Property
    Public Property No() As String
        Get
            Return _No
        End Get
        Set(ByVal value As String)
            _No = value
        End Set
    End Property


    Public Property FromSvcCenter() As String
        Get
            Return _fromSVCCenter
        End Get
        Set(ByVal value As String)
            _fromSVCCenter = value
        End Set
    End Property

    Public Property ToSvcCenter() As String
        Get
            Return _toSVCCenter
        End Get
        Set(ByVal value As String)
            _toSVCCenter = value
        End Set
    End Property

    Public Property FromState() As String
        Get
            Return _fromState
        End Get
        Set(ByVal value As String)
            _fromState = value
        End Set
    End Property

    Public Property ToState() As String
        Get
            Return _toState
        End Get
        Set(ByVal value As String)
            _toState = value
        End Set
    End Property

    Public Property FromArea() As String
        Get
            Return _fromArea
        End Get
        Set(ByVal value As String)
            _fromArea = value
        End Set
    End Property

    Public Property ToArea() As String
        Get
            Return _toArea
        End Get
        Set(ByVal value As String)
            _toArea = value
        End Set
    End Property

    Public Property FromProdClass() As String
        Get
            Return _fromProdClass
        End Get
        Set(ByVal value As String)
            _fromProdClass = value
        End Set
    End Property

    Public Property ToProdClass() As String
        Get
            Return _toProdClass
        End Get
        Set(ByVal value As String)
            _toProdClass = value
        End Set
    End Property

    Public Property FromModelID() As String
        Get
            Return _fromModelID
        End Get
        Set(ByVal value As String)
            _fromModelID = value
        End Set
    End Property

    Public Property ToModelID() As String
        Get
            Return _toModelID
        End Get
        Set(ByVal value As String)
            _toModelID = value
        End Set
    End Property

    Public Property FromServiceDueDate() As Date
        Get
            Return _fromServiceDueDate
        End Get
        Set(ByVal value As Date)
            _fromServiceDueDate = value
        End Set
    End Property

    Public Property ToServiceDueDate() As Date
        Get
            Return _toServiceDueDate
        End Get
        Set(ByVal value As Date)
            _toServiceDueDate = value
        End Set
    End Property

    Public Property CountryID() As String
        Get
            Return _countryID
        End Get
        Set(ByVal value As String)
            _countryID = value
        End Set
    End Property

    Public Property Username() As String
        Get
            Return _username
        End Get
        Set(ByVal value As String)
            _username = value
        End Set
    End Property

    Public Property ProductID() As String
        Get
            Return _productID
        End Get
        Set(ByVal value As String)
            _productID = value
        End Set
    End Property

    Public Property CustType() As String
        Get
            Return _custtype
        End Get
        Set(ByVal value As String)
            _custtype = value
        End Set
    End Property

#End Region

#Region "Private Functions"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)

        connection.Open()

        Return connection
    End Function
#End Region

#Region "Public Function"
    Public Function PopulateProductClass(ByVal flag As String) As DataTable
        Dim dt As New DataTable("ProductClass")

        _con = GetConnection(ConfigurationManager.AppSettings("ConnectionString"))
        _spName = "BB_SELECTSTATUS"

        Try
            Dim param() As SqlParameter = New SqlParameter(1) {}
            param = SqlHelperParameterCache.GetSpParameterSet(_con, _spName)
            param(0).Value = flag

            dt = SqlHelper.ExecuteDataset(_con, CommandType.StoredProcedure, _spName, param).Tables(0)
            dt.Columns.Remove("LCOD_CODTY")
            dt.Columns.Remove("LCOD_CODID1")
            dt.Columns.Remove("LCOD_SEQ")
        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(Username, ErrorLog.Item(1).ToString, _spName, WriteErrLog.SELE, "ClsRptInstall.vb")
        Finally
            _con.Close()
        End Try

        Return dt
    End Function

    Public Function RetrieveModelFromProduct() As DataSet
        Dim ds As New DataSet("Models")
        _con = GetConnection(ConfigurationManager.AppSettings("ConnectionString"))
        _spName = "MASPROD_IDNAME"

        Try
            Dim param() As SqlParameter = New SqlParameter(2) {}
            param = SqlHelperParameterCache.GetSpParameterSet(_con, _spName)
            param(0).Value = _productID
            param(1).Value = _countryID

            ds = SqlHelper.ExecuteDataset(_con, CommandType.StoredProcedure, _spName, param)

        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(_username, ErrorLog.Item(1).ToString, _spName, WriteErrLog.SELE, "ClsRpServiceDue.vb")
            Return Nothing
        End Try
        Return ds
    End Function

    Public Function RetrieveServiceDue() As DataSet
        Dim dt As New BlueBerryReports.ServiceBillListingDataTable
        Dim ds As New DataSet("Records")
        Dim dr As SqlDataReader
        Dim common As New clsCommonClass
        _con = GetConnection(ConfigurationManager.AppSettings("ConnectionString"))
        _spName = "BB_RPTSERVDUELST"

        Try
            Dim param() As SqlParameter = New SqlParameter(13) {}
            param = SqlHelperParameterCache.GetSpParameterSet(_con, _spName)
            param(0).Value = _fromProdClass
            param(1).Value = _toProdClass
            param(2).Value = _fromModelID
            param(3).Value = _toModelID
            param(4).Value = _fromServiceDueDate
            param(5).Value = _toServiceDueDate
            param(6).Value = _fromSVCCenter
            param(7).Value = _toSVCCenter
            param(8).Value = _fromState
            param(9).Value = _toState
            param(10).Value = _fromArea
            param(11).Value = _toArea
            param(12).Value = _custtype

            ds = SqlHelper.ExecuteDataset(_con, CommandType.StoredProcedure, _spName, param)
            dr = SqlHelper.ExecuteReader(_con, CommandType.StoredProcedure, _spName, param)
            dt.Load(dr)


            'Dim i As Integer
            'i = 1
            ''dt = CType(ds.Tables(0), BlueBerryReports.ServiceBillListingDataTable)
            'For Each row As DataRow In dt.Rows
            '    'row("MTEL_TELNO") = common.passconverttel(row("MTEL_TELNO").ToString())
            '    'Contact = Contact + "<br>" + row("MTEL_TELNO")
            '    'CustID = CustID + "<br>" + row("MCUS_CUSID")
            '    'No = No + "<br>" + i.ToString
            '    'Name = Name + "<br>" + Left(row("MCUS_ENAME"), 20)
            '    'Serial = Serial + "<br>" + row("MROU_SERNO")
            '    'SVC = SVC + "<br>" + row("SVC_NAME")
            '    'Area = Area + "<br>" + row("MARE_ENAME")
            '    'ModName = ModName + "<br>" + row("MMOD_ENAME")
            '    'LastX = LastX + "<br>" + row("MROU_APTDT")
            '    'DueType = DueType + "<br>" + row("MROU_LSVDT")

            '    i = i + 1
            'Next


            'ds.Tables.Clear()
            'ds.Tables.Add(dt)


        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(_username, ErrorLog.Item(1).ToString, _spName, WriteErrLog.SELE, "ClsRpServiceDue.vb")
        End Try

        Return ds
    End Function
#End Region

    
End Class