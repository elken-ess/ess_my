Imports System.Configuration
Imports CrystalDecisions.Web
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports BusinessEntity
Imports System.Data


Public Class ClsCommonReport
#Region "Declaration"

    Private fstrReportName As String
    Private fstrReportUrl As String

    Private fcrvViewer As New CrystalReportViewer
    Private fcrvReportSource As New CrystalReportSource
    Private fstrMessage As String = ""

    Dim lcrReport As New ReportDocument()

    Dim fstrPdfPath As String = ""
    Dim fstrPdfUrl As String = ""
    Dim fstrPdfFileName As String = ""

#End Region

#Region "Properties"
    Public Property PdfFileName() As String
        Get
            Return fstrPdfFileName
        End Get
        Set(ByVal Value As String)
            fstrPdfFileName = Value
        End Set
    End Property

    Public Property PdfPath() As String
        Get
            Return fstrPdfPath
        End Get
        Set(ByVal Value As String)
            fstrPdfPath = Value
        End Set
    End Property

    Public Property PdfUrl() As String
        Get
            Return fstrPdfUrl
        End Get
        Set(ByVal Value As String)
            fstrPdfUrl = Value
        End Set
    End Property


    Public Property ReportFileName() As String
        Get
            Return fstrReportName
        End Get
        Set(ByVal Value As String)
            fstrReportName = Value
        End Set
    End Property


    Public Property GetMessage() As String
        Get
            Return fstrMessage
        End Get
        Set(ByVal Value As String)
            fstrMessage = Value
        End Set
    End Property
#End Region

#Region "Methods"

    'inpViewer控件 ID ,ds 数据源,strHead 报表头字符串数组

    Public Sub SetReportDocument(ByRef ds As DataSet, ByVal strHead As Array)


        fstrReportUrl = ReportPath() & fstrReportName '得到报表的完整URL        
        If CheckReportExist() And Not (ds Is Nothing) Then
            Call SetReportParameter(ds, strHead)
        Else
            ' write log file for file not exist
        End If
    End Sub

    Public Sub SetMdstReportDocument(ByRef ds As DataSet, ByVal strHead As Array)


        fstrReportUrl = ReportPath() & fstrReportName '得到报表的完整URL        
        If CheckReportExist() And Not (ds Is Nothing) Then
            Call SetMdstReportParameter(ds, strHead)
        Else
            ' write log file for file not exist
        End If
    End Sub

    Public Sub SetReport(ByRef inpViewer As CrystalReportViewer, ByRef ds As DataSet, ByVal strHead As Array)

        fcrvViewer = inpViewer
        Call SetViewProperties(fcrvViewer) '设置fcrvViewer的属性
        fstrReportUrl = ReportPath() & fstrReportName '得到报表的完整URL        
        If CheckReportExist() And Not (ds Is Nothing) Then
            Call SetReportParameter(ds, strHead)
        Else
            ' write log file for file not exist
        End If
        'UnloadReport()
    End Sub

    'LLY 20160906
    Public Sub SetReminderLetterReport(ByRef inpViewer As CrystalReportViewer, ByRef ds As DataSet, ByVal strHead As Array, ByVal trnno As String, ByVal ageing As String)

        fcrvViewer = inpViewer
        Call SetViewProperties(fcrvViewer) '设置fcrvViewer的属性
        fstrReportUrl = ReportPath() & fstrReportName '得到报表的完整URL        
        If CheckReportExist() And Not (ds Is Nothing) Then
            Call SetReminderLetterReportParameter(ds, strHead, trnno, ageing)
        Else
            ' write log file for file not exist
        End If
        'UnloadReport()
    End Sub


    Public Sub UnloadReport()
        Try
            lcrReport.Dispose()
            lcrReport.Close()
        Catch ex As Exception
            fstrmessage = ex.Message.ToString
        End Try

    End Sub

    Private Function CheckReportExist() As Boolean
        Dim lblnfileExists As Boolean = False

        Try
            If IO.File.Exists(fstrReportUrl) Then
                Dim lcrReport As New ReportDocument()
                lcrReport.Load(fstrReportUrl)
                lcrReport.Close()
                lblnfileExists = True
            End If
        Catch e As Exception
            lblnfileExists = False
        End Try

        Return lblnfileExists
    End Function

    Public Sub SetViewProperties(ByRef inpViewer As CrystalReportViewer)
        With inpViewer
            .DisplayToolbar = True
            .DisplayGroupTree = False
            .HasExportButton = True
            .HasGotoPageButton = True
            .HasPrintButton = True
            .HasToggleGroupTreeButton = False
            .Width = 800
            .Height = 600
            .HasCrystalLogo = False
        End With
    End Sub

    Public Function ReportPath() As String
        ReportPath = ConfigurationSettings.AppSettings("ReportPath")
    End Function

    Private Sub SetMdstReportParameter(ByRef ds As DataSet, ByVal strHead As Array)
        Try
            Dim i As Integer
            lcrReport.Load(fstrReportUrl) '将物理路径下存在的crystalreport1.rpt和reportDocument关联。           
            i = 0


            'lcrReport.Database.Tables(i).SetDataSource(ds)
            lcrReport.SetDataSource(ds)
            lcrReport.Subreports.Item("CrystMDSL2").SetDataSource(ds.Tables(1))

            'While i < lcrReport.Database.Tables.Count And i < ds.Tables.Count
            '    lcrReport.Database.Tables(i).SetDataSource(ds.Tables(i))
            '    lcrReport.Subreports.Item("CrystMDSL2").SetDataSource(ds.Tables(i))
            '    i = i + 1
            'End While





            i = 0
            While i < lcrReport.ParameterFields.Count And i < strHead.Length
                lcrReport.SetParameterValue("id" + (i + 1).ToString(), strHead(i))
                i = i + 1
            End While
            fcrvViewer.ReportSource = lcrReport
        Catch ex As Exception
            fstrMessage = ex.Message.ToString()
        End Try
    End Sub

    Private Sub SetReportParameter(ByRef ds As DataSet, ByVal strHead As Array)
        Try
            Dim i As Integer
            lcrReport.Load(fstrReportUrl) '将物理路径下存在的crystalreport1.rpt和reportDocument关联。           
            i = 0
            While i < lcrReport.Database.Tables.Count And i < ds.Tables.Count
                lcrReport.Database.Tables(i).SetDataSource(ds.Tables(i))
                i = i + 1
            End While
            i = 0
            While i < lcrReport.ParameterFields.Count And i < strHead.Length
                lcrReport.SetParameterValue("id" + (i + 1).ToString(), strHead(i))
                i = i + 1
            End While
            fcrvViewer.ReportSource = lcrReport

        Catch ex As Exception
            fstrMessage = ex.Message.ToString()
        End Try
    End Sub

    'LLY 20160906
    Private Sub SetReminderLetterReportParameter(ByRef ds As DataSet, ByVal strHead As Array, ByVal Trnno As String, ByVal Ageing As String)
        Try
            Dim i As Integer
            lcrReport.Load(fstrReportUrl)

            Logon(lcrReport, "", "", "", "")

            i = 0
            While i < lcrReport.Database.Tables.Count And i < ds.Tables.Count
                lcrReport.Database.Tables(i).SetDataSource(ds.Tables(i))
                i = i + 1
            End While

            'i = 0
            'While i < ds2.Tables.Count
            '    'lcrReport.Subreports(0).Database.Tables(i).SetDataSource(ds2.Tables(i))
            '    i = i + 1
            'End While

            i = 0
            While i < lcrReport.ParameterFields.Count And i < strHead.Length
                lcrReport.SetParameterValue("id" + (i + 1).ToString(), strHead(i))
                'lcrReport.SetParameterValue("id18", ds.Tables(0).Rows(0).Item("INV").ToString(), "RPTDebtor_ReminderLetter_AccSum.rpt")
                'lcrReport.SetParameterValue("@Trnno", ds.Tables(0).Rows(0).Item("INV").ToString(), "RPTDebtor_ReminderLetter_AccSum.rpt")
                i = i + 1
            End While
            lcrReport.SetParameterValue("@Trnno", Trnno)
            lcrReport.SetParameterValue("@Ageing", Ageing)
            fcrvViewer.ReportSource = lcrReport
        Catch ex As Exception
            fstrMessage = ex.Message.ToString()
        End Try
    End Sub

    Public Function GetReportDocument() As ReportDocument
        Return lcrReport
    End Function

    Public Function ExportPdf()
        'Try

        
            fstrPdfPath = ConfigurationManager.AppSettings("PathPdf")
            fstrPdfUrl = ConfigurationManager.AppSettings("UrlPdf") & fstrPdfFileName


            Dim lstrPhysicalFile As String = fstrPdfPath & fstrPdfFileName


            If System.IO.Directory.Exists(fstrPdfPath) = False Then
                System.IO.Directory.CreateDirectory(fstrPdfPath)
            End If

            Dim crExportOptions As ExportOptions
            Dim crDiskFileDestinationOptions As DiskFileDestinationOptions

            crDiskFileDestinationOptions = New DiskFileDestinationOptions()
            crDiskFileDestinationOptions.DiskFileName = lstrPhysicalFile

            crExportOptions = lcrReport.ExportOptions
            With crExportOptions
                .ExportDestinationOptions = crDiskFileDestinationOptions
                .ExportDestinationType = ExportDestinationType.DiskFile
                .ExportFormatType = ExportFormatType.PortableDocFormat
            End With
            lcrReport.Export()
        'Catch ex As Exception
        '    fstrMessage = ex.Message.ToString()
        'End Try
    End Function
    
#End Region


    Private Function ApplyLogon(ByVal cr As CrystalDecisions.CrystalReports.Engine.ReportDocument, ByVal ci As CrystalDecisions.Shared.ConnectionInfo) As Boolean
        ' This function is called by the "Logon" function
        ' It loops through the report tables and applies
        ' the connection information to each table.

        ' Declare the TableLogOnInfo object and a table object for use later.
        Dim li As CrystalDecisions.Shared.TableLogOnInfo
        ' For each table apply connection info.

        For Each tbl As CrystalDecisions.CrystalReports.Engine.Table In cr.Database.Tables

            li = tbl.LogOnInfo
            li.ConnectionInfo.ServerName = "myhqcrms002"
            li.ConnectionInfo.DatabaseName = "BB_20150611"
            li.ConnectionInfo.UserID = "bbappl"
            li.ConnectionInfo.Password = "l2u6gw9i75d18"
            tbl.ApplyLogOnInfo(li)
            tbl.Location = ci.DatabaseName + ".dbo." + tbl.Name

            ' Verify that the logon was successful.
            ' If TestConnectivity returns false, correct table locations.
            If Not tbl.TestConnectivity() Then

                Return False
            End If
        Next
        Return True
    End Function


    'Check whether crytal report can login to the server
    Private Function Logon(ByVal cr As CrystalDecisions.CrystalReports.Engine.ReportDocument, ByVal server As String, ByVal database As String, ByVal user_id As String, ByVal password As String) As Boolean
        ' Declare and instantiate a new connection info object.
        Dim ci As CrystalDecisions.Shared.ConnectionInfo
        ci = New CrystalDecisions.Shared.ConnectionInfo()

        ci.ServerName = "myhqcrms002"
        ci.DatabaseName = "BB_20150611"
        ci.UserID = "bbappl"
        ci.Password = "l2u6gw9i75d18"
        'password;
        ' ci.IntegratedSecurity = false;
        ' If the ApplyLogon function fails then return a false for this function.
        ' We are applying logon information to the main report at this stage.
        If Not ApplyLogon(cr, ci) Then
            Return False
        End If

        ' Declare a subreport object.
        Dim subobj As CrystalDecisions.CrystalReports.Engine.SubreportObject

        ' Loop through all the report objects and locate subreports.
        ' If a subreport is found then apply logon information to
        ' the subreport.
        For Each obj As CrystalDecisions.CrystalReports.Engine.ReportObject In cr.ReportDefinition.ReportObjects
            If obj.Kind = CrystalDecisions.[Shared].ReportObjectKind.SubreportObject Then
                subobj = DirectCast(obj, CrystalDecisions.CrystalReports.Engine.SubreportObject)
                If Not ApplyLogon(cr.OpenSubreport(subobj.SubreportName), ci) Then
                    Return False
                End If
            End If
        Next

        ' Return True if the code runs to this stage.
        Return True


    End Function
End Class
