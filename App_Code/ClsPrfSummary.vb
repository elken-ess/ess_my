
Imports System.Data
Imports System.Data.SqlClient
Imports BusinessEntity
Imports SQLDataAccess

Public Class ClsPrfSummary
#Region "Declaration"
    Private _svcId As String
    Private _techId As String
    Private _prfNo As String
    Private _ctryId As String

    Private _isoDesc1 As String
    Private _isoDesc2 As String
    Private _col1 As String
    Private _loggedUser As String

#End Region

#Region "Properties"
    Public Property SvcId() As String
        Get
            Return _svcId
        End Get
        Set(ByVal value As String)
            _svcId = value
        End Set
    End Property

    Public Property TechId() As String
        Get
            Return _techId
        End Get
        Set(ByVal value As String)
            _techId = value
        End Set
    End Property

    Public Property PrfNo() As String
        Get
            Return _prfNo
        End Get
        Set(ByVal value As String)
            _prfNo = value
        End Set
    End Property

    Public Property CtryId() As String
        Get
            Return _ctryId
        End Get
        Set(ByVal value As String)
            _ctryId = value
        End Set
    End Property

    Public ReadOnly Property IsoDesc1() As String
        Get
            Return _isoDesc1
        End Get
    End Property

    Public ReadOnly Property IsoDesc2() As String
        Get
            Return _isoDesc2
        End Get
    End Property

    Public ReadOnly Property Col1() As String
        Get
            Return _col1
        End Get
    End Property

    Public Property LoggedUser() As String
	    Get
            Return _loggedUser
        End Get
        Set(ByVal value As String)
            _loggedUser = value
        End Set
    End Property

#End Region

#Region "Private methods"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)

        connection.Open()

        Return connection
    End Function
#End Region

#Region "Public Methods"
    Public Function GetPack() As DataRow()
        Dim dr As DataRow() = Nothing
        Dim dt As New DataTable("PRF")

        Dim _con As SqlConnection

        Const sp As String = "BB_RPTPRFSummary"

        _con = GetConnection(ConfigurationManager.AppSettings("ConnectionString"))

        Try
            Dim param() As SqlParameter = New SqlParameter(8) {}
            param = SqlHelperParameterCache.GetSpParameterSet(_con, sp)
            param(0).Value = _svcId
            param(1).Value = _techId
            param(2).Value = _prfNo
            param(3).Value = _ctryId
            param(4).Value = LoggedUser
            param(5).Value = Nothing
            param(6).Value = Nothing
			param(7).Value = Nothing
            dt = SqlHelper.ExecuteDataset(_con, CommandType.StoredProcedure, sp, param).Tables(0)
            dr = dt.Select()

            _isoDesc1 = param(4).Value.ToString()
            _isoDesc2 = param(5).Value.ToString()
            _col1 = param(6).Value.ToString()

        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(_loggedUser, ErrorLog.Item(1).ToString, sp, WriteErrLog.SELE, "ClsPrfSummary.vb")
        Finally
            _con.Close()
        End Try

        Return dr
    End Function
#End Region


End Class
