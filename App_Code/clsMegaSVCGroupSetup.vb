﻿Imports SQLDataAccess
Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Web
Imports System.Data

Public Class ClsMegaSVCGroupSetup
    Private strConn As String

#Region "Declaration"

    Private _group As String ' group
    Private _model As String ' model
    Private _months As String ' months
    Private _interval As String ' interval
    Private _frequency As String ' frequency
    Private _createdby As String ' created by
    Private _createddate As System.DateTime ' created date
    Private _modifiedby As String ' modified by
    Private _modifieddate As System.DateTime ' modified date
    Private fstrSpName As String ' store procedure name
    Private _id As String
    Private _stat As String



#End Region

#Region "Properties"

    Public Property ID() As String
        Get
            Return _id
        End Get
        Set(ByVal Value As String)
            _id = Value
        End Set
    End Property

    Public Property Group() As String
        Get
            Return _group
        End Get
        Set(ByVal Value As String)
            _group = Value
        End Set
    End Property

    Public Property Model() As String
        Get
            Return _model
        End Get
        Set(ByVal Value As String)
            _model = Value
        End Set
    End Property


    Public Property Months() As String
        Get
            Return _months
        End Get
        Set(ByVal Value As String)
            _months = Value
        End Set
    End Property

    Public Property Interval() As String
        Get
            Return _interval
        End Get
        Set(ByVal Value As String)
            _interval = Value
        End Set
    End Property
    Public Property Frequency() As String
        Get
            Return _frequency
        End Get
        Set(ByVal Value As String)
            _frequency = Value
        End Set
    End Property

    Public Property CreatedBy() As String
        Get
            Return _createdby
        End Get
        Set(ByVal Value As String)
            _createdby = Value
        End Set
    End Property
    Public Property ModifiedBy() As String
        Get
            Return _modifiedby
        End Get
        Set(ByVal Value As String)
            _modifiedby = Value
        End Set
    End Property

    Public Property CreateDate() As String
        Get
            Return _createddate
        End Get
        Set(ByVal Value As String)
            _createddate = Value
        End Set
    End Property
    Public Property ModifyDate() As String
        Get
            Return _modifieddate
        End Get
        Set(ByVal Value As String)
            _modifieddate = Value
        End Set
    End Property
    Public Property Stat() As String
        Get
            Return _stat
        End Get
        Set(ByVal Value As String)
            _stat = Value
        End Set
    End Property

#End Region

#Region "Methods"

#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)

        connection.Open()

        Return connection
    End Function
#End Region

#Region "Insert"

    Public Function Insert() As Integer

        Dim insConnection As SqlConnection = Nothing
        Dim insConn As SqlConnection = Nothing
        insConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        insConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MEGASVCGROUPSETUP_ADD"
        Try
            trans = insConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(7) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(insConn, fstrSpName)
            Param(0).Value = Trim(_group)
            Param(1).Value = Trim(_model)
            Param(2).Value = Trim(_months)
            Param(3).Value = Trim(_interval)
            Param(4).Value = Trim(_frequency)
            Param(5).Value = Trim(_createdby)
            Param(6).Value = Trim(_modifiedby)
            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(insConn, CommandType.StoredProcedure, fstrSpName, Param)

            trans.Commit()
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(_modifiedby, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsMegaSVCGroupSetup.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(_modifiedby, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsMegaSVCGroupSetup.vb")
            Return -1
        Finally
            insConnection.Close()
            insConn.Close()
        End Try

    End Function

#End Region

#Region "Update"

    Public Function Update() As Integer

        Dim UpdConnection As SqlConnection = Nothing
        Dim UpdConn As SqlConnection = Nothing
        UpdConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        UpdConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MEGASVCGROUPSETUP_UPD"
        Try
            trans = UpdConnection.BeginTransaction()
            Dim updParam() As SqlParameter = New SqlParameter(8) {}
            updParam = SqlHelperParameterCache.GetSpParameterSet(UpdConn, fstrSpName)
            updParam(0).Value = Trim(_id)
            updParam(1).Value = Trim(_group)
            updParam(2).Value = Trim(_model)
            updParam(3).Value = Trim(_months)
            updParam(4).Value = Trim(_interval)
            updParam(5).Value = Trim(_frequency)
            updParam(6).Value = Trim(_modifiedby)
            updParam(7).Value = Trim(_stat)

            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(UpdConn, CommandType.StoredProcedure, fstrSpName, updParam)

            trans.Commit()
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(_modifiedby, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "clsMegaSVCGroupSetup.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(_modifiedby, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "clsMegaSVCGroupSetup.vb")
            Return -1
        Finally
            UpdConnection.Close()
            UpdConn.Close()
        End Try

    End Function

#End Region

#Region "View"

    Public Function GetData() As DataSet

        Dim selConnection As SqlConnection = Nothing
        Dim selConn As SqlConnection = Nothing
        selConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "dbo.BB_MEGASVC_VIEW"
        Try
            trans = selConnection.BeginTransaction()

            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(4) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(_group)
            Param(1).Value = Trim(_model)
            Param(2).Value = Trim(_months)
            Param(3).Value = Trim(_stat)

            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)

            Return ds

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(_modifiedby, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsMegaSVCGroupSetup.vb")
        Finally
            selConnection.Close()
            selConn.Close()
        End Try

    End Function
#End Region

#Region "View Details"

    Public Function GetDataDetails() As ArrayList
        Dim retnArray As ArrayList = New ArrayList()
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "dbo.BB_MEGASVC_VIEW_DETAILS"
        Try
            trans = sidConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(_id)

            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            'Return dr
            If dr.Read() Then
                Dim count As Integer
                For count = 0 To dr.FieldCount - 1
                    retnArray.Add(dr.GetValue(count).ToString())
                Next
            End If
            dr.Close()
            Return retnArray

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(_modifiedby, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsMegaSVCGroupSetup.vb")
        Finally
            sidConnection.Close()
            sidConn.Close()
        End Try
    End Function
#End Region

#Region "Check existing record"
    Public Function CheckRecordIfExist() As Integer
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "dbo.BB_MEGASVCGROUPSETUP_SELBYNM"
        Try
            trans = sidConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(_group)
            Param(1).Value = Trim(_model)

            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            If dr.Read() Then
                dr.Close()
                Return -1
            End If
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(_modifiedby, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsMegaSVCGroupSetup.vb")
            Return -1
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(_modifiedby, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsMegaSVCGroupSetup.vb")
            Return -1
        Finally
            sidConnection.Close()
            sidConn.Close()
        End Try
    End Function
#End Region

#Region "confirm no duplicated group and name"
    Public Function GetDuplicated() As Integer
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MEGASVCGROUPSETUP_IFDUP"
        Try
            trans = sidConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(_group)
            Param(1).Value = Trim(_model)

            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            If dr.Read() Then
                dr.Close()
                Return -1
            End If
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(_modifiedby, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsMegaSVCGroupSetup.vb")
            Return -1
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(_modifiedby, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsMegaSVCGroupSetup.vb")
            Return -1
        Finally
            sidConnection.Close()
            sidConn.Close()
        End Try
    End Function
#End Region


#End Region

End Class
