Imports SQLDataAccess
Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Web
Imports System.Data
#Region " Module Amment Hisotry "
'Description     :  the Technician
'History         :  
'Modified Date   :  2006-04-16
'Version         :  v1.0
'Author          :  朱文姿


#End Region
Public Class clsTechnician
#Region "Declaration"

    Private fstrTechnicianID As String ' Technician ID
    Private fstrTechnicianName As String 'Technician Name
    Private fstrHRID As String ' Technician Staff ID - LLY 20160404
    Private fstrAlternateName As String ' alternate country  name
    Private fstrNRIC As String ' NRIC
    Private fstrTechnicianType As String ' Technician Type
    Private fstrAddress1 As String ' Address 1
    Private fstrAddress2 As String ' Address 2
    Private fstrPOCode As String  'PO Code
    Private fstrCountryID As String 'Country ID
    Private fstrStateID As String ' State ID

    Private fstrAreaID As String ' Area ID
    Private fstrPOCode2 As String  'PO Code
    Private fstrCountryID2 As String 'Country ID
    Private fstrStateID2 As String ' State ID

    Private fstrAreaID2 As String ' Area ID
    Private fstrTelephone1 As String 'Telephone 1
    Private fstrTelephone2 As String ' Telephone 2
    Private fstrMobile As String '  Mobile 
    Private fstrFax As String ' Fax

    Private fstrStatus As String ' Status

    Private fstrMailingAddress1 As String ' Mailing Address1
    Private fstrMailingAddress2 As String  'Mailing Address2
    Private fintMaxJobday As Integer 'Max Job day
    Private fstrDrivingLicense As String 'Driving License
    Private fstrServiceCenter As String 'Service Center

    Private fstrCreatedBy As String ' Created By
    'Private fstrCreatedDate As System.DateTime  'Created Date
    Private fstrModifiedBy As String 'Modified By
    'Private fstrModifiedDate As System.DateTime 'Modified Date

    Private fstrSpName As String ' store procedure name
    Private fstrusername As String ' username
    Private fstrrank As String ' rank

    Private fstrctry As String ' user country
    Private fstrcomp As String ' user company
    Private fstrsvc As String '  user svcid
    Private fstripadress As String ' ipadress
#End Region

#Region "Properties"

    Public Property Ipadress() As String
        Get
            Return fstripadress
        End Get
        Set(ByVal Value As String)
            fstripadress = Value
        End Set
    End Property
    Public Property username() As String
        Get
            Return fstrusername
        End Get
        Set(ByVal Value As String)
            fstrusername = Value
        End Set
    End Property
    Public Property ctryid() As String
        Get
            Return fstrctry
        End Get
        Set(ByVal Value As String)
            fstrctry = Value
        End Set
    End Property
    Public Property compid() As String
        Get
            Return fstrcomp
        End Get
        Set(ByVal Value As String)
            fstrcomp = Value
        End Set
    End Property
    Public Property svcid() As String
        Get
            Return fstrsvc
        End Get
        Set(ByVal Value As String)
            fstrsvc = Value
        End Set
    End Property
    Public Property rank() As String
        Get
            Return fstrrank
        End Get
        Set(ByVal Value As String)
            fstrrank = Value
        End Set
    End Property

    Public Property TechnicianID() As String
        Get
            Return fstrTechnicianID
        End Get
        Set(ByVal Value As String)
            fstrTechnicianID = Value
        End Set
    End Property

    'LLY 20160404
    Public Property HRID() As String
        Get
            Return fstrHRID
        End Get
        Set(ByVal Value As String)
            fstrHRID = Value
        End Set
    End Property

    Public Property TechnicianName() As String
        Get
            Return fstrTechnicianName
        End Get
        Set(ByVal Value As String)
            fstrTechnicianName = Value
        End Set
    End Property


    Public Property AlternateName() As String
        Get
            Return fstrAlternateName
        End Get
        Set(ByVal Value As String)
            fstrAlternateName = Value
        End Set
    End Property

    Public Property NRIC() As String
        Get
            Return fstrNRIC
        End Get
        Set(ByVal Value As String)
            fstrNRIC = Value
        End Set
    End Property
    Public Property TechnicianType() As String
        Get
            Return fstrTechnicianType
        End Get
        Set(ByVal Value As String)
            fstrTechnicianType = Value
        End Set
    End Property

    Public Property Address1() As String
        Get
            Return fstrAddress1
        End Get
        Set(ByVal Value As String)
            fstrAddress1 = Value
        End Set
    End Property
    Public Property Address2() As String
        Get
            Return fstrAddress2
        End Get
        Set(ByVal Value As String)
            fstrAddress2 = Value
        End Set
    End Property

    Public Property POCode() As String
        Get
            Return fstrPOCode
        End Get
        Set(ByVal Value As String)
            fstrPOCode = Value
        End Set
    End Property

    Public Property CountryID() As String
        Get
            Return fstrCountryID
        End Get
        Set(ByVal Value As String)
            fstrCountryID = Value
        End Set
    End Property
    Public Property StateID() As String
        Get
            Return fstrStateID
        End Get
        Set(ByVal Value As String)
            fstrStateID = Value
        End Set
    End Property

    Public Property AreaID() As String
        Get
            Return fstrAreaID
        End Get
        Set(ByVal Value As String)
            fstrAreaID = Value
        End Set
    End Property
    Public Property POCode2() As String
        Get
            Return fstrPOCode2
        End Get
        Set(ByVal Value As String)
            fstrPOCode2 = Value
        End Set
    End Property

    Public Property CountryID2() As String
        Get
            Return fstrCountryID2
        End Get
        Set(ByVal Value As String)
            fstrCountryID2 = Value
        End Set
    End Property
    Public Property StateID2() As String
        Get
            Return fstrStateID2
        End Get
        Set(ByVal Value As String)
            fstrStateID2 = Value
        End Set
    End Property

    Public Property AreaID2() As String
        Get
            Return fstrAreaID2
        End Get
        Set(ByVal Value As String)
            fstrAreaID2 = Value
        End Set
    End Property
    Public Property Telephone1() As String
        Get
            Return fstrTelephone1
        End Get
        Set(ByVal Value As String)
            fstrTelephone1 = Value
        End Set
    End Property
    Public Property Telephone2() As String
        Get
            Return fstrTelephone2
        End Get
        Set(ByVal Value As String)
            fstrTelephone2 = Value
        End Set
    End Property
    Public Property Mobile() As String
        Get
            Return fstrMobile
        End Get
        Set(ByVal Value As String)
            fstrMobile = Value
        End Set
    End Property
    Public Property Fax() As String
        Get
            Return fstrFax
        End Get
        Set(ByVal Value As String)
            fstrFax = Value
        End Set
    End Property
    Public Property Status() As String
        Get
            Return fstrStatus
        End Get
        Set(ByVal Value As String)
            fstrStatus = Value
        End Set
    End Property
    Public Property MailingAddress1() As String
        Get
            Return fstrMailingAddress1
        End Get
        Set(ByVal Value As String)
            fstrMailingAddress1 = Value
        End Set
    End Property
    Public Property MailingAddress2() As String
        Get
            Return fstrMailingAddress2
        End Get
        Set(ByVal Value As String)
            fstrMailingAddress2 = Value
        End Set
    End Property
    Public Property MaxJobday() As Integer
        Get
            Return fintMaxJobday
        End Get
        Set(ByVal Value As Integer)
            fintMaxJobday = Value
        End Set
    End Property
    Public Property DrivingLicense() As String
        Get
            Return fstrDrivingLicense
        End Get
        Set(ByVal Value As String)
            fstrDrivingLicense = Value
        End Set
    End Property
    Public Property ServiceCenter() As String
        Get
            Return fstrServiceCenter
        End Get
        Set(ByVal Value As String)
            fstrServiceCenter = Value
        End Set
    End Property
    Public Property CreateBy() As String
        Get
            Return fstrCreatedBy
        End Get
        Set(ByVal Value As String)
            fstrCreatedBy = Value
        End Set
    End Property

    'Public Property CreateDate() As String
    '    Get
    '        Return fstrCreatedDate
    '    End Get
    '    Set(ByVal Value As String)
    '        fstrCreatedDate = Convert.ToDateTime(Value)
    '    End Set
    'End Property

    Public Property ModifyBy() As String
        Get
            Return fstrModifiedBy
        End Get
        Set(ByVal Value As String)
            fstrModifiedBy = Value
        End Set
    End Property
    'Public Property ModifyDate() As String
    '    Get
    '        Return fstrModifiedDate
    '    End Get
    '    Set(ByVal Value As String)
    '        fstrModifiedDate = Convert.ToDateTime(Value)
    '    End Set
    'End Property

#End Region
#Region "Meth"
#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)

        connection.Open()

        Return connection
    End Function
#End Region
#Region "Get All Technician For View"
    Public Function GetAllTechnician() As DataSet
        Dim viewConnection As SqlConnection = Nothing
        Dim viewConn As SqlConnection = Nothing
        viewConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        viewConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASTECH_VIEW"
        Try
            trans = viewConnection.BeginTransaction()
            Dim ds As DataSet = SqlHelper.ExecuteDataset(viewConn, CommandType.StoredProcedure, fstrSpName)
            ' switch status id to status name for display
            Dim count As Integer
            Dim statXmlTr As New clsXml
            Dim strPanm As String
            Dim strPaty As String
            statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            For count = 0 To ds.Tables(0).Rows.Count - 1
                Dim statusid As String = ds.Tables(0).Rows(count).Item(3).ToString 'item(3) is status
                Dim technity As String = ds.Tables(0).Rows(count).Item(5).ToString
                strPanm = statXmlTr.GetLabelName("StatusMessage", statusid)
                strPaty = statXmlTr.GetLabelName("StatusMessage", technity)
                ds.Tables(0).Rows(count).Item(3) = strPanm
                ds.Tables(0).Rows(count).Item(5) = strPaty
            Next
            Return ds

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.VIEWS, "clsTechnician.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.VIEWS, "clsTechnician.vb")
        End Try
    End Function
#End Region

#Region "Insert"

    Public Function Insert() As Integer

        Dim insConnection As SqlConnection = Nothing
        Dim insConn As SqlConnection = Nothing
        insConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        insConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASTECH_Add"
        Try
            trans = insConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(27) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(insConn, fstrSpName)
            Param(0).Value = Trim(fstrTechnicianID)
            Param(1).Value = Trim(fstrTechnicianName)
            Param(2).Value = Trim(fstrAlternateName)
            Param(3).Value = Trim(fstrStatus)
            Param(4).Value = Trim(fstrNRIC)
            Param(5).Value = Trim(fstrTechnicianType)
            Param(6).Value = Trim(fstrAddress1)


            Param(7).Value = Trim(fstrAddress2)
            Param(8).Value = Trim(fstrPOCode)
            Param(9).Value = Trim(fstrAreaID)
            Param(10).Value = Trim(fstrCountryID)
            Param(11).Value = Trim(fstrStateID)

            Param(12).Value = Trim(fstrTelephone1)
            Param(13).Value = Trim(fstrTelephone2)
            Param(14).Value = Trim(fstrMobile)
            Param(15).Value = Trim(fstrFax)
            Param(16).Value = Trim(fstrMailingAddress1)
            Param(17).Value = Trim(fstrMailingAddress2)
            Param(18).Value = Trim(fstrPOCode2)
            Param(19).Value = Trim(fstrAreaID2)
            Param(20).Value = Trim(fstrCountryID2)

            Param(21).Value = Trim(fstrStateID2)
            Param(22).Value = Trim(fstrDrivingLicense)
            Param(23).Value = Trim(fstrServiceCenter)
            Param(24).Value = Trim(fintMaxJobday)


            Param(25).Value = Trim(fstrCreatedBy)
            Param(26).Value = Trim(fstrModifiedBy)


            'Param(23).Value = Convert.ToDateTime(Trim(fstrModifiedDate)) '最后修改者


            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(insConn, CommandType.StoredProcedure, fstrSpName, Param)

            ' insert audit log
            ' Dim WriteAuditLog As New clsLogFile()
            'Dim insData As String = Param(0).Value + "," + Param(1).Value + "," + Param(2).Value + "," + Param(3).Value + "," + Param(4).Value + "," + Param(5).Value + "," + Param(6).Value + "," + Param(7).Value + "," + Param(8).Value + "," + Param(9).Value + "," + Param(10).Value
            ' WriteAuditLog.AddLog("wang", "table name:mctr_fil", "Data:" + insData, WriteAuditLog.ADD)

            trans.Commit()
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.VIEWS, "clsTechnician.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.VIEWS, "clsTechnician.vb")
            Return -1
        End Try

    End Function

#End Region
#Region "Select Technician By ID"

    Public Function GetTechnicianDetailsByID() As SqlDataReader
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASTECH_SelByID"
        Try
            trans = sidConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(1) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstrTechnicianID)

            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            Return dr

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            'Return ErrorLog

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            'Return ErrorLog
        End Try
    End Function
#End Region
#Region "Update"

    Public Function Update() As Integer

        Dim UpdConnection As SqlConnection = Nothing
        Dim UpdConn As SqlConnection = Nothing
        UpdConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        UpdConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_MASTECH_Upd"
        Try
            trans = UpdConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(29) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(UpdConn, fstrSpName)
            Param(0).Value = Trim(fstrTechnicianID)
            Param(1).Value = Trim(fstrTechnicianName)
            Param(2).Value = Trim(fstrAlternateName)
            Param(3).Value = Trim(fstrStatus)
            Param(4).Value = Trim(fstrNRIC)
            Param(5).Value = Trim(fstrTechnicianType)
            Param(6).Value = Trim(fstrAddress1)


            Param(7).Value = Trim(fstrAddress2)
            Param(8).Value = Trim(fstrPOCode)
            Param(9).Value = Trim(fstrAreaID)
            Param(10).Value = Trim(fstrCountryID)
            Param(11).Value = Trim(fstrStateID)

            Param(12).Value = Trim(fstrTelephone1)
            Param(13).Value = Trim(fstrTelephone2)
            Param(14).Value = Trim(fstrMobile)
            Param(15).Value = Trim(fstrFax)
            Param(16).Value = Trim(fstrMailingAddress1)
            Param(17).Value = Trim(fstrMailingAddress2)
            Param(18).Value = Trim(fstrPOCode2)
            Param(19).Value = Trim(fstrAreaID2)
            Param(20).Value = Trim(fstrCountryID2)

            Param(21).Value = Trim(fstrStateID2)
            Param(22).Value = Trim(fstrDrivingLicense)
            Param(23).Value = Trim(fstrServiceCenter)
            Param(24).Value = Trim(fintMaxJobday)

            Param(25).Value = Trim(fstrModifiedBy)
            Param(26).Value = Trim(fstripadress)
            Param(27).Value = Trim(fstrsvc)
            'LLY 20160405
            'Param(28).Value = Trim(fstrHRID) commented by Tomas 20180606

            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(UpdConn, CommandType.StoredProcedure, fstrSpName, Param)
            ' insert audit log
            'Dim WriteAuditLog As New clsLogFile()
            'Dim insData As String = Param(0).Value + "," + Param(1).Value + "," + Param(2).Value + "," + Param(3).Value + "," + Param(4).Value + "," + Param(5).Value + "," + Param(6).Value + "," + Param(7).Value + "," + Param(8).Value + "," + Param(9).Value + "," + Param(10).Value
            'WriteAuditLog.AddLog("wang", "table name:mctr_fil", "Data:" + insData, WriteAuditLog.ADD)

            trans.Commit()
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "clsTechnician.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "clsTechnician.vb")
            Return -1
        End Try

    End Function

#End Region
#Region "Select Technician"

    Public Function GetTechnician() As DataSet

        '    Dim ds As DataSet = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
        Dim selConnection As SqlConnection = Nothing
        Dim selConn As SqlConnection = Nothing
        selConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASTECH_VIEW"
        Try
            trans = selConnection.BeginTransaction()

            'define search fields 
            Dim Param() As SqlParameter = New SqlParameter(8) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrTechnicianID)
            'Dim str As String = Param(0).Value
            Param(1).Value = Trim(fstrTechnicianName)
            Param(2).Value = Trim(fstrStatus)
            Param(3).Value = Trim(fstrctry)

            Param(4).Value = Trim(fstrcomp)
            Param(5).Value = Trim(fstrsvc)
            Param(6).Value = Trim(fstrrank)
            'Dim dr As SqlDataReader = SqlHelper.ExecuteReader(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            'Return dr

            Dim dst As DataSet = New DataSet()
            'Dim dst1 As DataSet = New DataSet()
            ''dst = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            'Dim sqda As New SqlDataAdapter("exec " + fstrSpName + " " + Param(0).Value + "," + Param(1).Value + "," + Param(2).Value, selConn)
            'sqda.Fill(dst1, "test")
            'Dim j As Integer
            'j = dst1.Tables("test").Rows.Count
            dst = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)

            ''''

            ' switch status id to status name for display
            Dim i As Integer
            i = dst.Tables(0).Rows.Count


            Dim count As Integer
            Dim statXmlTr As New clsXml
            Dim strPanm As String
            Dim strPaty As String
            statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            For count = 0 To dst.Tables(0).Rows.Count - 1
                Dim statusid As String = dst.Tables(0).Rows(count).Item(3).ToString 'item(3) is status
                Dim technity As String = dst.Tables(0).Rows(count).Item(5).ToString
                strPanm = statXmlTr.GetLabelName("StatusMessage", statusid)
                strPaty = statXmlTr.GetLabelName("StatusMessage", technity)
                dst.Tables(0).Rows(count).Item(3) = strPanm
                dst.Tables(0).Rows(count).Item(5) = strPaty
            Next
            Return dst

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsTechnician.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsTechnician.vb")
        End Try

    End Function
#End Region
#Region "confirm no duplicated id and name"
    Public Function GetDuplicatedTechnician() As Integer
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASTECH_IFDUP"
        Try
            trans = sidConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(1) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstrTechnicianID)
            'Param(1).Value = Trim(fstrNRIC)

            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            If dr.Read() Then
                dr.Close()
                Return -1
            End If
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsTechnician.vb")
            Return -1
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsTechnician.vb")
            Return -1
        Finally
            sidConnection.Close()
            sidConn.Close()
        End Try
    End Function
#End Region
#Region "select Technician by NRIC"
    Public Function GetNRICCount(ByVal fstrPrice1Name As String, ByVal fstrPrice2Name As String) As Integer

        Dim selConnection As SqlConnection = Nothing
        Dim selConn As SqlConnection = Nothing
        Dim trans As SqlTransaction = Nothing
        selConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        fstrSpName = "BB_MASTECH_SelByNRIC"
        Try
            trans = selConnection.BeginTransaction()
            Dim count As Integer
            Dim Param() As SqlParameter = New SqlParameter(1) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrPrice1Name)
            Param(1).Value = Trim(fstrPrice2Name)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            count = ds.Tables(0).Rows.Count
            Return count
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsTechnician.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsTechnician.vb")

        End Try
    End Function
#End Region
#Region "Select technician in rou"
    Public Function GetTelDel() As Integer
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_MASTECH_DEL"
        Try
            trans = sidConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(1) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(fstrTechnicianID)



            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            If dr.Read() Then
                dr.Close()
                Return -1
            End If
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsServiceCente.vb")
            Return -1
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsServiceCente.vb")
            Return -1
        Finally
            sidConnection.Close()
            sidConn.Close()
        End Try
    End Function
#End Region
#End Region
End Class
