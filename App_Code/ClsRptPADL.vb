Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web
Imports BusinessEntity
Imports SQLDataAccess


Public Class ClsRptPADL
#Region "Declaration"

    Private fstrSpName As String ' store procedure name
    Private fstrusername As String ' username
    Private fstruserid As String
    Private fstrminsvcid As String
    Private fstrmaxsvcid As String

    Private fstrmincustid As String
    Private fstrmaxcustid As String
    Private fstrmindate As System.DateTime
    Private fstrmaxdate As System.DateTime


    Private fstrminmodel As String

    Private fstrmaxmodel As String

    Private fstrminserialno As String

    Private fstrmaxserialno As String
    Private fstrminstat As String
    Private fstrmaxstat As String

    Private fstrminarea As String
    Private fstrmaxarea As String

    Private fstrrank As String ' rank

    Private fstrctry As String ' user country
    Private fstrcomp As String ' user company
    Private fstrsvc As String '  user svcid

#End Region

#Region "Properties"
    Public Property username() As String
        Get
            Return fstrusername
        End Get
        Set(ByVal Value As String)
            fstrusername = Value
        End Set
    End Property
    Public Property userid() As String
        Get
            Return fstruserid
        End Get
        Set(ByVal Value As String)
            fstruserid = Value
        End Set
    End Property
    Public Property rank() As String
        Get
            Return fstrrank
        End Get
        Set(ByVal Value As String)
            fstrrank = Value
        End Set
    End Property
    Public Property ctryid() As String
        Get
            Return fstrctry
        End Get
        Set(ByVal Value As String)
            fstrctry = Value
        End Set
    End Property
    Public Property compid() As String
        Get
            Return fstrcomp
        End Get
        Set(ByVal Value As String)
            fstrcomp = Value
        End Set
    End Property
    Public Property svcid() As String
        Get
            Return fstrsvc
        End Get
        Set(ByVal Value As String)
            fstrsvc = Value
        End Set
    End Property


    Public Property Minsvcid() As String
        Get
            Return fstrminsvcid
        End Get
        Set(ByVal Value As String)
            fstrminsvcid = Value
        End Set
    End Property

    Public Property Maxsvcid() As String
        Get
            Return fstrmaxsvcid
        End Get
        Set(ByVal Value As String)
            fstrmaxsvcid = Value
        End Set
    End Property

    Public Property Mincustid() As String
        Get
            Return fstrmincustid
        End Get
        Set(ByVal Value As String)
            fstrmincustid = Value
        End Set
    End Property

    Public Property Maxcustid() As String
        Get
            Return fstrmaxcustid
        End Get
        Set(ByVal Value As String)
            fstrmaxcustid = Value
        End Set
    End Property

    Public Property Mindate() As String
        Get
            Return fstrmindate
        End Get
        Set(ByVal Value As String)
            fstrmindate = Value
        End Set
    End Property

    Public Property Maxdate() As String
        Get
            Return fstrmaxdate
        End Get
        Set(ByVal Value As String)
            fstrmaxdate = Value
        End Set
    End Property

    Public Property Minstat() As String
        Get
            Return fstrminstat
        End Get
        Set(ByVal Value As String)
            fstrminstat = Value
        End Set
    End Property

    Public Property Maxstat() As String
        Get
            Return fstrmaxstat
        End Get
        Set(ByVal Value As String)
            fstrmaxstat = Value
        End Set
    End Property
    Public Property Minserialno() As String
        Get
            Return fstrminserialno
        End Get
        Set(ByVal Value As String)
            fstrminserialno = Value
        End Set
    End Property
    Public Property Maxserialno() As String
        Get
            Return fstrmaxserialno
        End Get
        Set(ByVal Value As String)
            fstrmaxserialno = Value
        End Set
    End Property
    Public Property Minmodel() As String
        Get
            Return fstrminmodel
        End Get
        Set(ByVal Value As String)
            fstrminmodel = Value
        End Set
    End Property

    Public Property Maxmodel() As String
        Get
            Return fstrmaxmodel
        End Get
        Set(ByVal Value As String)
            fstrmaxmodel = Value
        End Set
    End Property
    Public Property Minarea() As String
        Get
            Return fstrminarea
        End Get
        Set(ByVal Value As String)
            fstrminarea = Value
        End Set
    End Property
    Public Property Maxarea() As String
        Get
            Return fstrmaxarea
        End Get
        Set(ByVal Value As String)
            fstrmaxarea = Value
        End Set
    End Property

#End Region
#Region "Methods"
#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)

        connection.Open()

        Return connection
    End Function
#End Region
#Region "Select contact"

    Public Function GetRptpadl() As DataSet

        Dim selConnection As SqlConnection = Nothing
        Dim selConn As SqlConnection = Nothing
        selConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_RPTPADL_VIEW"
        Try
            trans = selConnection.BeginTransaction()

            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(19) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrminsvcid)
            Param(1).Value = Trim(fstrmaxsvcid)
            Param(2).Value = Trim(fstrmincustid)
            Param(3).Value = Trim(fstrmaxcustid)
            Param(4).Value = Trim(fstrmindate)
            Param(5).Value = Trim(fstrmaxdate)
            Param(6).Value = Trim(fstrminmodel)
            Param(7).Value = Trim(fstrmaxmodel)
            Param(8).Value = Trim(fstrminserialno)
            Param(9).Value = Trim(fstrmaxserialno)
            Param(10).Value = Trim(fstrminstat)
            Param(11).Value = Trim(fstrmaxstat)
            Param(12).Value = Trim(fstrminarea)
            Param(13).Value = Trim(fstrmaxarea)
            Param(14).Value = Trim(fstruserid)
            Param(15).Value = Trim(fstrctry)
            Param(16).Value = Trim(fstrcomp)
            Param(17).Value = Trim(fstrsvc)
            Param(18).Value = Trim(fstrrank)
            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)

            Return ds

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "ClsRptPADL.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "ClsRptPADL.vb")
        Finally
            selConnection.Close()
            selConn.Close()
        End Try

    End Function
#End Region
#End Region
End Class
