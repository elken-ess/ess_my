Imports System.Data.SqlClient
Imports SQLDataAccess
Imports System.Configuration
Imports BusinessEntity
Imports System.Data

'=================================================
'Description Apponitment Schedule
'Author: zqw
'=================================================
Public Class clsAppointment
#Region "Declaration"
    Private fstrSpName As String ' store procedure name
    Private fstrTransType As String
    Private fstrTransNo As String
    Private fstrServID As String
    Private fstrApptStartDate As String
    Private fstrApptEndDate As String
    Private fstrLogInStartDate As String
    Private fstrLogInEndDate As String
    Private fstrApptStatus As String
    Private fstrCustomID As String
    Private fstrCustomPf As String
    Private fstrModeID As String
    Private fstrSerialNo As String
    Private fstrApptDate As String
    Private fstrZoneID As String
    Private fstrPICName As String
    Private fstrPICContact As String
    Private fstrApptType As String
    Private fstrTCHID As String
    Private fstrResReason As String
    Private fstrApptNotes As String
    Private fstrROID As String
    Private fstrApptSTime As String
    Private fstrApptETime As String
    Private fstrAreaID As String
    Private fstrIPAddress As String
    Private fstrUserSvcID As String
    Private fstrCustomerName As String
    Private fstrTelNo As String
    Private fstrServiceType As String
    Private fstrCrID As String

    Private fintOtherAppointmentCount As Integer
    Private fintFixedAppointmentCount As Integer

    Private fstrErrorMessage As String

    Dim selConnection As SqlConnection = Nothing
    Dim selConn As SqlConnection = Nothing
    Dim selConnSYSMGR As SqlConnection = Nothing

    Dim trans As SqlTransaction = Nothing

    ' Added by Ryan Estandarte 20 Sept 2012
    Private _isInbound As Boolean

    ' Added by Ryan Estandarte 16 Oct 2012
    Private _isInboundSearch As String

    ' Added by JC ReasonObjection 31 May 2016
    Private _ReasonObjection As String

    ' Added by lyann NatureOfFeedback 27 Dec 2017
    Private _NatureFeedback As String

    ' Added by Tomas 20180613
    Private _RMSNo As String
#End Region
#Region "Properties"

    Public Property FixedAppointmentCount() As String
        Get
            Return fintFixedAppointmentCount

        End Get
        Set(ByVal value As String)
            fintFixedAppointmentCount = value
        End Set
    End Property

    Public Property OtherAppointmentCount() As String
        Get
            Return fintOtherAppointmentCount

        End Get
        Set(ByVal value As String)
            fintOtherAppointmentCount = value
        End Set
    End Property

    Public Property ErrorMessage() As String
        Get
            Return fstrErrorMessage

        End Get
        Set(ByVal value As String)
            fstrErrorMessage = value
        End Set
    End Property

    Public WriteOnly Property CrID() As String

        Set(ByVal Value As String)
            fstrCrID = Value
        End Set
    End Property

    Public WriteOnly Property CustomerName() As String

        Set(ByVal Value As String)
            fstrCustomerName = Value
        End Set
    End Property

    Public WriteOnly Property TelNo() As String

        Set(ByVal Value As String)
            fstrTelNo = Value
        End Set
    End Property

    Public WriteOnly Property ServiceType() As String

        Set(ByVal Value As String)
            fstrServiceType = Value
        End Set
    End Property


    Public WriteOnly Property TransactionType() As String
        'Get
        '    Return fstrTransType
        'End Get
        Set(ByVal Value As String)
            fstrTransType = Value
        End Set
    End Property
    Public WriteOnly Property TransactionNo() As String
        'Get
        '    Return fstrTransNo
        'End Get
        Set(ByVal Value As String)
            fstrTransNo = Value
        End Set
    End Property
    Public WriteOnly Property ServerCenterID() As String
        'Get
        '    Return fstrServID
        'End Get
        Set(ByVal Value As String)
            fstrServID = Value
        End Set
    End Property
    Public WriteOnly Property AppointmentStartDate() As String
        'Get
        '    Return fstrApptStartDate
        'End Get
        Set(ByVal Value As String)
            fstrApptStartDate = Value
        End Set
    End Property

    Public WriteOnly Property AppointmentEndDate() As String
        'Get
        '    Return fstrApptEndDate
        'End Get
        Set(ByVal Value As String)
            fstrApptEndDate = Value
        End Set
    End Property

    Public WriteOnly Property LogInStartDate() As String
        Set(ByVal Value As String)
            fstrLogInStartDate = Value
        End Set
    End Property

    Public WriteOnly Property LogInEndDate() As String
        Set(ByVal Value As String)
            fstrLogInEndDate = Value
        End Set
    End Property

    Public WriteOnly Property AppointmentStatus() As String
        'Get
        '    Return fstrApptStatus
        'End Get
        Set(ByVal Value As String)
            fstrApptStatus = Value
        End Set
    End Property
    Public WriteOnly Property CustomerID() As String
        'Get
        '    Return fstrCustomID
        'End Get
        Set(ByVal Value As String)
            fstrCustomID = Value
        End Set
    End Property
    Public WriteOnly Property CustomerPrifx() As String
        'Get
        '    Return fstrCustomPf
        'End Get
        Set(ByVal Value As String)
            fstrCustomPf = Value
        End Set
    End Property
    Public WriteOnly Property ModeID() As String
        'Get
        '    Return fstrModeID
        'End Get
        Set(ByVal Value As String)
            fstrModeID = Value
        End Set
    End Property
    Public WriteOnly Property SerialNo() As String
        'Get
        '    Return fstrSerialNo
        'End Get
        Set(ByVal Value As String)
            fstrSerialNo = Value
        End Set
    End Property
    Public WriteOnly Property AppointmentDate() As String
        'Get
        '    Return fstrApptDate
        'End Get
        Set(ByVal Value As String)
            fstrApptDate = Value
        End Set
    End Property
    Public WriteOnly Property ZoneID() As String
        'Get
        '    Return fstrZoneID
        'End Get
        Set(ByVal Value As String)
            fstrZoneID = Value
        End Set
    End Property
    Public WriteOnly Property PICName() As String
        'Get
        '    Return fstrPICName
        'End Get
        Set(ByVal Value As String)
            fstrPICName = Value
        End Set
    End Property
    Public WriteOnly Property PICContact() As String
        'Get
        '    Return fstrPICContact
        'End Get
        Set(ByVal Value As String)
            fstrPICContact = Value
        End Set
    End Property
    Public WriteOnly Property AppointmentType() As String
        'Get
        '    Return fstrApptType
        'End Get
        Set(ByVal Value As String)
            fstrApptType = Value
        End Set
    End Property
    Public WriteOnly Property AppointmentNotes() As String
        'Get
        '    Return fstrApptNotes
        'End Get
        Set(ByVal Value As String)
            fstrApptNotes = Value
        End Set
    End Property
    Public WriteOnly Property TechnicanID() As String
        'Get
        '    Return fstrTCHID
        'End Get
        Set(ByVal Value As String)
            fstrTCHID = Value
        End Set
    End Property
    Public WriteOnly Property ReschduleReason() As String
        'Get
        '    Return fstrResReason
        'End Get
        Set(ByVal Value As String)
            fstrResReason = Value
        End Set
    End Property
    Public WriteOnly Property ROID() As String
        'Get
        '    Return fstrROID
        'End Get
        Set(ByVal Value As String)
            fstrROID = Value
        End Set
    End Property
    Public WriteOnly Property AppointmentStartTime() As String
        'Get
        '    Return fstrApptSTime
        'End Get
        Set(ByVal Value As String)
            fstrApptSTime = Value
        End Set
    End Property
    Public WriteOnly Property AppointmentEndTime() As String
        'Get
        '    Return fstrApptETime
        'End Get
        Set(ByVal Value As String)
            fstrApptETime = Value
        End Set
    End Property
    Public WriteOnly Property CustomAreaID() As String
        'Get
        '    Return fstrAreaID
        'End Get
        Set(ByVal Value As String)
            fstrAreaID = Value
        End Set
    End Property
    Public WriteOnly Property IpAddress() As String
        'Get
        '    Return fstrIPAddress
        'End Get
        Set(ByVal Value As String)
            fstrIPAddress = Value
        End Set
    End Property
    Public WriteOnly Property UserServCenterID() As String
        'Get
        '    Return fstrUserSvcID
        'End Get
        Set(ByVal Value As String)
            fstrUserSvcID = Value
        End Set
    End Property

    ' Added by Ryan Estandarte 20 Sept 2012
    Public Property IsInbound() As Boolean
        Get
            Return _isInbound
        End Get
        Set(ByVal value As Boolean)
            _isInbound = value
        End Set
    End Property

    ' Added by Ryan Estandarte 16 Oct 2012
    Public Property IsInboundSearch() As String
        Get
            Return _isInboundSearch
        End Get
        Set(ByVal value As String)
            _isInboundSearch = value
        End Set
    End Property

    ' Added by JC ReasonObjection 31 May 2016
    Public Property ReasonObjection() As String
        Get
            Return _ReasonObjection
        End Get
        Set(ByVal value As String)
            _ReasonObjection = value
        End Set
    End Property

    ' Added by lyann NatureOfFeedback 27 Dec 2017
    Public Property NatureOfFeedback() As String
        Get
            Return _NatureFeedback
        End Get
        Set(ByVal value As String)
            _NatureFeedback = value
        End Set
    End Property
    ' Added by Tomas 20180613
    Public Property RMSNo() As String
        Get
            Return _RMSNo
        End Get
        Set(ByVal value As String)
            _RMSNo = value
        End Set
    End Property
#End Region

#Region "Methods"
#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)
        connection.Open()
        Return connection
    End Function
#End Region
#Region "Methods help" '??public???????
    Private Sub BeginMethod()
        selConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        selConnSYSMGR = GetConnection(ConfigurationSettings.AppSettings("SYSMGRConnectionString"))
        'selConnSYSMGR = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        trans = selConnection.BeginTransaction()
    End Sub
    Private Sub DealException(ByVal ex As Exception)
        trans.Rollback()
        Dim ErrorLog As ArrayList = New ArrayList
        ErrorLog.Add("Error").ToString()

        If ex.Message.Trim <> "" Then
            fstrErrorMessage = ex.Message.Trim
        End If
        ErrorLog.Add(ex.Message).ToString()
        Dim WriteErrLog As New clsLogFile()
        WriteErrLog.ErrorLog(fstrCrID, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsAppointment.vb")
    End Sub
    Private Sub EndMethod()
        selConnection.Dispose()
        selConn.Dispose()
    End Sub
#End Region
    '????????????????appointment records
#Region "GetAppointmentSchedule"
    Public Function GetAppointmentSchedule(ByVal strUserID As String) As DataSet

        fstrSpName = "BB_FNCAPPS_View"
        Try
            BeginMethod()
            ' define search fields 
            ' Modified by Ryan Estandarte 20 Sept 2012
            'Dim Param() As SqlParameter = New SqlParameter(15) {}
            Dim Param() As SqlParameter = New SqlParameter(19) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(strUserID)
            Param(1).Value = Trim(fstrTransType)
            Param(2).Value = Trim(fstrTransNo)
            Param(3).Value = Trim(fstrServID)
            Param(4).Value = Trim(fstrApptStartDate)
            Param(5).Value = Trim(fstrApptEndDate)
            Param(6).Value = Trim(fstrApptStatus)
            Param(7).Value = Trim(fstrCustomerName)
            Param(8).Value = Trim(fstrSerialNo)
            Param(9).Value = Trim(fstrTelNo)
            Param(10).Value = Trim(fstrCrID)
            Param(11).Value = Trim(fstrCustomID)
            Param(12).Value = Trim(fstrApptType)
            Param(13).Value = Trim(fstrTCHID)
            Param(14).Value = Trim(fstrAreaID)
            Param(15).Value = Trim(fstrLogInStartDate)
            Param(16).Value = Trim(fstrLogInEndDate)
            '' Added by Ryan Estandarte 20 Sept 2012
            'Param(17).Value = _isInbound
            ' Added by Ryan Estandarte 04 Jan 2013
            Param(17).DbType = DbType.String
            Param(17).Value = _isInboundSearch
            'TO DO Wey Edit SP - BB_FNCAPPS_View
            Param(18).Value = Trim(RMSNo)

            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            ' switch status id to status name for display

            ds.Tables(0).Columns.Remove("APTDT")
            'Dim count As Integer
            'Dim statXmlTr As New clsXml
            'Dim strPanm As String
            'statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            'For count = 0 To ds.Tables(0).Rows.Count - 1
            '    Dim statusid As String = ds.Tables(0).Rows(count).Item("FAPT_STAT").ToString 'item(4) is status
            '    strPanm = statXmlTr.GetLabelName("StatusMessage", statusid)
            '    ds.Tables(0).Rows(count).Item("FAPT_STAT") = strPanm
            'Next
            Return ds
        Catch ex As Exception
            DealException(ex)
            Return Nothing
        Finally
            EndMethod()
        End Try
    End Function
#End Region
    '??Tranaction Type ?Transaction No ?????appointment records
#Region "Select Appointment record by ID"
    Public Function GetAppointmentDetailsByID() As DataSet
        fstrSpName = "BB_FNCAPPS_SelByID"
        Try
            Dim CoverEntity As New clsCommonClass
            BeginMethod()

            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrTransType)
            Param(1).Value = Trim(fstrTransNo)

            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            ds.Tables(0).TableName = "FAPT_FIL"
            ds.Tables(1).TableName = "MADR_FIL"
            ds.Tables(2).TableName = "MCUS_FIL"
            ds.Tables(3).TableName = "MROU_FIL"
            'ds.Tables(4).TableName = "FCU1_FIL"
            'ds.Tables(4).TableName = "RMS_FIL"
            If ds.Tables("FAPT_FIL").Rows.Count = 1 Then
                Dim str As String = ds.Tables("FAPT_FIL").Rows(0).Item("FAPT_TELNO").ToString()
                ds.Tables("FAPT_FIL").Rows(0).Item("FAPT_TELNO") = CoverEntity.passconverttel(str)
            End If

            Return ds
        Catch ex As Exception
            DealException(ex)
            Return Nothing
        Finally
            EndMethod()
        End Try
    End Function
#End Region

    '??CustomerID?CustomerPrifx??????????
#Region "Select DropDownList Data by CustomerInfo"
    Public Function GetDropDownListData(ByVal UserID As String) As DataSet
        fstrSpName = "BB_FNCAPPS_SelByCustomInfo"
        Try
            Dim CoverEntity As New clsCommonClass
            BeginMethod()

            Dim Param() As SqlParameter = New SqlParameter(4) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrCustomID)
            Param(1).Value = Trim(fstrCustomPf)
            Param(2).Value = Trim(UserID)
            Param(3).Value = Trim(fstrServiceType)

            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            ds.Tables(0).TableName = "ROINFO"
            ds.Tables(1).TableName = "MSV1_FIL"
            ds.Tables(2).TableName = "MADR_FIL"
            ds.Tables(3).TableName = "MCUS_FIL"
            ds.Tables(4).TableName = "MTEL_FIL"
            Dim i As Integer

            For i = 0 To ds.Tables("MTEL_FIL").Rows.Count - 1
                Dim str As String = ds.Tables("MTEL_FIL").Rows(0).Item("MTEL_TELNO").ToString().Trim()
                ds.Tables("MTEL_FIL").Rows(0).Item("MTEL_TELNO") = CoverEntity.passconverttel(str)
            Next
            Return ds
        Catch ex As Exception
            DealException(ex)
            Return Nothing
        Finally
            EndMethod()
        End Try
    End Function
#End Region
    '??????ServerCenterID
#Region "Select ServerCenterID by UserID"
    Public Function GetSVCIDByUserID(ByVal UserID As String) As DataSet
        fstrSpName = "BB_FNCAPPS_SelSVCIDByUserID"
        Try
            BeginMethod()

            Dim Param() As SqlParameter = New SqlParameter(1) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(UserID)

            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            ds.Tables(0).TableName = "ServerCenterID"
            Return ds
        Catch ex As Exception
            DealException(ex)
            Return Nothing
        Finally
            EndMethod()
        End Try
    End Function
#End Region


#Region "Select user id by service center"
    Public Function GetCrIDBySvcID(ByVal ServiceCenterID As String) As DataSet
        fstrSpName = "BB_FNCAPPS_CrIDBySvcID"
        Try
            BeginMethod()

            Dim Param() As SqlParameter = New SqlParameter(1) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(ServiceCenterID)

            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)

            Return ds
        Catch ex As Exception
            DealException(ex)
            Return Nothing
        Finally
            EndMethod()
        End Try
    End Function
#End Region
    '??ServerCenterID???????
#Region "Select DropDownList Data by ServCenterID"
    Public Function GetTCHINFODrpListData() As DataSet
        fstrSpName = "BB_FNCAPPS_SelTCHBySVCID"
        Try
            BeginMethod()

            Dim Param() As SqlParameter = New SqlParameter() {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrServID)

            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            ds.Tables(0).TableName = "TCHINFO"
            Return ds
        Catch ex As Exception
            DealException(ex)
            Return Nothing
        Finally
            EndMethod()
        End Try
    End Function
#End Region

    '??Modle ID?SerialNo??????????????????
#Region "Select Data by ROInfo"
    Public Function GetROAndContractInfo() As DataSet
        fstrSpName = "BB_FNCAPPS_SelByROInfo"
        Try
            BeginMethod()

            Dim Param() As SqlParameter = New SqlParameter(3) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrModeID)
            Param(1).Value = Trim(fstrSerialNo)
            Param(2).Value = Trim(fstrROID)

            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            ds.Tables(0).TableName = "MROU_FIL"
            'ds.Tables(1).TableName = "FCU1_FIL"
            Return ds
        Catch ex As Exception
            DealException(ex)
            Return Nothing
        Finally
            EndMethod()
        End Try
    End Function
#End Region
    '??ServerCenterID ,Apointment Date,Zone ID ??Check Schedule
#Region "Select Scheck AppointmentSchedule"
    Public Function GetCHKSchedule() As DataSet
        fstrSpName = "BB_FNCAPPS_SelCHKSchedule"
        Try
            BeginMethod()
            Dim Param() As SqlParameter = New SqlParameter(3) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrServID)
            Param(1).Value = Trim(fstrApptDate)
            Param(2).Value = Trim(fstrZoneID)

            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)

            Dim count As Integer = 0
            Dim temp As String
            ds.Tables(0).TableName = "CheckSchedule"
            For count = 0 To ds.Tables("CheckSchedule").Rows.Count - 1
                ds.Tables("CheckSchedule").Rows(count).Item("No") = count + 1
                'temp = ds.Tables("CheckSchedule").Rows(count).Item("STime").ToString()
                'temp = temp.Insert(2, ":")
                'ds.Tables("CheckSchedule").Rows(count).Item("STime") = temp
                'temp = ds.Tables("CheckSchedule").Rows(count).Item("ETime").ToString()
                'temp = temp.Insert(2, ":")
                'ds.Tables("CheckSchedule").Rows(count).Item("ETime") = temp
            Next
            ds.AcceptChanges()
            Return ds
        Catch ex As Exception
            DealException(ex)
            Return Nothing
        Finally
            EndMethod()
        End Try
    End Function
#End Region
    '???????????????
#Region "Select Scheck AppointmentHistoryDetail"
    Public Function GetApptHSTDetail() As DataSet
        fstrSpName = "BB_FNCAPPS_SelApptHSTDetial"
        Try
            BeginMethod()
            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrROID)
            Param(1).Value = Trim(fstrCustomID)
            Param(2).Value = Trim(fstrCustomPf)

            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            Return ds
        Catch ex As Exception
            DealException(ex)
            Return Nothing
        Finally
            EndMethod()
        End Try
    End Function
#End Region

    '???????????
#Region "Select  ServerHistory"
    Public Function GetServerHistory() As DataSet
        fstrSpName = "BB_FNCAPPS_SelServerHistory"
        Try
            BeginMethod()
            Dim Param() As SqlParameter = New SqlParameter(3) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrROID)
            Param(1).Value = Trim(fstrCustomID)
            Param(2).Value = Trim(fstrCustomPf)

            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            Return ds
        Catch ex As Exception
            DealException(ex)
            Return Nothing
        Finally
            EndMethod()
        End Try
    End Function
#End Region
    '???????????
#Region "Select  ContactHistory"
    Public Function GetContractHistory() As DataSet
        fstrSpName = "BB_FNCAPPS_SelContractHistory"
        Try
            BeginMethod()
            Dim Param() As SqlParameter = New SqlParameter() {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrROID)
            Param(1).Value = Trim(fstrCustomID)
            Param(2).Value = Trim(fstrCustomPf)

            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            ' switch status id to status name for display
            Dim count As Integer
            Dim statXmlTr As New clsXml
            Dim strPanm As String
            statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            For count = 0 To ds.Tables(0).Rows.Count - 1
                Dim statusid As String = ds.Tables(0).Rows(count).Item("FCU1_STAT").ToString.Trim() 'item(6) is status
                strPanm = statXmlTr.GetLabelName("StatusMessage", statusid)
                ds.Tables(0).Rows(count).Item("FCU1_STAT") = strPanm
            Next
            Return ds
        Catch ex As Exception
            DealException(ex)
            Return Nothing
        Finally
            EndMethod()
        End Try
    End Function
#End Region
    '?? AreaID???????ZoneID
#Region "Select ZoneID by AppointmentDate and AreaID,ServCenterID"
    Public Function GetZoneID() As DataSet
        fstrSpName = "BB_FNCAPPS_SelZoneID"
        Try
            BeginMethod()
            Dim Param() As SqlParameter = New SqlParameter(3) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrAreaID)
            Param(1).Value = Trim(fstrApptDate)
            Param(2).Value = Trim(fstrServID)
            Param(3).Value = Trim(fstrTransType)
            Param(4).Value = Trim(fstrTransNo)

            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            ds.Tables(0).TableName = "FZN2_FIL"
            Return ds
        Catch ex As Exception
            DealException(ex)
            Return Nothing
        Finally
            EndMethod()
        End Try
    End Function
#End Region

#Region "Select ZoneID by AppointmentDate and AreaID,ServCenterID"
    Public Function GetZoneIDRS() As DataSet
        fstrSpName = "BB_FNCAPPS_SelZoneID_RS"
        Try
            BeginMethod()
            Dim Param() As SqlParameter = New SqlParameter(3) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrAreaID)
            Param(1).Value = Trim(fstrApptDate)
            Param(2).Value = Trim(fstrServID)
            Param(3).Value = Trim(fstrTransType)
            Param(4).Value = Trim(fstrTransNo)

            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            ds.Tables(0).TableName = "FZN2_RS_FIL"
            Return ds
        Catch ex As Exception
            DealException(ex)
            Return Nothing
        Finally
            EndMethod()
        End Try
    End Function
#End Region

    '???????????Appointment ?????
#Region "Update AppointmentSchedule"
    Public Function UpdateAppointment(ByVal UserID As String, ByVal IsStatusChanged As String) As Boolean
        fstrSpName = "BB_FNCAPPS_Upd"
        Try
            Dim CoverEntity As New clsCommonClass
            BeginMethod()
            ' Modified by Ryan Estandarte 20 Sept 2012
            'Dim Param() As SqlParameter = New SqlParameter(22) {}
            Dim Param() As SqlParameter = New SqlParameter(24) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrTransType).ToUpper()
            Param(1).Value = Trim(fstrTransNo).ToUpper()
            Param(2).Value = Trim(fstrApptDate).ToUpper()
            Param(3).Value = Trim(fstrCustomID).ToUpper()
            Param(4).Value = Trim(fstrCustomPf).ToUpper()
            Param(5).Value = Trim(fstrROID).ToUpper()
            Param(6).Value = Trim(fstrPICName).ToUpper()
            Param(7).Value = CoverEntity.telconvertpass(Trim(fstrPICContact).ToUpper())
            Param(8).Value = Trim(fstrApptType).ToUpper()
            Param(9).Value = Trim(fstrApptSTime).ToUpper()
            Param(10).Value = Trim(fstrApptETime).ToUpper()
            Param(11).Value = Trim(fstrTCHID).ToUpper()
            Param(12).Value = Trim(fstrResReason).ToUpper()
            Param(13).Value = Trim(fstrApptNotes).ToUpper()
            Param(14).Value = Trim(fstrApptStatus)
            Param(15).Value = Trim(fstrServID).ToUpper()
            Param(16).Value = Trim(fstrZoneID).ToUpper()
            Param(17).Value = Trim(UserID).ToUpper()
            Param(18).Value = Trim(IsStatusChanged)
            Param(19).Value = Trim(fstrIPAddress)
            Param(20).Value = Trim(fstrUserSvcID)
            Param(21).Value = Trim(fstrCrID)
            ' Added by Ryan Estandarte 20 Sept 2012
            Param(22).Value = Trim(_isInbound)
            ' Added by JC ReasonObjection 31 May 2016
            Param(23).Value = Trim(_ReasonObjection)
            ' Added by lyann NatureOfFeedback 27 Dec 2017
            Param(24).Value = Trim(_NatureFeedback)
            Param(25).Value = Trim(_RMSNo)

            SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            'SqlHelper.ExecuteNonQuery(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            Return True
        Catch ex As Exception

            DealException(ex)
            Return False
        Finally
            EndMethod()
        End Try
    End Function
#End Region
    '????
#Region "Add AppointmentSchedule"
    Public Function AddAppointment(ByVal UserID As String) As String
        fstrSpName = "BB_FNCAPPS_Add"
        Try
            Dim CoverEntity As New clsCommonClass
            BeginMethod()
            ' Modified by Ryan Estanarte 20 Sept 2012
            'Dim Param() As SqlParameter = New SqlParameter(18) {}
            Dim Param() As SqlParameter = New SqlParameter(21) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrTransType).ToUpper()
            'Param(1).Value = Trim(fstrTransNo).ToUpper() edit june 27 transNo is return value
            Param(2).Value = Trim(fstrApptDate).ToUpper()
            Param(3).Value = Trim(fstrCustomID).ToUpper()
            Param(4).Value = Trim(fstrCustomPf).ToUpper()
            Param(5).Value = Trim(fstrROID).ToUpper()
            Param(6).Value = Trim(fstrPICName).ToUpper()
            Param(7).Value = CoverEntity.telconvertpass(Trim(fstrPICContact).ToUpper())
            Param(8).Value = Trim(fstrApptType).ToUpper()
            Param(9).Value = Trim(fstrApptSTime).ToUpper()
            Param(10).Value = Trim(fstrApptETime).ToUpper()
            Param(11).Value = Trim(fstrTCHID).ToUpper()
            Param(12).Value = Trim(fstrApptNotes).ToUpper()
            Param(13).Value = Trim(fstrApptStatus)
            Param(14).Value = Trim(fstrServID).ToUpper()
            Param(15).Value = Trim(fstrZoneID).ToUpper()
            Param(16).Value = Trim(UserID).ToUpper()
            Param(17).Value = Trim(fstrCrID).ToUpper()
            ' Added by Ryan Estandarte 20 Sept 2012
            Param(18).Value = _isInbound
            Param(19).Value = Trim(_ReasonObjection)
            ' Added by lyann NatureOfFeedback 27 Dec 2017
            Param(20).Value = Trim(_NatureFeedback)
            Param(21).Value = Trim(RMSNo)

            SqlHelper.ExecuteNonQuery(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            Return Param(1).Value
        Catch ex As Exception
            DealException(ex)
            Return ""
        Finally
            EndMethod()
        End Try
    End Function
#End Region
    '?????????
#Region "Validator"
    Public Function Validator(ByVal IsModify As String) As Array
        fstrSpName = "BB_FNCAPPS_APPTNUMValidator"
        Try
            BeginMethod()
            Dim Param() As SqlParameter = New SqlParameter(10) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrApptDate)
            Param(1).Value = Trim(fstrApptSTime)
            Param(2).Value = Trim(fstrZoneID)
            'Param(2).Value = Trim(fstrTCHID)
            Param(3).Value = Trim(fstrServID)
            Param(4).Value = Trim(fstrCustomID)
            Param(5).Value = Trim(fstrCustomPf)
            Param(6).Value = Trim(IsModify)
            Param(7).Value = Trim(fstrTransType)
            Param(8).Value = Trim(fstrTransNo)
            Param(9).Value = Trim(fstrROID)

            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            Dim ValidatorArray As Array = Array.CreateInstance(GetType(Boolean), 7)
            ValidatorArray(0) = (Convert.ToInt32(ds.Tables(0).Rows(0).Item(0)) > 7)  'maximum prefix is 8
            ValidatorArray(1) = (Convert.ToInt32(ds.Tables(1).Rows(0).Item(0)) > 0)  'another customers already occupy a particular timeslot
            ValidatorArray(2) = (Convert.ToInt32(ds.Tables(2).Rows(0).Item(0)) > 4)  '5 appointments for a same technician with different timeslot.
            ValidatorArray(3) = (Convert.ToInt32(ds.Tables(3).Rows(0).Item(0)) > 2)  '3 appointments for a same technician with timeslot is 'other'
            ValidatorArray(4) = (Convert.ToInt32(ds.Tables(4).Rows(0).Item(0)) > 0)  'the customer already have an appointment at that day
            ValidatorArray(5) = (Convert.ToInt32(ds.Tables(5).Rows(0).Item(0)) <> 0)
            ValidatorArray(6) = (Convert.ToInt32(ds.Tables(6).Rows(0).Item(0)) > 0)


            fintFixedAppointmentCount = Convert.ToInt32(ds.Tables(2).Rows(0).Item(0))
            fintOtherAppointmentCount = Convert.ToInt32(ds.Tables(3).Rows(0).Item(0))


            Return ValidatorArray
        Catch ex As Exception
            DealException(ex)
            Return Nothing
        Finally
            EndMethod()
        End Try
    End Function
    Public Function SVCTValidator() As Boolean
        fstrSpName = "BB_FNCAPPS_SVCTValidator"
        Try
            BeginMethod()
            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrROID)
            Param(1).Value = Trim(fstrApptType)

            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            Return (Convert.ToInt32(ds.Tables(0).Rows(0).Item(0)) > 0)
        Catch ex As Exception
            DealException(ex)
            Return False
        Finally
            EndMethod()
        End Try
    End Function
#End Region
#Region "Select Data by ROInfo"
    Public Function GetContractInfo() As DataSet
        fstrSpName = "BB_FNCAPPS_SelContract"
        Try
            BeginMethod()

            Dim Param() As SqlParameter = New SqlParameter(4) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrROID)
            Param(1).Value = Trim(fstrCustomID)
            Param(2).Value = Trim(fstrCustomPf)
            Param(3).Value = Trim(fstrApptDate)

            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            ds.Tables(0).TableName = "FCU1_FIL"
            Return ds
        Catch ex As Exception
            DealException(ex)
            Return Nothing
        Finally
            EndMethod()
        End Try
    End Function
#End Region

#Region "GetLastApptDate"
    Public Function GetLastApptDate(ByVal CreateDate As Date) As String

        fstrSpName = "BB_FNCAPPS_SelLastApptDate"
        Try
            BeginMethod()
            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(4) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrROID)
            Param(1).Value = Trim(fstrCustomID)
            Param(2).Value = Trim(fstrCustomPf)
            Param(3).Value = CreateDate

            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            'The previous contract No is displayed the contract no that had been 
            'done previously and not display the contract no that had been done 
            'currenctly. If the customer is the first time to create the contract 
            'service record then the previous contract no is blank.
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(ds.Tables(0).Rows.Count - 1).Item("FAPT_APTDT").ToString()
            End If
            Return ""
        Catch ex As Exception
            DealException(ex)
            Return Nothing
        Finally
            EndMethod()
        End Try
    End Function
#End Region

#Region "Send SMS AppointmentSchedule"
    Public Function SendSMSAppointment(ByVal strMobile As String, ByVal strSMSText As String, ByVal strSMSType As String, ByVal strEntBy As String) As Boolean
        fstrSpName = "usp_SMS_Insert"
        Try
            Dim CoverEntity As New clsCommonClass
            BeginMethod()
            ' Modified by Ryan Estandarte 20 Sept 2012
            'Dim Param() As SqlParameter = New SqlParameter(22) {}
            Dim Param() As SqlParameter = New SqlParameter(23) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConnSYSMGR, fstrSpName)

            Param(0).Value = "0"
            Param(1).Value = Trim(strMobile).ToUpper()
            Param(2).Value = Trim(strSMSText).ToUpper()
            Param(3).Value = Trim(strSMSType).ToUpper()
            Param(4).Value = Trim(strEntBy).ToUpper()
            Param(5).Value = "S"
            Param(6).Value = "1"
            Param(7).Value = "NULL"
            Param(8).Value = "N"
            Param(9).Value = "1oyas@*&d9"

            SqlHelper.ExecuteDataset(selConnSYSMGR, CommandType.StoredProcedure, fstrSpName, Param)
            Return True
        Catch ex As Exception

            DealException(ex)
            Return False
        Finally
            EndMethod()
        End Try
    End Function
#End Region

#Region "Send SMS AppointmentSchedule Log"
    Public Function SendSMSAppointmentLog(ByVal strTRNTY As String, ByVal strTRNNO As String, ByVal strMobile As String, ByVal strMobileNo As String, ByVal strMobileName As String) As Boolean
        fstrSpName = "FAPL_SMSLog_Ins"
        Try
            Dim CoverEntity As New clsCommonClass
            BeginMethod()
            ' Modified by Ryan Estandarte 20 Sept 2012
            'Dim Param() As SqlParameter = New SqlParameter(22) {}
            Dim Param() As SqlParameter = New SqlParameter(23) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)

            Param(0).Value = Trim(strTRNTY).ToUpper()
            Param(1).Value = Trim(strTRNNO).ToUpper()
            Param(2).Value = Trim(strMobile).ToUpper()
            Param(3).Value = Trim(strMobileNo).ToUpper()
            Param(4).Value = Trim(strMobileName).ToUpper()

            SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            Return True
        Catch ex As Exception

            DealException(ex)
            Return False
        Finally
            EndMethod()
        End Try
    End Function
#End Region

#Region "Select Branch Contact No"
    Public Function GetBranchContactNo(ByVal msvc_svcid As String) As DataSet
        fstrSpName = "BB_GetBranchContactNo"
        Try
            BeginMethod()

            Dim Param() As SqlParameter = New SqlParameter() {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(msvc_svcid)

            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            ds.Tables(0).TableName = "MSVC_Contact"
            Return ds
        Catch ex As Exception
            DealException(ex)
            Return Nothing
        Finally
            EndMethod()
        End Try
    End Function
#End Region

#Region "GetObjectionReason"
    Public Function GetObjectionReason() As DataSet

        fstrSpName = "BB_ReasonObjection"
        Try
            BeginMethod()
            Dim Param() As SqlParameter = New SqlParameter(18) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)

            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)

            Return ds
        Catch ex As Exception
            DealException(ex)
            Return Nothing
        Finally
            EndMethod()
        End Try
    End Function
#End Region

#Region "GetNatureOfFeedback"
    Public Function GetNatureOfFeedback(ByVal ApptType As String) As DataSet

        fstrSpName = "BB_NatureOfFeedback"
        Try
            BeginMethod()
            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(ApptType)

            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)

            Return ds
        Catch ex As Exception
            DealException(ex)
            Return Nothing
        Finally
            EndMethod()
        End Try
    End Function
#End Region

#Region "SMS Error Log"
    Public Function AddSMSErrorLog(ByVal FuncName As String, ByVal ErrorDesc As String, ByVal TrnType As String, ByVal TrnNo As String) As Boolean
        fstrSpName = "BB_SMSErrorLog_Ins"
        Try
            BeginMethod()

            Dim Param() As SqlParameter = New SqlParameter(3) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(FuncName).ToUpper()
            Param(1).Value = Trim(ErrorDesc).ToUpper()
            Param(2).Value = Trim(TrnType).ToUpper()
            Param(3).Value = Trim(TrnNo).ToUpper()

            SqlHelper.ExecuteNonQuery(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            Return True
        Catch ex As Exception
            DealException(ex)
            Return False
        Finally
            EndMethod()
        End Try
    End Function
#End Region

#End Region
End Class