Imports System.Configuration
Imports CrystalDecisions.Shared
Imports SQLDataAccess
Imports BusinessEntity
Imports System.Data.SqlClient
Imports System.Web
Imports CrystalDecisions.Web
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data



Public Class ClsRptMdsl
#Region "Declaration"

    Private fstrReportName As String
    Private fstrReportUrl As String

    Private fcrvViewer As New CrystalReportViewer
    Private fcrvReportSource As New CrystalReportSource
    Private fstrSpName As String

    Private fstrcustmorid As String
    Private fstrmodelid As String
    Private fstrserialid As String
    Private fstrstate As String
    Private fddatebegin As System.DateTime
    Private fddateend As System.DateTime
    Private fstrsercid As String
    Private fstrModifiedBy As String

    Private fstrcustmoridend As String
    Private fstrmodelidend As String
    Private fstrserialidend As String
    Private fstrstateend As String
    Private fstrsercidend As String
    Private fstrsuser As String
    Private fdinstdatemin As System.DateTime
    Private fdinstdatemax As System.DateTime

    Private fstrlogctrid As String
    Private fstrlogcmpid As String
    Private fstrlogsvccid As String
    Private fstrlogrank As String

    Private fstrCustType As String
    Private fstrRace As String

    Private fstrminArea As String
    Private fstrmaxArea As String
    Private fstrClassID As String
#End Region

#Region "Properties"

    Public Property ReportFileName() As String
        Get
            Return fstrReportName
        End Get
        Set(ByVal Value As String)
            fstrReportName = Value
        End Set
    End Property
    Public Property uname() As String
        Get
            Return fstrsuser
        End Get
        Set(ByVal Value As String)
            fstrsuser = Value
        End Set
    End Property
    Public Property custmorid() As String
        Get
            Return fstrcustmorid
        End Get
        Set(ByVal Value As String)
            fstrcustmorid = Value
        End Set
    End Property
    Public Property custmoridend() As String
        Get
            Return fstrcustmoridend
        End Get
        Set(ByVal Value As String)
            fstrcustmoridend = Value
        End Set
    End Property
    Public Property modelid() As String
        Get
            Return fstrmodelid
        End Get
        Set(ByVal Value As String)
            fstrmodelid = Value
        End Set
    End Property
    Public Property modelidend() As String
        Get
            Return fstrmodelidend
        End Get
        Set(ByVal Value As String)
            fstrmodelidend = Value
        End Set
    End Property
    Public Property serialid() As String
        Get
            Return fstrserialid
        End Get
        Set(ByVal Value As String)
            fstrserialid = Value
        End Set
    End Property
    Public Property serialidend() As String
        Get
            Return fstrserialidend
        End Get
        Set(ByVal Value As String)
            fstrserialidend = Value
        End Set
    End Property
    Public Property state() As String
        Get
            Return fstrstate
        End Get
        Set(ByVal Value As String)
            fstrstate = Value
        End Set
    End Property
    Public Property stateend() As String
        Get
            Return fstrstateend
        End Get
        Set(ByVal Value As String)
            fstrstateend = Value
        End Set
    End Property
    Public Property datebegin() As String
        Get
            Return fddatebegin
        End Get
        Set(ByVal Value As String)
            fddatebegin = Value
        End Set
    End Property
    Public Property dateend() As String
        Get
            Return fddateend
        End Get
        Set(ByVal Value As String)
            fddateend = Value
        End Set
    End Property
    Public Property sercid() As String
        Get
            Return fstrsercid
        End Get
        Set(ByVal Value As String)
            fstrsercid = Value
        End Set
    End Property
    Public Property sercidend() As String
        Get
            Return fstrsercidend
        End Get
        Set(ByVal Value As String)
            fstrsercidend = Value
        End Set
    End Property
    Public Property instdatemin270() As String
        Get
            Return fdinstdatemin
        End Get
        Set(ByVal Value As String)
            fdinstdatemin = Value
        End Set
    End Property
    Public Property instdatemax270() As String
        Get
            Return fdinstdatemax
        End Get
        Set(ByVal Value As String)
            fdinstdatemax = Value
        End Set
    End Property
    Public Property ModifiedBy() As String
        Get
            Return fstrModifiedBy
        End Get
        Set(ByVal Value As String)
            fstrModifiedBy = Value
        End Set
    End Property

    Public Property logctrid() As String
        Get
            Return fstrlogctrid
        End Get
        Set(ByVal Value As String)
            fstrlogctrid = Value
        End Set
    End Property
    Public Property logcmpid() As String
        Get
            Return fstrlogcmpid
        End Get
        Set(ByVal Value As String)
            fstrlogcmpid = Value
        End Set
    End Property
    Public Property logsvccid() As String
        Get
            Return fstrlogsvccid
        End Get
        Set(ByVal Value As String)
            fstrlogsvccid = Value
        End Set
    End Property
    Public Property logrank() As String
        Get
            Return fstrlogrank
        End Get
        Set(ByVal Value As String)
            fstrlogrank = Value
        End Set
    End Property
    Public Property customerType() As String
        Get
            Return fstrCustType
        End Get
        Set(ByVal Value As String)
            fstrCustType = Value
        End Set
    End Property
    Public Property customerRace() As String
        Get
            Return fstrRace
        End Get
        Set(ByVal Value As String)
            fstrRace = Value
        End Set
    End Property
    Public Property minArea() As String
        Get
            Return fstrminArea
        End Get
        Set(ByVal Value As String)
            fstrminArea = Value
        End Set
    End Property
    Public Property maxArea() As String
        Get
            Return fstrmaxArea
        End Get
        Set(ByVal Value As String)
            fstrmaxArea = Value
        End Set
    End Property
    Public Property ClassID() As String
        Get
            Return fstrClassID
        End Get
        Set(ByVal Value As String)
            fstrClassID = Value
        End Set
    End Property

#End Region

#Region "Methods"
#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)

        connection.Open()

        Return connection
    End Function
#End Region
#Region "search Model due service list"

    Public Function Getmodeldue() As DataSet

        Dim selConnection As SqlConnection = Nothing
        Dim selConn As SqlConnection = Nothing
        selConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_RPTMDSL_VIEW"
        Try
            trans = selConnection.BeginTransaction()

            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(24) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrcustmorid)
            Param(1).Value = Trim(fstrmodelid)
            Param(2).Value = Trim(fstrserialid)
            Param(3).Value = Trim(fstrstate)
            Param(4).Value = Trim(fddatebegin)
            Param(5).Value = Trim(fddateend)
            Param(6).Value = Trim(fstrsercid)

            Param(7).Value = Trim(fstrcustmoridend)
            Param(8).Value = Trim(fstrmodelidend)
            Param(9).Value = Trim(fstrserialidend)
            Param(10).Value = Trim(fstrstateend)
            Param(11).Value = Trim(fstrsercidend)
            Param(12).Value = Trim(fstrsuser)
            Param(13).Value = Trim(fdinstdatemin)
            Param(14).Value = Trim(fdinstdatemax)

            Param(15).Value = Trim(fstrlogctrid)
            Param(16).Value = Trim(fstrlogcmpid)
            Param(17).Value = Trim(fstrlogsvccid)
            Param(18).Value = Trim(fstrlogrank)

            Param(19).Value = Trim(fstrCustType)
            Param(20).Value = Trim(fstrRace)

            Param(21).Value = Trim(fstrminArea)
            Param(22).Value = Trim(fstrmaxArea)
            Param(23).Value = Trim(fstrClassID)


            Dim ds As DataSet = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)

            '' switch status id to status name for display
            'Dim count As Integer
            'Dim statXmlTr As New clsXml
            'Dim strPanm As String
            'Dim strcontpar As String
            'statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            'count = 0
            'For count = 0 To ds.Tables(0).Rows.Count - 1
            '    Dim statusid As String = ds.Tables(0).Rows(count).Item(4).ToString 'item(4) is status
            '    strPanm = statXmlTr.GetLabelName("StatusMessage", statusid)
            '    ds.Tables(0).Rows(count).Item(4) = strPanm

            'Next
            Return ds

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "ClsRptMdsl.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "ClsRptMdsl.vb")
        Finally
            selConnection.Close()
            selConn.Close()
        End Try

    End Function

    Public Function GetmodeldueDataReader() As SqlDataReader

        'Dim selConnection As SqlConnection = Nothing
        Dim selConn As SqlConnection = Nothing
        'selConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        'Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_RPTMDSL_VIEW"
        Try
            'trans = selConnection.BeginTransaction()

            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(23) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrcustmorid)
            Param(1).Value = Trim(fstrmodelid)
            Param(2).Value = Trim(fstrserialid)
            Param(3).Value = Trim(fstrstate)
            Param(4).Value = Trim(fddatebegin)
            Param(5).Value = Trim(fddateend)
            Param(6).Value = Trim(fstrsercid)

            Param(7).Value = Trim(fstrcustmoridend)
            Param(8).Value = Trim(fstrmodelidend)
            Param(9).Value = Trim(fstrserialidend)
            Param(10).Value = Trim(fstrstateend)
            Param(11).Value = Trim(fstrsercidend)
            Param(12).Value = Trim(fstrsuser)
            Param(13).Value = Trim(fdinstdatemin)
            Param(14).Value = Trim(fdinstdatemax)

            Param(15).Value = Trim(fstrlogctrid)
            Param(16).Value = Trim(fstrlogcmpid)
            Param(17).Value = Trim(fstrlogsvccid)
            Param(18).Value = Trim(fstrlogrank)

            Param(19).Value = Trim(fstrCustType)
            Param(20).Value = Trim(fstrRace)

            Param(21).Value = Trim(fstrminArea)
            Param(22).Value = Trim(fstrmaxArea)

            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(selConn, CommandType.StoredProcedure, fstrSpName, Param)

            Return dr

        Catch ex As Exception
            'trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "ClsRptMdsl.vb")

        Catch ex As SqlException
            'trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "ClsRptMdsl.vb")
        Finally
            'selConnection.Close()
            'selConn.Close()
        End Try

    End Function

#End Region
#End Region
End Class
