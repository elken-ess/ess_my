Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web
Imports BusinessEntity
Imports SQLDataAccess

Public Class ClsRptCUSR
#Region "Declaration"


    Private fstrSpName As String ' store procedure name
    Private fstrusername As String ' username
    Private fstruserid As String ' username
    Private fstrminsvcid As String
    Private fstrmaxsvcid As String
    Private fstrmincustid As String
    Private fstrmaxcustid As String
    Private fstrmindate As System.DateTime
    Private fstrmaxdate As System.DateTime
    Private fstrminmodel As String
    Private fstrmaxmodel As String
    Private fstrminserialno As String
    Private fstrmaxserialno As String
    Private fstrminstat As String
    Private fstrmaxstat As String
    Private fstrminarea As String
    Private fstrmaxarea As String
    Private fstrcontract As String
    Private fstrrank As String ' rank
    Private fstrctry As String ' user country
    Private fstrcomp As String ' user company
    Private fstrsvc As String '  user svcid
    Private fstrcontracttext As String
    Private fstrCustType As String  'LW Added
    Private fstrCustRace As String  'LW Added
    Private fstrClassID As String

#End Region

#Region "Properties"
    Public Property username() As String
        Get
            Return fstrusername
        End Get
        Set(ByVal Value As String)
            fstrusername = Value
        End Set
    End Property
    Public Property userid() As String
        Get
            Return fstruserid
        End Get
        Set(ByVal Value As String)
            fstruserid = Value
        End Set
    End Property
    Public Property rank() As String
        Get
            Return fstrrank
        End Get
        Set(ByVal Value As String)
            fstrrank = Value
        End Set
    End Property
    Public Property ctryid() As String
        Get
            Return fstrctry
        End Get
        Set(ByVal Value As String)
            fstrctry = Value
        End Set
    End Property
    Public Property compid() As String
        Get
            Return fstrcomp
        End Get
        Set(ByVal Value As String)
            fstrcomp = Value
        End Set
    End Property
    Public Property svcid() As String
        Get
            Return fstrsvc
        End Get
        Set(ByVal Value As String)
            fstrsvc = Value
        End Set
    End Property

    Public Property Minsvcid() As String
        Get
            Return fstrminsvcid
        End Get
        Set(ByVal Value As String)
            fstrminsvcid = Value
        End Set
    End Property

    Public Property Maxsvcid() As String
        Get
            Return fstrmaxsvcid
        End Get
        Set(ByVal Value As String)
            fstrmaxsvcid = Value
        End Set
    End Property

    Public Property Mincustid() As String
        Get
            Return fstrmincustid
        End Get
        Set(ByVal Value As String)
            fstrmincustid = Value
        End Set
    End Property

    Public Property Maxcustid() As String
        Get
            Return fstrmaxcustid
        End Get
        Set(ByVal Value As String)
            fstrmaxcustid = Value
        End Set
    End Property

    Public Property Mindate() As String
        Get
            Return fstrmindate
        End Get
        Set(ByVal Value As String)
            fstrmindate = Value
        End Set
    End Property

    Public Property Maxdate() As String
        Get
            Return fstrmaxdate
        End Get
        Set(ByVal Value As String)
            fstrmaxdate = Value
        End Set
    End Property

    Public Property Minstat() As String
        Get
            Return fstrminstat
        End Get
        Set(ByVal Value As String)
            fstrminstat = Value
        End Set
    End Property

    Public Property Maxstat() As String
        Get
            Return fstrmaxstat
        End Get
        Set(ByVal Value As String)
            fstrmaxstat = Value
        End Set
    End Property
    Public Property Minserialno() As String
        Get
            Return fstrminserialno
        End Get
        Set(ByVal Value As String)
            fstrminserialno = Value
        End Set
    End Property
    Public Property Maxserialno() As String
        Get
            Return fstrmaxserialno
        End Get
        Set(ByVal Value As String)
            fstrmaxserialno = Value
        End Set
    End Property
    Public Property Minmodel() As String
        Get
            Return fstrminmodel
        End Get
        Set(ByVal Value As String)
            fstrminmodel = Value
        End Set
    End Property

    Public Property Maxmodel() As String
        Get
            Return fstrmaxmodel
        End Get
        Set(ByVal Value As String)
            fstrmaxmodel = Value
        End Set
    End Property
    Public Property Contract() As String
        Get
            Return fstrcontract
        End Get
        Set(ByVal Value As String)
            fstrcontract = Value
        End Set
    End Property
    Public Property Contracttext() As String
        Get
            Return fstrcontracttext
        End Get
        Set(ByVal Value As String)
            fstrcontracttext = Value
        End Set
    End Property
    'LW Added
    Public Property CustType() As String
        Get
            Return fstrCustType
        End Get
        Set(ByVal Value As String)
            fstrCustType = Value
        End Set
    End Property
    'LW Added
    Public Property CustRace() As String
        Get
            Return fstrCustRace
        End Get
        Set(ByVal Value As String)
            fstrCustRace = Value
        End Set
    End Property
    Public Property MinArea() As String
        Get
            Return fstrminarea
        End Get
        Set(ByVal Value As String)
            fstrminarea = Value
        End Set
    End Property
    Public Property MaxArea() As String
        Get
            Return fstrmaxarea
        End Get
        Set(ByVal Value As String)
            fstrmaxarea = Value
        End Set
    End Property
    Public Property ClassID() As String
        Get
            Return fstrClassID
        End Get
        Set(ByVal Value As String)
            fstrClassID = Value
        End Set
    End Property
   
#End Region
#Region "Methods"
#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)

        connection.Open()

        Return connection
    End Function
#End Region
#Region "Select contact"

    Public Function GetRptcusr() As DataSet

        Dim selConnection As SqlConnection = Nothing
        Dim selConn As SqlConnection = Nothing
        selConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "bbappl.BB_RPTCUSR_VIEW"



        Try
            trans = selConnection.BeginTransaction()


            ' define search fields 
            
           
          
            Dim Param() As SqlParameter = New SqlParameter(23) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrminsvcid)
            Param(1).Value = Trim(fstrmaxsvcid)
            Param(2).Value = Trim(fstrmincustid)
            Param(3).Value = Trim(fstrmaxcustid)
            Param(4).Value = Trim(fstrmindate)
            Param(5).Value = Trim(fstrmaxdate)
            Param(6).Value = Trim(fstrminmodel)
            Param(7).Value = Trim(fstrmaxmodel)
            Param(8).Value = Trim(fstrminserialno)
            Param(9).Value = Trim(fstrmaxserialno)
            Param(10).Value = Trim(fstrminstat)
            Param(11).Value = Trim(fstrmaxstat)
            Param(12).Value = Trim(fstrcontract)
            Param(13).Value = Trim(fstruserid)
            Param(14).Value = Trim(fstrctry)
            Param(15).Value = Trim(fstrcomp)
            Param(16).Value = Trim(fstrsvc)
            Param(17).Value = Trim(fstrrank)
            Param(18).Value = Trim(fstrCustType)
            Param(19).Value = Trim(fstrCustRace)
            Param(20).Value = Trim(fstrminarea)
            Param(21).Value = Trim(fstrmaxarea)
            Param(22).Value = Trim(fstrClassID)

            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)

            Dim count As Integer
            Dim statXmlTr As New clsXml
            Dim strPanm As String
            statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            For count = 0 To ds.Tables(0).Rows.Count - 1
                Dim statusid As String = ds.Tables(0).Rows(count).Item(3).ToString 'item(3) is status
                strPanm = statXmlTr.GetLabelName("StatusMessage", statusid)
                ds.Tables(0).Rows(count).Item(3) = strPanm
            Next
            Return ds

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "ClsRptCUSR.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "ClsRptCUSR.vb")
        Finally
            selConnection.Close()
            selConn.Close()
        End Try

    End Function
#End Region

#Region "getServiceCenter and States"
    Public Function SVCnStatebyUserID() As DataSet
        Dim sqlConn As SqlConnection = Nothing
        sqlConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        fstrSpName = "BB_RPTCUSR_GetSVCSTATE"
        Dim ds As DataSet = New DataSet
        Try
            Dim paramStorc() As SqlParameter = New SqlParameter(1) {}
            paramStorc = SqlHelperParameterCache.GetSpParameterSet(sqlConn, fstrSpName)
            paramStorc(0).Value = Trim(fstruserid)

            ds = SqlHelper.ExecuteDataset(sqlConn, CommandType.StoredProcedure, fstrSpName, paramStorc)

        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("Customer reference Report", ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "ClsRptCUSR.vb")
            Return Nothing
        Finally
            sqlConn.Close()
        End Try
        Return ds
    End Function

#End Region


#Region "Get State and area from service center"

    Public Function getStateAndArea() As DataSet
        Dim sqlConn As SqlConnection = Nothing
        sqlConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        fstrSpName = "BB_FNCCLAS_SVC_STA_ARE"
        Dim dsCLA As DataSet = New DataSet
        Try
            Dim paramStorc() As SqlParameter = New SqlParameter(1) {}
            paramStorc = SqlHelperParameterCache.GetSpParameterSet(sqlConn, fstrSpName)
            paramStorc(0).Value = Trim(fstrminsvcid)
            paramStorc(1).Value = Trim(fstrctry)

            dsCLA = SqlHelper.ExecuteDataset(sqlConn, CommandType.StoredProcedure, fstrSpName, paramStorc)

        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("Customer reference Report", ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "ClsRptCUSR.vb")
            Return Nothing
        Finally
            sqlConn.Close()
        End Try
        Return dsCLA
    End Function

#End Region

#Region "Get area from state"

    Public Function getAreafromState() As DataSet
        Dim sqlConn As SqlConnection = Nothing
        sqlConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        fstrSpName = "BB_FNCCLAS_STA_ARE"
        Dim dsCLA As DataSet = New DataSet
        Try
            Dim paramStorc() As SqlParameter = New SqlParameter(1) {}
            paramStorc = SqlHelperParameterCache.GetSpParameterSet(sqlConn, fstrSpName)
            paramStorc(0).Value = Trim(fstrminstat)
            paramStorc(1).Value = Trim(fstrminsvcid)

            dsCLA = SqlHelper.ExecuteDataset(sqlConn, CommandType.StoredProcedure, fstrSpName, paramStorc)

        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("Customer reference Report", ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "ClsRptCUSR.vb")
            Return Nothing
        Finally
            sqlConn.Close()
        End Try
        Return dsCLA
    End Function

#End Region
#End Region
End Class
