﻿Imports Microsoft.VisualBasic
Imports SQLDataAccess
Imports BusinessEntity
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Data

#Region "Module Description"
'Description        :   Audit Log Report
'Last Modified Date :   2006-05-27
'Version            :   1.0
'Author             :   BAIYUNPENG
#End Region

Public Class clsAuditLogReport

#Region "Declaration"
    Private strStoreProcedureName As String
    Private strFServiceCenter As String
    Private strTServiceCenter As String
    Private strFDept As String
    Private strTDept As String
    Private strFUserID As String
    Private strTUserID As String
    Private strFDate As String
    Private strTDate As String
    Private strFLoginTime As String
    Private strTLoginTime As String
    Private strFScreenID As String
    Private strTScreenID As String
    Private strFSessionID As String
    Private strTSessionID As String
    Private strSortBy As String
    Private strLoginUserID As String

#End Region

#Region "Properties"
    Public Property FromServiceCenter() As String
        Get
            Return strFServiceCenter
        End Get
        Set(ByVal value As String)
            strFServiceCenter = value
        End Set
    End Property

    Public Property ToServiceCenter() As String
        Get
            Return strTServiceCenter
        End Get
        Set(ByVal value As String)
            strTServiceCenter = value
        End Set
    End Property

    Public Property FromDept() As String
        Get
            Return strFDept
        End Get
        Set(ByVal value As String)
            strFDept = value
        End Set
    End Property

    Public Property ToDept() As String
        Get
            Return strTDept
        End Get
        Set(ByVal value As String)
            strTDept = value
        End Set
    End Property

    Public Property FromUserID() As String
        Get
            Return strFUserID
        End Get
        Set(ByVal value As String)
            strFUserID = value
        End Set
    End Property

    Public Property ToUserID() As String
        Get
            Return strTUserID
        End Get
        Set(ByVal value As String)
            strTUserID = value
        End Set
    End Property

    Public Property FromDate() As String
        Get
            Return strFDate
        End Get
        Set(ByVal value As String)
            strFDate = value
        End Set
    End Property

    Public Property ToDate() As String
        Get
            Return strTDate
        End Get
        Set(ByVal value As String)
            strTDate = value
        End Set
    End Property

    Public Property FromLoginTime() As String
        Get
            Return strFLoginTime
        End Get
        Set(ByVal value As String)
            strFLoginTime = value
        End Set
    End Property

    Public Property ToLoginTime() As String
        Get
            Return strTLoginTime
        End Get
        Set(ByVal value As String)
            strTLoginTime = value
        End Set
    End Property

    Public Property FromScreenID() As String
        Get
            Return strFScreenID
        End Get
        Set(ByVal value As String)
            strFScreenID = value
        End Set
    End Property

    Public Property ToScreenID() As String
        Get
            Return strTScreenID
        End Get
        Set(ByVal value As String)
            strTScreenID = value
        End Set
    End Property

    Public Property FromSessionID() As String
        Get
            Return strFSessionID
        End Get
        Set(ByVal value As String)
            strFSessionID = value
        End Set
    End Property

    Public Property ToSessionID() As String
        Get
            Return strTSessionID
        End Get
        Set(ByVal value As String)
            strTSessionID = value
        End Set
    End Property

    Public Property SortBy() As String
        Get
            Return strSortBy
        End Get
        Set(ByVal value As String)
            strSortBy = value
        End Set
    End Property

    Public Property LoginUserID() As String
        Get
            Return strLoginUserID
        End Get
        Set(ByVal value As String)
            strLoginUserID = value
        End Set
    End Property

#End Region

#Region "Method"
#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)

        connection.Open()

        Return connection
    End Function
#End Region

#Region "Retrive Info"

    Public Function RetriveAuditLogInfo() As DataSet
        Dim sqlConn As SqlConnection = Nothing
        sqlConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        strStoreProcedureName = "BB_RPTAULR_VIEW"
        Dim dsALR As DataSet = New DataSet
        Try
            Dim paramStorc() As SqlParameter = New SqlParameter(16) {}
            paramStorc = SqlHelperParameterCache.GetSpParameterSet(sqlConn, strStoreProcedureName)
            paramStorc(0).Value = Trim(strFServiceCenter)
            paramStorc(1).Value = Trim(strTServiceCenter)
            paramStorc(2).Value = Trim(strFDept)
            paramStorc(3).Value = Trim(strTDept)
            paramStorc(4).Value = Trim(strFUserID)
            paramStorc(5).Value = Trim(strTUserID)
            paramStorc(6).Value = Trim(strFDate)
            paramStorc(7).Value = Trim(strTDate)
            paramStorc(8).Value = Trim(strFLoginTime)
            paramStorc(9).Value = Trim(strTLoginTime)
            paramStorc(10).Value = Trim(strFScreenID)
            paramStorc(11).Value = Trim(strTScreenID)
            paramStorc(12).Value = Trim(strFSessionID)
            paramStorc(13).Value = Trim(strTSessionID)
            paramStorc(14).Value = Trim(strSortBy)
            paramStorc(15).Value = Trim(strLoginUserID)

            dsALR = SqlHelper.ExecuteDataset(sqlConn, CommandType.StoredProcedure, strStoreProcedureName, paramStorc)

        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("Audit Log Report", ErrorLog.Item(1).ToString, strStoreProcedureName, WriteErrLog.SELE, "clsAuditLogReport.vb")
            Return Nothing
        Finally
            sqlConn.Close()
        End Try
        Return dsALR
    End Function

#End Region

#End Region

End Class
