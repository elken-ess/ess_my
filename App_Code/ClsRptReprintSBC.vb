Imports CrystalDecisions.Shared
Imports SQLDataAccess
Imports BusinessEntity
Imports System.Data.SqlClient
Imports System.Web
Imports CrystalDecisions.Web
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data
Imports System.Configuration

Public Class ClsRptReprintSBC





#Region "Declaration"

    Private fstrReportName As String
    Private fstrReportUrl As String

    Private fcrvViewer As New CrystalReportViewer
    Private fcrvReportSource As New CrystalReportSource
    Private fstrSpName As String
    Private fstrModifiedBy As String
    Private fstrtecnicianid As String
    Private fstrtecnicianname As String
    Private fstrsbcno As String
    Private fstrMinivdate As String
    Private fstrMaxivdate As String
    Private fstrIsoDesc1 As String
    Private fstrIsoDesc2 As String
    Private fstrSvcId As String
    Private fstrSBCUser As String
    Private fstrSBCDate As String
    Private fstrCOL1 As String


#End Region

#Region "Properties"

    Public Property Col1() As String
        Get
            Return fstrCOL1
        End Get
        Set(ByVal Value As String)
            fstrCOL1 = Value
        End Set
    End Property

    Public Property Minivdate() As String
        Get
            Return fstrMinivdate
        End Get
        Set(ByVal Value As String)
            fstrMinivdate = Value
        End Set
    End Property

    Public Property Maxivdate() As String
        Get
            Return fstrMaxivdate
        End Get
        Set(ByVal Value As String)
            fstrMaxivdate = Value
        End Set
    End Property

    Public Property IsoDesc1() As String
        Get
            Return fstrIsoDesc1
        End Get
        Set(ByVal Value As String)
            fstrIsoDesc1 = Value
        End Set
    End Property

    Public Property IsoDesc2() As String
        Get
            Return fstrIsoDesc2
        End Get
        Set(ByVal Value As String)
            fstrIsoDesc2 = Value
        End Set
    End Property


    Public Property tecnicianid() As String
        Get
            Return fstrtecnicianid
        End Get
        Set(ByVal Value As String)
            fstrtecnicianid = Value
        End Set
    End Property
    Public Property tecnicianname() As String
        Get
            Return fstrtecnicianname
        End Get
        Set(ByVal Value As String)
            fstrtecnicianname = Value
        End Set
    End Property
    Public Property sbcno() As String
        Get
            Return fstrsbcno
        End Get
        Set(ByVal Value As String)
            fstrsbcno = Value
        End Set
    End Property

    Public Property ModifiedBy() As String
        Get
            Return fstrModifiedBy
        End Get
        Set(ByVal Value As String)
            fstrModifiedBy = Value
        End Set
    End Property

    Public Property SVCID() As String
        Get
            Return fstrSvcId
        End Get
        Set(ByVal Value As String)
            fstrSvcId = Value
        End Set
    End Property

    Public Property SBCUser() As String
        Get
            Return fstrSBCUser
        End Get
        Set(ByVal Value As String)
            fstrSBCUser = Value
        End Set
    End Property

    Public Property SBCDate() As String
        Get
            Return fstrSBCDate
        End Get
        Set(ByVal Value As String)
            fstrSBCDate = Value
        End Set
    End Property


    'Public Property logctrid() As String
    '    Get
    '        Return fstrlogctrid
    '    End Get
    '    Set(ByVal Value As String)
    '        fstrlogctrid = Value
    '    End Set
    'End Property
    'Public Property logcompanyid() As String
    '    Get
    '        Return fstrlogcompanyid
    '    End Get
    '    Set(ByVal Value As String)
    '        fstrlogcompanyid = Value
    '    End Set
    'End Property
    'Public Property logserviceid() As String
    '    Get
    '        Return fstrlogserviceid
    '    End Get
    '    Set(ByVal Value As String)
    '        fstrlogserviceid = Value
    '    End Set
    'End Property
    'Public Property logrank() As String
    '    Get
    '        Return fstrlogrank
    '    End Get
    '    Set(ByVal Value As String)
    '        fstrlogrank = Value
    '    End Set
    'End Property
#End Region

#Region "Methods"
#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)

        connection.Open()

        Return connection
    End Function
#End Region
#Region "search Pack list printing"

    Public Function GetPack() As DataSet

        Dim selConnection As SqlConnection = Nothing
        Dim selConn As SqlConnection = Nothing
        selConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        'fstrSpName = "BB_RPTPACK_VIEW"
        fstrSpName = "BB_RPTPRINT_SBC_VIEW"

        Try
            trans = selConnection.BeginTransaction()

            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(11) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrsbcno)
            Param(1).Value = Nothing
            Param(2).Value = Nothing
            Param(3).Value = Nothing
            Param(4).Value = Nothing
            Param(5).Value = Nothing
            Param(6).Value = Nothing
            Param(7).Value = Nothing
            Param(8).Value = Nothing
            Param(9).Value = Nothing
            Param(10).Value = Nothing

            Dim ds As DataSet = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            Minivdate = Param(1).Value
            Maxivdate = Param(2).Value
            tecnicianid = Param(3).Value
            tecnicianname = Param(4).Value
            IsoDesc1 = Param(5).Value
            IsoDesc2 = Param(6).Value
            SVCID = Param(7).Value
            SBCUser = Param(8).Value
            SBCDate = Param(9).Value
            Col1 = Param(10).Value
            Return ds

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "ClsRptReprintSBC.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "ClsRptReprintSBC.vb")
        Finally
            selConnection.Close()
            selConn.Close()
        End Try

    End Function

#End Region
#End Region



End Class
