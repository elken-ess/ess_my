Imports System.Configuration
Imports CrystalDecisions.Shared
Imports SQLDataAccess
Imports BusinessEntity
Imports System.Data.SqlClient
Imports System.Web
Imports CrystalDecisions.Web
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data
Imports System
Imports System.Web.UI.WebControls
Imports System.IOnamespace 'ExcelUtility
Imports Excel = Microsoft.Office.Interop.Excel



Public Class ClsExportPRFtoXLS
#Region "Declaration"

   
    Private fstrSpName As String

    Private fstrminsercenter As String
    Private fdmindate As System.DateTime
    Private fdmaxdate As System.DateTime
    Private fstrmintechnician As String
    Private fstrmaxtechnician As String
    Private fstrtxtPrfNoFrom As String
    Private fstrtxtPrfNoTo As String

#End Region

#Region "Properties"

    Public Property minsercenter() As String
        Get
            Return fstrminsercenter
        End Get
        Set(ByVal Value As String)
            fstrminsercenter = Value
        End Set
    End Property
    Public Property mindate() As String
        Get
            Return fdmindate
        End Get
        Set(ByVal Value As String)
            fdmindate = Value
        End Set
    End Property
    Public Property maxdate() As String
        Get
            Return fdmaxdate
        End Get
        Set(ByVal Value As String)
            fdmaxdate = Value
        End Set
    End Property
    Public Property mintechnician() As String
        Get
            Return fstrmintechnician
        End Get
        Set(ByVal Value As String)
            fstrmintechnician = Value
        End Set
    End Property
    Public Property maxtechnician() As String
        Get
            Return fstrmaxtechnician
        End Get
        Set(ByVal Value As String)
            fstrmaxtechnician = Value
        End Set
    End Property
    Public Property txtPrfNoFrom() As String
        Get
            Return fstrtxtPrfNoFrom
        End Get
        Set(ByVal Value As String)
            fstrtxtPrfNoFrom = Value
        End Set
    End Property
    Public Property txtPrfNoTo() As String
        Get
            Return fstrtxtPrfNoTo
        End Get
        Set(ByVal Value As String)
            fstrtxtPrfNoTo = Value
        End Set
    End Property

    
#End Region

#Region "Methods"
#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)

        connection.Open()

        Return connection
    End Function
#End Region
#Region "search PRFs"

    Public Function GetPrfs() As DataSet

        Dim selConnection As SqlConnection = Nothing
        Dim selConn As SqlConnection = Nothing
        selConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_EXPORTPRFS_VIEW"

        Try
            trans = selConnection.BeginTransaction()

            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(7) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrminsercenter)
            Param(1).Value = Trim(fdmindate)
            Param(2).Value = Trim(fdmaxdate)
            Param(3).Value = Trim(fstrmintechnician)
            Param(4).Value = Trim(fstrmaxtechnician)
            Param(5).Value = Trim(fstrtxtPrfNoFrom)
            Param(6).Value = Trim(fstrtxtPrfNoTo)

            Dim ds As DataSet = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)

            Return ds

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            'Dim WriteErrLog As New clsLogFile()
            'WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "ClsExportPRFtoXLS.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            'Dim WriteErrLog As New clsLogFile()
            'WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "ClsExportPRFtoXLS.vb")
        Finally
            selConnection.Close()
            selConn.Close()
        End Try

    End Function

#End Region
#Region "export to XLS"
    Public Sub Convert(ByVal ds As DataSet, ByVal Response As HttpResponse)
        Dim attachment As String = "attachment; filename= ExportPRFs_" & Today() & ".xls"
        Dim style As String = "<style> td { mso-number-format:\@; } </style> "

        Response.Clear()
        Response.Charset = ""
        Response.AddHeader("content-disposition", attachment)
        Response.ContentType = "application/vnd.ms-excel"
        Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter()
        Dim htmlWrite As System.Web.UI.HtmlTextWriter = New System.Web.UI.HtmlTextWriter(stringWrite)
        Dim dg As System.Web.UI.WebControls.DataGrid = New System.Web.UI.WebControls.DataGrid()
        dg.DataSource = ds.Tables(0)
        dg.DataBind()
        dg.RenderControl(htmlWrite)
        Response.Write(style) 'style is added dynamically
        Response.Write(stringWrite.ToString())
        Response.End()
    End Sub

#End Region
#End Region
End Class
