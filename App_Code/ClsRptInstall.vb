Imports System.Data
Imports System.Data.SqlClient
Imports BusinessEntity
Imports SQLDataAccess

Public Class ClsRptInstall
#Region "Declarations"
    Private _con As SqlConnection
    Private _spName As String

    Private _userName As String

    Private _prodClass As String
    Private _modID As String
    Private _modIDTo As String
    Private _startInstallDate As DateTime
    Private _endInstallDate As DateTime
    Private _svcId As String
    Private _stateID As String
    Private _areaID As String
    Private _svcIdTo As String
    Private _stateIDTo As String
    Private _areaIDTo As String
    Private _custType As String
    Private _status As String
    '----------start---------------
    'added by LLY 
    Private fstrrank As String ' rank
    Private fstrctry As String ' user country
    Private fstrcomp As String ' user company
    Private fstrsvc As String '  user svcid
    '----------end---------------
#End Region

#Region "Properties"
    Public Property UserName() As String
        Get
            Return _userName
        End Get
        Set(ByVal value As String)
            _userName = value
        End Set
    End Property

    '----------start---------------
    'added by LLY 
    Public Property UserRank() As String
        Get
            Return fstrrank
        End Get
        Set(ByVal Value As String)
            fstrrank = Value
        End Set
    End Property
    Public Property UserCtryid() As String
        Get
            Return fstrctry
        End Get
        Set(ByVal Value As String)
            fstrctry = Value
        End Set
    End Property
    Public Property UserCompid() As String
        Get
            Return fstrcomp
        End Get
        Set(ByVal Value As String)
            fstrcomp = Value
        End Set
    End Property
    Public Property UserSvcid() As String
        Get
            Return fstrsvc
        End Get
        Set(ByVal Value As String)
            fstrsvc = Value
        End Set
    End Property
    '----------end---------------

    Public Property ProdClass() As String
        Get
            Return _prodClass
        End Get
        Set(ByVal value As String)
            _prodClass = value
        End Set
    End Property

    Public Property ModID() As String
        Get
            Return _modID
        End Get
        Set(ByVal value As String)
            _modID = value
        End Set
    End Property

    Public Property ModIDTo() As String
        Get
            Return _modIDTo
        End Get
        Set(ByVal value As String)
            _modIDTo = value
        End Set
    End Property

    Public Property StartInstallDate() As Date
        Get
            Return _startInstallDate
        End Get
        Set(ByVal value As Date)
            _startInstallDate = value
        End Set
    End Property

    Public Property EndInstallDate() As Date
        Get
            Return _endInstallDate
        End Get
        Set(ByVal value As Date)
            _endInstallDate = value
        End Set
    End Property

    Public Property SvcId() As String
        Get
            Return _svcId
        End Get
        Set(ByVal value As String)
            _svcId = value
        End Set
    End Property
    Public Property SvcIdTo() As String
        Get
            Return _svcIdTo
        End Get
        Set(ByVal value As String)
            _svcIdTo = value
        End Set
    End Property

    Public Property StateID() As String
        Get
            Return _stateID
        End Get
        Set(ByVal value As String)
            _stateID = value
        End Set
    End Property

    Public Property StateIDTo() As String
        Get
            Return _stateIDTo
        End Get
        Set(ByVal value As String)
            _stateIDTo = value
        End Set
    End Property

    Public Property AreaID() As String
        Get
            Return _areaID
        End Get
        Set(ByVal value As String)
            _areaID = value
        End Set
    End Property
    Public Property AreaIDTo() As String
        Get
            Return _areaIDTo
        End Get
        Set(ByVal value As String)
            _areaIDTo = value
        End Set
    End Property

    Public Property CustType() As String
        Get
            Return _custType
        End Get
        Set(ByVal value As String)
            _custType = value
        End Set
    End Property
    Public Property Status() As String
        Get
            Return _status
        End Get
        Set(ByVal value As String)
            _status = value
        End Set
    End Property

#End Region

#Region "Private Function"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)

        connection.Open()

        Return connection
    End Function
#End Region

#Region "Public Function"
    Public Function PopulateProductClass(ByVal flag As String) As DataTable
        Dim dt As New DataTable("ProductClass")

        _con = GetConnection(ConfigurationManager.AppSettings("ConnectionString"))
        _spName = "BB_SELECTSTATUS"

        Try
            Dim param() As SqlParameter = New SqlParameter(1) {}
            param = SqlHelperParameterCache.GetSpParameterSet(_con, _spName)
            param(0).Value = flag

            dt = SqlHelper.ExecuteDataset(_con, CommandType.StoredProcedure, _spName, param).Tables(0)
            dt.Columns.Remove("LCOD_CODTY")
            dt.Columns.Remove("LCOD_CODID1")
            dt.Columns.Remove("LCOD_SEQ")
        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(_userName, ErrorLog.Item(1).ToString, _spName, WriteErrLog.SELE, "ClsRptInstall.vb")
        Finally
            _con.Close()
        End Try

        Return dt
    End Function

    public Function RetrieveInstallBase() As DataSet
        Dim ds As New DataSet("InstallBase")
        _con = GetConnection(ConfigurationManager.AppSettings("ConnectionString"))
        _spName = "BB_RPTINSTALLBASE"

        Try
            Dim param() As SqlParameter = New SqlParameter(17) {}
            param = SqlHelperParameterCache.GetSpParameterSet(_con, _spName)
            param(0).Value = _prodClass
            param(1).Value = _modID
            param(2).Value = _modIDTo
            param(3).Value = _startInstallDate
            param(4).Value = _endInstallDate
            param(5).Value = _svcId
            param(6).Value = _svcIdTo
            param(7).Value = _areaID
            param(8).Value = _areaIDTo
            param(9).Value = _stateID
            param(10).Value = _stateIDTo
            'added by LLY for OIL 30639
            param(11).Value = Trim(fstrctry)
            param(12).Value = Trim(fstrcomp)
            param(13).Value = Trim(fstrsvc)
            param(14).Value = Trim(fstrrank)
            param(15).Value = Trim(_custType)
            param(16).Value = Trim(_status)

            ds = SqlHelper.ExecuteDataset(_con, CommandType.StoredProcedure, _spName, param)
        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(_userName, ErrorLog.Item(1).ToString, _spName, WriteErrLog.SELE, "ClsRptInstall.vb")
        Finally
            _con.Close()
        End Try
        Return ds
    End Function


    Public Function RetrieveInstallBaseSummary() As DataSet
        Dim ds As New DataSet("InstallBaseSummary")
        _con = GetConnection(ConfigurationManager.AppSettings("ConnectionString"))
        _spName = "BB_RPTINSTALLBASE_SUMMARY"

        Try
            Dim param() As SqlParameter = New SqlParameter(9) {}
            param = SqlHelperParameterCache.GetSpParameterSet(_con, _spName)
            param(0).Value = _prodClass
            param(1).Value = _modID
            param(2).Value = _modIDTo
            param(3).Value = _startInstallDate.ToString("yyyy-MM-dd")
            param(4).Value = _endInstallDate.ToString("yyyy-MM-dd")
            param(5).Value = _svcId
            param(6).Value = _svcIdTo
            param(7).Value = _custType   'LLY 20160928
            param(8).Value = _status

            ds = SqlHelper.ExecuteDataset(_con, CommandType.StoredProcedure, _spName, param)
        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(_userName, ErrorLog.Item(1).ToString, _spName, WriteErrLog.SELE, "ClsRptInstall.vb")
        Finally
            _con.Close()
        End Try
        Return ds
    End Function
#End Region
End Class
