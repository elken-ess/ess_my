﻿Imports Microsoft.VisualBasic

Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web
Imports BusinessEntity
Imports SQLDataAccess

Public Class clsRPTSVOS
#Region "Declaration"

    Private fstrminSvcID As String
    Private fstrmaxSvcID As String
    Private fstrminCusrid As String
    Private fstrmaxCusrid As String
    Private fstrminPrdctMd As String
    Private fstrmaxPrdctMd As String
    Private fstrminState As String
    Private fstrmaxState As String
    Private fstrminTchID As String
    Private fstrmaxTchID As String
    Private fstrminRONo As String
    Private fstrmaxRONo As String
    Private fstrminArea As String
    Private fstrmaxArea As String
    Private fstrmindate As System.DateTime
    Private fstrmaxdate As System.DateTime
    Private fstrminINSTLdate As System.DateTime
    Private fstrmaxINSTLdate As System.DateTime
    Private fstrSortBy As String
    Private fstrUserName As String
    Private fstrUserID As String
    '//////////////////////////////////////
    Private fstrCountryID As String
    Private fstrCompID As String
    Private fstrSVCID As String
    Private fstrRank As String
    Private fstrStatus As String
    Private fstrStatusText As String

    Private fstrServiceType As String
    Private fstrServiceTypeText As String

    Private fstrSpName As String ' store procedure name
#End Region

#Region "Properties"
    Public Property ServiceType() As String
        Get
            Return fstrServiceType
        End Get
        Set(ByVal Value As String)
            fstrServiceType = Value
        End Set
    End Property

    Public Property ServiceTypeText() As String
        Get
            Return fstrServiceTypeText
        End Get
        Set(ByVal Value As String)
            fstrServiceTypeText = Value
        End Set
    End Property



    Public Property Status() As String
        Get
            Return fstrStatus
        End Get
        Set(ByVal Value As String)
            fstrStatus = Value
        End Set
    End Property

    Public Property StatusText() As String
        Get
            Return fstrStatusText
        End Get
        Set(ByVal Value As String)
            fstrStatusText = Value
        End Set
    End Property


    Public Property Rank() As String
        Get
            Return fstrRank
        End Get
        Set(ByVal Value As String)
            fstrRank = Value
        End Set
    End Property
    Public Property SVCID() As String
        Get
            Return fstrSVCID
        End Get
        Set(ByVal Value As String)
            fstrSVCID = Value
        End Set
    End Property
    Public Property CompID() As String
        Get
            Return fstrCompID
        End Get
        Set(ByVal Value As String)
            fstrCompID = Value
        End Set
    End Property
    Public Property CountryID() As String
        Get
            Return fstrCountryID
        End Get
        Set(ByVal Value As String)
            fstrCountryID = Value
        End Set
    End Property
    Public Property minCustmID() As String
        Get
            Return fstrminCusrid
        End Get
        Set(ByVal value As String)
            fstrminCusrid = value

        End Set
    End Property
    Public Property maxCustmID() As String
        Get
            Return fstrmaxCusrid
        End Get
        Set(ByVal value As String)
            fstrmaxCusrid = value
        End Set
    End Property
    Public Property minSvcID() As String
        Get
            Return fstrminSvcID
        End Get
        Set(ByVal value As String)
            fstrminSvcID = value
        End Set
    End Property
    Public Property maxSvcID() As String
        Get
            Return fstrmaxSvcID
        End Get
        Set(ByVal value As String)
            fstrmaxSvcID = value
        End Set
    End Property
    Public Property minPrdctMd() As String
        Get
            Return fstrminPrdctMd
        End Get
        Set(ByVal value As String)
            fstrminPrdctMd = value
        End Set
    End Property
    Public Property maxPrdctMd() As String
        Get
            Return fstrmaxPrdctMd
        End Get
        Set(ByVal value As String)
            fstrmaxPrdctMd = value
        End Set
    End Property
    Public Property minState() As String
        Get
            Return fstrminState
        End Get
        Set(ByVal value As String)
            fstrminState = value
        End Set
    End Property
    Public Property maxState() As String
        Get
            Return fstrmaxState
        End Get
        Set(ByVal value As String)
            fstrmaxState = value
        End Set
    End Property
    Public Property minTchID() As String
        Get
            Return fstrminTchID
        End Get
        Set(ByVal value As String)
            fstrminTchID = value
        End Set
    End Property
    Public Property maxTchID() As String
        Get
            Return fstrmaxTchID
        End Get
        Set(ByVal value As String)
            fstrmaxTchID = value
        End Set
    End Property
    Public Property minRONo() As String
        Get
            Return fstrminRONo
        End Get
        Set(ByVal value As String)
            fstrminRONo = value
        End Set
    End Property
    Public Property maxRONo() As String
        Get
            Return fstrmaxRONo
        End Get
        Set(ByVal value As String)
            fstrmaxRONo = value
        End Set
    End Property
    Public Property minArea() As String
        Get
            Return fstrminArea
        End Get
        Set(ByVal value As String)
            fstrminArea = value
        End Set
    End Property
    Public Property maxArea() As String
        Get
            Return fstrmaxArea
        End Get
        Set(ByVal value As String)
            fstrmaxArea = value
        End Set
    End Property
    Public Property mindate() As System.DateTime
        Get
            Return fstrmindate
        End Get
        Set(ByVal value As System.DateTime)
            fstrmindate = value
        End Set
    End Property
    Public Property maxdate() As System.DateTime
        Get
            Return fstrmaxdate
        End Get
        Set(ByVal value As System.DateTime)
            fstrmaxdate = value
        End Set
    End Property
    Public Property maxINSTLdate() As System.DateTime
        Get
            Return fstrmaxINSTLdate
        End Get
        Set(ByVal value As System.DateTime)
            fstrmaxINSTLdate = value
        End Set
    End Property
    Public Property minINSTLdate() As System.DateTime
        Get
            Return fstrminINSTLdate
        End Get
        Set(ByVal value As System.DateTime)
            fstrminINSTLdate = value
        End Set
    End Property
    Public Property SortBy() As String
        Get
            Return fstrSortBy
        End Get
        Set(ByVal Value As String)
            fstrSortBy = Value
        End Set
    End Property
    Public Property UserName() As String
        Get
            Return fstrUserName
        End Get
        Set(ByVal Value As String)
            fstrUserName = Value
        End Set
    End Property
#End Region

#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)

        connection.Open()

        Return connection
    End Function
#End Region

#Region "Service Order Summary (Overall) "
    Public Function GetServiceOrderSum() As DataSet
        Dim selConnection As SqlConnection = Nothing
        Dim selConn As SqlConnection = Nothing
        selConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_RPTSVOS_VIEW"
        Try
            trans = selConnection.BeginTransaction()
            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(23) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrminSvcID)
            Param(1).Value = Trim(fstrmaxSvcID)
            Param(2).Value = Trim(fstrminCusrid)
            Param(3).Value = Trim(fstrmaxCusrid)
            Param(4).Value = Trim(fstrminPrdctMd)
            Param(5).Value = Trim(fstrmaxPrdctMd)
            Param(6).Value = Trim(fstrminState)
            Param(7).Value = Trim(fstrmaxState)
            Param(8).Value = Trim(fstrminTchID)
            Param(9).Value = Trim(fstrmaxTchID)
            Param(10).Value = Trim(fstrminRONo)
            Param(11).Value = Trim(fstrmaxRONo)
            Param(12).Value = Trim(fstrminArea)
            Param(13).Value = Trim(fstrmaxArea)
            Param(14).Value = Trim(fstrmindate)
            Param(15).Value = Trim(fstrmaxdate)

            Param(16).Value = Trim(fstrCountryID)
            Param(17).Value = Trim(fstrCompID)
            Param(18).Value = Trim(fstrSVCID)
            Param(19).Value = Trim(fstrRank)
            Param(20).Value = Trim(fstrUserName)
            Param(21).Value = Trim(fstrStatus)
            Param(22).Value = Trim(fstrServiceType)

            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            Return ds
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrUserName, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsRPTSVOS.vb")
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrUserName, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsRPTSVOS.vb")

       
        Finally
            selConnection.Close()
            selConn.Close()
        End Try
    End Function
#End Region
End Class
