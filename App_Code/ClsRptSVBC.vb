﻿Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web
Imports BusinessEntity
Imports SQLDataAccess

Public Class ClsRptSVBC
#Region "Declaration"

    Private fstrSpName As String ' store procedure name
    Private fstrusername As String ' username

    Private fstrminstaid As String
    Private fstrmaxstaid As String

    Private fstrminserid As String
    Private fstrmaxserid As String

    Private fstrminivdate As System.DateTime ' store procedure name
    Private fstrmaxivdate As System.DateTime

    Private fstrmintecid As String
    Private fstrmaxtecid As String

    Private fstrminsubtime As System.DateTime
    Private fstrmaxsubtime As System.DateTime ' store procedure name
 

    Private fstrmincusid As String
    Private fstrmaxcusid As String

    Private fstrsortby As String
    Private fstrthenby As String
    Private fstrcancel As String

    Private fstrsortby1 As String
    Private fstrthenby1 As String
    Private fstrcancel1 As String

    Private fstrctrid As String
    Private fstrcomid As String
    Private fstrsvcid As String
    Private fstrrank As String

    Private fstrServiceBillType As String
    Private fstrServiceBillTypeText As String

#End Region
#Region "Properties"
    Public Property ServiceBillTypeText() As String
        Get
            Return fstrServiceBillTypeText
        End Get
        Set(ByVal Value As String)
            fstrServiceBillTypeText = Value
        End Set
    End Property

    Public Property ServiceBillType() As String
        Get
            Return fstrServiceBillType
        End Get
        Set(ByVal Value As String)
            fstrServiceBillType = Value
        End Set
    End Property

    Public Property username() As String
        Get
            Return fstrusername
        End Get
        Set(ByVal Value As String)
            fstrusername = Value
        End Set
    End Property
    Public Property ctrid() As String
        Get
            Return fstrctrid
        End Get
        Set(ByVal Value As String)
            fstrctrid = Value
        End Set
    End Property
    Public Property comid() As String
        Get
            Return fstrcomid
        End Get
        Set(ByVal Value As String)
            fstrcomid = Value
        End Set
    End Property
    Public Property svcid() As String
        Get
            Return fstrsvcid
        End Get
        Set(ByVal Value As String)
            fstrsvcid = Value
        End Set
    End Property
    Public Property rank() As String
        Get
            Return fstrrank
        End Get
        Set(ByVal Value As String)
            fstrrank = Value
        End Set
    End Property

    Public Property Minstaid() As String
        Get
            Return fstrminstaid
        End Get
        Set(ByVal Value As String)
            fstrminstaid = Value
        End Set
    End Property

    Public Property Maxstaid() As String
        Get
            Return fstrmaxstaid
        End Get
        Set(ByVal Value As String)
            fstrmaxstaid = Value
        End Set
    End Property

    Public Property Minserid() As String
        Get
            Return fstrminserid
        End Get
        Set(ByVal Value As String)
            fstrminserid = Value
        End Set
    End Property

    Public Property Maxserid() As String
        Get
            Return fstrmaxserid
        End Get
        Set(ByVal Value As String)
            fstrmaxserid = Value
        End Set
    End Property

    Public Property Minivdate() As String
        Get
            Return fstrminivdate
        End Get
        Set(ByVal Value As String)
            fstrminivdate = Value
        End Set
    End Property

    Public Property Maxivdate() As String
        Get
            Return fstrmaxivdate
        End Get
        Set(ByVal Value As String)
            fstrmaxivdate = Value
        End Set
    End Property

    Public Property Minsubtime() As String
        Get
            Return fstrminsubtime
        End Get
        Set(ByVal Value As String)
            fstrminsubtime = Value
        End Set
    End Property

    Public Property Maxsubtime() As String
        Get
            Return fstrmaxsubtime
        End Get
        Set(ByVal Value As String)
            fstrmaxsubtime = Value
        End Set
    End Property
    Public Property Mincusid() As String
        Get
            Return fstrmincusid
        End Get
        Set(ByVal Value As String)
            fstrmincusid = Value
        End Set
    End Property
    Public Property Maxcusid() As String
        Get
            Return fstrmaxcusid
        End Get
        Set(ByVal Value As String)
            fstrmaxcusid = Value
        End Set
    End Property
    Public Property Mintecid() As String
        Get
            Return fstrmintecid
        End Get
        Set(ByVal Value As String)
            fstrmintecid = Value
        End Set
    End Property
    Public Property Maxtecid() As String
        Get
            Return fstrmaxtecid
        End Get
        Set(ByVal Value As String)
            fstrmaxtecid = Value
        End Set
    End Property


    Public Property Thenby() As String
        Get
            Return fstrthenby
        End Get
        Set(ByVal Value As String)
            fstrthenby = Value
        End Set
    End Property
    Public Property Sortby() As String
        Get
            Return fstrsortby
        End Get
        Set(ByVal Value As String)
            fstrsortby = Value
        End Set
    End Property
    Public Property Cancel() As String
        Get
            Return fstrcancel
        End Get
        Set(ByVal Value As String)
            fstrcancel = Value
        End Set
    End Property


    Public Property Thenby1() As String
        Get
            Return fstrthenby1
        End Get
        Set(ByVal Value As String)
            fstrthenby1 = Value
        End Set
    End Property
    Public Property Sortby1() As String
        Get
            Return fstrsortby1
        End Get
        Set(ByVal Value As String)
            fstrsortby1 = Value
        End Set
    End Property
    Public Property Cancel1() As String
        Get
            Return fstrcancel1
        End Get
        Set(ByVal Value As String)
            fstrcancel1 = Value
        End Set
    End Property

#End Region
#Region "Methods"
#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)

        connection.Open()

        Return connection
    End Function
#End Region
#Region "Report SVBC"

    Public Function GetRptSVBT() As DataSet

        Dim selConnection As SqlConnection = Nothing
        Dim selConn As SqlConnection = Nothing
        selConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_RPTSVBC_VIEW"
        Try
            trans = selConnection.BeginTransaction()


            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(20) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrminstaid)
            Param(1).Value = Trim(fstrmaxstaid)
            Param(2).Value = Trim(fstrminserid)
            Param(3).Value = Trim(fstrmaxserid)
            Param(4).Value = Trim(fstrminivdate)
            Param(5).Value = Trim(fstrmaxivdate)
            Param(6).Value = Trim(fstrmintecid)
            Param(7).Value = Trim(fstrmaxtecid)
            Param(8).Value = Trim(fstrminsubtime)
            Param(9).Value = Trim(fstrmaxsubtime)
            Param(10).Value = Trim(fstrmincusid)
            Param(11).Value = Trim(fstrmaxcusid)
            Param(12).Value = Trim(fstrsortby)
            Param(13).Value = Trim(fstrthenby)
            Param(14).Value = Trim(fstrcancel)
            Param(15).Value = Trim(fstrctrid)
            Param(16).Value = Trim(fstrcomid)
            Param(17).Value = Trim(fstrsvcid)
            Param(18).Value = Trim(fstrrank)
            Param(19).Value = Trim(fstrServiceBillType)

            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)


            Return ds

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "ClsRptCVBT.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "ClsRptCVBT.vb")
        Finally
            selConnection.Close()
            selConn.Close()
        End Try

    End Function
#End Region
#End Region
End Class
