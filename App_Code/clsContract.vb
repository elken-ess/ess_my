Imports System.Data.SqlClient
Imports SQLDataAccess
Imports System.Configuration
Imports BusinessEntity
Imports System.data
Public Class clsContract
#Region "Declaration"
    Private fstrSpName As String ' store procedure name
    Private fstrcntNo As String
    Private fstrservCID As String
    Private fstrcntStatus As String
    Private fstrROID As String
    Private fstrcntTypeID As String
    Private fstrCustomPf As String
    Private fstrCustomID As String
    Private fstrcntStartDate As String
    Private fstrcntEndDate As String
    Private fstrsignedBy As String
    Private fstrSMSRem As String
    Private fstrcntNotes As String
    Private fstrServDate As String
    Private fstrActualServDate As String
    Private fstrTransType As String
    Private fstrTransNo As String
    Private fstrcnt2Status As String
    Private fstrIPAddress As String
    Private fstrUserSvcID As String
    Private fstrCustomName As String
    Private fstrCustomTelNo As String
    Private FSTRCOUNTRY As String
    Private fstrPreCntNo As String
    Private fstrSerialNo As String
    Private fstrSessionID As String
    Private fstrResetFlag As String
    Private fintfrequency As Integer
    Private fintNoOfYear As Integer
    Private fintMonthDistance As Integer
    Private _ccnumber As String
    Private _ccexpiry As String
    Private _initialamt As Decimal
    Private _monthlyamt As Decimal
    Private _ccname As String
    Private _cardtype As String
    Private _bankname As String

    Dim selConnection As SqlConnection = Nothing
    Dim selConn As SqlConnection = Nothing
    Dim trans As SqlTransaction = Nothing
#End Region
#Region "Properties"
    Public Property MonthDistance() As String
        Get
            Return fintMonthDistance
        End Get
        Set(ByVal value As String)
            fintMonthDistance = value
        End Set
    End Property

    Public Property Frequency() As String
        Get
            Return fintfrequency
        End Get
        Set(ByVal value As String)
            fintfrequency = value
        End Set
    End Property
    Public Property NoOfYear() As String
        Get
            Return fintNoOfYear
        End Get
        Set(ByVal value As String)
            fintNoOfYear = value
        End Set
    End Property

    Public Property ResetFlag() As String
        Get
            Return fstrResetFlag
        End Get
        Set(ByVal value As String)
            fstrResetFlag = value
        End Set
    End Property

    Public Property SessionID() As String
        Get
            Return fstrSessionID
        End Get
        Set(ByVal value As String)
            fstrSessionID = value
        End Set
    End Property

    Public Property COUNTRY() As String
        Get
            Return FSTRCOUNTRY
        End Get
        Set(ByVal value As String)
            FSTRCOUNTRY = value
        End Set
    End Property
    Public WriteOnly Property ContractNO() As String
        'Get
        '    Return fstrcntNo
        'End Get
        Set(ByVal Value As String)
            fstrcntNo = Value
        End Set
    End Property
    Public WriteOnly Property ServerCenterID() As String
        'Get
        '    Return fstrservCID
        'End Get
        Set(ByVal Value As String)
            fstrservCID = Value
        End Set
    End Property
    Public WriteOnly Property ContractStatus() As String
        'Get
        '    Return fstrcntStatus
        'End Get
        Set(ByVal Value As String)
            fstrcntStatus = Value
        End Set
    End Property
    Public WriteOnly Property ROID() As String
        'Get
        '    Return fstrROID
        'End Get
        Set(ByVal Value As String)
            fstrROID = Value
        End Set
    End Property
    Public WriteOnly Property ContractType() As String
        'Get
        '    Return fstrcntTypeID
        'End Get
        Set(ByVal Value As String)
            fstrcntTypeID = Value
        End Set
    End Property
    Public WriteOnly Property CustomerPrifx() As String
        'Get
        '    Return fstrCustomPf
        'End Get
        Set(ByVal Value As String)
            fstrCustomPf = Value
        End Set
    End Property
    Public WriteOnly Property CustomerID() As String
        'Get
        '    Return fstrCustomID 
        'End Get
        Set(ByVal Value As String)
            fstrCustomID = Value
        End Set
    End Property
    Public WriteOnly Property ContractStartDate() As String
        'Get
        '    Return fstrcntStartDate
        'End Get
        Set(ByVal Value As String)
            fstrcntStartDate = Value
        End Set
    End Property
    Public WriteOnly Property ContractEndDate() As String
        'Get
        '    Return fstrcntEndDate
        'End Get
        Set(ByVal Value As String)
            fstrcntEndDate = Value
        End Set
    End Property
    Public WriteOnly Property SignedBy() As String
        'Get
        '    Return fstrsignedBy
        'End Get
        Set(ByVal Value As String)
            fstrsignedBy = Value
        End Set
    End Property
    Public WriteOnly Property SMSReminder() As String
        'Get
        '    Return fstrSMSRem
        'End Get
        Set(ByVal Value As String)
            fstrSMSRem = Value
        End Set
    End Property
    Public WriteOnly Property ContractNotes() As String
        'Get
        '    Return fstrcntNotes
        'End Get
        Set(ByVal Value As String)
            fstrcntNotes = Value
        End Set
    End Property
    Public WriteOnly Property ServiceDate() As String
        'Get
        '    Return fstrServDate
        'End Get
        Set(ByVal Value As String)
            fstrServDate = Value
        End Set
    End Property
    Public WriteOnly Property ActualServiceDate() As String
        'Get
        '    Return fstrActualServDate
        'End Get
        Set(ByVal Value As String)
            fstrActualServDate = Value
        End Set
    End Property
    Public WriteOnly Property TransactionType() As String
        'Get
        '    Return fstrTransType
        'End Get
        Set(ByVal Value As String)
            fstrTransType = Value
        End Set
    End Property
    Public WriteOnly Property TransactionNo() As String
        'Get
        '    Return fstrTransNo
        'End Get
        Set(ByVal Value As String)
            fstrTransNo = Value
        End Set
    End Property
    Public WriteOnly Property CNT2Status() As String
        'Get
        '    Return fstrcnt2Status
        'End Get
        Set(ByVal Value As String)
            fstrcnt2Status = Value
        End Set
    End Property
    Public WriteOnly Property IpAddress() As String
        'Get
        '    Return fstrIPAddress
        'End Get
        Set(ByVal Value As String)
            fstrIPAddress = Value
        End Set
    End Property
    Public WriteOnly Property UserServCenterID() As String
        'Get
        '    Return fstrUserSvcID
        'End Get
        Set(ByVal Value As String)
            fstrUserSvcID = Value
        End Set
    End Property
    Public WriteOnly Property CustomName() As String
        'Get
        '    Return fstrCustomName
        'End Get
        Set(ByVal Value As String)
            fstrCustomName = Value
        End Set
    End Property
    Public WriteOnly Property TelphoneNo() As String
        'Get
        '    Return fstrCustomTelNo
        'End Get
        Set(ByVal Value As String)
            fstrCustomTelNo = Value
        End Set
    End Property
    Public WriteOnly Property PreContractNo() As String
        'Get
        '    Return fstrPreCntNo
        'End Get
        Set(ByVal Value As String)
            fstrPreCntNo = Value
        End Set
    End Property
    Public WriteOnly Property CCNumber() As String
        'Get
        '    Return fstrPreCntNo
        'End Get
        Set(ByVal Value As String)
            _ccnumber = Value
        End Set
    End Property
    Public WriteOnly Property CCExpiry() As String
        'Get
        '    Return fstrPreCntNo
        'End Get
        Set(ByVal Value As String)
            _ccexpiry = Value
        End Set
    End Property
    Public WriteOnly Property InitialAmt() As Decimal
        'Get
        '    Return fstrPreCntNo
        'End Get
        Set(ByVal Value As Decimal)
            _initialamt = Value
        End Set
    End Property
    Public WriteOnly Property MonthlyAmt() As Decimal
        'Get
        '    Return fstrPreCntNo
        'End Get
        Set(ByVal Value As Decimal)
            _monthlyamt = Value
        End Set
    End Property

    Public WriteOnly Property SerialNo() As String

        Set(ByVal Value As String)
            fstrSerialNo = Value
        End Set
    End Property
    Public WriteOnly Property CCName() As String

        Set(ByVal Value As String)
            _ccname = Value
        End Set
    End Property
    Public WriteOnly Property cardtype() As String

        Set(ByVal Value As String)
            _cardtype = Value
        End Set
    End Property
    Public WriteOnly Property bankname() As String

        Set(ByVal Value As String)
            _bankname = Value
        End Set
    End Property
#End Region


#Region "Methods"
#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)
        connection.Open()
        Return connection
    End Function
#End Region
#Region "Methods help" '类中public方法的辅助方法
    Private Sub BeginMethod()
        selConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        trans = selConnection.BeginTransaction()
    End Sub
    Private Sub DealException(ByVal ex As Exception)
        trans.Rollback()
        Dim ErrorLog As ArrayList = New ArrayList
        ErrorLog.Add("Error").ToString()
        ErrorLog.Add(ex.Message).ToString()
        Dim WriteErrLog As New clsLogFile()
        WriteErrLog.ErrorLog("zqw", ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsAppointment.vb")
    End Sub
    Private Sub EndMethod()
        selConnection.Close()
        selConn.Close()
    End Sub
#End Region
    '根据不同的模糊查询条件返回不同的contracts
#Region "GetContracts"
    Public Function GetContracts(ByVal strUserID As String) As DataSet

        fstrSpName = "BB_FNCCONU_View"
        Dim CoverEntity As New clsCommonClass
        Try
            BeginMethod()
            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(6) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(strUserID)
            Param(1).Value = Trim(fstrcntNo)
            Param(2).Value = Trim(fstrservCID)
            Param(3).Value = Trim(fstrcntStatus)
            Param(4).Value = Trim(fstrCustomName)
            Param(5).Value = Trim(fstrcntTypeID)
            Param(6).Value = Trim(fstrSerialNo)
            Param(7).Value = Trim(fstrCustomID)

            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            ' switch status id to status name for display

            Dim count As Integer
            Dim statXmlTr As New clsXml
            Dim strPanm As String
            statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            For count = 0 To ds.Tables(0).Rows.Count - 1
                Dim statusid As String = ds.Tables(0).Rows(count).Item("FCU1_STAT").ToString 'item(6) is status
                statusid = statusid.Trim()
                strPanm = statXmlTr.GetLabelName("StatusMessage", statusid)
                ds.Tables(0).Rows(count).Item("FCU1_STAT") = strPanm
            Next
            Return ds
        Catch ex As Exception
            DealException(ex)
            Return Nothing
        Finally
            EndMethod()
        End Try
    End Function
#End Region
    '获得某个水机的Pre Contract ID
#Region "Pre Contract ID"
    Public Function GetPreContractNO(ByVal CreateDate As Date) As String

        fstrSpName = "BB_FNCCONU_SelPreCntNoByCustom"
        Try
            BeginMethod()
            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(4) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrCustomID)
            Param(1).Value = Trim(fstrCustomPf)
            Param(2).Value = CreateDate
            Param(3).Value = fstrROID

            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            'The previous contract No is displayed the contract no that had been 
            'done previously and not display the contract no that had been done 
            'currenctly. If the customer is the first time to create the contract 
            'service record then the previous contract no is blank.
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(ds.Tables(0).Rows.Count - 1).Item("FCU1_CONID").ToString()
            End If
            Return ""
        Catch ex As Exception
            DealException(ex)
            Return Nothing
        Finally
            EndMethod()
        End Try
    End Function
#End Region
    '获得某中服务类型的信息
#Region "Server Type Infomation"
    Public Function GetContractTypeInfo() As DataSet

        fstrSpName = "BB_FNCCONU_SelByCNTTYPEID"
        Try
            BeginMethod()
            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrcntTypeID)
            Param(1).Value = Trim(FSTRCOUNTRY)

            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            ds.Tables(0).TableName = "MSV1_FIL"
            Return ds
        Catch ex As Exception
            DealException(ex)
            Return Nothing
        Finally
            EndMethod()
        End Try
    End Function
#End Region
    '增加一条Contract并且在FCU2_FIL中添加与这条Contract相关服务的信息
#Region "AddContract"
    Public Function AddContract(ByVal UserID As String, ByVal frequence As Integer, ByVal Monthdistance As Integer) As Boolean

        fstrSpName = "BB_FNCCONU_Add"
        Try
            BeginMethod()
            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(22) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrcntNo).ToUpper()
            Param(1).Value = Trim(fstrROID).ToUpper()
            Param(2).Value = Trim(fstrCustomPf).ToUpper()
            Param(3).Value = Trim(fstrCustomID).ToUpper()
            Param(4).Value = Trim(fstrcntTypeID).ToUpper()
            Param(5).Value = Trim(fstrcntStartDate).ToUpper()
            Param(6).Value = Trim(fstrcntEndDate).ToUpper()
            Param(7).Value = Trim(fstrservCID).ToUpper()
            Param(8).Value = Trim(fstrcntStatus)
            Param(9).Value = Trim(fstrsignedBy).ToUpper()
            Param(10).Value = Trim(fstrSMSRem).ToUpper()
            Param(11).Value = Trim(fstrcntNotes).ToUpper()
            Param(12).Value = Trim(UserID).ToUpper()
            Param(13).Value = Trim(frequence).ToUpper()
            Param(14).Value = Trim(Monthdistance).ToUpper()
            Param(15).Value = Trim(_ccname).toUpper()
            Param(16).Value = Trim(_ccnumber)
            Param(17).Value = Trim(_ccexpiry)
            Param(18).Value = Trim(_initialamt)
            Param(19).Value = Trim(_monthlyamt)
            Param(20).Value = Trim(_cardtype)
            Param(21).Value = Trim(_bankname)

            SqlHelper.ExecuteNonQuery(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            Return True
        Catch ex As Exception
            DealException(ex)
            Return False
        Finally
            EndMethod()
        End Try
    End Function
#End Region
    '更新Contract并写日志
#Region "UpdateContract"
    Public Function UpdateContract(ByVal UserID As String) As Boolean

        fstrSpName = "BB_FNCCONU_Upd"
        Try
            BeginMethod()
            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(26) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrcntNo).ToUpper()
            Param(1).Value = Trim(fstrROID).ToUpper()
            Param(2).Value = Trim(fstrCustomPf).ToUpper()
            Param(3).Value = Trim(fstrCustomID).ToUpper()
            Param(4).Value = Trim(fstrcntTypeID).ToUpper()
            Param(5).Value = Trim(fstrcntStartDate).ToUpper()
            Param(6).Value = Trim(fstrcntEndDate).ToUpper()
            Param(7).Value = Trim(fstrservCID).ToUpper()
            Param(8).Value = Trim(fstrcntStatus)
            Param(9).Value = Trim(fstrsignedBy).ToUpper()
            Param(10).Value = Trim(fstrSMSRem).ToUpper()
            Param(11).Value = Trim(fstrcntNotes).ToUpper()
            Param(12).Value = Trim(UserID).ToUpper()
            Param(13).Value = Trim(fstrIPAddress)
            Param(14).Value = Trim(fstrUserSvcID)
            Param(15).Value = Trim(fstrSessionID)
            Param(16).Value = Trim(fintfrequency)
            Param(17).Value = Trim(fintMonthDistance)
            Param(18).Value = Trim(fintNoOfYear)
            Param(19).Value = Trim(_ccname).ToUpper()
            Param(20).Value = Trim(_ccnumber).ToUpper()
            Param(21).Value = Trim(_ccexpiry).ToUpper()
            Param(22).Value = Trim(_cardtype)
            Param(23).Value = Trim(_bankname)
            Param(24).Value = Trim(_initialamt)
            Param(25).Value = Trim(_monthlyamt)

            SqlHelper.ExecuteNonQuery(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            Return True
        Catch ex As Exception
            DealException(ex)
            Return False
        Finally
            EndMethod()
        End Try
    End Function
#End Region

    '更新Contract Service并写日志
#Region "UpdateContract"
    Public Function UpdateContractService(ByVal UserID As String) As Boolean

        fstrSpName = "BB_FNCCONU_CNTServUpd"
        Try
            BeginMethod()
            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(9) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrcntNo).ToUpper()
            Param(1).Value = Trim(fstrServDate).ToUpper()
            Param(2).Value = Trim(fstrActualServDate).ToUpper()
            Param(3).Value = Trim(fstrTransType).ToUpper()
            Param(4).Value = Trim(fstrTransNo).ToUpper()
            Param(5).Value = Trim(fstrcnt2Status)
            Param(6).Value = Trim(UserID).ToUpper()
            Param(7).Value = Trim(fstrIPAddress)
            Param(8).Value = Trim(fstrUserSvcID)
            Param(9).Value = Trim(fstrSessionID)

            SqlHelper.ExecuteNonQuery(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            Return True
        Catch ex As Exception
            DealException(ex)
            Return False
        Finally
            EndMethod()
        End Try
    End Function
#End Region
    '获得某个Contract的服务信息
#Region "Server  Infomation"
    Public Function GetContractServInfo() As DataSet

        fstrSpName = "BB_FNCCONU_SelCNTServByNO"
        Try
            BeginMethod()
            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(1) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrcntNo)
            Param(1).Value = Trim(fstrcntTypeID)
            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            ds.Tables(0).TableName = "FCU2_FIL"
            Dim count As Integer
            Dim statXmlTr As New clsXml
            Dim strPanm As String
            statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")

            fstrResetFlag = 0

            For count = 0 To ds.Tables("FCU2_FIL").Rows.Count - 1
                ds.Tables("FCU2_FIL").Rows(count).Item("No") = count + 1


                If ds.Tables("FCU2_FIL").Rows(count).Item("FCU2_ASVDT").ToString() = "01/01/1900" Then
                    ds.Tables("FCU2_FIL").Rows(count).Item("FCU2_ASVDT") = ""
                Else
                    If fstrResetFlag = 0 Then
                        fstrResetFlag = 1
                    End If
                End If
            Next
            Return ds
        Catch ex As Exception
            DealException(ex)
            Return Nothing
        Finally
            EndMethod()
        End Try
    End Function
#End Region
    '获得某个Contract的信息
#Region "Server Type Infomation"
    Public Function GetContractByNO() As DataSet

        fstrSpName = "BB_FNCCONU_SelByID"
        Try
            BeginMethod()
            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(1) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrcntNo)
            Param(1).Value = Trim(fstrcntTypeID)

            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            ds.Tables(0).TableName = "FCU1_FIL"
            Return ds
        Catch ex As Exception
            DealException(ex)
            Return Nothing
        Finally
            EndMethod()
        End Try
    End Function

    Public Function GetContractByNO2() As DataSet

        fstrSpName = "BB_FNCCONU_SelByID2"
        Try
            BeginMethod()
            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(1) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrcntNo)

            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            ds.Tables(0).TableName = "FCU1_FIL"
            Return ds
        Catch ex As Exception
            DealException(ex)
            Return Nothing
        Finally
            EndMethod()
        End Try
    End Function
#End Region
    '校验用户输入的信息
#Region "Validator"
    Public Function Validator() As Array
        fstrSpName = "BB_FNCCONU_CNTNUMValidator"
        Try
            BeginMethod()
            Dim Param() As SqlParameter = New SqlParameter(7) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrcntNo)
            Param(1).Value = Trim(fstrCustomID)
            Param(2).Value = Trim(fstrCustomPf)
            Param(3).Value = Trim(fstrcntTypeID)
            Param(4).Value = Trim(fstrcntStartDate)
            Param(5).Value = Trim(fstrROID)
            Param(6).Value = Trim(fstrPreCntNo)

            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            Dim ValidatorArray As Array = Array.CreateInstance(GetType(Boolean), 5)
            ValidatorArray(0) = (Convert.ToInt32(ds.Tables(0).Rows(0).Item(0)) > 0)  'Contract No exists
            ValidatorArray(1) = (Convert.ToInt32(ds.Tables(1).Rows(0).Item(0)) > 0)  'One Customer can't have different Contract Type 
            ValidatorArray(2) = (Convert.ToInt32(ds.Tables(2).Rows(0).Item(0)) > 0) 'service is not all complete
            ValidatorArray(3) = (Convert.ToInt32(ds.Tables(3).Rows(0).Item(0)) > 0)
            ValidatorArray(4) = (Convert.ToInt32(ds.Tables(4).Rows(0).Item(0)) > 0)

            Return ValidatorArray
        Catch ex As Exception
            DealException(ex)
            Return Nothing
        Finally
            EndMethod()
        End Try
    End Function
#End Region
#End Region
End Class
