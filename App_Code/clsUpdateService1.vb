Imports SQLDataAccess
Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data

#Region " Module Amment Hisotry "
'Description     :  the country entity
'History         :  
'Modified Date   :  2006-05-16
'Version         :  v1.0
'Author          :  xjxted


#End Region

Public Class clsUpdateService1

#Region "Declaration"

    Private fstrSOID As String '服务预约号码
    Private fstrSTATUS As String
    Private fstrPhoneNo As String '客户电话
    Private fstrCustomerName As String  '客户姓名
    Private fstrAppointmentFix As String

    Private fstrServiceBillType As String
    Private fstrSERVICEBIL As String
    Private fstrServiceType As String '服务类型
    Private fstrPartCode As String '零件编号
    Private fstrPartName As String '零件名称
    Private fstrUnitPrice As String 'Unit Price
    Private fstrQuantity As Integer  'Quantity
    Private fstrtax As String 'tax
    Private fstrLineAmt As String 'line Amt

    Private fstrActionTaken As String '对员工意见
    Private fstrtechnician As String '技术工人
    Private fstrdiscount As String 'discount
    Private fstrtotal As String 'total
    Private fstrInvoiceTax As String 'InvoiceTax

    Private fstrCash As Decimal  'cash
    Private fstrCredit As Decimal 'credit
    Private fstrcheque As Decimal 'cheque
    Private fstrCreditCard As Decimal 'credit card 
    Private fstrother As Decimal 'other payment

    Private fstrInstallDate As System.DateTime ' 安装时间
    Private fstrModlty As String ' model type
    Private fstrSerialNo As String ' serial no
    Private fstrFreeService As String '  free service
    Private fstrContractID As String
    Private fstrDiscountRemark As String 'DiscountRemark
    Private fstrRemark As String 'remark
    Private fstrSpName As String ' store procedure name
    Private fstrCreatBy As String
    Private fstrMifyBy As String
    Private fcountryid As String

    Private fstrCustomerID As String
    Private fstrCustomerPrefix As String
    Private fstrAppointmentNo As String
    Private fstrAppointmentPrefix As String
    Private fstrManualServiceBillNo As String

    Private fstrStartDate As String
    Private fstrEndDate As String
    Private _RMSNo As String
#End Region

#Region "Property"

    Public Property StartBillDate()
        Get
            Return fstrStartDate
        End Get
        Set(ByVal value)
            fstrStartDate = value
        End Set
    End Property

    Public Property EndBillDate()
        Get
            Return fstrEndDate
        End Get
        Set(ByVal value)
            fstrEndDate = value
        End Set
    End Property

    Public Property AppointmentPrefix()
        Get
            Return fstrAppointmentPrefix
        End Get
        Set(ByVal value)
            fstrAppointmentPrefix = value
        End Set
    End Property

    Public Property CustomerPrefix()
        Get
            Return fstrCustomerPrefix
        End Get
        Set(ByVal value)
            fstrCustomerPrefix = value
        End Set
    End Property

    Public Property AppointmentNo()
        Get
            Return fstrAppointmentNo
        End Get
        Set(ByVal value)
            fstrAppointmentNo = value
        End Set
    End Property


    Public Property ManualServiceBillNo()
        Get
            Return fstrManualServiceBillNo
        End Get
        Set(ByVal value)
            fstrManualServiceBillNo = value
        End Set
    End Property


    Public Property Country()
        Get
            Return fcountryid
        End Get
        Set(ByVal value)
            fcountryid = value
        End Set
    End Property
    Public Property Technician()
        Get
            Return fstrtechnician
        End Get
        Set(ByVal value)
            fstrtechnician = value
        End Set
    End Property

    Public Property SERVICEBIL()
        Get
            Return fstrSERVICEBIL
        End Get
        Set(ByVal value)
            fstrSERVICEBIL = value
        End Set
    End Property
    Public Property SOID()
        Get
            Return fstrSOID
        End Get
        Set(ByVal value)
            fstrSOID = value
        End Set
    End Property
    Public Property AppointmentFix() As String
        Get
            Return fstrAppointmentFix
        End Get
        Set(ByVal value As String)
            fstrAppointmentFix = value
        End Set
    End Property
    Public Property ServiceType()
        Get
            Return fstrServiceType
        End Get
        Set(ByVal value)
            fstrServiceType = value
        End Set
    End Property
    Public Property PartID()
        Get
            Return fstrPartCode
        End Get
        Set(ByVal value)
            fstrPartCode = value
        End Set
    End Property
    Public Property PartName() As String
        Get
            Return fstrPartName
        End Get
        Set(ByVal value As String)
            fstrPartName = value
        End Set
    End Property
    Public Property InstallDate() As String
        Get
            Return fstrInstallDate
        End Get
        Set(ByVal Value As String)
            fstrInstallDate = DateTime.Parse(Value, New System.Globalization.CultureInfo("en-CA")).Date
        End Set
    End Property
    Public Property PhoneNo() As String
        Get
            Return fstrPhoneNo
        End Get
        Set(ByVal value As String)
            fstrPhoneNo = value
        End Set
    End Property
    Public Property CustomerName() As String
        Get
            Return fstrCustomerName
        End Get
        Set(ByVal value As String)
            fstrCustomerName = value
        End Set
    End Property
    Public Property ActionTaken() As String
        Get
            Return fstrActionTaken
        End Get
        Set(ByVal value As String)
            fstrActionTaken = value
        End Set
    End Property
    Public Property Modlty() As String
        Get
            Return fstrModlty
        End Get
        Set(ByVal value As String)
            fstrModlty = value
        End Set
    End Property
    Public Property SerialNo() As String
        Get
            Return fstrSerialNo
        End Get
        Set(ByVal value As String)
            fstrSerialNo = value
        End Set
    End Property
    Public Property FreeService() As String
        Get
            Return fstrFreeService
        End Get
        Set(ByVal value As String)
            fstrFreeService = value
        End Set
    End Property
    Public Property Remark() As String
        Get
            Return fstrRemark
        End Get
        Set(ByVal value As String)
            fstrRemark = value
        End Set
    End Property
    Public Property ContractID() As String
        Get
            Return fstrContractID
        End Get
        Set(ByVal value As String)
            fstrContractID = value
        End Set
    End Property
    Public Property CustomerID() As String
        Get
            Return fstrCustomerID
        End Get
        Set(ByVal value As String)
            fstrCustomerID = value
        End Set
    End Property
    Public Property SpName() As String
        Get
            Return fstrSpName
        End Get
        Set(ByVal value As String)
            fstrSpName = value
        End Set
    End Property
    Public Property STATUS() As String
        Get
            Return fstrSTATUS
        End Get
        Set(ByVal value As String)
            fstrSTATUS = value
        End Set
    End Property
    Public Property ServiceBillType() As String
        Get
            Return fstrServiceBillType
        End Get
        Set(ByVal Value As String)
            fstrServiceBillType = Value
        End Set
    End Property
    Public Property UnitPrice() As String
        Get
            Return fstrUnitPrice
        End Get
        Set(ByVal Value As String)
            fstrUnitPrice = Value
        End Set
    End Property
    Public Property Quantity() As String
        Get
            Return fstrQuantity
        End Get
        Set(ByVal Value As String)
            fstrQuantity = Value
        End Set
    End Property
    Public Property tax() As String
        Get
            Return fstrtax
        End Get
        Set(ByVal Value As String)
            fstrtax = Value
        End Set
    End Property
    Public Property LineAmt() As String
        Get
            Return fstrLineAmt
        End Get
        Set(ByVal Value As String)
            fstrLineAmt = Value
        End Set
    End Property
    Public Property discount() As String
        Get
            Return fstrdiscount
        End Get
        Set(ByVal Value As String)
            fstrdiscount = Value
        End Set
    End Property

    Public Property total() As String
        Get
            Return fstrtotal
        End Get
        Set(ByVal Value As String)
            fstrtotal = Value
        End Set
    End Property
    Public Property InvoiceTax() As String
        Get
            Return fstrInvoiceTax
        End Get
        Set(ByVal Value As String)
            fstrInvoiceTax = Value
        End Set
    End Property
    Public Property Cash() As Decimal
        Get
            Return fstrCash
        End Get
        Set(ByVal Value As Decimal)
            fstrCash = Value
        End Set
    End Property
    Public Property Credit() As Decimal
        Get
            Return fstrCredit
        End Get
        Set(ByVal Value As Decimal)
            fstrCredit = Value
        End Set
    End Property
    Public Property cheque() As Decimal
        Get
            Return fstrcheque
        End Get
        Set(ByVal Value As Decimal)
            fstrcheque = Value
        End Set
    End Property
    Public Property CreditCard() As Decimal
        Get
            Return fstrCreditCard
        End Get
        Set(ByVal Value As Decimal)
            fstrCreditCard = Value
        End Set
    End Property
    Public Property other() As Decimal
        Get
            Return fstrother
        End Get
        Set(ByVal Value As Decimal)
            fstrother = Value
        End Set
    End Property
    Public Property DiscountRemark() As String
        Get
            Return fstrDiscountRemark
        End Get
        Set(ByVal Value As String)
            fstrDiscountRemark = Value
        End Set
    End Property
    Public Property CreatBy() As String
        Get
            Return fstrCreatBy
        End Get
        Set(ByVal Value As String)
            fstrCreatBy = Value
        End Set
    End Property
    Public Property ModifyBy() As String
        Get
            Return fstrMifyBy
        End Get
        Set(ByVal Value As String)
            fstrMifyBy = Value
        End Set
    End Property
    Public Property RMSNo() As String
        Get
            Return _RMSNo
        End Get
        Set(ByVal Value As String)
            _RMSNo = Value
        End Set
    End Property

#End Region

#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)

        connection.Open()

        Return connection
    End Function
#End Region

#Region "查询页面"
    Public Function chaxun() As DataSet
        Dim selConn As SqlConnection = Nothing
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        fstrSpName = "BB_FNCUSB1_View"
        Dim ds As DataSet = New DataSet()
        Try
            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(16) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrSOID)
            Param(1).Value = Trim(fstrCustomerName)
            Param(2).Value = Trim(fstrPhoneNo)
            Param(3).Value = Trim(fstrSerialNo)
            Param(4).Value = Trim(fstrtechnician)
            Param(5).Value = Trim(fstrSTATUS)
            Param(6).Value = Trim(fcountryid)
            Param(7).Value = Trim(fstrMifyBy)

            Param(8).Value = Trim(fstrCustomerPrefix)
            Param(9).Value = Trim(fstrCustomerID)
            Param(10).Value = Trim(fstrAppointmentPrefix)
            Param(11).Value = Trim(fstrAppointmentNo)
            Param(12).Value = Trim(fstrManualServiceBillNo)

            Param(13).Value = Trim(fstrStartDate)
            Param(14).Value = Trim(fstrEndDate)
            Param(15).Value = Trim(fstrRemark)
            Param(16).Value = Trim(fstrAppointmentFix)
            Param(17).Value = Trim(_RMSNo)

            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)

            ' switch status id to status name for display

            Dim count As Integer
            Dim statXmlTr As New clsXml
            Dim strPanm As String
            statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            For count = 0 To ds.Tables(0).Rows.Count - 1
                Dim statusid As String = ds.Tables(0).Rows(count).Item(4).ToString 'item(4) is status
                strPanm = statXmlTr.GetLabelName("StatusMessage", statusid)
                ds.Tables(0).Rows(count).Item(4) = strPanm
            Next

        Catch ex As SqlException

        Finally
            selConn.Close()
        End Try
        Return ds
    End Function
#End Region

#Region "修改页面显示"
    Public Function SelByID(ByVal transType As String, ByVal transNo As String) As DataSet
        Dim selConn As SqlConnection = Nothing
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        fstrSpName = "BB_FNCUSB1_SelByID"
        Dim ds As DataSet = New DataSet()
        Try
            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(transType)
            Param(1).Value = Trim(transNo)
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
        Finally
            selConn.Close()
        End Try
        Return ds         '返回一个数据集，三张表
    End Function
#End Region

    '#Region "增加"

    'Public Function Insert() As Integer

    '    Dim insConnection As SqlConnection = Nothing
    '    Dim insConn As SqlConnection = Nothing
    '    insConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
    '    insConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
    '    Dim trans As SqlTransaction = Nothing
    '    fstrSpName = "BB_FNCUSB1_Add1"
    '    Try
    '        trans = insConnection.BeginTransaction()

    '        Dim Param() As SqlParameter = New SqlParameter(30) {}
    '        Param = SqlHelperParameterCache.GetSpParameterSet(insConn, fstrSpName)
    '        'Param(0).Value = Trim()
    '        'Param(1).Value = Trim()
    '        'Param(2).Value = Trim()
    '        'Param(3).Value = Trim()
    '        'Param(4).Value = Trim()
    '        'Param(5).Value = Trim()
    '        'Param(6).Value = Trim()
    '        'Param(7).Value = Trim()
    '        'Param(8).Value = Trim()
    '        'Param(9).Value = Trim()
    '        'Param(10).Value = Trim()
    '        'Param(11).Value = Trim()
    '        'Param(12).Value = Trim()
    '        'Param(13).Value = Trim()
    '        'Param(14).Value = Trim()
    '        'Param(15).Value = Trim()
    '        'Param(16).Value = Trim()
    '        'Param(17).Value = Trim()
    '        'Param(18).Value = Trim()
    '        'Param(19).Value = Trim()
    '        'Param(20).Value = Trim()
    '        'Param(21).Value = Trim()
    '        'Param(22).Value = Trim()
    '        'Param(23).Value = Trim()
    '        'Param(24).Value = Trim()
    '        'Param(25).Value = Trim()
    '        'Param(26).Value = Trim()
    '        'Param(27).Value = Trim()
    '        Param(28).Value = Trim(fstrCreatBy) '创建者
    '        Param(29).Value = Trim(fstrMifyBy) '最后修改者
    '        Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(insConn, CommandType.StoredProcedure, fstrSpName, Param)

    '        trans.Commit()
    '        Return 0
    '    Catch ex As Exception
    '        trans.Rollback()
    '        Dim ErrorLog As ArrayList = New ArrayList
    '        ErrorLog.Add("Error").ToString()
    '        ErrorLog.Add(ex.Message).ToString()
    '        Dim WriteErrLog As New clsLogFile()
    '        WriteErrLog.ErrorLog("wang", ErrorLog.Item(1).ToString, WriteErrLog.ADD, "clsCountry.vb")
    '        Return -1

    '    Catch ex As SqlException
    '        trans.Rollback()
    '        Dim ErrorLog As ArrayList = New ArrayList
    '        ErrorLog.Add("Error").ToString()
    '        ErrorLog.Add(ex.Number).ToString()
    '        Dim WriteErrLog As New clsLogFile()
    '        WriteErrLog.ErrorLog("wang", ErrorLog.Item(1).ToString, WriteErrLog.ADD, "clsCountry.vb")
    '        Return -1
    '    Finally
    '        insConnection.Close()
    '        insConn.Close()
    '    End Try

    'End Function

    '#End Region

#Region "添加支付数额"
    Public Function Insert(ByVal transType As String, ByVal transNo As String) As Integer


        Dim aConnection As SqlConnection = Nothing
        Dim aConn As SqlConnection = Nothing
        aConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        aConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_FNCUSB1_Add1"
        Try
            trans = aConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(8) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(aConn, fstrSpName)
            Param(0).Value = Trim(transType)
            Param(1).Value = Trim(transNo)
            Param(2).Value = Trim(fstrCash)
            Param(3).Value = Trim(fstrCredit)
            Param(4).Value = Trim(fstrcheque)
            Param(5).Value = Trim(fstrCreditCard)
            Param(6).Value = Trim(fstrother)
            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(aConn, CommandType.StoredProcedure, fstrSpName, Param)

            trans.Commit()
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("wang", ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsUpdateService1.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("wang", ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsUpdateService1.vb")
            Return -1
        Finally
            aConnection.Close()
            aConn.Close()
        End Try

    End Function
#End Region

#Region "update"
    Public Function update(ByVal transType As String, ByVal transNo As String) As Integer


        Dim aConnection As SqlConnection = Nothing
        Dim aConn As SqlConnection = Nothing
        aConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        aConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_FNCUSB1_Upd"
        Try
            trans = aConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(4) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(aConn, fstrSpName)
            Param(0).Value = Trim(transType)
            Param(1).Value = Trim(transNo)
            Param(2).Value = Trim(fstrQuantity)
            'Param(3).Value = Trim(fstrLineAmt)
            Param(3).Value = Trim(fstrMifyBy)
            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(aConn, CommandType.StoredProcedure, fstrSpName, Param)
            trans.Commit()
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("xjx", ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsUpdateService1.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("xjx", ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsUpdateService1.vb")
            Return -1
        Finally
            aConnection.Close()
            aConn.Close()
        End Try

    End Function
#End Region

#Region "update1"
    Public Function update1(ByVal transType As String, ByVal transNo As String) As Integer


        Dim aConnection As SqlConnection = Nothing
        Dim aConn As SqlConnection = Nothing
        aConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        aConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_FNCUSB1_Upd1"
        Try
            trans = aConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(10) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(aConn, fstrSpName)
            Param(0).Value = Trim(transType)
            Param(1).Value = Trim(transNo)
            Param(2).Value = Trim(fstrSERVICEBIL)
            Param(3).Value = Trim(fstrServiceType)
            Param(4).Value = Trim(fstrtechnician)
            Param(5).Value = Trim(fstrdiscount)
            Param(6).Value = Trim(fstrtotal)
            Param(7).Value = Trim(fstrInvoiceTax)
            Param(8).Value = Trim(fstrDiscountRemark)
            Param(9).Value = Trim(fstrMifyBy)


            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(aConn, CommandType.StoredProcedure, fstrSpName, Param)

            trans.Commit()
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("wang", ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsUpdateService1.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("wang", ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.ADD, "clsUpdateService1.vb")
            Return -1
        Finally
            aConnection.Close()
            aConn.Close()
        End Try

    End Function
#End Region

#Region "get status message name"
    Public Function searchmoney(ByVal transType As String, ByVal transno As String) As DataTable

        Dim statConnection As SqlConnection = Nothing
        Dim statConn As SqlConnection = Nothing
        statConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        statConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_FNCUSB2_STATUS"

        Dim ds As DataSet = New DataSet()
        Try
            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(statConn, fstrSpName)
            Param(0).Value = Trim(transType)
            Param(1).Value = Trim(transno)
            ds = SqlHelper.ExecuteDataset(statConn, CommandType.StoredProcedure, fstrSpName, Param)
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("xjx", ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsUpdateService1.vb")
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("xjx", ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsUpdateService1.vb")
        Finally
            statConn.Close()
            statConnection.Close()
        End Try

        Dim statXmlTr As New clsXml
        statXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        Dim strPaid As String
        Dim strpayam As String
        Dim infon As New DataTable
        Dim newrow As DataRow
        infon.Columns.Add("PayID", System.Type.GetType("System.String"))
        infon.Columns.Add("PayAM", System.Type.GetType("System.String"))

        For i As Integer = 0 To ds.Tables(1).Rows.Count - 1
            strPaid = ds.Tables(1).Rows(i).Item(0).ToString()
            strpayam = ds.Tables(1).Rows(i).Item(1).ToString()
            newrow = infon.Rows.Add()
            newrow.Item(0) = strPaid
            newrow.Item(1) = strpayam
        Next

        Return infon

    End Function
#End Region
End Class

