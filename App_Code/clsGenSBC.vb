Imports System.Data.SqlClient
Imports System.Configuration
Imports SQLDataAccess
Imports BusinessEntity
Imports System.Web
Imports System.Data

Public Class clsGenSBC

#Region "Declaration.................................................."
    Private fInvoiceDate As Date
    Private fTechnician As String
    Private fServiceCenterID As String
    Private fCreateBy As String
    Private fCreateDate As DateTime
    Private fModifyBy As String
    Private fModifyDate As DateTime

    Private fstrlogctrid As String
    Private fstrlogcompanyid As String
    Private fstrlogserviceid As String
    Private fstrlogrank As String
    Private fstrloguserid As String
#End Region


    'PROPERTIES
#Region "Properties..................................................."
    Public Property InvoiceDate() As Date
        Get
            Return fInvoiceDate
        End Get
        Set(ByVal value As Date)
            fInvoiceDate = value
        End Set
    End Property
    Public Property Technician() As String
        Get
            Return fTechnician
        End Get
        Set(ByVal value As String)
            fTechnician = value
        End Set
    End Property
    Public Property ServiceCenter() As String
        Get
            Return fServiceCenterID
        End Get
        Set(ByVal value As String)
            fServiceCenterID = value
        End Set
    End Property

    Public Property logctrid() As String
        Get
            Return fstrlogctrid
        End Get
        Set(ByVal Value As String)
            fstrlogctrid = Value
        End Set
    End Property
    Public Property logcompanyid() As String
        Get
            Return fstrlogcompanyid
        End Get
        Set(ByVal Value As String)
            fstrlogcompanyid = Value
        End Set
    End Property
    Public Property logserviceid() As String
        Get
            Return fstrlogserviceid
        End Get
        Set(ByVal Value As String)
            fstrlogserviceid = Value
        End Set
    End Property
    Public Property logrank() As String
        Get
            Return fstrlogrank
        End Get
        Set(ByVal Value As String)
            fstrlogrank = Value
        End Set
    End Property
    Public Property loguserid() As String
        Get
            Return fstrloguserid
        End Get
        Set(ByVal Value As String)
            fstrloguserid = Value
        End Set
    End Property
#End Region

    Public Function CheckSBC() As Integer
        Dim fstrSPName As String
        Dim scnTran As SqlConnection
        Dim scnProc As SqlConnection
        scnTran = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        scnProc = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        scnTran.Open()
        scnProc.Open()

        Dim trans As SqlTransaction = Nothing
        fstrSPName = "BB_FNC_SBCCHECK"
        Try
            trans = scnTran.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(scnProc, fstrSPName)
            Param(0).Value = Me.fTechnician
            Param(1).Value = 0

            SqlHelper.ExecuteNonQuery(scnProc, CommandType.StoredProcedure, fstrSPName, Param)
            Dim spCounter = Param(1).Value
            scnTran.Close()
            scnProc.Close()

            Return spCounter

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fModifyBy, ErrorLog.Item(1).ToString, fstrSPName, WriteErrLog.ADD, "clsGenCBCL.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fModifyBy, ErrorLog.Item(1).ToString, fstrSPName, WriteErrLog.ADD, "clsGenCBCL.vb")
            Return -1
        End Try
    End Function

    Public Function GenerateSBC() As String
        Dim fstrSPName As String
        Dim scnTran As SqlConnection
        Dim scnProc As SqlConnection
        scnTran = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        scnProc = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        scnTran.Open()
        scnProc.Open()

        Dim trans As SqlTransaction = Nothing
        fstrSPName = "BB_FNCGEN_SBC"
        Try
            trans = scnTran.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(4) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(scnProc, fstrSPName)
            Param(0).Value = Me.fServiceCenterID
            Param(1).Value = Me.fTechnician
            Param(2).Value = Me.loguserid
            Param(3).Value = ""

            SqlHelper.ExecuteNonQuery(scnProc, CommandType.StoredProcedure, fstrSPName, Param)
            Dim rtnCBCKNo As String
            rtnCBCKNo = Param(3).Value

            scnTran.Close()
            scnProc.Close()

            Return rtnCBCKNo

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fModifyBy, ErrorLog.Item(1).ToString, fstrSPName, WriteErrLog.ADD, "clsGenPRF.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fModifyBy, ErrorLog.Item(1).ToString, fstrSPName, WriteErrLog.ADD, "clsGenPRF.vb")
            Return -1
        End Try
    End Function
End Class
