Imports BusinessEntity
Imports System.Data.SqlClient
Imports System.Data
Imports System.Collections.Generic
Imports SQLDataAccess

Public Class clsCalIncenforTechNew

#Region "Declaration"

    'Private strUserID As String                         'User Login ID
    'Private strFrDate As String                         'From Date 
    'Private strToDate As String                         'To Date 
    'Private strStaffTy As String                        'Staff Type
    'Private strFrStaffCode As String                    'From Staff Code
    'Private strToStaffCode As String                    'To Staff Code
    'Private strServiceTy As String                      'Service Type
    'Private strFrSerCent As String                      'From Service Center
    'Private strToSerCent As String                      'To Service Center
    'Private strSerStats As String                       'Service Status 

    Private _strUserID As String                         'User Login ID
    Private _strFrDate As String                         'From Date 
    Private _strToDate As String                         'To Date 
    Private _strStaffTy As String                        'Staff Type
    'Private _staffList As List(Of String)
	Private _staffList As String
    Private _strSerStats As String
    Private _strFrSerCent As String                      'From Service Center
    Private _strToSerCent As String                      'To Service Center

    Private _strSpName As String
    Private _staffstatus As String


#End Region

#Region "Properties"
    Public Property StaffList() As String'As List(Of String)
        Get
            Return _staffList
        End Get
        Set(ByVal value As String)'List(Of String))
            _staffList = value
        End Set
    End Property

    Public Property StrUserID() As String
        Get
            Return _strUserID
        End Get
        Set(ByVal value As String)
            _strUserID = value
        End Set
    End Property

    Public Property StrFrDate() As String
        Get
            Return _strFrDate
        End Get
        Set(ByVal value As String)
            _strFrDate = value
        End Set
    End Property

    Public Property StrToDate() As String
        Get
            Return _strToDate
        End Get
        Set(ByVal value As String)
            _strToDate = value
        End Set
    End Property

    Public Property StrStaffTy() As String
        Get
            Return _strStaffTy
        End Get
        Set(ByVal value As String)
            _strStaffTy = value
        End Set
    End Property

    Public Property StrSerStats() As String
        Get
            Return _strSerStats
        End Get
        Set(ByVal value As String)
            _strSerStats = value
        End Set
    End Property

    Public Property StrFrSerCent() As String
        Get
            Return _strFrSerCent
        End Get
        Set(ByVal value As String)
            _strFrSerCent = value
        End Set
    End Property

    Public Property StrToSerCent() As String
        Get
            Return _strToSerCent
        End Get
        Set(ByVal value As String)
            _strToSerCent = value
        End Set
    End Property

    Public Property StaffStatus() As String
        Get
            Return _staffstatus
        End Get
        Set(ByVal value As String)
            _staffstatus = value
        End Set
    End Property


#End Region

#Region "Methods"

#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)

        connection.Open()

        Return connection
    End Function
#End Region

#Region "Seach and Display the Information"
    Public Function GetIncentiveInfo() As DataSet

        Dim selConn As SqlConnection = Nothing
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim ds As New DataSet()

        Dim dtIncen As New DataTable("Incentives")
        dtIncen.Columns.Add("FIV1_SVCID")
        dtIncen.Columns.Add("StaffIDandName")
        dtIncen.Columns.Add("MF", GetType(Integer))
        dtIncen.Columns.Add("MM", GetType(Integer))
        dtIncen.Columns.Add("MB", GetType(Integer))
        'dtIncen.Columns.Add("MR", GetType(Integer))
        dtIncen.Columns.Add("MC", GetType(Integer)) ''' Changed from "Contract" to "MC"
        dtIncen.Columns.Add("ML", GetType(Integer))
        dtIncen.Columns.Add("ML2", GetType(Integer))
        dtIncen.Columns.Add("PL", GetType(Integer))
        dtIncen.Columns.Add("PB", GetType(Integer))
        dtIncen.Columns.Add("PM", GetType(Integer))
        dtIncen.Columns.Add("SP", GetType(Integer))
        dtIncen.Columns.Add("CV", GetType(Integer))
        dtIncen.Columns.Add("MS", GetType(Integer))
        dtIncen.Columns.Add("CP", GetType(Integer))
        'dtIncen.Columns.Add("RI", GetType(Integer))
        'dtIncen.Columns.Add("Rental", GetType(Integer))
        dtIncen.Columns.Add("RP", GetType(Integer))
        dtIncen.Columns.Add("TOTALJOB", GetType(Decimal))
        dtIncen.Columns.Add("TOTALSALES", GetType(Decimal))

        _strSpName = "BB_FNCCINC_View_NEW"
        Try
            ' define search fields

            If _staffList <> "" Then
                'For Each staffCode As String In _staffList
                    Dim Param() As SqlParameter = New SqlParameter(8) {}
                    Param = SqlHelperParameterCache.GetSpParameterSet(selConn, _strSpName)

                    Param(0).Value = Trim(_strUserID)
                    Param(1).Value = Trim(_strFrDate)
                    Param(2).Value = Trim(_strToDate)
                    Param(3).Value = Trim(_staffList)
                    Param(4).Value = Trim(_strFrSerCent)
                    Param(5).Value = Trim(_strToSerCent)
                    Param(6).Value = Trim(_strStaffTy)
                    Param(7).Value = Trim(_strSerStats)

                    Dim dt As DataTable = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, _strSpName, Param).Tables(0)
                    Dim dr As DataRow() = dt.Select()

                    For Each row As DataRow In dr
                        dtIncen.ImportRow(row)
                    Next
                'Next
                ' Added by Ryan Estandarte 30 Oct 2012
            Else
                Dim Param() As SqlParameter = New SqlParameter(8) {}
                Param = SqlHelperParameterCache.GetSpParameterSet(selConn, _strSpName)

                Param(0).Value = Trim(_strUserID)
                Param(1).Value = Trim(_strFrDate)
                Param(2).Value = Trim(_strToDate)
                Param(3).Value = ""
                Param(4).Value = Trim(_strFrSerCent)
                Param(5).Value = Trim(_strToSerCent)
                Param(6).Value = Trim(_strStaffTy)
                Param(7).Value = Trim(_strSerStats)

                Dim dt As DataTable = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, _strSpName, Param).Tables(0)
                Dim dr As DataRow() = dt.Select()

                For Each row As DataRow In dr
                    dtIncen.ImportRow(row)
                Next
            End If


            ds.Tables.Add(dtIncen)
        Catch ex As SqlException
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("Fan", ErrorLog.Item(1).ToString, _strSpName, WriteErrLog.SELE, _
                                    "clsCalIncenforTech.vb")
            Return Nothing
        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("Fan", ErrorLog.Item(1).ToString, _strSpName, WriteErrLog.SELE, _
                                    "clsCalIncenforTech.vb")
            Return Nothing
        Finally
            selConn.Close()
        End Try

        Return ds
    End Function
#End Region

#Region "Seach and Display the Information 2"
    Public Function GetIncentiveInfo2() As DataSet

        Dim selConn As SqlConnection = Nothing
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim ds As New DataSet()

        Dim dtIncen As New DataTable("Incentives")
        dtIncen.Columns.Add("FIV1_SVCID")
        dtIncen.Columns.Add("StaffIDandName")
        dtIncen.Columns.Add("STAT")
        dtIncen.Columns.Add("MF", GetType(Integer))
        dtIncen.Columns.Add("MM", GetType(Integer))
        dtIncen.Columns.Add("MB", GetType(Integer))
        dtIncen.Columns.Add("MR", GetType(Integer))
        dtIncen.Columns.Add("CONTRACT", GetType(Integer))
        dtIncen.Columns.Add("ML", GetType(Integer))
        dtIncen.Columns.Add("ML2", GetType(Integer))
        dtIncen.Columns.Add("PL", GetType(Integer))
        dtIncen.Columns.Add("PB", GetType(Integer))
        dtIncen.Columns.Add("PM", GetType(Integer))
        dtIncen.Columns.Add("SP", GetType(Integer))
        dtIncen.Columns.Add("CV", GetType(Integer))
        dtIncen.Columns.Add("MS", GetType(Integer))
        dtIncen.Columns.Add("CP", GetType(Integer))
        dtIncen.Columns.Add("RI", GetType(Integer))
        dtIncen.Columns.Add("Rental", GetType(Integer))
        'dtIncen.Columns.Add("RP", GetType(Integer))
        dtIncen.Columns.Add("TOTALJOB", GetType(Decimal))
        dtIncen.Columns.Add("TOTALSALES", GetType(Decimal))

        _strSpName = "BB_FNCCINC_View_NEW2"
        Try
            ' define search fields

            If _staffList <> "" Then
                'For Each staffCode As String In _staffList
                Dim Param() As SqlParameter = New SqlParameter(9) {}
                Param = SqlHelperParameterCache.GetSpParameterSet(selConn, _strSpName)

                Param(0).Value = Trim(_strUserID)
                Param(1).Value = Trim(_strFrDate)
                Param(2).Value = Trim(_strToDate)
                Param(3).Value = Trim(_staffList)
                Param(4).Value = Trim(_strFrSerCent)
                Param(5).Value = Trim(_strToSerCent)
                Param(6).Value = Trim(_strStaffTy)
                Param(7).Value = Trim(_strSerStats)
                Param(8).Value = Trim(_staffstatus)

                Dim dt As DataTable = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, _strSpName, Param).Tables(0)
                Dim dr As DataRow() = dt.Select()

                For Each row As DataRow In dr
                    dtIncen.ImportRow(row)
                Next
                'Next
                ' Added by Ryan Estandarte 30 Oct 2012
            Else
                Dim Param() As SqlParameter = New SqlParameter() {}
                Param = SqlHelperParameterCache.GetSpParameterSet(selConn, _strSpName)

                Param(0).Value = Trim(_strUserID)
                Param(1).Value = Trim(_strFrDate)
                Param(2).Value = Trim(_strToDate)
                Param(3).Value = ""
                Param(4).Value = Trim(_strFrSerCent)
                Param(5).Value = Trim(_strToSerCent)
                Param(6).Value = Trim(_strStaffTy)
                Param(7).Value = Trim(_strSerStats)
                Param(8).Value = Trim(_staffstatus)

                Dim dt As DataTable = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, _strSpName, Param).Tables(0)
                Dim dr As DataRow() = dt.Select()

                For Each row As DataRow In dr
                    dtIncen.ImportRow(row)
                Next
            End If


            ds.Tables.Add(dtIncen)
        Catch ex As SqlException
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("Fan", ErrorLog.Item(1).ToString, _strSpName, WriteErrLog.SELE, _
                                    "clsCalIncenforTech.vb")
            Return Nothing
        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("Fan", ErrorLog.Item(1).ToString, _strSpName, WriteErrLog.SELE, _
                                    "clsCalIncenforTech.vb")
            Return Nothing
        Finally
            selConn.Close()
        End Try

        Return ds
    End Function
#End Region

#Region "Get Export Job Detail Information"
    Public Function GetExportInfo() As DataSet
        Dim ds As New DataSet

        Dim dtExport As New DataTable("Export")
        dtExport.Columns.Add("RMS#")
        dtExport.Columns.Add("Manual Bill #")
        dtExport.Columns.Add("Srvc Center")
        dtExport.Columns.Add("Cust ID")
        dtExport.Columns.Add("Model ID")
        dtExport.Columns.Add("Serial No")
        dtExport.Columns.Add("Date Create")
        dtExport.Columns.Add("Date Serviced")
        dtExport.Columns.Add("CR ID")
        dtExport.Columns.Add("Tech ID")
        dtExport.Columns.Add("SO Type")
        dtExport.Columns.Add("SO Status")
        dtExport.Columns.Add("Bill Amount")
        'dtExport.Columns.Add("CR PT")
        'dtExport.Columns.Add("Tech PT")
        dtExport.Columns.Add("Action Taken")

        Dim selConn As SqlConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        _strSpName = "BB_FNCCINC_Export_New"
        Try
            ' define search fields 
            If _staffList <> "" Then
                'For Each staffCode As String In _staffList
                Dim Param() As SqlParameter = New SqlParameter(8) {}

                Param = SqlHelperParameterCache.GetSpParameterSet(selConn, _strSpName)
                Param(0).Value = Trim(_strUserID)
                Param(1).Value = Trim(_strFrDate)
                Param(2).Value = Trim(_strToDate)
                Param(3).Value = Trim(_staffList)
                Param(4).Value = Trim(_strFrSerCent)
                Param(5).Value = Trim(_strToSerCent)
                Param(6).Value = Trim(_strStaffTy)
                Param(7).Value = Trim(_strSerStats)

                Dim dt As DataTable = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, _strSpName, Param).Tables(0)
                Dim dr As DataRow() = dt.Select()

                For Each row As DataRow In dr
                    dtExport.ImportRow(row)
                Next
                'Next
            Else
                'For Each staffCode As String In _staffList
                Dim Param() As SqlParameter = New SqlParameter(8) {}

                Param = SqlHelperParameterCache.GetSpParameterSet(selConn, _strSpName)
                Param(0).Value = Trim(_strUserID)
                Param(1).Value = Trim(_strFrDate)
                Param(2).Value = Trim(_strToDate)
                Param(3).Value = ""
                Param(4).Value = Trim(_strFrSerCent)
                Param(5).Value = Trim(_strToSerCent)
                Param(6).Value = Trim(_strStaffTy)
                Param(7).Value = Trim(_strSerStats)

                Dim dt As DataTable = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, _strSpName, Param).Tables(0)
                Dim dr As DataRow() = dt.Select()

                For Each row As DataRow In dr
                    dtExport.ImportRow(row)
                Next
                'Next
            End If


            ds.Tables.Add(dtExport)
        Catch ex As SqlException
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(StrUserID, ErrorLog.Item(1).ToString, _strSpName, WriteErrLog.SELE, _
                                    "clsCalIncenforTech.vb")
            Return Nothing
        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("Fan", ErrorLog.Item(1).ToString, _strSpName, WriteErrLog.SELE, _
                                    "clsCalIncenforTech.vb")
            Return Nothing
        Finally
            selConn.Close()
        End Try

        Return ds
    End Function
#End Region

#Region "Get Export Job Detail Information 2"
    Public Function GetExportInfo2() As DataSet
        Dim ds As New DataSet

        Dim dtExport As New DataTable("Export")
        dtExport.Columns.Add("RMS#")
        dtExport.Columns.Add("Manual Bill #")
        dtExport.Columns.Add("Srvc Center")
        dtExport.Columns.Add("Cust ID")
        dtExport.Columns.Add("Model ID")
        dtExport.Columns.Add("Serial No")
        dtExport.Columns.Add("Date Create")
        dtExport.Columns.Add("Date Serviced")
        dtExport.Columns.Add("CR ID")
        dtExport.Columns.Add("Tech ID")
        dtExport.Columns.Add("Status")
        dtExport.Columns.Add("SO Type")
        dtExport.Columns.Add("SO Status")
        dtExport.Columns.Add("Bill Amount")
        'dtExport.Columns.Add("CR PT")
        'dtExport.Columns.Add("Tech PT")
        dtExport.Columns.Add("Action Taken")

        Dim selConn As SqlConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        _strSpName = "BB_FNCCINC_Export_New2"
        Try
            ' define search fields 
            If _staffList <> "" Then
                'For Each staffCode As String In _staffList
                Dim Param() As SqlParameter = New SqlParameter(8) {}

                Param = SqlHelperParameterCache.GetSpParameterSet(selConn, _strSpName)
                Param(0).Value = Trim(_strUserID)
                Param(1).Value = Trim(_strFrDate)
                Param(2).Value = Trim(_strToDate)
                Param(3).Value = Trim(_staffList)
                Param(4).Value = Trim(_strFrSerCent)
                Param(5).Value = Trim(_strToSerCent)
                Param(6).Value = Trim(_strStaffTy)
                Param(7).Value = Trim(_strSerStats)

                Dim dt As DataTable = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, _strSpName, Param).Tables(0)
                Dim dr As DataRow() = dt.Select()

                For Each row As DataRow In dr
                    dtExport.ImportRow(row)
                Next
                'Next
            Else
                'For Each staffCode As String In _staffList
                Dim Param() As SqlParameter = New SqlParameter(8) {}

                Param = SqlHelperParameterCache.GetSpParameterSet(selConn, _strSpName)
                Param(0).Value = Trim(_strUserID)
                Param(1).Value = Trim(_strFrDate)
                Param(2).Value = Trim(_strToDate)
                Param(3).Value = ""
                Param(4).Value = Trim(_strFrSerCent)
                Param(5).Value = Trim(_strToSerCent)
                Param(6).Value = Trim(_strStaffTy)
                Param(7).Value = Trim(_strSerStats)

                Dim dt As DataTable = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, _strSpName, Param).Tables(0)
                Dim dr As DataRow() = dt.Select()

                For Each row As DataRow In dr
                    dtExport.ImportRow(row)
                Next
                'Next
            End If


            ds.Tables.Add(dtExport)
        Catch ex As SqlException
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(StrUserID, ErrorLog.Item(1).ToString, _strSpName, WriteErrLog.SELE, _
                                    "clsCalIncenforTech.vb")
            Return Nothing
        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("Fan", ErrorLog.Item(1).ToString, _strSpName, WriteErrLog.SELE, _
                                    "clsCalIncenforTech.vb")
            Return Nothing
        Finally
            selConn.Close()
        End Try

        Return ds
    End Function
#End Region

#Region "Retrieve Staffs"
    '''  Modified by PF, 17/10/2016 - SMR/1610/1890 (Filter staff list according to staff Rank)
    '''  Public Function RetrieveStaffs(ByVal staffType As String) As DataTable
    Public Function RetrieveStaffs(ByVal staffType As String, ByVal userId As String) As DataTable
        Dim con As SqlConnection
        con = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        _strSpName = "BB_FNCCINC_ListTechCR"

        'Dim param() As SqlParameter = New SqlParameter(1) {}
        Dim param() As SqlParameter = New SqlParameter(2) {}
        param = SqlHelperParameterCache.GetSpParameterSet(con, _strSpName)
        param(0).Value = staffType
        param(1).Value = userId

        Return SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, _strSpName, param).Tables(0)
    End Function
#End Region

#Region "Retrieve Staffs 2"
    '''  Modified by PF, 17/10/2016 - SMR/1610/1890 (Filter staff list according to staff Rank)
    '''  Public Function RetrieveStaffs(ByVal staffType As String) As DataTable
    Public Function RetrieveStaffs2(ByVal staffType As String, ByVal userId As String, ByVal staffstatus As String) As DataTable
        Dim con As SqlConnection
        con = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        _strSpName = "BB_FNCCINC_ListTechCR2"

        'Dim param() As SqlParameter = New SqlParameter(1) {}
        Dim param() As SqlParameter = New SqlParameter(3) {}
        param = SqlHelperParameterCache.GetSpParameterSet(con, _strSpName)
        param(0).Value = staffType
        param(1).Value = userId
        param(2).Value = staffstatus

        Return SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, _strSpName, param).Tables(0)
    End Function
#End Region


#End Region
End Class
