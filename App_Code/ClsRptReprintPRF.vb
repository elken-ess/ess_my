Imports System.Configuration
Imports CrystalDecisions.Shared
Imports SQLDataAccess
Imports BusinessEntity
Imports System.Data.SqlClient
Imports System.Web
Imports CrystalDecisions.Web
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data



Public Class ClsRptReprintPRF
#Region "Declaration"

    Private fstrReportName As String
    Private fstrReportUrl As String

    Private fcrvViewer As New CrystalReportViewer
    Private fcrvReportSource As New CrystalReportSource
    Private fstrSpName As String
    Private fstrModifiedBy As String

    Private fstrservcenterid As String
    Private fddatebegin As System.DateTime
    Private fstrtecnicianidbegin As String
    Private fstrprfno As String

    'Private fstrstatebegin As String
    'Private fstrsercidbegin As String
    'Private fddatebegin As System.DateTime
    'Private fstrtecnicianidbegin As String

    'Private fstrstateend As String
    'Private fstrsercidend As String
    'Private fddateend As System.DateTime
    'Private fstrtecnicianidend As String

    Private fstrlogctrid As String
    Private fstrlogcompanyid As String
    Private fstrlogserviceid As String
    Private fstrlogrank As String
    Private fstrIsoDesc1 As String
    Private fstrIsoDesc2 As String
    Private fstrCol1 As String

    'Private fstrPartCodeFrom As String
    'Private fstrPartCodeTo As String

#End Region

#Region "Properties"

    Public Property IsoDesc1() As String
        Get
            Return fstrIsoDesc1
        End Get
        Set(ByVal Value As String)
            fstrIsoDesc1 = Value
        End Set
    End Property

    Public Property IsoDesc2() As String
        Get
            Return fstrIsoDesc2
        End Get
        Set(ByVal Value As String)
            fstrIsoDesc2 = Value
        End Set
    End Property

    Public Property Col1() As String
        Get
            Return fstrCol1
        End Get
        Set(ByVal Value As String)
            fstrCol1 = Value
        End Set
    End Property

    Public Property ReportFileName() As String
        Get
            Return fstrReportName
        End Get
        Set(ByVal Value As String)
            fstrReportName = Value
        End Set
    End Property

    Public Property servcenterid() As String
        Get
            Return fstrservcenterid
        End Get
        Set(ByVal Value As String)
            fstrservcenterid = Value
        End Set
    End Property
    Public Property datemin() As String
        Get
            Return fddatebegin
        End Get
        Set(ByVal Value As String)
            fddatebegin = Value
        End Set
    End Property
    Public Property tecnicianidbegin() As String
        Get
            Return fstrtecnicianidbegin
        End Get
        Set(ByVal Value As String)
            fstrtecnicianidbegin = Value
        End Set
    End Property
    Public Property prfno() As String
        Get
            Return fstrprfno
        End Get
        Set(ByVal Value As String)
            fstrprfno = Value
        End Set
    End Property

    Public Property ModifiedBy() As String
        Get
            Return fstrModifiedBy
        End Get
        Set(ByVal Value As String)
            fstrModifiedBy = Value
        End Set
    End Property
    Public Property logctrid() As String
        Get
            Return fstrlogctrid
        End Get
        Set(ByVal Value As String)
            fstrlogctrid = Value
        End Set
    End Property
    Public Property logcompanyid() As String
        Get
            Return fstrlogcompanyid
        End Get
        Set(ByVal Value As String)
            fstrlogcompanyid = Value
        End Set
    End Property
    Public Property logserviceid() As String
        Get
            Return fstrlogserviceid
        End Get
        Set(ByVal Value As String)
            fstrlogserviceid = Value
        End Set
    End Property
    Public Property logrank() As String
        Get
            Return fstrlogrank
        End Get
        Set(ByVal Value As String)
            fstrlogrank = Value
        End Set
    End Property
#End Region

#Region "Methods"
#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)

        connection.Open()

        Return connection
    End Function
#End Region
#Region "search Pack list printing"

    Public Function GetPack() As DataSet

        Dim selConnection As SqlConnection = Nothing
        Dim selConn As SqlConnection = Nothing
        selConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        'fstrSpName = "BB_RPTPACK_VIEW"
        fstrSpName = "BB_RPTPRINT_PRF_VIEW"

        Try
            trans = selConnection.BeginTransaction()

            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(11) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrservcenterid)
            Param(1).Value = Trim(fddatebegin)
            Param(2).Value = Trim(fstrtecnicianidbegin)
            Param(3).Value = Trim(fstrprfno)

            Param(4).Value = Trim(fstrlogctrid)
            Param(5).Value = Trim(fstrlogcompanyid)
            Param(6).Value = Trim(fstrlogserviceid)
            Param(7).Value = Trim(fstrlogrank)
            Param(8).Value = Nothing
            Param(9).Value = Nothing
            Param(10).Value = Nothing

            Dim ds As DataSet = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)

            IsoDesc1 = Param(8).Value
            IsoDesc2 = Param(9).Value
            Col1 = Param(10).Value

            Return ds

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "ClsRptReprintPRF.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "ClsRptReprintPRF.vb")
        Finally
            selConnection.Close()
            selConn.Close()
        End Try

    End Function

    Public Function GetDetails() As DataSet

        Dim selConnection As SqlConnection = Nothing
        Dim selConn As SqlConnection = Nothing
        selConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        'fstrSpName = "BB_RPTPACK_VIEW"
        'fstrSpName = "BB_RPTPRINT_PRF_VIEW"
        fstrSpName = "BB_TEST_DETAILS"
        Try
            trans = selConnection.BeginTransaction()

            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(8) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrservcenterid)
            Param(1).Value = Trim(fddatebegin)
            Param(2).Value = Trim(fstrtecnicianidbegin)
            Param(3).Value = Trim(fstrprfno)

            Param(4).Value = Trim(fstrlogctrid)
            Param(5).Value = Trim(fstrlogcompanyid)
            Param(6).Value = Trim(fstrlogserviceid)
            Param(7).Value = Trim(fstrlogrank)


            Dim ds As DataSet = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)

            Return ds

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "ClsRptReprintPRF.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "ClsRptReprintPRF.vb")
        Finally
            selConnection.Close()
            selConn.Close()
        End Try

    End Function
#End Region
#End Region
End Class
