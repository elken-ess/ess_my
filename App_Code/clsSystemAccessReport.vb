﻿Imports Microsoft.VisualBasic
Imports SQLDataAccess
Imports BusinessEntity
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Data


#Region "Module Description"
'Description        :   System Access Report
'Last Modified Date :   2006-05-24
'Version            :   1.0
'Author             :   BAIYUNPENG
#End Region

Public Class clsSystemAccessReport

#Region "Declaration"
    Private strStoreProcedureName As String
    Private strFServiceCenter As String
    Private strTServiceCenter As String
    Private strFUserID As String
    Private strTUserID As String
    Private strFDate As String
    Private strTDate As String
    Private strFLoginTime As String
    Private strTLoginTime As String
    Private strFPCID As String
    Private strTPCID As String
    Private strLoginOutTime As String
    Private strLoginUserID As String

#End Region

#Region "Properties"
    Public Property FromServiceCenter() As String
        Get
            Return strFServiceCenter
        End Get
        Set(ByVal value As String)
            strFServiceCenter = value
        End Set
    End Property

    Public Property ToServiceCenter() As String
        Get
            Return strTServiceCenter
        End Get
        Set(ByVal value As String)
            strTServiceCenter = value
        End Set
    End Property

    Public Property FromUserID() As String
        Get
            Return strFUserID
        End Get
        Set(ByVal value As String)
            strFUserID = value
        End Set
    End Property

    Public Property ToUserID() As String
        Get
            Return strTUserID
        End Get
        Set(ByVal value As String)
            strTUserID = value
        End Set
    End Property

    Public Property FromDate() As String
        Get
            Return strFDate
        End Get
        Set(ByVal value As String)
            strFDate = value
        End Set
    End Property

    Public Property ToDate() As String
        Get
            Return strTDate
        End Get
        Set(ByVal value As String)
            strTDate = value
        End Set
    End Property

    Public Property FromLoginTime() As String
        Get
            Return strFLoginTime
        End Get
        Set(ByVal value As String)
            strFLoginTime = value
        End Set
    End Property

    Public Property ToLoginTime() As String
        Get
            Return strTLoginTime
        End Get
        Set(ByVal value As String)
            strTLoginTime = value
        End Set
    End Property

    Public Property FromPCID() As String
        Get
            Return strFPCID
        End Get
        Set(ByVal value As String)
            strFPCID = value
        End Set
    End Property

    Public Property ToPCID() As String
        Get
            Return strTPCID
        End Get
        Set(ByVal value As String)
            strTPCID = value
        End Set
    End Property

    Public Property LoginOutTime() As String
        Get
            Return strLoginOutTime
        End Get
        Set(ByVal value As String)
            strLoginOutTime = value
        End Set
    End Property

    Public Property LoginUserID() As String
        Get
            Return strLoginUserID
        End Get
        Set(ByVal value As String)
            strLoginUserID = value
        End Set
    End Property

#End Region

#Region "Method"

#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)

        connection.Open()

        Return connection
    End Function
#End Region

    '#Region "Query Option"

    '    Public Function PreQuery() As DataSet
    '        Dim sqlConn As SqlConnection = Nothing
    '        sqlConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))

    '        strStoreProcedureName = "BB_RPTSYSA_PreQuery"
    '        Dim dsSAR As DataSet = New DataSet
    '        Try
    '            dsSAR = SqlHelper.ExecuteDataset(sqlConn, CommandType.StoredProcedure, strStoreProcedureName)

    '        Catch ex As Exception
    '            Dim ErrorLog As ArrayList = New ArrayList
    '            ErrorLog.Add("Error").ToString()
    '            ErrorLog.Add(ex.Message).ToString()
    '            Dim WriteErrLog As New clsLogFile()
    '            WriteErrLog.ErrorLog("System Access Report", ErrorLog.Item(1).ToString, strStoreProcedureName, WriteErrLog.SELE, "clsSystemAccessReport.vb")
    '        Finally
    '            sqlConn.Close()
    '        End Try
    '        Return dsSAR
    '    End Function

    '#End Region

#Region "Retrive Info"

    Public Function RetriveSystemAccessInfo() As DataSet
        Dim sqlConn As SqlConnection = Nothing
        sqlConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        strStoreProcedureName = "BB_RPTSYSA_VIEW"
        Dim dsSAR As DataSet = New DataSet
        Try
            Dim paramStorc() As SqlParameter = New SqlParameter(12) {}
            paramStorc = SqlHelperParameterCache.GetSpParameterSet(sqlConn, strStoreProcedureName)
            paramStorc(0).Value = Trim(strFServiceCenter)
            paramStorc(1).Value = Trim(strTServiceCenter)
            paramStorc(2).Value = Trim(strFUserID)
            paramStorc(3).Value = Trim(strTUserID)
            paramStorc(4).Value = Trim(strFDate)
            paramStorc(5).Value = Trim(strTDate)
            paramStorc(6).Value = Trim(strFLoginTime)
            paramStorc(7).Value = Trim(strTLoginTime)
            paramStorc(8).Value = Trim(strFPCID)
            paramStorc(9).Value = Trim(strTPCID)
            paramStorc(10).Value = Trim(strLoginOutTime)
            paramStorc(11).Value = Trim(strLoginUserID)

            dsSAR = SqlHelper.ExecuteDataset(sqlConn, CommandType.StoredProcedure, strStoreProcedureName, paramStorc)

        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("System Access Report", ErrorLog.Item(1).ToString, strStoreProcedureName, WriteErrLog.SELE, "clsSystemAccessReport.vb")
            Return Nothing
        Finally
            sqlConn.Close()
        End Try
        Return dsSAR
    End Function

#End Region

#End Region

End Class
