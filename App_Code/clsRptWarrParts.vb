﻿Imports System.Data
Imports System.Data.SqlClient
Imports BusinessEntity
Imports SQLDataAccess

Public Class clsRptWarrParts
#Region "Declarations"
    Private _fromSvcId As String
    Private _toSvcId As String
    Private _fromSvcDate As DateTime
    Private _toSvcDate As DateTime
    Private _fromTechId As String
    Private _toTechId As String
    Private _fromPriceId As String
    Private _toPriceId As String
    Private _userName As String

    Private _con As SqlConnection
    Private _spName As String
#End Region

#Region "Properties"
    Public Property FromSvcId() As String
        Get
            Return _fromSvcId
        End Get
        Set(ByVal value As String)
            _fromSvcId = value
        End Set
    End Property

    Public Property ToSvcId() As String
        Get
            Return _toSvcId
        End Get
        Set(ByVal value As String)
            _toSvcId = value
        End Set
    End Property

    Public Property FromSvcDate() As Date
        Get
            Return _fromSvcDate
        End Get
        Set(ByVal value As Date)
            _fromSvcDate = value
        End Set
    End Property

    Public Property ToSvcDate() As Date
        Get
            Return _toSvcDate
        End Get
        Set(ByVal value As Date)
            _toSvcDate = value
        End Set
    End Property

    Public Property FromTechId() As String
        Get
            Return _fromTechId
        End Get
        Set(ByVal value As String)
            _fromTechId = value
        End Set
    End Property

    Public Property ToTechId() As String
        Get
            Return _toTechId
        End Get
        Set(ByVal value As String)
            _toTechId = value
        End Set
    End Property

    Public Property FromPriceId() As String
        Get
            Return _fromPriceId
        End Get
        Set(ByVal value As String)
            _fromPriceId = value
        End Set
    End Property

    Public Property ToPriceId() As String
        Get
            Return _toPriceId
        End Get
        Set(ByVal value As String)
            _toPriceId = value
        End Set
    End Property

    Public Property UserName() As String
        Get
            Return _userName
        End Get
        Set(ByVal value As String)
            _userName = value
        End Set
    End Property

#End Region
#Region "Private Functions"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)

        connection.Open()

        Return connection
    End Function
#End Region

    Public Function RetrieveWarrantyParts() As DataSet
        Dim dt As New BlueBerryReports.WarrantyPartsDataTable
        Dim ds As New DataSet("Records")
        Dim dr As SqlDataReader
        _con = GetConnection(ConfigurationManager.AppSettings("ConnectionString"))
        _spName = "dbo.BB_RPTWarrantyParts"

        Try
            Dim param() As SqlParameter = New SqlParameter(8) {}
            param = SqlHelperParameterCache.GetSpParameterSet(_con, _spName)
            param(0).Value = _fromSvcId
            param(1).Value = _toSvcId
            param(2).Value = _fromSvcDate
            param(3).Value = _toSvcDate
            param(4).Value = _fromTechId
            param(5).Value = _toTechId
            param(6).Value = _fromPriceId
            param(7).Value = _toPriceId

            ds = SqlHelper.ExecuteDataset(_con, CommandType.StoredProcedure, _spName, param)
            dr = SqlHelper.ExecuteReader(_con, CommandType.StoredProcedure, _spName, param)
            dt.Load(dr)

        Catch ex As SqlException
            Dim errLog As New ArrayList
            errLog.Add("Error").ToString()
            errLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(_userName, errLog.Item(1).ToString, _spName, WriteErrLog.SELE, "clsRptWarrParts.vb")
        Catch ex As Exception
            Dim errLog As New ArrayList
            errLog.Add("Error").ToString()
            errLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(_userName, errLog.Item(1).ToString, _spName, WriteErrLog.SELE, "clsRptWarrParts.vb")
        End Try
        Return ds
    End Function

End Class