Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web
Imports BusinessEntity
Imports SQLDataAccess

Public Class clsRPTPPL
#Region "Declaration"
    Private fstrPtcodeFromID As String
    Private fstrPtcodeToID As String
    Private fstrPriceFromID As String
    Private fstrPriceToID As String
    Private fstrSortby As String
    Private fstrThenby As String
    Private fstrCommPart As String
    Private fstrStatus As String
    Private fstrUserName As String
    Private fstrrank As String ' store procedure name
    Private fstrctrid As String
    Private fstrcomid As String
    Private fstrsvcid As String
  
    Private fstrSortByText As String
    Private fstrThenByText As String
    Private fstrOnlyTest As String
    Private fstrPdctStatus As String


     
    Private fstrReportName As String
    Private fstrReportPath As String
    Private fstrReportID As String
    Private fstrReportUrl As String  'fstrReportPath & fstrReprotName = fstrReportUrl
    Private fcrvViewer As New CrystalReportViewer   'Report viewer
    Private fcrvReportSource As New CrystalReportSource  'Report Source
    Private fstrSpName As String ' store procedure name
#End Region
#Region "Properties"


    Public Property SortByText() As String
        Get
            Return fstrSortByText
        End Get
        Set(ByVal value As String)
            fstrSortByText = value
        End Set
    End Property
    Public Property ThenByText() As String
        Get
            Return fstrThenByText
        End Get
        Set(ByVal value As String)
            fstrThenByText = value
        End Set
    End Property

    Public Property PdctStatus() As String
        Get
            Return fstrPdctStatus
        End Get
        Set(ByVal value As String)
            fstrPdctStatus = value
        End Set
    End Property
    Public Property OnlyTest() As String
        Get
            Return fstrOnlyTest
        End Get
        Set(ByVal value As String)
            fstrOnlyTest = value
        End Set
    End Property

    Public Property PtcodeFrom() As String
        Get
            Return fstrPtcodeFromID
        End Get
        Set(ByVal value As String)
            fstrPtcodeFromID = value

        End Set
    End Property
    Public Property PtcodeTo() As String
        Get
            Return fstrPtcodeToID
        End Get
        Set(ByVal value As String)
            fstrPtcodeToID = value

        End Set
    End Property
    Public Property PriceFrom() As String
        Get
            Return fstrPriceFromID
        End Get
        Set(ByVal value As String)
            fstrPriceFromID = value
        End Set
    End Property
    Public Property PriceTo() As String
        Get
            Return fstrPriceToID
        End Get
        Set(ByVal value As String)
            fstrPriceToID = value
        End Set
    End Property
    Public Property Sortby() As String
        Get
            Return fstrSortby

        End Get
        Set(ByVal value As String)
            fstrSortby = value

        End Set
    End Property
    Public Property Thenby() As String
        Get
            Return fstrThenby

        End Get
        Set(ByVal value As String)
            fstrThenby = value

        End Set
    End Property
    Public Property CommPart() As String
        Get
            Return fstrCommPart

        End Get
        Set(ByVal value As String)
            fstrCommPart = value

        End Set
    End Property
    Public Property SerialNoFrom() As String
        Get
            Return fstrStatus
        End Get
        Set(ByVal value As String)
            fstrStatus = value
        End Set
    End Property

    Public Property UserName()
        Get
            Return fstrUserName
        End Get
        Set(ByVal value)
            fstrUserName = value
        End Set
    End Property
    Public Property rank() As String
        Get
            Return fstrrank
        End Get
        Set(ByVal Value As String)
            fstrrank = Value
        End Set
    End Property
    Public Property ctrid() As String
        Get
            Return fstrctrid
        End Get
        Set(ByVal Value As String)
            fstrctrid = Value
        End Set
    End Property
    Public Property comid() As String
        Get
            Return fstrcomid
        End Get
        Set(ByVal Value As String)
            fstrcomid = Value
        End Set
    End Property
    Public Property svcid() As String
        Get
            Return fstrsvcid
        End Get
        Set(ByVal Value As String)
            fstrsvcid = Value
        End Set
    End Property
    'set fstrReportName
    Public Property ReportFileName() As String
        Get
            Return fstrReportName
        End Get
        Set(ByVal Value As String)
            fstrReportName = Value
        End Set
    End Property
    'set fstrReportPath
    Public Property ReportPath() As String
        Get
            Return fstrReportPath
        End Get
        Set(ByVal Value As String)
            fstrReportPath = Value
        End Set
    End Property
    'set ReportID
    Public Property ReportID() As String
        Get
            Return fstrReportID
        End Get
        Set(ByVal Value As String)
            fstrReportID = Value
        End Set
    End Property
#End Region
#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)

        connection.Open()

        Return connection
    End Function
#End Region


    Private Function CheckReportExist() As Boolean
        Dim lblnfileExists As Boolean = False
        Try
            If IO.File.Exists(fstrReportUrl) Then
                Dim lcrReport As New ReportDocument()
                lcrReport.Load(fstrReportUrl)
                lcrReport.Close()
                lblnfileExists = True
            End If
        Catch e As Exception
            lblnfileExists = False
        End Try

        Return lblnfileExists

    End Function



#Region "Product Price Listing date soure"
    Public Function GetProductPriceListing() As DataSet
        Dim selConnection As SqlConnection = Nothing
        Dim selConn As SqlConnection = Nothing
        selConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_RPTPPLS_VIEW"
        Try
            trans = selConnection.BeginTransaction()

            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(13) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrPtcodeFromID)
            Param(1).Value = Trim(fstrPtcodeToID)
            Param(2).Value = Trim(fstrPriceFromID)
            Param(3).Value = Trim(fstrPriceToID)
            Param(4).Value = Trim(fstrSortby)
            Param(5).Value = Trim(fstrThenby)
            Param(6).Value = Trim(fstrCommPart)
            Param(7).Value = Trim(fstrStatus)
            Param(8).Value = Trim(fstrUserName)
            Param(9).Value = Trim(fstrctrid)
            Param(10).Value = Trim(fstrcomid)
            Param(11).Value = Trim(fstrsvcid)
            Param(12).Value = Trim(fstrrank)
            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            ' switch status id to status name for display
            'Dim count As Integer
            'Dim statXmlTr As New clsXml
            'Dim strPanm As String
            'statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            'For count = 0 To ds.Tables(0).Rows.Count - 1
            '    Dim statusid As String = ds.Tables(0).Rows(count).Item(3).ToString 'item(3) is status
            '    strPanm = statXmlTr.GetLabelName("StatusMessage", statusid)
            '    ds.Tables(0).Rows(count).Item(3) = strPanm
            'Next
            Return ds

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrUserName, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsRPTPPL.vb.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrUserName, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsRPTPPL.vb.vb")
        Finally
            selConnection.Close()
            selConn.Close()
        End Try
    End Function
#End Region

End Class



























































































































































































































































































































































































































































































































































































































































































































































































































































































































































































