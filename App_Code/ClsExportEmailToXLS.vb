
Imports System.Data
Imports System.Data.SqlClient

Imports BusinessEntity

Imports SQLDataAccess

'******************************************************************************
' History
' -----------------------------------------------------------------------------
' Date(YYMMDD)  ModBy   	        Remarks
' =============================================================================
' 120217        Ryan Estandarte		Created
' 120409        Ryan Estandarte     Added ExportToMSWord() and ExportToNotepad()
' 120412        Ryan Estandarte     Added method to remove columns for 
'                                   ExportToMSWord() and ExportToNotepad()
'******************************************************************************

Namespace App_Code

    Public Class ClsExportEmailToXLS
#Region "Declaration"
        Private _fromServCenter As String
        Private _toServCenter As String
        Private _fromStateID As String
        Private _toStateID As String
        Private _fromAreaID As String
        Private _toAreaID As String
        Private _fromModelID As String
        Private _toModelID As String
        Private _custType As String
        Private _race As String
        Private _status As String

        Private _totalCust As Integer
        Private _exportEmailDataTable As DataTable

#End Region

#Region "Properties"
        Public Property FromServCenter() As String
            Get
                Return _fromServCenter
            End Get
            Set(ByVal value As String)
                _fromServCenter = value
            End Set
        End Property

        Public Property ToServCenter() As String
            Get
                Return _toServCenter
            End Get
            Set(ByVal value As String)
                _toServCenter = value
            End Set
        End Property

        Public Property FromStateId() As String
            Get
                Return _fromStateID
            End Get
            Set(ByVal value As String)
                _fromStateID = value
            End Set
        End Property

        Public Property ToStateId() As String
            Get
                Return _toStateID
            End Get
            Set(ByVal value As String)
                _toStateID = value
            End Set
        End Property

        Public Property FromAreaId() As String
            Get
                Return _fromAreaID
            End Get
            Set(ByVal value As String)
                _fromAreaID = value
            End Set
        End Property

        Public Property ToAreaId() As String
            Get
                Return _toAreaID
            End Get
            Set(ByVal value As String)
                _toAreaID = value
            End Set
        End Property

        Public Property FromModelId() As String
            Get
                Return _fromModelID
            End Get
            Set(ByVal value As String)
                _fromModelID = value
            End Set
        End Property

        Public Property ToModelId() As String
            Get
                Return _toModelID
            End Get
            Set(ByVal value As String)
                _toModelID = value
            End Set
        End Property

        Public Property CustType() As String
            Get
                Return _custType
            End Get
            Set(ByVal value As String)
                _custType = value
            End Set
        End Property

        Public Property Race() As String
            Get
                Return _race
            End Get
            Set(ByVal value As String)
                _race = value
            End Set
        End Property

        Public Property Status() As String
            Get
                Return _status
            End Get
            Set(ByVal value As String)
                _status = value
            End Set
        End Property

        ''' <summary>
        ''' Gets the number of customers based on the query
        ''' </summary>
        ''' <value>Total Customers</value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property TotalCust() As Integer
            Get
                Return _totalCust
            End Get
        End Property

        Public ReadOnly Property ExportEmailDataTable() As DataTable
            Get
                Return _exportEmailDataTable
            End Get
        End Property


#End Region

#Region "Methods"
        ''' <summary>
        ''' Retrieves the list of customer emails.
        ''' </summary>
        ''' <remarks>Once called, TotalCust and ExportEmailDataTable can be accessed.</remarks>
        Public Sub GetCustomerEmails()
            Dim sqlCon As New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
            Const sp_Name As String = "BB_RPTEXPEMAIL_VIEW"

            FilterCriteria()

            Dim param(11) As SqlParameter
            param = SqlHelperParameterCache.GetSpParameterSet(sqlCon, sp_Name)
            param(0).Value = _fromServCenter
            param(1).Value = _toServCenter
            param(2).Value = _fromStateID
            param(3).Value = _toStateID
            param(4).Value = _fromAreaID
            param(5).Value = _toAreaID
            param(6).Value = _fromModelID
            param(7).Value = _toModelID
            param(8).Value = _custType
            param(9).Value = _race
            param(10).Value = _status

            Try
                sqlCon.Open()

                _exportEmailDataTable = SqlHelper.ExecuteDataset(sqlCon, CommandType.StoredProcedure, sp_Name, param).Tables(0)

                _totalCust = _exportEmailDataTable.Rows.Count

            Catch ex As Exception
                sqlCon.Close()
                sqlCon.Dispose()

                Dim ErrorLog As ArrayList = New ArrayList
                ErrorLog.Add("Error").ToString()
                ErrorLog.Add(ex.Message).ToString()
                Dim WriteErrLog As New clsLogFile()
                WriteErrLog.ErrorLog("Customer reference Report", ErrorLog.Item(1).ToString, sp_Name, WriteErrLog.SELE, "ClsRptCUSR.vb")

            Finally
                sqlCon.Close()
                sqlCon.Dispose()

            End Try

        End Sub

        ''' <summary>
        ''' Prepares the data before being passed to the stored procedure.
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub FilterCriteria()

            If _fromServCenter = "%" Then
                _fromServCenter = "351"
            ElseIf _fromServCenter = "ZZZ" Then
                _fromServCenter = String.Empty
            End If

            If _toServCenter = "%" Then
                _toServCenter = "366"
            ElseIf _toServCenter = "ZZZ" Then
                _toServCenter = String.Empty
            End If

            If _fromAreaID <> "%" And _toAreaID <> "%" Then

                If Integer.Parse(_fromAreaID) > Integer.Parse(_toAreaID) Then
                    Dim temp As String = _fromAreaID
                    _fromAreaID = _toAreaID
                    _toAreaID = temp
                End If

            ElseIf _toAreaID = "%" And _fromAreaID <> "%" Then
                _toAreaID = _fromAreaID
                _fromAreaID = "%"
            End If


        End Sub

        ''' <summary>
        ''' Exports the datatable to MS Word
        ''' </summary>
        ''' <param name="dt"></param>
        ''' <param name="Response"></param>
        ''' <remarks>Added by Ryan Estandarte 9 April 2012</remarks>
        Public Sub ExportToMSWord(ByVal dt As DataTable, ByVal Response As HttpResponse)
            ' Added by Ryan Estandarte 12 April 2012

            Dim dtWord As DataTable = dt.Copy()
            If dtWord.Columns.Contains("Customer ID") And dt.Columns.Contains("Service Center") And dt.Columns.Contains("Customer Type") And dt.Columns.Contains("Customer Name") Then
                dtWord.Columns.Remove("Customer ID")
                dtWord.Columns.Remove("Service Center")
                dtWord.Columns.Remove("Customer Type")
                dtWord.Columns.Remove("Customer Name")

            End If

            Dim report As New StringBuilder()
            For rows As Integer = 0 To dtWord.Rows.Count - 1
                'report.Append(dt.Rows(rows)(0).ToString() + dt.Rows(rows)(1).ToString())
                Dim emails As String = dtWord.Rows(rows)(0).ToString() + " " + dtWord.Rows(rows)(1).ToString()
                report.Append(emails)
                'report.Append(Environment.NewLine)
            Next

            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=ExportEmails_" + DateTime.Now.ToString("ddMMyyyy") + "_" + DateTime.Now.ToString("hhmmss") + ".doc")

            Response.Charset = ""
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = "application/vnd.word"
            ' Modified by Ryan Estandarte 12 April 2012
            'Dim stringWrite As New IO.StringWriter()
            'Dim htmlWrite As New HtmlTextWriter(stringWrite)
            'Dim dg As New GridView()
            'dg.DataSource = dt
            'dg.DataBind()
            'dg.RenderControl(htmlWrite)
            'Response.Write(stringWrite.ToString())
            Response.Write(report.ToString())
            Response.[End]()
        End Sub

        ''' <summary>
        ''' Exports the datatable report to NotePad
        ''' </summary>
        ''' <param name="dt"></param>
        ''' <param name="Response"></param>
        ''' <remarks>Added by Ryan Estandarte 9 April 2012</remarks>
        Public Sub ExportToNotepad(ByVal dt As DataTable, ByVal Response As HttpResponse)
            Dim report As New StringBuilder

            ' Added by Ryan Aimel J. Estandarte 12 April 2012
            ' Description: Retain only Email 1 and Email 2
            Dim dtNotepad As DataTable = dt.Copy()
            If dtNotepad.Columns.Contains("Customer ID") And dt.Columns.Contains("Service Center") And dt.Columns.Contains("Customer Type") And dt.Columns.Contains("Customer Name") Then
                dtNotepad.Columns.Remove("Customer ID")
                dtNotepad.Columns.Remove("Service Center")
                dtNotepad.Columns.Remove("Customer Type")
                dtNotepad.Columns.Remove("Customer Name")
            End If

            ' Modified by Ryan Aimel J. Estandarte 12 April 2012
            ' Description: Modified to display emails in one line.
            'For rows As Integer = 0 To dt.Rows.Count - 1
            '    For cols As Integer = 0 To dt.Columns.Count - 1
            '        report.Append(dt.Rows(rows)(cols).ToString() + " | ")
            '    Next
            '    report.Append(Environment.NewLine)
            'Next

            For rows As Integer = 0 To dtNotepad.Rows.Count - 1
                Dim emails As String = dtNotepad.Rows(rows)(0).ToString() + " " + dtNotepad.Rows(rows)(1).ToString()
                report.Append(emails)
            Next

            Response.Clear()
            Response.ClearHeaders()
            Response.ClearContent()
            Response.AddHeader("content-disposition", "attachment;filename=ExportEmails_" + DateTime.Now.ToString("dd/MM/yyyy") + "_" + DateTime.Now.ToString("hhmmss") + ".txt")
            Response.Charset = ""
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = "text/plain"
            Response.Write(report.ToString())
            Response.[End]()
        End Sub
#End Region


    End Class

    
End Namespace