Imports Microsoft.VisualBasic
Imports System
#Region "description"
' <summary> 
'定义一个web页面，用来显示用户自定错误提示信息 
'</summary> 
#End Region
Public Class MessageBox

    Private msPage As System.Web.UI.Page

    Public Sub New(ByVal Page As System.Web.UI.Page)
        msPage = Page
    End Sub
    '<summary>  
    '  display messagebox 
    ' <param name="message">提示信息</param> 
    ' </summary> 

    Public Sub Show(ByVal message As String)

        Dim script As String = "<script> alert('" + message + "')</script>"
        msPage.Response.Write(script)
    End Sub
End Class