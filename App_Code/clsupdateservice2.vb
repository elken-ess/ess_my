Imports SQLDataAccess
Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data

#Region " Module Amment Hisotry "
'Description     :  the country entity
'History         :  
'Modified Date   :  2006-05--06
'Version         :  v1.0
'Author          :  xjxted


#End Region

Public Class clsupdateservice2
    Private strConn As String
#Region "Declaration"

    'Amended by KSLim on 17/06/2006
    Private fstrROUnitID As String 'RO号码
    Private fstrIPAddress As String 'IP地址
    Private fstrUserID As String '用户编号
    'Amended by KSLim on 17/06/2006
    Private fstrSOID As String '服务预约号码
    Private fstrPartCode As String '零件编号
    Private fstrPartName As String '零件名称
    Private fstrPhoneNo As String '客户电话
    Private fstrCustomerName As String  '客户姓名
    Private fstrServiceBillType As String
    Private fstrServiceType As String '服务类型
    Private fstrActionTaken As String '员工意见
    Private fstrtechnician As String
    Private fstrInstallDate As System.DateTime ' 安装时间
    Private fstrModlty As String ' model type
    Private fstrSerialNo As String ' serial no
    Private fstrFreeService As String '  free service
    Private fstrRemark As String 'remark
    Private fstrContractID As String
    Private fstrCustomerID As String
    Private fstrCustomerPrefix As String
    Private fstrSpName As String ' store procedure name
    Private fstrSTATUS As String
    Private fstrMifyBy As String

    Private fstrStartDate As String
    Private fstrEndDate As String

    Private fstrAppointmentNo As String
    Private fstrAppointmentPrefix As String
    Private fstrManualServiceBillNo As String
    Private fstrAppointmentFix As String '''''''''''''''''''''''''''''''' added: tomas 20120215
    Private _RMSNo As String ''''''''''''''''''''''''''added by Tomas 20180620

#End Region

#Region "Property"
    Public Property AppointmentPrefix()
        Get
            Return fstrAppointmentPrefix
        End Get
        Set(ByVal value)
            fstrAppointmentPrefix = value
        End Set
    End Property
    Public Property AppointmentNo()
        Get
            Return fstrAppointmentNo
        End Get
        Set(ByVal value)
            fstrAppointmentNo = value
        End Set
    End Property


    Public Property ManualServiceBillNo()
        Get
            Return fstrManualServiceBillNo
        End Get
        Set(ByVal value)
            fstrManualServiceBillNo = value
        End Set
    End Property

    Public Property StartBillDate()
        Get
            Return fstrStartDate
        End Get
        Set(ByVal value)
            fstrStartDate = value
        End Set
    End Property

    Public Property EndBillDate()
        Get
            Return fstrEndDate
        End Get
        Set(ByVal value)
            fstrEndDate = value
        End Set
    End Property


    'Amended by KSLim on 17/06/2006
    Public Property IPAddress()
        Get
            Return fstrIPAddress
        End Get
        Set(ByVal value)
            fstrIPAddress = value
        End Set
    End Property

    Public Property ROUnitID()
        Get
            Return fstrROUnitID
        End Get
        Set(ByVal value)
            fstrROUnitID = value
        End Set
    End Property
    'Amended by KSLim on 17/06/2006

    'Amended by KSLim on 18/06/2006
    Public Property UserID()
        Get
            Return fstrUserID
        End Get
        Set(ByVal value)
            fstrUserID = value
        End Set
    End Property
    'Amended by KSLim on 18/06/2006

    Public Property SOID()
        Get
            Return fstrSOID
        End Get
        Set(ByVal value)
            fstrSOID = value
        End Set
    End Property
    Public Property ServiceType()
        Get
            Return fstrServiceType
        End Get
        Set(ByVal value)
            fstrServiceType = value
        End Set
    End Property
    Public Property PartID()
        Get
            Return fstrPartCode
        End Get
        Set(ByVal value)
            fstrPartCode = value
        End Set
    End Property
    Public Property PartName() As String
        Get
            Return fstrPartName
        End Get
        Set(ByVal value As String)
            fstrPartName = value
        End Set
    End Property
    Public Property InstallDate() As String
        Get
            Return fstrInstallDate
        End Get
        Set(ByVal Value As String)
            fstrInstallDate = DateTime.Parse(Value, New System.Globalization.CultureInfo("en-CA")).Date
        End Set
    End Property
    Public Property PhoneNo() As String
        Get
            Return fstrPhoneNo
        End Get
        Set(ByVal value As String)
            fstrPhoneNo = value
        End Set
    End Property
    Public Property CustomerName() As String
        Get
            Return fstrCustomerName
        End Get
        Set(ByVal value As String)
            fstrCustomerName = value
        End Set
    End Property
    Public Property ActionTaken() As String
        Get
            Return fstrActionTaken
        End Get
        Set(ByVal value As String)
            fstrActionTaken = value
        End Set
    End Property
    Public Property Modlty() As String
        Get
            Return fstrModlty
        End Get
        Set(ByVal value As String)
            fstrModlty = value
        End Set
    End Property
    Public Property SerialNo() As String
        Get
            Return fstrSerialNo
        End Get
        Set(ByVal value As String)
            fstrSerialNo = value
        End Set
    End Property
    Public Property FreeService() As String
        Get
            Return fstrFreeService
        End Get
        Set(ByVal value As String)
            fstrFreeService = value
        End Set
    End Property
    Public Property Remark() As String
        Get
            Return fstrRemark
        End Get
        Set(ByVal value As String)
            fstrRemark = value
        End Set
    End Property
    Public Property ContractID() As String
        Get
            Return fstrContractID
        End Get
        Set(ByVal value As String)
            fstrContractID = value
        End Set
    End Property
    Public Property CustomerID() As String
        Get
            Return fstrCustomerID
        End Get
        Set(ByVal value As String)
            fstrCustomerID = value
        End Set
    End Property
    Public Property CustomerPrefix() As String
        Get
            Return fstrCustomerPrefix
        End Get
        Set(ByVal value As String)
            fstrCustomerPrefix = value
        End Set
    End Property

    Public Property SpName() As String
        Get
            Return fstrSpName
        End Get
        Set(ByVal value As String)
            fstrSpName = value
        End Set
    End Property
    Public Property STATUS() As String
        Get
            Return fstrSTATUS
        End Get
        Set(ByVal value As String)
            fstrSTATUS = value
        End Set
    End Property
    Public Property ModifyBy() As String
        Get
            Return fstrMifyBy
        End Get
        Set(ByVal Value As String)
            fstrMifyBy = Value
        End Set
    End Property

    Public Property Technician() As String ''''''''''''''''''''''''''added: Tomas 20120215
        Get
            Return fstrtechnician
        End Get
        Set(ByVal value As String)
            fstrtechnician = value
        End Set
    End Property

    Public Property AppointmentFix() As String '''''''''''''''''''''''''''''''' added: tomas 20120215
        Get
            Return fstrAppointmentFix
        End Get
        Set(ByVal value As String)
            fstrAppointmentFix = value
        End Set
    End Property
    Public Property RMSNo() As String '''''''''''''''''''''''''''''''' added: tomas 20180620
        Get
            Return _RMSNo
        End Get
        Set(ByVal value As String)
            _RMSNo = value
        End Set
    End Property


#End Region

#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)

        connection.Open()

        Return connection
    End Function
#End Region

#Region "search"
    Public Function SelectAllServiceBillPart2() As DataSet
        Dim selConn As SqlConnection = Nothing
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        fstrSpName = "BB_FNCUSB2_View"
        Dim ds As DataSet = New DataSet()
        Try
            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(17) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrSOID)
            Param(1).Value = Trim(fstrCustomerName)
            Param(2).Value = Trim(fstrPhoneNo)
            Param(3).Value = Trim(fstrSerialNo)
            Param(4).Value = Trim(fstrUserID)
            Param(5).Value = Trim(fstrSTATUS)
            Param(6).Value = Trim(fstrStartDate)
            Param(7).Value = Trim(fstrEndDate)

            Param(8).Value = Trim(fstrCustomerPrefix)
            Param(9).Value = Trim(fstrCustomerID)
            Param(10).Value = Trim(fstrAppointmentPrefix)
            Param(11).Value = Trim(fstrAppointmentNo)
            Param(12).Value = Trim(fstrManualServiceBillNo)
            Param(13).Value = Trim(fstrRemark) '''''''''''''''''''''''''''''Added: Tomas 20120212
            Param(14).Value = Trim(fstrtechnician) '''''''''''''''''''''''''''''Added: Tomas 20120212
            Param(15).Value = Trim(fstrAppointmentFix) '''''''''''''''''''''''''''''Added: Tomas 20120212
            Param(16).Value = Trim(_RMSNo) '''''''''''''''''''''''''''''Added: Tomas 20180620

            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            SelectAllServiceBillPart2 = ds

            selConn.Dispose()
        Catch ex As SqlException
            'Console.Write(True)
        Finally
            selConn.Close()
        End Try

    End Function
#End Region

#Region "select By ID"
    Public Function GetPartaCR(ByVal transType As String, ByVal transNo As String) As DataSet
        Dim selConn As SqlConnection = Nothing
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        fstrSpName = "BB_FNCUSB2_SelByID"
        Dim ds As DataSet = New DataSet()
        Try
            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(transType)
            Param(1).Value = Trim(transNo)
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
        Catch ex As SqlException
        Finally
            selConn.Close()
        End Try
        Return ds
    End Function
#End Region

#Region "Update1"

    Public Function Update1(ByVal transType As String, ByVal transNo As String) As Integer  '
        Dim UpdateConnection As SqlConnection = Nothing
        Dim UpdateConn As SqlConnection = Nothing
        UpdateConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        UpdateConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing
        fstrSpName = "BB_FNCUSB2_Upd"
        Try
            trans = UpdateConnection.BeginTransaction()
            'Amended by KSLim on 17/06/2006
            Dim updParam() As SqlParameter = New SqlParameter(14) {}
            updParam = SqlHelperParameterCache.GetSpParameterSet(UpdateConn, fstrSpName)
            updParam(0).Value = Trim(transType)
            updParam(1).Value = Trim(transNo)
            updParam(2).Value = Trim(fstrServiceType)
            updParam(3).Value = Trim(fstrActionTaken)
            updParam(4).Value = Trim(fstrInstallDate)
            updParam(5).Value = Trim(fstrModlty)
            updParam(6).Value = Trim(fstrSerialNo)
            updParam(7).Value = Trim(fstrRemark)
            updParam(8).Value = Trim(fstrFreeService)
            updParam(9).Value = Trim(fstrContractID)
            updParam(10).Value = Trim(fstrSTATUS)
            updParam(11).Value = Trim(fstrMifyBy)
            updParam(12).Value = Trim(fstrROUnitID)
            updParam(13).Value = Trim(fstrIPAddress)
            updParam(14).Value = Trim(CustomerPrefix)
            updParam(15).Value = Trim(CustomerID)


            'Amended by KSLim on 17/06/2006
            Dim intAddr As Integer = SqlHelper.ExecuteNonQuery(UpdateConn, CommandType.StoredProcedure, fstrSpName, updParam)
            trans.Commit()
            Return 0
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("wang", ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "clsupdateservice2.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("wang", ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.EDIT, "clsupdateservice2.vb")
            Return -1
        Finally
            UpdateConnection.Close()
            UpdateConn.Close()
        End Try

    End Function
#End Region

#Region "seldropdown"
    'Public Function selds(ByVal transType As String, ByVal transNo As String) As DataSet
    '    Dim selConn As SqlConnection = Nothing
    '    selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
    '    fstrSpName = "BB_FNCUSB2_SelByID"
    '    Dim ds As DataSet = New DataSet()
    '    Try
    '        Dim Param() As SqlParameter = New SqlParameter(2) {}
    '        Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
    '        Param(0).Value = Trim(transType)
    '        Param(1).Value = Trim(transNo)
    '        ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
    '    Catch ex As Exception
    '        Dim ErrorLog As ArrayList = New ArrayList
    '        ErrorLog.Add("Error").ToString()
    '        ErrorLog.Add(ex.Message).ToString()
    '        Dim WriteErrLog As New clsLogFile()
    '        WriteErrLog.ErrorLog("wang", ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsupdateservice2.vb")

    '    Finally
    '        selConn.Close()
    '    End Try
    '    Return ds
    'End Function


    Public Function selds(ByVal transType As String, ByVal transNo As String) As DataSet
        Dim selConn As SqlConnection = Nothing
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        fstrSpName = "BB_FNCUSB2_SELBYID_NEW"
        Dim ds As DataSet = New DataSet()
        Try
            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(transType)
            Param(1).Value = Trim(transNo)
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("wang", ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsupdateservice2.vb")

        Finally
            selConn.Close()
        End Try
        Return ds
    End Function

#End Region

#Region "get status message name"
    Public Function searchmoney(ByVal transType As String, ByVal transno As String) As DataTable

        Dim statConnection As SqlConnection = Nothing
        Dim statConn As SqlConnection = Nothing
        statConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        statConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_FNCUSB2_STATUS"

        Dim ds As DataSet = New DataSet()
        Try
            Dim Param() As SqlParameter = New SqlParameter(2) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(statConn, fstrSpName)
            Param(0).Value = Trim(transType)
            Param(1).Value = Trim(transno)
            ds = SqlHelper.ExecuteDataset(statConn, CommandType.StoredProcedure, fstrSpName, Param)
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("wang", ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsupdateservice2.vb")
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog("wang", ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsupdateservice2.vb")
        Finally
            statConn.Close()
            statConnection.Close()
        End Try

        Dim statXmlTr As New clsXml
        statXmlTr.XmlFile = ConfigurationSettings.AppSettings("XmlFilePath")
        Dim strPaid As String
        Dim strpayam As String
        Dim infon As New DataTable
        Dim newrow As DataRow
        infon.Columns.Add("PayID", System.Type.GetType("System.String"))
        infon.Columns.Add("PayAM", System.Type.GetType("System.String"))

        For i As Integer = 0 To ds.Tables(1).Rows.Count - 1
            strPaid = ds.Tables(1).Rows(i).Item(0).ToString()
            strpayam = ds.Tables(1).Rows(i).Item(1).ToString()
            newrow = infon.Rows.Add()
            newrow.Item(0) = strPaid
            newrow.Item(1) = strpayam
        Next

        Return infon

    End Function
#End Region

    '#Region "select CONTRACT ID"
    '    Public Function GetCustomerDetailsByContractID(ByVal transType As String, ByVal transNo As String) As Integer
    '        Dim sidConnection As SqlConnection = Nothing
    '        Dim sidConn As SqlConnection = Nothing
    '        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
    '        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
    '        Dim trans As SqlTransaction = Nothing

    '        fstrSpName = "BB_FNCUSB2_SELBYCTRID"
    '        Try
    '            trans = sidConnection.BeginTransaction()

    '            Dim Param() As SqlParameter = New SqlParameter(2) {}
    '            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
    '            Param(0).Value = Trim(transType)
    '            Param(1).Value = Trim(transNo)

    '            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(sidConn, CommandType.StoredProcedure, fstrSpName, Param) '执行存储过程，传输数据赋给dr
    '            If dr.Read() Then
    '                dr.Close()
    '                Return 0
    '            End If
    '            Return 0
    '        Catch ex As Exception
    '            trans.Rollback()
    '            Dim ErrorLog As ArrayList = New ArrayList
    '            ErrorLog.Add("Error").ToString()
    '            ErrorLog.Add(ex.Message).ToString()
    '            Dim WriteErrLog As New clsLogFile()
    '            WriteErrLog.ErrorLog("XJX", ErrorLog.Item(1).ToString, WriteErrLog.SELE, "clsupdateservice2")
    '            Return -1
    '        Catch ex As SqlException
    '            trans.Rollback()
    '            Dim ErrorLog As ArrayList = New ArrayList
    '            ErrorLog.Add("Error").ToString()
    '            ErrorLog.Add(ex.Number).ToString()
    '            Dim WriteErrLog As New clsLogFile()
    '            WriteErrLog.ErrorLog("XJX", ErrorLog.Item(1).ToString, WriteErrLog.SELE, "clsupdateservice2")
    '            Return -1
    '        Finally
    '            sidConnection.Close()
    '            sidConn.Close()
    '        End Try
    '    End Function
    '#End Region

End Class
