﻿Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Web
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web
Imports BusinessEntity
Imports System.Data
Imports SQLDataAccess

#Region " Module Amment Hisotry "
'Description     :   
'History         :  
'Modified Date   :  
'Version         :  v1.0
'Author          :  


#End Region

Public Class ClsRPTRAPT
    Private strConn As String

#Region "Declaration"
    Private fstrusername As String
    Private fstrServiceID1 As String
    Private fstrServiceID2 As String
    Private fstrDate1 As System.DateTime ' country name
    Private fstrDate2 As System.DateTime
    Private fstrCustomerID1 As String
    Private fstrCustomerID2 As String ' alternate country  name
    Private fstrSerialNo1 As String
    Private fstrSerialNo2 As String
    Private fstrModel1 As String ' country status
    Private fstrModel2 As String
    Private fstrReapted As String ' country currency
    Private fstrReaptedNum As String
    Private fstrSerType As String ' country currency
    Private fstrSerType_sub As String
    Private fstrSpName As String ' store procedure name
    Private fstrctrid As String
    Private fstrcomid As String
    Private fstrsvcid As String
    Private fstrrank As String
#End Region
#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)

        connection.Open()

        Return connection
    End Function
#End Region
#Region "Properties"
    Public Property sertype_sub() As String
        Get
            Return fstrSerType_sub
        End Get
        Set(ByVal Value As String)
            fstrSerType_sub = Value
        End Set
    End Property
    Public Property ctrid() As String
        Get
            Return fstrctrid
        End Get
        Set(ByVal Value As String)
            fstrctrid = Value
        End Set
    End Property
    Public Property comid() As String
        Get
            Return fstrcomid
        End Get
        Set(ByVal Value As String)
            fstrcomid = Value
        End Set
    End Property
    Public Property svcid() As String
        Get
            Return fstrsvcid
        End Get
        Set(ByVal Value As String)
            fstrsvcid = Value
        End Set
    End Property
    Public Property rank() As String
        Get
            Return fstrrank
        End Get
        Set(ByVal Value As String)
            fstrrank = Value
        End Set
    End Property
    Public Property Username() As String
        Get
            Return fstrusername
        End Get
        Set(ByVal Value As String)
            fstrusername = Value
        End Set
    End Property
    Public Property ServiceID1() As String
        Get
            Return fstrServiceID1
        End Get
        Set(ByVal Value As String)
            fstrServiceID1 = Value
        End Set
    End Property
    Public Property ServiceID2() As String
        Get
            Return fstrServiceID2
        End Get
        Set(ByVal Value As String)
            fstrServiceID2 = Value
        End Set
    End Property

    Public Property Datetime1() As String
        Get
            Return fstrDate1
        End Get
        Set(ByVal Value As String)
            fstrDate1 = Value
        End Set
    End Property
    Public Property Datetime2() As String
        Get
            Return fstrDate2
        End Get
        Set(ByVal Value As String)
            fstrDate2 = Value
        End Set
    End Property


    Public Property CustomerID1() As String
        Get
            Return fstrCustomerID1
        End Get
        Set(ByVal Value As String)
            fstrCustomerID1 = Value
        End Set
    End Property
    Public Property CustomerID2() As String
        Get
            Return fstrCustomerID2
        End Get
        Set(ByVal Value As String)
            fstrCustomerID2 = Value
        End Set
    End Property

    Public Property SerialNo1() As String
        Get
            Return fstrSerialNo1
        End Get
        Set(ByVal Value As String)
            fstrSerialNo1 = Value
        End Set
    End Property
    Public Property SerialNo2() As String
        Get
            Return fstrSerialNo2
        End Get
        Set(ByVal Value As String)
            fstrSerialNo2 = Value
        End Set
    End Property
    Public Property Model1() As String
        Get
            Return fstrModel1
        End Get
        Set(ByVal Value As String)
            fstrModel1 = Value
        End Set
    End Property
    Public Property Model2() As String
        Get
            Return fstrModel2
        End Get
        Set(ByVal Value As String)
            fstrModel2 = Value
        End Set
    End Property

    Public Property Reapted() As String
        Get
            Return fstrReapted
        End Get
        Set(ByVal Value As String)
            fstrReapted = Value
        End Set
    End Property
    Public Property Reaptednum() As String
        Get
            Return fstrReaptedNum
        End Get
        Set(ByVal Value As String)
            fstrReaptedNum = Value
        End Set
    End Property
    Public Property SerType() As String
        Get
            Return fstrSerType
        End Get
        Set(ByVal Value As String)
            fstrSerType = Value
        End Set
    End Property

#End Region
#Region "Select Reapted Services"

    Public Function GetRepeatedSer() As DataSet

        Dim selConnection As SqlConnection = Nothing
        Dim selConn As SqlConnection = Nothing
        selConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_RPTRAPT_VIEW"
        Try
            trans = selConnection.BeginTransaction()

            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(17) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrServiceID1)
            Param(1).Value = Trim(fstrServiceID2)
            Param(2).Value = Trim(fstrDate1)
            Param(3).Value = Trim(fstrDate2)
            Param(4).Value = Trim(fstrCustomerID1)
            Param(5).Value = Trim(fstrCustomerID2)
            Param(6).Value = Trim(fstrSerialNo1)
            Param(7).Value = Trim(fstrSerialNo2)
            Param(8).Value = Trim(fstrModel1)
            Param(9).Value = Trim(fstrModel2)
            Param(10).Value = Trim(fstrSerType)
            Param(11).Value = Trim(fstrReapted)
            Param(12).Value = Trim(fstrReaptedNum)
            Param(13).Value = Trim(fstrctrid)
            Param(14).Value = Trim(fstrcomid)
            Param(15).Value = Trim(fstrsvcid)
            Param(16).Value = Trim(fstrrank)


 
            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            ' switch status id to status name for display

            ' Modified by Ryan Estandarte 5 Sept 2012
            'Dim count As Integer
            'Dim statXmlTr As New clsXml
            'Dim strPanm As String
            'statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            'For count = 0 To ds.Tables(0).Rows.Count - 1
            '    Dim statusid As String = ds.Tables(0).Rows(count).Item(3).ToString 'item(3) is status
            '    strPanm = statXmlTr.GetLabelName("StatusMessage", statusid)
            '    ds.Tables(0).Rows(count).Item(3) = strPanm
            'Next

            Return ds

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "ClsReaptedApp.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "ClsRptCDAR.vb")

        Finally
            selConnection.Close()
            selConn.Close()
        End Try

    End Function
#End Region
End Class
