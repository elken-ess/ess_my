Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web
Imports BusinessEntity
Imports SQLDataAccess

Public Class clsRPTSBP
#Region "Declaration"
    Private fstrPartCodeFromID As String
    Private fstrPartCodeToID As String
    Private fstrSvcFromID As String
    Private fstrSvcToID As String
    Private fstrCategoryFromID As String
    Private fstrCategoryToID As String
    Private fstrPriceFrom As String
    Private fstrPriceTo As String
    Private fstrStateFrom As String
    Private fstrStateTo As String
    Private fstrTechnicianFrom As String
    Private fstrTechnicianTo As String
    Private fstrGroupbypice As String
    Private fstrIncludeSeritem As String
    Private fstrGroupbypice1 As String
    Private fstrIncludeSeritem1 As String

    Private fdtStartDate As System.DateTime
    Private fdtEndDate As System.DateTime
    Private fstrUserName As String
    Private fstrrank As String ' store procedure name
    Private fstrctrid As String
    Private fstrcomid As String
    Private fstrsvcid As String


    Private fstrReportName As String
    Private fstrReportPath As String
    Private fstrReportID As String
    Private fstrReportUrl As String  'fstrReportPath & fstrReprotName = fstrReportUrl
    Private fcrvViewer As New CrystalReportViewer   'Report viewer
    Private fcrvReportSource As New CrystalReportSource  'Report Source
    Private fstrSpName As String ' store procedure name

    Private fstrServiceBillType As String
    Private fstrServiceBillTypeText As String

#End Region
#Region "Properties"
    Public Property ServiceBillTypeText() As String
        Get
            Return fstrServiceBillTypeText
        End Get
        Set(ByVal value As String)
            fstrServiceBillTypeText = value

        End Set
    End Property

    Public Property ServiceBillType() As String
        Get
            Return fstrServiceBillType
        End Get
        Set(ByVal value As String)
            fstrServiceBillType = value

        End Set
    End Property

    Public Property PartCodeFrom() As String
        Get
            Return fstrPartCodeFromID
        End Get
        Set(ByVal value As String)
            fstrPartCodeFromID = value

        End Set
    End Property
    Public Property PartCodeTo() As String
        Get
            Return fstrPartCodeToID
        End Get
        Set(ByVal value As String)
            fstrPartCodeToID = value

        End Set
    End Property
    Public Property SvcIDFrom() As String
        Get
            Return fstrSvcFromID
        End Get
        Set(ByVal value As String)
            fstrSvcFromID = value
        End Set
    End Property
    Public Property SvcIDTo() As String
        Get
            Return fstrSvcToID
        End Get
        Set(ByVal value As String)
            fstrSvcToID = value
        End Set
    End Property
    Public Property CategoryFrom() As String
        Get
            Return fstrCategoryFromID

        End Get
        Set(ByVal value As String)
            fstrCategoryFromID = value

        End Set
    End Property
    Public Property CategoryTo() As String
        Get
            Return fstrCategoryToID

        End Get
        Set(ByVal value As String)
            fstrCategoryToID = value

        End Set
    End Property
    Public Property PriceFrom() As String
        Get
            Return fstrPriceFrom
        End Get
        Set(ByVal value As String)
            fstrPriceFrom = value
        End Set
    End Property
    Public Property PriceTo() As String
        Get
            Return fstrPriceTo
        End Get
        Set(ByVal value As String)
            fstrPriceTo = value
        End Set
    End Property
    Public Property StateFrom() As String
        Get
            Return fstrStateFrom
        End Get
        Set(ByVal value As String)
            fstrStateFrom = value
        End Set
    End Property
    Public Property StateTo() As String
        Get
            Return fstrStateTo
        End Get
        Set(ByVal value As String)
            fstrStateTo = value
        End Set
    End Property
    Public Property TechnicianFrom() As String
        Get
            Return fstrTechnicianFrom
        End Get
        Set(ByVal value As String)
            fstrTechnicianFrom = value
        End Set
    End Property
    Public Property TechnicianTo() As String
        Get
            Return fstrTechnicianTo
        End Get
        Set(ByVal value As String)
            fstrTechnicianTo = value
        End Set
    End Property
    Public Property Groupbypice() As String
        Get
            Return fstrGroupbypice
        End Get
        Set(ByVal value As String)
            fstrGroupbypice = value
        End Set
    End Property
    Public Property IncludeSeritem() As String
        Get
            Return fstrIncludeSeritem
        End Get
        Set(ByVal value As String)
            fstrIncludeSeritem = value
        End Set
    End Property
    Public Property Groupbypice1() As String
        Get
            Return fstrGroupbypice1
        End Get
        Set(ByVal value As String)
            fstrGroupbypice1 = value
        End Set
    End Property
    Public Property IncludeSeritem1() As String
        Get
            Return fstrIncludeSeritem1
        End Get
        Set(ByVal value As String)
            fstrIncludeSeritem1 = value
        End Set
    End Property

    Public Property StartDate() As String
        Get
            Return fdtStartDate
        End Get
        Set(ByVal value As String)
            fdtStartDate = value
        End Set
    End Property
    Public Property EndDate() As String
        Get
            Return fdtEndDate
        End Get
        Set(ByVal value As String)
            fdtEndDate = value
        End Set
    End Property
    Public Property UserName()
        Get
            Return fstrUserName
        End Get
        Set(ByVal value)
            fstrUserName = value
        End Set
    End Property
    Public Property rank() As String
        Get
            Return fstrrank
        End Get
        Set(ByVal Value As String)
            fstrrank = Value
        End Set
    End Property
    Public Property ctrid() As String
        Get
            Return fstrctrid
        End Get
        Set(ByVal Value As String)
            fstrctrid = Value
        End Set
    End Property
    Public Property comid() As String
        Get
            Return fstrcomid
        End Get
        Set(ByVal Value As String)
            fstrcomid = Value
        End Set
    End Property
    Public Property svcid() As String
        Get
            Return fstrsvcid
        End Get
        Set(ByVal Value As String)
            fstrsvcid = Value
        End Set
    End Property
    'set fstrReportName
    Public Property ReportFileName() As String
        Get
            Return fstrReportName
        End Get
        Set(ByVal Value As String)
            fstrReportName = Value
        End Set
    End Property
    'set fstrReportPath
    Public Property ReportPath() As String
        Get
            Return fstrReportPath
        End Get
        Set(ByVal Value As String)
            fstrReportPath = Value
        End Set
    End Property
    'set ReportID
    Public Property ReportID() As String
        Get
            Return fstrReportID
        End Get
        Set(ByVal Value As String)
            fstrReportID = Value
        End Set
    End Property
#End Region
#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)

        connection.Open()

        Return connection
    End Function
#End Region


    Private Function CheckReportExist() As Boolean
        Dim lblnfileExists As Boolean = False
        Try
            If IO.File.Exists(fstrReportUrl) Then
                Dim lcrReport As New ReportDocument()
                lcrReport.Load(fstrReportUrl)
                lcrReport.Close()
                lblnfileExists = True
            End If
        Catch e As Exception
            lblnfileExists = False
        End Try

        Return lblnfileExists

    End Function



#Region "Sales By Product date soure"
    Public Function GetSalesByProduct() As DataSet
        Dim selConnection As SqlConnection = Nothing
        Dim selConn As SqlConnection = Nothing
        selConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_RPTSALP_VIEW"
        Try
            trans = selConnection.BeginTransaction()

            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(22) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrPartCodeFromID)
            Param(1).Value = Trim(fstrPartCodeToID)
            Param(2).Value = Trim(fstrCategoryFromID)
            Param(3).Value = Trim(fstrCategoryToID)
            Param(4).Value = Trim(fstrPriceFrom)
            Param(5).Value = Trim(fstrPriceTo)
            Param(6).Value = Trim(fstrStateFrom)
            Param(7).Value = Trim(fstrStateTo)
            Param(8).Value = Trim(fstrTechnicianFrom)
            Param(9).Value = Trim(fstrTechnicianTo)
            Param(10).Value = Trim(fstrSvcFromID)
            Param(11).Value = Trim(fstrSvcToID)
            Param(12).Value = Trim(fstrGroupbypice)
            Param(13).Value = Trim(fstrIncludeSeritem)
            Param(14).Value = Trim(fstrUserName)
            Param(15).Value = Trim(fdtStartDate)
            Param(16).Value = Trim(fdtEndDate)
            Param(17).Value = Trim(fstrctrid)
            Param(18).Value = Trim(fstrcomid)
            Param(19).Value = Trim(fstrsvcid)
            Param(20).Value = Trim(fstrrank)
            Param(21).Value = Trim(fstrServiceBillType)

            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            ' switch status id to status name for display
            'Dim count As Integer
            'Dim statXmlTr As New clsXml
            'Dim strPanm As String
            'statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            'For count = 0 To ds.Tables(0).Rows.Count - 1
            '    Dim statusid As String = ds.Tables(0).Rows(count).Item(3).ToString 'item(3) is status
            '    strPanm = statXmlTr.GetLabelName("StatusMessage", statusid)
            '    ds.Tables(0).Rows(count).Item(3) = strPanm
            'Next
            Return ds

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrUserName, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsRPTSBP.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrUserName, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsRPTSBP.vb")
        Finally
            selConnection.Close()
            selConn.Close()
        End Try
    End Function
#End Region

End Class



























































































































































































































































































































































































































































































































































































































































































































































































































































































































































































