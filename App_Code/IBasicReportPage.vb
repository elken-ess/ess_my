
Imports System.Data

Public Interface IBasicReportPage
    Sub PopulateLabels()
    Sub GenerateReport()
    Sub DefineDefaultValues()

    Sub GenerateReport(ByVal dataSet As DataSet)
End Interface

