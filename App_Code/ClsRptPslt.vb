Imports System.Configuration
Imports CrystalDecisions.Shared
Imports SQLDataAccess
Imports BusinessEntity
Imports System.Data.SqlClient
Imports System.Web
Imports CrystalDecisions.Web
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data



Public Class ClsRptPslt
#Region "Declaration"

    Private fstrReportName As String
    Private fstrReportUrl As String

    Private fcrvViewer As New CrystalReportViewer
    Private fcrvReportSource As New CrystalReportSource
    Private fstrSpName As String

    Private fstrminstate As String
    Private fstrmaxstate As String
    Private fstrminsercenter As String
    Private fstrmaxsercenter As String
    Private fstrmintech As String
    Private fstrmaxtech As String
    Private fstrminPartcode As String
    Private fstrmaxPartcode As String

    Private fstrservicetype As String

    Private fddate1min As System.DateTime
    Private fddate2min As System.DateTime
    Private fddate3min As System.DateTime
    Private fddate4min As System.DateTime
    Private fddate5min As System.DateTime
    Private fddate6min As System.DateTime

    Private fddate1max As System.DateTime
    Private fddate2max As System.DateTime
    Private fddate3max As System.DateTime
    Private fddate4max As System.DateTime
    Private fddate5max As System.DateTime
    Private fddate6max As System.DateTime

    Private fddatemin As System.DateTime
    Private fddatemax As System.DateTime
    Private fstrModifiedBy As String
    Private fstrsuser As String

    Private fstrlogctrid As String
    Private fstrlogcompid As String
    Private fstrlogsvrcenterid As String
    Private fstrlogrank As String

    Private fstrdatetype As String
    Private fstrappnametype As String
  
  
#End Region

#Region "Properties"

    Public Property ReportFileName() As String
        Get
            Return fstrReportName
        End Get
        Set(ByVal Value As String)
            fstrReportName = Value
        End Set
    End Property
    Public Property uname() As String
        Get
            Return fstrsuser
        End Get
        Set(ByVal Value As String)
            fstrsuser = Value
        End Set
    End Property
    Public Property minstate() As String
        Get
            Return fstrminstate
        End Get
        Set(ByVal Value As String)
            fstrminstate = Value
        End Set
    End Property
    Public Property minsercenter() As String
        Get
            Return fstrminsercenter
        End Get
        Set(ByVal Value As String)
            fstrminsercenter = Value
        End Set
    End Property
    Public Property mintech() As String
        Get
            Return fstrmintech
        End Get
        Set(ByVal Value As String)
            fstrmintech = Value
        End Set
    End Property
    Public Property minPartcode() As String
        Get
            Return fstrminPartcode
        End Get
        Set(ByVal Value As String)
            fstrminPartcode = Value
        End Set
    End Property
   
    Public Property maxstate() As String
        Get
            Return fstrmaxstate
        End Get
        Set(ByVal Value As String)
            fstrmaxstate = Value
        End Set
    End Property
    Public Property maxsercenter() As String
        Get
            Return fstrmaxsercenter
        End Get
        Set(ByVal Value As String)
            fstrmaxsercenter = Value
        End Set
    End Property
    Public Property maxtech() As String
        Get
            Return fstrmaxtech
        End Get
        Set(ByVal Value As String)
            fstrmaxtech = Value
        End Set
    End Property
    Public Property maxPartcode() As String
        Get
            Return fstrmaxPartcode
        End Get
        Set(ByVal Value As String)
            fstrmaxPartcode = Value
        End Set
    End Property
    Public Property ModifiedBy() As String
        Get
            Return fstrModifiedBy
        End Get
        Set(ByVal Value As String)
            fstrModifiedBy = Value
        End Set
    End Property
    Public Property servicetype() As String
        Get
            Return fstrservicetype
        End Get
        Set(ByVal Value As String)
            fstrservicetype = Value
        End Set
    End Property
 
    Public Property datemax() As String
        Get
            Return fddatemax
        End Get
        Set(ByVal Value As String)
            fddatemax = Value
        End Set
    End Property
    Public Property datemin() As String
        Get
            Return fddatemin
        End Get
        Set(ByVal Value As String)
            fddatemin = Value
        End Set
    End Property
    Public Property logctrid() As String
        Get
            Return fstrlogctrid
        End Get
        Set(ByVal Value As String)
            fstrlogctrid = Value
        End Set
    End Property
    Public Property logcompid() As String
        Get
            Return fstrlogcompid
        End Get
        Set(ByVal Value As String)
            fstrlogcompid = Value
        End Set
    End Property
    Public Property logsvrcenterid() As String
        Get
            Return fstrlogsvrcenterid
        End Get
        Set(ByVal Value As String)
            fstrlogsvrcenterid = Value
        End Set
    End Property

    Public Property logrank() As String
        Get
            Return fstrlogrank
        End Get
        Set(ByVal Value As String)
            fstrlogrank = Value
        End Set
    End Property
    Public Property datetype() As String
        Get
            Return fstrdatetype
        End Get
        Set(ByVal Value As String)
            fstrdatetype = Value
        End Set
    End Property
    Public Property apptype() As String
        Get
            Return fstrappnametype
        End Get
        Set(ByVal Value As String)
            fstrappnametype = Value
        End Set
    End Property

#End Region

#Region "Methods"
#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)

        connection.Open()

        Return connection
    End Function
#End Region
#Region "search product summary list"

    Public Function Getpslt() As DataSet

        Dim selConnection As SqlConnection = Nothing
        Dim selConn As SqlConnection = Nothing
        selConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_RPTPSLT_VIEW"
        Try
            trans = selConnection.BeginTransaction()

            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(15) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrminstate)
            Param(1).Value = Trim(fstrmaxstate)
            Param(2).Value = Trim(fstrminsercenter)
            Param(3).Value = Trim(fstrmaxsercenter)
            Param(4).Value = Trim(fstrmintech)
            Param(5).Value = Trim(fstrmaxtech)
            Param(6).Value = Trim(fstrminPartcode)
            Param(7).Value = Trim(fstrmaxPartcode)
            Param(8).Value = Trim(fstrservicetype)
            Param(9).Value = Trim(fddatemin)
            Param(10).Value = Trim(fddatemax)

            Param(11).Value = Trim(fstrlogctrid)
            Param(12).Value = Trim(fstrlogcompid)
            Param(13).Value = Trim(fstrlogsvrcenterid)
            Param(14).Value = Trim(fstrlogrank)



            Dim ds As DataSet = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)

            Return ds

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "ClsRptMdsl.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrModifiedBy, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "ClsRptMdsl.vb")
        Finally
            selConnection.Close()
            selConn.Close()
        End Try

    End Function
#End Region

#Region "Select ServerCenterID by UserID"
    Public Function GetSVCIDByUserID(ByVal UserID As String) As DataSet
        Dim sidConnection As SqlConnection = Nothing
        Dim sidConn As SqlConnection = Nothing
        sidConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        sidConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_FNCAPPS_SelSVCIDByUserID"
        Try
            trans = sidConnection.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(1) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sidConn, fstrSpName)
            Param(0).Value = Trim(UserID)

            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(sidConn, CommandType.StoredProcedure, fstrSpName, Param)
            ds.Tables(0).TableName = "ServerCenterID"
            Return ds

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Return Nothing
        End Try
    End Function
#End Region

#Region "Select DropDownList Data for technician based on ServCenterID"
    Public Function GetTCHINFODrpListData() As DataSet
        Dim selConnection As SqlConnection = Nothing
        Dim selConn As SqlConnection = Nothing
        selConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        fstrSpName = "BB_FNCAPPS_LoadTCHBySVCID_Frm_To"
        Try
            Dim Param() As SqlParameter = New SqlParameter() {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrminsercenter)
            Param(1).Value = Trim(fstrmaxsercenter)

            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            ds.Tables(0).TableName = "TCHINFO"
            Return ds
        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add(ex.Message).ToString()
            Return Nothing
        End Try
    End Function
#End Region


#End Region
End Class
