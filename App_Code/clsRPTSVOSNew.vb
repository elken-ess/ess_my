﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Imports BusinessEntity
Imports SQLDataAccess

Public Class ClsRptsvosNew
#Region "Declaration"

    Private _fstrminSvcID As String
    Private _fstrmaxSvcID As String
    Private _fstrminCusrid As String
    Private _fstrmaxCusrid As String
    Private _fstrminPrdctMd As String
    Private _fstrmaxPrdctMd As String
    Private _fstrminState As String
    Private _fstrmaxState As String
    Private _fstrminTchID As String
    Private _fstrmaxTchID As String
    Private _fstrminRoNo As String
    Private _fstrmaxRoNo As String
    Private _fstrminArea As String
    Private _fstrmaxArea As String
    Private _fstrmindate As DateTime
    Private _fstrmaxdate As DateTime
    Private _fstrminInstLdate As DateTime
    Private _fstrmaxInstLdate As DateTime
    Private _fstrSortBy As String
    Private _fstrUserName As String
    Private _fstrUserID As String
    '//////////////////////////////////////
    Private _fstrCountryID As String
    Private _fstrCompID As String
    Private _fstrSvcid As String
    Private _fstrRank As String
    Private _fstrStatus As String
    Private _fstrStatusText As String

    Private _fstrServiceType As String
    Private _fstrServiceTypeText As String

    Private _fstrSpName As String ' store procedure name
    Private _stafftype As String
#End Region

#Region "Properties"
    Public Property ServiceType() As String
        Get
            Return _fstrServiceType
        End Get
        Set(ByVal value As String)
            _fstrServiceType = Value
        End Set
    End Property

    Public Property ServiceTypeText() As String
        Get
            Return _fstrServiceTypeText
        End Get
        Set(ByVal value As String)
            _fstrServiceTypeText = Value
        End Set
    End Property



    Public Property Status() As String
        Get
            Return _fstrStatus
        End Get
        Set(ByVal value As String)
            _fstrStatus = Value
        End Set
    End Property

    Public Property StatusText() As String
        Get
            Return _fstrStatusText
        End Get
        Set(ByVal value As String)
            _fstrStatusText = Value
        End Set
    End Property


    Public Property Rank() As String
        Get
            Return _fstrRank
        End Get
        Set(ByVal value As String)
            _fstrRank = Value
        End Set
    End Property
    Public Property Svcid() As String
        Get
            Return _fstrSvcid
        End Get
        Set(ByVal value As String)
            _fstrSvcid = Value
        End Set
    End Property
    Public Property CompID() As String
        Get
            Return _fstrCompID
        End Get
        Set(ByVal value As String)
            _fstrCompID = Value
        End Set
    End Property
    Public Property CountryID() As String
        Get
            Return _fstrCountryID
        End Get
        Set(ByVal value As String)
            _fstrCountryID = Value
        End Set
    End Property
    Public Property MinCustmID() As String
        Get
            Return _fstrminCusrid
        End Get
        Set(ByVal value As String)
            _fstrminCusrid = value

        End Set
    End Property
    Public Property MaxCustmID() As String
        Get
            Return _fstrmaxCusrid
        End Get
        Set(ByVal value As String)
            _fstrmaxCusrid = value
        End Set
    End Property
    Public Property MinSvcID() As String
        Get
            Return _fstrminSvcID
        End Get
        Set(ByVal value As String)
            _fstrminSvcID = value
        End Set
    End Property
    Public Property MaxSvcID() As String
        Get
            Return _fstrmaxSvcID
        End Get
        Set(ByVal value As String)
            _fstrmaxSvcID = value
        End Set
    End Property
    Public Property MinPrdctMd() As String
        Get
            Return _fstrminPrdctMd
        End Get
        Set(ByVal value As String)
            _fstrminPrdctMd = value
        End Set
    End Property
    Public Property MaxPrdctMd() As String
        Get
            Return _fstrmaxPrdctMd
        End Get
        Set(ByVal value As String)
            _fstrmaxPrdctMd = value
        End Set
    End Property
    Public Property MinState() As String
        Get
            Return _fstrminState
        End Get
        Set(ByVal value As String)
            _fstrminState = value
        End Set
    End Property
    Public Property MaxState() As String
        Get
            Return _fstrmaxState
        End Get
        Set(ByVal value As String)
            _fstrmaxState = value
        End Set
    End Property
    Public Property MinTchID() As String
        Get
            Return _fstrminTchID
        End Get
        Set(ByVal value As String)
            _fstrminTchID = value
        End Set
    End Property
    Public Property MaxTchID() As String
        Get
            Return _fstrmaxTchID
        End Get
        Set(ByVal value As String)
            _fstrmaxTchID = value
        End Set
    End Property
    Public Property MinRoNo() As String
        Get
            Return _fstrminRoNo
        End Get
        Set(ByVal value As String)
            _fstrminRoNo = value
        End Set
    End Property
    Public Property MaxRoNo() As String
        Get
            Return _fstrmaxRoNo
        End Get
        Set(ByVal value As String)
            _fstrmaxRoNo = value
        End Set
    End Property
    Public Property MinArea() As String
        Get
            Return _fstrminArea
        End Get
        Set(ByVal value As String)
            _fstrminArea = value
        End Set
    End Property
    Public Property MaxArea() As String
        Get
            Return _fstrmaxArea
        End Get
        Set(ByVal value As String)
            _fstrmaxArea = value
        End Set
    End Property
    Public Property Mindate() As DateTime
        Get
            Return _fstrmindate
        End Get
        Set(ByVal value As DateTime)
            _fstrmindate = value
        End Set
    End Property
    Public Property Maxdate() As DateTime
        Get
            Return _fstrmaxdate
        End Get
        Set(ByVal value As DateTime)
            _fstrmaxdate = value
        End Set
    End Property
    Public Property MaxInstLdate() As DateTime
        Get
            Return _fstrmaxInstLdate
        End Get
        Set(ByVal value As DateTime)
            _fstrmaxInstLdate = value
        End Set
    End Property
    Public Property MinInstLdate() As DateTime
        Get
            Return _fstrminInstLdate
        End Get
        Set(ByVal value As DateTime)
            _fstrminInstLdate = value
        End Set
    End Property
    Public Property SortBy() As String
        Get
            Return _fstrSortBy
        End Get
        Set(ByVal value As String)
            _fstrSortBy = Value
        End Set
    End Property
    Public Property UserName() As String
        Get
            Return _fstrUserName
        End Get
        Set(ByVal value As String)
            _fstrUserName = Value
        End Set
    End Property
    Public Property StaffType() As String
        Get
            Return _stafftype
        End Get
        Set(ByVal value As String)
            _stafftype = value
        End Set
    End Property
#End Region

#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)

        connection.Open()

        Return connection
    End Function
#End Region

#Region "Service Order Summary (Overall) "
    Public Function GetServiceOrderSum() As DataSet
        Dim selConnection As SqlConnection = Nothing
        Dim selConn As SqlConnection = Nothing
        selConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        _fstrSpName = "BB_RPTSVOS_New_VIEW"

        Dim ds As DataSet = New DataSet()
        Try
            trans = selConnection.BeginTransaction()
            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(18) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, _fstrSpName)
            Param(0).Value = Trim(_fstrminSvcID)
            Param(1).Value = Trim(_fstrmaxSvcID)
            'Param(2).Value = Trim(_fstrminCusrid)
            'Param(3).Value = Trim(_fstrmaxCusrid)
            'Param(4).Value = Trim(_fstrminPrdctMd)
            'Param(5).Value = Trim(_fstrmaxPrdctMd)
            Param(2).Value = Trim(_fstrminState)
            Param(3).Value = Trim(_fstrmaxState)
            Param(4).Value = Trim(_fstrminTchID)
            Param(5).Value = Trim(_fstrmaxTchID)
            'Param(10).Value = Trim(_fstrminRoNo)
            'Param(11).Value = Trim(_fstrmaxRoNo)
            Param(6).Value = Trim(_fstrminArea)
            Param(7).Value = Trim(_fstrmaxArea)
            Param(8).Value = Trim(_fstrmindate)
            Param(9).Value = Trim(_fstrmaxdate)

            Param(10).Value = Trim(_fstrCountryID)
            Param(11).Value = Trim(_fstrCompID)
            Param(12).Value = Trim(_fstrSvcid)
            Param(13).Value = Trim(_fstrRank)
            Param(14).Value = Trim(_fstrUserName)
            Param(15).Value = Trim(_fstrStatus)
            Param(16).Value = Trim(_fstrServiceType)
            Param(17).Value = Trim(_stafftype)

            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, _fstrSpName, Param)
        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(_fstrUserName, ErrorLog.Item(1).ToString, _fstrSpName, WriteErrLog.SELE, "clsRPTSVOS.vb")
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(_fstrUserName, ErrorLog.Item(1).ToString, _fstrSpName, WriteErrLog.SELE, "clsRPTSVOS.vb")


        Finally
            selConnection.Close()
            selConn.Close()
        End Try
        Return ds

    End Function
#End Region
End Class
