Imports SQLDataAccess
Imports BusinessEntity
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Web
Imports System.Data

Public Class clsServiceBillUpdate

    'DECLARATION
#Region "Declaration.................................................."
    Private fServiceBillType As String
    Private fInvoice As String
    Private fInvoiceDate As Date
    Private fServiceBill As String
    Private fTechnician As String
    Private fServiceType As String
    Private fAppointmentPf As String
    Private fAppointmentNo As String
    Private fCustomerPf As String
    Private fCustomerNo As String
    Private fROSerialNo As String
    Private fCustomerName As String
    Private fCountryID As String
    Private fCompanyID As String
    Private fServiceCenterID As String
    Private fCreateBy As String
    Private fCreateDate As DateTime
    Private fModifyBy As String
    Private fModifyDate As DateTime
    Private fTotalGross As Double
    Private fDiscount As Double
    Private fTotalNet As Double
    Private fTax As Double
    Private fDiscRem As String
    Private fPriceID As String
    Private fPartID As String

    Private fstrErrorMessage As String

    Private fTaxPerc As Double
    Private fTaxID As String
    Private fCsAm As Double
    Private fCqAm As Double
    Private fCrAm As Double
    Private fCCAm As Double
    Private fOPAm As Double

    Private fTransactionPf As String
    Private fTransactionNo As String
    Private fStatus As String
    Private fInvDetail As DataSet


    Private fIPAddress As String
    Private fAppStat As String
    Private fROInstallDate As Date


    Private fresultcount As Integer

    Private fConID As String
    Private fConTY As String

    Dim fPKLDocNo As String
    Private fstrSessionID As String
    Private _RMSNo As String

#End Region

    'PROPERTIES
#Region "Properties..................................................."



    Public Property ErrorMessage() As String
        Get
            Return fstrErrorMessage

        End Get
        Set(ByVal value As String)
            fstrErrorMessage = value
        End Set
    End Property

    Public Property SessionID() As String
        Get
            Return fstrSessionID

        End Get
        Set(ByVal value As String)
            fstrSessionID = value
        End Set
    End Property

    Public Property InvoiceDetails() As DataSet
        Get
            Return fInvDetail

        End Get
        Set(ByVal value As DataSet)
            fInvDetail = value
        End Set
    End Property



    Public Property ServiceBillType() As String
        Get
            Return fServiceBillType
        End Get
        Set(ByVal value As String)
            fServiceBillType = value
        End Set
    End Property


    Public Property Invoice() As String
        Get
            Return fInvoice
        End Get
        Set(ByVal value As String)
            fInvoice = value
        End Set
    End Property

    Public Property InvoiceDate() As Date
        Get
            Return fInvoiceDate
        End Get
        Set(ByVal value As Date)
            fInvoiceDate = value
        End Set
    End Property

    Public Property ServiceBill() As String
        Get
            Return fServiceBill
        End Get
        Set(ByVal value As String)
            fServiceBill = value
        End Set
    End Property

    Public Property Technician() As String
        Get
            Return fTechnician
        End Get
        Set(ByVal value As String)
            fTechnician = value
        End Set
    End Property

    Public Property ServiceType() As String
        Get
            Return fServiceType
        End Get
        Set(ByVal value As String)
            fServiceType = value
        End Set
    End Property

    Public Property AppointmentPrefix() As String
        Get
            Return fAppointmentPf
        End Get
        Set(ByVal value As String)
            fAppointmentPf = value
        End Set
    End Property

    Public Property AppointmentNo() As String
        Get
            Return fAppointmentNo
        End Get
        Set(ByVal value As String)
            fAppointmentNo = value
        End Set
    End Property

    Public Property CustomerPrefix() As String
        Get
            Return fCustomerPf
        End Get
        Set(ByVal value As String)
            fCustomerPf = value
        End Set
    End Property

    Public Property CustomerNo() As String
        Get
            Return fCustomerNo
        End Get
        Set(ByVal value As String)
            fCustomerNo = value
        End Set
    End Property

    Public Property ROSerialNo() As String
        Get
            Return fROSerialNo
        End Get
        Set(ByVal value As String)
            fROSerialNo = value
        End Set
    End Property

    Public Property CustomerName() As String
        Get
            Return fCustomerName
        End Get
        Set(ByVal value As String)
            fCustomerName = value
        End Set
    End Property


    Public Property Country() As String
        Get
            Return fCountryID
        End Get
        Set(ByVal value As String)
            fCountryID = value
        End Set
    End Property


    Public Property Company() As String
        Get
            Return fCompanyID

        End Get
        Set(ByVal value As String)
            fCompanyID = value

        End Set
    End Property

    Public Property ServiceCenter() As String
        Get
            Return fServiceCenterID
        End Get
        Set(ByVal value As String)
            fServiceCenterID = value
        End Set
    End Property

    Public Property CreateBy() As String
        Get
            Return fCreateBy
        End Get
        Set(ByVal value As String)
            fCreateBy = value
        End Set
    End Property

    Public Property CreateDate() As Date
        Get
            Return fCreateDate
        End Get
        Set(ByVal value As Date)
            fCreateDate = value
        End Set
    End Property

    Public Property ModifyBy() As String
        Get
            Return fModifyBy
        End Get
        Set(ByVal value As String)
            fModifyBy = value
        End Set
    End Property

    Public Property ModifyDate() As Date
        Get
            Return fModifyDate
        End Get
        Set(ByVal value As Date)
            fModifyDate = value
        End Set
    End Property


    Public Property TotalGross() As Double
        Get
            Return fTotalGross
        End Get
        Set(ByVal value As Double)
            fTotalGross = value
        End Set
    End Property

    Public Property Discount() As Double
        Get
            Return fDiscount
        End Get
        Set(ByVal value As Double)
            fDiscount = value
        End Set
    End Property


    Public Property TotalNet() As Double
        Get
            Return fTotalNet
        End Get
        Set(ByVal value As Double)
            fTotalNet = value
        End Set
    End Property

    Public Property Tax() As Double
        Get
            Return fTax
        End Get
        Set(ByVal value As Double)
            fTax = value
        End Set
    End Property

    Public Property DiscRem() As String
        Get
            Return fDiscRem
        End Get
        Set(ByVal value As String)
            fDiscRem = value
        End Set
    End Property

    Public Property PartID() As String
        Get
            Return fPartID
        End Get
        Set(ByVal value As String)
            fPartID = value
        End Set
    End Property

    Public Property PriceID() As String
        Get
            Return fPriceID
        End Get
        Set(ByVal value As String)
            fPriceID = value
        End Set
    End Property


    Public Property TaxPercentage() As Double
        Get
            Return fTaxPerc
        End Get
        Set(ByVal value As Double)
            fTaxPerc = value
        End Set
    End Property

    Public Property TaxID() As String
        Get
            Return fTaxID
        End Get
        Set(ByVal value As String)
            fTaxID = value
        End Set
    End Property

    Public Property CashAmount() As Double
        Get
            Return fCsAm
        End Get
        Set(ByVal value As Double)
            fCsAm = value
        End Set
    End Property

    Public Property ChequeAmount() As Double
        Get
            Return fCqAm
        End Get
        Set(ByVal value As Double)
            fCqAm = value
        End Set
    End Property

    Public Property CreditAmount() As Double
        Get
            Return fCrAm
        End Get
        Set(ByVal value As Double)
            fCrAm = value
        End Set
    End Property

    Public Property CreditCardAmount() As Double
        Get
            Return fCCAm
        End Get
        Set(ByVal value As Double)
            fCCAm = value
        End Set
    End Property


    Public Property OtherPaymentAmount() As Double
        Get
            Return fOPAm
        End Get
        Set(ByVal value As Double)
            fOPAm = value
        End Set
    End Property


    Public Property Status() As String
        Get
            Return fStatus
        End Get
        Set(ByVal value As String)
            fStatus = value
        End Set
    End Property


    Public Property TransactionPrefix() As String
        Get
            Return fTransactionPf
        End Get
        Set(ByVal value As String)
            fTransactionPf = value
        End Set
    End Property

    Public Property TransactionNo() As String
        Get
            Return fTransactionNo
        End Get
        Set(ByVal value As String)
            fTransactionNo = value
        End Set
    End Property

    Public Property IPAddress() As String
        Get
            Return fIPAddress
        End Get
        Set(ByVal value As String)
            fIPAddress = value
        End Set
    End Property


    Public Property AppointmentStatus() As String
        Get
            Return fAppStat
        End Get
        Set(ByVal value As String)
            fAppStat = value
        End Set
    End Property



    Public Property ROInstallDate() As Date
        Get
            Return fROInstallDate
        End Get
        Set(ByVal value As Date)
            fROInstallDate = value
        End Set
    End Property


    Public Property PKLDocNo()
        Get
            Return fPKLDocNo
        End Get
        Set(ByVal value)
            fPKLDocNo = value
        End Set
    End Property

    Public Property ResultCount()
        Get
            Return Me.ResultCount
        End Get
        Set(ByVal value)
            ResultCount = value
        End Set
    End Property

    Public Property ContractID() As String
        Get
            Return fConID
        End Get
        Set(ByVal value As String)
            fConID = value
        End Set
    End Property

    Public Property ContractType() As String
        Get
            Return fConTY
        End Get
        Set(ByVal value As String)
            fConTY = value
        End Set
    End Property
    Public Property RMSNo() As String
        Get
            Return _RMSNo
        End Get
        Set(ByVal value As String)
            _RMSNo = value
        End Set
    End Property

#End Region

    'GET METHOD
#Region "Get Method.................................................................."
    Public Function GetPartName() As String

        Dim fstrSPName As String
        Dim scnTran As SqlConnection
        Dim scnProc As SqlConnection
        scnTran = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        scnProc = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        scnTran.Open()
        scnProc.Open()

        Dim trans As SqlTransaction = Nothing
        fstrSPName = "BB_FNCPARTID"
        Try
            trans = scnTran.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(9) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(scnProc, fstrSPName)
            Param(0).Value = Trim(fCountryID)
            Param(1).Value = Trim(fPartID)

            Dim myDataSet As DataSet = SqlHelper.ExecuteDataset(scnProc, CommandType.StoredProcedure, fstrSPName, Param)

            scnTran.Close()
            scnProc.Close()

            If myDataSet.Tables(0).Rows.Count <> 0 Then
                Return myDataSet.Tables(0).Rows(0).Item(0)
            Else
                Return ""
            End If

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fModifyBy, ErrorLog.Item(1).ToString, fstrSPName, WriteErrLog.ADD, "clsServiceBillUpdate.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fModifyBy, ErrorLog.Item(1).ToString, fstrSPName, WriteErrLog.ADD, "clsServiceBillUpdate.vb")
            Return -1
        End Try

    End Function

    Public Function GetPriceID() As String

        Dim fstrSPName As String
        Dim scnTran As SqlConnection
        Dim scnProc As SqlConnection
        scnTran = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        scnProc = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        scnTran.Open()
        scnProc.Open()

        Dim trans As SqlTransaction = Nothing

        fstrSPName = "BB_FNCPRICEID"
        Try
            trans = scnTran.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(9) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(scnProc, fstrSPName)
            Param(0).Value = Trim(fCountryID)
            Param(1).Value = Trim(fPartID)
            Param(2).Value = Trim(fPriceID)

            Dim myDataSet As DataSet = SqlHelper.ExecuteDataset(scnProc, CommandType.StoredProcedure, fstrSPName, Param)

            scnTran.Close()
            scnProc.Close()

            If myDataSet.Tables(0).Rows.Count <> 0 Then
                Return myDataSet.Tables(0).Rows(0).Item(0)
            Else
                Return ""
            End If

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fModifyBy, ErrorLog.Item(1).ToString, fstrSPName, WriteErrLog.ADD, "clsServiceBillUpdate.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fModifyBy, ErrorLog.Item(1).ToString, fstrSPName, WriteErrLog.ADD, "clsServiceBillUpdate.vb")
            Return -1
        End Try

    End Function

    Public Function GetApptDetails() As DataSet

        Dim fstrSPName As String
        Dim scnTran As SqlConnection
        Dim scnProc As SqlConnection
        scnTran = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        scnProc = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        scnTran.Open()
        scnProc.Open()

        Dim trans As SqlTransaction = Nothing
        fstrSPName = "BB_FNCAPPOINTMENT"
        Try
            trans = scnTran.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(10) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(scnProc, fstrSPName)
            Param(0).Value = Trim(fCountryID)
            Param(1).Value = Trim(fAppointmentPf)
            Param(2).Value = Trim(fAppointmentNo)
            Dim myDataSet As DataSet = SqlHelper.ExecuteDataset(scnProc, CommandType.StoredProcedure, fstrSPName, Param)
            scnTran.Close()
            scnProc.Close()

            Return myDataSet

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fModifyBy, ErrorLog.Item(1).ToString, fstrSPName, WriteErrLog.ADD, "clsServiceBillUpdate.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fModifyBy, ErrorLog.Item(1).ToString, fstrSPName, WriteErrLog.ADD, "clsServiceBillUpdate.vb")
        End Try
    End Function

    Public Function GetAppointments(ByVal TRNTY As String, _
                                    ByVal TRNNO As String, _
                                    ByVal CUSPF As String, _
                                    ByVal CUSID As String, _
                                    ByVal SVCID As String, _
                                    ByVal TCHID As String, _
                                    ByVal APTDT As Date, _
                                    ByVal STAT As String, _
                                    ByVal SVTID As String, _
                                    ByVal CTRID As String, _
                                    ByVal USERID As String) As DataSet

        Dim fstrSPName As String
        Dim scnTran As SqlConnection
        Dim scnProc As SqlConnection
        scnTran = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        scnProc = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        scnTran.Open()
        scnProc.Open()

        Dim trans As SqlTransaction = Nothing
        fstrSPName = "BB_FNCAPPOINTMENT_SEARCH" 'Tomas - something changed here 20180613
        Try
            trans = scnTran.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(10) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(scnProc, fstrSPName)
            Param(0).Value = TRNTY
            Param(1).Value = TRNNO
            Param(2).Value = CUSPF
            Param(3).Value = CUSID
            Param(4).Value = SVCID
            Param(5).Value = TCHID
            Param(6).Value = APTDT
            Param(7).Value = SVTID
            Param(8).Value = STAT
            Param(9).Value = CTRID
            Param(10).Value = USERID

            Dim myDataSet As DataSet = SqlHelper.ExecuteDataset(scnProc, CommandType.StoredProcedure, fstrSPName, Param)
            scnTran.Close()
            scnProc.Close()

            Return myDataSet

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fModifyBy, ErrorLog.Item(1).ToString, fstrSPName, WriteErrLog.ADD, "clsServiceBillUpdate.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fModifyBy, ErrorLog.Item(1).ToString, fstrSPName, WriteErrLog.ADD, "clsServiceBillUpdate.vb")
        End Try

    End Function

    Public Function GetServiceBill() As DataSet

        Dim fstrSPName As String
        Dim scnTran As SqlConnection
        Dim scnProc As SqlConnection
        scnTran = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        scnProc = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        scnTran.Open()
        scnProc.Open()

        Dim trans As SqlTransaction = Nothing
        fstrSPName = "BB_FNCSBU1_SELBYID"
        Try
            trans = scnTran.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(9) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(scnProc, fstrSPName)
            Param(0).Value = fTransactionPf
            Param(1).Value = fTransactionNo

            Dim myDataSet As DataSet = SqlHelper.ExecuteDataset(scnProc, CommandType.StoredProcedure, fstrSPName, Param)

            scnTran.Close()
            scnProc.Close()

            Return myDataSet

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fModifyBy, ErrorLog.Item(1).ToString, fstrSPName, WriteErrLog.ADD, "clsServiceBillUpdate.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fModifyBy, ErrorLog.Item(1).ToString, fstrSPName, WriteErrLog.ADD, "clsServiceBillUpdate.vb")
        End Try

    End Function

    Public Function GetCustomerRO() As DataSet
        Dim fstrSPName As String
        Dim scnTran As SqlConnection
        Dim scnProc As SqlConnection
        scnTran = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        scnProc = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        scnTran.Open()
        scnProc.Open()

        Dim trans As SqlTransaction = Nothing

        fstrSPName = "BB_FNCROU_SELBYCUS"
        Try
            trans = scnTran.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(9) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(scnProc, fstrSPName)
            Param(0).Value = Trim(fCustomerPf)
            Param(1).Value = Trim(fCustomerNo)

            Dim myDataSet As DataSet = SqlHelper.ExecuteDataset(scnProc, CommandType.StoredProcedure, fstrSPName, Param)

            scnTran.Close()
            scnProc.Close()

            Return myDataSet

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fModifyBy, ErrorLog.Item(1).ToString, fstrSPName, WriteErrLog.ADD, "clsServiceBillUpdate.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fModifyBy, ErrorLog.Item(1).ToString, fstrSPName, WriteErrLog.ADD, "clsServiceBillUpdate.vb")
        End Try

    End Function

    Public Function GetCustomers(ByVal TRNTY As String, _
                                   ByVal TRNNO As String, _
                                   ByVal CUSPF As String, _
                                   ByVal CUSID As String, _
                                   ByVal SVCID As String, _
                                   ByVal TCHID As String, _
                                   ByVal APTDT As Date, _
                                   ByVal STAT As String, _
                                   ByVal CTRID As String) As DataSet

        Dim fstrSPName As String
        Dim scnTran As SqlConnection
        Dim scnProc As SqlConnection
        scnTran = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        scnProc = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        scnTran.Open()
        scnProc.Open()

        Dim trans As SqlTransaction = Nothing
        fstrSPName = "BB_FNCAPPOINTMENT_SEARCH"
        Try
            trans = scnTran.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(9) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(scnProc, fstrSPName)
            Param(0).Value = TRNTY
            Param(1).Value = TRNNO
            Param(2).Value = CUSPF
            Param(3).Value = CUSID
            Param(4).Value = SVCID
            Param(5).Value = TCHID
            Param(6).Value = APTDT
            Param(7).Value = STAT
            Param(8).Value = CTRID

            Dim myDataSet As DataSet = SqlHelper.ExecuteDataset(scnProc, CommandType.StoredProcedure, fstrSPName, Param)
            scnTran.Close()
            scnProc.Close()

            Return myDataSet

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fModifyBy, ErrorLog.Item(1).ToString, fstrSPName, WriteErrLog.ADD, "clsServiceBillUpdate.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fModifyBy, ErrorLog.Item(1).ToString, fstrSPName, WriteErrLog.ADD, "clsServiceBillUpdate.vb")
        End Try
    End Function

    Public Function CheckROProductionDate() As Integer
        Dim fstrSPName As String
        Dim scnTran As SqlConnection
        Dim scnProc As SqlConnection
        scnTran = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        scnProc = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        scnTran.Open()
        scnProc.Open()

        Dim trans As SqlTransaction = Nothing
        fstrSPName = "CHECKROINSTALLDATE"
        Try
            trans = scnTran.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(5) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(scnProc, fstrSPName)
            Param(0).Value = fROSerialNo
            Param(1).Value = fROInstallDate
            Param(2).Value = 0

            SqlHelper.ExecuteNonQuery(scnProc, CommandType.StoredProcedure, fstrSPName, Param)
            Dim spCounter = Param(2).Value
            scnTran.Close()
            scnProc.Close()

            Return spCounter

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fModifyBy, ErrorLog.Item(1).ToString, fstrSPName, WriteErrLog.ADD, "clsServiceBillUpdate.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fModifyBy, ErrorLog.Item(1).ToString, fstrSPName, WriteErrLog.ADD, "clsServiceBillUpdate.vb")
            Return -1
        End Try
    End Function

    Public Function CheckDuplicateServiceBillNo() As Integer
        Dim fstrSPName As String
        Dim scnTran As SqlConnection
        Dim scnProc As SqlConnection
        scnTran = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        scnProc = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        scnTran.Open()
        scnProc.Open()

        Dim trans As SqlTransaction = Nothing
        fstrSPName = "BB_FNCSVCBILL_CHECKDUP"
        Try
            trans = scnTran.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(5) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(scnProc, fstrSPName)
            Param(0).Value = Me.fTransactionPf
            Param(1).Value = Me.fTransactionNo
            Param(2).Value = Me.fServiceBill
            Param(3).Value = 0

            SqlHelper.ExecuteNonQuery(scnProc, CommandType.StoredProcedure, fstrSPName, Param)
            Dim spCounter = Param(3).Value
            scnTran.Close()
            scnProc.Close()

            Return spCounter

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fModifyBy, ErrorLog.Item(1).ToString, fstrSPName, WriteErrLog.ADD, "clsServiceBillUpdate.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fModifyBy, ErrorLog.Item(1).ToString, fstrSPName, WriteErrLog.ADD, "clsServiceBillUpdate.vb")
            Return -1
        End Try
    End Function

    Public Function CheckGenPackList() As Integer
        Dim fstrSPName As String
        Dim scnTran As SqlConnection
        Dim scnProc As SqlConnection
        scnTran = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        scnProc = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        scnTran.Open()
        scnProc.Open()

        Dim trans As SqlTransaction = Nothing
        fstrSPName = "BB_FNCGENPACKLIST_CHECK"
        Try
            trans = scnTran.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(7) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(scnProc, fstrSPName)
            Param(0).Value = Me.fTechnician
            Param(1).Value = Me.fServiceCenterID
            Param(2).Value = Me.fCompanyID
            Param(3).Value = Me.fCountryID
            Param(4).Value = Me.fCreateBy
            Param(5).Value = 0

            SqlHelper.ExecuteNonQuery(scnProc, CommandType.StoredProcedure, fstrSPName, Param)
            Dim spCounter = Param(5).Value
            scnTran.Close()
            scnProc.Close()

            Return spCounter

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fModifyBy, ErrorLog.Item(1).ToString, fstrSPName, WriteErrLog.ADD, "clsServiceBillUpdate.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fModifyBy, ErrorLog.Item(1).ToString, fstrSPName, WriteErrLog.ADD, "clsServiceBillUpdate.vb")
            Return -1
        End Try
    End Function

    Public Function GetContractID() As Integer
        Dim fstrSPName As String
        Dim scnTran As SqlConnection
        Dim scnProc As SqlConnection
        scnTran = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        scnProc = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        scnTran.Open()
        scnProc.Open()

        Dim trans As SqlTransaction = Nothing

        fstrSPName = "BB_FNCGETCONID_BYROUID"
        Try
            trans = scnTran.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(9) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(scnProc, fstrSPName)
            Param(0).Value = Trim(fCustomerPf)
            Param(1).Value = Trim(fCustomerNo)
            Param(2).Value = Trim(Me.fROSerialNo)
            Param(3).Value = Trim(Me.fInvoiceDate)
            Param(4).Value = ""
            Param(5).Value = ""

            SqlHelper.ExecuteNonQuery(scnProc, CommandType.StoredProcedure, fstrSPName, Param)
            fConID = Param(4).Value
            fConTY = Param(5).Value
            Return 0

            scnTran.Close()
            scnProc.Close()

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fModifyBy, ErrorLog.Item(1).ToString, fstrSPName, WriteErrLog.ADD, "clsServiceBillUpdate.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fModifyBy, ErrorLog.Item(1).ToString, fstrSPName, WriteErrLog.ADD, "clsServiceBillUpdate.vb")
            Return -1
        End Try
    End Function

#End Region
    Public Function getStdStockListByTechIDRPT(ByVal TRANTYPE As String, _
                                             ByVal TRANNO As String, _
                                             ByVal SVCID As String, _
                                             ByVal TECHID As String, _
                                              ByVal USERID As String) As DataSet
        Dim stocklistds As New DataSet

        Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim fstrSpName As String = "BB_RPTSDTS_SelByTechID"

        Try
            Dim Param() As SqlParameter = New SqlParameter(5) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(conn, fstrSpName)
            Param(0).Value = TRANTYPE '"STD"
            Param(1).Value = TRANNO
            Param(2).Value = SVCID
            Param(3).Value = TECHID
            Param(4).Value = USERID
            stocklistds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, fstrSpName, Param)
            getStdStockListByTechIDRPT = stocklistds
        Catch ex As SqlException
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(USERID, ErrorLog.Item(1).ToString, fstrSpName, _
                               WriteErrLog.ADD, "clsStdStockList.vb")

        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(USERID, ErrorLog.Item(1).ToString, fstrSpName, _
                               WriteErrLog.ADD, "clsStdStockList.vb")

        Finally
            conn.Close()
        End Try

    End Function
    'SET METHOD
#Region "set Method.................................................................."

    Public Function SaveServiceBill() As String

        Dim fstrSPName As String
        Dim scnTran As SqlConnection
        Dim scnProc As SqlConnection
        scnTran = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        scnProc = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        scnTran.Open()
        scnProc.Open()

        Dim trans As SqlTransaction = Nothing

        fstrSPName = "BB_FNCSVCBILL_ADD"
        Try
            trans = scnTran.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(32) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(scnProc, fstrSPName)
            Param(0).Value = ""
            Param(1).Value = ""
            Param(2).Value = ""
            Param(3).Value = fServiceBill
            Param(4).Value = fServiceCenterID.ToUpper
            Param(5).Value = fCompanyID.ToUpper
            Param(6).Value = fCountryID.ToUpper
            Param(7).Value = fServiceBillType.ToUpper
            Param(8).Value = fInvoiceDate
            Param(9).Value = fTechnician.ToUpper
            Param(10).Value = fServiceType.ToUpper
            Param(11).Value = fROSerialNo.ToUpper
            Param(12).Value = fCustomerPf.ToUpper
            Param(13).Value = fCustomerNo
            Param(14).Value = fTotalGross
            Param(15).Value = fDiscount
            Param(16).Value = fTax
            Param(17).Value = fDiscRem.ToUpper
            Param(18).Value = fStatus
            Param(19).Value = fAppointmentPf.ToUpper
            Param(20).Value = fAppointmentNo
            Param(21).Value = fCreateBy
            Param(22).Value = fCreateDate
            Param(23).Value = fModifyBy
            Param(24).Value = fModifyDate
            Param(25).Value = fCsAm
            Param(26).Value = fCqAm
            Param(27).Value = fCrAm
            Param(28).Value = fCCAm
            Param(29).Value = fOPAm
            Param(30).Value = fstrSessionID
            Param(31).Value = fIPAddress

            SqlHelper.ExecuteNonQuery(scnProc, CommandType.StoredProcedure, fstrSPName, Param)
            fTransactionPf = Trim(Param(0).Value)
            fTransactionNo = Trim(Param(1).Value)
            fServiceBill = Trim(Param(2).Value)

            scnTran.Close()
            scnProc.Close()

            SaveServiceBillDetails()
            Return SaveStockMovement()

        Catch ex As Exception
            trans.Rollback()
            scnTran.Close()
            scnProc.Close()

            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            If ex.Message.Trim <> "" Then
                fstrErrorMessage = "[1]" & ex.Message
            End If
            ErrorLog.Add(ex.Message).ToString()

            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fModifyBy, ErrorLog.Item(1).ToString, fstrSPName, WriteErrLog.ADD, "clsServiceBillUpdate.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            scnTran.Close()
            scnProc.Close()

            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()

            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fModifyBy, ErrorLog.Item(1).ToString, fstrSPName, WriteErrLog.ADD, "clsServiceBillUpdate.vb")
            Return -1
        End Try

    End Function

    Public Function SaveServiceBillDetails() As String

        Dim fstrSPName As String
        Dim scnTran As SqlConnection
        Dim scnProc As SqlConnection
        scnTran = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        scnProc = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        scnTran.Open()
        scnProc.Open()

        SaveServiceBillDetails = fTransactionPf + fTransactionNo

        Dim trans As SqlTransaction = Nothing
        Dim Param() As SqlParameter = New SqlParameter(20) {}

        fstrSPName = "BB_FNCSVCBILL_ADD1"
        Dim i As Integer
        trans = scnTran.BeginTransaction()
        If fInvDetail.Tables.Count = 0 Then Exit Function

        While i <= fInvDetail.Tables(0).Rows.Count - 1
            Try
                Param = SqlHelperParameterCache.GetSpParameterSet(scnProc, fstrSPName)
                Param(0).Value = fTransactionPf
                Param(1).Value = fTransactionNo
                Param(2).Value = fInvDetail.Tables(0).Rows(i).Item("ItemNo")
                Param(3).Value = fCountryID
                Param(4).Value = fInvDetail.Tables(0).Rows(i).Item("Part Code")
                Param(5).Value = fInvDetail.Tables(0).Rows(i).Item("PriceID")
                Param(6).Value = fInvDetail.Tables(0).Rows(i).Item("UnitPrice")
                Param(7).Value = fInvDetail.Tables(0).Rows(i).Item("Quantity")
                Param(8).Value = fTaxID
                Param(9).Value = fTaxPerc
                Param(10).Value = fInvDetail.Tables(0).Rows(i).Item("Tax")
                Param(11).Value = fInvDetail.Tables(0).Rows(i).Item("Amount")
                Param(12).Value = fStatus

                SqlHelper.ExecuteNonQuery(scnProc, CommandType.StoredProcedure, fstrSPName, Param)

                'LLY 20160613
                'if service type = SP - check in store procedure
                SaveServiceBillDetails_SP()

            Catch ex As Exception
                trans.Rollback()
                scnTran.Close()
                scnProc.Close()

                Dim ErrorLog As ArrayList = New ArrayList
                ErrorLog.Add("Error").ToString()
                If ex.Message.Trim <> "" Then
                    fstrErrorMessage = "[2]" & ex.Message
                End If
                ErrorLog.Add(ex.Message).ToString()

                Dim WriteErrLog As New clsLogFile()
                WriteErrLog.ErrorLog(fModifyBy, ErrorLog.Item(1).ToString, fstrSPName, WriteErrLog.ADD, "clsServiceBillUpdate.vb")
                Return -1

            Catch ex As SqlException
                trans.Rollback()
                scnTran.Close()
                scnProc.Close()

                Dim ErrorLog As ArrayList = New ArrayList
                ErrorLog.Add("Error").ToString()
                ErrorLog.Add(ex.Number).ToString()

                Dim WriteErrLog As New clsLogFile()
                WriteErrLog.ErrorLog(fModifyBy, ErrorLog.Item(1).ToString, fstrSPName, WriteErrLog.ADD, "clsServiceBillUpdate.vb")
                Return -1
            End Try

            i = i + 1
        End While

        scnTran.Close()
        scnProc.Close()

    End Function

    'LLY 20160603 for SP unit Plan 
    Public Function SaveServiceBillDetails_SP() As String

        Dim fstrSPName As String
        Dim scnTran As SqlConnection
        Dim scnProc As SqlConnection
        scnTran = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        scnProc = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        scnTran.Open()
        scnProc.Open()

        SaveServiceBillDetails_SP = fTransactionPf + fTransactionNo

        Dim trans As SqlTransaction = Nothing
        Dim Param() As SqlParameter = New SqlParameter(20) {}

        fstrSPName = "BB_FNCSVCBILL_ADD_SP"
        Dim i As Integer
        trans = scnTran.BeginTransaction()
        If fInvDetail.Tables.Count = 0 Then Exit Function

        While i <= fInvDetail.Tables(0).Rows.Count - 1
            Try
                Param = SqlHelperParameterCache.GetSpParameterSet(scnProc, fstrSPName)
                Param(0).Value = fTransactionNo
                Param(1).Value = fTechnician.ToUpper
                Param(2).Value = fInvDetail.Tables(0).Rows(i).Item("Part Code")
                Param(3).Value = DBNull.Value 'SP generate datetime
                Param(4).Value = DBNull.Value 'payable datetime 
                Param(5).Value = fInvDetail.Tables(0).Rows(i).Item("Amount") 'payable amount
                Param(6).Value = "BL" 'status - cancel, completed, pay1, pay2, pay3
                Param(7).Value = "" ' remark
                'generate 3 

                SqlHelper.ExecuteNonQuery(scnProc, CommandType.StoredProcedure, fstrSPName, Param)

            Catch ex As Exception
                trans.Rollback()
                scnTran.Close()
                scnProc.Close()

                Dim ErrorLog As ArrayList = New ArrayList
                ErrorLog.Add("Error").ToString()
                If ex.Message.Trim <> "" Then
                    fstrErrorMessage = "[2]" & ex.Message
                End If
                ErrorLog.Add(ex.Message).ToString()

                Dim WriteErrLog As New clsLogFile()
                WriteErrLog.ErrorLog(fModifyBy, ErrorLog.Item(1).ToString, fstrSPName, WriteErrLog.ADD, "clsServiceBillUpdate.vb")
                Return -1

            Catch ex As SqlException
                trans.Rollback()
                scnTran.Close()
                scnProc.Close()

                Dim ErrorLog As ArrayList = New ArrayList
                ErrorLog.Add("Error").ToString()
                ErrorLog.Add(ex.Number).ToString()

                Dim WriteErrLog As New clsLogFile()
                WriteErrLog.ErrorLog(fModifyBy, ErrorLog.Item(1).ToString, fstrSPName, WriteErrLog.ADD, "clsServiceBillUpdate.vb")
                Return -1
            End Try

            i = i + 1
        End While

        scnTran.Close()
        scnProc.Close()

    End Function


    Public Function SaveStockMovement() As String

        Dim fstrSPName As String
        Dim scnTran As SqlConnection
        Dim scnProc As SqlConnection
        scnTran = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        scnProc = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        scnTran.Open()
        scnProc.Open()

        SaveStockMovement = fTransactionPf + fTransactionNo

        Dim trans As SqlTransaction = Nothing
        Dim Param() As SqlParameter = New SqlParameter(20) {}

        fstrSPName = "BB_FNCITPM_ADD"
        Dim i As Integer
        trans = scnTran.BeginTransaction()

        Try
            Param = SqlHelperParameterCache.GetSpParameterSet(scnProc, fstrSPName)
            Param(0).Value = fTransactionPf
            Param(1).Value = fTransactionNo
            SqlHelper.ExecuteNonQuery(scnProc, CommandType.StoredProcedure, fstrSPName, Param)

        Catch ex As Exception
            trans.Rollback()
            scnTran.Close()
            scnProc.Close()

            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            If ex.Message.Trim <> "" Then
                fstrErrorMessage = "[3]" & ex.Message
            End If

            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fModifyBy, ErrorLog.Item(1).ToString, fstrSPName, WriteErrLog.ADD, "clsServiceBillUpdate.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            scnTran.Close()
            scnProc.Close()

            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fModifyBy, ErrorLog.Item(1).ToString, fstrSPName, WriteErrLog.ADD, "clsServiceBillUpdate.vb")
            Return -1
        End Try

        scnTran.Close()
        scnProc.Close()

    End Function

    Public Function GeneratePackingList() As String

        Dim fstrSPName As String
        Dim scnTran As SqlConnection
        Dim scnProc As SqlConnection
        scnTran = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        scnProc = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        scnTran.Open()
        scnProc.Open()

        GeneratePackingList = fTransactionPf + fTransactionNo

        Dim trans As SqlTransaction = Nothing
        Dim Param() As SqlParameter = New SqlParameter(20) {}

        fstrSPName = "BB_FNCGENPACKLIST"
        Dim i As Integer
        trans = scnTran.BeginTransaction()

        Try
            Param = SqlHelperParameterCache.GetSpParameterSet(scnProc, fstrSPName)
            Param(0).Value = Me.fTechnician
            Param(1).Value = Me.fServiceCenterID
            Param(2).Value = Me.fCompanyID
            Param(3).Value = Me.fCountryID
            Param(4).Value = Me.fCreateBy
            Param(5).Value = ""
            Param(6).Value = ""

            SqlHelper.ExecuteNonQuery(scnProc, CommandType.StoredProcedure, fstrSPName, Param)
            Me.PKLDocNo = Param(6).Value
            Return Param(5).Value
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fModifyBy, ErrorLog.Item(1).ToString, fstrSPName, WriteErrLog.ADD, "clsServiceBillUpdate.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fModifyBy, ErrorLog.Item(1).ToString, fstrSPName, WriteErrLog.ADD, "clsServiceBillUpdate.vb")
            Return -1
        End Try

        scnTran.Close()
        scnProc.Close()
    End Function

    Public Function UpdateServiceBill() As String

        Dim fstrSPName As String
        Dim scnTran As SqlConnection
        Dim scnProc As SqlConnection
        scnTran = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        scnProc = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        scnTran.Open()
        scnProc.Open()

        Dim trans As SqlTransaction = Nothing

        fstrSPName = "BB_FNCSVCBILL_UPD"
        Try
            trans = scnTran.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(31) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(scnProc, fstrSPName)
            Param(0).Value = fTransactionPf
            Param(1).Value = fTransactionNo
            Param(2).Value = fServiceCenterID
            Param(3).Value = fCompanyID
            Param(4).Value = fCountryID
            Param(5).Value = fServiceBill
            Param(6).Value = fServiceBillType
            Param(7).Value = fInvoiceDate
            Param(8).Value = fTechnician
            Param(9).Value = fServiceType
            Param(10).Value = fROSerialNo
            Param(11).Value = fCustomerPf
            Param(12).Value = fCustomerNo
            Param(13).Value = fTotalGross
            Param(14).Value = fDiscount
            Param(15).Value = fTax
            Param(16).Value = fDiscRem
            Param(17).Value = fStatus
            Param(18).Value = fAppointmentPf
            Param(19).Value = fAppointmentNo
            Param(20).Value = fCreateBy
            Param(21).Value = fCreateDate
            Param(22).Value = fModifyBy
            Param(23).Value = fModifyDate
            Param(24).Value = fCsAm
            Param(25).Value = fCqAm
            Param(26).Value = fCrAm
            Param(27).Value = fCCAm
            Param(28).Value = fOPAm
            Param(29).Value = fIPAddress
            Param(30).Value = fstrSessionID

            SqlHelper.ExecuteNonQuery(scnProc, CommandType.StoredProcedure, fstrSPName, Param)
            fTransactionNo = Trim(Param(1).Value)
            fTransactionPf = Trim(Param(0).Value)

            scnTran.Close()
            scnProc.Close()

            'SaveServiceBillDetails()
            'Return SaveStockMovement()
            Return 1
        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fModifyBy, ErrorLog.Item(1).ToString, fstrSPName, WriteErrLog.ADD, "clsServiceBillUpdate.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fModifyBy, ErrorLog.Item(1).ToString, fstrSPName, WriteErrLog.ADD, "clsServiceBillUpdate.vb")
            Return -1
        End Try

    End Function

    Public Function SaveDummyAppointment() As String

        Dim fstrSPName As String
        Dim scnTran As SqlConnection
        Dim scnProc As SqlConnection
        scnTran = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        scnProc = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        scnTran.Open()
        scnProc.Open()

        Dim trans As SqlTransaction = Nothing

        fstrSPName = "BB_FNCDUMMYAPPT_ADD"
        Try
            trans = scnTran.BeginTransaction()

            Dim Param() As SqlParameter = New SqlParameter(30) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(scnProc, fstrSPName)
            Param(0).Value = ""
            Param(1).Value = ""
            Param(2).Value = Me.fInvoiceDate
            Param(3).Value = Me.fCustomerPf
            Param(4).Value = Me.fCustomerNo
            PARAM(5).VALUE = Me.fROSerialNo
            Param(6).Value = Me.fCustomerName
            Param(7).Value = ""
            Param(8).Value = Me.fServiceType
            Param(9).Value = Me.fTechnician
            Param(10).Value = Me.fAppStat
            Param(11).Value = Me.fServiceCenterID
            Param(12).Value = Me.fCompanyID
            Param(13).Value = Me.fCountryID
            Param(14).Value = Me.CreateBy
            Param(15).Value = Me.CreateDate
            Param(16).Value = Me.ModifyBy
            Param(17).Value = Me.ModifyDate

            SqlHelper.ExecuteNonQuery(scnProc, CommandType.StoredProcedure, fstrSPName, Param)
            fAppointmentNo = Trim(Param(1).Value)
            fAppointmentPf = Trim(Param(0).Value)

            scnTran.Close()
            scnProc.Close()

            Return fAppointmentNo

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fModifyBy, ErrorLog.Item(1).ToString, fstrSPName, WriteErrLog.ADD, "clsServiceBillUpdate.vb")
            Return -1

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fModifyBy, ErrorLog.Item(1).ToString, fstrSPName, WriteErrLog.ADD, "clsServiceBillUpdate.vb")
            Return -1
        End Try

    End Function

#End Region

End Class
