Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web
Imports BusinessEntity
Imports SQLDataAccess

Public Class ClsRptSOSA
#Region "Declaration"

    Private fstrSpName As String ' store procedure name
    Private fstruserid As String ' username
    Private fstrusername As String ' username
    Private fstrminsvcid As String
    Private fstrmaxsvcid As String

    Private fstrmincustid As String
    Private fstrmaxcustid As String
    Private fstrmindate As System.DateTime
    Private fstrmaxdate As System.DateTime

   
    Private fstrminapptno As String

    Private fstrmaxapptno As String

    
    Private fstrminmodel As String
    Private fstrmaxmodel As String

    Private fstrApptStatus As String
    Private fstrApptType As String
    Private fstrrank As String ' rank

    Private fstrctry As String ' user country
    Private fstrcomp As String ' user company
    Private fstrsvc As String '  user svcid
    Private fstrApptStatustext As String
    Private fstrApptTypetext As String

    Private fstrTransactionType As String
#End Region

#Region "Properties"

    Public Property TransactionType() As String
        Get
            Return fstrTransactionType
        End Get
        Set(ByVal Value As String)
            fstrTransactionType = Value
        End Set
    End Property


    Public Property userid() As String
        Get
            Return fstruserid
        End Get
        Set(ByVal Value As String)
            fstruserid = Value
        End Set
    End Property
    Public Property username() As String
        Get
            Return fstrusername
        End Get
        Set(ByVal Value As String)
            fstrusername = Value
        End Set
    End Property
    Public Property rank() As String
        Get
            Return fstrrank
        End Get
        Set(ByVal Value As String)
            fstrrank = Value
        End Set
    End Property
    Public Property ctryid() As String
        Get
            Return fstrctry
        End Get
        Set(ByVal Value As String)
            fstrctry = Value
        End Set
    End Property
    Public Property compid() As String
        Get
            Return fstrcomp
        End Get
        Set(ByVal Value As String)
            fstrcomp = Value
        End Set
    End Property
    Public Property svcid() As String
        Get
            Return fstrsvc
        End Get
        Set(ByVal Value As String)
            fstrsvc = Value
        End Set
    End Property


    Public Property Minsvcid() As String
        Get
            Return fstrminsvcid
        End Get
        Set(ByVal Value As String)
            fstrminsvcid = Value
        End Set
    End Property

    Public Property Maxsvcid() As String
        Get
            Return fstrmaxsvcid
        End Get
        Set(ByVal Value As String)
            fstrmaxsvcid = Value
        End Set
    End Property

    Public Property Mincustid() As String
        Get
            Return fstrmincustid
        End Get
        Set(ByVal Value As String)
            fstrmincustid = Value
        End Set
    End Property

    Public Property Maxcustid() As String
        Get
            Return fstrmaxcustid
        End Get
        Set(ByVal Value As String)
            fstrmaxcustid = Value
        End Set
    End Property

    Public Property Mindate() As String
        Get
            Return fstrmindate
        End Get
        Set(ByVal Value As String)
            fstrmindate = Value
        End Set
    End Property

    Public Property Maxdate() As String
        Get
            Return fstrmaxdate
        End Get
        Set(ByVal Value As String)
            fstrmaxdate = Value
        End Set
    End Property

    Public Property Minapptno() As String
        Get
            Return fstrminapptno
        End Get
        Set(ByVal Value As String)
            fstrminapptno = Value
        End Set
    End Property

    Public Property Maxapptno() As String
        Get
            Return fstrmaxapptno
        End Get
        Set(ByVal Value As String)
            fstrmaxapptno = Value
        End Set
    End Property
    Public Property Minmodel() As String
        Get
            Return fstrminmodel
        End Get
        Set(ByVal Value As String)
            fstrminmodel = Value
        End Set
    End Property
    Public Property Maxmodel() As String
        Get
            Return fstrmaxmodel
        End Get
        Set(ByVal Value As String)
            fstrmaxmodel = Value
        End Set
    End Property
    Public Property ApptStatus() As String
        Get
            Return fstrApptStatus
        End Get
        Set(ByVal Value As String)
            fstrApptStatus = Value
        End Set
    End Property

    Public Property ApptType() As String
        Get
            Return fstrApptType
        End Get
        Set(ByVal Value As String)
            fstrApptType = Value
        End Set
    End Property
    Public Property ApptStatustext() As String
        Get
            Return fstrApptStatustext
        End Get
        Set(ByVal Value As String)
            fstrApptStatustext = Value
        End Set
    End Property

    Public Property ApptTypetext() As String
        Get
            Return fstrApptTypetext
        End Get
        Set(ByVal Value As String)
            fstrApptTypetext = Value
        End Set
    End Property

#End Region
#Region "Methods"
#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)

        connection.Open()

        Return connection
    End Function
#End Region
#Region "Select contact"

    Public Function GetRptsosa() As DataSet

        Dim selConnection As SqlConnection = Nothing
        Dim selConn As SqlConnection = Nothing
        selConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_RPTSOSA_VIEW"
        Try
            trans = selConnection.BeginTransaction()


            ' define search fields 
            Dim Param() As SqlParameter = New SqlParameter(17) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrminsvcid)
            Param(1).Value = Trim(fstrmaxsvcid)
            Param(2).Value = Trim(fstrmincustid)
            Param(3).Value = Trim(fstrmaxcustid)
            Param(4).Value = Trim(fstrmindate)
            Param(5).Value = Trim(fstrmaxdate)
            Param(6).Value = Trim(fstrminapptno)
            Param(7).Value = Trim(fstrmaxapptno)
            Param(8).Value = Trim(fstrminmodel)
            Param(9).Value = Trim(fstrmaxmodel)
            Param(10).Value = Trim(fstrApptStatus)
            Param(11).Value = Trim(fstrApptType)
            Param(12).Value = Trim(Me.fstrctry)
            Param(13).Value = Trim(fstrcomp)
            Param(14).Value = Trim(fstrsvc)
            Param(15).Value = Trim(fstrrank)
            Param(16).Value = Trim(fstrTransactionType)

            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)
            Dim count As Integer
            Dim statXmlTr As New clsXml
            Dim strPanm As String
            statXmlTr.XmlFile = ConfigurationSettings.AppSettings("StatMsg")
            For count = 0 To ds.Tables(0).Rows.Count - 1
                Dim statusid As String = ds.Tables(0).Rows(count).Item(4).ToString
                strPanm = statXmlTr.GetLabelName("StatusMessage", statusid)
                ds.Tables(0).Rows(count).Item(4) = strPanm

                Dim lstrReason As String = ds.Tables(0).Rows(count).Item(7).ToString
                If lstrReason <> "" Then
                    strPanm = statXmlTr.GetLabelName("StatusMessage", lstrReason)
                    ds.Tables(0).Rows(count).Item(7) = strPanm
                End If
            Next

            Return ds

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "ClsRptSOSA.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "ClsRptSOSA.vb")
        Finally
            selConnection.Close()
            selConn.Close()
        End Try

    End Function
#End Region
#End Region

End Class
