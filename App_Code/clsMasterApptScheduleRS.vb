Imports Microsoft.VisualBasic
Imports SQLDataAccess
Imports System.Data
Imports System.Data.SqlClient
Imports BusinessEntity
Imports System.Configuration
Imports System.Globalization

Public Structure Index0RS
    Public CustName As Integer
    Public StartTime As Integer
    Public EndTime As Integer
    Public ZoneID As Integer
    Public CreateTime As Integer
    Public IsInOthers As Integer
End Structure

Public Class clsMasterApptScheduleRS
    Private ds As DataSet
    Private presentationtab As DataTable
    Private fstrSpName As String
    Private MaxApptAllocNum As Integer
    Private tabindex0 As Index0
    Private RealTotalAppt As Integer
    Private ObjTotalAppt As Integer

    Private datatable As DataTable
    Private apptdate As DateTime
    Private svcid As String
    Private user As String
    Private fstrJobType As String
    Private fintTotalSS As Integer
    Private fintTotalAS As Integer

#Region "Properties"
    Public Property TotalSS() As Integer
        Get
            Return fintTotalSS
        End Get
        Set(ByVal Value As Integer)
            fintTotalSS = Value
        End Set
    End Property

    Public Property TotalAS() As Integer
        Get
            Return fintTotalAS
        End Get
        Set(ByVal Value As Integer)
            fintTotalAS = Value
        End Set
    End Property


    Public Property JobType() As String
        Get
            Return fstrJobType
        End Get
        Set(ByVal Value As String)
            fstrJobType = Value
        End Set
    End Property

    Public Property AppointDate() As DateTime
        Get
            Return apptdate
        End Get
        Set(ByVal Value As DateTime)
            apptdate = Value
        End Set
    End Property
    Public Property ServiceCenter() As String
        Get
            Return svcid
        End Get
        Set(ByVal Value As String)
            svcid = Value
        End Set
    End Property
    Public Property UserID() As String
        Get
            Return user
        End Get
        Set(ByVal Value As String)
            user = Value
        End Set
    End Property
#End Region

    Sub New()
        ds = New DataSet()
        tabindex0 = New Index0()
        Me.presentationtab = New DataTable
        Me.datatable = New DataTable
        MaxApptAllocNum = 9
    End Sub

    Public Sub setMaxApptAllocNum(ByVal num As Integer)
        MaxApptAllocNum = num
    End Sub

    Protected Function getInfo(ByVal userid As String, ByVal apptdate As String, _
                             ByVal svcid As String) As Integer
        Dim result As Integer = 0
        Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        'fstrSpName = "BB_FNCAPPS_View2"

        fstrSpName = "BB_FNCAPPS_ViewMasterAppt_RS"
        Try
            Dim Param() As SqlParameter = New SqlParameter(5) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(conn, fstrSpName)
            Param(0).Value = apptdate
            Param(1).Value = svcid
            Param(2).Value = userid
            Param(3).Value = Me.fstrJobType
            Param(4).Direction = ParameterDirection.Output
            ds = New DataSet()

            ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, fstrSpName, Param)
            result = Param(4).Value
        Catch ex As SqlException
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(userid, ErrorLog.Item(1).ToString, fstrSpName, _
                               WriteErrLog.ADD, "clsMasterApptScheduleRS.vb")
            result = -1
        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(userid, ErrorLog.Item(1).ToString, fstrSpName, _
                               WriteErrLog.ADD, "clsMasterApptScheduleRS.vb")
            result = -1
        Finally
            conn.Close()
        End Try

        Return result
    End Function

    Private Sub alterDataTable()
        If Me.ds IsNot Nothing Then
            If Me.ds.Tables.Count > 0 Then
                tabindex0.CustName = Me.ds.Tables(0).Columns.IndexOf("MCUS_ENAME")
                tabindex0.StartTime = Me.ds.Tables(0).Columns.IndexOf("FAPT_STRTM")
                tabindex0.EndTime = Me.ds.Tables(0).Columns.IndexOf("FAPT_ENDTM")
                tabindex0.ZoneID = Me.ds.Tables(0).Columns.IndexOf("FAPT_ZONID")
                tabindex0.CreateTime = Me.ds.Tables(0).Columns.IndexOf("FAPT_CREDT")

                Me.ds.Tables(0).Columns.Add("IsInOthers", System.Type.GetType("System.Int32"))
                tabindex0.IsInOthers = Me.ds.Tables(0).Columns.IndexOf("IsInOthers")
                For i As Integer = 0 To Me.ds.Tables(0).Rows.Count - 1
                    Me.ds.Tables(0).Rows(i).Item(tabindex0.IsInOthers) = 1
                Next

                '*******  create presentationtab ********
                Me.presentationtab.Columns.Add("ZoneID", System.Type.GetType("System.String"))
                Me.presentationtab.Columns.Add("0", System.Type.GetType("System.String"))
                Me.presentationtab.Columns.Add("0900", System.Type.GetType("System.Int32"))
                Me.presentationtab.Columns.Add("1030", System.Type.GetType("System.Int32"))
                Me.presentationtab.Columns.Add("1200", System.Type.GetType("System.Int32"))
                Me.presentationtab.Columns.Add("1330", System.Type.GetType("System.Int32"))
                Me.presentationtab.Columns.Add("1500", System.Type.GetType("System.Int32"))
                Me.presentationtab.Columns.Add("1630", System.Type.GetType("System.Int32"))
                Me.presentationtab.Columns.Add("1800", System.Type.GetType("System.Int32"))
                Me.presentationtab.Columns.Add("1930", System.Type.GetType("System.Int32"))
                Me.presentationtab.Columns.Add("2100", System.Type.GetType("System.Int32"))
                Me.presentationtab.Columns.Add("Others", System.Type.GetType("System.Int32"))
                Me.presentationtab.Columns.Add("Total", System.Type.GetType("System.Int32"))

                Me.datatable.Columns.Add("TRNTY", System.Type.GetType("System.String"))
                Me.datatable.Columns.Add("TRNNO", System.Type.GetType("System.String"))
                Me.datatable.Columns.Add("CustName", System.Type.GetType("System.String"))
                Me.datatable.Columns.Add("StartTime", System.Type.GetType("System.String"))
                Me.datatable.Columns.Add("EndTime", System.Type.GetType("System.String"))
                Me.datatable.Columns.Add("ZoneID", System.Type.GetType("System.String"))
                Me.datatable.Columns.Add("CRETM", System.Type.GetType("System.DateTime"))
                Me.datatable.Columns.Add("CRID", System.Type.GetType("System.String"))
                Me.datatable.Columns.Add("TCHID", System.Type.GetType("System.String"))
                Me.datatable.Columns.Add("CUSTID", System.Type.GetType("System.String"))

            End If
        End If
    End Sub

    Private Function processData() As Integer
        '*******  validatation begin *********
        If ds Is Nothing Then
            Return -1
        End If
        If ds.Tables.Count < 2 Then
            Return 0
        End If

        '*******  modify datatable *********
        alterDataTable()

        '*******    process data   *********
        Dim rows1 As DataRow()
        Dim tbl As DataTable
        Dim newrow As DataRow
        Dim zoneid As String
        Dim format As New CultureInfo("fr-FR", True)

        Dim timezones As String() = { _
             "0900", "1030", "1200", "1330", "1500", "1630", "1800", "1930", "2100" _
        }

        Me.RealTotalAppt = Me.ds.Tables(0).Rows.Count
        If Me.ds.Tables(2).Rows(0).Item(0) IsNot DBNull.Value Then
            Me.ObjTotalAppt = Me.ds.Tables(2).Rows(0).Item(0)
        Else
            Me.ObjTotalAppt = 0
        End If

        If ds.Tables(3).Rows.Count > 0 Then
            For c As Integer = 0 To ds.Tables(3).Rows.Count - 1
                If ds.Tables(3).Rows(c).Item(0) = "RS" Then
                    Me.fintTotalAS = Val(ds.Tables(3).Rows(c).Item(1))
                End If

                If ds.Tables(3).Rows(c).Item(0) = "RS" Then
                    Me.fintTotalSS = Val(ds.Tables(3).Rows(c).Item(1))
                End If
            Next
        End If

        For i As Integer = 0 To ds.Tables(1).Rows.Count - 1
            zoneid = ds.Tables(1).Rows(i).Item(0)  '??zoneid??????
            '??newrow
            newrow = Me.presentationtab.NewRow()
            Dim othersIndex As Integer = Me.presentationtab.Columns.IndexOf("Others")
            Dim totalIndex As Integer = Me.presentationtab.Columns.IndexOf("Total")
            'newrow ???  
            newrow.Item(0) = zoneid
            For j As Integer = 1 To totalIndex
                If j = 1 Or j = 8 Or j = 9 Then
                    newrow.Item(j) = DBNull.Value
                    'newrow.Item(j) = 99
                Else
                    newrow.Item(j) = 0
                End If
            Next

            '??newrow????
            rows1 = ds.Tables(0).Select("FAPT_ZONID='" + zoneid + "'", "FAPT_CREDT Asc")
            Dim norapptcount As Integer = 0
            For j As Integer = 0 To timezones.Length - 1
                tbl = Me.getApptByStrTm(zoneid, timezones(j).ToString(), 1)
                If tbl Is Nothing Then
                    Continue For
                End If
                If tbl.Rows.Count > 0 And norapptcount < Me.MaxApptAllocNum Then
                    If norapptcount + tbl.Rows.Count <= Me.MaxApptAllocNum Then
                        newrow.Item(j + 2) = tbl.Rows.Count
                        norapptcount += tbl.Rows.Count
                    Else
                        newrow.Item(j + 2) = Me.MaxApptAllocNum - norapptcount
                        norapptcount = Me.MaxApptAllocNum
                    End If
                    For k As Integer = 0 To newrow.ItemArray(j + 2) - 1
                        For m As Integer = 0 To rows1.Length - 1
                            If rows1(m).ItemArray(0) = tbl.Rows(k).ItemArray(0) _
                               And rows1(m).ItemArray(1) = tbl.Rows(k).ItemArray(1) Then
                                rows1(m).Item(Me.tabindex0.IsInOthers) = 0
                            End If
                        Next
                    Next
                Else
                    newrow.Item(j + 2) = DBNull.Value
                End If
            Next

            If rows1.Length > norapptcount Then
                newrow.Item(othersIndex) = rows1.Length - norapptcount
            Else
                newrow.Item(othersIndex) = DBNull.Value
            End If

            newrow.Item(totalIndex) = rows1.Length
            '?newrow??datatable?
            Me.presentationtab.Rows.Add(newrow)
        Next

    End Function

    Public Function getApptStatView(ByVal userid As String, ByVal apptdate As String, _
                             ByVal svcid As String, ByRef result As Integer) As DataTable
        Me.UserID = userid
        Dim format As New System.Globalization.CultureInfo("fr-FR", True)
        Me.AppointDate = DateTime.Parse(apptdate, format, _
                  System.Globalization.DateTimeStyles.NoCurrentDateDefault _
        )
        Me.ServiceCenter = svcid

        result = Me.getInfo(userid, apptdate, svcid)
        If result = 1 Then
            Me.processData()
        End If
        Return Me.presentationtab
    End Function

    Public Function getTotalAppt() As ArrayList
        Dim array As New ArrayList()
        array.Add(Me.RealTotalAppt)
        array.Add(Me.ObjTotalAppt)
        Return array
    End Function

    Public Function getApptByStrTm(ByVal zoneid As String) As DataTable
        Dim searchstr As String = "FAPT_ZONID='" + zoneid + "'"
        Return Me.Search(searchstr)
    End Function

    Public Function getApptByStrTm(ByVal zoneid As String, ByVal IsInOthers As Integer) As DataTable
        Dim searchstr As String = "FAPT_ZONID='" + zoneid + "'" + _
                             " and IsInOthers=" + IsInOthers.ToString()
        Return Search(searchstr)
    End Function

    Public Function getApptByStrTm(ByVal zoneid As String, ByVal starttime As String, _
                                 ByVal IsInOthers As Integer) As DataTable
        Dim searchstr As String = "FAPT_ZONID='" + zoneid + "'" + _
                                  " and FAPT_STRTM='" + starttime + "'" + _
                                  " and IsInOthers=" + IsInOthers.ToString()
        Return Search(searchstr)
    End Function

    Private Function Search(ByVal searchstr As String) As DataTable
        Dim rows As DataRow() = Nothing
        Dim sortorder As String = "FAPT_STRTM,MCUS_ENAME asc"
        Me.datatable.Rows.Clear()
        If ds IsNot Nothing Then
            If ds.Tables.Count > 0 Then
                If Me.ds.Tables(0).Rows.Count > 0 Then
                    rows = ds.Tables(0).Select(searchstr, sortorder)
                    For i As Integer = 0 To rows.Length - 1
                        Dim row As DataRow = datatable.NewRow()
                        For j As Integer = 0 To row.ItemArray.Length - 1
                            row.Item(j) = rows(i).ItemArray(j)
                        Next
                        datatable.Rows.Add(row)
                    Next
                End If
            End If
        End If

        Return datatable
    End Function

End Class
