﻿Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web
Imports BusinessEntity
Imports SQLDataAccess


Public Class ClsRptTSKH

#Region "Declaration"

    Private fstrSpName As String ' store procedure name
    Private fstrusername As String ' username

    Private fstrmintecid As String
    Private fstrmaxtecid As String

    Private fstrminserid As String
    Private fstrmaxserid As String

    Private fstrmindate As System.DateTime ' store procedure name
    Private fstrmaxdate As System.DateTime


    Private fstrctrid As String
    Private fstrcomid As String
    Private fstrsvcid As String
    Private fstrrank As String


#End Region
#Region "Properties"
    Public Property username() As String
        Get
            Return fstrusername
        End Get
        Set(ByVal Value As String)
            fstrusername = Value
        End Set
    End Property

    Public Property ctrid() As String
        Get
            Return fstrctrid
        End Get
        Set(ByVal Value As String)
            fstrctrid = Value
        End Set
    End Property
    Public Property comid() As String
        Get
            Return fstrcomid
        End Get
        Set(ByVal Value As String)
            fstrcomid = Value
        End Set
    End Property
    Public Property svcid() As String
        Get
            Return fstrsvcid
        End Get
        Set(ByVal Value As String)
            fstrsvcid = Value
        End Set
    End Property
    Public Property rank() As String
        Get
            Return fstrrank
        End Get
        Set(ByVal Value As String)
            fstrrank = Value
        End Set
    End Property


    Public Property Mintechid() As String
        Get
            Return fstrmintecid
        End Get
        Set(ByVal Value As String)
            fstrmintecid = Value
        End Set
    End Property

    Public Property Maxtechid() As String
        Get
            Return fstrmaxtecid
        End Get
        Set(ByVal Value As String)
            fstrmaxtecid = Value
        End Set
    End Property

    Public Property Minserid() As String
        Get
            Return fstrminserid
        End Get
        Set(ByVal Value As String)
            fstrminserid = Value
        End Set
    End Property

    Public Property Maxserid() As String
        Get
            Return fstrmaxserid
        End Get
        Set(ByVal Value As String)
            fstrmaxserid = Value
        End Set
    End Property

    Public Property Mindate() As String
        Get
            Return fstrmindate
        End Get
        Set(ByVal Value As String)
            fstrmindate = Value
        End Set
    End Property

    Public Property Maxdate() As String
        Get
            Return fstrmaxdate
        End Get
        Set(ByVal Value As String)
            fstrmaxdate = Value
        End Set
    End Property

    

#End Region
#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)

        connection.Open()

        Return connection
    End Function
#End Region
#Region "Report mas_TSKH"

    Public Function GetRptmas_TSKH() As DataSet

        Dim selConnection As SqlConnection = Nothing
        Dim selConn As SqlConnection = Nothing
        selConnection = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        selConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim trans As SqlTransaction = Nothing

        fstrSpName = "BB_RPTTSKH_VIEW"
        Try
            trans = selConnection.BeginTransaction()
            Dim Param() As SqlParameter = New SqlParameter(10) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(selConn, fstrSpName)
            Param(0).Value = Trim(fstrmintecid)
            Param(1).Value = Trim(fstrmaxtecid)
            Param(2).Value = Trim(fstrminserid)
            Param(3).Value = Trim(fstrmaxserid)
            Param(4).Value = Trim(fstrmindate)
            Param(5).Value = Trim(fstrmaxdate)

            Param(6).Value = Trim(fstrctrid)
            Param(7).Value = Trim(fstrcomid)
            Param(8).Value = Trim(fstrsvcid)
            Param(9).Value = Trim(fstrrank)

            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(selConn, CommandType.StoredProcedure, fstrSpName, Param)


            Return ds

        Catch ex As Exception
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "ClsRptTSKH.vb")

        Catch ex As SqlException
            trans.Rollback()
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Number).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrusername, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "ClsRptTSKH.vb")
        Finally
            selConnection.Close()
            selConn.Close()
        End Try

    End Function
#End Region
   

End Class
