#Region "Writing Infomation"
'******************************************************
'* --Funtion:allocate appointment to technicians.
'*|--Author:nealliu(???)
'*|--CreateTime:2006-4-25
'*|--Last Modified Time:2006-5-8
'*|--Version:1.0
'******************************************************
#End Region

#Region "Imports"
Imports System
Imports System.IO
Imports System.Configuration
Imports System.Data
Imports System.Data.SqlClient
Imports SQLDataAccess
Imports BusinessEntity
#End Region

#Region "Structure Declaration"

Public Structure PersonInfoRS
    Public ID As String
    Public Name As String
    Public PO As String
    Public ZoneID As String
    Public AreaID As String
    Public addr1 As String
    Public addr2 As String
End Structure

Public Structure IndexRS
    Public TransType As Integer
    Public TransNO As Integer
    Public CustPF As Integer
    Public CustID As Integer
    Public CustName As Integer
    Public StartTime As Integer
    Public EndTime As Integer
    Public TechID As Integer
    Public ZoneID As Integer
    Public Stat As Integer
    Public Modified As Integer
End Structure

#End Region

#Region "Class Declaration"

Public Class clsApptAllocRS

#Region "Declaration"
    Private tablenames As String()
    Private ds As DataSet
    Private spParams As SqlParameter()
    Private tabIndex As Index
    Private errorMessage As String
    Private userID As String
    Private UnknownZone As String

    Private _sessionID As String
    Private _IPaddr As String

    Private fstrAppointmentNo As String
    Private fstrCustomerID As String
    Private fstrSorting As String = "FAPT_ZONID ASC"
    Private _techorsub As String

#End Region

#Region "Properties"

    Public Property AppointmentNo() As String
        Get
            Return fstrAppointmentNo
        End Get
        Set(ByVal Value As String)
            fstrAppointmentNo = Value
        End Set
    End Property

    Public Property CustomerID() As String
        Get
            Return fstrCustomerID
        End Get
        Set(ByVal Value As String)
            fstrCustomerID = Value
        End Set
    End Property


    Public Property SessionID() As String
        Get
            Return _sessionID
        End Get
        Set(ByVal Value As String)
            _sessionID = Value
        End Set
    End Property
    Public Property IPaddr() As String
        Get
            Return _IPaddr
        End Get
        Set(ByVal Value As String)
            _IPaddr = Value
        End Set
    End Property
    Public Property TechorSub() As String
        Get
            Return _techorsub
        End Get
        Set(ByVal Value As String)
            _techorsub = Value
        End Set
    End Property

#End Region

#Region "Methods"

#Region "New"

    Public Sub New()
        Me.tablenames = New String() { _
                  New String("tabUnassigned"), _
                  New String("tabZoneStat") _
                 }
        Me.ds = New DataSet()
        Me.errorMessage = Nothing
        Me.UnknownZone = "Unknown"
    End Sub

    Public Sub New(ByVal userid As String)
        Me.tablenames = New String() { _
                  New String("tabUnassigned"), _
                  New String("tabZoneStat") _
                 }
        Me.ds = New DataSet()
        Me.userID = userid
        Me.errorMessage = Nothing
        Me.UnknownZone = "Unknown"
    End Sub

#End Region

#Region "Fill Parameters"

    '*********************************************************************************
    '* Function: ???????Parameter???
    '*********************************************************************************
    Private Sub FillParameters(ByVal UserID As String, ByVal ApptDate As String, _
                                ByVal SVC As String, ByVal FromZone As String, _
                                ByVal ToZone As String, ByVal TechID As String, ByVal AppointmentNo As String, ByVal CustomerID As String)
        Me.spParams = New SqlParameter() { _
            New SqlParameter("@UserID", SqlDbType.VarChar, 20), _
            New SqlParameter("@ApptDate", SqlDbType.VarChar, 10), _
            New SqlParameter("@SVCID", SqlDbType.VarChar, 10), _
            New SqlParameter("@FromZoneID", SqlDbType.VarChar, 10), _
            New SqlParameter("@ToZoneID", SqlDbType.VarChar, 10), _
             New SqlParameter("@TechID", SqlDbType.VarChar, 50), _
             New SqlParameter("@AppointmentNo", SqlDbType.VarChar, 50), _
             New SqlParameter("@CustomerID", SqlDbType.VarChar, 50), _
           New SqlParameter("@ReturnValue", SqlDbType.Int, 4) _
        }
        Me.spParams(0).Value = UserID
        Me.spParams(1).Value = ApptDate
        Me.spParams(2).Value = SVC
        Me.spParams(3).Value = FromZone
        Me.spParams(4).Value = ToZone
        Me.spParams(5).Value = TechID
        Me.spParams(6).Value = AppointmentNo
        Me.spParams(7).Value = CustomerID
        Me.spParams(8).Direction = ParameterDirection.Output
    End Sub

#End Region

#Region "Fill DataSet"
    Function FillDataSet(ByVal ApptDate As String, ByVal SVC As String, _
                   ByVal FromZone As String, ByVal ToZone As String, ByVal TechID As String, ByVal AppointmentNo As String, ByVal CustomerID As String, ByVal _techorsub As String) As Integer
        Dim result As Integer
        FillParameters(Me.userID, ApptDate, SVC, FromZone, ToZone, TechID, AppointmentNo, CustomerID)
        Try
            Me.Clear()
            If _techorsub = "Technician" Then
                SqlHelper.FillDataset(ConfigurationSettings.AppSettings("ConnectionString"), _
                                                CommandType.StoredProcedure, "BB_FNCAPPS_Sel_RS", Me.ds, _
                                                tablenames, Me.spParams)
                result = Me.spParams(8).Value
            Else
                SqlHelper.FillDataset(ConfigurationSettings.AppSettings("ConnectionString"), _
                                                                CommandType.StoredProcedure, "BB_FNCAPPS_Sel_RS_Con", Me.ds, _
                                                                tablenames, Me.spParams)
                result = Me.spParams(8).Value
            End If

        Catch ex As SqlException
            Me.errorMessage = ex.Message
            Me.ds.Clear()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(Me.userID, ex.Message.ToString, "BB_FNCAPPS_Sel_RS", _
                                WriteErrLog.ADD, "clsApptAllocRS.vb")
            Return 0
        End Try
        'get each data field index in table(0) in ds
        If Me.ds Is Nothing Then
            Return -1
        End If

        If Me.ds.Tables.Count > 0 Then
            Try
                Me.tabIndex.TransType = Me.ds.Tables(0).Columns.IndexOf("FAPT_TRNTY")
                Me.tabIndex.TransNO = Me.ds.Tables(0).Columns.IndexOf("FAPT_TRNNO")
                Me.tabIndex.CustPF = Me.ds.Tables(0).Columns.IndexOf("FAPT_CUSPF")
                Me.tabIndex.CustID = Me.ds.Tables(0).Columns.IndexOf("FAPT_CUSID")
                Me.tabIndex.CustName = Me.ds.Tables(0).Columns.IndexOf("MCUS_ENAME")
                Me.tabIndex.StartTime = Me.ds.Tables(0).Columns.IndexOf("FAPT_STRTM")
                Me.tabIndex.EndTime = Me.ds.Tables(0).Columns.IndexOf("FAPT_ENDTM")
                Me.tabIndex.TechID = Me.ds.Tables(0).Columns.IndexOf("FAPT_TCHID")
                Me.tabIndex.ZoneID = Me.ds.Tables(0).Columns.IndexOf("FAPT_ZONID")
                Me.tabIndex.Stat = Me.ds.Tables(0).Columns.IndexOf("FAPT_STAT")
                '???????????????

                Dim delCol As DataColumn = Me.ds.Tables(0).Columns.Add("modified", System.Type.GetType("System.Int32"))
                Me.tabIndex.Modified = Me.ds.Tables(0).Columns.IndexOf("modified")
                For i As Integer = 0 To Me.ds.Tables(0).Rows.Count - 1
                    Me.ds.Tables(0).Rows(i).Item(Me.tabIndex.Modified) = 0
                    '?????Zone????????unknown zone
                    If Me.ds.Tables(0).Rows(i).ItemArray(Me.tabIndex.ZoneID).Equals(System.DBNull.Value) Then
                        Me.ds.Tables(0).Rows(i).Item(Me.tabIndex.ZoneID) = Me.UnknownZone
                    ElseIf Me.ds.Tables(0).Rows(i).ItemArray(Me.tabIndex.ZoneID).ToString().Trim().Equals("") Then
                        Me.ds.Tables(0).Rows(i).Item(Me.tabIndex.ZoneID) = Me.UnknownZone
                    End If
                Next

                '???zone???????0??????0
                For j As Integer = 0 To Me.ds.Tables(1).Rows.Count - 1
                    If ds.Tables(1).Rows(j).ItemArray(2).Equals(System.DBNull.Value) Then
                        ds.Tables(1).Rows(j).Item(2) = 0
                    End If
                Next
            Catch ex As Exception
                result = 0
            End Try
        End If

        Return result
    End Function
#End Region

#Region "Get Unassigned Appointment"
    '*********************************************************************************
    '* Function: ??????????????????
    '*********************************************************************************
    Function GetUnAssignedAppt() As DataRow()
        If Me.ds IsNot Nothing Then
            If Me.ds.Tables.Count > 1 Then
                If Me.ds.Tables(0).Rows.Count > 0 Then
                    Return Me.ds.Tables(0).Select("FAPT_STAT = 'SA' OR FAPT_STAT = 'TG'", fstrSorting)
                End If
            End If
        End If

        Return Nothing
    End Function

    Function GetUnAssignedAppt(ByVal zoneid As String) As DataRow()
        If Me.ds IsNot Nothing Then
            If Me.ds.Tables.Count > 1 Then
                If Me.ds.Tables(0).Rows.Count > 0 Then
                    Return Me.ds.Tables(0).Select("FAPT_ZONID='" + zoneid + "'" + " and (FAPT_STAT = 'SA' OR FAPT_STAT = 'TG')", fstrSorting)
                End If
            End If
        End If

        Return Nothing
    End Function

    Function GetUnAssignedAppt(ByVal transtype As String, ByVal transno As String) As DataRow()
        If Me.ds IsNot Nothing Then
            If Me.ds.Tables.Count > 1 Then
                If Me.ds.Tables(0).Rows.Count > 0 Then

                    Return Me.ds.Tables(0).Select("FAPT_TRNTY='" + transtype + "'" + _
                                                  " and FAPT_TRNNO ='" + transno + "'" + _
                                                  " and (FAPT_STAT = 'SA' OR FAPT_STAT = 'TG')", fstrSorting)
                End If
            End If
        End If
        Return Nothing
    End Function
#End Region

#Region "Get assigned Appointment"
    '*********************************************************************************
    '* Function: ??????????????????
    '*********************************************************************************
    Function GetAssignedAppt(ByVal searchStr As String) As DataRow()
        If Me.ds IsNot Nothing Then
            If Me.ds.Tables.Count > 1 Then
                If Me.ds.Tables(0).Rows.Count > 0 Then
                    searchStr += " and (FAPT_STAT = 'SM')"
                    Return Me.ds.Tables(0).Select(searchStr, fstrSorting)
                End If
            End If
        End If

        Return Nothing
    End Function

    Function GetAssignedAppt() As DataRow()
        If Me.ds IsNot Nothing Then
            If Me.ds.Tables.Count > 1 Then
                If Me.ds.Tables(0).Rows.Count > 0 Then
                    Return Me.ds.Tables(0).Select("FAPT_STAT = 'SM'", fstrSorting)
                End If
            End If
        End If

        Return Nothing
    End Function
#End Region

#Region "GetModifiedAppt()"
    '*********************************************************************************
    '* Function: ??????????
    '*********************************************************************************
    Function GetModifiedAppt() As DataRow()
        If Me.ds IsNot Nothing Then
            If Me.ds.Tables.Count > 1 Then
                If Me.ds.Tables(0).Rows.Count > 0 Then
                    Return Me.ds.Tables(0).Select("modified=1")
                End If
            End If
        End If

        Return Nothing
    End Function
#End Region

#Region "Get Zone Appointment Stat."
    '*********************************************************************************
    '* Function: ???????????Zone????????
    '*********************************************************************************
    Function GetApptStat() As DataTable
        If ds IsNot Nothing Then
            If Me.ds.Tables.Count > 1 Then
                Return Me.ds.Tables(1)
            End If
        End If

        Return Nothing
    End Function

#End Region

#Region "Add Appointment"
    '*********************************************************************************
    '* Function: ??????????????
    '*********************************************************************************
    Function AddAppt(ByVal tranType As String, ByVal tranNO As String, ByVal techID As String) As Integer
        Dim rows As DataRow()

        Try
            rows = Me.ds.Tables(0).Select("FAPT_TRNTY='" + tranType + "'" + " and " + _
                           "FAPT_TRNNO='" + tranNO + "'")
            rows(0).Item(tabIndex.Modified) = 1
            rows(0).Item(tabIndex.TechID) = techID
            rows(0).Item(tabIndex.Stat) = "SM"

            Dim zoneid As String = rows(0).ItemArray(tabIndex.ZoneID).ToString()
            rows = Me.ds.Tables(1).Select("FAPT_ZONID='" + zoneid + "'")
            rows(0).Item(2) = Integer.Parse(rows(0).Item(2)) - 1
            Return 1
        Catch ex As Exception
            Return -1
        End Try
    End Function
#End Region

#Region "Remove Appointment"
    '*********************************************************************************
    '* Function: ???????????????
    '*********************************************************************************
    Function RemoveAppt(ByVal tranType As String, ByVal tranNO As String) As Integer
        Dim rows As DataRow()

        Try
            rows = Me.ds.Tables(0).Select("FAPT_TRNTY='" + tranType + "' and " + _
                           "FAPT_TRNNO='" + tranNO + "'")
            rows(0).Item(tabIndex.Modified) = 1
            rows(0).Item(tabIndex.TechID) = DBNull.Value
            rows(0).Item(tabIndex.Stat) = "SA"

            Dim zoneid As String = rows(0).ItemArray(tabIndex.ZoneID)
            rows = Me.ds.Tables(1).Select("FAPT_ZONID='" + zoneid + "'")
            rows(0).Item(2) = Integer.Parse(rows(0).Item(2)) + 1
            Return 1
        Catch ex As Exception
            Return -1
        End Try
    End Function
#End Region

#Region "Get Technicians in certain Service center on certain date"
    '*********************************************************************************
    '* Function: ?????????????????
    '*********************************************************************************
    Function GetTechBySVC() As DataTable
        If Me.ds IsNot Nothing Then
            If Me.ds.Tables.Count > 1 Then
                Return Me.ds.Tables(2)
            End If
        End If

        Return Nothing
    End Function
#End Region

#Region "Save Technician's Appointments"
    '*********************************************************************************
    '* Function: ??????????
    '*********************************************************************************
    Function SaveTechAppt() As Integer
        If Me.ds Is Nothing Then
            Return -1
        End If
        If Me.ds.Tables.Count < 1 Then
            Return -1
        End If

        Dim sp As SqlParameter() = {}
        Dim result As Integer = 1
        For i As Integer = 0 To Me.ds.Tables(0).Rows.Count - 1
            Dim row As DataRow = Me.ds.Tables(0).Rows(i)
            If Me.ds.Tables(0).Rows(i).ItemArray(tabIndex.Modified) = 1 Then
                Try
                    sp = ResetSP()
                    sp(0).Value = Me.userID
                    sp(1).Value = Me.ds.Tables(0).Rows(i).ItemArray(tabIndex.TransType)
                    sp(2).Value = Me.ds.Tables(0).Rows(i).ItemArray(tabIndex.TransNO)
                    sp(3).Value = Me.ds.Tables(0).Rows(i).ItemArray(tabIndex.TechID)
                    sp(4).Value = Me.IPaddr
                    sp(5).Value = Me.SessionID
                    sp(6).Direction = ParameterDirection.Output

                    SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings("ConnectionString"), _
                                            CommandType.StoredProcedure, "BB_FNCAPPS_UpdApptAlloc", _
                                            sp)
                    If sp(6).Value <> 1 Then
                        result = 0
                    End If
                Catch ex As SqlException
                    Me.errorMessage = ex.Message
                    Dim WriteErrLog As New clsLogFile()
                    WriteErrLog.ErrorLog(Me.userID, ex.Message.ToString, "BB_FNCAPPS_UpdApptAlloc", _
                                        WriteErrLog.ADD, "clsApptAllocRS.vb")
                    result = -1
                Catch ex As Exception
                    Me.errorMessage = ex.Message
                    Dim WriteErrLog As New clsLogFile()
                    WriteErrLog.ErrorLog(Me.userID, ex.Message.ToString, "BB_FNCAPPS_UpdApptAlloc", _
                                        WriteErrLog.ADD, "clsApptAllocRS.vb")
                    result = -1
                End Try
            End If
        Next

        Return result
    End Function
#End Region

    Protected Function ResetSP() As SqlParameter()
        Dim Param() As SqlParameter = New SqlParameter() { _
                    New SqlParameter("@UserID", SqlDbType.VarChar, 20), _
                    New SqlParameter("@TranType", SqlDbType.VarChar, 10), _
                    New SqlParameter("@TranNO", SqlDbType.VarChar, 20), _
                    New SqlParameter("@TechID", SqlDbType.VarChar, 10), _
                    New SqlParameter("@IPaddr", SqlDbType.VarChar, 50), _
                    New SqlParameter("@SessionID", SqlDbType.VarChar, 50), _
                    New SqlParameter("@RetVal", SqlDbType.Int, 4) _
                }

        Return Param
    End Function
#Region "Get Zones in certain service center"
    '*********************************************************************************
    '* Function: ???????????Zone?
    '*********************************************************************************
    Function GetZonesBySVC() As DataTable
        If Me.ds IsNot Nothing Then
            If Me.ds.Tables.Count > 1 Then
                Return Me.ds.Tables(3)
            End If
        End If
        Return Nothing
    End Function
#End Region

#Region "Get areas in certain service center"
    '*********************************************************************************
    '* Function: ???????????Area?
    '*********************************************************************************
    Function GetAreasBySVC() As DataTable
        If Me.ds IsNot Nothing Then
            If Me.ds.Tables.Count > 1 Then
                Return Me.ds.Tables(4)
            End If
        End If

        Return Nothing
    End Function
#End Region

#Region "Get Customer's Infomation"
    '*********************************************************************************
    'Function: ???????????????????
    '*********************************************************************************
    Function GetCustInfo(ByVal transType As String, ByVal transNo As String) As PersonInfo
        Dim custinfo As PersonInfo = Nothing, reader As SqlDataReader = Nothing
        Dim sp As SqlParameter()
        sp = New SqlParameter() { _
            New SqlParameter("@transType", SqlDbType.VarChar, 10), _
            New SqlParameter("@transNo", SqlDbType.VarChar, 20) _
        }
        sp(0).Value = transType
        sp(1).Value = transNo
        Try
            reader = SqlHelper.ExecuteReader(ConfigurationSettings.AppSettings("ConnectionString"), _
                                                  CommandType.StoredProcedure, "BB_FNCAPPS_CustInfo", _
                                                  sp)
            If (reader.Read()) Then
                custinfo.ID = reader.Item(1).ToString()
                custinfo.Name = reader.Item(2).ToString()
                custinfo.addr1 = reader.Item(3).ToString()
                custinfo.addr2 = reader.Item(4).ToString()
                custinfo.PO = reader.Item(5).ToString()
                custinfo.AreaID = reader.Item(6).ToString()
                custinfo.ZoneID = reader.Item(7).ToString()
            End If
            reader.Close()
        Catch ex As SqlException
            custinfo = Nothing
            Me.errorMessage = ex.Message
            reader.Close()
        End Try

        Return custinfo
    End Function
#End Region

#Region "Save Customer's Infomation"
    '*********************************************************************************
    '* Function: ???????????
    '*********************************************************************************
    Function SaveCustInfo(ByVal transType As String, ByVal transNo As String, _
                        ByVal AreaID As String, ByVal PO As String, _
                        ByVal ZoneID As String) As Integer
        Dim result As Integer
        Try
            Dim sp As SqlParameter()
            sp = New SqlParameter() { _
                New SqlParameter("@UserID", SqlDbType.VarChar, 20), _
                New SqlParameter("@TranType", SqlDbType.VarChar, 10), _
                New SqlParameter("@TranNO", SqlDbType.VarChar, 20), _
                New SqlParameter("@PO", SqlDbType.VarChar, 10), _
                New SqlParameter("@AreaID", SqlDbType.VarChar, 10), _
                New SqlParameter("@ZoneID", SqlDbType.VarChar, 10), _
                New SqlParameter("@SessionID", SqlDbType.VarChar, 50), _
                New SqlParameter("@IPaddr", SqlDbType.VarChar, 50), _
                New SqlParameter("@RetVal", SqlDbType.Int, 4) _
            }
            sp(0).Value = Me.userID
            sp(1).Value = transType
            sp(2).Value = transNo
            sp(3).Value = PO
            sp(4).Value = AreaID
            sp(5).Value = ZoneID
            sp(6).Value = Me.SessionID
            sp(7).Value = Me.IPaddr
            sp(8).Direction = ParameterDirection.Output
            SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings("ConnectionString"), _
                                      CommandType.StoredProcedure, "BB_FNCAPPS_SaveCust", _
                                      sp)
            result = sp(8).Value
        Catch ex As SqlException
            result = 0
            Me.errorMessage = ex.Message
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(Me.userID, ex.Message.ToString, "BB_FNCAPPS_SaveCust", WriteErrLog.ADD, "clsApptAllocRS.vb")
        Catch ex As Exception
            result = 0
            Me.errorMessage = ex.Message
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(Me.userID, ex.Message.ToString, "BB_FNCAPPS_SaveCust", WriteErrLog.ADD, "clsApptAllocRS.vb")
        End Try

        Return result
    End Function

#End Region

#Region "Print Technician's Appointment"
    '*********************************************************************************
    '* Function: ???????????
    '*********************************************************************************
    Function getPrintTechAppt(ByVal ApptDate As String, ByVal SVC As String, _
                              ByVal FromZone As String, ByVal ToZone As String, _
                              ByVal TechID As String, ByRef result As Integer, ByVal AppointmentNo As String, ByVal CustomerID As String) As DataSet
        Dim ds1 As DataSet = Nothing
        FillParameters(Me.userID, ApptDate, SVC, FromZone, ToZone, TechID, AppointmentNo, CustomerID)
        Try
            ds1 = New DataSet()
            SqlHelper.FillDataset(ConfigurationSettings.AppSettings("ConnectionString"), _
                                CommandType.StoredProcedure, "BB_FNCAPPC_RptTechApptAlloc", _
                                ds1, tablenames, Me.spParams)
            result = Me.spParams(6).Value
        Catch ex As SqlException
            Me.errorMessage = ex.Message
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(Me.userID, ex.Message.ToString, "BB_FNCAPPC_RptTechApptAlloc", _
                                WriteErrLog.ADD, "clsApptAlloc.vb")
            result = -1
            ds1 = Nothing
        End Try

        Return ds1
    End Function

#End Region

#Region "get Index"
    '*********************************************************************************
    '* Function: ????????????
    '*********************************************************************************
    Function getIndex(ByVal i As Integer) As Index
        Return Me.tabIndex
    End Function
#End Region

#Region "Clear"
    '*********************************************************************************
    '* Function: ??ds?????????
    '*********************************************************************************
    Sub Clear()
        If Me.ds IsNot Nothing Then
            Me.ds.Tables.Clear()
            Me.ds.Clear()
        End If
        Me.errorMessage = Nothing
    End Sub

#End Region

#Region "Get Error Message"
    Function getErrorMessage() As String
        Return Me.errorMessage
    End Function
#End Region

#End Region

End Class

#End Region
