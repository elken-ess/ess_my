Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web
Imports BusinessEntity
Imports SQLDataAccess

Public Class clsRPTProductFeedbackSummary
#Region "Declaration"
    Private ServBillDateFr As Date
    Private ServBillDateTo As Date
    Private ServCtrFr As String
    Private ServCtrTo As String
    Private PriceId As String 'lly 20170512
    Private ModelId As String
    Private Status As String
    Private UserId As String

    Private fstrUserName As String
    Private fstrSpName As String

    Private fstrIsoDesc1 As String
    Private fstrIsoDesc2 As String
    Private fstrCol1 As String
#End Region

#Region "Properties"
    Public Property FB_ServBillDateFr() As Date
        Get
            Return ServBillDateFr
        End Get
        Set(ByVal value As Date)
            ServBillDateFr = value
        End Set
    End Property

    Public Property FB_ServBillDateTo() As Date
        Get
            Return ServBillDateTo
        End Get
        Set(ByVal value As Date)
            ServBillDateTo = value
        End Set
    End Property

    Public Property FB_ServCtrFr() As String
        Get
            Return ServCtrFr
        End Get
        Set(ByVal value As String)
            ServCtrFr = value
        End Set
    End Property

    Public Property FB_ServCtrTo() As String
        Get
            Return ServCtrTo
        End Get
        Set(ByVal value As String)
            ServCtrTo = value
        End Set
    End Property

    Public Property FB_PriceId() As String
        Get
            Return PriceId
        End Get
        Set(ByVal value As String)
            PriceId = value
        End Set
    End Property


    Public Property FB_ModelId() As String
        Get
            Return ModelId
        End Get
        Set(ByVal value As String)
            ModelId = value
        End Set
    End Property

    Public Property FB_Status() As String
        Get
            Return Status
        End Get
        Set(ByVal value As String)
            Status = value
        End Set
    End Property

    Public Property FB_Userid() As String
        Get
            Return UserId
        End Get
        Set(ByVal value As String)
            UserId = value
        End Set
    End Property

    Public Property IsoDesc1() As String
        Get
            Return fstrIsoDesc1
        End Get
        Set(ByVal Value As String)
            fstrIsoDesc1 = Value
        End Set
    End Property

    Public Property IsoDesc2() As String
        Get
            Return fstrIsoDesc2
        End Get
        Set(ByVal Value As String)
            fstrIsoDesc2 = Value
        End Set
    End Property

    Public Property Col1() As String
        Get
            Return fstrCol1
        End Get
        Set(ByVal Value As String)
            fstrCol1 = Value
        End Set
    End Property

#End Region

#Region "GetConnection"
    Private Shared Function GetConnection(ByVal connectionString As String) As SqlConnection
        Dim connection As New SqlConnection(connectionString)

        connection.Open()

        Return connection
    End Function
#End Region

#Region "Retrive Info"
    Public Function RetriveProductFeedbackSummary() As DataSet
        Dim sqlConn As SqlConnection = Nothing
        sqlConn = GetConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        fstrSpName = "BB_ProductFeedbackSummary"
        Dim ds As DataSet = New DataSet
        Try
            Dim Param() As SqlParameter = New SqlParameter(6) {}
            Param = SqlHelperParameterCache.GetSpParameterSet(sqlConn, fstrSpName)

            Param(0).Value = ServBillDateFr
            Param(1).Value = ServBillDateTo
            Param(2).Value = Trim(ServCtrFr)
            Param(3).Value = Trim(ServCtrTo)
            Param(4).Value = Trim(Status)
            Param(5).Value = Trim(PriceId)
            Param(6).Value = Trim(ModelId)
            Param(7).Value = Nothing
            Param(8).Value = Nothing
            Param(9).Value = Nothing

            ds = SqlHelper.ExecuteDataset(sqlConn, CommandType.StoredProcedure, fstrSpName, Param)

            IsoDesc1 = Param(7).Value
            IsoDesc2 = Param(8).Value
            Col1 = Param(9).Value

        Catch ex As Exception
            Dim ErrorLog As ArrayList = New ArrayList
            ErrorLog.Add("Error").ToString()
            ErrorLog.Add(ex.Message).ToString()
            Dim WriteErrLog As New clsLogFile()
            WriteErrLog.ErrorLog(fstrUserName, ErrorLog.Item(1).ToString, fstrSpName, WriteErrLog.SELE, "clsRPTProductFeedbackSummary.vb")
            Return Nothing
        Finally
            sqlConn.Close()
        End Try
        Return ds
    End Function

#End Region
End Class
